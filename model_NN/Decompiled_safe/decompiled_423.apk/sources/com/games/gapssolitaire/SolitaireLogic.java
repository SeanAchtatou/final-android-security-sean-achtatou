package com.games.gapssolitaire;

import android.content.Context;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;

public class SolitaireLogic {
    public static final int ROWS_NUMBER = 4;
    public static final int ROW_LENGTH = 10;
    public static final int SCORE_PER_SECOND = 1;
    public static final int SCORE_PER_SHUFFLE = 100;
    public static final int SCORE_PER_STEP = 5;
    public static final int START_POINTS = 1000;
    public int cardsInPlace = 0;
    public final FieldActivity fieldActivity;
    public Card[] field_array = new Card[40];
    public int score = START_POINTS;

    public SolitaireLogic(Context context) {
        this.fieldActivity = (FieldActivity) context;
    }

    public int getScore() {
        return this.score;
    }

    public void minusScore(int score2) {
        this.score -= score2;
    }

    public void shuffle() {
        shuffleCards();
        setGaps();
        minusScore(100);
    }

    public void shuffleCards() {
        int randomInt1;
        int randomInt2;
        Random randomGenerator = new Random();
        for (int i = 1; i <= 100; i++) {
            do {
                randomInt1 = randomGenerator.nextInt(40);
            } while (this.field_array[randomInt1].isInPlace());
            do {
                randomInt2 = randomGenerator.nextInt(40);
            } while (this.field_array[randomInt2].isInPlace());
            Card tempCard = this.field_array[randomInt1];
            this.field_array[randomInt1] = this.field_array[randomInt2];
            this.field_array[randomInt2] = tempCard;
        }
    }

    private void setGaps() {
        Stack<Integer> gapsStack = new Stack<>();
        LinkedList<Integer> nonReadyRowsList = new LinkedList<>();
        for (int i = 0; i < 4; i++) {
            nonReadyRowsList.add(Integer.valueOf(i));
        }
        for (int i2 = 0; i2 < 4; i2++) {
            for (int j = 0; j < 10; j++) {
                if (this.field_array[(i2 * 10) + j].value == 0) {
                    if (j == 0 || (this.field_array[((i2 * 10) + j) - 1].isInPlace() && this.field_array[((i2 * 10) + j) - 1].value != 0)) {
                        nonReadyRowsList.remove(new Integer(i2));
                    } else {
                        gapsStack.push(Integer.valueOf((i2 * 10) + j));
                    }
                }
            }
        }
        while (true) {
            int j2 = 0;
            if (!nonReadyRowsList.isEmpty()) {
                int row = ((Integer) nonReadyRowsList.poll()).intValue();
                while (this.field_array[(row * 10) + j2].isInPlace() && j2 < 9) {
                    j2++;
                }
                exchangeTwoCards(((Integer) gapsStack.pop()).intValue(), (row * 10) + j2);
            } else {
                return;
            }
        }
    }

    private void exchangeTwoCards(int first, int second) {
        Card tempCard = this.field_array[first];
        this.field_array[first] = this.field_array[second];
        this.field_array[second] = tempCard;
    }

    public boolean setLeastCardInPlace(int xIndex, int yIndex) {
        for (int i = 0; i < 40; i += 10) {
            if (this.field_array[i].value == 0) {
                exchangeTwoCards(i, (xIndex * 10) + yIndex);
                this.field_array[i].setInPlace();
                checkGameFinished();
                minusScore(5);
                return true;
            }
        }
        return false;
    }

    private void checkGameFinished() {
        increaseCardInPlaceCounter();
        if (getCardsInPlaceCount() == 36) {
            this.fieldActivity.finishGame();
        }
    }

    private void increaseCardInPlaceCounter() {
        this.cardsInPlace++;
    }

    private int getCardsInPlaceCount() {
        return this.cardsInPlace;
    }

    /* access modifiers changed from: protected */
    public boolean doStep(int xGap, int yGap) {
        boolean result = false;
        int targetIndex = 0;
        int gapIndex = (xGap * 10) + yGap;
        if (isValidToExchange(gapIndex)) {
            result = true;
            Card previousCard = this.field_array[gapIndex - 1];
            while (true) {
                if (this.field_array[targetIndex].value == previousCard.value + 1 && this.field_array[targetIndex].suit == previousCard.suit) {
                    break;
                }
                targetIndex++;
            }
            exchangeTwoCards(gapIndex, targetIndex);
            if (this.field_array[gapIndex - 1].isInPlace()) {
                this.field_array[gapIndex].setInPlace();
                int curIndex = gapIndex;
                while (this.field_array[curIndex + 1].value - 1 == this.field_array[curIndex].value && this.field_array[curIndex + 1].suit == this.field_array[curIndex].suit) {
                    this.field_array[curIndex + 1].setInPlace();
                    increaseCardInPlaceCounter();
                    curIndex++;
                }
                checkGameFinished();
            }
            minusScore(5);
        }
        return result;
    }

    private boolean isValidToExchange(int index) {
        if (index % 10 == 0 || this.field_array[index - 1].value == 9 || this.field_array[index - 1].value == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public int getCardValue(int x, int y) {
        if (x < 0 || x >= 4 || y < 0 || y >= 10) {
            return -1;
        }
        return this.field_array[(x * 10) + y].value;
    }

    /* access modifiers changed from: protected */
    public Card getCardObject(int x, int y) {
        if (x < 0 || x >= 4 || y < 0 || y >= 10) {
            return null;
        }
        return this.field_array[(x * 10) + y];
    }

    public void setInPlaceAfterInitShuffle() {
        for (int i = 0; i < 4; i++) {
            if (this.field_array[i * 10].value == 1) {
                this.field_array[i * 10].setInPlace();
                increaseCardInPlaceCounter();
            }
        }
    }

    /* access modifiers changed from: protected */
    public LinkedList<Card> findPossibleTargets(LinkedList<Integer> gapsArray) {
        LinkedList<Card> resultList = new LinkedList<>();
        while (!gapsArray.isEmpty()) {
            int gapIndex = gapsArray.getFirst().intValue();
            gapsArray.removeFirst();
            int targetIndex = 0;
            if (isValidToExchange(gapIndex)) {
                Card previousCard = this.field_array[gapIndex - 1];
                while (true) {
                    if (this.field_array[targetIndex].value == previousCard.value + 1 && this.field_array[targetIndex].suit == previousCard.suit) {
                        break;
                    }
                    targetIndex++;
                }
                Card cardToAdd = this.field_array[targetIndex];
                cardToAdd.x = targetIndex / 10;
                cardToAdd.y = targetIndex % 10;
                resultList.add(cardToAdd);
            }
        }
        return resultList;
    }

    /* access modifiers changed from: protected */
    public LinkedList<Card> findLeastCards() {
        LinkedList<Card> resultList = new LinkedList<>();
        int i = 0;
        while (true) {
            if (i >= 4) {
                break;
            } else if (this.field_array[i * 10].value == 0) {
                for (int index = 0; index < 40; index++) {
                    if (this.field_array[index].value == 1 && index % 10 != 0) {
                        Card cardToAdd = this.field_array[index];
                        cardToAdd.x = index / 10;
                        cardToAdd.y = index % 10;
                        resultList.add(cardToAdd);
                    }
                }
            } else {
                i++;
            }
        }
        return resultList;
    }

    /* access modifiers changed from: protected */
    public void generateFieldOfCards() {
        for (int suit = 0; suit < 4; suit++) {
            for (int value = 0; value < 10; value++) {
                this.field_array[(suit * 10) + value] = new Card(suit, value);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void generateFieldOfCardsWithTiles() {
        for (int suit = 0; suit < 4; suit++) {
            for (int value = 0; value < 10; value++) {
                this.field_array[(suit * 10) + value] = new Card(suit, value);
            }
        }
        Integer[] images = getImagesArray();
        for (int i = 0; i < 40; i++) {
            this.field_array[i].tile = this.fieldActivity.getResources().getDrawable(images[i].intValue());
        }
    }

    public static Integer[] getImagesArray() {
        return new Integer[]{Integer.valueOf((int) R.drawable.icon), Integer.valueOf((int) R.drawable.h6), Integer.valueOf((int) R.drawable.h7), Integer.valueOf((int) R.drawable.h8), Integer.valueOf((int) R.drawable.h9), Integer.valueOf((int) R.drawable.h10), Integer.valueOf((int) R.drawable.hj), Integer.valueOf((int) R.drawable.hq), Integer.valueOf((int) R.drawable.hk), Integer.valueOf((int) R.drawable.ha), Integer.valueOf((int) R.drawable.icon), Integer.valueOf((int) R.drawable.d6), Integer.valueOf((int) R.drawable.d7), Integer.valueOf((int) R.drawable.d8), Integer.valueOf((int) R.drawable.d9), Integer.valueOf((int) R.drawable.d10), Integer.valueOf((int) R.drawable.dj), Integer.valueOf((int) R.drawable.dq), Integer.valueOf((int) R.drawable.dk), Integer.valueOf((int) R.drawable.da), Integer.valueOf((int) R.drawable.icon), Integer.valueOf((int) R.drawable.c6), Integer.valueOf((int) R.drawable.c7), Integer.valueOf((int) R.drawable.c8), Integer.valueOf((int) R.drawable.c9), Integer.valueOf((int) R.drawable.c10), Integer.valueOf((int) R.drawable.cj), Integer.valueOf((int) R.drawable.cq), Integer.valueOf((int) R.drawable.ck), Integer.valueOf((int) R.drawable.ca), Integer.valueOf((int) R.drawable.icon), Integer.valueOf((int) R.drawable.s6), Integer.valueOf((int) R.drawable.s7), Integer.valueOf((int) R.drawable.s8), Integer.valueOf((int) R.drawable.s9), Integer.valueOf((int) R.drawable.s10), Integer.valueOf((int) R.drawable.sj), Integer.valueOf((int) R.drawable.sq), Integer.valueOf((int) R.drawable.sk), Integer.valueOf((int) R.drawable.sa)};
    }
}
