package com.games.gapssolitaire;

import android.graphics.drawable.Drawable;

public class Card {
    private boolean isInPlace;
    public int suit;
    public Drawable tile;
    public int value;
    public int x;
    public int y;

    public Card(int suit2, int value2, Drawable tile2) {
        this.suit = suit2;
        this.value = value2;
        this.tile = tile2;
        this.isInPlace = false;
    }

    public Card(int suit2, int value2) {
        this.suit = suit2;
        this.value = value2;
        this.tile = null;
        this.isInPlace = false;
    }

    public Card(int x2, int y2, int suit2, int value2, boolean isInPlace2) {
        this.x = x2;
        this.y = y2;
        this.suit = suit2;
        this.value = value2;
        this.tile = null;
        this.isInPlace = isInPlace2;
    }

    public boolean isInPlace() {
        return this.isInPlace;
    }

    public void setInPlace() {
        this.isInPlace = true;
    }

    public void offInPlace() {
        this.isInPlace = false;
    }
}
