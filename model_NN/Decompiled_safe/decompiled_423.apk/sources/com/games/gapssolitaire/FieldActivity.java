package com.games.gapssolitaire;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.Calendar;

public class FieldActivity extends Activity {
    public static final int FIELD_VIEW_ID = 1;
    public static final String MY_AD_UNIT_ID = "a14e45618b321da";
    public static final int SCORE_PER_SECOND = 1;
    private AdView adView;
    private Chronometer chronometer;
    private Dialog dialogFinishGame;
    /* access modifiers changed from: private */
    public Dialog dialogHowToPlay;
    /* access modifiers changed from: private */
    public Dialog dialogPause;
    private FieldView fieldView;
    private FileHelper fileHelper = new FileHelper();
    private boolean isGameOver = false;
    private boolean isNewGame = true;
    /* access modifiers changed from: private */
    public boolean isPause = false;
    private long lastPause;
    public Context mContext;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            if (!FieldActivity.this.isPause) {
                FieldActivity.this.mHandler.postDelayed(FieldActivity.this.mUpdateTimeTask, 3000);
            }
            FieldActivity.this.solitaire.minusScore(1);
            TextView textViewScore = (TextView) FieldActivity.this.findViewById(R.id.text_view_score);
            textViewScore.setText(String.valueOf(FieldActivity.this.solitaire.getScore()));
            textViewScore.invalidate();
        }
    };
    /* access modifiers changed from: private */
    public SolitaireLogic solitaire = new SolitaireLogic(this);
    public MediaPlayer stepPlayer;
    private int xScreenSize;
    private int yScreenSize;

    public SolitaireLogic getSolitaireObject() {
        return this.solitaire;
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.isGameOver) {
            saveGameData();
        } else {
            FileHelper.removeGameFiles(this);
        }
        this.isNewGame = false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.isNewGame) {
            initGame();
        }
    }

    public void onDestroy() {
        this.adView.destroy();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = this;
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.isGameOver = false;
        setContentView(R.layout.field);
        ViewGroup container = (ViewGroup) findViewById(R.id.field_layout);
        this.stepPlayer = MediaPlayer.create(this, R.raw.step);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        this.xScreenSize = metrics.heightPixels;
        this.yScreenSize = metrics.widthPixels;
        HeaderView headerView = new HeaderView(this);
        headerView.setLayoutParams(new LinearLayout.LayoutParams(this.yScreenSize, this.xScreenSize / 6));
        container.addView(headerView);
        this.fieldView = new FieldView(this);
        this.fieldView.setId(1);
        this.fieldView.setLayoutParams(new ViewGroup.LayoutParams(this.yScreenSize, (this.xScreenSize * 2) / 3));
        this.fieldView.setTvScoreToUpdate((TextView) findViewById(R.id.text_view_score));
        container.addView(this.fieldView);
        ((LinearLayout) findViewById(R.id.footer)).setLayoutParams(new LinearLayout.LayoutParams(this.yScreenSize, this.xScreenSize / 6));
        LinearLayout score_Layout = (LinearLayout) findViewById(R.id.score_layout);
        ViewGroup.LayoutParams score_layout_params = score_Layout.getLayoutParams();
        score_layout_params.width = (this.yScreenSize * 3) / 7;
        score_Layout.setLayoutParams(score_layout_params);
        ViewGroup.LayoutParams time_layout_params = score_Layout.getLayoutParams();
        time_layout_params.width = (this.yScreenSize * 3) / 7;
        ((LinearLayout) findViewById(R.id.time_layout)).setLayoutParams(time_layout_params);
        Typeface fontIronman2 = Typeface.createFromAsset(getAssets(), "Ironman2.ttf");
        ((TextView) findViewById(R.id.text_view_score_title)).setTypeface(fontIronman2);
        ((TextView) findViewById(R.id.text_view_score)).setTypeface(fontIronman2);
        ((TextView) findViewById(R.id.chronometer_title)).setTypeface(fontIronman2);
        ((TextView) findViewById(R.id.chronometer)).setTypeface(fontIronman2);
        this.solitaire.generateFieldOfCardsWithTiles();
        this.solitaire.shuffleCards();
        this.fieldView.invalidate();
        this.solitaire.setInPlaceAfterInitShuffle();
        this.chronometer = (Chronometer) findViewById(R.id.chronometer);
        this.chronometer.setBase(SystemClock.elapsedRealtime());
        this.chronometer.start();
        this.mHandler.removeCallbacks(this.mUpdateTimeTask);
        this.mHandler.postDelayed(this.mUpdateTimeTask, 3000);
        this.dialogHowToPlay = new Dialog(this);
        this.dialogPause = new Dialog(this);
        this.dialogFinishGame = new Dialog(this);
        if (!getIntent().getExtras().getBoolean("new_game")) {
            initGame();
            this.isNewGame = true;
        }
        LinearLayout ad_Layout = (LinearLayout) findViewById(R.id.ad_layout);
        ad_Layout.setLayoutParams(new LinearLayout.LayoutParams((this.yScreenSize * 19) / 32, this.xScreenSize / 6));
        AdView adView2 = new AdView(this, AdSize.BANNER, MY_AD_UNIT_ID);
        ad_Layout.addView(adView2);
        AdRequest request = new AdRequest();
        request.setTesting(false);
        adView2.loadAd(request);
    }

    public void menuButtonClick() {
        finish();
    }

    public void howToPlayClick() {
        stopGame();
        this.dialogHowToPlay.setTitle("How to play?");
        this.dialogHowToPlay.setContentView(((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate((int) R.layout.howtoplay, (ViewGroup) null), new ViewGroup.LayoutParams((this.yScreenSize * 7) / 8, (this.xScreenSize * 2) / 3));
        this.dialogHowToPlay.setCancelable(true);
        this.dialogHowToPlay.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                FieldActivity.this.resumeGame();
            }
        });
        ((LinearLayout) this.dialogHowToPlay.findViewById(R.id.howtoplay_text_layout)).setLayoutParams(new LinearLayout.LayoutParams((this.yScreenSize * 7) / 8, this.xScreenSize / 2));
        ((TextView) this.dialogHowToPlay.findViewById(R.id.text_howtoplay)).setTypeface(Typeface.createFromAsset(getAssets(), "Kitty Kat.ttf"));
        TextView textView_return = (TextView) this.dialogHowToPlay.findViewById(R.id.textView_return);
        textView_return.setTypeface(Typeface.createFromAsset(getAssets(), "Ironman2.ttf"));
        textView_return.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FieldActivity.this.dialogHowToPlay.cancel();
            }
        });
        this.dialogHowToPlay.show();
    }

    public void pauseButtonClick() {
        stopGame();
        this.dialogPause.setTitle("Pause");
        this.dialogPause.setContentView(((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate((int) R.layout.alert_view, (ViewGroup) null), new ViewGroup.LayoutParams((this.yScreenSize * 2) / 3, (this.xScreenSize * 4) / 7));
        this.dialogPause.setCancelable(true);
        this.dialogPause.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                FieldActivity.this.resumeGame();
            }
        });
        TextView textView_return = (TextView) this.dialogPause.findViewById(R.id.textView_resume_game);
        textView_return.setTypeface(Typeface.createFromAsset(getAssets(), "Ironman2.ttf"));
        textView_return.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FieldActivity.this.dialogPause.cancel();
            }
        });
        this.dialogPause.show();
    }

    public void shuffleButtonClick() {
        this.solitaire.shuffle();
        this.fieldView.invalidate();
    }

    public void finishGame() {
        stopGame();
        this.dialogFinishGame.setTitle("Congratulations!!!");
        Context context = this.mContext;
        Typeface fontHotplate = Typeface.createFromAsset(getAssets(), "Hotplate.ttf");
        this.dialogFinishGame.setContentView(((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.end_of_game, (ViewGroup) null), new ViewGroup.LayoutParams((this.yScreenSize * 2) / 3, this.xScreenSize / 2));
        this.dialogFinishGame.setCancelable(true);
        TextView textView_start_new = (TextView) this.dialogFinishGame.findViewById(R.id.textView_start_new);
        textView_start_new.setTypeface(fontHotplate);
        textView_start_new.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                Intent intent = new Intent(FieldActivity.this, FieldActivity.class);
                intent.putExtra("new_game", true);
                FieldActivity.this.startActivity(intent);
                FieldActivity.this.finish();
            }
        });
        TextView textView_main_menu = (TextView) this.dialogFinishGame.findViewById(R.id.textView_main_menu);
        textView_main_menu.setTypeface(fontHotplate);
        textView_main_menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FieldActivity.this.startActivity(new Intent(FieldActivity.this, GapsSolitaireActivity.class));
                FieldActivity.this.finish();
            }
        });
        this.fileHelper.saveScoreResult(this.mContext, this.solitaire.getScore(), Calendar.getInstance().getTime().toLocaleString());
        this.isGameOver = true;
        this.dialogFinishGame.show();
    }

    private void initGame() {
        GameData gameData = this.fileHelper.readGameData(this);
        if (gameData != null) {
            this.solitaire.cardsInPlace = gameData.cardsInPlace;
            this.solitaire.score = gameData.score;
            this.chronometer.stop();
            this.chronometer.setBase(SystemClock.elapsedRealtime() - gameData.duration);
            this.chronometer.start();
        }
        Card[] list = this.fileHelper.readField(this);
        if (list != null) {
            this.solitaire.field_array = list;
        }
    }

    private void saveGameData() {
        this.fileHelper.saveGameData(this, this.solitaire.cardsInPlace, this.solitaire.score, SystemClock.elapsedRealtime() - this.chronometer.getBase());
        this.fileHelper.saveField(this, this.solitaire.field_array);
    }

    /* access modifiers changed from: private */
    public void resumeGame() {
        this.isPause = false;
        this.chronometer.stop();
        this.chronometer.setBase((this.chronometer.getBase() + SystemClock.elapsedRealtime()) - this.lastPause);
        this.chronometer.start();
        this.mHandler.postDelayed(this.mUpdateTimeTask, 3000);
    }

    private void stopGame() {
        this.lastPause = SystemClock.elapsedRealtime();
        this.chronometer.stop();
        this.isPause = true;
    }
}
