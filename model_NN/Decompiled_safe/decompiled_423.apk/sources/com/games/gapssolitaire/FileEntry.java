package com.games.gapssolitaire;

public class FileEntry {
    public String date;
    public int score;

    FileEntry() {
    }

    FileEntry(int score2, String date2) {
        this.score = score2;
        this.date = date2;
    }
}
