package com.games.gapssolitaire;

import android.content.Context;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FileHelper {
    public static final String FILENAME_FIELD = "field";
    public static final String FILENAME_GAME_DATA = "game_data";
    public static final String FILENAME_SCORE = "score";
    public static final int MAX_ENTRIES_IN_SCORE_FILE = 10;

    public static boolean isSavedGameExist(Context context) {
        try {
            InputStream streamGameData = context.openFileInput(FILENAME_GAME_DATA);
            InputStream streamField = context.openFileInput(FILENAME_FIELD);
            if (streamGameData == null || streamField == null) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public static void removeGameFiles(Context context) {
        context.deleteFile(FILENAME_FIELD);
        context.deleteFile(FILENAME_GAME_DATA);
    }

    public static ArrayList<FileEntry> getScoreFileData(Context context) {
        boolean flag = true;
        ArrayList<FileEntry> listEntries = new ArrayList<>();
        try {
            InputStream inStream = context.openFileInput(FILENAME_SCORE);
            if (inStream != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
                int score = 0;
                while (true) {
                    String str = reader.readLine();
                    if (str == null) {
                        break;
                    } else if (flag) {
                        score = Integer.parseInt(str);
                        flag = false;
                    } else {
                        flag = true;
                        listEntries.add(new FileEntry(score, str));
                    }
                }
                inStream.close();
            }
            return listEntries;
        } catch (Throwable th) {
            return null;
        }
    }

    public void saveScoreResult(Context context, int score, String date) {
        ArrayList<FileEntry> listEntries = getScoreFileData(context);
        if (listEntries != null) {
            context.deleteFile(FILENAME_SCORE);
        }
        if (listEntries == null) {
            ArrayList<FileEntry> listEntries2 = new ArrayList<>();
            listEntries2.add(new FileEntry(score, date));
            writeScoreToFile(context, listEntries2);
            return;
        }
        int minScoreIndex = minValueIndex(listEntries, score);
        if (minScoreIndex != -1 || listEntries.size() < 10) {
            if (listEntries.size() < 10) {
                listEntries.add(new FileEntry(score, date));
            } else {
                listEntries.set(minScoreIndex, new FileEntry(score, date));
            }
            Collections.sort(listEntries, new Comparator<FileEntry>() {
                public int compare(FileEntry o1, FileEntry o2) {
                    FileEntry p1 = o1;
                    FileEntry p2 = o2;
                    if (p1.score > p2.score) {
                        return -1;
                    }
                    if (p1.score < p2.score) {
                        return 1;
                    }
                    return 0;
                }
            });
            writeScoreToFile(context, listEntries);
        }
    }

    private void writeScoreToFile(Context context, ArrayList<FileEntry> listEntries) {
        String buffer = "";
        for (int i = 0; i < listEntries.size(); i++) {
            buffer = String.valueOf(buffer) + listEntries.get(i).score + "\n" + listEntries.get(i).date + "\n";
        }
        try {
            OutputStreamWriter outStream = new OutputStreamWriter(context.openFileOutput(FILENAME_SCORE, 2));
            outStream.write(buffer);
            outStream.close();
        } catch (Throwable th) {
        }
    }

    private int minValueIndex(ArrayList<FileEntry> listEntries, int score) {
        int minScore = Integer.MAX_VALUE;
        int minScoreIndex = -1;
        for (int i = 0; i < listEntries.size(); i++) {
            if (listEntries.get(i).score < minScore) {
                minScore = listEntries.get(i).score;
                minScoreIndex = i;
            }
        }
        if (minScore < score) {
            return minScoreIndex;
        }
        return -1;
    }

    public void saveGameData(Context context, int cardsInPlace, int score, long lastPause) {
        String buffer = String.valueOf(String.valueOf(cardsInPlace)) + "\n" + String.valueOf(score) + "\n" + String.valueOf(lastPause) + "\n";
        try {
            OutputStreamWriter outStream = new OutputStreamWriter(context.openFileOutput(FILENAME_GAME_DATA, 2));
            outStream.write(buffer);
            outStream.close();
        } catch (Throwable th) {
        }
    }

    public void saveField(Context context, Card[] field_array) {
        String buffer = "";
        for (int i = 0; i < field_array.length; i++) {
            buffer = String.valueOf(buffer) + String.valueOf(field_array[i].x) + "\n" + String.valueOf(field_array[i].y) + "\n" + String.valueOf(field_array[i].suit) + "\n" + String.valueOf(field_array[i].value) + "\n" + String.valueOf(field_array[i].isInPlace()) + "\n";
        }
        try {
            OutputStreamWriter outStream = new OutputStreamWriter(context.openFileOutput(FILENAME_FIELD, 2));
            outStream.write(buffer);
            outStream.close();
        } catch (Throwable th) {
        }
    }

    public Card[] readField(Context context) {
        Card[] field_array = new Card[40];
        try {
            InputStream inStream = context.openFileInput(FILENAME_FIELD);
            if (inStream != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
                Integer[] images = SolitaireLogic.getImagesArray();
                for (int i = 0; i < 40; i++) {
                    int x = Integer.valueOf(reader.readLine()).intValue();
                    int y = Integer.valueOf(reader.readLine()).intValue();
                    int suit = Integer.valueOf(reader.readLine()).intValue();
                    int value = Integer.valueOf(reader.readLine()).intValue();
                    field_array[i] = new Card(x, y, suit, value, Boolean.valueOf(reader.readLine()).booleanValue());
                    field_array[i].tile = context.getResources().getDrawable(images[(suit * 10) + value].intValue());
                }
                inStream.close();
            }
            return field_array;
        } catch (Throwable th) {
            return null;
        }
    }

    public GameData readGameData(Context context) {
        try {
            InputStream inStream = context.openFileInput(FILENAME_GAME_DATA);
            if (inStream == null) {
                return null;
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
            GameData result = new GameData(Integer.valueOf(reader.readLine()).intValue(), Integer.valueOf(reader.readLine()).intValue(), Long.valueOf(reader.readLine()).longValue());
            try {
                inStream.close();
                return result;
            } catch (Throwable th) {
                return result;
            }
        } catch (Throwable th2) {
            return null;
        }
    }
}
