package com.games.gapssolitaire;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import java.util.LinkedList;

public class FieldView extends View {
    public static final int CARDS_PADDING = 3;
    public static final int ROWS_NUMBER = 4;
    public static final int ROW_LENGTH = 10;
    private final FieldActivity fieldActivity;
    private float height;
    private int selectedX = -1;
    private int selectedY = -1;
    private final SolitaireLogic solitaire;
    private TextView textViewScore;
    private float width;

    public FieldView(Context context) {
        super(context);
        this.fieldActivity = (FieldActivity) context;
        this.solitaire = this.fieldActivity.getSolitaireObject();
        setFocusable(true);
    }

    public int getCanvasWidth() {
        return (int) this.width;
    }

    public int getCanvasHeight() {
        return (int) this.height;
    }

    public void setTvScoreToUpdate(TextView tv) {
        this.textViewScore = tv;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.width = ((float) w) / 13.0f;
        this.height = ((float) h) / 4.0f;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.textViewScore.setText(String.valueOf(this.solitaire.getScore()));
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 10; j++) {
                Card currentCard = this.solitaire.getCardObject(i, j);
                if (currentCard.value != 0) {
                    Drawable tile = currentCard.tile;
                    tile.setBounds((int) Math.floor(((double) (((float) j) * this.width)) + (1.5d * ((double) this.width)) + 3.0d), (int) Math.floor((double) ((((float) i) * this.height) + 3.0f)), (int) Math.floor((((double) (((float) j) * this.width)) + (2.5d * ((double) this.width))) - 3.0d), (int) Math.floor((double) (((((float) i) * this.height) + this.height) - 3.0f)));
                    tile.draw(canvas);
                } else {
                    if (j != 0) {
                        int previous_card_value = this.solitaire.getCardObject(i, j - 1).value;
                        if (previous_card_value == 9 || previous_card_value == 0) {
                            Paint paint = new Paint();
                            paint.setARGB(120, 255, 100, 180);
                            canvas.drawRect(new Rect((int) Math.floor(((double) (((float) j) * this.width)) + (1.5d * ((double) this.width)) + 3.0d), (int) Math.floor((double) ((((float) i) * this.height) + 3.0f)), (int) Math.floor((((double) (((float) j) * this.width)) + (2.5d * ((double) this.width))) - 3.0d), (int) Math.floor((double) (((((float) i) * this.height) + this.height) - 3.0f))), paint);
                        }
                    }
                    Paint paint2 = new Paint();
                    paint2.setARGB(250, 0, 0, 0);
                    drawPath(canvas, paint2, (float) ((int) (((double) (((float) j) * this.width)) + (1.5d * ((double) this.width)) + 3.0d)), (float) ((int) ((((double) (((float) j) * this.width)) + (2.5d * ((double) this.width))) - 3.0d)), (((float) i) * this.height) + 3.0f, ((((float) i) * this.height) + this.height) - 3.0f);
                }
            }
        }
        LinkedList<Integer> gapsArray = new LinkedList<>();
        for (int i2 = 0; i2 < 4; i2++) {
            for (int j2 = 0; j2 < 10; j2++) {
                if (this.solitaire.getCardValue(i2, j2) == 0) {
                    gapsArray.add(Integer.valueOf((i2 * 10) + j2));
                }
            }
        }
        Redraw(canvas, this.solitaire.findPossibleTargets(gapsArray), 100, 30, 30, 255);
        Redraw(canvas, this.solitaire.findLeastCards(), 120, 9, 255, 9);
    }

    private void drawPath(Canvas canvas, Paint paint, float xLeft, float xRight, float yTop, float yBottom) {
        canvas.drawLine(xLeft, yTop, xRight, yTop, paint);
        canvas.drawLine(xRight, yTop, xRight, yBottom, paint);
        canvas.drawLine(xRight, yBottom, xLeft, yBottom, paint);
        canvas.drawLine(xLeft, yBottom, xLeft, yTop, paint);
    }

    private void Redraw(Canvas canvas, LinkedList<Card> targetsArray, int a, int r, int g, int b) {
        while (!targetsArray.isEmpty()) {
            Card tempCard = targetsArray.getFirst();
            Drawable tile = tempCard.tile;
            int row = tempCard.x;
            int column = tempCard.y;
            targetsArray.removeFirst();
            tile.setBounds((int) Math.floor(((double) (((float) column) * this.width)) + (1.5d * ((double) this.width)) + 3.0d), (int) Math.floor((double) ((((float) row) * this.height) + 3.0f)), (int) Math.floor((((double) (((float) column) * this.width)) + (2.5d * ((double) this.width))) - 3.0d), (int) Math.floor((double) (((((float) row) * this.height) + this.height) - 3.0f)));
            tile.draw(canvas);
            Paint borderPaint = new Paint();
            borderPaint.setARGB(a, r, g, b);
            canvas.drawRect(new Rect((int) Math.floor(((double) (((float) column) * this.width)) + (1.5d * ((double) this.width)) + 3.0d), (int) Math.floor((double) ((((float) row) * this.height) + 3.0f)), (int) Math.floor((((double) (((float) column) * this.width)) + (2.5d * ((double) this.width))) - 3.0d), (int) Math.floor((double) (((((float) row) * this.height) + this.height) - 3.0f))), borderPaint);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return super.onTouchEvent(event);
        }
        float xCoordinate = event.getX();
        float yCoordinate = event.getY();
        if (xCoordinate > 2.0f * this.width) {
            this.selectedY = (int) Math.floor((((double) Math.round(xCoordinate)) - (1.5d * ((double) this.width))) / ((double) this.width));
            this.selectedX = (int) Math.floor((double) (((float) Math.round(yCoordinate)) / this.height));
            if (this.solitaire.getCardValue(this.selectedX, this.selectedY) == 0) {
                if (this.solitaire.doStep(this.selectedX, this.selectedY)) {
                    this.fieldActivity.stepPlayer.start();
                }
            } else if (this.solitaire.getCardValue(this.selectedX, this.selectedY) == 1 && this.selectedY != 0 && this.solitaire.setLeastCardInPlace(this.selectedX, this.selectedY)) {
                this.fieldActivity.stepPlayer.start();
            }
            invalidate();
        }
        return true;
    }
}
