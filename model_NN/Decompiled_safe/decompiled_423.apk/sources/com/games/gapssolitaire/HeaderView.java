package com.games.gapssolitaire;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;

public class HeaderView extends View {
    private final FieldActivity field;
    private float screenWidth;

    HeaderView(Context context) {
        super(context);
        this.field = (FieldActivity) context;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.screenWidth = (float) w;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return super.onTouchEvent(event);
        }
        float xCoordinate = event.getX();
        if (xCoordinate > 0.0f && xCoordinate < this.screenWidth / 8.0f) {
            this.field.pauseButtonClick();
        } else if (xCoordinate > (this.screenWidth * 3.0f) / 8.0f && xCoordinate < (this.screenWidth * 5.0f) / 8.0f) {
            this.field.shuffleButtonClick();
        } else if (xCoordinate > (this.screenWidth * 7.0f) / 8.0f && xCoordinate < this.screenWidth) {
            this.field.howToPlayClick();
        }
        invalidate();
        return true;
    }
}
