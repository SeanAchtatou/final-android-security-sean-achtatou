package org.games4all.game.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import org.games4all.c.c;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.lifecycle.j;
import org.games4all.game.model.b;
import org.games4all.game.model.d;
import org.games4all.game.model.f;
import org.games4all.game.move.PlayerMove;
import org.games4all.game.option.e;
import org.games4all.game.table.a;
import org.games4all.json.h;
import org.games4all.util.RandomGenerator;

public abstract class GameModelImpl<Hidden extends b, Public extends d, Private extends f> implements e<Hidden, Public, Private>, Serializable {
    private static final long serialVersionUID = -7862342959717149223L;
    private transient c<a> a;
    private transient c<org.games4all.game.lifecycle.f> b;
    private Hidden hiddenModel;
    private Private[] privateModels;
    private Public publicModel;

    protected GameModelImpl() {
        a();
    }

    public GameModelImpl(Hidden hidden, Public publicR, Private[] privateArr) {
        this();
        this.hiddenModel = hidden;
        this.publicModel = publicR;
        this.privateModels = privateArr;
    }

    private void a() {
        this.a = new c<>(a.class);
        this.b = new c<>(org.games4all.game.lifecycle.f.class);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        a();
    }

    public void i() {
        this.a.c().a();
    }

    public org.games4all.c.b a(org.games4all.game.lifecycle.f fVar) {
        return this.b.c(fVar);
    }

    public void j() {
        this.b.c().a();
    }

    public void k() {
        this.b.c().b();
    }

    public Hidden l() {
        return this.hiddenModel;
    }

    public Public m() {
        return this.publicModel;
    }

    public Private k(int i) {
        return this.privateModels[i];
    }

    public int n() {
        return this.privateModels.length;
    }

    public a o() {
        return this.publicModel.f();
    }

    public e p() {
        return o().a();
    }

    public Stage q() {
        return m().g();
    }

    public j r() {
        return m().i();
    }

    public RandomGenerator s() {
        return this.hiddenModel.a();
    }

    public void t() {
        this.hiddenModel.a().b();
        for (Private e : this.privateModels) {
            e.e().b();
        }
    }

    public PlayerMove u() {
        return this.publicModel.m();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: Public
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void a(org.games4all.game.lifecycle.Stage r2) {
        /*
            r1 = this;
            Public r0 = r1.publicModel
            r0.b(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.model.GameModelImpl.a(org.games4all.game.lifecycle.Stage):void");
    }

    public Stage v() {
        return this.publicModel.h();
    }

    public String toString() {
        try {
            return new h().a(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
