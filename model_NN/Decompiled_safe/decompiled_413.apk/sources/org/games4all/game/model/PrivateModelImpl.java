package org.games4all.game.model;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.games4all.c.b;
import org.games4all.c.c;
import org.games4all.game.controller.a;
import org.games4all.game.lifecycle.d;
import org.games4all.game.rating.Rating;
import org.games4all.game.rating.RatingDescriptor;
import org.games4all.util.RandomGenerator;

public class PrivateModelImpl implements Serializable, f {
    private static final long serialVersionUID = -1964497870610558251L;
    private transient c<a> a;
    private transient c<d> b;
    private RandomGenerator randomGenerator;
    private final Map<RatingDescriptor, Rating> ratings = new HashMap();

    public PrivateModelImpl() {
        a();
    }

    private void a() {
        this.a = new c<>(a.class);
        this.b = new c<>(d.class);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        a();
    }

    public void a(a aVar) {
        this.a.a(aVar);
    }

    public void b(a aVar) {
        this.a.b(aVar);
    }

    public b c(a aVar) {
        return this.a.c(aVar);
    }

    public a c() {
        return this.a.c();
    }

    public b a(d dVar) {
        return this.b.c(dVar);
    }

    public d d() {
        return this.b.c();
    }

    public RandomGenerator e() {
        return this.randomGenerator;
    }

    public void a(RandomGenerator randomGenerator2) {
        this.randomGenerator = randomGenerator2;
    }
}
