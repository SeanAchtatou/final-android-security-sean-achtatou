package org.games4all.game.model;

import org.games4all.game.lifecycle.Stage;
import org.games4all.game.model.b;
import org.games4all.game.model.d;
import org.games4all.game.model.f;

public interface e<Hidden extends b, Public extends d, Private extends f> {
    org.games4all.c.b a(org.games4all.game.lifecycle.f fVar);

    void a(Stage stage);

    boolean a(int i);

    void i();

    void j();

    Private k(int i);

    void k();

    Hidden l();

    Public m();

    int n();

    org.games4all.game.option.e p();

    Stage q();

    Stage v();
}
