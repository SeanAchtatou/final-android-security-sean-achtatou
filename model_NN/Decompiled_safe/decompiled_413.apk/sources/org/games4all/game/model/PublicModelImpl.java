package org.games4all.game.model;

import java.io.ObjectInputStream;
import java.io.Serializable;
import org.games4all.c.b;
import org.games4all.c.c;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.lifecycle.i;
import org.games4all.game.lifecycle.j;
import org.games4all.game.move.PlayerMove;
import org.games4all.game.table.a;

public class PublicModelImpl implements Serializable, d {
    private static final long serialVersionUID = -519098246399549110L;
    private transient c<i> a;
    private boolean active;
    private transient c<org.games4all.game.lifecycle.c> b;
    private PlayerMove lastMove;
    private Stage stage = Stage.NONE;
    private j stageCounter;
    private a tableModel;
    private Stage targetStage;

    public PublicModelImpl() {
        a();
    }

    private void a() {
        this.a = new c<>(i.class);
        this.b = new c<>(org.games4all.game.lifecycle.c.class);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        a();
    }

    public b a(i iVar) {
        return this.a.c(iVar);
    }

    public i l() {
        return this.a.c();
    }

    public b a(org.games4all.game.lifecycle.c cVar) {
        return this.b.c(cVar);
    }

    public org.games4all.game.lifecycle.c j() {
        return this.b.c();
    }

    public Stage g() {
        return this.stage;
    }

    public void a(Stage stage2) {
        this.stage = stage2;
    }

    public void b(Stage stage2) {
        this.targetStage = stage2;
    }

    public Stage h() {
        return this.targetStage;
    }

    public void a(j jVar) {
        this.stageCounter = jVar;
    }

    public j i() {
        return this.stageCounter;
    }

    public void a(a aVar) {
        this.tableModel = aVar;
    }

    public a f() {
        return this.tableModel;
    }

    public boolean k() {
        return this.active;
    }

    public void a(boolean z) {
        this.active = z;
    }

    public void a(PlayerMove playerMove) {
        this.lastMove = playerMove;
    }

    public PlayerMove m() {
        return this.lastMove;
    }
}
