package org.games4all.game;

import org.games4all.game.lifecycle.Stage;
import org.games4all.game.model.e;
import org.games4all.game.move.Move;
import org.games4all.game.move.c;
import org.games4all.game.option.b;
import org.games4all.util.k;

public interface h<M extends e<?, ?, ?>> extends k {
    int a(PlayerInfo playerInfo, b bVar);

    c a(int i, Move move);

    e n();

    M o();

    void p();

    void t();

    Stage u();

    boolean v();
}
