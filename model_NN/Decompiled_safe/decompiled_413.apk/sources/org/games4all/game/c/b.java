package org.games4all.game.c;

import org.games4all.game.controller.a.c;
import org.games4all.game.lifecycle.g;

public class b implements org.games4all.game.controller.b {
    private final c a;
    private final org.games4all.c.b b;

    public b(c cVar) {
        this.a = cVar;
        this.b = cVar.a(new a(cVar));
    }

    class a extends g {
        final /* synthetic */ c a;

        a(c cVar) {
            this.a = cVar;
        }

        public void a() {
            this.a.a(((c) b.this.a()).a());
        }
    }

    public void d() {
        this.b.a();
    }

    public org.games4all.game.e.a a() {
        return this.a.c();
    }

    public c e() {
        return this.a;
    }
}
