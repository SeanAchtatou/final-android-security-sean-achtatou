package org.games4all.game.c;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.games4all.game.e.c;
import org.games4all.game.e.d;
import org.games4all.game.model.e;

public class a<Model extends e<?, ?, ?>> {
    private final Map<String, c<Model>> a = new HashMap();
    private final Map<String, org.games4all.util.a.a<org.games4all.game.option.e>> b = new LinkedHashMap();
    private final Map<String, Boolean> c = new HashMap();

    public void a(String str, org.games4all.util.a.a<org.games4all.game.option.e> aVar, c<Model> cVar, boolean z) {
        this.a.put(str, cVar);
        this.b.put(str, aVar);
        this.c.put(str, Boolean.valueOf(z));
    }

    public c<Model> a(d dVar) {
        return this.a.get(((e) dVar).a());
    }

    public List<String> a(org.games4all.game.option.e eVar) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : this.b.entrySet()) {
            org.games4all.util.a.a aVar = (org.games4all.util.a.a) next.getValue();
            if (aVar == null || aVar.a(eVar)) {
                arrayList.add(next.getKey());
            }
        }
        return arrayList;
    }

    private boolean a(String str) {
        return this.c.get(str).booleanValue();
    }

    public String b(org.games4all.game.option.e eVar) {
        for (String next : a(eVar)) {
            if (a(next)) {
                return next;
            }
        }
        return null;
    }
}
