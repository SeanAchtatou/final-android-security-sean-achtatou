package org.games4all.game.table;

import java.io.ObjectInputStream;
import java.io.Serializable;
import org.games4all.c.c;
import org.games4all.game.PlayerInfo;
import org.games4all.game.option.b;
import org.games4all.game.option.e;

public class TableModelImpl implements Serializable, a {
    private static final long serialVersionUID = -4916395089180242399L;
    private transient c<b> a;
    private e gameOptions;
    private PlayerInfo[] playerInfo;
    private b[] playerOptions;

    public TableModelImpl() {
        c();
    }

    public TableModelImpl(int i) {
        this();
        this.playerOptions = new b[i];
        this.playerInfo = new PlayerInfo[i];
    }

    private void c() {
        this.a = new c<>(b.class);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        c();
    }

    public org.games4all.c.b a(b bVar) {
        return this.a.c(bVar);
    }

    public b b() {
        return this.a.c();
    }

    public e a() {
        return this.gameOptions;
    }

    public PlayerInfo a(int i) {
        return this.playerInfo[i];
    }

    public void a(int i, PlayerInfo playerInfo2) {
        this.playerInfo[i] = playerInfo2;
    }

    public void a(e eVar) {
        this.gameOptions = eVar;
        int b = this.gameOptions.b();
        this.playerOptions = new b[b];
        this.playerInfo = new PlayerInfo[b];
    }
}
