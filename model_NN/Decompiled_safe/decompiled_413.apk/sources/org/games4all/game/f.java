package org.games4all.game;

import org.games4all.c.d;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.lifecycle.a;
import org.games4all.game.model.e;
import org.games4all.game.move.Move;
import org.games4all.game.move.PlayerMove;
import org.games4all.game.move.c;
import org.games4all.game.option.b;

public abstract class f<M extends e<?, ?, ?>> extends a implements h<M>, org.games4all.game.move.a {
    private final M a;
    private final d b = new d();

    public f(M m) {
        this.a = m;
        this.b.a(m.m().a(this));
        t();
    }

    public void d() {
        this.b.a();
    }

    public M o() {
        return this.a;
    }

    public int a(PlayerInfo playerInfo, b bVar) {
        int a2 = bVar.a();
        org.games4all.game.table.a f = this.a.m().f();
        if (a2 != -1 && f.a(a2) == null) {
            return a2;
        }
        int n = this.a.n();
        for (int i = 0; i < n; i++) {
            if (f.a(i) == null) {
                return i;
            }
        }
        throw new RuntimeException("No seat available");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void c(org.games4all.game.lifecycle.Stage r3) {
        /*
            r2 = this;
            M r0 = r2.a
            M r1 = r2.a
            org.games4all.game.lifecycle.Stage r1 = r1.v()
            org.games4all.game.lifecycle.Stage r1 = org.games4all.game.lifecycle.Stage.a(r1, r3)
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.f.c(org.games4all.game.lifecycle.Stage):void");
    }

    public void p() {
        c(Stage.SESSION);
    }

    public void q() {
        c(Stage.MATCH);
    }

    public void r() {
        c(Stage.GAME);
    }

    public void s() {
        c(Stage.TURN);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void t() {
        /*
            r2 = this;
            M r0 = r2.a
            org.games4all.game.lifecycle.Stage r1 = org.games4all.game.lifecycle.Stage.DONE
            r0.a(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.f.t():void");
    }

    public Stage u() {
        return this.a.v();
    }

    public c a(int i, Move move) {
        c a2 = move.a(i, n());
        if (a2.a()) {
            a2 = move.a(i, this);
            if (a2.a()) {
                this.a.m().a(new PlayerMove(i, move, a2));
            } else {
                System.err.println("WARNING: ILLEGAL MOVE ACCEPTED BY RULES " + move + ": " + a2);
            }
        } else {
            System.err.println("WARNING: ILLEGAL MOVE REJECTED BY RULES " + move + ": " + a2);
        }
        return a2;
    }

    public boolean v() {
        return false;
    }
}
