package org.games4all.game.test;

public class i {
    private final GameAction a;
    private final int b;

    public i(GameAction gameAction, int i) {
        this.a = gameAction;
        this.b = i;
    }

    public GameAction a() {
        return this.a;
    }

    public int b() {
        return this.b;
    }

    public String toString() {
        return "PlayerAction[seat=" + this.b + ",action=" + this.a + "]";
    }
}
