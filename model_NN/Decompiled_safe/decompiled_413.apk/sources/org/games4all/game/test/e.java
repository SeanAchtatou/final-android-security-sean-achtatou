package org.games4all.game.test;

import org.games4all.a.b;
import org.games4all.util.d;

public class e implements Runnable, org.games4all.a.a {
    private final b a = new b();
    private int b;
    private boolean c;
    private final Thread d = new Thread(this, "ScenarioExecutor");
    private boolean e = true;

    public e() {
        this.d.setDaemon(true);
        this.d.start();
    }

    public void execute(Runnable runnable) {
        a(runnable, null);
    }

    public void a(Runnable runnable, String str) {
        synchronized (this.a) {
            this.a.a(runnable, str);
            this.a.notify();
        }
    }

    public void a(String str, Runnable runnable, Runnable runnable2) {
        this.a.a(str, runnable, runnable2);
    }

    public void c() {
        if (this.d == Thread.currentThread()) {
            throw new RuntimeException("runEvents cannot be called from the event thread!");
        }
        Object obj = new Object();
        synchronized (obj) {
            execute(new a(obj));
            try {
                obj.wait();
            } catch (InterruptedException e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    class a implements Runnable {
        final /* synthetic */ Object a;

        a(Object obj) {
            this.a = obj;
        }

        public void run() {
            synchronized (this.a) {
                this.a.notify();
            }
        }

        public String toString() {
            return "SYNC TASK";
        }
    }

    public void run() {
        while (this.e) {
            synchronized (this) {
                if (e()) {
                    this.c = true;
                    try {
                        wait();
                        this.c = false;
                    } catch (InterruptedException e2) {
                        throw new RuntimeException(e2);
                    }
                }
            }
            synchronized (this.a) {
                if (this.a.a()) {
                    try {
                        this.a.wait();
                    } catch (Exception e3) {
                        d.a(e3);
                        throw new RuntimeException(e3);
                    } catch (InterruptedException e4) {
                        throw new RuntimeException(e4);
                    }
                }
                this.a.b();
            }
        }
    }

    public boolean e() {
        return this.b > 0;
    }

    public synchronized void a() {
        this.b++;
    }

    public synchronized void b() {
        int i = this.b - 1;
        this.b = i;
        if (i == 0 && this.c) {
            notify();
        }
    }

    public synchronized void d() {
        this.a.c();
    }
}
