package org.games4all.game.test;

import org.games4all.game.PlayerInfo;
import org.games4all.game.model.e;

public class d implements f {
    private final PlayerInfo a;
    private final org.games4all.a.a b;
    private final org.games4all.util.b.a.a<i> c;
    private a d;
    private final org.games4all.c.d e = new org.games4all.c.d();

    public d(PlayerInfo playerInfo, org.games4all.a.a aVar, org.games4all.util.b.a.a<i> aVar2) {
        this.a = playerInfo;
        this.b = aVar;
        this.c = aVar2;
    }

    public void a(a aVar) {
        this.d = aVar;
        aVar.a(this.c);
    }

    private void e() {
        this.b.c();
    }

    class a implements org.games4all.util.a.a<i> {
        a() {
        }

        public boolean a(i iVar) {
            return iVar.b() == d.this.d();
        }
    }

    public GameAction c() {
        e();
        return this.c.a(new a()).a();
    }

    public void a() {
        this.d.a();
        e();
    }

    public int d() {
        return this.d.c();
    }

    public e<?, ?, ?> b() {
        return this.d.b();
    }
}
