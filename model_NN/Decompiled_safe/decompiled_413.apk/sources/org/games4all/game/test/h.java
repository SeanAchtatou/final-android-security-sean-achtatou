package org.games4all.game.test;

import org.games4all.game.d.a;

public abstract class h {
    private c a;
    private final a b;

    public abstract void d();

    public String a() {
        return getClass().getSimpleName();
    }

    public c b() {
        if (this.a == null) {
            this.a = new g(this.b);
        }
        return this.a;
    }

    public void c() {
        try {
            d();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
