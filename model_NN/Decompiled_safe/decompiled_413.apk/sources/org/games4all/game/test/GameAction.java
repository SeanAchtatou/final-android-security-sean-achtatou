package org.games4all.game.test;

public enum GameAction {
    MOVE,
    RESUME,
    CONTINUE,
    END_SESSION
}
