package org.games4all.game.test;

import org.games4all.game.controller.a.c;
import org.games4all.game.lifecycle.e;

public class a implements org.games4all.game.controller.b {
    private final c a;
    private org.games4all.util.b.a.a<i> b = new org.games4all.util.b.a.a<>();

    public a(c cVar) {
        this.a = cVar;
        cVar.a(new b());
        cVar.a().m().a(new C0022a());
    }

    class b implements e {
        b() {
        }

        public void a() {
            a.this.a(GameAction.MOVE);
        }

        public void b() {
        }

        public void a(org.games4all.game.move.c cVar) {
        }

        public void c() {
        }

        public void d() {
        }

        public void e() {
            a.this.a(GameAction.RESUME);
        }

        public void f() {
        }

        public void g() {
        }

        public void h() {
        }
    }

    /* renamed from: org.games4all.game.test.a$a  reason: collision with other inner class name */
    class C0022a extends org.games4all.game.lifecycle.a {
        C0022a() {
        }

        /* access modifiers changed from: protected */
        public void c_() {
            a.this.a(GameAction.CONTINUE);
        }

        /* access modifiers changed from: protected */
        public void j() {
            a.this.a(GameAction.END_SESSION);
        }
    }

    public void a(org.games4all.util.b.a.a<i> aVar) {
        this.b = aVar;
    }

    /* access modifiers changed from: package-private */
    public void a(GameAction gameAction) {
        this.b.a(new i(gameAction, c()));
    }

    public void a() {
        this.a.e();
    }

    public org.games4all.game.model.e<?, ?, ?> b() {
        return this.a.a();
    }

    public int c() {
        return this.a.b();
    }

    public void d() {
    }

    public c e() {
        return this.a;
    }
}
