package org.games4all.game.option;

import java.lang.Enum;
import org.games4all.game.b;
import org.games4all.game.g;

public class VariantOptions<V extends Enum<V> & g & b> extends GameOptionsImpl {
    private static final long serialVersionUID = -7783469328022863353L;
    private V variant;

    public int b() {
        return ((b) this.variant).b();
    }

    public V d() {
        return this.variant;
    }

    public void a(V v) {
        this.variant = v;
    }

    public long a() {
        return ((g) this.variant).a();
    }

    public String toString() {
        return "GameOptions[variant=" + ((Object) this.variant) + "]";
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (this.variant == null ? 0 : this.variant.hashCode());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r3 = 1
            r2 = 0
            if (r4 != r5) goto L_0x0006
            r0 = r3
        L_0x0005:
            return r0
        L_0x0006:
            boolean r0 = super.equals(r5)
            if (r0 != 0) goto L_0x000e
            r0 = r2
            goto L_0x0005
        L_0x000e:
            java.lang.Class r0 = r4.getClass()
            java.lang.Class r1 = r5.getClass()
            if (r0 == r1) goto L_0x001a
            r0 = r2
            goto L_0x0005
        L_0x001a:
            org.games4all.game.option.VariantOptions r5 = (org.games4all.game.option.VariantOptions) r5
            V r0 = r4.variant
            if (r0 != 0) goto L_0x0026
            V r0 = r5.variant
            if (r0 == 0) goto L_0x0032
            r0 = r2
            goto L_0x0005
        L_0x0026:
            V r0 = r4.variant
            V r1 = r5.variant
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0032
            r0 = r2
            goto L_0x0005
        L_0x0032:
            r0 = r3
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.option.VariantOptions.equals(java.lang.Object):boolean");
    }
}
