package org.games4all.game.option;

import org.games4all.game.g;

public class SingleVariantOptions extends GameOptionsImpl {
    private static final long serialVersionUID = -3129937207302533393L;
    private long variantId;

    public SingleVariantOptions(int i, g gVar) {
        a(gVar.a());
        a(i);
    }

    private void a(long j) {
        this.variantId = j;
    }

    public SingleVariantOptions() {
    }

    public long a() {
        return this.variantId;
    }
}
