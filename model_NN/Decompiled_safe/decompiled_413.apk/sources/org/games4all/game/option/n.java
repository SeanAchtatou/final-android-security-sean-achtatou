package org.games4all.game.option;

import java.lang.Enum;
import java.util.Collections;
import java.util.List;
import org.games4all.game.b;
import org.games4all.game.g;

public class n<V extends Enum<V> & g & b> implements k {
    private final int a;
    private final V b;

    public i a() {
        return new SingleVariantOptions(this.a, (g) this.b);
    }

    public List<m> b() {
        return Collections.emptyList();
    }

    public void a(i iVar) {
    }

    public void b(i iVar) {
    }
}
