package org.games4all.game.option;

import java.util.HashMap;
import java.util.Map;

public abstract class GameOptionsImpl extends OptionsImpl implements Cloneable, e {
    private static final long serialVersionUID = 7398306462679098582L;
    private Map<String, Object> extras;
    private int seatCount;

    public int b() {
        return this.seatCount;
    }

    public void a(int i) {
        this.seatCount = i;
    }

    /* renamed from: c */
    public e clone() {
        try {
            GameOptionsImpl gameOptionsImpl = (GameOptionsImpl) super.clone();
            if (this.extras != null) {
                gameOptionsImpl.extras = new HashMap();
                gameOptionsImpl.extras.putAll(this.extras);
            }
            return gameOptionsImpl;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public int hashCode() {
        int i = 1 * 31;
        return (((this.extras == null ? 0 : this.extras.hashCode()) + 31) * 31) + this.seatCount;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GameOptionsImpl gameOptionsImpl = (GameOptionsImpl) obj;
        if (this.extras == null) {
            if (gameOptionsImpl.extras != null) {
                return false;
            }
        } else if (!this.extras.equals(gameOptionsImpl.extras)) {
            return false;
        }
        return this.seatCount == gameOptionsImpl.seatCount;
    }
}
