package org.games4all.game.option;

import java.lang.Enum;
import java.util.ArrayList;
import java.util.List;
import org.games4all.game.b;
import org.games4all.game.g;

public class a<V extends Enum<V> & g & b> implements k {
    private final Class<? extends VariantOptions<V>> a;
    private final V b;
    private final o<V> c;

    public a(Class<? extends VariantOptions<V>> cls, List<V> list, V v) {
        this.a = cls;
        this.b = v;
        this.c = new d("basic", "variant", list, v);
    }

    public i a() {
        try {
            VariantOptions variantOptions = (VariantOptions) this.a.newInstance();
            variantOptions.a(this.b);
            return variantOptions;
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        }
    }

    public List<m> b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.c);
        return arrayList;
    }

    public void a(i iVar) {
        ((VariantOptions) iVar).a((Enum) this.c.c());
    }

    public void b(i iVar) {
        this.c.a(((VariantOptions) iVar).d());
    }
}
