package org.games4all.game.option;

import java.util.ArrayList;
import java.util.List;

public class d<T> extends j<T> implements o<T> {
    private final List<T> a = new ArrayList();
    private final List<T> b;

    public d(String str, String str2, List<T> list, T t) {
        super(str, str2, t);
        this.a.addAll(list);
        this.b = new ArrayList(list);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Object d = d();
        if (this.a.contains(d)) {
            super.a(d);
        } else {
            super.a((Object) this.a.get(0));
        }
    }

    public void a(Object obj) {
        if (!this.a.contains(obj)) {
            a();
        } else {
            super.a(obj);
        }
    }

    public List<T> b() {
        return this.a;
    }
}
