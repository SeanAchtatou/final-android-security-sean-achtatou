package org.games4all.game.b;

import org.games4all.game.PlayerInfo;
import org.games4all.game.c.a;
import org.games4all.game.controller.a.d;
import org.games4all.game.controller.a.f;
import org.games4all.game.controller.a.k;
import org.games4all.game.e.c;
import org.games4all.game.model.e;

public class b {
    private final d a;
    private final c b;
    private final d c = new org.games4all.game.c.d();
    private final a d;
    private final org.games4all.game.test.b e;
    private final c f;

    public b(d dVar, c cVar, a aVar) {
        this.a = dVar;
        this.b = cVar;
        this.d = aVar;
        this.e = new org.games4all.game.test.b();
        this.f = new org.games4all.game.e.b();
    }

    public org.games4all.game.controller.b a(c cVar, PlayerInfo playerInfo, org.games4all.game.option.b bVar) {
        int a2 = cVar.a();
        e<?, ?, ?> b2 = cVar.b();
        return this.a.a(new f(b2, a2, this.b.a(b2, a2, playerInfo), new k(cVar), cVar));
    }

    public org.games4all.game.controller.b b(c cVar, PlayerInfo playerInfo, org.games4all.game.option.b bVar) {
        c a2 = this.d.a(new org.games4all.game.c.e(playerInfo.a()));
        if (a2 == null) {
            throw new RuntimeException("No robot factory found for " + playerInfo.a());
        }
        int a3 = cVar.a();
        return this.c.a(new f(cVar.b(), a3, a2.a(cVar.b(), a3, playerInfo), new k(cVar), cVar));
    }
}
