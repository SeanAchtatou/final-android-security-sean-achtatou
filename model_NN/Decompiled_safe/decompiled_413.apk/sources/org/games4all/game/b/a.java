package org.games4all.game.b;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.games4all.game.PlayerInfo;
import org.games4all.game.controller.server.GameSeed;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.move.Move;
import org.games4all.game.move.PlayerMove;
import org.games4all.game.rating.ContestResult;
import org.games4all.game.rating.h;
import org.games4all.game.table.TableModelImpl;
import org.games4all.match.MatchResult;
import org.games4all.util.k;

public class a implements k {
    static final /* synthetic */ boolean a = (!a.class.desiredAssertionStatus());
    private final org.games4all.game.a b;
    private final b c;
    private final org.games4all.a.d d;
    private org.games4all.game.model.e e;
    private org.games4all.game.controller.server.d f;
    private org.games4all.game.controller.b[] g;
    private final org.games4all.game.f.a h = new org.games4all.game.f.a();
    private final org.games4all.c.c<org.games4all.game.move.b> i = new org.games4all.c.c<>(org.games4all.game.move.b.class);
    private final List<g> j = new ArrayList();
    private final h<org.games4all.game.model.e<?, ?, ?>> k;
    private final h<org.games4all.game.model.e<?, ?, ?>> l;
    private MatchResult m;
    private boolean n;

    private class g implements org.games4all.c.b {
        private org.games4all.c.b b;
        private final org.games4all.game.lifecycle.c c;

        public g(org.games4all.game.lifecycle.c cVar) {
            this.c = cVar;
        }

        public void a() {
            if (this.b != null) {
                this.b.a();
            }
            a.this.a(this);
        }

        public org.games4all.game.lifecycle.c b() {
            return this.c;
        }

        public void a(org.games4all.c.b bVar) {
            this.b = bVar;
        }

        public org.games4all.c.b c() {
            return this.b;
        }
    }

    public a(org.games4all.game.a aVar, b bVar, org.games4all.a.d dVar) {
        this.b = aVar;
        this.c = bVar;
        this.d = dVar;
        this.k = aVar.a(Stage.GAME);
        this.l = aVar.a(Stage.MATCH);
        a(new c());
    }

    class c extends org.games4all.game.lifecycle.a {
        c() {
        }

        /* access modifiers changed from: protected */
        public void a_() {
            a.this.h();
        }

        /* access modifiers changed from: protected */
        public void b_() {
            a.this.f();
        }

        /* access modifiers changed from: protected */
        public void c() {
            a.this.i();
        }

        /* access modifiers changed from: protected */
        public void c_() {
            a.this.g();
        }
    }

    public void a(boolean z) {
        this.n = z;
    }

    public org.games4all.c.b a(org.games4all.game.lifecycle.c cVar) {
        g gVar = new g(cVar);
        this.j.add(gVar);
        if (this.e != null) {
            gVar.a(this.e.m().a(cVar));
        }
        return gVar;
    }

    /* access modifiers changed from: package-private */
    public void a(g gVar) {
        this.j.remove(gVar);
    }

    public org.games4all.a.d a() {
        return this.d;
    }

    public void a(org.games4all.game.model.e eVar, MatchResult matchResult, List<List<PlayerMove>> list) {
        org.games4all.game.model.d m2 = eVar.m();
        m2.a(false);
        org.games4all.game.table.a f2 = m2.f();
        int n2 = eVar.n();
        for (int i2 = 0; i2 < n2; i2++) {
            f2.a(i2, null);
        }
        a(eVar);
        this.m = matchResult;
        this.h.a(list);
    }

    public void a(org.games4all.game.option.e eVar, int i2, GameSeed gameSeed) {
        int b2 = eVar.b();
        if (b2 == 0) {
            throw new IllegalArgumentException("seatCount == 0");
        }
        org.games4all.game.model.e a2 = this.b.a(eVar);
        TableModelImpl tableModelImpl = new TableModelImpl(b2);
        tableModelImpl.a(eVar);
        a2.m().a(tableModelImpl);
        org.games4all.game.controller.server.b.a(a2, gameSeed);
        this.m = new MatchResult(i2, -1);
        h();
        a(a2);
    }

    private void a(org.games4all.game.model.e eVar) {
        org.games4all.game.h a2 = this.b.a(eVar);
        b(eVar);
        this.f = a(a2);
        this.g = new org.games4all.game.controller.b[this.e.n()];
    }

    private void b(org.games4all.game.model.e eVar) {
        org.games4all.game.model.d m2 = eVar.m();
        for (g next : this.j) {
            org.games4all.c.b c2 = next.c();
            if (c2 != null) {
                c2.a();
            }
            next.a(m2.a(next.b()));
        }
        this.e = eVar;
    }

    public org.games4all.game.model.e b() {
        return this.e;
    }

    public void c() {
        for (g next : this.j) {
            org.games4all.c.b c2 = next.c();
            if (c2 != null) {
                c2.a();
            }
            next.a(null);
        }
        this.e = null;
        if (this.g != null) {
            for (org.games4all.game.controller.b bVar : this.g) {
                if (bVar != null) {
                    bVar.d();
                }
            }
        }
        if (this.f != null) {
            this.f.d();
        }
        this.f = null;
        this.g = null;
    }

    class b extends org.games4all.game.controller.server.c {
        b(org.games4all.game.h hVar) {
            super(hVar);
        }

        /* renamed from: org.games4all.game.b.a$b$a  reason: collision with other inner class name */
        class C0019a implements Runnable {
            C0019a() {
            }

            public void run() {
                a.this.n();
            }

            public String toString() {
                return "modelChanged";
            }
        }

        /* access modifiers changed from: protected */
        public void a() {
            a.this.a().a(new C0019a(), "modelChanged");
        }
    }

    private org.games4all.game.controller.server.c a(org.games4all.game.h hVar) {
        return new b(hVar);
    }

    public org.games4all.game.controller.b a(PlayerInfo playerInfo, org.games4all.game.option.b bVar) {
        int c2 = c(playerInfo, bVar);
        org.games4all.game.controller.b a2 = this.c.a(b(c2), playerInfo, bVar);
        a(playerInfo, c2, bVar, a2);
        return a2;
    }

    public org.games4all.game.controller.b a(int i2) {
        return this.g[i2];
    }

    public void b(PlayerInfo playerInfo, org.games4all.game.option.b bVar) {
        int c2 = c(playerInfo, bVar);
        a(playerInfo, c2, bVar, this.c.b(b(c2), playerInfo, bVar));
    }

    public void e() {
        this.f.c();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.m.a(this.l.a(this.e));
    }

    /* access modifiers changed from: package-private */
    public void g() {
        String str;
        ContestResult a2 = this.k.a(this.e);
        List<PlayerMove> c2 = this.h.c();
        org.games4all.json.h hVar = new org.games4all.json.h();
        if (this.n) {
            try {
                str = hVar.a(c2);
            } catch (IOException e2) {
                throw new RuntimeException(e2);
            }
        } else {
            str = org.games4all.json.h.a;
        }
        this.m.a(a2, str);
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.h.a();
    }

    /* access modifiers changed from: package-private */
    public void i() {
        this.h.d();
    }

    public MatchResult j() {
        return this.m;
    }

    public List<List<PlayerMove>> k() {
        return this.h.b();
    }

    public c b(int i2) {
        return new d(this, this.e, i2);
    }

    public int c(PlayerInfo playerInfo, org.games4all.game.option.b bVar) {
        return this.f.a(playerInfo, bVar);
    }

    public void a(PlayerInfo playerInfo, int i2, org.games4all.game.option.b bVar, org.games4all.game.controller.b bVar2) {
        if (this.g[i2] != null) {
            throw new RuntimeException("Seat already taken: " + i2);
        }
        this.g[i2] = bVar2;
        org.games4all.game.table.a f2 = this.e.m().f();
        f2.a(i2, playerInfo);
        f2.b().a(playerInfo, i2, bVar);
        this.f.b();
    }

    /* renamed from: org.games4all.game.b.a$a  reason: collision with other inner class name */
    class C0018a implements Runnable {
        C0018a() {
        }

        public void run() {
            a.this.m();
        }

        public String toString() {
            return "checkActivation";
        }
    }

    public void l() {
        this.d.a(new C0018a(), "checkActivation");
    }

    class f implements Runnable {
        final /* synthetic */ int a;
        final /* synthetic */ Move b;

        f(int i, Move move) {
            this.a = i;
            this.b = move;
        }

        public void run() {
            a.this.b(this.a, this.b);
        }

        public String toString() {
            return "move:" + this.b.toString();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, Move move) {
        this.d.a(new f(i2, move), "move");
    }

    class e implements Runnable {
        final /* synthetic */ int a;

        e(int i) {
            this.a = i;
        }

        public void run() {
            a.this.e(this.a);
        }

        public String toString() {
            return "resume:" + this.a;
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        this.d.a(new e(i2), "resume");
    }

    class d implements Runnable {
        final /* synthetic */ int a;

        d(int i) {
            this.a = i;
        }

        public void run() {
            a.this.f(this.a);
        }
    }

    /* access modifiers changed from: package-private */
    public void d(int i2) {
        this.d.a(new d(i2), "continue");
    }

    /* access modifiers changed from: package-private */
    public void m() {
        if (this.f != null) {
            this.f.b();
        }
    }

    /* access modifiers changed from: package-private */
    public void n() {
        if (this.e != null) {
            this.e.i();
        }
    }

    /* access modifiers changed from: package-private */
    public void e(int i2) {
        if (this.f != null) {
            this.f.c(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void f(int i2) {
        if (this.f != null) {
            this.f.a(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, Move move) {
        if (this.f != null) {
            org.games4all.game.move.c a2 = this.f.a(i2, move);
            this.h.a(new PlayerMove(i2, move, a2));
            this.i.c().a(i2, move, a2);
        }
    }

    public void d() {
        for (g a2 : this.j) {
            a2.a();
        }
        for (org.games4all.game.controller.b d2 : this.g) {
            d2.d();
        }
        this.f.d();
    }
}
