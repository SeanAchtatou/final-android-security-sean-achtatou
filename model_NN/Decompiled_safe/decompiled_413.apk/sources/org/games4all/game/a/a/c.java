package org.games4all.game.a.a;

public class c {
    private int a;
    private String b;

    public c() {
        this("");
    }

    public c(String str) {
        this(str, 0);
    }

    public c(String str, int i) {
        this.b = str;
        this.a = i;
    }

    public String a() {
        return this.b;
    }
}
