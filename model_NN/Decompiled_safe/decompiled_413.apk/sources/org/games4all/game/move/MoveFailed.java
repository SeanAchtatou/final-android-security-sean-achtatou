package org.games4all.game.move;

import java.io.Serializable;

public class MoveFailed implements Serializable, c {
    private static final long serialVersionUID = -5796167562933016808L;
    private String reason;

    public MoveFailed(String str) {
        this.reason = str;
    }

    public MoveFailed() {
    }

    public boolean a() {
        return false;
    }

    public String toString() {
        return "MoveFailed[reason=\"" + this.reason + "\"]";
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.reason == null ? 0 : this.reason.hashCode()) + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MoveFailed moveFailed = (MoveFailed) obj;
        if (this.reason == null) {
            if (moveFailed.reason != null) {
                return false;
            }
        } else if (!this.reason.equals(moveFailed.reason)) {
            return false;
        }
        return true;
    }
}
