package org.games4all.game.move;

import java.io.Serializable;

public class MoveSucceeded implements Serializable, c {
    private static final long serialVersionUID = 6946166003575027081L;

    public boolean a() {
        return true;
    }

    public String toString() {
        return "MoveSucceeded[]";
    }

    public int hashCode() {
        return 1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        return getClass() == obj.getClass();
    }
}
