package org.games4all.game.move;

import java.io.Serializable;

public class PlayerMove implements Serializable {
    private static final long serialVersionUID = -5836343679729920228L;
    private Move move;
    private c result;
    private int seat;

    public PlayerMove(int i, Move move2, c cVar) {
        this.seat = i;
        this.move = move2;
        this.result = cVar;
    }

    public PlayerMove() {
    }

    public int a() {
        return this.seat;
    }

    public Move b() {
        return this.move;
    }

    public String toString() {
        return "PlayerMove[seat=" + this.seat + ",move=" + this.move + ",result=" + this.result + "]";
    }
}
