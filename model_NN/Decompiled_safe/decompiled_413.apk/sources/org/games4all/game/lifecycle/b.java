package org.games4all.game.lifecycle;

public abstract class b implements c {
    private final String a;

    /* access modifiers changed from: protected */
    public abstract void a(String str);

    public b(String str) {
        this.a = str;
    }

    private void a(String str, int i) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
        a(this.a + " - " + sb.toString() + str);
    }

    public void a(StageTransition stageTransition) {
        a(stageTransition.toString(), stageTransition.a().ordinal());
    }
}
