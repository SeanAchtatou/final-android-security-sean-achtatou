package org.games4all.game.lifecycle;

public class a implements org.games4all.c.a, c {
    private final int a = 0;

    public int e() {
        return this.a;
    }

    public void a(StageTransition stageTransition) {
        switch (stageTransition.b()) {
            case START:
                a(stageTransition.a());
                return;
            case END:
                b(stageTransition.a());
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Stage stage) {
        switch (stage) {
            case SESSION:
                f();
                return;
            case MATCH:
                a_();
                return;
            case GAME:
                c();
                return;
            case ROUND:
                g();
                return;
            case TURN:
                h();
                return;
            case MOVE:
                i();
                return;
            default:
                throw new RuntimeException("illegal stage: " + stage);
        }
    }

    /* access modifiers changed from: protected */
    public void b(Stage stage) {
        switch (stage) {
            case SESSION:
                j();
                return;
            case MATCH:
                b_();
                return;
            case GAME:
                c_();
                return;
            case ROUND:
                k();
                return;
            case TURN:
                l();
                return;
            case MOVE:
                m();
                return;
            default:
                throw new RuntimeException("illegal stage: " + stage);
        }
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    /* access modifiers changed from: protected */
    public void a_() {
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void g() {
    }

    /* access modifiers changed from: protected */
    public void h() {
    }

    /* access modifiers changed from: protected */
    public void i() {
    }

    /* access modifiers changed from: protected */
    public void j() {
    }

    /* access modifiers changed from: protected */
    public void b_() {
    }

    /* access modifiers changed from: protected */
    public void c_() {
    }

    /* access modifiers changed from: protected */
    public void k() {
    }

    /* access modifiers changed from: protected */
    public void l() {
    }

    /* access modifiers changed from: protected */
    public void m() {
    }
}
