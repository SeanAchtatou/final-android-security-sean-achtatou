package org.games4all.game.lifecycle;

import java.io.Serializable;

public class StageCounterImpl implements Serializable, j {
    private static final long serialVersionUID = 8032155019612096230L;
    private int gameCount;
    private int matchCount;
    private int moveCount;
    private int roundCount;
    private int sessionCount;
    private int turnCount;

    public int a() {
        return this.matchCount;
    }

    public int b() {
        return this.gameCount;
    }

    public int c() {
        return this.roundCount;
    }

    public int d() {
        return this.turnCount;
    }

    public int e() {
        return this.moveCount;
    }

    public void a(int i) {
        this.matchCount = i;
    }

    public void b(int i) {
        this.gameCount = i;
    }

    public void c(int i) {
        this.roundCount = i;
    }

    public void d(int i) {
        this.turnCount = i;
    }

    public void e(int i) {
        this.moveCount = i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StageCounter[");
        sb.append("session=").append(this.sessionCount).append(',');
        sb.append("match=").append(this.matchCount).append(',');
        sb.append("game=").append(this.gameCount).append(',');
        sb.append("round=").append(this.roundCount).append(',');
        sb.append("turn=").append(this.turnCount).append(',');
        sb.append("move=").append(this.moveCount).append(']');
        return sb.toString();
    }
}
