package org.games4all.game.lifecycle;

public enum Transition {
    START,
    END
}
