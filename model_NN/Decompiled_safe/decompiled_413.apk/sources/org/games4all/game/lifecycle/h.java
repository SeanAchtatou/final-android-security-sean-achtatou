package org.games4all.game.lifecycle;

public class h extends a implements i {
    private final String a;
    private boolean b;
    private Stage c;
    private boolean d;

    private static void a(String str) {
        throw new RuntimeException(str);
    }

    private void a(String str, String str2) {
        a((this.a == null ? "" : this.a + " - ") + str + ": " + str2);
    }

    public h() {
        this(null);
    }

    public h(String str) {
        this.a = str;
        this.d = true;
        this.b = false;
    }

    private void a(String str, Stage stage) {
        if (stage != this.c) {
            a(str, stage + " not the current stage (" + this.c + ").");
        }
    }

    private void b(String str, Stage stage) {
        if (c(stage)) {
            a(str, stage + " not ended.");
        }
    }

    private void b(String str) {
        if (!this.b) {
            a(str, "not active.");
        }
    }

    private boolean c(Stage stage) {
        return this.c.compareTo(stage) >= 0;
    }

    public void a(StageTransition stageTransition) {
        if (this.d) {
            if (stageTransition.b() == Transition.END) {
                this.c = stageTransition.a().a();
            } else {
                this.c = stageTransition.a();
            }
            this.b = true;
            this.d = false;
        } else {
            super.a(stageTransition);
        }
        b("stageTransition");
    }

    public void b(Stage stage) {
        String str = stage.toString().toLowerCase() + "Ended";
        a(str, stage);
        if (stage != Stage.MOVE) {
            b(str, stage.b());
        }
        this.c = stage.a();
    }

    public void a(Stage stage) {
        a(stage.toString().toLowerCase() + "Started", stage.a());
        this.c = stage;
    }

    public void a() {
        if (this.b && !this.d) {
            a("activated: already active");
        }
        this.b = true;
    }

    public void b() {
        if (!this.b && !this.d) {
            a("deactivated: already inactive");
        }
        this.b = false;
    }
}
