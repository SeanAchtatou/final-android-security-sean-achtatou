package org.games4all.game.lifecycle;

public enum StageTransition {
    SESSION_START(Stage.SESSION, Transition.START),
    SESSION_END(Stage.SESSION, Transition.END),
    MATCH_START(Stage.MATCH, Transition.START),
    MATCH_END(Stage.MATCH, Transition.END),
    GAME_START(Stage.GAME, Transition.START),
    GAME_END(Stage.GAME, Transition.END),
    ROUND_START(Stage.ROUND, Transition.START),
    ROUND_END(Stage.ROUND, Transition.END),
    TURN_START(Stage.TURN, Transition.START),
    TURN_END(Stage.TURN, Transition.END),
    MOVE_START(Stage.MOVE, Transition.START),
    MOVE_END(Stage.MOVE, Transition.END);
    
    private final Stage stage;
    private final Transition transition;

    private StageTransition(Stage stage2, Transition transition2) {
        this.stage = stage2;
        this.transition = transition2;
    }

    public static StageTransition a(Stage stage2, Transition transition2) {
        switch (stage2) {
            case SESSION:
                switch (transition2) {
                    case START:
                        return SESSION_START;
                    case END:
                        return SESSION_END;
                    default:
                        throw new IllegalArgumentException("no stage transition for " + stage2 + "." + transition2);
                }
            case MATCH:
                switch (transition2) {
                    case START:
                        return MATCH_START;
                    case END:
                        return MATCH_END;
                    default:
                        throw new IllegalArgumentException("no stage transition for " + stage2 + "." + transition2);
                }
            case GAME:
                switch (transition2) {
                    case START:
                        return GAME_START;
                    case END:
                        return GAME_END;
                    default:
                        throw new IllegalArgumentException("no stage transition for " + stage2 + "." + transition2);
                }
            case ROUND:
                switch (transition2) {
                    case START:
                        return ROUND_START;
                    case END:
                        return ROUND_END;
                    default:
                        throw new IllegalArgumentException("no stage transition for " + stage2 + "." + transition2);
                }
            case TURN:
                switch (transition2) {
                    case START:
                        return TURN_START;
                    case END:
                        return TURN_END;
                    default:
                        throw new IllegalArgumentException("no stage transition for " + stage2 + "." + transition2);
                }
            case MOVE:
                switch (transition2) {
                    case START:
                        return MOVE_START;
                    case END:
                        return MOVE_END;
                    default:
                        throw new IllegalArgumentException("no stage transition for " + stage2 + "." + transition2);
                }
            default:
                throw new IllegalArgumentException("no stage transition for " + stage2 + "." + transition2);
        }
    }

    public Stage a() {
        return this.stage;
    }

    public Transition b() {
        return this.transition;
    }
}
