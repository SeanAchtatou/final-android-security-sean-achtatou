package org.games4all.game.lifecycle;

public enum Stage {
    NONE,
    SESSION,
    MATCH,
    GAME,
    ROUND,
    TURN,
    MOVE,
    DONE;

    public static Stage a(Stage stage, Stage stage2) {
        if (stage.compareTo((Enum) stage2) <= 0) {
            return stage;
        }
        return stage2;
    }

    public Stage a() {
        if (this != NONE) {
            return values()[ordinal() - 1];
        }
        throw new IllegalStateException(SESSION + " does not have a predecessor value");
    }

    public Stage b() {
        if (this != DONE) {
            return values()[ordinal() + 1];
        }
        throw new IllegalStateException(DONE + " does not have a successor value");
    }
}
