package org.games4all.game.rating;

import java.util.EnumSet;
import java.util.List;
import org.games4all.game.rating.RatingDescriptor;

public class j extends c {
    private final int a;

    public j(long j, int i, int i2) {
        super(j, i, "AvgGamePoints", 0, 0, EnumSet.noneOf(RatingDescriptor.Flag.class), "%.1f", "");
        this.a = i2;
    }

    public boolean b(Rating rating, ContestResult contestResult, List<ContestResult> list) {
        c.a(rating, ((BasicContestResult) contestResult).a(0), this.a);
        return true;
    }
}
