package org.games4all.game.rating;

import java.io.Serializable;

public class Rating implements Serializable {
    private static final long serialVersionUID = 6185254768955538774L;
    private long rating;
    private final int ratingId;
    private final long ratingTypeId;
    private final int userId;
    private long value;

    public Rating(int i, int i2, long j) {
        this.ratingId = i;
        this.userId = i2;
        this.ratingTypeId = j;
    }

    public int a() {
        return this.ratingId;
    }

    public int b() {
        return this.userId;
    }

    public long c() {
        return this.ratingTypeId;
    }

    public long d() {
        return this.rating;
    }

    public void a(long j) {
        this.rating = j;
    }

    public long e() {
        return this.value;
    }

    public void b(long j) {
        this.value = j;
    }

    public String toString() {
        return "Rating[id=" + this.ratingId + ",userId=" + this.userId + ",type=" + this.ratingTypeId + ",rating=" + this.rating + ",value=" + this.value + "]";
    }
}
