package org.games4all.game.rating;

import java.util.Comparator;

public class g implements Comparator<k> {
    private final int a;

    public g(int i) {
        this.a = i;
    }

    /* renamed from: a */
    public int compare(k kVar, k kVar2) {
        Outcome a2 = kVar.c().a(kVar2.c(), this.a);
        switch (r0.a(r1, this.a)) {
            case WIN:
                return 1;
            case LOSS:
                return -1;
            case TIE:
                return 0;
            default:
                throw new RuntimeException(a2.toString());
        }
    }
}
