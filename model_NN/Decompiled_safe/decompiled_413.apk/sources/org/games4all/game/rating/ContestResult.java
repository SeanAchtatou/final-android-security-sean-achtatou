package org.games4all.game.rating;

import java.io.Serializable;

public interface ContestResult extends Serializable {
    int a();

    Outcome a(int i, int i2);

    Outcome a(ContestResult contestResult, int i);

    int b(int i);

    boolean b();
}
