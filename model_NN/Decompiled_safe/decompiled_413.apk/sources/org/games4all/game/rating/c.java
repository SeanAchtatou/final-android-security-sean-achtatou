package org.games4all.game.rating;

import java.util.EnumSet;
import java.util.List;
import org.games4all.game.d;
import org.games4all.game.rating.RatingDescriptor;

public abstract class c implements b {
    private final RatingDescriptor a;

    public c(long j, int i, String str, long j2, long j3, EnumSet<RatingDescriptor.Flag> enumSet, String str2, String str3) {
        this.a = new RatingDescriptor(d.a(j, i), str, j2, j3, enumSet, str2, str3);
    }

    public RatingDescriptor a() {
        return this.a;
    }

    public boolean b(Rating rating, ContestResult contestResult, List<ContestResult> list) {
        return false;
    }

    public boolean c(Rating rating, ContestResult contestResult, List<ContestResult> list) {
        return false;
    }

    public static void a(Rating rating, long j, int i) {
        a(rating, j, i, 1);
    }

    public static void a(Rating rating, long j, int i, int i2) {
        long d = rating.d();
        long e = rating.e();
        rating.a(a(d, j, e / 1000000, i, i2));
        rating.b((((long) i2) * 1000000) + e);
    }

    public static long a(long j, long j2, long j3, int i, int i2) {
        if (j3 < ((long) i)) {
            return ((j * j3) + (((long) i2) * j2)) / (((long) i2) + j3);
        }
        return ((((long) i) * j) + (((long) i2) * j2)) / ((long) (i + i2));
    }
}
