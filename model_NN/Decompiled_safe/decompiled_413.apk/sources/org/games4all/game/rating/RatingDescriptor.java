package org.games4all.game.rating;

import java.io.Serializable;
import java.util.EnumSet;
import org.games4all.game.d;
import org.games4all.util.e;

public class RatingDescriptor implements Serializable {
    private static final long serialVersionUID = 3932120255742102367L;
    private EnumSet<Flag> flags;
    private String format;
    private long initialRating;
    private long initialValue;
    private String name;
    private String options;
    private long ratingTypeId;

    public enum Flag {
        RATING_ASCENDING,
        VALUE_ASCENDING,
        MONOTONIC
    }

    public RatingDescriptor(long j, String str, long j2, long j3, EnumSet<Flag> enumSet, String str2, String str3) {
        this.ratingTypeId = j;
        this.name = str;
        this.initialRating = j2;
        this.initialValue = j3;
        this.flags = enumSet;
        this.format = str2;
        this.options = str3;
    }

    public RatingDescriptor() {
    }

    public long a() {
        return this.ratingTypeId;
    }

    public long b() {
        return d.a(this.ratingTypeId);
    }

    public String c() {
        return this.name;
    }

    public long d() {
        return this.initialRating;
    }

    public long e() {
        return this.initialValue;
    }

    public EnumSet<Flag> f() {
        return this.flags;
    }

    public String g() {
        return this.format;
    }

    public String h() {
        return this.options;
    }

    public void a(String str) {
        this.name = str;
    }

    public void a(long j) {
        this.initialRating = j;
    }

    public void b(long j) {
        this.initialValue = j;
    }

    public void b(String str) {
        this.format = str;
    }

    public void c(String str) {
        this.options = str;
    }

    public String toString() {
        return a() + '|' + c() + '|' + d() + '|' + e() + '|' + e.a(Flag.class, f(), ",") + '|' + h() + '|' + g();
    }

    public static RatingDescriptor d(String str) {
        String[] split = str.split("\\|");
        int i = 0 + 1;
        int i2 = i + 1;
        int i3 = i2 + 1;
        int i4 = i3 + 1;
        int i5 = i4 + 1;
        int i6 = i5 + 1;
        int i7 = i6 + 1;
        return new RatingDescriptor(Long.parseLong(split[0]), split[i], Long.parseLong(split[i2]), Long.parseLong(split[i3]), e.a(Flag.class, split[i4], ","), split[i6], split[i5]);
    }
}
