package org.games4all.game.rating;

import java.util.EnumSet;
import java.util.List;
import org.games4all.game.rating.RatingDescriptor;

public class l extends c {
    public l(long j, int i) {
        super(j, i, "TotalMatchesWon", 0, 0, EnumSet.of(RatingDescriptor.Flag.MONOTONIC), "%.0f", "");
    }

    public boolean c(Rating rating, ContestResult contestResult, List<ContestResult> list) {
        rating.b(rating.e() + 1000000);
        int a = contestResult.a();
        for (int i = 1; i < a; i++) {
            if (contestResult.a(0, i) != Outcome.WIN) {
                return true;
            }
        }
        rating.a(rating.d() + 1000000);
        return true;
    }
}
