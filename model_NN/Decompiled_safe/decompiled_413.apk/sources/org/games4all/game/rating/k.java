package org.games4all.game.rating;

public class k {
    private final int a;
    private final String b;
    private final ContestResult c;

    public k(int i, String str, ContestResult contestResult) {
        this.a = i;
        this.b = str;
        this.c = contestResult;
    }

    public int a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public ContestResult c() {
        return this.c;
    }
}
