package org.games4all.game.rating;

public enum Outcome {
    LOSS,
    TIE,
    WIN;

    public Outcome a() {
        switch (this) {
            case LOSS:
                return WIN;
            case TIE:
                return TIE;
            case WIN:
                return LOSS;
            default:
                throw new RuntimeException(String.valueOf(this));
        }
    }
}
