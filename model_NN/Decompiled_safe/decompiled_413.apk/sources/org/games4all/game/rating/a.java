package org.games4all.game.rating;

import java.util.EnumSet;
import java.util.List;
import org.games4all.game.rating.RatingDescriptor;

public abstract class a extends c {
    public a(long j, int i, String str) {
        super(j, i, str, 0, 0, EnumSet.of(RatingDescriptor.Flag.MONOTONIC), "%.0f (%.0f)", "");
    }

    /* access modifiers changed from: protected */
    public boolean a(Rating rating, ContestResult contestResult, List<ContestResult> list) {
        int i = 0;
        int a = contestResult.a();
        int b = contestResult.b(0);
        while (i < a) {
            if (i == b || contestResult.a(b, i) == Outcome.WIN) {
                i++;
            } else {
                rating.b(0);
                return true;
            }
        }
        long e = rating.e() + 1000000;
        rating.b(e);
        if (e > rating.d()) {
            rating.a(e);
        }
        return true;
    }
}
