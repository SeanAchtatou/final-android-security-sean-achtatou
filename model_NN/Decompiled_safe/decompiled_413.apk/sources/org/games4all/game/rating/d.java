package org.games4all.game.rating;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.games4all.b.a.c;
import org.games4all.game.move.MoveFailed;
import org.games4all.game.move.MoveSucceeded;
import org.games4all.game.move.PlayerMove;
import org.games4all.json.h;
import org.games4all.match.Match;
import org.games4all.match.MatchResult;

public abstract class d implements i {
    static final /* synthetic */ boolean a = (!d.class.desiredAssertionStatus());
    private final Map<Long, List<RatingDescriptor>> b = new HashMap();
    private final Map<Long, b> c = new HashMap();

    /* access modifiers changed from: protected */
    public abstract void a(Collection<Rating> collection);

    public static h a(String str) {
        h hVar = new h(str);
        hVar.a(true);
        hVar.b(true);
        hVar.a((Class<?>) MatchResult.class);
        hVar.a((Class<?>) String.class);
        hVar.a((Class<?>) ArrayList.class);
        hVar.a((Class<?>) PlayerMove.class);
        hVar.a((Class<?>) MoveSucceeded.class);
        hVar.a((Class<?>) MoveFailed.class);
        return hVar;
    }

    public RatingDescriptor a(long j) {
        List<RatingDescriptor> list = this.b.get(Long.valueOf(org.games4all.game.d.a(j)));
        if (list == null) {
            return null;
        }
        for (RatingDescriptor ratingDescriptor : list) {
            if (ratingDescriptor.a() == j) {
                return ratingDescriptor;
            }
        }
        return null;
    }

    public void a(b bVar) {
        RatingDescriptor a2 = bVar.a();
        a(a2);
        long a3 = a2.a();
        if (a || !this.c.containsKey(Long.valueOf(a3))) {
            this.c.put(Long.valueOf(a3), bVar);
            return;
        }
        throw new AssertionError();
    }

    public void a(RatingDescriptor ratingDescriptor) {
        long b2 = ratingDescriptor.b();
        List list = this.b.get(Long.valueOf(b2));
        if (list == null) {
            list = new ArrayList();
            this.b.put(Long.valueOf(b2), list);
        }
        if (a || !list.contains(ratingDescriptor)) {
            list.add(ratingDescriptor);
            return;
        }
        throw new AssertionError();
    }

    public Set<Rating> a(long j, c cVar, MatchResult matchResult, Match match) {
        Map<Long, Rating> a2 = a(j, cVar);
        HashSet hashSet = new HashSet();
        hashSet.addAll(a(j, a2, matchResult, match));
        a(hashSet);
        return hashSet;
    }

    public List<Rating> a(long j, Map<Long, Rating> map, MatchResult matchResult, Match match) {
        ArrayList arrayList = new ArrayList();
        int d = matchResult.d();
        for (int i = 0; i < d; i++) {
            a(j, map, matchResult.a(i), match.b(i), arrayList);
        }
        b(j, map, matchResult.c(), match.d(), arrayList);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(long j, Map<Long, Rating> map, ContestResult contestResult, List<ContestResult> list, List<Rating> list2) {
        for (Map.Entry next : map.entrySet()) {
            b bVar = this.c.get(next.getKey());
            Rating rating = (Rating) next.getValue();
            if (bVar != null && bVar.b(rating, contestResult, list)) {
                list2.add(rating);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(long j, Map<Long, Rating> map, ContestResult contestResult, List<ContestResult> list, List<Rating> list2) {
        for (Map.Entry next : map.entrySet()) {
            b bVar = this.c.get(next.getKey());
            Rating rating = (Rating) next.getValue();
            if (bVar != null && bVar.c(rating, contestResult, list)) {
                list2.add(rating);
            }
        }
    }

    public List<RatingDescriptor> b(long j) {
        return this.b.get(Long.valueOf(j));
    }

    public Set<Long> a() {
        return this.b.keySet();
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.b.clear();
        this.c.clear();
    }
}
