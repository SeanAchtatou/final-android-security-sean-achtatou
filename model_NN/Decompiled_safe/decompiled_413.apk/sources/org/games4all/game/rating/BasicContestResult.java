package org.games4all.game.rating;

public abstract class BasicContestResult implements ContestResult {
    private static final long serialVersionUID = -2969853565236940531L;
    private int seatCount;
    private long[] teamPoints;

    public BasicContestResult(int i, int i2) {
        this.seatCount = i;
        this.teamPoints = new long[i2];
    }

    public BasicContestResult(int i) {
        this(i, i);
    }

    public BasicContestResult() {
    }

    public void a(int i, long j) {
        this.teamPoints[i] = j;
    }

    public long a(int i) {
        return this.teamPoints[i];
    }

    public int b(int i) {
        return i % this.teamPoints.length;
    }

    public int a() {
        return this.teamPoints.length;
    }

    public Outcome a(int i, int i2) {
        if (this.teamPoints[i] > this.teamPoints[i2]) {
            return Outcome.WIN;
        }
        if (this.teamPoints[i] < this.teamPoints[i2]) {
            return Outcome.LOSS;
        }
        return Outcome.TIE;
    }

    public Outcome a(ContestResult contestResult, int i) {
        BasicContestResult basicContestResult = (BasicContestResult) contestResult;
        int i2 = (i + 1) % 2;
        long j = this.teamPoints[i] - this.teamPoints[i2];
        long j2 = basicContestResult.teamPoints[i] - basicContestResult.teamPoints[i2];
        if (j > j2) {
            return Outcome.WIN;
        }
        if (j < j2) {
            return Outcome.LOSS;
        }
        return Outcome.TIE;
    }

    public boolean b() {
        return true;
    }

    public String toString() {
        return String.valueOf(this.teamPoints[0] / 1000000);
    }
}
