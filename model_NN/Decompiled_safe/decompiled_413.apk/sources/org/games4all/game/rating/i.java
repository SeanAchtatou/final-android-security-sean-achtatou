package org.games4all.game.rating;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.games4all.b.a.c;
import org.games4all.match.Match;
import org.games4all.match.MatchResult;

public interface i {
    Map<Long, Rating> a(long j, c cVar);

    Set<Rating> a(long j, c cVar, MatchResult matchResult, Match match);

    List<RatingDescriptor> b(long j);

    Map<Long, Integer> b(long j, c cVar);
}
