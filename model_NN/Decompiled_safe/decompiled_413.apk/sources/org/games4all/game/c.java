package org.games4all.game;

import java.lang.reflect.InvocationTargetException;
import java.util.EnumSet;
import java.util.Set;
import org.games4all.util.h;

public class c {
    private final Class<? extends g> a;
    private final String b;
    private final long c;
    private final long d;

    public c(Class<? extends g> cls) {
        this.b = cls.getPackage().getName();
        this.a = cls;
        try {
            this.c = ((Long) cls.getMethod("getGameId", new Class[0]).invoke(null, new Object[0])).longValue();
            this.d = h.a(cls);
        } catch (SecurityException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e2) {
            throw new RuntimeException(e2);
        } catch (IllegalArgumentException e3) {
            throw new RuntimeException(e3);
        } catch (IllegalAccessException e4) {
            throw new RuntimeException(e4);
        } catch (InvocationTargetException e5) {
            throw new RuntimeException(e5);
        }
    }

    public Set<g> a() {
        return EnumSet.allOf(this.a);
    }

    public long b() {
        return this.c;
    }

    public String c() {
        return this.b;
    }

    public String toString() {
        return "GameDescriptor[pkg=" + this.b + ",id=" + this.c + ",version=" + this.d + ",variants=" + this.a + "]";
    }
}
