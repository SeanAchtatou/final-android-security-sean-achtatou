package org.games4all.game;

import java.io.Serializable;

public class PlayerInfo implements Serializable {
    private static final long serialVersionUID = -3794444791163856803L;
    private String name;
    private boolean robot;
    private int userId;

    public PlayerInfo(String str, int i, boolean z) {
        this.name = str;
        this.userId = i;
        this.robot = z;
    }

    public PlayerInfo() {
    }

    public String a() {
        return this.name;
    }

    public boolean b() {
        return this.robot;
    }

    public String toString() {
        return "PlayerInfo[name=" + this.name + ",userId=" + this.userId + "]";
    }
}
