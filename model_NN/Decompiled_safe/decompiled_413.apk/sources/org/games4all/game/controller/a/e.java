package org.games4all.game.controller.a;

import org.games4all.c.d;
import org.games4all.game.PlayerInfo;
import org.games4all.game.table.b;

public abstract class e {
    private final d a = new d();

    /* access modifiers changed from: protected */
    public abstract void a(int i, PlayerInfo playerInfo);

    public e(c cVar) {
        a(cVar);
    }

    private void a(c cVar) {
        org.games4all.game.model.e a2 = cVar.a();
        org.games4all.game.table.a f = a2.m().f();
        int n = a2.n();
        for (int i = 0; i < n; i++) {
            a(i, f.a(i));
        }
        this.a.a(f.a(b()));
    }

    class a implements b {
        a() {
        }

        public void a(PlayerInfo playerInfo, int i, org.games4all.game.option.b bVar) {
            e.this.a(i, playerInfo);
        }
    }

    private b b() {
        return new a();
    }

    public void a() {
        this.a.a();
    }
}
