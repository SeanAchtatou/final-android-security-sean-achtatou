package org.games4all.game.controller.server;

import org.games4all.game.h;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.lifecycle.StageTransition;
import org.games4all.game.lifecycle.Transition;
import org.games4all.game.lifecycle.j;
import org.games4all.game.model.d;
import org.games4all.game.model.e;

public class b<G extends h<M>, M extends e<?, ?, ?>> implements a {
    private final G a;
    private final M b;
    private final int c = this.b.n();

    /* JADX WARN: Type inference failed for: r0v0, types: [org.games4all.game.model.d] */
    /* JADX WARN: Type inference failed for: r0v1, types: [org.games4all.game.model.b] */
    /* JADX WARN: Type inference failed for: r2v1, types: [org.games4all.game.model.f] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static void a(org.games4all.game.model.e<?, ?, ?> r6, org.games4all.game.controller.server.GameSeed r7) {
        /*
            org.games4all.game.model.d r0 = r6.m()
            org.games4all.game.lifecycle.StageCounterImpl r1 = new org.games4all.game.lifecycle.StageCounterImpl
            r1.<init>()
            r0.a(r1)
            org.games4all.game.model.b r0 = r6.l()
            org.games4all.util.RandomGenerator r1 = new org.games4all.util.RandomGenerator
            long r2 = r7.a()
            r1.<init>(r2)
            r0.a(r1)
            int r0 = r6.n()
            r1 = 0
        L_0x0021:
            if (r1 >= r0) goto L_0x0036
            org.games4all.game.model.f r2 = r6.k(r1)
            org.games4all.util.RandomGenerator r3 = new org.games4all.util.RandomGenerator
            long r4 = r7.a(r1)
            r3.<init>(r4)
            r2.a(r3)
            int r1 = r1 + 1
            goto L_0x0021
        L_0x0036:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.controller.server.b.a(org.games4all.game.model.e, org.games4all.game.controller.server.GameSeed):void");
    }

    public b(G g) {
        this.a = g;
        this.b = g.o();
    }

    public G e() {
        return this.a;
    }

    public M f() {
        return this.b;
    }

    public int g() {
        return this.c;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: G
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int a(org.games4all.game.PlayerInfo r2, org.games4all.game.option.b r3) {
        /*
            r1 = this;
            G r0 = r1.a
            int r0 = r0.a(r2, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.controller.server.b.a(org.games4all.game.PlayerInfo, org.games4all.game.option.b):int");
    }

    public void b() {
        d m = this.b.m();
        if (m.k()) {
            if (!l()) {
                b(m);
            }
        } else if (l()) {
            a(m);
        }
        a();
    }

    /* access modifiers changed from: protected */
    public void a(d dVar) {
        dVar.a(true);
        dVar.l().a();
        if (dVar.g() == Stage.NONE) {
            dVar.a(Stage.SESSION);
            k();
            if (this.b.v() != Stage.DONE) {
                h();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(d dVar) {
        dVar.a(false);
        dVar.l().b();
    }

    private boolean l() {
        org.games4all.game.table.a f = this.b.m().f();
        for (int i = 0; i < this.c; i++) {
            if (f.a(i) == null) {
                return false;
            }
        }
        return true;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: M
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void a(int r2, org.games4all.game.move.c r3) {
        /*
            r1 = this;
            M r0 = r1.b
            org.games4all.game.model.f r0 = r0.k(r2)
            org.games4all.game.controller.a r0 = r0.c()
            r0.a(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.controller.server.b.a(int, org.games4all.game.move.c):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: G
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public org.games4all.game.move.c a(int r5, org.games4all.game.move.Move r6) {
        /*
            r4 = this;
            M r0 = r4.b
            org.games4all.game.lifecycle.Stage r0 = r0.q()
            org.games4all.game.lifecycle.Stage r1 = org.games4all.game.lifecycle.Stage.MOVE
            if (r0 == r1) goto L_0x0029
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Stage: "
            java.lang.StringBuilder r1 = r1.append(r2)
            M r2 = r4.b
            org.games4all.game.lifecycle.Stage r2 = r2.q()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0029:
            G r0 = r4.a
            org.games4all.game.e r0 = r0.n()
            org.games4all.game.move.c r0 = r6.a(r5, r0)
            boolean r1 = r0.a()
            if (r1 == 0) goto L_0x0046
            G r0 = r4.a
            org.games4all.game.move.c r0 = r0.a(r5, r6)
        L_0x003f:
            r4.a(r5, r0)
            r4.h()
            return r0
        L_0x0046:
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "warning, move "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = " failed: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ", model: "
            java.lang.StringBuilder r2 = r2.append(r3)
            M r3 = r4.b
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.println(r2)
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.controller.server.b.a(int, org.games4all.game.move.Move):org.games4all.game.move.c");
    }

    public void h() {
        do {
            j();
            if (this.b.q() != Stage.SESSION) {
                i();
                a();
            }
        } while (this.b.v() != Stage.DONE);
    }

    /* access modifiers changed from: protected */
    public void i() {
        d m = f().m();
        j i = m.i();
        switch (m.g()) {
            case MATCH:
                i.a(i.a() + 1);
                i.b(0);
                i.c(0);
                i.d(0);
                i.e(0);
                break;
            case GAME:
                i.b(i.b() + 1);
                i.c(0);
                i.d(0);
                i.e(0);
                break;
            case ROUND:
                i.c(i.c() + 1);
                i.d(0);
                i.e(0);
                break;
            case TURN:
                i.d(i.d() + 1);
                i.e(0);
                break;
            case MOVE:
                i.e(i.e() + 1);
                break;
            default:
                throw new RuntimeException("Illegal 'next' " + m.g());
        }
        k();
    }

    /* access modifiers changed from: protected */
    public void j() {
        a(Transition.END);
        Stage u = this.a.u();
        d m = this.b.m();
        Stage stage = u;
        Stage g = m.g();
        while (stage.compareTo((Enum) g) < 0) {
            g = g.a();
            m.a(g);
            a(Transition.END);
            stage = this.a.u();
        }
        this.a.t();
    }

    /* access modifiers changed from: protected */
    public void k() {
        d m = this.b.m();
        Stage g = m.g();
        do {
            m.a(g);
            a(Transition.START);
            g = g.b();
            if (g == Stage.DONE) {
                return;
            }
        } while (this.b.v() == Stage.DONE);
    }

    private void a(Transition transition) {
        this.b.m().j().a(StageTransition.a(this.b.q(), transition));
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public void d() {
        this.a.d();
    }

    public void c() {
        this.a.p();
    }
}
