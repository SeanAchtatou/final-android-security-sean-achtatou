package org.games4all.game.controller.server;

import org.games4all.game.h;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.model.d;
import org.games4all.game.model.e;
import org.games4all.game.move.Move;
import org.games4all.game.table.a;

public abstract class c<G extends h<M>, M extends e<?, ?, ?>> extends b<G, M> implements d {
    private final boolean[] a;
    private int b;
    private final boolean[] c;
    private final Stage d = Stage.GAME;

    public c(G g) {
        super(g);
        f().m().a((org.games4all.game.lifecycle.c) new org.games4all.game.lifecycle.h(d.class.getSimpleName()));
        int g2 = g();
        this.a = new boolean[g2];
        this.c = new boolean[g2];
    }

    public void a(int i) {
        if (!this.a[i]) {
            this.a[i] = true;
            this.b--;
            if (this.b == 0) {
                super.i();
                e f = f();
                if (f.v() != Stage.DONE) {
                    h();
                }
                f.k();
                a();
            }
        }
    }

    private boolean l() {
        if (this.b > 0) {
            return true;
        }
        if (f().q().compareTo((Enum) this.d) > 0) {
            return false;
        }
        m();
        return true;
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (!l()) {
            if (e().v()) {
                n();
            }
            super.i();
        }
    }

    private void m() {
        e f = f();
        a f2 = f.m().f();
        int g = g();
        this.b = 0;
        for (int i = 0; i < g; i++) {
            if (f2.a(i).b()) {
                this.a[i] = true;
            } else {
                this.a[i] = false;
                this.b++;
            }
        }
        f.j();
    }

    private void n() {
        int g = g();
        for (int i = 0; i < g; i++) {
            if (!f().m().f().a(i).b()) {
                b(i);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i) {
        f().k(i).d().a();
        this.c[i] = true;
    }

    /* access modifiers changed from: protected */
    public void a(int i, boolean z) {
        this.c[i] = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.game.controller.server.c.a(int, boolean):void
     arg types: [int, int]
     candidates:
      org.games4all.game.controller.server.c.a(int, org.games4all.game.move.Move):org.games4all.game.move.c
      org.games4all.game.controller.server.b.a(org.games4all.game.model.e<?, ?, ?>, org.games4all.game.controller.server.GameSeed):void
      org.games4all.game.controller.server.b.a(org.games4all.game.PlayerInfo, org.games4all.game.option.b):int
      org.games4all.game.controller.server.b.a(int, org.games4all.game.move.Move):org.games4all.game.move.c
      org.games4all.game.controller.server.b.a(int, org.games4all.game.move.c):void
      org.games4all.game.controller.server.a.a(org.games4all.game.PlayerInfo, org.games4all.game.option.b):int
      org.games4all.game.controller.server.a.a(int, org.games4all.game.move.Move):org.games4all.game.move.c
      org.games4all.game.controller.server.a.a(org.games4all.game.PlayerInfo, org.games4all.game.option.b):int
      org.games4all.game.controller.server.a.a(int, org.games4all.game.move.Move):org.games4all.game.move.c
      org.games4all.game.controller.server.c.a(int, boolean):void */
    public void c(int i) {
        a(i, false);
        f().k(i).d().b();
    }

    /* access modifiers changed from: protected */
    public void a(d dVar) {
        super.a(dVar);
        l();
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public org.games4all.game.move.c a(int i, Move move) {
        if (!this.c[i]) {
            return super.a(i, move);
        }
        System.err.println("WARN: ignoring move " + move + " because player " + i + " suspended.");
        return null;
    }
}
