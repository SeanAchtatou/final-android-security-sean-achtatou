package org.games4all.game.controller.a;

import org.games4all.game.lifecycle.i;
import org.games4all.game.model.e;
import org.games4all.game.model.f;
import org.games4all.game.move.Move;

public class b implements c {
    private final e a;
    private final int b;
    private final org.games4all.game.e.a c;
    private final org.games4all.c.c<org.games4all.game.lifecycle.e> d = new org.games4all.c.c<>(org.games4all.game.lifecycle.e.class);
    private final org.games4all.c.d e;

    public b(e eVar, int i, org.games4all.game.e.a aVar) {
        this.a = eVar;
        this.b = i;
        this.c = aVar;
        org.games4all.game.lifecycle.e c2 = this.d.c();
        org.games4all.game.model.d m = eVar.m();
        f k = eVar.k(i);
        this.e = new org.games4all.c.d();
        this.e.a(m.a(new a(c2)));
        this.e.a(k.c(new C0020b(c2)));
        this.e.a(k.a(new c(c2)));
        this.e.a(eVar.a(new d(c2)));
    }

    class a implements i {
        final /* synthetic */ org.games4all.game.lifecycle.e a;

        a(org.games4all.game.lifecycle.e eVar) {
            this.a = eVar;
        }

        public void a() {
            this.a.c();
        }

        public void b() {
            this.a.d();
        }
    }

    /* renamed from: org.games4all.game.controller.a.b$b  reason: collision with other inner class name */
    class C0020b implements org.games4all.game.controller.a {
        final /* synthetic */ org.games4all.game.lifecycle.e a;

        C0020b(org.games4all.game.lifecycle.e eVar) {
            this.a = eVar;
        }

        public void a(org.games4all.game.move.c cVar) {
            this.a.a(cVar);
        }
    }

    class c implements org.games4all.game.lifecycle.d {
        final /* synthetic */ org.games4all.game.lifecycle.e a;

        c(org.games4all.game.lifecycle.e eVar) {
            this.a = eVar;
        }

        public void a() {
            this.a.e();
        }

        public void b() {
            this.a.f();
        }
    }

    class d implements org.games4all.game.lifecycle.f {
        final /* synthetic */ org.games4all.game.lifecycle.e a;

        d(org.games4all.game.lifecycle.e eVar) {
            this.a = eVar;
        }

        public void a() {
            this.a.g();
        }

        public void b() {
            this.a.h();
        }
    }

    /* access modifiers changed from: protected */
    public org.games4all.game.lifecycle.e h() {
        return this.d.c();
    }

    /* access modifiers changed from: protected */
    public void a(org.games4all.c.b bVar) {
        this.e.a(bVar);
    }

    public org.games4all.c.b a(org.games4all.game.lifecycle.e eVar) {
        return this.d.c(eVar);
    }

    public org.games4all.c.b a(org.games4all.game.lifecycle.c cVar) {
        return this.a.m().a(cVar);
    }

    public e a() {
        return this.a;
    }

    public int b() {
        return this.b;
    }

    public org.games4all.game.e.a c() {
        return this.c;
    }

    public boolean d() {
        return false;
    }

    public void a(Move move) {
        throw new UnsupportedOperationException();
    }

    public void e() {
        throw new UnsupportedOperationException();
    }

    public void f() {
        throw new UnsupportedOperationException();
    }

    public void g() {
        this.e.a();
    }
}
