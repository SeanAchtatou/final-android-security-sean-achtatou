package org.games4all.game.controller.a;

import org.games4all.game.lifecycle.Stage;
import org.games4all.game.lifecycle.StageTransition;
import org.games4all.game.lifecycle.Transition;
import org.games4all.game.lifecycle.i;
import org.games4all.game.model.e;

public class k implements g {
    static final /* synthetic */ boolean a = (!k.class.desiredAssertionStatus());
    private final org.games4all.game.b.c b;
    private final org.games4all.c.c<a> c = new org.games4all.c.c<>(a.class);
    private final org.games4all.c.d d;
    private boolean e;
    private boolean f;
    private boolean g;

    /* JADX WARN: Type inference failed for: r1v1, types: [org.games4all.game.model.d] */
    /* JADX WARN: Type inference failed for: r0v2, types: [org.games4all.game.model.f] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public k(org.games4all.game.b.c r5) {
        /*
            r4 = this;
            r4.<init>()
            r4.b = r5
            org.games4all.c.c r0 = new org.games4all.c.c
            java.lang.Class<org.games4all.game.controller.a.a> r1 = org.games4all.game.controller.a.a.class
            r0.<init>(r1)
            r4.c = r0
            org.games4all.game.model.e r0 = r5.b()
            org.games4all.game.model.d r1 = r0.m()
            int r2 = r5.a()
            org.games4all.game.model.f r0 = r0.k(r2)
            org.games4all.c.d r2 = new org.games4all.c.d
            r2.<init>()
            r4.d = r2
            org.games4all.c.d r2 = r4.d
            org.games4all.game.lifecycle.i r3 = r4.e()
            org.games4all.c.b r3 = r1.a(r3)
            r2.a(r3)
            org.games4all.c.d r2 = r4.d
            org.games4all.game.lifecycle.c r3 = r4.f()
            org.games4all.c.b r1 = r1.a(r3)
            r2.a(r1)
            org.games4all.c.d r1 = r4.d
            org.games4all.game.controller.a.j r2 = r4.g()
            org.games4all.c.b r2 = r5.a(r2)
            r1.a(r2)
            org.games4all.c.d r1 = r4.d
            org.games4all.game.lifecycle.d r2 = r4.h()
            org.games4all.c.b r0 = r0.a(r2)
            r1.a(r0)
            r4.b()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.controller.a.k.<init>(org.games4all.game.b.c):void");
    }

    public org.games4all.c.b a(a aVar) {
        return this.c.c(aVar);
    }

    public void d() {
        this.d.a();
    }

    public boolean a() {
        return this.e;
    }

    class a implements i {
        a() {
        }

        public void a() {
            k.this.b();
        }

        public void b() {
            k.this.c();
        }
    }

    private i e() {
        return new a();
    }

    class b implements org.games4all.game.lifecycle.c {
        b() {
        }

        public void a(StageTransition stageTransition) {
            if (stageTransition.a() != Stage.MOVE) {
                return;
            }
            if (stageTransition.b() == Transition.START) {
                k.this.b();
            } else {
                k.this.c();
            }
        }
    }

    private org.games4all.game.lifecycle.c f() {
        return new b();
    }

    class c implements j {
        c() {
        }

        public void a(org.games4all.game.move.c cVar) {
            k.this.b(false);
        }

        public void a() {
            k.this.b(true);
            k.this.c();
        }
    }

    private j g() {
        return new c();
    }

    class d implements org.games4all.game.lifecycle.d {
        d() {
        }

        public void b() {
            k.this.a(false);
        }

        public void a() {
            k.this.a(true);
        }
    }

    private org.games4all.game.lifecycle.d h() {
        return new d();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.g = z;
        if (z) {
            c();
        } else {
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        if (a || this.f != z) {
            this.f = z;
            return;
        }
        throw new AssertionError();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [org.games4all.game.model.d] */
    /* access modifiers changed from: package-private */
    public void b() {
        boolean z;
        if (!this.e) {
            e<?, ?, ?> b2 = this.b.b();
            int a2 = this.b.a();
            ? m = b2.m();
            if (this.f || this.g || !m.k() || m.g() != Stage.MOVE || !b2.a(a2)) {
                z = false;
            } else {
                z = true;
            }
            this.e = z;
            if (this.e) {
                this.c.c().a();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.e) {
            this.e = false;
            this.c.c().b();
        }
    }
}
