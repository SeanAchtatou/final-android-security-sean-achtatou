package org.games4all.game.controller.server;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import org.games4all.game.model.e;

public class GameSeed implements Serializable {
    private static Random a = null;
    private static final long serialVersionUID = -6514848349463672894L;
    private long[] seeds;

    public static final void a(Random random) {
        a = random;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [org.games4all.game.model.b] */
    /* JADX WARN: Type inference failed for: r3v1, types: [org.games4all.game.model.f] */
    public static GameSeed a(e<?, ?, ?> eVar) {
        ? l = eVar.l();
        int n = eVar.n();
        long[] jArr = new long[(n + 1)];
        jArr[0] = l.a().a();
        for (int i = 0; i < n; i++) {
            jArr[i + 1] = eVar.k(i).e().a();
        }
        return new GameSeed(jArr);
    }

    public GameSeed(long[] jArr) {
        this.seeds = new long[jArr.length];
        for (int i = 0; i < jArr.length; i++) {
            this.seeds[i] = jArr[i];
        }
    }

    public GameSeed(int i) {
        if (a == null) {
            try {
                a = SecureRandom.getInstance("SHA1PRNG");
                a.setSeed(System.currentTimeMillis());
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
        this.seeds = new long[(i + 1)];
        for (int i2 = 0; i2 <= i; i2++) {
            this.seeds[i2] = a.nextLong();
        }
    }

    public GameSeed() {
    }

    public long a() {
        return this.seeds[0];
    }

    public long a(int i) {
        return this.seeds[i + 1];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (long j : this.seeds) {
            if (z) {
                z = false;
            } else {
                sb.append(',');
            }
            sb.append(j);
        }
        return sb.toString();
    }
}
