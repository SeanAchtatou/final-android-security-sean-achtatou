package org.games4all.game.controller.a;

import org.games4all.c.b;
import org.games4all.game.b.c;
import org.games4all.game.model.e;
import org.games4all.game.move.Move;
import org.games4all.util.k;

public abstract class h implements c, k {
    private final int a;
    private final e<?, ?, ?> b;
    private final org.games4all.game.controller.a c = f();
    private final org.games4all.c.c<j> d;

    /* access modifiers changed from: protected */
    public abstract void b(Move move);

    /* JADX WARN: Type inference failed for: r0v0, types: [org.games4all.game.model.f] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public h(org.games4all.game.model.e<?, ?, ?> r3, int r4) {
        /*
            r2 = this;
            r2.<init>()
            r2.b = r3
            r2.a = r4
            org.games4all.game.model.f r0 = r3.k(r4)
            org.games4all.game.controller.a r1 = r2.f()
            r2.c = r1
            org.games4all.game.controller.a r1 = r2.c
            r0.a(r1)
            org.games4all.c.c r0 = new org.games4all.c.c
            java.lang.Class<org.games4all.game.controller.a.j> r1 = org.games4all.game.controller.a.j.class
            r0.<init>(r1)
            r2.d = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.controller.a.h.<init>(org.games4all.game.model.e, int):void");
    }

    public b a(j jVar) {
        return this.d.c(jVar);
    }

    /* access modifiers changed from: package-private */
    public void a(org.games4all.game.move.c cVar) {
        this.d.c().a(cVar);
    }

    class a implements org.games4all.game.controller.a {
        a() {
        }

        public void a(org.games4all.game.move.c cVar) {
            h.this.a(cVar);
        }
    }

    private org.games4all.game.controller.a f() {
        return new a();
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [org.games4all.game.model.f] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void d() {
        /*
            r2 = this;
            org.games4all.game.model.e<?, ?, ?> r0 = r2.b
            int r1 = r2.a
            org.games4all.game.model.f r0 = r0.k(r1)
            org.games4all.game.controller.a r1 = r2.c
            r0.b(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.game.controller.a.h.d():void");
    }

    public void a(Move move) {
        this.d.c().a();
        b(move);
    }

    public int a() {
        return this.a;
    }

    public e<?, ?, ?> b() {
        return this.b;
    }
}
