package org.games4all.game.controller.a;

import org.games4all.game.b.c;
import org.games4all.game.model.e;
import org.games4all.game.move.Move;

public class f extends b {
    private final g a;
    private final c b;

    public f(e eVar, int i, org.games4all.game.e.a aVar, g gVar, c cVar) {
        super(eVar, i, aVar);
        this.a = gVar;
        this.b = cVar;
        a(gVar.a(new a(h())));
    }

    class a implements a {
        final /* synthetic */ org.games4all.game.lifecycle.e a;

        a(org.games4all.game.lifecycle.e eVar) {
            this.a = eVar;
        }

        public void a() {
            this.a.a();
        }

        public void b() {
            this.a.b();
        }
    }

    public boolean d() {
        return this.a.a();
    }

    public void a(Move move) {
        this.b.a(move);
    }

    public void e() {
        this.b.c();
    }

    public void f() {
        this.b.e();
    }
}
