package org.games4all.logging;

public class e {
    private static a a = new b(System.err, new c(), false);
    private static e b;
    private String c;
    private LogLevel d = LogLevel.WARN;
    private a e = a;

    public static void a(e eVar) {
        b = eVar;
    }

    public static e a(String str) {
        return new e(str);
    }

    public static e a(Class<?> cls) {
        return a(cls.getSimpleName());
    }

    protected e(String str) {
        b(str);
    }

    public String a() {
        return this.c;
    }

    public void b(String str) {
        this.c = str;
    }

    public e a(a aVar) {
        this.e = aVar;
        return this;
    }

    public boolean a(LogLevel logLevel) {
        return logLevel.compareTo(this.d) >= 0;
    }

    public e b(LogLevel logLevel) {
        this.d = logLevel;
        return this;
    }

    public void a(LogLevel logLevel, String str) {
        a(logLevel, str, null);
    }

    public void a(LogLevel logLevel, String str, Throwable th) {
        if (a(logLevel)) {
            this.e.a(this, logLevel, str, th);
        }
    }

    public void c(String str) {
        a(LogLevel.INFO, str);
    }

    public void d(String str) {
        a(LogLevel.WARN, str);
    }
}
