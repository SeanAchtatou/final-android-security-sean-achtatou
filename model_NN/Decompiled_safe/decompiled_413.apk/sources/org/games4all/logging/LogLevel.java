package org.games4all.logging;

public enum LogLevel {
    TRACE,
    DEBUG,
    CONFIG,
    INFO,
    WARN,
    ERROR,
    FATAL
}
