package org.games4all.logging;

import java.io.PrintStream;

public class b implements a {
    private final PrintStream a;
    private final d b;
    private final boolean c;

    public b(PrintStream printStream, d dVar, boolean z) {
        this.a = printStream;
        this.b = dVar;
        this.c = z;
    }

    public void a(e eVar, LogLevel logLevel, String str, Throwable th) {
        this.a.println(this.b.a(eVar, logLevel, str, th));
        if (this.c) {
            this.a.flush();
        }
    }
}
