package org.games4all.logging;

import java.io.PrintWriter;
import java.io.StringWriter;

public class c implements d {
    public String a(e eVar, LogLevel logLevel, String str, Throwable th) {
        h a = h.a();
        String b = a == null ? "" : a.b();
        String str2 = "";
        if (th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            printWriter.close();
            str2 = " " + stringWriter.toString();
        }
        return String.format("%d %s %-6s [%s] %s%s", Long.valueOf(System.currentTimeMillis()), logLevel, b, eVar.a(), str, str2);
    }
}
