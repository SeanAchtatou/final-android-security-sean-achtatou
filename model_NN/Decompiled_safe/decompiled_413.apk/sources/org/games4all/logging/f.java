package org.games4all.logging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.zip.GZIPOutputStream;

public class f implements a {
    private final String[] a;
    private final d b;
    private int c = 0;
    private int d = 0;

    public f(int i, d dVar) {
        this.a = new String[(i + 1)];
        this.b = dVar;
    }

    public void a(e eVar, LogLevel logLevel, String str, Throwable th) {
        this.a[this.d] = this.b.a(eVar, logLevel, str, th);
        this.d = (this.d + 1) % this.a.length;
        if (this.d == this.c) {
            this.c = (this.d + 1) % this.a.length;
        }
    }

    public int a() {
        return ((this.d - this.c) + this.a.length) % this.a.length;
    }

    public String a(int i) {
        return this.a[(this.c + i) % this.a.length];
    }

    public byte[] b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            PrintStream printStream = new PrintStream(new GZIPOutputStream(byteArrayOutputStream));
            int a2 = a();
            for (int i = 0; i < a2; i++) {
                printStream.println(a(i));
            }
            printStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int a2 = a();
        for (int i = 0; i < a2; i++) {
            sb.append(a(i)).append(10);
        }
        return sb.toString();
    }
}
