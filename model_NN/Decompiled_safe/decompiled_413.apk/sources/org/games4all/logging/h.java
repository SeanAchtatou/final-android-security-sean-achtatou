package org.games4all.logging;

import java.util.Stack;

public class h {
    private static ThreadLocal<Stack<h>> b = new ThreadLocal<>();
    public String a;

    public static h a() {
        Stack stack = b.get();
        if (stack == null) {
            return null;
        }
        return (h) stack.peek();
    }

    public String b() {
        return this.a;
    }
}
