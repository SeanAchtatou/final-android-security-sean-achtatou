package org.games4all.gamestore.client;

public class LoadMatchFailedException extends Exception {
    private static final long serialVersionUID = 3146019449078082107L;
    private final int code;

    public LoadMatchFailedException(String str, int i) {
        super(str);
        this.code = i;
    }
}
