package org.games4all.gamestore.client;

public class LoadGameFailedException extends Exception {
    private static final long serialVersionUID = -1739075543615913020L;
    private final int code;

    public LoadGameFailedException(String str, int i) {
        super(str);
        this.code = i;
    }

    public int a() {
        return this.code;
    }
}
