package org.games4all.gamestore.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.games4all.b.a.c;
import org.games4all.util.g;

public class b {
    private final g a;
    private String b;
    private boolean c;
    private boolean d;
    private org.games4all.b.a.b e;

    public b(g gVar) {
        this.a = gVar;
        h();
    }

    public boolean a() {
        return this.d;
    }

    public boolean b() {
        return this.c;
    }

    public String c() {
        if (this.e == null) {
            return null;
        }
        return this.e.b();
    }

    public String d() {
        return this.b;
    }

    public void e() {
        f();
        this.c = false;
        this.d = false;
    }

    public void f() {
        this.e = new org.games4all.b.a.b(0, "", new Date());
        this.d = false;
        this.b = null;
        this.c = true;
        this.a.b();
        this.a.a("last-account");
        this.a.c();
    }

    public void a(c cVar) {
        this.e = (org.games4all.b.a.b) cVar;
        this.d = true;
        String b2 = this.e.b();
        this.b = b2;
        this.a.b();
        this.a.b("account." + b2, this.e.c());
        this.a.b("last-account", this.b);
        this.a.c();
        this.c = true;
    }

    public org.games4all.b.a.b g() {
        return this.e;
    }

    public void h() {
        this.b = this.a.a("last-account", null);
    }

    public void a(String str) {
        this.a.b();
        this.a.a("account." + str);
        this.a.c();
        if (str.equals(this.b)) {
            e();
        }
    }

    public List<String> i() {
        ArrayList arrayList = new ArrayList();
        for (String next : this.a.a().keySet()) {
            if (next.startsWith("account.")) {
                arrayList.add(next.substring("account.".length()));
            }
        }
        return arrayList;
    }

    public String b(String str) {
        return this.a.a("account." + str, null);
    }
}
