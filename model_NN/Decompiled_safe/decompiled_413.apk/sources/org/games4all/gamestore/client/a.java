package org.games4all.gamestore.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.games4all.b.a.b;
import org.games4all.game.c;
import org.games4all.game.controller.server.GameSeed;
import org.games4all.game.model.e;
import org.games4all.game.move.PlayerMove;
import org.games4all.game.rating.RatingDescriptor;
import org.games4all.game.rating.d;
import org.games4all.game.rating.i;
import org.games4all.json.h;
import org.games4all.match.Match;
import org.games4all.match.MatchResult;
import org.games4all.util.SoftwareVersion;
import org.games4all.util.g;

public class a {
    private String a;
    private final SoftwareVersion b;
    private final c c;
    private final b d;
    private final i e;
    private final d f = new d();
    private final h g = d.a(this.c.c());

    public a(String str, SoftwareVersion softwareVersion, Class<? extends Enum<?>> cls, g gVar, i iVar) {
        this.a = str;
        this.b = softwareVersion;
        this.c = new c(cls);
        this.e = iVar;
        this.d = new b(gVar);
        this.g.b(false);
    }

    public void a(String str) {
        this.a = str;
    }

    public String a() {
        return this.a + "/login";
    }

    public String b() {
        return this.a + "/create-account";
    }

    public Set<org.games4all.game.g> c() {
        return this.c.a();
    }

    public b d() {
        return this.d;
    }

    public i e() {
        if (!this.d.b()) {
            return null;
        }
        if (this.d.a()) {
            return this.f;
        }
        return this.e;
    }

    private void a(StringBuilder sb, String str, String str2) {
        try {
            sb.append('?');
            sb.append("name").append('=').append(URLEncoder.encode(str, "UTF-8"));
            if (str2 != null) {
                sb.append('&');
                sb.append("password").append('=').append(URLEncoder.encode(str2, "UTF-8"));
            }
            sb.append('&');
            sb.append("version").append('=').append(this.b.toString());
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException(e2);
        }
    }

    public String a(String str, String str2, String str3, String str4) {
        StringBuilder sb = new StringBuilder(str);
        try {
            a(sb, str2, str3);
            sb.append('&');
            sb.append("email").append('=').append(URLEncoder.encode(str4, "UTF-8"));
            sb.append('&');
            sb.append("game").append('=').append(this.c.b());
            System.err.println("TRYING URL: " + ((Object) sb));
            try {
                HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(sb.toString()));
                StatusLine statusLine = execute.getStatusLine();
                if (statusLine.getStatusCode() != 200) {
                    return statusLine.getReasonPhrase();
                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(execute.getEntity().getContent()));
                String trim = bufferedReader.readLine().trim();
                if (trim.startsWith("!")) {
                    return trim.substring(1);
                }
                org.games4all.b.a.c a2 = b.a(trim);
                this.d.a(a2);
                this.f.b();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null && !readLine.equals("DONE")) {
                        RatingDescriptor d2 = RatingDescriptor.d(readLine);
                        this.f.a(d2);
                        String readLine2 = bufferedReader.readLine();
                        if (!readLine2.equals("")) {
                            String[] split = readLine2.split(",");
                            this.f.a(a2, d2, Long.parseLong(split[0]), Long.parseLong(split[1]), Integer.parseInt(split[2]));
                        }
                    }
                }
                return null;
            } catch (ClientProtocolException e2) {
                e2.printStackTrace();
                return e2.getMessage();
            } catch (IOException e3) {
                e3.printStackTrace();
                return e3.getMessage();
            }
        } catch (UnsupportedEncodingException e4) {
            throw new RuntimeException(e4);
        }
    }

    public c a(int i) {
        InputStream inputStream;
        String str = this.a + "/load-game";
        b g2 = this.d.g();
        String b2 = g2.b();
        String c2 = g2.c();
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            StringBuilder sb = new StringBuilder(str);
            a(sb, b2, c2);
            sb.append('&');
            sb.append("id=").append(i);
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(sb.toString()));
            HttpEntity entity = execute.getEntity();
            StatusLine statusLine = execute.getStatusLine();
            if (statusLine.getStatusCode() != 200 || entity == null) {
                throw new LoadGameFailedException(statusLine.getReasonPhrase(), statusLine.getStatusCode());
            }
            InputStream content = entity.getContent();
            Header firstHeader = execute.getFirstHeader("Content-Encoding");
            if (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("gzip")) {
                inputStream = content;
            } else {
                inputStream = new GZIPInputStream(content);
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String readLine = bufferedReader.readLine();
            if (readLine.startsWith("!")) {
                throw new LoadGameFailedException(readLine.substring(1), 200);
            }
            long parseLong = Long.parseLong(readLine);
            if (!a(parseLong)) {
                throw new LoadGameFailedException("Unsupported variant: " + parseLong, -1);
            }
            String readLine2 = bufferedReader.readLine();
            String readLine3 = bufferedReader.readLine();
            bufferedReader.close();
            return new c((List) this.g.a(readLine2), (e) this.g.a(readLine3));
        } catch (ClientProtocolException e2) {
            throw new RuntimeException((Throwable) e2);
        } catch (IOException e3) {
            throw new LoadGameFailedException(e3.getMessage(), -1);
        }
    }

    public Match a(long j, int i, MatchResult matchResult) {
        String str = this.a + "/new-match";
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(str);
            b g2 = this.d.g();
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(new BasicNameValuePair("name", g2.b()));
            arrayList.add(new BasicNameValuePair("password", g2.c()));
            arrayList.add(new BasicNameValuePair("version", this.b.toString()));
            arrayList.add(new BasicNameValuePair("variant", String.valueOf(j)));
            arrayList.add(new BasicNameValuePair("count", String.valueOf(i)));
            if (matchResult != null) {
                String a2 = this.g.a(matchResult);
                System.err.println("PREV RESULT SIZE: " + a2.length());
                arrayList.add(new BasicNameValuePair("result", a2));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            StatusLine statusLine = execute.getStatusLine();
            if (statusLine.getStatusCode() == 200) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(execute.getEntity().getContent()));
                String readLine = bufferedReader.readLine();
                if (readLine.startsWith("!")) {
                    throw new LoadMatchFailedException(readLine.substring(1), 200);
                }
                System.err.println("JSON LINE SIZE: " + readLine.length());
                Match match = (Match) this.g.a(readLine);
                match.e();
                String trim = bufferedReader.readLine().trim();
                if (!trim.equals("")) {
                    for (String str2 : trim.split(",")) {
                        String[] split = str2.split("\\|");
                        long parseLong = Long.parseLong(split[0]);
                        long parseLong2 = Long.parseLong(split[1]);
                        long parseLong3 = Long.parseLong(split[2]);
                        int parseInt = Integer.parseInt(split[3]);
                        RatingDescriptor a3 = this.f.a(parseLong);
                        if (a3 == null) {
                            System.err.println("Unknown descriptor: " + parseLong + ", spec: " + str2);
                        } else {
                            this.f.a(g2, a3, parseLong2, parseLong3, parseInt);
                        }
                    }
                }
                return match;
            }
            throw new LoadMatchFailedException(statusLine.getReasonPhrase(), statusLine.getStatusCode());
        } catch (ClientProtocolException e2) {
            throw new LoadMatchFailedException(e2.getMessage(), -1);
        } catch (IOException e3) {
            throw new LoadMatchFailedException(e3.getMessage(), -1);
        }
    }

    private boolean a(long j) {
        for (org.games4all.game.g a2 : c()) {
            if (a2.a() == j) {
                return true;
            }
        }
        return false;
    }

    public int a(String str, String str2, org.games4all.json.jsonorg.c cVar, byte[] bArr, GameSeed gameSeed, e<?, ?, ?> eVar, List<List<PlayerMove>> list, byte[] bArr2) {
        InputStream inputStream;
        org.apache.http.entity.mime.d dVar = new org.apache.http.entity.mime.d();
        a(dVar, cVar);
        if (bArr != null) {
            a(dVar, bArr);
        }
        if (gameSeed != null) {
            a(dVar, gameSeed);
        }
        if (list != null) {
            a(dVar, list);
        }
        if (eVar != null) {
            a(dVar, eVar);
        }
        if (bArr2 != null) {
            b(dVar, bArr2);
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(a(str, str2));
        httpPost.setEntity(dVar);
        HttpResponse execute = defaultHttpClient.execute(httpPost);
        InputStream content = execute.getEntity().getContent();
        Header firstHeader = execute.getFirstHeader("Content-Encoding");
        if (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("gzip")) {
            inputStream = content;
        } else {
            inputStream = new GZIPInputStream(content);
        }
        String b2 = org.games4all.util.c.b(inputStream);
        inputStream.close();
        return Integer.parseInt(b2.trim());
    }

    public void a(String str, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
        org.apache.http.entity.mime.d dVar = new org.apache.http.entity.mime.d();
        dVar.a("report", new org.apache.http.entity.mime.a.b(str));
        a(dVar, "screenshot", bArr);
        a(dVar, "model", bArr2);
        a(dVar, "moves", bArr3);
        a(dVar, "log", bArr4);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(g());
        httpPost.setEntity(dVar);
        defaultHttpClient.execute(httpPost);
    }

    private void a(org.apache.http.entity.mime.d dVar, String str, byte[] bArr) {
        if (bArr != null) {
            dVar.a(str, new f(str, bArr));
        }
    }

    private void a(org.apache.http.entity.mime.d dVar, byte[] bArr) {
        a(dVar, "screenshot", bArr);
    }

    private void a(org.apache.http.entity.mime.d dVar, org.games4all.json.jsonorg.c cVar) {
        try {
            dVar.a("report", new org.apache.http.entity.mime.a.b(cVar.toString()));
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException(e2);
        }
    }

    private void a(org.apache.http.entity.mime.d dVar, e<?, ?, ?> eVar) {
        try {
            String a2 = new h().a(eVar);
            System.err.println(a2);
            dVar.a("model", new org.apache.http.entity.mime.a.b(a2));
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    private void a(org.apache.http.entity.mime.d dVar, GameSeed gameSeed) {
        try {
            dVar.a("seed", new org.apache.http.entity.mime.a.b(gameSeed.toString()));
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException(e2);
        }
    }

    private void a(org.apache.http.entity.mime.d dVar, List<List<PlayerMove>> list) {
        try {
            dVar.a("moves", new org.apache.http.entity.mime.a.b(this.g.a(list)));
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    private void b(org.apache.http.entity.mime.d dVar, byte[] bArr) {
        dVar.a("log", new f("log", bArr));
    }

    private String a(String str, String str2) {
        StringBuilder sb = new StringBuilder(this.a);
        sb.append("/submit-report");
        a(sb, str, str2);
        return sb.toString();
    }

    private String g() {
        StringBuilder sb = new StringBuilder(this.a);
        sb.append("/submit-crash");
        if (this.d.a()) {
            b g2 = this.d.g();
            a(sb, g2.b(), g2.c());
        }
        return sb.toString();
    }

    public SoftwareVersion f() {
        return this.b;
    }
}
