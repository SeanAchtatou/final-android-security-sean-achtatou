package org.games4all.gamestore.client;

import java.util.List;
import org.games4all.game.model.e;
import org.games4all.game.move.PlayerMove;

public class c {
    private final List<List<PlayerMove>> a;
    private final e<?, ?, ?> b;

    public c(List<List<PlayerMove>> list, e<?, ?, ?> eVar) {
        this.a = list;
        this.b = eVar;
    }

    public List<List<PlayerMove>> a() {
        return this.a;
    }

    public e<?, ?, ?> b() {
        return this.b;
    }
}
