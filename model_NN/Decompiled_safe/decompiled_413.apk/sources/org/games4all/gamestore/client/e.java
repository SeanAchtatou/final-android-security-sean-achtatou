package org.games4all.gamestore.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import org.games4all.game.rating.Rating;
import org.games4all.game.rating.RatingDescriptor;

public class e extends d {
    private final File c;

    public e(File file) {
        this.c = file;
        d();
    }

    private void d() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(this.c));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    String trim = readLine.trim();
                    if (trim.length() > 0) {
                        char charAt = trim.charAt(0);
                        if (charAt == '!') {
                            b(trim.substring(1));
                        } else if (charAt == '+') {
                            c(trim.substring(1));
                        }
                    }
                } else {
                    bufferedReader.close();
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    private void b(String str) {
    }

    private void c(String str) {
        String[] split = str.split("|");
        int i = 0 + 1;
        int parseInt = Integer.parseInt(split[0]);
        int i2 = i + 1;
        int parseInt2 = Integer.parseInt(split[i]);
        int i3 = i2 + 1;
        int parseInt3 = Integer.parseInt(split[i2]);
        int i4 = i3 + 1;
        long parseLong = Long.parseLong(split[i3]);
        int i5 = i4 + 1;
        long parseLong2 = Long.parseLong(split[i4]);
        if (a((long) parseInt3) != null) {
            Rating rating = new Rating(parseInt, parseInt2, (long) parseInt3);
            rating.a(parseLong);
            rating.b(parseLong2);
            a(rating);
        }
    }

    private void e() {
        try {
            PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(this.c)));
            a(printWriter);
            b(printWriter);
            printWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void a(PrintWriter printWriter) {
        for (Long longValue : a()) {
            for (RatingDescriptor ratingDescriptor : b(longValue.longValue())) {
                printWriter.print('!');
                printWriter.println(ratingDescriptor.toString());
            }
        }
    }

    private void b(PrintWriter printWriter) {
        for (Integer intValue : c()) {
            for (Rating next : a(intValue.intValue()).values()) {
                printWriter.print('+');
                printWriter.print(next.a());
                printWriter.print('|');
                printWriter.print(next.b());
                printWriter.print('|');
                printWriter.print(next.c());
                printWriter.print('|');
                printWriter.print(next.d());
                printWriter.print('|');
                printWriter.print(next.e());
                printWriter.println();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Collection<Rating> collection) {
        super.a(collection);
        e();
    }

    public synchronized void a(RatingDescriptor ratingDescriptor) {
        RatingDescriptor a = a(ratingDescriptor.a());
        if (a == null) {
            super.a(ratingDescriptor);
        } else {
            a.a(ratingDescriptor.c());
            a.a(ratingDescriptor.d());
            a.b(ratingDescriptor.e());
            a.b(ratingDescriptor.g());
            a.c(ratingDescriptor.h());
        }
    }
}
