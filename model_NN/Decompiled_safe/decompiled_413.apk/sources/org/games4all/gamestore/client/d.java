package org.games4all.gamestore.client;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.games4all.b.a.c;
import org.games4all.game.rating.Rating;
import org.games4all.game.rating.RatingDescriptor;

public class d extends org.games4all.game.rating.d {
    static final /* synthetic */ boolean b = (!d.class.desiredAssertionStatus());
    private int c = 1;
    private final Map<Integer, Map<Long, Rating>> d = new HashMap();
    private final Map<Integer, Map<Long, Integer>> e = new HashMap();

    /* access modifiers changed from: protected */
    public Map<Long, Rating> a(int i) {
        Map<Long, Rating> map = this.d.get(Integer.valueOf(i));
        if (map != null) {
            return map;
        }
        HashMap hashMap = new HashMap();
        this.d.put(Integer.valueOf(i), hashMap);
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public void a(Collection<Rating> collection) {
        Map<Long, Rating> map = null;
        for (Rating next : collection) {
            long c2 = next.c();
            int b2 = next.b();
            if (map == null || -1 != b2) {
                map = a(b2);
            }
            map.put(Long.valueOf(c2), next);
        }
    }

    public Map<Long, Rating> a(long j, c cVar) {
        Map<Long, Rating> a = a(cVar.a());
        for (RatingDescriptor next : b(j)) {
            if (a.get(Long.valueOf(next.a())) == null) {
                a(cVar, next);
            }
        }
        return a;
    }

    private Rating a(c cVar, RatingDescriptor ratingDescriptor) {
        int a = cVar.a();
        long a2 = ratingDescriptor.a();
        long d2 = ratingDescriptor.d();
        long e2 = ratingDescriptor.e();
        int i = this.c;
        this.c = i + 1;
        Rating rating = new Rating(i, a, a2);
        rating.a(d2);
        rating.b(e2);
        a(rating);
        return rating;
    }

    /* access modifiers changed from: protected */
    public void a(Rating rating) {
        a(rating.b()).put(Long.valueOf(rating.c()), rating);
        this.c = Math.max(rating.a() + 1, this.c);
    }

    /* access modifiers changed from: protected */
    public void a(c cVar, RatingDescriptor ratingDescriptor, long j, long j2, int i) {
        Map<Long, Rating> a = a(cVar.a());
        if (b || ratingDescriptor != null) {
            long a2 = ratingDescriptor.a();
            Rating rating = a.get(Long.valueOf(a2));
            if (rating == null) {
                rating = a(cVar, ratingDescriptor);
            }
            rating.a(j);
            rating.b(j2);
            b(0, cVar).put(Long.valueOf(a2), Integer.valueOf(i));
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    public Set<Integer> c() {
        return this.d.keySet();
    }

    public void b() {
        this.d.clear();
        super.b();
    }

    public Map<Long, Integer> b(long j, c cVar) {
        int a = cVar.a();
        Map<Long, Integer> map = this.e.get(Integer.valueOf(a));
        if (map != null) {
            return map;
        }
        HashMap hashMap = new HashMap();
        this.e.put(Integer.valueOf(a), hashMap);
        return hashMap;
    }
}
