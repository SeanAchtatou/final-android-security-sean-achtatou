package org.games4all.gamestore.client;

import java.io.OutputStream;
import org.apache.http.entity.mime.a.a;

public class f extends a {
    private final String a;
    private final byte[] b;

    public f(String str, byte[] bArr) {
        this("application/octet-stream", str, bArr);
    }

    public f(String str, String str2, byte[] bArr) {
        super(str);
        this.a = str2;
        this.b = bArr;
    }

    public void a(OutputStream outputStream) {
        outputStream.write(this.b);
    }

    public String e() {
        return this.a;
    }

    public String b() {
        return null;
    }

    public long d() {
        return (long) this.b.length;
    }

    public String c() {
        return "binary";
    }
}
