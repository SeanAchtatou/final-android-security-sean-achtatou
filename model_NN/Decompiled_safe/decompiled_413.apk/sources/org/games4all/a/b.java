package org.games4all.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class b {
    private final Queue<a> a = new LinkedList();
    private final Map<String, List<C0004b>> b = new HashMap();

    public class a {
        private final Runnable b;
        private final String c;

        public a(Runnable runnable, String str) {
            this.b = runnable;
            this.c = str;
        }

        public void a() {
            this.b.run();
        }

        public String b() {
            return this.c;
        }
    }

    /* renamed from: org.games4all.a.b$b  reason: collision with other inner class name */
    public class C0004b {
        private final Runnable b;
        private final Runnable c;

        public C0004b(Runnable runnable, Runnable runnable2) {
            this.c = runnable2;
            this.b = runnable;
        }

        public void a() {
            if (this.b != null) {
                this.b.run();
            }
        }

        public void b() {
            if (this.c != null) {
                this.c.run();
            }
        }
    }

    public void a(Runnable runnable, String str) {
        this.a.offer(new a(runnable, str));
    }

    public void a(String str, Runnable runnable, Runnable runnable2) {
        Object obj = this.b.get(str);
        if (obj == null) {
            obj = new ArrayList();
            this.b.put(str, obj);
        }
        obj.add(new C0004b(runnable, runnable2));
    }

    public boolean a() {
        return this.a.isEmpty();
    }

    public void b() {
        if (!this.a.isEmpty()) {
            a remove = this.a.remove();
            String b2 = remove.b();
            if (b2 == null) {
                remove.a();
                return;
            }
            List<C0004b> list = this.b.get(b2);
            if (list == null) {
                remove.a();
                return;
            }
            for (C0004b a2 : list) {
                a2.a();
            }
            remove.a();
            for (C0004b b3 : list) {
                b3.b();
            }
        }
    }

    public void c() {
        this.a.clear();
    }

    public String toString() {
        return "MarkedScheduler[" + this.a + "]";
    }
}
