package org.games4all.match;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.games4all.game.controller.server.GameSeed;
import org.games4all.game.rating.ContestResult;
import org.games4all.game.rating.k;

public class Match implements Serializable {
    private static final long serialVersionUID = -8200186670742017414L;
    private Date archived;
    private Date closed;
    private int completeCount;
    private Date created;
    private int dispenseCount;
    private int matchId;
    private List<MatchResult> results;
    private GameSeed seed;
    private long variantId;

    public Match(int i, long j, GameSeed gameSeed) {
        this.matchId = i;
        this.variantId = j;
        this.seed = gameSeed;
        this.created = new Date();
    }

    public Match() {
    }

    public int a() {
        return this.matchId;
    }

    public GameSeed b() {
        return this.seed;
    }

    public List<k> c() {
        ArrayList arrayList = new ArrayList();
        for (MatchResult next : this.results) {
            arrayList.add(new k(next.a(), next.b(), next.c()));
        }
        return arrayList;
    }

    public List<k> a(int i) {
        ArrayList arrayList = new ArrayList();
        for (MatchResult next : this.results) {
            ContestResult a = next.a(i);
            if (a != null) {
                arrayList.add(new k(next.a(), next.b(), a));
            }
        }
        return arrayList;
    }

    public List<ContestResult> b(int i) {
        ArrayList arrayList = new ArrayList();
        if (this.results != null) {
            for (MatchResult next : this.results) {
                if (next.d() > i) {
                    arrayList.add(next.a(i));
                } else {
                    arrayList.add(null);
                }
            }
        }
        return arrayList;
    }

    public List<ContestResult> d() {
        ArrayList arrayList = new ArrayList();
        if (this.results != null) {
            for (MatchResult c : this.results) {
                arrayList.add(c.c());
            }
        }
        return arrayList;
    }

    public boolean e() {
        for (MatchResult e : this.results) {
            if (!e.e()) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Match: id=").append(this.matchId);
        sb.append(",variant=").append(this.variantId);
        sb.append("\n  seed=").append(this.seed);
        sb.append("\n  ");
        sb.append("created=").append(this.created);
        sb.append(",closed=").append(this.closed);
        sb.append(",archived=").append(this.archived);
        sb.append("\n  dispensed=").append(this.dispenseCount);
        sb.append(",completed=").append(this.completeCount);
        sb.append("\n  Played before by:\n");
        if (this.results == null) {
            sb.append("-");
        } else {
            for (MatchResult matchResult : this.results) {
                sb.append(matchResult.toString()).append("\n");
            }
        }
        return sb.toString();
    }
}
