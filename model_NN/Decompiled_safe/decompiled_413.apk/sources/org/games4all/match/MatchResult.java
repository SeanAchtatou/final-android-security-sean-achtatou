package org.games4all.match;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.games4all.game.rating.ContestResult;
import org.games4all.json.h;

public class MatchResult implements Serializable {
    private static final long serialVersionUID = -1603165470354872265L;
    private Date completedAt;
    private List<String> gameMoves;
    private List<ContestResult> gameResults;
    private int matchId;
    private ContestResult matchResult;
    private int userId;
    private String userName;

    public MatchResult(int i, int i2) {
        this.matchId = i;
        this.userId = i2;
        this.gameResults = new ArrayList();
        this.gameMoves = new ArrayList();
    }

    public MatchResult() {
    }

    public int a() {
        return this.userId;
    }

    public String b() {
        return this.userName;
    }

    public ContestResult c() {
        return this.matchResult;
    }

    public void a(ContestResult contestResult) {
        this.matchResult = contestResult;
    }

    public int d() {
        return this.gameResults.size();
    }

    public ContestResult a(int i) {
        if (i < this.gameResults.size()) {
            return this.gameResults.get(i);
        }
        return null;
    }

    public void a(ContestResult contestResult, String str) {
        this.gameResults.add(contestResult);
        this.gameMoves.add(str);
    }

    public boolean e() {
        if (this.matchResult == null) {
            System.err.println("missing match result");
            return false;
        }
        for (ContestResult next : this.gameResults) {
            if (next == null) {
                System.err.println("missing game result");
                return false;
            } else if (!next.b()) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        try {
            return new h().a(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
