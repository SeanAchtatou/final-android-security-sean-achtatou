package org.games4all.util;

import java.io.PrintStream;

public class d {
    public static void a(Throwable th) {
        a(th, System.err);
    }

    public static void a(Throwable th, PrintStream printStream) {
        printStream.println(th.getClass() + ": " + th.getMessage());
        for (StackTraceElement stackTraceElement : th.getStackTrace()) {
            printStream.print("        at ");
            printStream.print(stackTraceElement.getClassName());
            printStream.print(".");
            printStream.print(stackTraceElement.getMethodName());
            printStream.print("(");
            printStream.print(stackTraceElement.getFileName());
            printStream.print(":");
            printStream.print(stackTraceElement.getLineNumber());
            printStream.print(")");
            printStream.println();
        }
        Throwable cause = th.getCause();
        if (cause != null) {
            printStream.print("Caused by: ");
            a(cause, printStream);
        }
    }
}
