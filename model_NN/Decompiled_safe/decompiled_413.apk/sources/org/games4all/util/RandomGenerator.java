package org.games4all.util;

import java.io.Serializable;

public class RandomGenerator implements Serializable {
    private static final long serialVersionUID = 5473054305051223644L;
    private long initialSeed;
    private long lastSync;
    private long seed;

    public RandomGenerator() {
        this(System.currentTimeMillis());
    }

    public RandomGenerator(long j) {
        a(j);
    }

    public long a() {
        return this.initialSeed;
    }

    public void a(long j) {
        this.initialSeed = j;
        this.lastSync = j;
        this.seed = j;
    }

    public synchronized void b() {
        this.lastSync *= 13;
        this.seed = this.lastSync;
    }

    /* access modifiers changed from: protected */
    public synchronized int a(int i) {
        this.seed = ((this.seed * 25214903917L) + 11) & 281474976710655L;
        return (int) (this.seed >>> (48 - i));
    }

    public int b(int i) {
        int a;
        int i2;
        if (i <= 0) {
            throw new IllegalArgumentException("n must be positive");
        } else if (((-i) & i) == i) {
            return (int) ((((long) i) * ((long) a(31))) >> 31);
        } else {
            do {
                a = a(31);
                i2 = a % i;
            } while ((a - i2) + (i - 1) < 0);
            return i2;
        }
    }
}
