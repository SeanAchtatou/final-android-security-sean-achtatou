package org.games4all.util;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

public class f {
    public static byte[] a(byte[] bArr, String str) {
        return a(bArr, str, true);
    }

    public static byte[] b(byte[] bArr, String str) {
        return a(bArr, str, false);
    }

    private static byte[] a(byte[] bArr, String str, boolean z) {
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        SecureRandom instance2 = SecureRandom.getInstance("SHA1PRNG");
        instance2.setSeed(str.getBytes("UTF-8"));
        instance.init(128, instance2);
        SecretKeySpec secretKeySpec = new SecretKeySpec(instance.generateKey().getEncoded(), "AES");
        Cipher instance3 = Cipher.getInstance("AES");
        instance3.init(z ? 1 : 2, secretKeySpec);
        return instance3.doFinal(bArr);
    }
}
