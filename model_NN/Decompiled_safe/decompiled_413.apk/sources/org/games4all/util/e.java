package org.games4all.util;

import java.util.EnumSet;

public class e {
    public static <E extends Enum<E>> E a(Class<E> cls, String str) {
        for (E e : (Enum[]) cls.getEnumConstants()) {
            if (e.name().equals(str)) {
                return e;
            }
        }
        throw new RuntimeException(str + " is not an enum constant in " + cls);
    }

    public static <E extends Enum<E>> String a(Class cls, EnumSet enumSet, String str) {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
            if (enumSet.contains(enumR)) {
                if (z) {
                    z = false;
                } else {
                    sb.append(str);
                }
                sb.append(enumR.name());
            }
        }
        return sb.toString();
    }

    public static <E extends Enum<E>> EnumSet<E> a(Class cls, String str, String str2) {
        EnumSet<E> noneOf = EnumSet.noneOf(cls);
        for (String trim : str.split(str2)) {
            String trim2 = trim.trim();
            if (!trim2.equals("")) {
                noneOf.add(a(cls, trim2));
            }
        }
        return noneOf;
    }
}
