package org.games4all.util;

import java.io.Serializable;
import java.text.ParseException;

public class SoftwareVersion implements Comparable<SoftwareVersion>, Serializable {
    private static final long serialVersionUID = -141841319967098492L;
    private String label;
    private int major;
    private int minor;
    private int patch;
    private int update;

    public SoftwareVersion(String str) {
        String str2;
        try {
            int indexOf = str.indexOf(45) + 1;
            if (indexOf > 0) {
                this.label = str.substring(indexOf);
                str2 = str.substring(0, indexOf - 1);
            } else {
                this.label = null;
                str2 = str;
            }
            try {
                int indexOf2 = str2.indexOf(95) + 1;
                if (indexOf2 > 0) {
                    this.update = Integer.parseInt(str2.substring(indexOf2));
                    str2 = str2.substring(0, indexOf2 - 1);
                } else {
                    this.update = -1;
                }
                int indexOf3 = str2.indexOf(".") + 1;
                if (indexOf3 > 0) {
                    this.major = Integer.parseInt(str2.substring(0, indexOf3 - 1));
                    int indexOf4 = str2.indexOf(".", indexOf3) + 1;
                    if (indexOf4 > 0) {
                        this.minor = Integer.parseInt(str2.substring(indexOf3, indexOf4 - 1));
                        this.patch = Integer.parseInt(str2.substring(indexOf4));
                        return;
                    }
                    this.minor = Integer.parseInt(str2.substring(indexOf3));
                    this.patch = -1;
                    return;
                }
                this.major = Integer.parseInt(str2);
                this.minor = -1;
                this.patch = -1;
            } catch (NumberFormatException e) {
            }
        } catch (NumberFormatException e2) {
            str2 = str;
            throw new ParseException("illegal version string: " + str2, 0);
        }
    }

    /* renamed from: a */
    public int compareTo(SoftwareVersion softwareVersion) {
        if (softwareVersion.major > this.major) {
            return -1;
        }
        if (softwareVersion.major < this.major) {
            return 1;
        }
        if (softwareVersion.minor > this.minor) {
            return -1;
        }
        if (softwareVersion.minor < this.minor) {
            return 1;
        }
        if (softwareVersion.patch > this.patch) {
            return -1;
        }
        if (softwareVersion.patch < this.patch) {
            return 1;
        }
        if (softwareVersion.update > this.update) {
            return -1;
        }
        if (softwareVersion.update < this.update) {
            return 1;
        }
        return 0;
    }

    public int b(SoftwareVersion softwareVersion) {
        if (softwareVersion.major > this.major) {
            return -1;
        }
        if (softwareVersion.major < this.major) {
            return 1;
        }
        if (softwareVersion.minor > this.minor) {
            return -1;
        }
        if (softwareVersion.minor < this.minor) {
            return 1;
        }
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        if (this.minor >= 0) {
            sb.append('.').append(this.minor);
            if (this.patch >= 0) {
                sb.append('.').append(this.patch);
            }
        }
        if (this.update >= 0) {
            sb.append('_');
            if (this.update < 10) {
                sb.append('0');
            }
            sb.append(this.update);
        }
        if (this.label != null) {
            sb.append('-');
            sb.append(this.label);
        }
        return sb.toString();
    }
}
