package org.games4all.util.b.a;

import org.games4all.game.test.i;

public class a<T> {
    private final Object a = new Object();
    private T b;

    public T a(org.games4all.util.a.a<i> aVar) {
        T t;
        synchronized (this.a) {
            while (true) {
                if (this.b == null || !aVar.a(this.b)) {
                    try {
                        this.a.wait();
                    } catch (InterruptedException e) {
                    }
                } else {
                    t = this.b;
                    this.b = null;
                }
            }
        }
        return t;
    }

    public void a(i iVar) {
        synchronized (this.a) {
            this.b = iVar;
            this.a.notifyAll();
        }
    }
}
