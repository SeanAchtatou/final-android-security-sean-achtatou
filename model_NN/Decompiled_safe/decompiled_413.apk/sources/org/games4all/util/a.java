package org.games4all.util;

public class a {
    private final String a;
    private final String b;
    private j c;

    public a(String str, String str2, j jVar) {
        this(str, str2);
        this.c = jVar;
    }

    public a(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public void a(j jVar) {
        this.c = jVar;
    }

    public j a() {
        return this.c;
    }

    public String a(String str) {
        return a(str, a(), this.a, this.b);
    }

    public static String a(String str, j jVar, String str2, String str3) {
        boolean z;
        int indexOf;
        String str4;
        String str5;
        int i = 0;
        boolean z2 = true;
        String str6 = str;
        while (z2) {
            int i2 = i + 1;
            if (i >= 256) {
                break;
            }
            int lastIndexOf = str6.lastIndexOf(str2);
            if (lastIndexOf == -1 || (indexOf = str6.indexOf(str3, lastIndexOf)) == -1) {
                z = false;
            } else {
                String substring = str6.substring(0, lastIndexOf);
                String substring2 = str6.substring(str3.length() + indexOf);
                String a2 = a(str6.substring(lastIndexOf + str2.length(), indexOf), jVar, str2, str3);
                int indexOf2 = a2.indexOf(61);
                if (indexOf2 >= 0) {
                    String substring3 = a2.substring(indexOf2 + 1);
                    str4 = a2.substring(0, indexOf2);
                    str5 = substring3;
                } else {
                    str4 = a2;
                    str5 = null;
                }
                String a3 = jVar.a(str4);
                if (a3 != null) {
                    str5 = a3;
                } else if (str5 == null) {
                    throw new RuntimeException("undefined variable: " + str4);
                }
                str6 = substring + str5 + substring2;
                z = true;
            }
            int i3 = i2;
            z2 = z;
            i = i3;
        }
        return str6;
    }
}
