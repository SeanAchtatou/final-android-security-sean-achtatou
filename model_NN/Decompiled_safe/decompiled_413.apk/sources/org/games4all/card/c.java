package org.games4all.card;

import java.util.Comparator;

public abstract class c implements Comparator<Card> {
    public abstract int a(Face face, Face face2);

    public abstract int a(Suit suit, Suit suit2);

    /* renamed from: a */
    public int compare(Card card, Card card2) {
        if (card.g()) {
            if (card2.g()) {
                return 0;
            }
            return a(card2);
        } else if (card2.g()) {
            return -a(card);
        } else {
            int a = a(card.e(), card2.e());
            return (a < 0 || a > 0) ? a : a(card.d(), card2.d());
        }
    }

    public int a(Card card) {
        return 1;
    }
}
