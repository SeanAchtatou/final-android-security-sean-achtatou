package org.games4all.card;

import java.io.Serializable;
import org.games4all.d.d;

public final class Card implements Serializable {
    public static final Card A = new Card(Face.ACE, Suit.HEARTS);
    public static final Card B = new Card(Face.TWO, Suit.CLUBS);
    public static final Card C = new Card(Face.THREE, Suit.CLUBS);
    public static final Card D = new Card(Face.FOUR, Suit.CLUBS);
    public static final Card E = new Card(Face.FIVE, Suit.CLUBS);
    public static final Card F = new Card(Face.SIX, Suit.CLUBS);
    public static final Card G = new Card(Face.SEVEN, Suit.CLUBS);
    public static final Card H = new Card(Face.EIGHT, Suit.CLUBS);
    public static final Card I = new Card(Face.NINE, Suit.CLUBS);
    public static final Card J = new Card(Face.TEN, Suit.CLUBS);
    public static final Card K = new Card(Face.JACK, Suit.CLUBS);
    public static final Card L = new Card(Face.QUEEN, Suit.CLUBS);
    public static final Card M = new Card(Face.KING, Suit.CLUBS);
    public static final Card N = new Card(Face.ACE, Suit.CLUBS);
    public static final Card O = new Card(Face.TWO, Suit.DIAMONDS);
    public static final Card P = new Card(Face.THREE, Suit.DIAMONDS);
    public static final Card Q = new Card(Face.FOUR, Suit.DIAMONDS);
    public static final Card R = new Card(Face.FIVE, Suit.DIAMONDS);
    public static final Card S = new Card(Face.SIX, Suit.DIAMONDS);
    public static final Card T = new Card(Face.SEVEN, Suit.DIAMONDS);
    public static final Card U = new Card(Face.EIGHT, Suit.DIAMONDS);
    public static final Card V = new Card(Face.NINE, Suit.DIAMONDS);
    public static final Card W = new Card(Face.TEN, Suit.DIAMONDS);
    public static final Card X = new Card(Face.JACK, Suit.DIAMONDS);
    public static final Card Y = new Card(Face.QUEEN, Suit.DIAMONDS);
    public static final Card Z = new Card(Face.KING, Suit.DIAMONDS);
    public static final Card a = b();
    public static final Card aa = new Card(Face.ACE, Suit.DIAMONDS);
    public static final Card b = new Card(Face.TWO, Suit.SPADES);
    public static final Card c = new Card(Face.THREE, Suit.SPADES);
    public static final Card d = new Card(Face.FOUR, Suit.SPADES);
    public static final Card e = new Card(Face.FIVE, Suit.SPADES);
    public static final Card f = new Card(Face.SIX, Suit.SPADES);
    public static final Card g = new Card(Face.SEVEN, Suit.SPADES);
    public static final Card h = new Card(Face.EIGHT, Suit.SPADES);
    public static final Card i = new Card(Face.NINE, Suit.SPADES);
    public static final Card j = new Card(Face.TEN, Suit.SPADES);
    public static final Card k = new Card(Face.JACK, Suit.SPADES);
    public static final Card l = new Card(Face.QUEEN, Suit.SPADES);
    public static final Card m = new Card(Face.KING, Suit.SPADES);
    public static final Card n = new Card(Face.ACE, Suit.SPADES);
    public static final Card o = new Card(Face.TWO, Suit.HEARTS);
    public static final Card p = new Card(Face.THREE, Suit.HEARTS);
    public static final Card q = new Card(Face.FOUR, Suit.HEARTS);
    public static final Card r = new Card(Face.FIVE, Suit.HEARTS);
    public static final Card s = new Card(Face.SIX, Suit.HEARTS);
    private static final long serialVersionUID = -8289133214158448961L;
    public static final Card t = new Card(Face.SEVEN, Suit.HEARTS);
    public static final Card u = new Card(Face.EIGHT, Suit.HEARTS);
    public static final Card v = new Card(Face.NINE, Suit.HEARTS);
    public static final Card w = new Card(Face.TEN, Suit.HEARTS);
    public static final Card x = new Card(Face.JACK, Suit.HEARTS);
    public static final Card y = new Card(Face.QUEEN, Suit.HEARTS);
    public static final Card z = new Card(Face.KING, Suit.HEARTS);
    private Face face;
    private int special;
    private Suit suit;

    public interface Trans extends d {
        String faceName(Face face);

        String faceShort(Face face);

        String specialName(Card card);

        String suitName(Suit suit);

        String suitShort(Suit suit);
    }

    public static Card a() {
        return new Card(2);
    }

    public static Card b() {
        return new Card(1);
    }

    public static Card c() {
        return new Card(3);
    }

    public static Card a(String str) {
        if (str.length() < 1 || str.length() > 2) {
            throw new RuntimeException("illegal card spec: " + str);
        } else if (str.charAt(0) == '?') {
            return b();
        } else {
            if (str.charAt(0) == '*') {
                return a();
            }
            if (str.charAt(0) == '-') {
                return c();
            }
            if (str.charAt(0) == '.') {
                return null;
            }
            if (str.length() == 2) {
                return new Card(Face.a(str.substring(0, 1)), Suit.a(str.substring(1)));
            }
            throw new RuntimeException("illegal card spec: " + str);
        }
    }

    public Card(Face face2, Suit suit2) {
        this.face = face2;
        this.suit = suit2;
        this.special = 0;
    }

    private Card(int i2) {
        this.face = null;
        this.suit = null;
        this.special = i2;
    }

    public Card() {
    }

    public Face d() {
        return this.face;
    }

    public Suit e() {
        return this.suit;
    }

    public boolean f() {
        return this.special == 1;
    }

    public boolean g() {
        return this.special == 2;
    }

    public boolean h() {
        return this.special == 3;
    }

    public String toString() {
        switch (this.special) {
            case 0:
                return this.face.toString() + this.suit.toString();
            case 1:
                return "??";
            case 2:
                return "**";
            case 3:
                return "--";
            default:
                throw new RuntimeException(String.valueOf(this.special));
        }
    }

    public int hashCode() {
        int i2 = 1 * 31;
        return (((((this.face == null ? 0 : this.face.hashCode()) + 31) * 31) + this.special) * 31) + (this.suit == null ? 0 : this.suit.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Card card = (Card) obj;
        if (this.face == null) {
            if (card.face != null) {
                return false;
            }
        } else if (!this.face.equals(card.face)) {
            return false;
        }
        if (this.special != card.special) {
            return false;
        }
        if (this.suit == null) {
            if (card.suit != null) {
                return false;
            }
        } else if (!this.suit.equals(card.suit)) {
            return false;
        }
        return true;
    }

    public int i() {
        switch (this.special) {
            case 0:
                return (this.suit.ordinal() * 13) + this.face.ordinal();
            case 1:
            default:
                throw new RuntimeException("no ordinal for special: " + this.special);
            case 2:
                return 52;
        }
    }
}
