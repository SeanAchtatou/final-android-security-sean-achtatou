package org.games4all.card;

import java.util.Arrays;
import java.util.Comparator;

public class d {
    private Comparator<Card> a;
    private Integer[] b;
    private int[] c;

    public void a(Cards cards) {
        int size = cards.size();
        if (this.b == null || size != this.b.length) {
            this.b = new Integer[size];
            this.c = new int[size];
            for (int i = 0; i < size; i++) {
                this.b[i] = Integer.valueOf(i);
                this.c[i] = i;
            }
        }
        if (this.a != null) {
            Arrays.sort(this.b, new a(this.a, cards));
            for (int i2 = 0; i2 < size; i2++) {
                this.c[this.b[i2].intValue()] = i2;
            }
        }
    }

    class a implements Comparator<Integer> {
        final /* synthetic */ Comparator a;
        final /* synthetic */ Cards b;

        a(Comparator comparator, Cards cards) {
            this.a = comparator;
            this.b = cards;
        }

        /* renamed from: a */
        public int compare(Integer num, Integer num2) {
            return this.a.compare(this.b.a(num.intValue()), this.b.a(num2.intValue()));
        }
    }

    public int a(int i) {
        if (i < 0 || i >= this.c.length) {
            return -1;
        }
        return this.c[i];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CardMapping[");
        for (int i = 0; i < this.b.length; i++) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append("" + i + "->" + this.b[i]);
        }
        sb.append(']');
        return sb.toString();
    }
}
