package org.games4all.card;

public enum Face {
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),
    TEN("T"),
    JACK("J"),
    QUEEN("Q"),
    KING("K"),
    ACE("A");
    
    private final String abbr;

    private Face(String str) {
        this.abbr = str;
    }

    public String toString() {
        return this.abbr;
    }

    public Face a() {
        if (this == ACE) {
            return null;
        }
        return values()[ordinal() + 1];
    }

    public static Face a(String str) {
        for (Face face : values()) {
            if (face.abbr.equals(str)) {
                return face;
            }
        }
        throw new RuntimeException("illegal face spec: " + str);
    }
}
