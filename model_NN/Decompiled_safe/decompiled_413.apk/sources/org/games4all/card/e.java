package org.games4all.card;

public class e {
    private final b a;
    private final int b;

    public e(b bVar, int i) {
        this.a = bVar;
        this.b = i;
    }

    public b a() {
        return this.a;
    }

    public int b() {
        return this.b;
    }

    public String toString() {
        return "card@" + this.a + "/" + this.b;
    }
}
