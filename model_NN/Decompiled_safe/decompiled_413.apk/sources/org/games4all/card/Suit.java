package org.games4all.card;

public enum Suit {
    SPADES("S"),
    HEARTS("H"),
    CLUBS("C"),
    DIAMONDS("D");
    
    private final String abbr;

    private Suit(String str) {
        this.abbr = str;
    }

    public String toString() {
        return this.abbr;
    }

    public static Suit a(String str) {
        for (Suit suit : values()) {
            if (suit.abbr.equals(str)) {
                return suit;
            }
        }
        throw new RuntimeException("illegal suit spec: " + str);
    }
}
