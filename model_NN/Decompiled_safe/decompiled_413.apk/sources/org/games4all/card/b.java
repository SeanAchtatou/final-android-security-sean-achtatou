package org.games4all.card;

public class b {
    private final String a;
    private final int b;

    public b(String str, int i) {
        this.a = str;
        this.b = i;
    }

    public b(String str) {
        this.a = str;
        this.b = 0;
    }

    public int hashCode() {
        int i = 1 * 31;
        return ((this.b + 31) * 31) + (this.a == null ? 0 : this.a.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        if (this.b != bVar.b) {
            return false;
        }
        if (this.a == null) {
            if (bVar.a != null) {
                return false;
            }
        } else if (!this.a.equals(bVar.a)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return this.a + "-" + this.b;
    }
}
