package org.games4all.card;

public class a extends c {
    private boolean a;

    public a() {
    }

    public a(boolean z) {
        this.a = z;
    }

    public int a(Suit suit, Suit suit2) {
        return Integer.signum(suit.ordinal() - suit2.ordinal());
    }

    public int a(Face face, Face face2) {
        int signum = Integer.signum(face2.ordinal() - face.ordinal());
        if (this.a) {
            return -signum;
        }
        return signum;
    }
}
