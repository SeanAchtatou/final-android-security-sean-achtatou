package org.games4all.card;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;
import org.games4all.util.RandomGenerator;

public class Cards extends ArrayList<Card> {
    public static final Cards a = new Cards();
    private static final long serialVersionUID = 8658397037201736268L;

    public static Cards a() {
        Cards cards = new Cards();
        for (Suit suit : Suit.values()) {
            for (Face card : Face.values()) {
                cards.add(new Card(card, suit));
            }
        }
        return cards;
    }

    public static String a(Collection<Card> collection) {
        StringBuilder sb = new StringBuilder();
        int size = collection.size();
        for (Card next : collection) {
            if (next == null) {
                sb.append("..");
            } else {
                sb.append(next);
            }
            if (size > 1) {
                sb.append(' ');
                size--;
            }
        }
        return sb.toString();
    }

    public Cards() {
    }

    public Cards(int i) {
        for (int i2 = 0; i2 < i; i2++) {
            add(null);
        }
    }

    public Cards(Card... cardArr) {
        a(cardArr);
    }

    public Cards(List<Card> list, int i) {
        super(list.size() - i);
        int size = list.size();
        for (int i2 = i; i2 < size; i2++) {
            add(list.get(i2));
        }
    }

    public Cards(List<Card> list) {
        super(list.size());
        addAll(list);
    }

    public Cards(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str);
        while (stringTokenizer.hasMoreTokens()) {
            add(Card.a(stringTokenizer.nextToken()));
        }
    }

    public Card a(int i) {
        return (Card) get(i);
    }

    public void a(Card card) {
        add(card);
    }

    public Card b(int i) {
        return (Card) remove(i);
    }

    public void a(Card[] cardArr) {
        clear();
        addAll(Arrays.asList(cardArr));
    }

    public void a(Cards cards) {
        clear();
        addAll(cards);
    }

    public Cards a(List<Integer> list) {
        Cards cards = new Cards();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return cards;
            }
            cards.add(a(list.get(i2).intValue()));
            i = i2 + 1;
        }
    }

    public String toString() {
        return a((Collection<Card>) this);
    }

    public void a(RandomGenerator randomGenerator) {
        int size = size();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < size - 1) {
                int b = i2 + randomGenerator.b(size - i2);
                set(b, get(i2));
                set(i2, (Card) get(b));
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public Card b() {
        return b(size() - 1);
    }

    public Card c() {
        return a(size() - 1);
    }
}
