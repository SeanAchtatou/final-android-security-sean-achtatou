package org.games4all.games.card.indianrummy.rating;

import org.games4all.game.rating.BasicContestResult;
import org.games4all.game.rating.ContestResult;
import org.games4all.game.rating.Outcome;
import org.games4all.games.card.indianrummy.IndianRummyModel;

public class IndianRummyMatchResult extends BasicContestResult {
    private static final long serialVersionUID = 8216576772147808595L;
    private int[] points;

    public IndianRummyMatchResult(IndianRummyModel indianRummyModel) {
        super(4);
        this.points = new int[4];
        for (int i = 0; i < 4; i++) {
            this.points[i] = indianRummyModel.j(i);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            a(i2, (((long) (d(i2) * 100)) * 1000000) / 6);
        }
    }

    public IndianRummyMatchResult() {
    }

    public Outcome a(int i, int i2) {
        return super.a(i, i2).a();
    }

    public Outcome a(ContestResult contestResult, int i) {
        long a = a(i);
        long a2 = ((BasicContestResult) contestResult).a(i);
        if (a < a2) {
            return Outcome.WIN;
        }
        if (a > a2) {
            return Outcome.LOSS;
        }
        return Outcome.TIE;
    }

    public int c(int i) {
        return this.points[i];
    }

    private int d(int i) {
        int i2 = 0;
        for (int i3 = 0; i3 < 4; i3++) {
            if (i3 != i) {
                switch (Integer.signum(this.points[i] - this.points[i3])) {
                    case 0:
                        i2++;
                        continue;
                    case 1:
                        i2 += 2;
                        continue;
                }
            }
        }
        return i2;
    }
}
