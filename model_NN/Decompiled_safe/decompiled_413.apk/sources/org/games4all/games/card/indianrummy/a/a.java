package org.games4all.games.card.indianrummy.a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.card.Face;
import org.games4all.card.Suit;
import org.games4all.game.c.c;
import org.games4all.game.move.Move;
import org.games4all.games.card.indianrummy.IndianRummyModel;
import org.games4all.games.card.indianrummy.IndianRummyOptions;
import org.games4all.games.card.indianrummy.IndianRummyPrivateModel;
import org.games4all.games.card.indianrummy.IndianRummyVariant;
import org.games4all.games.card.indianrummy.move.Discard;
import org.games4all.games.card.indianrummy.move.Done;
import org.games4all.games.card.indianrummy.move.OrderCards;

public class a implements c {
    static final /* synthetic */ boolean a = (!a.class.desiredAssertionStatus());
    private static final Card b = Card.a();
    private final IndianRummyModel c;
    private final org.games4all.games.card.indianrummy.b d;
    private final IndianRummyVariant e;
    private final int f;
    private int g;
    private int h;
    private int i;
    private long j;

    public class b {
        public int a;
        public Cards b;
        public int c;
        public List<Cards> d;
        public List<Cards> e;
        public Cards f;
        public boolean g;
        public boolean h;

        public b() {
        }

        public b(b bVar) {
            this.b = new Cards(bVar.b);
            this.c = bVar.c;
            this.d = new ArrayList();
            this.d.addAll(bVar.d);
            this.e = new ArrayList();
            this.e.addAll(bVar.e);
            this.f = new Cards(bVar.f);
            this.g = bVar.g;
            this.h = bVar.h;
            this.a = bVar.a;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{left=").append(this.b);
            sb.append(",j=").append(this.c);
            sb.append(",dead=").append(this.f);
            sb.append(",runs=").append(this.d);
            sb.append(",sets=").append(this.e);
            sb.append(",v=").append(this.a);
            sb.append("}");
            return sb.toString();
        }
    }

    public a(IndianRummyModel indianRummyModel, int i2) {
        this.c = indianRummyModel;
        this.f = i2;
        this.d = new org.games4all.games.card.indianrummy.b(indianRummyModel);
        this.e = (IndianRummyVariant) ((IndianRummyOptions) indianRummyModel.p()).d();
    }

    public void d() {
    }

    public Move a() {
        long currentTimeMillis = System.currentTimeMillis();
        Move c2 = c();
        this.j = Math.max(this.j, System.currentTimeMillis() - currentTimeMillis);
        this.i = Math.max(this.i, this.h);
        return c2;
    }

    private Move c() {
        if (this.c.g()) {
            return new Done();
        }
        if (this.c.h()) {
            return b();
        }
        if (this.c.r().e() == 1) {
            return e();
        }
        return f();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0062  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.games4all.game.move.Move b() {
        /*
            r10 = this;
            r9 = 100
            r8 = 13
            r7 = 0
            r6 = 1
            org.games4all.card.Cards r0 = new org.games4all.card.Cards
            org.games4all.games.card.indianrummy.IndianRummyModel r1 = r10.c
            int r2 = r10.f
            org.games4all.card.Cards r1 = r1.e(r2)
            r0.<init>(r1)
            org.games4all.card.Card r1 = org.games4all.card.Card.a()
            r0.add(r1)
            org.games4all.card.Cards r1 = r10.a(r0)
            org.games4all.games.card.indianrummy.b r2 = r10.d
            int r1 = r2.a(r1, r6)
            org.games4all.games.card.indianrummy.b r2 = r10.d
            org.games4all.games.card.indianrummy.human.IndianRummyViewer$HandState r2 = r2.a()
            org.games4all.games.card.indianrummy.b r3 = r10.d
            int r3 = r3.b()
            org.games4all.games.card.indianrummy.IndianRummyModel r4 = r10.c
            org.games4all.card.Cards r4 = r4.c()
            org.games4all.card.Card r4 = r4.c()
            boolean r5 = r4.g()
            if (r5 == 0) goto L_0x0046
            org.games4all.games.card.indianrummy.move.Take r0 = new org.games4all.games.card.indianrummy.move.Take
            r0.<init>(r6, r8)
        L_0x0045:
            return r0
        L_0x0046:
            r0.add(r4)
            org.games4all.card.Cards r0 = r10.a(r0)
            org.games4all.games.card.indianrummy.b r4 = r10.d
            int r0 = r4.a(r0, r6)
            org.games4all.games.card.indianrummy.b r4 = r10.d
            int r4 = r4.b()
            if (r0 < r1) goto L_0x009d
            org.games4all.games.card.indianrummy.human.IndianRummyViewer$HandState r0 = org.games4all.games.card.indianrummy.human.IndianRummyViewer.HandState.SECOND_LIFE
            if (r2 != r0) goto L_0x0085
            r1 = r6
        L_0x0060:
            if (r1 == 0) goto L_0x007d
            org.games4all.games.card.indianrummy.IndianRummyModel r0 = r10.c
            int r2 = r10.f
            org.games4all.game.model.f r0 = r0.k(r2)
            org.games4all.games.card.indianrummy.IndianRummyPrivateModel r0 = (org.games4all.games.card.indianrummy.IndianRummyPrivateModel) r0
            org.games4all.util.RandomGenerator r0 = r0.e()
            int[] r2 = org.games4all.games.card.indianrummy.a.a.C0024a.a
            org.games4all.games.card.indianrummy.IndianRummyVariant r3 = r10.e
            int r3 = r3.ordinal()
            r2 = r2[r3]
            switch(r2) {
                case 1: goto L_0x0089;
                case 2: goto L_0x0093;
                default: goto L_0x007d;
            }
        L_0x007d:
            r0 = r1
        L_0x007e:
            org.games4all.games.card.indianrummy.move.Take r1 = new org.games4all.games.card.indianrummy.move.Take
            r1.<init>(r0, r8)
            r0 = r1
            goto L_0x0045
        L_0x0085:
            if (r4 <= r3) goto L_0x009d
            r1 = r6
            goto L_0x0060
        L_0x0089:
            int r0 = r0.b(r9)
            r2 = 20
            if (r0 <= r2) goto L_0x007d
            r0 = r7
            goto L_0x007e
        L_0x0093:
            int r0 = r0.b(r9)
            r2 = 75
            if (r0 <= r2) goto L_0x007d
            r0 = r7
            goto L_0x007e
        L_0x009d:
            r1 = r7
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.games.card.indianrummy.a.a.b():org.games4all.game.move.Move");
    }

    private Move e() {
        Cards cards;
        int i2;
        Cards cards2 = new Cards(this.c.e(this.f));
        int size = cards2.size();
        int i3 = Integer.MIN_VALUE;
        int i4 = 0;
        Cards cards3 = new Cards();
        while (i4 < size) {
            Cards cards4 = new Cards(cards2);
            Card card = (Card) cards4.remove(i4);
            Cards a2 = a(cards4);
            int a3 = this.d.a(a2, true);
            if (a3 > i3) {
                a2.add(card);
                cards = a2;
                i2 = a3;
            } else {
                cards = cards3;
                i2 = i3;
            }
            i4++;
            i3 = i2;
            cards3 = cards;
        }
        System.err.println("BEST ORDERING = " + i3 + " : " + cards3);
        return new OrderCards(cards3);
    }

    private Move f() {
        int i2;
        int i3;
        int i4;
        int i5;
        Cards e2 = this.c.e(this.f);
        int size = e2.size();
        int[] iArr = new int[size];
        int[] iArr2 = new int[size];
        int a2 = a(e2, iArr, iArr2);
        int i6 = iArr[a2];
        int i7 = iArr2[a2];
        ArrayList arrayList = new ArrayList();
        for (int i8 = 0; i8 < size; i8++) {
            if (i7 == iArr2[i8]) {
                arrayList.add(Integer.valueOf(i8));
            }
        }
        int size2 = arrayList.size();
        if (this.e == IndianRummyVariant.HARD) {
            int[] iArr3 = new int[53];
            a(e2.a((List<Integer>) arrayList), iArr3);
            int i9 = 0;
            int i10 = Integer.MIN_VALUE;
            int i11 = -1;
            while (i9 < size2) {
                int intValue = ((Integer) arrayList.get(i9)).intValue();
                Card card = (Card) e2.get(intValue);
                if (iArr3[card.i()] <= 1 || (i4 = -org.games4all.games.card.indianrummy.b.a(card)) <= i10) {
                    i4 = i10;
                    i5 = i11;
                } else {
                    i5 = intValue;
                }
                i9++;
                i11 = i5;
                i10 = i4;
            }
            if (i11 > 0) {
                return new Discard(i11, (Card) e2.get(i11));
            }
        }
        int i12 = 0;
        int i13 = Integer.MIN_VALUE;
        int i14 = 0;
        while (i12 < size2) {
            int intValue2 = ((Integer) arrayList.get(i12)).intValue();
            Cards cards = new Cards(e2);
            cards.set(intValue2, b);
            int a3 = this.d.a(a(cards), true);
            if (a3 > i13) {
                if (!(i13 == Integer.MIN_VALUE || this.e == IndianRummyVariant.HARD)) {
                    int b2 = ((IndianRummyPrivateModel) this.c.k(this.f)).e().b(100);
                    if (this.e == IndianRummyVariant.EASY && b2 > 50) {
                        i2 = i13;
                        i3 = i14;
                    } else if (this.e == IndianRummyVariant.MEDIUM && b2 > 85) {
                        i2 = i13;
                        i3 = i14;
                    }
                }
                i2 = a3;
                i3 = intValue2;
            } else {
                i2 = i13;
                i3 = i14;
            }
            i12++;
            i14 = i3;
            i13 = i2;
        }
        return new Discard(i14, (Card) e2.get(i14));
    }

    private void a(Cards cards, int[] iArr) {
        Iterator it = cards.iterator();
        while (it.hasNext()) {
            int i2 = ((Card) it.next()).i();
            iArr[i2] = iArr[i2] + 1;
        }
    }

    private int a(Cards cards, int[] iArr, int[] iArr2) {
        int i2;
        Card e2 = this.c.e();
        int size = cards.size();
        this.g = Integer.MIN_VALUE;
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            Cards cards2 = new Cards(cards);
            Card card = (Card) cards2.remove(i4);
            if (card.equals(e2) || card.g()) {
                i2 = -536870912;
                iArr2[i4] = size + 1;
            } else {
                i2 = this.d.a(cards2, true);
                iArr2[i4] = this.d.c();
                if (i2 > this.g) {
                    this.g = i2;
                    i3 = i4;
                }
            }
            iArr[i4] = i2;
        }
        return i3;
    }

    /* access modifiers changed from: package-private */
    public Cards a(Cards cards) {
        int i2 = 0;
        this.h = 0;
        Cards cards2 = new Cards(cards);
        Iterator it = cards2.iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                Collections.sort(cards2, new org.games4all.card.a(true));
                b bVar = new b();
                bVar.b = cards2;
                bVar.c = i3;
                bVar.f = new Cards();
                bVar.d = new ArrayList();
                bVar.e = new ArrayList();
                this.g = Integer.MAX_VALUE;
                return c(a(bVar));
            } else if (((Card) it.next()).g()) {
                it.remove();
                i2 = i3 + 1;
            } else {
                i2 = i3;
            }
        }
    }

    private b a(b bVar) {
        if (bVar.b.isEmpty()) {
            return d(bVar);
        }
        if (b(bVar) >= this.g) {
            return null;
        }
        Card card = (Card) bVar.b.remove(0);
        b a2 = a(bVar, card);
        Face d2 = card.d();
        if (!bVar.b.isEmpty() && d2.compareTo((Enum) Face.FIVE) < 0) {
            Card card2 = new Card(Face.ACE, card.e());
            if (bVar.b.remove(card2)) {
                bVar.b.add(0, card);
                for (Cards a3 : a(bVar.b, bVar.c, card2)) {
                    b a4 = a(bVar, a3, card2, true);
                    if (a4 == null || (a2 != null && a4.a >= a2.a)) {
                        a4 = a2;
                    }
                    a2 = a4;
                }
            }
        }
        return a2;
    }

    private int b(b bVar) {
        int i2 = bVar.a;
        if (!bVar.g) {
            i2 += 0;
        }
        if (!bVar.h) {
            return i2 + 0;
        }
        return i2;
    }

    private b a(b bVar, Card card) {
        b bVar2;
        b b2 = b(bVar, card);
        if (!bVar.b.isEmpty()) {
            Iterator<Cards> it = a(bVar.b, bVar.c, card).iterator();
            while (true) {
                bVar2 = b2;
                if (!it.hasNext()) {
                    break;
                }
                b2 = a(bVar, it.next(), card, true);
                if (b2 == null || (bVar2 != null && b2.a >= bVar2.a)) {
                    b2 = bVar2;
                }
            }
            b2 = bVar2;
        }
        if (bVar.e.size() >= 2) {
            return b2;
        }
        Iterator<Cards> it2 = b(bVar.b, bVar.c, card).iterator();
        while (true) {
            b bVar3 = b2;
            if (!it2.hasNext()) {
                return bVar3;
            }
            b a2 = a(bVar, it2.next(), card, false);
            if (a2 == null || (bVar3 != null && a2.a >= bVar3.a)) {
                a2 = bVar3;
            }
        }
    }

    private b b(b bVar, Card card) {
        b bVar2 = new b(bVar);
        bVar2.f.add(card);
        bVar2.a -= org.games4all.games.card.indianrummy.b.a(card);
        return a(bVar2);
    }

    private b a(b bVar, Cards cards, Card card, boolean z) {
        boolean z2;
        b bVar2 = new b(bVar);
        Iterator it = cards.iterator();
        boolean z3 = false;
        while (it.hasNext()) {
            Card card2 = (Card) it.next();
            if (!card2.equals(card)) {
                if (card2.g()) {
                    bVar2.c--;
                    z2 = true;
                    z3 = z2;
                } else {
                    bVar2.b.remove(card2);
                }
            }
            z2 = z3;
            z3 = z2;
        }
        if (z) {
            bVar2.d.add(cards);
            if (!bVar2.g) {
                if (!z3) {
                    bVar2.g = true;
                    bVar2.a -= 0;
                } else if (!bVar2.h) {
                    bVar2.h = true;
                    bVar2.a -= 0;
                }
            } else if (!bVar2.h) {
                bVar2.h = true;
                bVar2.a -= 0;
            }
        } else {
            bVar2.e.add(cards);
        }
        return a(bVar2);
    }

    public static List<Cards> a(Cards cards, int i2, Card card) {
        Face face;
        int i3;
        int size = cards.size();
        ArrayList arrayList = new ArrayList();
        Suit e2 = card.e();
        Face d2 = card.d();
        if (i2 >= 2 && d2.compareTo((Enum) Face.QUEEN) > 0) {
            arrayList.add(new Cards(b, b, card));
        }
        Cards cards2 = new Cards(card);
        Card card2 = (Card) cards.get(0);
        if (card2.e() != e2) {
            return arrayList;
        }
        int i4 = 0;
        Face face2 = d2;
        int i5 = i2;
        while (true) {
            face2 = face2.a();
            if (face2 == null) {
                if (cards2.size() != 1) {
                    break;
                }
                face2 = Face.TWO;
            }
            if (face2 != card2.d()) {
                if (i5 <= 0) {
                    break;
                }
                i5--;
                cards2.add(b);
                if (cards2.size() >= 3) {
                    arrayList.add(new Cards(cards2));
                }
            } else {
                cards2.add(card2);
                if (cards2.size() >= 3) {
                    arrayList.add(new Cards(cards2));
                }
                i4++;
                if (i4 < size) {
                    card2 = (Card) cards.get(i4);
                    if (card2.e() != e2) {
                        i3 = i5;
                        face = face2;
                        break;
                    }
                } else {
                    i3 = i5;
                    face = face2;
                    break;
                }
            }
            if (cards2.size() >= 5) {
                i3 = i5;
                face = face2;
                break;
            }
        }
        i3 = i5;
        face = face2;
        while (face != null && i3 > 0 && cards2.size() < 5) {
            i3--;
            cards2.add(b);
            if (cards2.size() >= 3) {
                arrayList.add(new Cards(cards2));
            }
        }
        return arrayList;
    }

    private List<Cards> b(Cards cards, int i2, Card card) {
        ArrayList arrayList = new ArrayList();
        Face d2 = card.d();
        Suit e2 = card.e();
        EnumSet noneOf = EnumSet.noneOf(Suit.class);
        Iterator it = cards.iterator();
        while (it.hasNext()) {
            Card card2 = (Card) it.next();
            if (card2.d() == d2 && card2.e() != e2) {
                noneOf.add(card2.e());
            }
        }
        Suit[] values = Suit.values();
        for (int i3 = 0; i3 < values.length - 1; i3++) {
            Suit suit = values[i3];
            if (noneOf.contains(suit)) {
                for (int i4 = i3 + 1; i4 < values.length; i4++) {
                    Suit suit2 = values[i4];
                    if (noneOf.contains(suit2)) {
                        arrayList.add(new Cards(card, new Card(d2, suit), new Card(d2, suit2)));
                    }
                }
            }
        }
        if (noneOf.size() == 3) {
            arrayList.add(new Cards(new Card(d2, Suit.SPADES), new Card(d2, Suit.HEARTS), new Card(d2, Suit.CLUBS), new Card(d2, Suit.DIAMONDS)));
        }
        if (i2 > 0) {
            Iterator it2 = noneOf.iterator();
            while (it2.hasNext()) {
                arrayList.add(new Cards(card, new Card(d2, (Suit) it2.next()), b));
            }
        }
        return arrayList;
    }

    private Cards c(b bVar) {
        Cards cards = new Cards();
        while (true) {
            if (bVar.d.isEmpty() && bVar.e.isEmpty() && bVar.f.isEmpty()) {
                break;
            }
            a(bVar.d, cards);
            a(bVar.e, cards);
            a(bVar.f, cards);
        }
        for (int i2 = 0; i2 < bVar.c; i2++) {
            cards.add(b);
        }
        return cards;
    }

    private void a(List<Cards> list, Cards cards) {
        if (!list.isEmpty()) {
            cards.addAll(list.remove(list.size() - 1));
        }
    }

    private void a(Cards cards, Cards cards2) {
        if (!cards.isEmpty()) {
            cards2.add(cards.b());
        }
    }

    private b d(b bVar) {
        b bVar2 = new b(bVar);
        if (a || bVar2.b.isEmpty()) {
            this.h++;
            if (bVar2.a < this.g) {
                this.g = bVar2.a;
            }
            return bVar2;
        }
        throw new AssertionError();
    }
}
