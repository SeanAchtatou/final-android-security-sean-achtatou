package org.games4all.games.card.indianrummy.move;

import org.games4all.game.move.Move;
import org.games4all.game.move.a;
import org.games4all.game.move.c;

public class Done implements Move {
    private static final long serialVersionUID = -1983546878151357937L;

    public c a(int i, a aVar) {
        return ((a) aVar).c(i);
    }

    public String toString() {
        return "Done[]";
    }

    public int hashCode() {
        return -1535899121;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        return getClass() == obj.getClass();
    }
}
