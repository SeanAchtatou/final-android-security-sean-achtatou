package org.games4all.games.card.indianrummy.move;

import org.games4all.card.Cards;
import org.games4all.game.move.Move;
import org.games4all.game.move.a;
import org.games4all.game.move.c;

public class OrderCards implements Move {
    private static final long serialVersionUID = 2470648514859272676L;
    private Cards cards;

    public OrderCards(Cards cards2) {
        this.cards = cards2;
    }

    public OrderCards() {
    }

    public c a(int i, a aVar) {
        return ((a) aVar).b(i, this.cards);
    }

    public String toString() {
        return "OrderCards[cards=" + this.cards + "]";
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.cards == null ? 0 : this.cards.hashCode()) + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderCards orderCards = (OrderCards) obj;
        if (this.cards == null) {
            if (orderCards.cards != null) {
                return false;
            }
        } else if (!this.cards.equals(orderCards.cards)) {
            return false;
        }
        return true;
    }
}
