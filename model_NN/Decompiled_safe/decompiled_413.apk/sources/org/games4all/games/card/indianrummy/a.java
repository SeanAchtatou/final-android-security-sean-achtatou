package org.games4all.games.card.indianrummy;

import java.util.ArrayList;
import java.util.List;
import org.games4all.game.PlayerInfo;
import org.games4all.game.c;
import org.games4all.game.controller.a.d;
import org.games4all.game.g;
import org.games4all.game.option.k;
import org.games4all.game.rating.e;
import org.games4all.game.rating.f;
import org.games4all.game.rating.j;
import org.games4all.game.rating.l;

public class a implements org.games4all.game.d.a {
    public c a() {
        return new c(IndianRummyVariant.class);
    }

    public List<org.games4all.game.rating.b> a(g gVar, boolean z) {
        ArrayList arrayList = new ArrayList();
        long a = gVar.a();
        arrayList.add(new j(a, 1, 1000));
        arrayList.add(new org.games4all.games.card.indianrummy.rating.c(a, 2));
        arrayList.add(new l(a, 3));
        arrayList.add(new e(a, 4));
        arrayList.add(new f(a, 5));
        return arrayList;
    }

    public org.games4all.game.a<?, ?> b() {
        return new c();
    }

    public d c() {
        return new org.games4all.games.card.indianrummy.human.b();
    }

    public org.games4all.game.c.a<?> d() {
        org.games4all.game.c.a<?> aVar = new org.games4all.game.c.a<>();
        aVar.a("@Randy", null, new b(), false);
        aVar.a("@Irma", null, new C0023a(), true);
        return aVar;
    }

    class b implements org.games4all.game.e.c<IndianRummyModel> {
        b() {
        }

        public org.games4all.game.e.a a(IndianRummyModel indianRummyModel, int i, PlayerInfo playerInfo) {
            return new org.games4all.games.card.indianrummy.a.b(indianRummyModel, i);
        }
    }

    /* renamed from: org.games4all.games.card.indianrummy.a$a  reason: collision with other inner class name */
    class C0023a implements org.games4all.game.e.c<IndianRummyModel> {
        C0023a() {
        }

        public org.games4all.game.e.a a(IndianRummyModel indianRummyModel, int i, PlayerInfo playerInfo) {
            return new org.games4all.games.card.indianrummy.a.a(indianRummyModel, i);
        }
    }

    public k e() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(IndianRummyVariant.EASY);
        arrayList.add(IndianRummyVariant.MEDIUM);
        arrayList.add(IndianRummyVariant.HARD);
        return new org.games4all.game.option.a(IndianRummyOptions.class, arrayList, IndianRummyVariant.MEDIUM);
    }
}
