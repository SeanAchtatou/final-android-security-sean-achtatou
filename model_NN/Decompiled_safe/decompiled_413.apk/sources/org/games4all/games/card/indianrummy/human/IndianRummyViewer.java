package org.games4all.games.card.indianrummy.human;

import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.game.PlayerInfo;

public interface IndianRummyViewer extends org.games4all.game.e.a, org.games4all.games.card.indianrummy.move.a {

    public enum HandState {
        UNKNOWN,
        NOTHING,
        FIRST_LIFE,
        SECOND_LIFE
    }

    public interface a {
        void a();

        boolean a(Card card, int i);

        boolean a(Card card, int i, int i2);

        boolean a(boolean z, int i);

        boolean b();
    }

    void a(int i);

    void a(int i, int i2, HandState handState);

    void a(int i, Cards cards);

    void a(int i, PlayerInfo playerInfo);

    void a(Cards cards);

    void a(a aVar);

    void a(boolean z);

    void b(int i);

    void c(int i, Cards cards);

    void f();

    void g();
}
