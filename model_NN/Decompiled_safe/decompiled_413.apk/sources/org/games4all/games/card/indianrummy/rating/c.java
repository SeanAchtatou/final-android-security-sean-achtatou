package org.games4all.games.card.indianrummy.rating;

import java.util.EnumSet;
import java.util.List;
import org.games4all.game.rating.ContestResult;
import org.games4all.game.rating.Rating;
import org.games4all.game.rating.RatingDescriptor;

public class c extends org.games4all.game.rating.c {
    public c(long j, int i) {
        super(j, i, "LowestMatchScore", 999000000, 0, EnumSet.of(RatingDescriptor.Flag.RATING_ASCENDING), "%.0f (x%.0f)", "");
    }

    public boolean c(Rating rating, ContestResult contestResult, List<ContestResult> list) {
        long d = rating.d();
        long c = ((long) ((IndianRummyMatchResult) contestResult).c(0)) * 1000000;
        if (d > c) {
            rating.a(c);
            rating.b(1000000);
            return true;
        } else if (d != c) {
            return false;
        } else {
            rating.b(rating.e() + 1000000);
            return true;
        }
    }
}
