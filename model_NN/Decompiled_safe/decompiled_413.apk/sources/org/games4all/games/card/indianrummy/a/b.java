package org.games4all.games.card.indianrummy.a;

import org.games4all.card.Cards;
import org.games4all.game.c.c;
import org.games4all.game.move.Move;
import org.games4all.games.card.indianrummy.IndianRummyModel;
import org.games4all.games.card.indianrummy.IndianRummyPrivateModel;
import org.games4all.games.card.indianrummy.move.Discard;
import org.games4all.games.card.indianrummy.move.Done;
import org.games4all.games.card.indianrummy.move.OrderCards;
import org.games4all.games.card.indianrummy.move.Take;
import org.games4all.util.RandomGenerator;

public class b implements c {
    private final IndianRummyModel a;
    private final int b;

    public b(IndianRummyModel indianRummyModel, int i) {
        this.a = indianRummyModel;
        this.b = i;
    }

    public void d() {
    }

    private RandomGenerator b() {
        return ((IndianRummyPrivateModel) this.a.k(this.b)).e();
    }

    public Move a() {
        if (this.a.g()) {
            return new Done();
        }
        RandomGenerator b2 = b();
        if (this.a.h()) {
            return new Take(b2.b(100) > 80, 13);
        }
        Cards cards = new Cards(this.a.e(this.b));
        if (b2.b(100) > 90) {
            cards.a(b2);
            return new OrderCards(cards);
        }
        int b3 = b2.b(14);
        return new Discard(b3, cards.a(b3));
    }
}
