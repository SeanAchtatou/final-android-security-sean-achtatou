package org.games4all.games.card.indianrummy;

import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.game.model.GameModelImpl;

public class IndianRummyModel extends GameModelImpl<IndianRummyHiddenModel, IndianRummyPublicModel, IndianRummyPrivateModel> {
    private static final long serialVersionUID = -8393073972012251100L;

    public IndianRummyModel(IndianRummyHiddenModel indianRummyHiddenModel, IndianRummyPublicModel indianRummyPublicModel, IndianRummyPrivateModel[] indianRummyPrivateModelArr) {
        super(indianRummyHiddenModel, indianRummyPublicModel, indianRummyPrivateModelArr);
    }

    public IndianRummyModel() {
    }

    public boolean a(int i) {
        return i == ((IndianRummyPublicModel) m()).b();
    }

    public int a() {
        return ((IndianRummyPublicModel) m()).a();
    }

    public void b(int i) {
        ((IndianRummyPublicModel) m()).b(i);
    }

    public int b() {
        return ((IndianRummyPublicModel) m()).b();
    }

    public void c(int i) {
        ((IndianRummyPublicModel) m()).c(i);
    }

    public Cards d(int i) {
        return ((IndianRummyPublicModel) m()).a(i);
    }

    public Cards e(int i) {
        return ((IndianRummyPrivateModel) k(i)).a();
    }

    public int f(int i) {
        return ((IndianRummyPublicModel) m()).d(i);
    }

    public void a(int i, int i2) {
        ((IndianRummyPublicModel) m()).a(i, i2);
    }

    public Cards c() {
        return ((IndianRummyPublicModel) m()).c();
    }

    public Cards d() {
        return ((IndianRummyHiddenModel) l()).b();
    }

    public Card e() {
        return ((IndianRummyPublicModel) m()).d();
    }

    public void a(Card card) {
        ((IndianRummyPublicModel) m()).a(card);
    }

    public int f() {
        return ((IndianRummyPublicModel) m()).e();
    }

    public boolean g() {
        return f() >= 0;
    }

    public boolean h() {
        return !g() && f(b()) == 13;
    }

    public void g(int i) {
        ((IndianRummyPublicModel) m()).e(i);
    }

    public void b(int i, int i2) {
        ((IndianRummyPublicModel) m()).b(i, i2);
        ((IndianRummyPrivateModel) k(i)).a(i2);
    }

    public int h(int i) {
        return ((IndianRummyPublicModel) m()).f(i);
    }

    public int i(int i) {
        return ((IndianRummyPrivateModel) k(i)).b();
    }

    public void c(int i, int i2) {
        ((IndianRummyPrivateModel) k(i)).a(i2);
    }

    public int j(int i) {
        return ((IndianRummyPublicModel) m()).g(i);
    }

    public void d(int i, int i2) {
        ((IndianRummyPublicModel) m()).c(i, i2);
    }
}
