package org.games4all.games.card.indianrummy;

import java.io.Serializable;
import org.games4all.card.Cards;
import org.games4all.game.model.PrivateModelImpl;

public class IndianRummyPrivateModel extends PrivateModelImpl implements Serializable {
    private static final long serialVersionUID = 7803200976676437867L;
    private Cards cards;
    private int handScore;

    public IndianRummyPrivateModel(int i) {
        this.cards = new Cards();
    }

    public IndianRummyPrivateModel() {
    }

    public Cards a() {
        return this.cards;
    }

    public int b() {
        return this.handScore;
    }

    public void a(int i) {
        this.handScore = i;
    }

    public String toString() {
        return "IndianRummyPrivateModel[cards=" + this.cards + "]";
    }
}
