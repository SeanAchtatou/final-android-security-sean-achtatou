package org.games4all.games.card.indianrummy;

import org.games4all.game.b;
import org.games4all.game.g;

public enum IndianRummyVariant implements g, b {
    EASY,
    MEDIUM,
    HARD;

    public static final long getGameId() {
        return 8218770682285129728L;
    }

    public long a() {
        return 8218770682285129728L + ((long) (ordinal() << 16));
    }

    public int b() {
        return 4;
    }
}
