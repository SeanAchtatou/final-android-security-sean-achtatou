package org.games4all.games.card.indianrummy.move;

import org.games4all.card.Card;
import org.games4all.game.move.Move;
import org.games4all.game.move.a;
import org.games4all.game.move.c;

public class OrderCard implements Move {
    private static final long serialVersionUID = 4187439686411460905L;
    private Card card;
    private int from;
    private int to;

    public OrderCard(Card card2, int i, int i2) {
        this.card = card2;
        this.from = i;
        this.to = i2;
    }

    public OrderCard() {
    }

    public c a(int i, a aVar) {
        return ((a) aVar).a(i, this.card, this.from, this.to);
    }

    public String toString() {
        return "OrderCard[card=" + this.card + ",from=" + this.from + ",to=" + this.to + "]";
    }

    public int hashCode() {
        int i = 1 * 31;
        return (((((this.card == null ? 0 : this.card.hashCode()) + 31) * 31) + this.from) * 31) + this.to;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderCard orderCard = (OrderCard) obj;
        if (this.card == null) {
            if (orderCard.card != null) {
                return false;
            }
        } else if (!this.card.equals(orderCard.card)) {
            return false;
        }
        if (this.from != orderCard.from) {
            return false;
        }
        return this.to == orderCard.to;
    }
}
