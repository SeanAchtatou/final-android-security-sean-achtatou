package org.games4all.games.card.indianrummy.human;

import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.game.PlayerInfo;
import org.games4all.game.controller.a.e;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.lifecycle.g;
import org.games4all.game.move.PlayerMove;
import org.games4all.games.card.indianrummy.IndianRummyModel;
import org.games4all.games.card.indianrummy.human.IndianRummyViewer;
import org.games4all.games.card.indianrummy.move.Discard;
import org.games4all.games.card.indianrummy.move.Done;
import org.games4all.games.card.indianrummy.move.OrderCard;
import org.games4all.games.card.indianrummy.move.Take;

public class a implements org.games4all.game.controller.b {
    private final org.games4all.game.controller.a.c a;
    private final e b;
    private final org.games4all.games.card.indianrummy.b c = new org.games4all.games.card.indianrummy.b(b());
    private final org.games4all.c.d d = new org.games4all.c.d();

    public a(org.games4all.game.controller.a.c cVar) {
        this.a = cVar;
        this.b = new c(cVar);
        a(cVar);
        b(cVar);
        c(cVar);
        j();
    }

    class c extends e {
        c(org.games4all.game.controller.a.c cVar) {
            super(cVar);
        }

        public void a(int i, PlayerInfo playerInfo) {
            a.this.a().a(i, playerInfo);
        }
    }

    public org.games4all.game.controller.a.c e() {
        return this.a;
    }

    public IndianRummyViewer a() {
        return (IndianRummyViewer) this.a.c();
    }

    /* access modifiers changed from: package-private */
    public IndianRummyModel b() {
        return (IndianRummyModel) this.a.a();
    }

    /* access modifiers changed from: package-private */
    public org.games4all.games.card.indianrummy.b c() {
        return this.c;
    }

    class d extends g {
        d() {
        }

        public void a() {
            IndianRummyViewer a2 = a.this.a();
            IndianRummyModel b = a.this.b();
            if (b.h()) {
                a2.f();
            } else if (b.g()) {
                int f = b.f();
                a2.c(f, b.d(f));
            } else {
                a2.g();
            }
        }

        public void a(org.games4all.game.move.c cVar) {
            if (!cVar.a()) {
                System.err.println("Warning, move failed: " + cVar);
            }
        }

        public void e() {
            throw new RuntimeException("play suspended?");
        }
    }

    private void a(org.games4all.game.controller.a.c cVar) {
        this.d.a(cVar.a(new d()));
    }

    class b implements IndianRummyViewer.a {
        final /* synthetic */ org.games4all.game.controller.a.c a;

        b(org.games4all.game.controller.a.c cVar) {
            this.a = cVar;
        }

        public boolean a(Card card, int i) {
            if (!this.a.d() || !a.this.c().a(this.a.b(), i, card).a()) {
                return false;
            }
            this.a.a(new Discard(i, card));
            return true;
        }

        public boolean a(boolean z, int i) {
            if (!this.a.d() || !a.this.b().h()) {
                return false;
            }
            this.a.a(new Take(z, i));
            return true;
        }

        public boolean a(Card card, int i, int i2) {
            if (!this.a.d() || !a.this.c().a(0, card, i, i2).a()) {
                return false;
            }
            this.a.a(new OrderCard(card, i, i2));
            return true;
        }

        public void a() {
            this.a.f();
        }

        public boolean b() {
            if (!this.a.d() || !a.this.c().c(0).a()) {
                return false;
            }
            this.a.a(new Done());
            return true;
        }
    }

    private void b(org.games4all.game.controller.a.c cVar) {
        a().a(new b(cVar));
    }

    /* renamed from: org.games4all.games.card.indianrummy.human.a$a  reason: collision with other inner class name */
    class C0025a extends org.games4all.game.lifecycle.a {
        C0025a() {
        }

        /* access modifiers changed from: protected */
        public void c() {
            a.this.f();
        }

        /* access modifiers changed from: protected */
        public void c_() {
            a.this.g();
        }

        /* access modifiers changed from: protected */
        public void i() {
            a.this.i();
        }

        /* access modifiers changed from: protected */
        public void m() {
            a.this.h();
        }
    }

    private void c(org.games4all.game.controller.a.c cVar) {
        this.d.a(cVar.a(new C0025a()));
    }

    /* access modifiers changed from: package-private */
    public void f() {
        IndianRummyModel b2 = b();
        IndianRummyViewer a2 = a();
        a2.a(b2.c());
        a2.a(0, b2.e(0));
        for (int i = 1; i < 4; i++) {
            Cards cards = new Cards();
            int f = b2.f(i);
            for (int i2 = 0; i2 < f; i2++) {
                cards.add(Card.a);
            }
            a2.a(i, cards);
        }
        for (int i3 = 0; i3 < 4; i3++) {
            a2.b(i3);
        }
        a2.a(0, b2.i(0), this.c.b(b2.e(0)));
    }

    /* access modifiers changed from: package-private */
    public void g() {
        a().a(false);
    }

    /* access modifiers changed from: package-private */
    public void h() {
        PlayerMove u = b().u();
        int a2 = u.a();
        if (u != null) {
            u.b().a(a2, a());
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        a().a(b().b());
    }

    public void d() {
        this.d.a();
        this.b.a();
        a().d();
        this.a.g();
    }

    private void j() {
        IndianRummyModel b2 = b();
        IndianRummyViewer a2 = a();
        if (b2.q() != Stage.NONE) {
            if (b2.q() == Stage.GAME) {
                for (int i = 0; i < 4; i++) {
                    Cards cards = new Cards(b2.d(i));
                    a2.a(i, cards);
                    a2.a(i, b2.h(i), this.c.b(cards));
                }
                a2.a(true);
                return;
            }
            f();
            a2.a(b2.b());
        }
    }
}
