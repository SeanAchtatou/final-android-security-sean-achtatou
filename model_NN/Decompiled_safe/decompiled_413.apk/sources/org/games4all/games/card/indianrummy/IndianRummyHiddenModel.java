package org.games4all.games.card.indianrummy;

import java.io.Serializable;
import org.games4all.card.Cards;
import org.games4all.game.model.HiddenModelImpl;

public class IndianRummyHiddenModel extends HiddenModelImpl implements Serializable {
    private static final long serialVersionUID = 5017665842255419238L;
    private Cards stockPile;

    public IndianRummyHiddenModel(int i) {
        this.stockPile = new Cards();
    }

    public IndianRummyHiddenModel() {
    }

    public Cards b() {
        return this.stockPile;
    }
}
