package org.games4all.games.card.indianrummy;

import org.games4all.game.lifecycle.Stage;
import org.games4all.game.option.e;
import org.games4all.game.rating.h;
import org.games4all.games.card.indianrummy.rating.b;

public class c implements org.games4all.game.a<d, IndianRummyModel> {
    public d a(IndianRummyModel indianRummyModel) {
        return new d(indianRummyModel);
    }

    /* renamed from: b */
    public IndianRummyModel a(e eVar) {
        IndianRummyHiddenModel indianRummyHiddenModel = new IndianRummyHiddenModel(4);
        IndianRummyPublicModel indianRummyPublicModel = new IndianRummyPublicModel((IndianRummyVariant) ((IndianRummyOptions) eVar).d());
        IndianRummyPrivateModel[] indianRummyPrivateModelArr = new IndianRummyPrivateModel[4];
        for (int i = 0; i < 4; i++) {
            indianRummyPrivateModelArr[i] = new IndianRummyPrivateModel(i);
        }
        return new IndianRummyModel(indianRummyHiddenModel, indianRummyPublicModel, indianRummyPrivateModelArr);
    }

    public h<IndianRummyModel> a(Stage stage) {
        switch (stage) {
            case GAME:
                return new b();
            case MATCH:
                return new org.games4all.games.card.indianrummy.rating.a();
            default:
                return null;
        }
    }
}
