package org.games4all.games.card.indianrummy.move;

import org.games4all.game.move.Move;
import org.games4all.game.move.a;
import org.games4all.game.move.c;

public class Take implements Move {
    private static final long serialVersionUID = 2470648514859272676L;
    private boolean fromDiscard;
    private int position;

    public Take(boolean z, int i) {
        this.fromDiscard = z;
        this.position = i;
    }

    public Take() {
    }

    public c a(int i, a aVar) {
        return ((a) aVar).a(i, this.fromDiscard, this.position);
    }

    public String toString() {
        return "Take[discard=" + this.fromDiscard + ",pos=" + this.position + "]";
    }

    public int hashCode() {
        int i = 1 * 31;
        return (((this.fromDiscard ? 1231 : 1237) + 31) * 31) + this.position;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Take take = (Take) obj;
        if (this.fromDiscard != take.fromDiscard) {
            return false;
        }
        return this.position == take.position;
    }
}
