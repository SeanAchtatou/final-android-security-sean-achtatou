package org.games4all.games.card.indianrummy;

import java.io.Serializable;
import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.game.model.PublicModelImpl;

public class IndianRummyPublicModel extends PublicModelImpl implements Serializable {
    private static final long serialVersionUID = 6453109761783053382L;
    private int[] cardCount;
    private int currentPlayer;
    private Cards discardPile;
    private Card discardTaken;
    private int endPlayer;
    private Cards[] finalHand;
    private int[] handScore;
    private int[] matchScore;
    private int startingPlayer;

    public IndianRummyPublicModel(IndianRummyVariant indianRummyVariant) {
        this.discardPile = new Cards();
        this.cardCount = new int[4];
        this.finalHand = new Cards[4];
        for (int i = 0; i < 4; i++) {
            this.finalHand[i] = new Cards();
        }
        this.handScore = new int[4];
        this.matchScore = new int[400];
    }

    public IndianRummyPublicModel() {
    }

    public Cards a(int i) {
        return this.finalHand[i];
    }

    public int a() {
        return this.startingPlayer;
    }

    public void b(int i) {
        this.startingPlayer = i;
    }

    public int b() {
        return this.currentPlayer;
    }

    public void c(int i) {
        this.currentPlayer = i;
    }

    public Cards c() {
        return this.discardPile;
    }

    public void a(int i, int i2) {
        this.cardCount[i] = i2;
    }

    public int d(int i) {
        return this.cardCount[i];
    }

    public void a(Card card) {
        this.discardTaken = card;
    }

    public Card d() {
        return this.discardTaken;
    }

    public int e() {
        return this.endPlayer;
    }

    public void e(int i) {
        this.endPlayer = i;
    }

    public void b(int i, int i2) {
        this.handScore[i] = i2;
    }

    public int f(int i) {
        return this.handScore[i];
    }

    public void c(int i, int i2) {
        this.matchScore[i] = i2;
    }

    public int g(int i) {
        return this.matchScore[i];
    }
}
