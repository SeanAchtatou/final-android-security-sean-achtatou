package org.games4all.games.card.indianrummy.move;

import org.games4all.card.Card;
import org.games4all.game.move.Move;
import org.games4all.game.move.a;
import org.games4all.game.move.c;

public class Discard implements Move {
    private static final long serialVersionUID = 2470648514859272676L;
    private Card card;
    private int index;

    public Discard(int i, Card card2) {
        this.index = i;
        this.card = card2;
    }

    public Discard() {
    }

    public c a(int i, a aVar) {
        return ((a) aVar).a(i, this.index, this.card);
    }

    public String toString() {
        return "Discard[card=" + this.card + "@" + this.index + "]";
    }

    public int hashCode() {
        int i = 1 * 31;
        return (((this.card == null ? 0 : this.card.hashCode()) + 31) * 31) + this.index;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Discard discard = (Discard) obj;
        if (this.card == null) {
            if (discard.card != null) {
                return false;
            }
        } else if (!this.card.equals(discard.card)) {
            return false;
        }
        return this.index == discard.index;
    }
}
