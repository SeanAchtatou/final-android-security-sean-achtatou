package org.games4all.games.card.indianrummy;

import java.util.EnumSet;
import java.util.Iterator;
import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.card.Face;
import org.games4all.card.Suit;
import org.games4all.game.e;
import org.games4all.game.move.MoveFailed;
import org.games4all.game.move.c;
import org.games4all.games.card.indianrummy.human.IndianRummyViewer;

public class b implements e, org.games4all.games.card.indianrummy.move.a {
    static final /* synthetic */ boolean a = (!b.class.desiredAssertionStatus());
    private final IndianRummyModel b;
    private int c;
    private int d;
    private int e;
    private IndianRummyViewer.HandState f;

    public b(IndianRummyModel indianRummyModel) {
        this.b = indianRummyModel;
    }

    public c a(int i, boolean z, int i2) {
        if (i != this.b.b()) {
            return new MoveFailed("illegal player: " + i);
        }
        if (!this.b.h()) {
            return new MoveFailed("already taken");
        }
        if (i2 < 0 || i2 > 13) {
            return new MoveFailed("illegal position: " + i2);
        }
        return c.a;
    }

    public c b(int i, Cards cards) {
        if (i != this.b.b()) {
            return new MoveFailed("illegal player: " + i);
        }
        return c.a;
    }

    public static int a(Cards cards) {
        int i = 0;
        Iterator it = cards.iterator();
        while (it.hasNext()) {
            i += a((Card) it.next());
        }
        return i;
    }

    public c a(int i, int i2, Card card) {
        if (i != this.b.b()) {
            return new MoveFailed("illegal player: " + i);
        }
        int f2 = this.b.f(i);
        if (f2 != 14) {
            return new MoveFailed("not taken yet");
        }
        if (i2 < 0 || i2 >= f2) {
            return new MoveFailed("illegal index");
        }
        Card a2 = this.b.e(i).a(i2);
        if (!a2.equals(card)) {
            return new MoveFailed("illegal card");
        }
        if (a2.equals(this.b.e())) {
            return new MoveFailed("cannot discard card taken from discard");
        }
        return c.a;
    }

    public c c(int i) {
        if (i != this.b.b()) {
            return new MoveFailed("illegal player: " + i);
        }
        if (!this.b.g()) {
            return new MoveFailed("not in end-play");
        }
        return c.a;
    }

    public IndianRummyViewer.HandState a() {
        return this.f;
    }

    public static int a(Card card) {
        if (card.g()) {
            return 0;
        }
        switch (card.d()) {
            case ACE:
                return -10;
            case KING:
            case QUEEN:
            case JACK:
            case TEN:
                return -10;
            case NINE:
                return -9;
            case EIGHT:
                return -8;
            case SEVEN:
                return -7;
            case SIX:
                return -6;
            case FIVE:
                return -5;
            case FOUR:
                return -4;
            case THREE:
                return -3;
            case TWO:
                return -2;
            default:
                throw new IllegalArgumentException(String.valueOf(card));
        }
    }

    public c a(int i, Card card, int i2, int i3) {
        int f2 = this.b.f(i);
        if (i2 < 0 || i2 >= f2) {
            return new MoveFailed("Illegal from index: " + i2);
        }
        if (i3 < 0 || i3 >= f2) {
            return new MoveFailed("Illegal to index: " + i3);
        }
        return c.a;
    }

    public IndianRummyViewer.HandState b(Cards cards) {
        a(cards, false);
        return this.f;
    }

    public int b() {
        return this.d;
    }

    public int a(Cards cards, boolean z) {
        int i;
        this.c = Integer.MIN_VALUE;
        this.f = IndianRummyViewer.HandState.NOTHING;
        int a2 = a(cards, 0, 0, a(cards), 0, false);
        if (a2 >= 99850) {
            i = a2 - 100000;
            if (i >= 49850) {
                i -= 50000;
                this.f = IndianRummyViewer.HandState.SECOND_LIFE;
            } else {
                this.f = IndianRummyViewer.HandState.FIRST_LIFE;
            }
        } else {
            i = a2;
        }
        if (i >= 49850) {
            i -= 50000;
        }
        return z ? a2 : i;
    }

    private int a(int i, int i2, int i3, boolean z) {
        int i4;
        if (z) {
            i4 = 100000 + i2;
            if (i3 >= 2) {
                i4 += 50000;
            }
        } else {
            i4 = i3 == 1 ? i2 + 50000 : i2;
        }
        if (i4 >= this.c) {
            this.c = i4;
            this.d = i3;
            this.e = i;
        }
        return i4;
    }

    public int c() {
        return this.e;
    }

    private int a(Cards cards, int i, int i2, int i3, int i4, boolean z) {
        if (100000 + i2 + 50000 <= this.c) {
            return Integer.MIN_VALUE;
        }
        if (cards.size() < 3) {
            return a(cards.size() + i, a(cards) + i2, i4, z);
        }
        return Math.max(c(cards, i, i2, i3, i4, z), Math.max(d(cards, i, i2, i3, i4, z), b(cards, i, i2, i3, i4, z)));
    }

    private int b(Cards cards, int i, int i2, int i3, int i4, boolean z) {
        return a(new Cards(cards, 1), i + 1, i2 + a((Card) cards.get(0)), i3, i4, z);
    }

    private int c(Cards cards, int i, int i2, int i3, int i4, boolean z) {
        Face a2;
        Card card = (Card) cards.get(0);
        if (a || card != null) {
            Cards cards2 = new Cards(cards, 1);
            Card card2 = card;
            int i5 = 0;
            while (!cards2.isEmpty() && card2.g()) {
                i5++;
                card2 = (Card) cards2.remove(0);
            }
            if (cards2.isEmpty() && card2.g()) {
                return a(i + 1, i2, i4, z);
            }
            Suit e2 = card2.e();
            Face d2 = card2.d();
            if (d2 != Face.ACE) {
                a2 = d2.a();
            } else if (i5 != 0) {
                return Integer.MIN_VALUE;
            } else {
                a2 = Face.TWO;
            }
            return a(e2, a2, i5 + 1, false, 1, cards2, i, i2, i3, i4, z);
        }
        throw new AssertionError();
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(org.games4all.card.Suit r16, org.games4all.card.Face r17, int r18, boolean r19, int r20, org.games4all.card.Cards r21, int r22, int r23, int r24, int r25, boolean r26) {
        /*
            r15 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r17 == 0) goto L_0x0091
            boolean r3 = r21.isEmpty()
            if (r3 != 0) goto L_0x0091
            org.games4all.card.Face r5 = r17.a()
            r3 = 0
            r0 = r21
            r1 = r3
            java.lang.Object r3 = r0.get(r1)
            org.games4all.card.Card r3 = (org.games4all.card.Card) r3
            org.games4all.card.Cards r9 = new org.games4all.card.Cards
            r6 = 1
            r0 = r9
            r1 = r21
            r2 = r6
            r0.<init>(r1, r2)
            boolean r6 = r3.g()
            if (r6 == 0) goto L_0x005b
            int r6 = r18 + 1
            r8 = 0
            r3 = r15
            r4 = r16
            r7 = r19
            r10 = r22
            r11 = r23
            r12 = r24
            r13 = r25
            r14 = r26
            int r3 = r3.a(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r10 = r3
        L_0x003f:
            r3 = 3
            r0 = r18
            r1 = r3
            if (r0 < r1) goto L_0x008f
            int r8 = r25 + 1
            r9 = r26 | r19
            r3 = r15
            r4 = r21
            r5 = r22
            r6 = r23
            r7 = r24
            int r3 = r3.a(r4, r5, r6, r7, r8, r9)
            int r3 = java.lang.Math.max(r10, r3)
        L_0x005a:
            return r3
        L_0x005b:
            org.games4all.card.Suit r6 = r3.e()
            r0 = r6
            r1 = r16
            if (r0 != r1) goto L_0x0091
            org.games4all.card.Face r3 = r3.d()
            r0 = r3
            r1 = r17
            if (r0 != r1) goto L_0x0091
            r3 = 2
            r0 = r20
            r1 = r3
            if (r0 < r1) goto L_0x008d
            r3 = 1
        L_0x0074:
            r7 = r19 | r3
            int r6 = r18 + 1
            int r8 = r20 + 1
            r3 = r15
            r4 = r16
            r10 = r22
            r11 = r23
            r12 = r24
            r13 = r25
            r14 = r26
            int r3 = r3.a(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r10 = r3
            goto L_0x003f
        L_0x008d:
            r3 = 0
            goto L_0x0074
        L_0x008f:
            r3 = r10
            goto L_0x005a
        L_0x0091:
            r10 = r4
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.games.card.indianrummy.b.a(org.games4all.card.Suit, org.games4all.card.Face, int, boolean, int, org.games4all.card.Cards, int, int, int, int, boolean):int");
    }

    private int d(Cards cards, int i, int i2, int i3, int i4, boolean z) {
        Cards cards2 = new Cards(cards, 1);
        Card card = (Card) cards.get(0);
        int i5 = 0;
        while (!cards2.isEmpty() && card.g() && i5 <= 3) {
            i5++;
            card = (Card) cards2.remove(0);
        }
        if (i5 == 3 || cards2.isEmpty()) {
            return Integer.MIN_VALUE;
        }
        return a(card.d(), EnumSet.of(card.e()), i5 + 1, cards2, i, i2, i3, i4, z);
    }

    private int a(Face face, EnumSet<Suit> enumSet, int i, Cards cards, int i2, int i3, int i4, int i5, boolean z) {
        int i6;
        if (i >= 3) {
            i6 = a(cards, i2, i3, i4, i5, z);
        } else {
            i6 = Integer.MIN_VALUE;
        }
        if (!cards.isEmpty() && i < 4) {
            Card card = (Card) cards.get(0);
            if (card.g()) {
                return Math.max(i6, a(face, enumSet, i + 1, new Cards(cards, 1), i2, i3, i4, i5, z));
            } else if (card.d() == face) {
                Suit e2 = card.e();
                if (!enumSet.contains(e2)) {
                    EnumSet copyOf = EnumSet.copyOf((EnumSet) enumSet);
                    copyOf.add(e2);
                    return Math.max(i6, a(face, copyOf, i + 1, new Cards(cards, 1), i2, i3, i4, i5, z));
                }
            }
        }
        return i6;
    }
}
