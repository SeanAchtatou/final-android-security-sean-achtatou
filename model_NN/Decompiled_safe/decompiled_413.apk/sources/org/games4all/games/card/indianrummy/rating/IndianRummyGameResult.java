package org.games4all.games.card.indianrummy.rating;

import org.games4all.game.rating.BasicContestResult;
import org.games4all.game.rating.ContestResult;
import org.games4all.game.rating.Outcome;
import org.games4all.games.card.indianrummy.IndianRummyModel;

public class IndianRummyGameResult extends BasicContestResult {
    private static final long serialVersionUID = 3717413969663274181L;

    public IndianRummyGameResult(IndianRummyModel indianRummyModel) {
        super(4);
        for (int i = 0; i < 4; i++) {
            a(i, ((long) indianRummyModel.i(i)) * 1000000);
        }
    }

    public IndianRummyGameResult() {
    }

    public Outcome a(int i, int i2) {
        return super.a(i, i2).a();
    }

    public Outcome a(ContestResult contestResult, int i) {
        long a = a(i);
        long a2 = ((BasicContestResult) contestResult).a(i);
        if (a < a2) {
            return Outcome.WIN;
        }
        if (a > a2) {
            return Outcome.LOSS;
        }
        return Outcome.TIE;
    }
}
