package org.games4all.games.card.indianrummy;

import java.util.Collections;
import java.util.Iterator;
import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.game.e;
import org.games4all.game.f;
import org.games4all.game.move.MoveFailed;
import org.games4all.game.move.c;
import org.games4all.games.card.indianrummy.human.IndianRummyViewer;
import org.games4all.games.card.indianrummy.move.TakeResult;
import org.games4all.games.card.indianrummy.move.a;

public class d extends f<IndianRummyModel> implements a {
    private final b a;

    public d(IndianRummyModel indianRummyModel) {
        super(indianRummyModel);
        this.a = new b(indianRummyModel);
    }

    public e n() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void a_() {
        IndianRummyModel indianRummyModel = (IndianRummyModel) o();
        for (int i = 0; i < 4; i++) {
            indianRummyModel.d(i, 0);
        }
        indianRummyModel.b(0);
    }

    /* access modifiers changed from: protected */
    public void c() {
        IndianRummyModel indianRummyModel = (IndianRummyModel) o();
        Cards cards = new Cards();
        cards.addAll(Cards.a());
        cards.addAll(Cards.a());
        cards.add(Card.a());
        cards.add(Card.a());
        cards.add(Card.a());
        cards.add(Card.a());
        cards.a(((IndianRummyHiddenModel) indianRummyModel.l()).a());
        for (int i = 0; i < 4; i++) {
            Cards e = indianRummyModel.e(i);
            e.clear();
            Cards d = indianRummyModel.d(i);
            d.clear();
            for (int i2 = 0; i2 < 13; i2++) {
                d.a(Card.a);
            }
            for (int i3 = 0; i3 < 13; i3++) {
                e.add(cards.remove(cards.size() - 1));
            }
            Collections.sort(e, new org.games4all.card.a(true));
            indianRummyModel.a(i, 13);
            indianRummyModel.b(i, 0);
            a(i);
        }
        Cards c = indianRummyModel.c();
        c.clear();
        c.add(cards.remove(cards.size() - 1));
        Cards d2 = indianRummyModel.d();
        d2.clear();
        d2.addAll(cards);
        indianRummyModel.g(-1);
        indianRummyModel.c(indianRummyModel.a());
    }

    /* access modifiers changed from: protected */
    public void c_() {
        IndianRummyModel indianRummyModel = (IndianRummyModel) o();
        for (int i = 0; i < 4; i++) {
            int i2 = indianRummyModel.i(i);
            indianRummyModel.b(i, indianRummyModel.i(i));
            int j = i2 + indianRummyModel.j(i);
            indianRummyModel.d(i, j);
            if (j >= 400) {
                q();
            }
            Cards d = indianRummyModel.d(i);
            d.clear();
            d.addAll(indianRummyModel.e(i));
        }
        indianRummyModel.b((indianRummyModel.a() + 1) % 4);
        indianRummyModel.t();
    }

    public c a(int i, boolean z, int i2) {
        Card card;
        IndianRummyModel indianRummyModel = (IndianRummyModel) o();
        if (z) {
            Cards c = indianRummyModel.c();
            card = (Card) c.remove(c.size() - 1);
            indianRummyModel.a(card);
        } else {
            Cards d = indianRummyModel.d();
            if (d.isEmpty()) {
                Cards c2 = indianRummyModel.c();
                Card b = c2.b();
                d.addAll(c2);
                c2.clear();
                c2.add(b);
                d.a(indianRummyModel.s());
            }
            int size = d.size() - 1;
            Card a2 = d.a(size);
            d.b(size);
            indianRummyModel.a(Card.a);
            card = a2;
        }
        indianRummyModel.e(i).add(i2, card);
        indianRummyModel.a(i, indianRummyModel.f(i) + 1);
        a(i);
        return new TakeResult(card);
    }

    public c b(int i, Cards cards) {
        Cards e = ((IndianRummyModel) o()).e(i);
        if (e.size() != cards.size()) {
            return new MoveFailed("card mismatch 1: " + cards + " != " + e);
        }
        Cards cards2 = new Cards(cards);
        Iterator it = e.iterator();
        while (it.hasNext()) {
            if (!cards2.remove((Card) it.next())) {
                return new MoveFailed("card mismatch 2: " + cards + " != " + e);
            }
        }
        if (!cards2.isEmpty()) {
            return new MoveFailed("card mismatch 3: " + cards + " != " + e);
        }
        e.clear();
        e.addAll(cards);
        a(i);
        return c.a;
    }

    public c a(int i, Card card, int i2, int i3) {
        Cards e = ((IndianRummyModel) o()).e(i);
        if (!e.contains(card)) {
            return new MoveFailed("illegal card: " + card);
        }
        if (!e.a(i2).equals(card)) {
            return new MoveFailed("illegal position: " + i2 + " != " + e.indexOf(card));
        }
        e.add(i3, (Card) e.remove(i2));
        a(i);
        return c.a;
    }

    private int a(int i) {
        int i2;
        IndianRummyModel indianRummyModel = (IndianRummyModel) o();
        Cards e = indianRummyModel.e(i);
        int i3 = -this.a.a(e, false);
        if (this.a.a() != IndianRummyViewer.HandState.SECOND_LIFE) {
            i2 = -b.a(e);
        } else {
            i2 = i3;
        }
        indianRummyModel.c(i, i2);
        return i2;
    }

    public c a(int i, int i2, Card card) {
        IndianRummyModel indianRummyModel = (IndianRummyModel) o();
        Cards e = indianRummyModel.e(i);
        indianRummyModel.c().add(card);
        if (!card.equals(e.b(i2))) {
            throw new RuntimeException("Discard mismatch");
        }
        indianRummyModel.a(i, indianRummyModel.f(i) - 1);
        if (a(i) == 0) {
            indianRummyModel.g(i);
            indianRummyModel.d(i).a(indianRummyModel.e(i));
            org.games4all.game.table.a o = indianRummyModel.o();
            int i3 = i;
            do {
                i3 = (i3 + 1) % 4;
                if (i3 == i) {
                    break;
                }
            } while (o.a(i3).b());
            if (i3 == i) {
                r();
            } else {
                indianRummyModel.c(i3);
                s();
            }
        } else {
            indianRummyModel.c((indianRummyModel.b() + 1) % 4);
            s();
        }
        return c.a;
    }

    public c c(int i) {
        IndianRummyModel indianRummyModel = (IndianRummyModel) o();
        int b = (indianRummyModel.b() + 1) % 4;
        if (b == indianRummyModel.f()) {
            r();
        } else {
            indianRummyModel.c(b);
        }
        return c.a;
    }
}
