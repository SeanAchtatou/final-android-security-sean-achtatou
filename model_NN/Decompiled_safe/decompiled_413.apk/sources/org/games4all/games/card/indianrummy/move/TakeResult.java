package org.games4all.games.card.indianrummy.move;

import org.games4all.card.Card;
import org.games4all.game.move.MoveSucceeded;

public class TakeResult extends MoveSucceeded {
    private static final long serialVersionUID = -7357854221993320296L;
    private Card card;

    public TakeResult(Card card2) {
        this.card = card2;
    }

    public TakeResult() {
    }

    public String toString() {
        return "TakeResult[card=" + this.card + "]";
    }
}
