package org.games4all.f;

import java.lang.Enum;

class b<S extends Enum<S>, E extends Enum<E>> {
    private final E a;
    private final S b;

    public b(E e, S s) {
        this.b = s;
        this.a = e;
    }

    public S a() {
        return this.b;
    }

    public E b() {
        return this.a;
    }
}
