package org.games4all.f;

import java.lang.Enum;

public abstract class d<S extends Enum<S>, E extends Enum<E>> implements a<S, E> {
    private final String a;

    /* access modifiers changed from: protected */
    public abstract void a(String str);

    public d(String str) {
        this.a = str;
    }

    public void a(S s, E e, S s2) {
        a(this.a + " - transition from " + ((Object) s) + " to " + ((Object) s2) + " on event " + ((Object) e));
    }
}
