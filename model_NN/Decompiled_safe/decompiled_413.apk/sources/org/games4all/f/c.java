package org.games4all.f;

import java.lang.Enum;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.games4all.c.b;
import org.games4all.e.a;
import org.games4all.util.e;

public class c<S extends Enum<S>, E extends Enum<E>> {
    private final Object a;
    private final Class<S> b;
    private final Class<E> c;
    private S d;
    private final Map<S, List<b<S, E>>> e;
    private final Map<S, Method> f;
    private final Map<S, Method> g;
    private final Map<E, Method> h;
    private final org.games4all.c.c<a<S, E>> i;

    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c(java.lang.Object r4, java.lang.Class<S> r5, java.lang.Class<E> r6, java.lang.Class<? extends java.lang.annotation.Annotation> r7, java.lang.Class<? extends java.lang.annotation.Annotation> r8, java.lang.Class<? extends java.lang.annotation.Annotation> r9) {
        /*
            r3 = this;
            r3.<init>()
            if (r4 != 0) goto L_0x0056
            r0 = r3
        L_0x0006:
            r3.a = r0
            r3.b = r5
            r3.c = r6
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.e = r0
            java.util.EnumMap r0 = new java.util.EnumMap
            r0.<init>(r5)
            r3.f = r0
            java.util.EnumMap r0 = new java.util.EnumMap
            r0.<init>(r5)
            r3.g = r0
            java.util.EnumMap r0 = new java.util.EnumMap
            r0.<init>(r6)
            r3.h = r0
            java.lang.Object r0 = r3.a
            java.lang.Class r0 = r0.getClass()
            java.lang.String r1 = "onEnter"
            java.util.Map<S, java.lang.reflect.Method> r2 = r3.f
            r3.a(r0, r7, r1, r2)
            java.lang.Object r0 = r3.a
            java.lang.Class r0 = r0.getClass()
            java.lang.String r1 = "onExit"
            java.util.Map<S, java.lang.reflect.Method> r2 = r3.g
            r3.a(r0, r8, r1, r2)
            java.lang.Object r0 = r3.a
            java.lang.Class r0 = r0.getClass()
            java.lang.String r1 = "onEvent"
            java.util.Map<E, java.lang.reflect.Method> r2 = r3.h
            r3.b(r0, r9, r1, r2)
            org.games4all.c.c r0 = r3.a()
            r3.i = r0
            return
        L_0x0056:
            r0 = r4
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.f.c.<init>(java.lang.Object, java.lang.Class, java.lang.Class, java.lang.Class, java.lang.Class, java.lang.Class):void");
    }

    private org.games4all.c.c<a<S, E>> a() {
        return new org.games4all.c.c<>(a.class);
    }

    public b a(a aVar) {
        return this.i.c(aVar);
    }

    private void a(Class<?> cls, Class<? extends Annotation> cls2, String str, Map<S, Method> map) {
        for (Method method : cls.getDeclaredMethods()) {
            Annotation annotation = cls2 == null ? null : method.getAnnotation(cls2);
            if (annotation != null) {
                try {
                    Enum enumR = (Enum) a.a(annotation.getClass().getMethod("value", new Class[0]), annotation, new Object[0]);
                    if (map.get(enumR) != null) {
                        throw new RuntimeException("duplicate enter/exit method: " + method + " and " + map.get(enumR));
                    }
                    method.setAccessible(true);
                    map.put(enumR, method);
                } catch (SecurityException e2) {
                    throw new RuntimeException(e2);
                } catch (NoSuchMethodException e3) {
                    throw new RuntimeException(e3);
                }
            } else {
                String name = method.getName();
                if (name.startsWith(str)) {
                    Enum a2 = e.a(this.b, name.substring(str.length()));
                    method.setAccessible(true);
                    map.put(a2, method);
                }
            }
        }
        Class<? super Object> superclass = cls.getSuperclass();
        if (superclass != null && superclass != Object.class) {
            a(superclass, cls2, str, map);
        }
    }

    private void b(Class<?> cls, Class<? extends Annotation> cls2, String str, Map<E, Method> map) {
        for (Method method : cls.getDeclaredMethods()) {
            Annotation annotation = cls2 == null ? null : method.getAnnotation(cls2);
            if (annotation != null) {
                try {
                    Enum enumR = (Enum) a.a(annotation.getClass().getMethod("value", new Class[0]), annotation, new Object[0]);
                    if (map.get(enumR) != null) {
                        throw new RuntimeException("duplicate enter/exit method: " + method + " and " + map.get(enumR));
                    }
                    method.setAccessible(true);
                    map.put(enumR, method);
                } catch (SecurityException e2) {
                    throw new RuntimeException(e2);
                } catch (NoSuchMethodException e3) {
                    throw new RuntimeException(e3);
                }
            } else {
                String name = method.getName();
                if (name.startsWith(str)) {
                    Enum a2 = e.a(this.c, name.substring(str.length()));
                    method.setAccessible(true);
                    map.put(a2, method);
                }
            }
        }
        Class<? super Object> superclass = cls.getSuperclass();
        if (superclass != null && superclass != Object.class) {
            b(superclass, cls2, str, map);
        }
    }

    public void a(Enum enumR) {
        this.d = enumR;
        a(enumR, this.f);
    }

    private void d(E e2) {
        Method method = this.h.get(e2);
        if (method != null) {
            a.a(method, this.a, new Object[0]);
        }
    }

    private void a(Enum enumR, Map map) {
        Method method = (Method) map.get(enumR);
        if (method != null) {
            a.a(method, this.a, new Object[0]);
        }
    }

    public void a(Enum enumR, Enum enumR2, Enum enumR3) {
        b bVar = new b(enumR2, enumR3);
        Object obj = this.e.get(enumR);
        if (obj == null) {
            obj = new ArrayList();
            this.e.put(enumR, obj);
        }
        obj.add(bVar);
    }

    public void a(EnumSet enumSet, Enum enumR, Enum enumR2) {
        Iterator it = enumSet.iterator();
        while (it.hasNext()) {
            a((Enum) it.next(), enumR, enumR2);
        }
    }

    public void a(Enum enumR, Enum enumR2) {
        if (this.d == enumR) {
            b(enumR2);
        }
    }

    public void b(E e2) {
        List<b> list = this.e.get(this.d);
        if (list == null) {
            throw new RuntimeException("No transitions for this state: " + ((Object) this.d));
        }
        for (b bVar : list) {
            if (bVar.b() == e2) {
                S s = this.d;
                a(this.d, this.g);
                d(e2);
                this.d = bVar.a();
                this.i.c().a(s, e2, this.d);
                a(this.d, this.f);
                return;
            }
        }
        throw new RuntimeException("No transitions for event " + ((Object) e2) + " in state " + ((Object) this.d));
    }

    public boolean c(S s) {
        return this.d == s;
    }
}
