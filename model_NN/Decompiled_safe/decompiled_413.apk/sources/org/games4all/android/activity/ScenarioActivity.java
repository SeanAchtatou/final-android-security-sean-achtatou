package org.games4all.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.games4all.android.b.d;
import org.games4all.game.test.h;

public class ScenarioActivity extends Games4AllActivity {
    private View a;
    private final Map<String, Integer> b = new HashMap();
    private int c;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        d dVar = new d();
        a(dVar, a(dVar));
    }

    private Runnable a(org.games4all.a.a aVar) {
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("SCREENSHOT");
        String stringExtra2 = intent.getStringExtra("WHEN");
        if (stringExtra == null) {
            return null;
        }
        if (stringExtra2 != null) {
            return new c(stringExtra);
        }
        StringTokenizer stringTokenizer = new StringTokenizer(stringExtra2, ",");
        b bVar = null;
        while (stringTokenizer.hasMoreTokens()) {
            String trim = stringTokenizer.nextToken().trim();
            b bVar2 = new b(stringExtra, trim);
            if (trim.equals("end")) {
                bVar = bVar2;
            } else {
                char charAt = trim.charAt(0);
                if (charAt == '<') {
                    aVar.a(trim.substring(1), bVar2, null);
                } else if (charAt == '>') {
                    aVar.a(trim.substring(1), null, bVar2);
                } else if (charAt == '^') {
                    aVar.a(trim.substring(1), bVar2, bVar2);
                } else {
                    aVar.a(trim, null, bVar2);
                }
            }
        }
        return bVar;
    }

    class c implements Runnable {
        final /* synthetic */ String a;

        c(String str) {
            this.a = str;
        }

        public void run() {
            ScenarioActivity.this.a(ScenarioActivity.this.a(this.a, "end"));
        }
    }

    class b implements Runnable {
        final /* synthetic */ String a;
        final /* synthetic */ String b;

        b(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        public void run() {
            ScenarioActivity.this.a(ScenarioActivity.this.a(this.a, this.b));
        }
    }

    private void a(org.games4all.a.a aVar, Runnable runnable) {
        h hVar;
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("SCENARIO");
        String stringExtra2 = intent.getStringExtra("DONE");
        if (stringExtra != null) {
            try {
                hVar = (h) Class.forName(stringExtra).newInstance();
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            } catch (InstantiationException e2) {
                throw new RuntimeException(e2);
            } catch (IllegalAccessException e3) {
                throw new RuntimeException(e3);
            }
        } else {
            String stringExtra3 = intent.getStringExtra("SCENARIO_INDEX");
            if (stringExtra3 == null) {
                throw new RuntimeException("NO SCENARIO SPECIFIED");
            }
            hVar = (h) Collections.emptyList().get(Integer.parseInt(stringExtra3));
        }
        boolean equals = "true".equals(intent.getStringExtra("FINISH"));
        System.err.println("finishWhenDone: " + equals);
        a(aVar, runnable, stringExtra2, equals, hVar);
    }

    private void a(org.games4all.a.a aVar, Runnable runnable, String str, boolean z, h hVar) {
        a(hVar.b(), aVar);
        new Thread(new a(hVar, runnable, aVar, z, str)).start();
    }

    class a implements Runnable {
        final /* synthetic */ h a;
        final /* synthetic */ Runnable b;
        final /* synthetic */ org.games4all.a.a c;
        final /* synthetic */ boolean d;
        final /* synthetic */ String e;

        a(h hVar, Runnable runnable, org.games4all.a.a aVar, boolean z, String str) {
            this.a = hVar;
            this.b = runnable;
            this.c = aVar;
            this.d = z;
            this.e = str;
        }

        public void run() {
            String str;
            int i;
            long currentTimeMillis = System.currentTimeMillis();
            System.err.println("Running: " + this.a.a());
            try {
                this.a.c();
                str = "Scenario finished in " + (System.currentTimeMillis() - currentTimeMillis) + " ms.";
                i = 1;
            } catch (Exception e2) {
                System.err.println("EXCEPTION IN SCENARIO:");
                e2.printStackTrace();
                str = "Scenario failed in " + (System.currentTimeMillis() - currentTimeMillis) + "ms.:" + e2.getMessage();
                i = 2;
            }
            System.err.println(str);
            if (this.b != null) {
                this.c.execute(this.b);
            }
            this.c.execute(new C0007a(str, i, str));
            this.c.d();
        }

        /* renamed from: org.games4all.android.activity.ScenarioActivity$a$a  reason: collision with other inner class name */
        class C0007a implements Runnable {
            final /* synthetic */ String a;
            final /* synthetic */ int b;
            final /* synthetic */ String c;

            C0007a(String str, int i, String str2) {
                this.a = str;
                this.b = i;
                this.c = str2;
            }

            public void run() {
                if (a.this.d) {
                    Intent intent = new Intent();
                    intent.putExtra("RESULT", this.a);
                    ScenarioActivity.this.setResult(this.b, intent);
                    ScenarioActivity.this.finish();
                }
                if (a.this.e != null) {
                    File file = new File(a.this.e);
                    file.getParentFile().mkdirs();
                    try {
                        PrintWriter printWriter = new PrintWriter(file);
                        printWriter.println(this.c);
                        printWriter.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    private void a(org.games4all.game.test.c cVar, org.games4all.a.a aVar) {
        cVar.a(p().a(this, aVar));
        cVar.a(aVar);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        File file = new File(str);
        file.getParentFile().mkdirs();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            this.a.setDrawingCacheEnabled(true);
            Bitmap createBitmap = Bitmap.createBitmap(this.a.getDrawingCache());
            this.a.setDrawingCacheEnabled(false);
            createBitmap.compress(Bitmap.CompressFormat.PNG, 60, fileOutputStream);
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        super.setContentView(view);
        this.a = view;
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2) {
        int intValue;
        Integer num = this.b.get(str2);
        if (num == null) {
            intValue = 0;
        } else {
            intValue = num.intValue();
        }
        int i = intValue + 1;
        this.b.put(str2, Integer.valueOf(i));
        String replace = str.replace("{mark}", str2).replace("{count}", String.valueOf(i)).replace("{total}", String.valueOf(this.c));
        this.c++;
        return replace;
    }
}
