package org.games4all.android.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebView;

public class WebPageActivity extends Activity {
    private WebView a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = new WebView(this);
        this.a.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setContentView(this.a);
        this.a.loadUrl(getIntent().getDataString());
    }
}
