package org.games4all.android.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import java.io.ByteArrayOutputStream;
import java.util.Stack;
import org.games4all.android.GameApplication;
import org.games4all.android.a;
import org.games4all.android.e.b;
import org.games4all.android.e.d;
import org.games4all.logging.LogLevel;

public abstract class Games4AllActivity extends Activity {
    private a a;
    private View b;
    private Menu c;
    private final Stack<d> d = new Stack<>();

    /* access modifiers changed from: protected */
    public abstract void a(View view);

    public void a(a aVar) {
        this.a = aVar;
        a(((b) aVar).a());
    }

    public GameApplication p() {
        return (GameApplication) getApplication();
    }

    public a q() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        org.games4all.android.test.a d2 = p().d();
        if (d2 != null) {
            d2.a(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        for (int size = this.d.size() - 1; size >= 0; size--) {
            this.d.get(size).dismiss();
        }
        p().d().b(this);
    }

    public org.games4all.android.report.a b(int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(20500);
        System.err.println("*** START SCREENSHOT");
        this.b.setDrawingCacheEnabled(true);
        Bitmap drawingCache = this.b.getDrawingCache();
        if (drawingCache == null) {
            System.err.println("no screen drawing cache so no screenshot");
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(drawingCache);
        this.b.setDrawingCacheEnabled(false);
        System.err.println("*** END SCREENSHOT");
        d a2 = p().d().a();
        if (a2 != null) {
            View d2 = a2.d();
            d2.setDrawingCacheEnabled(true);
            Bitmap drawingCache2 = d2.getDrawingCache();
            if (drawingCache2 == null) {
                System.err.println("no dialog drawing cache so no screenshot");
                return null;
            }
            Bitmap createBitmap2 = Bitmap.createBitmap(drawingCache2);
            d2.setDrawingCacheEnabled(false);
            int width = createBitmap.getWidth();
            int height = createBitmap.getHeight();
            int width2 = createBitmap2.getWidth();
            int height2 = createBitmap2.getHeight();
            if (!createBitmap.isMutable()) {
                createBitmap = createBitmap.copy(createBitmap.getConfig(), true);
            }
            Canvas canvas = new Canvas(createBitmap);
            WindowManager.LayoutParams attributes = a2.getWindow().getAttributes();
            canvas.drawBitmap(createBitmap2, (float) (((width - width2) / 2) + attributes.x), (float) (attributes.y + ((height - height2) / 2)), new Paint());
        }
        if (i != 1) {
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(createBitmap, createBitmap.getWidth() / i, createBitmap.getHeight() / i, true);
            createBitmap.recycle();
            createBitmap = createScaledBitmap;
        }
        byteArrayOutputStream.reset();
        createBitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        createBitmap.recycle();
        return new org.games4all.android.report.a(byteArrayOutputStream.toByteArray());
    }

    public void setContentView(View view) {
        this.b = view;
        super.setContentView(view);
    }

    public void setContentView(int i) {
        setContentView(LayoutInflater.from(this).inflate(i, (ViewGroup) null));
    }

    public View r() {
        return this.b;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.c = menu;
        return super.onCreateOptionsMenu(menu);
    }

    public Menu s() {
        return this.c;
    }

    public d t() {
        if (this.d.isEmpty()) {
            return null;
        }
        return this.d.peek();
    }

    public void a(d dVar) {
        this.d.push(dVar);
    }

    public void b(d dVar) {
        if (dVar != t()) {
            System.err.println("Warning, dismissed dialog: " + dVar + " != top dialog: " + t());
        }
        this.d.remove(dVar);
    }

    public void a(LogLevel logLevel, String str) {
        p().a().a(logLevel, str);
    }
}
