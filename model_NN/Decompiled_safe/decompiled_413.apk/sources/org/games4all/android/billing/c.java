package org.games4all.android.billing;

import android.os.Bundle;
import com.a.a.a.a;

public class c extends f {
    long a;
    final String[] b;

    public /* bridge */ /* synthetic */ int a() {
        return super.a();
    }

    public c(String str, int i, String[] strArr) {
        super(str, i);
        this.b = strArr;
    }

    /* access modifiers changed from: protected */
    public long a(a aVar) {
        this.a = a.a();
        Bundle a2 = a("GET_PURCHASE_INFORMATION");
        a2.putLong("NONCE", this.a);
        a2.putStringArray("NOTIFY_IDS", this.b);
        Bundle a3 = aVar.a(a2);
        a("getPurchaseInformation", a3);
        return a3.getLong("REQUEST_ID", c);
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc) {
        a.a(this.a);
    }
}
