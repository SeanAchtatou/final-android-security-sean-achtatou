package org.games4all.android.billing;

import android.text.TextUtils;
import android.util.Log;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashSet;
import org.games4all.json.jsonorg.JSONException;
import org.games4all.json.jsonorg.b;
import org.games4all.json.jsonorg.c;

public class a {
    private static final SecureRandom a = new SecureRandom();
    private static HashSet<Long> b = new HashSet<>();

    /* renamed from: org.games4all.android.billing.a$a  reason: collision with other inner class name */
    public static class C0008a {
        public PurchaseState a;
        public String b;
        public String c;
        public String d;
        public long e;
        public String f;

        public C0008a(PurchaseState purchaseState, String str, String str2, String str3, long j, String str4) {
            this.a = purchaseState;
            this.b = str;
            this.c = str2;
            this.d = str3;
            this.e = j;
            this.f = str4;
        }
    }

    public static long a() {
        long nextLong = a.nextLong();
        b.add(Long.valueOf(nextLong));
        return nextLong;
    }

    public static void a(long j) {
        b.remove(Long.valueOf(j));
    }

    public static boolean b(long j) {
        return b.contains(Long.valueOf(j));
    }

    public static ArrayList<C0008a> a(String str, String str2) {
        boolean z;
        if (str == null) {
            Log.e("Security", "data is null");
            return null;
        }
        Log.i("Security", "signedData: " + str);
        if (!TextUtils.isEmpty(str2)) {
            z = a(a("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm83InZAWHAsP5rP7hyZ/X2rGM0Z7SFDBz1chWHUn+gLiKT59jju/yHkGAFPplDji98cCOcyPkw5x1oMv+WPMw633Ce7iNea2zJ6uq9wfS32PfEIHlSJoJQ8O2Nsv18mahoQwjiMTN/dmOl8bDvX/yLlNYMFZA7hO4iBLgFEJI4hj/OVv+qEQ/LPiytVqF4GmWkE+/Cni+sb9qPM7f2gPvcgt4Em4eVNraKA5+VBoVvEdX7urMl4bhrpU8s26XNZ2S1fOJwvcVRaAxRG1GSSktnUdhjnE2WOiB3hHrfcQEl9pUDdKGAx2h75ydxvE2HfZySvbw300Vg6Tkcmd2HqRgwIDAQAB"), str, str2);
            if (!z) {
                Log.w("Security", "signature does not match data.");
                return null;
            }
        } else {
            z = false;
        }
        try {
            c cVar = new c(str);
            long i = cVar.i("nonce");
            b h = cVar.h("orders");
            if (h == null) {
                System.err.println("warning: orders null");
                return null;
            }
            int a2 = h.a();
            if (!b(i)) {
                Log.w("Security", "Nonce not found: " + i);
                return null;
            }
            ArrayList<C0008a> arrayList = new ArrayList<>();
            int i2 = 0;
            while (i2 < a2) {
                try {
                    c b2 = h.b(i2);
                    PurchaseState a3 = PurchaseState.a(b2.b("purchaseState"));
                    String e = b2.e("productId");
                    long d = b2.d("purchaseTime");
                    String a4 = b2.a("orderId", "");
                    String str3 = null;
                    if (b2.f("notificationId")) {
                        str3 = b2.e("notificationId");
                    }
                    String a5 = b2.a("developerPayload", (String) null);
                    if (a3 != PurchaseState.PURCHASED || z) {
                        arrayList.add(new C0008a(a3, str3, e, a4, d, a5));
                    }
                    i2++;
                } catch (JSONException e2) {
                    Log.e("Security", "JSON exception: ", e2);
                    return null;
                }
            }
            a(i);
            return arrayList;
        } catch (JSONException e3) {
            return null;
        }
    }

    public static PublicKey a(String str) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(d.a(str)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e2) {
            Log.e("Security", "Invalid key specification.");
            throw new IllegalArgumentException(e2);
        } catch (Base64DecoderException e3) {
            Log.e("Security", "Base64 decoding failed.");
            throw new IllegalArgumentException(e3);
        }
    }

    public static boolean a(PublicKey publicKey, String str, String str2) {
        Log.i("Security", "signature: " + str2);
        try {
            Signature instance = Signature.getInstance("SHA1withRSA");
            instance.initVerify(publicKey);
            instance.update(str.getBytes());
            if (instance.verify(d.a(str2))) {
                return true;
            }
            Log.e("Security", "Signature verification failed.");
            return false;
        } catch (NoSuchAlgorithmException e) {
            Log.e("Security", "NoSuchAlgorithmException.");
            return false;
        } catch (InvalidKeyException e2) {
            Log.e("Security", "Invalid key specification.");
            return false;
        } catch (SignatureException e3) {
            Log.e("Security", "Signature exception.");
            return false;
        } catch (Base64DecoderException e4) {
            Log.e("Security", "Base64 decoding failed.");
            return false;
        }
    }
}
