package org.games4all.android.billing;

public enum PurchaseState {
    PURCHASED,
    CANCELED,
    REFUNDED;

    public static PurchaseState a(int i) {
        PurchaseState[] values = values();
        if (i < 0 || i >= values.length) {
            return CANCELED;
        }
        return values[i];
    }
}
