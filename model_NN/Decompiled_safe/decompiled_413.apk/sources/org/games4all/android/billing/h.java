package org.games4all.android.billing;

import android.os.Bundle;
import com.a.a.a.a;

public class h extends f {
    final String[] a;

    public /* bridge */ /* synthetic */ int a() {
        return super.a();
    }

    public h(String str, int i, String[] strArr) {
        super(str, i);
        this.a = strArr;
    }

    /* access modifiers changed from: protected */
    public long a(a aVar) {
        Bundle a2 = a("CONFIRM_NOTIFICATIONS");
        a2.putStringArray("NOTIFY_IDS", this.a);
        Bundle a3 = aVar.a(a2);
        a("confirmNotifications", a3);
        return a3.getLong("REQUEST_ID", c);
    }
}
