package org.games4all.android.billing;

import android.os.Bundle;
import android.util.Log;
import com.a.a.a.a;

abstract class f {
    static long c = -1;
    private final String a;
    private final int b;

    /* access modifiers changed from: protected */
    public abstract long a(a aVar);

    public f(String str, int i) {
        this.a = str;
        this.b = i;
    }

    public int a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc) {
    }

    /* access modifiers changed from: protected */
    public void a(ResponseCode responseCode) {
    }

    /* access modifiers changed from: protected */
    public Bundle a(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("BILLING_REQUEST", str);
        bundle.putInt("API_VERSION", 1);
        bundle.putString("PACKAGE_NAME", this.a);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void a(String str, Bundle bundle) {
        Log.e("BILL", str + " received " + ResponseCode.a(bundle.getInt("RESPONSE_CODE")).toString());
    }
}
