package org.games4all.android.billing;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import java.lang.reflect.Method;

public abstract class b {
    private static final Class[] d = {IntentSender.class, Intent.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
    private final Activity a;
    private Method b;
    private final Object[] c = new Object[5];

    public abstract void a(PurchaseState purchaseState, String str, int i, long j, String str2);

    public abstract void a(boolean z);

    public b(Activity activity) {
        this.a = activity;
        b();
    }

    /* access modifiers changed from: protected */
    public Activity a() {
        return this.a;
    }

    private void b() {
        try {
            this.b = this.a.getClass().getMethod("startIntentSender", d);
        } catch (SecurityException e) {
            this.b = null;
        } catch (NoSuchMethodException e2) {
            this.b = null;
        }
    }
}
