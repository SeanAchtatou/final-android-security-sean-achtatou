package org.games4all.android.billing;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.a.a.a.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import org.games4all.android.billing.a;

public class BillingService extends Service implements ServiceConnection {
    private static e a;
    private a b;
    private final Queue<f> c = new LinkedList();
    private final Map<Long, f> d = new HashMap();

    public static e a(Context context) {
        if (a == null) {
            a = new e(context);
        }
        return a;
    }

    public void onCreate() {
        super.onCreate();
        b();
    }

    private boolean b() {
        System.err.println("binding to market billing service");
        try {
            boolean bindService = bindService(new Intent("com.android.vending.billing.MarketBillingService.BIND"), this, 1);
            if (bindService) {
                Log.i("BILL", "Service bind successful.");
                return bindService;
            }
            Log.e("BILL", "Could not bind to the MarketBillingService.");
            return bindService;
        } catch (SecurityException e) {
            Log.e("BILL", "Security exception: " + e);
            return false;
        }
    }

    public void onDestroy() {
        try {
            unbindService(this);
        } catch (IllegalArgumentException e) {
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        System.err.println("billing service connected");
        Log.d("BILL", "Billing service connected");
        this.b = a.C0001a.a(iBinder);
        c();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        Log.w("BILL", "Billing service disconnected");
        d();
    }

    public boolean a(f fVar) {
        if (b(fVar)) {
            return true;
        }
        if (!b()) {
            return false;
        }
        c(fVar);
        return true;
    }

    public boolean b(f fVar) {
        Log.d("BILL", fVar.getClass().getSimpleName());
        if (this.b != null) {
            try {
                long a2 = fVar.a(this.b);
                Log.d("BILL", "request id: " + a2);
                if (a2 >= 0) {
                    a(a2, fVar);
                }
                return true;
            } catch (RemoteException e) {
                Log.w("BILL", "Billing service crashed");
                fVar.a(e);
                d();
            }
        }
        return false;
    }

    private void c() {
        int i = -1;
        while (true) {
            f peek = this.c.peek();
            if (peek != null) {
                if (b(peek)) {
                    this.c.remove();
                    if (i < peek.a()) {
                        i = peek.a();
                    }
                } else {
                    b();
                    return;
                }
            } else if (i >= 0) {
                Log.i("BILL", "stopping service, startId: " + i);
                stopSelf(i);
                return;
            } else {
                return;
            }
        }
    }

    private void d() {
        this.b = null;
    }

    /* access modifiers changed from: package-private */
    public void c(f fVar) {
        this.c.add(fVar);
    }

    /* access modifiers changed from: package-private */
    public void a(long j, f fVar) {
        this.d.put(Long.valueOf(j), fVar);
    }

    public void b(Context context) {
        attachBaseContext(context);
    }

    public void onStart(Intent intent, int i) {
        a(intent, i);
    }

    public void a(Intent intent, int i) {
        String action = intent.getAction();
        Log.i("BILL", "handleCommand() action: " + action);
        if ("org.games4all.android.CONFIRM_NOTIFICATION".equals(action)) {
            a(i, intent.getStringArrayExtra("notification_id"));
        } else if ("org.games4all.android.GET_PURCHASE_INFORMATION".equals(action)) {
            b(i, new String[]{intent.getStringExtra("notification_id")});
        } else if ("com.android.vending.billing.PURCHASE_STATE_CHANGED".equals(action)) {
            a(i, intent.getStringExtra("inapp_signed_data"), intent.getStringExtra("inapp_signature"));
        } else if ("com.android.vending.billing.RESPONSE_CODE".equals(action)) {
            a(intent.getLongExtra("request_id", -1), ResponseCode.a(intent.getIntExtra("response_code", ResponseCode.RESULT_ERROR.ordinal())));
        }
    }

    public boolean a() {
        return a(new g(getPackageName()));
    }

    private boolean a(int i, String[] strArr) {
        return a(new h(getPackageName(), i, strArr));
    }

    private boolean b(int i, String[] strArr) {
        return a(new c(getPackageName(), i, strArr));
    }

    private void a(int i, String str, String str2) {
        ArrayList<a.C0008a> a2 = a.a(str, str2);
        if (a2 != null) {
            e a3 = a(this);
            ArrayList arrayList = new ArrayList();
            Iterator<a.C0008a> it = a2.iterator();
            while (it.hasNext()) {
                a.C0008a next = it.next();
                int a4 = a3.a(next.d, next.c, next.a, next.e, next.f);
                if (next.b != null) {
                    arrayList.add(next.b);
                }
                i.a(this, next.a, next.c, a4, next.d, next.e, next.f);
            }
            if (!arrayList.isEmpty()) {
                a(i, (String[]) arrayList.toArray(new String[arrayList.size()]));
            }
        }
    }

    private void a(long j, ResponseCode responseCode) {
        f fVar = this.d.get(Long.valueOf(j));
        if (fVar != null) {
            Log.d("BILL", fVar.getClass().getSimpleName() + ": " + responseCode);
            fVar.a(responseCode);
        }
        this.d.remove(Long.valueOf(j));
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
