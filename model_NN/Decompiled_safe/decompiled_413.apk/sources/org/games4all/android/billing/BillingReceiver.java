package org.games4all.android.billing;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BillingReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.android.vending.billing.PURCHASE_STATE_CHANGED".equals(action)) {
            a(context, intent.getStringExtra("inapp_signed_data"), intent.getStringExtra("inapp_signature"));
        } else if ("com.android.vending.billing.IN_APP_NOTIFY".equals(action)) {
            String stringExtra = intent.getStringExtra("notification_id");
            Log.i("BillingReceiver", "notifyId: " + stringExtra);
            a(context, stringExtra);
        } else if ("com.android.vending.billing.RESPONSE_CODE".equals(action)) {
            a(context, intent.getLongExtra("request_id", -1), intent.getIntExtra("response_code", ResponseCode.RESULT_ERROR.ordinal()));
        } else {
            Log.w("BillingReceiver", "unexpected action: " + action);
        }
    }

    private void a(Context context, String str, String str2) {
        Intent intent = new Intent("com.android.vending.billing.PURCHASE_STATE_CHANGED");
        intent.setClass(context, BillingService.class);
        intent.putExtra("inapp_signed_data", str);
        intent.putExtra("inapp_signature", str2);
        context.startService(intent);
    }

    private void a(Context context, String str) {
        Intent intent = new Intent("org.games4all.android.GET_PURCHASE_INFORMATION");
        intent.setClass(context, BillingService.class);
        intent.putExtra("notification_id", str);
        context.startService(intent);
    }

    private void a(Context context, long j, int i) {
        Intent intent = new Intent("com.android.vending.billing.RESPONSE_CODE");
        intent.setClass(context, BillingService.class);
        intent.putExtra("request_id", j);
        intent.putExtra("response_code", i);
        context.startService(intent);
    }
}
