package org.games4all.android.billing;

public enum ResponseCode {
    RESULT_OK,
    RESULT_USER_CANCELED,
    RESULT_SERVICE_UNAVAILABLE,
    RESULT_BILLING_UNAVAILABLE,
    RESULT_ITEM_UNAVAILABLE,
    RESULT_DEVELOPER_ERROR,
    RESULT_ERROR;

    public static ResponseCode a(int i) {
        ResponseCode[] values = values();
        if (i < 0 || i >= values.length) {
            return RESULT_ERROR;
        }
        return values[i];
    }
}
