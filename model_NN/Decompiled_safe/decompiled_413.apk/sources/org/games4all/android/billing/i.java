package org.games4all.android.billing;

import android.content.Context;

public class i {
    private static b a;

    public static synchronized void a(b bVar) {
        synchronized (i.class) {
            a = bVar;
        }
    }

    public static void a(boolean z) {
        if (a != null) {
            a.a(z);
        }
    }

    public static void a(Context context, PurchaseState purchaseState, String str, int i, String str2, long j, String str3) {
        if (a != null) {
            a.a(purchaseState, str, i, j, str3);
        }
    }
}
