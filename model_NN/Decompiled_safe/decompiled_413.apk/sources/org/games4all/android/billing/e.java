package org.games4all.android.billing;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.games4all.json.h;
import org.games4all.util.c;
import org.games4all.util.f;
import org.games4all.util.j;

public class e {
    private final Context a;
    private List<a> b = new ArrayList();
    private Map<String, Integer> c = new HashMap();

    class a {
        String a;
        String b;
        PurchaseState c;
        long d;
        String e;

        a() {
        }
    }

    public e(Context context) {
        this.a = context;
        b();
    }

    public synchronized int a(String str, String str2, PurchaseState purchaseState, long j, String str3) {
        int intValue;
        Integer num = this.c.get(str2);
        intValue = num == null ? 0 : num.intValue();
        a aVar = new a();
        aVar.a = str;
        aVar.b = str2;
        aVar.c = purchaseState;
        aVar.d = j;
        aVar.e = str3;
        this.b.add(aVar);
        if (purchaseState == PurchaseState.PURCHASED || purchaseState == PurchaseState.REFUNDED) {
            intValue++;
            this.c.put(str2, Integer.valueOf(intValue));
        }
        e();
        return intValue;
    }

    class b implements j {
        b() {
        }

        public String a(String str) {
            if (str.equals("x")) {
                return "93";
            }
            if (str.equals("j")) {
                return "%";
            }
            return "?";
        }
    }

    private String a() {
        org.games4all.util.a aVar = new org.games4all.util.a("v", "t");
        aVar.a(new b());
        String a2 = aVar.a("xvxtjjvjt@11vabj");
        System.err.println("password=" + a2);
        return a2;
    }

    private void b() {
        try {
            c();
            d();
        } catch (Exception e) {
            System.err.println("Warning: couldn't load purchase database: " + e.getMessage());
            e.printStackTrace();
            this.b = new ArrayList();
            this.c = new HashMap();
        }
        throw new UnsupportedOperationException();
    }

    private void c() {
        h hVar = new h();
        InputStreamReader inputStreamReader = new InputStreamReader(this.a.openFileInput("orders.dat"));
        this.b = (List) hVar.a((Reader) inputStreamReader);
        inputStreamReader.close();
    }

    private void d() {
        h hVar = new h();
        FileInputStream openFileInput = this.a.openFileInput("items.dat");
        this.c = (Map) hVar.a(new String(f.b(c.a(openFileInput), a()), "UTF-8"));
        openFileInput.close();
    }

    private void e() {
        try {
            f();
            g();
        } catch (Exception e) {
            System.err.println("Warning: couldn't save purchase database: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void f() {
        h hVar = new h();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.a.openFileOutput("orders.dat", 0));
        hVar.a(this.b, outputStreamWriter);
        outputStreamWriter.close();
    }

    private void g() {
        h hVar = new h();
        byte[] a2 = f.a(hVar.a(this.c).getBytes("UTF-8"), a());
        FileOutputStream openFileOutput = this.a.openFileOutput("items.dat", 0);
        openFileOutput.write(a2);
        openFileOutput.close();
    }
}
