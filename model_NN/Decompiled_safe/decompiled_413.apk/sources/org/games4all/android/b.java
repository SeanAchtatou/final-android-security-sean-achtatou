package org.games4all.android;

import android.os.Build;
import org.games4all.util.l;

public class b extends l {
    public static boolean a = false;
    public static boolean b = false;
    public static final int c = e();

    public static boolean a() {
        return false;
    }

    public static boolean b() {
        return false;
    }

    public static boolean c() {
        return false;
    }

    public static boolean d() {
        return "google_sdk".equals(Build.PRODUCT) || "sdk".equals(Build.PRODUCT);
    }

    private static int e() {
        try {
            return Build.VERSION.class.getField("SDK_INT").getInt(null);
        } catch (Throwable th) {
            return 3;
        }
    }
}
