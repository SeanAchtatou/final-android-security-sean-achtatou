package org.games4all.android.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.flurry.android.u;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import org.games4all.android.b;

public class InstallReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String str;
        String str2;
        String stringExtra = intent.getStringExtra("referrer");
        if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction()) && stringExtra != null) {
            String decode = URLDecoder.decode(stringExtra);
            SharedPreferences.Editor edit = context.getSharedPreferences("install", 0).edit();
            for (String trim : decode.split("&")) {
                String trim2 = trim.trim();
                if (trim2.length() > 0) {
                    int indexOf = trim2.indexOf(61);
                    if (indexOf > 0) {
                        String substring = trim2.substring(0, indexOf);
                        str2 = trim2.substring(indexOf + 1);
                        str = substring;
                    } else {
                        str = trim2;
                        str2 = "TRUE";
                    }
                    edit.putString(str, str2);
                }
            }
            edit.putBoolean("sent", false);
            edit.commit();
            new com.google.ads.InstallReceiver().onReceive(context, intent);
        }
    }

    public static final void a(Context context) {
        if (!b.d()) {
            try {
                SharedPreferences sharedPreferences = context.getSharedPreferences("install", 0);
                Map<String, ?> all = sharedPreferences.getAll();
                if (all.size() != 0 && !sharedPreferences.getBoolean("sent", true)) {
                    System.err.println("sending new install to flurry");
                    HashMap hashMap = new HashMap();
                    for (String next : all.keySet()) {
                        hashMap.put(next, String.valueOf(all.get(next)));
                    }
                    u.b("newInstall", hashMap);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putBoolean("sent", false);
                    edit.commit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
