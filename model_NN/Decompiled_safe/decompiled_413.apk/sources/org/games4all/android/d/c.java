package org.games4all.android.d;

import android.view.View;
import android.widget.ImageButton;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.d;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.play.PlayEvent;
import org.games4all.android.play.PlayState;
import org.games4all.android.play.b;

public class c extends d implements View.OnClickListener {
    private final b a;
    private final ImageButton b = ((ImageButton) findViewById(R.id.g4a_selectLoginNew));
    private final ImageButton c = ((ImageButton) findViewById(R.id.g4a_selectLoginExisting));
    private final ImageButton d = ((ImageButton) findViewById(R.id.g4a_selectLoginOffline));

    public c(Games4AllActivity games4AllActivity, b bVar) {
        super(games4AllActivity, 16973913);
        this.a = bVar;
        setContentView((int) R.layout.g4a_select_login_dialog);
        this.b.setOnClickListener(this);
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
        setCancelable(false);
    }

    public void onClick(View view) {
        if (view == this.b) {
            dismiss();
            this.a.a(PlayState.SELECT_LOGIN, PlayEvent.CREATE_ACCOUNT);
        } else if (view == this.c) {
            dismiss();
            this.a.a(PlayState.SELECT_LOGIN, PlayEvent.LOGIN_EXISTING);
        } else if (view == this.d) {
            dismiss();
            this.a.a(PlayState.SELECT_LOGIN, PlayEvent.GO_OFFLINE);
        }
    }
}
