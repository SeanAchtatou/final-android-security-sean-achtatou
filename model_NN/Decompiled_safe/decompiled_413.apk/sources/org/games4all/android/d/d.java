package org.games4all.android.d;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.InputFilter;
import android.text.LoginFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.lang.reflect.Array;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.play.PlayEvent;
import org.games4all.android.play.PlayState;
import org.games4all.android.play.b;
import org.games4all.gamestore.client.a;

public class d extends org.games4all.android.e.d implements DialogInterface.OnCancelListener, View.OnClickListener {
    private final b a;
    private final a b;
    private final EditText c = ((EditText) findViewById(R.id.g4a_createAccountName));
    private final EditText d = ((EditText) findViewById(R.id.g4a_createAccountPassword));
    private final EditText e = ((EditText) findViewById(R.id.g4a_createAccountEmail));
    private final Button f = ((Button) findViewById(R.id.g4a_createButton));
    private final Button g = ((Button) findViewById(R.id.g4a_cancelButton));
    private Dialog h;

    public d(Games4AllActivity games4AllActivity, b bVar, a aVar) {
        super(games4AllActivity, 16973913);
        this.a = bVar;
        this.b = aVar;
        setContentView((int) R.layout.g4a_create_account);
        this.f.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.c.setFilters(new InputFilter[]{new LoginFilter.UsernameFilterGeneric()});
        this.d.setFilters(new InputFilter[]{new LoginFilter.PasswordFilterGMail()});
        if (a.a != null) {
            this.c.setText(a.a);
        }
        if (a.b != null) {
            this.d.setText(a.b);
        }
        if (a.c != null) {
            this.e.setText(a.c);
        } else {
            this.e.setText(a());
        }
        setOnCancelListener(this);
    }

    private String a() {
        try {
            Object invoke = Class.forName("android.accounts.AccountManager").getMethod("get", Context.class).invoke(null, getContext());
            Object invoke2 = invoke.getClass().getMethod("getAccounts", new Class[0]).invoke(invoke, new Object[0]);
            if (invoke2 != null) {
                int length = Array.getLength(invoke2);
                for (int i = 0; i < length; i++) {
                    Object obj = Array.get(invoke2, i);
                    String str = (String) obj.getClass().getField("name").get(obj);
                    if (str.contains("@")) {
                        return str;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    public void onClick(View view) {
        if (view == this.f) {
            if (this.a.c(PlayState.CREATE_ACCOUNT)) {
                String trim = this.c.getText().toString().trim();
                String trim2 = this.d.getText().toString().trim();
                String trim3 = this.e.getText().toString().trim();
                a.a = trim;
                a.b = trim2;
                a.c = trim3;
                Resources resources = getContext().getResources();
                if (trim.equals("")) {
                    a(resources.getString(R.string.g4a_createAccountMissingName));
                } else if (trim.length() < 4 || trim.length() > 12) {
                    a(resources.getString(R.string.g4a_createAccountNameLength, 4, 12));
                } else if (trim2.equals("")) {
                    a(resources.getString(R.string.g4a_createAccountMissingPassword));
                } else if (trim2.length() < 5 || trim2.length() > 16) {
                    a(resources.getString(R.string.g4a_createAccountPasswordLength, 5, 16));
                } else if (!TextUtils.isEmpty(trim3) && !trim3.contains("@")) {
                    a(resources.getString(R.string.g4a_createAccountInvalidEmail, trim3));
                }
                dismiss();
                new a(this.a, this.b, this.b.b()).a(trim, trim2, trim3, resources.getString(R.string.g4a_createAccountProgressTitle), resources.getString(R.string.g4a_createAccountProgressMessage, trim));
            }
        } else if (view == this.g) {
            cancel();
        }
    }

    private void a(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle((int) R.string.g4a_createAccountError);
        builder.setMessage(str);
        builder.setPositiveButton((int) R.string.g4a_acceptButton, (DialogInterface.OnClickListener) null);
        this.h = builder.show();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.h != null && this.h.isShowing()) {
            this.h.dismiss();
        }
        super.onStop();
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.a.a(PlayState.CREATE_ACCOUNT, PlayEvent.CREATE_CANCELLED);
    }
}
