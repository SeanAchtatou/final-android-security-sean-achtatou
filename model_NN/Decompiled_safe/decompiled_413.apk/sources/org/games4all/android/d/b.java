package org.games4all.android.d;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.InputFilter;
import android.text.LoginFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import java.util.List;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.c;
import org.games4all.android.e.d;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.play.PlayEvent;
import org.games4all.android.play.PlayState;

public class b extends d implements DialogInterface.OnCancelListener, View.OnClickListener, View.OnLongClickListener {
    private final org.games4all.android.play.b a;
    private final org.games4all.gamestore.client.a b;
    private final org.games4all.gamestore.client.b c;
    private final EditText d = ((EditText) findViewById(R.id.g4a_loginExistingName));
    private final EditText e = ((EditText) findViewById(R.id.g4a_loginExistingPassword));
    private final ImageButton f = ((ImageButton) findViewById(R.id.g4a_selectAccount));
    private final Button g = ((Button) findViewById(R.id.g4a_loginButton));
    private final Button h = ((Button) findViewById(R.id.g4a_cancelButton));
    private Dialog i;

    public b(Games4AllActivity games4AllActivity, org.games4all.android.play.b bVar, org.games4all.gamestore.client.a aVar) {
        super(games4AllActivity, 16973913);
        this.a = bVar;
        this.b = aVar;
        setContentView((int) R.layout.g4a_login_existing);
        this.f.setOnClickListener(this);
        this.f.setOnLongClickListener(this);
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
        this.d.setFilters(new InputFilter[]{new LoginFilter.UsernameFilterGeneric()});
        this.e.setFilters(new InputFilter[]{new LoginFilter.PasswordFilterGMail()});
        if (a.a != null) {
            this.d.setText(a.a);
        }
        if (a.b != null) {
            this.e.setText(a.b);
        }
        this.c = games4AllActivity.p().g();
        if (this.c.i().isEmpty()) {
            this.f.setVisibility(4);
        }
        setOnCancelListener(this);
    }

    public void onClick(View view) {
        if (view == this.f) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            List<String> i2 = this.c.i();
            builder.setItems((String[]) i2.toArray(new String[i2.size()]), new C0011b(i2));
            this.i = builder.show();
        } else if (view == this.g) {
            if (this.a.c(PlayState.LOGIN_EXISTING)) {
                String trim = this.d.getText().toString().trim();
                String trim2 = this.e.getText().toString().trim();
                a.a = trim;
                a.b = trim2;
                Resources resources = getContext().getResources();
                if (trim.equals("")) {
                    c(resources.getString(R.string.g4a_createAccountMissingName));
                } else if (trim.length() < 4 || trim.length() > 12) {
                    c(resources.getString(R.string.g4a_createAccountNameLength, 4, 12));
                } else if (trim2.equals("")) {
                    c(resources.getString(R.string.g4a_createAccountMissingPassword));
                } else if (trim2.length() < 5 || trim2.length() > 16) {
                    c(resources.getString(R.string.g4a_createAccountPasswordLength, 5, 16));
                }
                a(trim, trim2);
            }
        } else if (view == this.h) {
            cancel();
        }
    }

    /* renamed from: org.games4all.android.d.b$b  reason: collision with other inner class name */
    class C0011b implements DialogInterface.OnClickListener {
        final /* synthetic */ List a;

        C0011b(List list) {
            this.a = list;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            b.this.a((String) this.a.get(i));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        String b2 = this.c.b(str);
        this.d.setText(str);
        this.e.setText(b2);
    }

    public boolean onLongClick(View view) {
        if (view != this.f) {
            return false;
        }
        String obj = this.d.getText().toString();
        if (this.c.i().contains(obj)) {
            c cVar = new c(c(), 2);
            Resources resources = getContext().getResources();
            cVar.setTitle((int) R.string.g4a_unregisterTitle);
            cVar.b(resources.getString(R.string.g4a_unregisterMessage, obj));
            cVar.a(0, (int) R.string.g4a_unregisterButton);
            cVar.a(1, (int) R.string.g4a_cancelButton);
            cVar.a(new a(obj));
            cVar.show();
        }
        return true;
    }

    class a implements DialogInterface.OnClickListener {
        final /* synthetic */ String a;

        a(String str) {
            this.a = str;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (i == 0) {
                b.this.b(this.a);
            }
            dialogInterface.dismiss();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.c.a(str);
        this.d.setText("");
        this.e.setText("");
    }

    private void a(String str, String str2) {
        dismiss();
        Resources resources = getContext().getResources();
        new a(this.a, this.b, this.b.a()).a(str, str2, "", resources.getString(R.string.g4a_loginProgressTitle), resources.getString(R.string.g4a_loginProgressMessage, str));
    }

    private void c(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle((int) R.string.g4a_loginExistingError);
        builder.setMessage(str);
        builder.setPositiveButton((int) R.string.g4a_acceptButton, (DialogInterface.OnClickListener) null);
        this.i = builder.show();
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.a.a(PlayState.LOGIN_EXISTING, PlayEvent.LOGIN_CANCELLED);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.i != null && this.i.isShowing()) {
            this.i.dismiss();
        }
        super.onStop();
    }
}
