package org.games4all.android.d;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import org.games4all.android.b.e;
import org.games4all.android.e.c;
import org.games4all.android.e.f;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.play.GamePlayActivity;
import org.games4all.android.play.PlayEvent;
import org.games4all.android.play.PlayState;

public class a extends AsyncTask<Void, Void, String> {
    public static String a;
    public static String b;
    public static String c;
    private final org.games4all.android.play.b d;
    private final org.games4all.gamestore.client.a e;
    private f f;
    private final String g;
    private String h;
    private String i;
    private String j;

    public a(org.games4all.android.play.b bVar, org.games4all.gamestore.client.a aVar, String str) {
        org.games4all.b.a.a.a();
        this.d = bVar;
        this.e = aVar;
        this.g = str;
    }

    public void a(String str, String str2, String str3, String str4, String str5) {
        this.h = str;
        this.i = str2;
        this.j = str3;
        this.f = new f(this.d.a());
        this.f.a(str4);
        this.f.b(str5);
        this.f.show();
        execute(new Void[0]);
        this.d.b(PlayEvent.LOGIN_SENT);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Void... voidArr) {
        return this.e.a(this.g, this.h, this.i, this.j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        this.f.dismiss();
        if (str == null) {
            a();
        } else {
            a(this.d, (int) R.string.g4a_loginErrorDialogTitle, str, new C0010a());
        }
    }

    /* renamed from: org.games4all.android.d.a$a  reason: collision with other inner class name */
    class C0010a implements DialogInterface.OnClickListener {
        C0010a() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            a.this.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        this.f.dismiss();
        b();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.d.c(PlayState.LOGGING_IN) || this.d.c(PlayState.CREATING_ACCOUNT)) {
            this.d.b(PlayEvent.LOGGED_IN);
        }
    }

    static void a(org.games4all.android.play.b bVar, int i2, String str, DialogInterface.OnClickListener onClickListener) {
        if (bVar.c(PlayState.LOGGING_IN) || bVar.c(PlayState.CREATING_ACCOUNT)) {
            a(bVar.a(), i2, str, onClickListener);
        }
    }

    public static void a(GamePlayActivity gamePlayActivity, int i2, String str, DialogInterface.OnClickListener onClickListener) {
        String str2;
        int i3;
        String str3;
        b bVar;
        Resources resources = gamePlayActivity.getResources();
        String string = resources.getString(R.string.g4a_acceptButton);
        String str4 = "";
        if (str.startsWith("g4a_")) {
            String b2 = new e(gamePlayActivity).b(str);
            if (str.equals("g4a_oldClientVersion")) {
                str3 = b2;
                str2 = resources.getString(R.string.g4a_updateClient);
                i3 = 2;
                str4 = string;
                bVar = new b(gamePlayActivity, onClickListener);
            } else {
                i3 = 1;
                str3 = b2;
                str2 = string;
                bVar = onClickListener;
            }
        } else {
            str2 = string;
            i3 = 1;
            str3 = str;
            bVar = onClickListener;
        }
        c cVar = new c(gamePlayActivity, i3);
        cVar.a(resources.getString(i2));
        cVar.b(str3);
        cVar.a(0, str2);
        if (i3 > 1) {
            cVar.a(1, str4);
        }
        cVar.a(bVar);
        cVar.show();
    }

    static class b implements DialogInterface.OnClickListener {
        final /* synthetic */ GamePlayActivity a;
        final /* synthetic */ DialogInterface.OnClickListener b;

        b(GamePlayActivity gamePlayActivity, DialogInterface.OnClickListener onClickListener) {
            this.a = gamePlayActivity;
            this.b = onClickListener;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (i == 0) {
                a.a(this.a);
            }
            this.b.onClick(dialogInterface, 0);
        }
    }

    static void a(GamePlayActivity gamePlayActivity) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + gamePlayActivity.getPackageName()));
        intent.setFlags(524288);
        gamePlayActivity.startActivity(intent);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.d.c(PlayState.LOGGING_IN) || this.d.c(PlayState.CREATING_ACCOUNT)) {
            this.d.b(PlayEvent.LOGIN_FAILED);
        }
    }
}
