package org.games4all.android.option;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import java.util.Map;
import org.games4all.android.e.b;
import org.games4all.d.d;
import org.games4all.game.option.c;
import org.games4all.game.option.g;

public class OptionCheckBox implements CompoundButton.OnCheckedChangeListener, b, c {
    static final /* synthetic */ boolean a = (!OptionCheckBox.class.desiredAssertionStatus());
    private final LinearLayout b;
    private final CheckBox c;
    private final TextView d;
    private final g e;

    public interface Translator extends d {
        String label();
    }

    public OptionCheckBox(Context context, g gVar, Map<String, Object> map, Translator translator) {
        this.e = gVar;
        this.b = new LinearLayout(context);
        this.b.setOrientation(0);
        this.c = new CheckBox(context);
        this.d = new TextView(context);
        this.d.setText(translator.label());
        this.d.setTextColor(-1);
        this.d.setTextSize(1, 18.0f);
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 0.0f));
        this.d.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        this.b.addView(this.d);
        this.b.addView(this.c);
        gVar.a((c) this);
        this.c.setOnCheckedChangeListener(this);
        b();
    }

    public View a() {
        return this.b;
    }

    public void b() {
        List b2 = this.e.b();
        if (b2.size() == 1) {
            this.c.setChecked(((Boolean) b2.get(0)).booleanValue());
            this.c.setEnabled(false);
        } else if (a || b2.size() == 2) {
            this.c.setChecked(((Boolean) this.e.c()).booleanValue());
            this.c.setEnabled(true);
        } else {
            throw new AssertionError();
        }
    }

    public void c() {
        this.c.setChecked(((Boolean) this.e.c()).booleanValue());
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.e.a(Boolean.valueOf(z));
    }
}
