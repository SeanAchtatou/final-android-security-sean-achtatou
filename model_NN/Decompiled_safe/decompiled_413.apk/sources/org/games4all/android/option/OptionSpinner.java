package org.games4all.android.option;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.List;
import java.util.Map;
import org.games4all.android.e.b;
import org.games4all.d.d;
import org.games4all.game.option.c;
import org.games4all.game.option.o;

public class OptionSpinner implements AdapterView.OnItemSelectedListener, b, c {
    private final LinearLayout a;
    private final TextView b;
    private final Spinner c;
    private final Context d;
    private final o<?> e;
    private final Translator f;

    public interface Translator extends d {
        String item(String str);

        String label();

        String prompt();
    }

    public OptionSpinner(Context context, o<?> oVar, Map<String, Object> map, Translator translator) {
        this.d = context;
        this.e = oVar;
        this.f = translator;
        this.a = new LinearLayout(context);
        this.a.setOrientation(0);
        this.b = new TextView(context);
        this.b.setText(translator.label());
        this.b.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        this.b.setTextColor(-1);
        this.b.setTextSize(1, 18.0f);
        this.c = new Spinner(context);
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        this.c.setPrompt(translator.prompt());
        this.c.setOnItemSelectedListener(this);
        this.a.addView(this.b);
        this.a.addView(this.c);
        b();
    }

    public View a() {
        return this.a;
    }

    public void b() {
        List<?> b2 = this.e.b();
        int size = b2.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            strArr[i] = this.f.item(b2.get(i).toString());
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.d, 17367048, strArr);
        arrayAdapter.setDropDownViewResource(17367050);
        this.c.setAdapter((SpinnerAdapter) arrayAdapter);
        c();
    }

    public void c() {
        this.c.setSelection(this.e.b().indexOf(this.e.c()));
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        this.e.a(this.e.b().get((int) j));
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
