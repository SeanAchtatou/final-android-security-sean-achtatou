package org.games4all.android.option;

import android.view.View;
import android.widget.ImageButton;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.d;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.option.a;

public class c extends d implements View.OnClickListener {
    private final Games4AllActivity a;
    private final a.C0013a b;
    private final ImageButton c = ((ImageButton) findViewById(R.id.g4a_gameSettingsButton));
    private final ImageButton d = ((ImageButton) findViewById(R.id.g4a_interfaceSettingsButton));

    public c(Games4AllActivity games4AllActivity, a.C0013a aVar) {
        super(games4AllActivity, 16973913);
        this.a = games4AllActivity;
        this.b = aVar;
        setContentView((int) R.layout.g4a_settings_dialog);
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
    }

    public void onClick(View view) {
        if (view == this.c) {
            dismiss();
            new a(this.a, this.b).show();
        } else if (view == this.d) {
            dismiss();
            new b(this.a).show();
        }
    }
}
