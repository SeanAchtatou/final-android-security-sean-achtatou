package org.games4all.android.option;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.d;
import org.games4all.android.games.indianrummy.prod.R;

public class b extends d implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private final ViewGroup a = ((ViewGroup) findViewById(R.id.g4a_animSpeedPanel));
    private final SeekBar b = ((SeekBar) findViewById(R.id.g4a_animSpeedBar));
    private final TextView c = ((TextView) findViewById(R.id.g4a_animSpeedLabel));
    private final ViewGroup d = ((ViewGroup) findViewById(R.id.g4a_roundDelayPanel));
    private final SeekBar e = ((SeekBar) findViewById(R.id.g4a_roundDelayBar));
    private final TextView f = ((TextView) findViewById(R.id.g4a_roundDelayLabel));
    private final ViewGroup g = ((ViewGroup) findViewById(R.id.g4a_gameDelayPanel));
    private final SeekBar h = ((SeekBar) findViewById(R.id.g4a_gameDelayBar));
    private final TextView i = ((TextView) findViewById(R.id.g4a_gameDelayLabel));
    private final Button j = ((Button) findViewById(R.id.g4a_acceptButton));
    private final Button k = ((Button) findViewById(R.id.g4a_cancelButton));
    private final d l;

    public b(Games4AllActivity games4AllActivity) {
        super(games4AllActivity, 16973913);
        setContentView((int) R.layout.g4a_interface_settings_dialog);
        this.b.setOnSeekBarChangeListener(this);
        this.e.setOnSeekBarChangeListener(this);
        this.h.setOnSeekBarChangeListener(this);
        this.j.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.l = ((GameApplication) games4AllActivity.getApplication()).e();
        if (!this.l.a("animationSpeed")) {
            this.a.setVisibility(8);
        }
        if (!this.l.a("roundDelay")) {
            this.d.setVisibility(8);
        }
        if (!this.l.a("gameDelay")) {
            this.g.setVisibility(8);
        }
        this.b.setProgress(this.l.a());
        this.e.setProgress(this.l.b());
        this.h.setProgress(this.l.c());
    }

    public void onClick(View view) {
        if (!f()) {
            return;
        }
        if (view == this.j) {
            dismiss();
            this.l.b(this.b.getProgress());
            this.l.c(this.e.getProgress());
            this.l.d(this.h.getProgress());
            this.l.a(getContext());
        } else if (view == this.k) {
            dismiss();
        }
    }

    public void onProgressChanged(SeekBar seekBar, int i2, boolean z) {
        String string;
        String string2;
        if (seekBar == this.b) {
            this.c.setText(String.valueOf(i2 + 1));
        } else if (seekBar == this.e) {
            Resources resources = getContext().getResources();
            if (i2 == 9) {
                string2 = resources.getString(R.string.g4a_roundDelayWait);
            } else {
                string2 = resources.getString(R.string.g4a_roundDelayValue, String.format("%1.1f", Float.valueOf(((float) i2) / 2.0f)));
            }
            this.f.setText(string2);
        } else if (seekBar == this.h) {
            Resources resources2 = getContext().getResources();
            if (i2 == 9) {
                string = resources2.getString(R.string.g4a_gameDelayWait);
            } else {
                string = resources2.getString(R.string.g4a_gameDelayValue, String.format("%1.1f", Float.valueOf(((float) i2) / 2.0f)));
            }
            this.i.setText(string);
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
