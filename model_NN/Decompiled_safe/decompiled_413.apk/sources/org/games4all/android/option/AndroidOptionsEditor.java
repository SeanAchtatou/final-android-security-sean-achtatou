package org.games4all.android.option;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import java.util.Map;
import org.games4all.android.e.b;
import org.games4all.android.option.OptionCheckBox;
import org.games4all.android.option.OptionSpinner;
import org.games4all.d.d;
import org.games4all.game.option.g;
import org.games4all.game.option.k;
import org.games4all.game.option.m;
import org.games4all.game.option.o;

public class AndroidOptionsEditor implements b {
    public final Object a = new Object();
    public final Object b = new Object();
    private final Context c;
    private final LinearLayout d;
    private final k e;

    public interface Translator extends d {
        OptionCheckBox.Translator booleanEditor(String str);

        OptionSpinner.Translator listEditor(String str);
    }

    public AndroidOptionsEditor(Context context, k kVar, Map<String, Object> map, Translator translator) {
        this.c = context;
        this.e = kVar;
        this.d = new LinearLayout(context);
        this.d.setOrientation(1);
        for (m a2 : kVar.b()) {
            a(a2, map, translator);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.option.AndroidOptionsEditor.a(org.games4all.game.option.o<?>, java.util.Map<java.lang.String, java.lang.Object>, org.games4all.d.d):void
     arg types: [org.games4all.game.option.o, java.util.Map<java.lang.String, java.lang.Object>, org.games4all.android.option.AndroidOptionsEditor$Translator]
     candidates:
      org.games4all.android.option.AndroidOptionsEditor.a(org.games4all.game.option.g, java.util.Map<java.lang.String, java.lang.Object>, org.games4all.android.option.OptionCheckBox$Translator):void
      org.games4all.android.option.AndroidOptionsEditor.a(org.games4all.game.option.m, java.util.Map<java.lang.String, java.lang.Object>, org.games4all.android.option.AndroidOptionsEditor$Translator):void
      org.games4all.android.option.AndroidOptionsEditor.a(org.games4all.game.option.o<?>, java.util.Map<java.lang.String, java.lang.Object>, org.games4all.android.option.OptionSpinner$Translator):void
      org.games4all.android.option.AndroidOptionsEditor.a(org.games4all.game.option.o<?>, java.util.Map<java.lang.String, java.lang.Object>, org.games4all.d.d):void */
    private void a(m mVar, Map<String, Object> map, Translator translator) {
        if (mVar instanceof g) {
            Log.i("G4A", "Adding checkbox: " + mVar.f());
            a((g) mVar, map, translator.booleanEditor(mVar.f()));
        } else if (mVar instanceof o) {
            Log.i("G4A", "Adding list: " + mVar.f());
            if (map.get(mVar.f()) == this.b) {
                a((o<?>) ((o) mVar), map, (d) translator);
            } else {
                a((o<?>) ((o) mVar), map, translator.listEditor(mVar.f()));
            }
        } else {
            throw new RuntimeException("Option editor type not supported: " + mVar);
        }
    }

    private void a(g gVar, Map<String, Object> map, OptionCheckBox.Translator translator) {
        a(new OptionCheckBox(this.c, gVar, map, translator));
    }

    private void a(o<?> oVar, Map<String, Object> map, d dVar) {
        throw new UnsupportedOperationException();
    }

    private void a(o<?> oVar, Map<String, Object> map, OptionSpinner.Translator translator) {
        a(new OptionSpinner(this.c, oVar, map, translator));
    }

    private void a(b bVar) {
        View a2 = bVar.a();
        a2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.d.addView(a2);
    }

    public View a() {
        return this.d;
    }
}
