package org.games4all.android.option;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.HashSet;
import java.util.Set;
import org.games4all.c.c;

public class d {
    private final Set<String> a = new HashSet();
    private int b = 5;
    private int c = 4;
    private int d = 0;
    private int e;
    private int f;
    private int g;
    private final c<a> h = new c<>(a.class);

    public interface a {
        void a();
    }

    public void a(int i) {
        this.d = i;
    }

    public void a(String str, boolean z) {
        if (z) {
            this.a.add(str);
        } else {
            this.a.remove(str);
        }
    }

    public boolean a(String str) {
        return this.a.contains(str);
    }

    public int a() {
        return this.e;
    }

    public int a(int i, int i2) {
        return (((i2 - i) * (9 - this.e)) / 9) + i;
    }

    public void b(int i) {
        this.e = i;
        this.h.c().a();
    }

    public int b() {
        return this.f;
    }

    public void c(int i) {
        this.f = i;
        this.h.c().a();
    }

    public int c() {
        return this.g;
    }

    public int d() {
        if (this.g == 9) {
            return Integer.MAX_VALUE;
        }
        return this.f * 500;
    }

    public void d(int i) {
        this.g = i;
        this.h.c().a();
    }

    public void a(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("preferences", 0).edit();
        edit.putInt("animationSpeed", this.e);
        edit.putInt("roundDelay", this.f);
        edit.putInt("gameDelay", this.g);
        edit.commit();
    }

    public void b(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("preferences", 0);
        this.e = sharedPreferences.getInt("animationSpeed", this.b);
        this.f = sharedPreferences.getInt("roundDelay", this.c);
        this.g = sharedPreferences.getInt("gameDelay", this.d);
    }
}
