package org.games4all.android.option;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import java.util.HashMap;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.d;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.game.option.UnsupportedOptionCombination;
import org.games4all.game.option.e;
import org.games4all.game.option.k;

public class a extends d implements View.OnClickListener {
    private final k a;
    private e b;
    private final C0013a c;
    private final Button d;

    /* renamed from: org.games4all.android.option.a$a  reason: collision with other inner class name */
    public interface C0013a {
        void a(e eVar);
    }

    public a(Games4AllActivity games4AllActivity, C0013a aVar) {
        super(games4AllActivity, 16973913);
        this.c = aVar;
        setContentView((int) R.layout.g4a_game_settings_dialog);
        GameApplication gameApplication = (GameApplication) games4AllActivity.getApplication();
        this.a = gameApplication.p();
        this.b = gameApplication.z().c();
        try {
            this.a.b(this.b);
        } catch (UnsupportedOptionCombination e) {
            Log.e("G4A", "Unsupported option combination: " + e.getMessage());
            this.b = (e) this.a.a();
        }
        View a2 = new AndroidOptionsEditor(games4AllActivity, this.a, new HashMap(), gameApplication.w()).a();
        a2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        ((ViewGroup) findViewById(R.id.g4a_gameOptionsPanel)).addView(a2);
        this.d = (Button) findViewById(R.id.g4a_acceptButton);
        this.d.setOnClickListener(this);
    }

    public void onClick(View view) {
        if (view == this.d && f()) {
            dismiss();
            this.a.a(this.b);
            c().p().a().c("Selected options: " + this.b);
            this.c.a(this.b);
        }
    }
}
