package org.games4all.android.report;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.List;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.game.PlayerInfo;
import org.games4all.game.controller.a.d;
import org.games4all.game.controller.b;
import org.games4all.game.e.a;
import org.games4all.game.e.c;
import org.games4all.game.model.e;

public class ReplayActivity extends Games4AllActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private b a;
    private PlayerInfo b;
    private List<e> c;
    private int d;
    private c e;
    private d f;
    private a g;
    private b h;
    private FrameLayout i;
    private View j;
    private LinearLayout k;
    private LinearLayout l;
    private Button m;
    private Button n;
    private Button o;
    private TextView p;
    private Button q;
    private Button r;
    private Button s;
    private Button t;
    private SeekBar u;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.a = ((GameApplication) getApplication()).t();
        this.i = new FrameLayout(this);
        setContentView(this.i);
        this.k = new LinearLayout(this);
        this.k.setOrientation(1);
        this.k.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        TextView textView = new TextView(this);
        textView.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 1.0f));
        this.l = new LinearLayout(this);
        this.l.setOrientation(0);
        this.l.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.l.setBackgroundDrawable(getResources().getDrawable(R.drawable.g4a_border_darker_inner));
        this.u = new SeekBar(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.leftMargin = 20;
        layoutParams.rightMargin = 20;
        layoutParams.topMargin = 20;
        this.u.setLayoutParams(layoutParams);
        this.u.setOnSeekBarChangeListener(this);
        this.p = new TextView(this);
        this.p.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 0.14f));
        this.p.setTextSize(18.0f);
        this.p.setGravity(17);
        this.p.setTextColor(-1);
        this.m = a("|<");
        this.m.setId(R.id.g4a_replayFirst);
        this.s = a(">|");
        this.s.setId(R.id.g4a_replayLast);
        this.n = a("<<");
        this.n.setId(R.id.g4a_replayPrevPage);
        this.r = a(">>");
        this.r.setId(R.id.g4a_replayNextPage);
        this.o = a("<");
        this.o.setId(R.id.g4a_replayPrevStep);
        this.q = a(">");
        this.q.setId(R.id.g4a_replayNextStep);
        this.l.addView(this.m);
        this.l.addView(this.n);
        this.l.addView(this.o);
        this.l.addView(this.p);
        this.l.addView(this.q);
        this.l.addView(this.r);
        this.l.addView(this.s);
        this.t = new Button(this);
        this.t.setId(R.id.g4a_replayClose);
        this.t.setTextSize(18.0f);
        this.t.setText("X");
        this.t.setOnClickListener(this);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -2, 0.14f);
        layoutParams2.leftMargin = 20;
        this.t.setLayoutParams(layoutParams2);
        this.l.addView(this.t);
        this.k.addView(this.u);
        this.k.addView(textView);
        this.k.addView(this.l);
        this.i.addView(this.k);
        this.c = this.a.n();
        this.d = this.c.size() - 1;
        GameApplication p2 = p();
        this.e = p2.a(this, new org.games4all.a.c());
        this.f = p2.n();
        org.games4all.b.a.b g2 = p2.g().g();
        this.b = new PlayerInfo(g2.b(), g2.a(), false);
        System.err.println("snapshotIndex: " + this.d);
        if (this.d >= 0) {
            a();
        } else {
            d();
        }
    }

    private Button a(String str) {
        Button button = new Button(this);
        button.setTextSize(18.0f);
        button.setText(str);
        button.setOnClickListener(this);
        button.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 0.14f));
        return button;
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        this.j = view;
        view.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.i.addView(view, 0);
    }

    private void b() {
        e eVar = this.c.get(this.d);
        if (this.j != null) {
            this.i.removeView(this.j);
            this.j = null;
        }
        if (this.g != null) {
            this.g.d();
            this.g = null;
        }
        if (this.h != null) {
            this.h.d();
            this.h = null;
        }
        this.g = this.e.a(eVar, 0, this.b);
        this.h = this.f.a(new org.games4all.game.controller.a.b(eVar, 0, this.g));
        this.i.invalidate();
    }

    private void c() {
        this.u.setMax(this.c.size() - 1);
        this.u.setProgress(this.d);
    }

    private void d() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8 = this.d <= 0;
        if (this.d >= this.c.size() - 1) {
            z = true;
        } else {
            z = false;
        }
        Button button = this.m;
        if (!z8) {
            z2 = true;
        } else {
            z2 = false;
        }
        button.setEnabled(z2);
        Button button2 = this.n;
        if (!z8) {
            z3 = true;
        } else {
            z3 = false;
        }
        button2.setEnabled(z3);
        Button button3 = this.o;
        if (!z8) {
            z4 = true;
        } else {
            z4 = false;
        }
        button3.setEnabled(z4);
        Button button4 = this.s;
        if (!z) {
            z5 = true;
        } else {
            z5 = false;
        }
        button4.setEnabled(z5);
        Button button5 = this.r;
        if (!z) {
            z6 = true;
        } else {
            z6 = false;
        }
        button5.setEnabled(z6);
        Button button6 = this.q;
        if (!z) {
            z7 = true;
        } else {
            z7 = false;
        }
        button6.setEnabled(z7);
    }

    private void e() {
        this.p.setText((this.d + 1) + "/" + this.c.size());
    }

    public void onClick(View view) {
        int min;
        if (view == this.t) {
            finish();
            return;
        }
        int size = this.c.size() - 1;
        int min2 = Math.min(4, size / 10);
        int i2 = this.d;
        if (view == this.m) {
            min = 0;
        } else if (view == this.s) {
            min = this.c.size() - 1;
        } else if (view == this.o) {
            min = Math.max(0, this.d - 1);
        } else if (view == this.q) {
            min = Math.min(size, this.d + 1);
        } else if (view == this.n) {
            min = Math.max(0, this.d - min2);
        } else {
            min = view == this.r ? Math.min(size, min2 + this.d) : i2;
        }
        a(min);
    }

    private void a(int i2) {
        if (i2 != this.d) {
            this.d = i2;
            a();
        }
    }

    public void a() {
        e();
        d();
        c();
        b();
    }

    public void onProgressChanged(SeekBar seekBar, int i2, boolean z) {
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        a(seekBar.getProgress());
    }
}
