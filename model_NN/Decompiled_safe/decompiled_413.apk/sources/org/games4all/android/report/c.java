package org.games4all.android.report;

import android.content.Context;
import android.content.pm.PackageManager;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import java.util.List;
import org.games4all.android.GameApplication;
import org.games4all.android.play.GamePlayActivity;
import org.games4all.game.option.e;
import org.games4all.gamestore.client.b;
import org.games4all.json.h;
import org.games4all.json.jsonorg.JSONException;
import org.games4all.logging.LogLevel;

public class c implements Thread.UncaughtExceptionHandler {
    private static final String[] a = {"crash.log", "crash.model", "crash.moves", "crash.image"};
    private final GamePlayActivity b;
    private final Thread.UncaughtExceptionHandler c = Thread.getDefaultUncaughtExceptionHandler();
    private final h d = new h();

    public c(GamePlayActivity gamePlayActivity) {
        this.b = gamePlayActivity;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        try {
            GameApplication p = this.b.p();
            p.a().a(LogLevel.FATAL, "Uncaught exception", th);
            a(th);
            a("crash.log", p.b().b());
            if (p.k() != null) {
                a("crash.model", p.k());
            }
            if (p.i() != null) {
                a("crash.moves", p.i());
            }
            a b2 = this.b.b(2);
            if (b2 != null) {
                a("crash.image", b2.a());
            }
            this.b.j();
            this.c.uncaughtException(thread, th);
        } catch (Exception e) {
            e.initCause(th);
            this.c.uncaughtException(thread, e);
        }
    }

    private void a(String str, Object obj) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.b.openFileOutput(str, 0), "UTF-8");
            this.d.a(obj, outputStreamWriter);
            outputStreamWriter.close();
        } catch (IOException e) {
            System.err.println("WARNING, COULD NOT WRITE " + str + ": " + e.getMessage());
        }
    }

    private void a(String str, byte[] bArr) {
        try {
            FileOutputStream openFileOutput = this.b.openFileOutput(str, 0);
            openFileOutput.write(bArr);
            openFileOutput.close();
        } catch (IOException e) {
            System.err.println("WARNING, COULD NOT WRITE " + str + ": " + e.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.games4all.json.jsonorg.c.a(java.lang.String, long):long
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.String):java.lang.String
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c */
    private void a(Throwable th) {
        String str;
        String valueOf;
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.b.openFileOutput("crash.report", 0), "UTF-8");
            org.games4all.json.jsonorg.c cVar = new org.games4all.json.jsonorg.c();
            GameApplication p = this.b.p();
            b g = p.g();
            String str2 = "";
            if (g.a()) {
                org.games4all.b.a.b g2 = g.g();
                str2 = g2.b();
                str = g2.d();
            } else if (g.d() != null) {
                str2 = g.d();
                str = "";
            } else {
                List<String> i = g.i();
                if (!i.isEmpty()) {
                    str2 = i.get(0);
                    str = "";
                } else {
                    str = "";
                }
            }
            cVar.a("version", (Object) a(this.b));
            cVar.a("category", (Object) "Crash");
            cVar.a("name", (Object) str2);
            cVar.a("email", (Object) str);
            cVar.a("summary", (Object) ("Automatic report after crash: " + th.getMessage()));
            e z = p.z();
            if (z == null) {
                valueOf = "";
            } else {
                valueOf = String.valueOf(z.a());
            }
            cVar.a("variant", (Object) valueOf);
            cVar.b("creation", System.currentTimeMillis());
            cVar.a("package", (Object) this.b.getPackageName());
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            printWriter.close();
            cVar.a("details", (Object) stringWriter.toString());
            cVar.a((Writer) outputStreamWriter);
            outputStreamWriter.close();
        } catch (JSONException e) {
            System.err.println("WARNING, JSON EXCEPTION: " + e.getMessage());
        } catch (IOException e2) {
            System.err.println("WARNING, COULD NOT WRITE crash.report: " + e2.getMessage());
        }
    }

    public void a() {
        String str;
        String str2;
        if (this.b.getFileStreamPath("crash.report").exists()) {
            try {
                FileInputStream openFileInput = this.b.openFileInput("crash.report");
                String b2 = org.games4all.util.c.b(openFileInput);
                System.err.println("SUBMITTING CRASH REPORT");
                openFileInput.close();
                byte[][] bArr = new byte[a.length][];
                for (int i = 0; i < a.length; i++) {
                    str2 = a[i];
                    if (this.b.getFileStreamPath(str2).exists()) {
                        FileInputStream openFileInput2 = this.b.openFileInput(str2);
                        bArr[i] = org.games4all.util.c.a(openFileInput2);
                        openFileInput2.close();
                        this.b.deleteFile(str2);
                    }
                }
                new a("CrashReport", this.b.p().f(), b2, bArr).start();
                this.b.deleteFile("crash.report");
            } catch (Exception e) {
                try {
                    System.err.println("WARNING, EXCEPTION WHEN SUBMITTING CRASH REPORT: " + e.getMessage());
                    e.printStackTrace();
                } finally {
                    str = "crash.report";
                    this.b.deleteFile(str);
                }
            } catch (Throwable th) {
                this.b.deleteFile(str2);
                throw th;
            }
        }
    }

    class a extends Thread {
        final /* synthetic */ org.games4all.gamestore.client.a a;
        final /* synthetic */ String b;
        final /* synthetic */ byte[][] c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        a(String str, org.games4all.gamestore.client.a aVar, String str2, byte[][] bArr) {
            super(str);
            this.a = aVar;
            this.b = str2;
            this.c = bArr;
        }

        public void run() {
            try {
                this.a.a(this.b, this.c[3], this.c[1], this.c[2], this.c[0]);
            } catch (Exception e) {
                System.err.println("CRASH SUBMIT FAILED: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public static String a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}
