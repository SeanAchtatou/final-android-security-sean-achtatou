package org.games4all.android.report;

import java.util.ArrayList;
import java.util.List;
import org.games4all.android.play.GamePlayActivity;
import org.games4all.c.d;
import org.games4all.game.lifecycle.i;
import org.games4all.game.model.c;
import org.games4all.game.model.e;

public class b extends org.games4all.game.lifecycle.a implements i {
    private final org.games4all.gamestore.client.b a;
    private final org.games4all.a.a b;
    private final List<a> c = new ArrayList();
    private final List<e> d = new ArrayList();
    private GamePlayActivity e;
    private e f;
    private final d g = new d();

    public b(org.games4all.gamestore.client.b bVar, org.games4all.game.b.a aVar, org.games4all.a.a aVar2) {
        this.a = bVar;
        this.b = aVar2;
    }

    public void a(GamePlayActivity gamePlayActivity) {
        this.e = gamePlayActivity;
    }

    public List<e> n() {
        return this.d;
    }

    public List<a> o() {
        return this.c;
    }

    public void p() {
        a b2 = this.e.b(2);
        if (b2 != null) {
            this.c.add(b2);
        }
    }

    public void q() {
        this.c.clear();
        this.d.clear();
    }

    /* access modifiers changed from: protected */
    public void c() {
        q();
        r();
    }

    private boolean t() {
        if (this.a.a()) {
            return this.a.g().a(org.games4all.b.a.a.c);
        }
        return false;
    }

    class a implements Runnable {
        a() {
        }

        public void run() {
            b.this.p();
        }
    }

    /* access modifiers changed from: protected */
    public void k() {
        if (t()) {
            this.b.execute(new a());
        }
    }

    /* access modifiers changed from: protected */
    public void m() {
        r();
    }

    public void r() {
        if (t() && this.f != null) {
            this.d.add(c.a(this.f));
        }
    }

    public e s() {
        return this.f;
    }

    public void a(e eVar) {
        q();
        this.g.a();
        this.f = eVar;
        org.games4all.game.model.d m = eVar.m();
        this.g.a(m.a((org.games4all.game.lifecycle.c) this));
        this.g.a(m.a((i) this));
        if (m.k()) {
            r();
        }
    }

    public void a() {
        if (this.d.isEmpty()) {
            r();
        }
    }

    public void b() {
    }
}
