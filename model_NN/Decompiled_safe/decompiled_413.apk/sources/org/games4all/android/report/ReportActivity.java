package org.games4all.android.report;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.game.controller.server.GameSeed;
import org.games4all.game.model.e;
import org.games4all.game.move.PlayerMove;
import org.games4all.json.jsonorg.JSONException;
import org.games4all.json.jsonorg.c;

public class ReportActivity extends Games4AllActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private org.games4all.b.a.b a;
    private LinearLayout b;
    private LinearLayout c;
    private TextView d;
    private EditText e;
    private EditText f;
    private EditText g;
    private EditText h;
    private EditText i;
    private EditText j;
    private CheckBox k;
    private LinearLayout l;
    private Button m;
    private Button n;
    private Button o;
    private b p;
    private a q;
    private a r;
    private Thread s;

    public interface a {
        void a(int i);

        void a(String str);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.g4a_report);
        this.b = (LinearLayout) findViewById(R.id.g4a_reportCredentialsEntry);
        this.c = (LinearLayout) findViewById(R.id.g4a_reportCredentialsDisplay);
        this.e = (EditText) findViewById(R.id.g4a_reportFieldLoginEntry);
        this.f = (EditText) findViewById(R.id.g4a_reportFieldPasswordEntry);
        this.d = (TextView) findViewById(R.id.g4a_reportFieldLogin);
        this.g = (EditText) findViewById(R.id.g4a_reportFieldName);
        this.h = (EditText) findViewById(R.id.g4a_reportFieldEmail);
        this.i = (EditText) findViewById(R.id.g4a_reportFieldSummary);
        this.j = (EditText) findViewById(R.id.g4a_reportFieldDetails);
        this.k = (CheckBox) findViewById(R.id.g4a_reportIncludeState);
        this.l = (LinearLayout) findViewById(R.id.g4a_reportScreenshots);
        this.m = (Button) findViewById(R.id.g4a_reportReviewGame);
        this.n = (Button) findViewById(R.id.g4a_reportSubmitButton);
        this.o = (Button) findViewById(R.id.g4a_cancelButton);
        this.k.setOnCheckedChangeListener(this);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
        this.o.setOnClickListener(this);
        a();
        this.p = ((GameApplication) getApplication()).t();
        List<a> o2 = this.p.o();
        this.q = o2.get(o2.size() - 1);
        a(this.q);
    }

    public void a() {
        org.games4all.gamestore.client.b g2 = ((GameApplication) getApplication()).g();
        this.a = g2.a() ? g2.g() : null;
        SharedPreferences sharedPreferences = getSharedPreferences("report", 0);
        if (this.a != null) {
            this.b.setVisibility(8);
            this.c.setVisibility(0);
            this.d.setText(this.a.b());
        } else {
            this.b.setVisibility(0);
            this.c.setVisibility(8);
            this.e.setText(sharedPreferences.getString("user", ""));
            this.f.setText(sharedPreferences.getString("password", ""));
        }
        this.g.setText(sharedPreferences.getString("name", ""));
        String string = sharedPreferences.getString("email", "");
        if (string.equals("") && this.a != null && (string = this.a.d()) == null) {
            string = "";
        }
        this.h.setText(string);
        this.k.setChecked(true);
    }

    private void a(a aVar) {
        ImageView imageView = new ImageView(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(0, 10, 0, 0);
        imageView.setLayoutParams(layoutParams);
        byte[] a2 = aVar.a();
        imageView.setImageBitmap(BitmapFactory.decodeByteArray(a2, 0, a2.length));
        this.l.addView(imageView);
    }

    public void b() {
        List<List<PlayerMove>> list;
        e eVar;
        byte[] bArr;
        byte[] bArr2;
        boolean isChecked = this.k.isChecked();
        c c2 = c();
        GameSeed d2 = isChecked ? d() : null;
        GameApplication gameApplication = (GameApplication) getApplication();
        if (isChecked) {
            list = a(gameApplication.i());
        } else {
            list = null;
        }
        if (isChecked) {
            eVar = org.games4all.game.model.c.a(this.p.s());
        } else {
            eVar = null;
        }
        if (isChecked) {
            bArr = this.q.a();
        } else {
            bArr = null;
        }
        if (isChecked) {
            bArr2 = gameApplication.b().b();
        } else {
            bArr2 = null;
        }
        String obj = this.a == null ? this.e.getText().toString() : this.a.b();
        String obj2 = this.a == null ? this.f.getText().toString() : this.a.c();
        SharedPreferences.Editor edit = getSharedPreferences("report", 0).edit();
        edit.putString("user", obj);
        edit.putString("password", obj2);
        edit.commit();
        this.s = new Thread(new b(gameApplication, obj, obj2, c2, bArr, d2, eVar, list, bArr2), "ReportSender");
        this.s.start();
        Toast.makeText(this, "Sending report", 1).show();
        finish();
    }

    class b implements Runnable {
        final /* synthetic */ GameApplication a;
        final /* synthetic */ String b;
        final /* synthetic */ String c;
        final /* synthetic */ c d;
        final /* synthetic */ byte[] e;
        final /* synthetic */ GameSeed f;
        final /* synthetic */ e g;
        final /* synthetic */ List h;
        final /* synthetic */ byte[] i;

        b(GameApplication gameApplication, String str, String str2, c cVar, byte[] bArr, GameSeed gameSeed, e eVar, List list, byte[] bArr2) {
            this.a = gameApplication;
            this.b = str;
            this.c = str2;
            this.d = cVar;
            this.e = bArr;
            this.f = gameSeed;
            this.g = eVar;
            this.h = list;
            this.i = bArr2;
        }

        public void run() {
            try {
                ReportActivity.this.a(this.a.f().a(this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i));
            } catch (IOException e2) {
                ReportActivity.this.a(e2.getMessage());
            }
        }
    }

    private List<List<PlayerMove>> a(List<List<PlayerMove>> list) {
        ArrayList arrayList = new ArrayList();
        for (List<PlayerMove> it : list) {
            ArrayList arrayList2 = new ArrayList();
            for (PlayerMove add : it) {
                arrayList2.add(add);
            }
            arrayList.add(arrayList2);
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        Resources resources = getResources();
        a(resources.getString(R.string.g4a_reportOkTitle), resources.getString(R.string.g4a_reportOkMessage, Integer.valueOf(i2)), resources.getString(R.string.g4a_reportOkUrl, Integer.valueOf(i2)));
        if (this.r != null) {
            this.r.a(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        Resources resources = getResources();
        a(resources.getString(R.string.g4a_reportFailTitle), resources.getString(R.string.g4a_reportFailMessage, str), resources.getString(R.string.g4a_reportFailUrl, str));
        if (this.r != null) {
            this.r.a(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, String str3) {
        Notification notification = new Notification(new org.games4all.android.b.e(this).a("drawable", "icon"), str, System.currentTimeMillis());
        notification.flags |= 16;
        notification.setLatestEventInfo(this, str, str2, PendingIntent.getActivity(this, 0, new Intent("android.intent.action.VIEW", Uri.parse(str3)), 0));
        ((NotificationManager) getSystemService("notification")).notify(435683, notification);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.games4all.json.jsonorg.c.a(java.lang.String, long):long
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.String):java.lang.String
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c */
    private c c() {
        c cVar = new c();
        SharedPreferences.Editor edit = getSharedPreferences("report", 0).edit();
        String obj = this.g.getText().toString();
        String obj2 = this.h.getText().toString();
        edit.putString("name", obj);
        edit.putString("email", obj2);
        edit.commit();
        try {
            cVar.a("name", (Object) obj);
            cVar.a("email", (Object) obj2);
            cVar.a("summary", (Object) this.i.getText().toString());
            cVar.a("details", (Object) this.j.getText().toString());
            cVar.a("version", (Object) c.a(this));
            cVar.b("creation", System.currentTimeMillis());
            cVar.a("package", (Object) getPackageName());
            cVar.a("variant", (Object) String.valueOf(((GameApplication) getApplication()).z().a()));
            return cVar;
        } catch (JSONException e2) {
            throw new RuntimeException(e2);
        }
    }

    private GameSeed d() {
        return GameSeed.a(this.p.s());
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (compoundButton == this.k) {
            this.l.setVisibility(z ? 0 : 8);
        }
    }

    public void onClick(View view) {
        if (view == this.m) {
            startActivity(new Intent(this, ReplayActivity.class));
        } else if (view == this.o) {
            finish();
        } else if (view == this.n && e()) {
            b();
            finish();
        }
    }

    private boolean e() {
        if (this.a == null) {
            String obj = this.e.getText().toString();
            String obj2 = this.f.getText().toString();
            if (obj.length() <= 2 || obj2.length() <= 2) {
                org.games4all.android.e.c cVar = new org.games4all.android.e.c(this, 1);
                cVar.setTitle((int) R.string.g4a_reportErrorIncompleteReport);
                cVar.a((int) R.string.g4a_reportErrorNamePassword);
                cVar.a(0, (int) R.string.g4a_acceptButton);
                cVar.show();
                return false;
            }
        }
        return true;
    }
}
