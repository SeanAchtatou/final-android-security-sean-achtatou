package org.games4all.android.c;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.d;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.game.rating.i;
import org.games4all.match.Match;
import org.games4all.match.MatchResult;

public class c extends d implements View.OnClickListener {
    private static boolean h = true;
    private final TextView a = ((TextView) findViewById(R.id.g4a_matchResult));
    private final ViewGroup b;
    private final e c;
    private final ViewGroup d;
    private final a e;
    private final Button f;
    private final Button g;

    public c(Games4AllActivity games4AllActivity, org.games4all.b.a.c cVar, long j, i iVar, MatchResult matchResult, Match match) {
        super(games4AllActivity, 16973913);
        setContentView((int) R.layout.g4a_end_match_dialog);
        GameApplication p = games4AllActivity.p();
        this.a.setText(p.a(games4AllActivity.getResources(), matchResult.c()));
        this.b = (ViewGroup) findViewById(R.id.g4a_endMatchRatingPanel);
        this.c = new e(this.b);
        this.c.a(cVar, j, iVar);
        this.d = (ViewGroup) findViewById(R.id.g4a_endMatchDuplicatePanel);
        this.f = (Button) findViewById(R.id.g4a_closeButton);
        this.f.setOnClickListener(this);
        this.g = (Button) findViewById(R.id.g4a_endMatchDialogSwitchButton);
        boolean a2 = p.g().a();
        if (a2) {
            this.e = new a(this.d);
            this.e.a(matchResult.c(), match.c());
            this.g.setOnClickListener(this);
        } else {
            this.e = null;
            this.g.setVisibility(8);
            findViewById(R.id.g4a_endMatchMiddleFiller).setVisibility(8);
        }
        ((TextView) findViewById(R.id.g4a_ratingPanelTitleLabel)).setText(games4AllActivity.getResources().getString(R.string.g4a_endMatchDialogRatings));
        if (!a2 || h) {
            a();
        } else {
            b();
        }
        if (d.b()) {
            new Handler().postDelayed(new a(), 1000);
        }
    }

    class a implements Runnable {
        a() {
        }

        public void run() {
            c.this.dismiss();
        }
    }

    private void a() {
        System.err.println("ratingMode!");
        this.b.setVisibility(0);
        this.d.setVisibility(4);
        this.g.setText((int) R.string.g4a_endMatchDialogShowDuplicateButton);
        h = true;
    }

    private void b() {
        System.err.println("duplicateMode!");
        this.b.setVisibility(4);
        this.d.setVisibility(0);
        this.g.setText((int) R.string.g4a_endMatchDialogShowRatingsButton);
        h = false;
    }

    public void onClick(View view) {
        if (view == this.g) {
            if (h) {
                b();
            } else {
                a();
            }
        } else if (view == this.f) {
            dismiss();
        }
    }
}
