package org.games4all.android.c;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Map;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.b.a.c;
import org.games4all.game.rating.Rating;
import org.games4all.game.rating.RatingDescriptor;
import org.games4all.game.rating.i;

public class e {
    private final ViewGroup a;
    private final ViewGroup b;

    public e(ViewGroup viewGroup) {
        this.a = viewGroup;
        this.b = (ViewGroup) viewGroup.findViewById(R.id.g4a_ratingPanelRatingList);
    }

    public void a(c cVar, long j, i iVar) {
        if (cVar.a() <= 0) {
            ((TextView) this.a.findViewById(R.id.g4a_ratingPanelColRanking)).setVisibility(8);
        }
        Context context = this.b.getContext();
        Map<Long, Rating> a2 = iVar.a(j, cVar);
        Map<Long, Integer> b2 = iVar.b(j, cVar);
        org.games4all.android.b.e eVar = new org.games4all.android.b.e(context);
        boolean z = true;
        for (RatingDescriptor next : iVar.b(j)) {
            if (z) {
                z = false;
            } else {
                TextView textView = new TextView(context);
                textView.setLayoutParams(new LinearLayout.LayoutParams(-1, eVar.a(1)));
                textView.setBackgroundResource(R.drawable.g4a_rating_row_divider);
                int a3 = eVar.a(3);
                textView.setPadding(0, a3, 0, a3);
                this.b.addView(textView);
            }
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(0);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            linearLayout.addView(a(eVar.b("g4a_rating" + next.c()), 0.4f, 3));
            long a4 = next.a();
            Rating rating = a2.get(Long.valueOf(a4));
            linearLayout.addView(a(String.format(next.g(), Float.valueOf(((float) rating.d()) / 1000000.0f), Float.valueOf(((float) rating.e()) / 1000000.0f)), 0.3f, 17));
            if (cVar.a() > 0) {
                Integer num = b2.get(Long.valueOf(a4));
                linearLayout.addView(a(num == null ? "-" : num.toString(), 0.3f, 17));
            }
            this.b.addView(linearLayout);
        }
    }

    private View a(String str, float f, int i) {
        TextView textView = new TextView(this.b.getContext());
        textView.setLayoutParams(new LinearLayout.LayoutParams(0, -2, f));
        textView.setGravity(i);
        textView.setTextColor(-1);
        textView.setText(str);
        textView.setTextSize(14.0f);
        return textView;
    }
}
