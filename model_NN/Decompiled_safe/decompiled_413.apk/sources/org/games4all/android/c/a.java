package org.games4all.android.c;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.games4all.android.b.e;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.game.rating.ContestResult;
import org.games4all.game.rating.g;
import org.games4all.game.rating.k;

class a {
    private final View a;
    private final LinearLayout b;

    public a(View view) {
        this.a = view;
        this.b = (LinearLayout) view.findViewById(R.id.g4a_rating_dialog_duplicate_list);
    }

    /* access modifiers changed from: package-private */
    public void a(ContestResult contestResult, List<k> list) {
        if (contestResult == null) {
            throw new RuntimeException("missing player result");
        }
        ArrayList arrayList = new ArrayList();
        String string = this.a.getResources().getString(R.string.g4a_player);
        arrayList.addAll(list);
        arrayList.add(new k(0, string, contestResult));
        Collections.sort(arrayList, new g(0));
        a(arrayList, a(arrayList));
    }

    private int[] a(List<k> list) {
        int size = list.size();
        int[] iArr = new int[size];
        ContestResult[] contestResultArr = new ContestResult[size];
        for (int i = 0; i < size; i++) {
            contestResultArr[i] = list.get(i).c();
        }
        for (int i2 = 0; i2 < size; i2++) {
            ContestResult contestResult = contestResultArr[i2];
            int i3 = 0;
            for (int i4 = 0; i4 < size; i4++) {
                if (i2 != i4) {
                    ContestResult contestResult2 = contestResultArr[i4];
                    switch (contestResult.a(contestResult2, 0)) {
                        case TIE:
                            i3++;
                            break;
                        case WIN:
                            i3 += 2;
                            break;
                    }
                }
            }
            iArr[i2] = i3;
        }
        return iArr;
    }

    private void a(List<k> list, int[] iArr) {
        float f;
        Context context = this.a.getContext();
        e eVar = new e(context);
        int size = (list.size() * 2) - 2;
        boolean z = true;
        int i = 0;
        for (k next : list) {
            String b2 = next.b();
            int i2 = iArr[i];
            i++;
            if (size == 0) {
                f = 50.0f;
            } else {
                f = (((float) i2) * 100.0f) / ((float) size);
            }
            if (z) {
                z = false;
            } else {
                TextView textView = new TextView(context);
                textView.setLayoutParams(new LinearLayout.LayoutParams(-1, eVar.a(1)));
                textView.setBackgroundResource(R.drawable.g4a_rating_row_divider);
                int a2 = eVar.a(3);
                textView.setPadding(0, a2, 0, a2);
                this.b.addView(textView);
            }
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(0);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            linearLayout.addView(a(String.valueOf(i), 0.1f, 5));
            linearLayout.addView(a(b2, 0.3f, 17));
            ContestResult c = next.c();
            linearLayout.addView(a(c == null ? "-" : c.toString(), 0.45f, 17));
            linearLayout.addView(a(String.format("%2.1f%%", Float.valueOf(f)), 0.15f, 5));
            if (next.a() == 0) {
                linearLayout.setBackgroundColor(1073807104);
            }
            this.b.addView(linearLayout);
        }
    }

    private View a(String str, float f, int i) {
        TextView textView = new TextView(this.a.getContext());
        textView.setLayoutParams(new LinearLayout.LayoutParams(0, -2, f));
        textView.setGravity(i);
        textView.setTextColor(-1);
        textView.setText(str);
        textView.setTextSize(14.0f);
        return textView;
    }
}
