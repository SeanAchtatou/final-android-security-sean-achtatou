package org.games4all.android.c;

import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.b.e;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.game.rating.ContestResult;
import org.games4all.match.Match;
import org.games4all.match.MatchResult;

public class d extends org.games4all.android.e.d implements View.OnClickListener {
    private static boolean h;
    private static boolean i;
    private final FrameLayout a = ((FrameLayout) findViewById(R.id.g4a_endGamePanelFrame));
    private final View b;
    private final View c;
    private final a d;
    private final Button e;
    private final Button f;
    private final Handler g;

    public d(Games4AllActivity games4AllActivity, MatchResult matchResult, Match match, int i2, boolean z) {
        super(games4AllActivity, 16973913);
        boolean z2;
        setContentView((int) R.layout.g4a_end_game_dialog);
        GameApplication p = games4AllActivity.p();
        this.b = p.a(games4AllActivity).a();
        this.b.setVisibility(8);
        this.a.addView(this.b);
        this.c = findViewById(R.id.g4a_endGameDuplicatePanel);
        this.e = (Button) findViewById(R.id.g4a_closeButton);
        this.e.setOnClickListener(this);
        this.e.setEnabled(false);
        this.g = new Handler();
        this.g.postDelayed(new a(), (long) p.B());
        TextView textView = (TextView) findViewById(R.id.g4a_endGameDialogTitle);
        Resources resources = games4AllActivity.getResources();
        e eVar = new e(games4AllActivity);
        if (z) {
            textView.setText(resources.getString(R.string.g4a_endMatchDialogTitle));
            this.e.setText(resources.getString(R.string.g4a_endGameRatings));
        } else {
            textView.setText(eVar.b("g4a_endGameDialogTitle"));
        }
        this.f = (Button) findViewById(R.id.g4a_endGameDialogSwitchButton);
        ContestResult a2 = matchResult.a(i2);
        if (!p.g().a() || a2 == null) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (z2) {
            this.d = new a(this.c);
            this.d.a(a2, match.a(i2));
            this.f.setOnClickListener(this);
        } else {
            this.d = null;
            this.f.setVisibility(8);
            findViewById(R.id.g4a_endGameMiddleFiller).setVisibility(8);
        }
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.g4a_adContainer);
        if (org.games4all.android.b.a || org.games4all.android.b.c <= 4) {
            linearLayout.setVisibility(8);
        } else if (!org.games4all.android.b.b) {
            try {
                com.google.ads.a aVar = new com.google.ads.a(games4AllActivity, com.google.ads.e.a, (String) games4AllActivity.getPackageManager().getApplicationInfo(games4AllActivity.getComponentName().getPackageName(), 128).metaData.get("ADMOB_PUBLISHER_ID"));
                aVar.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                linearLayout.addView(aVar);
                AdRequest adRequest = new AdRequest();
                adRequest.a(eVar.b("g4a_game"));
                adRequest.a("card");
                adRequest.a("game");
                adRequest.a("kaarten");
                adRequest.a("kaartspel");
                adRequest.a("turnbased");
                adRequest.b("7D71458E88E8D3FEB2C58C9A6AF876B0");
                aVar.a(adRequest);
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
            }
        }
        if (!z2 || !h) {
            g();
        } else {
            h();
        }
        if (i) {
            this.g.postDelayed(new b(), 1000);
        }
    }

    class a implements Runnable {
        a() {
        }

        public void run() {
            d.this.a();
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            d.this.dismiss();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.e.setEnabled(true);
    }

    private void g() {
        this.f.setText((int) R.string.g4a_endGameDialogShowDuplicateButton);
        this.b.setVisibility(0);
        this.c.setVisibility(4);
        h = false;
    }

    private void h() {
        this.f.setText((int) R.string.g4a_endGameDialogShowGameButton);
        this.b.setVisibility(4);
        this.c.setVisibility(0);
        h = true;
    }

    public void onClick(View view) {
        if (view == this.e) {
            dismiss();
        } else if (view != this.f) {
        } else {
            if (h) {
                g();
            } else {
                h();
            }
        }
    }

    public static void a(boolean z) {
        i = z;
    }

    public static boolean b() {
        return i;
    }
}
