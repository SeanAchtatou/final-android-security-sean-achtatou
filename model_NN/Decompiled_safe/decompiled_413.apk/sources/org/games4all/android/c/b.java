package org.games4all.android.c;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.d;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.b.a.c;
import org.games4all.game.rating.i;

public class b extends d implements View.OnClickListener {
    private final e a = new e((LinearLayout) findViewById(R.id.g4a_ratingDialogRatingPanel));
    private final Button b;

    public b(Games4AllActivity games4AllActivity, c cVar, long j, i iVar) {
        super(games4AllActivity, 16973913);
        setContentView((int) R.layout.g4a_rating_dialog);
        this.a.a(cVar, j, iVar);
        this.b = (Button) findViewById(R.id.g4a_closeButton);
        this.b.setOnClickListener(this);
        ((Button) findViewById(R.id.g4a_ratingDialogSwitchButton)).setVisibility(8);
        findViewById(R.id.g4a_ratingDialogMiddleFiller).setVisibility(8);
    }

    public void onClick(View view) {
        if (view == this.b) {
            dismiss();
        }
    }
}
