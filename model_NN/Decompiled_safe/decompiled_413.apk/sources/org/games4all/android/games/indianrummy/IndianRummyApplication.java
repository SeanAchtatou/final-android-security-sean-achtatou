package org.games4all.android.games.indianrummy;

import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.b;
import org.games4all.android.option.AndroidOptionsEditor;
import org.games4all.android.option.d;
import org.games4all.game.d.a;
import org.games4all.game.e.c;
import org.games4all.games.card.indianrummy.IndianRummyVariant;

public class IndianRummyApplication extends GameApplication {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.option.d.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      org.games4all.android.option.d.a(int, int):int
      org.games4all.android.option.d.a(java.lang.String, boolean):void */
    public IndianRummyApplication() {
        d e = e();
        e.a("animationSpeed", true);
        e.a("gameDelay", true);
        e.a(4);
    }

    public Class<? extends Enum<?>> u() {
        return IndianRummyVariant.class;
    }

    public a v() {
        return new org.games4all.games.card.indianrummy.a();
    }

    public c<?> a(Games4AllActivity games4AllActivity, org.games4all.a.a aVar) {
        return new c(games4AllActivity, aVar);
    }

    public b a(Games4AllActivity games4AllActivity) {
        return new b(games4AllActivity);
    }

    public org.games4all.game.move.a x() {
        return new org.games4all.android.games.indianrummy.test.a(this);
    }

    public AndroidOptionsEditor.Translator w() {
        return (AndroidOptionsEditor.Translator) org.games4all.d.b.a(AndroidOptionsEditor.Translator.class, "org.games4all.android.games.indianrummy.options", getResources().getConfiguration().locale);
    }
}
