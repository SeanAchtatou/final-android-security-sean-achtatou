package org.games4all.android.games.indianrummy;

import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.card.e;
import org.games4all.android.card.i;
import org.games4all.android.games.indianrummy.IndianRummyTable;
import org.games4all.android.option.d;
import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.game.PlayerInfo;
import org.games4all.game.move.c;
import org.games4all.games.card.indianrummy.IndianRummyModel;
import org.games4all.games.card.indianrummy.b;
import org.games4all.games.card.indianrummy.human.IndianRummyViewer;

public class a extends i<IndianRummyTable> implements IndianRummyViewer, IndianRummyTable.a {
    public static final String[] a = {"", "Ingrid", "Ruben", "Jasmin"};
    private IndianRummyViewer.a b;
    private final IndianRummyModel c;
    private final b d;

    public a(Games4AllActivity games4AllActivity, d dVar, org.games4all.a.a aVar, IndianRummyModel indianRummyModel) {
        super(games4AllActivity, aVar, new IndianRummyTable(games4AllActivity, aVar));
        ((IndianRummyTable) j()).a(this);
        this.c = indianRummyModel;
        this.d = new b(indianRummyModel);
    }

    public void a(IndianRummyViewer.a aVar) {
        this.b = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.card.e.a(int, java.lang.String, boolean):void
     arg types: [int, java.lang.String, int]
     candidates:
      org.games4all.android.card.e.a(org.games4all.android.card.j, int, android.graphics.Point):void
      org.games4all.android.card.e.a(int, java.lang.String, boolean):void */
    public void a(int i, PlayerInfo playerInfo) {
        e j = j();
        if (i == 0) {
            j.a(i, playerInfo == null ? "-" : playerInfo.a(), false);
        } else {
            j.a(i, a[i], true);
        }
    }

    public void a(boolean z) {
        ((IndianRummyTable) j()).c();
        super.a(z);
    }

    /* access modifiers changed from: protected */
    public void b() {
        IndianRummyTable indianRummyTable = (IndianRummyTable) j();
        indianRummyTable.o();
        Cards d2 = this.c.d(0);
        int h = this.c.h(0);
        IndianRummyViewer.HandState b2 = this.d.b(d2);
        Cards d3 = this.c.d(1);
        int h2 = this.c.h(1);
        IndianRummyViewer.HandState b3 = this.d.b(d3);
        Cards d4 = this.c.d(2);
        int h3 = this.c.h(2);
        IndianRummyViewer.HandState b4 = this.d.b(d4);
        Cards d5 = this.c.d(3);
        indianRummyTable.a(d2, h, b2, d3, h2, b3, d4, h3, b4, d5, this.c.h(3), this.d.b(d5));
    }

    /* access modifiers changed from: protected */
    public void c() {
        ((IndianRummyTable) j()).d();
        this.b.a();
    }

    public void a(int i) {
        ((IndianRummyTable) j()).b(i);
    }

    public void a(Cards cards) {
        ((IndianRummyTable) j()).a(cards);
    }

    public void a(int i, Cards cards) {
        ((IndianRummyTable) j()).b(i, cards);
    }

    public void a(int i, int i2, IndianRummyViewer.HandState handState) {
        ((IndianRummyTable) j()).a(i, i2, handState);
    }

    public void b(int i) {
        ((IndianRummyTable) j()).a(i);
    }

    public void d() {
        super.d();
    }

    public c a(int i, boolean z, int i2) {
        Card card;
        IndianRummyViewer.HandState handState;
        int i3;
        IndianRummyTable indianRummyTable = (IndianRummyTable) j();
        IndianRummyViewer.HandState handState2 = IndianRummyViewer.HandState.UNKNOWN;
        if (i == 0) {
            Cards e = this.c.e(i);
            Card a2 = e.a(i2);
            int i4 = this.c.i(i);
            handState = this.d.b(e);
            i3 = i4;
            card = a2;
        } else if (z) {
            card = indianRummyTable.e();
            handState = handState2;
            i3 = 0;
        } else {
            card = Card.a;
            handState = handState2;
            i3 = 0;
        }
        indianRummyTable.a(i, z, card, i2, i3, handState);
        return null;
    }

    public c a(int i, int i2, Card card) {
        IndianRummyViewer.HandState handState;
        int i3 = 0;
        IndianRummyTable indianRummyTable = (IndianRummyTable) j();
        IndianRummyViewer.HandState handState2 = IndianRummyViewer.HandState.UNKNOWN;
        if (i == 0) {
            int i4 = this.c.i(i);
            handState = this.d.b(this.c.e(0));
            i3 = i4;
        } else {
            handState = handState2;
        }
        indianRummyTable.a(i, card, i2, i3, handState);
        return null;
    }

    public c b(int i, Cards cards) {
        if (i != 0) {
            return null;
        }
        IndianRummyTable indianRummyTable = (IndianRummyTable) j();
        indianRummyTable.b(i, cards);
        indianRummyTable.a(i, this.c.i(i), this.d.b(cards));
        indianRummyTable.invalidate();
        return null;
    }

    public c a(int i, Card card, int i2, int i3) {
        if (i != 0) {
            return null;
        }
        ((IndianRummyTable) j()).a(i, card, i2, i3, this.c.i(i), this.d.b(this.c.e(0)));
        return null;
    }

    public c c(int i) {
        return null;
    }

    public boolean a(boolean z, int i) {
        return this.b.a(z, i);
    }

    public boolean a(Card card, int i) {
        return this.b.a(card, i);
    }

    public boolean a(Card card, int i, int i2) {
        return this.b.a(card, i, i2);
    }

    public boolean e() {
        return this.b.b();
    }

    public void f() {
        ((IndianRummyTable) j()).a();
    }

    /* renamed from: org.games4all.android.games.indianrummy.a$a  reason: collision with other inner class name */
    class C0012a implements Runnable {
        C0012a() {
        }

        public void run() {
            ((IndianRummyTable) a.this.j()).b();
        }
    }

    public void g() {
        h().execute(new C0012a());
    }

    public void c(int i, Cards cards) {
        ((IndianRummyTable) j()).a(i, cards);
    }
}
