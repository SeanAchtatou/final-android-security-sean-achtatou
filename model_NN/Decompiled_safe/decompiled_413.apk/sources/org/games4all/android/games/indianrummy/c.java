package org.games4all.android.games.indianrummy;

import org.games4all.a.a;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.game.PlayerInfo;
import org.games4all.games.card.indianrummy.IndianRummyModel;

public class c implements org.games4all.game.e.c<IndianRummyModel> {
    static final /* synthetic */ boolean a = (!c.class.desiredAssertionStatus());
    private final Games4AllActivity b;
    private final a c;

    public c(Games4AllActivity games4AllActivity, a aVar) {
        this.b = games4AllActivity;
        this.c = aVar;
    }

    public org.games4all.game.e.a a(IndianRummyModel indianRummyModel, int i, PlayerInfo playerInfo) {
        if (a || i == 0) {
            a aVar = new a(this.b, this.b.p().e(), this.c, indianRummyModel);
            this.b.a(aVar);
            return aVar;
        }
        throw new AssertionError();
    }
}
