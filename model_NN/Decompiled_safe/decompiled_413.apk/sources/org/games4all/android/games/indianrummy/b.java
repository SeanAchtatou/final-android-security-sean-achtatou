package org.games4all.android.games.indianrummy;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.f.a;
import org.games4all.android.f.c;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.games.card.indianrummy.IndianRummyModel;

public class b implements org.games4all.android.e.b, org.games4all.android.f.b {
    private final IndianRummyModel a;
    private final LinearLayout b;
    private final c c;

    public b(Games4AllActivity games4AllActivity) {
        this.a = (IndianRummyModel) games4AllActivity.p().k();
        this.b = new LinearLayout(games4AllActivity);
        this.b.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.b.setOrientation(1);
        this.b.setGravity(17);
        this.c = new c(games4AllActivity);
        this.c.a(new a(games4AllActivity));
        this.c.a(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2, 1.0f);
        layoutParams.leftMargin = 10;
        this.c.setLayoutParams(layoutParams);
        this.b.addView(this.c);
    }

    public View a() {
        return this.b;
    }

    public int b() {
        return 4;
    }

    public int a(int i) {
        return 2;
    }

    public float c() {
        return 400.0f;
    }

    public float a(int i, int i2) {
        int h = this.a.h(i);
        if (i2 == 0) {
            return (float) (this.a.j(i) - h);
        }
        return (float) h;
    }

    public String b(int i, int i2) {
        if (i2 == 1) {
            return String.valueOf(this.a.j(i));
        }
        return null;
    }

    public String b(int i) {
        return "+" + String.valueOf(this.a.h(i));
    }

    public String c(int i) {
        switch (i) {
            case 0:
                return this.b.getContext().getResources().getString(R.string.g4a_player);
            case 1:
            case 2:
            case 3:
                return a.a[i];
            default:
                throw new RuntimeException(String.valueOf(i));
        }
    }
}
