package org.games4all.android.games.indianrummy.test;

import android.graphics.Point;
import org.games4all.android.card.g;
import org.games4all.android.games.indianrummy.IndianRummyApplication;
import org.games4all.android.games.indianrummy.IndianRummyTable;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.game.move.MoveFailed;
import org.games4all.game.move.c;

public class a implements org.games4all.games.card.indianrummy.move.a {
    private final org.games4all.android.test.a a;

    public a(IndianRummyApplication indianRummyApplication) {
        this.a = indianRummyApplication.d();
    }

    public c a(int i, boolean z, int i2) {
        Point g;
        Point f;
        IndianRummyTable indianRummyTable = (IndianRummyTable) ((org.games4all.android.games.indianrummy.a) this.a.v()).j();
        if (z) {
            g = indianRummyTable.f();
        } else {
            g = indianRummyTable.g();
        }
        if (i2 == 13) {
            f = new Point(g);
            f.y += indianRummyTable.m().c() / 2;
        } else {
            f = indianRummyTable.a(i, i2).f();
        }
        this.a.b(indianRummyTable, g);
        this.a.d(indianRummyTable, f);
        this.a.c(indianRummyTable, f);
        System.err.println("take from " + g + " to " + f);
        return null;
    }

    public c a(int i, int i2, Card card) {
        IndianRummyTable indianRummyTable = (IndianRummyTable) ((org.games4all.android.games.indianrummy.a) this.a.v()).j();
        g a2 = indianRummyTable.a(i, i2);
        Point f = a2.f();
        f.x = a2.d().x;
        Point f2 = indianRummyTable.f();
        System.err.println("discard from " + f + " to " + f2);
        this.a.b(indianRummyTable, f);
        this.a.d(indianRummyTable, f2);
        this.a.c(indianRummyTable, f2);
        return null;
    }

    public c b(int i, Cards cards) {
        return new MoveFailed("not implemented");
    }

    public c a(int i, Card card, int i2, int i3) {
        throw new UnsupportedOperationException();
    }

    public c c(int i) {
        this.a.e(R.id.done_button);
        return null;
    }
}
