package org.games4all.android.games.indianrummy.prod;

/* This class is generated by JADX */
public final class R {

    public static final class drawable {
        public static final int back_107x150 = 2130837504;
        public static final int back_167x242 = 2130837505;
        public static final int back_43x64 = 2130837506;
        public static final int back_67x91 = 2130837507;
        public static final int deck_107x150 = 2130837508;
        public static final int deck_167x242 = 2130837509;
        public static final int deck_43x64 = 2130837510;
        public static final int deck_67x91 = 2130837511;
        public static final int done_button = 2130837512;
        public static final int done_pressed = 2130837513;
        public static final int done_unpressed = 2130837514;
        public static final int empty_107x150 = 2130837515;
        public static final int empty_167x242 = 2130837516;
        public static final int empty_43x64 = 2130837517;
        public static final int empty_67x91 = 2130837518;
        public static final int g4a_arrow_up = 2130837519;
        public static final int g4a_arrow_up_2 = 2130837520;
        public static final int g4a_border_dark_inner = 2130837521;
        public static final int g4a_border_darker_inner = 2130837522;
        public static final int g4a_border_tab_content = 2130837523;
        public static final int g4a_button_connected = 2130837524;
        public static final int g4a_button_disconnected = 2130837525;
        public static final int g4a_button_info = 2130837526;
        public static final int g4a_button_menu_close = 2130837527;
        public static final int g4a_button_menu_open = 2130837528;
        public static final int g4a_button_rankings = 2130837529;
        public static final int g4a_button_reset_match = 2130837530;
        public static final int g4a_button_settings = 2130837531;
        public static final int g4a_button_shopping = 2130837532;
        public static final int g4a_cart_down = 2130837533;
        public static final int g4a_cart_up = 2130837534;
        public static final int g4a_connected_down = 2130837535;
        public static final int g4a_connected_up = 2130837536;
        public static final int g4a_create_account = 2130837537;
        public static final int g4a_disconnected_down = 2130837538;
        public static final int g4a_disconnected_up = 2130837539;
        public static final int g4a_divider_gradient = 2130837540;
        public static final int g4a_droid = 2130837541;
        public static final int g4a_gameplay_settings = 2130837542;
        public static final int g4a_hand_label_0_active = 2130837543;
        public static final int g4a_hand_label_0_inactive = 2130837544;
        public static final int g4a_info_down = 2130837545;
        public static final int g4a_info_up = 2130837546;
        public static final int g4a_interface_settings = 2130837547;
        public static final int g4a_light = 2130837548;
        public static final int g4a_login_existing = 2130837549;
        public static final int g4a_menu_close_down = 2130837550;
        public static final int g4a_menu_close_up = 2130837551;
        public static final int g4a_menu_open_down = 2130837552;
        public static final int g4a_menu_open_up = 2130837553;
        public static final int g4a_name_frame = 2130837554;
        public static final int g4a_name_frame_active = 2130837555;
        public static final int g4a_name_frame_active_2 = 2130837556;
        public static final int g4a_name_frame_inactive = 2130837557;
        public static final int g4a_name_frame_inactive_2 = 2130837558;
        public static final int g4a_offline = 2130837559;
        public static final int g4a_rankings_down = 2130837560;
        public static final int g4a_rankings_up = 2130837561;
        public static final int g4a_rating_row_divider = 2130837562;
        public static final int g4a_rating_title_row_divider = 2130837563;
        public static final int g4a_reset_match_down = 2130837564;
        public static final int g4a_reset_match_up = 2130837565;
        public static final int g4a_select_account = 2130837566;
        public static final int g4a_settings_down = 2130837567;
        public static final int g4a_settings_up = 2130837568;
        public static final int g4a_suit_clubs_icon = 2130837569;
        public static final int g4a_suit_clubs_large = 2130837570;
        public static final int g4a_suit_diamonds_icon = 2130837571;
        public static final int g4a_suit_diamonds_large = 2130837572;
        public static final int g4a_suit_hearts_icon = 2130837573;
        public static final int g4a_suit_hearts_large = 2130837574;
        public static final int g4a_suit_spades_icon = 2130837575;
        public static final int g4a_suit_spades_large = 2130837576;
        public static final int icon = 2130837577;
        public static final int joker_107x150 = 2130837578;
        public static final int joker_167x242 = 2130837579;
        public static final int joker_43x64 = 2130837580;
        public static final int joker_67x91 = 2130837581;
        public static final int label_frame_nothing_0 = 2130837582;
        public static final int label_frame_nothing_0_active = 2130837583;
        public static final int label_frame_nothing_0_inactive = 2130837584;
        public static final int label_frame_nothing_1 = 2130837585;
        public static final int label_frame_nothing_1_active = 2130837586;
        public static final int label_frame_nothing_1_inactive = 2130837587;
        public static final int label_frame_nothing_2 = 2130837588;
        public static final int label_frame_nothing_2_active = 2130837589;
        public static final int label_frame_nothing_2_inactive = 2130837590;
        public static final int label_frame_nothing_3 = 2130837591;
        public static final int label_frame_nothing_3_active = 2130837592;
        public static final int label_frame_nothing_3_inactive = 2130837593;
        public static final int label_frame_second_life_0 = 2130837594;
        public static final int label_frame_second_life_0_active = 2130837595;
        public static final int label_frame_second_life_0_inactive = 2130837596;
        public static final int label_frame_second_life_1 = 2130837597;
        public static final int label_frame_second_life_1_active = 2130837598;
        public static final int label_frame_second_life_1_inactive = 2130837599;
        public static final int label_frame_second_life_2 = 2130837600;
        public static final int label_frame_second_life_2_active = 2130837601;
        public static final int label_frame_second_life_2_inactive = 2130837602;
        public static final int label_frame_second_life_3 = 2130837603;
        public static final int label_frame_second_life_3_active = 2130837604;
        public static final int label_frame_second_life_3_inactive = 2130837605;
        public static final int shadow_107x150 = 2130837606;
        public static final int shadow_167x242 = 2130837607;
        public static final int shadow_43x64 = 2130837608;
        public static final int shadow_67x91 = 2130837609;
    }

    public static final class layout {
        public static final int g4a_about_dialog = 2130903040;
        public static final int g4a_alert_dialog = 2130903041;
        public static final int g4a_create_account = 2130903042;
        public static final int g4a_duplicate_panel = 2130903043;
        public static final int g4a_end_game_dialog = 2130903044;
        public static final int g4a_end_match_dialog = 2130903045;
        public static final int g4a_game_settings_dialog = 2130903046;
        public static final int g4a_help = 2130903047;
        public static final int g4a_hint = 2130903048;
        public static final int g4a_interface_settings_dialog = 2130903049;
        public static final int g4a_login_existing = 2130903050;
        public static final int g4a_login_progress = 2130903051;
        public static final int g4a_progress_dialog = 2130903052;
        public static final int g4a_rating_dialog = 2130903053;
        public static final int g4a_rating_panel = 2130903054;
        public static final int g4a_report = 2130903055;
        public static final int g4a_select_login_dialog = 2130903056;
        public static final int g4a_select_suit_dialog = 2130903057;
        public static final int g4a_settings_dialog = 2130903058;
        public static final int g4a_tooltip = 2130903059;
    }

    public static final class color {
        public static final int hand_unknown = 2130968576;
        public static final int hand_nothing = 2130968577;
        public static final int hand_first_life = 2130968578;
        public static final int hand_second_life = 2130968579;
    }

    public static final class integer {
        public static final int DIST = 2131034112;
    }

    public static final class dimen {
        public static final int g4a_nameLabelSize = 2131099648;
        public static final int g4a_selectSuitDialogButtonWidth = 2131099649;
    }

    public static final class id {
        public static final int g4a_menuToggle = 2131165184;
        public static final int g4a_menuAccount = 2131165185;
        public static final int g4a_menuResetMatch = 2131165186;
        public static final int g4a_menuRatings = 2131165187;
        public static final int g4a_menuSettings = 2131165188;
        public static final int g4a_menuShopping = 2131165189;
        public static final int g4a_menuInfo = 2131165190;
        public static final int g4a_dialogButton0 = 2131165191;
        public static final int g4a_dialogButton1 = 2131165192;
        public static final int g4a_dialogButton2 = 2131165193;
        public static final int g4a_dialogButton3 = 2131165194;
        public static final int g4a_loadGameDialogInput = 2131165195;
        public static final int g4a_loadGameDialog = 2131165196;
        public static final int g4a_loadGameFailedDialog = 2131165197;
        public static final int g4a_replayFirst = 2131165198;
        public static final int g4a_replayPrevPage = 2131165199;
        public static final int g4a_replayPrevStep = 2131165200;
        public static final int g4a_replayNextStep = 2131165201;
        public static final int g4a_replayNextPage = 2131165202;
        public static final int g4a_replayLast = 2131165203;
        public static final int g4a_replayClose = 2131165204;
        public static final int done_button = 2131165205;
        public static final int g4a_aboutDialogIcon = 2131165206;
        public static final int g4a_aboutDialogTitle = 2131165207;
        public static final int g4a_aboutDialogVersion = 2131165208;
        public static final int g4a_helpButton = 2131165209;
        public static final int g4a_closeButton = 2131165210;
        public static final int g4a_alertDialogTitle = 2131165211;
        public static final int g4a_alertDialogMessage = 2131165212;
        public static final int g4a_alertDialogContent = 2131165213;
        public static final int g4a_alertDialogButtons = 2131165214;
        public static final int g4a_createAccountDialog = 2131165215;
        public static final int g4a_createAccountName = 2131165216;
        public static final int g4a_createAccountPassword = 2131165217;
        public static final int g4a_createAccountEmail = 2131165218;
        public static final int g4a_createButton = 2131165219;
        public static final int g4a_cancelButton = 2131165220;
        public static final int g4a_rating_dialog_duplicate_list = 2131165221;
        public static final int g4a_endGameDialogTitle = 2131165222;
        public static final int g4a_endGamePanelFrame = 2131165223;
        public static final int g4a_endGameDuplicatePanel = 2131165224;
        public static final int g4a_adContainer = 2131165225;
        public static final int g4a_endGameMiddleFiller = 2131165226;
        public static final int g4a_endGameDialogSwitchButton = 2131165227;
        public static final int g4a_matchResult = 2131165228;
        public static final int g4a_endMatchDialogFrame = 2131165229;
        public static final int g4a_endMatchRatingPanel = 2131165230;
        public static final int g4a_endMatchDuplicatePanel = 2131165231;
        public static final int g4a_endMatchMiddleFiller = 2131165232;
        public static final int g4a_endMatchDialogSwitchButton = 2131165233;
        public static final int g4a_gameOptionsPanel = 2131165234;
        public static final int g4a_acceptButton = 2131165235;
        public static final int g4a_helpTitle = 2131165236;
        public static final int g4a_helpContent = 2131165237;
        public static final int g4a_tooltipTitle = 2131165238;
        public static final int g4a_tooltipContent = 2131165239;
        public static final int g4a_noShowBox = 2131165240;
        public static final int g4a_interfaceOptionsPanel = 2131165241;
        public static final int g4a_animSpeedPanel = 2131165242;
        public static final int g4a_animSpeedBar = 2131165243;
        public static final int g4a_animSpeedLabel = 2131165244;
        public static final int g4a_roundDelayPanel = 2131165245;
        public static final int g4a_roundDelayBar = 2131165246;
        public static final int g4a_roundDelayLabel = 2131165247;
        public static final int g4a_gameDelayPanel = 2131165248;
        public static final int g4a_gameDelayBar = 2131165249;
        public static final int g4a_gameDelayLabel = 2131165250;
        public static final int g4a_loginExistingName = 2131165251;
        public static final int g4a_selectAccount = 2131165252;
        public static final int g4a_loginExistingPassword = 2131165253;
        public static final int g4a_loginButton = 2131165254;
        public static final int g4a_progressDialogTitle = 2131165255;
        public static final int g4a_progressDialogProgress = 2131165256;
        public static final int g4a_progressDialogMessage = 2131165257;
        public static final int g4a_ratingDialogContent = 2131165258;
        public static final int g4a_ratingDialogRatingPanel = 2131165259;
        public static final int g4a_ratingDialogMiddleFiller = 2131165260;
        public static final int g4a_ratingDialogSwitchButton = 2131165261;
        public static final int g4a_ratingPanelTitleLabel = 2131165262;
        public static final int g4a_ratingPanelColRanking = 2131165263;
        public static final int g4a_ratingPanelRatingList = 2131165264;
        public static final int g4a_reportForm = 2131165265;
        public static final int g4a_reportCredentialsDisplay = 2131165266;
        public static final int g4a_reportFieldLogin = 2131165267;
        public static final int g4a_reportCredentialsEntry = 2131165268;
        public static final int g4a_reportFieldLoginEntry = 2131165269;
        public static final int g4a_reportFieldPasswordEntry = 2131165270;
        public static final int g4a_reportFieldName = 2131165271;
        public static final int g4a_reportFieldEmail = 2131165272;
        public static final int g4a_reportFieldSummary = 2131165273;
        public static final int g4a_reportFieldDetails = 2131165274;
        public static final int g4a_reportIncludeState = 2131165275;
        public static final int g4a_reportScreenshots = 2131165276;
        public static final int g4a_reportReviewGame = 2131165277;
        public static final int g4a_reportSubmitButton = 2131165278;
        public static final int g4a_selectLoginDialog = 2131165279;
        public static final int g4a_selectLoginNew = 2131165280;
        public static final int g4a_selectLoginExisting = 2131165281;
        public static final int g4a_selectLoginOffline = 2131165282;
        public static final int selectTrumpDialog = 2131165283;
        public static final int g4a_selectSuitTitle = 2131165284;
        public static final int g4a_selectSpadesButton = 2131165285;
        public static final int g4a_selectHeartsButton = 2131165286;
        public static final int g4a_selectClubsButton = 2131165287;
        public static final int g4a_selectDiamondsButton = 2131165288;
        public static final int g4a_gameSettingsButton = 2131165289;
        public static final int g4a_interfaceSettingsButton = 2131165290;
    }

    public static final class string {
        public static final int games4allLabel = 2131230720;
        public static final int g4a_player = 2131230721;
        public static final int g4a_menuReport = 2131230722;
        public static final int g4a_menuLoad = 2131230723;
        public static final int g4a_menuReset = 2131230724;
        public static final int g4a_menuError = 2131230725;
        public static final int g4a_menuScenarioStart = 2131230726;
        public static final int g4a_menuScenarioStop = 2131230727;
        public static final int g4a_aboutDialogTitle = 2131230728;
        public static final int g4a_aboutDialogHelp = 2131230729;
        public static final int g4a_version = 2131230730;
        public static final int g4a_settingsDialogTitle = 2131230731;
        public static final int g4a_gameSettingsLabel = 2131230732;
        public static final int g4a_interfaceSettingsLabel = 2131230733;
        public static final int g4a_gameSettingsDialogTitle = 2131230734;
        public static final int g4a_animSpeedLabel = 2131230735;
        public static final int g4a_roundDelayLabel = 2131230736;
        public static final int g4a_roundDelayWait = 2131230737;
        public static final int g4a_roundDelayValue = 2131230738;
        public static final int g4a_gameDelayLabel = 2131230739;
        public static final int g4a_gameDelayWait = 2131230740;
        public static final int g4a_gameDelayValue = 2131230741;
        public static final int g4a_interfaceSettingsDialogTitle = 2131230742;
        public static final int g4a_acceptButton = 2131230743;
        public static final int g4a_cancelButton = 2131230744;
        public static final int g4a_closeButton = 2131230745;
        public static final int g4a_continueButton = 2131230746;
        public static final int g4a_selectLoginDialogTitle = 2131230747;
        public static final int g4a_selectLoginNew = 2131230748;
        public static final int g4a_selectLoginExisting = 2131230749;
        public static final int g4a_selectLoginOffline = 2131230750;
        public static final int g4a_createAccountTitle = 2131230751;
        public static final int g4a_createAccountNameLabel = 2131230752;
        public static final int g4a_createAccountPasswordLabel = 2131230753;
        public static final int g4a_createAccountEmailLabel = 2131230754;
        public static final int g4a_createAccountEmailOptionalLabel = 2131230755;
        public static final int g4a_createAccountButton = 2131230756;
        public static final int g4a_createAccountError = 2131230757;
        public static final int g4a_createAccountMissingName = 2131230758;
        public static final int g4a_createAccountNameLength = 2131230759;
        public static final int g4a_createAccountMissingPassword = 2131230760;
        public static final int g4a_createAccountPasswordLength = 2131230761;
        public static final int g4a_createAccountInvalidEmail = 2131230762;
        public static final int g4a_createAccountServerProblem = 2131230763;
        public static final int g4a_createAccountConnectionProblem = 2131230764;
        public static final int g4a_createAccountProgressTitle = 2131230765;
        public static final int g4a_createAccountProgressMessage = 2131230766;
        public static final int g4a_loginExistingTitle = 2131230767;
        public static final int g4a_loginExistingNameLabel = 2131230768;
        public static final int g4a_loginExistingPasswordLabel = 2131230769;
        public static final int g4a_loginExistingButton = 2131230770;
        public static final int g4a_loginExistingError = 2131230771;
        public static final int g4a_unregisterTitle = 2131230772;
        public static final int g4a_unregisterMessage = 2131230773;
        public static final int g4a_unregisterButton = 2131230774;
        public static final int g4a_loginProgressTitle = 2131230775;
        public static final int g4a_loginProgressMessage = 2131230776;
        public static final int g4a_loginErrorDialogTitle = 2131230777;
        public static final int g4a_logoutDialogTitle = 2131230778;
        public static final int g4a_logoutDialogMessage = 2131230779;
        public static final int g4a_logoutDialogAccept = 2131230780;
        public static final int g4a_logoutDialogCancel = 2131230781;
        public static final int g4a_loadMatchProgressDialogTitle = 2131230782;
        public static final int g4a_loadMatchProgressDialogMessage = 2131230783;
        public static final int g4a_loadMatchCancelled = 2131230784;
        public static final int g4a_loadMatchErrorTitle = 2131230785;
        public static final int g4a_loadMatchErrorMsg = 2131230786;
        public static final int g4a_loadMatchErrorRetry = 2131230787;
        public static final int g4a_loadMatchErrorOffline = 2131230788;
        public static final int g4a_resetMatchTitle = 2131230789;
        public static final int g4a_resetMatchMessage = 2131230790;
        public static final int g4a_resetMatchAcceptButton = 2131230791;
        public static final int g4a_resetMatchRejectButton = 2131230792;
        public static final int g4a_playResetTitle = 2131230793;
        public static final int g4a_playResetMessage = 2131230794;
        public static final int g4a_playResetAccept = 2131230795;
        public static final int g4a_playResetCancel = 2131230796;
        public static final int g4a_loadDialogTitle = 2131230797;
        public static final int g4a_loadDialogMessage = 2131230798;
        public static final int g4a_loadDialogAccept = 2131230799;
        public static final int g4a_loadDialogCancel = 2131230800;
        public static final int g4a_loadGameSuccessToast = 2131230801;
        public static final int g4a_loadGameFailure = 2131230802;
        public static final int g4a_loadGameErrorCode = 2131230803;
        public static final int g4a_loadGameErrorMsg = 2131230804;
        public static final int g4a_endMatchDialogTitle = 2131230805;
        public static final int g4a_endMatchResultWin = 2131230806;
        public static final int g4a_endMatchResultTie = 2131230807;
        public static final int g4a_endMatchResultLoss = 2131230808;
        public static final int g4a_endMatchResultFirst = 2131230809;
        public static final int g4a_endMatchResultLast = 2131230810;
        public static final int g4a_endMatchResultPlace = 2131230811;
        public static final int g4a_endMatchResultFirstShared = 2131230812;
        public static final int g4a_endMatchResultLastShared = 2131230813;
        public static final int g4a_endMatchResultPlaceShared = 2131230814;
        public static final int g4a_endMatchResult = 2131230815;
        public static final int g4a_endMatchDialogNextMatchButton = 2131230816;
        public static final int g4a_endMatchDialogShowDuplicateButton = 2131230817;
        public static final int g4a_endMatchDialogShowRatingsButton = 2131230818;
        public static final int g4a_endMatchDialogRatings = 2131230819;
        public static final int g4a_ratingDialogTitle = 2131230820;
        public static final int g4a_ratingPanelColRating = 2131230821;
        public static final int g4a_ratingPanelColRanking = 2131230822;
        public static final int g4a_endGameDialogNextGameButton = 2131230823;
        public static final int g4a_endGameDialogShowDuplicateButton = 2131230824;
        public static final int g4a_endGameDialogShowGameButton = 2131230825;
        public static final int g4a_endGameRatings = 2131230826;
        public static final int g4a_dupDialogColPlace = 2131230827;
        public static final int g4a_dupDialogColPlayer = 2131230828;
        public static final int g4a_dupDialogColDupResult = 2131230829;
        public static final int g4a_dupDialogColDupPct = 2131230830;
        public static final int g4a_reportTitle = 2131230831;
        public static final int g4a_reportExplanation = 2131230832;
        public static final int g4a_reportReviewGame = 2131230833;
        public static final int g4a_reportFieldLogin = 2131230834;
        public static final int g4a_reportFieldPassword = 2131230835;
        public static final int g4a_reportFieldName = 2131230836;
        public static final int g4a_reportFieldEmail = 2131230837;
        public static final int g4a_reportFieldSummary = 2131230838;
        public static final int g4a_reportFieldDetails = 2131230839;
        public static final int g4a_reportIncludeState = 2131230840;
        public static final int g4a_reportIncludeStateInfo = 2131230841;
        public static final int g4a_reportSubmitButton = 2131230842;
        public static final int g4a_reportErrorIncompleteReport = 2131230843;
        public static final int g4a_reportErrorNamePassword = 2131230844;
        public static final int g4a_reportFailTitle = 2131230845;
        public static final int g4a_reportFailMessage = 2131230846;
        public static final int g4a_reportFailUrl = 2131230847;
        public static final int g4a_reportOkTitle = 2131230848;
        public static final int g4a_reportOkMessage = 2131230849;
        public static final int g4a_reportOkUrl = 2131230850;
        public static final int g4a_invalidCredentials = 2131230851;
        public static final int g4a_unauthorizedAccess = 2131230852;
        public static final int g4a_oldClientVersion = 2131230853;
        public static final int g4a_newClientVersion = 2131230854;
        public static final int g4a_gameNotFound = 2131230855;
        public static final int g4a_updateClient = 2131230856;
        public static final int g4a_dontShowMessage = 2131230857;
        public static final int g4a_help_long_click_title = 2131230858;
        public static final int g4a_help_long_click_content = 2131230859;
        public static final int g4a_help_title = 2131230860;
        public static final int g4a_tt_menu_title = 2131230861;
        public static final int g4a_tt_menu_content = 2131230862;
        public static final int g4a_tt_menu_account_title = 2131230863;
        public static final int g4a_tt_menu_account_content = 2131230864;
        public static final int g4a_tt_menu_reset_title = 2131230865;
        public static final int g4a_tt_menu_reset_content = 2131230866;
        public static final int g4a_tt_menu_rating_title = 2131230867;
        public static final int g4a_tt_menu_rating_content = 2131230868;
        public static final int g4a_tt_menu_settings_title = 2131230869;
        public static final int g4a_tt_menu_settings_content = 2131230870;
        public static final int g4a_tt_menu_shopping_title = 2131230871;
        public static final int g4a_tt_menu_shopping_content = 2131230872;
        public static final int g4a_tt_menu_info_title = 2131230873;
        public static final int g4a_tt_menu_info_content = 2131230874;
        public static final int app_name_dev = 2131230875;
        public static final int app_name_prod = 2131230876;
        public static final int app_name_test = 2131230877;
        public static final int player_score = 2131230878;
        public static final int g4a_endGameDialogTitle = 2131230879;
        public static final int g4a_ratingAvgGamePoints = 2131230880;
        public static final int g4a_ratingLowestMatchScore = 2131230881;
        public static final int g4a_ratingTotalMatchesWon = 2131230882;
        public static final int g4a_ratingGameWinStreak = 2131230883;
        public static final int g4a_ratingMatchWinStreak = 2131230884;
        public static final int g4a_game = 2131230885;
        public static final int g4a_helpUrl = 2131230886;
    }

    public static final class style {
        public static final int G4A_PanelDialogTheme = 2131296256;
    }
}
