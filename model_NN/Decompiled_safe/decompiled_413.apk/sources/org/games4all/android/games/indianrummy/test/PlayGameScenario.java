package org.games4all.android.games.indianrummy.test;

import org.games4all.android.option.d;
import org.games4all.android.test.a;
import org.games4all.android.test.b;

public class PlayGameScenario extends b {
    public void setRunner(a aVar) {
        super.setRunner(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.games.indianrummy.test.a.a(int, boolean, int):org.games4all.game.move.c
     arg types: [int, int, int]
     candidates:
      org.games4all.android.games.indianrummy.test.a.a(int, int, org.games4all.card.Card):org.games4all.game.move.c
      org.games4all.games.card.indianrummy.move.a.a(int, int, org.games4all.card.Card):org.games4all.game.move.c
      org.games4all.android.games.indianrummy.test.a.a(int, boolean, int):org.games4all.game.move.c */
    public void run() {
        d preferences = getPreferences();
        preferences.b(9);
        preferences.d(0);
        createAccount(false);
        waitForGame();
        checkScreenshot("game-start");
        waitMove();
        ((a) getRunner().g()).a(0, false, 13);
        waitMove();
    }
}
