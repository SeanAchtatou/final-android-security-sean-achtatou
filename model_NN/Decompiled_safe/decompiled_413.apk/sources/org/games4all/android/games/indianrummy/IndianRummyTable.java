package org.games4all.android.games.indianrummy;

import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageButton;
import java.lang.reflect.Array;
import org.games4all.android.a.g;
import org.games4all.android.a.k;
import org.games4all.android.a.o;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.card.Hand;
import org.games4all.android.card.e;
import org.games4all.android.card.h;
import org.games4all.android.card.j;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.games.card.indianrummy.human.IndianRummyViewer;

public class IndianRummyTable extends e implements View.OnClickListener {
    private final Paint[] b = new Paint[IndianRummyViewer.HandState.values().length];
    private final Drawable[][] c;
    private Mode d;
    private final org.games4all.android.card.b e;
    private final org.games4all.android.card.a f;
    private a g;
    private final ImageButton h;

    private enum Mode {
        IDLE,
        TAKE,
        DISCARD,
        END_PHASE,
        END_GAME
    }

    public interface a {
        boolean a(Card card, int i);

        boolean a(Card card, int i, int i2);

        boolean a(boolean z, int i);

        boolean e();
    }

    public IndianRummyTable(Games4AllActivity games4AllActivity, org.games4all.a.a aVar) {
        super(games4AllActivity, aVar, 4);
        this.h = new ImageButton(games4AllActivity);
        this.h.setBackgroundResource(R.drawable.done_button);
        this.h.setOnClickListener(this);
        this.h.setId(R.id.done_button);
        addView(this.h);
        this.h.setVisibility(4);
        g i = i();
        this.e = new org.games4all.android.card.b(new org.games4all.card.b("discard"), i);
        this.f = new org.games4all.android.card.a(new org.games4all.card.b("stock"), i);
        h m = m();
        this.e.a(m);
        this.f.a(m);
        this.e.a(0);
        this.f.a(0);
        a(this.e);
        a(this.f);
        i.a(new b());
        Resources resources = games4AllActivity.getResources();
        this.b[IndianRummyViewer.HandState.UNKNOWN.ordinal()] = a(resources, (int) R.color.hand_unknown);
        this.b[IndianRummyViewer.HandState.NOTHING.ordinal()] = a(resources, (int) R.color.hand_nothing);
        this.b[IndianRummyViewer.HandState.FIRST_LIFE.ordinal()] = a(resources, (int) R.color.hand_first_life);
        this.b[IndianRummyViewer.HandState.SECOND_LIFE.ordinal()] = a(resources, (int) R.color.hand_second_life);
        this.c = (Drawable[][]) Array.newInstance(Drawable.class, IndianRummyViewer.HandState.values().length, 4);
        for (IndianRummyViewer.HandState handState : IndianRummyViewer.HandState.values()) {
            this.c[handState.ordinal()] = a(resources, handState);
        }
    }

    class b implements k {
        b() {
        }

        public void a(o oVar) {
        }

        public void b(o oVar) {
            org.games4all.android.card.d dVar = (org.games4all.android.card.d) oVar.k();
            IndianRummyTable.this.a(dVar.a(), dVar.b(), oVar.d());
        }

        public void c(o oVar) {
            org.games4all.android.card.d dVar = (org.games4all.android.card.d) oVar.k();
            IndianRummyTable.this.a(dVar.a(), dVar.b());
        }

        public void a(o oVar, int i, int i2) {
        }

        public void a() {
            IndianRummyTable.this.invalidate();
        }

        public void a(org.games4all.android.b.a aVar) {
            IndianRummyTable.this.a(aVar);
        }
    }

    private Paint a(Resources resources, int i) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize(resources.getDimension(R.dimen.g4a_nameLabelSize));
        paint.setColor(resources.getColor(i));
        return paint;
    }

    private Drawable[] a(Resources resources, IndianRummyViewer.HandState handState) {
        Drawable[] drawableArr = new Drawable[4];
        if (handState == IndianRummyViewer.HandState.SECOND_LIFE) {
            drawableArr[0] = resources.getDrawable(R.drawable.label_frame_second_life_0);
            drawableArr[1] = resources.getDrawable(R.drawable.label_frame_second_life_1);
            drawableArr[2] = resources.getDrawable(R.drawable.label_frame_second_life_2);
            drawableArr[3] = resources.getDrawable(R.drawable.label_frame_second_life_3);
        } else {
            drawableArr[0] = resources.getDrawable(R.drawable.label_frame_nothing_0);
            drawableArr[1] = resources.getDrawable(R.drawable.label_frame_nothing_1);
            drawableArr[2] = resources.getDrawable(R.drawable.label_frame_nothing_2);
            drawableArr[3] = resources.getDrawable(R.drawable.label_frame_nothing_3);
        }
        return drawableArr;
    }

    public void a(a aVar) {
        this.g = aVar;
    }

    private void b(int i, int i2) {
        m().a((i - 18) / 3, (int) (((double) (i2 - 18)) / 2.5d));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.games.indianrummy.IndianRummyTable.a(int, int, int, int, boolean):void
     arg types: [int, int, int, int, int]
     candidates:
      org.games4all.android.games.indianrummy.IndianRummyTable.a(int, org.games4all.card.Card, int, int, org.games4all.games.card.indianrummy.human.IndianRummyViewer$HandState):void
      org.games4all.android.games.indianrummy.IndianRummyTable.a(int, int, int, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        a(i, i2, i3, i4, false);
    }

    /* access modifiers changed from: protected */
    public void a(int i, int i2, int i3, int i4, boolean z) {
        b(i3 - i, i4 - i2);
        h m = m();
        int a2 = m.a();
        int b2 = m.b();
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int i7 = a2 + 3;
        int i8 = i5 - (i7 * 2);
        int i9 = ((i8 / 3) + i7) - (a2 / 2);
        int i10 = (i7 + ((i8 * 2) / 3)) - (a2 / 2);
        int i11 = (-b2) / 2;
        int i12 = (i6 - b2) - 4;
        int i13 = (i12 - (i11 + b2)) / 2;
        int i14 = (i10 - i9) - a2;
        this.h.measure(View.MeasureSpec.makeMeasureSpec(i14, Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(i14, Integer.MIN_VALUE));
        int measuredWidth = this.h.getMeasuredWidth();
        int measuredHeight = this.h.getMeasuredHeight();
        int i15 = ((i14 - measuredWidth) / 2) + i9 + a2;
        int i16 = ((b2 - measuredHeight) / 2) + i13;
        this.h.layout(i15, i16, measuredWidth + i15, measuredHeight + i16);
        if (this.d == Mode.END_GAME) {
            a(e(0), a2 + 3, i12, (i5 - a2) - 3, i6 - 3, z);
            a(e(1), 3, 3, a2 + 3, i6 + ((b2 * 2) / 3), z);
            a(e(2), a2 + 3, 3, (i5 - a2) - 3, b2 + 3, z);
            a(e(3), (i5 - a2) - 3, 3, i5 - 3, i6 + ((b2 * 2) / 3), z);
        } else {
            a(e(0), 3, i12, i5 - 3, i6 - 3, z);
            a(e(1), 3, 3, a2 + 3, i12 - 3, z);
            a(e(2), a2 + 3, i11, (i5 - a2) - 3, i11 + b2, z);
            a(e(3), (i5 - a2) - 3, 3, i5 - 3, i12 - 3, z);
        }
        this.e.a(i9, i13, i9 + a2, i13 + b2);
        this.f.a(i10, i13, i10 + a2, i13 + b2);
        a(z);
    }

    private void a(Hand hand, int i, int i2, int i3, int i4, boolean z) {
        if (z) {
            hand.b(i, i2, i3, i4, getWidth(), getHeight());
            return;
        }
        hand.a(i, i2, i3, i4, getWidth(), getHeight());
    }

    public void a(int i) {
        Hand e2 = e(i);
        e2.a((String) null);
        e2.c().a(this.c[IndianRummyViewer.HandState.UNKNOWN.ordinal()][i]);
    }

    public void a(Cards cards) {
        this.e.a(cards);
        invalidate();
    }

    public void a() {
        e(0).i();
        this.f.b();
        this.e.b();
        this.d = Mode.TAKE;
    }

    public void b() {
        e(0).i();
        this.d = Mode.DISCARD;
    }

    public void a(int i, Cards cards) {
        a(i, cards, 0, IndianRummyViewer.HandState.SECOND_LIFE);
        this.d = Mode.END_PHASE;
        e(0).i();
        this.h.setVisibility(0);
        this.f.a(false);
        this.e.a(false);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void a(j jVar, int i) {
        org.games4all.android.card.g b2 = jVar.b(i);
        b2.a(255);
        b2.b(1000);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.games.indianrummy.IndianRummyTable.a(org.games4all.android.card.g, boolean):boolean
     arg types: [org.games4all.android.card.g, int]
     candidates:
      org.games4all.android.games.indianrummy.IndianRummyTable.a(android.content.res.Resources, int):android.graphics.Paint
      org.games4all.android.games.indianrummy.IndianRummyTable.a(android.content.res.Resources, org.games4all.games.card.indianrummy.human.IndianRummyViewer$HandState):android.graphics.drawable.Drawable[]
      org.games4all.android.games.indianrummy.IndianRummyTable.a(int, org.games4all.card.Cards):void
      org.games4all.android.games.indianrummy.IndianRummyTable.a(org.games4all.android.card.j, int):void
      org.games4all.android.card.e.a(org.games4all.a.a, int):void
      org.games4all.android.card.e.a(int, int):org.games4all.android.card.g
      org.games4all.android.card.e.a(org.games4all.android.card.j, int):void
      org.games4all.android.games.indianrummy.IndianRummyTable.a(org.games4all.android.card.g, boolean):boolean */
    /* access modifiers changed from: protected */
    public void a(j jVar, int i, Point point) {
        org.games4all.android.card.g b2 = jVar.b(i);
        Point d2 = b2.d();
        Hand e2 = e(0);
        if (jVar == e2) {
            if (d2.y > e2.d().top - (m().c() / 4)) {
                int a2 = a(b2);
                if (a2 >= 0 && a2 != i) {
                    if (a2 > i) {
                        a2--;
                    }
                    if (this.g.a(b2.c(), i, a2)) {
                        e2.j();
                        this.f.c();
                        this.e.c();
                        return;
                    }
                    return;
                }
            } else if (this.d == Mode.DISCARD && d2.y < e2.d().top - (m().c() / 4) && this.g.a(b2.c(), i)) {
                e2.j();
                return;
            }
        } else if (this.d == Mode.TAKE) {
            if (jVar == this.f) {
                if (a(b2, false)) {
                    return;
                }
            } else if (jVar == this.e && a(b2, true)) {
                return;
            }
        }
        b(jVar, i);
    }

    private int a(org.games4all.android.card.g gVar) {
        Hand e2 = e(0);
        int size = e2.h().size();
        Point d2 = gVar.d();
        int a2 = d2.y + (gVar.a() / 2);
        org.games4all.android.card.g b2 = e2.b(0);
        org.games4all.android.card.g b3 = e2.b(size - 1);
        if (d2.x < b2.e().x) {
            return 0;
        }
        if (d2.x > b3.b() + b3.e().x) {
            return size;
        }
        int a3 = e2.a(d2.x, a2);
        if (a3 >= 0) {
            return a3 + 1;
        }
        return a3;
    }

    private boolean a(org.games4all.android.card.g gVar, boolean z) {
        j jVar = z ? this.e : this.f;
        Hand e2 = e(0);
        if (gVar.d().y + (gVar.a() / 2) > jVar.d().centerY()) {
            int a2 = a(gVar);
            if (a2 < 0) {
                a2 = e2.h().size();
            }
            if (this.g.a(z, a2)) {
                e2.j();
                this.f.c();
                this.e.c();
                this.d = null;
                return true;
            }
        }
        return false;
    }

    public void a(Cards cards, int i, IndianRummyViewer.HandState handState, Cards cards2, int i2, IndianRummyViewer.HandState handState2, Cards cards3, int i3, IndianRummyViewer.HandState handState3, Cards cards4, int i4, IndianRummyViewer.HandState handState4) {
        a(0, cards, i, handState);
        a(1, cards2, i2, handState2);
        a(2, cards3, i3, handState3);
        a(3, cards4, i4, handState4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.games.indianrummy.IndianRummyTable.a(int, int, int, int, boolean):void
     arg types: [int, int, int, int, int]
     candidates:
      org.games4all.android.games.indianrummy.IndianRummyTable.a(int, org.games4all.card.Card, int, int, org.games4all.games.card.indianrummy.human.IndianRummyViewer$HandState):void
      org.games4all.android.games.indianrummy.IndianRummyTable.a(int, int, int, int, boolean):void */
    public void c() {
        this.f.a(false);
        this.e.a(false);
        this.d = Mode.END_GAME;
        e(1).a(16);
        e(3).a(16);
        for (int i = 0; i < 4; i++) {
            e(i).c().a(200);
        }
        if (getWidth() > 0 && getHeight() > 0) {
            a(0, 0, getWidth(), getHeight(), true);
        }
        invalidate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.games.indianrummy.IndianRummyTable.a(int, int, int, int, boolean):void
     arg types: [int, int, int, int, int]
     candidates:
      org.games4all.android.games.indianrummy.IndianRummyTable.a(int, org.games4all.card.Card, int, int, org.games4all.games.card.indianrummy.human.IndianRummyViewer$HandState):void
      org.games4all.android.games.indianrummy.IndianRummyTable.a(int, int, int, int, boolean):void */
    public void d() {
        this.f.a(true);
        this.e.a(true);
        this.d = Mode.IDLE;
        e(1).a(80);
        e(3).a(80);
        for (int i = 0; i < 4; i++) {
            e(i).c().a(255);
        }
        if (getWidth() > 0 && getHeight() > 0) {
            a(0, 0, getWidth(), getHeight(), false);
        }
        invalidate();
    }

    private void a(int i, Cards cards, int i2, IndianRummyViewer.HandState handState) {
        e(i).a(cards);
        a(i, i2, handState);
    }

    public void a(int i, int i2, IndianRummyViewer.HandState handState) {
        String string = getResources().getString(R.string.player_score, Integer.valueOf(i2));
        Hand e2 = e(i);
        e2.a(string);
        e2.c().a(this.c[handState.ordinal()][i]);
        a(true);
        invalidate();
    }

    public void a(int i, Card card, int i2, int i3, IndianRummyViewer.HandState handState) {
        org.games4all.android.card.g b2 = e(i).b(i2);
        int a2 = j().a(50, 1000);
        this.e.a(i == 0 ? card : Card.a, card, b2.d(), a2, 0);
        a(e(i).e(i2));
        if (i == 0) {
            a(i, i3, handState);
        }
    }

    public void a(int i, boolean z, Card card, int i2, int i3, IndianRummyViewer.HandState handState) {
        j jVar;
        Hand e2 = e(i);
        if (z) {
            jVar = this.e;
        } else {
            jVar = this.f;
        }
        org.games4all.android.card.g e3 = jVar.e();
        Card card2 = z ? card : Card.a;
        Point d2 = e3.d();
        e3.g();
        e3.a(0);
        e3.b(jVar.d(0));
        Card card3 = i == 0 ? card : Card.a;
        if (z) {
            this.e.f();
        }
        e2.a(i2, card2, d2, card3, j().a(50, 1000), 0);
        if (i == 0) {
            h().execute(new c(i, i3, handState));
        }
    }

    class c implements Runnable {
        final /* synthetic */ int a;
        final /* synthetic */ int b;
        final /* synthetic */ IndianRummyViewer.HandState c;

        c(int i, int i2, IndianRummyViewer.HandState handState) {
            this.a = i;
            this.b = i2;
            this.c = handState;
        }

        public void run() {
            IndianRummyTable.this.a(this.a, this.b, this.c);
        }
    }

    class d implements Runnable {
        final /* synthetic */ int a;
        final /* synthetic */ int b;
        final /* synthetic */ IndianRummyViewer.HandState c;

        d(int i, int i2, IndianRummyViewer.HandState handState) {
            this.a = i;
            this.b = i2;
            this.c = handState;
        }

        public void run() {
            IndianRummyTable.this.a(this.a, this.b, this.c);
        }
    }

    public void a(int i, Card card, int i2, int i3, int i4, IndianRummyViewer.HandState handState) {
        a(e(i), card, i2, i3);
        h().execute(new d(i, i4, handState));
    }

    public Card e() {
        return this.e.h().c();
    }

    public void b(int i, Cards cards) {
        super.b(i, cards);
    }

    public Point f() {
        return this.e.i();
    }

    public Point g() {
        return this.f.f();
    }

    public void onClick(View view) {
        if (view == this.h && this.d == Mode.END_PHASE && this.g.e()) {
            this.h.setVisibility(4);
            e(0).j();
            this.d = null;
        }
    }

    public void b(int i) {
        if (k() != i) {
            super.b(i);
        }
    }
}
