package org.games4all.android.play;

import java.util.EnumSet;

public enum PlayState {
    START(new Marker[0]),
    SHOW_HELP(Marker.PAUSABLE),
    SELECT_DEFAULT_OPTIONS(Marker.PAUSABLE),
    RESUME(Marker.ARTIFICIAL),
    SELECT_LOGIN(Marker.PAUSABLE),
    CREATE_ACCOUNT(Marker.PAUSABLE),
    CREATING_ACCOUNT(Marker.PAUSABLE),
    LOGIN_EXISTING(Marker.PAUSABLE),
    LOGGING_IN(Marker.PAUSABLE),
    LOAD_CURRENT(Marker.ARTIFICIAL),
    PLAYING(Marker.PAUSABLE),
    NEW_MATCH(Marker.ARTIFICIAL),
    DOWNLOADING_MATCH(Marker.PAUSABLE),
    DOWNLOADING_GAME(Marker.PAUSABLE),
    REPLAYING(Marker.PAUSABLE),
    PAUSE(new Marker[0]),
    STOP(new Marker[0]);
    
    private EnumSet<Marker> markers = EnumSet.noneOf(Marker.class);

    public enum Marker {
        PAUSABLE,
        ARTIFICIAL
    }

    private PlayState(Marker... markerArr) {
        for (Marker add : markerArr) {
            this.markers.add(add);
        }
    }

    public boolean is(Marker marker) {
        return this.markers.contains(marker);
    }
}
