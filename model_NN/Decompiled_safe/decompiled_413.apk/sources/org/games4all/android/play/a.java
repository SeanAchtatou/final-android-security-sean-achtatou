package org.games4all.android.play;

import android.content.DialogInterface;
import android.os.AsyncTask;
import org.games4all.android.e.f;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.gamestore.client.LoadMatchFailedException;
import org.games4all.match.Match;
import org.games4all.match.MatchResult;

public class a extends AsyncTask<Void, Void, Match> {
    private final GamePlayActivity a;
    private long b;
    private int c;
    private MatchResult d;
    private f e;
    private LoadMatchFailedException f;

    public a(GamePlayActivity gamePlayActivity) {
        this.a = gamePlayActivity;
    }

    public void a(long j, int i, MatchResult matchResult) {
        this.b = j;
        this.c = i;
        this.d = matchResult;
        execute(new Void[0]);
        this.e = new f(this.a);
        this.e.setTitle((int) R.string.g4a_loadMatchProgressDialogTitle);
        this.e.b(this.a.getResources().getString(R.string.g4a_loadMatchProgressDialogMessage));
        this.e.a(true);
        this.e.setCancelable(true);
        this.e.setOnCancelListener(new C0014a());
        this.e.show();
    }

    /* renamed from: org.games4all.android.play.a$a  reason: collision with other inner class name */
    class C0014a implements DialogInterface.OnCancelListener {
        C0014a() {
        }

        public void onCancel(DialogInterface dialogInterface) {
            a.this.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Match doInBackground(Void... voidArr) {
        try {
            return this.a.p().f().a(this.b, this.c, this.d);
        } catch (LoadMatchFailedException e2) {
            this.f = e2;
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Match match) {
        if (!isCancelled() && this.a.a().c(PlayState.DOWNLOADING_MATCH)) {
            this.e.dismiss();
            if (match == null) {
                this.a.c(this.f == null ? "<unknown error>" : this.f.getMessage());
            } else {
                this.a.a(match);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        if (this.e.isShowing()) {
            this.e.dismiss();
        }
        this.a.c(this.a.getResources().getString(R.string.g4a_loadMatchCancelled));
    }
}
