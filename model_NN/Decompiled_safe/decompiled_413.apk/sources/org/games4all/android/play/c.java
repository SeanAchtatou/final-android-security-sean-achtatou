package org.games4all.android.play;

import android.os.AsyncTask;
import org.games4all.android.GameApplication;
import org.games4all.gamestore.client.LoadGameFailedException;

public class c extends AsyncTask<Void, Void, Void> {
    private final GamePlayActivity a;
    private final int b;
    private org.games4all.gamestore.client.c c;
    private LoadGameFailedException d;

    public c(GamePlayActivity gamePlayActivity, int i) {
        this.a = gamePlayActivity;
        this.b = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        this.d = null;
        this.c = null;
        try {
            this.c = ((GameApplication) this.a.getApplication()).f().a(this.b);
        } catch (LoadGameFailedException e) {
            this.d = e;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Void voidR) {
        if (this.c != null) {
            this.a.a(this.c.b(), this.c.a());
        } else {
            this.a.a(this.d.getMessage(), this.d.a());
        }
    }
}
