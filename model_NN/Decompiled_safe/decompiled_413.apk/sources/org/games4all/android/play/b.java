package org.games4all.android.play;

import android.content.DialogInterface;
import java.util.EnumSet;
import org.games4all.android.GameApplication;
import org.games4all.android.card.i;
import org.games4all.android.d.d;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.play.PlayState;
import org.games4all.game.PlayerInfo;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.option.PlayerOptionsImpl;
import org.games4all.game.option.e;
import org.games4all.game.option.k;
import org.games4all.game.option.n;

public class b extends org.games4all.f.c<PlayState, PlayEvent> {
    private final GamePlayActivity a;
    private final org.games4all.game.b.a b;
    private final GameApplication c;
    private final org.games4all.a.a d;
    private org.games4all.android.d.c e;

    public b(GamePlayActivity gamePlayActivity, org.games4all.a.a aVar) {
        super(null, PlayState.class, PlayEvent.class, OnEnter.class, OnExit.class, OnEvent.class);
        this.c = gamePlayActivity.p();
        this.b = gamePlayActivity.c();
        this.a = gamePlayActivity;
        this.d = aVar;
        b();
        a(PlayState.START);
    }

    private void b() {
        a(EnumSet.of(PlayState.START, PlayState.PAUSE, PlayState.SHOW_HELP, PlayState.SELECT_DEFAULT_OPTIONS), PlayEvent.ON_RESUME, PlayState.RESUME);
        a(PlayState.RESUME, PlayEvent.HELP_REQUIRED, PlayState.SHOW_HELP);
        a(PlayState.RESUME, PlayEvent.DEFAULT_OPTIONS_REQUIRED, PlayState.SELECT_DEFAULT_OPTIONS);
        a(PlayState.RESUME, PlayEvent.GO_OFFLINE, PlayState.LOAD_CURRENT);
        a(PlayState.RESUME, PlayEvent.LOAD_NEEDED, PlayState.LOAD_CURRENT);
        a(PlayState.PLAYING, PlayEvent.MATCH_ENDED, PlayState.NEW_MATCH);
        a(PlayState.PLAYING, PlayEvent.GO_OFFLINE, PlayState.LOAD_CURRENT);
        a(PlayState.PLAYING, PlayEvent.RESET, PlayState.LOAD_CURRENT);
        a(PlayState.PLAYING, PlayEvent.LOGIN_REQUIRED, PlayState.SELECT_LOGIN);
        a(PlayState.PLAYING, PlayEvent.OPTIONS_CHANGED, PlayState.LOAD_CURRENT);
        a(PlayState.PLAYING, PlayEvent.REPLAY_STARTED, PlayState.REPLAYING);
        a(PlayState.NEW_MATCH, PlayEvent.DOWNLOAD_MATCH_STARTED, PlayState.DOWNLOADING_MATCH);
        a(PlayState.DOWNLOADING_MATCH, PlayEvent.MATCH_LOADED, PlayState.PLAYING);
        a(PlayState.LOAD_CURRENT, PlayEvent.LOAD_FAILED, PlayState.NEW_MATCH);
        a(PlayState.LOAD_CURRENT, PlayEvent.LOADED_MATCH_DONE, PlayState.NEW_MATCH);
        a(PlayState.LOAD_CURRENT, PlayEvent.LOAD_SUCCEEDED, PlayState.PLAYING);
        a(PlayState.REPLAYING, PlayEvent.RESUME_REPLAYED, PlayState.LOAD_CURRENT);
        a(PlayState.REPLAYING, PlayEvent.RESET, PlayState.LOAD_CURRENT);
        for (PlayState playState : (PlayState[]) PlayState.class.getEnumConstants()) {
            if (playState.is(PlayState.Marker.PAUSABLE)) {
                a(playState, PlayEvent.ON_PAUSE, PlayState.PAUSE);
            }
        }
        a(PlayState.PAUSE, PlayEvent.ON_DESTROY, PlayState.STOP);
    }

    public GamePlayActivity a() {
        return this.a;
    }

    class a implements Runnable {
        final /* synthetic */ PlayEvent a;

        a(PlayEvent playEvent) {
            this.a = playEvent;
        }

        public void run() {
            b.this.b(this.a);
        }
    }

    public void a(PlayEvent playEvent) {
        this.d.a(new a(playEvent), "EVENT: " + playEvent);
    }

    @OnEnter(PlayState.START)
    public void enterStart() {
        this.a.a(new i(this.a, this.d, null));
    }

    @OnEnter(PlayState.RESUME)
    public void enterResume() {
        org.games4all.android.e.a a2 = org.games4all.android.e.a.a(this.a);
        if (a2 != null) {
            a2.setOnCancelListener(new C0015b());
            b(PlayEvent.HELP_REQUIRED);
            return;
        }
        if (this.a.h() == null) {
            k p = this.c.p();
            if (this.c.p() instanceof n) {
                this.a.d((e) p.a());
            } else {
                b(PlayEvent.DEFAULT_OPTIONS_REQUIRED);
                return;
            }
        }
        if (this.c.g().b()) {
            b(PlayEvent.LOAD_NEEDED);
        } else {
            b(PlayEvent.GO_OFFLINE);
        }
    }

    /* renamed from: org.games4all.android.play.b$b  reason: collision with other inner class name */
    class C0015b implements DialogInterface.OnCancelListener {
        C0015b() {
        }

        public void onCancel(DialogInterface dialogInterface) {
            b.this.a().e();
        }
    }

    @OnEnter(PlayState.SELECT_DEFAULT_OPTIONS)
    public void enterSelectDefaultOptions() {
        this.a.g();
    }

    @OnEnter(PlayState.SELECT_LOGIN)
    public void enterSelectLogin() {
        this.e = new org.games4all.android.d.c(this.a, this);
        this.e.show();
    }

    @OnExit(PlayState.SELECT_LOGIN)
    public void exitSelectLogin() {
        if (this.e != null && this.e.isShowing()) {
            this.e.dismiss();
        }
    }

    @OnEnter(PlayState.CREATE_ACCOUNT)
    public void enterCreateAccount() {
        new d(this.a, this, this.c.f()).show();
    }

    @OnEnter(PlayState.LOGIN_EXISTING)
    public void enterLoginExisting() {
        new org.games4all.android.d.b(this.a, this, this.c.f()).show();
    }

    @OnEvent(PlayEvent.GO_OFFLINE)
    public void eventGoOffline() {
        this.c.g().f();
    }

    @OnEnter(PlayState.PLAYING)
    public void enterPlaying() {
        this.a.b(this.c.g().a());
        this.b.a(false);
        this.c.t().a(this.b.b());
        c();
        this.c.d().e();
    }

    @OnExit(PlayState.PLAYING)
    public void exitPlaying() {
        this.c.d().f();
        this.a.k();
        this.b.c();
        this.d.d();
    }

    @OnEnter(PlayState.LOAD_CURRENT)
    public void enterLoadCurrent() {
        if (!this.a.m()) {
            b(PlayEvent.LOAD_FAILED);
        } else if (this.b.b().q() == Stage.SESSION) {
            b(PlayEvent.LOADED_MATCH_DONE);
        } else {
            b(PlayEvent.LOAD_SUCCEEDED);
        }
    }

    @OnEnter(PlayState.NEW_MATCH)
    public void enterNewMatch() {
        b(PlayEvent.DOWNLOAD_MATCH_STARTED);
        this.a.n();
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [org.games4all.game.model.d] */
    private void c() {
        String str;
        org.games4all.game.b.a c2 = this.a.c();
        e a2 = this.c.k().m().f().a();
        PlayerInfo playerInfo = new PlayerInfo(this.a.getResources().getString(R.string.g4a_player), 0, false);
        PlayerOptionsImpl playerOptionsImpl = new PlayerOptionsImpl();
        c2.a(playerInfo, playerOptionsImpl);
        String a3 = this.c.l().a();
        if (a3 == null) {
            str = this.a.d().b(a2);
        } else {
            str = a3;
        }
        this.d.execute(new c(a2, str, c2, playerOptionsImpl));
        c2.l();
    }

    class c implements Runnable {
        final /* synthetic */ e a;
        final /* synthetic */ String b;
        final /* synthetic */ org.games4all.game.b.a c;
        final /* synthetic */ org.games4all.game.option.b d;

        c(e eVar, String str, org.games4all.game.b.a aVar, org.games4all.game.option.b bVar) {
            this.a = eVar;
            this.b = str;
            this.c = aVar;
            this.d = bVar;
        }

        public void run() {
            int b2 = this.a.b();
            for (int i = 1; i < b2; i++) {
                this.c.b(new PlayerInfo(this.b, i, true), this.d);
            }
        }
    }
}
