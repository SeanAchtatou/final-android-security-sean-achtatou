package org.games4all.android.play;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.flurry.android.u;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.analytics.InstallReceiver;
import org.games4all.android.billing.BillingService;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.option.a;
import org.games4all.android.report.ReportActivity;
import org.games4all.android.test.AutoplayScenario;
import org.games4all.game.controller.server.GameSeed;
import org.games4all.game.move.PlayerMove;
import org.games4all.game.option.n;
import org.games4all.logging.LogLevel;
import org.games4all.match.Match;
import org.games4all.match.MatchResult;
import org.games4all.util.SoftwareVersion;

public class GamePlayActivity extends Games4AllActivity implements MenuItem.OnMenuItemClickListener, View.OnClickListener, a.C0013a {
    private GameApplication a;
    private b b;
    private org.games4all.android.b.d c;
    private org.games4all.game.b.a d;
    private org.games4all.game.c.a<?> e;
    private FrameLayout f;
    private View g;
    private LinearLayout h;
    private ImageButton i;
    private ImageButton j;
    private ImageButton k;
    private ImageButton l;
    private ImageButton m;
    private ImageButton n;
    private ImageButton o;
    private FrameLayout p;
    private LinearLayout q;
    private boolean r;
    private a s;
    private org.games4all.game.option.e t;
    private org.games4all.game.option.e u;
    private org.games4all.android.test.b v;
    private MenuItem w;
    private boolean x;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        org.games4all.android.report.c cVar = new org.games4all.android.report.c(this);
        Thread.setDefaultUncaughtExceptionHandler(cVar);
        requestWindowFeature(1);
        this.c = new org.games4all.android.b.d();
        this.a = p();
        String stringExtra = getIntent().getStringExtra("URL");
        if (stringExtra != null) {
            this.a.a(stringExtra);
        }
        d(stringExtra);
        this.f = new FrameLayout(this);
        setContentView(this.f);
        v();
        u();
        this.b = new b(this, this.c);
        this.b.a(new k("FSM"));
        this.a.t().a(this);
        this.u = l();
        if (!(this.u == null || this.a.p().a().getClass() == this.u.getClass())) {
            this.u = null;
        }
        cVar.a();
        this.x = org.games4all.android.b.a() || org.games4all.android.b.b();
        BillingService billingService = new BillingService();
        billingService.b(this);
        org.games4all.android.billing.i.a(new org.games4all.android.billing.j(this));
        if (org.games4all.android.b.a() || org.games4all.android.b.b()) {
            billingService.a();
        }
    }

    class k extends org.games4all.f.d<PlayState, PlayEvent> {
        k(String str) {
            super(str);
        }

        /* access modifiers changed from: protected */
        public void a(String str) {
            GamePlayActivity.this.a(LogLevel.INFO, str);
        }
    }

    public void a(boolean z) {
        this.x = z;
    }

    public b a() {
        return this.b;
    }

    private void d(String str) {
        boolean z = false;
        String stringExtra = getIntent().getStringExtra("SCENARIO");
        String str2 = null;
        if (str != null) {
            str2 = str.substring(0, str.lastIndexOf(47)) + "/scenario";
        }
        GameApplication gameApplication = this.a;
        org.games4all.android.b.d dVar = this.c;
        if (stringExtra != null) {
            z = true;
        }
        gameApplication.a(dVar, str2, z);
        if (stringExtra != null) {
            System.err.println("Url: " + str2);
            e(stringExtra);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.play.GamePlayActivity.a(org.games4all.android.test.b, boolean):void
     arg types: [org.games4all.android.test.b, int]
     candidates:
      org.games4all.android.play.GamePlayActivity.a(java.lang.String, org.games4all.game.option.e):java.lang.String
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, java.util.List<java.util.List<org.games4all.game.move.PlayerMove>>):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, org.games4all.match.Match):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, org.games4all.match.MatchResult):void
      org.games4all.android.play.GamePlayActivity.a(java.lang.String, int):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.model.e<?, ?, ?>, java.util.List<java.util.List<org.games4all.game.move.PlayerMove>>):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, java.lang.String):void
      org.games4all.android.activity.Games4AllActivity.a(org.games4all.logging.LogLevel, java.lang.String):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.android.test.b, boolean):void */
    private void e(String str) {
        System.err.println("Scenario: " + str);
        try {
            a((org.games4all.android.test.b) Class.forName(str).newInstance(), true);
        } catch (ClassNotFoundException e2) {
            System.err.println("Cannot find scenario class: " + str);
        } catch (IllegalAccessException e3) {
            System.err.println("Cannot create scenario " + str + ": " + e3.getMessage());
        } catch (InstantiationException e4) {
            System.err.println("Cannot create scenario " + str + ": " + e4.getMessage());
        }
    }

    private void a(org.games4all.android.test.b bVar, boolean z) {
        org.games4all.android.test.a d2 = this.a.d();
        d2.a(z);
        d2.a(bVar);
    }

    private void u() {
        org.games4all.game.a<?, ?> m2 = this.a.m();
        org.games4all.game.controller.a.d n2 = this.a.n();
        org.games4all.game.e.c<?> a2 = this.a.a(this, this.c);
        this.e = this.a.o();
        this.d = new org.games4all.game.b.a(m2, new org.games4all.game.b.b(n2, a2, this.e), this.c);
        this.a.a(this.d);
        this.d.a(new i());
        this.d.a(new j("LC"));
    }

    class i extends org.games4all.game.lifecycle.a {
        i() {
        }

        /* access modifiers changed from: protected */
        public void b_() {
            GamePlayActivity.this.c().e();
        }

        /* access modifiers changed from: protected */
        public void j() {
            GamePlayActivity.this.b();
        }
    }

    class j extends org.games4all.game.lifecycle.b {
        j(String str) {
            super(str);
        }

        /* access modifiers changed from: protected */
        public void a(String str) {
            GamePlayActivity.this.a(LogLevel.INFO, str);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.b.a(PlayEvent.MATCH_ENDED);
        if (!org.games4all.android.b.d()) {
            u.b("matchCompleted", new HashMap());
        }
    }

    public org.games4all.game.b.a c() {
        return this.d;
    }

    public org.games4all.game.c.a<?> d() {
        return this.e;
    }

    private void v() {
        this.p = new FrameLayout(this);
        this.p.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        this.f.addView(this.p);
        this.i = new ImageButton(this);
        this.i.setId(R.id.g4a_menuToggle);
        this.i.setBackgroundResource(R.drawable.g4a_button_menu_open);
        this.i.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        org.games4all.android.b.b.a(this, this.i, new org.games4all.android.b.a(R.string.g4a_tt_menu_title, R.string.g4a_tt_menu_content));
        this.q = new LinearLayout(this);
        this.q.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.p.addView(this.q);
        this.p.addView(this.i);
        ImageButton imageButton = new ImageButton(this);
        imageButton.setBackgroundResource(R.drawable.g4a_button_menu_open);
        imageButton.setVisibility(4);
        imageButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.q.addView(imageButton);
        this.h = new LinearLayout(this);
        this.h.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        if (org.games4all.android.b.c()) {
            this.j = c((int) R.drawable.g4a_button_disconnected);
            this.j.setId(R.id.g4a_menuAccount);
            org.games4all.android.b.b.a(this, this.j, new org.games4all.android.b.a(R.string.g4a_tt_menu_account_title, R.string.g4a_tt_menu_account_content));
            this.h.addView(this.j);
        }
        this.k = c((int) R.drawable.g4a_button_reset_match);
        this.k.setId(R.id.g4a_menuResetMatch);
        org.games4all.android.b.b.a(this, this.k, new org.games4all.android.b.a(R.string.g4a_tt_menu_reset_title, R.string.g4a_tt_menu_reset_content));
        this.h.addView(this.k);
        this.m = c((int) R.drawable.g4a_button_rankings);
        this.m.setId(R.id.g4a_menuRatings);
        org.games4all.android.b.b.a(this, this.m, new org.games4all.android.b.a(R.string.g4a_tt_menu_rating_title, R.string.g4a_tt_menu_rating_content));
        this.h.addView(this.m);
        this.l = c((int) R.drawable.g4a_button_settings);
        this.l.setId(R.id.g4a_menuSettings);
        org.games4all.android.b.b.a(this, this.l, new org.games4all.android.b.a(R.string.g4a_tt_menu_settings_title, R.string.g4a_tt_menu_settings_content));
        this.h.addView(this.l);
        this.n = c((int) R.drawable.g4a_button_shopping);
        this.n.setId(R.id.g4a_menuShopping);
        org.games4all.android.b.b.a(this, this.n, new org.games4all.android.b.a(R.string.g4a_tt_menu_shopping_title, R.string.g4a_tt_menu_shopping_content));
        this.h.addView(this.n);
        this.n.setVisibility(8);
        this.o = c((int) R.drawable.g4a_button_info);
        this.o.setId(R.id.g4a_menuInfo);
        org.games4all.android.b.b.a(this, this.o, new org.games4all.android.b.a(R.string.g4a_tt_menu_info_title, R.string.g4a_tt_menu_info_content));
        this.h.addView(this.o);
        this.i.setOnClickListener(this);
        this.h.setVisibility(4);
        this.q.addView(this.h);
    }

    private ImageButton c(int i2) {
        ImageButton imageButton = new ImageButton(this);
        imageButton.setBackgroundDrawable(getResources().getDrawable(i2));
        imageButton.setOnClickListener(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(5, 5, 0, 0);
        imageButton.setLayoutParams(layoutParams);
        return imageButton;
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        if (this.g != null) {
            this.f.removeView(this.g);
        }
        this.g = view;
        this.f.addView(this.g, 0);
    }

    public void onPause() {
        super.onPause();
        if (this.b.c(PlayState.DOWNLOADING_MATCH) && this.s != null) {
            this.s.cancel(true);
            this.s = null;
        }
        this.b.b(PlayEvent.ON_PAUSE);
        this.c.d();
    }

    public void onResume() {
        super.onResume();
        e();
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.b.b(PlayEvent.ON_RESUME);
    }

    public void onDestroy() {
        super.onDestroy();
        this.b.b(PlayEvent.ON_DESTROY);
    }

    public void onClick(View view) {
        if (!this.b.c(PlayState.PLAYING)) {
            return;
        }
        if (view == this.i) {
            y();
        } else if (view == this.m) {
            w();
        } else if (view == this.l) {
            z();
        } else if (view == this.o) {
            x();
        } else if (view == this.j) {
            k();
            this.b.b(PlayEvent.LOGIN_REQUIRED);
        } else if (view == this.k) {
            A();
        }
    }

    private void w() {
        if (t() == null) {
            new org.games4all.android.c.b(this, this.a.g().g(), this.a.z().a(), this.a.f().e()).show();
        }
    }

    private void x() {
        if (t() == null) {
            org.games4all.android.e.a.b(this);
        }
    }

    private void y() {
        if (this.r) {
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
            translateAnimation.setDuration(1000);
            translateAnimation.setAnimationListener(new g());
            this.h.startAnimation(translateAnimation);
            this.i.setBackgroundResource(R.drawable.g4a_button_menu_open);
            this.r = false;
            return;
        }
        this.n.setVisibility(this.x ? 0 : 8);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
        translateAnimation2.setDuration(1000);
        this.q.setLayoutAnimation(new LayoutAnimationController(translateAnimation2, 0.25f));
        this.h.setVisibility(0);
        this.i.setBackgroundResource(R.drawable.g4a_button_menu_close);
        this.r = true;
    }

    class g implements Animation.AnimationListener {
        g() {
        }

        public void onAnimationEnd(Animation animation) {
            GamePlayActivity.this.f();
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.h.setVisibility(4);
    }

    private void z() {
        if (t() != null) {
            this.a.a().d("showSettingsDialog failed: " + t());
        } else if (this.a.p() instanceof n) {
            new org.games4all.android.option.b(this).show();
        } else {
            new org.games4all.android.option.c(this, this).show();
        }
    }

    private void A() {
        if (t() == null) {
            org.games4all.android.e.c cVar = new org.games4all.android.e.c(this, 2);
            cVar.setTitle((int) R.string.g4a_resetMatchTitle);
            cVar.a((int) R.string.g4a_resetMatchMessage);
            cVar.a(0, (int) R.string.g4a_resetMatchAcceptButton);
            cVar.a(1, (int) R.string.g4a_resetMatchRejectButton);
            cVar.a(new h());
            cVar.show();
        }
    }

    class h implements DialogInterface.OnClickListener {
        h() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (i == 0) {
                GamePlayActivity.this.i();
            }
        }
    }

    class e implements a.C0013a {
        e() {
        }

        public void a(org.games4all.game.option.e eVar) {
            GamePlayActivity.this.d(eVar);
            GamePlayActivity.this.b(eVar);
            GamePlayActivity.this.e();
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        org.games4all.android.option.a aVar = new org.games4all.android.option.a(this, new e());
        aVar.setCancelable(false);
        aVar.show();
    }

    /* access modifiers changed from: package-private */
    public void b(org.games4all.game.option.e eVar) {
        this.u = eVar;
        this.a.a(eVar);
        try {
            c(eVar);
        } catch (IOException e2) {
            System.err.println("Could not save options: " + e2.getMessage());
            e2.printStackTrace();
        }
    }

    public org.games4all.game.option.e h() {
        return this.u;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        a(menu, 0, (int) R.string.g4a_menuReport);
        a(menu, 1, (int) R.string.g4a_menuLoad);
        a(menu, 2, (int) R.string.g4a_menuReset);
        a(menu, 3, (int) R.string.g4a_menuError);
        this.w = a(menu, 4, (int) R.string.g4a_menuScenarioStart);
        return true;
    }

    private MenuItem a(Menu menu, int i2, int i3) {
        MenuItem add = menu.add(0, i2, 0, i3);
        add.setOnMenuItemClickListener(this);
        return add;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        boolean z2;
        menu.getItem(0).setVisible(this.a.r() || org.games4all.android.b.a() || org.games4all.android.b.b());
        menu.getItem(1).setVisible(this.a.s());
        menu.getItem(2).setVisible(this.a.r());
        MenuItem item = menu.getItem(3);
        if (this.a.s() || org.games4all.android.b.a()) {
            z = true;
        } else {
            z = false;
        }
        item.setVisible(z);
        MenuItem item2 = menu.getItem(4);
        if (this.a.s() || org.games4all.android.b.a()) {
            z2 = true;
        } else {
            z2 = false;
        }
        item2.setVisible(z2);
        if (!this.a.r() && !this.a.s()) {
            y();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                B();
                return true;
            case 1:
                C();
                return true;
            case 2:
                D();
                return true;
            case 3:
                throw new RuntimeException("Just a test");
            case 4:
                if (this.v == null) {
                    this.w.setTitle((int) R.string.g4a_menuScenarioStop);
                    E();
                } else {
                    this.w.setTitle((int) R.string.g4a_menuScenarioStart);
                    F();
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void B() {
        Toast.makeText(this, "Generating Report...", 1).show();
        p().t().p();
        startActivityForResult(new Intent(this, ReportActivity.class), 1);
    }

    private void C() {
        org.games4all.android.e.c cVar = new org.games4all.android.e.c(this, 2);
        cVar.b((int) R.id.g4a_loadGameDialog);
        cVar.setTitle((int) R.string.g4a_loadDialogTitle);
        cVar.a((int) R.string.g4a_loadDialogMessage);
        EditText editText = new EditText(this);
        editText.setKeyListener(new DigitsKeyListener());
        editText.setId(R.id.g4a_loadGameDialogInput);
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        cVar.a(editText);
        cVar.a(0, (int) R.string.g4a_loadDialogAccept);
        cVar.a(1, (int) R.string.g4a_loadDialogCancel);
        cVar.a(new f(editText));
        cVar.show();
    }

    class f implements DialogInterface.OnClickListener {
        final /* synthetic */ EditText a;

        f(EditText editText) {
            this.a = editText;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (i == 0) {
                try {
                    GamePlayActivity.this.a(Integer.parseInt(this.a.getText().toString()));
                } catch (NumberFormatException e) {
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        new c(this, i2).execute(new Void[0]);
        this.b.b(PlayEvent.GAME_DOWNLOAD_INITIATED);
    }

    public void a(org.games4all.game.model.e<?, ?, ?> eVar, List<List<PlayerMove>> list) {
        this.d.a(eVar, new MatchResult(this.a.A().a(), this.a.g().g().a()), list);
        Toast.makeText(this, (int) R.string.g4a_loadGameSuccessToast, 1).show();
        this.b.b(PlayEvent.GAME_DOWNLOAD_SUCCEEDED);
    }

    public void a(String str, int i2) {
        org.games4all.android.e.c cVar = new org.games4all.android.e.c(this, 1);
        cVar.b((int) R.id.g4a_loadGameFailedDialog);
        Resources resources = getResources();
        cVar.a(resources.getString(R.string.g4a_loadGameFailure));
        String string = resources.getString(R.string.g4a_loadGameErrorCode, Integer.valueOf(i2));
        cVar.b(string + "\n" + resources.getString(R.string.g4a_loadGameErrorMsg, str));
        cVar.a(0, (int) R.string.g4a_acceptButton);
        cVar.show();
        cVar.a(new c(this.b));
    }

    class c implements DialogInterface.OnClickListener {
        final /* synthetic */ org.games4all.f.c a;

        c(org.games4all.f.c cVar) {
            this.a = cVar;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            this.a.b(PlayEvent.GAME_DOWNLOAD_FAILED);
        }
    }

    private void D() {
        org.games4all.android.e.c cVar = new org.games4all.android.e.c(this, 2);
        cVar.setTitle((int) R.string.g4a_playResetTitle);
        cVar.a((int) R.string.g4a_playResetMessage);
        cVar.a(0, (int) R.string.g4a_playResetAccept);
        cVar.a(1, (int) R.string.g4a_playResetCancel);
        cVar.a(new d());
        cVar.show();
    }

    class d implements DialogInterface.OnClickListener {
        d() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (i == 0) {
                GamePlayActivity.this.i();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.android.play.GamePlayActivity.a(org.games4all.android.test.b, boolean):void
     arg types: [org.games4all.android.test.b, int]
     candidates:
      org.games4all.android.play.GamePlayActivity.a(java.lang.String, org.games4all.game.option.e):java.lang.String
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, java.util.List<java.util.List<org.games4all.game.move.PlayerMove>>):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, org.games4all.match.Match):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, org.games4all.match.MatchResult):void
      org.games4all.android.play.GamePlayActivity.a(java.lang.String, int):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.model.e<?, ?, ?>, java.util.List<java.util.List<org.games4all.game.move.PlayerMove>>):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, java.lang.String):void
      org.games4all.android.activity.Games4AllActivity.a(org.games4all.logging.LogLevel, java.lang.String):void
      org.games4all.android.play.GamePlayActivity.a(org.games4all.android.test.b, boolean):void */
    private void E() {
        this.v = new AutoplayScenario();
        a(this.v, false);
    }

    private void F() {
        this.v.stop();
        this.v = null;
    }

    public void a(org.games4all.game.option.e eVar) {
        this.t = eVar;
        d(eVar);
        this.a.t().q();
        this.b.a(PlayState.PLAYING, PlayEvent.OPTIONS_CHANGED);
    }

    /* access modifiers changed from: package-private */
    public void i() {
        this.d.c();
        j();
        this.b.b(PlayEvent.RESET);
    }

    private String a(String str, org.games4all.game.option.e eVar) {
        return (H() + "-" + eVar.a()) + "." + str;
    }

    private String f(String str) {
        return (this.a.y() + "-" + H()) + "." + str;
    }

    public void j() {
        org.games4all.game.option.e z = this.a.z();
        deleteFile(a("game", z));
        deleteFile(a("moves", z));
        deleteFile(a("match", z));
        deleteFile(a("result", z));
        this.a.a((Match) null);
    }

    private void g(String str) {
        Runtime runtime = Runtime.getRuntime();
        runtime.gc();
        StringBuilder sb = new StringBuilder();
        sb.append("Memory at ").append(str).append(": ");
        sb.append(runtime.freeMemory()).append("/");
        sb.append(runtime.maxMemory()).append("/");
        sb.append(runtime.totalMemory());
        this.a.a().c(sb.toString());
    }

    public void k() {
        g("save");
        try {
            org.games4all.game.model.e<?, ?, ?> k2 = this.a.k();
            org.games4all.game.option.e z = this.a.z();
            if (k2 == null) {
                j();
            } else {
                a(this.a.f().f());
                a(k2);
                a(z, this.a.i());
                a(z, this.a.j());
                a(z, this.a.A());
            }
            if (this.t != null) {
                c(this.t);
                this.t = null;
                return;
            }
            c(z);
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0031 A[SYNTHETIC, Splitter:B:12:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055 A[SYNTHETIC, Splitter:B:19:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(org.games4all.game.option.e r7, java.lang.String r8) {
        /*
            r6 = this;
            r0 = 0
            r1 = 0
            java.io.FileOutputStream r1 = r6.openFileOutput(r8, r1)     // Catch:{ IOException -> 0x002e, all -> 0x004f }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x002e, all -> 0x004f }
            r2.<init>(r1)     // Catch:{ IOException -> 0x002e, all -> 0x004f }
            r2.writeObject(r7)     // Catch:{ IOException -> 0x0076, all -> 0x0073 }
            if (r2 == 0) goto L_0x0013
            r2.close()     // Catch:{ IOException -> 0x0014 }
        L_0x0013:
            return
        L_0x0014:
            r0 = move-exception
            java.lang.String r1 = "G4A"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Failed to close "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r1, r2, r0)
            goto L_0x0013
        L_0x002e:
            r1 = move-exception
        L_0x002f:
            if (r0 == 0) goto L_0x0013
            r0.close()     // Catch:{ IOException -> 0x0035 }
            goto L_0x0013
        L_0x0035:
            r0 = move-exception
            java.lang.String r1 = "G4A"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Failed to close "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r1, r2, r0)
            goto L_0x0013
        L_0x004f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0058:
            throw r0
        L_0x0059:
            r1 = move-exception
            java.lang.String r2 = "G4A"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to close "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3, r1)
            goto L_0x0058
        L_0x0073:
            r0 = move-exception
            r1 = r2
            goto L_0x0053
        L_0x0076:
            r0 = move-exception
            r0 = r2
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, java.lang.String):void");
    }

    /* access modifiers changed from: package-private */
    public void c(org.games4all.game.option.e eVar) {
        a(eVar, f("options"));
    }

    private String G() {
        return this.a.y() + ".options";
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037 A[SYNTHETIC, Splitter:B:12:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b A[SYNTHETIC, Splitter:B:19:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(org.games4all.util.SoftwareVersion r8) {
        /*
            r7 = this;
            java.lang.String r0 = "version"
            java.lang.String r0 = r7.f(r0)
            r1 = 0
            r2 = 0
            java.io.FileOutputStream r2 = r7.openFileOutput(r0, r2)     // Catch:{ IOException -> 0x0034, all -> 0x0055 }
            java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0034, all -> 0x0055 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x0034, all -> 0x0055 }
            r3.writeObject(r8)     // Catch:{ IOException -> 0x007c, all -> 0x0079 }
            if (r3 == 0) goto L_0x0019
            r3.close()     // Catch:{ IOException -> 0x001a }
        L_0x0019:
            return
        L_0x001a:
            r1 = move-exception
            java.lang.String r2 = "G4A"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to close "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x0019
        L_0x0034:
            r2 = move-exception
        L_0x0035:
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ IOException -> 0x003b }
            goto L_0x0019
        L_0x003b:
            r1 = move-exception
            java.lang.String r2 = "G4A"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to close "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x0019
        L_0x0055:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0059:
            if (r2 == 0) goto L_0x005e
            r2.close()     // Catch:{ IOException -> 0x005f }
        L_0x005e:
            throw r1
        L_0x005f:
            r2 = move-exception
            java.lang.String r3 = "G4A"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Failed to close "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r3, r0, r2)
            goto L_0x005e
        L_0x0079:
            r1 = move-exception
            r2 = r3
            goto L_0x0059
        L_0x007c:
            r1 = move-exception
            r1 = r3
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.android.play.GamePlayActivity.a(org.games4all.util.SoftwareVersion):void");
    }

    /* access modifiers changed from: package-private */
    public SoftwareVersion a(String str) {
        try {
            return (SoftwareVersion) new ObjectInputStream(openFileInput(str)).readObject();
        } catch (ClassNotFoundException e2) {
            return null;
        } catch (IOException e3) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public org.games4all.game.option.e b(String str) {
        try {
            org.games4all.game.option.e eVar = (org.games4all.game.option.e) new ObjectInputStream(openFileInput(str)).readObject();
            if (eVar.getClass() != this.a.p().a().getClass()) {
                return null;
            }
            return eVar;
        } catch (ClassNotFoundException e2) {
            return null;
        } catch (Exception e3) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void d(org.games4all.game.option.e eVar) {
        a(eVar, G());
        this.u = eVar;
    }

    /* access modifiers changed from: package-private */
    public org.games4all.game.option.e l() {
        return b(G());
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [org.games4all.game.model.d] */
    /* JADX WARN: Type inference failed for: r1v19, types: [org.games4all.game.model.d] */
    public boolean m() {
        g("load");
        ObjectInputStream objectInputStream = null;
        this.a.a((Match) null);
        try {
            SoftwareVersion a2 = a(f("version"));
            SoftwareVersion f2 = this.a.f().f();
            if (a2 == null || a2.b(f2) < 0) {
                System.err.println("version too old: " + a2 + ", current: " + f2);
                this.a.a(this.u);
                if (0 != 0) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e2) {
                        Log.e("G4A", "Failed to close 'game.dat'", e2);
                    }
                }
                return false;
            }
            String f3 = f("options");
            org.games4all.game.option.e b2 = b(f3);
            if (b2 == null) {
                System.err.println("Option file not found: " + f3);
                this.a.a(this.u);
                if (0 != 0) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e3) {
                        Log.e("G4A", "Failed to close 'game.dat'", e3);
                    }
                }
                return false;
            }
            this.a.a(b2);
            org.games4all.game.model.e<?, ?, ?> e4 = e(b2);
            List<List<PlayerMove>> g2 = g(b2);
            MatchResult f4 = f(b2);
            Match h2 = h(b2);
            if (e4 == null || g2 == null || f4 == null) {
                if (0 != 0) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e5) {
                        Log.e("G4A", "Failed to close 'game.dat'", e5);
                    }
                }
                return false;
            } else if (!b2.equals(e4.m().f().a())) {
                System.err.println("OPTIONS MISMATCH, LOAD FAILED!");
                System.err.println("loaded options: " + b2);
                System.err.println("model options:  " + e4.m().f().a());
                if (0 != 0) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e6) {
                        Log.e("G4A", "Failed to close 'game.dat'", e6);
                    }
                }
                return false;
            } else {
                this.a.a(h2);
                this.d.a(e4, f4, g2);
                if (0 != 0) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e7) {
                        Log.e("G4A", "Failed to close 'game.dat'", e7);
                    }
                }
                return true;
            }
        } catch (Exception e8) {
            e8.printStackTrace();
            if (0 != 0) {
                try {
                    objectInputStream.close();
                } catch (IOException e9) {
                    Log.e("G4A", "Failed to close 'game.dat'", e9);
                }
            }
            return false;
        } catch (Throwable th) {
            if (0 != 0) {
                try {
                    objectInputStream.close();
                } catch (IOException e10) {
                    Log.e("G4A", "Failed to close 'game.dat'", e10);
                }
            }
            throw th;
        }
    }

    private String H() {
        String c2 = this.a.g().c();
        if (c2 == null) {
            return "@offline";
        }
        return c2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x007a A[SYNTHETIC, Splitter:B:27:0x007a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(org.games4all.game.option.e r8, java.lang.Object r9, java.lang.String r10) {
        /*
            r7 = this;
            r3 = 0
            java.lang.String r0 = r7.a(r10, r8)
            if (r9 != 0) goto L_0x000b
            r7.deleteFile(r0)
        L_0x000a:
            return
        L_0x000b:
            r1 = 0
            java.io.FileOutputStream r1 = r7.openFileOutput(r0, r1)     // Catch:{ IOException -> 0x003c, all -> 0x0076 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x009f, all -> 0x0098 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x009f, all -> 0x0098 }
            r2.writeObject(r9)     // Catch:{ IOException -> 0x009f, all -> 0x0098 }
            r2.close()     // Catch:{ IOException -> 0x009f, all -> 0x0098 }
            r1 = 0
            if (r3 == 0) goto L_0x000a
            r1.close()     // Catch:{ IOException -> 0x0022 }
            goto L_0x000a
        L_0x0022:
            r1 = move-exception
            java.lang.String r2 = "G4A"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to close output: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x000a
        L_0x003c:
            r1 = move-exception
            r2 = r3
        L_0x003e:
            java.lang.String r3 = "G4A"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x009d }
            r4.<init>()     // Catch:{ all -> 0x009d }
            java.lang.String r5 = "Failed to save "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x009d }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x009d }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x009d }
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x009d }
            if (r2 == 0) goto L_0x000a
            r2.close()     // Catch:{ IOException -> 0x005c }
            goto L_0x000a
        L_0x005c:
            r1 = move-exception
            java.lang.String r2 = "G4A"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to close output: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x000a
        L_0x0076:
            r1 = move-exception
            r2 = r3
        L_0x0078:
            if (r2 == 0) goto L_0x007d
            r2.close()     // Catch:{ IOException -> 0x007e }
        L_0x007d:
            throw r1
        L_0x007e:
            r2 = move-exception
            java.lang.String r3 = "G4A"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Failed to close output: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r3, r0, r2)
            goto L_0x007d
        L_0x0098:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0078
        L_0x009d:
            r1 = move-exception
            goto L_0x0078
        L_0x009f:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.android.play.GamePlayActivity.a(org.games4all.game.option.e, java.lang.Object, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x008e A[SYNTHETIC, Splitter:B:29:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b0 A[SYNTHETIC, Splitter:B:36:0x00b0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object b(org.games4all.game.option.e r9, java.lang.String r10) {
        /*
            r8 = this;
            r6 = 0
            java.lang.String r0 = r8.a(r10, r9)
            java.io.FileInputStream r1 = r8.openFileInput(r0)     // Catch:{ IOException -> 0x0037, Exception -> 0x0072, all -> 0x00ac }
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x00da, Exception -> 0x00d5, all -> 0x00ce }
            r2.<init>(r1)     // Catch:{ IOException -> 0x00da, Exception -> 0x00d5, all -> 0x00ce }
            java.lang.Object r3 = r2.readObject()     // Catch:{ IOException -> 0x00da, Exception -> 0x00d5, all -> 0x00ce }
            r2.close()     // Catch:{ IOException -> 0x00da, Exception -> 0x00d5, all -> 0x00ce }
            r1 = 0
            if (r6 == 0) goto L_0x001b
            r1.close()     // Catch:{ IOException -> 0x001d }
        L_0x001b:
            r0 = r3
        L_0x001c:
            return r0
        L_0x001d:
            r1 = move-exception
            java.lang.String r2 = "G4A"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Failed to close input "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x001b
        L_0x0037:
            r1 = move-exception
            r2 = r6
        L_0x0039:
            java.lang.String r3 = "G4A"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r4.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r5 = "Failed to load "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00d3 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x00d3 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00d3 }
            android.util.Log.w(r3, r4, r1)     // Catch:{ all -> 0x00d3 }
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch:{ IOException -> 0x0058 }
        L_0x0056:
            r0 = r6
            goto L_0x001c
        L_0x0058:
            r1 = move-exception
            java.lang.String r2 = "G4A"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to close input "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x0056
        L_0x0072:
            r1 = move-exception
            r2 = r6
        L_0x0074:
            java.lang.String r3 = "G4A"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r4.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r5 = "Could not load object: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00d3 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x00d3 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00d3 }
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x00d3 }
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x0056
        L_0x0092:
            r1 = move-exception
            java.lang.String r2 = "G4A"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to close input "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x0056
        L_0x00ac:
            r1 = move-exception
            r2 = r6
        L_0x00ae:
            if (r2 == 0) goto L_0x00b3
            r2.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x00b3:
            throw r1
        L_0x00b4:
            r2 = move-exception
            java.lang.String r3 = "G4A"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Failed to close input "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r3, r0, r2)
            goto L_0x00b3
        L_0x00ce:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x00ae
        L_0x00d3:
            r1 = move-exception
            goto L_0x00ae
        L_0x00d5:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0074
        L_0x00da:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.android.play.GamePlayActivity.b(org.games4all.game.option.e, java.lang.String):java.lang.Object");
    }

    private void a(org.games4all.game.model.e<?, ?, ?> eVar) {
        a(eVar.p(), eVar, "model");
    }

    private void a(org.games4all.game.option.e eVar, List<List<PlayerMove>> list) {
        a(eVar, list, "moves");
    }

    private void a(org.games4all.game.option.e eVar, MatchResult matchResult) {
        a(eVar, matchResult, "result");
    }

    private void a(org.games4all.game.option.e eVar, Match match) {
        a(eVar, match, "match");
    }

    private org.games4all.game.model.e<?, ?, ?> e(org.games4all.game.option.e eVar) {
        return (org.games4all.game.model.e) b(eVar, "model");
    }

    private MatchResult f(org.games4all.game.option.e eVar) {
        return (MatchResult) b(eVar, "result");
    }

    private List<List<PlayerMove>> g(org.games4all.game.option.e eVar) {
        List<List<PlayerMove>> list = (List) b(eVar, "moves");
        if (list == null || list.size() == 0 || !(list.get(0) instanceof List)) {
            return new ArrayList();
        }
        return list;
    }

    private Match h(org.games4all.game.option.e eVar) {
        return (Match) b(eVar, "match");
    }

    public void n() {
        if (this.a.g().a()) {
            org.games4all.game.option.e z = this.a.z();
            this.s = new a(this);
            MatchResult matchResult = null;
            if (this.a.A() != null) {
                matchResult = this.a.j();
            }
            this.s.a(z.a(), z.b(), matchResult);
            return;
        }
        org.games4all.game.rating.i e2 = this.a.f().e();
        long a2 = this.a.z().a();
        org.games4all.b.a.b g2 = this.a.g().g();
        MatchResult j2 = this.a.j();
        Match A = this.a.A();
        if (!(A == null || j2.c() == null)) {
            e2.a(a2, g2, j2, A);
        }
        org.games4all.game.option.e z2 = this.a.z();
        a(new Match(-1, z2.a(), new GameSeed(z2.b())));
    }

    class a implements DialogInterface.OnClickListener {
        a() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (i == 0) {
                GamePlayActivity.this.n();
            } else {
                GamePlayActivity.this.o();
            }
        }
    }

    public void c(String str) {
        String str2;
        a aVar = new a();
        this.s = null;
        Resources resources = getResources();
        if (!str.startsWith("g4a_")) {
            str2 = str;
        } else if (str.equals("g4a_oldClientVersion")) {
            org.games4all.android.d.a.a(this, (int) R.string.g4a_loadMatchErrorTitle, str, aVar);
            return;
        } else {
            str2 = new org.games4all.android.b.e(this).b(str);
        }
        org.games4all.android.e.c cVar = new org.games4all.android.e.c(this, 2);
        cVar.setTitle((int) R.string.g4a_loadMatchErrorTitle);
        cVar.b(resources.getString(R.string.g4a_loadMatchErrorMsg, str2));
        cVar.a(0, (int) R.string.g4a_loadMatchErrorRetry);
        cVar.a(1, (int) R.string.g4a_loadMatchErrorOffline);
        cVar.a(aVar);
        cVar.setCancelable(false);
        cVar.show();
    }

    /* access modifiers changed from: package-private */
    public void o() {
        this.b.b(PlayEvent.GO_OFFLINE);
    }

    public void a(Match match) {
        this.s = null;
        org.games4all.b.a.b g2 = this.a.g().g();
        long a2 = this.a.z().a();
        org.games4all.game.rating.i e2 = this.a.f().e();
        Match A = this.a.A();
        if (A == null) {
            b(match);
        } else if (this.d.j().c() != null) {
            org.games4all.android.c.c cVar = new org.games4all.android.c.c(this, g2, a2, e2, this.d.j(), A);
            cVar.setOnDismissListener(new b(match));
            cVar.show();
        }
    }

    class b implements DialogInterface.OnDismissListener {
        final /* synthetic */ Match a;

        b(Match match) {
            this.a = match;
        }

        public void onDismiss(DialogInterface dialogInterface) {
            GamePlayActivity.this.b(this.a);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Match match) {
        this.a.a(match);
        this.d.a(this.a.z(), match.a(), match.b());
        k();
        this.b.a(PlayState.DOWNLOADING_MATCH, PlayEvent.MATCH_LOADED);
    }

    public void b(boolean z) {
        if (this.j != null) {
            this.j.setBackgroundResource(z ? R.drawable.g4a_button_connected : R.drawable.g4a_button_disconnected);
        }
    }

    public void onStart() {
        super.onStart();
        if (!org.games4all.android.b.d()) {
            try {
                u.a(this, (String) getPackageManager().getApplicationInfo(getComponentName().getPackageName(), 128).metaData.get("FLURRY_ID"));
                InstallReceiver.a(this);
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void onStop() {
        super.onStop();
        if (!org.games4all.android.b.d()) {
            u.a(this);
        }
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
        }
        return true;
    }
}
