package org.games4all.android.card;

import android.graphics.Point;
import android.graphics.Rect;
import org.games4all.android.a.g;
import org.games4all.card.Card;
import org.games4all.card.b;

public class a implements j {
    static int a = 0;
    private final b b;
    private final g c;
    private h d;
    private g e;
    private g f;
    private final Rect g = new Rect();

    public a(b bVar, g gVar) {
        this.b = bVar;
        this.c = gVar;
    }

    public void a(int i) {
        this.e.b(i + 1);
        this.f.b(i);
    }

    public void a(h hVar) {
        this.d = hVar;
        this.e = new g(hVar, null);
        this.e.a(new d(this, 0));
        this.e.b(a + 1);
        this.e.a(Card.a);
        this.c.a(this.e);
        this.f = new g(hVar, null);
        this.f.a(new d(this, 1));
        this.f.b(a);
        this.f.a(Card.a);
        this.c.a(this.f);
    }

    public void a(int i, int i2, int i3, int i4) {
        this.g.set(i, i2, i3, i4);
        int a2 = (((i3 - i) - this.d.a()) / 2) + i;
        int b2 = (((i4 - i2) - this.d.b()) / 2) + i2;
        this.e.a(a2, b2);
        this.f.a(a2, b2);
    }

    public g b(int i) {
        if (i == 0) {
            return this.e;
        }
        return null;
    }

    public b a() {
        return this.b;
    }

    public void b() {
        this.e.a(true);
    }

    public void c() {
        this.e.a(false);
    }

    public Card c(int i) {
        if (i == 0 || i == 1) {
            return Card.a;
        }
        return null;
    }

    public int d(int i) {
        if (i == 0) {
            return a + 1;
        }
        if (i == 1) {
            return a;
        }
        throw new IllegalArgumentException(String.valueOf(i));
    }

    public Rect d() {
        return this.g;
    }

    public g e() {
        return this.e;
    }

    public Point f() {
        Rect d2 = d();
        return new Point(d2.centerX(), d2.centerY());
    }

    public void a(boolean z) {
        this.f.c(z);
        this.e.c(z);
    }
}
