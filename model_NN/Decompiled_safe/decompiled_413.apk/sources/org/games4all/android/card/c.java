package org.games4all.android.card;

import android.view.View;
import org.games4all.android.e.d;
import org.games4all.card.Suit;

public class c extends d implements View.OnClickListener {
    private final a a;

    public interface a {
        void a(Suit suit);
    }

    public void onClick(View view) {
        this.a.a((Suit) view.getTag());
        dismiss();
    }
}
