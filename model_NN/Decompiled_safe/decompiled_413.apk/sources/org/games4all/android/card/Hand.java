package org.games4all.android.card;

import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.List;
import org.games4all.android.a.g;
import org.games4all.android.a.i;
import org.games4all.android.a.l;
import org.games4all.android.a.m;
import org.games4all.card.Card;
import org.games4all.card.Cards;

public class Hand implements j {
    private final org.games4all.card.b a;
    private final g b;
    private final Drawable c;
    private h d;
    private final Cards e;
    private final List<g> f;
    private final List<d> g;
    private final Rect h = new Rect();
    private final Rect i = new Rect();
    private final Orientation j;
    private int k;
    private m l;
    private int m;
    private org.games4all.android.b.a n;
    private final org.games4all.card.d o;
    private int p;
    private int q;

    public enum Orientation {
        HORIZONTAL,
        VERTICAL,
        STACK
    }

    public Hand(org.games4all.card.b bVar, g gVar, Drawable drawable, Orientation orientation) {
        this.a = bVar;
        this.b = gVar;
        this.c = drawable;
        this.j = orientation;
        this.e = new Cards();
        this.f = new ArrayList();
        this.g = new ArrayList();
        this.o = new org.games4all.card.d();
        this.k = 100;
        this.m = 80;
    }

    public org.games4all.android.b.a b() {
        return this.n;
    }

    public void a(String str) {
        c();
        this.l.b(str);
        g();
    }

    public void a(int i2) {
        this.m = i2;
    }

    public m c() {
        if (this.l == null) {
            this.l = new m(this.c);
            this.l.b(1000);
            this.b.a(this.l);
        }
        return this.l;
    }

    public void a(h hVar) {
        this.d = hVar;
    }

    public void a(int i2, int i3, int i4, int i5, int i6, int i7) {
        this.p = i6;
        this.q = i7;
        this.h.set(i2, i3, i4, i5);
        f();
    }

    public void b(int i2, int i3, int i4, int i5, int i6, int i7) {
        this.p = i6;
        this.q = i7;
        this.h.set(i2, i3, i4, i5);
        List<Point> l2 = l();
        int size = l2.size();
        for (int i8 = 0; i8 < size; i8++) {
            g gVar = this.f.get(i8);
            i iVar = new i(gVar, gVar.e(), l2.get(i8), 1000);
            iVar.a(true);
            this.b.a(iVar, (Runnable) null, 0);
        }
        this.b.a(new i(this.l, this.l.e(), k(), 1000), (Runnable) null, 0);
    }

    public Rect d() {
        return this.h;
    }

    public void f() {
        p();
        a(l());
        g();
    }

    public void g() {
        if (this.l != null) {
            this.l.o();
            Point k2 = k();
            this.l.a(k2.x, k2.y);
        }
    }

    private Point k() {
        int i2;
        int a2;
        int b2 = this.l.b();
        int max = Math.max(((this.h.left + this.h.right) - b2) / 2, 2);
        if (max + b2 > this.p - 2) {
            i2 = (this.p - 2) - b2;
        } else {
            i2 = max;
        }
        if (this.m == 80) {
            a2 = (this.h.bottom - this.l.a()) - (this.d.b() / 16);
        } else {
            a2 = this.h.top + (((this.h.bottom - this.h.top) - this.l.a()) / 2);
        }
        return new Point(i2, a2);
    }

    /* access modifiers changed from: package-private */
    public void a(List<Point> list) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            g gVar = this.f.get(i2);
            gVar.a(this.e.a(i2));
            gVar.a(this.g.get(i2));
            gVar.c(true);
            gVar.b(this.k + this.o.a(i2));
            Point point = list.get(i2);
            gVar.a(point.x, point.y);
        }
        int size = list.size();
        while (true) {
            int i3 = size;
            if (i3 < this.f.size()) {
                this.f.get(i3).c(false);
                size = i3 + 1;
            } else {
                return;
            }
        }
    }

    public int d(int i2) {
        return this.k + this.o.a(i2);
    }

    private void a(List<Point> list, int i2, int i3) {
        l lVar = new l();
        int size = list.size();
        for (int i4 = 0; i4 < size; i4++) {
            g gVar = this.f.get(i4);
            gVar.a(this.e.a(i4));
            gVar.a(this.g.get(i4));
            gVar.c(true);
            Point d2 = gVar.d();
            Point point = list.get(i4);
            if (!point.equals(d2)) {
                lVar.a(new i(gVar, d2, point, i2));
            }
        }
        for (int i5 = size; i5 < this.f.size(); i5++) {
            this.f.get(i5).c(false);
        }
        this.b.a(lVar, new e(list), (long) i3);
    }

    class e implements Runnable {
        final /* synthetic */ List a;

        e(List list) {
            this.a = list;
        }

        public void run() {
            Hand.this.a(this.a);
        }
    }

    private List<Point> l() {
        Point[] o2;
        switch (this.j) {
            case HORIZONTAL:
                o2 = m();
                break;
            case VERTICAL:
                o2 = n();
                break;
            case STACK:
                o2 = o();
                break;
            default:
                throw new RuntimeException(String.valueOf(this.j));
        }
        ArrayList arrayList = new ArrayList(o2.length);
        this.o.a(this.e);
        for (int i2 = 0; i2 < o2.length; i2++) {
            arrayList.add(o2[this.o.a(i2)]);
        }
        return arrayList;
    }

    private Point[] m() {
        int i2;
        int i3;
        int size = this.e.size();
        Point[] pointArr = new Point[size];
        int a2 = this.d.a();
        int b2 = this.d.b();
        int i4 = this.h.right - this.h.left;
        int i5 = this.h.bottom - this.h.top;
        if (size == 0) {
            i3 = 0;
            i2 = 0;
        } else if (size == 1) {
            i2 = a2;
            i3 = 0;
        } else {
            int i6 = (i4 - a2) / (size - 1);
            if (i6 - a2 > a2 / 5) {
                i6 = (a2 / 5) + a2;
            }
            int i7 = i6;
            i2 = a2 + ((size - 1) * i6);
            i3 = i7;
        }
        int i8 = ((i5 - b2) / 2) + this.h.top;
        int i9 = ((i4 - i2) / 2) + this.h.left;
        for (int i10 = 0; i10 < size; i10++) {
            pointArr[i10] = new Point((i10 * i3) + i9, i8);
        }
        this.i.set(i9, i8, i9 + i2, b2 + i8);
        return pointArr;
    }

    private Point[] n() {
        int i2;
        int i3;
        int size = this.e.size();
        Point[] pointArr = new Point[size];
        int a2 = this.d.a();
        int b2 = this.d.b();
        int i4 = this.h.right - this.h.left;
        int i5 = this.h.bottom - this.h.top;
        if (size == 0) {
            i3 = 0;
            i2 = 0;
        } else if (size == 1) {
            i2 = b2;
            i3 = 0;
        } else {
            int i6 = (i5 - b2) / (size - 1);
            if (i6 - b2 > b2 / 5) {
                i6 = (b2 / 5) + b2;
            }
            int i7 = i6;
            i2 = b2 + ((size - 1) * i6);
            i3 = i7;
        }
        int i8 = ((i4 - a2) / 2) + this.h.left;
        int i9 = ((i5 - i2) / 2) + this.h.top;
        for (int i10 = 0; i10 < size; i10++) {
            pointArr[i10] = new Point(i8, (i10 * i3) + i9);
        }
        this.i.set(i8, i9, a2 + i8, i9 + i2);
        return pointArr;
    }

    private Point[] o() {
        int size = this.e.size();
        Point[] pointArr = new Point[size];
        int a2 = this.d.a();
        int b2 = this.d.b();
        int i2 = this.h.right - this.h.left;
        int i3 = this.h.bottom - this.h.top;
        int i4 = ((i2 - a2) / 2) + this.h.left;
        int i5 = ((i3 - b2) / 2) + this.h.top;
        for (int i6 = 0; i6 < size; i6++) {
            pointArr[i6] = new Point(i4, i5);
        }
        return pointArr;
    }

    private void p() {
        boolean z;
        int size = this.e.size();
        while (this.f.size() < size) {
            int size2 = this.f.size();
            c cVar = new c(this.d, Card.b());
            this.b.a(cVar);
            this.f.add(cVar);
            this.g.add(new d(this, size2));
        }
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            g gVar = this.f.get(i2);
            if (i2 < size) {
                z = true;
            } else {
                z = false;
            }
            gVar.c(z);
        }
    }

    class c extends g {
        c(h hVar, Card card) {
            super(hVar, card);
        }

        public org.games4all.android.b.a l() {
            return Hand.this.b();
        }
    }

    public void a(Cards cards) {
        this.e.a(cards);
        p();
        f();
    }

    public Cards h() {
        return this.e;
    }

    public Card c(int i2) {
        if (i2 < 0 || i2 >= this.e.size()) {
            return null;
        }
        return this.e.a(i2);
    }

    public void i() {
        if (this.j == Orientation.STACK) {
            this.f.get(this.f.size() - 1).a(true);
            return;
        }
        for (g a2 : this.f) {
            a2.a(true);
        }
    }

    public void j() {
        for (g a2 : this.f) {
            a2.a(false);
        }
    }

    public int a(int i2, int i3) {
        int i4;
        int i5;
        int i6 = -1;
        int i7 = 0;
        int i8 = Integer.MIN_VALUE;
        while (i7 < this.e.size()) {
            int d2 = d(i7);
            if (!this.f.get(i7).d(i2, i3) || d2 <= i8) {
                i4 = i8;
                i5 = i6;
            } else {
                i4 = d2;
                i5 = i7;
            }
            i7++;
            i6 = i5;
            i8 = i4;
        }
        return i6;
    }

    public g b(int i2) {
        return this.f.get(i2);
    }

    public org.games4all.card.b a() {
        return this.a;
    }

    public org.games4all.card.e e(int i2) {
        return new org.games4all.card.e(this.a, i2);
    }

    public void a(Card card, int i2, int i3, int i4) {
        g gVar = this.f.get(i3);
        List<Point> l2 = l();
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(l2);
        l2.remove(i2);
        l2.add(i3, new Point(this.f.get(i2).d()));
        this.e.remove(i2);
        this.e.add(i3, card);
        int size = this.e.size();
        for (int i5 = 0; i5 < size; i5++) {
            Point point = (Point) arrayList.get(i5);
            g gVar2 = this.f.get(i5);
            Point point2 = l2.get(i5);
            gVar2.a((Card) this.e.get(i5));
            if (gVar2 == gVar) {
                gVar2.b(1000);
                gVar2.a(255);
            } else {
                gVar2.b(d(i5));
                gVar2.a(0);
            }
            this.b.a(new i(gVar2, point2, point, i4), new a(gVar2, point, i5), 0);
        }
    }

    class a implements Runnable {
        final /* synthetic */ g a;
        final /* synthetic */ Point b;
        final /* synthetic */ int c;

        a(g gVar, Point point, int i) {
            this.a = gVar;
            this.b = point;
            this.c = i;
        }

        public void run() {
            this.a.a(this.b);
            this.a.a(0);
            this.a.b(Hand.this.d(this.c));
        }
    }

    public void a(int i2, Card card, Point point, Card card2, int i3, int i4) {
        List<Point> l2 = l();
        l2.add(i2, point);
        this.e.add(i2, card2);
        p();
        List<Point> l3 = l();
        int size = this.e.size();
        for (int i5 = 0; i5 < size; i5++) {
            Point point2 = l3.get(i5);
            int d2 = d(i5);
            g gVar = this.f.get(i5);
            if (i5 == i2) {
                gVar.a(255);
                gVar.b(1000);
                gVar.a(card);
                if (!card.equals(card2)) {
                    this.b.a(new k(gVar, card2, i3 / 2), (Runnable) null, (long) ((i3 / 4) + i4));
                }
            } else {
                gVar.b(d2);
                gVar.a((Card) this.e.get(i5));
            }
            gVar.a(this.g.get(i5));
            this.b.a(new i(gVar, l2.get(i5), point2, i3), new b(gVar, point2, d2), (long) i4);
        }
    }

    class b implements Runnable {
        final /* synthetic */ g a;
        final /* synthetic */ Point b;
        final /* synthetic */ int c;

        b(g gVar, Point point, int i) {
            this.a = gVar;
            this.b = point;
            this.c = i;
        }

        public void run() {
            this.a.a(this.b);
            this.a.a(0);
            this.a.b(this.c);
        }
    }

    public void a(int i2, int i3, int i4) {
        List<Point> l2 = l();
        l2.remove(i2);
        this.e.b(i2);
        List<Point> l3 = l();
        int size = this.e.size();
        for (int i5 = 0; i5 < size; i5++) {
            g gVar = this.f.get(i5);
            gVar.a(l2.get(i5));
            gVar.a((Card) this.e.get(i5));
            gVar.a(this.g.get(i5));
            gVar.b(this.k + this.o.a(i5));
        }
        a(l3, i3, i4);
    }

    public g e() {
        return this.f.get(this.e.size() - 1);
    }

    public String toString() {
        return "Hand[id=" + this.a + ",cards=" + this.e + ",bounds=" + this.h + ",vw=" + this.p + ",vh=" + this.q + "]";
    }
}
