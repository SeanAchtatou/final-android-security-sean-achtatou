package org.games4all.android.card;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import org.games4all.android.b.e;

public class f {
    private final String a;
    private final int b;
    private final int c;

    public f(String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    private Bitmap a(Context context, String str) {
        return ((BitmapDrawable) new e(context).a(str + this.a)).getBitmap();
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public Bitmap a(Context context) {
        return a(context, "deck");
    }

    public Bitmap b(Context context) {
        return a(context, "joker");
    }

    public Bitmap c(Context context) {
        return a(context, "back");
    }

    public Bitmap d(Context context) {
        return a(context, "empty");
    }

    public Bitmap e(Context context) {
        return a(context, "shadow");
    }
}
