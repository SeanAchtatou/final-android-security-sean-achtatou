package org.games4all.android.card;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.os.Handler;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import org.games4all.android.a.g;
import org.games4all.android.a.h;
import org.games4all.android.a.i;
import org.games4all.android.a.j;
import org.games4all.android.a.m;
import org.games4all.android.a.n;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.card.Hand;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.card.Card;
import org.games4all.card.Cards;

public class e extends ViewGroup {
    static final /* synthetic */ boolean a = (!e.class.desiredAssertionStatus());
    private static boolean d;
    private static h e;
    private static org.games4all.android.a.b f;
    private static org.games4all.android.a.b g;
    private static int n;
    private final Games4AllActivity b;
    private boolean c;
    private org.games4all.android.a.a h;
    private org.games4all.a.a i;
    private List<j> j;
    private g k;
    private boolean l;
    private Handler m;
    private Hand[] o;
    private int p;
    private int q = -1;
    private boolean r;
    private d s;

    public interface d {
        boolean a(int i);
    }

    public e(Games4AllActivity games4AllActivity, org.games4all.a.a aVar, int i2) {
        super(games4AllActivity);
        this.b = games4AllActivity;
        if (!d) {
            e = new h(games4AllActivity);
            d = true;
        }
        a(aVar, i2);
    }

    private void a(org.games4all.a.a aVar, int i2) {
        setWillNotDraw(false);
        this.i = aVar;
        this.j = new ArrayList();
        this.k = new g(aVar);
        Resources resources = getResources();
        float applyDimension = TypedValue.applyDimension(1, 6.0f, resources.getDisplayMetrics());
        Path path = new Path();
        path.moveTo(applyDimension * 1.0f, applyDimension * 4.0f);
        path.lineTo(applyDimension * 1.0f, applyDimension * 2.0f);
        path.lineTo(applyDimension * 0.0f, applyDimension * 2.0f);
        path.lineTo(applyDimension * 2.0f, applyDimension * 0.0f);
        path.lineTo(applyDimension * 4.0f, applyDimension * 2.0f);
        path.lineTo(3.0f * applyDimension, applyDimension * 2.0f);
        path.lineTo(3.0f * applyDimension, applyDimension * 4.0f);
        path.close();
        Paint paint = new Paint();
        Paint paint2 = new Paint();
        paint.setColor(-6496257);
        paint.setStyle(Paint.Style.FILL);
        paint2.setColor(-16777216);
        paint2.setStrokeWidth(this.b.getResources().getDisplayMetrics().density * 1.0f);
        paint2.setAntiAlias(true);
        paint2.setStrokeJoin(Paint.Join.ROUND);
        paint2.setStyle(Paint.Style.STROKE);
        g = new n(path, paint, paint2);
        this.m = new Handler();
        this.o = new Hand[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            this.o[i3] = new Hand(new org.games4all.card.b("hand", i3), this.k, resources.getDrawable(R.drawable.g4a_name_frame), c(i3));
            this.o[i3].c().a("-");
            this.o[i3].c().a(resources.getDimension(R.dimen.g4a_nameLabelSize));
            this.o[i3].a(e);
            a(this.o[i3]);
        }
        this.h = new org.games4all.android.a.a(g);
        this.h.c(false);
        this.h.a(255);
        this.h.b(400);
        this.k.a(this.h);
        f = new h(resources, R.drawable.g4a_droid);
    }

    /* access modifiers changed from: protected */
    public org.games4all.a.a h() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public Hand.Orientation c(int i2) {
        return i2 % 2 == 0 ? Hand.Orientation.HORIZONTAL : Hand.Orientation.VERTICAL;
    }

    /* access modifiers changed from: protected */
    public boolean d(int i2) {
        if (this.s != null) {
            return this.s.a(i2);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(j jVar, int i2) {
        if (this.c && this.q != -1) {
            if (i2 != this.q) {
                b(e(0), this.q);
            }
            this.q = -1;
        }
        jVar.b(i2).a(255);
    }

    /* access modifiers changed from: protected */
    public void a(j jVar, int i2, Point point) {
        g b2 = jVar.b(i2);
        if (b2 == null) {
            System.err.println("warning, no sprite at " + i2);
        } else if (b2.e().y - point.y <= m().c() / 5) {
            if (!(!this.c || this.q == -1 || this.q == i2)) {
                b(e(0), this.q);
                this.q = -1;
            }
            b(jVar, i2);
        } else if (this.r) {
            if (this.c && this.q != -1) {
                throw new AssertionError();
            } else if (d(i2)) {
                this.r = false;
            } else {
                b(e(0), i2);
            }
        } else if (this.c && this.q != i2) {
            if (this.q != -1) {
                b(e(0), this.q);
            }
            this.q = i2;
        }
    }

    public void b(j jVar, int i2) {
        if (jVar.c(i2) == null) {
            System.err.println("Cannot undo drag: " + i2);
            return;
        }
        g b2 = jVar.b(i2);
        i().a(i.a(b2, b2.d(), b2.e(), 300), new c(b2, jVar, i2), 0);
    }

    class c implements Runnable {
        final /* synthetic */ g a;
        final /* synthetic */ j b;
        final /* synthetic */ int c;

        c(g gVar, j jVar, int i) {
            this.a = gVar;
            this.b = jVar;
            this.c = i;
        }

        public void run() {
            this.a.a(0);
            this.a.b(this.b.d(this.c));
        }
    }

    public g i() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public org.games4all.android.option.d j() {
        return this.b.p().e();
    }

    public Hand e(int i2) {
        return this.o[i2];
    }

    public void a(int i2, String str, boolean z) {
        m c2 = this.o[i2].c();
        if (z) {
            c2.a(f);
            c2.n().bottom = 2;
        } else {
            c2.a((org.games4all.android.a.b) null);
            c2.n().bottom = 6;
        }
        c2.a(str);
        onLayout(false, getLeft(), getTop(), getRight(), getBottom());
        invalidate();
    }

    public void b(int i2, Cards cards) {
        this.o[i2].a(cards);
        invalidate();
    }

    public g a(int i2, int i3) {
        return this.o[i2].b(i3);
    }

    /* access modifiers changed from: protected */
    public int k() {
        return this.p;
    }

    /* access modifiers changed from: protected */
    public void l() {
        for (Hand c2 : this.o) {
            c2.c().c().setState(new int[0]);
        }
    }

    public h m() {
        return e;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        float f2;
        m c2 = e(this.p).c();
        Point point = new Point(c2.d());
        point.y += (c2.a() - this.h.a()) / 2;
        switch (this.p) {
            case 0:
                point.x -= this.h.b() + 5;
                f2 = 90.0f;
                break;
            case 1:
                point.x = c2.b() + 5 + point.x;
                f2 = -90.0f;
                break;
            case 2:
                point.x -= this.h.b() + 5;
                f2 = 90.0f;
                break;
            case 3:
                point.x -= this.h.b() + 5;
                f2 = 90.0f;
                break;
            default:
                throw new RuntimeException(String.valueOf(this.p));
        }
        l();
        if (!z || !this.h.j()) {
            this.h.c(true);
            this.h.a(f2);
            this.h.a(point);
            invalidate();
            n();
            return;
        }
        int a2 = j().a(100, 900);
        i iVar = new i(this.h, this.h.d(), point, a2);
        j jVar = new j(this.h, this.h.c(), f2, a2);
        g i2 = i();
        i2.a(iVar, new b(), 0);
        i2.a(jVar, (Runnable) null, 0);
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            e.this.n();
        }
    }

    public void b(int i2) {
        this.p = i2;
        a(true);
    }

    /* access modifiers changed from: package-private */
    public void n() {
        e(this.p).c().c().setState(new int[]{16842914});
    }

    public void o() {
        this.h.c(false);
        l();
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void a(j jVar) {
        this.j.add(jVar);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.k.a(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.l) {
            this.i.b();
            this.l = false;
        }
        if (this.k.a(motionEvent)) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    private j a(org.games4all.card.b bVar) {
        for (j next : this.j) {
            if (bVar.equals(next.a())) {
                return next;
            }
        }
        throw new RuntimeException("Cannot find container: " + bVar);
    }

    public void a(Hand hand, Card card, int i2, int i3) {
        hand.a(card, i2, i3, j().a(50, 1000));
    }

    public void a(org.games4all.card.e eVar) {
        j a2 = a(eVar.a());
        if (!(a2 instanceof Hand)) {
            throw new RuntimeException("Slots can only be deleted from hands");
        }
        Hand hand = (Hand) a2;
        if (hand == this.o[0] && this.q != -1) {
            int b2 = eVar.b();
            if (b2 == this.q) {
                this.q = -1;
            } else if (b2 < this.q) {
                this.q--;
            }
        }
        hand.a(eVar.b(), j().a(50, 1000), j().a(30, 600));
    }

    public void f(int i2) {
        if (i2 != 0) {
            this.i.a();
            int i3 = n + 1;
            n = i3;
            if (i2 != Integer.MAX_VALUE) {
                this.m.postDelayed(new a(i3), (long) i2);
            }
            this.l = true;
        }
    }

    class a implements Runnable {
        final /* synthetic */ int a;

        a(int i) {
            this.a = i;
        }

        public void run() {
            e.this.g(this.a);
        }
    }

    /* access modifiers changed from: package-private */
    public void g(int i2) {
        if (i2 == n && this.l) {
            this.i.b();
            this.l = false;
        }
    }

    public void p() {
        this.k.a();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
    }

    public void a(org.games4all.android.b.a aVar) {
        org.games4all.android.e.e eVar = new org.games4all.android.e.e(this.b);
        eVar.a(aVar);
        eVar.show();
    }
}
