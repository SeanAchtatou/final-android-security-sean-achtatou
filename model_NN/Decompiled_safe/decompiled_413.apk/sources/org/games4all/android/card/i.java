package org.games4all.android.card;

import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.view.View;
import android.widget.LinearLayout;
import org.games4all.android.a.g;
import org.games4all.android.card.e;
import org.games4all.android.e.b;

public class i<T extends e> extends org.games4all.android.a implements b {
    private final T a;
    private final LinearLayout b;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public i(org.games4all.android.activity.Games4AllActivity r5, org.games4all.a.a r6, T r7) {
        /*
            r4 = this;
            r4.<init>(r5, r6)
            r4.a = r7
            android.widget.LinearLayout r0 = new android.widget.LinearLayout
            r0.<init>(r5)
            r4.b = r0
            android.widget.LinearLayout r0 = r4.b
            r1 = 1
            r0.setOrientation(r1)
            org.games4all.android.card.i$a r0 = new org.games4all.android.card.i$a
            r0.<init>()
            android.graphics.drawable.PaintDrawable r1 = new android.graphics.drawable.PaintDrawable
            r1.<init>()
            android.graphics.drawable.shapes.RectShape r2 = new android.graphics.drawable.shapes.RectShape
            r2.<init>()
            r1.setShape(r2)
            r1.setShaderFactory(r0)
            android.widget.LinearLayout r0 = r4.b
            r0.setBackgroundDrawable(r1)
            if (r7 == 0) goto L_0x003f
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r1 = -1
            r2 = -2
            r3 = 1065353216(0x3f800000, float:1.0)
            r0.<init>(r1, r2, r3)
            r7.setLayoutParams(r0)
            android.widget.LinearLayout r0 = r4.b
            r0.addView(r7)
        L_0x003f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.android.card.i.<init>(org.games4all.android.activity.Games4AllActivity, org.games4all.a.a, org.games4all.android.card.e):void");
    }

    class a extends ShapeDrawable.ShaderFactory {
        a() {
        }

        public Shader resize(int i, int i2) {
            return new LinearGradient((float) (i / 2), (float) (i2 / 8), (float) (i / 2), (float) i2, -1575945, -16731393, Shader.TileMode.CLAMP);
        }
    }

    public T j() {
        return this.a;
    }

    public View a() {
        return this.b;
    }

    public void d() {
        this.a.p();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void d(int r2) {
        /*
            r1 = this;
            T r0 = r1.a
            r0.f(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.android.card.i.d(int):void");
    }

    public g i() {
        return this.a.i();
    }
}
