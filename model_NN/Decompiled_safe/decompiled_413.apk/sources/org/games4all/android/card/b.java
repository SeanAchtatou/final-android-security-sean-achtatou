package org.games4all.android.card;

import android.graphics.Point;
import android.graphics.Rect;
import org.games4all.android.a.g;
import org.games4all.android.a.i;
import org.games4all.card.Card;
import org.games4all.card.Cards;

public class b implements j {
    static int a = 0;
    private final org.games4all.card.b b;
    private final g c;
    private h d;
    private g e;
    private g f;
    private final Cards g = new Cards();
    private final Rect h = new Rect();

    public b(org.games4all.card.b bVar, g gVar) {
        this.b = bVar;
        this.c = gVar;
    }

    public void a(int i) {
        this.e.b(i + 1);
        this.f.b(i);
    }

    public void a(h hVar) {
        this.d = hVar;
        this.e = new g(hVar, null);
        this.e.a(new d(this, 0));
        this.e.b(a + 1);
        this.c.a(this.e);
        this.f = new g(hVar, null);
        this.f.a(new d(this, 1));
        this.f.b(a);
        this.c.a(this.f);
    }

    public void a(int i, int i2, int i3, int i4) {
        this.h.set(i, i2, i3, i4);
        int a2 = (((i3 - i) - this.d.a()) / 2) + i;
        int b2 = (((i4 - i2) - this.d.b()) / 2) + i2;
        this.e.a(a2, b2);
        this.f.a(a2, b2);
    }

    public g b(int i) {
        if (i == 0) {
            return this.e;
        }
        return null;
    }

    public org.games4all.card.b a() {
        return this.b;
    }

    public void b() {
        this.e.a(true);
    }

    public void c() {
        this.e.a(false);
    }

    public void f() {
        this.g.b();
        g();
    }

    public void a(Cards cards) {
        this.g.a(cards);
        g();
    }

    public void g() {
        int size = this.g.size();
        if (size > 0) {
            this.e.a((Card) this.g.get(size - 1));
        } else {
            this.e.a((Card) null);
        }
        if (size > 1) {
            this.f.a((Card) this.g.get(size - 2));
        } else {
            this.f.a((Card) null);
        }
    }

    public Card c(int i) {
        if (i == 0) {
            return this.e.c();
        }
        if (i == 1) {
            return this.f.c();
        }
        return null;
    }

    public int d(int i) {
        if (i == 0) {
            return a + 1;
        }
        if (i == 1) {
            return a;
        }
        throw new IllegalArgumentException(String.valueOf(i));
    }

    public Rect d() {
        return this.h;
    }

    public g e() {
        return this.e;
    }

    public Cards h() {
        return this.g;
    }

    public Point i() {
        Rect d2 = d();
        return new Point(d2.centerX(), d2.centerY());
    }

    public void a(Card card, Card card2, Point point, int i, int i2) {
        this.f.a(this.e.c());
        this.e.a(card);
        this.e.c(point.x, point.y);
        this.e.b(1000);
        g gVar = this.e;
        this.c.a(new i(this.e, point, this.e.e(), i), new a(gVar), 0);
        if (!card.equals(card2)) {
            this.c.a(new k(this.e, card2, i / 2), (Runnable) null, (long) ((i / 4) + i2));
        }
        this.g.add(card2);
    }

    class a implements Runnable {
        final /* synthetic */ g a;

        a(g gVar) {
            this.a = gVar;
        }

        public void run() {
            this.a.b(b.this.d(0));
        }
    }

    public void a(boolean z) {
        this.f.c(z);
        this.e.c(z);
    }
}
