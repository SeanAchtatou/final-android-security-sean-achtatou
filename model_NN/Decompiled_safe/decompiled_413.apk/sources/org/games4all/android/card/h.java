package org.games4all.android.card;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.FloatMath;
import java.lang.reflect.Array;
import org.games4all.card.Card;
import org.games4all.card.Face;
import org.games4all.card.Suit;

public class h {
    public static final Paint a = new Paint();
    private static final f[] b = {new f("_43x64", 43, 64), new f("_67x91", 67, 91), new f("_107x150", 107, 150)};
    private Context c;
    private f d;
    private int e;
    private int f;
    private final Bitmap[][] g = ((Bitmap[][]) Array.newInstance(Bitmap.class, 4, 13));
    private Bitmap h;
    private Bitmap i;
    private Bitmap j;
    private Bitmap k;
    private int l;
    private int m;
    private final Rect n = new Rect();
    private final RectF o = new RectF();
    private int p;
    private int q;
    private final Rect r = new Rect();
    private final RectF s = new RectF();

    static {
        a.setAntiAlias(true);
    }

    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4, Bitmap bitmap5) {
        for (Suit ordinal : Suit.values()) {
            int ordinal2 = ordinal.ordinal();
            int i2 = this.f * ordinal2;
            for (Face ordinal3 : Face.values()) {
                int ordinal4 = ordinal3.ordinal();
                this.g[ordinal2][ordinal4] = Bitmap.createBitmap(bitmap, this.e * ordinal4, i2, this.e, this.f);
            }
        }
        this.h = bitmap3;
        this.i = bitmap4;
        this.j = bitmap2;
        this.k = bitmap5;
        this.p = this.k.getWidth();
        this.q = this.k.getHeight();
    }

    public h(Context context) {
        this.c = context;
    }

    public void a(Canvas canvas, Card card, int i2, int i3, Paint paint, float f2, int i4, int i5) {
        Bitmap bitmap;
        if (card != null) {
            if (card.g()) {
                bitmap = this.j;
            } else if (card.f()) {
                bitmap = this.h;
            } else if (card.h()) {
                bitmap = this.i;
            } else {
                bitmap = this.g[card.e().ordinal()][card.d().ordinal()];
            }
            a(canvas, i2, i3, paint, bitmap, a(), b(), f2, i4, i5);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas, int i2, int i3, Paint paint, Bitmap bitmap, int i4, int i5, float f2, int i6, int i7) {
        float f3 = 0.0f;
        if (f2 != 0.0f) {
            f3 = (((float) i4) / 2.0f) * FloatMath.sin(f2);
        }
        if (i7 > 0 && this.l == this.e && this.m == this.f) {
            paint.setAlpha(i7);
            if (f3 == 0.0f) {
                this.r.set(0, 0, this.p, this.q);
                this.s.set((float) i2, (float) i3, (float) (this.p + i2), (float) (this.q + i3));
                canvas.drawBitmap(this.k, this.r, this.s, paint);
            } else {
                int i8 = this.e / 2;
                int i9 = i2 + i8;
                this.r.set(0, 0, (int) (((float) i8) - f3), this.q);
                this.s.set(((float) i2) + f3, (float) i3, (float) i9, (float) (this.q + i3));
                canvas.drawBitmap(this.k, this.r, this.s, paint);
                this.r.set((int) (((float) i8) + f3), 0, this.p, this.q);
                this.s.set((float) i9, (float) i3, ((float) ((i9 + this.p) - i8)) - f3, (float) (this.q + i3));
                canvas.drawBitmap(this.k, this.r, this.s, paint);
            }
        }
        this.n.set(0, 0, this.l, this.m);
        this.o.set((float) i2, (float) i3, (float) (i2 + i4), (float) (i3 + i5));
        this.o.left += f3;
        this.o.right -= f3;
        paint.setAlpha(i6);
        canvas.drawBitmap(bitmap, this.n, this.o, paint);
    }

    public int a() {
        return this.l;
    }

    public int b() {
        return this.m;
    }

    public int c() {
        return this.f;
    }

    public void a(int i2, int i3) {
        if (this.c != null) {
            f fVar = b[0];
            f[] fVarArr = b;
            int length = fVarArr.length;
            f fVar2 = fVar;
            int i4 = 0;
            while (i4 < length) {
                f fVar3 = fVarArr[i4];
                if (fVar3.a() > i2 || fVar3.b() >= i3) {
                    fVar3 = fVar2;
                }
                i4++;
                fVar2 = fVar3;
            }
            if (fVar2 != this.d) {
                this.d = fVar2;
                this.e = fVar2.a();
                this.f = fVar2.b();
                a(fVar2.a(this.c), fVar2.b(this.c), fVar2.c(this.c), fVar2.d(this.c), fVar2.e(this.c));
            }
        }
        this.l = Math.min(i2, this.e);
        this.m = Math.min(i3, this.f);
    }
}
