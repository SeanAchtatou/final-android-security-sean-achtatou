package org.games4all.android.card;

import android.graphics.Canvas;
import org.games4all.android.a.d;
import org.games4all.card.Card;

public class k implements d {
    private final g a;
    private final Card b;
    private final int c;
    private final int d;
    private long e = 0;
    private boolean f = false;

    public k(g gVar, Card card, int i) {
        this.a = gVar;
        this.b = card;
        this.c = i;
        this.d = i / 2;
    }

    public boolean a(long j) {
        if (this.e <= 0 || j - this.e < ((long) this.c)) {
            return false;
        }
        this.a.a(0.0f);
        this.a.a(this.b);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void a(Canvas canvas, long j) {
        float f2;
        if (this.e == 0) {
            this.e = j;
        }
        float f3 = (float) (j - this.e);
        if (f3 > ((float) this.d)) {
            float f4 = f3 - ((float) this.d);
            if (!this.f) {
                this.f = true;
                this.a.a(this.b);
            }
            f2 = Math.max(0.0f, 1.5707964f - ((f4 / ((float) this.d)) * 1.5707964f));
        } else {
            f2 = (f3 / ((float) this.d)) * 1.5707964f;
        }
        this.a.a(f2);
    }
}
