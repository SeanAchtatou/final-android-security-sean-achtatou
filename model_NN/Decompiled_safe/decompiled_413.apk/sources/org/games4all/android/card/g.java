package org.games4all.android.card;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import org.games4all.android.a.e;
import org.games4all.card.Card;

public class g extends e implements Cloneable {
    private static final ColorFilter a = new LightingColorFilter(-3355444, 0);
    private final Paint b = new Paint();
    private final h c;
    private Card d;
    private float e;
    private int f;
    private int g;

    public g(h hVar, Card card) {
        this.c = hVar;
        this.d = card;
        this.b.setAntiAlias(true);
        this.f = 255;
    }

    public Object clone() {
        g gVar = new g(this.c, this.d);
        gVar.e = this.e;
        gVar.a((e) this);
        return gVar;
    }

    public void a(Card card) {
        this.d = card;
    }

    public Card c() {
        return this.d;
    }

    public void a(float f2) {
        this.e = f2;
    }

    public boolean b(int i, int i2) {
        Point d2 = d();
        int i3 = i - d2.x;
        int i4 = i2 - d2.y;
        if (i3 < 0 || i4 < 0 || i3 >= b() || i4 >= a()) {
            return false;
        }
        return true;
    }

    public boolean d(int i, int i2) {
        Point e2 = e();
        int i3 = i - e2.x;
        int i4 = i2 - e2.y;
        if (i3 < 0 || i4 < 0 || i3 >= b() || i4 >= a()) {
            return false;
        }
        return true;
    }

    public void a(int i) {
        this.g = i;
    }

    public void a(Canvas canvas) {
        if (this.d != null) {
            Point d2 = d();
            Canvas canvas2 = canvas;
            this.c.a(canvas2, this.d, d2.x, d2.y, this.b, this.e, this.f, (this.g * this.f) / 255);
        }
    }

    public int a() {
        return this.c.b();
    }

    public int b() {
        return this.c.a();
    }

    public String toString() {
        Point d2 = d();
        Point e2 = e();
        return "CardSprite[card=" + this.d + ",pos=" + d2.x + "," + d2.y + ",orig=" + e2.x + "," + e2.y + ",w=" + b() + ",h=" + a() + ",depth=" + i() + "]";
    }
}
