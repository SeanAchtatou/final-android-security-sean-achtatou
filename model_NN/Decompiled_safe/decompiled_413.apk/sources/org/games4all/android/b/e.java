package org.games4all.android.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

public class e {
    private final Context a;
    private final String b;
    private final Resources c;

    public e(Context context) {
        this.a = context;
        this.b = context.getPackageName();
        this.c = context.getResources();
    }

    public int a(String str, String str2) {
        int identifier = this.c.getIdentifier(str2, str, this.b);
        if (identifier != 0) {
            return identifier;
        }
        throw new RuntimeException("missing resource: " + this.b + ":" + str + "/" + str2);
    }

    public Drawable a(String str) {
        return this.c.getDrawable(a("drawable", str));
    }

    public String b(String str) {
        return this.c.getString(a("string", str));
    }

    public int c(String str) {
        return a("id", str);
    }

    public int a(int i) {
        return Math.round(TypedValue.applyDimension(1, (float) i, this.c.getDisplayMetrics()));
    }
}
