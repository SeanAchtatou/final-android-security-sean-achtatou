package org.games4all.android.b;

import android.view.View;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.e.e;

public class b implements View.OnLongClickListener {
    private final Games4AllActivity a;
    private final a b;

    public static final void a(Games4AllActivity games4AllActivity, View view, a aVar) {
        view.setOnLongClickListener(new b(games4AllActivity, aVar));
    }

    private b(Games4AllActivity games4AllActivity, a aVar) {
        this.a = games4AllActivity;
        this.b = aVar;
    }

    public boolean onLongClick(View view) {
        e eVar = new e(this.a);
        eVar.a(this.b);
        eVar.show();
        return true;
    }
}
