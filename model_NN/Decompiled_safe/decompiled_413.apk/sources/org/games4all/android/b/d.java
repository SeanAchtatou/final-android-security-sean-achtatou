package org.games4all.android.b;

import android.os.Handler;
import android.os.Looper;
import org.games4all.a.b;
import org.games4all.logging.e;

public class d implements Runnable, org.games4all.a.a {
    private static final e a = e.a(d.class);
    private final boolean b = false;
    private final b c = new b();
    private final Handler d = new Handler(Looper.getMainLooper());
    private int e;
    private boolean f;
    private Object g = new Object();

    public void a(String str, Runnable runnable, Runnable runnable2) {
        this.c.a(str, runnable, runnable2);
    }

    public boolean e() {
        return this.e > 0 || this.c.a();
    }

    public synchronized void execute(Runnable runnable) {
        this.c.a(runnable, null);
        g();
    }

    public synchronized void a(Runnable runnable, String str) {
        this.c.a(runnable, str);
        g();
    }

    private void g() {
        if (!this.f && this.e == 0 && !this.c.a()) {
            this.d.post(this);
            this.f = true;
        }
    }

    public synchronized void a() {
        if (this.d.getLooper().getThread() != Thread.currentThread()) {
            throw new RuntimeException("pause can only be called from the main event thread!");
        }
        this.e++;
    }

    public synchronized void b() {
        if (this.d.getLooper().getThread() != Thread.currentThread()) {
            throw new RuntimeException("resume can only be called from the main event thread!");
        }
        if (this.e <= 0) {
            System.err.println("Warning: pause stack underflow");
            this.e = 1;
        }
        int i = this.e - 1;
        this.e = i;
        if (i == 0) {
            g();
        }
    }

    public synchronized void run() {
        this.c.b();
        this.f = false;
        g();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        synchronized (this.g) {
            this.g.notify();
        }
    }

    public void c() {
        if (this.d.getLooper().getThread() == Thread.currentThread()) {
            throw new RuntimeException("runEvents cannot be called from the main event thread!");
        }
        synchronized (this.g) {
            execute(new a());
            try {
                this.g.wait();
            } catch (InterruptedException e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    class a implements Runnable {
        a() {
        }

        public void run() {
            d.this.f();
        }

        public String toString() {
            return "SYNC TASK";
        }
    }

    public synchronized void d() {
        f();
        this.g = new Object();
        this.c.c();
        this.e = 0;
    }
}
