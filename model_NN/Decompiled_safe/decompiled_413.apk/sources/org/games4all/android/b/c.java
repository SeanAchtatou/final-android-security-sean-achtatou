package org.games4all.android.b;

import android.content.SharedPreferences;
import java.util.Map;
import org.games4all.util.g;

public class c implements g {
    private final SharedPreferences a;
    private SharedPreferences.Editor b;

    public c(SharedPreferences sharedPreferences) {
        this.a = sharedPreferences;
    }

    public Map<String, ?> a() {
        return this.a.getAll();
    }

    public String a(String str, String str2) {
        return this.a.getString(str, str2);
    }

    public void b() {
        this.b = this.a.edit();
    }

    public boolean c() {
        boolean commit = this.b.commit();
        this.b = null;
        return commit;
    }

    public void b(String str, String str2) {
        this.b.putString(str, str2);
    }

    public void a(String str) {
        this.b.remove(str);
    }
}
