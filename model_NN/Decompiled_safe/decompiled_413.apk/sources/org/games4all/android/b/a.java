package org.games4all.android.b;

import android.content.res.Resources;

public class a {
    private String a;
    private String b;
    private int c;
    private int d;

    public a() {
    }

    public a(int i, int i2) {
        this.c = i;
        this.d = i2;
    }

    public String a(Resources resources) {
        if (this.a != null) {
            return this.a;
        }
        if (this.c > 0) {
            return resources.getString(this.c);
        }
        return null;
    }

    public String b(Resources resources) {
        if (this.b != null) {
            return this.b;
        }
        if (this.d > 0) {
            return resources.getString(this.d);
        }
        return null;
    }

    public String toString() {
        return "Tooltip[title=" + this.a + "/" + this.c + ", content=" + this.b + "/" + this.d + "]";
    }
}
