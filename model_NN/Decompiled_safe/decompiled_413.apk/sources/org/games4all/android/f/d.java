package org.games4all.android.f;

import android.graphics.Paint;

public interface d {
    Paint a(int i);

    int[] a();

    int[] a(int i, int i2);

    Paint b(int i);

    int[] b();

    int[] b(int i, int i2);

    Paint c(int i, int i2);
}
