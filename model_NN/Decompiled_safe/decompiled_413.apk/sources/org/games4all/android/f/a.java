package org.games4all.android.f;

import android.content.Context;
import android.graphics.Paint;

public class a implements d {
    private static final int[][] a = {new int[]{-8388864, -16744448}, new int[]{-8388864, -10040064}};
    private static final int[][] b = {new int[]{-3473552, -11179217}, new int[]{-3473552, -4395416}};
    private static final int[][] c = {new int[]{-256, -7632128}, new int[]{-256, -3289856}};
    private static final int[][] d = {new int[]{-23296, -7644672}, new int[]{-23296, -3308288}};
    private static final int[][] e = {new int[]{-7876865, -11898741}, new int[]{-7876865, -9656627}};
    private static final int[][] f = {new int[]{-10039894, -13676721}, new int[]{-8388652, -10039894}};
    private static final int[][] g = {new int[]{-1154205, -5952982}, new int[]{-38294, -3320491}};
    private static final int[][] h = {new int[]{-20807, -7643291}, new int[]{-20807, -3306347}};
    private static final int[][] i = {new int[]{Integer.MAX_VALUE, 2139851145}, new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE}};
    private static final int[][][][] j = {new int[][][]{b, a}, new int[][][]{d, c}, new int[][][]{f, e}, new int[][][]{g, h}};
    private final Paint k = new Paint();
    private final Paint l;
    private final float m;

    public a(Context context) {
        this.m = context.getResources().getDisplayMetrics().density;
        this.k.setColor(-1);
        this.k.setAntiAlias(true);
        this.k.setTextSize(this.m * 14.0f);
        this.k.setTextAlign(Paint.Align.RIGHT);
        this.l = new Paint();
        this.l.setColor(-1);
        this.l.setAntiAlias(true);
        this.l.setTextSize(this.m * 14.0f);
    }

    public int[] a(int i2, int i3) {
        return j[i2][i3][0];
    }

    public int[] b(int i2, int i3) {
        return j[i2][i3][1];
    }

    public Paint c(int i2, int i3) {
        Paint paint = new Paint();
        paint.setColor(j[i2][i3][1][1]);
        paint.setAntiAlias(true);
        paint.setTextSize(14.0f * this.m);
        paint.setTextAlign(Paint.Align.RIGHT);
        return paint;
    }

    public int[] a() {
        return i[0];
    }

    public int[] b() {
        return i[1];
    }

    public Paint a(int i2) {
        return this.l;
    }

    public Paint b(int i2) {
        Paint paint = new Paint();
        paint.setColor(-12303292);
        paint.setAntiAlias(true);
        paint.setTextSize(14.0f * this.m);
        paint.setTextAlign(Paint.Align.CENTER);
        return paint;
    }
}
