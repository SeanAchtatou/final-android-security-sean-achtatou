package org.games4all.android.f;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.widget.LinearLayout;

public class c extends LinearLayout {
    private b a;
    private d b;
    private boolean c;
    private boolean d;

    public c(Context context) {
        super(context);
        setWillNotDraw(false);
    }

    private Paint a(int i, int i2, boolean z, float f, float f2, float f3, float f4) {
        int[] b2;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        if (z) {
            b2 = this.b.a(i, i2);
        } else {
            b2 = this.b.b(i, i2);
        }
        paint.setShader(new LinearGradient(f, f2, f3, f4, b2[0], b2[1], Shader.TileMode.CLAMP));
        return paint;
    }

    private Paint a(boolean z, float f, float f2, float f3, float f4) {
        int[] b2;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        if (z) {
            b2 = this.b.a();
        } else {
            b2 = this.b.b();
        }
        paint.setShader(new LinearGradient(f, f2, f3, f4, b2[0], b2[1], Shader.TileMode.CLAMP));
        return paint;
    }

    public void a(b bVar) {
        this.a = bVar;
    }

    public void a(d dVar) {
        this.b = dVar;
    }

    public void onDraw(Canvas canvas) {
        if (this.a != null && this.b != null) {
            int b2 = this.a.b();
            if (this.c) {
                int height = getHeight();
                int i = height / b2;
                for (int i2 = 0; i2 < b2; i2++) {
                    a(canvas, i2, ((i2 + 1) * height) / (b2 + 1), i);
                }
                return;
            }
            int width = getWidth() / b2;
            for (int i3 = 0; i3 < b2; i3++) {
                b(canvas, i3, (width * i3) + (width / 3), width);
            }
        }
    }

    private void a(Canvas canvas, int i, int i2, int i3) {
        int i4 = (i3 * 2) / 3;
        int i5 = i2 - (i4 / 2);
        int i6 = i5 + i4;
        int a2 = this.a.a(i);
        int width = getWidth() - 10;
        int i7 = i4 / 4;
        int i8 = i4 / 2;
        canvas.drawRoundRect(new RectF((float) 10, (float) i5, (float) width, (float) i6), (float) i7, (float) i8, a(i, a2, true, 0.0f, (float) i5, 0.0f, (float) i6));
        int i9 = (width - 10) - (i7 * 2);
        float c2 = this.a.c();
        int i10 = 0;
        float f = 0.0f;
        int i11 = 10 + i7;
        while (i10 < a2) {
            float a3 = f + this.a.a(i, i10);
            int i12 = 10 + i7 + ((int) ((((float) i9) * a3) / c2));
            canvas.drawRoundRect(new RectF((float) (i11 - i7), (float) i5, (float) (i12 + i7), (float) i6), (float) i7, (float) i8, a(i, i10, true, 0.0f, (float) i5, 0.0f, (float) i6));
            canvas.drawRoundRect(new RectF((float) (i12 - i7), (float) i5, (float) (i12 + i7), (float) i6), (float) i7, (float) i8, a(i, i10, false, 0.0f, (float) i5, 0.0f, (float) i6));
            i10++;
            f = a3;
            i11 = i12;
        }
        canvas.drawRoundRect(new RectF((float) (width - (i7 * 2)), (float) i5, (float) width, (float) i6), (float) i7, (float) i8, a(i, a2, false, 0.0f, (float) i5, 0.0f, (float) i6));
    }

    private void b(Canvas canvas, int i, int i2, int i3) {
        int i4 = (i3 * 55) / 100;
        int i5 = i2 - (i4 / 2);
        int i6 = i5 + i4;
        int a2 = this.a.a(i);
        int height = getHeight();
        Paint a3 = this.b.a(i);
        Paint.FontMetrics fontMetrics = a3.getFontMetrics();
        int i7 = ((((int) fontMetrics.ascent) + height) - ((int) fontMetrics.descent)) - 2;
        int i8 = i7 / 8;
        int i9 = i4 / 2;
        int i10 = i4 / 4;
        float f = (float) i9;
        float f2 = (float) i10;
        canvas.drawRoundRect(new RectF((float) i5, (float) i8, (float) i6, (float) i7), f, f2, a(true, (float) i5, (float) i8, (float) i6, (float) i8));
        int i11 = (i7 - i8) - (i10 * 2);
        float f3 = 0.0f;
        float c2 = this.a.c();
        int i12 = 0;
        int i13 = i7 - i10;
        while (i12 < a2) {
            float a4 = this.a.a(i, i12);
            float f4 = f3 + a4;
            int i14 = (i7 - i10) - ((int) ((((float) i11) * f4) / c2));
            Paint a5 = a(i, i12, true, (float) i5, (float) i8, (float) i6, (float) i8);
            RectF rectF = new RectF((float) i5, (float) (i14 - i10), (float) i6, (float) (i13 + i10));
            if (i12 == 0 || a4 != 0.0f || this.d) {
                canvas.drawRoundRect(rectF, (float) i9, (float) i10, a5);
            }
            Paint a6 = a(i, i12, false, (float) i5, (float) i8, (float) i6, (float) i8);
            RectF rectF2 = new RectF((float) i5, (float) (i14 - i10), (float) i6, (float) (i14 + i10));
            if (i12 == 0 || a4 != 0.0f || this.d) {
                canvas.drawRoundRect(rectF2, (float) i9, (float) i10, a6);
            }
            a(canvas, this.a.b(i, i12), i2, i3, i4, i14, i13, this.b.c(i, i12));
            if (i12 == a2 - 1) {
                a(canvas, this.a.b(i), i2, i3, i4, i14, i13, this.b.b(i));
            }
            i12++;
            i13 = i14;
            f3 = f4;
        }
        Paint a7 = a(false, (float) i5, (float) i8, (float) i6, (float) i8);
        canvas.drawRoundRect(new RectF((float) i5, (float) i8, (float) i6, (float) ((i10 * 2) + i8)), (float) i9, (float) i10, a7);
        String c3 = this.a.c(i);
        a3.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(c3, (float) i2, (((float) height) - fontMetrics.descent) - 2.0f, a3);
    }

    public void a(Canvas canvas, String str, int i, int i2, int i3, int i4, int i5, Paint paint) {
        float f;
        if (str != null) {
            float f2 = getResources().getDisplayMetrics().density;
            float f3 = paint.getFontMetrics().descent + ((float) ((i5 + i4) / 2));
            float measureText = paint.measureText(str);
            switch (a.a[paint.getTextAlign().ordinal()]) {
                case 1:
                    f = (float) (i - (i2 / 2));
                    break;
                case 2:
                    f = (float) i;
                    break;
                case 3:
                    f = (float) ((int) ((f2 * 4.0f) + measureText + ((float) ((i3 / 2) + i))));
                    break;
                default:
                    throw new RuntimeException(String.valueOf(paint.getTextAlign()));
            }
            canvas.drawText(str, f, f3, paint);
        }
    }

    static /* synthetic */ class a {
        static final /* synthetic */ int[] a = new int[Paint.Align.values().length];

        static {
            try {
                a[Paint.Align.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Paint.Align.CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Paint.Align.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }
}
