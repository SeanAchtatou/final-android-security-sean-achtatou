package org.games4all.android.test;

import org.games4all.android.d.c;
import org.games4all.android.d.d;
import org.games4all.android.e.a;
import org.games4all.android.games.indianrummy.prod.R;

public class CreateAccountScenario extends b {
    public void run() {
        waitForDialog(a.class);
        checkScreenshot("help");
        waitForView(R.id.g4a_closeButton);
        click(R.id.g4a_closeButton);
        waitForDialog(org.games4all.android.option.a.class);
        click(R.id.g4a_acceptButton);
        waitForDialog(c.class);
        checkScreenshot("select-login");
        click(R.id.g4a_selectLoginNew);
        waitForDialog(d.class);
        enterText(R.id.g4a_createAccountName, "testplayer");
        enterText(R.id.g4a_createAccountPassword, "testpwd");
        checkScreenshot("create-account");
        click(R.id.g4a_createButton);
        System.err.println("waiting for SelectSuit");
        waitForDialog(org.games4all.android.card.c.class);
        checkScreenshot("select-suit");
        click(R.id.g4a_selectSpadesButton);
        waitForDialogDismiss();
    }
}
