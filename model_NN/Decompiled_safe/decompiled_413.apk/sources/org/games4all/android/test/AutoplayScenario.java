package org.games4all.android.test;

import org.games4all.android.GameApplication;
import org.games4all.android.c.d;
import org.games4all.game.e.c;
import org.games4all.game.model.e;
import org.games4all.game.move.Move;
import org.games4all.game.move.a;
import org.games4all.game.option.k;
import org.games4all.game.test.GameAction;

public class AutoplayScenario extends b {
    private static final int SEAT = 0;
    private a actuator;
    private c robotFactory;
    private boolean running;
    private org.games4all.game.table.a tableModel;

    /* JADX WARN: Type inference failed for: r0v6, types: [org.games4all.game.model.d] */
    private void init() {
        GameApplication b = getRunner().b();
        org.games4all.game.d.a v = b.v();
        k e = v.e();
        e<?, ?, ?> k = b.k();
        org.games4all.game.c.a<?> d = v.d();
        this.robotFactory = d.a(new org.games4all.game.c.e(d.b((org.games4all.game.option.e) e.a())));
        this.tableModel = k.m().f();
        this.actuator = b.x();
    }

    public void run() {
        this.running = true;
        init();
        d.a(true);
        GameApplication b = getRunner().b();
        org.games4all.game.b.a h = b.h();
        while (this.running) {
            System.err.println("waiting for initiative");
            if (waitForInitiative() == GameAction.MOVE) {
                e<?, ?, ?> k = b.k();
                System.err.println("Basing decisions on model " + System.identityHashCode(k));
                Move a = ((org.games4all.game.c.c) this.robotFactory.a(k, 0, this.tableModel.a(0))).a();
                if (this.actuator == null) {
                    executeMove(h, a);
                } else {
                    System.err.println("actuating move: " + a);
                    getRunner().a(200);
                    System.err.println("executor settled!");
                    org.games4all.game.move.c a2 = a.a(0, this.actuator);
                    if (a2 != null && !a2.a()) {
                        executeMove(h, a);
                    }
                }
            }
        }
        d.a(false);
    }

    public void executeMove(org.games4all.game.b.a aVar, Move move) {
        System.err.println("executing move: " + move);
        aVar.a(0).e().a(move);
    }

    public void stop() {
        d.a(false);
        this.running = false;
    }
}
