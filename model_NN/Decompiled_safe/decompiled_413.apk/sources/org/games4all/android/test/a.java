package org.games4all.android.test;

import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.Thread;
import java.net.URLEncoder;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.game.controller.server.GameSeed;
import org.games4all.game.test.GameAction;

public class a implements Runnable {
    private final String a;
    private final String b;
    private final org.games4all.android.b.d c;
    private final org.games4all.android.b.d d;
    private b e;
    private final Thread f;
    private Thread g;
    private boolean h;
    private final GameApplication i;
    private Games4AllActivity j;
    private org.games4all.android.e.d k;
    private boolean l;
    private boolean m;
    private Throwable n;
    private final org.games4all.util.b.a.a<org.games4all.game.test.i> o;
    private org.games4all.game.test.a p;
    private org.games4all.game.test.d q;

    public a(GameApplication gameApplication, org.games4all.android.b.d dVar, String str, boolean z2) {
        this.i = gameApplication;
        if (str == null) {
            this.a = null;
            this.b = null;
        } else {
            this.a = str + "/event";
            this.b = str + "/regression";
        }
        this.c = new org.games4all.android.b.d();
        this.d = dVar;
        this.c.a();
        this.f = Thread.currentThread();
        this.o = new org.games4all.util.b.a.a<>();
        if (z2) {
            this.f.setUncaughtExceptionHandler(new w(this.f.getUncaughtExceptionHandler()));
        }
        this.m = true;
    }

    class w implements Thread.UncaughtExceptionHandler {
        final /* synthetic */ Thread.UncaughtExceptionHandler a;

        w(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.a = uncaughtExceptionHandler;
        }

        public void uncaughtException(Thread thread, Throwable th) {
            System.err.println("**** uncaughtException: " + th.getMessage());
            th.printStackTrace();
            a.this.d();
            this.a.uncaughtException(thread, th);
        }
    }

    public void a(boolean z2) {
        this.m = z2;
    }

    public org.games4all.android.e.d a() {
        return this.k;
    }

    public GameApplication b() {
        return this.i;
    }

    public Games4AllActivity c() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.h = true;
        this.g.interrupt();
    }

    private void x() {
        if (Thread.currentThread() != this.f) {
            throw new IllegalStateException("Must be called from UI thread");
        }
    }

    private void y() {
        if (Thread.currentThread() != this.g) {
            throw new IllegalStateException("Must be called from scenario thread");
        } else if (this.h) {
            throw new RuntimeException("scenario aborted");
        }
    }

    public void e() {
        org.games4all.game.b.a w2 = w();
        org.games4all.game.b.d dVar = new org.games4all.game.b.d(w2, w2.b(), 0);
        this.p = new org.games4all.game.test.a(new org.games4all.game.controller.a.f(w2.b(), 0, v(), new org.games4all.game.controller.a.k(dVar), dVar));
        this.q = new org.games4all.game.test.d(null, this.c, this.o);
        this.q.a(this.p);
    }

    public void f() {
        this.p.d();
        this.p = null;
        this.q = null;
    }

    public void a(b bVar) {
        GameSeed.a(new Random(0));
        this.h = false;
        x();
        bVar.setRunner(this);
        this.e = bVar;
        this.g = new Thread(this);
        this.g.start();
    }

    public org.games4all.game.move.a g() {
        return this.i.x();
    }

    public boolean h() {
        return this.e != null;
    }

    public void a(Games4AllActivity games4AllActivity) {
        x();
        this.j = games4AllActivity;
        if (h()) {
            c(games4AllActivity.r());
        }
        this.c.b();
    }

    public void b(Games4AllActivity games4AllActivity) {
        x();
        if (games4AllActivity == this.j) {
            this.c.a();
        }
        this.j = null;
    }

    public void a(org.games4all.android.e.d dVar) {
        x();
        if (dVar != null && h()) {
            c(dVar.d());
        }
        this.k = dVar;
    }

    public void run() {
        y();
        org.games4all.android.b.b = true;
        a("STARTED", "");
        try {
            this.e.run();
            i();
            a("final");
            a("FINISHED", "");
            if (this.j != null && this.m) {
                a(new v());
            }
            this.e = null;
            this.g = null;
        } catch (Throwable th) {
            System.err.println("CAUGHT EXCEPTION IN SCENARIO: " + th.getMessage());
            th.printStackTrace();
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            a("ABORTED", stringWriter.toString());
        }
    }

    class v implements Runnable {
        v() {
        }

        public void run() {
            Games4AllActivity c = a.this.c();
            if (c != null) {
                c.finish();
            }
        }
    }

    public void i() {
        a(1000);
    }

    public void a(int i2) {
        org.games4all.android.a.g gVar;
        org.games4all.game.e.a c2 = this.i.h().a(0).e().c();
        if (c2 instanceof org.games4all.android.a) {
            gVar = ((org.games4all.android.a) c2).i();
        } else {
            gVar = null;
        }
        while (true) {
            a(new y());
            try {
                Thread.sleep((long) i2);
            } catch (InterruptedException e2) {
            }
            a(new x());
            if (this.c.e() && this.d.e()) {
                if (gVar == null || !gVar.b()) {
                    return;
                }
            }
        }
    }

    class y implements Runnable {
        y() {
        }

        public void run() {
        }
    }

    class x implements Runnable {
        x() {
        }

        public void run() {
        }
    }

    public void a(Runnable runnable) {
        if (Thread.currentThread() == this.f) {
            runnable.run();
            return;
        }
        y();
        synchronized (runnable) {
            this.c.execute(new s(runnable));
            try {
                runnable.wait();
            } catch (InterruptedException e2) {
                throw new RuntimeException("Scenario interrupted", this.n);
            }
        }
    }

    class s implements Runnable {
        final /* synthetic */ Runnable a;

        s(Runnable runnable) {
            this.a = runnable;
        }

        public void run() {
            a.this.b(this.a);
        }
    }

    public boolean a(String str, String str2) {
        if (Thread.currentThread() != this.g) {
            throw new IllegalStateException("Must be called from scenario thread");
        }
        String name = this.e.getClass().getName();
        System.err.println("SCENARIO " + name + ": " + str + " - " + str2);
        System.err.println("eventUrl: " + this.a);
        if (this.a == null) {
            return true;
        }
        StringBuilder sb = new StringBuilder(this.a);
        try {
            sb.append("?scenario=").append(name);
            sb.append("&event=").append(str);
            sb.append("&details=").append(URLEncoder.encode(str2 == null ? "" : str2, "UTF-8"));
            try {
                StatusLine statusLine = new DefaultHttpClient().execute(new HttpGet(sb.toString())).getStatusLine();
                if (statusLine.getStatusCode() == 200) {
                    return true;
                }
                System.err.println("could not send event: " + statusLine.getReasonPhrase());
                return false;
            } catch (ClientProtocolException e2) {
                e2.printStackTrace();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        } catch (UnsupportedEncodingException e4) {
            throw new RuntimeException(e4);
        }
    }

    private void a(String str, byte[] bArr) {
        InputStream inputStream;
        if (this.b != null) {
            try {
                org.apache.http.entity.mime.d dVar = new org.apache.http.entity.mime.d();
                dVar.a("scenario", new org.apache.http.entity.mime.a.b(this.e.getClass().getName()));
                dVar.a("filename", new org.apache.http.entity.mime.a.b(str));
                dVar.a("content", new org.games4all.gamestore.client.f("content", bArr));
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(this.b);
                httpPost.setEntity(dVar);
                HttpResponse execute = defaultHttpClient.execute(httpPost);
                InputStream content = execute.getEntity().getContent();
                Header firstHeader = execute.getFirstHeader("Content-Encoding");
                if (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("gzip")) {
                    inputStream = content;
                } else {
                    inputStream = new GZIPInputStream(content);
                }
                String b2 = org.games4all.util.c.b(inputStream);
                inputStream.close();
                String trim = b2.trim();
                if (trim.equals("ORIGINAL")) {
                    System.err.println("NO REGRESSION TESTING DONE, GENERATED ORIGINAL IMAGE " + str);
                } else if (!trim.equals("OK")) {
                    System.err.println("REGRESSION TEST FAILED FOR " + str + ", CONTINUING SCENARIO");
                }
            } catch (IOException e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    public void a(String str) {
        y();
        j();
        l();
        i();
        a(new r(str));
    }

    class r implements Runnable {
        final /* synthetic */ String a;

        r(String str) {
            this.a = str;
        }

        public void run() {
            a.this.b(this.a);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (this.b == null) {
            System.err.println("ignoring regression check because no web-app registered");
            return;
        }
        x();
        a(str + ".png", this.j.b(1).a());
    }

    public void a(org.games4all.util.a.a<a> aVar) {
        y();
        do {
            a(new u(aVar));
            if (!this.l) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e2) {
                }
            }
        } while (!this.l);
    }

    class u implements Runnable {
        final /* synthetic */ org.games4all.util.a.a a;

        u(org.games4all.util.a.a aVar) {
            this.a = aVar;
        }

        public void run() {
            a.this.b(this.a.a(a.this));
        }
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.l = z2;
    }

    class t implements org.games4all.util.a.a<a> {
        final /* synthetic */ int a;

        t(int i) {
            this.a = i;
        }

        public boolean a(a aVar) {
            View f = aVar.f(this.a);
            if (f == null) {
                return false;
            }
            while (f != null) {
                if (f.isEnabled()) {
                    int visibility = f.getVisibility();
                    if (visibility != 4 && visibility != 8) {
                        ViewParent parent = f.getParent();
                        if (!(parent instanceof View)) {
                            break;
                        }
                        f = (View) parent;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    public void b(int i2) {
        a(new t(i2));
    }

    class z implements org.games4all.util.a.a<a> {
        final /* synthetic */ Class a;

        z(Class cls) {
            this.a = cls;
        }

        public boolean a(a aVar) {
            org.games4all.android.e.d a2 = aVar.a();
            if (a2 == null || a2.getClass() != this.a) {
                return false;
            }
            a.this.a(a2.d());
            return true;
        }
    }

    public void a(Class<? extends org.games4all.android.e.d> cls) {
        a(new z(cls));
    }

    class j implements org.games4all.util.a.a<a> {
        final /* synthetic */ Class a;
        final /* synthetic */ int b;

        j(Class cls, int i) {
            this.a = cls;
            this.b = i;
        }

        public boolean a(a aVar) {
            org.games4all.android.e.d a2 = aVar.a();
            if (a2 == null || a2.getClass() != this.a || a2.e() != this.b) {
                return false;
            }
            a.this.a(a2.d());
            return true;
        }
    }

    public void a(Class<? extends org.games4all.android.e.d> cls, int i2) {
        a(new j(cls, i2));
    }

    class k implements org.games4all.util.a.a<a> {
        final /* synthetic */ org.games4all.game.a.a.b a;

        k(org.games4all.game.a.a.b bVar) {
            this.a = bVar;
        }

        public boolean a(a aVar) {
            org.games4all.android.e.d a2 = aVar.a();
            if (a2 == null || !(a2 instanceof org.games4all.game.a.a.a) || ((org.games4all.game.a.a.a) a2).a() != this.a) {
                return false;
            }
            return true;
        }
    }

    public void a(org.games4all.game.a.a.b bVar) {
        a(new k(bVar));
    }

    class l implements Runnable {
        final /* synthetic */ int a;

        l(int i) {
            this.a = i;
        }

        public void run() {
            org.games4all.android.e.d a2 = a.this.a();
            org.games4all.game.a.a.a aVar = (org.games4all.game.a.a.a) a2;
            aVar.a().a(aVar.a(this.a));
            a2.dismiss();
        }
    }

    public void c(int i2) {
        a(new l(i2));
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        if (view instanceof EditText) {
            ((EditText) view).setCursorVisible(false);
        }
        if (view.isSelected()) {
            view.setSelected(false);
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                a(viewGroup.getChildAt(i2));
            }
        }
    }

    class m implements Runnable {
        m() {
        }

        public void run() {
            a.this.k();
        }
    }

    public void j() {
        y();
        a(new m());
    }

    /* access modifiers changed from: package-private */
    public void k() {
        if (this.k != null) {
            b(this.k.d());
        }
        if (this.j != null) {
            b(this.j.r());
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view) {
        if (view != null) {
            if (view.isSelected()) {
                view.setSelected(false);
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int childCount = viewGroup.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    b(viewGroup.getChildAt(i2));
                }
            }
        }
    }

    class n implements Runnable {
        n() {
        }

        public void run() {
            a.this.m();
        }
    }

    public void l() {
        y();
        a(new n());
    }

    /* access modifiers changed from: package-private */
    public void m() {
        View currentFocus;
        x();
        if (this.k != null) {
            View currentFocus2 = this.k.getWindow().getCurrentFocus();
            if (currentFocus2 != null) {
                currentFocus2.setFocusable(false);
            }
        } else if (this.j != null && (currentFocus = this.j.getCurrentFocus()) != null) {
            currentFocus.setFocusable(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(View view) {
        x();
        view.setFocusable(false);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                c(viewGroup.getChildAt(i2));
            }
        }
    }

    class o implements org.games4all.util.a.a<a> {
        o() {
        }

        public boolean a(a aVar) {
            return aVar.a() == null;
        }
    }

    public void n() {
        a(new o());
    }

    class p implements org.games4all.util.a.a<a> {
        final /* synthetic */ Class a;

        p(Class cls) {
            this.a = cls;
        }

        public boolean a(a aVar) {
            Games4AllActivity c = aVar.c();
            return c != null && c.getClass() == this.a;
        }
    }

    public void b(Class<? extends Games4AllActivity> cls) {
        a(new p(cls));
    }

    class q implements Runnable {
        q() {
        }

        public void run() {
            a.this.c().openOptionsMenu();
        }
    }

    public void o() {
        a(new q());
    }

    class i implements Runnable {
        final /* synthetic */ int a;

        i(int i) {
            this.a = i;
        }

        public void run() {
            a.this.c().s().performIdentifierAction(this.a, 0);
        }
    }

    public void d(int i2) {
        a(new i(i2));
    }

    public org.games4all.game.test.f p() {
        return this.q;
    }

    public boolean q() {
        return this.q != null;
    }

    class h implements org.games4all.util.a.a<a> {
        h() {
        }

        public boolean a(a aVar) {
            return aVar.q();
        }
    }

    public org.games4all.game.test.f r() {
        i();
        a(new h());
        return this.q;
    }

    public GameAction s() {
        y();
        if (this.q == null) {
            r();
        }
        return this.q.c();
    }

    /* renamed from: org.games4all.android.test.a$a  reason: collision with other inner class name */
    class C0016a implements Runnable {
        final /* synthetic */ int a;
        final /* synthetic */ String b;

        C0016a(int i, String str) {
            this.a = i;
            this.b = str;
        }

        public void run() {
            ((TextView) a.this.f(this.a)).setText(this.b);
        }
    }

    public void a(int i2, String str) {
        a(new C0016a(i2, str));
    }

    class d implements Runnable {
        final /* synthetic */ int a;
        final /* synthetic */ int b;

        d(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        public void run() {
            ((SeekBar) a.this.f(this.a)).setProgress(this.b);
        }
    }

    public void a(int i2, int i3) {
        a(new d(i2, i3));
    }

    /* access modifiers changed from: package-private */
    public void b(Runnable runnable) {
        x();
        synchronized (runnable) {
            try {
                runnable.run();
                runnable.notify();
            } catch (Throwable th) {
                this.n = th;
                org.games4all.util.b.a(this.g, "Exception caught in action.run, scenario stack:");
                this.g.interrupt();
                throw new RuntimeException(th);
            }
        }
    }

    class e implements Runnable {
        final /* synthetic */ int a;

        e(int i) {
            this.a = i;
        }

        public void run() {
            a.this.g(this.a);
        }
    }

    public void e(int i2) {
        a(new e(i2));
    }

    /* access modifiers changed from: package-private */
    public View f(int i2) {
        x();
        if (this.k == null) {
            return this.j.findViewById(i2);
        }
        return this.k.findViewById(i2);
    }

    /* access modifiers changed from: package-private */
    public void g(int i2) {
        View f2 = f(i2);
        if (f2 == null) {
            throw new RuntimeException("View not found: " + i2);
        } else if (!f2.isEnabled()) {
            throw new RuntimeException("View not enabled: " + i2);
        } else {
            f2.performClick();
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            a.this.u();
        }
    }

    public void t() {
        a(new b());
    }

    /* access modifiers changed from: package-private */
    public void u() {
        a().cancel();
    }

    public org.games4all.game.e.a v() {
        return this.j.q();
    }

    public org.games4all.game.b.a w() {
        return this.i.h();
    }

    public void a(View view, Point point) {
        b(view, point);
        c(view, point);
    }

    class c implements Runnable {
        final /* synthetic */ View a;
        final /* synthetic */ Point b;

        c(View view, Point point) {
            this.a = view;
            this.b = point;
        }

        public void run() {
            a.this.e(this.a, this.b);
        }
    }

    public void b(View view, Point point) {
        a(new c(view, point));
    }

    class f implements Runnable {
        final /* synthetic */ View a;
        final /* synthetic */ Point b;

        f(View view, Point point) {
            this.a = view;
            this.b = point;
        }

        public void run() {
            a.this.g(this.a, this.b);
        }
    }

    public void c(View view, Point point) {
        a(new f(view, point));
    }

    class g implements Runnable {
        final /* synthetic */ View a;
        final /* synthetic */ Point b;

        g(View view, Point point) {
            this.a = view;
            this.b = point;
        }

        public void run() {
            a.this.f(this.a, this.b);
        }
    }

    public void d(View view, Point point) {
        a(new g(view, point));
    }

    /* access modifiers changed from: package-private */
    public void e(View view, Point point) {
        long currentTimeMillis = System.currentTimeMillis();
        view.onTouchEvent(MotionEvent.obtain(currentTimeMillis, currentTimeMillis, 0, (float) point.x, (float) point.y, 0));
    }

    /* access modifiers changed from: package-private */
    public void f(View view, Point point) {
        long currentTimeMillis = System.currentTimeMillis();
        view.onTouchEvent(MotionEvent.obtain(currentTimeMillis, currentTimeMillis, 2, (float) point.x, (float) point.y, 0));
    }

    /* access modifiers changed from: package-private */
    public void g(View view, Point point) {
        long currentTimeMillis = System.currentTimeMillis();
        view.onTouchEvent(MotionEvent.obtain(currentTimeMillis, currentTimeMillis, 1, (float) point.x, (float) point.y, 0));
    }
}
