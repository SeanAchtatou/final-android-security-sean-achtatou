package org.games4all.android.test;

import android.graphics.Point;
import android.view.View;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.d.c;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.option.d;
import org.games4all.game.e.a;
import org.games4all.game.test.GameAction;
import org.games4all.game.test.f;
import org.games4all.logging.LogLevel;
import org.games4all.logging.e;

public abstract class b implements Runnable {
    private final e log = e.a(getClass());
    private a runner;

    public b() {
        this.log.b(LogLevel.INFO);
    }

    public void setRunner(a aVar) {
        this.runner = aVar;
    }

    /* access modifiers changed from: protected */
    public a getRunner() {
        return this.runner;
    }

    /* access modifiers changed from: protected */
    public d getPreferences() {
        return this.runner.b().e();
    }

    /* access modifiers changed from: protected */
    public f getPlayer() {
        return this.runner.p();
    }

    /* access modifiers changed from: protected */
    public org.games4all.game.model.e<?, ?, ?> getModel() {
        return getPlayer().b();
    }

    /* access modifiers changed from: protected */
    public f waitForGame() {
        this.log.c("wait for game");
        return this.runner.r();
    }

    /* access modifiers changed from: protected */
    public GameAction waitForInitiative() {
        this.log.c("wait for initiative");
        return this.runner.s();
    }

    /* access modifiers changed from: protected */
    public void waitForInitiative(GameAction gameAction) {
        GameAction s = this.runner.s();
        if (s != gameAction) {
            throw new AssertionError(s + " != " + gameAction);
        }
    }

    /* access modifiers changed from: protected */
    public void waitForView(int i) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Waiting for view: " + i);
        }
        this.runner.b(i);
    }

    /* access modifiers changed from: protected */
    public void waitForDialog(Class<? extends org.games4all.android.e.d> cls) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Waiting for dialog: " + cls.getName());
        }
        this.runner.a(cls);
    }

    /* access modifiers changed from: protected */
    public void waitForDialog(Class<? extends org.games4all.android.e.d> cls, int i) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Waiting for dialog: " + i);
        }
        this.runner.a(cls, i);
    }

    /* access modifiers changed from: protected */
    public void waitForDialogDismiss() {
        this.log.c("Waiting for dialog dismiss");
        this.runner.n();
    }

    /* access modifiers changed from: protected */
    public void waitForQuestion(org.games4all.game.a.a.b bVar) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Waiting for dialog with question: " + bVar.a());
        }
        this.runner.a(bVar);
    }

    /* access modifiers changed from: protected */
    public void answerQuestion(int i) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Answer question, index: " + i);
        }
        this.runner.c(i);
    }

    public void waitForActivity(Class<? extends Games4AllActivity> cls) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Waiting for activity: " + cls);
        }
        this.runner.b(cls);
    }

    public void openMenu() {
        this.runner.o();
    }

    public void selectMenuItem(int i) {
        this.runner.d(i);
    }

    /* access modifiers changed from: protected */
    public void click(int i) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Waiting for: " + i + " to click it");
        }
        waitForView(i);
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Click: " + i);
        }
        this.runner.e(i);
    }

    /* access modifiers changed from: protected */
    public void enterText(int i, String str) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Enter text: " + str + " in " + i);
        }
        this.runner.a(i, str);
    }

    public void setProgress(int i, int i2) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Set progress: " + i2 + " in " + i);
        }
        this.runner.a(i, i2);
    }

    public void cancelDialog() {
        this.runner.t();
    }

    /* access modifiers changed from: protected */
    public void checkScreenshot(String str) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Check screenshot: " + str);
        }
        this.runner.a(str);
    }

    /* access modifiers changed from: protected */
    public a getViewer() {
        return this.runner.v();
    }

    /* access modifiers changed from: protected */
    public org.games4all.game.b.a getGameRunner() {
        return this.runner.w();
    }

    /* access modifiers changed from: protected */
    public void click(View view, Point point) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Click at " + point + " in " + view);
        }
        this.runner.a(view, point);
    }

    /* access modifiers changed from: protected */
    public void touchDown(View view, Point point) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("touch down at " + point + " in " + view);
        }
        this.runner.b(view, point);
    }

    /* access modifiers changed from: protected */
    public void touchMove(View view, Point point) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("touch move at " + point + " in " + view);
        }
        this.runner.d(view, point);
    }

    /* access modifiers changed from: protected */
    public void touchUp(View view, Point point) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("touch up at " + point + " in " + view);
        }
        this.runner.c(view, point);
    }

    /* access modifiers changed from: protected */
    public void waitMove() {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("Wait for move");
        }
        f player = getPlayer();
        if (waitForInitiative() == GameAction.RESUME) {
            player.a();
            waitForInitiative(GameAction.MOVE);
        }
    }

    /* access modifiers changed from: protected */
    public void delay(long j) {
        if (this.log.a(LogLevel.INFO)) {
            this.log.c("delay for " + j + " ms.");
        }
        try {
            Thread.sleep(j);
        } catch (InterruptedException e) {
        }
    }

    public void stop() {
        throw new UnsupportedOperationException();
    }

    public void createAccount(boolean z) {
        System.err.println("Waiting for HelpDialog");
        waitForDialog(org.games4all.android.e.a.class);
        checkScreenshot("help");
        System.err.println("Pressing close button");
        click(R.id.g4a_closeButton);
        if (z) {
            System.err.println("Waiting for GameSettingsDialog");
            waitForDialog(org.games4all.android.option.a.class);
            System.err.println("Pressing accept button");
            click(R.id.g4a_acceptButton);
        }
        System.err.println("Waiting for SelectLoginDialog");
        waitForDialog(c.class);
        checkScreenshot("select-login");
        click(R.id.g4a_selectLoginNew);
        waitForDialog(org.games4all.android.d.d.class);
        enterText(R.id.g4a_createAccountName, "testplayer");
        enterText(R.id.g4a_createAccountPassword, "testpwd");
        checkScreenshot("create-account");
        click(R.id.g4a_createButton);
    }
}
