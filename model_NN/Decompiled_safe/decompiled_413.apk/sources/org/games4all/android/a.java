package org.games4all.android;

import android.content.DialogInterface;
import org.games4all.android.a.g;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.option.d;
import org.games4all.game.lifecycle.Stage;
import org.games4all.game.model.e;

public abstract class a implements org.games4all.game.e.a {
    private final Games4AllActivity a;
    private final org.games4all.a.a b;

    /* access modifiers changed from: protected */
    public abstract void d(int i);

    public abstract g i();

    public a(Games4AllActivity games4AllActivity, org.games4all.a.a aVar) {
        this.a = games4AllActivity;
        this.b = aVar;
    }

    public org.games4all.a.a h() {
        return this.b;
    }

    public void a(boolean z) {
        Stage v;
        org.games4all.a.a aVar = this.b;
        Games4AllActivity games4AllActivity = this.a;
        d e = this.a.p().e();
        if (!z) {
            aVar.execute(new b(e));
        }
        e<?, ?, ?> k = games4AllActivity.p().k();
        if (k == null) {
            v = Stage.GAME;
        } else {
            v = k.v();
        }
        aVar.execute(new C0005a(games4AllActivity, v.compareTo(Stage.MATCH) <= 0, aVar));
    }

    class b implements Runnable {
        final /* synthetic */ d a;

        b(d dVar) {
            this.a = dVar;
        }

        public void run() {
            a.this.b();
            a.this.d(this.a.d());
        }
    }

    /* renamed from: org.games4all.android.a$a  reason: collision with other inner class name */
    class C0005a implements Runnable {
        final /* synthetic */ Games4AllActivity a;
        final /* synthetic */ boolean b;
        final /* synthetic */ org.games4all.a.a c;

        C0005a(Games4AllActivity games4AllActivity, boolean z, org.games4all.a.a aVar) {
            this.a = games4AllActivity;
            this.b = z;
            this.c = aVar;
        }

        /* JADX WARN: Type inference failed for: r1v1, types: [org.games4all.game.model.d] */
        public void run() {
            GameApplication p = this.a.p();
            int b2 = p.k().m().i().b();
            org.games4all.android.c.d dVar = new org.games4all.android.c.d(this.a, p.h().j(), p.A(), b2, this.b);
            dVar.setOnDismissListener(new C0006a());
            this.c.a();
            dVar.show();
        }

        /* renamed from: org.games4all.android.a$a$a  reason: collision with other inner class name */
        class C0006a implements DialogInterface.OnDismissListener {
            C0006a() {
            }

            public void onDismiss(DialogInterface dialogInterface) {
                a.this.c();
                C0005a.this.c.b();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    /* access modifiers changed from: protected */
    public void c() {
    }
}
