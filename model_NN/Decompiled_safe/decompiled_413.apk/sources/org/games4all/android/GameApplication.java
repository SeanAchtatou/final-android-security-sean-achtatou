package org.games4all.android;

import android.app.Application;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.android.option.AndroidOptionsEditor;
import org.games4all.android.report.b;
import org.games4all.game.controller.a.d;
import org.games4all.game.e.c;
import org.games4all.game.move.PlayerMove;
import org.games4all.game.option.RobotOptionsImpl;
import org.games4all.game.option.e;
import org.games4all.game.option.f;
import org.games4all.game.option.k;
import org.games4all.game.rating.ContestResult;
import org.games4all.logging.LogLevel;
import org.games4all.logging.g;
import org.games4all.match.Match;
import org.games4all.match.MatchResult;
import org.games4all.util.SoftwareVersion;

public abstract class GameApplication extends Application {
    private org.games4all.game.d.a a;
    private org.games4all.game.a<?, ?> b;
    private d c;
    private k d;
    private final org.games4all.android.option.d e;
    private f f;
    private b g;
    private org.games4all.gamestore.client.a h;
    private e i;
    private org.games4all.game.b.a j;
    private org.games4all.android.test.a k;
    private Match l;
    private final org.games4all.logging.e m = org.games4all.logging.e.a("G4A");
    private final org.games4all.logging.f n;

    public abstract org.games4all.android.e.b a(Games4AllActivity games4AllActivity);

    public abstract c<?> a(Games4AllActivity games4AllActivity, org.games4all.a.a aVar);

    public abstract Class<? extends Enum<?>> u();

    public abstract org.games4all.game.d.a v();

    public GameApplication() {
        org.games4all.logging.e.a(this.m);
        this.n = new org.games4all.logging.f(1000, new org.games4all.logging.c());
        if (b.a()) {
            g gVar = new g();
            gVar.a(this.n);
            this.m.a(gVar);
        } else {
            this.m.a(this.n);
        }
        this.m.b(LogLevel.DEBUG);
        this.e = new org.games4all.android.option.d();
    }

    public org.games4all.logging.e a() {
        return this.m;
    }

    public org.games4all.logging.f b() {
        return this.n;
    }

    public void onCreate() {
        super.onCreate();
        this.a = v();
        this.b = this.a.b();
        this.c = this.a.c();
        this.d = this.a.e();
        this.f = new RobotOptionsImpl();
        this.e.b(this);
        c();
    }

    public void c() {
        org.games4all.android.b.c cVar = new org.games4all.android.b.c(getSharedPreferences("login-prefs", 0));
        org.games4all.gamestore.client.e eVar = new org.games4all.gamestore.client.e(new File(getFilesDir(), "offline-ratings.dat"));
        for (org.games4all.game.rating.b a2 : q()) {
            eVar.a(a2);
        }
        try {
            try {
                this.h = new org.games4all.gamestore.client.a((String) getPackageManager().getApplicationInfo(getApplicationInfo().packageName, 128).metaData.get("GAMES4ALL_URL"), new SoftwareVersion(getPackageManager().getPackageInfo(getPackageName(), 128).versionName), u(), cVar, eVar);
            } catch (PackageManager.NameNotFoundException e2) {
                throw new RuntimeException(e2);
            } catch (ParseException e3) {
                throw new RuntimeException(e3);
            }
        } catch (PackageManager.NameNotFoundException e4) {
            throw new RuntimeException(e4);
        }
    }

    public void a(String str) {
        this.h.a(str);
    }

    public void a(org.games4all.android.b.d dVar, String str, boolean z) {
        this.k = new org.games4all.android.test.a(this, dVar, str, z);
    }

    public org.games4all.android.test.a d() {
        return this.k;
    }

    public org.games4all.android.option.d e() {
        return this.e;
    }

    public org.games4all.gamestore.client.a f() {
        return this.h;
    }

    public org.games4all.gamestore.client.b g() {
        return this.h.d();
    }

    public void a(org.games4all.game.b.a aVar) {
        this.j = aVar;
        this.g = new b(g(), aVar, (org.games4all.a.a) aVar.a());
    }

    public org.games4all.game.b.a h() {
        return this.j;
    }

    public List<List<PlayerMove>> i() {
        return this.j.k();
    }

    public MatchResult j() {
        return this.j.j();
    }

    public org.games4all.game.model.e<?, ?, ?> k() {
        if (this.j == null) {
            return null;
        }
        return this.j.b();
    }

    public f l() {
        return this.f;
    }

    public org.games4all.game.a<?, ?> m() {
        return this.b;
    }

    public d n() {
        return this.c;
    }

    public org.games4all.game.c.a<?> o() {
        return this.a.d();
    }

    public k p() {
        return this.d;
    }

    public List<org.games4all.game.rating.b> q() {
        ArrayList arrayList = new ArrayList();
        for (org.games4all.game.g a2 : this.a.a().a()) {
            arrayList.addAll(this.a.a(a2, false));
        }
        return arrayList;
    }

    public boolean r() {
        org.games4all.b.a.b g2 = g().g();
        if (g2 == null || !g2.a(org.games4all.b.a.a.c)) {
            return false;
        }
        return true;
    }

    public boolean s() {
        org.games4all.b.a.b g2 = g().g();
        if (g2 == null || !g2.a(org.games4all.b.a.a.d)) {
            return false;
        }
        return true;
    }

    public b t() {
        return this.g;
    }

    public AndroidOptionsEditor.Translator w() {
        return null;
    }

    public org.games4all.game.move.a x() {
        return null;
    }

    public String y() {
        return getClass().getPackage().getName();
    }

    public e z() {
        if (this.i == null) {
            this.i = (e) this.d.a();
        }
        return this.i;
    }

    public void a(e eVar) {
        this.i = eVar;
    }

    public Match A() {
        return this.l;
    }

    public void a(Match match) {
        this.l = match;
    }

    public String a(Resources resources, ContestResult contestResult) {
        int i2;
        int i3;
        if (contestResult.a() == 2) {
            switch (contestResult.a(0, 1)) {
                case WIN:
                    i2 = R.string.g4a_endMatchResultWin;
                    i3 = 1;
                    break;
                case TIE:
                    i2 = R.string.g4a_endMatchResultTie;
                    i3 = 1;
                    break;
                case LOSS:
                    i2 = R.string.g4a_endMatchResultLoss;
                    i3 = 2;
                    break;
                default:
                    throw new RuntimeException();
            }
        } else {
            int a2 = contestResult.a();
            int i4 = 0;
            int i5 = 0;
            for (int i6 = 1; i6 < a2; i6++) {
                switch (contestResult.a(0, i6)) {
                    case WIN:
                        i5++;
                        break;
                    case TIE:
                        i4++;
                        break;
                }
            }
            if (i5 + i4 == a2 - 1) {
                if (i4 > 0) {
                    i2 = R.string.g4a_endMatchResultFirstShared;
                } else {
                    i2 = R.string.g4a_endMatchResultFirst;
                }
            } else if (i5 == 0) {
                if (i4 > 0) {
                    i2 = R.string.g4a_endMatchResultLastShared;
                } else {
                    i2 = R.string.g4a_endMatchResultLast;
                }
            } else if (i4 > 0) {
                i2 = R.string.g4a_endMatchResultPlaceShared;
            } else {
                i2 = R.string.g4a_endMatchResultPlace;
            }
            i3 = a2 - i5;
        }
        return resources.getString(i2, contestResult.toString(), Integer.valueOf(i3));
    }

    public int B() {
        return 2500;
    }
}
