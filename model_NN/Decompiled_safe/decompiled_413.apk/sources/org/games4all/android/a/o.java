package org.games4all.android.a;

import android.graphics.Canvas;
import android.graphics.Point;
import org.games4all.android.b.a;

public interface o {
    int a();

    void a(int i, int i2);

    void a(Canvas canvas);

    int b();

    void b(boolean z);

    boolean b(int i, int i2);

    void c(int i, int i2);

    Point d();

    Point e();

    boolean h();

    int i();

    boolean j();

    Object k();

    a l();

    int m();
}
