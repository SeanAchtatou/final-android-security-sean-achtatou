package org.games4all.android.a;

import android.graphics.Point;
import android.graphics.PointF;
import org.games4all.android.b.a;
import org.games4all.util.b;

public abstract class e implements o {
    private final Point a = new Point();
    private final Point b = new Point();
    private int c;
    private boolean d = true;
    private boolean e;
    private boolean f;
    private a g;
    private Object h;

    /* access modifiers changed from: protected */
    public void a(e eVar) {
        this.a.set(eVar.a.x, eVar.a.y);
        this.b.set(eVar.b.x, eVar.b.y);
        this.c = eVar.c;
        this.d = eVar.d;
    }

    public Point d() {
        return new Point(this.a.x, this.a.y);
    }

    public Point e() {
        return new Point(this.b.x, this.b.y);
    }

    public Point f() {
        return new Point(this.a.x + (b() / 2), this.a.y + (a() / 2));
    }

    public void g() {
        this.a.set(this.b.x, this.b.y);
    }

    public void c(int i, int i2) {
        if (this.a.x == 250 && this.a.y == 106 && i == 8 && i2 == 288) {
            b.a("unexpected change?");
        }
        this.a.set(i, i2);
    }

    public void a(int i, int i2) {
        c(i, i2);
        this.b.set(i, i2);
    }

    public void a(Point point) {
        a(point.x, point.y);
    }

    public boolean h() {
        return this.e;
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void b(boolean z) {
        this.f = z;
    }

    public int i() {
        return this.c;
    }

    public void b(int i) {
        this.c = i;
    }

    public boolean j() {
        return this.d;
    }

    public void c(boolean z) {
        this.d = z;
    }

    public void a(Object obj) {
        this.h = obj;
    }

    public Object k() {
        return this.h;
    }

    public String toString() {
        return "AbstractSprite[x=" + this.a.x + ",y=" + this.a.y + "]";
    }

    public a l() {
        return this.g;
    }

    public int m() {
        return (int) PointF.length((float) (this.a.x - this.b.x), (float) (this.a.y - this.b.y));
    }
}
