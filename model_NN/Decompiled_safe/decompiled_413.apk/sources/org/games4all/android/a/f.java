package org.games4all.android.a;

public abstract class f implements b {
    private int a;
    private int b;
    private float c;
    private float d;

    public f() {
        e();
    }

    public int b() {
        return this.b == -1 ? d() : this.b;
    }

    public int a() {
        return this.a == -1 ? c() : this.a;
    }

    public void e() {
        this.a = -1;
        this.b = -1;
    }

    public float f() {
        return this.c;
    }

    public float g() {
        return this.d;
    }
}
