package org.games4all.android.a;

import android.graphics.Canvas;
import android.graphics.Point;
import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.games4all.c.b;
import org.games4all.c.c;

public class g {
    private static final Comparator<o> a = new c();
    private final org.games4all.a.a b;
    private final List<o> c = new ArrayList();
    private final List<a> d = new ArrayList();
    private final List<a> e = new ArrayList();
    private final List<Runnable> f = new ArrayList();
    private o g;
    private final List<o> h = new ArrayList();
    private final Point i = new Point();
    private boolean j;
    private final c<k> k = new c<>(k.class);

    private class a {
        private final d b;
        private final Runnable c;
        private final long d;

        public a(d dVar, Runnable runnable, long j) {
            this.d = j;
            this.b = dVar;
            this.c = runnable;
        }

        public d a() {
            return this.b;
        }

        public Runnable b() {
            return this.c;
        }

        public long c() {
            return this.d;
        }
    }

    public g(org.games4all.a.a aVar) {
        this.b = aVar;
    }

    public b a(k kVar) {
        return this.k.c(kVar);
    }

    public void a(o oVar) {
        this.c.add(oVar);
    }

    public void a(d dVar, Runnable runnable, long j2) {
        long currentTimeMillis;
        if (j2 == 0) {
            currentTimeMillis = System.currentTimeMillis();
        } else {
            currentTimeMillis = j2 < 60000 ? System.currentTimeMillis() + j2 : j2;
        }
        this.d.add(new a(dVar, runnable, currentTimeMillis));
        this.b.a();
        this.k.c().a();
    }

    public void a(Canvas canvas) {
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.d.isEmpty()) {
            for (a next : this.d) {
                if (currentTimeMillis >= next.c()) {
                    next.a().a(canvas, currentTimeMillis);
                }
            }
            this.e.clear();
            this.e.addAll(this.d);
            for (a next2 : this.e) {
                if (next2.a().a(currentTimeMillis)) {
                    this.d.remove(next2);
                    Runnable b2 = next2.b();
                    if (b2 != null) {
                        b2.run();
                    }
                    this.b.b();
                }
            }
        } else if (!this.f.isEmpty()) {
            this.f.remove(0).run();
            this.b.b();
        }
        Collections.sort(this.c, a);
        for (o next3 : this.c) {
            if (next3.j()) {
                next3.a(canvas);
            }
        }
        if (!this.d.isEmpty() || !this.f.isEmpty()) {
            this.k.c().a();
        }
    }

    public boolean a(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        switch (action) {
            case 0:
                o a2 = a(x, y);
                if (a2 != null) {
                    a(a2, x, y);
                    this.j = true;
                    break;
                }
                break;
            case 1:
                if (this.g != null) {
                    if (motionEvent.getEventTime() - motionEvent.getDownTime() > 1500 && this.j && this.g.l() != null) {
                        d();
                        break;
                    } else {
                        c();
                        break;
                    }
                }
                break;
            case 2:
                if (this.g != null) {
                    if (this.g.m() > 10) {
                        this.j = false;
                    }
                    a(motionEvent, this.i.x + x, this.i.y + y);
                    break;
                }
                break;
        }
        return true;
    }

    private o a(int i2, int i3) {
        o oVar = null;
        int i4 = Integer.MIN_VALUE;
        for (o next : this.c) {
            if (next.j() && next.h() && next.b(i2, i3)) {
                if (oVar == null || i4 < next.i()) {
                    i4 = next.i();
                    oVar = next;
                }
            }
        }
        return oVar;
    }

    private void a(o oVar, int i2, int i3) {
        this.g = oVar;
        this.h.clear();
        oVar.b(true);
        Point d2 = oVar.d();
        this.i.set(d2.x - i2, d2.y - i3);
        k c2 = this.k.c();
        c2.c(oVar);
        c2.a();
    }

    private void a(MotionEvent motionEvent, int i2, int i3) {
        this.g.c(i2, i3);
        Point e2 = this.g.e();
        int size = this.h.size();
        for (int i4 = 0; i4 < size; i4++) {
            o oVar = this.h.get(i4);
            Point e3 = oVar.e();
            oVar.c((e3.x - e2.x) + i2, (e3.y - e2.y) + i3);
        }
        k c2 = this.k.c();
        c2.a(this.g, (int) motionEvent.getX(), (int) motionEvent.getY());
        c2.a();
    }

    private void c() {
        this.g.b(false);
        k c2 = this.k.c();
        c2.b(this.g);
        this.g = null;
        c2.a();
    }

    private void d() {
        org.games4all.android.b.a l = this.g.l();
        if (l != null) {
            this.g.b(false);
            k c2 = this.k.c();
            System.err.println("calling dragAborted");
            c2.a(this.g);
            this.g = null;
            c2.a();
            c2.a(l);
        }
    }

    public void a() {
        int size = this.d.size() + this.f.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.b.b();
        }
        this.d.clear();
        this.f.clear();
    }

    public boolean b() {
        return !this.d.isEmpty();
    }
}
