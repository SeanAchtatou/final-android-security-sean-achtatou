package org.games4all.android.a;

import android.graphics.Canvas;
import android.graphics.Point;

public class i implements d {
    private final o a;
    private final Point b;
    private final Point c;
    private long d = 0;
    private final int e;
    private boolean f;

    public static int a(Point point, Point point2, int i) {
        int i2 = point.x - point2.x;
        int i3 = point.y - point2.y;
        return (((int) Math.sqrt((double) ((i2 * i2) + (i3 * i3)))) * 1000) / i;
    }

    public static i a(o oVar, Point point, Point point2, int i) {
        return new i(oVar, point, point2, a(point, point2, i));
    }

    public i(o oVar, Point point, Point point2, int i) {
        this.a = oVar;
        this.b = new Point(point);
        this.c = new Point(point2);
        this.e = i;
    }

    public void a(boolean z) {
        this.f = z;
    }

    public void a(Canvas canvas, long j) {
        int i;
        int i2;
        if (this.d == 0) {
            this.d = j;
        }
        int i3 = (int) (j - this.d);
        if (i3 >= this.e) {
            int i4 = this.c.x;
            i = i4;
            i2 = this.c.y;
        } else {
            i = this.b.x + (((this.c.x - this.b.x) * i3) / this.e);
            i2 = ((i3 * (this.c.y - this.b.y)) / this.e) + this.b.y;
        }
        if (this.f) {
            this.a.a(i, i2);
        } else {
            this.a.c(i, i2);
        }
    }

    public boolean a(long j) {
        return this.d != 0 && j - this.d >= ((long) this.e);
    }
}
