package org.games4all.android.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class m extends e implements Cloneable {
    private Drawable a;
    private final Rect b = new Rect();
    private String c;
    private String d;
    private b e;
    private b f;
    private Paint g;
    private int h;
    private int i;
    private int j;

    public m(Drawable drawable) {
        this.a = drawable;
        if (drawable != null) {
            drawable.getPadding(this.b);
        }
        this.g = new Paint();
        this.g.setAntiAlias(true);
        this.g.setTextSize(15.0f);
        a("");
        this.j = 255;
    }

    public Drawable c() {
        return this.a;
    }

    public void a(Drawable drawable) {
        this.a = drawable;
        drawable.getPadding(this.b);
    }

    public Rect n() {
        return this.b;
    }

    public void a(float f2) {
        this.g.setTextSize(f2);
    }

    public Object clone() {
        m mVar = new m(this.a);
        mVar.c = this.c;
        mVar.h = this.h;
        mVar.i = this.i;
        mVar.j = this.j;
        mVar.g.set(this.g);
        mVar.a((e) this);
        return mVar;
    }

    public void a(String str) {
        this.c = str;
        o();
    }

    public void b(String str) {
        this.d = str;
        o();
    }

    public void a(b bVar) {
        this.e = bVar;
    }

    public void o() {
        int b2;
        int i2;
        int b3 = this.e == null ? 0 : this.e.b();
        if (this.f == null) {
            b2 = 0;
        } else {
            b2 = this.f.b();
        }
        int max = Math.max(b3, b2);
        this.i = (int) this.g.getFontSpacing();
        if (this.d != null) {
            this.i = (int) (((float) this.i) + this.g.getFontSpacing());
            i2 = (int) this.g.measureText(this.d);
        } else {
            i2 = 0;
        }
        this.i = Math.max(this.i, max);
        this.i += this.b.top + this.b.bottom;
        this.h = Math.max((this.e == null ? 0 : this.e.a() + 2) + ((int) this.g.measureText(this.c)) + (this.f == null ? 0 : this.f.a() + 2) + this.b.left + this.b.right, i2 + this.b.left + this.b.right);
    }

    public boolean b(int i2, int i3) {
        Point d2 = d();
        int i4 = i2 - d2.x;
        int i5 = i3 - d2.y;
        if (i4 < 0 || i5 < 0 || i4 >= this.h || i5 >= this.h) {
            return false;
        }
        return true;
    }

    public void a(Canvas canvas) {
        int i2;
        Point d2 = d();
        this.g.setAlpha(this.j);
        if (this.a != null) {
            this.a.setBounds(d2.x, d2.y, d2.x + this.h, d2.y + this.i);
            this.a.setAlpha(this.j);
            this.a.draw(canvas);
        }
        int i3 = d2.x + this.b.left;
        int i4 = (this.h - this.b.left) - this.b.right;
        if (this.e != null) {
            int a2 = this.e.a();
            i3 += a2 + 2;
            i4 -= a2 + 2;
            this.e.a(canvas, d2.x + this.b.left, (d2.y + this.b.top) - 2, this.j);
        }
        int i5 = i4;
        int i6 = i3;
        int i7 = i5;
        if (this.f != null) {
            int a3 = this.f.a();
            i7 -= a3 + 2;
            this.f.a(canvas, ((d2.x + this.h) - this.b.right) - a3, (d2.y + this.b.top) - 2, this.j);
        }
        if (!(this.e == null && this.f == null) && (this.e == null || this.f == null)) {
            i2 = i6;
        } else {
            i2 = (int) (((((float) i7) - this.g.measureText(this.c)) / 2.0f) + ((float) i6));
        }
        int fontSpacing = (int) this.g.getFontSpacing();
        int ascent = (((this.i - (this.d == null ? fontSpacing : fontSpacing * 2)) / 2) + d2.y) - ((int) this.g.ascent());
        float density = (float) (canvas.getDensity() / 160);
        float f2 = 0.0f;
        if (!(this.e == null && this.f == null)) {
            f2 = 4.0f * density;
        }
        canvas.drawText(this.c, (float) i2, f2 + ((float) ascent), this.g);
        if (this.d != null) {
            canvas.drawText(this.d, (float) (d2.x + ((this.h - ((int) this.g.measureText(this.d))) / 2)), ((float) (fontSpacing + ascent)) + (2.0f * density), this.g);
        }
    }

    public int a() {
        return this.i;
    }

    public int b() {
        return this.h;
    }

    public void a(int i2) {
        this.j = i2;
    }
}
