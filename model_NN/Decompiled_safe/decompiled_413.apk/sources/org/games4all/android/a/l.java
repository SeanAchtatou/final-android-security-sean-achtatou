package org.games4all.android.a;

import android.graphics.Canvas;
import java.util.ArrayList;
import java.util.List;

public class l implements d {
    public List<d> a = new ArrayList();

    public void a(d dVar) {
        this.a.add(dVar);
    }

    public boolean a(long j) {
        boolean z = true;
        for (d a2 : this.a) {
            z &= a2.a(j);
        }
        return z;
    }

    public void a(Canvas canvas, long j) {
        for (d a2 : this.a) {
            a2.a(canvas, j);
        }
    }
}
