package org.games4all.android.a;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.util.FloatMath;

public class h extends f {
    public static final Paint a = new Paint();
    private final Bitmap b;
    private final Rect c = new Rect();
    private final RectF d = new RectF();

    public h(Resources resources, int i) {
        this.b = ((BitmapDrawable) resources.getDrawable(i)).getBitmap();
    }

    public void a(Canvas canvas, int i, int i2, int i3) {
        float f;
        float f2;
        int width = this.b.getWidth();
        int height = this.b.getHeight();
        float g = g();
        if (g != 0.0f) {
            f = (((float) width) / 2.0f) * FloatMath.sin(g);
        } else {
            f = 0.0f;
        }
        if (f() != 0.0f) {
            f2 = FloatMath.sin(g) * (((float) width) / 2.0f);
        } else {
            f2 = 0.0f;
        }
        this.c.set(0, 0, width, width);
        this.d.set((float) i, (float) i2, (float) (width + i), (float) (height + i2));
        this.d.left += f;
        this.d.right -= f;
        this.d.top += f2;
        this.d.bottom -= f2;
        a.setAlpha(i3);
        canvas.drawBitmap(this.b, this.c, this.d, a);
    }

    public int d() {
        return this.b.getHeight();
    }

    public int c() {
        return this.b.getWidth();
    }
}
