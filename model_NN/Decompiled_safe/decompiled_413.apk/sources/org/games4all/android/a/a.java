package org.games4all.android.a;

import android.graphics.Canvas;
import android.graphics.Point;

public class a extends e {
    private b a;
    private int b = 255;
    private float c;

    public a(b bVar) {
        a(bVar);
    }

    public void a(b bVar) {
        this.a = bVar;
    }

    public void a(Canvas canvas) {
        Point d = d();
        canvas.save(1);
        if (this.c == 0.0f) {
            canvas.translate((float) d.x, (float) d.y);
            this.a.a(canvas, 0, 0, this.b);
        } else {
            canvas.rotate(this.c, (float) (d.x + (b() / 2)), (float) (d.y + (a() / 2)));
            canvas.translate((float) d.x, (float) d.y);
            this.a.a(canvas, 0, 0, this.b);
        }
        canvas.restore();
    }

    public void a(int i, int i2) {
        super.a(i, i2);
    }

    public int a() {
        return this.a.b();
    }

    public int b() {
        return this.a.a();
    }

    public boolean b(int i, int i2) {
        Point d = d();
        if (i < d.x || i2 < d.y || i >= d.x + this.a.a() || i2 >= d.y + this.a.b()) {
            return false;
        }
        return true;
    }

    public void a(int i) {
        this.b = i;
    }

    public void a(float f) {
        this.c = f;
    }

    public float c() {
        return this.c;
    }
}
