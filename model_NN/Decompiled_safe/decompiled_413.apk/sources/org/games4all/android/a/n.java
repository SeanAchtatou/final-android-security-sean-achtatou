package org.games4all.android.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

public class n extends f {
    public static final Paint a = new Paint();
    private final Path b;
    private final Paint c;
    private final Paint d;
    private final RectF e = new RectF();

    public n(Path path, Paint paint, Paint paint2) {
        this.b = path;
        this.c = paint;
        this.d = paint2;
        path.computeBounds(this.e, true);
    }

    public void a(Canvas canvas, int i, int i2, int i3) {
        this.b.moveTo((float) i, (float) i2);
        this.c.setAlpha(i3);
        canvas.drawPath(this.b, this.c);
        this.d.setAlpha(i3);
        canvas.drawPath(this.b, this.d);
    }

    public int d() {
        return (int) this.e.bottom;
    }

    public int c() {
        return (int) this.e.right;
    }
}
