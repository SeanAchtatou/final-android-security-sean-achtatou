package org.games4all.android.a;

import java.util.Comparator;

class c implements Comparator<o> {
    c() {
    }

    /* renamed from: a */
    public int compare(o oVar, o oVar2) {
        int i = oVar.i();
        int i2 = oVar2.i();
        if (i < i2) {
            return -1;
        }
        if (i > i2) {
            return 1;
        }
        return 0;
    }
}
