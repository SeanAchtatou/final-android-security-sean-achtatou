package org.games4all.android.a;

import android.graphics.Canvas;

public class j implements d {
    private final a a;
    private final float b;
    private final float c;
    private long d = 0;
    private final int e;

    public j(a aVar, float f, float f2, int i) {
        this.a = aVar;
        this.b = f;
        this.c = f2;
        this.e = i;
    }

    public void a(Canvas canvas, long j) {
        float f;
        if (this.d == 0) {
            this.d = j;
        }
        int i = (int) (j - this.d);
        if (i >= this.e) {
            f = this.c;
        } else {
            f = ((((float) i) * (this.c - this.b)) / ((float) this.e)) + this.b;
        }
        this.a.a(f);
    }

    public boolean a(long j) {
        return this.d != 0 && j - this.d >= ((long) this.e);
    }
}
