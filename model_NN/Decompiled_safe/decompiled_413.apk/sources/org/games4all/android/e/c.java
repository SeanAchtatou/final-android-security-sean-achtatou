package org.games4all.android.e;

import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.b.e;
import org.games4all.android.games.indianrummy.prod.R;

public class c extends d implements View.OnClickListener {
    private final TextView a = ((TextView) findViewById(R.id.g4a_alertDialogTitle));
    private final TextView b = ((TextView) findViewById(R.id.g4a_alertDialogMessage));
    private final LinearLayout c = ((LinearLayout) findViewById(R.id.g4a_alertDialogContent));
    private final LinearLayout d = ((LinearLayout) findViewById(R.id.g4a_alertDialogButtons));
    private final Button[] e;
    private DialogInterface.OnClickListener f;

    public c(Games4AllActivity games4AllActivity, int i) {
        super(games4AllActivity, 16973913);
        setContentView((int) R.layout.g4a_alert_dialog);
        e eVar = new e(games4AllActivity);
        this.e = new Button[i];
        for (int i2 = 0; i2 < i; i2++) {
            Button button = new Button(games4AllActivity);
            this.e[i2] = button;
            this.e[i2].setId(eVar.c("g4a_dialogButton" + i2));
            button.setOnClickListener(this);
            button.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
            this.d.addView(button);
        }
    }

    public void a(View view) {
        this.c.addView(view);
    }

    public void setTitle(int i) {
        this.a.setText(i);
    }

    public void a(String str) {
        this.a.setText(str);
    }

    public void a(int i) {
        this.b.setText(i);
    }

    public void b(String str) {
        this.b.setText(str);
    }

    public void a(int i, int i2) {
        this.e[i].setText(i2);
    }

    public void a(int i, String str) {
        this.e[i].setText(str);
    }

    public void onClick(View view) {
        int i = 0;
        while (i < this.e.length) {
            if (view != this.e[i] || this.f == null) {
                i++;
            } else {
                dismiss();
                this.f.onClick(this, i);
                return;
            }
        }
    }

    public void a(DialogInterface.OnClickListener onClickListener) {
        this.f = onClickListener;
    }
}
