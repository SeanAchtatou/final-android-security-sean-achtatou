package org.games4all.android.e;

import android.content.res.Resources;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.flurry.android.u;
import java.util.HashMap;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.b.a;
import org.games4all.android.games.indianrummy.prod.R;

public class e extends d implements View.OnClickListener {
    private final TextView a = ((TextView) findViewById(R.id.g4a_tooltipTitle));
    private final TextView b = ((TextView) findViewById(R.id.g4a_tooltipContent));
    private final Button c = ((Button) findViewById(R.id.g4a_closeButton));

    public e(Games4AllActivity games4AllActivity) {
        super(games4AllActivity, 16973913);
        setContentView((int) R.layout.g4a_tooltip);
        this.c.setOnClickListener(this);
    }

    public void a(a aVar) {
        Resources resources = getContext().getResources();
        String a2 = aVar.a(resources);
        String b2 = aVar.b(resources);
        this.a.setText(a2);
        this.b.setText(Html.fromHtml(b2));
        HashMap hashMap = new HashMap();
        hashMap.put("tooltip", a2);
        u.a("tooltip", hashMap);
    }

    public void onClick(View view) {
        if (view == this.c) {
            dismiss();
        }
    }
}
