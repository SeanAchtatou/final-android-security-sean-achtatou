package org.games4all.android.e;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import org.games4all.android.GameApplication;
import org.games4all.android.activity.Games4AllActivity;

public class d extends Dialog {
    private final GameApplication a;
    private View b;
    private int c = -1;

    public d(Games4AllActivity games4AllActivity, int i) {
        super(games4AllActivity, i);
        this.a = games4AllActivity.p();
        setOwnerActivity(games4AllActivity);
    }

    public d(Games4AllActivity games4AllActivity) {
        super(games4AllActivity);
        this.a = games4AllActivity.p();
        setOwnerActivity(games4AllActivity);
    }

    public Games4AllActivity c() {
        return (Games4AllActivity) getOwnerActivity();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        c().a(this);
        this.a.d().a(this);
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        c().b(this);
        this.a.d().a((d) null);
        super.onStop();
    }

    public void setContentView(int i) {
        setContentView(LayoutInflater.from(getContext()).inflate(i, (ViewGroup) null));
    }

    public void setContentView(View view) {
        super.setContentView(view);
        this.b = view;
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(view, layoutParams);
        this.b = view;
    }

    public View d() {
        return this.b;
    }

    public int e() {
        if (this.c == -1) {
            return this.b.getId();
        }
        return this.c;
    }

    public void b(int i) {
        this.c = i;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        Activity ownerActivity;
        if ((!this.a.r() && !this.a.s()) || (ownerActivity = getOwnerActivity()) == null) {
            return super.onCreateOptionsMenu(menu);
        }
        if (ownerActivity.onCreateOptionsMenu(menu)) {
            return ownerActivity.onPrepareOptionsMenu(menu);
        }
        return false;
    }

    public boolean f() {
        return c().t() == this;
    }
}
