package org.games4all.android.e;

import android.widget.ProgressBar;
import android.widget.TextView;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.games.indianrummy.prod.R;

public class f extends d {
    private final TextView a = ((TextView) findViewById(R.id.g4a_progressDialogTitle));
    private final TextView b = ((TextView) findViewById(R.id.g4a_progressDialogMessage));
    private final ProgressBar c = ((ProgressBar) findViewById(R.id.g4a_progressDialogProgress));

    public f(Games4AllActivity games4AllActivity) {
        super(games4AllActivity, 16973913);
        setContentView((int) R.layout.g4a_progress_dialog);
    }

    public void setTitle(int i) {
        this.a.setText(i);
    }

    public void a(String str) {
        this.a.setText(str);
    }

    public void b(String str) {
        this.b.setText(str);
    }

    public void a(boolean z) {
        this.c.setIndeterminate(z);
    }
}
