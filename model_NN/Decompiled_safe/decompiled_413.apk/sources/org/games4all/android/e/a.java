package org.games4all.android.e;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import com.flurry.android.u;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import org.games4all.android.activity.Games4AllActivity;
import org.games4all.android.b.e;
import org.games4all.android.games.indianrummy.prod.R;
import org.games4all.util.c;

public class a extends d implements View.OnClickListener {
    private static boolean a;
    private final TextView b = ((TextView) findViewById(R.id.g4a_helpTitle));
    private final WebView c = ((WebView) findViewById(R.id.g4a_helpContent));
    private final Button d;

    public static final a a(Games4AllActivity games4AllActivity) {
        SharedPreferences sharedPreferences = games4AllActivity.getSharedPreferences("help", 0);
        if (a) {
            return null;
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("help-main", false);
        edit.commit();
        a = true;
        return b(games4AllActivity);
    }

    public static final a b(Games4AllActivity games4AllActivity) {
        a aVar = new a(games4AllActivity);
        Resources resources = games4AllActivity.getResources();
        String b2 = new e(games4AllActivity).b("g4a_game");
        try {
            aVar.a(resources.getString(R.string.g4a_help_title, b2, games4AllActivity.getPackageManager().getPackageInfo(games4AllActivity.getPackageName(), 128).versionName), "help");
            aVar.show();
            return aVar;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public a(Games4AllActivity games4AllActivity) {
        super(games4AllActivity, 16973913);
        setContentView((int) R.layout.g4a_help);
        this.c.setBackgroundColor(0);
        this.d = (Button) findViewById(R.id.g4a_closeButton);
        this.d.setOnClickListener(this);
    }

    public void a(String str, String str2) {
        InputStream open;
        String str3;
        try {
            this.b.setText(str);
            AssetManager assets = getContext().getResources().getAssets();
            String str4 = str2 + "/index.html";
            try {
                open = assets.open(str2 + "-" + Locale.getDefault().getLanguage() + "/index.html");
            } catch (IOException e) {
                open = assets.open(str4);
            }
            DisplayMetrics displayMetrics = new DisplayMetrics();
            c().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            switch (displayMetrics.densityDpi) {
                case 120:
                    str3 = "ldpi";
                    break;
                case 160:
                    str3 = "mdpi";
                    break;
                case 240:
                    str3 = "hdpi";
                    break;
                default:
                    str3 = "hdpi";
                    break;
            }
            String str5 = "file:///android_asset/img-" + str3 + "/index.html";
            if (open != null) {
                System.err.println("baseUrl: " + str5);
                this.c.loadDataWithBaseURL(str5, c.b(open), "text/html", "UTF-8", null);
            }
            u.a("help");
        } catch (IOException e2) {
        }
    }

    public void onClick(View view) {
        if (view == this.d) {
            cancel();
        }
    }
}
