package org.games4all.c;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class c<L> {
    private final Class<L> a;
    private final List<L> b = new ArrayList();
    private Comparator<L> c = new C0017c();
    private L d;

    class a implements InvocationHandler {
        a() {
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            if (method.getDeclaringClass() == c.this.b()) {
                for (Object invoke : c.this.a()) {
                    method.invoke(invoke, objArr);
                }
                return null;
            }
            method.invoke(this, objArr);
            return null;
        }
    }

    public c(Class<L> cls) {
        this.a = cls;
    }

    /* renamed from: org.games4all.c.c$c  reason: collision with other inner class name */
    class C0017c implements Comparator<L> {
        C0017c() {
        }

        public int compare(L l, L l2) {
            return Integer.signum(a(l) - a(l2));
        }

        private int a(L l) {
            if (l instanceof a) {
                return ((a) l).e();
            }
            e eVar = (e) l.getClass().getAnnotation(e.class);
            if (eVar != null) {
                return eVar.a();
            }
            return 0;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r3.b.add(0, r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(L r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.List<L> r0 = r3.b     // Catch:{ all -> 0x002c }
            int r0 = r0.size()     // Catch:{ all -> 0x002c }
            r1 = 1
            int r0 = r0 - r1
        L_0x0009:
            if (r0 < 0) goto L_0x0025
            java.util.Comparator<L> r1 = r3.c     // Catch:{ all -> 0x002c }
            java.util.List<L> r2 = r3.b     // Catch:{ all -> 0x002c }
            java.lang.Object r2 = r2.get(r0)     // Catch:{ all -> 0x002c }
            int r1 = r1.compare(r2, r4)     // Catch:{ all -> 0x002c }
            if (r1 > 0) goto L_0x0022
            java.util.List<L> r1 = r3.b     // Catch:{ all -> 0x002c }
            int r0 = r0 + 1
            r1.add(r0, r4)     // Catch:{ all -> 0x002c }
        L_0x0020:
            monitor-exit(r3)
            return
        L_0x0022:
            int r0 = r0 + -1
            goto L_0x0009
        L_0x0025:
            java.util.List<L> r0 = r3.b     // Catch:{ all -> 0x002c }
            r1 = 0
            r0.add(r1, r4)     // Catch:{ all -> 0x002c }
            goto L_0x0020
        L_0x002c:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.c.c.a(java.lang.Object):void");
    }

    public void b(L l) {
        synchronized (this.b) {
            Iterator<L> it = this.b.iterator();
            while (it.hasNext()) {
                if (it.next() == l) {
                    it.remove();
                    return;
                }
            }
        }
    }

    class b implements b {
        final /* synthetic */ Object a;

        b(Object obj) {
            this.a = obj;
        }

        public void a() {
            c.this.b(this.a);
        }
    }

    public b c(L l) {
        a(l);
        return new b(l);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<L>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    /* access modifiers changed from: protected */
    public L[] a() {
        return this.b.toArray((Object[]) Array.newInstance((Class<?>) this.a, this.b.size()));
    }

    /* access modifiers changed from: package-private */
    public Class<L> b() {
        return this.a;
    }

    private L d() {
        try {
            a aVar = new a();
            return Proxy.newProxyInstance(c.class.getClassLoader(), new Class[]{this.a}, aVar);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    public L c() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    this.d = d();
                }
            }
        }
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (L next : this.b) {
            if (z) {
                z = false;
            } else {
                sb.append(',');
            }
            sb.append(next.getClass());
        }
        return sb.toString();
    }
}
