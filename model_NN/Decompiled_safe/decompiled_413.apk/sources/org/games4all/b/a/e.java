package org.games4all.b.a;

public class e implements f {
    private final String a;

    public e(String str) {
        this.a = str;
    }

    public String a() {
        return this.a;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.a == null ? 0 : this.a.hashCode()) + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        e eVar = (e) obj;
        if (this.a == null) {
            if (eVar.a != null) {
                return false;
            }
        } else if (!this.a.equals(eVar.a)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return this.a;
    }
}
