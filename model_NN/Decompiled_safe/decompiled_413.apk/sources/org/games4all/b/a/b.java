package org.games4all.b.a;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class b implements c {
    private final int a;
    private final String b;
    private String c;
    private int d;
    private int e;
    private final Date f;
    private Date g;
    private String h;
    private final Set<f> i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static c a(String str) {
        String[] split = str.split(",");
        int i2 = 0 + 1;
        int parseInt = Integer.parseInt(split[0]);
        int i3 = i2 + 1;
        String str2 = split[i2];
        int i4 = i3 + 1;
        String str3 = split[i3];
        int i5 = i4 + 1;
        int parseInt2 = Integer.parseInt(split[i4]);
        int i6 = i5 + 1;
        int parseInt3 = Integer.parseInt(split[i5]);
        int i7 = i6 + 1;
        Date date = new Date(Long.parseLong(split[i6]));
        int i8 = i7 + 1;
        Date date2 = new Date(Long.parseLong(split[i7]));
        int i9 = i8 + 1;
        String replace = split[i8].replace('*', ',');
        d a2 = a.a();
        HashSet hashSet = new HashSet();
        System.err.println("ROLE STRING: " + split[i9]);
        int i10 = i9 + 1;
        for (String a3 : split[i9].split("\\|")) {
            f a4 = a2.a(a3);
            hashSet.add(a4);
            System.err.println("added " + a4);
        }
        return new b(parseInt, str2, str3, parseInt2, parseInt3, date, date2, replace, hashSet);
    }

    public b(int i2, String str, Date date) {
        this.a = i2;
        this.b = str;
        this.f = date;
        this.i = new HashSet();
    }

    public b(int i2, String str, String str2, int i3, int i4, Date date, Date date2, String str3, Set<f> set) {
        this.a = i2;
        this.b = str;
        this.c = str2;
        this.d = i3;
        this.e = i4;
        this.f = date;
        this.g = date2;
        this.h = str3;
        this.i = set;
    }

    public int a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public boolean a(f fVar) {
        return this.i.contains(fVar);
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.h;
    }

    public String toString() {
        return "PrincipalImpl[name=" + this.b + ",id=" + this.a + ",loginCount=" + this.d + ",email=" + this.h + "]";
    }
}
