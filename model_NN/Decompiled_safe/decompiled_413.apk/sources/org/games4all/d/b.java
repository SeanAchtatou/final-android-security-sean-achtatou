package org.games4all.d;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.text.FieldPosition;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import org.games4all.util.j;

public class b implements InvocationHandler, j {
    private static final Object[] g = new Object[0];
    private final ResourceBundle a;
    private final String b;
    private final org.games4all.util.a c = new org.games4all.util.a("$[", "]", this);
    private final org.games4all.util.a d = new org.games4all.util.a("#{", "}", this.e);
    private final a e = new a();
    private final Map<Method, c> f = new IdentityHashMap();

    static class a implements j {
        private Object[] a;
        private int b;

        a() {
        }

        public void a(Object[] objArr, int i) {
            this.a = objArr;
            this.b = i;
        }

        public String a(String str) {
            String valueOf;
            Object obj = this.a[Integer.parseInt(str) + this.b];
            if (obj instanceof Enum) {
                valueOf = ((Enum) obj).name();
            } else {
                valueOf = String.valueOf(obj);
            }
            return "[" + valueOf + "]";
        }
    }

    public static <T extends d> T a(Class cls, String str, Locale locale) {
        String str2;
        ClassLoader classLoader = cls.getClassLoader();
        if (str.startsWith(".")) {
            str2 = cls.getPackage().getName() + str;
        } else {
            str2 = str;
        }
        return a(cls, ResourceBundle.getBundle(str2, locale, classLoader), (String) null);
    }

    public static <T extends d> T a(Class cls, ResourceBundle resourceBundle, String str) {
        return (d) Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new b(resourceBundle, str));
    }

    public b(ResourceBundle resourceBundle, String str) {
        this.a = resourceBundle;
        this.b = str;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        Object[] objArr2 = objArr == null ? g : objArr;
        Class<?> returnType = method.getReturnType();
        if (returnType == String.class) {
            return a(method, objArr2);
        }
        if (d.class.isAssignableFrom(returnType)) {
            return c(method, objArr2);
        }
        throw new RuntimeException("No translator method: " + method);
    }

    private String a(Method method, Object[] objArr) {
        c cVar = this.f.get(method);
        if (cVar == null) {
            cVar = b(method, objArr);
            this.f.put(method, cVar);
        }
        return a(a(cVar.a(), cVar.b(), objArr), objArr, cVar.b());
    }

    private String a(String str, Object[] objArr, int i) {
        this.e.a(objArr, i);
        String a2 = this.d.a(str);
        try {
            a2 = this.c.a(this.a.getString(a2));
        } catch (MissingResourceException e2) {
        }
        int length = objArr.length - i;
        Object[] objArr2 = new Object[length];
        for (int i2 = 0; i2 < length; i2++) {
            objArr2[i2] = objArr[i + i2];
        }
        MessageFormat messageFormat = new MessageFormat(a2);
        StringBuffer stringBuffer = new StringBuffer();
        messageFormat.format(objArr2, stringBuffer, (FieldPosition) null);
        return stringBuffer.toString();
    }

    private c b(Method method, Object[] objArr) {
        boolean z;
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        boolean z2 = false;
        int i = 0;
        while (!z2 && i < parameterAnnotations.length) {
            Annotation[] annotationArr = parameterAnnotations[i];
            int length = annotationArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                } else if (annotationArr[i2].annotationType() == a.class) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z2) {
                i++;
            }
        }
        for (int i3 = i; i3 < parameterAnnotations.length; i3++) {
            Annotation[] annotationArr2 = parameterAnnotations[i];
            int length2 = annotationArr2.length;
            int i4 = 0;
            while (true) {
                if (i4 >= length2) {
                    z = false;
                    break;
                } else if (annotationArr2[i4].annotationType() == a.class) {
                    z = true;
                    break;
                } else {
                    i4++;
                }
            }
            if (!z) {
                throw new RuntimeException("illegal mix of index and argument parameters");
            }
        }
        return new c(b(method.getName()), i);
    }

    private d c(Method method, Object[] objArr) {
        c cVar = this.f.get(method);
        if (cVar == null) {
            cVar = d(method, objArr);
            this.f.put(method, cVar);
        }
        return a(method.getReturnType(), this.a, a(cVar.a(), cVar.b(), objArr));
    }

    private c d(Method method, Object[] objArr) {
        for (Annotation[] annotationArr : method.getParameterAnnotations()) {
            for (Annotation annotationType : r0[r2]) {
                if (annotationType.annotationType() == a.class) {
                    throw new RuntimeException("No argument parameters allowed in translator creation methods");
                }
            }
        }
        return new c(b(method.getName()), objArr.length);
    }

    private String b(String str) {
        if (this.b == null) {
            return str;
        }
        return this.b + "." + str;
    }

    private String a(String str, int i, Object[] objArr) {
        String obj;
        StringBuilder sb = new StringBuilder(str);
        for (int i2 = 0; i2 < i; i2++) {
            Object obj2 = objArr[i2];
            if (obj2 instanceof Enum) {
                obj = ((Enum) obj2).name();
            } else {
                obj = obj2.toString();
            }
            sb.append('[');
            sb.append(obj);
            sb.append(']');
        }
        return sb.toString();
    }

    public String a(String str) {
        int indexOf = str.indexOf(40);
        if (indexOf == -1) {
            String a2 = this.d.a(str);
            try {
                return this.a.getString(a2);
            } catch (MissingResourceException e2) {
                return a2;
            }
        } else {
            String substring = str.substring(0, indexOf);
            if (str.lastIndexOf(41) != str.length() - 1) {
                return str;
            }
            String substring2 = str.substring(indexOf + 1, str.length() - 1);
            ArrayList arrayList = new ArrayList();
            StringTokenizer stringTokenizer = new StringTokenizer(substring2, ",");
            while (stringTokenizer.hasMoreTokens()) {
                arrayList.add(stringTokenizer.nextToken().trim());
            }
            return a(substring, arrayList.toArray(), 0);
        }
    }
}
