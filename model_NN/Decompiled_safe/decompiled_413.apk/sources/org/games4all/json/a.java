package org.games4all.json;

import java.util.List;
import org.games4all.card.Cards;

public class a extends d {
    public a(h hVar) {
        super(hVar);
    }

    /* access modifiers changed from: protected */
    public Object a(List<?> list) {
        return ((Cards) list).toString();
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, List list) {
        if (!(obj instanceof String) || !(list instanceof Cards)) {
            super.a(obj, list);
        } else {
            list.addAll(new Cards((String) obj));
        }
    }
}
