package org.games4all.json;

import org.games4all.json.jsonorg.c;

public class g implements l {
    private final c a;

    public g(c cVar) {
        this.a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.games4all.json.jsonorg.c.a(java.lang.String, long):long
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.String):java.lang.String
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c */
    public void a(c cVar, String str, Class<?> cls, Object obj) {
        cVar.a(str, (Object) this.a.a(obj));
    }

    public Object a(c cVar, String str, Class<?> cls) {
        Object a2 = cVar.a(str);
        if (a2 == c.a) {
            return null;
        }
        return this.a.a((String) a2, cls);
    }
}
