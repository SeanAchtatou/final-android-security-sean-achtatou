package org.games4all.json;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import org.games4all.json.jsonorg.JSONException;
import org.games4all.json.jsonorg.c;

public class m implements b {
    private final h a;
    private final Map<Class<?>, List<Field>> b = new IdentityHashMap();

    public m(h hVar) {
        this.a = hVar;
    }

    public void a(Object obj, c cVar) {
        b(obj, cVar);
    }

    private List<Field> a(Class<?> cls) {
        List<Field> list = this.b.get(cls);
        if (list != null) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        a(cls, arrayList);
        this.b.put(cls, arrayList);
        return arrayList;
    }

    private void b(Object obj, c cVar) {
        for (Field next : a(obj.getClass())) {
            try {
                Object obj2 = next.get(obj);
                if (obj2 == null) {
                    cVar.a(next.getName(), c.a);
                } else {
                    this.a.c(next.getType()).a(cVar, next.getName(), next.getType(), obj2);
                }
            } catch (IllegalArgumentException e) {
                throw new JSONException(e);
            } catch (IllegalAccessException e2) {
                throw new JSONException(e2);
            }
        }
    }

    private void a(Class<?> cls, List<Field> list) {
        if (cls != null && cls != Object.class && !cls.isInterface()) {
            for (Field field : cls.getDeclaredFields()) {
                if (!field.isSynthetic()) {
                    int modifiers = field.getModifiers();
                    if ((modifiers & 128) == 0 && (modifiers & 8) == 0) {
                        field.setAccessible(true);
                        list.add(field);
                    }
                }
            }
            a(cls.getSuperclass(), list);
        }
    }

    public Object a(c cVar, Class<?> cls) {
        try {
            Object newInstance = cls.newInstance();
            for (Field next : a(cls)) {
                Class<?> type = next.getType();
                next.set(newInstance, this.a.c(type).a(cVar, next.getName(), type));
            }
            return newInstance;
        } catch (InstantiationException e) {
            throw new JSONException(e);
        } catch (IllegalAccessException e2) {
            throw new JSONException(e2);
        }
    }
}
