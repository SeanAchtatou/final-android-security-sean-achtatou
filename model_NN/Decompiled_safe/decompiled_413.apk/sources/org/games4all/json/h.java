package org.games4all.json;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.games4all.card.Card;
import org.games4all.card.Cards;
import org.games4all.json.a.a;
import org.games4all.json.a.b;
import org.games4all.json.a.c;
import org.games4all.json.a.d;
import org.games4all.json.a.e;
import org.games4all.json.a.f;
import org.games4all.json.a.g;
import org.games4all.json.a.i;
import org.games4all.json.a.j;
import org.games4all.json.a.k;
import org.games4all.json.jsonorg.JSONException;

public class h {
    public static String a = "{\"@c\":\"java.util.ArrayList\",\"list\":[]}";
    private final Map<Class<?>, b> b;
    private final Map<Class<?>, l> c;
    private final Map<Class<?>, c> d;
    private final l e;
    private final b f;
    private final String g;
    private final Set<String> h;
    private boolean i;
    private boolean j;

    public h() {
        this(null);
    }

    public h(String str) {
        this.g = str;
        this.h = new HashSet();
        this.b = new IdentityHashMap();
        this.c = new IdentityHashMap();
        this.d = new IdentityHashMap();
        this.e = new j(this);
        this.f = new e(this.e, "array");
        this.d.put(String.class, new f());
        this.d.put(Integer.class, new org.games4all.json.a.h());
        this.d.put(Integer.TYPE, new org.games4all.json.a.h());
        this.d.put(Long.class, new b());
        this.d.put(Long.TYPE, new b());
        this.d.put(Short.class, new i());
        this.d.put(Short.TYPE, new i());
        this.d.put(Byte.class, new a());
        this.d.put(Byte.TYPE, new a());
        this.d.put(Float.class, new g());
        this.d.put(Float.TYPE, new g());
        this.d.put(Double.class, new k());
        this.d.put(Double.TYPE, new k());
        this.d.put(Boolean.class, new c());
        this.d.put(Boolean.TYPE, new c());
        this.d.put(Card.class, new d());
        this.d.put(Date.class, new j());
        for (Map.Entry next : this.d.entrySet()) {
            Class cls = (Class) next.getKey();
            c cVar = (c) next.getValue();
            this.c.put(cls, new g(cVar));
            this.b.put(cls, new k(cVar));
        }
        d dVar = new d(this);
        a aVar = new a(this);
        f fVar = new f(this);
        e eVar = new e();
        this.c.put(Object.class, new i(this));
        this.c.put(List.class, dVar);
        this.c.put(ArrayList.class, dVar);
        this.c.put(LinkedList.class, dVar);
        this.c.put(Map.class, fVar);
        this.c.put(TreeMap.class, fVar);
        this.c.put(HashMap.class, fVar);
        this.c.put(Enum.class, eVar);
        this.c.put(Cards.class, aVar);
        e eVar2 = new e(dVar, "list");
        e eVar3 = new e(fVar, "map");
        this.b.put(Object.class, new m(this));
        this.b.put(List.class, eVar2);
        this.b.put(ArrayList.class, eVar2);
        this.b.put(LinkedList.class, eVar2);
        this.b.put(Map.class, eVar3);
        this.b.put(TreeMap.class, eVar3);
        this.b.put(HashMap.class, eVar3);
        this.b.put(Enum.class, new e(eVar, "enum"));
        this.b.put(Cards.class, new e(aVar, "cards"));
    }

    public void a(Class<?> cls) {
        this.h.add(cls.getName());
    }

    public void a(Object obj, Writer writer) {
        if (obj == null) {
            try {
                writer.write("null");
            } catch (JSONException e2) {
                e2.printStackTrace();
                throw new IOException("Exception generating JSON: " + e2.getMessage());
            }
        } else {
            a(obj, (Class<?>) null).a(writer);
        }
    }

    public String a(Object obj) {
        StringWriter stringWriter = new StringWriter();
        a(obj, stringWriter);
        return stringWriter.toString();
    }

    /* access modifiers changed from: package-private */
    public b b(Class<?> cls) {
        if (cls.isArray()) {
            return this.f;
        }
        b bVar = this.b.get(cls);
        if (bVar != null) {
            return bVar;
        }
        Class<? super Object> cls2 = cls;
        while (true) {
            b bVar2 = this.b.get(cls2);
            if (bVar2 != null) {
                return bVar2;
            }
            Class<? super Object> superclass = cls2.getSuperclass();
            if (superclass == null) {
                throw new JSONException("Cannot find object serializer for " + cls);
            }
            cls2 = superclass;
        }
    }

    /* access modifiers changed from: package-private */
    public l c(Class<?> cls) {
        if (cls.isArray()) {
            return this.e;
        }
        l lVar = this.c.get(cls);
        if (lVar != null) {
            return lVar;
        }
        Class<? super Object> cls2 = cls;
        while (true) {
            l lVar2 = this.c.get(cls2);
            if (lVar2 != null) {
                return lVar2;
            }
            Class<? super Object> superclass = cls2.getSuperclass();
            if (superclass == null) {
                l lVar3 = this.c.get(Object.class);
                if (lVar3 != null) {
                    return lVar3;
                }
                throw new JSONException("Cannot find field serializer for " + cls);
            }
            cls2 = superclass;
        }
    }

    /* access modifiers changed from: package-private */
    public c d(Class<?> cls) {
        return this.d.get(cls);
    }

    public String e(Class<?> cls) {
        String name = cls.getName();
        if (this.g == null) {
            return name;
        }
        if (name.startsWith(this.g)) {
            return name.substring(this.g.length());
        }
        if (!this.i || this.h.contains(name)) {
            return name;
        }
        throw new RuntimeException("Cannot serialize outside package: " + name);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.games4all.json.jsonorg.c.a(java.lang.String, long):long
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.String):java.lang.String
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c */
    public org.games4all.json.jsonorg.c a(Object obj, Class<?> cls) {
        Class<?> cls2 = obj.getClass();
        org.games4all.json.jsonorg.c cVar = new org.games4all.json.jsonorg.c();
        long a2 = org.games4all.util.h.a(cls2);
        if (a2 != 0) {
            cVar.a("@v", (Object) String.valueOf(a2));
        }
        cVar.a("@c", (Object) e(cls2));
        b(cls2).a(obj, cVar);
        return cVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Class<?> cls, Class<?> cls2) {
        if (cls == cls2) {
            return true;
        }
        if (cls2.isPrimitive()) {
            if (cls == Integer.class && cls2 == Integer.TYPE) {
                return true;
            }
            if (cls == Short.class && cls2 == Short.TYPE) {
                return true;
            }
            if (cls == Long.class && cls2 == Long.TYPE) {
                return true;
            }
            if (cls == Boolean.class && cls2 == Boolean.TYPE) {
                return true;
            }
            if (cls == Character.class && cls2 == Character.TYPE) {
                return true;
            }
            if (cls == Byte.class && cls2 == Byte.TYPE) {
                return true;
            }
            if (cls == Float.class && cls2 == Float.TYPE) {
                return true;
            }
            if (cls == Double.class && cls2 == Double.TYPE) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003a, code lost:
        throw new java.io.IOException("Exception parsing JSON: " + r1.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0059, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a A[ExcHandler: JSONException (r1v3 'e' org.games4all.json.jsonorg.JSONException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(java.io.Reader r6) {
        /*
            r5 = this;
            r3 = 0
            org.games4all.json.jsonorg.d r1 = new org.games4all.json.jsonorg.d     // Catch:{ JSONException -> 0x001a, ClassCastException -> 0x003b }
            r1.<init>(r6)     // Catch:{ JSONException -> 0x001a, ClassCastException -> 0x003b }
            java.lang.Object r2 = r1.e()     // Catch:{ JSONException -> 0x001a, ClassCastException -> 0x003b }
            java.lang.Object r1 = org.games4all.json.jsonorg.c.a     // Catch:{ JSONException -> 0x001a, ClassCastException -> 0x0059 }
            if (r2 != r1) goto L_0x0010
            r1 = r3
        L_0x000f:
            return r1
        L_0x0010:
            r0 = r2
            org.games4all.json.jsonorg.c r0 = (org.games4all.json.jsonorg.c) r0     // Catch:{ JSONException -> 0x001a, ClassCastException -> 0x0059 }
            r1 = r0
            r3 = 0
            java.lang.Object r1 = r5.a(r1, r3)     // Catch:{ JSONException -> 0x001a, ClassCastException -> 0x0059 }
            goto L_0x000f
        L_0x001a:
            r1 = move-exception
            r1.printStackTrace()
            java.io.IOException r2 = new java.io.IOException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Exception parsing JSON: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r1 = r1.getMessage()
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x003b:
            r1 = move-exception
            r2 = r3
        L_0x003d:
            r1.printStackTrace()
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Not a JSONObject: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0059:
            r1 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.json.h.a(java.io.Reader):java.lang.Object");
    }

    public Object a(String str) {
        return a((Reader) new StringReader(str));
    }

    public Object a(org.games4all.json.jsonorg.c cVar, Class<?> cls) {
        Class<?> cls2;
        long j2;
        if (cVar.f("@c")) {
            try {
                cls2 = b(cVar.e("@c"));
            } catch (ClassNotFoundException e2) {
                throw new JSONException(e2);
            }
        } else if (cls == null) {
            throw new JSONException("No type indication and no type in JSON representation");
        } else {
            cls2 = cls;
        }
        long a2 = org.games4all.util.h.a(cls2);
        if (cVar.f("@v")) {
            j2 = Long.parseLong(cVar.e("@v"));
        } else {
            j2 = 0;
        }
        if (j2 == a2 || a(cVar, j2, a2)) {
            return b(cls2).a(cVar, cls2);
        }
        throw new JSONException("Cannot convert " + cls2.getName() + " from version " + j2 + " to " + a2);
    }

    private boolean a(org.games4all.json.jsonorg.c cVar, long j2, long j3) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public Class<?> b(String str) {
        String str2;
        if (str.equals("int")) {
            return Integer.TYPE;
        }
        if (str.equals("byte")) {
            return Byte.TYPE;
        }
        if (str.equals("short")) {
            return Short.TYPE;
        }
        if (str.equals("long")) {
            return Long.TYPE;
        }
        if (str.equals("char")) {
            return Character.TYPE;
        }
        if (str.equals("float")) {
            return Float.TYPE;
        }
        if (str.equals("double")) {
            return Double.TYPE;
        }
        if (str.equals("boolean")) {
            return Boolean.TYPE;
        }
        if (str.equals("void")) {
            return Void.TYPE;
        }
        if (str.startsWith(".")) {
            str2 = this.g + str;
        } else if (!this.j || this.h.contains(str)) {
            str2 = str;
        } else {
            throw new RuntimeException("Cannot deserialize outside package: " + str);
        }
        return Class.forName(str2);
    }

    public void a(boolean z) {
        this.i = z;
    }

    public void b(boolean z) {
        this.j = z;
    }
}
