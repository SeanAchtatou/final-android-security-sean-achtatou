package org.games4all.json.jsonorg;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class c {
    public static final Object a = new a();
    private final Map b;

    static final class a {
        a() {
        }

        /* access modifiers changed from: protected */
        public final Object clone() {
            return this;
        }

        public boolean equals(Object obj) {
            return obj == null || obj == this;
        }

        public String toString() {
            return "null";
        }

        public int hashCode() {
            return 0;
        }
    }

    public c() {
        this.b = new HashMap();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x004f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001c  */
    public c(org.games4all.json.jsonorg.d r4) {
        /*
            r3 = this;
            r3.<init>()
            char r0 = r4.d()
            r1 = 123(0x7b, float:1.72E-43)
            if (r0 == r1) goto L_0x0015
            java.lang.String r0 = "A JSONObject text must begin with '{'"
            org.games4all.json.jsonorg.JSONException r0 = r4.a(r0)
            throw r0
        L_0x0012:
            r4.a()
        L_0x0015:
            char r0 = r4.d()
            switch(r0) {
                case 0: goto L_0x004f;
                case 125: goto L_0x0069;
                default: goto L_0x001c;
            }
        L_0x001c:
            r4.a()
            java.lang.Object r0 = r4.e()
            java.lang.String r0 = r0.toString()
            char r1 = r4.d()
            r2 = 61
            if (r1 != r2) goto L_0x0056
            char r1 = r4.c()
            r2 = 62
            if (r1 == r2) goto L_0x003a
            r4.a()
        L_0x003a:
            java.lang.Object r1 = r4.e()
            r3.b(r0, r1)
            char r0 = r4.d()
            switch(r0) {
                case 44: goto L_0x0061;
                case 59: goto L_0x0061;
                case 125: goto L_0x0069;
                default: goto L_0x0048;
            }
        L_0x0048:
            java.lang.String r0 = "Expected a ',' or '}'"
            org.games4all.json.jsonorg.JSONException r0 = r4.a(r0)
            throw r0
        L_0x004f:
            java.lang.String r0 = "A JSONObject text must end with '}'"
            org.games4all.json.jsonorg.JSONException r0 = r4.a(r0)
            throw r0
        L_0x0056:
            r2 = 58
            if (r1 == r2) goto L_0x003a
            java.lang.String r0 = "Expected a ':' after a key"
            org.games4all.json.jsonorg.JSONException r0 = r4.a(r0)
            throw r0
        L_0x0061:
            char r0 = r4.d()
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x0012
        L_0x0069:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.json.jsonorg.c.<init>(org.games4all.json.jsonorg.d):void");
    }

    public c(Map map) {
        this.b = new HashMap();
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                this.b.put(entry.getKey(), c(entry.getValue()));
            }
        }
    }

    public c(Object obj) {
        this();
        d(obj);
    }

    public c(String str) {
        this(new d(str));
    }

    public Object a(String str) {
        Object g = g(str);
        if (g != null) {
            return g;
        }
        throw new JSONException("JSONObject[" + j(str) + "] not found.");
    }

    public int b(String str) {
        Object a2 = a(str);
        try {
            return a2 instanceof Number ? ((Number) a2).intValue() : Integer.parseInt((String) a2);
        } catch (Exception e) {
            throw new JSONException("JSONObject[" + j(str) + "] is not an int.");
        }
    }

    public c c(String str) {
        Object a2 = a(str);
        if (a2 instanceof c) {
            return (c) a2;
        }
        throw new JSONException("JSONObject[" + j(str) + "] is not a JSONObject.");
    }

    public long d(String str) {
        Object a2 = a(str);
        try {
            return a2 instanceof Number ? ((Number) a2).longValue() : Long.parseLong((String) a2);
        } catch (Exception e) {
            throw new JSONException("JSONObject[" + j(str) + "] is not a long.");
        }
    }

    public String e(String str) {
        return a(str).toString();
    }

    public boolean f(String str) {
        return this.b.containsKey(str);
    }

    public Iterator a() {
        return this.b.keySet().iterator();
    }

    public static String a(Number number) {
        if (number == null) {
            throw new JSONException("Null pointer");
        }
        a((Object) number);
        String obj = number.toString();
        if (obj.indexOf(46) <= 0 || obj.indexOf(101) >= 0 || obj.indexOf(69) >= 0) {
            return obj;
        }
        while (obj.endsWith("0")) {
            obj = obj.substring(0, obj.length() - 1);
        }
        if (obj.endsWith(".")) {
            return obj.substring(0, obj.length() - 1);
        }
        return obj;
    }

    public Object g(String str) {
        if (str == null) {
            return null;
        }
        return this.b.get(str);
    }

    public b h(String str) {
        Object g = g(str);
        if (g instanceof b) {
            return (b) g;
        }
        return null;
    }

    public long i(String str) {
        return a(str, 0);
    }

    public long a(String str, long j) {
        try {
            return d(str);
        } catch (Exception e) {
            return j;
        }
    }

    public String a(String str, String str2) {
        Object g = g(str);
        return g != null ? g.toString() : str2;
    }

    private void d(Object obj) {
        boolean z;
        String str;
        String str2;
        Class<?> cls = obj.getClass();
        if (cls.getClassLoader() != null) {
            z = true;
        } else {
            z = false;
        }
        Method[] methods = z ? cls.getMethods() : cls.getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            try {
                Method method = methods[i];
                if (Modifier.isPublic(method.getModifiers())) {
                    String name = method.getName();
                    if (name.startsWith("get")) {
                        if (name.equals("getClass") || name.equals("getDeclaringClass")) {
                            str = "";
                        } else {
                            str = name.substring(3);
                        }
                    } else if (name.startsWith("is")) {
                        str = name.substring(2);
                    } else {
                        str = "";
                    }
                    if (str.length() > 0 && Character.isUpperCase(str.charAt(0)) && method.getParameterTypes().length == 0) {
                        if (str.length() == 1) {
                            str2 = str.toLowerCase();
                        } else if (!Character.isUpperCase(str.charAt(1))) {
                            str2 = str.substring(0, 1).toLowerCase() + str.substring(1);
                        } else {
                            str2 = str;
                        }
                        this.b.put(str2, c(method.invoke(obj, null)));
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public c b(String str, long j) {
        a(str, new Long(j));
        return this;
    }

    public c a(String str, Object obj) {
        if (str == null) {
            throw new JSONException("Null key.");
        }
        if (obj != null) {
            a(obj);
            this.b.put(str, obj);
        } else {
            k(str);
        }
        return this;
    }

    public c b(String str, Object obj) {
        if (!(str == null || obj == null)) {
            if (g(str) != null) {
                throw new JSONException("Duplicate key \"" + str + "\"");
            }
            a(str, obj);
        }
        return this;
    }

    public static String j(String str) {
        char c = 0;
        if (str == null || str.length() == 0) {
            return "\"\"";
        }
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length + 4);
        stringBuffer.append('\"');
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                case '\\':
                    stringBuffer.append('\\');
                    stringBuffer.append(charAt);
                    break;
                case '/':
                    if (c == '<') {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt);
                    break;
                default:
                    if (charAt >= ' ' && ((charAt < 128 || charAt >= 160) && (charAt < 8192 || charAt >= 8448))) {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "000" + Integer.toHexString(charAt);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4));
                        break;
                    }
            }
            i++;
            c = charAt;
        }
        stringBuffer.append('\"');
        return stringBuffer.toString();
    }

    public Object k(String str) {
        return this.b.remove(str);
    }

    public static Object l(String str) {
        if (str.equals("")) {
            return str;
        }
        if (str.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        }
        if (str.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }
        if (str.equalsIgnoreCase("null")) {
            return a;
        }
        char charAt = str.charAt(0);
        if ((charAt >= '0' && charAt <= '9') || charAt == '.' || charAt == '-' || charAt == '+') {
            if (charAt == '0' && str.length() > 2 && (str.charAt(1) == 'x' || str.charAt(1) == 'X')) {
                try {
                    return new Integer(Integer.parseInt(str.substring(2), 16));
                } catch (Exception e) {
                }
            }
            try {
                if (str.indexOf(46) > -1 || str.indexOf(101) > -1 || str.indexOf(69) > -1) {
                    return Double.valueOf(str);
                }
                Long l = new Long(str);
                if (l.longValue() == ((long) l.intValue())) {
                    return new Integer(l.intValue());
                }
                return l;
            } catch (Exception e2) {
            }
        }
        return str;
    }

    static void a(Object obj) {
        if (obj == null) {
            return;
        }
        if (obj instanceof Double) {
            if (((Double) obj).isInfinite() || ((Double) obj).isNaN()) {
                throw new JSONException("JSON does not allow non-finite numbers.");
            }
        } else if (!(obj instanceof Float)) {
        } else {
            if (((Float) obj).isInfinite() || ((Float) obj).isNaN()) {
                throw new JSONException("JSON does not allow non-finite numbers.");
            }
        }
    }

    public String toString() {
        try {
            Iterator a2 = a();
            StringBuffer stringBuffer = new StringBuffer("{");
            while (a2.hasNext()) {
                if (stringBuffer.length() > 1) {
                    stringBuffer.append(',');
                }
                Object next = a2.next();
                stringBuffer.append(j(next.toString()));
                stringBuffer.append(':');
                stringBuffer.append(b(this.b.get(next)));
            }
            stringBuffer.append('}');
            return stringBuffer.toString();
        } catch (Exception e) {
            return null;
        }
    }

    static String b(Object obj) {
        if (obj == null || obj.equals(null)) {
            return "null";
        }
        if (obj instanceof a) {
            try {
                String a2 = ((a) obj).a();
                if (a2 instanceof String) {
                    return a2;
                }
                throw new JSONException("Bad value from toJSONString: " + ((Object) a2));
            } catch (Exception e) {
                throw new JSONException(e);
            }
        } else if (obj instanceof Number) {
            return a((Number) obj);
        } else {
            if ((obj instanceof Boolean) || (obj instanceof c) || (obj instanceof b)) {
                return obj.toString();
            }
            if (obj instanceof Map) {
                return new c((Map) obj).toString();
            }
            if (obj instanceof Collection) {
                return new b((Collection) obj).toString();
            }
            if (obj.getClass().isArray()) {
                return new b(obj).toString();
            }
            return j(obj.toString());
        }
    }

    static Object c(Object obj) {
        if (obj == null) {
            try {
                return a;
            } catch (Exception e) {
                return null;
            }
        } else if ((obj instanceof c) || (obj instanceof b) || a.equals(obj) || (obj instanceof a) || (obj instanceof Byte) || (obj instanceof Character) || (obj instanceof Short) || (obj instanceof Integer) || (obj instanceof Long) || (obj instanceof Boolean) || (obj instanceof Float) || (obj instanceof Double) || (obj instanceof String)) {
            return obj;
        } else {
            if (obj instanceof Collection) {
                return new b((Collection) obj);
            }
            if (obj.getClass().isArray()) {
                return new b(obj);
            }
            if (obj instanceof Map) {
                return new c((Map) obj);
            }
            Package packageR = obj.getClass().getPackage();
            String name = packageR != null ? packageR.getName() : "";
            if (name.startsWith("java.") || name.startsWith("javax.") || obj.getClass().getClassLoader() == null) {
                return obj.toString();
            }
            return new c(obj);
        }
    }

    public Writer a(Writer writer) {
        boolean z = false;
        try {
            Iterator a2 = a();
            writer.write(123);
            while (a2.hasNext()) {
                if (z) {
                    writer.write(44);
                }
                Object next = a2.next();
                String obj = next.toString();
                if (m(obj)) {
                    writer.write(obj);
                } else {
                    writer.write(j(obj));
                }
                writer.write(58);
                Object obj2 = this.b.get(next);
                if (obj2 instanceof c) {
                    ((c) obj2).a(writer);
                } else if (obj2 instanceof b) {
                    ((b) obj2).a(writer);
                } else {
                    writer.write(b(obj2));
                }
                z = true;
            }
            writer.write(125);
            return writer;
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }

    public static boolean m(String str) {
        int length = str.length();
        if (length == 0) {
            return false;
        }
        if (!Character.isJavaIdentifierStart(str.charAt(0))) {
            return false;
        }
        for (int i = 1; i < length; i++) {
            if (!Character.isJavaIdentifierPart(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
