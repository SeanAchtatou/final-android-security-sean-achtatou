package org.games4all.json.jsonorg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class d {
    private int a;
    private boolean b;
    private int c;
    private int d;
    private char e;
    private final Reader f;
    private boolean g;

    public d(Reader reader) {
        this.f = reader.markSupported() ? reader : new BufferedReader(reader);
        this.b = false;
        this.g = false;
        this.e = 0;
        this.c = 0;
        this.a = 1;
        this.d = 1;
    }

    public d(String str) {
        this(new StringReader(str));
    }

    public void a() {
        if (this.g || this.c <= 0) {
            throw new JSONException("Stepping back two steps is not supported");
        }
        this.c--;
        this.a--;
        this.g = true;
        this.b = false;
    }

    public boolean b() {
        return this.b && !this.g;
    }

    public char c() {
        int read;
        if (this.g) {
            this.g = false;
            read = this.e;
        } else {
            try {
                read = this.f.read();
                if (read <= 0) {
                    this.b = true;
                    read = 0;
                }
            } catch (IOException e2) {
                throw new JSONException(e2);
            }
        }
        this.c++;
        if (this.e == 13) {
            this.d++;
            this.a = read == 10 ? 0 : 1;
        } else if (read == 10) {
            this.d++;
            this.a = 0;
        } else {
            this.a++;
        }
        this.e = (char) read;
        return this.e;
    }

    public String a(int i) {
        if (i == 0) {
            return "";
        }
        char[] cArr = new char[i];
        for (int i2 = 0; i2 < i; i2++) {
            cArr[i2] = c();
            if (b()) {
                throw a("Substring bounds error");
            }
        }
        return new String(cArr);
    }

    public char d() {
        char c2;
        do {
            c2 = c();
            if (c2 == 0) {
                break;
            }
        } while (c2 <= ' ');
        return c2;
    }

    public String a(char c2) {
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            char c3 = c();
            switch (c3) {
                case 0:
                case 10:
                case 13:
                    throw a("Unterminated string");
                case '\\':
                    char c4 = c();
                    switch (c4) {
                        case '\"':
                        case '\'':
                        case '/':
                        case '\\':
                            stringBuffer.append(c4);
                            continue;
                        case 'b':
                            stringBuffer.append(8);
                            continue;
                        case 'f':
                            stringBuffer.append(12);
                            continue;
                        case 'n':
                            stringBuffer.append(10);
                            continue;
                        case 'r':
                            stringBuffer.append(13);
                            continue;
                        case 't':
                            stringBuffer.append(9);
                            continue;
                        case 'u':
                            stringBuffer.append((char) Integer.parseInt(a(4), 16));
                            continue;
                        default:
                            throw a("Illegal escape.");
                    }
                default:
                    if (c3 != c2) {
                        stringBuffer.append(c3);
                        break;
                    } else {
                        return stringBuffer.toString();
                    }
            }
        }
    }

    public Object e() {
        char d2 = d();
        switch (d2) {
            case '\"':
            case '\'':
                return a(d2);
            case '(':
            case '[':
                a();
                return new b(this);
            case '{':
                a();
                return new c(this);
            default:
                StringBuffer stringBuffer = new StringBuffer();
                while (d2 >= ' ' && ",:]}/\\\"[{;=#".indexOf(d2) < 0) {
                    stringBuffer.append(d2);
                    d2 = c();
                }
                a();
                String trim = stringBuffer.toString().trim();
                if (!trim.equals("")) {
                    return c.l(trim);
                }
                throw a("Missing value");
        }
    }

    public JSONException a(String str) {
        return new JSONException(str + toString());
    }

    public String toString() {
        return " at " + this.c + " [character " + this.a + " line " + this.d + "]";
    }
}
