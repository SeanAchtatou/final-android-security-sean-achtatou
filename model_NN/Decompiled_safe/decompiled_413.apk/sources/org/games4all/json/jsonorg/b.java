package org.games4all.json.jsonorg;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;

public class b {
    private final ArrayList a;

    public b() {
        this.a = new ArrayList();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005c A[SYNTHETIC] */
    public b(org.games4all.json.jsonorg.d r5) {
        /*
            r4 = this;
            r3 = 93
            r4.<init>()
            char r0 = r5.d()
            r1 = 91
            if (r0 != r1) goto L_0x0015
            r0 = r3
        L_0x000e:
            char r1 = r5.d()
            if (r1 != r3) goto L_0x0023
        L_0x0014:
            return
        L_0x0015:
            r1 = 40
            if (r0 != r1) goto L_0x001c
            r0 = 41
            goto L_0x000e
        L_0x001c:
            java.lang.String r0 = "A JSONArray text must start with '['"
            org.games4all.json.jsonorg.JSONException r0 = r5.a(r0)
            throw r0
        L_0x0023:
            r5.a()
        L_0x0026:
            char r1 = r5.d()
            r2 = 44
            if (r1 != r2) goto L_0x0045
            r5.a()
            java.util.ArrayList r1 = r4.a
            r2 = 0
            r1.add(r2)
        L_0x0037:
            char r1 = r5.d()
            switch(r1) {
                case 41: goto L_0x005c;
                case 44: goto L_0x0052;
                case 59: goto L_0x0052;
                case 93: goto L_0x005c;
                default: goto L_0x003e;
            }
        L_0x003e:
            java.lang.String r0 = "Expected a ',' or ']'"
            org.games4all.json.jsonorg.JSONException r0 = r5.a(r0)
            throw r0
        L_0x0045:
            r5.a()
            java.util.ArrayList r1 = r4.a
            java.lang.Object r2 = r5.e()
            r1.add(r2)
            goto L_0x0037
        L_0x0052:
            char r1 = r5.d()
            if (r1 == r3) goto L_0x0014
            r5.a()
            goto L_0x0026
        L_0x005c:
            if (r0 == r1) goto L_0x0014
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Expected a '"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.Character r2 = new java.lang.Character
            r2.<init>(r0)
            java.lang.StringBuilder r0 = r1.append(r2)
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            org.games4all.json.jsonorg.JSONException r0 = r5.a(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.games4all.json.jsonorg.b.<init>(org.games4all.json.jsonorg.d):void");
    }

    public b(Collection collection) {
        this.a = new ArrayList();
        if (collection != null) {
            for (Object c : collection) {
                this.a.add(c.c(c));
            }
        }
    }

    public b(Object obj) {
        this();
        if (obj.getClass().isArray()) {
            int length = Array.getLength(obj);
            for (int i = 0; i < length; i++) {
                a(c.c(Array.get(obj, i)));
            }
            return;
        }
        throw new JSONException("JSONArray initial value should be a string or collection or array.");
    }

    public Object a(int i) {
        Object c = c(i);
        if (c != null) {
            return c;
        }
        throw new JSONException("JSONArray[" + i + "] not found.");
    }

    public c b(int i) {
        Object a2 = a(i);
        if (a2 instanceof c) {
            return (c) a2;
        }
        throw new JSONException("JSONArray[" + i + "] is not a JSONObject.");
    }

    public String a(String str) {
        int a2 = a();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < a2; i++) {
            if (i > 0) {
                stringBuffer.append(str);
            }
            stringBuffer.append(c.b(this.a.get(i)));
        }
        return stringBuffer.toString();
    }

    public int a() {
        return this.a.size();
    }

    public Object c(int i) {
        if (i < 0 || i >= a()) {
            return null;
        }
        return this.a.get(i);
    }

    public b a(Object obj) {
        this.a.add(obj);
        return this;
    }

    public String toString() {
        try {
            return '[' + a(",") + ']';
        } catch (Exception e) {
            return null;
        }
    }

    public Writer a(Writer writer) {
        try {
            int a2 = a();
            writer.write(91);
            boolean z = false;
            for (int i = 0; i < a2; i++) {
                if (z) {
                    writer.write(44);
                }
                Object obj = this.a.get(i);
                if (obj instanceof c) {
                    ((c) obj).a(writer);
                } else if (obj instanceof b) {
                    ((b) obj).a(writer);
                } else {
                    writer.write(c.b(obj));
                }
                z = true;
            }
            writer.write(93);
            return writer;
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }
}
