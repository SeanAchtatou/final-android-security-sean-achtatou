package org.games4all.json;

import org.games4all.json.jsonorg.c;

public class i implements l {
    private final h a;

    public i(h hVar) {
        this.a = hVar;
    }

    public void a(c cVar, String str, Class<?> cls, Object obj) {
        cVar.a(str, this.a.a(obj, cls));
    }

    public Object a(c cVar, String str, Class<?> cls) {
        Object a2 = cVar.a(str);
        if (a2 == c.a) {
            return null;
        }
        return this.a.a((c) a2, cls);
    }
}
