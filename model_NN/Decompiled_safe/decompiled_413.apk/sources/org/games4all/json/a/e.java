package org.games4all.json.a;

import org.games4all.json.jsonorg.JSONException;
import org.games4all.json.jsonorg.c;
import org.games4all.json.l;

public class e implements l {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.games4all.json.jsonorg.c.a(java.lang.String, long):long
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.String):java.lang.String
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c */
    public void a(c cVar, String str, Class<?> cls, Object obj) {
        if (cls == Enum.class) {
            cVar.a("@c-" + str, (Object) obj.getClass().getName());
        }
        cVar.a(str, (Object) ((Enum) obj).name());
    }

    public Object a(c cVar, String str, Class<?> cls) {
        Class<?> cls2;
        String e;
        Object a = cVar.a(str);
        if (a == c.a) {
            return null;
        }
        if (!cVar.f("@c-" + str) || (e = cVar.e("@c-" + str)) == null) {
            cls2 = cls;
        } else {
            try {
                cls2 = Class.forName(e);
            } catch (ClassNotFoundException e2) {
                throw new JSONException(e2);
            }
        }
        return Enum.valueOf(cls2, (String) a);
    }
}
