package org.games4all.json.a;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.games4all.json.c;
import org.games4all.json.jsonorg.JSONException;

public class j implements c {
    private final DateFormat a = SimpleDateFormat.getDateTimeInstance(2, 2, Locale.US);

    public String a(Object obj) {
        return this.a.format((Date) obj);
    }

    public Object a(String str, Class<?> cls) {
        try {
            return this.a.parse(str);
        } catch (ParseException e) {
            throw new JSONException(e);
        }
    }
}
