package org.games4all.json;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.games4all.json.jsonorg.JSONException;
import org.games4all.json.jsonorg.c;

public class f implements l {
    private final h a;

    public f(h hVar) {
        this.a = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.games4all.json.jsonorg.c.a(java.lang.String, long):long
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.String):java.lang.String
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c */
    public void a(c cVar, String str, Class<?> cls, Object obj) {
        Class<?> cls2 = obj.getClass();
        c cVar2 = new c();
        if (cls2 != HashMap.class) {
            cVar2.a("@c", (Object) cls2.getName());
        }
        int i = 0;
        Iterator it = ((Map) obj).entrySet().iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                c a2 = this.a.a(entry.getValue(), (Class<?>) null);
                Class<?> cls3 = key.getClass();
                c d = this.a.d(cls3);
                if (d == null) {
                    a2.a("@key", this.a.a(key, (Class<?>) null));
                    cVar2.a(String.valueOf(i2), a2);
                } else {
                    String a3 = d.a(key);
                    a2.a("@k", (Object) cls3.getName());
                    cVar2.a(a3, a2);
                }
                i = i2 + 1;
            } else {
                cVar.a(str, cVar2);
                return;
            }
        }
    }

    public Object a(c cVar, String str, Class<?> cls) {
        Class<?> cls2;
        Object a2 = cVar.a(str);
        if (a2 == c.a) {
            return null;
        }
        c cVar2 = (c) a2;
        try {
            if (cVar2.f("@c")) {
                cls2 = this.a.b(cVar.e("@c"));
            } else if ((cls.getModifiers() & 1024) == 0) {
                cls2 = cls;
            } else {
                cls2 = HashMap.class;
            }
            Map map = (Map) cls2.newInstance();
            Iterator a3 = cVar2.a();
            while (a3.hasNext()) {
                String str2 = (String) a3.next();
                if (!str2.equals("@c")) {
                    c cVar3 = (c) cVar2.a(str2);
                    Object a4 = this.a.a(cVar3, (Class<?>) null);
                    if (Character.isDigit(str2.charAt(0))) {
                        map.put(this.a.a(cVar3.c("@key"), (Class<?>) null), a4);
                    } else {
                        Class<?> b = this.a.b(cVar3.e("@k"));
                        map.put(this.a.d(b).a(str2, b), a4);
                    }
                }
            }
            return map;
        } catch (ClassNotFoundException e) {
            throw new JSONException(e);
        } catch (InstantiationException e2) {
            throw new JSONException(e2);
        } catch (IllegalAccessException e3) {
            throw new JSONException(e3);
        }
    }
}
