package org.games4all.json;

import java.lang.reflect.Array;
import org.games4all.json.jsonorg.JSONException;
import org.games4all.json.jsonorg.b;
import org.games4all.json.jsonorg.c;

public class j implements l {
    private final h a;

    public j(h hVar) {
        this.a = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.games4all.json.jsonorg.c.a(java.lang.String, long):long
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.String):java.lang.String
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c */
    public void a(c cVar, String str, Class<?> cls, Object obj) {
        Class<?> componentType = obj.getClass().getComponentType();
        if (!cls.isArray() || cls.getComponentType() != componentType) {
            cVar.a(str + "@c", (Object) componentType.getName());
        }
        cVar.a(str, a(obj, cls));
    }

    private b a(Object obj, Class<?> cls) {
        Class<?> componentType = obj.getClass().getComponentType();
        b bVar = new b();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            Object obj2 = Array.get(obj, i);
            if (obj2 == null) {
                bVar.a(c.a);
            } else if (obj2.getClass().isArray()) {
                bVar.a(a(obj2, componentType));
            } else {
                c d = this.a.d(componentType);
                if (d == null || !this.a.a(obj2.getClass(), componentType)) {
                    bVar.a(this.a.a(obj2, componentType));
                } else {
                    bVar.a((Object) d.a(obj2));
                }
            }
        }
        return bVar;
    }

    public Object a(c cVar, String str, Class<?> cls) {
        Class<?> cls2;
        try {
            String str2 = str + "@c";
            if (cVar.f(str2)) {
                cls2 = this.a.b(cVar.e(str2));
            } else if (cls.isArray()) {
                cls2 = cls.getComponentType();
            } else {
                cls2 = Object.class;
            }
            Object a2 = cVar.a(str);
            if (a2 == c.a) {
                return null;
            }
            return a((b) a2, cls2);
        } catch (ClassNotFoundException e) {
            throw new JSONException(e);
        }
    }

    private Object a(b bVar, Class<?> cls) {
        Object a2;
        int a3 = bVar.a();
        Object newInstance = Array.newInstance(cls, a3);
        for (int i = 0; i < a3; i++) {
            Object a4 = bVar.a(i);
            if (a4 == c.a) {
                a2 = null;
            } else if (a4 instanceof String) {
                a2 = this.a.d(cls).a((String) a4, cls);
            } else if (a4 instanceof b) {
                a2 = a((b) a4, cls.getComponentType());
            } else {
                a2 = this.a.a((c) a4, cls);
            }
            Array.set(newInstance, i, a2);
        }
        return newInstance;
    }
}
