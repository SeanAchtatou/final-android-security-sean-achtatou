package org.games4all.json;

import java.util.ArrayList;
import java.util.List;
import org.games4all.json.jsonorg.JSONException;
import org.games4all.json.jsonorg.b;
import org.games4all.json.jsonorg.c;

public class d implements l {
    private final h a;

    public d(h hVar) {
        this.a = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.games4all.json.jsonorg.c.a(java.lang.String, long):long
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.String):java.lang.String
      org.games4all.json.jsonorg.c.a(java.lang.String, java.lang.Object):org.games4all.json.jsonorg.c */
    public void a(c cVar, String str, Class<?> cls, Object obj) {
        Class<?> cls2 = obj.getClass();
        if (cls2 != ArrayList.class) {
            cVar.a(str + "-" + "class", (Object) cls2.getName());
        }
        cVar.a(str, a((List) obj));
    }

    /* access modifiers changed from: protected */
    public Object a(List<?> list) {
        b bVar = new b();
        for (Object next : list) {
            if (next == null) {
                bVar.a(c.a);
            } else {
                bVar.a(this.a.a(next, (Class<?>) null));
            }
        }
        return bVar;
    }

    public Object a(c cVar, String str, Class<?> cls) {
        Class<?> cls2;
        try {
            Object a2 = cVar.a(str);
            if (a2 == c.a) {
                return null;
            }
            String str2 = str + "-" + "class";
            if (cVar.f(str2)) {
                cls2 = this.a.b(cVar.e(str2));
            } else if ((cls.getModifiers() & 1024) == 0) {
                cls2 = cls;
            } else {
                cls2 = ArrayList.class;
            }
            List list = (List) cls2.newInstance();
            a(a2, list);
            return list;
        } catch (ClassNotFoundException e) {
            throw new JSONException(e);
        } catch (InstantiationException e2) {
            throw new JSONException(e2);
        } catch (IllegalAccessException e3) {
            throw new JSONException(e3);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, List list) {
        b bVar = (b) obj;
        int a2 = bVar.a();
        for (int i = 0; i < a2; i++) {
            Object a3 = bVar.a(i);
            if (a3 == c.a) {
                list.add(null);
            } else {
                list.add(this.a.a((c) a3, (Class<?>) null));
            }
        }
    }
}
