package org.apache.james.mime4j.field;

import java.util.regex.Pattern;
import org.apache.james.mime4j.util.f;

public abstract class g implements d {
    private static final Pattern a = Pattern.compile("^([\\x21-\\x39\\x3b-\\x7e]+):");
    private static final b b = new b();
    private final String c;
    private final String d;
    private final f e;

    protected g(String str, String str2, f fVar) {
        this.c = str;
        this.d = str2;
        this.e = fVar;
    }

    public String a() {
        return this.c;
    }

    public f c() {
        return this.e;
    }

    public String b() {
        return this.d;
    }

    public String toString() {
        return this.c + ": " + this.d;
    }
}
