package org.apache.james.mime4j.field;

import org.apache.james.mime4j.util.f;

public class s extends g {
    static final r a = new i();
    private String b;

    s(String str, String str2, f fVar) {
        super(str, str2, fVar);
        this.b = str2.trim().toLowerCase();
    }

    public String d() {
        return this.b;
    }

    public static String a(s sVar) {
        if (sVar == null || sVar.d().length() == 0) {
            return "7bit";
        }
        return sVar.d();
    }
}
