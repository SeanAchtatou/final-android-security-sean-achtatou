package org.apache.james.mime4j.field;

import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.util.f;

public class e implements r {
    private Map<String, r> a = new HashMap();
    private r b = l.a;

    public void a(String str, r rVar) {
        this.a.put(str.toLowerCase(), rVar);
    }

    public r a(String str) {
        r rVar = this.a.get(str.toLowerCase());
        if (rVar == null) {
            return this.b;
        }
        return rVar;
    }

    public d a(String str, String str2, f fVar) {
        return a(str).a(str, str2, fVar);
    }
}
