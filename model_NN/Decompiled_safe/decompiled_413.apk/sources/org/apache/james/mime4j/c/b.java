package org.apache.james.mime4j.c;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.james.mime4j.b.a;

public class b implements Iterable<a> {
    private List<a> a = new LinkedList();
    private Map<String, List<a>> b = new HashMap();

    public void a(a aVar) {
        Object obj = this.b.get(aVar.a().toLowerCase());
        if (obj == null) {
            obj = new LinkedList();
            this.b.put(aVar.a().toLowerCase(), obj);
        }
        obj.add(aVar);
        this.a.add(aVar);
    }

    public List<a> a() {
        return Collections.unmodifiableList(this.a);
    }

    public a a(String str) {
        List list = this.b.get(str.toLowerCase());
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (a) list.get(0);
    }

    public Iterator<a> iterator() {
        return Collections.unmodifiableList(this.a).iterator();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        for (a obj : this.a) {
            sb.append(obj.toString());
            sb.append("\r\n");
        }
        return sb.toString();
    }
}
