package org.apache.james.mime4j.util;

import java.text.DateFormat;
import org.apache.james.mime4j.util.MimeUtil;

class a extends ThreadLocal<DateFormat> {
    a() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public DateFormat initialValue() {
        return new MimeUtil.Rfc822DateFormat();
    }
}
