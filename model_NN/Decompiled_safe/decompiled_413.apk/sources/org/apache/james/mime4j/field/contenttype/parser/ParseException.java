package org.apache.james.mime4j.field.contenttype.parser;

public class ParseException extends org.apache.james.mime4j.field.ParseException {
    private static final long serialVersionUID = 1;
    public e currentToken;
    protected String eol = System.getProperty("line.separator", "\n");
    public int[][] expectedTokenSequences;
    protected boolean specialConstructor = false;
    public String[] tokenImage;

    public ParseException(e eVar, int[][] iArr, String[] strArr) {
        super("");
        this.currentToken = eVar;
        this.expectedTokenSequences = iArr;
        this.tokenImage = strArr;
    }

    public ParseException() {
        super("Cannot parse field");
    }

    public ParseException(Throwable th) {
        super(th);
    }

    public ParseException(String str) {
        super(str);
    }

    public String getMessage() {
        String str;
        String str2;
        if (!this.specialConstructor) {
            return super.getMessage();
        }
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        for (int i2 = 0; i2 < this.expectedTokenSequences.length; i2++) {
            if (i < this.expectedTokenSequences[i2].length) {
                i = this.expectedTokenSequences[i2].length;
            }
            for (int i3 : this.expectedTokenSequences[i2]) {
                stringBuffer.append(this.tokenImage[i3]).append(" ");
            }
            if (this.expectedTokenSequences[i2][this.expectedTokenSequences[i2].length - 1] != 0) {
                stringBuffer.append("...");
            }
            stringBuffer.append(this.eol).append("    ");
        }
        e eVar = this.currentToken.g;
        String str3 = "Encountered \"";
        int i4 = 0;
        while (true) {
            if (i4 >= i) {
                str = str3;
                break;
            }
            if (i4 != 0) {
                str3 = str3 + " ";
            }
            if (eVar.a == 0) {
                str = str3 + this.tokenImage[0];
                break;
            }
            str3 = str3 + a(eVar.f);
            eVar = eVar.g;
            i4++;
        }
        String str4 = (str + "\" at line " + this.currentToken.g.b + ", column " + this.currentToken.g.c) + "." + this.eol;
        if (this.expectedTokenSequences.length == 1) {
            str2 = str4 + "Was expecting:" + this.eol + "    ";
        } else {
            str2 = str4 + "Was expecting one of:" + this.eol + "    ";
        }
        return str2 + stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case 0:
                    break;
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                    stringBuffer.append("\\\"");
                    break;
                case '\'':
                    stringBuffer.append("\\'");
                    break;
                case '\\':
                    stringBuffer.append("\\\\");
                    break;
                default:
                    char charAt = str.charAt(i);
                    if (charAt >= ' ' && charAt <= '~') {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "0000" + Integer.toString(charAt, 16);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4, str2.length()));
                        break;
                    }
                    break;
            }
        }
        return stringBuffer.toString();
    }
}
