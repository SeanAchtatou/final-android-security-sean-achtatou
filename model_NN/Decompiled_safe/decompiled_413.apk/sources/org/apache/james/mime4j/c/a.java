package org.apache.james.mime4j.c;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.apache.james.mime4j.util.d;
import org.apache.james.mime4j.util.f;

public class a implements c {
    private List<f> a = new LinkedList();
    private i b = null;
    private f c = f.a;
    private transient String d = "";
    private f e = f.a;
    private transient String f = "";
    private String g;

    public a(String str) {
        this.g = str;
    }

    public i a() {
        return this.b;
    }

    public void a(i iVar) {
        this.b = iVar;
        for (f a2 : this.a) {
            a2.a(iVar);
        }
    }

    public List<f> b() {
        return Collections.unmodifiableList(this.a);
    }

    public void a(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException();
        }
        this.a.add(fVar);
        fVar.a(this.b);
    }

    /* access modifiers changed from: package-private */
    public f c() {
        return this.c;
    }

    public String d() {
        if (this.d == null) {
            this.d = d.a(this.c);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public f e() {
        return this.e;
    }

    public String f() {
        if (this.f == null) {
            this.f = d.a(this.e);
        }
        return this.f;
    }
}
