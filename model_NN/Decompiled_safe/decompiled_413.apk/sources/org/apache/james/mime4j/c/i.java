package org.apache.james.mime4j.c;

import org.apache.james.mime4j.field.s;

public abstract class i {
    private b a = null;
    private c b = null;
    private i c = null;

    protected i() {
    }

    public void a(i iVar) {
        this.c = iVar;
    }

    public b b() {
        return this.a;
    }

    public void a(b bVar) {
        this.a = bVar;
    }

    public c c() {
        return this.b;
    }

    public void a(c cVar) {
        if (this.b != null) {
            throw new IllegalStateException("body already set");
        }
        this.b = cVar;
        cVar.a(this);
    }

    public String d() {
        return s.a((s) b().a("Content-Transfer-Encoding"));
    }
}
