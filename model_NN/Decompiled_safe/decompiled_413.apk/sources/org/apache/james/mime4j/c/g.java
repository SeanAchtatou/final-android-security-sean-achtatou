package org.apache.james.mime4j.c;

import java.io.OutputStream;
import java.util.Iterator;
import org.apache.james.mime4j.b.a;
import org.apache.james.mime4j.d.c;
import org.apache.james.mime4j.util.MimeUtil;
import org.apache.james.mime4j.util.d;
import org.apache.james.mime4j.util.f;

public class g {
    public static final g a = new g();
    private static final byte[] b = {13, 10};
    private static final byte[] c = {45, 45};

    protected g() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.james.mime4j.c.g.a(org.apache.james.mime4j.c.i, java.io.OutputStream):void
     arg types: [org.apache.james.mime4j.c.e, java.io.OutputStream]
     candidates:
      org.apache.james.mime4j.c.g.a(org.apache.james.mime4j.util.f, java.io.OutputStream):void
      org.apache.james.mime4j.c.g.a(org.apache.james.mime4j.c.a, java.io.OutputStream):void
      org.apache.james.mime4j.c.g.a(org.apache.james.mime4j.c.b, java.io.OutputStream):void
      org.apache.james.mime4j.c.g.a(org.apache.james.mime4j.c.c, java.io.OutputStream):void
      org.apache.james.mime4j.c.g.a(org.apache.james.mime4j.c.i, java.io.OutputStream):void */
    public void a(c cVar, OutputStream outputStream) {
        if (cVar instanceof e) {
            a((i) ((e) cVar), outputStream);
        } else if (cVar instanceof a) {
            a((a) cVar, outputStream);
        } else if (cVar instanceof h) {
            ((h) cVar).a(outputStream);
        } else {
            throw new IllegalArgumentException("Unsupported body class");
        }
    }

    public void a(i iVar, OutputStream outputStream) {
        b b2 = iVar.b();
        if (b2 == null) {
            throw new IllegalArgumentException("Missing header");
        }
        a(b2, outputStream);
        c c2 = iVar.c();
        if (c2 == null) {
            throw new IllegalArgumentException("Missing body");
        }
        OutputStream a2 = a(outputStream, iVar.d(), c2 instanceof d);
        a(c2, a2);
        if (a2 != outputStream) {
            a2.close();
        }
    }

    public void a(a aVar, OutputStream outputStream) {
        f a2 = a(a(aVar));
        a(aVar.c(), outputStream);
        outputStream.write(b);
        for (f a3 : aVar.b()) {
            outputStream.write(c);
            a(a2, outputStream);
            outputStream.write(b);
            a(a3, outputStream);
            outputStream.write(b);
        }
        outputStream.write(c);
        a(a2, outputStream);
        outputStream.write(c);
        outputStream.write(b);
        a(aVar.e(), outputStream);
    }

    public void a(b bVar, OutputStream outputStream) {
        Iterator<a> it = bVar.iterator();
        while (it.hasNext()) {
            a(it.next().c(), outputStream);
            outputStream.write(b);
        }
        outputStream.write(b);
    }

    /* access modifiers changed from: protected */
    public OutputStream a(OutputStream outputStream, String str, boolean z) {
        if (MimeUtil.a(str)) {
            return c.a(outputStream);
        }
        return MimeUtil.b(str) ? c.a(outputStream, z) : outputStream;
    }

    private org.apache.james.mime4j.field.f a(a aVar) {
        i a2 = aVar.a();
        if (a2 == null) {
            throw new IllegalArgumentException("Missing parent entity in multipart");
        }
        b b2 = a2.b();
        if (b2 == null) {
            throw new IllegalArgumentException("Missing header in parent entity");
        }
        org.apache.james.mime4j.field.f fVar = (org.apache.james.mime4j.field.f) b2.a("Content-Type");
        if (fVar != null) {
            return fVar;
        }
        throw new IllegalArgumentException("Content-Type field not specified");
    }

    private f a(org.apache.james.mime4j.field.f fVar) {
        String d = fVar.d();
        if (d != null) {
            return d.a(d);
        }
        throw new IllegalArgumentException("Multipart boundary not specified");
    }

    private void a(f fVar, OutputStream outputStream) {
        if (fVar instanceof org.apache.james.mime4j.util.c) {
            org.apache.james.mime4j.util.c cVar = (org.apache.james.mime4j.util.c) fVar;
            outputStream.write(cVar.c(), 0, cVar.a());
            return;
        }
        outputStream.write(fVar.b());
    }
}
