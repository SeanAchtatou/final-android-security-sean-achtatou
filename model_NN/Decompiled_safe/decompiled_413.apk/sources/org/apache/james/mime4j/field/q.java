package org.apache.james.mime4j.field;

import java.util.regex.Pattern;
import org.apache.james.mime4j.b.a;
import org.apache.james.mime4j.util.MimeUtil;
import org.apache.james.mime4j.util.d;

public class q {
    private static final Pattern a = Pattern.compile("[\\x21-\\x39\\x3b-\\x7e]+");

    private q() {
    }

    public static f a(String str) {
        return (f) a(f.a, "Content-Type", str);
    }

    private static <F extends a> F a(r rVar, String str, String str2) {
        return rVar.a(str, str2, d.a(MimeUtil.a(str + ": " + str2, 0)));
    }
}
