package org.apache.james.mime4j.field;

import org.apache.james.mime4j.MimeException;

public class ParseException extends MimeException {
    private static final long serialVersionUID = 1;

    protected ParseException(String str) {
        super(str);
    }

    protected ParseException(Throwable th) {
        super(th);
    }

    protected ParseException(String str, Throwable th) {
        super(str, th);
    }
}
