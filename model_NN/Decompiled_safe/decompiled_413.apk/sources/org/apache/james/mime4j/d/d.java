package org.apache.james.mime4j.d;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class d extends FilterOutputStream {
    private a a;
    private boolean b = false;

    public d(OutputStream outputStream, boolean z) {
        super(outputStream);
        this.a = new a(1024, z);
        this.a.a(outputStream);
    }

    public void close() {
        if (!this.b) {
            try {
                this.a.a();
            } finally {
                this.b = true;
            }
        }
    }

    public void flush() {
        this.a.b();
    }

    public void write(int i) {
        write(new byte[]{(byte) i}, 0, 1);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (this.b) {
            throw new IOException("QuotedPrintableOutputStream has been closed");
        }
        this.a.a(bArr, i, i2);
    }
}
