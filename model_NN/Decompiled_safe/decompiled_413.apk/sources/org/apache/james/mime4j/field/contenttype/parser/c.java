package org.apache.james.mime4j.field.contenttype.parser;

import java.io.IOException;
import java.io.PrintStream;

public class c implements d {
    static int a;
    static final long[] c = {0, 0, -1, -1};
    static final int[] d = new int[0];
    public static final String[] e = {"", "\r", "\n", "/", ";", "=", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null};
    public static final String[] f = {"DEFAULT", "INCOMMENT", "NESTED_COMMENT", "INQUOTEDSTRING"};
    public static final int[] g = {-1, -1, -1, -1, -1, -1, -1, 1, 0, -1, 2, -1, -1, -1, -1, -1, 3, -1, -1, 0, -1, -1, -1, -1};
    static final long[] h = {3670079};
    static final long[] i = {320};
    static final long[] j = {64};
    static final long[] k = {523904};
    public PrintStream b = System.out;
    protected b l;
    StringBuffer m;
    int n;
    int o;
    protected char p;
    int q = 0;
    int r = 0;
    int s;
    int t;
    int u;
    int v;
    private final int[] x = new int[3];
    private final int[] y = new int[6];

    private final int a(int i2, int i3) {
        this.v = i3;
        this.u = i2;
        return i2 + 1;
    }

    private final int a(int i2, int i3, int i4) {
        this.v = i3;
        this.u = i2;
        try {
            this.p = this.l.c();
            return b(i4, i2 + 1);
        } catch (IOException e2) {
            return i2 + 1;
        }
    }

    private final int d() {
        switch (this.p) {
            case 10:
                return a(0, 2, 2);
            case 13:
                return a(0, 1, 2);
            case '\"':
                return a(0, 16);
            case '(':
                return a(0, 7);
            case '/':
                return a(0, 3);
            case ';':
                return a(0, 4);
            case '=':
                return a(0, 5);
            default:
                return b(3, 0);
        }
    }

    private final void b(int i2) {
        if (this.x[i2] != this.t) {
            int[] iArr = this.y;
            int i3 = this.s;
            this.s = i3 + 1;
            iArr[i3] = i2;
            this.x[i2] = this.t;
        }
    }

    private final int b(int i2, int i3) {
        this.s = 3;
        int i4 = 1;
        this.y[0] = i2;
        int i5 = i3;
        int i6 = Integer.MAX_VALUE;
        int i7 = 0;
        while (true) {
            int i8 = this.t + 1;
            this.t = i8;
            if (i8 == Integer.MAX_VALUE) {
                h();
            }
            if (this.p < '@') {
                long j2 = 1 << this.p;
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                            if ((4294967808L & j2) != 0) {
                                i6 = 6;
                                b(0);
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if ((287948901175001088L & j2) == 0) {
                                continue;
                            } else {
                                if (i6 > 20) {
                                    i6 = 20;
                                }
                                b(1);
                                continue;
                            }
                        case 2:
                            if ((288068726467591679L & j2) == 0) {
                                continue;
                            } else {
                                if (i6 > 21) {
                                    i6 = 21;
                                }
                                b(2);
                                continue;
                            }
                        case 3:
                            if ((288068726467591679L & j2) != 0) {
                                if (i6 > 21) {
                                    i6 = 21;
                                }
                                b(2);
                            } else if ((4294967808L & j2) != 0) {
                                if (i6 > 6) {
                                    i6 = 6;
                                }
                                b(0);
                            }
                            if ((287948901175001088L & j2) == 0) {
                                continue;
                            } else {
                                if (i6 > 20) {
                                    i6 = 20;
                                }
                                b(1);
                                continue;
                            }
                    }
                } while (i4 != i7);
            } else if (this.p < 128) {
                long j3 = 1 << (this.p & '?');
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 2:
                        case 3:
                            if ((-939524098 & j3) != 0) {
                                i6 = 21;
                                b(2);
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i4 != i7);
            } else {
                int i9 = (this.p & 255) >> 6;
                long j4 = 1 << (this.p & '?');
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 2:
                        case 3:
                            if ((c[i9] & j4) == 0) {
                                continue;
                            } else {
                                if (i6 > 21) {
                                    i6 = 21;
                                }
                                b(2);
                                continue;
                            }
                    }
                } while (i4 != i7);
            }
            if (i6 != Integer.MAX_VALUE) {
                this.v = i6;
                this.u = i5;
                i6 = Integer.MAX_VALUE;
            }
            int i10 = i5 + 1;
            int i11 = this.s;
            this.s = i7;
            i7 = 3 - i7;
            if (i11 == i7) {
                return i10;
            }
            try {
                this.p = this.l.c();
                int i12 = i11;
                i5 = i10;
                i4 = i12;
            } catch (IOException e2) {
                return i10;
            }
        }
    }

    private final int e() {
        switch (this.p) {
            case '(':
                return a(0, 10);
            case ')':
                return a(0, 8);
            default:
                return c(0, 0);
        }
    }

    private final int c(int i2, int i3) {
        this.s = 3;
        int i4 = 1;
        this.y[0] = i2;
        int i5 = i3;
        int i6 = Integer.MAX_VALUE;
        int i7 = 0;
        while (true) {
            int i8 = this.t + 1;
            this.t = i8;
            if (i8 == Integer.MAX_VALUE) {
                h();
            }
            if (this.p < '@') {
                long j2 = 1 << this.p;
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                            if (i6 > 11) {
                                i6 = 11;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (i6 > 9) {
                                i6 = 9;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i4 != i7);
            } else if (this.p < 128) {
                long j3 = 1 << (this.p & '?');
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                            if (i6 > 11) {
                                i6 = 11;
                            }
                            if (this.p == '\\') {
                                int[] iArr = this.y;
                                int i9 = this.s;
                                this.s = i9 + 1;
                                iArr[i9] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (i6 > 9) {
                                i6 = 9;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if (i6 > 11) {
                                i6 = 11;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i4 != i7);
            } else {
                int i10 = (this.p & 255) >> 6;
                long j4 = 1 << (this.p & '?');
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                            if ((c[i10] & j4) != 0 && i6 > 11) {
                                i6 = 11;
                                continue;
                            }
                        case 1:
                            if ((c[i10] & j4) != 0 && i6 > 9) {
                                i6 = 9;
                                continue;
                            }
                    }
                } while (i4 != i7);
            }
            if (i6 != Integer.MAX_VALUE) {
                this.v = i6;
                this.u = i5;
                i6 = Integer.MAX_VALUE;
            }
            int i11 = i5 + 1;
            int i12 = this.s;
            this.s = i7;
            i7 = 3 - i7;
            if (i12 == i7) {
                return i11;
            }
            try {
                this.p = this.l.c();
                int i13 = i12;
                i5 = i11;
                i4 = i13;
            } catch (IOException e2) {
                return i11;
            }
        }
    }

    private final int f() {
        switch (this.p) {
            case '\"':
                return a(0, 19);
            default:
                return d(0, 0);
        }
    }

    private final int d(int i2, int i3) {
        this.s = 3;
        int i4 = 1;
        this.y[0] = i2;
        int i5 = i3;
        int i6 = Integer.MAX_VALUE;
        int i7 = 0;
        while (true) {
            int i8 = this.t + 1;
            this.t = i8;
            if (i8 == Integer.MAX_VALUE) {
                h();
            }
            if (this.p < '@') {
                long j2 = 1 << this.p;
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                        case 2:
                            if ((-17179869185L & j2) == 0) {
                                continue;
                            } else {
                                if (i6 > 18) {
                                    i6 = 18;
                                }
                                b(2);
                                continue;
                            }
                        case 1:
                            if (i6 > 17) {
                                i6 = 17;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i4 != i7);
            } else if (this.p < 128) {
                long j3 = 1 << (this.p & '?');
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                            if ((-268435457 & j3) != 0) {
                                if (i6 > 18) {
                                    i6 = 18;
                                }
                                b(2);
                                continue;
                            } else if (this.p == '\\') {
                                int[] iArr = this.y;
                                int i9 = this.s;
                                this.s = i9 + 1;
                                iArr[i9] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (i6 > 17) {
                                i6 = 17;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if ((-268435457 & j3) == 0) {
                                continue;
                            } else {
                                if (i6 > 18) {
                                    i6 = 18;
                                }
                                b(2);
                                continue;
                            }
                    }
                } while (i4 != i7);
            } else {
                int i10 = (this.p & 255) >> 6;
                long j4 = 1 << (this.p & '?');
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                        case 2:
                            if ((c[i10] & j4) == 0) {
                                continue;
                            } else {
                                if (i6 > 18) {
                                    i6 = 18;
                                }
                                b(2);
                                continue;
                            }
                        case 1:
                            if ((c[i10] & j4) != 0 && i6 > 17) {
                                i6 = 17;
                                continue;
                            }
                    }
                } while (i4 != i7);
            }
            if (i6 != Integer.MAX_VALUE) {
                this.v = i6;
                this.u = i5;
                i6 = Integer.MAX_VALUE;
            }
            int i11 = i5 + 1;
            int i12 = this.s;
            this.s = i7;
            i7 = 3 - i7;
            if (i12 == i7) {
                return i11;
            }
            try {
                this.p = this.l.c();
                int i13 = i12;
                i5 = i11;
                i4 = i13;
            } catch (IOException e2) {
                return i11;
            }
        }
    }

    private final int g() {
        switch (this.p) {
            case '(':
                return a(0, 13);
            case ')':
                return a(0, 14);
            default:
                return e(0, 0);
        }
    }

    private final int e(int i2, int i3) {
        this.s = 3;
        int i4 = 1;
        this.y[0] = i2;
        int i5 = i3;
        int i6 = Integer.MAX_VALUE;
        int i7 = 0;
        while (true) {
            int i8 = this.t + 1;
            this.t = i8;
            if (i8 == Integer.MAX_VALUE) {
                h();
            }
            if (this.p < '@') {
                long j2 = 1 << this.p;
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                            if (i6 > 15) {
                                i6 = 15;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (i6 > 12) {
                                i6 = 12;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i4 != i7);
            } else if (this.p < 128) {
                long j3 = 1 << (this.p & '?');
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                            if (i6 > 15) {
                                i6 = 15;
                            }
                            if (this.p == '\\') {
                                int[] iArr = this.y;
                                int i9 = this.s;
                                this.s = i9 + 1;
                                iArr[i9] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (i6 > 12) {
                                i6 = 12;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if (i6 > 15) {
                                i6 = 15;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i4 != i7);
            } else {
                int i10 = (this.p & 255) >> 6;
                long j4 = 1 << (this.p & '?');
                do {
                    i4--;
                    switch (this.y[i4]) {
                        case 0:
                            if ((c[i10] & j4) != 0 && i6 > 15) {
                                i6 = 15;
                                continue;
                            }
                        case 1:
                            if ((c[i10] & j4) != 0 && i6 > 12) {
                                i6 = 12;
                                continue;
                            }
                    }
                } while (i4 != i7);
            }
            if (i6 != Integer.MAX_VALUE) {
                this.v = i6;
                this.u = i5;
                i6 = Integer.MAX_VALUE;
            }
            int i11 = i5 + 1;
            int i12 = this.s;
            this.s = i7;
            i7 = 3 - i7;
            if (i12 == i7) {
                return i11;
            }
            try {
                this.p = this.l.c();
                int i13 = i12;
                i5 = i11;
                i4 = i13;
            } catch (IOException e2) {
                return i11;
            }
        }
    }

    public c(b bVar) {
        this.l = bVar;
    }

    private final void h() {
        this.t = -2147483647;
        int i2 = 3;
        while (true) {
            int i3 = i2 - 1;
            if (i2 > 0) {
                this.x[i3] = Integer.MIN_VALUE;
                i2 = i3;
            } else {
                return;
            }
        }
    }

    public void a(int i2) {
        if (i2 >= 4 || i2 < 0) {
            throw new TokenMgrError("Error: Ignoring invalid lexical state : " + i2 + ". State unchanged.", 2);
        }
        this.q = i2;
    }

    /* access modifiers changed from: protected */
    public e a() {
        e a2 = e.a(this.v);
        a2.a = this.v;
        String str = e[this.v];
        if (str == null) {
            str = this.l.h();
        }
        a2.f = str;
        a2.b = this.l.g();
        a2.c = this.l.f();
        a2.d = this.l.e();
        a2.e = this.l.d();
        return a2;
    }

    public e b() {
        String str;
        int i2;
        int i3;
        boolean z;
        String str2;
        String h2;
        int i4 = 0;
        e eVar = null;
        while (true) {
            try {
                this.p = this.l.b();
                this.m = null;
                this.n = 0;
                while (true) {
                    switch (this.q) {
                        case 0:
                            this.v = Integer.MAX_VALUE;
                            this.u = 0;
                            i4 = d();
                            break;
                        case 1:
                            this.v = Integer.MAX_VALUE;
                            this.u = 0;
                            i4 = e();
                            break;
                        case 2:
                            this.v = Integer.MAX_VALUE;
                            this.u = 0;
                            i4 = g();
                            break;
                        case 3:
                            this.v = Integer.MAX_VALUE;
                            this.u = 0;
                            i4 = f();
                            break;
                    }
                    if (this.v != Integer.MAX_VALUE) {
                        if (this.u + 1 < i4) {
                            this.l.a((i4 - this.u) - 1);
                        }
                        if ((h[this.v >> 6] & (1 << (this.v & 63))) != 0) {
                            e a2 = a();
                            a2.h = eVar;
                            a(a2);
                            if (g[this.v] == -1) {
                                return a2;
                            }
                            this.q = g[this.v];
                            return a2;
                        } else if ((i[this.v >> 6] & (1 << (this.v & 63))) != 0) {
                            if ((j[this.v >> 6] & (1 << (this.v & 63))) != 0) {
                                e a3 = a();
                                if (eVar == null) {
                                    eVar = a3;
                                } else {
                                    a3.h = eVar;
                                    eVar.g = a3;
                                    eVar = a3;
                                }
                            }
                            if (g[this.v] != -1) {
                                this.q = g[this.v];
                            }
                        } else {
                            c();
                            if (g[this.v] != -1) {
                                this.q = g[this.v];
                            }
                            this.v = Integer.MAX_VALUE;
                            try {
                                this.p = this.l.c();
                                i4 = 0;
                            } catch (IOException e2) {
                                i4 = 0;
                            }
                        }
                    }
                }
            } catch (IOException e3) {
                this.v = 0;
                e a4 = a();
                a4.h = eVar;
                return a4;
            }
        }
        int e4 = this.l.e();
        int d2 = this.l.d();
        try {
            this.l.c();
            this.l.a(1);
            i2 = d2;
            i3 = e4;
            z = false;
            str = null;
        } catch (IOException e5) {
            String h3 = i4 <= 1 ? "" : this.l.h();
            if (this.p == 10 || this.p == 13) {
                str = h3;
                i2 = 0;
                i3 = e4 + 1;
                z = true;
            } else {
                i2 = d2 + 1;
                str = h3;
                i3 = e4;
                z = true;
            }
        }
        if (!z) {
            this.l.a(1);
            if (i4 <= 1) {
                h2 = "";
            } else {
                h2 = this.l.h();
            }
            str2 = h2;
        } else {
            str2 = str;
        }
        throw new TokenMgrError(z, this.q, i3, i2, str2, this.p, 0);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int i2 = this.n;
        int i3 = this.u + 1;
        this.o = i3;
        this.n = i2 + i3;
        switch (this.v) {
            case 9:
                if (this.m == null) {
                    this.m = new StringBuffer();
                }
                this.m.append(this.l.b(this.n));
                this.n = 0;
                this.m.deleteCharAt(this.m.length() - 2);
                return;
            case 10:
                if (this.m == null) {
                    this.m = new StringBuffer();
                }
                this.m.append(this.l.b(this.n));
                this.n = 0;
                a = 1;
                return;
            case 11:
            case 15:
            default:
                return;
            case 12:
                if (this.m == null) {
                    this.m = new StringBuffer();
                }
                this.m.append(this.l.b(this.n));
                this.n = 0;
                this.m.deleteCharAt(this.m.length() - 2);
                return;
            case 13:
                if (this.m == null) {
                    this.m = new StringBuffer();
                }
                this.m.append(this.l.b(this.n));
                this.n = 0;
                a++;
                return;
            case 14:
                if (this.m == null) {
                    this.m = new StringBuffer();
                }
                this.m.append(this.l.b(this.n));
                this.n = 0;
                a--;
                if (a == 0) {
                    a(1);
                    return;
                }
                return;
            case 16:
                if (this.m == null) {
                    this.m = new StringBuffer();
                }
                this.m.append(this.l.b(this.n));
                this.n = 0;
                this.m.deleteCharAt(this.m.length() - 1);
                return;
            case 17:
                if (this.m == null) {
                    this.m = new StringBuffer();
                }
                this.m.append(this.l.b(this.n));
                this.n = 0;
                this.m.deleteCharAt(this.m.length() - 2);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar) {
        switch (this.v) {
            case 19:
                if (this.m == null) {
                    this.m = new StringBuffer();
                }
                StringBuffer stringBuffer = this.m;
                b bVar = this.l;
                int i2 = this.n;
                int i3 = this.u + 1;
                this.o = i3;
                stringBuffer.append(bVar.b(i2 + i3));
                eVar.f = this.m.substring(0, this.m.length() - 1);
                return;
            default:
                return;
        }
    }
}
