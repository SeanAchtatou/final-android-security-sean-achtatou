package org.apache.james.mime4j.util;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class d {
    private d() {
    }

    public static f a(String str) {
        return a(e.a, str);
    }

    public static f a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        c cVar = new c(encode.remaining());
        cVar.a(encode.array(), encode.position(), encode.remaining());
        return cVar;
    }

    public static String a(f fVar) {
        return a(e.a, fVar, 0, fVar.a());
    }

    public static String a(Charset charset, f fVar, int i, int i2) {
        if (fVar instanceof c) {
            return a(charset, ((c) fVar).c(), i, i2);
        }
        return a(charset, fVar.b(), i, i2);
    }

    private static String a(Charset charset, byte[] bArr, int i, int i2) {
        return charset.decode(ByteBuffer.wrap(bArr, i, i2)).toString();
    }
}
