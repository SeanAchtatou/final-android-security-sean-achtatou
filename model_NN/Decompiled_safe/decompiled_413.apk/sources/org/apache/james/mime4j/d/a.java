package org.apache.james.mime4j.d;

import java.io.OutputStream;

final class a {
    private static final byte[] a = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70};
    private final byte[] b;
    private final byte[] c;
    private final boolean d;
    private boolean e;
    private boolean f;
    private boolean g;
    private int h = 77;
    private int i = 0;
    private OutputStream j = null;

    public a(int i2, boolean z) {
        this.b = new byte[i2];
        this.c = new byte[(i2 * 3)];
        this.d = z;
        this.e = false;
        this.f = false;
        this.g = false;
    }

    /* access modifiers changed from: package-private */
    public void a(OutputStream outputStream) {
        this.j = outputStream;
        this.e = false;
        this.f = false;
        this.g = false;
        this.h = 77;
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] bArr, int i2, int i3) {
        for (int i4 = i2; i4 < i3 + i2; i4++) {
            a(bArr[i4]);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        c();
        b();
    }

    private void c() {
        if (this.e) {
            b((byte) 32);
        } else if (this.f) {
            b((byte) 9);
        } else if (this.g) {
            b((byte) 13);
        }
        d();
    }

    private void d() {
        this.e = false;
        this.f = false;
        this.g = false;
    }

    private void a(byte b2) {
        if (b2 == 10) {
            if (this.d) {
                c();
                c(b2);
            } else if (this.g) {
                if (this.e) {
                    c((byte) 32);
                } else if (this.f) {
                    c((byte) 9);
                }
                f();
                d();
            } else {
                c();
                b(b2);
            }
        } else if (b2 != 13) {
            c();
            if (b2 == 32) {
                if (this.d) {
                    c(b2);
                } else {
                    this.e = true;
                }
            } else if (b2 == 9) {
                if (this.d) {
                    c(b2);
                } else {
                    this.f = true;
                }
            } else if (b2 < 32) {
                c(b2);
            } else if (b2 > 126) {
                c(b2);
            } else if (b2 == 61) {
                c(b2);
            } else {
                b(b2);
            }
        } else if (this.d) {
            c(b2);
        } else {
            this.g = true;
        }
    }

    private void b(byte b2) {
        int i2 = this.h - 1;
        this.h = i2;
        if (i2 <= 1) {
            e();
        }
        d(b2);
    }

    private void c(byte b2) {
        int i2 = this.h - 1;
        this.h = i2;
        if (i2 <= 3) {
            e();
        }
        byte b3 = b2 & 255;
        d((byte) 61);
        this.h--;
        d(a[b3 >> 4]);
        this.h--;
        d(a[b3 % 16]);
    }

    private void d(byte b2) {
        byte[] bArr = this.c;
        int i2 = this.i;
        this.i = i2 + 1;
        bArr[i2] = b2;
        if (this.i >= this.c.length) {
            b();
        }
    }

    private void e() {
        d((byte) 61);
        f();
    }

    private void f() {
        d((byte) 13);
        d((byte) 10);
        this.h = 76;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.i < this.c.length) {
            this.j.write(this.c, 0, this.i);
        } else {
            this.j.write(this.c);
        }
        this.i = 0;
    }
}
