package org.apache.james.mime4j.util;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class MimeUtil {
    private static final Log a = LogFactory.getLog(MimeUtil.class);
    private static final Random b = new Random();
    private static int c = 0;
    private static final ThreadLocal<DateFormat> d = new a();

    private MimeUtil() {
    }

    public static boolean a(String str) {
        return "base64".equalsIgnoreCase(str);
    }

    public static boolean b(String str) {
        return "quoted-printable".equalsIgnoreCase(str);
    }

    public static String a(String str, int i) {
        int length = str.length();
        if (i + length <= 76) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        int i2 = -i;
        int b2 = b(str, 0);
        while (b2 != length) {
            int b3 = b(str, b2 + 1);
            if (b3 - i2 > 76) {
                sb.append(str.substring(Math.max(0, i2), b2));
                sb.append("\r\n");
            } else {
                b2 = i2;
            }
            i2 = b2;
            b2 = b3;
        }
        sb.append(str.substring(Math.max(0, i2)));
        return sb.toString();
    }

    private static int b(String str, int i) {
        int length = str.length();
        for (int i2 = i; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt == ' ' || charAt == 9) {
                return i2;
            }
        }
        return length;
    }

    private static final class Rfc822DateFormat extends SimpleDateFormat {
        private static final long serialVersionUID = 1;

        public Rfc822DateFormat() {
            super("EEE, d MMM yyyy HH:mm:ss ", Locale.US);
        }

        public StringBuffer format(Date date, StringBuffer stringBuffer, FieldPosition fieldPosition) {
            StringBuffer format = super.format(date, stringBuffer, fieldPosition);
            int i = ((this.calendar.get(15) + this.calendar.get(16)) / 1000) / 60;
            if (i < 0) {
                format.append('-');
                i = -i;
            } else {
                format.append('+');
            }
            format.append(String.format("%02d%02d", Integer.valueOf(i / 60), Integer.valueOf(i % 60)));
            return format;
        }
    }
}
