package org.apache.james.mime4j.field.contenttype.parser;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class a implements d {
    private static int[] l;
    public c a;
    b b;
    public e c;
    public e d;
    private String e;
    private String f;
    private List<String> g = new ArrayList();
    private List<String> h = new ArrayList();
    private int i;
    private int j;
    private final int[] k = new int[3];
    private Vector<int[]> m = new Vector<>();
    private int[] n;
    private int o = -1;

    public String a() {
        return this.e;
    }

    public String b() {
        return this.f;
    }

    public List<String> c() {
        return this.g;
    }

    public List<String> d() {
        return this.h;
    }

    public final void e() {
        f();
        a(0);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x001b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d A[FALL_THROUGH, LOOP:0: B:1:0x0016->B:8:0x002d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022 A[SYNTHETIC] */
    public final void f() {
        /*
            r3 = this;
            r2 = 21
            org.apache.james.mime4j.field.contenttype.parser.e r0 = r3.a(r2)
            r1 = 3
            r3.a(r1)
            org.apache.james.mime4j.field.contenttype.parser.e r1 = r3.a(r2)
            java.lang.String r0 = r0.f
            r3.e = r0
            java.lang.String r0 = r1.f
            r3.f = r0
        L_0x0016:
            int r0 = r3.i
            r1 = -1
            if (r0 != r1) goto L_0x002a
            int r0 = r3.k()
        L_0x001f:
            switch(r0) {
                case 4: goto L_0x002d;
                default: goto L_0x0022;
            }
        L_0x0022:
            int[] r0 = r3.k
            r1 = 1
            int r2 = r3.j
            r0[r1] = r2
            return
        L_0x002a:
            int r0 = r3.i
            goto L_0x001f
        L_0x002d:
            r0 = 4
            r3.a(r0)
            r3.g()
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.contenttype.parser.a.f():void");
    }

    public final void g() {
        e a2 = a(21);
        a(5);
        String h2 = h();
        this.g.add(a2.f);
        this.h.add(h2);
    }

    public final String h() {
        int i2;
        e a2;
        if (this.i == -1) {
            i2 = k();
        } else {
            i2 = this.i;
        }
        switch (i2) {
            case 19:
                a2 = a(19);
                break;
            case 20:
                a2 = a(20);
                break;
            case 21:
                a2 = a(21);
                break;
            default:
                this.k[2] = this.j;
                a(-1);
                throw new ParseException();
        }
        return a2.f;
    }

    static {
        j();
    }

    private static void j() {
        l = new int[]{2, 16, 3670016};
    }

    public a(Reader reader) {
        this.b = new b(reader, 1, 1);
        this.a = new c(this.b);
        this.c = new e();
        this.i = -1;
        this.j = 0;
        for (int i2 = 0; i2 < 3; i2++) {
            this.k[i2] = -1;
        }
    }

    private final e a(int i2) {
        e eVar = this.c;
        if (eVar.g != null) {
            this.c = this.c.g;
        } else {
            e eVar2 = this.c;
            e b2 = this.a.b();
            eVar2.g = b2;
            this.c = b2;
        }
        this.i = -1;
        if (this.c.a == i2) {
            this.j++;
            return this.c;
        }
        this.c = eVar;
        this.o = i2;
        throw i();
    }

    private final int k() {
        e eVar = this.c.g;
        this.d = eVar;
        if (eVar == null) {
            e eVar2 = this.c;
            e b2 = this.a.b();
            eVar2.g = b2;
            int i2 = b2.a;
            this.i = i2;
            return i2;
        }
        int i3 = this.d.a;
        this.i = i3;
        return i3;
    }

    public ParseException i() {
        this.m.removeAllElements();
        boolean[] zArr = new boolean[24];
        for (int i2 = 0; i2 < 24; i2++) {
            zArr[i2] = false;
        }
        if (this.o >= 0) {
            zArr[this.o] = true;
            this.o = -1;
        }
        for (int i3 = 0; i3 < 3; i3++) {
            if (this.k[i3] == this.j) {
                for (int i4 = 0; i4 < 32; i4++) {
                    if ((l[i3] & (1 << i4)) != 0) {
                        zArr[i4] = true;
                    }
                }
            }
        }
        for (int i5 = 0; i5 < 24; i5++) {
            if (zArr[i5]) {
                this.n = new int[1];
                this.n[0] = i5;
                this.m.addElement(this.n);
            }
        }
        int[][] iArr = new int[this.m.size()][];
        for (int i6 = 0; i6 < this.m.size(); i6++) {
            iArr[i6] = this.m.elementAt(i6);
        }
        return new ParseException(this.c, iArr, w);
    }
}
