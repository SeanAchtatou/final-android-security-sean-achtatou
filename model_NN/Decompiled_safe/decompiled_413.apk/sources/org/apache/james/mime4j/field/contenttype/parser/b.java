package org.apache.james.mime4j.field.contenttype.parser;

import java.io.IOException;
import java.io.Reader;

public class b {
    int a;
    int b;
    int c;
    public int d;
    protected int[] e;
    protected int[] f;
    protected int g;
    protected int h;
    protected boolean i;
    protected boolean j;
    protected Reader k;
    protected char[] l;
    protected int m;
    protected int n;
    protected int o;

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        char[] cArr = new char[(this.a + 2048)];
        int[] iArr = new int[(this.a + 2048)];
        int[] iArr2 = new int[(this.a + 2048)];
        if (z) {
            try {
                System.arraycopy(this.l, this.c, cArr, 0, this.a - this.c);
                System.arraycopy(this.l, 0, cArr, this.a - this.c, this.d);
                this.l = cArr;
                System.arraycopy(this.e, this.c, iArr, 0, this.a - this.c);
                System.arraycopy(this.e, 0, iArr, this.a - this.c, this.d);
                this.e = iArr;
                System.arraycopy(this.f, this.c, iArr2, 0, this.a - this.c);
                System.arraycopy(this.f, 0, iArr2, this.a - this.c, this.d);
                this.f = iArr2;
                int i2 = this.d + (this.a - this.c);
                this.d = i2;
                this.m = i2;
            } catch (Throwable th) {
                throw new Error(th.getMessage());
            }
        } else {
            System.arraycopy(this.l, this.c, cArr, 0, this.a - this.c);
            this.l = cArr;
            System.arraycopy(this.e, this.c, iArr, 0, this.a - this.c);
            this.e = iArr;
            System.arraycopy(this.f, this.c, iArr2, 0, this.a - this.c);
            this.f = iArr2;
            int i3 = this.d - this.c;
            this.d = i3;
            this.m = i3;
        }
        this.a += 2048;
        this.b = this.a;
        this.c = 0;
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.m == this.b) {
            if (this.b == this.a) {
                if (this.c > 2048) {
                    this.m = 0;
                    this.d = 0;
                    this.b = this.c;
                } else if (this.c < 0) {
                    this.m = 0;
                    this.d = 0;
                } else {
                    a(false);
                }
            } else if (this.b > this.c) {
                this.b = this.a;
            } else if (this.c - this.b < 2048) {
                a(true);
            } else {
                this.b = this.c;
            }
        }
        try {
            int read = this.k.read(this.l, this.m, this.b - this.m);
            if (read == -1) {
                this.k.close();
                throw new IOException();
            } else {
                this.m = read + this.m;
            }
        } catch (IOException e2) {
            this.d--;
            a(0);
            if (this.c == -1) {
                this.c = this.d;
            }
            throw e2;
        }
    }

    public char b() {
        this.c = -1;
        char c2 = c();
        this.c = this.d;
        return c2;
    }

    /* access modifiers changed from: protected */
    public void a(char c2) {
        this.g++;
        if (this.j) {
            this.j = false;
            int i2 = this.h;
            this.g = 1;
            this.h = i2 + 1;
        } else if (this.i) {
            this.i = false;
            if (c2 == 10) {
                this.j = true;
            } else {
                int i3 = this.h;
                this.g = 1;
                this.h = i3 + 1;
            }
        }
        switch (c2) {
            case 9:
                this.g--;
                this.g += this.o - (this.g % this.o);
                break;
            case 10:
                this.j = true;
                break;
            case 13:
                this.i = true;
                break;
        }
        this.e[this.d] = this.h;
        this.f[this.d] = this.g;
    }

    public char c() {
        if (this.n > 0) {
            this.n--;
            int i2 = this.d + 1;
            this.d = i2;
            if (i2 == this.a) {
                this.d = 0;
            }
            return this.l[this.d];
        }
        int i3 = this.d + 1;
        this.d = i3;
        if (i3 >= this.m) {
            a();
        }
        char c2 = this.l[this.d];
        a(c2);
        return c2;
    }

    public int d() {
        return this.f[this.d];
    }

    public int e() {
        return this.e[this.d];
    }

    public int f() {
        return this.f[this.c];
    }

    public int g() {
        return this.e[this.c];
    }

    public void a(int i2) {
        this.n += i2;
        int i3 = this.d - i2;
        this.d = i3;
        if (i3 < 0) {
            this.d += this.a;
        }
    }

    public b(Reader reader, int i2, int i3, int i4) {
        this.d = -1;
        this.g = 0;
        this.h = 1;
        this.i = false;
        this.j = false;
        this.m = 0;
        this.n = 0;
        this.o = 8;
        this.k = reader;
        this.h = i2;
        this.g = i3 - 1;
        this.a = i4;
        this.b = i4;
        this.l = new char[i4];
        this.e = new int[i4];
        this.f = new int[i4];
    }

    public b(Reader reader, int i2, int i3) {
        this(reader, i2, i3, 4096);
    }

    public String h() {
        if (this.d >= this.c) {
            return new String(this.l, this.c, (this.d - this.c) + 1);
        }
        return new String(this.l, this.c, this.a - this.c) + new String(this.l, 0, this.d + 1);
    }

    public char[] b(int i2) {
        char[] cArr = new char[i2];
        if (this.d + 1 >= i2) {
            System.arraycopy(this.l, (this.d - i2) + 1, cArr, 0, i2);
        } else {
            System.arraycopy(this.l, this.a - ((i2 - this.d) - 1), cArr, 0, (i2 - this.d) - 1);
            System.arraycopy(this.l, 0, cArr, (i2 - this.d) - 1, this.d + 1);
        }
        return cArr;
    }
}
