package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.contenttype.parser.ParseException;
import org.apache.james.mime4j.field.contenttype.parser.TokenMgrError;
import org.apache.james.mime4j.field.contenttype.parser.a;

public class f extends g {
    static final r a = new n();
    private static Log b = LogFactory.getLog(f.class);
    private boolean c = false;
    private String d = "";
    private Map<String, String> e = new HashMap();
    private ParseException f;

    f(String str, String str2, org.apache.james.mime4j.util.f fVar) {
        super(str, str2, fVar);
    }

    public String a(String str) {
        if (!this.c) {
            f();
        }
        return this.e.get(str.toLowerCase());
    }

    public String d() {
        return a("boundary");
    }

    public String e() {
        return a("charset");
    }

    private void f() {
        String b2 = b();
        a aVar = new a(new StringReader(b2));
        try {
            aVar.e();
        } catch (ParseException e2) {
            if (b.isDebugEnabled()) {
                b.debug("Parsing value '" + b2 + "': " + e2.getMessage());
            }
            this.f = e2;
        } catch (TokenMgrError e3) {
            if (b.isDebugEnabled()) {
                b.debug("Parsing value '" + b2 + "': " + e3.getMessage());
            }
            this.f = new ParseException(e3.getMessage());
        }
        String a2 = aVar.a();
        String b3 = aVar.b();
        if (!(a2 == null || b3 == null)) {
            this.d = (a2 + "/" + b3).toLowerCase();
            List<String> c2 = aVar.c();
            List<String> d2 = aVar.d();
            if (!(c2 == null || d2 == null)) {
                int min = Math.min(c2.size(), d2.size());
                for (int i = 0; i < min; i++) {
                    this.e.put(c2.get(i).toLowerCase(), d2.get(i));
                }
            }
        }
        this.c = true;
    }
}
