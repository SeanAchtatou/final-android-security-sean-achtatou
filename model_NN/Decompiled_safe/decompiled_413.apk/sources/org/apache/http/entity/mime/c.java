package org.apache.http.entity.mime;

import org.apache.http.annotation.Immutable;
import org.apache.james.mime4j.b.a;
import org.apache.james.mime4j.util.d;
import org.apache.james.mime4j.util.f;

@Immutable
public class c implements a {
    private final String a;
    private final String b;
    private f c = null;

    c(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public f c() {
        if (this.c == null) {
            this.c = d.a(toString());
        }
        return this.c;
    }

    public String toString() {
        return this.a + ": " + this.b;
    }
}
