package org.apache.http.entity.mime.a;

import org.apache.http.annotation.NotThreadSafe;
import org.apache.james.mime4j.c.h;
import org.apache.james.mime4j.c.i;

@NotThreadSafe
public abstract class a extends h implements c {
    private final String a;
    private final String b;
    private final String c;
    private i d = null;

    public a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("MIME type may not be null");
        }
        this.a = str;
        int indexOf = str.indexOf(47);
        if (indexOf != -1) {
            this.b = str.substring(0, indexOf);
            this.c = str.substring(indexOf + 1);
            return;
        }
        this.b = str;
        this.c = null;
    }

    public void a(i iVar) {
        this.d = iVar;
    }

    public String a() {
        return this.a;
    }
}
