package org.apache.http.entity.mime;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.entity.mime.a.c;
import org.apache.http.message.BasicHeader;
import org.apache.james.mime4j.c.b;
import org.apache.james.mime4j.c.e;
import org.apache.james.mime4j.c.f;
import org.apache.james.mime4j.field.q;

@ThreadSafe
public class d implements HttpEntity {
    private static final char[] a = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private final e b;
    private final e c;
    private final Header d;
    private long e;
    private volatile boolean f;

    public d(HttpMultipartMode httpMultipartMode, String str, Charset charset) {
        HttpMultipartMode httpMultipartMode2;
        this.c = new e("form-data");
        this.d = new BasicHeader("Content-Type", a(str, charset));
        this.f = true;
        this.b = new e();
        this.b.a(new b());
        this.c.a(this.b);
        if (httpMultipartMode == null) {
            httpMultipartMode2 = HttpMultipartMode.STRICT;
        } else {
            httpMultipartMode2 = httpMultipartMode;
        }
        this.c.a(httpMultipartMode2);
        this.b.b().a(q.a(this.d.getValue()));
    }

    public d() {
        this(HttpMultipartMode.STRICT, null, null);
    }

    /* access modifiers changed from: protected */
    public String a(String str, Charset charset) {
        StringBuilder sb = new StringBuilder();
        sb.append("multipart/form-data; boundary=");
        if (str != null) {
            sb.append(str);
        } else {
            Random random = new Random();
            int nextInt = random.nextInt(11) + 30;
            for (int i = 0; i < nextInt; i++) {
                sb.append(a[random.nextInt(a.length)]);
            }
        }
        if (charset != null) {
            sb.append("; charset=");
            sb.append(charset.name());
        }
        return sb.toString();
    }

    public void a(String str, c cVar) {
        this.c.a((f) new a(str, cVar));
        this.f = true;
    }

    public boolean isRepeatable() {
        Iterator<f> it = this.c.b().iterator();
        while (it.hasNext()) {
            if (((c) ((a) it.next()).c()).d() < 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isChunked() {
        return !isRepeatable();
    }

    public boolean isStreaming() {
        return !isRepeatable();
    }

    public long getContentLength() {
        if (this.f) {
            this.e = this.c.i();
            this.f = false;
        }
        return this.e;
    }

    public Header getContentType() {
        return this.d;
    }

    public Header getContentEncoding() {
        return null;
    }

    public void consumeContent() {
        if (isStreaming()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    public InputStream getContent() {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    public void writeTo(OutputStream outputStream) {
        this.c.a(outputStream);
    }
}
