package org.apache.http.entity.mime;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.List;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.james.mime4j.c.g;
import org.apache.james.mime4j.util.c;
import org.apache.james.mime4j.util.f;

@NotThreadSafe
public class e extends org.apache.james.mime4j.c.a {
    private static final c a = a(b.a, "\r\n");
    private static final c b = a(b.a, "--");
    private HttpMultipartMode c = HttpMultipartMode.STRICT;

    private static c a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        c cVar = new c(encode.remaining());
        cVar.a(encode.array(), encode.position(), encode.remaining());
        return cVar;
    }

    private static void a(c cVar, OutputStream outputStream) {
        outputStream.write(cVar.c(), 0, cVar.a());
    }

    private static void a(f fVar, OutputStream outputStream) {
        if (fVar instanceof c) {
            a((c) fVar, outputStream);
        } else {
            outputStream.write(fVar.b());
        }
    }

    public e(String str) {
        super(str);
    }

    public void a(HttpMultipartMode httpMultipartMode) {
        this.c = httpMultipartMode;
    }

    /* access modifiers changed from: protected */
    public Charset g() {
        org.apache.james.mime4j.field.f fVar = (org.apache.james.mime4j.field.f) a().b().a("Content-Type");
        switch (this.c) {
            case STRICT:
                return b.a;
            case BROWSER_COMPATIBLE:
                if (fVar.e() != null) {
                    return org.apache.james.mime4j.util.e.a(fVar.e());
                }
                return org.apache.james.mime4j.util.e.a("ISO-8859-1");
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public String h() {
        return ((org.apache.james.mime4j.field.f) a().b().a("Content-Type")).d();
    }

    private void a(HttpMultipartMode httpMultipartMode, OutputStream outputStream, boolean z) {
        List<org.apache.james.mime4j.c.f> b2 = b();
        Charset g = g();
        c a2 = a(g, h());
        switch (httpMultipartMode) {
            case STRICT:
                String d = d();
                if (!(d == null || d.length() == 0)) {
                    a(a(g, d), outputStream);
                    a(a, outputStream);
                }
                for (int i = 0; i < b2.size(); i++) {
                    a(b, outputStream);
                    a(a2, outputStream);
                    a(a, outputStream);
                    org.apache.james.mime4j.c.f fVar = b2.get(i);
                    for (org.apache.james.mime4j.b.a c2 : fVar.b().a()) {
                        a(c2.c(), outputStream);
                        a(a, outputStream);
                    }
                    a(a, outputStream);
                    if (z) {
                        g.a.a(fVar.c(), outputStream);
                    }
                    a(a, outputStream);
                }
                a(b, outputStream);
                a(a2, outputStream);
                a(b, outputStream);
                a(a, outputStream);
                String f = f();
                if (f != null && f.length() != 0) {
                    a(a(g, f), outputStream);
                    a(a, outputStream);
                    return;
                }
                return;
            case BROWSER_COMPATIBLE:
                for (int i2 = 0; i2 < b2.size(); i2++) {
                    a(b, outputStream);
                    a(a2, outputStream);
                    a(a, outputStream);
                    org.apache.james.mime4j.c.f fVar2 = b2.get(i2);
                    org.apache.james.mime4j.b.a a3 = fVar2.b().a("Content-Disposition");
                    a(a(g, a3.a() + ": " + a3.b()), outputStream);
                    a(a, outputStream);
                    a(a, outputStream);
                    if (z) {
                        g.a.a(fVar2.c(), outputStream);
                    }
                    a(a, outputStream);
                }
                a(b, outputStream);
                a(a2, outputStream);
                a(b, outputStream);
                a(a, outputStream);
                return;
            default:
                return;
        }
    }

    public void a(OutputStream outputStream) {
        a(this.c, outputStream, true);
    }

    public long i() {
        List<org.apache.james.mime4j.c.f> b2 = b();
        long j = 0;
        for (int i = 0; i < b2.size(); i++) {
            org.apache.james.mime4j.c.c c2 = b2.get(i).c();
            if (!(c2 instanceof org.apache.http.entity.mime.a.c)) {
                return -1;
            }
            long d = ((org.apache.http.entity.mime.a.c) c2).d();
            if (d < 0) {
                return -1;
            }
            j += d;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(this.c, byteArrayOutputStream, false);
            return ((long) byteArrayOutputStream.toByteArray().length) + j;
        } catch (IOException e) {
            return -1;
        }
    }
}
