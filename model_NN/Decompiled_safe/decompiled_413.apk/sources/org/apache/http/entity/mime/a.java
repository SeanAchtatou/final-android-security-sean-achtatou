package org.apache.http.entity.mime;

import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.entity.mime.a.c;
import org.apache.james.mime4j.c.b;
import org.apache.james.mime4j.c.f;

@NotThreadSafe
public class a extends f {
    private final String a;

    public a(String str, c cVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("Body may not be null");
        } else {
            this.a = str;
            a(new b());
            a((org.apache.james.mime4j.c.c) cVar);
            a(cVar);
            a((org.apache.james.mime4j.a.a) cVar);
            b(cVar);
        }
    }

    public String a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(a());
        sb.append("\"");
        if (cVar.e() != null) {
            sb.append("; filename=\"");
            sb.append(cVar.e());
            sb.append("\"");
        }
        a("Content-Disposition", sb.toString());
    }

    /* access modifiers changed from: protected */
    public void a(org.apache.james.mime4j.a.a aVar) {
        if (aVar.a() != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(aVar.a());
            if (aVar.b() != null) {
                sb.append("; charset=");
                sb.append(aVar.b());
            }
            a("Content-Type", sb.toString());
        }
    }

    /* access modifiers changed from: protected */
    public void b(org.apache.james.mime4j.a.a aVar) {
        if (aVar.c() != null) {
            a("Content-Transfer-Encoding", aVar.c());
        }
    }

    private void a(String str, String str2) {
        b().a(new c(str, str2));
    }
}
