package org.apache.http.entity.mime.a;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import org.apache.http.annotation.NotThreadSafe;

@NotThreadSafe
public class b extends a {
    private final byte[] a;
    private final Charset b;

    public b(String str, String str2, Charset charset) {
        super(str2);
        Charset charset2;
        if (str == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        if (charset == null) {
            charset2 = Charset.defaultCharset();
        } else {
            charset2 = charset;
        }
        this.a = str.getBytes(charset2.name());
        this.b = charset2;
    }

    public b(String str) {
        this(str, "text/plain", null);
    }

    public void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.a);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = byteArrayInputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                outputStream.flush();
                return;
            }
        }
    }

    public String c() {
        return "8bit";
    }

    public String b() {
        return this.b.name();
    }

    public long d() {
        return (long) this.a.length;
    }

    public String e() {
        return null;
    }
}
