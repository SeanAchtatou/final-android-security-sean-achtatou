package defpackage;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: k  reason: default package */
public final class k implements o {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("js");
        if (str == null) {
            b.b("Could not get the JS to evaluate.");
        }
        if (webView instanceof b) {
            AdActivity b = ((b) webView).b();
            if (b == null) {
                b.b("Could not get the AdActivity from the AdWebView.");
                return;
            }
            b b2 = b.b();
            if (b2 == null) {
                b.b("Could not get the opening WebView.");
            } else {
                g.a(b2, str);
            }
        } else {
            b.b("Trying to evaluate JS in a WebView that isn't an AdWebView");
        }
    }
}
