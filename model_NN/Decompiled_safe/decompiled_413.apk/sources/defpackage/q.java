package defpackage;

import android.app.Activity;
import android.webkit.WebView;
import android.widget.VideoView;
import com.google.ads.AdActivity;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: q  reason: default package */
public final class q implements o {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("action");
        if (webView instanceof b) {
            AdActivity b = ((b) webView).b();
            if (b == null) {
                b.a("Could not get adActivity to create the video.");
            } else if (str.equals("load")) {
                String str2 = hashMap.get("url");
                Activity d = dVar.d();
                if (d == null) {
                    b.e("activity was null while loading a video.");
                    return;
                }
                VideoView videoView = new VideoView(d);
                videoView.setVideoPath(str2);
                b.a(videoView);
            } else {
                VideoView a = b.a();
                if (a == null) {
                    b.e("Could not get the VideoView for a video GMSG.");
                } else if (str.equals("play")) {
                    a.setVisibility(0);
                    a.start();
                    b.d("Video is now playing.");
                    g.a(webView, "onVideoEvent", "{'event': 'playing'}");
                } else if (str.equals("pause")) {
                    a.pause();
                } else if (str.equals("stop")) {
                    a.stopPlayback();
                } else if (str.equals("remove")) {
                    a.setVisibility(8);
                } else if (str.equals("replay")) {
                    a.setVisibility(0);
                    a.seekTo(0);
                    a.start();
                } else if (str.equals("currentTime")) {
                    String str3 = hashMap.get("time");
                    if (str3 == null) {
                        b.e("No \"time\" parameter!");
                    } else {
                        a.seekTo((int) (Float.valueOf(str3).floatValue() * 1000.0f));
                    }
                } else if (!str.equals("position")) {
                    b.e("Unknown movie action: " + str);
                }
            }
        } else {
            b.a("Could not get adWebView to create the video.");
        }
    }
}
