package defpackage;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: h  reason: default package */
public final class h implements o {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("u");
        if (str == null) {
            b.e("Could not get URL from click gmsg.");
        } else {
            new Thread(new p(str, webView.getContext().getApplicationContext())).start();
        }
    }
}
