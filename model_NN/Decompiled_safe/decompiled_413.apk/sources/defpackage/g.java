package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: g  reason: default package */
public final class g {
    public static final Map<String, o> a;
    public static final Map<String, o> b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("/invalidRequest", new i());
        hashMap.put("/loadAdURL", new w());
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("/open", new u());
        hashMap2.put("/canOpenURLs", new l());
        hashMap2.put("/close", new j());
        hashMap2.put("/evalInOpener", new k());
        hashMap2.put("/log", new v());
        hashMap2.put("/click", new m());
        hashMap2.put("/httpTrack", new h());
        hashMap2.put("/reloadRequest", new t());
        hashMap2.put("/settings", new s());
        hashMap2.put("/touch", new r());
        hashMap2.put("/video", new q());
        b = Collections.unmodifiableMap(hashMap2);
    }

    private g() {
    }

    public static void a(WebView webView) {
        b.d("Calling onshow.");
        a(webView, "onshow", "{'version': 'afma-sdk-a-v4.1.1'}");
    }

    public static void a(WebView webView, String str) {
        webView.loadUrl("javascript:" + str);
    }

    public static void a(WebView webView, String str, String str2) {
        if (str2 != null) {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "', " + str2 + ");");
        } else {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "');");
        }
    }

    public static void a(WebView webView, Map<String, Boolean> map) {
        a(webView, "openableURLs", new JSONObject(map).toString());
    }

    static void a(d dVar, Map<String, o> map, Uri uri, WebView webView) {
        String str;
        HashMap<String, String> b2 = AdUtil.b(uri);
        if (b2 == null) {
            b.e("An error occurred while parsing the message parameters.");
            return;
        }
        if (c(uri)) {
            String host = uri.getHost();
            if (host == null) {
                b.e("An error occurred while parsing the AMSG parameters.");
                str = null;
            } else if (host.equals("launch")) {
                b2.put("a", "intent");
                b2.put("u", b2.get("url"));
                b2.remove("url");
                str = "/open";
            } else if (host.equals("closecanvas")) {
                str = "/close";
            } else if (host.equals("log")) {
                str = "/log";
            } else {
                b.e("An error occurred while parsing the AMSG: " + uri.toString());
                str = null;
            }
        } else if (b(uri)) {
            str = uri.getPath();
        } else {
            b.e("Message was neither a GMSG nor an AMSG.");
            str = null;
        }
        if (str == null) {
            b.e("An error occurred while parsing the message.");
            return;
        }
        o oVar = map.get(str);
        if (oVar == null) {
            b.e("No AdResponse found, <message: " + str + ">");
        } else {
            oVar.a(dVar, b2, webView);
        }
    }

    public static boolean a(Uri uri) {
        if (uri == null || !uri.isHierarchical()) {
            return false;
        }
        return b(uri) || c(uri);
    }

    public static void b(WebView webView) {
        b.d("Calling onhide.");
        a(webView, "onhide", null);
    }

    private static boolean b(Uri uri) {
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.equals("gmsg")) {
            return false;
        }
        String authority = uri.getAuthority();
        return authority != null && authority.equals("mobileads.google.com");
    }

    private static boolean c(Uri uri) {
        String scheme = uri.getScheme();
        return scheme != null && scheme.equals("admob");
    }
}
