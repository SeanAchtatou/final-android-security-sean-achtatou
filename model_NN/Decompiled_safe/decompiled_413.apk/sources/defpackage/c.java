package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/* renamed from: c  reason: default package */
public final class c implements Runnable {
    private String a;
    private String b = null;
    private f c;
    private d d;
    private AdRequest e;
    private WebView f;
    private String g = null;
    private LinkedList<String> h = new LinkedList<>();
    private volatile boolean i;
    private AdRequest.ErrorCode j = null;
    private boolean k = false;
    private int l = -1;
    private Thread m;

    /* renamed from: c$a */
    private class a implements Runnable {
        private final d b;
        private final WebView c;
        private final f d;
        private final AdRequest.ErrorCode e;
        private final boolean f;

        public a(d dVar, WebView webView, f fVar, AdRequest.ErrorCode errorCode, boolean z) {
            this.b = dVar;
            this.c = webView;
            this.d = fVar;
            this.e = errorCode;
            this.f = z;
        }

        public final void run() {
            this.c.stopLoading();
            this.c.destroy();
            this.d.a();
            if (this.f) {
                b h = this.b.h();
                h.stopLoading();
                h.setVisibility(8);
            }
            this.b.a(this.e);
        }
    }

    /* renamed from: c$b */
    private class b extends Exception {
        public b(String str) {
            super(str);
        }
    }

    /* renamed from: c$c  reason: collision with other inner class name */
    private class C0000c implements Runnable {
        private final String b;
        private final String c;
        private final WebView d;

        public C0000c(WebView webView, String str, String str2) {
            this.d = webView;
            this.b = str;
            this.c = str2;
        }

        public final void run() {
            this.d.loadDataWithBaseURL(this.b, this.c, "text/html", "utf-8", null);
        }
    }

    /* renamed from: c$d */
    private class d extends Exception {
        public d(String str) {
            super(str);
        }
    }

    /* renamed from: c$e */
    private class e implements Runnable {
        private final d b;
        private final LinkedList<String> c;
        private final int d;

        public e(d dVar, LinkedList<String> linkedList, int i) {
            this.b = dVar;
            this.c = linkedList;
            this.d = i;
        }

        public final void run() {
            this.b.a(this.c);
            this.b.a(this.d);
            this.b.o();
        }
    }

    public c(d dVar) {
        this.d = dVar;
        Activity d2 = dVar.d();
        if (d2 != null) {
            this.f = new b(d2.getApplicationContext(), null);
            this.f.setWebViewClient(new n(dVar, g.a, false, false));
            this.f.setVisibility(8);
            this.f.setWillNotDraw(true);
            this.c = new f(this, dVar);
            return;
        }
        this.f = null;
        this.c = null;
        com.google.ads.util.b.e("activity was null while trying to create an AdLoader.");
    }

    private String a(AdRequest adRequest, Activity activity) {
        Context applicationContext = activity.getApplicationContext();
        Map<String, Object> a2 = adRequest.a(applicationContext);
        a k2 = this.d.k();
        long h2 = k2.h();
        if (h2 > 0) {
            a2.put("prl", Long.valueOf(h2));
        }
        String g2 = k2.g();
        if (g2 != null) {
            a2.put("ppcl", g2);
        }
        String f2 = k2.f();
        if (f2 != null) {
            a2.put("pcl", f2);
        }
        long e2 = k2.e();
        if (e2 > 0) {
            a2.put("pcc", Long.valueOf(e2));
        }
        a2.put("preqs", Long.valueOf(a.i()));
        String j2 = k2.j();
        if (j2 != null) {
            a2.put("pai", j2);
        }
        if (k2.k()) {
            a2.put("aoi_timeout", "true");
        }
        if (k2.m()) {
            a2.put("aoi_nofill", "true");
        }
        String p = k2.p();
        if (p != null) {
            a2.put("pit", p);
        }
        k2.a();
        k2.d();
        if (this.d.e() instanceof com.google.ads.d) {
            a2.put("format", "interstitial_mb");
        } else {
            com.google.ads.e j3 = this.d.j();
            String eVar = j3.toString();
            if (eVar != null) {
                a2.put("format", eVar);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(j3.a()));
                hashMap.put("h", Integer.valueOf(j3.b()));
                a2.put("ad_frame", hashMap);
            }
        }
        a2.put("slotname", this.d.g());
        a2.put("js", "afma-sdk-a-v4.1.1");
        try {
            int i2 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            a2.put("msid", applicationContext.getPackageName());
            a2.put("app_name", i2 + ".android." + applicationContext.getPackageName());
            a2.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                throw new d("NETWORK_ERROR");
            }
            a2.put("net", d2);
            String e3 = AdUtil.e(applicationContext);
            if (!(e3 == null || e3.length() == 0)) {
                a2.put("cap", e3);
            }
            a2.put("u_audio", Integer.valueOf(AdUtil.f(applicationContext).ordinal()));
            DisplayMetrics a3 = AdUtil.a(activity);
            a2.put("u_sd", Float.valueOf(a3.density));
            a2.put("u_h", Integer.valueOf(AdUtil.a(applicationContext, a3)));
            a2.put("u_w", Integer.valueOf(AdUtil.b(applicationContext, a3)));
            a2.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.c()) {
                a2.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(a2) + ");" + "</script></head><body></body></html>";
            com.google.ads.util.b.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e4) {
            throw new b("NameNotFoundException");
        }
    }

    private void a(AdRequest.ErrorCode errorCode, boolean z) {
        this.c.a();
        this.d.a(new a(this.d, this.f, this.c, errorCode, z));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.k = true;
        notify();
    }

    public final synchronized void a(int i2) {
        this.l = i2;
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.j = errorCode;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final void a(AdRequest adRequest) {
        this.e = adRequest;
        this.i = false;
        this.m = new Thread(this);
        this.m.start();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.h.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.a = str2;
        this.b = str;
        notify();
    }

    public final synchronized void b(String str) {
        this.g = str;
        notify();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(com.google.ads.AdRequest$ErrorCode, boolean):void
     arg types: [com.google.ads.AdRequest$ErrorCode, int]
     candidates:
      c.a(com.google.ads.AdRequest, android.app.Activity):java.lang.String
      c.a(java.lang.String, java.lang.String):void
      c.a(com.google.ads.AdRequest$ErrorCode, boolean):void */
    public final void run() {
        synchronized (this) {
            if (this.f == null || this.c == null) {
                com.google.ads.util.b.e("adRequestWebView was null while trying to load an ad.");
                a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
                return;
            }
            Activity d2 = this.d.d();
            if (d2 == null) {
                com.google.ads.util.b.e("activity was null while forming an ad request.");
                a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
                return;
            }
            try {
                this.d.a(new C0000c(this.f, null, a(this.e, d2)));
                long m2 = this.d.m();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (m2 > 0) {
                    try {
                        wait(m2);
                    } catch (InterruptedException e2) {
                        com.google.ads.util.b.a("AdLoader InterruptedException while getting the URL: " + e2);
                        return;
                    }
                }
                try {
                    if (!this.i) {
                        if (this.j != null) {
                            a(this.j, false);
                            return;
                        } else if (this.g == null) {
                            com.google.ads.util.b.c("AdLoader timed out after " + m2 + "ms while getting the URL.");
                            a(AdRequest.ErrorCode.NETWORK_ERROR, false);
                            return;
                        } else {
                            this.c.a(this.g);
                            long elapsedRealtime2 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                            if (elapsedRealtime2 > 0) {
                                try {
                                    wait(elapsedRealtime2);
                                } catch (InterruptedException e3) {
                                    com.google.ads.util.b.a("AdLoader InterruptedException while getting the HTML: " + e3);
                                    return;
                                }
                            }
                            if (!this.i) {
                                if (this.j != null) {
                                    a(this.j, false);
                                    return;
                                } else if (this.b == null) {
                                    com.google.ads.util.b.c("AdLoader timed out after " + m2 + "ms while getting the HTML.");
                                    a(AdRequest.ErrorCode.NETWORK_ERROR, false);
                                    return;
                                } else {
                                    b h2 = this.d.h();
                                    this.d.i().a();
                                    this.d.a(new C0000c(h2, this.a, this.b));
                                    long elapsedRealtime3 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                                    if (elapsedRealtime3 > 0) {
                                        try {
                                            wait(elapsedRealtime3);
                                        } catch (InterruptedException e4) {
                                            com.google.ads.util.b.a("AdLoader InterruptedException while loading the HTML: " + e4);
                                            return;
                                        }
                                    }
                                    if (this.k) {
                                        this.d.a(new e(this.d, this.h, this.l));
                                    } else {
                                        com.google.ads.util.b.c("AdLoader timed out after " + m2 + "ms while loading the HTML.");
                                        a(AdRequest.ErrorCode.NETWORK_ERROR, true);
                                    }
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e5) {
                    com.google.ads.util.b.a("An unknown error occurred in AdLoader.", e5);
                    a(AdRequest.ErrorCode.INTERNAL_ERROR, true);
                }
            } catch (d e6) {
                com.google.ads.util.b.c("Unable to connect to network: " + e6);
                a(AdRequest.ErrorCode.NETWORK_ERROR, false);
                return;
            } catch (b e7) {
                com.google.ads.util.b.c("Caught internal exception: " + e7);
                a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
                return;
            }
        }
    }
}
