package defpackage;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.e;
import com.google.ads.util.AdUtil;

/* renamed from: b  reason: default package */
public final class b extends WebView {
    private AdActivity a;
    private e b;

    public b(Context context, e eVar) {
        super(context);
        this.b = eVar;
        setBackgroundColor(0);
        AdUtil.a(this);
        getSettings().setJavaScriptEnabled(true);
        setScrollBarStyle(0);
    }

    public final void a() {
        if (this.a != null) {
            this.a.finish();
        }
    }

    public final void a(AdActivity adActivity) {
        this.a = adActivity;
    }

    public final AdActivity b() {
        return this.a;
    }

    public final void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        try {
            super.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } catch (Exception e) {
            com.google.ads.util.b.a("An error occurred while loading data in AdWebView:", e);
        }
    }

    public final void loadUrl(String str) {
        try {
            super.loadUrl(str);
        } catch (Exception e) {
            com.google.ads.util.b.a("An error occurred while loading a URL in AdWebView:", e);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        if (isInEditMode()) {
            super.onMeasure(i, i2);
        } else if (this.b == null) {
            super.onMeasure(i, i2);
        } else {
            int mode = View.MeasureSpec.getMode(i);
            int size = View.MeasureSpec.getSize(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int size2 = View.MeasureSpec.getSize(i2);
            float f = getContext().getResources().getDisplayMetrics().density;
            int a2 = (int) (((float) this.b.a()) * f);
            int b2 = (int) (((float) this.b.b()) * f);
            if (mode == 0 || mode2 == 0) {
                super.onMeasure(i, i2);
            } else if (((float) a2) - (6.0f * f) > ((float) size) || b2 > size2) {
                com.google.ads.util.b.e("Not enough space to show ad! Wants: <" + a2 + ", " + b2 + ">, Has: <" + size + ", " + size2 + ">");
                setVisibility(8);
                setMeasuredDimension(0, 0);
            } else {
                super.onMeasure(i, i2);
            }
        }
    }
}
