package defpackage;

import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: i  reason: default package */
public final class i implements o {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        b.e("Invalid " + hashMap.get("type") + " request error: " + hashMap.get("errors"));
        c f = dVar.f();
        if (f != null) {
            f.a(AdRequest.ErrorCode.INVALID_REQUEST);
        }
    }
}
