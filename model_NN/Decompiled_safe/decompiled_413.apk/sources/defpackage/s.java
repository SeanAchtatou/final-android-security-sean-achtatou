package defpackage;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: s  reason: default package */
public final class s implements o {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("applicationTimeout");
        if (str != null) {
            try {
                dVar.a((long) (Float.parseFloat(str) * 1000.0f));
            } catch (NumberFormatException e) {
                b.b("Trying to set applicationTimeout to invalid value: " + str, e);
            }
        }
    }
}
