package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.AdActivity;
import com.google.ads.AdRequest;
import com.google.ads.a;
import com.google.ads.b;
import com.google.ads.c;
import com.google.ads.e;
import com.google.ads.util.AdUtil;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: d  reason: default package */
public final class d {
    private static final Object a = new Object();
    private WeakReference<Activity> b;
    private b c;
    private c d = null;
    private c e = null;
    private AdRequest f = null;
    private e g;
    private a h = new a();
    private String i;
    private b j;
    private n k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private x r;
    private boolean s;
    private LinkedList<String> t;
    private LinkedList<String> u;
    private int v;

    public d(Activity activity, b bVar, e eVar, String str, boolean z) {
        this.b = new WeakReference<>(activity);
        this.c = bVar;
        this.g = eVar;
        this.i = str;
        this.s = z;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            if (z) {
                long j2 = this.p.getLong("Timeout" + str, -1);
                if (j2 < 0) {
                    this.m = 5000;
                } else {
                    this.m = j2;
                }
            } else {
                this.m = 60000;
            }
        }
        this.r = new x(this);
        this.t = new LinkedList<>();
        this.u = new LinkedList<>();
        a();
        AdUtil.g(activity.getApplicationContext());
    }

    private synchronized boolean w() {
        return this.e != null;
    }

    private synchronized void x() {
        Activity activity = this.b.get();
        if (activity == null) {
            com.google.ads.util.b.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator<String> it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new p(it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    public final synchronized void a() {
        Activity d2 = d();
        if (d2 == null) {
            com.google.ads.util.b.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new b(d2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof a) {
                this.k = new n(this, g.b, true, false);
            } else {
                this.k = new n(this, g.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.e = null;
        if (this.c instanceof com.google.ads.d) {
            if (errorCode == AdRequest.ErrorCode.NO_FILL) {
                this.h.n();
            } else if (errorCode == AdRequest.ErrorCode.NETWORK_ERROR) {
                this.h.l();
            }
        }
        com.google.ads.util.b.c("onFailedToReceiveAd(" + errorCode + ")");
        if (this.d != null) {
            this.d.a(this.c, errorCode);
        }
    }

    public final synchronized void a(AdRequest adRequest) {
        if (w()) {
            com.google.ads.util.b.e("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.c()) {
            com.google.ads.util.b.e("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity d2 = d();
            if (d2 == null) {
                com.google.ads.util.b.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(d2.getApplicationContext()) && AdUtil.b(d2.getApplicationContext())) {
                this.n = false;
                this.t.clear();
                this.f = adRequest;
                this.e = new c(this);
                this.e.a(adRequest);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        com.google.ads.util.b.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList<String> linkedList) {
        Iterator<String> it = linkedList.iterator();
        while (it.hasNext()) {
            com.google.ads.util.b.a("Adding a click tracking URL: " + it.next());
        }
        this.u = linkedList;
    }

    public final synchronized void b() {
        if (this.o) {
            com.google.ads.util.b.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            com.google.ads.util.b.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void c() {
        if (!(this.c instanceof a)) {
            com.google.ads.util.b.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            com.google.ads.util.b.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            com.google.ads.util.b.a("Refreshing is already enabled.");
        }
    }

    public final Activity d() {
        return this.b.get();
    }

    public final b e() {
        return this.c;
    }

    public final synchronized c f() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        return this.i;
    }

    public final synchronized b h() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized n i() {
        return this.k;
    }

    public final e j() {
        return this.g;
    }

    public final a k() {
        return this.h;
    }

    public final synchronized int l() {
        return this.v;
    }

    public final long m() {
        return this.m;
    }

    public final synchronized boolean n() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void o() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof a) {
            t();
        }
        com.google.ads.util.b.c("onReceiveAd()");
        if (this.d != null) {
            this.d.a(this.c);
        }
    }

    public final synchronized void p() {
        this.h.o();
        com.google.ads.util.b.c("onDismissScreen()");
        if (this.d != null) {
            this.d.c(this.c);
        }
    }

    public final synchronized void q() {
        com.google.ads.util.b.c("onPresentScreen()");
        if (this.d != null) {
            this.d.b(this.c);
        }
    }

    public final synchronized void r() {
        com.google.ads.util.b.c("onLeaveApplication()");
        if (this.d != null) {
            this.d.d(this.c);
        }
    }

    public final void s() {
        this.h.b();
        x();
    }

    public final synchronized void t() {
        Activity activity = this.b.get();
        if (activity == null) {
            com.google.ads.util.b.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator<String> it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new p(it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean u() {
        return !this.u.isEmpty();
    }

    public final synchronized void v() {
        if (this.f == null) {
            com.google.ads.util.b.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof a) {
            if (!((a) this.c).isShown() || !AdUtil.d()) {
                com.google.ads.util.b.a("Not refreshing because the ad is not visible.");
            } else {
                com.google.ads.util.b.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            com.google.ads.util.b.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }
}
