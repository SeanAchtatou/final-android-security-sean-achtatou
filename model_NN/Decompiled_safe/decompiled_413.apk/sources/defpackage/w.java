package defpackage;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: w  reason: default package */
public final class w implements o {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("url");
        b.c("Received ad url: <\"url\": \"" + str + "\", \"afmaNotifyDt\": \"" + hashMap.get("afma_notify_dt") + "\">");
        c f = dVar.f();
        if (f != null) {
            f.b(str);
        }
    }
}
