package defpackage;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

/* renamed from: j  reason: default package */
public final class j implements o {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        if (webView instanceof b) {
            ((b) webView).a();
        } else {
            b.b("Trying to close WebView that isn't an AdWebView");
        }
    }
}
