package com.flurry.android;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.LinearLayout;

/* renamed from: com.flurry.android.g  reason: case insensitive filesystem */
final class C0032g extends LinearLayout {
    public C0032g(CatalogActivity catalogActivity, Context context) {
        super(context);
        setBackgroundColor(-1);
        C0026a i = catalogActivity.e.i();
        if (i != null) {
            ImageView imageView = new ImageView(context);
            imageView.setId(10000);
            byte[] bArr = i.e;
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
            C0029d.a(context, imageView, C0029d.a(context, i.b), C0029d.a(context, i.c));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(0, 0, 0, -3);
            setGravity(3);
            addView(imageView, layoutParams);
        }
    }
}
