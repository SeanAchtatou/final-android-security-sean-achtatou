package com.flurry.android;

/* renamed from: com.flurry.android.e  reason: case insensitive filesystem */
final class C0030e {
    final byte a;
    final long b;

    C0030e(byte b2, long j) {
        this.a = b2;
        this.b = j;
    }

    public final String toString() {
        return "[" + this.b + "] " + ((int) this.a);
    }
}
