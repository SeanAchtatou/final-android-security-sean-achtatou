package com.flurry.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public final class u implements LocationListener {
    private static String X;
    private static volatile String a = null;
    private static volatile String b = "http://data.flurry.com/aap.do";
    private static volatile String c = "https://data.flurry.com/aap.do";
    private static volatile String d = null;
    private static volatile String e = "http://ad.flurry.com/getCanvas.do";
    private static volatile String f = null;
    private static volatile String g = "http://ad.flurry.com/getAndroidApp.do";
    /* access modifiers changed from: private */
    public static final u h = new u();
    /* access modifiers changed from: private */
    public static long i = 10000;
    private static boolean j = true;
    private static boolean k = false;
    private static boolean l = true;
    private static Criteria m = null;
    /* access modifiers changed from: private */
    public static boolean n = false;
    private static D o = new D();
    private LocationManager A;
    private String B;
    private boolean C;
    private long D;
    /* access modifiers changed from: private */
    public List E = new ArrayList();
    private long F;
    private long G;
    private long H;
    private String I = "";
    private String J = "";
    private byte K = -1;
    private String L;
    private byte M = -1;
    private Long N;
    private int O;
    private Location P;
    private Map Q;
    private List R;
    private boolean S;
    private int T;
    private List U;
    private int V;
    /* access modifiers changed from: private */
    public C0036k W;
    /* access modifiers changed from: private */
    public final Handler p;
    private File q = null;
    private volatile boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    private long t;
    private Map u = new WeakHashMap();
    private String v;
    private String w;
    private String x;
    private boolean y = true;
    private List z;

    /* JADX WARNING: Removed duplicated region for block: B:18:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.flurry.android.u r5, android.content.Context r6) {
        /*
            java.io.File r0 = r5.q
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0037
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0046, all -> 0x0055 }
            java.io.File r2 = r5.q     // Catch:{ Throwable -> 0x0046, all -> 0x0055 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0046, all -> 0x0055 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0046, all -> 0x0055 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0046, all -> 0x0055 }
            int r0 = r2.readUnsignedShort()     // Catch:{ Throwable -> 0x006b, all -> 0x0066 }
            r1 = 46586(0xb5fa, float:6.5281E-41)
            if (r0 != r1) goto L_0x0021
            r5.b(r2)     // Catch:{ Throwable -> 0x006b, all -> 0x0066 }
        L_0x0021:
            com.flurry.android.C0029d.a(r2)
        L_0x0024:
            boolean r0 = r5.s     // Catch:{ Throwable -> 0x005d }
            if (r0 != 0) goto L_0x0037
            java.io.File r0 = r5.q     // Catch:{ Throwable -> 0x005d }
            boolean r0 = r0.delete()     // Catch:{ Throwable -> 0x005d }
            if (r0 != 0) goto L_0x0037
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Cannot delete persistence file"
            com.flurry.android.K.b(r0, r1)     // Catch:{ Throwable -> 0x005d }
        L_0x0037:
            boolean r0 = r5.s
            if (r0 != 0) goto L_0x0045
            r0 = 0
            r5.C = r0
            long r0 = r5.F
            r5.D = r0
            r0 = 1
            r5.s = r0
        L_0x0045:
            return
        L_0x0046:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x004a:
            java.lang.String r2 = "FlurryAgent"
            java.lang.String r3 = ""
            com.flurry.android.K.b(r2, r3, r0)     // Catch:{ all -> 0x0069 }
            com.flurry.android.C0029d.a(r1)
            goto L_0x0024
        L_0x0055:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0059:
            com.flurry.android.C0029d.a(r1)
            throw r0
        L_0x005d:
            r0 = move-exception
            java.lang.String r1 = "FlurryAgent"
            java.lang.String r2 = ""
            com.flurry.android.K.b(r1, r2, r0)
            goto L_0x0037
        L_0x0066:
            r0 = move-exception
            r1 = r2
            goto L_0x0059
        L_0x0069:
            r0 = move-exception
            goto L_0x0059
        L_0x006b:
            r0 = move-exception
            r1 = r2
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.u.a(com.flurry.android.u, android.content.Context):void");
    }

    static /* synthetic */ void a(u uVar, Context context, boolean z2) {
        Location location = null;
        if (z2) {
            try {
                location = uVar.c(context);
            } catch (Throwable th) {
                K.b("FlurryAgent", "", th);
                return;
            }
        }
        synchronized (uVar) {
            uVar.P = location;
        }
        if (n) {
            uVar.W.a();
        }
        uVar.b(true);
    }

    static /* synthetic */ void b(u uVar, Context context) {
        boolean z2;
        try {
            synchronized (uVar) {
                z2 = !uVar.r && SystemClock.elapsedRealtime() - uVar.t > i && uVar.E.size() > 0;
            }
            if (z2) {
                uVar.b(false);
            }
        } catch (Throwable th) {
            K.b("FlurryAgent", "", th);
        }
    }

    static /* synthetic */ void c(u uVar) {
        DataOutputStream dataOutputStream;
        IOException e2;
        Throwable th;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(1);
                dataOutputStream2.writeUTF(uVar.x);
                dataOutputStream2.writeLong(uVar.F);
                dataOutputStream2.writeLong(uVar.H);
                dataOutputStream2.writeLong(0);
                dataOutputStream2.writeUTF(uVar.I);
                dataOutputStream2.writeUTF(uVar.J);
                dataOutputStream2.writeByte(uVar.K);
                dataOutputStream2.writeUTF(uVar.L);
                if (uVar.P == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeDouble(uVar.P.getLatitude());
                    dataOutputStream2.writeDouble(uVar.P.getLongitude());
                    dataOutputStream2.writeFloat(uVar.P.getAccuracy());
                }
                dataOutputStream2.writeInt(uVar.V);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(uVar.M);
                if (uVar.N == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeLong(uVar.N.longValue());
                }
                dataOutputStream2.writeShort(uVar.Q.size());
                for (Map.Entry entry : uVar.Q.entrySet()) {
                    dataOutputStream2.writeUTF((String) entry.getKey());
                    dataOutputStream2.writeInt(((C0031f) entry.getValue()).a);
                }
                dataOutputStream2.writeShort(uVar.R.size());
                for (C0033h a2 : uVar.R) {
                    dataOutputStream2.write(a2.a());
                }
                dataOutputStream2.writeBoolean(uVar.S);
                dataOutputStream2.writeInt(uVar.O);
                dataOutputStream2.writeShort(uVar.U.size());
                for (z zVar : uVar.U) {
                    dataOutputStream2.writeLong(zVar.a);
                    dataOutputStream2.writeUTF(zVar.b);
                    dataOutputStream2.writeUTF(zVar.c);
                    dataOutputStream2.writeUTF(zVar.d);
                }
                if (n) {
                    List<J> e3 = uVar.W.e();
                    dataOutputStream2.writeShort(e3.size());
                    for (J a3 : e3) {
                        a3.a(dataOutputStream2);
                    }
                } else {
                    dataOutputStream2.writeShort(0);
                }
                uVar.E.add(byteArrayOutputStream.toByteArray());
                C0029d.a(dataOutputStream2);
            } catch (IOException e4) {
                e2 = e4;
                dataOutputStream = dataOutputStream2;
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream = dataOutputStream2;
                C0029d.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e5) {
            IOException iOException = e5;
            dataOutputStream = null;
            e2 = iOException;
            try {
                K.b("FlurryAgent", "", e2);
                C0029d.a(dataOutputStream);
            } catch (Throwable th3) {
                th = th3;
                C0029d.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            dataOutputStream = null;
            th = th5;
            C0029d.a(dataOutputStream);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.u.a(android.content.Context, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.u.a(android.content.Context, java.lang.String):void
      com.flurry.android.u.a(com.flurry.android.u, android.content.Context):void
      com.flurry.android.u.a(java.lang.String, java.util.Map):void
      com.flurry.android.u.a(byte[], java.lang.String):boolean
      com.flurry.android.u.a(android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Throwable th) {
        th.printStackTrace();
        String str = "";
        StackTraceElement[] stackTrace = th.getStackTrace();
        if (stackTrace != null && stackTrace.length > 0) {
            StackTraceElement stackTraceElement = stackTrace[0];
            StringBuilder sb = new StringBuilder();
            sb.append(stackTraceElement.getClassName()).append(".").append(stackTraceElement.getMethodName()).append(":").append(stackTraceElement.getLineNumber());
            if (th.getMessage() != null) {
                sb.append(" (" + th.getMessage() + ")");
            }
            str = sb.toString();
        } else if (th.getMessage() != null) {
            str = th.getMessage();
        }
        a("uncaught", str, th.getClass().toString());
        this.u.clear();
        a((Context) null, true);
    }

    private u() {
        HandlerThread handlerThread = new HandlerThread("FlurryAgent");
        handlerThread.start();
        this.p = new Handler(handlerThread.getLooper());
    }

    public static void a(Context context, String str) {
        if (context == null) {
            throw new NullPointerException("Null context");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Api key not specified");
        } else {
            try {
                h.b(context, str);
            } catch (Throwable th) {
                K.b("FlurryAgent", "", th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.u.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.flurry.android.u.a(android.content.Context, java.lang.String):void
      com.flurry.android.u.a(com.flurry.android.u, android.content.Context):void
      com.flurry.android.u.a(java.lang.String, java.util.Map):void
      com.flurry.android.u.a(byte[], java.lang.String):boolean
      com.flurry.android.u.a(android.content.Context, boolean):void */
    public static void a(Context context) {
        if (context == null) {
            throw new NullPointerException("Null context");
        }
        try {
            h.a(context, false);
        } catch (Throwable th) {
            K.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.u.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.u.a(com.flurry.android.u, android.content.Context, boolean):void
      com.flurry.android.u.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.u.a(java.lang.String, java.util.Map, boolean):void */
    public static void a(String str) {
        try {
            h.a(str, (Map) null, false);
        } catch (Throwable th) {
            K.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.u.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      com.flurry.android.u.a(com.flurry.android.u, android.content.Context, boolean):void
      com.flurry.android.u.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.u.a(java.lang.String, java.util.Map, boolean):void */
    public static void a(String str, Map map) {
        try {
            h.a(str, map, false);
        } catch (Throwable th) {
            K.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void a(String str, String str2, String str3) {
        try {
            h.b(str, str2, str3);
        } catch (Throwable th) {
            K.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.u.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      com.flurry.android.u.a(com.flurry.android.u, android.content.Context, boolean):void
      com.flurry.android.u.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.u.a(java.lang.String, java.util.Map, boolean):void */
    public static void b(String str, Map map) {
        try {
            h.a(str, map, false);
        } catch (Throwable th) {
            K.b("FlurryAgent", "", th);
        }
    }

    static C0036k a() {
        return h.W;
    }

    private synchronized void b(Context context, String str) {
        String str2;
        K.a("FlurryAgent", "startSession called");
        if (this.v != null && !this.v.equals(str)) {
            K.b("FlurryAgent", "onStartSession called with different api keys: " + this.v + " and " + str);
        }
        if (((Context) this.u.put(context, context)) != null) {
            K.d("FlurryAgent", "onStartSession called with duplicate context, use a specific Activity or Service as context instead of using a global context");
        }
        if (!this.r) {
            K.a("FlurryAgent", "Initializing Flurry session");
            this.v = str;
            this.q = context.getFileStreamPath(".flurryagent." + Integer.toString(this.v.hashCode(), 16));
            if (l) {
                Thread.setDefaultUncaughtExceptionHandler(new C0027b());
            }
            Context applicationContext = context.getApplicationContext();
            if (this.x == null) {
                this.x = b(applicationContext);
            }
            String packageName = applicationContext.getPackageName();
            if (this.w != null && !this.w.equals(packageName)) {
                K.b("FlurryAgent", "onStartSession called from different application packages: " + this.w + " and " + packageName);
            }
            this.w = packageName;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (elapsedRealtime - this.t > i) {
                K.a("FlurryAgent", "Starting new session");
                String string = Settings.System.getString(context.getContentResolver(), "android_id");
                if (string == null || string.length() <= 0 || string.equals("null") || string.equals("9774d56d682e549c")) {
                    str2 = "ID" + Long.toString(Double.doubleToLongBits(Math.random()) + (37 * (System.nanoTime() + ((long) (this.v.hashCode() * 37)))), 16);
                } else {
                    str2 = "AND" + string;
                }
                this.B = str2;
                this.F = System.currentTimeMillis();
                this.G = elapsedRealtime;
                this.H = -1;
                this.L = "";
                this.O = 0;
                this.P = null;
                this.J = TimeZone.getDefault().getID();
                this.I = Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry();
                this.Q = new HashMap();
                this.R = new ArrayList();
                this.S = true;
                this.U = new ArrayList();
                this.T = 0;
                this.V = 0;
                if (n && this.W == null) {
                    K.a("FlurryAgent", "Initializing AppCircle");
                    s sVar = new s();
                    sVar.a = this.v;
                    sVar.b = this.B;
                    sVar.c = this.D;
                    sVar.d = this.F;
                    sVar.e = this.G;
                    sVar.f = d != null ? d : e;
                    sVar.g = b();
                    sVar.h = this.p;
                    this.W = new C0036k(context, sVar);
                    if (X != null) {
                        C0036k.a(X);
                    }
                    K.a("FlurryAgent", "AppCircle initialized");
                }
                a(new x(this, applicationContext, this.y));
            } else {
                K.a("FlurryAgent", "Continuing previous session");
                a(new r(this));
            }
            this.r = true;
        }
    }

    private synchronized void a(Context context, boolean z2) {
        if (context != null) {
            if (((Context) this.u.remove(context)) == null) {
                K.d("FlurryAgent", "onEndSession called without context from corresponding onStartSession");
            }
        }
        if (this.r && this.u.isEmpty()) {
            K.a("FlurryAgent", "Ending session");
            g();
            Context applicationContext = context == null ? null : context.getApplicationContext();
            if (context != null) {
                String packageName = applicationContext.getPackageName();
                if (!this.w.equals(packageName)) {
                    K.b("FlurryAgent", "onEndSession called from different application package, expected: " + this.w + " actual: " + packageName);
                }
            }
            this.r = false;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.t = elapsedRealtime;
            this.H = elapsedRealtime - this.G;
            a(new v(this, z2, applicationContext));
        }
    }

    private void a(Runnable runnable) {
        this.p.post(runnable);
    }

    private synchronized void a(String str, Map map, boolean z2) {
        Map map2;
        if (this.R == null) {
            K.b("FlurryAgent", "onEvent called before onStartSession.  Event: " + str);
        } else {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.G;
            String a2 = C0029d.a(str, 128);
            if (a2.length() != 0) {
                C0031f fVar = (C0031f) this.Q.get(a2);
                if (fVar != null) {
                    fVar.a++;
                } else if (this.Q.size() < 100) {
                    C0031f fVar2 = new C0031f();
                    fVar2.a = 1;
                    this.Q.put(a2, fVar2);
                } else if (K.a("FlurryAgent")) {
                    K.a("FlurryAgent", "MaxEventIds exceeded: " + a2);
                }
                if (!j || this.R.size() >= 100 || this.T >= 8000) {
                    this.S = false;
                } else {
                    if (map == null) {
                        map2 = Collections.emptyMap();
                    } else {
                        map2 = map;
                    }
                    if (map2.size() <= 10) {
                        C0033h hVar = new C0033h(a2, map2, elapsedRealtime, z2);
                        if (hVar.a().length + this.T <= 8000) {
                            this.R.add(hVar);
                            this.T = hVar.a().length + this.T;
                        } else {
                            this.T = 8000;
                            this.S = false;
                        }
                    } else if (K.a("FlurryAgent")) {
                        K.a("FlurryAgent", "MaxEventParams exceeded: " + map2.size());
                    }
                }
            }
        }
    }

    private synchronized void b(String str, String str2, String str3) {
        if (this.U == null) {
            K.b("FlurryAgent", "onError called before onStartSession.  Error: " + str);
        } else {
            this.O++;
            if (this.U.size() < 10) {
                z zVar = new z();
                zVar.a = System.currentTimeMillis();
                zVar.b = C0029d.a(str, 128);
                zVar.c = C0029d.a(str2, 512);
                zVar.d = C0029d.a(str3, 128);
                this.U.add(zVar);
            }
        }
    }

    private synchronized byte[] a(boolean z2) {
        DataOutputStream dataOutputStream;
        byte[] bArr;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(15);
                if (!n || !z2) {
                    dataOutputStream2.writeShort(0);
                } else {
                    dataOutputStream2.writeShort(1);
                }
                if (n) {
                    dataOutputStream2.writeLong(this.W.c());
                    Set<Long> d2 = this.W.d();
                    dataOutputStream2.writeShort(d2.size());
                    for (Long longValue : d2) {
                        long longValue2 = longValue.longValue();
                        dataOutputStream2.writeByte(1);
                        dataOutputStream2.writeLong(longValue2);
                    }
                } else {
                    dataOutputStream2.writeLong(0);
                    dataOutputStream2.writeShort(0);
                }
                dataOutputStream2.writeShort(3);
                dataOutputStream2.writeShort(112);
                dataOutputStream2.writeLong(System.currentTimeMillis());
                dataOutputStream2.writeUTF(this.v);
                dataOutputStream2.writeUTF(this.x);
                dataOutputStream2.writeShort(0);
                dataOutputStream2.writeUTF(this.B);
                dataOutputStream2.writeLong(this.D);
                dataOutputStream2.writeLong(this.F);
                dataOutputStream2.writeShort(6);
                dataOutputStream2.writeUTF("device.model");
                dataOutputStream2.writeUTF(Build.MODEL);
                dataOutputStream2.writeUTF("build.brand");
                dataOutputStream2.writeUTF(Build.BRAND);
                dataOutputStream2.writeUTF("build.id");
                dataOutputStream2.writeUTF(Build.ID);
                dataOutputStream2.writeUTF("version.release");
                dataOutputStream2.writeUTF(Build.VERSION.RELEASE);
                dataOutputStream2.writeUTF("build.device");
                dataOutputStream2.writeUTF(Build.DEVICE);
                dataOutputStream2.writeUTF("build.product");
                dataOutputStream2.writeUTF(Build.PRODUCT);
                int size = this.E.size();
                dataOutputStream2.writeShort(size);
                for (int i2 = 0; i2 < size; i2++) {
                    dataOutputStream2.write((byte[]) this.E.get(i2));
                }
                this.z = new ArrayList(this.E);
                dataOutputStream2.close();
                bArr = byteArrayOutputStream.toByteArray();
                C0029d.a(dataOutputStream2);
            } catch (IOException e2) {
                e = e2;
                dataOutputStream = dataOutputStream2;
            } catch (Throwable th) {
                th = th;
                dataOutputStream = dataOutputStream2;
                C0029d.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            dataOutputStream = null;
            try {
                K.b("FlurryAgent", "", e);
                C0029d.a(dataOutputStream);
                bArr = null;
                return bArr;
            } catch (Throwable th2) {
                th = th2;
                C0029d.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            C0029d.a(dataOutputStream);
            throw th;
        }
        return bArr;
    }

    static String b() {
        return f != null ? f : g;
    }

    private boolean a(byte[] bArr) {
        boolean z2;
        String str = a != null ? a : b;
        if (str == null) {
            return false;
        }
        try {
            z2 = a(bArr, str);
        } catch (Exception e2) {
            K.a("FlurryAgent", "Sending report exception: " + e2.getMessage());
            z2 = false;
        }
        if (z2 || a != null) {
        }
        return z2;
    }

    private boolean a(byte[] bArr, String str) {
        if ("local".equals(str)) {
            return true;
        }
        K.a("FlurryAgent", "Sending report to: " + str);
        boolean z2 = false;
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bArr);
        byteArrayEntity.setContentType("application/octet-stream");
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(byteArrayEntity);
        HttpResponse execute = new DefaultHttpClient().execute(httpPost);
        int statusCode = execute.getStatusLine().getStatusCode();
        synchronized (this) {
            if (statusCode == 200) {
                K.a("FlurryAgent", "Report successful");
                this.C = true;
                this.E.removeAll(this.z);
                HttpEntity entity = execute.getEntity();
                K.a("FlurryAgent", "Processing report response");
                if (entity == null || entity.getContentLength() == 0) {
                    z2 = true;
                } else {
                    try {
                        a(new DataInputStream(entity.getContent()));
                        entity.consumeContent();
                        z2 = true;
                    } catch (Throwable th) {
                        entity.consumeContent();
                        throw th;
                    }
                }
            } else {
                K.a("FlurryAgent", "Report failed. HTTP response: " + statusCode);
            }
        }
        this.z = null;
        return z2;
    }

    /* JADX WARN: Type inference failed for: r0v12, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.io.DataInputStream r15) {
        /*
            r14 = this;
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.util.HashMap r6 = new java.util.HashMap
            r6.<init>()
        L_0x001e:
            int r7 = r15.readUnsignedShort()
            int r0 = r15.readInt()
            switch(r7) {
                case 258: goto L_0x005f;
                case 259: goto L_0x0063;
                case 260: goto L_0x0029;
                case 261: goto L_0x0029;
                case 262: goto L_0x0082;
                case 263: goto L_0x00b4;
                case 264: goto L_0x0044;
                case 265: goto L_0x0029;
                case 266: goto L_0x00cd;
                case 267: goto L_0x0029;
                case 268: goto L_0x0104;
                case 269: goto L_0x014b;
                case 270: goto L_0x00c8;
                case 271: goto L_0x00e5;
                case 272: goto L_0x0121;
                case 273: goto L_0x0150;
                default: goto L_0x0029;
            }
        L_0x0029:
            java.lang.String r8 = "FlurryAgent"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Unknown chunkType: "
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r9 = r9.toString()
            com.flurry.android.K.a(r8, r9)
            r15.skipBytes(r0)
        L_0x0044:
            r0 = 264(0x108, float:3.7E-43)
            if (r7 != r0) goto L_0x001e
            boolean r0 = com.flurry.android.u.n
            if (r0 == 0) goto L_0x005e
            boolean r0 = r1.isEmpty()
            if (r0 == 0) goto L_0x0059
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r7 = "No ads from server"
            com.flurry.android.K.a(r0, r7)
        L_0x0059:
            com.flurry.android.k r0 = r14.W
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x005e:
            return
        L_0x005f:
            r15.readInt()
            goto L_0x0044
        L_0x0063:
            byte r0 = r15.readByte()
            int r8 = r15.readUnsignedShort()
            com.flurry.android.n[] r9 = new com.flurry.android.C0039n[r8]
            r10 = 0
        L_0x006e:
            if (r10 >= r8) goto L_0x007a
            com.flurry.android.n r11 = new com.flurry.android.n
            r11.<init>(r15)
            r9[r10] = r11
            int r10 = r10 + 1
            goto L_0x006e
        L_0x007a:
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            r1.put(r0, r9)
            goto L_0x0044
        L_0x0082:
            int r0 = r15.readUnsignedShort()
            r8 = 0
        L_0x0087:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.a r9 = new com.flurry.android.a
            r9.<init>(r15)
            long r10 = r9.a
            java.lang.Long r10 = java.lang.Long.valueOf(r10)
            r4.put(r10, r9)
            java.lang.String r10 = "FlurryAgent"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Parsed image: "
            java.lang.StringBuilder r11 = r11.append(r12)
            long r12 = r9.a
            java.lang.StringBuilder r9 = r11.append(r12)
            java.lang.String r9 = r9.toString()
            com.flurry.android.K.a(r10, r9)
            int r8 = r8 + 1
            goto L_0x0087
        L_0x00b4:
            int r0 = r15.readInt()
            r8 = 0
        L_0x00b9:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.w r9 = new com.flurry.android.w
            r9.<init>(r15)
            java.lang.String r10 = r9.a
            r2.put(r10, r9)
            int r8 = r8 + 1
            goto L_0x00b9
        L_0x00c8:
            r15.skipBytes(r0)
            goto L_0x0044
        L_0x00cd:
            byte r0 = r15.readByte()
            r8 = 0
        L_0x00d2:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.t r9 = new com.flurry.android.t
            r9.<init>(r15)
            byte r10 = r9.a
            java.lang.Byte r10 = java.lang.Byte.valueOf(r10)
            r3.put(r10, r9)
            int r8 = r8 + 1
            goto L_0x00d2
        L_0x00e5:
            byte r8 = r15.readByte()
            r0 = 0
            r9 = r0
        L_0x00eb:
            if (r9 >= r8) goto L_0x0044
            byte r0 = r15.readByte()
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            java.lang.Object r0 = r3.get(r0)
            com.flurry.android.t r0 = (com.flurry.android.t) r0
            if (r0 == 0) goto L_0x0100
            r0.a(r15)
        L_0x0100:
            int r0 = r9 + 1
            r9 = r0
            goto L_0x00eb
        L_0x0104:
            int r0 = r15.readInt()
            r8 = 0
        L_0x0109:
            if (r8 >= r0) goto L_0x0044
            long r9 = r15.readLong()
            short r11 = r15.readShort()
            java.lang.Short r11 = java.lang.Short.valueOf(r11)
            java.lang.Long r9 = java.lang.Long.valueOf(r9)
            r6.put(r11, r9)
            int r8 = r8 + 1
            goto L_0x0109
        L_0x0121:
            long r8 = r15.readLong()
            java.lang.Long r0 = java.lang.Long.valueOf(r8)
            java.lang.Object r0 = r5.get(r0)
            com.flurry.android.F r0 = (com.flurry.android.F) r0
            if (r0 != 0) goto L_0x0136
            com.flurry.android.F r0 = new com.flurry.android.F
            r0.<init>()
        L_0x0136:
            java.lang.String r10 = r15.readUTF()
            r0.a = r10
            int r10 = r15.readInt()
            r0.c = r10
            java.lang.Long r8 = java.lang.Long.valueOf(r8)
            r5.put(r8, r0)
            goto L_0x0044
        L_0x014b:
            r15.skipBytes(r0)
            goto L_0x0044
        L_0x0150:
            r15.skipBytes(r0)
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.u.a(java.io.DataInputStream):void");
    }

    private void b(boolean z2) {
        try {
            byte[] a2 = a(z2);
            if (a2 != null && a(a2)) {
                K.a("FlurryAgent", "Done sending " + (this.r ? "initial " : "") + "agent report");
                f();
            }
        } catch (IOException e2) {
            K.a("FlurryAgent", "", e2);
        } catch (Throwable th) {
            K.b("FlurryAgent", "", th);
        }
    }

    private synchronized void b(DataInputStream dataInputStream) {
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        if (readUnsignedShort > 2) {
            throw new IOException("Unknown agent file version: " + readUnsignedShort);
        } else if (readUnsignedShort >= 2) {
            String readUTF = dataInputStream.readUTF();
            if (readUTF.equals(this.v)) {
                this.B = dataInputStream.readUTF();
                this.C = dataInputStream.readBoolean();
                this.D = dataInputStream.readLong();
                while (true) {
                    int readUnsignedShort2 = dataInputStream.readUnsignedShort();
                    if (readUnsignedShort2 == 0) {
                        break;
                    }
                    byte[] bArr = new byte[readUnsignedShort2];
                    dataInputStream.readFully(bArr);
                    this.E.add(0, bArr);
                }
                this.s = true;
            } else {
                K.a("FlurryAgent", "Api keys do not match, old: " + readUTF + ", new: " + this.v);
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void f() {
        DataOutputStream dataOutputStream;
        try {
            File parentFile = this.q.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.q));
                try {
                    dataOutputStream.writeShort(46586);
                    dataOutputStream.writeShort(2);
                    dataOutputStream.writeUTF(this.v);
                    dataOutputStream.writeUTF(this.B);
                    dataOutputStream.writeBoolean(this.C);
                    dataOutputStream.writeLong(this.D);
                    for (int size = this.E.size() - 1; size >= 0; size--) {
                        byte[] bArr = (byte[]) this.E.get(size);
                        int length = bArr.length;
                        if (length + 2 + dataOutputStream.size() > 50000) {
                            break;
                        }
                        dataOutputStream.writeShort(length);
                        dataOutputStream.write(bArr);
                    }
                    dataOutputStream.writeShort(0);
                    C0029d.a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                    try {
                        K.b("FlurryAgent", "", th);
                        C0029d.a(dataOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        C0029d.a(dataOutputStream);
                        throw th;
                    }
                }
            } else {
                K.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                C0029d.a((Closeable) null);
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            C0029d.a(dataOutputStream);
            throw th;
        }
    }

    private static String b(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo.versionName != null) {
                return packageInfo.versionName;
            }
            if (packageInfo.versionCode != 0) {
                return Integer.toString(packageInfo.versionCode);
            }
            return "Unknown";
        } catch (Throwable th) {
            K.b("FlurryAgent", "", th);
        }
    }

    private Location c(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            synchronized (this) {
                if (this.A == null) {
                    this.A = locationManager;
                } else {
                    locationManager = this.A;
                }
            }
            Criteria criteria = m;
            if (criteria == null) {
                criteria = new Criteria();
            }
            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (bestProvider != null) {
                locationManager.requestLocationUpdates(bestProvider, 0, 0.0f, this, Looper.getMainLooper());
                return locationManager.getLastKnownLocation(bestProvider);
            }
        }
        return null;
    }

    private synchronized void g() {
        if (this.A != null) {
            this.A.removeUpdates(this);
        }
    }

    public final synchronized void onLocationChanged(Location location) {
        try {
            this.P = location;
            g();
        } catch (Throwable th) {
            K.b("FlurryAgent", "", th);
        }
        return;
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i2, Bundle bundle) {
    }
}
