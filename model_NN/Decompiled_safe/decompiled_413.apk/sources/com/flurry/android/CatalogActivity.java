package com.flurry.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.List;

public class CatalogActivity extends Activity implements View.OnClickListener {
    private static volatile String a = "<html><body><table height='100%' width='100%' border='0'><tr><td style='vertical-align:middle;text-align:center'>No recommendations available<p><button type='input' onClick='activity.finish()'>Back</button></td></tr></table></body></html>";
    private WebView b;
    private C0040o c;
    private List d = new ArrayList();
    /* access modifiers changed from: private */
    public C0036k e;
    /* access modifiers changed from: private */
    public J f;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Long valueOf;
        setTheme(16973839);
        super.onCreate(bundle);
        this.e = u.a();
        Intent intent = getIntent();
        if (!(intent.getExtras() == null || (valueOf = Long.valueOf(intent.getExtras().getLong("o"))) == null)) {
            this.f = this.e.b(valueOf.longValue());
        }
        y yVar = new y(this, this);
        yVar.setId(1);
        yVar.setBackgroundColor(-16777216);
        this.b = new WebView(this);
        this.b.setId(2);
        this.b.setScrollBarStyle(0);
        this.b.setBackgroundColor(-1);
        if (this.f != null) {
            this.b.setWebViewClient(new C0028c(this));
        }
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.addJavascriptInterface(this, "activity");
        this.c = new C0040o(this, this);
        this.c.setId(3);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10, yVar.getId());
        relativeLayout.addView(yVar, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(3, yVar.getId());
        layoutParams2.addRule(2, this.c.getId());
        relativeLayout.addView(this.b, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(12, yVar.getId());
        relativeLayout.addView(this.c, layoutParams3);
        Bundle extras = getIntent().getExtras();
        String string = extras == null ? null : extras.getString("u");
        if (string == null) {
            this.b.loadDataWithBaseURL(null, a, "text/html", "utf-8", null);
        } else {
            this.b.loadUrl(string);
        }
        setContentView(relativeLayout);
    }

    public void finish() {
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.e.f();
        super.onDestroy();
    }

    public void onClick(View view) {
        if (view instanceof q) {
            C0041p pVar = new C0041p();
            pVar.a = this.f;
            pVar.b = this.b.getUrl();
            pVar.c = new ArrayList(this.c.b());
            this.d.add(pVar);
            if (this.d.size() > 5) {
                this.d.remove(0);
            }
            C0041p pVar2 = new C0041p();
            q qVar = (q) view;
            String b2 = qVar.b(this.e.g());
            this.f = qVar.a();
            pVar2.a = qVar.a();
            pVar2.a.a(new C0030e((byte) 4, this.e.h()));
            pVar2.b = b2;
            pVar2.c = this.c.a(view.getContext());
            a(pVar2);
        } else if (view.getId() == 10000) {
            finish();
        } else if (view.getId() == 10002) {
            this.c.a();
        } else if (this.d.isEmpty()) {
            finish();
        } else {
            a((C0041p) this.d.remove(this.d.size() - 1));
        }
    }

    private void a(C0041p pVar) {
        try {
            this.b.loadUrl(pVar.b);
            this.c.a(pVar.c);
        } catch (Exception e2) {
            K.a("FlurryAgent", "Error loading url: " + pVar.b);
        }
    }
}
