package com.flurry.android;

import java.io.DataInput;

final class w extends L {
    String a;
    byte b;
    byte c;
    t d;

    w() {
    }

    w(DataInput dataInput) {
        this.a = dataInput.readUTF();
        this.b = dataInput.readByte();
        this.c = dataInput.readByte();
    }

    public final String toString() {
        return "{name: " + this.a + ", blockId: " + ((int) this.b) + ", themeId: " + ((int) this.c);
    }
}
