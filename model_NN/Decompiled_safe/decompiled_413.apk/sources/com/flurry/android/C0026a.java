package com.flurry.android;

import java.io.DataInput;

/* renamed from: com.flurry.android.a  reason: case insensitive filesystem */
public final class C0026a extends L {
    long a;
    int b;
    int c;
    String d;
    byte[] e;

    C0026a() {
    }

    C0026a(DataInput dataInput) {
        a(dataInput);
    }

    /* access modifiers changed from: package-private */
    public final void a(DataInput dataInput) {
        this.a = dataInput.readLong();
        this.b = dataInput.readInt();
        this.c = dataInput.readInt();
        this.d = dataInput.readUTF();
        this.e = new byte[dataInput.readInt()];
        dataInput.readFully(this.e);
    }
}
