package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/* renamed from: com.flurry.android.k  reason: case insensitive filesystem */
final class C0036k implements View.OnClickListener {
    static String a = "FlurryAgent";
    static String b = "";
    private static volatile String c = "market://";
    private static volatile String d = "market://details?id=";
    private static volatile String e = "https://market.android.com/details?id=";
    private static String f = "com.flurry.android.ACTION_CATALOG";
    private static Random g = new Random(System.currentTimeMillis());
    private static int h = 5000;
    private static volatile int z = 0;
    private String i;
    private String j;
    private String k;
    private String l;
    private long m;
    private long n;
    private long o;
    private B p;
    private boolean q = true;
    private Map r = new HashMap();
    private Handler s;
    private boolean t;
    private transient Map u = new HashMap();
    private I v;
    private List w = new ArrayList();
    private Map x = new HashMap();
    /* access modifiers changed from: private */
    public C y;

    static /* synthetic */ void a(C0036k kVar, Context context, String str) {
        if (str.startsWith(d)) {
            String substring = str.substring(d.length());
            if (kVar.q) {
                try {
                    K.a(a, "Launching Android Market for app " + substring);
                    context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(str)));
                } catch (Exception e2) {
                    K.c(a, "Cannot launch Marketplace url " + str, e2);
                }
            } else {
                K.a(a, "Launching Android Market website for app " + substring);
                context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(e + substring)));
            }
        } else {
            K.d(a, "Unexpected android market url scheme: " + str);
        }
    }

    static {
        g.nextInt();
    }

    public C0036k(Context context, s sVar) {
        this.i = sVar.f;
        this.j = sVar.g;
        this.k = sVar.a;
        this.l = sVar.b;
        this.m = sVar.c;
        this.n = sVar.d;
        this.o = sVar.e;
        this.s = sVar.h;
        this.v = new I(this.s, h);
        context.getResources().getDisplayMetrics();
        this.p = new B(context, this, sVar);
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(d + context.getPackageName()));
        this.q = packageManager.queryIntentActivities(intent, 65536).size() > 0;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.p.d();
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.p.e();
    }

    /* access modifiers changed from: package-private */
    public final void a(Map map, Map map2, Map map3, Map map4, Map map5, Map map6) {
        this.p.a(map, map2, map3, map4, map5, map6);
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.p.c();
    }

    /* access modifiers changed from: package-private */
    public final Set d() {
        return this.p.a();
    }

    /* access modifiers changed from: package-private */
    public final C0026a a(long j2) {
        return this.p.b(j2);
    }

    /* access modifiers changed from: package-private */
    public final List e() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public final J b(long j2) {
        return (J) this.u.get(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.u.clear();
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        return this.i;
    }

    static void a(String str) {
        f = str;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, J j2, String str) {
        this.s.post(new E(this, str, context, j2));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0052, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        com.flurry.android.K.c(com.flurry.android.C0036k.a, "Unknown host: " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
        if (r7.y != null) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0073, code lost:
        c("Unknown host: " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00ab, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ac, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052 A[ExcHandler: UnknownHostException (r0v3 'e' java.net.UnknownHostException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String b(java.lang.String r8) {
        /*
            r7 = this;
            r5 = 0
            java.lang.String r0 = com.flurry.android.C0036k.c     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            boolean r0 = r8.startsWith(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            if (r0 != 0) goto L_0x0050
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r0.<init>(r8)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r1.<init>()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.HttpResponse r0 = r1.execute(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            int r1 = r1.getStatusCode()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0038
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r0 = org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r1 = com.flurry.android.C0036k.c     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
            boolean r1 = r0.startsWith(r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
            if (r1 != 0) goto L_0x0037
            java.lang.String r0 = r7.b(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
        L_0x0037:
            return r0
        L_0x0038:
            java.lang.String r0 = com.flurry.android.C0036k.a     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r2.<init>()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r3 = "Cannot process with responseCode "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r1 = r1.toString()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            com.flurry.android.K.c(r0, r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
        L_0x0050:
            r0 = r8
            goto L_0x0037
        L_0x0052:
            r0 = move-exception
            java.lang.String r1 = com.flurry.android.C0036k.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unknown host: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.flurry.android.K.c(r1, r2)
            com.flurry.android.C r1 = r7.y
            if (r1 == 0) goto L_0x008d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown host: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r7.c(r0)
        L_0x008d:
            r0 = r5
            goto L_0x0037
        L_0x008f:
            r0 = move-exception
            r1 = r8
        L_0x0091:
            java.lang.String r2 = com.flurry.android.C0036k.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed on url: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            com.flurry.android.K.c(r2, r1, r0)
            r0 = r5
            goto L_0x0037
        L_0x00ab:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.C0036k.b(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        a(new N(this, str));
    }

    /* access modifiers changed from: package-private */
    public final synchronized List a(Context context, List list, Long l2, int i2, boolean z2) {
        List emptyList;
        if (!this.p.b() || list == null) {
            emptyList = Collections.emptyList();
        } else {
            List a2 = a(list, l2);
            int min = Math.min(list.size(), a2.size());
            ArrayList arrayList = new ArrayList();
            for (int i3 = 0; i3 < min; i3++) {
                String str = (String) list.get(i3);
                w b2 = this.p.b(str);
                if (b2 != null) {
                    J j2 = new J((String) list.get(i3), (byte) 1, h());
                    a(j2);
                    if (i3 < a2.size()) {
                        j2.b = (C0039n) a2.get(i3);
                        j2.a(new C0030e((byte) 2, h()));
                        q qVar = new q(context, this, j2, b2, i2, z2);
                        qVar.a(a(j2, (Long) null));
                        arrayList.add(qVar);
                    }
                } else {
                    K.d(a, "Cannot find hook: " + str);
                }
            }
            emptyList = arrayList;
        }
        return emptyList;
    }

    private void a(J j2) {
        if (this.w.size() < 32767) {
            this.w.add(j2);
            this.u.put(Long.valueOf(j2.a()), j2);
        }
    }

    private List a(List list, Long l2) {
        if (!this.p.b() && list != null && !list.isEmpty()) {
            return Collections.emptyList();
        }
        C0039n[] a2 = this.p.a((String) list.get(0));
        if (a2 == null || a2.length <= 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(Arrays.asList(a2));
        Collections.shuffle(arrayList);
        if (l2 != null) {
            Iterator it = arrayList.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((C0039n) it.next()).a == l2.longValue()) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return arrayList.subList(0, Math.min(arrayList.size(), list.size()));
    }

    /* access modifiers changed from: package-private */
    public final long h() {
        return SystemClock.elapsedRealtime() - this.o;
    }

    public final synchronized void onClick(View view) {
        q qVar = (q) view;
        J a2 = qVar.a();
        a2.a(new C0030e((byte) 4, h()));
        if (this.t) {
            b(view.getContext(), a2, qVar.b(this.i));
        } else {
            a(view.getContext(), a2, qVar.b(this.j));
        }
    }

    private static void b(Context context, J j2, String str) {
        Intent intent = new Intent(f);
        intent.addCategory("android.intent.category.DEFAULT");
        intent.putExtra("u", str);
        if (j2 != null) {
            intent.putExtra("o", j2.a());
        }
        context.startActivity(intent);
    }

    private String a(J j2, Long l2) {
        C0039n nVar = j2.b;
        StringBuilder append = new StringBuilder().append("?apik=").append(this.k).append("&cid=").append(nVar.e).append("&adid=").append(nVar.a).append("&pid=").append(this.l).append("&iid=").append(this.m).append("&sid=").append(this.n).append("&its=").append(j2.a()).append("&hid=").append(C0029d.a(j2.a)).append("&ac=").append(a(nVar.g));
        if (this.r != null && !this.r.isEmpty()) {
            for (Map.Entry entry : this.r.entrySet()) {
                append.append("&").append("c_" + C0029d.a((String) entry.getKey())).append("=").append(C0029d.a((String) entry.getValue()));
            }
        }
        append.append("&ats=");
        if (l2 != null) {
            append.append(l2);
        }
        return append.toString();
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < bArr.length; i2++) {
            int i3 = (bArr[i2] >> 4) & 15;
            if (i3 < 10) {
                sb.append((char) (i3 + 48));
            } else {
                sb.append((char) ((i3 + 65) - 10));
            }
            byte b2 = bArr[i2] & 15;
            if (b2 < 10) {
                sb.append((char) (b2 + 48));
            } else {
                sb.append((char) ((b2 + 65) - 10));
            }
        }
        return sb.toString();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[adLogs=").append(this.w).append("]");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final C0026a i() {
        return this.p.a((short) 1);
    }

    private static void a(Runnable runnable) {
        new Handler().post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        if (this.y != null) {
            a(new M(this, i2));
        }
    }
}
