package com.flurry.android;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.flurry.android.o  reason: case insensitive filesystem */
final class C0040o extends LinearLayout {
    private View a;
    private List b = new ArrayList();
    private boolean c = true;
    private /* synthetic */ CatalogActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0040o(CatalogActivity catalogActivity, Context context) {
        super(context);
        this.d = catalogActivity;
        setOrientation(1);
        setGravity(48);
        this.a = new C0032g(catalogActivity, context);
        this.a.setId(10002);
        this.a.setOnClickListener(catalogActivity);
        a(a(context), this.c);
    }

    /* access modifiers changed from: package-private */
    public final List a(Context context) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 3; i++) {
            arrayList.add("hook" + i);
        }
        C0039n nVar = this.d.f == null ? null : this.d.f.b;
        List<q> a2 = this.d.e.a(context, arrayList, nVar == null ? null : Long.valueOf(nVar.a), 1, true);
        for (q onClickListener : a2) {
            onClickListener.setOnClickListener(this.d);
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c = !this.c;
        a(null, this.c);
    }

    /* access modifiers changed from: package-private */
    public final void a(List list) {
        a(list, this.c);
    }

    private void a(List list, boolean z) {
        removeAllViews();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(0, 0, 0, 0);
        addView(this.a, layoutParams);
        if (list != null) {
            this.b.clear();
            this.b.addAll(list);
        }
        if (z) {
            for (q qVar : this.b) {
                addView(qVar, layoutParams);
                qVar.a().a(new C0030e((byte) 3, this.d.e.h()));
            }
        }
        refreshDrawableState();
    }

    /* access modifiers changed from: package-private */
    public final List b() {
        return this.b;
    }
}
