package com.flurry.android;

import android.content.Context;
import android.os.Handler;

final class E implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ Context b;
    final /* synthetic */ J c;
    final /* synthetic */ C0036k d;

    E(C0036k kVar, String str, Context context, J j) {
        this.d = kVar;
        this.a = str;
        this.b = context;
        this.c = j;
    }

    public final void run() {
        new Handler().post(new A(this, this.d.b(this.a)));
    }
}
