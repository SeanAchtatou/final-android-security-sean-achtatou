package com.flurry.android;

final class A implements Runnable {
    private /* synthetic */ String a;
    private /* synthetic */ E b;

    A(E e, String str) {
        this.b = e;
        this.a = str;
    }

    public final void run() {
        if (this.a != null) {
            C0036k.a(this.b.d, this.b.b, this.a);
            this.b.c.a(new C0030e((byte) 8, this.b.d.h()));
            return;
        }
        String str = "Unable to launch in app market: " + this.b.a;
        K.d(C0036k.a, str);
        this.b.d.c(str);
    }
}
