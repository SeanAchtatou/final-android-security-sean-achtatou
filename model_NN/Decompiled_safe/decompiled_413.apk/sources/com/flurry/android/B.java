package com.flurry.android;

import android.content.Context;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class B {
    private Context a;
    private C0036k b;
    private s c;
    private volatile long d;
    private P e = new P(30);
    private P f = new P(30);
    private Map g = new HashMap();
    private Map h = new HashMap();
    private Map i = new HashMap();
    private Map j = new HashMap();
    private volatile boolean k;

    public B(Context context, C0036k kVar, s sVar) {
        this.a = context;
        this.b = kVar;
        this.c = sVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized C0039n[] a(String str) {
        C0039n[] nVarArr;
        nVarArr = (C0039n[]) this.g.get(str);
        if (nVarArr == null) {
            nVarArr = (C0039n[]) this.g.get("");
        }
        return nVarArr;
    }

    /* access modifiers changed from: package-private */
    public final synchronized F a(long j2) {
        return (F) this.f.a(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized Set a() {
        return this.e.c();
    }

    /* access modifiers changed from: package-private */
    public final synchronized C0026a b(long j2) {
        return (C0026a) this.e.a(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized C0026a a(short s) {
        Long l;
        l = (Long) this.j.get((short) 1);
        return l == null ? null : b(l.longValue());
    }

    /* access modifiers changed from: package-private */
    public final synchronized w b(String str) {
        w wVar;
        wVar = (w) this.h.get(str);
        if (wVar == null) {
            wVar = (w) this.h.get("");
        }
        return wVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.k;
    }

    private synchronized t a(byte b2) {
        return (t) this.i.get(Byte.valueOf(b2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Map map, Map map2, Map map3, Map map4, Map map5, Map map6) {
        this.d = System.currentTimeMillis();
        for (Map.Entry entry : map4.entrySet()) {
            if (entry.getValue() != null) {
                this.e.a(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry entry2 : map5.entrySet()) {
            if (entry2.getValue() != null) {
                this.f.a(entry2.getKey(), entry2.getValue());
            }
        }
        if (map2 != null && !map2.isEmpty()) {
            this.h = map2;
        }
        if (map3 != null && !map3.isEmpty()) {
            this.i = map3;
        }
        if (map6 != null && !map6.isEmpty()) {
            this.j = map6;
        }
        this.g = new HashMap();
        for (Map.Entry entry3 : map2.entrySet()) {
            w wVar = (w) entry3.getValue();
            C0039n[] nVarArr = (C0039n[]) map.get(Byte.valueOf(wVar.b));
            if (nVarArr != null) {
                this.g.put(entry3.getKey(), nVarArr);
            }
            t tVar = (t) map3.get(Byte.valueOf(wVar.c));
            if (tVar != null) {
                wVar.d = tVar;
            }
        }
        f();
        a(202);
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x004a=Splitter:B:28:0x004a, B:11:0x002d=Splitter:B:11:0x002d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void d() {
        /*
            r6 = this;
            monitor-enter(r6)
            android.content.Context r0 = r6.a     // Catch:{ all -> 0x0046 }
            java.lang.String r1 = r6.g()     // Catch:{ all -> 0x0046 }
            java.io.File r0 = r0.getFileStreamPath(r1)     // Catch:{ all -> 0x0046 }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x0046 }
            if (r1 == 0) goto L_0x004e
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0071, all -> 0x0049 }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0071, all -> 0x0049 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0071, all -> 0x0049 }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x0071, all -> 0x0049 }
            int r1 = r3.readUnsignedShort()     // Catch:{ Throwable -> 0x0036, all -> 0x006b }
            r2 = 46587(0xb5fb, float:6.5282E-41)
            if (r1 != r2) goto L_0x0032
            r6.a(r3)     // Catch:{ Throwable -> 0x0036, all -> 0x006b }
            r1 = 201(0xc9, float:2.82E-43)
            r6.a(r1)     // Catch:{ Throwable -> 0x0036, all -> 0x006b }
        L_0x002d:
            com.flurry.android.C0029d.a(r3)     // Catch:{ all -> 0x0046 }
        L_0x0030:
            monitor-exit(r6)
            return
        L_0x0032:
            a(r0)     // Catch:{ Throwable -> 0x0036, all -> 0x006b }
            goto L_0x002d
        L_0x0036:
            r1 = move-exception
            r2 = r3
        L_0x0038:
            java.lang.String r3 = "FlurryAgent"
            java.lang.String r4 = "Discarding cache"
            com.flurry.android.K.a(r3, r4, r1)     // Catch:{ all -> 0x006e }
            a(r0)     // Catch:{ all -> 0x006e }
            com.flurry.android.C0029d.a(r2)     // Catch:{ all -> 0x0046 }
            goto L_0x0030
        L_0x0046:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0049:
            r0 = move-exception
        L_0x004a:
            com.flurry.android.C0029d.a(r1)     // Catch:{ all -> 0x0046 }
            throw r0     // Catch:{ all -> 0x0046 }
        L_0x004e:
            java.lang.String r1 = "FlurryAgent"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0046 }
            r2.<init>()     // Catch:{ all -> 0x0046 }
            java.lang.String r3 = "cache file does not exist, path="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0046 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0046 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0046 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0046 }
            com.flurry.android.K.c(r1, r0)     // Catch:{ all -> 0x0046 }
            goto L_0x0030
        L_0x006b:
            r0 = move-exception
            r1 = r3
            goto L_0x004a
        L_0x006e:
            r0 = move-exception
            r1 = r2
            goto L_0x004a
        L_0x0071:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.B.d():void");
    }

    private static void a(File file) {
        if (!file.delete()) {
            K.b("FlurryAgent", "Cannot delete cached ads");
        }
    }

    private void f() {
        Iterator it = this.i.values().iterator();
        while (it.hasNext()) {
            it.next();
        }
        for (C0039n[] nVarArr : this.g.values()) {
            if (nVarArr != null) {
                for (C0039n nVar : nVarArr) {
                    nVar.h = b(nVar.f.longValue());
                    a(nVar.a);
                }
            }
        }
        for (w wVar : this.h.values()) {
            if (wVar.d == null) {
                wVar.d = a(wVar.c);
            }
            if (wVar.d == null) {
                K.d("FlurryAgent", "No ad theme found for " + ((int) wVar.c));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void e() {
        DataOutputStream dataOutputStream;
        try {
            File fileStreamPath = this.a.getFileStreamPath(g());
            File parentFile = fileStreamPath.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(fileStreamPath));
                try {
                    dataOutputStream2.writeShort(46587);
                    a(dataOutputStream2);
                    C0029d.a(dataOutputStream2);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    dataOutputStream = dataOutputStream2;
                    th = th2;
                    C0029d.a(dataOutputStream);
                    throw th;
                }
            } else {
                K.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                C0029d.a((Closeable) null);
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            C0029d.a(dataOutputStream);
            throw th;
        }
    }

    private void a(DataInputStream dataInputStream) {
        K.a("FlurryAgent", "Reading cache");
        if (dataInputStream.readUnsignedShort() == 2) {
            this.d = dataInputStream.readLong();
            int readUnsignedShort = dataInputStream.readUnsignedShort();
            this.e = new P(30);
            for (int i2 = 0; i2 < readUnsignedShort; i2++) {
                long readLong = dataInputStream.readLong();
                C0026a aVar = new C0026a();
                aVar.a(dataInputStream);
                this.e.a(Long.valueOf(readLong), aVar);
            }
            int readUnsignedShort2 = dataInputStream.readUnsignedShort();
            this.f = new P(30);
            for (int i3 = 0; i3 < readUnsignedShort2; i3++) {
                long readLong2 = dataInputStream.readLong();
                F f2 = new F();
                if (dataInputStream.readBoolean()) {
                    f2.a = dataInputStream.readUTF();
                }
                if (dataInputStream.readBoolean()) {
                    f2.b = dataInputStream.readUTF();
                }
                f2.c = dataInputStream.readInt();
                this.f.a(Long.valueOf(readLong2), f2);
            }
            int readUnsignedShort3 = dataInputStream.readUnsignedShort();
            this.h = new HashMap(readUnsignedShort3);
            for (int i4 = 0; i4 < readUnsignedShort3; i4++) {
                this.h.put(dataInputStream.readUTF(), new w(dataInputStream));
            }
            int readUnsignedShort4 = dataInputStream.readUnsignedShort();
            this.g = new HashMap(readUnsignedShort4);
            for (int i5 = 0; i5 < readUnsignedShort4; i5++) {
                String readUTF = dataInputStream.readUTF();
                int readUnsignedShort5 = dataInputStream.readUnsignedShort();
                C0039n[] nVarArr = new C0039n[readUnsignedShort5];
                for (int i6 = 0; i6 < readUnsignedShort5; i6++) {
                    C0039n nVar = new C0039n();
                    nVar.a(dataInputStream);
                    nVarArr[i6] = nVar;
                }
                this.g.put(readUTF, nVarArr);
            }
            int readUnsignedShort6 = dataInputStream.readUnsignedShort();
            this.i = new HashMap();
            for (int i7 = 0; i7 < readUnsignedShort6; i7++) {
                byte readByte = dataInputStream.readByte();
                t tVar = new t();
                tVar.b(dataInputStream);
                this.i.put(Byte.valueOf(readByte), tVar);
            }
            int readUnsignedShort7 = dataInputStream.readUnsignedShort();
            this.j = new HashMap(readUnsignedShort7);
            for (int i8 = 0; i8 < readUnsignedShort7; i8++) {
                this.j.put(Short.valueOf(dataInputStream.readShort()), Long.valueOf(dataInputStream.readLong()));
            }
            f();
            K.a("FlurryAgent", "Cache read, num images: " + this.e.a());
        }
    }

    private void a(DataOutputStream dataOutputStream) {
        dataOutputStream.writeShort(2);
        dataOutputStream.writeLong(this.d);
        Collection<Map.Entry> b2 = this.e.b();
        dataOutputStream.writeShort(b2.size());
        for (Map.Entry entry : b2) {
            dataOutputStream.writeLong(((Long) entry.getKey()).longValue());
            C0026a aVar = (C0026a) entry.getValue();
            dataOutputStream.writeLong(aVar.a);
            dataOutputStream.writeInt(aVar.b);
            dataOutputStream.writeInt(aVar.c);
            dataOutputStream.writeUTF(aVar.d);
            dataOutputStream.writeInt(aVar.e.length);
            dataOutputStream.write(aVar.e);
        }
        Collection<Map.Entry> b3 = this.f.b();
        dataOutputStream.writeShort(b3.size());
        for (Map.Entry entry2 : b3) {
            dataOutputStream.writeLong(((Long) entry2.getKey()).longValue());
            F f2 = (F) entry2.getValue();
            boolean z = f2.a != null;
            dataOutputStream.writeBoolean(z);
            if (z) {
                dataOutputStream.writeUTF(f2.a);
            }
            boolean z2 = f2.b != null;
            dataOutputStream.writeBoolean(z2);
            if (z2) {
                dataOutputStream.writeUTF(f2.b);
            }
            dataOutputStream.writeInt(f2.c);
        }
        dataOutputStream.writeShort(this.h.size());
        for (Map.Entry entry3 : this.h.entrySet()) {
            dataOutputStream.writeUTF((String) entry3.getKey());
            w wVar = (w) entry3.getValue();
            dataOutputStream.writeUTF(wVar.a);
            dataOutputStream.writeByte(wVar.b);
            dataOutputStream.writeByte(wVar.c);
        }
        dataOutputStream.writeShort(this.g.size());
        for (Map.Entry entry4 : this.g.entrySet()) {
            dataOutputStream.writeUTF((String) entry4.getKey());
            C0039n[] nVarArr = (C0039n[]) entry4.getValue();
            int length = nVarArr == null ? 0 : nVarArr.length;
            dataOutputStream.writeShort(length);
            for (int i2 = 0; i2 < length; i2++) {
                C0039n nVar = nVarArr[i2];
                dataOutputStream.writeLong(nVar.a);
                dataOutputStream.writeLong(nVar.b);
                dataOutputStream.writeUTF(nVar.d);
                dataOutputStream.writeUTF(nVar.c);
                dataOutputStream.writeLong(nVar.e);
                dataOutputStream.writeLong(nVar.f.longValue());
                dataOutputStream.writeByte(nVar.g.length);
                dataOutputStream.write(nVar.g);
            }
        }
        dataOutputStream.writeShort(this.i.size());
        for (Map.Entry entry5 : this.i.entrySet()) {
            dataOutputStream.writeByte(((Byte) entry5.getKey()).byteValue());
            ((t) entry5.getValue()).a(dataOutputStream);
        }
        dataOutputStream.writeShort(this.j.size());
        for (Map.Entry entry6 : this.j.entrySet()) {
            dataOutputStream.writeShort(((Short) entry6.getKey()).shortValue());
            dataOutputStream.writeLong(((Long) entry6.getValue()).longValue());
        }
    }

    private String g() {
        return ".flurryappcircle." + Integer.toString(this.c.a.hashCode(), 16);
    }

    private void a(int i2) {
        this.k = !this.g.isEmpty();
        if (this.k) {
            this.b.a(i2);
        }
    }
}
