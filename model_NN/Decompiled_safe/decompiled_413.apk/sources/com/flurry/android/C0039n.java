package com.flurry.android;

import java.io.DataInput;

/* renamed from: com.flurry.android.n  reason: case insensitive filesystem */
final class C0039n extends L {
    long a;
    long b;
    String c;
    String d;
    long e;
    Long f;
    byte[] g;
    C0026a h;

    C0039n() {
    }

    C0039n(DataInput dataInput) {
        b(dataInput);
    }

    /* access modifiers changed from: package-private */
    public final void a(DataInput dataInput) {
        b(dataInput);
    }

    private void b(DataInput dataInput) {
        this.a = dataInput.readLong();
        this.b = dataInput.readLong();
        this.d = dataInput.readUTF();
        this.c = dataInput.readUTF();
        this.e = dataInput.readLong();
        this.f = Long.valueOf(dataInput.readLong());
        this.g = new byte[dataInput.readUnsignedByte()];
        dataInput.readFully(this.g);
    }

    public final String toString() {
        return "ad {id=" + this.a + ", name='" + this.d + "'}";
    }
}
