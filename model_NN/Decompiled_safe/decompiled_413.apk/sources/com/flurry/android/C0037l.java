package com.flurry.android;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: com.flurry.android.l  reason: case insensitive filesystem */
final class C0037l extends LinkedHashMap {
    private /* synthetic */ P a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C0037l(P p, int i, float f) {
        super(i, f, true);
        this.a = p;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.a.b;
    }
}
