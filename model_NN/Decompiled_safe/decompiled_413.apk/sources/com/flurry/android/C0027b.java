package com.flurry.android;

import java.lang.Thread;

/* renamed from: com.flurry.android.b  reason: case insensitive filesystem */
final class C0027b implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();

    C0027b() {
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            u.h.a(th);
        } catch (Throwable th2) {
            K.b("FlurryAgent", "", th2);
        }
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
        }
    }
}
