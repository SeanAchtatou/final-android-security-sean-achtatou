package com.flurry.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

final class q extends RelativeLayout {
    private C0036k a;
    private J b;
    private String c;
    private int d;

    public q(Context context, C0036k kVar, J j, w wVar, int i, boolean z) {
        super(context);
        this.a = kVar;
        this.b = j;
        C0039n nVar = j.b;
        this.d = i;
        switch (this.d) {
            case 2:
                if (z) {
                    a(context, wVar, nVar, false);
                } else {
                    a(context, wVar, nVar, true);
                }
            case 1:
                if (!z) {
                    a(context, wVar, nVar, true);
                    break;
                } else {
                    a(context, wVar, nVar, false);
                    break;
                }
        }
        setFocusable(true);
    }

    private void a(Context context, w wVar, C0039n nVar, boolean z) {
        Drawable bitmapDrawable;
        Bitmap bitmap;
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        t tVar = wVar.d;
        ImageView imageView = new ImageView(context);
        imageView.setId(1);
        C0026a aVar = nVar.h;
        if (aVar != null) {
            byte[] bArr = aVar.e;
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            Bitmap createBitmap = Bitmap.createBitmap(decodeByteArray.getWidth(), decodeByteArray.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, decodeByteArray.getWidth(), decodeByteArray.getHeight());
            RectF rectF = new RectF(rect);
            float a2 = (float) C0029d.a(context, 8);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(-16777216);
            canvas.drawRoundRect(rectF, a2, a2, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(decodeByteArray, rect, rect, paint);
            if (Integer.parseInt(Build.VERSION.SDK) > 4) {
                BlurMaskFilter blurMaskFilter = new BlurMaskFilter(3.0f, BlurMaskFilter.Blur.OUTER);
                Paint paint2 = new Paint();
                paint2.setMaskFilter(blurMaskFilter);
                int[] iArr = new int[2];
                Bitmap copy = createBitmap.extractAlpha(paint2, iArr).copy(Bitmap.Config.ARGB_8888, true);
                new Canvas(copy).drawBitmap(createBitmap, (float) (-iArr[0]), (float) (-iArr[1]), (Paint) null);
                bitmap = copy;
            } else {
                bitmap = createBitmap;
            }
            imageView.setImageBitmap(bitmap);
            C0029d.a(context, imageView, C0029d.a(context, tVar.m), C0029d.a(context, tVar.n));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        C0026a a3 = this.a.a(tVar.c);
        if (a3 != null) {
            byte[] bArr2 = a3.e;
            Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length);
            if (NinePatch.isNinePatchChunk(decodeByteArray2.getNinePatchChunk())) {
                bitmapDrawable = new NinePatchDrawable(decodeByteArray2, decodeByteArray2.getNinePatchChunk(), new Rect(0, 0, 0, 0), null);
            } else {
                bitmapDrawable = new BitmapDrawable(decodeByteArray2);
            }
            setBackgroundDrawable(bitmapDrawable);
        }
        TextView textView = new TextView(context);
        textView.setId(5);
        textView.setPadding(0, 0, 0, 0);
        TextView textView2 = new TextView(context);
        textView2.setId(3);
        textView2.setPadding(0, 0, 0, 0);
        if (z) {
            textView.setTextColor(tVar.f);
            textView.setTextSize((float) tVar.e);
            textView.setText(new String("• " + tVar.b));
            textView.setTypeface(Typeface.create(tVar.d, 0));
            textView2.setTextColor(tVar.i);
            textView2.setTextSize((float) tVar.h);
            textView2.setTypeface(Typeface.create(tVar.g, 0));
            textView2.setText(nVar.d);
        } else {
            textView.setId(3);
            textView.setText(nVar.d);
            textView.setTextColor(tVar.i);
            textView.setTextSize((float) tVar.h);
            textView.setTypeface(Typeface.create(tVar.g, 0));
            textView2.setId(4);
            textView2.setText(nVar.c);
            textView2.setTextColor(tVar.l);
            textView2.setTextSize((float) tVar.k);
            textView2.setTypeface(Typeface.create(tVar.j, 0));
        }
        setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        addView(new ImageView(context), new RelativeLayout.LayoutParams(-1, -2));
        int i = (tVar.q - (tVar.o << 1)) - tVar.m;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.setMargins(tVar.o, tVar.p, i, 0);
        addView(imageView, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(6, imageView.getId());
        layoutParams2.addRule(1, imageView.getId());
        layoutParams2.setMargins(0, 0, 0, 0);
        addView(textView, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(1, imageView.getId());
        layoutParams3.addRule(3, textView.getId());
        layoutParams3.setMargins(0, -2, 0, 0);
        addView(textView2, layoutParams3);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public final String b(String str) {
        return str + this.c + System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    public final J a() {
        return this.b;
    }
}
