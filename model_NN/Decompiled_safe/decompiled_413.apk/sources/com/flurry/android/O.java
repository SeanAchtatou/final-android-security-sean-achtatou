package com.flurry.android;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Arrays;
import java.util.List;

final class O extends RelativeLayout {
    private C0036k a;
    private Context b;
    private String c;
    private int d;
    private boolean e;
    private boolean f;

    /* access modifiers changed from: package-private */
    public final void a() {
        if (!this.e) {
            q c2 = c();
            if (c2 != null) {
                removeAllViews();
                addView(c2, b());
                c2.a().a(new C0030e((byte) 3, this.a.h()));
                this.e = true;
            } else if (!this.f) {
                TextView textView = new TextView(this.b);
                textView.setText(C0036k.b);
                textView.setTextSize(1, 20.0f);
                addView(textView, b());
            }
            this.f = true;
        }
    }

    private static RelativeLayout.LayoutParams b() {
        return new RelativeLayout.LayoutParams(-1, -1);
    }

    private synchronized q c() {
        q qVar;
        List a2 = this.a.a(this.b, Arrays.asList(this.c), null, this.d, false);
        if (!a2.isEmpty()) {
            qVar = (q) a2.get(0);
            qVar.setOnClickListener(this.a);
        } else {
            qVar = null;
        }
        return qVar;
    }
}
