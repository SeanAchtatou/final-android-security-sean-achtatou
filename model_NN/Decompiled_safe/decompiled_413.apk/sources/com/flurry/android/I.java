package com.flurry.android;

import android.os.Handler;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class I {
    private List a = new ArrayList();
    private Handler b;
    private Handler c;
    private int d;
    private Runnable e;

    I(Handler handler, int i) {
        this.b = handler;
        this.c = new Handler();
        this.d = i;
        this.e = new C0038m(this);
        b();
    }

    /* access modifiers changed from: private */
    public synchronized void a() {
        ArrayList arrayList = new ArrayList();
        for (WeakReference weakReference : this.a) {
            O o = (O) weakReference.get();
            if (o != null) {
                arrayList.add(o);
            }
        }
        this.c.post(new C0035j(arrayList));
        b();
    }

    private synchronized void b() {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            if (((WeakReference) it.next()).get() == null) {
                it.remove();
            }
        }
        this.b.removeCallbacks(this.e);
        this.b.postDelayed(this.e, (long) this.d);
    }
}
