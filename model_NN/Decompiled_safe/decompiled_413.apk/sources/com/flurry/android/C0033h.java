package com.flurry.android;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

/* renamed from: com.flurry.android.h  reason: case insensitive filesystem */
final class C0033h {
    private String a;
    private Map b;
    private long c;
    private boolean d;
    private long e;
    private byte[] f;

    public C0033h(String str, Map map, long j, boolean z) {
        this.a = str;
        this.b = map;
        this.c = j;
        this.d = z;
    }

    public final byte[] a() {
        DataOutputStream dataOutputStream;
        Throwable th;
        if (this.f == null) {
            DataOutputStream dataOutputStream2 = null;
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                DataOutputStream dataOutputStream3 = new DataOutputStream(byteArrayOutputStream);
                try {
                    dataOutputStream3.writeUTF(this.a);
                    if (this.b == null) {
                        dataOutputStream3.writeShort(0);
                    } else {
                        dataOutputStream3.writeShort(this.b.size());
                        for (Map.Entry entry : this.b.entrySet()) {
                            dataOutputStream3.writeUTF(C0029d.a((String) entry.getKey(), 128));
                            dataOutputStream3.writeUTF(C0029d.a((String) entry.getValue(), 128));
                        }
                    }
                    dataOutputStream3.writeLong(this.c);
                    dataOutputStream3.writeLong(this.e);
                    dataOutputStream3.flush();
                    this.f = byteArrayOutputStream.toByteArray();
                    C0029d.a(dataOutputStream3);
                } catch (IOException e2) {
                    dataOutputStream2 = dataOutputStream3;
                    try {
                        this.f = new byte[0];
                        C0029d.a(dataOutputStream2);
                        return this.f;
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        dataOutputStream = dataOutputStream2;
                        th = th3;
                        C0029d.a(dataOutputStream);
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    dataOutputStream = dataOutputStream3;
                    C0029d.a(dataOutputStream);
                    throw th;
                }
            } catch (IOException e3) {
                this.f = new byte[0];
                C0029d.a(dataOutputStream2);
                return this.f;
            } catch (Throwable th5) {
                Throwable th6 = th5;
                dataOutputStream = null;
                th = th6;
                C0029d.a(dataOutputStream);
                throw th;
            }
        }
        return this.f;
    }
}
