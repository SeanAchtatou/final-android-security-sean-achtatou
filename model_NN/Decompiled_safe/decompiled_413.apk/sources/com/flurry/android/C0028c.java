package com.flurry.android;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/* renamed from: com.flurry.android.c  reason: case insensitive filesystem */
final class C0028c extends WebViewClient {
    private /* synthetic */ CatalogActivity a;

    C0028c(CatalogActivity catalogActivity) {
        this.a = catalogActivity;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str == null) {
            return false;
        }
        if (this.a.f != null) {
            this.a.f.a(new C0030e((byte) 6, this.a.e.h()));
        }
        this.a.e.a(webView.getContext(), this.a.f, str);
        return true;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        K.c("FlurryAgent", "Failed to load url: " + str2);
        webView.loadData("Cannot find Android Market information. <p>Please check your network", "text/html", "UTF-8");
    }

    public final void onPageFinished(WebView webView, String str) {
        try {
            J a2 = this.a.f;
            C0030e eVar = new C0030e((byte) 5, this.a.e.h());
            long j = this.a.f.c;
            a2.d.add(eVar);
            a2.c = j;
        } catch (Exception e) {
        }
    }
}
