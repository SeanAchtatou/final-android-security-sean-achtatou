package com.google.ads.util;

import android.util.Log;

public final class b {
    private b() {
    }

    public static void a(String str) {
        if (a("Ads", 3)) {
            Log.d("Ads", str);
        }
    }

    public static void a(String str, Throwable th) {
        if (a("Ads", 6)) {
            Log.e("Ads", str, th);
        }
    }

    private static boolean a(String str, int i) {
        return (i >= 5) || Log.isLoggable(str, i);
    }

    public static void b(String str) {
        if (a("Ads", 6)) {
            Log.e("Ads", str);
        }
    }

    public static void b(String str, Throwable th) {
        if (a("Ads", 5)) {
            Log.w("Ads", str, th);
        }
    }

    public static void c(String str) {
        if (a("Ads", 4)) {
            Log.i("Ads", str);
        }
    }

    public static void d(String str) {
        if (a("Ads", 2)) {
            Log.v("Ads", str);
        }
    }

    public static void e(String str) {
        if (a("Ads", 5)) {
            Log.w("Ads", str);
        }
    }
}
