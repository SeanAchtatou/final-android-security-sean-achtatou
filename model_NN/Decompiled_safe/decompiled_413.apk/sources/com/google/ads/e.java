package com.google.ads;

public class e {
    public static final e a = new e(320, 50, "320x50_mb");
    public static final e b = new e(300, 250, "300x250_as");
    public static final e c = new e(468, 60, "468x60_as");
    public static final e d = new e(728, 90, "728x90_as");
    private int e;
    private int f;
    private String g;

    private e(int i, int i2, String str) {
        this.e = i;
        this.f = i2;
        this.g = str;
    }

    public int a() {
        return this.e;
    }

    public int b() {
        return this.f;
    }

    public String toString() {
        return this.g;
    }
}
