package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdRequest {
    public static final String a = AdUtil.a("emulator");
    private Gender b = null;
    private String c = null;
    private Set<String> d = null;
    private Map<String, Object> e = null;
    private Location f = null;
    private boolean g = false;
    private boolean h = false;
    private Set<String> i = null;

    public enum ErrorCode {
        INVALID_REQUEST("Invalid Google Ad request."),
        NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
        NETWORK_ERROR("A network error occurred."),
        INTERNAL_ERROR("There was an internal error.");
        
        private String a;

        private ErrorCode(String str) {
            this.a = str;
        }

        public final String toString() {
            return this.a;
        }
    }

    public enum Gender {
        MALE("m"),
        FEMALE("f");
        
        private String a;

        private Gender(String str) {
            this.a = str;
        }

        public final String toString() {
            return this.a;
        }
    }

    public Map<String, Object> a(Context context) {
        HashMap hashMap = new HashMap();
        if (this.d != null) {
            hashMap.put("kw", this.d);
        }
        if (this.b != null) {
            hashMap.put("cust_gender", this.b.toString());
        }
        if (this.c != null) {
            hashMap.put("cust_age", this.c);
        }
        if (this.f != null) {
            hashMap.put("uule", AdUtil.a(this.f));
        }
        if (this.g) {
            hashMap.put("testing", 1);
        }
        if (b(context)) {
            hashMap.put("adtest", "on");
        } else if (!this.h) {
            b.c("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.c() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.h = true;
        }
        if (this.e != null) {
            hashMap.put("extras", this.e);
        }
        return hashMap;
    }

    public void a(String str) {
        if (this.d == null) {
            this.d = new HashSet();
        }
        this.d.add(str);
    }

    public void b(String str) {
        if (this.i == null) {
            this.i = new HashSet();
        }
        this.i.add(str);
    }

    public boolean b(Context context) {
        if (this.i != null) {
            String a2 = AdUtil.a(context);
            if (a2 == null) {
                return false;
            }
            if (this.i.contains(a2)) {
                return true;
            }
        }
        return false;
    }
}
