   var revmob = null;
    function onDeviceReady() {
      revmob = new RevMob(RevmobAppID);
      revmob.setTestingMode(false, null, null);
      revmob.showFullscreen(null, null);
    }

    function onBodyLoad() {
      document.addEventListener("deviceready", onDeviceReady, false);
    }

    function defaultSuccessCallback(params) {
    }

    function defaultErrorCallback(params) {
    }

  

    // function onDeviceReady() {
    //   cordova.exec(null, null, "SplashScreen", "hide", []);
    //   window.MacAddress = new MacAddress();
    //   window.MacAddress.getMacAddress(function(result){
    //       database._mac_address = result.mac;
    //   }, function(){
    //       database._mac_address = '01:02:03:04:05:06';
    //   });  
    // }




    jQuery(document).ready(function() {


      themeRefresh(theme);

      $('#title').html(title);


      $(".nav").remove();
      
      
      if (videoApp && puzzleApp){

        $('.footer').append($('<div data-role="navbar" class="nav"><ul><li id="videos-button"><a href="#videos" data-icon="home">Videos</a></li><li id="puzzle-button"><a href="#puzzle" data-icon="more">Game</a></li></ul></div>'));
     
      }else if (puzzleApp){
        $.mobile.changePage( "#puzzle", { transition: "fade"} );

        $('.footer').append($('<div data-role="navbar" class="nav"><ul><li id="puzzle-button"><a href="#puzzle" data-icon="more">Game</a></li></ul></div>'));
      }else if (videoApp){
      }

      $(".nav").navbar();


      if(videoApp){
        
          if(query !== ""){
            youTubeAjaxCall(query, exclude);
          }
            if(include.length >0 ) {youTubeAjaxCallArray(include);}
      }


      if(puzzleApp){

        $("#level").bind( "change", function(event, ui) {
          var level= $("#level").val();
          // console.log(level);
          var top = 5;

          if(level=="hard"){
              size = 75;
          }else{
            size = 100;
            var top = 35;
          }

          var imgnum = parseInt($("#puzzleContainer").attr('index'));
          $('#puzzleContainer').empty();
          $('#puzzleContainer').html('<div class="puzzle" style="margin-right: auto ;margin-left:auto;margin-top:'+top+'px;" data-position="fixed"><img id="puzzleImage" src="" width="320" height="390" alt="Puzzle Girl One" /></div>');
          $("#puzzleImage").attr("src","./"+imgnum+".jpg");
          $("#puzzleContainer").attr("index",imgnum);
          $( "div.puzzle, p" ).puzzle( size );
          $( "div.puzzle, p" ).puzzle(size);
        });

        $(function(){
          $("#puzzleContainer").attr('index','1');
          $( "div.puzzle, p" ).puzzle(100);
        });

        $('#next').live("click", function(){

          var level= $("#level").val();
          // console.log(level);
          var top = 5;

          if(level=="hard"){
            size = 75;
          }else{
            size = 100;
            var top = 35;
          }

          var imgnum = parseInt($("#puzzleContainer").attr('index'));
          // console.log('./'+imgnum+'.jpg');
          var image = $('<img src="./'+(imgnum+1)+'.jpg" />');
          // console.log(image[0]['width']);
          if (image[0]['width'] > 0){
            imgnum = imgnum+1;
          } else{
            imgnum = 1;
          }

          // if (ImageExist('./'+(imgnum+1)+'.jpg')){imgnum = 1;}else{imgnum = imgnum + 1;}
          $('#puzzleContainer').empty();
          $('#puzzleContainer').html('<div class="puzzle" style="margin-right: auto ;margin-left:auto;margin-top:'+top+'px;" data-position="fixed"><img id="puzzleImage" src="" width="320" height="390" alt="Puzzle Girl One" /></div>');
          $("#puzzleImage").attr("src","./"+imgnum+".jpg");
          $("#puzzleContainer").attr("index",imgnum);
          $( "div.puzzle, p" ).puzzle( size );
          }
        );

      }else{
        $("puzzle-button").remove()
      }
  
      $( "#nointernet" ).bind({
        popupafterclose:
        function(event, ui){
          youTubeAjaxCall();
        }

      });
          

        function  youTubeAjaxCallArray(videos){
        
          var funcs = [];
          for (var i = 0; i < videos.length ; i++) {
              funcs[i] = createfunc(videos[i]);
          }

          for (var j = 0; j < videos.length; j++) {
              funcs[j]();                        // and now let's run each one to see
          }
        }


        function createfunc(video) {
          // var id = video;
            return function() { 
               $.get('http://gdata.youtube.com/feeds/api/videos/'+video+'?alt=json',
                  function(response) {
                     jQuery('#listview').append('<li><a id="video" href="#" vidID="'+video+'"><img src="http://i.ytimg.com/vi/'+video+'/default.jpg" alt="" style="margin-top:10px;"><h2>'+response.entry.title.$t+'</h2><p>'+response.entry.content.$t+'</p></a></li>');
                     $('#listview').listview('refresh');
                  }).error(function(){ 
                    $( "#nointernet" ).popup( "open",{ transition: "flip"} ); 
                  });
            };
        }



        function  youTubeAjaxCall(queryString, exclude){
            var query=escape(queryString);
            $.get('https://gdata.youtube.com/feeds/api/videos?q='+query+'&max-results=20&orderby=relevance&v=2&alt=jsonc&safeSearch=strict&restriction=DE',
            function(response) {
                      if(response.data && response.data.items) {
                          var items = response.data.items;
                          if(items.length>0) {
                            for (var i = 0; i < items.length ; i++) {
                              var item = items[i];
                              if(jQuery.inArray(item.id, exclude)== -1){
                                ///if (item.status.value != 'restricted'){
                                  jQuery('#listview').append('<li><a id="video" href="#" vidID="'+item.id+'"><img src="http://i.ytimg.com/vi/'+item.id+'/default.jpg" alt="" style="margin-top:10px;"><h2>'+item.title+'</h2><p>'+item.description+'</p></a></li>');
                                 $('#listview').listview('refresh');
                                //}
                              }
                            };
                          }
                      }
            })
            .error(function(){ 
              $( "#nointernet" ).popup( "open",{ transition: "flip"} ); 
            });

            return;
        }



          function themeRefresh(theme){

              //the only difference between this block of code and the same code above is that it doesn't target list-dividers by calling: `.not('.ui-li-divider')`
              $.mobile.activePage.find('.ui-btn').not('.ui-li-divider')
                                 .removeClass('ui-btn-up-a ui-btn-up-b ui-btn-up-c ui-btn-up-d ui-btn-up-e ui-btn-hover-a ui-btn-hover-b ui-btn-hover-c ui-btn-hover-d ui-btn-hover-e')
                                 .addClass('ui-btn-up-' + theme)
                                 .attr('data-theme', theme);

              //target the list divider elements, then iterate through them to check if they have a theme set, if a theme is set then do nothing, otherwise change its theme to `b` (this is the jQuery Mobile default for list-dividers)
              $.mobile.activePage.find('.ui-li-divider').each(function (index, obj) {
                  if ($(this).parent().attr('data-divider-theme') == 'undefined') {
                      $(this).removeClass('ui-bar-a ui-bar-b ui-bar-c ui-bar-d ui-bar-e')
                             .addClass('ui-bar-b')
                             .attr('data-theme', 'b');
                  }
              })


               //reset all the buttons widgets
              $.mobile.activePage.find('.ui-btn')
                                 .removeClass('ui-btn-up-a ui-btn-up-b ui-btn-up-c ui-btn-up-d ui-btn-up-e ui-btn-hover-a ui-btn-hover-b ui-btn-hover-c ui-btn-hover-d ui-btn-hover-e')
                                 .addClass('ui-btn-up-' + theme)
                                 .attr('data-theme', theme);

              //reset the header/footer widgets
              $.mobile.activePage.find('.ui-header, .ui-footer')
                                 .removeClass('ui-bar-a ui-bar-b ui-bar-c ui-bar-d ui-bar-e')
                                 .addClass('ui-bar-' + theme)
                                 .attr('data-theme', theme);

              //reset the page widget
              $.mobile.activePage.removeClass('ui-body-a ui-body-b ui-body-c ui-body-d ui-body-e')
                                 .addClass('ui-body-' + theme)
                                 .attr('data-theme', theme);
              /*The rest of this code example is the same as the above example*/
          }








        $('#video').live("click", function(event){

          event.preventDefault();
          vidID = $(this).attr('vidID');

          jQuery('#player').html('<iframe src="http://www.youtube.com/embed/'+vidID+'?autoplay=1" width="160" height="90" ></iframe><br><br>');
          iframeAutoSize();
           $("html, body").animate({ scrollTop: 0 }, "slow");
        });


        function parseRSS(url) {
          $.ajax({
            url: 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
            dataType: 'json',
            success: function(data) {
             var items = data.responseData.feed.entries;
              if(items.length>0) {
                for (var i = 0; i < items.length ; i++) {
                  var item = items[i];
                  ///if (item.status.value != 'restricted'){
                    jQuery('#rsslistview').append('<li><a  id="rss" rssID="'+i+'" href="#" ><h2>'+item.title+'</h2><p>'+item.contentSnippet+'</p></a></li>');
                  //}
                  }
                }
                   $('#rsslistview').listview('refresh');
              }
          });
        }

   
        $('#rss').live("click", function(event){

          event.preventDefault();
          rssID = $(this).attr('rssID');

          jQuery('#article').html('<iframe src="http://www.youtube.com/embed/'+vidID+'?autoplay=1" width="160" height="90" ></iframe><br><br>');
          iframeAutoSize();
           $("html, body").animate({ scrollTop: 0 }, "slow");
        });




      $( document ).on( "pagebeforecreate", function( event, data ){
        // Let the framework know we're going to handle the load.
        event.preventDefault();
        iframeAutoSize();
        // themeRefresh(theme);
      });


      $( document ).on( "pagechange", function( event, data ){
        // Let the framework know we're going to handle the load.
        revmob.showPopup(null, null);
      });



      function iframeAutoSize() {
       // Find all YouTube videos
       var $allVideos = jQuery("iframe"),
           // The element that is fluid width
       $fluidEl = jQuery("[data-role='content']");
       // Figure out and save aspect ratio for each video
       $allVideos.each(function() {
        jQuery(this)
           .data('aspectRatio', this.height / this.width)
           // and remove the hard coded width/height
           .removeAttr('height')
           .removeAttr('width');
        });
        // When the window is resized
        jQuery(window).resize(function() {
          var newWidth = $fluidEl.width();
          // Resize all videos according to their own aspect ratio
          $allVideos.each(function() {
            var $el = jQuery(this);
            $el.height(newWidth * $el.data('aspectRatio'));
            $el.width(newWidth);
          });
          // Kick off one resize to fix all videos on page load
        }).resize();
      }

    });