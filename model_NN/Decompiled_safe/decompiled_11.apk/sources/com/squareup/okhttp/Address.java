package com.squareup.okhttp;

import com.squareup.okhttp.internal.Util;
import java.net.Proxy;
import java.net.UnknownHostException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class Address {
    final HostnameVerifier hostnameVerifier;
    final Proxy proxy;
    final SSLSocketFactory sslSocketFactory;
    final String uriHost;
    final int uriPort;

    public Address(String uriHost2, int uriPort2, SSLSocketFactory sslSocketFactory2, HostnameVerifier hostnameVerifier2, Proxy proxy2) throws UnknownHostException {
        if (uriHost2 == null) {
            throw new NullPointerException("uriHost == null");
        } else if (uriPort2 <= 0) {
            throw new IllegalArgumentException("uriPort <= 0: " + uriPort2);
        } else {
            this.proxy = proxy2;
            this.uriHost = uriHost2;
            this.uriPort = uriPort2;
            this.sslSocketFactory = sslSocketFactory2;
            this.hostnameVerifier = hostnameVerifier2;
        }
    }

    public String getUriHost() {
        return this.uriHost;
    }

    public int getUriPort() {
        return this.uriPort;
    }

    public SSLSocketFactory getSslSocketFactory() {
        return this.sslSocketFactory;
    }

    public HostnameVerifier getHostnameVerifier() {
        return this.hostnameVerifier;
    }

    public Proxy getProxy() {
        return this.proxy;
    }

    public boolean equals(Object other) {
        if (!(other instanceof Address)) {
            return false;
        }
        Address that = (Address) other;
        if (!Util.equal(this.proxy, that.proxy) || !this.uriHost.equals(that.uriHost) || this.uriPort != that.uriPort || !Util.equal(this.sslSocketFactory, that.sslSocketFactory) || !Util.equal(this.hostnameVerifier, that.hostnameVerifier)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int hashCode = (((this.uriHost.hashCode() + 527) * 31) + this.uriPort) * 31;
        if (this.sslSocketFactory != null) {
            i = this.sslSocketFactory.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 31;
        if (this.hostnameVerifier != null) {
            i2 = this.hostnameVerifier.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 31;
        if (this.proxy != null) {
            i3 = this.proxy.hashCode();
        }
        return i5 + i3;
    }
}
