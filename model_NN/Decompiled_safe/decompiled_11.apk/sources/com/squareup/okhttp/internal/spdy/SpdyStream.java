package com.squareup.okhttp.internal.spdy;

import android.support.v4.view.MotionEventCompat;
import com.squareup.okhttp.internal.Util;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public final class SpdyStream {
    static final /* synthetic */ boolean $assertionsDisabled = (!SpdyStream.class.desiredAssertionStatus());
    private static final int DATA_FRAME_HEADER_LENGTH = 8;
    public static final int RST_CANCEL = 5;
    public static final int RST_FLOW_CONTROL_ERROR = 7;
    public static final int RST_FRAME_TOO_LARGE = 11;
    public static final int RST_INTERNAL_ERROR = 6;
    public static final int RST_INVALID_CREDENTIALS = 10;
    public static final int RST_INVALID_STREAM = 2;
    public static final int RST_PROTOCOL_ERROR = 1;
    public static final int RST_REFUSED_STREAM = 3;
    public static final int RST_STREAM_ALREADY_CLOSED = 9;
    public static final int RST_STREAM_IN_USE = 8;
    public static final int RST_UNSUPPORTED_VERSION = 4;
    private static final String[] STATUS_CODE_NAMES = {null, "PROTOCOL_ERROR", "INVALID_STREAM", "REFUSED_STREAM", "UNSUPPORTED_VERSION", "CANCEL", "INTERNAL_ERROR", "FLOW_CONTROL_ERROR", "STREAM_IN_USE", "STREAM_ALREADY_CLOSED", "INVALID_CREDENTIALS", "FRAME_TOO_LARGE"};
    public static final int WINDOW_UPDATE_THRESHOLD = 32768;
    /* access modifiers changed from: private */
    public final SpdyConnection connection;
    /* access modifiers changed from: private */
    public final int id;
    private final SpdyDataInputStream in = new SpdyDataInputStream();
    private final SpdyDataOutputStream out = new SpdyDataOutputStream();
    private final int priority;
    /* access modifiers changed from: private */
    public long readTimeoutMillis = 0;
    private final List<String> requestHeaders;
    private List<String> responseHeaders;
    /* access modifiers changed from: private */
    public int rstStatusCode = -1;
    private final int slot;
    /* access modifiers changed from: private */
    public int writeWindowSize;

    SpdyStream(int id2, SpdyConnection connection2, int flags, int priority2, int slot2, List<String> requestHeaders2, Settings settings) {
        boolean z;
        boolean z2 = true;
        if (connection2 == null) {
            throw new NullPointerException("connection == null");
        } else if (requestHeaders2 == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.id = id2;
            this.connection = connection2;
            this.priority = priority2;
            this.slot = slot2;
            this.requestHeaders = requestHeaders2;
            if (isLocallyInitiated()) {
                boolean unused = this.in.finished = (flags & 2) != 0;
                boolean unused2 = this.out.finished = (flags & 1) == 0 ? false : z2;
            } else {
                SpdyDataInputStream spdyDataInputStream = this.in;
                if ((flags & 1) != 0) {
                    z = true;
                } else {
                    z = false;
                }
                boolean unused3 = spdyDataInputStream.finished = z;
                boolean unused4 = this.out.finished = (flags & 2) == 0 ? false : z2;
            }
            setSettings(settings);
        }
    }

    public synchronized boolean isOpen() {
        boolean z = $assertionsDisabled;
        synchronized (this) {
            if (this.rstStatusCode == -1) {
                if ((!this.in.finished && !this.in.closed) || ((!this.out.finished && !this.out.closed) || this.responseHeaders == null)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public boolean isLocallyInitiated() {
        boolean streamIsClient;
        if (this.id % 2 == 1) {
            streamIsClient = true;
        } else {
            streamIsClient = false;
        }
        if (this.connection.client == streamIsClient) {
            return true;
        }
        return $assertionsDisabled;
    }

    public SpdyConnection getConnection() {
        return this.connection;
    }

    public List<String> getRequestHeaders() {
        return this.requestHeaders;
    }

    public synchronized List<String> getResponseHeaders() throws IOException {
        while (this.responseHeaders == null && this.rstStatusCode == -1) {
            try {
                wait();
            } catch (InterruptedException e) {
                InterruptedIOException rethrow = new InterruptedIOException();
                rethrow.initCause(e);
                throw rethrow;
            }
        }
        if (this.responseHeaders != null) {
        } else {
            throw new IOException("stream was reset: " + rstStatusString());
        }
        return this.responseHeaders;
    }

    public synchronized int getRstStatusCode() {
        return this.rstStatusCode;
    }

    public void reply(List<String> responseHeaders2, boolean out2) throws IOException {
        if ($assertionsDisabled || !Thread.holdsLock(this)) {
            int flags = 0;
            synchronized (this) {
                if (responseHeaders2 == null) {
                    throw new NullPointerException("responseHeaders == null");
                } else if (isLocallyInitiated()) {
                    throw new IllegalStateException("cannot reply to a locally initiated stream");
                } else if (this.responseHeaders != null) {
                    throw new IllegalStateException("reply already sent");
                } else {
                    this.responseHeaders = responseHeaders2;
                    if (!out2) {
                        boolean unused = this.out.finished = true;
                        flags = 0 | 1;
                    }
                }
            }
            this.connection.writeSynReply(this.id, flags, responseHeaders2);
            return;
        }
        throw new AssertionError();
    }

    public void setReadTimeout(long readTimeoutMillis2) {
        this.readTimeoutMillis = readTimeoutMillis2;
    }

    public long getReadTimeoutMillis() {
        return this.readTimeoutMillis;
    }

    public InputStream getInputStream() {
        return this.in;
    }

    public OutputStream getOutputStream() {
        synchronized (this) {
            if (this.responseHeaders == null && !isLocallyInitiated()) {
                throw new IllegalStateException("reply before requesting the output stream");
            }
        }
        return this.out;
    }

    public void close(int rstStatusCode2) throws IOException {
        if (closeInternal(rstStatusCode2)) {
            this.connection.writeSynReset(this.id, rstStatusCode2);
        }
    }

    public void closeLater(int rstStatusCode2) {
        if (closeInternal(rstStatusCode2)) {
            this.connection.writeSynResetLater(this.id, rstStatusCode2);
        }
    }

    private boolean closeInternal(int rstStatusCode2) {
        if ($assertionsDisabled || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.rstStatusCode != -1) {
                    return $assertionsDisabled;
                }
                if (this.in.finished && this.out.finished) {
                    return $assertionsDisabled;
                }
                this.rstStatusCode = rstStatusCode2;
                notifyAll();
                this.connection.removeStream(this.id);
                return true;
            }
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void receiveReply(List<String> strings) throws IOException {
        if ($assertionsDisabled || !Thread.holdsLock(this)) {
            boolean streamInUseError = $assertionsDisabled;
            boolean open = true;
            synchronized (this) {
                if (!isLocallyInitiated() || this.responseHeaders != null) {
                    streamInUseError = true;
                } else {
                    this.responseHeaders = strings;
                    open = isOpen();
                    notifyAll();
                }
            }
            if (streamInUseError) {
                closeLater(8);
            } else if (!open) {
                this.connection.removeStream(this.id);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    public void receiveHeaders(List<String> headers) throws IOException {
        if ($assertionsDisabled || !Thread.holdsLock(this)) {
            boolean protocolError = $assertionsDisabled;
            synchronized (this) {
                if (this.responseHeaders != null) {
                    List<String> newHeaders = new ArrayList<>();
                    newHeaders.addAll(this.responseHeaders);
                    newHeaders.addAll(headers);
                    this.responseHeaders = newHeaders;
                } else {
                    protocolError = true;
                }
            }
            if (protocolError) {
                closeLater(1);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void receiveData(InputStream in2, int length) throws IOException {
        if ($assertionsDisabled || !Thread.holdsLock(this)) {
            this.in.receive(in2, length);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void receiveFin() {
        boolean open;
        if ($assertionsDisabled || !Thread.holdsLock(this)) {
            synchronized (this) {
                boolean unused = this.in.finished = true;
                open = isOpen();
                notifyAll();
            }
            if (!open) {
                this.connection.removeStream(this.id);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public synchronized void receiveRstStream(int statusCode) {
        if (this.rstStatusCode == -1) {
            this.rstStatusCode = statusCode;
            notifyAll();
        }
    }

    private void setSettings(Settings settings) {
        int i = 65536;
        if ($assertionsDisabled || Thread.holdsLock(this.connection)) {
            if (settings != null) {
                i = settings.getInitialWindowSize(65536);
            }
            this.writeWindowSize = i;
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void receiveSettings(Settings settings) {
        if ($assertionsDisabled || Thread.holdsLock(this)) {
            setSettings(settings);
            notifyAll();
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public synchronized void receiveWindowUpdate(int deltaWindowSize) {
        SpdyDataOutputStream.access$620(this.out, deltaWindowSize);
        notifyAll();
    }

    /* access modifiers changed from: private */
    public String rstStatusString() {
        return (this.rstStatusCode <= 0 || this.rstStatusCode >= STATUS_CODE_NAMES.length) ? Integer.toString(this.rstStatusCode) : STATUS_CODE_NAMES[this.rstStatusCode];
    }

    /* access modifiers changed from: package-private */
    public int getPriority() {
        return this.priority;
    }

    /* access modifiers changed from: package-private */
    public int getSlot() {
        return this.slot;
    }

    private final class SpdyDataInputStream extends InputStream {
        static final /* synthetic */ boolean $assertionsDisabled = (!SpdyStream.class.desiredAssertionStatus() ? true : SpdyStream.$assertionsDisabled);
        private final byte[] buffer;
        /* access modifiers changed from: private */
        public boolean closed;
        /* access modifiers changed from: private */
        public boolean finished;
        private int limit;
        private int pos;
        private int unacknowledgedBytes;

        private SpdyDataInputStream() {
            this.buffer = new byte[65536];
            this.pos = -1;
            this.unacknowledgedBytes = 0;
        }

        public int available() throws IOException {
            int length;
            synchronized (SpdyStream.this) {
                checkNotClosed();
                if (this.pos == -1) {
                    length = 0;
                } else if (this.limit > this.pos) {
                    length = this.limit - this.pos;
                } else {
                    length = this.limit + (this.buffer.length - this.pos);
                }
            }
            return length;
        }

        public int read() throws IOException {
            return Util.readSingleByte(this);
        }

        public int read(byte[] b, int offset, int count) throws IOException {
            int copied = -1;
            synchronized (SpdyStream.this) {
                Util.checkOffsetAndCount(b.length, offset, count);
                waitUntilReadable();
                checkNotClosed();
                if (this.pos != -1) {
                    copied = 0;
                    if (this.limit <= this.pos) {
                        int bytesToCopy = Math.min(count, this.buffer.length - this.pos);
                        System.arraycopy(this.buffer, this.pos, b, offset, bytesToCopy);
                        this.pos += bytesToCopy;
                        copied = 0 + bytesToCopy;
                        if (this.pos == this.buffer.length) {
                            this.pos = 0;
                        }
                    }
                    if (copied < count) {
                        int bytesToCopy2 = Math.min(this.limit - this.pos, count - copied);
                        System.arraycopy(this.buffer, this.pos, b, offset + copied, bytesToCopy2);
                        this.pos += bytesToCopy2;
                        copied += bytesToCopy2;
                    }
                    this.unacknowledgedBytes += copied;
                    if (this.unacknowledgedBytes >= 32768) {
                        SpdyStream.this.connection.writeWindowUpdateLater(SpdyStream.this.id, this.unacknowledgedBytes);
                        this.unacknowledgedBytes = 0;
                    }
                    if (this.pos == this.limit) {
                        this.pos = -1;
                        this.limit = 0;
                    }
                }
            }
            return copied;
        }

        private void waitUntilReadable() throws IOException {
            long start = 0;
            long remaining = 0;
            if (SpdyStream.this.readTimeoutMillis != 0) {
                start = System.nanoTime() / 1000000;
                remaining = SpdyStream.this.readTimeoutMillis;
            }
            while (this.pos == -1 && !this.finished && !this.closed && SpdyStream.this.rstStatusCode == -1) {
                try {
                    if (SpdyStream.this.readTimeoutMillis == 0) {
                        SpdyStream.this.wait();
                    } else if (remaining > 0) {
                        SpdyStream.this.wait(remaining);
                        remaining = (SpdyStream.this.readTimeoutMillis + start) - (System.nanoTime() / 1000000);
                    } else {
                        throw new SocketTimeoutException();
                    }
                } catch (InterruptedException e) {
                    throw new InterruptedIOException();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void receive(InputStream in, int byteCount) throws IOException {
            boolean finished2;
            int pos2;
            int firstNewByte;
            int limit2;
            boolean flowControlError;
            if (!$assertionsDisabled && Thread.holdsLock(SpdyStream.this)) {
                throw new AssertionError();
            } else if (byteCount != 0) {
                synchronized (SpdyStream.this) {
                    finished2 = this.finished;
                    pos2 = this.pos;
                    firstNewByte = this.limit;
                    limit2 = this.limit;
                    flowControlError = byteCount > this.buffer.length - available() ? true : SpdyStream.$assertionsDisabled;
                }
                if (flowControlError) {
                    Util.skipByReading(in, (long) byteCount);
                    SpdyStream.this.closeLater(7);
                } else if (finished2) {
                    Util.skipByReading(in, (long) byteCount);
                } else {
                    if (pos2 < limit2) {
                        int firstCopyCount = Math.min(byteCount, this.buffer.length - limit2);
                        Util.readFully(in, this.buffer, limit2, firstCopyCount);
                        limit2 += firstCopyCount;
                        byteCount -= firstCopyCount;
                        if (limit2 == this.buffer.length) {
                            limit2 = 0;
                        }
                    }
                    if (byteCount > 0) {
                        Util.readFully(in, this.buffer, limit2, byteCount);
                        limit2 += byteCount;
                    }
                    synchronized (SpdyStream.this) {
                        this.limit = limit2;
                        if (this.pos == -1) {
                            this.pos = firstNewByte;
                            SpdyStream.this.notifyAll();
                        }
                    }
                }
            }
        }

        public void close() throws IOException {
            synchronized (SpdyStream.this) {
                this.closed = true;
                SpdyStream.this.notifyAll();
            }
            SpdyStream.this.cancelStreamIfNecessary();
        }

        private void checkNotClosed() throws IOException {
            if (this.closed) {
                throw new IOException("stream closed");
            } else if (SpdyStream.this.rstStatusCode != -1) {
                throw new IOException("stream was reset: " + SpdyStream.this.rstStatusString());
            }
        }
    }

    /* access modifiers changed from: private */
    public void cancelStreamIfNecessary() throws IOException {
        boolean cancel;
        boolean open;
        if ($assertionsDisabled || !Thread.holdsLock(this)) {
            synchronized (this) {
                cancel = (this.in.finished || !this.in.closed || (!this.out.finished && !this.out.closed)) ? $assertionsDisabled : true;
                open = isOpen();
            }
            if (cancel) {
                close(5);
            } else if (!open) {
                this.connection.removeStream(this.id);
            }
        } else {
            throw new AssertionError();
        }
    }

    private final class SpdyDataOutputStream extends OutputStream {
        static final /* synthetic */ boolean $assertionsDisabled = (!SpdyStream.class.desiredAssertionStatus() ? true : SpdyStream.$assertionsDisabled);
        private final byte[] buffer;
        /* access modifiers changed from: private */
        public boolean closed;
        /* access modifiers changed from: private */
        public boolean finished;
        private int pos;
        private int unacknowledgedBytes;

        private SpdyDataOutputStream() {
            this.buffer = new byte[8192];
            this.pos = 8;
            this.unacknowledgedBytes = 0;
        }

        static /* synthetic */ int access$620(SpdyDataOutputStream x0, int x1) {
            int i = x0.unacknowledgedBytes - x1;
            x0.unacknowledgedBytes = i;
            return i;
        }

        public void write(int b) throws IOException {
            Util.writeSingleByte(this, b);
        }

        public void write(byte[] bytes, int offset, int count) throws IOException {
            if ($assertionsDisabled || !Thread.holdsLock(SpdyStream.this)) {
                Util.checkOffsetAndCount(bytes.length, offset, count);
                checkNotClosed();
                while (count > 0) {
                    if (this.pos == this.buffer.length) {
                        writeFrame(SpdyStream.$assertionsDisabled);
                    }
                    int bytesToCopy = Math.min(count, this.buffer.length - this.pos);
                    System.arraycopy(bytes, offset, this.buffer, this.pos, bytesToCopy);
                    this.pos += bytesToCopy;
                    offset += bytesToCopy;
                    count -= bytesToCopy;
                }
                return;
            }
            throw new AssertionError();
        }

        public void flush() throws IOException {
            if ($assertionsDisabled || !Thread.holdsLock(SpdyStream.this)) {
                checkNotClosed();
                if (this.pos > 8) {
                    writeFrame(SpdyStream.$assertionsDisabled);
                    SpdyStream.this.connection.flush();
                    return;
                }
                return;
            }
            throw new AssertionError();
        }

        public void close() throws IOException {
            if ($assertionsDisabled || !Thread.holdsLock(SpdyStream.this)) {
                synchronized (SpdyStream.this) {
                    if (!this.closed) {
                        this.closed = true;
                        writeFrame(true);
                        SpdyStream.this.connection.flush();
                        SpdyStream.this.cancelStreamIfNecessary();
                        return;
                    }
                    return;
                }
            }
            throw new AssertionError();
        }

        private void writeFrame(boolean last) throws IOException {
            if ($assertionsDisabled || !Thread.holdsLock(SpdyStream.this)) {
                int length = this.pos - 8;
                synchronized (SpdyStream.this) {
                    waitUntilWritable(length, last);
                    this.unacknowledgedBytes += length;
                }
                int flags = 0;
                if (last) {
                    flags = 0 | 1;
                }
                Util.pokeInt(this.buffer, 0, SpdyStream.this.id & Integer.MAX_VALUE, ByteOrder.BIG_ENDIAN);
                Util.pokeInt(this.buffer, 4, ((flags & MotionEventCompat.ACTION_MASK) << 24) | (16777215 & length), ByteOrder.BIG_ENDIAN);
                SpdyStream.this.connection.writeFrame(this.buffer, 0, this.pos);
                this.pos = 8;
                return;
            }
            throw new AssertionError();
        }

        private void waitUntilWritable(int count, boolean last) throws IOException {
            do {
                try {
                    if (this.unacknowledgedBytes + count >= SpdyStream.this.writeWindowSize) {
                        SpdyStream.this.wait();
                        if (!last && this.closed) {
                            throw new IOException("stream closed");
                        } else if (this.finished) {
                            throw new IOException("stream finished");
                        }
                    } else {
                        return;
                    }
                } catch (InterruptedException e) {
                    throw new InterruptedIOException();
                }
            } while (SpdyStream.this.rstStatusCode == -1);
            throw new IOException("stream was reset: " + SpdyStream.this.rstStatusString());
        }

        private void checkNotClosed() throws IOException {
            synchronized (SpdyStream.this) {
                if (this.closed) {
                    throw new IOException("stream closed");
                } else if (this.finished) {
                    throw new IOException("stream finished");
                } else if (SpdyStream.this.rstStatusCode != -1) {
                    throw new IOException("stream was reset: " + SpdyStream.this.rstStatusString());
                }
            }
        }
    }
}
