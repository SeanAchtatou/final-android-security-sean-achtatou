package com.squareup.okhttp.internal.http;

import com.squareup.okhttp.OkResponseCache;
import com.squareup.okhttp.ResponseSource;
import com.squareup.okhttp.internal.Base64;
import com.squareup.okhttp.internal.DiskLruCache;
import com.squareup.okhttp.internal.StrictLineReader;
import com.squareup.okhttp.internal.Util;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.CacheRequest;
import java.net.CacheResponse;
import java.net.HttpURLConnection;
import java.net.ResponseCache;
import java.net.SecureCacheResponse;
import java.net.URI;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class HttpResponseCache extends ResponseCache implements OkResponseCache {
    private static final char[] DIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final int ENTRY_BODY = 1;
    private static final int ENTRY_COUNT = 2;
    private static final int ENTRY_METADATA = 0;
    private static final int VERSION = 201105;
    private final DiskLruCache cache;
    private int hitCount;
    private int networkCount;
    private int requestCount;
    private int writeAbortCount;
    private int writeSuccessCount;

    static /* synthetic */ int access$408(HttpResponseCache x0) {
        int i = x0.writeSuccessCount;
        x0.writeSuccessCount = i + 1;
        return i;
    }

    static /* synthetic */ int access$508(HttpResponseCache x0) {
        int i = x0.writeAbortCount;
        x0.writeAbortCount = i + 1;
        return i;
    }

    public HttpResponseCache(File directory, long maxSize) throws IOException {
        this.cache = DiskLruCache.open(directory, VERSION, 2, maxSize);
    }

    private String uriToKey(URI uri) {
        try {
            return bytesToHexString(MessageDigest.getInstance("MD5").digest(uri.toString().getBytes("UTF-8")));
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        } catch (UnsupportedEncodingException e2) {
            throw new AssertionError(e2);
        }
    }

    private static String bytesToHexString(byte[] bytes) {
        char[] digits = DIGITS;
        char[] buf = new char[(bytes.length * 2)];
        int c = 0;
        for (byte b : bytes) {
            int c2 = c + 1;
            buf[c] = digits[(b >> 4) & 15];
            c = c2 + 1;
            buf[c2] = digits[b & 15];
        }
        return new String(buf);
    }

    public CacheResponse get(URI uri, String requestMethod, Map<String, List<String>> requestHeaders) {
        try {
            DiskLruCache.Snapshot snapshot = this.cache.get(uriToKey(uri));
            if (snapshot == null) {
                return null;
            }
            Entry entry = new Entry(snapshot.getInputStream(0));
            if (entry.matches(uri, requestMethod, requestHeaders)) {
                return entry.isHttps() ? new EntrySecureCacheResponse(entry, snapshot) : new EntryCacheResponse(entry, snapshot);
            }
            snapshot.close();
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public CacheRequest put(URI uri, URLConnection urlConnection) throws IOException {
        HttpEngine httpEngine;
        if (!(urlConnection instanceof HttpURLConnection)) {
            return null;
        }
        HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;
        String requestMethod = httpConnection.getRequestMethod();
        String key = uriToKey(uri);
        if (requestMethod.equals("POST") || requestMethod.equals("PUT") || requestMethod.equals("DELETE")) {
            try {
                this.cache.remove(key);
                return null;
            } catch (IOException e) {
                return null;
            }
        } else if (!requestMethod.equals("GET") || (httpEngine = getHttpEngine(httpConnection)) == null) {
            return null;
        } else {
            ResponseHeaders response = httpEngine.getResponseHeaders();
            if (response.hasVaryAll()) {
                return null;
            }
            Entry entry = new Entry(uri, httpEngine.getRequestHeaders().getHeaders().getAll(response.getVaryFields()), httpConnection);
            try {
                DiskLruCache.Editor editor = this.cache.edit(key);
                if (editor == null) {
                    return null;
                }
                entry.writeTo(editor);
                return new CacheRequestImpl(editor);
            } catch (IOException e2) {
                abortQuietly(null);
                return null;
            }
        }
    }

    public void update(CacheResponse conditionalCacheHit, HttpURLConnection httpConnection) throws IOException {
        HttpEngine httpEngine = getHttpEngine(httpConnection);
        Entry entry = new Entry(httpEngine.getUri(), httpEngine.getRequestHeaders().getHeaders().getAll(httpEngine.getResponseHeaders().getVaryFields()), httpConnection);
        try {
            DiskLruCache.Editor editor = (conditionalCacheHit instanceof EntryCacheResponse ? ((EntryCacheResponse) conditionalCacheHit).snapshot : ((EntrySecureCacheResponse) conditionalCacheHit).snapshot).edit();
            if (editor != null) {
                entry.writeTo(editor);
                editor.commit();
            }
        } catch (IOException e) {
            abortQuietly(null);
        }
    }

    private void abortQuietly(DiskLruCache.Editor editor) {
        if (editor != null) {
            try {
                editor.abort();
            } catch (IOException e) {
            }
        }
    }

    private HttpEngine getHttpEngine(URLConnection httpConnection) {
        if (httpConnection instanceof HttpURLConnectionImpl) {
            return ((HttpURLConnectionImpl) httpConnection).getHttpEngine();
        }
        if (httpConnection instanceof HttpsURLConnectionImpl) {
            return ((HttpsURLConnectionImpl) httpConnection).getHttpEngine();
        }
        return null;
    }

    public DiskLruCache getCache() {
        return this.cache;
    }

    public synchronized int getWriteAbortCount() {
        return this.writeAbortCount;
    }

    public synchronized int getWriteSuccessCount() {
        return this.writeSuccessCount;
    }

    public synchronized void trackResponse(ResponseSource source) {
        this.requestCount++;
        switch (source) {
            case CACHE:
                this.hitCount++;
                break;
            case CONDITIONAL_CACHE:
            case NETWORK:
                this.networkCount++;
                break;
        }
    }

    public synchronized void trackConditionalCacheHit() {
        this.hitCount++;
    }

    public synchronized int getNetworkCount() {
        return this.networkCount;
    }

    public synchronized int getHitCount() {
        return this.hitCount;
    }

    public synchronized int getRequestCount() {
        return this.requestCount;
    }

    private final class CacheRequestImpl extends CacheRequest {
        private OutputStream body;
        private OutputStream cacheOut;
        /* access modifiers changed from: private */
        public boolean done;
        private final DiskLruCache.Editor editor;

        public CacheRequestImpl(final DiskLruCache.Editor editor2) throws IOException {
            this.editor = editor2;
            this.cacheOut = editor2.newOutputStream(1);
            this.body = new FilterOutputStream(this.cacheOut, HttpResponseCache.this) {
                public void close() throws IOException {
                    synchronized (HttpResponseCache.this) {
                        if (!CacheRequestImpl.this.done) {
                            boolean unused = CacheRequestImpl.this.done = true;
                            HttpResponseCache.access$408(HttpResponseCache.this);
                            super.close();
                            editor2.commit();
                        }
                    }
                }

                public void write(byte[] buffer, int offset, int length) throws IOException {
                    this.out.write(buffer, offset, length);
                }
            };
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void abort() {
            /*
                r2 = this;
                com.squareup.okhttp.internal.http.HttpResponseCache r1 = com.squareup.okhttp.internal.http.HttpResponseCache.this
                monitor-enter(r1)
                boolean r0 = r2.done     // Catch:{ all -> 0x001f }
                if (r0 == 0) goto L_0x0009
                monitor-exit(r1)     // Catch:{ all -> 0x001f }
            L_0x0008:
                return
            L_0x0009:
                r0 = 1
                r2.done = r0     // Catch:{ all -> 0x001f }
                com.squareup.okhttp.internal.http.HttpResponseCache r0 = com.squareup.okhttp.internal.http.HttpResponseCache.this     // Catch:{ all -> 0x001f }
                com.squareup.okhttp.internal.http.HttpResponseCache.access$508(r0)     // Catch:{ all -> 0x001f }
                monitor-exit(r1)     // Catch:{ all -> 0x001f }
                java.io.OutputStream r0 = r2.cacheOut
                com.squareup.okhttp.internal.Util.closeQuietly(r0)
                com.squareup.okhttp.internal.DiskLruCache$Editor r0 = r2.editor     // Catch:{ IOException -> 0x001d }
                r0.abort()     // Catch:{ IOException -> 0x001d }
                goto L_0x0008
            L_0x001d:
                r0 = move-exception
                goto L_0x0008
            L_0x001f:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x001f }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.http.HttpResponseCache.CacheRequestImpl.abort():void");
        }

        public OutputStream getBody() throws IOException {
            return this.body;
        }
    }

    private static final class Entry {
        /* access modifiers changed from: private */
        public final String cipherSuite;
        /* access modifiers changed from: private */
        public final Certificate[] localCertificates;
        /* access modifiers changed from: private */
        public final Certificate[] peerCertificates;
        private final String requestMethod;
        /* access modifiers changed from: private */
        public final RawHeaders responseHeaders;
        private final String uri;
        private final RawHeaders varyHeaders;

        public Entry(InputStream in) throws IOException {
            try {
                StrictLineReader reader = new StrictLineReader(in, Util.US_ASCII);
                this.uri = reader.readLine();
                this.requestMethod = reader.readLine();
                this.varyHeaders = new RawHeaders();
                int varyRequestHeaderLineCount = reader.readInt();
                for (int i = 0; i < varyRequestHeaderLineCount; i++) {
                    this.varyHeaders.addLine(reader.readLine());
                }
                this.responseHeaders = new RawHeaders();
                this.responseHeaders.setStatusLine(reader.readLine());
                int responseHeaderLineCount = reader.readInt();
                for (int i2 = 0; i2 < responseHeaderLineCount; i2++) {
                    this.responseHeaders.addLine(reader.readLine());
                }
                if (isHttps()) {
                    String blank = reader.readLine();
                    if (!blank.isEmpty()) {
                        throw new IOException("expected \"\" but was \"" + blank + "\"");
                    }
                    this.cipherSuite = reader.readLine();
                    this.peerCertificates = readCertArray(reader);
                    this.localCertificates = readCertArray(reader);
                } else {
                    this.cipherSuite = null;
                    this.peerCertificates = null;
                    this.localCertificates = null;
                }
            } finally {
                in.close();
            }
        }

        public Entry(URI uri2, RawHeaders varyHeaders2, HttpURLConnection httpConnection) throws IOException {
            this.uri = uri2.toString();
            this.varyHeaders = varyHeaders2;
            this.requestMethod = httpConnection.getRequestMethod();
            this.responseHeaders = RawHeaders.fromMultimap(httpConnection.getHeaderFields(), true);
            if (isHttps()) {
                HttpsURLConnection httpsConnection = (HttpsURLConnection) httpConnection;
                this.cipherSuite = httpsConnection.getCipherSuite();
                Certificate[] peerCertificatesNonFinal = null;
                try {
                    peerCertificatesNonFinal = httpsConnection.getServerCertificates();
                } catch (SSLPeerUnverifiedException e) {
                }
                this.peerCertificates = peerCertificatesNonFinal;
                this.localCertificates = httpsConnection.getLocalCertificates();
                return;
            }
            this.cipherSuite = null;
            this.peerCertificates = null;
            this.localCertificates = null;
        }

        public void writeTo(DiskLruCache.Editor editor) throws IOException {
            Writer writer = new BufferedWriter(new OutputStreamWriter(editor.newOutputStream(0), Util.UTF_8));
            writer.write(this.uri + 10);
            writer.write(this.requestMethod + 10);
            writer.write(Integer.toString(this.varyHeaders.length()) + 10);
            for (int i = 0; i < this.varyHeaders.length(); i++) {
                writer.write(this.varyHeaders.getFieldName(i) + ": " + this.varyHeaders.getValue(i) + 10);
            }
            writer.write(this.responseHeaders.getStatusLine() + 10);
            writer.write(Integer.toString(this.responseHeaders.length()) + 10);
            for (int i2 = 0; i2 < this.responseHeaders.length(); i2++) {
                writer.write(this.responseHeaders.getFieldName(i2) + ": " + this.responseHeaders.getValue(i2) + 10);
            }
            if (isHttps()) {
                writer.write(10);
                writer.write(this.cipherSuite + 10);
                writeCertArray(writer, this.peerCertificates);
                writeCertArray(writer, this.localCertificates);
            }
            writer.close();
        }

        /* access modifiers changed from: private */
        public boolean isHttps() {
            return this.uri.startsWith("https://");
        }

        private Certificate[] readCertArray(StrictLineReader reader) throws IOException {
            int length = reader.readInt();
            if (length == -1) {
                return null;
            }
            try {
                CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
                Certificate[] result = new Certificate[length];
                for (int i = 0; i < result.length; i++) {
                    result[i] = certificateFactory.generateCertificate(new ByteArrayInputStream(Base64.decode(reader.readLine().getBytes("US-ASCII"))));
                }
                return result;
            } catch (CertificateException e) {
                throw new IOException(e);
            }
        }

        private void writeCertArray(Writer writer, Certificate[] certificates) throws IOException {
            if (certificates == null) {
                writer.write("-1\n");
                return;
            }
            try {
                writer.write(Integer.toString(certificates.length) + 10);
                Certificate[] arr$ = certificates;
                int len$ = arr$.length;
                for (int i$ = 0; i$ < len$; i$++) {
                    writer.write(Base64.encode(arr$[i$].getEncoded()) + 10);
                }
            } catch (CertificateEncodingException e) {
                throw new IOException(e);
            }
        }

        public boolean matches(URI uri2, String requestMethod2, Map<String, List<String>> requestHeaders) {
            return this.uri.equals(uri2.toString()) && this.requestMethod.equals(requestMethod2) && new ResponseHeaders(uri2, this.responseHeaders).varyMatches(this.varyHeaders.toMultimap(false), requestHeaders);
        }
    }

    /* access modifiers changed from: private */
    public static InputStream newBodyInputStream(final DiskLruCache.Snapshot snapshot) {
        return new FilterInputStream(snapshot.getInputStream(1)) {
            public void close() throws IOException {
                snapshot.close();
                super.close();
            }
        };
    }

    static class EntryCacheResponse extends CacheResponse {
        private final Entry entry;
        private final InputStream in;
        /* access modifiers changed from: private */
        public final DiskLruCache.Snapshot snapshot;

        public EntryCacheResponse(Entry entry2, DiskLruCache.Snapshot snapshot2) {
            this.entry = entry2;
            this.snapshot = snapshot2;
            this.in = HttpResponseCache.newBodyInputStream(snapshot2);
        }

        public Map<String, List<String>> getHeaders() {
            return this.entry.responseHeaders.toMultimap(true);
        }

        public InputStream getBody() {
            return this.in;
        }
    }

    static class EntrySecureCacheResponse extends SecureCacheResponse {
        private final Entry entry;
        private final InputStream in;
        /* access modifiers changed from: private */
        public final DiskLruCache.Snapshot snapshot;

        public EntrySecureCacheResponse(Entry entry2, DiskLruCache.Snapshot snapshot2) {
            this.entry = entry2;
            this.snapshot = snapshot2;
            this.in = HttpResponseCache.newBodyInputStream(snapshot2);
        }

        public Map<String, List<String>> getHeaders() {
            return this.entry.responseHeaders.toMultimap(true);
        }

        public InputStream getBody() {
            return this.in;
        }

        public String getCipherSuite() {
            return this.entry.cipherSuite;
        }

        public List<Certificate> getServerCertificateChain() throws SSLPeerUnverifiedException {
            if (this.entry.peerCertificates != null && this.entry.peerCertificates.length != 0) {
                return Arrays.asList((Object[]) this.entry.peerCertificates.clone());
            }
            throw new SSLPeerUnverifiedException(null);
        }

        public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
            if (this.entry.peerCertificates != null && this.entry.peerCertificates.length != 0) {
                return ((X509Certificate) this.entry.peerCertificates[0]).getSubjectX500Principal();
            }
            throw new SSLPeerUnverifiedException(null);
        }

        public List<Certificate> getLocalCertificateChain() {
            if (this.entry.localCertificates == null || this.entry.localCertificates.length == 0) {
                return null;
            }
            return Arrays.asList((Object[]) this.entry.localCertificates.clone());
        }

        public Principal getLocalPrincipal() {
            if (this.entry.localCertificates == null || this.entry.localCertificates.length == 0) {
                return null;
            }
            return ((X509Certificate) this.entry.localCertificates[0]).getSubjectX500Principal();
        }
    }
}
