package com.squareup.okhttp.internal.http;

import com.squareup.okhttp.internal.http.HeaderParser;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Map;

public final class RequestHeaders {
    private String acceptEncoding;
    private String connection;
    private int contentLength = -1;
    private String contentType;
    private boolean hasAuthorization;
    private final RawHeaders headers;
    private String host;
    private String ifModifiedSince;
    private String ifNoneMatch;
    /* access modifiers changed from: private */
    public int maxAgeSeconds = -1;
    /* access modifiers changed from: private */
    public int maxStaleSeconds = -1;
    /* access modifiers changed from: private */
    public int minFreshSeconds = -1;
    /* access modifiers changed from: private */
    public boolean noCache;
    /* access modifiers changed from: private */
    public boolean onlyIfCached;
    private String proxyAuthorization;
    private String transferEncoding;
    private final URI uri;
    private String userAgent;

    public RequestHeaders(URI uri2, RawHeaders headers2) {
        this.uri = uri2;
        this.headers = headers2;
        HeaderParser.CacheControlHandler handler = new HeaderParser.CacheControlHandler() {
            public void handle(String directive, String parameter) {
                if ("no-cache".equalsIgnoreCase(directive)) {
                    boolean unused = RequestHeaders.this.noCache = true;
                } else if ("max-age".equalsIgnoreCase(directive)) {
                    int unused2 = RequestHeaders.this.maxAgeSeconds = HeaderParser.parseSeconds(parameter);
                } else if ("max-stale".equalsIgnoreCase(directive)) {
                    int unused3 = RequestHeaders.this.maxStaleSeconds = HeaderParser.parseSeconds(parameter);
                } else if ("min-fresh".equalsIgnoreCase(directive)) {
                    int unused4 = RequestHeaders.this.minFreshSeconds = HeaderParser.parseSeconds(parameter);
                } else if ("only-if-cached".equalsIgnoreCase(directive)) {
                    boolean unused5 = RequestHeaders.this.onlyIfCached = true;
                }
            }
        };
        for (int i = 0; i < headers2.length(); i++) {
            String fieldName = headers2.getFieldName(i);
            String value = headers2.getValue(i);
            if ("Cache-Control".equalsIgnoreCase(fieldName)) {
                HeaderParser.parseCacheControl(value, handler);
            } else if ("Pragma".equalsIgnoreCase(fieldName)) {
                if ("no-cache".equalsIgnoreCase(value)) {
                    this.noCache = true;
                }
            } else if ("If-None-Match".equalsIgnoreCase(fieldName)) {
                this.ifNoneMatch = value;
            } else if ("If-Modified-Since".equalsIgnoreCase(fieldName)) {
                this.ifModifiedSince = value;
            } else if ("Authorization".equalsIgnoreCase(fieldName)) {
                this.hasAuthorization = true;
            } else if ("Content-Length".equalsIgnoreCase(fieldName)) {
                try {
                    this.contentLength = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                }
            } else if ("Transfer-Encoding".equalsIgnoreCase(fieldName)) {
                this.transferEncoding = value;
            } else if ("User-Agent".equalsIgnoreCase(fieldName)) {
                this.userAgent = value;
            } else if ("Host".equalsIgnoreCase(fieldName)) {
                this.host = value;
            } else if ("Connection".equalsIgnoreCase(fieldName)) {
                this.connection = value;
            } else if ("Accept-Encoding".equalsIgnoreCase(fieldName)) {
                this.acceptEncoding = value;
            } else if ("Content-Type".equalsIgnoreCase(fieldName)) {
                this.contentType = value;
            } else if ("Proxy-Authorization".equalsIgnoreCase(fieldName)) {
                this.proxyAuthorization = value;
            }
        }
    }

    public boolean isChunked() {
        return "chunked".equalsIgnoreCase(this.transferEncoding);
    }

    public boolean hasConnectionClose() {
        return "close".equalsIgnoreCase(this.connection);
    }

    public URI getUri() {
        return this.uri;
    }

    public RawHeaders getHeaders() {
        return this.headers;
    }

    public boolean isNoCache() {
        return this.noCache;
    }

    public int getMaxAgeSeconds() {
        return this.maxAgeSeconds;
    }

    public int getMaxStaleSeconds() {
        return this.maxStaleSeconds;
    }

    public int getMinFreshSeconds() {
        return this.minFreshSeconds;
    }

    public boolean isOnlyIfCached() {
        return this.onlyIfCached;
    }

    public boolean hasAuthorization() {
        return this.hasAuthorization;
    }

    public int getContentLength() {
        return this.contentLength;
    }

    public String getTransferEncoding() {
        return this.transferEncoding;
    }

    public String getUserAgent() {
        return this.userAgent;
    }

    public String getHost() {
        return this.host;
    }

    public String getConnection() {
        return this.connection;
    }

    public String getAcceptEncoding() {
        return this.acceptEncoding;
    }

    public String getContentType() {
        return this.contentType;
    }

    public String getIfModifiedSince() {
        return this.ifModifiedSince;
    }

    public String getIfNoneMatch() {
        return this.ifNoneMatch;
    }

    public String getProxyAuthorization() {
        return this.proxyAuthorization;
    }

    public void setChunked() {
        if (this.transferEncoding != null) {
            this.headers.removeAll("Transfer-Encoding");
        }
        this.headers.add("Transfer-Encoding", "chunked");
        this.transferEncoding = "chunked";
    }

    public void setContentLength(int contentLength2) {
        if (this.contentLength != -1) {
            this.headers.removeAll("Content-Length");
        }
        this.headers.add("Content-Length", Integer.toString(contentLength2));
        this.contentLength = contentLength2;
    }

    public void setUserAgent(String userAgent2) {
        if (this.userAgent != null) {
            this.headers.removeAll("User-Agent");
        }
        this.headers.add("User-Agent", userAgent2);
        this.userAgent = userAgent2;
    }

    public void setHost(String host2) {
        if (this.host != null) {
            this.headers.removeAll("Host");
        }
        this.headers.add("Host", host2);
        this.host = host2;
    }

    public void setConnection(String connection2) {
        if (this.connection != null) {
            this.headers.removeAll("Connection");
        }
        this.headers.add("Connection", connection2);
        this.connection = connection2;
    }

    public void setAcceptEncoding(String acceptEncoding2) {
        if (this.acceptEncoding != null) {
            this.headers.removeAll("Accept-Encoding");
        }
        this.headers.add("Accept-Encoding", acceptEncoding2);
        this.acceptEncoding = acceptEncoding2;
    }

    public void setContentType(String contentType2) {
        if (this.contentType != null) {
            this.headers.removeAll("Content-Type");
        }
        this.headers.add("Content-Type", contentType2);
        this.contentType = contentType2;
    }

    public void setIfModifiedSince(Date date) {
        if (this.ifModifiedSince != null) {
            this.headers.removeAll("If-Modified-Since");
        }
        String formattedDate = HttpDate.format(date);
        this.headers.add("If-Modified-Since", formattedDate);
        this.ifModifiedSince = formattedDate;
    }

    public void setIfNoneMatch(String ifNoneMatch2) {
        if (this.ifNoneMatch != null) {
            this.headers.removeAll("If-None-Match");
        }
        this.headers.add("If-None-Match", ifNoneMatch2);
        this.ifNoneMatch = ifNoneMatch2;
    }

    public boolean hasConditions() {
        return (this.ifModifiedSince == null && this.ifNoneMatch == null) ? false : true;
    }

    public void addCookies(Map<String, List<String>> allCookieHeaders) {
        for (Map.Entry<String, List<String>> entry : allCookieHeaders.entrySet()) {
            String key = (String) entry.getKey();
            if ("Cookie".equalsIgnoreCase(key) || "Cookie2".equalsIgnoreCase(key)) {
                this.headers.addAll(key, (List) entry.getValue());
            }
        }
    }
}
