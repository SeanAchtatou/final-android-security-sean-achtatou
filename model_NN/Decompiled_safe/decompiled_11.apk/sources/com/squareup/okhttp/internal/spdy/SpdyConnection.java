package com.squareup.okhttp.internal.spdy;

import com.squareup.okhttp.internal.NamedRunnable;
import com.squareup.okhttp.internal.Util;
import com.squareup.okhttp.internal.spdy.SpdyReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class SpdyConnection implements Closeable {
    static final /* synthetic */ boolean $assertionsDisabled;
    static final int FLAG_FIN = 1;
    static final int FLAG_UNIDIRECTIONAL = 2;
    static final int GOAWAY_INTERNAL_ERROR = 2;
    static final int GOAWAY_OK = 0;
    static final int GOAWAY_PROTOCOL_ERROR = 1;
    static final int TYPE_CREDENTIAL = 16;
    static final int TYPE_DATA = 0;
    static final int TYPE_GOAWAY = 7;
    static final int TYPE_HEADERS = 8;
    static final int TYPE_NOOP = 5;
    static final int TYPE_PING = 6;
    static final int TYPE_RST_STREAM = 3;
    static final int TYPE_SETTINGS = 4;
    static final int TYPE_SYN_REPLY = 2;
    static final int TYPE_SYN_STREAM = 1;
    static final int TYPE_WINDOW_UPDATE = 9;
    static final int VERSION = 3;
    /* access modifiers changed from: private */
    public static final ExecutorService executor = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), Executors.defaultThreadFactory());
    final boolean client;
    /* access modifiers changed from: private */
    public final IncomingStreamHandler handler;
    /* access modifiers changed from: private */
    public final String hostName;
    private long idleStartTimeNs;
    /* access modifiers changed from: private */
    public int lastGoodStreamId;
    private int nextPingId;
    private int nextStreamId;
    private Map<Integer, Ping> pings;
    Settings settings;
    /* access modifiers changed from: private */
    public boolean shutdown;
    /* access modifiers changed from: private */
    public final SpdyReader spdyReader;
    private final SpdyWriter spdyWriter;
    /* access modifiers changed from: private */
    public final Map<Integer, SpdyStream> streams;

    static {
        boolean z;
        if (!SpdyConnection.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        $assertionsDisabled = z;
    }

    private SpdyConnection(Builder builder) {
        int i = 1;
        this.streams = new HashMap();
        this.idleStartTimeNs = System.nanoTime();
        this.client = builder.client;
        this.handler = builder.handler;
        this.spdyReader = new SpdyReader(builder.in);
        this.spdyWriter = new SpdyWriter(builder.out);
        this.nextStreamId = builder.client ? 1 : 2;
        this.nextPingId = !builder.client ? 2 : i;
        this.hostName = builder.hostName;
        new Thread(new Reader(), "Spdy Reader " + this.hostName).start();
    }

    public synchronized int openStreamCount() {
        return this.streams.size();
    }

    /* access modifiers changed from: private */
    public synchronized SpdyStream getStream(int id) {
        return this.streams.get(Integer.valueOf(id));
    }

    /* access modifiers changed from: package-private */
    public synchronized SpdyStream removeStream(int streamId) {
        SpdyStream stream;
        stream = this.streams.remove(Integer.valueOf(streamId));
        if (stream != null && this.streams.isEmpty()) {
            setIdle(true);
        }
        return stream;
    }

    private synchronized void setIdle(boolean value) {
        this.idleStartTimeNs = value ? System.nanoTime() : 0;
    }

    public synchronized boolean isIdle() {
        return this.idleStartTimeNs != 0 ? true : $assertionsDisabled;
    }

    public synchronized long getIdleStartTimeNs() {
        return this.idleStartTimeNs;
    }

    public SpdyStream newStream(List<String> requestHeaders, boolean out, boolean in) throws IOException {
        int i;
        int i2;
        int streamId;
        SpdyStream stream;
        if (out) {
            i = 0;
        } else {
            i = 1;
        }
        if (in) {
            i2 = 0;
        } else {
            i2 = 2;
        }
        int flags = i | i2;
        synchronized (this.spdyWriter) {
            synchronized (this) {
                if (this.shutdown) {
                    throw new IOException("shutdown");
                }
                streamId = this.nextStreamId;
                this.nextStreamId += 2;
                stream = new SpdyStream(streamId, this, flags, 0, 0, requestHeaders, this.settings);
                if (stream.isOpen()) {
                    this.streams.put(Integer.valueOf(streamId), stream);
                    setIdle($assertionsDisabled);
                }
            }
            this.spdyWriter.synStream(flags, streamId, 0, 0, 0, requestHeaders);
        }
        return stream;
    }

    /* access modifiers changed from: package-private */
    public void writeSynReply(int streamId, int flags, List<String> alternating) throws IOException {
        this.spdyWriter.synReply(flags, streamId, alternating);
    }

    /* access modifiers changed from: package-private */
    public void writeFrame(byte[] bytes, int offset, int length) throws IOException {
        synchronized (this.spdyWriter) {
            this.spdyWriter.out.write(bytes, offset, length);
        }
    }

    /* access modifiers changed from: package-private */
    public void writeSynResetLater(final int streamId, final int statusCode) {
        executor.submit(new NamedRunnable(String.format("Spdy Writer %s stream %d", this.hostName, Integer.valueOf(streamId))) {
            public void execute() {
                try {
                    SpdyConnection.this.writeSynReset(streamId, statusCode);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void writeSynReset(int streamId, int statusCode) throws IOException {
        this.spdyWriter.rstStream(streamId, statusCode);
    }

    /* access modifiers changed from: package-private */
    public void writeWindowUpdateLater(final int streamId, final int deltaWindowSize) {
        executor.submit(new NamedRunnable(String.format("Spdy Writer %s stream %d", this.hostName, Integer.valueOf(streamId))) {
            public void execute() {
                try {
                    SpdyConnection.this.writeWindowUpdate(streamId, deltaWindowSize);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void writeWindowUpdate(int streamId, int deltaWindowSize) throws IOException {
        this.spdyWriter.windowUpdate(streamId, deltaWindowSize);
    }

    public Ping ping() throws IOException {
        int pingId;
        Ping ping = new Ping();
        synchronized (this) {
            if (this.shutdown) {
                throw new IOException("shutdown");
            }
            pingId = this.nextPingId;
            this.nextPingId += 2;
            if (this.pings == null) {
                this.pings = new HashMap();
            }
            this.pings.put(Integer.valueOf(pingId), ping);
        }
        writePing(pingId, ping);
        return ping;
    }

    /* access modifiers changed from: private */
    public void writePingLater(final int streamId, final Ping ping) {
        executor.submit(new NamedRunnable(String.format("Spdy Writer %s ping %d", this.hostName, Integer.valueOf(streamId))) {
            public void execute() {
                try {
                    SpdyConnection.this.writePing(streamId, ping);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void writePing(int id, Ping ping) throws IOException {
        synchronized (this.spdyWriter) {
            if (ping != null) {
                ping.send();
            }
            this.spdyWriter.ping(0, id);
        }
    }

    /* access modifiers changed from: private */
    public synchronized Ping removePing(int id) {
        return this.pings != null ? this.pings.remove(Integer.valueOf(id)) : null;
    }

    public void noop() throws IOException {
        this.spdyWriter.noop();
    }

    public void flush() throws IOException {
        synchronized (this.spdyWriter) {
            this.spdyWriter.out.flush();
        }
    }

    public void shutdown(int statusCode) throws IOException {
        synchronized (this.spdyWriter) {
            synchronized (this) {
                if (!this.shutdown) {
                    this.shutdown = true;
                    int lastGoodStreamId2 = this.lastGoodStreamId;
                    this.spdyWriter.goAway(0, lastGoodStreamId2, statusCode);
                }
            }
        }
    }

    public void close() throws IOException {
        close(0, 5);
    }

    /* JADX WARN: Type inference failed for: r10v9, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r10v13, types: [java.lang.Object[]] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void close(int r13, int r14) throws java.io.IOException {
        /*
            r12 = this;
            boolean r10 = com.squareup.okhttp.internal.spdy.SpdyConnection.$assertionsDisabled
            if (r10 != 0) goto L_0x0010
            boolean r10 = java.lang.Thread.holdsLock(r12)
            if (r10 == 0) goto L_0x0010
            java.lang.AssertionError r10 = new java.lang.AssertionError
            r10.<init>()
            throw r10
        L_0x0010:
            r9 = 0
            r12.shutdown(r13)     // Catch:{ IOException -> 0x006b }
        L_0x0014:
            r8 = 0
            r6 = 0
            monitor-enter(r12)
            java.util.Map<java.lang.Integer, com.squareup.okhttp.internal.spdy.SpdyStream> r10 = r12.streams     // Catch:{ all -> 0x006e }
            boolean r10 = r10.isEmpty()     // Catch:{ all -> 0x006e }
            if (r10 != 0) goto L_0x003e
            java.util.Map<java.lang.Integer, com.squareup.okhttp.internal.spdy.SpdyStream> r10 = r12.streams     // Catch:{ all -> 0x006e }
            java.util.Collection r10 = r10.values()     // Catch:{ all -> 0x006e }
            java.util.Map<java.lang.Integer, com.squareup.okhttp.internal.spdy.SpdyStream> r11 = r12.streams     // Catch:{ all -> 0x006e }
            int r11 = r11.size()     // Catch:{ all -> 0x006e }
            com.squareup.okhttp.internal.spdy.SpdyStream[] r11 = new com.squareup.okhttp.internal.spdy.SpdyStream[r11]     // Catch:{ all -> 0x006e }
            java.lang.Object[] r10 = r10.toArray(r11)     // Catch:{ all -> 0x006e }
            r0 = r10
            com.squareup.okhttp.internal.spdy.SpdyStream[] r0 = (com.squareup.okhttp.internal.spdy.SpdyStream[]) r0     // Catch:{ all -> 0x006e }
            r8 = r0
            java.util.Map<java.lang.Integer, com.squareup.okhttp.internal.spdy.SpdyStream> r10 = r12.streams     // Catch:{ all -> 0x006e }
            r10.clear()     // Catch:{ all -> 0x006e }
            r10 = 0
            r12.setIdle(r10)     // Catch:{ all -> 0x006e }
        L_0x003e:
            java.util.Map<java.lang.Integer, com.squareup.okhttp.internal.spdy.Ping> r10 = r12.pings     // Catch:{ all -> 0x006e }
            if (r10 == 0) goto L_0x005b
            java.util.Map<java.lang.Integer, com.squareup.okhttp.internal.spdy.Ping> r10 = r12.pings     // Catch:{ all -> 0x006e }
            java.util.Collection r10 = r10.values()     // Catch:{ all -> 0x006e }
            java.util.Map<java.lang.Integer, com.squareup.okhttp.internal.spdy.Ping> r11 = r12.pings     // Catch:{ all -> 0x006e }
            int r11 = r11.size()     // Catch:{ all -> 0x006e }
            com.squareup.okhttp.internal.spdy.Ping[] r11 = new com.squareup.okhttp.internal.spdy.Ping[r11]     // Catch:{ all -> 0x006e }
            java.lang.Object[] r10 = r10.toArray(r11)     // Catch:{ all -> 0x006e }
            r0 = r10
            com.squareup.okhttp.internal.spdy.Ping[] r0 = (com.squareup.okhttp.internal.spdy.Ping[]) r0     // Catch:{ all -> 0x006e }
            r6 = r0
            r10 = 0
            r12.pings = r10     // Catch:{ all -> 0x006e }
        L_0x005b:
            monitor-exit(r12)     // Catch:{ all -> 0x006e }
            if (r8 == 0) goto L_0x0076
            r1 = r8
            int r4 = r1.length
            r3 = 0
        L_0x0061:
            if (r3 >= r4) goto L_0x0076
            r7 = r1[r3]
            r7.close(r14)     // Catch:{ IOException -> 0x0071 }
        L_0x0068:
            int r3 = r3 + 1
            goto L_0x0061
        L_0x006b:
            r2 = move-exception
            r9 = r2
            goto L_0x0014
        L_0x006e:
            r10 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x006e }
            throw r10
        L_0x0071:
            r2 = move-exception
            if (r9 == 0) goto L_0x0068
            r9 = r2
            goto L_0x0068
        L_0x0076:
            if (r6 == 0) goto L_0x0085
            r1 = r6
            int r4 = r1.length
            r3 = 0
        L_0x007b:
            if (r3 >= r4) goto L_0x0085
            r5 = r1[r3]
            r5.cancel()
            int r3 = r3 + 1
            goto L_0x007b
        L_0x0085:
            com.squareup.okhttp.internal.spdy.SpdyReader r10 = r12.spdyReader     // Catch:{ IOException -> 0x0092 }
            r10.close()     // Catch:{ IOException -> 0x0092 }
        L_0x008a:
            com.squareup.okhttp.internal.spdy.SpdyWriter r10 = r12.spdyWriter     // Catch:{ IOException -> 0x0095 }
            r10.close()     // Catch:{ IOException -> 0x0095 }
        L_0x008f:
            if (r9 == 0) goto L_0x009a
            throw r9
        L_0x0092:
            r2 = move-exception
            r9 = r2
            goto L_0x008a
        L_0x0095:
            r2 = move-exception
            if (r9 != 0) goto L_0x008f
            r9 = r2
            goto L_0x008f
        L_0x009a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.SpdyConnection.close(int, int):void");
    }

    public static class Builder {
        public boolean client;
        /* access modifiers changed from: private */
        public IncomingStreamHandler handler;
        /* access modifiers changed from: private */
        public String hostName;
        /* access modifiers changed from: private */
        public InputStream in;
        /* access modifiers changed from: private */
        public OutputStream out;

        public Builder(boolean client2, Socket socket) throws IOException {
            this("", client2, socket.getInputStream(), socket.getOutputStream());
        }

        public Builder(boolean client2, InputStream in2, OutputStream out2) {
            this("", client2, in2, out2);
        }

        public Builder(String hostName2, boolean client2, Socket socket) throws IOException {
            this(hostName2, client2, socket.getInputStream(), socket.getOutputStream());
        }

        public Builder(String hostName2, boolean client2, InputStream in2, OutputStream out2) {
            this.handler = IncomingStreamHandler.REFUSE_INCOMING_STREAMS;
            this.hostName = hostName2;
            this.client = client2;
            this.in = in2;
            this.out = out2;
        }

        public Builder handler(IncomingStreamHandler handler2) {
            this.handler = handler2;
            return this;
        }

        public SpdyConnection build() {
            return new SpdyConnection(this);
        }
    }

    private class Reader implements Runnable, SpdyReader.Handler {
        private Reader() {
        }

        public void run() {
            do {
                try {
                } catch (IOException e) {
                    SpdyConnection.this.close(1, 1);
                    return;
                } catch (Throwable th) {
                    try {
                        SpdyConnection.this.close(2, 6);
                    } catch (IOException e2) {
                    }
                    throw th;
                }
            } while (SpdyConnection.this.spdyReader.nextFrame(this));
            try {
                SpdyConnection.this.close(0, 5);
            } catch (IOException e3) {
            }
        }

        public void data(int flags, int streamId, InputStream in, int length) throws IOException {
            SpdyStream dataStream = SpdyConnection.this.getStream(streamId);
            if (dataStream == null) {
                SpdyConnection.this.writeSynResetLater(streamId, 2);
                Util.skipByReading(in, (long) length);
                return;
            }
            dataStream.receiveData(in, length);
            if ((flags & 1) != 0) {
                dataStream.receiveFin();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0043, code lost:
            com.squareup.okhttp.internal.spdy.SpdyConnection.access$1500().submit(new com.squareup.okhttp.internal.spdy.SpdyConnection.Reader.AnonymousClass1(r10, java.lang.String.format("Callback %s stream %d", com.squareup.okhttp.internal.spdy.SpdyConnection.access$1300(r10.this$0), java.lang.Integer.valueOf(r12))));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0034, code lost:
            if (r8 == null) goto L_0x0043;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0036, code lost:
            r8.closeLater(1);
            r10.this$0.removeStream(r12);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void synStream(int r11, int r12, int r13, int r14, int r15, java.util.List<java.lang.String> r16) {
            /*
                r10 = this;
                com.squareup.okhttp.internal.spdy.SpdyConnection r9 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                monitor-enter(r9)
                com.squareup.okhttp.internal.spdy.SpdyStream r0 = new com.squareup.okhttp.internal.spdy.SpdyStream     // Catch:{ all -> 0x0040 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r2 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x0040 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x0040 }
                com.squareup.okhttp.internal.spdy.Settings r7 = r1.settings     // Catch:{ all -> 0x0040 }
                r1 = r12
                r3 = r11
                r4 = r14
                r5 = r15
                r6 = r16
                r0.<init>(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0040 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x0040 }
                boolean r1 = r1.shutdown     // Catch:{ all -> 0x0040 }
                if (r1 == 0) goto L_0x001e
                monitor-exit(r9)     // Catch:{ all -> 0x0040 }
            L_0x001d:
                return
            L_0x001e:
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x0040 }
                int unused = r1.lastGoodStreamId = r12     // Catch:{ all -> 0x0040 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x0040 }
                java.util.Map r1 = r1.streams     // Catch:{ all -> 0x0040 }
                java.lang.Integer r2 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x0040 }
                java.lang.Object r8 = r1.put(r2, r0)     // Catch:{ all -> 0x0040 }
                com.squareup.okhttp.internal.spdy.SpdyStream r8 = (com.squareup.okhttp.internal.spdy.SpdyStream) r8     // Catch:{ all -> 0x0040 }
                monitor-exit(r9)     // Catch:{ all -> 0x0040 }
                if (r8 == 0) goto L_0x0043
                r1 = 1
                r8.closeLater(r1)
                com.squareup.okhttp.internal.spdy.SpdyConnection r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                r1.removeStream(r12)
                goto L_0x001d
            L_0x0040:
                r1 = move-exception
                monitor-exit(r9)     // Catch:{ all -> 0x0040 }
                throw r1
            L_0x0043:
                java.util.concurrent.ExecutorService r1 = com.squareup.okhttp.internal.spdy.SpdyConnection.executor
                com.squareup.okhttp.internal.spdy.SpdyConnection$Reader$1 r2 = new com.squareup.okhttp.internal.spdy.SpdyConnection$Reader$1
                java.lang.String r3 = "Callback %s stream %d"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]
                r5 = 0
                com.squareup.okhttp.internal.spdy.SpdyConnection r6 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                java.lang.String r6 = r6.hostName
                r4[r5] = r6
                r5 = 1
                java.lang.Integer r6 = java.lang.Integer.valueOf(r12)
                r4[r5] = r6
                java.lang.String r3 = java.lang.String.format(r3, r4)
                r2.<init>(r3, r0)
                r1.submit(r2)
                goto L_0x001d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.SpdyConnection.Reader.synStream(int, int, int, int, int, java.util.List):void");
        }

        public void synReply(int flags, int streamId, List<String> nameValueBlock) throws IOException {
            SpdyStream replyStream = SpdyConnection.this.getStream(streamId);
            if (replyStream == null) {
                SpdyConnection.this.writeSynResetLater(streamId, 2);
                return;
            }
            replyStream.receiveReply(nameValueBlock);
            if ((flags & 1) != 0) {
                replyStream.receiveFin();
            }
        }

        public void headers(int flags, int streamId, List<String> nameValueBlock) throws IOException {
            SpdyStream replyStream = SpdyConnection.this.getStream(streamId);
            if (replyStream != null) {
                replyStream.receiveHeaders(nameValueBlock);
            }
        }

        public void rstStream(int flags, int streamId, int statusCode) {
            SpdyStream rstStream = SpdyConnection.this.removeStream(streamId);
            if (rstStream != null) {
                rstStream.receiveRstStream(statusCode);
            }
        }

        /* JADX WARN: Type inference failed for: r6v13, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void settings(int r10, com.squareup.okhttp.internal.spdy.Settings r11) {
            /*
                r9 = this;
                r5 = 0
                com.squareup.okhttp.internal.spdy.SpdyConnection r7 = com.squareup.okhttp.internal.spdy.SpdyConnection.this
                monitor-enter(r7)
                com.squareup.okhttp.internal.spdy.SpdyConnection r6 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x005c }
                com.squareup.okhttp.internal.spdy.Settings r6 = r6.settings     // Catch:{ all -> 0x005c }
                if (r6 == 0) goto L_0x000e
                r6 = r10 & 1
                if (r6 == 0) goto L_0x0054
            L_0x000e:
                com.squareup.okhttp.internal.spdy.SpdyConnection r6 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x005c }
                r6.settings = r11     // Catch:{ all -> 0x005c }
            L_0x0012:
                com.squareup.okhttp.internal.spdy.SpdyConnection r6 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x005c }
                java.util.Map r6 = r6.streams     // Catch:{ all -> 0x005c }
                boolean r6 = r6.isEmpty()     // Catch:{ all -> 0x005c }
                if (r6 != 0) goto L_0x003c
                com.squareup.okhttp.internal.spdy.SpdyConnection r6 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x005c }
                java.util.Map r6 = r6.streams     // Catch:{ all -> 0x005c }
                java.util.Collection r6 = r6.values()     // Catch:{ all -> 0x005c }
                com.squareup.okhttp.internal.spdy.SpdyConnection r8 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x005c }
                java.util.Map r8 = r8.streams     // Catch:{ all -> 0x005c }
                int r8 = r8.size()     // Catch:{ all -> 0x005c }
                com.squareup.okhttp.internal.spdy.SpdyStream[] r8 = new com.squareup.okhttp.internal.spdy.SpdyStream[r8]     // Catch:{ all -> 0x005c }
                java.lang.Object[] r6 = r6.toArray(r8)     // Catch:{ all -> 0x005c }
                r0 = r6
                com.squareup.okhttp.internal.spdy.SpdyStream[] r0 = (com.squareup.okhttp.internal.spdy.SpdyStream[]) r0     // Catch:{ all -> 0x005c }
                r5 = r0
            L_0x003c:
                monitor-exit(r7)     // Catch:{ all -> 0x005c }
                if (r5 == 0) goto L_0x0065
                r1 = r5
                int r3 = r1.length
                r2 = 0
            L_0x0042:
                if (r2 >= r3) goto L_0x0065
                r4 = r1[r2]
                monitor-enter(r4)
                monitor-enter(r9)     // Catch:{ all -> 0x0062 }
                com.squareup.okhttp.internal.spdy.SpdyConnection r6 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x005f }
                com.squareup.okhttp.internal.spdy.Settings r6 = r6.settings     // Catch:{ all -> 0x005f }
                r4.receiveSettings(r6)     // Catch:{ all -> 0x005f }
                monitor-exit(r9)     // Catch:{ all -> 0x005f }
                monitor-exit(r4)     // Catch:{ all -> 0x0062 }
                int r2 = r2 + 1
                goto L_0x0042
            L_0x0054:
                com.squareup.okhttp.internal.spdy.SpdyConnection r6 = com.squareup.okhttp.internal.spdy.SpdyConnection.this     // Catch:{ all -> 0x005c }
                com.squareup.okhttp.internal.spdy.Settings r6 = r6.settings     // Catch:{ all -> 0x005c }
                r6.merge(r11)     // Catch:{ all -> 0x005c }
                goto L_0x0012
            L_0x005c:
                r6 = move-exception
                monitor-exit(r7)     // Catch:{ all -> 0x005c }
                throw r6
            L_0x005f:
                r6 = move-exception
                monitor-exit(r9)     // Catch:{ all -> 0x005f }
                throw r6     // Catch:{ all -> 0x0062 }
            L_0x0062:
                r6 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0062 }
                throw r6
            L_0x0065:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.SpdyConnection.Reader.settings(int, com.squareup.okhttp.internal.spdy.Settings):void");
        }

        public void noop() {
        }

        public void ping(int flags, int streamId) {
            boolean z = true;
            boolean z2 = SpdyConnection.this.client;
            if (streamId % 2 != 1) {
                z = SpdyConnection.$assertionsDisabled;
            }
            if (z2 != z) {
                SpdyConnection.this.writePingLater(streamId, null);
                return;
            }
            Ping ping = SpdyConnection.this.removePing(streamId);
            if (ping != null) {
                ping.receive();
            }
        }

        public void goAway(int flags, int lastGoodStreamId, int statusCode) {
            synchronized (SpdyConnection.this) {
                boolean unused = SpdyConnection.this.shutdown = true;
                Iterator<Map.Entry<Integer, SpdyStream>> i = SpdyConnection.this.streams.entrySet().iterator();
                while (i.hasNext()) {
                    Map.Entry<Integer, SpdyStream> entry = i.next();
                    if (((Integer) entry.getKey()).intValue() > lastGoodStreamId && ((SpdyStream) entry.getValue()).isLocallyInitiated()) {
                        ((SpdyStream) entry.getValue()).receiveRstStream(3);
                        i.remove();
                    }
                }
            }
        }

        public void windowUpdate(int flags, int streamId, int deltaWindowSize) {
            SpdyStream stream = SpdyConnection.this.getStream(streamId);
            if (stream != null) {
                stream.receiveWindowUpdate(deltaWindowSize);
            }
        }
    }
}
