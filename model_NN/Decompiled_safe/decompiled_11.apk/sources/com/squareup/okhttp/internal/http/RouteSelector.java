package com.squareup.okhttp.internal.http;

import com.squareup.okhttp.Address;
import com.squareup.okhttp.Connection;
import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.Route;
import com.squareup.okhttp.internal.Dns;
import com.squareup.okhttp.internal.Util;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.net.ssl.SSLHandshakeException;

public final class RouteSelector {
    private static final int TLS_MODE_COMPATIBLE = 0;
    private static final int TLS_MODE_MODERN = 1;
    private static final int TLS_MODE_NULL = -1;
    private final Address address;
    private final Dns dns;
    private final Set<Route> failedRoutes;
    private boolean hasNextProxy;
    private InetSocketAddress lastInetSocketAddress;
    private Proxy lastProxy;
    private int nextSocketAddressIndex;
    private int nextTlsMode = -1;
    private final ConnectionPool pool;
    private final List<Route> postponedRoutes;
    private final ProxySelector proxySelector;
    private Iterator<Proxy> proxySelectorProxies;
    private InetAddress[] socketAddresses;
    private int socketPort;
    private final URI uri;
    private Proxy userSpecifiedProxy;

    public RouteSelector(Address address2, URI uri2, ProxySelector proxySelector2, ConnectionPool pool2, Dns dns2, Set<Route> failedRoutes2) {
        this.address = address2;
        this.uri = uri2;
        this.proxySelector = proxySelector2;
        this.pool = pool2;
        this.dns = dns2;
        this.failedRoutes = failedRoutes2;
        this.postponedRoutes = new LinkedList();
        resetNextProxy(uri2, address2.getProxy());
    }

    public boolean hasNext() {
        return hasNextTlsMode() || hasNextInetSocketAddress() || hasNextProxy() || hasNextPostponed();
    }

    public Connection next() throws IOException {
        boolean modernTls = true;
        Connection pooled = this.pool.get(this.address);
        if (pooled != null) {
            return pooled;
        }
        if (!hasNextTlsMode()) {
            if (!hasNextInetSocketAddress()) {
                if (hasNextProxy()) {
                    this.lastProxy = nextProxy();
                    resetNextInetSocketAddress(this.lastProxy);
                } else if (hasNextPostponed()) {
                    return new Connection(nextPostponed());
                } else {
                    throw new NoSuchElementException();
                }
            }
            this.lastInetSocketAddress = nextInetSocketAddress();
            resetNextTlsMode();
        }
        if (nextTlsMode() != 1) {
            modernTls = false;
        }
        Route route = new Route(this.address, this.lastProxy, this.lastInetSocketAddress, modernTls);
        if (!this.failedRoutes.contains(route)) {
            return new Connection(route);
        }
        this.postponedRoutes.add(route);
        return next();
    }

    public void connectFailed(Connection connection, IOException failure) {
        Route failedRoute = connection.getRoute();
        if (!(failedRoute.getProxy().type() == Proxy.Type.DIRECT || this.proxySelector == null)) {
            this.proxySelector.connectFailed(this.uri, failedRoute.getProxy().address(), failure);
        }
        this.failedRoutes.add(failedRoute);
        if (!(failure instanceof SSLHandshakeException)) {
            this.failedRoutes.add(failedRoute.flipTlsMode());
        }
    }

    private void resetNextProxy(URI uri2, Proxy proxy) {
        this.hasNextProxy = true;
        if (proxy != null) {
            this.userSpecifiedProxy = proxy;
            return;
        }
        List<Proxy> proxyList = this.proxySelector.select(uri2);
        if (proxyList != null) {
            this.proxySelectorProxies = proxyList.iterator();
        }
    }

    private boolean hasNextProxy() {
        return this.hasNextProxy;
    }

    private Proxy nextProxy() {
        if (this.userSpecifiedProxy != null) {
            this.hasNextProxy = false;
            return this.userSpecifiedProxy;
        }
        if (this.proxySelectorProxies != null) {
            while (this.proxySelectorProxies.hasNext()) {
                Proxy candidate = this.proxySelectorProxies.next();
                if (candidate.type() != Proxy.Type.DIRECT) {
                    return candidate;
                }
            }
        }
        this.hasNextProxy = false;
        return Proxy.NO_PROXY;
    }

    private void resetNextInetSocketAddress(Proxy proxy) throws UnknownHostException {
        String socketHost;
        this.socketAddresses = null;
        if (proxy.type() == Proxy.Type.DIRECT) {
            socketHost = this.uri.getHost();
            this.socketPort = Util.getEffectivePort(this.uri);
        } else {
            SocketAddress proxyAddress = proxy.address();
            if (!(proxyAddress instanceof InetSocketAddress)) {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + proxyAddress.getClass());
            }
            InetSocketAddress proxySocketAddress = (InetSocketAddress) proxyAddress;
            socketHost = proxySocketAddress.getHostName();
            this.socketPort = proxySocketAddress.getPort();
        }
        this.socketAddresses = this.dns.getAllByName(socketHost);
        this.nextSocketAddressIndex = 0;
    }

    private boolean hasNextInetSocketAddress() {
        return this.socketAddresses != null;
    }

    private InetSocketAddress nextInetSocketAddress() throws UnknownHostException {
        InetAddress[] inetAddressArr = this.socketAddresses;
        int i = this.nextSocketAddressIndex;
        this.nextSocketAddressIndex = i + 1;
        InetSocketAddress result = new InetSocketAddress(inetAddressArr[i], this.socketPort);
        if (this.nextSocketAddressIndex == this.socketAddresses.length) {
            this.socketAddresses = null;
            this.nextSocketAddressIndex = 0;
        }
        return result;
    }

    private void resetNextTlsMode() {
        this.nextTlsMode = this.address.getSslSocketFactory() != null ? 1 : 0;
    }

    private boolean hasNextTlsMode() {
        return this.nextTlsMode != -1;
    }

    private int nextTlsMode() {
        if (this.nextTlsMode == 1) {
            this.nextTlsMode = 0;
            return 1;
        } else if (this.nextTlsMode == 0) {
            this.nextTlsMode = -1;
            return 0;
        } else {
            throw new AssertionError();
        }
    }

    private boolean hasNextPostponed() {
        return !this.postponedRoutes.isEmpty();
    }

    private Route nextPostponed() {
        return this.postponedRoutes.remove(0);
    }
}
