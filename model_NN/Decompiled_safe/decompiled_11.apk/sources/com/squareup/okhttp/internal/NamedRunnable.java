package com.squareup.okhttp.internal;

public abstract class NamedRunnable implements Runnable {
    private String name;

    /* access modifiers changed from: protected */
    public abstract void execute();

    public NamedRunnable(String name2) {
        this.name = name2;
    }

    public final void run() {
        String oldName = Thread.currentThread().getName();
        Thread.currentThread().setName(this.name);
        try {
            execute();
        } finally {
            Thread.currentThread().setName(oldName);
        }
    }
}
