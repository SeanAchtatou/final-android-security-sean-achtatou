package com.squareup.okhttp.internal.spdy;

final class Settings {
    static final int CLIENT_CERTIFICATE_VECTOR_SIZE = 8;
    static final int COUNT = 9;
    static final int CURRENT_CWND = 5;
    static final int DEFAULT_INITIAL_WINDOW_SIZE = 65536;
    static final int DOWNLOAD_BANDWIDTH = 2;
    static final int DOWNLOAD_RETRANS_RATE = 6;
    static final int FLAG_CLEAR_PREVIOUSLY_PERSISTED_SETTINGS = 1;
    static final int INITIAL_WINDOW_SIZE = 7;
    static final int MAX_CONCURRENT_STREAMS = 4;
    static final int PERSISTED = 2;
    static final int PERSIST_VALUE = 1;
    static final int ROUND_TRIP_TIME = 3;
    static final int UPLOAD_BANDWIDTH = 1;
    private int persistValue;
    private int persisted;
    private int set;
    private final int[] values = new int[9];

    Settings() {
    }

    /* access modifiers changed from: package-private */
    public void set(int id, int idFlags, int value) {
        if (id < this.values.length) {
            int bit = 1 << id;
            this.set |= bit;
            if ((idFlags & 1) != 0) {
                this.persistValue |= bit;
            } else {
                this.persistValue &= bit ^ -1;
            }
            if ((idFlags & 2) != 0) {
                this.persisted |= bit;
            } else {
                this.persisted &= bit ^ -1;
            }
            this.values[id] = value;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isSet(int id) {
        if ((this.set & (1 << id)) != 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int get(int id) {
        return this.values[id];
    }

    /* access modifiers changed from: package-private */
    public int flags(int id) {
        int result = 0;
        if (isPersisted(id)) {
            result = 0 | 2;
        }
        if (persistValue(id)) {
            return result | 1;
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public int size() {
        return Integer.bitCount(this.set);
    }

    /* access modifiers changed from: package-private */
    public int getUploadBandwidth(int defaultValue) {
        return (this.set & 2) != 0 ? this.values[1] : defaultValue;
    }

    /* access modifiers changed from: package-private */
    public int getDownloadBandwidth(int defaultValue) {
        return (this.set & 4) != 0 ? this.values[2] : defaultValue;
    }

    /* access modifiers changed from: package-private */
    public int getRoundTripTime(int defaultValue) {
        return (this.set & 8) != 0 ? this.values[3] : defaultValue;
    }

    /* access modifiers changed from: package-private */
    public int getMaxConcurrentStreams(int defaultValue) {
        return (this.set & 16) != 0 ? this.values[4] : defaultValue;
    }

    /* access modifiers changed from: package-private */
    public int getCurrentCwnd(int defaultValue) {
        return (this.set & 32) != 0 ? this.values[5] : defaultValue;
    }

    /* access modifiers changed from: package-private */
    public int getDownloadRetransRate(int defaultValue) {
        return (this.set & 64) != 0 ? this.values[6] : defaultValue;
    }

    /* access modifiers changed from: package-private */
    public int getInitialWindowSize(int defaultValue) {
        return (this.set & 128) != 0 ? this.values[7] : defaultValue;
    }

    /* access modifiers changed from: package-private */
    public int getClientCertificateVectorSize(int defaultValue) {
        return (this.set & 256) != 0 ? this.values[8] : defaultValue;
    }

    /* access modifiers changed from: package-private */
    public boolean persistValue(int id) {
        if ((this.persistValue & (1 << id)) != 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isPersisted(int id) {
        if ((this.persisted & (1 << id)) != 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void merge(Settings other) {
        for (int i = 0; i < 9; i++) {
            if (other.isSet(i)) {
                set(i, other.flags(i), other.get(i));
            }
        }
    }
}
