package com.squareup.okhttp.internal.http;

import com.squareup.okhttp.internal.Util;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public final class RawHeaders {
    private static final Comparator<String> FIELD_NAME_COMPARATOR = new Comparator<String>() {
        public int compare(String a, String b) {
            if (a == b) {
                return 0;
            }
            if (a == null) {
                return -1;
            }
            if (b == null) {
                return 1;
            }
            return String.CASE_INSENSITIVE_ORDER.compare(a, b);
        }
    };
    private int httpMinorVersion = 1;
    private final List<String> namesAndValues = new ArrayList(20);
    private String requestLine;
    private int responseCode = -1;
    private String responseMessage;
    private String statusLine;

    public RawHeaders() {
    }

    public RawHeaders(RawHeaders copyFrom) {
        this.namesAndValues.addAll(copyFrom.namesAndValues);
        this.requestLine = copyFrom.requestLine;
        this.statusLine = copyFrom.statusLine;
        this.httpMinorVersion = copyFrom.httpMinorVersion;
        this.responseCode = copyFrom.responseCode;
        this.responseMessage = copyFrom.responseMessage;
    }

    public void setRequestLine(String requestLine2) {
        this.requestLine = requestLine2.trim();
    }

    public void setStatusLine(String statusLine2) throws IOException {
        if (this.responseMessage != null) {
            throw new IllegalStateException("statusLine is already set");
        }
        boolean hasMessage = statusLine2.length() > 13;
        if (!statusLine2.startsWith("HTTP/1.") || statusLine2.length() < 12 || statusLine2.charAt(8) != ' ' || (hasMessage && statusLine2.charAt(12) != ' ')) {
            throw new ProtocolException("Unexpected status line: " + statusLine2);
        }
        int httpMinorVersion2 = statusLine2.charAt(7) - '0';
        if (httpMinorVersion2 < 0 || httpMinorVersion2 > 9) {
            throw new ProtocolException("Unexpected status line: " + statusLine2);
        }
        try {
            int responseCode2 = Integer.parseInt(statusLine2.substring(9, 12));
            this.responseMessage = hasMessage ? statusLine2.substring(13) : "";
            this.responseCode = responseCode2;
            this.statusLine = statusLine2;
            this.httpMinorVersion = httpMinorVersion2;
        } catch (NumberFormatException e) {
            throw new ProtocolException("Unexpected status line: " + statusLine2);
        }
    }

    public void computeResponseStatusLineFromSpdyHeaders() throws IOException {
        String status = null;
        String version = null;
        for (int i = 0; i < this.namesAndValues.size(); i += 2) {
            String name = this.namesAndValues.get(i);
            if (":status".equals(name)) {
                status = this.namesAndValues.get(i + 1);
            } else if (":version".equals(name)) {
                version = this.namesAndValues.get(i + 1);
            }
        }
        if (status == null || version == null) {
            throw new ProtocolException("Expected ':status' and ':version' headers not present");
        }
        setStatusLine(version + " " + status);
    }

    public void addSpdyRequestHeaders(String method, String path, String version, String host, String scheme) {
        add(":method", method);
        add(":scheme", scheme);
        add(":path", path);
        add(":version", version);
        add(":host", host);
    }

    public String getStatusLine() {
        return this.statusLine;
    }

    public int getHttpMinorVersion() {
        if (this.httpMinorVersion != -1) {
            return this.httpMinorVersion;
        }
        return 1;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public String getResponseMessage() {
        return this.responseMessage;
    }

    public void addLine(String line) {
        int index = line.indexOf(":");
        if (index == -1) {
            addLenient("", line);
        } else {
            addLenient(line.substring(0, index), line.substring(index + 1));
        }
    }

    public void add(String fieldName, String value) {
        if (fieldName == null) {
            throw new IllegalArgumentException("fieldname == null");
        } else if (value == null) {
            throw new IllegalArgumentException("value == null");
        } else if (fieldName.length() != 0 && fieldName.indexOf(0) == -1 && value.indexOf(0) == -1) {
            addLenient(fieldName, value);
        } else {
            throw new IllegalArgumentException("Unexpected header: " + fieldName + ": " + value);
        }
    }

    private void addLenient(String fieldName, String value) {
        this.namesAndValues.add(fieldName);
        this.namesAndValues.add(value.trim());
    }

    public void removeAll(String fieldName) {
        for (int i = 0; i < this.namesAndValues.size(); i += 2) {
            if (fieldName.equalsIgnoreCase(this.namesAndValues.get(i))) {
                this.namesAndValues.remove(i);
                this.namesAndValues.remove(i);
            }
        }
    }

    public void addAll(String fieldName, List<String> headerFields) {
        for (String value : headerFields) {
            add(fieldName, value);
        }
    }

    public void set(String fieldName, String value) {
        removeAll(fieldName);
        add(fieldName, value);
    }

    public int length() {
        return this.namesAndValues.size() / 2;
    }

    public String getFieldName(int index) {
        int fieldNameIndex = index * 2;
        if (fieldNameIndex < 0 || fieldNameIndex >= this.namesAndValues.size()) {
            return null;
        }
        return this.namesAndValues.get(fieldNameIndex);
    }

    public String getValue(int index) {
        int valueIndex = (index * 2) + 1;
        if (valueIndex < 0 || valueIndex >= this.namesAndValues.size()) {
            return null;
        }
        return this.namesAndValues.get(valueIndex);
    }

    public String get(String fieldName) {
        for (int i = this.namesAndValues.size() - 2; i >= 0; i -= 2) {
            if (fieldName.equalsIgnoreCase(this.namesAndValues.get(i))) {
                return this.namesAndValues.get(i + 1);
            }
        }
        return null;
    }

    public RawHeaders getAll(Set<String> fieldNames) {
        RawHeaders result = new RawHeaders();
        for (int i = 0; i < this.namesAndValues.size(); i += 2) {
            String fieldName = this.namesAndValues.get(i);
            if (fieldNames.contains(fieldName)) {
                result.add(fieldName, this.namesAndValues.get(i + 1));
            }
        }
        return result;
    }

    public byte[] toBytes() throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder(256);
        result.append(this.requestLine).append("\r\n");
        for (int i = 0; i < this.namesAndValues.size(); i += 2) {
            result.append(this.namesAndValues.get(i)).append(": ").append(this.namesAndValues.get(i + 1)).append("\r\n");
        }
        result.append("\r\n");
        return result.toString().getBytes("ISO-8859-1");
    }

    public static RawHeaders fromBytes(InputStream in) throws IOException {
        RawHeaders headers;
        do {
            headers = new RawHeaders();
            headers.setStatusLine(Util.readAsciiLine(in));
            readHeaders(in, headers);
        } while (headers.getResponseCode() == 100);
        return headers;
    }

    public static void readHeaders(InputStream in, RawHeaders out) throws IOException {
        while (true) {
            String line = Util.readAsciiLine(in);
            if (line.length() != 0) {
                out.addLine(line);
            } else {
                return;
            }
        }
    }

    public Map<String, List<String>> toMultimap(boolean response) {
        Map<String, List<String>> result = new TreeMap<>(FIELD_NAME_COMPARATOR);
        for (int i = 0; i < this.namesAndValues.size(); i += 2) {
            String fieldName = this.namesAndValues.get(i);
            String value = this.namesAndValues.get(i + 1);
            List<String> allValues = new ArrayList<>();
            List<String> otherValues = result.get(fieldName);
            if (otherValues != null) {
                allValues.addAll(otherValues);
            }
            allValues.add(value);
            result.put(fieldName, Collections.unmodifiableList(allValues));
        }
        if (response && this.statusLine != null) {
            result.put(null, Collections.unmodifiableList(Collections.singletonList(this.statusLine)));
        } else if (this.requestLine != null) {
            result.put(null, Collections.unmodifiableList(Collections.singletonList(this.requestLine)));
        }
        return Collections.unmodifiableMap(result);
    }

    public static RawHeaders fromMultimap(Map<String, List<String>> map, boolean response) throws IOException {
        if (!response) {
            throw new UnsupportedOperationException();
        }
        RawHeaders result = new RawHeaders();
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            String fieldName = (String) entry.getKey();
            List<String> values = entry.getValue();
            if (fieldName != null) {
                for (String value : values) {
                    result.addLenient(fieldName, value);
                }
            } else if (!values.isEmpty()) {
                result.setStatusLine((String) values.get(values.size() - 1));
            }
        }
        return result;
    }

    public List<String> toNameValueBlock() {
        Set<String> names = new HashSet<>();
        List<String> result = new ArrayList<>();
        for (int i = 0; i < this.namesAndValues.size(); i += 2) {
            String name = this.namesAndValues.get(i).toLowerCase(Locale.US);
            String value = this.namesAndValues.get(i + 1);
            if (!name.equals("connection") && !name.equals("host") && !name.equals("keep-alive") && !name.equals("proxy-connection") && !name.equals("transfer-encoding")) {
                if (names.add(name)) {
                    result.add(name);
                    result.add(value);
                } else {
                    int j = 0;
                    while (true) {
                        if (j >= result.size()) {
                            break;
                        } else if (name.equals(result.get(j))) {
                            result.set(j + 1, ((String) result.get(j + 1)) + "\u0000" + value);
                            break;
                        } else {
                            j += 2;
                        }
                    }
                }
            }
        }
        return result;
    }

    public static RawHeaders fromNameValueBlock(List<String> nameValueBlock) {
        if (nameValueBlock.size() % 2 != 0) {
            throw new IllegalArgumentException("Unexpected name value block: " + nameValueBlock);
        }
        RawHeaders result = new RawHeaders();
        for (int i = 0; i < nameValueBlock.size(); i += 2) {
            String name = nameValueBlock.get(i);
            String values = nameValueBlock.get(i + 1);
            int start = 0;
            while (start < values.length()) {
                int end = values.indexOf(0, start);
                if (end == -1) {
                    end = values.length();
                }
                result.namesAndValues.add(name);
                result.namesAndValues.add(values.substring(start, end));
                start = end + 1;
            }
        }
        return result;
    }
}
