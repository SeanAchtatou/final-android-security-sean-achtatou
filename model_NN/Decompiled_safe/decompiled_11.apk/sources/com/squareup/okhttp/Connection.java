package com.squareup.okhttp;

import com.squareup.okhttp.internal.Platform;
import com.squareup.okhttp.internal.http.HttpEngine;
import com.squareup.okhttp.internal.http.HttpTransport;
import com.squareup.okhttp.internal.http.SpdyTransport;
import com.squareup.okhttp.internal.spdy.SpdyConnection;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Proxy;
import java.net.Socket;
import java.util.Arrays;
import javax.net.ssl.SSLSocket;

public final class Connection implements Closeable {
    private static final byte[] HTTP_11 = {104, 116, 116, 112, 47, 49, 46, 49};
    private static final byte[] NPN_PROTOCOLS = {6, 115, 112, 100, 121, 47, 51, 8, 104, 116, 116, 112, 47, 49, 46, 49};
    private static final byte[] SPDY3 = {115, 112, 100, 121, 47, 51};
    private boolean connected = false;
    private int httpMinorVersion = 1;
    private long idleStartTimeNs;
    private InputStream in;
    private OutputStream out;
    private final Route route;
    private Socket socket;
    private SpdyConnection spdyConnection;

    public Connection(Route route2) {
        this.route = route2;
    }

    public void connect(int connectTimeout, int readTimeout, TunnelRequest tunnelRequest) throws IOException {
        if (this.connected) {
            throw new IllegalStateException("already connected");
        }
        this.connected = true;
        this.socket = this.route.proxy.type() != Proxy.Type.HTTP ? new Socket(this.route.proxy) : new Socket();
        this.socket.connect(this.route.inetSocketAddress, connectTimeout);
        this.socket.setSoTimeout(readTimeout);
        this.in = this.socket.getInputStream();
        this.out = this.socket.getOutputStream();
        if (this.route.address.sslSocketFactory != null) {
            upgradeToTls(tunnelRequest);
        }
        int mtu = Platform.get().getMtu(this.socket);
        this.in = new BufferedInputStream(this.in, mtu);
        this.out = new BufferedOutputStream(this.out, mtu);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    private void upgradeToTls(TunnelRequest tunnelRequest) throws IOException {
        byte[] selectedProtocol;
        Platform platform = Platform.get();
        if (requiresTunnel()) {
            makeTunnel(tunnelRequest);
        }
        this.socket = this.route.address.sslSocketFactory.createSocket(this.socket, this.route.address.uriHost, this.route.address.uriPort, true);
        SSLSocket sslSocket = (SSLSocket) this.socket;
        if (this.route.modernTls) {
            platform.enableTlsExtensions(sslSocket, this.route.address.uriHost);
        } else {
            platform.supportTlsIntolerantServer(sslSocket);
        }
        if (this.route.modernTls) {
            platform.setNpnProtocols(sslSocket, NPN_PROTOCOLS);
        }
        sslSocket.startHandshake();
        if (!this.route.address.hostnameVerifier.verify(this.route.address.uriHost, sslSocket.getSession())) {
            throw new IOException("Hostname '" + this.route.address.uriHost + "' was not verified");
        }
        this.out = sslSocket.getOutputStream();
        this.in = sslSocket.getInputStream();
        if (this.route.modernTls && (selectedProtocol = platform.getNpnSelectedProtocol(sslSocket)) != null) {
            if (Arrays.equals(selectedProtocol, SPDY3)) {
                sslSocket.setSoTimeout(0);
                this.spdyConnection = new SpdyConnection.Builder(this.route.address.getUriHost(), true, this.in, this.out).build();
            } else if (!Arrays.equals(selectedProtocol, HTTP_11)) {
                throw new IOException("Unexpected NPN transport " + new String(selectedProtocol, "ISO-8859-1"));
            }
        }
    }

    public boolean isConnected() {
        return this.connected;
    }

    public void close() throws IOException {
        this.socket.close();
    }

    public Route getRoute() {
        return this.route;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public boolean isAlive() {
        return !this.socket.isClosed() && !this.socket.isInputShutdown() && !this.socket.isOutputShutdown();
    }

    public void resetIdleStartTime() {
        if (this.spdyConnection != null) {
            throw new IllegalStateException("spdyConnection != null");
        }
        this.idleStartTimeNs = System.nanoTime();
    }

    public boolean isIdle() {
        return this.spdyConnection == null || this.spdyConnection.isIdle();
    }

    public boolean isExpired(long keepAliveDurationNs) {
        return isIdle() && System.nanoTime() - getIdleStartTimeNs() > keepAliveDurationNs;
    }

    public long getIdleStartTimeNs() {
        return this.spdyConnection == null ? this.idleStartTimeNs : this.spdyConnection.getIdleStartTimeNs();
    }

    public Object newTransport(HttpEngine httpEngine) throws IOException {
        return this.spdyConnection != null ? new SpdyTransport(httpEngine, this.spdyConnection) : new HttpTransport(httpEngine, this.out, this.in);
    }

    public boolean isSpdy() {
        return this.spdyConnection != null;
    }

    public SpdyConnection getSpdyConnection() {
        return this.spdyConnection;
    }

    public int getHttpMinorVersion() {
        return this.httpMinorVersion;
    }

    public void setHttpMinorVersion(int httpMinorVersion2) {
        this.httpMinorVersion = httpMinorVersion2;
    }

    public boolean requiresTunnel() {
        return this.route.address.sslSocketFactory != null && this.route.proxy.type() == Proxy.Type.HTTP;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0037  */
    private void makeTunnel(com.squareup.okhttp.TunnelRequest r10) throws java.io.IOException {
        /*
            r9 = this;
            com.squareup.okhttp.internal.http.RawHeaders r1 = r10.getRequestHeaders()
        L_0x0004:
            java.io.OutputStream r5 = r9.out
            byte[] r6 = r1.toBytes()
            r5.write(r6)
            java.io.InputStream r5 = r9.in
            com.squareup.okhttp.internal.http.RawHeaders r3 = com.squareup.okhttp.internal.http.RawHeaders.fromBytes(r5)
            int r5 = r3.getResponseCode()
            switch(r5) {
                case 200: goto L_0x005f;
                case 407: goto L_0x0037;
                default: goto L_0x001a;
            }
        L_0x001a:
            java.io.IOException r5 = new java.io.IOException
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Unexpected response code for CONNECT: "
            java.lang.StringBuilder r6 = r6.append(r7)
            int r7 = r3.getResponseCode()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.<init>(r6)
            throw r5
        L_0x0037:
            com.squareup.okhttp.internal.http.RawHeaders r2 = new com.squareup.okhttp.internal.http.RawHeaders
            r2.<init>(r1)
            java.net.URL r4 = new java.net.URL
            java.lang.String r5 = "https"
            java.lang.String r6 = r10.host
            int r7 = r10.port
            java.lang.String r8 = "/"
            r4.<init>(r5, r6, r7, r8)
            r5 = 407(0x197, float:5.7E-43)
            com.squareup.okhttp.Route r6 = r9.route
            java.net.Proxy r6 = r6.proxy
            boolean r0 = com.squareup.okhttp.internal.http.HttpAuthenticator.processAuthHeader(r5, r3, r2, r6, r4)
            if (r0 == 0) goto L_0x0057
            r1 = r2
            goto L_0x0004
        L_0x0057:
            java.io.IOException r5 = new java.io.IOException
            java.lang.String r6 = "Failed to authenticate with proxy"
            r5.<init>(r6)
            throw r5
        L_0x005f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.Connection.makeTunnel(com.squareup.okhttp.TunnelRequest):void");
    }
}
