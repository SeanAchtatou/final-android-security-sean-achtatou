package com.google.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.ak;
import com.google.ads.al;
import com.google.ads.l;
import com.google.ads.m;
import com.google.ads.n;
import com.google.ads.searchads.SearchAdRequest;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.cordova.Globalization;
import org.json.JSONException;

public class c implements Runnable {
    boolean a;
    private String b;
    private String c;
    private String d;
    private String e;
    private boolean f;
    private f g;
    private AdRequest h;
    /* access modifiers changed from: private */
    public WebView i;
    /* access modifiers changed from: private */
    public l j;
    private String k;
    /* access modifiers changed from: private */
    public String l;
    private LinkedList<String> m;
    /* access modifiers changed from: private */
    public String n;
    /* access modifiers changed from: private */
    public AdSize o;
    /* access modifiers changed from: private */
    public boolean p = false;
    private volatile boolean q;
    private boolean r;
    private AdRequest.ErrorCode s;
    private boolean t;
    private int u;
    private Thread v;
    private boolean w;
    private d x = d.ONLINE_SERVER_REQUEST;

    private class b extends Exception {
        public b(String str) {
            super(str);
        }
    }

    private static class a implements Runnable {
        private final d a;
        private final WebView b;
        private final f c;
        private final AdRequest.ErrorCode d;
        private final boolean e;

        public a(d dVar, WebView webView, f fVar, AdRequest.ErrorCode errorCode, boolean z) {
            this.a = dVar;
            this.b = webView;
            this.c = fVar;
            this.d = errorCode;
            this.e = z;
        }

        public void run() {
            if (this.b != null) {
                this.b.stopLoading();
                this.b.destroy();
            }
            if (this.c != null) {
                this.c.a();
            }
            if (this.e) {
                this.a.l().stopLoading();
                if (this.a.i().i.a() != null) {
                    this.a.i().i.a().setVisibility(8);
                }
            }
            this.a.a(this.d);
        }
    }

    /* renamed from: com.google.ads.internal.c$c  reason: collision with other inner class name */
    private class C0000c implements Runnable {
        private final String b;
        private final String c;
        private final WebView d;

        public C0000c(WebView webView, String str, String str2) {
            this.d = webView;
            this.b = str;
            this.c = str2;
        }

        public void run() {
            c.this.j.c.a(Boolean.valueOf(c.this.p));
            c.this.j.a.a().b.a().l().a(c.this.p);
            if (c.this.j.a.a().e.a() != null) {
                c.this.j.a.a().e.a().setOverlayEnabled(!c.this.p);
            }
            if (this.c != null) {
                this.d.loadDataWithBaseURL(this.b, this.c, "text/html", "utf-8", null);
            } else {
                this.d.loadUrl(this.b);
            }
        }
    }

    private class e implements Runnable {
        private final d b;
        private final WebView c;
        private final LinkedList<String> d;
        private final int e;
        private final boolean f;
        private final String g;
        private final AdSize h;

        public e(d dVar, WebView webView, LinkedList<String> linkedList, int i, boolean z, String str, AdSize adSize) {
            this.b = dVar;
            this.c = webView;
            this.d = linkedList;
            this.e = i;
            this.f = z;
            this.g = str;
            this.h = adSize;
        }

        public void run() {
            if (this.c != null) {
                this.c.stopLoading();
                this.c.destroy();
            }
            this.b.a(this.d);
            this.b.a(this.e);
            this.b.a(this.f);
            this.b.a(this.g);
            if (this.h != null) {
                c.this.j.a.a().g.a().b(this.h);
                this.b.l().setAdSize(this.h);
            }
            this.b.E();
        }
    }

    public enum d {
        ONLINE_USING_BUFFERED_ADS("online_buffered"),
        ONLINE_SERVER_REQUEST("online_request"),
        OFFLINE_USING_BUFFERED_ADS("offline_buffered"),
        OFFLINE_EMPTY("offline_empty");
        
        public String e;

        private d(String str) {
            this.e = str;
        }
    }

    public synchronized void a(boolean z) {
        this.p = z;
    }

    protected c() {
    }

    public c(l lVar) {
        this.j = lVar;
        this.k = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.m = new LinkedList<>();
        this.s = null;
        this.t = false;
        this.u = -1;
        this.f = false;
        this.r = false;
        this.n = null;
        this.o = null;
        if (lVar.a.a().c.a() != null) {
            this.i = new AdWebView(lVar.a.a(), null);
            this.i.setWebViewClient(i.a(lVar.a.a().b.a(), a.b, false, false));
            this.i.setVisibility(8);
            this.i.setWillNotDraw(true);
            this.g = new f(lVar);
            return;
        }
        this.i = null;
        this.g = null;
        com.google.ads.util.b.e("activity was null while trying to create an AdLoader.");
    }

    /* access modifiers changed from: protected */
    public synchronized void a(String str) {
        this.m.add(str);
    }

    /* access modifiers changed from: protected */
    public void a() {
        com.google.ads.util.b.a("AdLoader cancelled.");
        if (this.i != null) {
            this.i.stopLoading();
            this.i.destroy();
        }
        if (this.v != null) {
            this.v.interrupt();
            this.v = null;
        }
        if (this.g != null) {
            this.g.a();
        }
        this.q = true;
    }

    /* access modifiers changed from: protected */
    public void a(AdRequest adRequest) {
        this.h = adRequest;
        this.q = false;
        this.v = new Thread(this);
        this.v.start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ads.internal.c.a(com.google.ads.AdRequest$ErrorCode, boolean):void
     arg types: [com.google.ads.AdRequest$ErrorCode, int]
     candidates:
      com.google.ads.internal.c.a(java.util.Map<java.lang.String, java.lang.Object>, android.app.Activity):java.lang.String
      com.google.ads.internal.c.a(java.lang.String, java.lang.String):void
      com.google.ads.internal.c.a(com.google.ads.AdRequest$ErrorCode, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0313 A[Catch:{ Throwable -> 0x0115 }] */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x042b A[Catch:{ InterruptedException -> 0x042f }, LOOP:1: B:186:0x03fa->B:197:0x042b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x044d A[Catch:{ Throwable -> 0x0115 }] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x0452 A[Catch:{ Throwable -> 0x0115 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            r3 = 0
            r10 = 0
            monitor-enter(r12)
            android.webkit.WebView r0 = r12.i     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x000c
            com.google.ads.internal.f r0 = r12.g     // Catch:{ Throwable -> 0x0115 }
            if (r0 != 0) goto L_0x0019
        L_0x000c:
            java.lang.String r0 = "adRequestWebView was null while trying to load an ad."
            com.google.ads.util.b.e(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
        L_0x0018:
            return
        L_0x0019:
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$d<android.app.Activity> r0 = r0.c     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            android.app.Activity r0 = (android.app.Activity) r0     // Catch:{ Throwable -> 0x0115 }
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = "activity was null while forming an ad request."
            com.google.ads.util.b.e(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x003a:
            r0 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            throw r0
        L_0x003d:
            com.google.ads.l r1 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r1 = r1.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r1 = r1.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r1 = (com.google.ads.n) r1     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.internal.d> r1 = r1.b     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r1 = r1.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.d r1 = (com.google.ads.internal.d) r1     // Catch:{ Throwable -> 0x0115 }
            long r4 = r1.p()     // Catch:{ Throwable -> 0x0115 }
            long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest r2 = r12.h     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.l r1 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r1 = r1.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r1 = r1.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r1 = (com.google.ads.n) r1     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<android.content.Context> r1 = r1.f     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r1 = r1.a()     // Catch:{ Throwable -> 0x0115 }
            android.content.Context r1 = (android.content.Context) r1     // Catch:{ Throwable -> 0x0115 }
            java.util.Map r8 = r2.getRequestMap(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "extras"
            java.lang.Object r1 = r8.get(r1)     // Catch:{ Throwable -> 0x0115 }
            boolean r2 = r1 instanceof java.util.Map     // Catch:{ Throwable -> 0x0115 }
            if (r2 == 0) goto L_0x00e1
            java.util.Map r1 = (java.util.Map) r1     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r2 = "_adUrl"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ Throwable -> 0x0115 }
            boolean r9 = r2 instanceof java.lang.String     // Catch:{ Throwable -> 0x0115 }
            if (r9 == 0) goto L_0x0089
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Throwable -> 0x0115 }
            r12.b = r2     // Catch:{ Throwable -> 0x0115 }
        L_0x0089:
            java.lang.String r2 = "_requestUrl"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ Throwable -> 0x0115 }
            boolean r9 = r2 instanceof java.lang.String     // Catch:{ Throwable -> 0x0115 }
            if (r9 == 0) goto L_0x0097
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Throwable -> 0x0115 }
            r12.k = r2     // Catch:{ Throwable -> 0x0115 }
        L_0x0097:
            java.lang.String r2 = "_activationOverlayUrl"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ Throwable -> 0x0115 }
            boolean r9 = r2 instanceof java.lang.String     // Catch:{ Throwable -> 0x0115 }
            if (r9 == 0) goto L_0x00a5
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Throwable -> 0x0115 }
            r12.l = r2     // Catch:{ Throwable -> 0x0115 }
        L_0x00a5:
            java.lang.String r2 = "_orientation"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ Throwable -> 0x0115 }
            boolean r9 = r2 instanceof java.lang.String     // Catch:{ Throwable -> 0x0115 }
            if (r9 == 0) goto L_0x00ba
            java.lang.String r9 = "p"
            boolean r9 = r2.equals(r9)     // Catch:{ Throwable -> 0x0115 }
            if (r9 == 0) goto L_0x0109
            r2 = 1
            r12.u = r2     // Catch:{ Throwable -> 0x0115 }
        L_0x00ba:
            java.lang.String r2 = "_norefresh"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Throwable -> 0x0115 }
            boolean r2 = r1 instanceof java.lang.String     // Catch:{ Throwable -> 0x0115 }
            if (r2 == 0) goto L_0x00e1
            java.lang.String r2 = "t"
            boolean r1 = r1.equals(r2)     // Catch:{ Throwable -> 0x0115 }
            if (r1 == 0) goto L_0x00e1
            com.google.ads.l r1 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r1 = r1.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r1 = r1.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r1 = (com.google.ads.n) r1     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.internal.d> r1 = r1.b     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r1 = r1.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.d r1 = (com.google.ads.internal.d) r1     // Catch:{ Throwable -> 0x0115 }
            r1.e()     // Catch:{ Throwable -> 0x0115 }
        L_0x00e1:
            java.lang.String r1 = r12.b     // Catch:{ Throwable -> 0x0115 }
            if (r1 != 0) goto L_0x030e
            java.lang.String r1 = r12.k     // Catch:{ Throwable -> 0x0115 }
            if (r1 != 0) goto L_0x01c2
            java.lang.String r0 = r12.a(r8, r0)     // Catch:{ b -> 0x0124 }
            java.lang.String r1 = r12.f()     // Catch:{ Throwable -> 0x0115 }
            r12.b(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Throwable -> 0x0115 }
            long r0 = r0 - r6
            long r0 = r4 - r0
            int r2 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r2 <= 0) goto L_0x0102
            r12.wait(r0)     // Catch:{ InterruptedException -> 0x0144 }
        L_0x0102:
            boolean r0 = r12.q     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x015e
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0109:
            java.lang.String r9 = "l"
            boolean r2 = r2.equals(r9)     // Catch:{ Throwable -> 0x0115 }
            if (r2 == 0) goto L_0x00ba
            r2 = 0
            r12.u = r2     // Catch:{ Throwable -> 0x0115 }
            goto L_0x00ba
        L_0x0115:
            r0 = move-exception
            java.lang.String r1 = "An unknown error occurred in AdLoader."
            com.google.ads.util.b.b(r1, r0)     // Catch:{ all -> 0x003a }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x003a }
            r1 = 1
            r12.a(r0, r1)     // Catch:{ all -> 0x003a }
        L_0x0121:
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0124:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r1.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r2 = "Caught internal exception: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0144:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r1.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the URL: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.a(r0)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x015e:
            com.google.ads.AdRequest$ErrorCode r0 = r12.s     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x016b
            com.google.ads.AdRequest$ErrorCode r0 = r12.s     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x016b:
            java.lang.String r0 = r12.k     // Catch:{ Throwable -> 0x0115 }
            if (r0 != 0) goto L_0x0194
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r0.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "ms while getting the URL."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0194:
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.internal.h> r0 = r0.g     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.h r0 = (com.google.ads.internal.h) r0     // Catch:{ Throwable -> 0x0115 }
            boolean r0 = r0.b()     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x01c2
            java.lang.String r0 = r12.l     // Catch:{ Throwable -> 0x0115 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x01c2
            java.lang.String r0 = "AdLoader doesn't have a URL for the activation overlay"
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x01c2:
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.internal.d> r0 = r0.b     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.d r0 = (com.google.ads.internal.d) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.g r0 = r0.n()     // Catch:{ Throwable -> 0x0115 }
            int[] r1 = com.google.ads.internal.c.AnonymousClass3.a     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.c$d r2 = r12.x     // Catch:{ Throwable -> 0x0115 }
            int r2 = r2.ordinal()     // Catch:{ Throwable -> 0x0115 }
            r1 = r1[r2]     // Catch:{ Throwable -> 0x0115 }
            switch(r1) {
                case 1: goto L_0x0278;
                case 2: goto L_0x0288;
                case 3: goto L_0x0292;
                case 4: goto L_0x029f;
                default: goto L_0x01e5;
            }     // Catch:{ Throwable -> 0x0115 }
        L_0x01e5:
            boolean r0 = r12.a     // Catch:{ Throwable -> 0x0115 }
            if (r0 != 0) goto L_0x02f2
            java.lang.String r0 = "Not using loadUrl()."
            com.google.ads.util.b.a(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.f r0 = r12.g     // Catch:{ Throwable -> 0x0115 }
            boolean r1 = r12.w     // Catch:{ Throwable -> 0x0115 }
            r0.a(r1)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.internal.h> r0 = r0.g     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.h r0 = (com.google.ads.internal.h) r0     // Catch:{ Throwable -> 0x0115 }
            boolean r0 = r0.b()     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x0479
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.internal.ActivationOverlay> r0 = r0.e     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.ActivationOverlay r0 = (com.google.ads.internal.ActivationOverlay) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.i r3 = r0.e()     // Catch:{ Throwable -> 0x0115 }
            r0 = 1
            r3.c(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.m r0 = com.google.ads.m.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<android.os.Handler> r0 = r0.c     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            android.os.Handler r0 = (android.os.Handler) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.c$1 r1 = new com.google.ads.internal.c$1     // Catch:{ Throwable -> 0x0115 }
            r1.<init>()     // Catch:{ Throwable -> 0x0115 }
            r0.post(r1)     // Catch:{ Throwable -> 0x0115 }
            r0 = r3
        L_0x023c:
            com.google.ads.internal.f r1 = r12.g     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r2 = r12.k     // Catch:{ Throwable -> 0x0115 }
            r1.a(r2)     // Catch:{ Throwable -> 0x0115 }
        L_0x0243:
            boolean r1 = r12.q     // Catch:{ InterruptedException -> 0x025e }
            if (r1 != 0) goto L_0x02b5
            com.google.ads.AdRequest$ErrorCode r1 = r12.s     // Catch:{ InterruptedException -> 0x025e }
            if (r1 != 0) goto L_0x02b5
            java.lang.String r1 = r12.c     // Catch:{ InterruptedException -> 0x025e }
            if (r1 != 0) goto L_0x02b5
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ InterruptedException -> 0x025e }
            long r1 = r1 - r6
            long r1 = r4 - r1
            int r3 = (r1 > r10 ? 1 : (r1 == r10 ? 0 : -1))
            if (r3 <= 0) goto L_0x02b5
            r12.wait(r1)     // Catch:{ InterruptedException -> 0x025e }
            goto L_0x0243
        L_0x025e:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r1.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the ad server's response: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.a(r0)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0278:
            r0.r()     // Catch:{ Throwable -> 0x0115 }
            r0.u()     // Catch:{ Throwable -> 0x0115 }
            r0.x()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = "Request scenario: Online server request."
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            goto L_0x01e5
        L_0x0288:
            r0.t()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = "Request scenario: Online using buffered ads."
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            goto L_0x01e5
        L_0x0292:
            r0.w()     // Catch:{ Throwable -> 0x0115 }
            r0.q()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = "Request scenario: Offline using buffered ads."
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            goto L_0x01e5
        L_0x029f:
            r0.q()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = "Request scenario: Offline with no buffered ads."
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = "Network is unavailable.  Aborting ad request."
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x02b5:
            boolean r1 = r12.q     // Catch:{ Throwable -> 0x0115 }
            if (r1 == 0) goto L_0x02bc
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x02bc:
            com.google.ads.AdRequest$ErrorCode r1 = r12.s     // Catch:{ Throwable -> 0x0115 }
            if (r1 == 0) goto L_0x02c9
            com.google.ads.AdRequest$ErrorCode r0 = r12.s     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x02c9:
            java.lang.String r1 = r12.c     // Catch:{ Throwable -> 0x0115 }
            if (r1 != 0) goto L_0x0476
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r0.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "ms while waiting for the ad server's response."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x02f2:
            java.lang.String r0 = r12.k     // Catch:{ Throwable -> 0x0115 }
            r12.b = r0     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r0.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "Using loadUrl.  adBaseUrl: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = r12.b     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.a(r0)     // Catch:{ Throwable -> 0x0115 }
        L_0x030e:
            r1 = r3
        L_0x030f:
            boolean r0 = r12.a     // Catch:{ Throwable -> 0x0115 }
            if (r0 != 0) goto L_0x03e1
            boolean r0 = r12.f     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x0333
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.internal.d> r0 = r0.b     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.d r0 = (com.google.ads.internal.d) r0     // Catch:{ Throwable -> 0x0115 }
            r1 = 1
            r0.b(r1)     // Catch:{ Throwable -> 0x0115 }
            r12.b()     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0333:
            java.lang.String r0 = r12.e     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x0372
            java.lang.String r0 = r12.e     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r2 = "application/json"
            boolean r0 = r0.startsWith(r2)     // Catch:{ Throwable -> 0x0115 }
            if (r0 != 0) goto L_0x034b
            java.lang.String r0 = r12.e     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r2 = "text/javascript"
            boolean r0 = r0.startsWith(r2)     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x0372
        L_0x034b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r0.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "Expected HTML but received "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = r12.e     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.b(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0372:
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$c<com.google.ads.AdSize[]> r0 = r0.n     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x03d5
            com.google.ads.AdSize r0 = r12.o     // Catch:{ Throwable -> 0x0115 }
            if (r0 != 0) goto L_0x0396
            java.lang.String r0 = "Multiple supported ad sizes were specified, but the server did not respond with a size."
            com.google.ads.util.b.b(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0396:
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$c<com.google.ads.AdSize[]> r0 = r0.n     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Throwable -> 0x0115 }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdSize r2 = r12.o     // Catch:{ Throwable -> 0x0115 }
            boolean r0 = r0.contains(r2)     // Catch:{ Throwable -> 0x0115 }
            if (r0 != 0) goto L_0x03e1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r0.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "The ad server did not respond with a supported AdSize: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdSize r1 = r12.o     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.b(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x03d5:
            com.google.ads.AdSize r0 = r12.o     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x03e1
            java.lang.String r0 = "adSize was expected to be null in AdLoader."
            com.google.ads.util.b.e(r0)     // Catch:{ Throwable -> 0x0115 }
            r0 = 0
            r12.o = r0     // Catch:{ Throwable -> 0x0115 }
        L_0x03e1:
            com.google.ads.l r0 = r12.j     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.i$b<com.google.ads.internal.d> r0 = r0.b     // Catch:{ Throwable -> 0x0115 }
            java.lang.Object r0 = r0.a()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.internal.d r0 = (com.google.ads.internal.d) r0     // Catch:{ Throwable -> 0x0115 }
            r2 = 0
            r0.b(r2)     // Catch:{ Throwable -> 0x0115 }
            r12.i()     // Catch:{ Throwable -> 0x0115 }
        L_0x03fa:
            boolean r0 = r12.q     // Catch:{ InterruptedException -> 0x042f }
            if (r0 != 0) goto L_0x0449
            boolean r0 = r12.t     // Catch:{ InterruptedException -> 0x042f }
            if (r0 == 0) goto L_0x0420
            com.google.ads.l r0 = r12.j     // Catch:{ InterruptedException -> 0x042f }
            com.google.ads.util.i$b<com.google.ads.n> r0 = r0.a     // Catch:{ InterruptedException -> 0x042f }
            java.lang.Object r0 = r0.a()     // Catch:{ InterruptedException -> 0x042f }
            com.google.ads.n r0 = (com.google.ads.n) r0     // Catch:{ InterruptedException -> 0x042f }
            com.google.ads.util.i$b<com.google.ads.internal.h> r0 = r0.g     // Catch:{ InterruptedException -> 0x042f }
            java.lang.Object r0 = r0.a()     // Catch:{ InterruptedException -> 0x042f }
            com.google.ads.internal.h r0 = (com.google.ads.internal.h) r0     // Catch:{ InterruptedException -> 0x042f }
            boolean r0 = r0.b()     // Catch:{ InterruptedException -> 0x042f }
            if (r0 == 0) goto L_0x0449
            boolean r0 = r1.a()     // Catch:{ InterruptedException -> 0x042f }
            if (r0 == 0) goto L_0x0449
        L_0x0420:
            long r2 = android.os.SystemClock.elapsedRealtime()     // Catch:{ InterruptedException -> 0x042f }
            long r2 = r2 - r6
            long r2 = r4 - r2
            int r0 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x0449
            r12.wait(r2)     // Catch:{ InterruptedException -> 0x042f }
            goto L_0x03fa
        L_0x042f:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r1.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r2 = "AdLoader InterruptedException while loading the HTML: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.a(r0)     // Catch:{ Throwable -> 0x0115 }
            monitor-exit(r12)     // Catch:{ all -> 0x003a }
            goto L_0x0018
        L_0x0449:
            boolean r0 = r12.t     // Catch:{ Throwable -> 0x0115 }
            if (r0 == 0) goto L_0x0452
            r12.j()     // Catch:{ Throwable -> 0x0115 }
            goto L_0x0121
        L_0x0452:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0115 }
            r0.<init>()     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r1 = "ms while loading the HTML."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.util.b.c(r0)     // Catch:{ Throwable -> 0x0115 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ Throwable -> 0x0115 }
            r1 = 1
            r12.a(r0, r1)     // Catch:{ Throwable -> 0x0115 }
            goto L_0x0121
        L_0x0476:
            r1 = r0
            goto L_0x030f
        L_0x0479:
            r0 = r3
            goto L_0x023c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.internal.c.run():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ads.internal.c.a(com.google.ads.AdRequest$ErrorCode, boolean):void
     arg types: [com.google.ads.AdRequest$ErrorCode, int]
     candidates:
      com.google.ads.internal.c.a(java.util.Map<java.lang.String, java.lang.Object>, android.app.Activity):java.lang.String
      com.google.ads.internal.c.a(java.lang.String, java.lang.String):void
      com.google.ads.internal.c.a(com.google.ads.AdRequest$ErrorCode, boolean):void */
    /* access modifiers changed from: protected */
    public void b() {
        try {
            if (TextUtils.isEmpty(this.e)) {
                com.google.ads.util.b.b("Got a mediation response with no content type. Aborting mediation.");
                a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
            } else if (!this.e.startsWith("application/json")) {
                com.google.ads.util.b.b("Got a mediation response with a content type: '" + this.e + "'. Expected something starting with 'application/json'. Aborting mediation.");
                a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
            } else {
                final com.google.ads.c a2 = com.google.ads.c.a(this.c);
                a(this.d, a2, this.j.a.a().b.a().j());
                m.a().c.a().post(new Runnable() {
                    public void run() {
                        if (c.this.i != null) {
                            c.this.i.stopLoading();
                            c.this.i.destroy();
                        }
                        c.this.j.a.a().b.a().a(c.this.n);
                        if (c.this.o != null) {
                            c.this.j.a.a().g.a().b(c.this.o);
                        }
                        c.this.j.a.a().b.a().a(a2);
                    }
                });
            }
        } catch (JSONException e2) {
            com.google.ads.util.b.b("AdLoader can't parse gWhirl server configuration.", e2);
            a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
        }
    }

    static void a(String str, com.google.ads.c cVar, com.google.ads.d dVar) {
        if (str != null && !str.contains("no-store") && !str.contains("no-cache")) {
            Matcher matcher = Pattern.compile("max-age\\s*=\\s*(\\d+)").matcher(str);
            if (matcher.find()) {
                try {
                    int parseInt = Integer.parseInt(matcher.group(1));
                    dVar.a(cVar, parseInt);
                    com.google.ads.util.b.c(String.format(Locale.US, "Caching gWhirl configuration for: %d seconds", Integer.valueOf(parseInt)));
                } catch (NumberFormatException e2) {
                    com.google.ads.util.b.b("Caught exception trying to parse cache control directive. Overflow?", e2);
                }
            } else {
                com.google.ads.util.b.c("Unrecognized cacheControlDirective: '" + str + "'. Not caching configuration.");
            }
        }
    }

    public String a(Map<String, Object> map, Activity activity) throws b {
        int i2;
        Context applicationContext = activity.getApplicationContext();
        g n2 = this.j.a.a().b.a().n();
        long m2 = n2.m();
        if (m2 > 0) {
            map.put("prl", Long.valueOf(m2));
        }
        long n3 = n2.n();
        if (n3 > 0) {
            map.put("prnl", Long.valueOf(n3));
        }
        String l2 = n2.l();
        if (l2 != null) {
            map.put("ppcl", l2);
        }
        String k2 = n2.k();
        if (k2 != null) {
            map.put("pcl", k2);
        }
        long j2 = n2.j();
        if (j2 > 0) {
            map.put("pcc", Long.valueOf(j2));
        }
        map.put("preqs", Long.valueOf(n2.o()));
        map.put("oar", Long.valueOf(n2.p()));
        map.put("bas_on", Long.valueOf(n2.s()));
        map.put("bas_off", Long.valueOf(n2.v()));
        if (n2.y()) {
            map.put("aoi_timeout", "true");
        }
        if (n2.A()) {
            map.put("aoi_nofill", "true");
        }
        String D = n2.D();
        if (D != null) {
            map.put("pit", D);
        }
        map.put("ptime", Long.valueOf(g.E()));
        n2.a();
        n2.i();
        if (this.j.a.a().b()) {
            map.put("format", "interstitial_mb");
        } else {
            AdSize c2 = this.j.a.a().g.a().c();
            if (c2.isFullWidth()) {
                map.put("smart_w", Globalization.FULL);
            }
            if (c2.isAutoHeight()) {
                map.put("smart_h", "auto");
            }
            if (!c2.isCustomAdSize()) {
                map.put("format", c2.toString());
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(c2.getWidth()));
                hashMap.put("h", Integer.valueOf(c2.getHeight()));
                map.put("ad_frame", hashMap);
            }
        }
        map.put("slotname", this.j.a.a().h.a());
        map.put("js", "afma-sdk-a-v6.4.1");
        try {
            int i3 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            String f2 = AdUtil.f(applicationContext);
            if (!TextUtils.isEmpty(f2)) {
                map.put("mv", f2);
            }
            String a2 = m.a().a.a();
            if (!TextUtils.isEmpty(a2)) {
                map.put("imbf", a2);
            }
            map.put("msid", applicationContext.getPackageName());
            map.put("app_name", i3 + ".android." + applicationContext.getPackageName());
            map.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                d2 = "null";
            }
            map.put("net", d2);
            String e2 = AdUtil.e(applicationContext);
            if (!(e2 == null || e2.length() == 0)) {
                map.put("cap", e2);
            }
            map.put("u_audio", Integer.valueOf(AdUtil.g(applicationContext).ordinal()));
            DisplayMetrics a3 = AdUtil.a(activity);
            map.put("u_sd", Float.valueOf(a3.density));
            map.put("u_h", Integer.valueOf(AdUtil.a(applicationContext, a3)));
            map.put("u_w", Integer.valueOf(AdUtil.b(applicationContext, a3)));
            map.put("hl", Locale.getDefault().getLanguage());
            n a4 = this.j.a.a();
            ak a5 = a4.r.a();
            if (a5 == null) {
                a5 = ak.a("afma-sdk-a-v6.4.1", activity);
                a4.r.a(a5);
                a4.s.a(new al(a5));
            }
            map.put("ms", a5.a(applicationContext));
            if (!(this.j.a.a().j == null || this.j.a.a().j.a() == null)) {
                AdView a6 = this.j.a.a().j.a();
                if (a6.getParent() != null) {
                    int[] iArr = new int[2];
                    a6.getLocationOnScreen(iArr);
                    int i4 = iArr[0];
                    int i5 = iArr[1];
                    DisplayMetrics displayMetrics = this.j.a.a().f.a().getResources().getDisplayMetrics();
                    int i6 = displayMetrics.widthPixels;
                    int i7 = displayMetrics.heightPixels;
                    if (!a6.isShown() || a6.getWidth() + i4 <= 0 || a6.getHeight() + i5 <= 0 || i4 > i6 || i5 > i7) {
                        i2 = 0;
                    } else {
                        i2 = 1;
                    }
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("x", Integer.valueOf(i4));
                    hashMap2.put("y", Integer.valueOf(i5));
                    hashMap2.put("width", Integer.valueOf(a6.getWidth()));
                    hashMap2.put("height", Integer.valueOf(a6.getHeight()));
                    hashMap2.put("visible", Integer.valueOf(i2));
                    map.put("ad_pos", hashMap2);
                }
            }
            StringBuilder sb = new StringBuilder();
            AdSize[] a7 = this.j.a.a().n.a();
            if (a7 != null) {
                for (AdSize adSize : a7) {
                    if (sb.length() != 0) {
                        sb.append("|");
                    }
                    sb.append(adSize.getWidth() + "x" + adSize.getHeight());
                }
                map.put("sz", sb.toString());
            }
            TelephonyManager telephonyManager = (TelephonyManager) applicationContext.getSystemService("phone");
            String networkOperator = telephonyManager.getNetworkOperator();
            if (!TextUtils.isEmpty(networkOperator)) {
                map.put("carrier", networkOperator);
            }
            map.put("pt", Integer.valueOf(telephonyManager.getPhoneType()));
            map.put("gnt", Integer.valueOf(telephonyManager.getNetworkType()));
            if (AdUtil.c()) {
                map.put("simulator", 1);
            }
            map.put("session_id", com.google.ads.b.a().b().toString());
            map.put("seq_num", com.google.ads.b.a().c().toString());
            if (this.j.a.a().g.a().b()) {
                map.put("swipeable", 1);
            }
            if (this.j.a.a().t.a().booleanValue()) {
                map.put("d_imp_hdr", 1);
            }
            String a8 = AdUtil.a(map);
            String str = this.j.a.a().d.a().b.a().o.a().booleanValue() ? g() + d() + "(" + a8 + ");" + h() : g() + e() + d() + "(" + a8 + ");" + h();
            com.google.ads.util.b.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e3) {
            throw new b("NameNotFoundException");
        }
    }

    private String d() {
        if (this.h instanceof SearchAdRequest) {
            return "AFMA_buildAdURL";
        }
        return "AFMA_buildAdURL";
    }

    private String e() {
        if (this.h instanceof SearchAdRequest) {
            return "AFMA_getSdkConstants();";
        }
        return "AFMA_getSdkConstants();";
    }

    private String f() {
        if (this.h instanceof SearchAdRequest) {
            return "http://www.gstatic.com/safa/";
        }
        return "http://media.admob.com/";
    }

    private String g() {
        if (this.h instanceof SearchAdRequest) {
            return "<html><head><script src=\"http://www.gstatic.com/safa/sdk-core-v40.js\"></script><script>";
        }
        return "<html><head><script src=\"http://media.admob.com/sdk-core-v40.js\"></script><script>";
    }

    private String h() {
        if (this.h instanceof SearchAdRequest) {
            return "</script></head><body></body></html>";
        }
        return "</script></head><body></body></html>";
    }

    /* access modifiers changed from: protected */
    public void a(AdRequest.ErrorCode errorCode, boolean z) {
        m.a().c.a().post(new a(this.j.a.a().b.a(), this.i, this.g, errorCode, z));
    }

    private void b(String str, String str2) {
        m.a().c.a().post(new C0000c(this.i, str2, str));
    }

    private void i() {
        AdWebView l2 = this.j.a.a().b.a().l();
        this.j.a.a().b.a().m().c(true);
        this.j.a.a().b.a().n().h();
        m.a().c.a().post(new C0000c(l2, this.b, this.c));
    }

    private void j() {
        m.a().c.a().post(new e(this.j.a.a().b.a(), this.i, this.m, this.u, this.r, this.n, this.o));
    }

    /* access modifiers changed from: protected */
    public synchronized void b(boolean z) {
        this.f = z;
    }

    /* access modifiers changed from: protected */
    public synchronized void b(String str) {
        this.e = str;
    }

    /* access modifiers changed from: protected */
    public synchronized void a(String str, String str2) {
        this.b = str2;
        this.c = str;
        notify();
    }

    /* access modifiers changed from: protected */
    public synchronized void c(String str) {
        this.d = str;
    }

    public synchronized void d(String str) {
        this.k = str;
        notify();
    }

    public synchronized void e(String str) {
        this.l = str;
    }

    public synchronized void f(String str) {
        this.n = str;
    }

    public synchronized void a(AdSize adSize) {
        this.o = adSize;
    }

    public synchronized void a(AdRequest.ErrorCode errorCode) {
        this.s = errorCode;
        notify();
    }

    /* access modifiers changed from: protected */
    public synchronized void c() {
        this.t = true;
        notify();
    }

    public synchronized void c(boolean z) {
        this.r = z;
    }

    public synchronized void a(int i2) {
        this.u = i2;
    }

    public synchronized void d(boolean z) {
        this.w = z;
    }

    public synchronized void a(d dVar) {
        this.x = dVar;
    }

    public synchronized void e(boolean z) {
        this.a = z;
    }
}
