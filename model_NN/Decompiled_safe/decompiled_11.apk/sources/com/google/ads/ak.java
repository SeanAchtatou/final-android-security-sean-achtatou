package com.google.ads;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import com.google.ads.an;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class ak extends aj {
    static boolean c = false;
    private static Method d = null;
    private static Method e = null;
    private static Method f = null;
    private static Method g = null;
    private static Method h = null;
    private static String i = null;
    private static long j = 0;

    static class a extends Exception {
        public a() {
        }

        public a(Throwable th) {
            super(th);
        }
    }

    public static ak a(String str, Context context) {
        b(str, context);
        return new ak(context);
    }

    protected static synchronized void b(String str, Context context) {
        synchronized (ak.class) {
            if (!c) {
                try {
                    i = str;
                    f(context);
                    j = b().longValue();
                    c = true;
                } catch (a | UnsupportedOperationException e2) {
                }
            }
        }
    }

    protected ak(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(android.content.Context r4) {
        /*
            r3 = this;
            r0 = 1
            java.lang.String r1 = c()     // Catch:{ a -> 0x002f, IOException -> 0x0027 }
            r3.a(r0, r1)     // Catch:{ a -> 0x002f, IOException -> 0x0027 }
        L_0x0008:
            r0 = 2
            java.lang.String r1 = a()     // Catch:{ a -> 0x002d, IOException -> 0x0027 }
            r3.a(r0, r1)     // Catch:{ a -> 0x002d, IOException -> 0x0027 }
        L_0x0010:
            r0 = 25
            java.lang.Long r1 = b()     // Catch:{ a -> 0x002b, IOException -> 0x0027 }
            long r1 = r1.longValue()     // Catch:{ a -> 0x002b, IOException -> 0x0027 }
            r3.a(r0, r1)     // Catch:{ a -> 0x002b, IOException -> 0x0027 }
        L_0x001d:
            r0 = 24
            java.lang.String r1 = d(r4)     // Catch:{ a -> 0x0029, IOException -> 0x0027 }
            r3.a(r0, r1)     // Catch:{ a -> 0x0029, IOException -> 0x0027 }
        L_0x0026:
            return
        L_0x0027:
            r0 = move-exception
            goto L_0x0026
        L_0x0029:
            r0 = move-exception
            goto L_0x0026
        L_0x002b:
            r0 = move-exception
            goto L_0x001d
        L_0x002d:
            r0 = move-exception
            goto L_0x0010
        L_0x002f:
            r0 = move-exception
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.ak.b(android.content.Context):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d A[ExcHandler: IOException (e java.io.IOException), Splitter:B:6:0x0010] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(android.content.Context r7) {
        /*
            r6 = this;
            r0 = 2
            java.lang.String r1 = a()     // Catch:{ a -> 0x0087, IOException -> 0x007d }
            r6.a(r0, r1)     // Catch:{ a -> 0x0087, IOException -> 0x007d }
        L_0x0008:
            r0 = 1
            java.lang.String r1 = c()     // Catch:{ a -> 0x0085, IOException -> 0x007d }
            r6.a(r0, r1)     // Catch:{ a -> 0x0085, IOException -> 0x007d }
        L_0x0010:
            java.lang.Long r0 = b()     // Catch:{ a -> 0x0083, IOException -> 0x007d }
            long r0 = r0.longValue()     // Catch:{ a -> 0x0083, IOException -> 0x007d }
            r2 = 25
            r6.a(r2, r0)     // Catch:{ a -> 0x0083, IOException -> 0x007d }
            long r2 = com.google.ads.ak.j     // Catch:{ a -> 0x0083, IOException -> 0x007d }
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 == 0) goto L_0x0034
            r2 = 17
            long r3 = com.google.ads.ak.j     // Catch:{ a -> 0x0083, IOException -> 0x007d }
            long r0 = r0 - r3
            r6.a(r2, r0)     // Catch:{ a -> 0x0083, IOException -> 0x007d }
            r0 = 23
            long r1 = com.google.ads.ak.j     // Catch:{ a -> 0x0083, IOException -> 0x007d }
            r6.a(r0, r1)     // Catch:{ a -> 0x0083, IOException -> 0x007d }
        L_0x0034:
            android.view.MotionEvent r0 = r6.a     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            android.util.DisplayMetrics r1 = r6.b     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            java.util.ArrayList r1 = a(r0, r1)     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            r2 = 14
            r0 = 0
            java.lang.Object r0 = r1.get(r0)     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            long r3 = r0.longValue()     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            r6.a(r2, r3)     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            r2 = 15
            r0 = 1
            java.lang.Object r0 = r1.get(r0)     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            long r3 = r0.longValue()     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            r6.a(r2, r3)     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            int r0 = r1.size()     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            r2 = 3
            if (r0 < r2) goto L_0x0073
            r2 = 16
            r0 = 2
            java.lang.Object r0 = r1.get(r0)     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            long r0 = r0.longValue()     // Catch:{ a -> 0x0081, IOException -> 0x007d }
            r6.a(r2, r0)     // Catch:{ a -> 0x0081, IOException -> 0x007d }
        L_0x0073:
            r0 = 27
            java.lang.String r1 = e(r7)     // Catch:{ a -> 0x007f, IOException -> 0x007d }
            r6.a(r0, r1)     // Catch:{ a -> 0x007f, IOException -> 0x007d }
        L_0x007c:
            return
        L_0x007d:
            r0 = move-exception
            goto L_0x007c
        L_0x007f:
            r0 = move-exception
            goto L_0x007c
        L_0x0081:
            r0 = move-exception
            goto L_0x0073
        L_0x0083:
            r0 = move-exception
            goto L_0x0034
        L_0x0085:
            r0 = move-exception
            goto L_0x0010
        L_0x0087:
            r0 = move-exception
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.ak.c(android.content.Context):void");
    }

    static String a() throws a {
        if (i != null) {
            return i;
        }
        throw new a();
    }

    static Long b() throws a {
        if (d == null) {
            throw new a();
        }
        try {
            return (Long) d.invoke(null, new Object[0]);
        } catch (IllegalAccessException e2) {
            throw new a(e2);
        } catch (InvocationTargetException e3) {
            throw new a(e3);
        }
    }

    static String d(Context context) throws a {
        if (h == null) {
            throw new a();
        }
        try {
            String str = (String) h.invoke(null, context);
            if (str != null) {
                return str;
            }
            throw new a();
        } catch (IllegalAccessException e2) {
            throw new a(e2);
        } catch (InvocationTargetException e3) {
            throw new a(e3);
        }
    }

    static String c() throws a {
        if (e == null) {
            throw new a();
        }
        try {
            return (String) e.invoke(null, new Object[0]);
        } catch (IllegalAccessException e2) {
            throw new a(e2);
        } catch (InvocationTargetException e3) {
            throw new a(e3);
        }
    }

    static ArrayList<Long> a(MotionEvent motionEvent, DisplayMetrics displayMetrics) throws a {
        if (g == null || motionEvent == null) {
            throw new a();
        }
        try {
            return (ArrayList) g.invoke(null, motionEvent, displayMetrics);
        } catch (IllegalAccessException e2) {
            throw new a(e2);
        } catch (InvocationTargetException e3) {
            throw new a(e3);
        }
    }

    static String e(Context context) throws a {
        if (f == null) {
            throw new a();
        }
        try {
            ByteBuffer byteBuffer = (ByteBuffer) f.invoke(null, context);
            if (byteBuffer != null) {
                return aq.a(byteBuffer.array(), false);
            }
            throw new a();
        } catch (IllegalAccessException e2) {
            throw new a(e2);
        } catch (InvocationTargetException e3) {
            throw new a(e3);
        }
    }

    private static String b(byte[] bArr, String str) throws a {
        try {
            return new String(an.a(bArr, str), "UTF-8");
        } catch (an.a e2) {
            throw new a(e2);
        } catch (ap e3) {
            throw new a(e3);
        } catch (UnsupportedEncodingException e4) {
            throw new a(e4);
        }
    }

    private static void f(Context context) throws a {
        try {
            byte[] a2 = an.a(ao.a());
            byte[] a3 = an.a(a2, ao.b());
            File cacheDir = context.getCacheDir();
            if (cacheDir == null && (cacheDir = context.getDir("dex", 0)) == null) {
                throw new a();
            }
            File createTempFile = File.createTempFile("ads", ".jar", cacheDir);
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            fileOutputStream.write(a3, 0, a3.length);
            fileOutputStream.close();
            DexClassLoader dexClassLoader = new DexClassLoader(createTempFile.getAbsolutePath(), cacheDir.getAbsolutePath(), null, context.getClassLoader());
            Class loadClass = dexClassLoader.loadClass(b(a2, ao.c()));
            Class loadClass2 = dexClassLoader.loadClass(b(a2, ao.i()));
            Class loadClass3 = dexClassLoader.loadClass(b(a2, ao.g()));
            Class loadClass4 = dexClassLoader.loadClass(b(a2, ao.k()));
            Class loadClass5 = dexClassLoader.loadClass(b(a2, ao.e()));
            d = loadClass.getMethod(b(a2, ao.d()), new Class[0]);
            e = loadClass2.getMethod(b(a2, ao.j()), new Class[0]);
            f = loadClass3.getMethod(b(a2, ao.h()), Context.class);
            g = loadClass4.getMethod(b(a2, ao.l()), MotionEvent.class, DisplayMetrics.class);
            h = loadClass5.getMethod(b(a2, ao.f()), Context.class);
            String name = createTempFile.getName();
            createTempFile.delete();
            new File(cacheDir, name.replace(".jar", ".dex")).delete();
        } catch (ap e2) {
            throw new a(e2);
        } catch (FileNotFoundException e3) {
            throw new a(e3);
        } catch (IOException e4) {
            throw new a(e4);
        } catch (ClassNotFoundException e5) {
            throw new a(e5);
        } catch (an.a e6) {
            throw new a(e6);
        } catch (NoSuchMethodException e7) {
            throw new a(e7);
        } catch (NullPointerException e8) {
            throw new a(e8);
        }
    }
}
