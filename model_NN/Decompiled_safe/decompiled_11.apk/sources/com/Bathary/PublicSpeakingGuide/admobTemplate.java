package com.Bathary.PublicSpeakingGuide;

import android.os.Bundle;
import android.util.Log;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import org.apache.cordova.DroidGap;

public class admobTemplate extends DroidGap implements AdListener {
    private static final String ADMOB_APPLICATION_ID = "a15280f17ef21d4";
    private AdView adView;
    private InterstitialAd interstitial;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setIntegerProperty("loadUrlTimeoutValue", 20000);
        setContentView((int) R.layout.main);
        this.interstitial = new InterstitialAd(this, ADMOB_APPLICATION_ID);
        this.interstitial.loadAd(new AdRequest());
        this.interstitial.setAdListener(this);
        super.loadUrl("file:///android_asset/www/index.html");
        this.adView = new AdView(this, AdSize.BANNER, ADMOB_APPLICATION_ID);
        this.root.addView(this.adView);
        this.adView.loadAd(new AdRequest());
        this.interstitial.show();
    }

    public void onReceiveAd(Ad ad) {
        Log.d("OK", "Received ad");
        if (ad == this.interstitial) {
            this.interstitial.show();
        }
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }
}
