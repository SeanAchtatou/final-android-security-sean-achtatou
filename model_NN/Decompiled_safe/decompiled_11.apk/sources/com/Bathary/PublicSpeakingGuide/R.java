package com.Bathary.PublicSpeakingGuide;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }

    public static final class xml {
        public static final int config = 2130968576;
    }
}
