package org.apache.cordova;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.widget.ViewDragHelper;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.squareup.okhttp.internal.spdy.SpdyStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.apache.cordova.api.CordovaInterface;
import org.apache.cordova.api.LOG;

@TargetApi(SpdyStream.RST_FRAME_TOO_LARGE)
public class IceCreamCordovaWebViewClient extends CordovaWebViewClient {
    public IceCreamCordovaWebViewClient(CordovaInterface cordova) {
        super(cordova);
    }

    public IceCreamCordovaWebViewClient(CordovaInterface cordova, CordovaWebView view) {
        super(cordova, view);
    }

    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        WebResourceResponse ret = super.shouldInterceptRequest(view, url);
        if (!Config.isUrlWhiteListed(url) && (url.startsWith("http://") || url.startsWith("https://"))) {
            return getWhitelistResponse();
        }
        if (ret == null && (url.contains("?") || url.contains("#") || needsIceCreamSpecialsInAssetUrlFix(url))) {
            return generateWebResourceResponse(url);
        }
        if (ret != null || this.appView.pluginManager == null) {
            return ret;
        }
        return this.appView.pluginManager.shouldInterceptRequest(url);
    }

    private WebResourceResponse getWhitelistResponse() {
        return new WebResourceResponse("text/plain", "UTF-8", new ByteArrayInputStream("".getBytes()));
    }

    private WebResourceResponse generateWebResourceResponse(String url) {
        if (url.startsWith("file:///android_asset/")) {
            try {
                return new WebResourceResponse(FileHelper.getMimeType(url, this.cordova), "UTF-8", FileHelper.getInputStreamFromUriString(url, this.cordova));
            } catch (IOException e) {
                LOG.e("generateWebResourceResponse", e.getMessage(), e);
            }
        }
        return null;
    }

    private static boolean needsIceCreamSpecialsInAssetUrlFix(String url) {
        if (!url.contains("%20")) {
            return false;
        }
        switch (Build.VERSION.SDK_INT) {
            case 14:
            case ViewDragHelper.EDGE_ALL:
                return true;
            default:
                return false;
        }
    }
}
