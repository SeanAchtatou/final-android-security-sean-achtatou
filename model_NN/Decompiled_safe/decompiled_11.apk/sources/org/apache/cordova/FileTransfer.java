package org.apache.cordova;

import android.util.Log;
import android.webkit.CookieManager;
import com.squareup.okhttp.OkHttpClient;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FileTransfer extends CordovaPlugin {
    public static int ABORTED_ERR = 4;
    private static final String BOUNDARY = "+++++";
    public static int CONNECTION_ERR = 3;
    /* access modifiers changed from: private */
    public static final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    public static int FILE_NOT_FOUND_ERR = 1;
    public static int INVALID_URL_ERR = 2;
    private static final String LINE_END = "\r\n";
    private static final String LINE_START = "--";
    private static final String LOG_TAG = "FileTransfer";
    private static final int MAX_BUFFER_SIZE = 16384;
    /* access modifiers changed from: private */
    public static HashMap<String, RequestContext> activeRequests = new HashMap<>();
    /* access modifiers changed from: private */
    public static OkHttpClient httpClient = new OkHttpClient();
    private static final TrustManager[] trustAllCerts = {new X509TrustManager() {
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }
    }};

    private static final class RequestContext {
        boolean aborted;
        CallbackContext callbackContext;
        InputStream currentInputStream;
        OutputStream currentOutputStream;
        String source;
        String target;
        File targetFile;

        RequestContext(String source2, String target2, CallbackContext callbackContext2) {
            this.source = source2;
            this.target = target2;
            this.callbackContext = callbackContext2;
        }

        /* access modifiers changed from: package-private */
        public void sendPluginResult(PluginResult pluginResult) {
            synchronized (this) {
                if (!this.aborted) {
                    this.callbackContext.sendPluginResult(pluginResult);
                }
            }
        }
    }

    private static abstract class TrackingInputStream extends FilterInputStream {
        public abstract long getTotalRawBytesRead();

        public TrackingInputStream(InputStream in) {
            super(in);
        }
    }

    private static class ExposedGZIPInputStream extends GZIPInputStream {
        public ExposedGZIPInputStream(InputStream in) throws IOException {
            super(in);
        }

        public Inflater getInflater() {
            return this.inf;
        }
    }

    private static class TrackingGZIPInputStream extends TrackingInputStream {
        private ExposedGZIPInputStream gzin;

        public TrackingGZIPInputStream(ExposedGZIPInputStream gzin2) throws IOException {
            super(gzin2);
            this.gzin = gzin2;
        }

        public long getTotalRawBytesRead() {
            return this.gzin.getInflater().getBytesRead();
        }
    }

    private static class TrackingHTTPInputStream extends TrackingInputStream {
        private long bytesRead = 0;

        public TrackingHTTPInputStream(InputStream stream) {
            super(stream);
        }

        private int updateBytesRead(int newBytesRead) {
            if (newBytesRead != -1) {
                this.bytesRead += (long) newBytesRead;
            }
            return newBytesRead;
        }

        public int read() throws IOException {
            return updateBytesRead(super.read());
        }

        public int read(byte[] buffer) throws IOException {
            return updateBytesRead(super.read(buffer));
        }

        public int read(byte[] bytes, int offset, int count) throws IOException {
            return updateBytesRead(super.read(bytes, offset, count));
        }

        public long getTotalRawBytesRead() {
            return this.bytesRead;
        }
    }

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("upload") || action.equals("download")) {
            String source = args.getString(0);
            String target = args.getString(1);
            if (action.equals("upload")) {
                try {
                    upload(URLDecoder.decode(source, "UTF-8"), target, args, callbackContext);
                    return true;
                } catch (UnsupportedEncodingException e) {
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.MALFORMED_URL_EXCEPTION, "UTF-8 error."));
                    return true;
                }
            } else {
                download(source, target, args, callbackContext);
                return true;
            }
        } else if (!action.equals("abort")) {
            return false;
        } else {
            abort(args.getString(0));
            callbackContext.success();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public static void addHeadersToRequest(URLConnection connection, JSONObject headers) {
        try {
            Iterator<?> iter = headers.keys();
            while (iter.hasNext()) {
                String headerKey = iter.next().toString();
                JSONArray headerValues = headers.optJSONArray(headerKey);
                if (headerValues == null) {
                    headerValues = new JSONArray();
                    headerValues.put(headers.getString(headerKey));
                }
                connection.setRequestProperty(headerKey, headerValues.getString(0));
                for (int i = 1; i < headerValues.length(); i++) {
                    connection.addRequestProperty(headerKey, headerValues.getString(i));
                }
            }
        } catch (JSONException e) {
        }
    }

    private void upload(String source, String target, JSONArray args, CallbackContext callbackContext) throws JSONException {
        final JSONObject headers;
        Log.d(LOG_TAG, "upload " + source + " to " + target);
        final String fileKey = getArgument(args, 2, "file");
        final String fileName = getArgument(args, 3, "image.jpg");
        final String mimeType = getArgument(args, 4, "image/jpeg");
        final JSONObject params = args.optJSONObject(5) == null ? new JSONObject() : args.optJSONObject(5);
        final boolean trustEveryone = args.optBoolean(6);
        final boolean chunkedMode = args.optBoolean(7) || args.isNull(7);
        if (args.optJSONObject(8) == null) {
            headers = params.optJSONObject("headers");
        } else {
            headers = args.optJSONObject(8);
        }
        final String objectId = args.getString(9);
        final String httpMethod = getArgument(args, 10, "POST");
        Log.d(LOG_TAG, "fileKey: " + fileKey);
        Log.d(LOG_TAG, "fileName: " + fileName);
        Log.d(LOG_TAG, "mimeType: " + mimeType);
        Log.d(LOG_TAG, "params: " + params);
        Log.d(LOG_TAG, "trustEveryone: " + trustEveryone);
        Log.d(LOG_TAG, "chunkedMode: " + chunkedMode);
        Log.d(LOG_TAG, "headers: " + headers);
        Log.d(LOG_TAG, "objectId: " + objectId);
        Log.d(LOG_TAG, "httpMethod: " + httpMethod);
        try {
            final URL url = new URL(target);
            final boolean useHttps = url.getProtocol().equals("https");
            final RequestContext context = new RequestContext(source, target, callbackContext);
            synchronized (activeRequests) {
                activeRequests.put(objectId, context);
            }
            final String str = target;
            final String str2 = source;
            this.cordova.getThreadPool().execute(new Runnable() {
                /* JADX WARNING: Code restructure failed: missing block: B:100:0x0399, code lost:
                    if (((long) r33) <= (102400 + r21)) goto L_0x03ce;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:101:0x039b, code lost:
                    r21 = (long) r33;
                    android.util.Log.d(org.apache.cordova.FileTransfer.LOG_TAG, "Uploaded " + r33 + " of " + r13 + " bytes");
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:102:0x03ce, code lost:
                    r8 = r29.read(r5, 0, java.lang.Math.min(r29.available(), 16384));
                    r23.setLoaded((long) r33);
                    r0 = new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.OK, r23.toJSONObject());
                    r0.setKeepCallback(true);
                    r5.sendPluginResult(r0);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:107:0x0421, code lost:
                    r11 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
                    r12 = org.apache.cordova.FileTransfer.access$700(org.apache.cordova.FileTransfer.CONNECTION_ERR, r16, r10, r9);
                    android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r12.toString(), r11);
                    android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, "Failed after uploading " + r33 + " of " + r13 + " bytes.");
                    r5.sendPluginResult(new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r12));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:111:0x048d, code lost:
                    monitor-enter(org.apache.cordova.FileTransfer.access$800());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:113:?, code lost:
                    org.apache.cordova.FileTransfer.access$800().remove(r18);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:115:0x04a0, code lost:
                    if (r9 == null) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:120:0x04b2, code lost:
                    r14 = (javax.net.ssl.HttpsURLConnection) r9;
                    r14.setHostnameVerifier(r18);
                    r14.setSSLSocketFactory(r19);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:126:0x04c4, code lost:
                    r28.write(r32);
                    r33 = r33 + r32.length;
                    r28.flush();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:128:?, code lost:
                    org.apache.cordova.FileTransfer.access$500(r29);
                    org.apache.cordova.FileTransfer.access$500(r28);
                    r5.currentOutputStream = null;
                    android.util.Log.d(org.apache.cordova.FileTransfer.LOG_TAG, "Sent " + r33 + " of " + r13);
                    r25 = r9.getResponseCode();
                    android.util.Log.d(org.apache.cordova.FileTransfer.LOG_TAG, "response code: " + r25);
                    android.util.Log.d(org.apache.cordova.FileTransfer.LOG_TAG, "response headers: " + r9.getHeaderFields());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:129:0x054d, code lost:
                    r15 = null;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:131:?, code lost:
                    r15 = org.apache.cordova.FileTransfer.access$600(r9);
                    r36 = r5;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:132:0x0558, code lost:
                    monitor-enter(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:136:0x0565, code lost:
                    if (r5.aborted == false) goto L_0x05b4;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:137:0x0567, code lost:
                    monitor-exit(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:140:?, code lost:
                    r5.currentInputStream = null;
                    org.apache.cordova.FileTransfer.access$500(r15);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:141:0x0579, code lost:
                    r36 = org.apache.cordova.FileTransfer.access$800();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:142:0x057d, code lost:
                    monitor-enter(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:144:?, code lost:
                    org.apache.cordova.FileTransfer.access$800().remove(r18);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:145:0x058f, code lost:
                    monitor-exit(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:146:0x0590, code lost:
                    if (r9 == null) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:148:0x0598, code lost:
                    if (r7 == false) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:150:0x05a0, code lost:
                    if (r6 == false) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:151:0x05a2, code lost:
                    r14 = (javax.net.ssl.HttpsURLConnection) r9;
                    r14.setHostnameVerifier(r18);
                    r14.setSSLSocketFactory(r19);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:158:?, code lost:
                    r5.currentInputStream = r15;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:159:0x05be, code lost:
                    monitor-exit(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:161:?, code lost:
                    r0 = new java.io.ByteArrayOutputStream(java.lang.Math.max(1024, r9.getContentLength()));
                    r5 = new byte[1024];
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:162:0x05d9, code lost:
                    r8 = r15.read(r5);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:163:0x05dd, code lost:
                    if (r8 <= 0) goto L_0x0655;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:164:0x05df, code lost:
                    r0.write(r5, 0, r8);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:190:0x0655, code lost:
                    r26 = r0.toString("UTF-8");
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:193:?, code lost:
                    r5.currentInputStream = null;
                    org.apache.cordova.FileTransfer.access$500(r15);
                    android.util.Log.d(org.apache.cordova.FileTransfer.LOG_TAG, "got response from server");
                    android.util.Log.d(org.apache.cordova.FileTransfer.LOG_TAG, r26.substring(0, java.lang.Math.min(256, r26.length())));
                    r27.setResponseCode(r25);
                    r27.setResponse(r26);
                    r5.sendPluginResult(new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.OK, r27.toJSONObject()));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:194:0x06b4, code lost:
                    r36 = org.apache.cordova.FileTransfer.access$800();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:195:0x06b8, code lost:
                    monitor-enter(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:197:?, code lost:
                    org.apache.cordova.FileTransfer.access$800().remove(r18);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:198:0x06ca, code lost:
                    monitor-exit(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:199:0x06cb, code lost:
                    if (r9 == null) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:201:0x06d3, code lost:
                    if (r7 == false) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:203:0x06db, code lost:
                    if (r6 == false) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:204:0x06dd, code lost:
                    r14 = (javax.net.ssl.HttpsURLConnection) r9;
                    r14.setHostnameVerifier(r18);
                    r14.setSSLSocketFactory(r19);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:221:0x06f8, code lost:
                    r31 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:223:?, code lost:
                    r12 = org.apache.cordova.FileTransfer.access$700(org.apache.cordova.FileTransfer.CONNECTION_ERR, r16, r10, r9);
                    android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r12.toString(), r31);
                    r5.sendPluginResult(new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r12));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:225:0x0738, code lost:
                    monitor-enter(org.apache.cordova.FileTransfer.access$800());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:227:?, code lost:
                    org.apache.cordova.FileTransfer.access$800().remove(r18);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:229:0x074b, code lost:
                    if (r9 == null) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:234:0x075d, code lost:
                    r14 = (javax.net.ssl.HttpsURLConnection) r9;
                    r14.setHostnameVerifier(r18);
                    r14.setSSLSocketFactory(r19);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x010e, code lost:
                    r11 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
                    android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r11.getMessage(), r11);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:295:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:296:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:297:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:298:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:301:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:302:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:305:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:306:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:307:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:308:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:309:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:310:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:315:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:316:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:317:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:318:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:321:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:322:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
                    org.apache.cordova.FileTransfer.access$500(r29);
                    org.apache.cordova.FileTransfer.access$500(r28);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:56:0x0255, code lost:
                    r36 = org.apache.cordova.FileTransfer.access$800();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:57:0x0259, code lost:
                    monitor-enter(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
                    org.apache.cordova.FileTransfer.access$800().remove(r18);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:60:0x026b, code lost:
                    monitor-exit(r36);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:61:0x026c, code lost:
                    if (r9 == null) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:63:0x0274, code lost:
                    if (r7 == false) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:65:0x027c, code lost:
                    if (r6 == false) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:66:0x027e, code lost:
                    r14 = (javax.net.ssl.HttpsURLConnection) r9;
                    r14.setHostnameVerifier(r18);
                    r14.setSSLSocketFactory(r19);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:73:0x02ce, code lost:
                    r11 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
                    r12 = org.apache.cordova.FileTransfer.access$700(org.apache.cordova.FileTransfer.FILE_NOT_FOUND_ERR, r16, r10, r9);
                    android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r12.toString(), r11);
                    r5.sendPluginResult(new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r12));
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:77:0x030c, code lost:
                    monitor-enter(org.apache.cordova.FileTransfer.access$800());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
                    org.apache.cordova.FileTransfer.access$800().remove(r18);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:81:0x031f, code lost:
                    if (r9 == null) goto L_?;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:86:0x0331, code lost:
                    r14 = (javax.net.ssl.HttpsURLConnection) r9;
                    r14.setHostnameVerifier(r18);
                    r14.setSSLSocketFactory(r19);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
                    r28.write(r4);
                    r33 = 0 + r4.length;
                    r6 = java.lang.Math.min(r29.available(), 16384);
                    r5 = new byte[r6];
                    r8 = r29.read(r5, 0, r6);
                    r21 = 0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:98:0x0374, code lost:
                    if (r8 <= 0) goto L_0x04c4;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:99:0x0376, code lost:
                    r27.setBytesSent((long) r33);
                    r28.write(r5, 0, r8);
                    r33 = r33 + r8;
                 */
                /* JADX WARNING: Failed to process nested try/catch */
                /* JADX WARNING: Removed duplicated region for block: B:107:0x0421 A[ExcHandler: IOException (r11v1 'e' java.io.IOException A[CUSTOM_DECLARE]), PHI: r9 r13 r18 r19 r33 
                  PHI: (r9v4 'conn' java.net.HttpURLConnection) = (r9v0 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v0 'conn' java.net.HttpURLConnection) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE]
                  PHI: (r13v1 'fixedLength' int) = (r13v0 'fixedLength' int), (r13v0 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v2 'fixedLength' int), (r13v0 'fixedLength' int), (r13v0 'fixedLength' int) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE]
                  PHI: (r18v4 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier) = (r18v0 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v0 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE]
                  PHI: (r19v4 'oldSocketFactory' javax.net.ssl.SSLSocketFactory) = (r19v0 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v0 'oldSocketFactory' javax.net.ssl.SSLSocketFactory) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE]
                  PHI: (r33v1 'totalBytes' int) = (r33v0 'totalBytes' int), (r33v0 'totalBytes' int), (r33v0 'totalBytes' int), (r33v4 'totalBytes' int), (r33v4 'totalBytes' int), (r33v4 'totalBytes' int), (r33v4 'totalBytes' int), (r33v4 'totalBytes' int), (r33v4 'totalBytes' int), (r33v4 'totalBytes' int), (r33v0 'totalBytes' int), (r33v0 'totalBytes' int), (r33v0 'totalBytes' int), (r33v0 'totalBytes' int) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0017] */
                /* JADX WARNING: Removed duplicated region for block: B:221:0x06f8 A[ExcHandler: Throwable (r31v0 't' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r9 r18 r19 
                  PHI: (r9v2 'conn' java.net.HttpURLConnection) = (r9v0 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v0 'conn' java.net.HttpURLConnection) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE]
                  PHI: (r18v2 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier) = (r18v0 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v0 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE]
                  PHI: (r19v2 'oldSocketFactory' javax.net.ssl.SSLSocketFactory) = (r19v0 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v0 'oldSocketFactory' javax.net.ssl.SSLSocketFactory) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0017] */
                /* JADX WARNING: Removed duplicated region for block: B:73:0x02ce A[ExcHandler: FileNotFoundException (r11v2 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), PHI: r9 r18 r19 
                  PHI: (r9v5 'conn' java.net.HttpURLConnection) = (r9v0 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v7 'conn' java.net.HttpURLConnection), (r9v0 'conn' java.net.HttpURLConnection) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE]
                  PHI: (r18v5 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier) = (r18v0 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v6 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier), (r18v0 'oldHostnameVerifier' javax.net.ssl.HostnameVerifier) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE]
                  PHI: (r19v5 'oldSocketFactory' javax.net.ssl.SSLSocketFactory) = (r19v0 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v6 'oldSocketFactory' javax.net.ssl.SSLSocketFactory), (r19v0 'oldSocketFactory' javax.net.ssl.SSLSocketFactory) binds: [B:3:0x0017, B:17:0x00a0, B:104:0x041a, B:127:0x04d5, B:128:?, B:167:0x05ec, B:192:0x0661, B:193:?, B:139:0x056a, B:140:?, B:54:0x024f, B:55:?, B:26:0x0111, B:67:0x028d] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0017] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r39 = this;
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5
                        r35 = r0
                        r0 = r35
                        boolean r0 = r0.aborted
                        r35 = r0
                        if (r35 == 0) goto L_0x000f
                    L_0x000e:
                        return
                    L_0x000f:
                        r9 = 0
                        r18 = 0
                        r19 = 0
                        r33 = 0
                        r13 = -1
                        org.apache.cordova.FileUploadResult r27 = new org.apache.cordova.FileUploadResult     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r27.<init>()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.apache.cordova.FileProgressResult r23 = new org.apache.cordova.FileProgressResult     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r23.<init>()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        boolean r0 = r6     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        if (r35 == 0) goto L_0x02b1
                        r0 = r39
                        boolean r0 = r7     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        if (r35 != 0) goto L_0x028d
                        com.squareup.okhttp.OkHttpClient r35 = org.apache.cordova.FileTransfer.httpClient     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        java.net.URL r0 = r8     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        java.net.HttpURLConnection r35 = r35.open(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r35
                        javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r9 = r0
                    L_0x0044:
                        r35 = 1
                        r0 = r35
                        r9.setDoInput(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = 1
                        r0 = r35
                        r9.setDoOutput(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = 0
                        r0 = r35
                        r9.setUseCaches(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        java.lang.String r0 = r9     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        r0 = r35
                        r9.setRequestMethod(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "Content-Type"
                        java.lang.String r36 = "multipart/form-data;boundary=+++++"
                        r0 = r35
                        r1 = r36
                        r9.setRequestProperty(r0, r1)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        android.webkit.CookieManager r35 = android.webkit.CookieManager.getInstance()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        java.lang.String r0 = r10     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        java.lang.String r10 = r35.getCookie(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        if (r10 == 0) goto L_0x0086
                        java.lang.String r35 = "Cookie"
                        r0 = r35
                        r9.setRequestProperty(r0, r10)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                    L_0x0086:
                        r0 = r39
                        org.json.JSONObject r0 = r11     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        if (r35 == 0) goto L_0x0099
                        r0 = r39
                        org.json.JSONObject r0 = r11     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        r0 = r35
                        org.apache.cordova.FileTransfer.addHeadersToRequest(r9, r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                    L_0x0099:
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r3.<init>()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        org.json.JSONObject r0 = r12     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        r35 = r0
                        java.util.Iterator r16 = r35.keys()     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                    L_0x00a8:
                        boolean r35 = r16.hasNext()     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        if (r35 == 0) goto L_0x011c
                        java.lang.Object r17 = r16.next()     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r35 = java.lang.String.valueOf(r17)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r36 = "headers"
                        boolean r35 = r35.equals(r36)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        if (r35 != 0) goto L_0x00a8
                        java.lang.String r35 = "--"
                        r0 = r35
                        java.lang.StringBuilder r35 = r3.append(r0)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r36 = "+++++"
                        java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r36 = "\r\n"
                        r35.append(r36)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r35 = "Content-Disposition: form-data; name=\""
                        r0 = r35
                        java.lang.StringBuilder r35 = r3.append(r0)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r36 = r17.toString()     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        r36 = 34
                        r35.append(r36)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r35 = "\r\n"
                        r0 = r35
                        java.lang.StringBuilder r35 = r3.append(r0)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r36 = "\r\n"
                        r35.append(r36)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        r0 = r39
                        org.json.JSONObject r0 = r12     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        r35 = r0
                        java.lang.String r36 = r17.toString()     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r35 = r35.getString(r36)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        r0 = r35
                        r3.append(r0)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        java.lang.String r35 = "\r\n"
                        r0 = r35
                        r3.append(r0)     // Catch:{ JSONException -> 0x010e, FileNotFoundException -> 0x02ce, IOException -> 0x0421, Throwable -> 0x06f8 }
                        goto L_0x00a8
                    L_0x010e:
                        r11 = move-exception
                        java.lang.String r35 = "FileTransfer"
                        java.lang.String r36 = r11.getMessage()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r35
                        r1 = r36
                        android.util.Log.e(r0, r1, r11)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                    L_0x011c:
                        java.lang.String r35 = "--"
                        r0 = r35
                        java.lang.StringBuilder r35 = r3.append(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = "+++++"
                        java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = "\r\n"
                        r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "Content-Disposition: form-data; name=\""
                        r0 = r35
                        java.lang.StringBuilder r35 = r3.append(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        java.lang.String r0 = r13     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = "\";"
                        r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = " filename=\""
                        r0 = r35
                        java.lang.StringBuilder r35 = r3.append(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        java.lang.String r0 = r14     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = 34
                        java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = "\r\n"
                        r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "Content-Type: "
                        r0 = r35
                        java.lang.StringBuilder r35 = r3.append(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        java.lang.String r0 = r15     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = "\r\n"
                        java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = "\r\n"
                        r35.append(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = r3.toString()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = "UTF-8"
                        byte[] r4 = r35.getBytes(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "\r\n--+++++--\r\n"
                        java.lang.String r36 = "UTF-8"
                        byte[] r32 = r35.getBytes(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        org.apache.cordova.FileTransfer r0 = org.apache.cordova.FileTransfer.this     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        r0 = r39
                        java.lang.String r0 = r16     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        java.io.InputStream r29 = r35.getPathFromUri(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        int r0 = r4.length     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        r0 = r32
                        int r0 = r0.length     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        int r30 = r35 + r36
                        r0 = r29
                        boolean r0 = r0 instanceof java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        if (r35 == 0) goto L_0x01dc
                        r0 = r29
                        java.io.FileInputStream r0 = (java.io.FileInputStream) r0     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        java.nio.channels.FileChannel r35 = r35.getChannel()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        long r35 = r35.size()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r35
                        int r0 = (int) r0     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        int r13 = r35 + r30
                        r35 = 1
                        r0 = r23
                        r1 = r35
                        r0.setLengthComputable(r1)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        long r0 = (long) r13     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        r0 = r23
                        r1 = r35
                        r0.setTotal(r1)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                    L_0x01dc:
                        java.lang.String r35 = "FileTransfer"
                        java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36.<init>()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r37 = "Content Length: "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r36
                        java.lang.StringBuilder r36 = r0.append(r13)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = r36.toString()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        android.util.Log.d(r35, r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        boolean r0 = r17     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        if (r35 == 0) goto L_0x02c1
                        int r35 = android.os.Build.VERSION.SDK_INT     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = 8
                        r0 = r35
                        r1 = r36
                        if (r0 < r1) goto L_0x0210
                        r0 = r39
                        boolean r0 = r6     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        if (r35 == 0) goto L_0x02c1
                    L_0x0210:
                        r34 = 1
                    L_0x0212:
                        if (r34 != 0) goto L_0x021a
                        r35 = -1
                        r0 = r35
                        if (r13 != r0) goto L_0x02c5
                    L_0x021a:
                        r34 = 1
                    L_0x021c:
                        if (r34 == 0) goto L_0x02c9
                        r35 = 16384(0x4000, float:2.2959E-41)
                        r0 = r35
                        r9.setChunkedStreamingMode(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "Transfer-Encoding"
                        java.lang.String r36 = "chunked"
                        r0 = r35
                        r1 = r36
                        r9.setRequestProperty(r0, r1)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                    L_0x0230:
                        r9.connect()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r28 = 0
                        java.io.OutputStream r28 = r9.getOutputStream()     // Catch:{ all -> 0x0419 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x0419 }
                        r36 = r0
                        monitor-enter(r36)     // Catch:{ all -> 0x0419 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x04c1 }
                        r35 = r0
                        r0 = r35
                        boolean r0 = r0.aborted     // Catch:{ all -> 0x04c1 }
                        r35 = r0
                        if (r35 == 0) goto L_0x0343
                        monitor-exit(r36)     // Catch:{ all -> 0x04c1 }
                        org.apache.cordova.FileTransfer.safeClose(r29)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.apache.cordova.FileTransfer.safeClose(r28)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.util.HashMap r36 = org.apache.cordova.FileTransfer.activeRequests
                        monitor-enter(r36)
                        java.util.HashMap r35 = org.apache.cordova.FileTransfer.activeRequests     // Catch:{ all -> 0x0340 }
                        r0 = r39
                        java.lang.String r0 = r18     // Catch:{ all -> 0x0340 }
                        r37 = r0
                        r0 = r35
                        r1 = r37
                        r0.remove(r1)     // Catch:{ all -> 0x0340 }
                        monitor-exit(r36)     // Catch:{ all -> 0x0340 }
                        if (r9 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r7
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r6
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r14 = r9
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14
                        r0 = r18
                        r14.setHostnameVerifier(r0)
                        r0 = r19
                        r14.setSSLSocketFactory(r0)
                        goto L_0x000e
                    L_0x028d:
                        com.squareup.okhttp.OkHttpClient r35 = org.apache.cordova.FileTransfer.httpClient     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        java.net.URL r0 = r8     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        java.net.HttpURLConnection r14 = r35.open(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        javax.net.ssl.SSLSocketFactory r19 = org.apache.cordova.FileTransfer.trustAllHosts(r14)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        javax.net.ssl.HostnameVerifier r18 = r14.getHostnameVerifier()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        javax.net.ssl.HostnameVerifier r35 = org.apache.cordova.FileTransfer.DO_NOT_VERIFY     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r35
                        r14.setHostnameVerifier(r0)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r9 = r14
                        goto L_0x0044
                    L_0x02b1:
                        com.squareup.okhttp.OkHttpClient r35 = org.apache.cordova.FileTransfer.httpClient     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        java.net.URL r0 = r8     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        java.net.HttpURLConnection r9 = r35.open(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        goto L_0x0044
                    L_0x02c1:
                        r34 = 0
                        goto L_0x0212
                    L_0x02c5:
                        r34 = 0
                        goto L_0x021c
                    L_0x02c9:
                        r9.setFixedLengthStreamingMode(r13)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        goto L_0x0230
                    L_0x02ce:
                        r11 = move-exception
                        int r35 = org.apache.cordova.FileTransfer.FILE_NOT_FOUND_ERR     // Catch:{ all -> 0x076f }
                        r0 = r39
                        java.lang.String r0 = r16     // Catch:{ all -> 0x076f }
                        r36 = r0
                        r0 = r39
                        java.lang.String r0 = r10     // Catch:{ all -> 0x076f }
                        r37 = r0
                        r0 = r35
                        r1 = r36
                        r2 = r37
                        org.json.JSONObject r12 = org.apache.cordova.FileTransfer.createFileTransferError(r0, r1, r2, r9)     // Catch:{ all -> 0x076f }
                        java.lang.String r35 = "FileTransfer"
                        java.lang.String r36 = r12.toString()     // Catch:{ all -> 0x076f }
                        r0 = r35
                        r1 = r36
                        android.util.Log.e(r0, r1, r11)     // Catch:{ all -> 0x076f }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x076f }
                        r35 = r0
                        org.apache.cordova.api.PluginResult r36 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x076f }
                        org.apache.cordova.api.PluginResult$Status r37 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x076f }
                        r0 = r36
                        r1 = r37
                        r0.<init>(r1, r12)     // Catch:{ all -> 0x076f }
                        r35.sendPluginResult(r36)     // Catch:{ all -> 0x076f }
                        java.util.HashMap r36 = org.apache.cordova.FileTransfer.activeRequests
                        monitor-enter(r36)
                        java.util.HashMap r35 = org.apache.cordova.FileTransfer.activeRequests     // Catch:{ all -> 0x06ef }
                        r0 = r39
                        java.lang.String r0 = r18     // Catch:{ all -> 0x06ef }
                        r37 = r0
                        r0 = r35
                        r1 = r37
                        r0.remove(r1)     // Catch:{ all -> 0x06ef }
                        monitor-exit(r36)     // Catch:{ all -> 0x06ef }
                        if (r9 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r7
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r6
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r14 = r9
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14
                        r0 = r18
                        r14.setHostnameVerifier(r0)
                        r0 = r19
                        r14.setSSLSocketFactory(r0)
                        goto L_0x000e
                    L_0x0340:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x0340 }
                        throw r35
                    L_0x0343:
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x04c1 }
                        r35 = r0
                        r0 = r28
                        r1 = r35
                        r1.currentOutputStream = r0     // Catch:{ all -> 0x04c1 }
                        monitor-exit(r36)     // Catch:{ all -> 0x04c1 }
                        r0 = r28
                        r0.write(r4)     // Catch:{ all -> 0x0419 }
                        int r0 = r4.length     // Catch:{ all -> 0x0419 }
                        r35 = r0
                        int r33 = r33 + r35
                        int r7 = r29.available()     // Catch:{ all -> 0x0419 }
                        r35 = 16384(0x4000, float:2.2959E-41)
                        r0 = r35
                        int r6 = java.lang.Math.min(r7, r0)     // Catch:{ all -> 0x0419 }
                        byte[] r5 = new byte[r6]     // Catch:{ all -> 0x0419 }
                        r35 = 0
                        r0 = r29
                        r1 = r35
                        int r8 = r0.read(r5, r1, r6)     // Catch:{ all -> 0x0419 }
                        r21 = 0
                    L_0x0374:
                        if (r8 <= 0) goto L_0x04c4
                        r0 = r33
                        long r0 = (long) r0     // Catch:{ all -> 0x0419 }
                        r35 = r0
                        r0 = r27
                        r1 = r35
                        r0.setBytesSent(r1)     // Catch:{ all -> 0x0419 }
                        r35 = 0
                        r0 = r28
                        r1 = r35
                        r0.write(r5, r1, r8)     // Catch:{ all -> 0x0419 }
                        int r33 = r33 + r8
                        r0 = r33
                        long r0 = (long) r0     // Catch:{ all -> 0x0419 }
                        r35 = r0
                        r37 = 102400(0x19000, double:5.05923E-319)
                        long r37 = r37 + r21
                        int r35 = (r35 > r37 ? 1 : (r35 == r37 ? 0 : -1))
                        if (r35 <= 0) goto L_0x03ce
                        r0 = r33
                        long r0 = (long) r0     // Catch:{ all -> 0x0419 }
                        r21 = r0
                        java.lang.String r35 = "FileTransfer"
                        java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ all -> 0x0419 }
                        r36.<init>()     // Catch:{ all -> 0x0419 }
                        java.lang.String r37 = "Uploaded "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ all -> 0x0419 }
                        r0 = r36
                        r1 = r33
                        java.lang.StringBuilder r36 = r0.append(r1)     // Catch:{ all -> 0x0419 }
                        java.lang.String r37 = " of "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ all -> 0x0419 }
                        r0 = r36
                        java.lang.StringBuilder r36 = r0.append(r13)     // Catch:{ all -> 0x0419 }
                        java.lang.String r37 = " bytes"
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ all -> 0x0419 }
                        java.lang.String r36 = r36.toString()     // Catch:{ all -> 0x0419 }
                        android.util.Log.d(r35, r36)     // Catch:{ all -> 0x0419 }
                    L_0x03ce:
                        int r7 = r29.available()     // Catch:{ all -> 0x0419 }
                        r35 = 16384(0x4000, float:2.2959E-41)
                        r0 = r35
                        int r6 = java.lang.Math.min(r7, r0)     // Catch:{ all -> 0x0419 }
                        r35 = 0
                        r0 = r29
                        r1 = r35
                        int r8 = r0.read(r5, r1, r6)     // Catch:{ all -> 0x0419 }
                        r0 = r33
                        long r0 = (long) r0     // Catch:{ all -> 0x0419 }
                        r35 = r0
                        r0 = r23
                        r1 = r35
                        r0.setLoaded(r1)     // Catch:{ all -> 0x0419 }
                        org.apache.cordova.api.PluginResult r24 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x0419 }
                        org.apache.cordova.api.PluginResult$Status r35 = org.apache.cordova.api.PluginResult.Status.OK     // Catch:{ all -> 0x0419 }
                        org.json.JSONObject r36 = r23.toJSONObject()     // Catch:{ all -> 0x0419 }
                        r0 = r24
                        r1 = r35
                        r2 = r36
                        r0.<init>(r1, r2)     // Catch:{ all -> 0x0419 }
                        r35 = 1
                        r0 = r24
                        r1 = r35
                        r0.setKeepCallback(r1)     // Catch:{ all -> 0x0419 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x0419 }
                        r35 = r0
                        r0 = r35
                        r1 = r24
                        r0.sendPluginResult(r1)     // Catch:{ all -> 0x0419 }
                        goto L_0x0374
                    L_0x0419:
                        r35 = move-exception
                        org.apache.cordova.FileTransfer.safeClose(r29)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.apache.cordova.FileTransfer.safeClose(r28)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        throw r35     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                    L_0x0421:
                        r11 = move-exception
                        int r35 = org.apache.cordova.FileTransfer.CONNECTION_ERR     // Catch:{ all -> 0x076f }
                        r0 = r39
                        java.lang.String r0 = r16     // Catch:{ all -> 0x076f }
                        r36 = r0
                        r0 = r39
                        java.lang.String r0 = r10     // Catch:{ all -> 0x076f }
                        r37 = r0
                        r0 = r35
                        r1 = r36
                        r2 = r37
                        org.json.JSONObject r12 = org.apache.cordova.FileTransfer.createFileTransferError(r0, r1, r2, r9)     // Catch:{ all -> 0x076f }
                        java.lang.String r35 = "FileTransfer"
                        java.lang.String r36 = r12.toString()     // Catch:{ all -> 0x076f }
                        r0 = r35
                        r1 = r36
                        android.util.Log.e(r0, r1, r11)     // Catch:{ all -> 0x076f }
                        java.lang.String r35 = "FileTransfer"
                        java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ all -> 0x076f }
                        r36.<init>()     // Catch:{ all -> 0x076f }
                        java.lang.String r37 = "Failed after uploading "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ all -> 0x076f }
                        r0 = r36
                        r1 = r33
                        java.lang.StringBuilder r36 = r0.append(r1)     // Catch:{ all -> 0x076f }
                        java.lang.String r37 = " of "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ all -> 0x076f }
                        r0 = r36
                        java.lang.StringBuilder r36 = r0.append(r13)     // Catch:{ all -> 0x076f }
                        java.lang.String r37 = " bytes."
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ all -> 0x076f }
                        java.lang.String r36 = r36.toString()     // Catch:{ all -> 0x076f }
                        android.util.Log.e(r35, r36)     // Catch:{ all -> 0x076f }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x076f }
                        r35 = r0
                        org.apache.cordova.api.PluginResult r36 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x076f }
                        org.apache.cordova.api.PluginResult$Status r37 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x076f }
                        r0 = r36
                        r1 = r37
                        r0.<init>(r1, r12)     // Catch:{ all -> 0x076f }
                        r35.sendPluginResult(r36)     // Catch:{ all -> 0x076f }
                        java.util.HashMap r36 = org.apache.cordova.FileTransfer.activeRequests
                        monitor-enter(r36)
                        java.util.HashMap r35 = org.apache.cordova.FileTransfer.activeRequests     // Catch:{ all -> 0x06f2 }
                        r0 = r39
                        java.lang.String r0 = r18     // Catch:{ all -> 0x06f2 }
                        r37 = r0
                        r0 = r35
                        r1 = r37
                        r0.remove(r1)     // Catch:{ all -> 0x06f2 }
                        monitor-exit(r36)     // Catch:{ all -> 0x06f2 }
                        if (r9 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r7
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r6
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r14 = r9
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14
                        r0 = r18
                        r14.setHostnameVerifier(r0)
                        r0 = r19
                        r14.setSSLSocketFactory(r0)
                        goto L_0x000e
                    L_0x04c1:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x04c1 }
                        throw r35     // Catch:{ all -> 0x0419 }
                    L_0x04c4:
                        r0 = r28
                        r1 = r32
                        r0.write(r1)     // Catch:{ all -> 0x0419 }
                        r0 = r32
                        int r0 = r0.length     // Catch:{ all -> 0x0419 }
                        r35 = r0
                        int r33 = r33 + r35
                        r28.flush()     // Catch:{ all -> 0x0419 }
                        org.apache.cordova.FileTransfer.safeClose(r29)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.apache.cordova.FileTransfer.safeClose(r28)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        r36 = 0
                        r0 = r36
                        r1 = r35
                        r1.currentOutputStream = r0     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "FileTransfer"
                        java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36.<init>()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r37 = "Sent "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r36
                        r1 = r33
                        java.lang.StringBuilder r36 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r37 = " of "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r36
                        java.lang.StringBuilder r36 = r0.append(r13)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = r36.toString()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        android.util.Log.d(r35, r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        int r25 = r9.getResponseCode()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "FileTransfer"
                        java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36.<init>()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r37 = "response code: "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r36
                        r1 = r25
                        java.lang.StringBuilder r36 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = r36.toString()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        android.util.Log.d(r35, r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "FileTransfer"
                        java.lang.StringBuilder r36 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36.<init>()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r37 = "response headers: "
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.util.Map r37 = r9.getHeaderFields()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.StringBuilder r36 = r36.append(r37)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r36 = r36.toString()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        android.util.Log.d(r35, r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r15 = 0
                        org.apache.cordova.FileTransfer$TrackingInputStream r15 = org.apache.cordova.FileTransfer.getInputStream(r9)     // Catch:{ all -> 0x05e9 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x05e9 }
                        r36 = r0
                        monitor-enter(r36)     // Catch:{ all -> 0x05e9 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x0652 }
                        r35 = r0
                        r0 = r35
                        boolean r0 = r0.aborted     // Catch:{ all -> 0x0652 }
                        r35 = r0
                        if (r35 == 0) goto L_0x05b4
                        monitor-exit(r36)     // Catch:{ all -> 0x0652 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        r36 = 0
                        r0 = r36
                        r1 = r35
                        r1.currentInputStream = r0     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.apache.cordova.FileTransfer.safeClose(r15)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.util.HashMap r36 = org.apache.cordova.FileTransfer.activeRequests
                        monitor-enter(r36)
                        java.util.HashMap r35 = org.apache.cordova.FileTransfer.activeRequests     // Catch:{ all -> 0x05b1 }
                        r0 = r39
                        java.lang.String r0 = r18     // Catch:{ all -> 0x05b1 }
                        r37 = r0
                        r0 = r35
                        r1 = r37
                        r0.remove(r1)     // Catch:{ all -> 0x05b1 }
                        monitor-exit(r36)     // Catch:{ all -> 0x05b1 }
                        if (r9 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r7
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r6
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r14 = r9
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14
                        r0 = r18
                        r14.setHostnameVerifier(r0)
                        r0 = r19
                        r14.setSSLSocketFactory(r0)
                        goto L_0x000e
                    L_0x05b1:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x05b1 }
                        throw r35
                    L_0x05b4:
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x0652 }
                        r35 = r0
                        r0 = r35
                        r0.currentInputStream = r15     // Catch:{ all -> 0x0652 }
                        monitor-exit(r36)     // Catch:{ all -> 0x0652 }
                        java.io.ByteArrayOutputStream r20 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x05e9 }
                        r35 = 1024(0x400, float:1.435E-42)
                        int r36 = r9.getContentLength()     // Catch:{ all -> 0x05e9 }
                        int r35 = java.lang.Math.max(r35, r36)     // Catch:{ all -> 0x05e9 }
                        r0 = r20
                        r1 = r35
                        r0.<init>(r1)     // Catch:{ all -> 0x05e9 }
                        r35 = 1024(0x400, float:1.435E-42)
                        r0 = r35
                        byte[] r5 = new byte[r0]     // Catch:{ all -> 0x05e9 }
                        r8 = 0
                    L_0x05d9:
                        int r8 = r15.read(r5)     // Catch:{ all -> 0x05e9 }
                        if (r8 <= 0) goto L_0x0655
                        r35 = 0
                        r0 = r20
                        r1 = r35
                        r0.write(r5, r1, r8)     // Catch:{ all -> 0x05e9 }
                        goto L_0x05d9
                    L_0x05e9:
                        r35 = move-exception
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36 = r0
                        r37 = 0
                        r0 = r37
                        r1 = r36
                        r1.currentInputStream = r0     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.apache.cordova.FileTransfer.safeClose(r15)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        throw r35     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                    L_0x05fc:
                        r11 = move-exception
                        java.lang.String r35 = "FileTransfer"
                        java.lang.String r36 = r11.getMessage()     // Catch:{ all -> 0x076f }
                        r0 = r35
                        r1 = r36
                        android.util.Log.e(r0, r1, r11)     // Catch:{ all -> 0x076f }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x076f }
                        r35 = r0
                        org.apache.cordova.api.PluginResult r36 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x076f }
                        org.apache.cordova.api.PluginResult$Status r37 = org.apache.cordova.api.PluginResult.Status.JSON_EXCEPTION     // Catch:{ all -> 0x076f }
                        r36.<init>(r37)     // Catch:{ all -> 0x076f }
                        r35.sendPluginResult(r36)     // Catch:{ all -> 0x076f }
                        java.util.HashMap r36 = org.apache.cordova.FileTransfer.activeRequests
                        monitor-enter(r36)
                        java.util.HashMap r35 = org.apache.cordova.FileTransfer.activeRequests     // Catch:{ all -> 0x06f5 }
                        r0 = r39
                        java.lang.String r0 = r18     // Catch:{ all -> 0x06f5 }
                        r37 = r0
                        r0 = r35
                        r1 = r37
                        r0.remove(r1)     // Catch:{ all -> 0x06f5 }
                        monitor-exit(r36)     // Catch:{ all -> 0x06f5 }
                        if (r9 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r7
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r6
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r14 = r9
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14
                        r0 = r18
                        r14.setHostnameVerifier(r0)
                        r0 = r19
                        r14.setSSLSocketFactory(r0)
                        goto L_0x000e
                    L_0x0652:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x0652 }
                        throw r35     // Catch:{ all -> 0x05e9 }
                    L_0x0655:
                        java.lang.String r35 = "UTF-8"
                        r0 = r20
                        r1 = r35
                        java.lang.String r26 = r0.toString(r1)     // Catch:{ all -> 0x05e9 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        r36 = 0
                        r0 = r36
                        r1 = r35
                        r1.currentInputStream = r0     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.apache.cordova.FileTransfer.safeClose(r15)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "FileTransfer"
                        java.lang.String r36 = "got response from server"
                        android.util.Log.d(r35, r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.lang.String r35 = "FileTransfer"
                        r36 = 0
                        r37 = 256(0x100, float:3.59E-43)
                        int r38 = r26.length()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        int r37 = java.lang.Math.min(r37, r38)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r26
                        r1 = r36
                        r2 = r37
                        java.lang.String r36 = r0.substring(r1, r2)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        android.util.Log.d(r35, r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r27
                        r1 = r25
                        r0.setResponseCode(r1)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r27
                        r1 = r26
                        r0.setResponse(r1)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35 = r0
                        org.apache.cordova.api.PluginResult r36 = new org.apache.cordova.api.PluginResult     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.apache.cordova.api.PluginResult$Status r37 = org.apache.cordova.api.PluginResult.Status.OK     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        org.json.JSONObject r38 = r27.toJSONObject()     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r36.<init>(r37, r38)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        r35.sendPluginResult(r36)     // Catch:{ FileNotFoundException -> 0x02ce, IOException -> 0x0421, JSONException -> 0x05fc, Throwable -> 0x06f8 }
                        java.util.HashMap r36 = org.apache.cordova.FileTransfer.activeRequests
                        monitor-enter(r36)
                        java.util.HashMap r35 = org.apache.cordova.FileTransfer.activeRequests     // Catch:{ all -> 0x06ec }
                        r0 = r39
                        java.lang.String r0 = r18     // Catch:{ all -> 0x06ec }
                        r37 = r0
                        r0 = r35
                        r1 = r37
                        r0.remove(r1)     // Catch:{ all -> 0x06ec }
                        monitor-exit(r36)     // Catch:{ all -> 0x06ec }
                        if (r9 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r7
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r6
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r14 = r9
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14
                        r0 = r18
                        r14.setHostnameVerifier(r0)
                        r0 = r19
                        r14.setSSLSocketFactory(r0)
                        goto L_0x000e
                    L_0x06ec:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x06ec }
                        throw r35
                    L_0x06ef:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x06ef }
                        throw r35
                    L_0x06f2:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x06f2 }
                        throw r35
                    L_0x06f5:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x06f5 }
                        throw r35
                    L_0x06f8:
                        r31 = move-exception
                        int r35 = org.apache.cordova.FileTransfer.CONNECTION_ERR     // Catch:{ all -> 0x076f }
                        r0 = r39
                        java.lang.String r0 = r16     // Catch:{ all -> 0x076f }
                        r36 = r0
                        r0 = r39
                        java.lang.String r0 = r10     // Catch:{ all -> 0x076f }
                        r37 = r0
                        r0 = r35
                        r1 = r36
                        r2 = r37
                        org.json.JSONObject r12 = org.apache.cordova.FileTransfer.createFileTransferError(r0, r1, r2, r9)     // Catch:{ all -> 0x076f }
                        java.lang.String r35 = "FileTransfer"
                        java.lang.String r36 = r12.toString()     // Catch:{ all -> 0x076f }
                        r0 = r35
                        r1 = r36
                        r2 = r31
                        android.util.Log.e(r0, r1, r2)     // Catch:{ all -> 0x076f }
                        r0 = r39
                        org.apache.cordova.FileTransfer$RequestContext r0 = r5     // Catch:{ all -> 0x076f }
                        r35 = r0
                        org.apache.cordova.api.PluginResult r36 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x076f }
                        org.apache.cordova.api.PluginResult$Status r37 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x076f }
                        r0 = r36
                        r1 = r37
                        r0.<init>(r1, r12)     // Catch:{ all -> 0x076f }
                        r35.sendPluginResult(r36)     // Catch:{ all -> 0x076f }
                        java.util.HashMap r36 = org.apache.cordova.FileTransfer.activeRequests
                        monitor-enter(r36)
                        java.util.HashMap r35 = org.apache.cordova.FileTransfer.activeRequests     // Catch:{ all -> 0x076c }
                        r0 = r39
                        java.lang.String r0 = r18     // Catch:{ all -> 0x076c }
                        r37 = r0
                        r0 = r35
                        r1 = r37
                        r0.remove(r1)     // Catch:{ all -> 0x076c }
                        monitor-exit(r36)     // Catch:{ all -> 0x076c }
                        if (r9 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r7
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r0 = r39
                        boolean r0 = r6
                        r35 = r0
                        if (r35 == 0) goto L_0x000e
                        r14 = r9
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14
                        r0 = r18
                        r14.setHostnameVerifier(r0)
                        r0 = r19
                        r14.setSSLSocketFactory(r0)
                        goto L_0x000e
                    L_0x076c:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x076c }
                        throw r35
                    L_0x076f:
                        r35 = move-exception
                        java.util.HashMap r36 = org.apache.cordova.FileTransfer.activeRequests
                        monitor-enter(r36)
                        java.util.HashMap r37 = org.apache.cordova.FileTransfer.activeRequests     // Catch:{ all -> 0x07a3 }
                        r0 = r39
                        java.lang.String r0 = r18     // Catch:{ all -> 0x07a3 }
                        r38 = r0
                        r37.remove(r38)     // Catch:{ all -> 0x07a3 }
                        monitor-exit(r36)     // Catch:{ all -> 0x07a3 }
                        if (r9 == 0) goto L_0x07a2
                        r0 = r39
                        boolean r0 = r7
                        r36 = r0
                        if (r36 == 0) goto L_0x07a2
                        r0 = r39
                        boolean r0 = r6
                        r36 = r0
                        if (r36 == 0) goto L_0x07a2
                        r14 = r9
                        javax.net.ssl.HttpsURLConnection r14 = (javax.net.ssl.HttpsURLConnection) r14
                        r0 = r18
                        r14.setHostnameVerifier(r0)
                        r0 = r19
                        r14.setSSLSocketFactory(r0)
                    L_0x07a2:
                        throw r35
                    L_0x07a3:
                        r35 = move-exception
                        monitor-exit(r36)     // Catch:{ all -> 0x07a3 }
                        throw r35
                    */
                    throw new UnsupportedOperationException("Method not decompiled: org.apache.cordova.FileTransfer.AnonymousClass1.run():void");
                }
            });
        } catch (MalformedURLException e) {
            JSONObject error = createFileTransferError(INVALID_URL_ERR, source, target, null, 0);
            Log.e(LOG_TAG, error.toString(), e);
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, error));
        }
    }

    /* access modifiers changed from: private */
    public static void safeClose(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public static TrackingInputStream getInputStream(URLConnection conn) throws IOException {
        String encoding = conn.getContentEncoding();
        if (encoding == null || !encoding.equalsIgnoreCase("gzip")) {
            return new TrackingHTTPInputStream(conn.getInputStream());
        }
        return new TrackingGZIPInputStream(new ExposedGZIPInputStream(conn.getInputStream()));
    }

    /* access modifiers changed from: private */
    public static SSLSocketFactory trustAllHosts(HttpsURLConnection connection) {
        SSLSocketFactory oldFactory = connection.getSSLSocketFactory();
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            connection.setSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }
        return oldFactory;
    }

    /* access modifiers changed from: private */
    public static JSONObject createFileTransferError(int errorCode, String source, String target, URLConnection connection) {
        int httpStatus = 0;
        StringBuilder bodyBuilder = new StringBuilder();
        String body = null;
        if (connection != null) {
            try {
                if (connection instanceof HttpURLConnection) {
                    httpStatus = ((HttpURLConnection) connection).getResponseCode();
                    InputStream err = ((HttpURLConnection) connection).getErrorStream();
                    if (err != null) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(err, "UTF-8"));
                        String line = reader.readLine();
                        while (line != null) {
                            bodyBuilder.append(line);
                            line = reader.readLine();
                            if (line != null) {
                                bodyBuilder.append(10);
                            }
                        }
                        body = bodyBuilder.toString();
                    }
                }
            } catch (IOException e) {
                Log.w(LOG_TAG, "Error getting HTTP status code from connection.", e);
            }
        }
        return createFileTransferError(errorCode, source, target, body, Integer.valueOf(httpStatus));
    }

    private static JSONObject createFileTransferError(int errorCode, String source, String target, String body, Integer httpStatus) {
        JSONObject error = null;
        try {
            JSONObject error2 = new JSONObject();
            try {
                error2.put("code", errorCode);
                error2.put("source", source);
                error2.put("target", target);
                if (body != null) {
                    error2.put("body", body);
                }
                if (httpStatus != null) {
                    error2.put("http_status", httpStatus);
                }
                return error2;
            } catch (JSONException e) {
                e = e;
                error = error2;
                Log.e(LOG_TAG, e.getMessage(), e);
                return error;
            }
        } catch (JSONException e2) {
            e = e2;
            Log.e(LOG_TAG, e.getMessage(), e);
            return error;
        }
    }

    private static String getArgument(JSONArray args, int position, String defaultString) {
        String arg = defaultString;
        if (args.length() <= position) {
            return arg;
        }
        String arg2 = args.optString(position);
        if (arg2 == null || "null".equals(arg2)) {
            return defaultString;
        }
        return arg2;
    }

    private void download(String source, String target, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Log.d(LOG_TAG, "download " + source + " to " + target);
        final boolean trustEveryone = args.optBoolean(2);
        final String objectId = args.getString(3);
        final JSONObject headers = args.optJSONObject(4);
        try {
            final URL url = new URL(source);
            final boolean useHttps = url.getProtocol().equals("https");
            if (!Config.isUrlWhiteListed(source)) {
                Log.w(LOG_TAG, "Source URL is not in white list: '" + source + "'");
                CallbackContext callbackContext2 = callbackContext;
                callbackContext2.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, createFileTransferError(CONNECTION_ERR, source, target, null, 401)));
                return;
            }
            final RequestContext context = new RequestContext(source, target, callbackContext);
            synchronized (activeRequests) {
                activeRequests.put(objectId, context);
            }
            final String str = target;
            final String str2 = source;
            this.cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    PluginResult result;
                    PluginResult result2;
                    RequestContext requestContext;
                    PluginResult result3;
                    PluginResult result4;
                    PluginResult result5;
                    if (!context.aborted) {
                        URLConnection connection = null;
                        HostnameVerifier oldHostnameVerifier = null;
                        SSLSocketFactory oldSocketFactory = null;
                        File file = null;
                        PluginResult result6 = null;
                        try {
                            file = FileTransfer.this.getFileFromPath(str);
                            context.targetFile = file;
                            file.getParentFile().mkdirs();
                            if (!useHttps) {
                                connection = FileTransfer.httpClient.open(url);
                            } else if (!trustEveryone) {
                                connection = (HttpsURLConnection) FileTransfer.httpClient.open(url);
                            } else {
                                HttpsURLConnection https = (HttpsURLConnection) FileTransfer.httpClient.open(url);
                                oldSocketFactory = FileTransfer.trustAllHosts(https);
                                oldHostnameVerifier = https.getHostnameVerifier();
                                https.setHostnameVerifier(FileTransfer.DO_NOT_VERIFY);
                                connection = https;
                            }
                            if (connection instanceof HttpURLConnection) {
                                ((HttpURLConnection) connection).setRequestMethod("GET");
                            }
                            String cookie = CookieManager.getInstance().getCookie(str2);
                            if (cookie != null) {
                                connection.setRequestProperty("cookie", cookie);
                            }
                            connection.setRequestProperty("Accept-Encoding", "gzip");
                            if (headers != null) {
                                FileTransfer.addHeadersToRequest(connection, headers);
                            }
                            connection.connect();
                            Log.d(FileTransfer.LOG_TAG, "Download file:" + url);
                            FileProgressResult progress = new FileProgressResult();
                            if (connection.getContentEncoding() == null || connection.getContentEncoding().equalsIgnoreCase("gzip")) {
                                progress.setLengthComputable(true);
                                progress.setTotal((long) connection.getContentLength());
                            }
                            FileOutputStream outputStream = null;
                            TrackingInputStream inputStream = null;
                            try {
                                inputStream = FileTransfer.getInputStream(connection);
                                FileOutputStream fileOutputStream = new FileOutputStream(file);
                                try {
                                    synchronized (context) {
                                        if (context.aborted) {
                                            context.currentInputStream = null;
                                            FileTransfer.safeClose(inputStream);
                                            FileTransfer.safeClose(fileOutputStream);
                                            synchronized (FileTransfer.activeRequests) {
                                                FileTransfer.activeRequests.remove(objectId);
                                            }
                                            if (connection != null && trustEveryone && useHttps) {
                                                HttpsURLConnection https2 = (HttpsURLConnection) connection;
                                                https2.setHostnameVerifier(oldHostnameVerifier);
                                                https2.setSSLSocketFactory(oldSocketFactory);
                                            }
                                            if (0 == 0) {
                                                result6 = new PluginResult(PluginResult.Status.ERROR, FileTransfer.createFileTransferError(FileTransfer.CONNECTION_ERR, str2, str, connection));
                                            }
                                            if (!(result2.getStatus() == PluginResult.Status.OK.ordinal() || file == null)) {
                                                file.delete();
                                            }
                                            requestContext = context;
                                        } else {
                                            context.currentInputStream = inputStream;
                                            byte[] buffer = new byte[16384];
                                            while (true) {
                                                int bytesRead = inputStream.read(buffer);
                                                if (bytesRead <= 0) {
                                                    break;
                                                }
                                                fileOutputStream.write(buffer, 0, bytesRead);
                                                progress.setLoaded(inputStream.getTotalRawBytesRead());
                                                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, progress.toJSONObject());
                                                pluginResult.setKeepCallback(true);
                                                context.sendPluginResult(pluginResult);
                                            }
                                            context.currentInputStream = null;
                                            FileTransfer.safeClose(inputStream);
                                            FileTransfer.safeClose(fileOutputStream);
                                            Log.d(FileTransfer.LOG_TAG, "Saved file: " + str);
                                            PluginResult pluginResult2 = new PluginResult(PluginResult.Status.OK, FileUtils.getEntry(file));
                                            synchronized (FileTransfer.activeRequests) {
                                                FileTransfer.activeRequests.remove(objectId);
                                            }
                                            if (connection != null && trustEveryone && useHttps) {
                                                HttpsURLConnection https3 = (HttpsURLConnection) connection;
                                                https3.setHostnameVerifier(oldHostnameVerifier);
                                                https3.setSSLSocketFactory(oldSocketFactory);
                                            }
                                            if (pluginResult2 == null) {
                                                result2 = new PluginResult(PluginResult.Status.ERROR, FileTransfer.createFileTransferError(FileTransfer.CONNECTION_ERR, str2, str, connection));
                                            } else {
                                                result2 = pluginResult2;
                                            }
                                            if (!(result2.getStatus() == PluginResult.Status.OK.ordinal() || file == null)) {
                                                file.delete();
                                            }
                                            requestContext = context;
                                        }
                                    }
                                    requestContext.sendPluginResult(result2);
                                } catch (Throwable th) {
                                    th = th;
                                    outputStream = fileOutputStream;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                context.currentInputStream = null;
                                FileTransfer.safeClose(inputStream);
                                FileTransfer.safeClose(outputStream);
                                throw th;
                            }
                        } catch (FileNotFoundException e) {
                            JSONObject error = FileTransfer.createFileTransferError(FileTransfer.FILE_NOT_FOUND_ERR, str2, str, connection);
                            Log.e(FileTransfer.LOG_TAG, error.toString(), e);
                            PluginResult pluginResult3 = new PluginResult(PluginResult.Status.IO_EXCEPTION, error);
                            synchronized (FileTransfer.activeRequests) {
                                FileTransfer.activeRequests.remove(objectId);
                                if (connection != null && trustEveryone && useHttps) {
                                    HttpsURLConnection https4 = (HttpsURLConnection) connection;
                                    https4.setHostnameVerifier(oldHostnameVerifier);
                                    https4.setSSLSocketFactory(oldSocketFactory);
                                }
                                if (pluginResult3 == null) {
                                    result5 = new PluginResult(PluginResult.Status.ERROR, FileTransfer.createFileTransferError(FileTransfer.CONNECTION_ERR, str2, str, connection));
                                } else {
                                    result5 = pluginResult3;
                                }
                                if (!(result2.getStatus() == PluginResult.Status.OK.ordinal() || file == null)) {
                                    file.delete();
                                }
                                requestContext = context;
                            }
                        } catch (IOException e2) {
                            JSONObject error2 = FileTransfer.createFileTransferError(FileTransfer.CONNECTION_ERR, str2, str, connection);
                            Log.e(FileTransfer.LOG_TAG, error2.toString(), e2);
                            PluginResult pluginResult4 = new PluginResult(PluginResult.Status.IO_EXCEPTION, error2);
                            synchronized (FileTransfer.activeRequests) {
                                FileTransfer.activeRequests.remove(objectId);
                                if (connection != null && trustEveryone && useHttps) {
                                    HttpsURLConnection https5 = (HttpsURLConnection) connection;
                                    https5.setHostnameVerifier(oldHostnameVerifier);
                                    https5.setSSLSocketFactory(oldSocketFactory);
                                }
                                if (pluginResult4 == null) {
                                    result4 = new PluginResult(PluginResult.Status.ERROR, FileTransfer.createFileTransferError(FileTransfer.CONNECTION_ERR, str2, str, connection));
                                } else {
                                    result4 = pluginResult4;
                                }
                                if (!(result2.getStatus() == PluginResult.Status.OK.ordinal() || file == null)) {
                                    file.delete();
                                }
                                requestContext = context;
                            }
                        } catch (JSONException e3) {
                            Log.e(FileTransfer.LOG_TAG, e3.getMessage(), e3);
                            PluginResult result7 = new PluginResult(PluginResult.Status.JSON_EXCEPTION);
                            synchronized (FileTransfer.activeRequests) {
                                FileTransfer.activeRequests.remove(objectId);
                                if (connection != null && trustEveryone && useHttps) {
                                    HttpsURLConnection https6 = (HttpsURLConnection) connection;
                                    https6.setHostnameVerifier(oldHostnameVerifier);
                                    https6.setSSLSocketFactory(oldSocketFactory);
                                }
                                if (result7 == null) {
                                    result3 = new PluginResult(PluginResult.Status.ERROR, FileTransfer.createFileTransferError(FileTransfer.CONNECTION_ERR, str2, str, connection));
                                } else {
                                    result3 = result7;
                                }
                                if (!(result2.getStatus() == PluginResult.Status.OK.ordinal() || file == null)) {
                                    file.delete();
                                }
                                requestContext = context;
                            }
                        } catch (Throwable th3) {
                            synchronized (FileTransfer.activeRequests) {
                                FileTransfer.activeRequests.remove(objectId);
                                if (connection != null && trustEveryone && useHttps) {
                                    HttpsURLConnection https7 = (HttpsURLConnection) connection;
                                    https7.setHostnameVerifier(oldHostnameVerifier);
                                    https7.setSSLSocketFactory(oldSocketFactory);
                                }
                                if (0 == 0) {
                                    result6 = new PluginResult(PluginResult.Status.ERROR, FileTransfer.createFileTransferError(FileTransfer.CONNECTION_ERR, str2, str, connection));
                                }
                                if (!(result6.getStatus() == PluginResult.Status.OK.ordinal() || file == null)) {
                                    file.delete();
                                }
                                context.sendPluginResult(result6);
                                throw th3;
                            }
                        }
                    }
                }
            });
        } catch (MalformedURLException e) {
            JSONObject error = createFileTransferError(INVALID_URL_ERR, source, target, null, 0);
            Log.e(LOG_TAG, error.toString(), e);
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, error));
        }
    }

    /* access modifiers changed from: private */
    public InputStream getPathFromUri(String path) throws FileNotFoundException {
        try {
            InputStream stream = FileHelper.getInputStreamFromUriString(path, this.cordova);
            if (stream == null) {
                return new FileInputStream(path);
            }
            return stream;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    /* access modifiers changed from: private */
    public File getFileFromPath(String path) throws FileNotFoundException {
        File file;
        if (path.startsWith("file://")) {
            file = new File(path.substring("file://".length()));
        } else {
            file = new File(path);
        }
        if (file.getParent() != null) {
            return file;
        }
        throw new FileNotFoundException();
    }

    private void abort(String objectId) {
        final RequestContext context;
        synchronized (activeRequests) {
            context = activeRequests.remove(objectId);
        }
        if (context != null) {
            File file = context.targetFile;
            if (file != null) {
                file.delete();
            }
            JSONObject error = createFileTransferError(ABORTED_ERR, context.source, context.target, null, -1);
            synchronized (context) {
                context.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, error));
                context.aborted = true;
            }
            this.cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    synchronized (context) {
                        FileTransfer.safeClose(context.currentInputStream);
                        FileTransfer.safeClose(context.currentOutputStream);
                    }
                }
            });
        }
    }
}
