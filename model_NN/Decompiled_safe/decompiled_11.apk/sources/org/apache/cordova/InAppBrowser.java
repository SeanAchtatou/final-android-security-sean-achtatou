package org.apache.cordova;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.HashMap;
import java.util.StringTokenizer;
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.LOG;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"SetJavaScriptEnabled"})
public class InAppBrowser extends CordovaPlugin {
    private static final String CLOSE_BUTTON_CAPTION = "closebuttoncaption";
    private static final String EXIT_EVENT = "exit";
    private static final String HIDDEN = "hidden";
    private static final String LOAD_ERROR_EVENT = "loaderror";
    private static final String LOAD_START_EVENT = "loadstart";
    private static final String LOAD_STOP_EVENT = "loadstop";
    private static final String LOCATION = "location";
    protected static final String LOG_TAG = "InAppBrowser";
    private static final String NULL = "null";
    private static final String SELF = "_self";
    private static final String SYSTEM = "_system";
    /* access modifiers changed from: private */
    public long MAX_QUOTA = 104857600;
    /* access modifiers changed from: private */
    public String buttonLabel = "Done";
    private CallbackContext callbackContext;
    /* access modifiers changed from: private */
    public Dialog dialog;
    /* access modifiers changed from: private */
    public EditText edittext;
    /* access modifiers changed from: private */
    public WebView inAppWebView;
    /* access modifiers changed from: private */
    public boolean openWindowHidden = false;
    private boolean showLocationBar = true;

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext2) throws JSONException {
        String jsWrapper;
        String jsWrapper2;
        String jsWrapper3;
        try {
            if (action.equals("open")) {
                this.callbackContext = callbackContext2;
                String url = args.getString(0);
                String target = args.optString(1);
                if (target == null || target.equals("") || target.equals(NULL)) {
                    target = SELF;
                }
                HashMap<String, Boolean> features = parseFeature(args.optString(2));
                Log.d(LOG_TAG, "target = " + target);
                String url2 = updateUrl(url);
                String result = "";
                if (SELF.equals(target)) {
                    Log.d(LOG_TAG, "in self");
                    if (url2.startsWith("file://") || url2.startsWith("javascript:") || Config.isUrlWhiteListed(url2)) {
                        this.webView.loadUrl(url2);
                    } else if (url2.startsWith("tel:")) {
                        try {
                            Intent intent = new Intent("android.intent.action.DIAL");
                            intent.setData(Uri.parse(url2));
                            this.cordova.getActivity().startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            LOG.e(LOG_TAG, "Error dialing " + url2 + ": " + e.toString());
                        }
                    } else {
                        result = showWebPage(url2, features);
                    }
                } else if (SYSTEM.equals(target)) {
                    Log.d(LOG_TAG, "in system");
                    result = openExternal(url2);
                } else {
                    Log.d(LOG_TAG, "in blank");
                    result = showWebPage(url2, features);
                }
                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, result);
                pluginResult.setKeepCallback(true);
                this.callbackContext.sendPluginResult(pluginResult);
            } else if (action.equals("close")) {
                closeDialog();
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
            } else if (action.equals("injectScriptCode")) {
                String jsWrapper4 = null;
                if (args.getBoolean(1)) {
                    jsWrapper4 = String.format("prompt(JSON.stringify([eval(%%s)]), 'gap-iab://%s')", callbackContext2.getCallbackId());
                }
                injectDeferredObject(args.getString(0), jsWrapper4);
            } else if (action.equals("injectScriptFile")) {
                if (args.getBoolean(1)) {
                    jsWrapper3 = String.format("(function(d) { var c = d.createElement('script'); c.src = %%s; c.onload = function() { prompt('', 'gap-iab://%s'); }; d.body.appendChild(c); })(document)", callbackContext2.getCallbackId());
                } else {
                    jsWrapper3 = "(function(d) { var c = d.createElement('script'); c.src = %s; d.body.appendChild(c); })(document)";
                }
                injectDeferredObject(args.getString(0), jsWrapper3);
            } else if (action.equals("injectStyleCode")) {
                if (args.getBoolean(1)) {
                    jsWrapper2 = String.format("(function(d) { var c = d.createElement('style'); c.innerHTML = %%s; d.body.appendChild(c); prompt('', 'gap-iab://%s');})(document)", callbackContext2.getCallbackId());
                } else {
                    jsWrapper2 = "(function(d) { var c = d.createElement('style'); c.innerHTML = %s; d.body.appendChild(c); })(document)";
                }
                injectDeferredObject(args.getString(0), jsWrapper2);
            } else if (action.equals("injectStyleFile")) {
                if (args.getBoolean(1)) {
                    jsWrapper = String.format("(function(d) { var c = d.createElement('link'); c.rel='stylesheet'; c.type='text/css'; c.href = %%s; d.head.appendChild(c); prompt('', 'gap-iab://%s');})(document)", callbackContext2.getCallbackId());
                } else {
                    jsWrapper = "(function(d) { var c = d.createElement('link'); c.rel='stylesheet'; c.type='text/css'; c.href = %s; d.head.appendChild(c); })(document)";
                }
                injectDeferredObject(args.getString(0), jsWrapper);
            } else if (!action.equals("show")) {
                return false;
            } else {
                this.cordova.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        InAppBrowser.this.dialog.show();
                    }
                });
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
            }
        } catch (JSONException e2) {
            this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION));
        }
        return true;
    }

    private void injectDeferredObject(String source, String jsWrapper) {
        String scriptToInject;
        if (jsWrapper != null) {
            JSONArray jsonEsc = new JSONArray();
            jsonEsc.put(source);
            String jsonRepr = jsonEsc.toString();
            scriptToInject = String.format(jsWrapper, jsonRepr.substring(1, jsonRepr.length() - 1));
        } else {
            scriptToInject = source;
        }
        this.inAppWebView.loadUrl("javascript:" + scriptToInject);
    }

    private HashMap<String, Boolean> parseFeature(String optString) {
        if (optString.equals(NULL)) {
            return null;
        }
        HashMap<String, Boolean> map = new HashMap<>();
        StringTokenizer features = new StringTokenizer(optString, ",");
        while (features.hasMoreElements()) {
            StringTokenizer option = new StringTokenizer(features.nextToken(), "=");
            if (option.hasMoreElements()) {
                String key = option.nextToken();
                if (key.equalsIgnoreCase(CLOSE_BUTTON_CAPTION)) {
                    this.buttonLabel = option.nextToken();
                } else {
                    map.put(key, option.nextToken().equals("no") ? Boolean.FALSE : Boolean.TRUE);
                }
            }
        }
        return map;
    }

    private String updateUrl(String url) {
        if (Uri.parse(url).isRelative()) {
            return this.webView.getUrl().substring(0, this.webView.getUrl().lastIndexOf("/") + 1) + url;
        }
        return url;
    }

    public String openExternal(String url) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            try {
                intent.setData(Uri.parse(url));
                this.cordova.getActivity().startActivity(intent);
                return "";
            } catch (ActivityNotFoundException e) {
                e = e;
            }
        } catch (ActivityNotFoundException e2) {
            e = e2;
            Log.d(LOG_TAG, "InAppBrowser: Error loading url " + url + ":" + e.toString());
            return e.toString();
        }
    }

    /* access modifiers changed from: private */
    public void closeDialog() {
        try {
            this.inAppWebView.loadUrl("about:blank");
            JSONObject obj = new JSONObject();
            obj.put(Globalization.TYPE, EXIT_EVENT);
            sendUpdate(obj, false);
        } catch (JSONException e) {
            Log.d(LOG_TAG, "Should never happen");
        }
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void goBack() {
        if (this.inAppWebView.canGoBack()) {
            this.inAppWebView.goBack();
        }
    }

    /* access modifiers changed from: private */
    public void goForward() {
        if (this.inAppWebView.canGoForward()) {
            this.inAppWebView.goForward();
        }
    }

    /* access modifiers changed from: private */
    public void navigate(String url) {
        ((InputMethodManager) this.cordova.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.edittext.getWindowToken(), 0);
        if (url.startsWith("http") || url.startsWith("file:")) {
            this.inAppWebView.loadUrl(url);
        } else {
            this.inAppWebView.loadUrl("http://" + url);
        }
        this.inAppWebView.requestFocus();
    }

    /* access modifiers changed from: private */
    public boolean getShowLocationBar() {
        return this.showLocationBar;
    }

    public String showWebPage(final String url, HashMap<String, Boolean> features) {
        this.showLocationBar = true;
        this.openWindowHidden = false;
        if (features != null) {
            Boolean show = features.get(LOCATION);
            if (show != null) {
                this.showLocationBar = show.booleanValue();
            }
            Boolean hidden = features.get(HIDDEN);
            if (hidden != null) {
                this.openWindowHidden = hidden.booleanValue();
            }
        }
        final CordovaWebView thatWebView = this.webView;
        this.cordova.getActivity().runOnUiThread(new Runnable() {
            private int dpToPixels(int dipValue) {
                return (int) TypedValue.applyDimension(1, (float) dipValue, InAppBrowser.this.cordova.getActivity().getResources().getDisplayMetrics());
            }

            public void run() {
                Dialog unused = InAppBrowser.this.dialog = new Dialog(InAppBrowser.this.cordova.getActivity(), 16973830);
                InAppBrowser.this.dialog.getWindow().getAttributes().windowAnimations = 16973826;
                InAppBrowser.this.dialog.requestWindowFeature(1);
                InAppBrowser.this.dialog.setCancelable(true);
                InAppBrowser.this.dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        try {
                            JSONObject obj = new JSONObject();
                            obj.put(Globalization.TYPE, InAppBrowser.EXIT_EVENT);
                            InAppBrowser.this.sendUpdate(obj, false);
                        } catch (JSONException e) {
                            Log.d(InAppBrowser.LOG_TAG, "Should never happen");
                        }
                    }
                });
                LinearLayout main = new LinearLayout(InAppBrowser.this.cordova.getActivity());
                main.setOrientation(1);
                RelativeLayout toolbar = new RelativeLayout(InAppBrowser.this.cordova.getActivity());
                toolbar.setLayoutParams(new RelativeLayout.LayoutParams(-1, dpToPixels(44)));
                toolbar.setPadding(dpToPixels(2), dpToPixels(2), dpToPixels(2), dpToPixels(2));
                toolbar.setHorizontalGravity(3);
                toolbar.setVerticalGravity(48);
                RelativeLayout actionButtonContainer = new RelativeLayout(InAppBrowser.this.cordova.getActivity());
                actionButtonContainer.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                actionButtonContainer.setHorizontalGravity(3);
                actionButtonContainer.setVerticalGravity(16);
                actionButtonContainer.setId(1);
                Button back = new Button(InAppBrowser.this.cordova.getActivity());
                RelativeLayout.LayoutParams backLayoutParams = new RelativeLayout.LayoutParams(-2, -1);
                backLayoutParams.addRule(5);
                back.setLayoutParams(backLayoutParams);
                back.setContentDescription("Back Button");
                back.setId(2);
                back.setText("<");
                back.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        InAppBrowser.this.goBack();
                    }
                });
                Button forward = new Button(InAppBrowser.this.cordova.getActivity());
                RelativeLayout.LayoutParams forwardLayoutParams = new RelativeLayout.LayoutParams(-2, -1);
                forwardLayoutParams.addRule(1, 2);
                forward.setLayoutParams(forwardLayoutParams);
                forward.setContentDescription("Forward Button");
                forward.setId(3);
                forward.setText(">");
                forward.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        InAppBrowser.this.goForward();
                    }
                });
                EditText unused2 = InAppBrowser.this.edittext = new EditText(InAppBrowser.this.cordova.getActivity());
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams.addRule(1, 1);
                layoutParams.addRule(0, 5);
                InAppBrowser.this.edittext.setLayoutParams(layoutParams);
                InAppBrowser.this.edittext.setId(4);
                InAppBrowser.this.edittext.setSingleLine(true);
                InAppBrowser.this.edittext.setText(url);
                InAppBrowser.this.edittext.setInputType(16);
                InAppBrowser.this.edittext.setImeOptions(2);
                InAppBrowser.this.edittext.setInputType(0);
                InAppBrowser.this.edittext.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() != 0 || keyCode != 66) {
                            return false;
                        }
                        InAppBrowser.this.navigate(InAppBrowser.this.edittext.getText().toString());
                        return true;
                    }
                });
                Button close = new Button(InAppBrowser.this.cordova.getActivity());
                RelativeLayout.LayoutParams closeLayoutParams = new RelativeLayout.LayoutParams(-2, -1);
                closeLayoutParams.addRule(11);
                close.setLayoutParams(closeLayoutParams);
                forward.setContentDescription("Close Button");
                close.setId(5);
                close.setText(InAppBrowser.this.buttonLabel);
                close.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        InAppBrowser.this.closeDialog();
                    }
                });
                WebView unused3 = InAppBrowser.this.inAppWebView = new WebView(InAppBrowser.this.cordova.getActivity());
                InAppBrowser.this.inAppWebView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                InAppBrowser.this.inAppWebView.setWebChromeClient(new InAppChromeClient(thatWebView));
                InAppBrowser.this.inAppWebView.setWebViewClient(new InAppBrowserClient(thatWebView, InAppBrowser.this.edittext));
                WebSettings settings = InAppBrowser.this.inAppWebView.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setJavaScriptCanOpenWindowsAutomatically(true);
                settings.setBuiltInZoomControls(true);
                settings.setPluginsEnabled(true);
                Bundle appSettings = InAppBrowser.this.cordova.getActivity().getIntent().getExtras();
                if (appSettings == null ? true : appSettings.getBoolean("InAppBrowserStorageEnabled", true)) {
                    settings.setDatabasePath(InAppBrowser.this.cordova.getActivity().getApplicationContext().getDir("inAppBrowserDB", 0).getPath());
                    settings.setDatabaseEnabled(true);
                }
                settings.setDomStorageEnabled(true);
                InAppBrowser.this.inAppWebView.loadUrl(url);
                InAppBrowser.this.inAppWebView.setId(6);
                InAppBrowser.this.inAppWebView.getSettings().setLoadWithOverviewMode(true);
                InAppBrowser.this.inAppWebView.getSettings().setUseWideViewPort(true);
                InAppBrowser.this.inAppWebView.requestFocus();
                InAppBrowser.this.inAppWebView.requestFocusFromTouch();
                actionButtonContainer.addView(back);
                actionButtonContainer.addView(forward);
                toolbar.addView(actionButtonContainer);
                toolbar.addView(InAppBrowser.this.edittext);
                toolbar.addView(close);
                if (InAppBrowser.this.getShowLocationBar()) {
                    main.addView(toolbar);
                }
                main.addView(InAppBrowser.this.inAppWebView);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(InAppBrowser.this.dialog.getWindow().getAttributes());
                lp.width = -1;
                lp.height = -1;
                InAppBrowser.this.dialog.setContentView(main);
                InAppBrowser.this.dialog.show();
                InAppBrowser.this.dialog.getWindow().setAttributes(lp);
                if (InAppBrowser.this.openWindowHidden) {
                    InAppBrowser.this.dialog.hide();
                }
            }
        });
        return "";
    }

    /* access modifiers changed from: private */
    public void sendUpdate(JSONObject obj, boolean keepCallback) {
        sendUpdate(obj, keepCallback, PluginResult.Status.OK);
    }

    /* access modifiers changed from: private */
    public void sendUpdate(JSONObject obj, boolean keepCallback, PluginResult.Status status) {
        PluginResult result = new PluginResult(status, obj);
        result.setKeepCallback(keepCallback);
        this.callbackContext.sendPluginResult(result);
    }

    public class InAppChromeClient extends WebChromeClient {
        private CordovaWebView webView;

        public InAppChromeClient(CordovaWebView webView2) {
            this.webView = webView2;
        }

        public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
            LOG.d(InAppBrowser.LOG_TAG, "onExceededDatabaseQuota estimatedSize: %d  currentQuota: %d  totalUsedQuota: %d", Long.valueOf(estimatedSize), Long.valueOf(currentQuota), Long.valueOf(totalUsedQuota));
            if (estimatedSize < InAppBrowser.this.MAX_QUOTA) {
                long newQuota = estimatedSize;
                LOG.d(InAppBrowser.LOG_TAG, "calling quotaUpdater.updateQuota newQuota: %d", Long.valueOf(newQuota));
                quotaUpdater.updateQuota(newQuota);
                return;
            }
            quotaUpdater.updateQuota(currentQuota);
        }

        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            super.onGeolocationPermissionsShowPrompt(origin, callback);
            callback.invoke(origin, true, false);
        }

        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
            PluginResult scriptResult;
            if (defaultValue != null && defaultValue.startsWith("gap-iab://")) {
                String scriptCallbackId = defaultValue.substring(10);
                if (scriptCallbackId.startsWith(InAppBrowser.LOG_TAG)) {
                    if (message == null || message.length() == 0) {
                        scriptResult = new PluginResult(PluginResult.Status.OK, new JSONArray());
                    } else {
                        try {
                            scriptResult = new PluginResult(PluginResult.Status.OK, new JSONArray(message));
                        } catch (JSONException e) {
                            scriptResult = new PluginResult(PluginResult.Status.JSON_EXCEPTION, e.getMessage());
                        }
                    }
                    this.webView.sendPluginResult(scriptResult, scriptCallbackId);
                    result.confirm("");
                    return true;
                }
            }
            return false;
        }
    }

    public class InAppBrowserClient extends WebViewClient {
        EditText edittext;
        CordovaWebView webView;

        public InAppBrowserClient(CordovaWebView webView2, EditText mEditText) {
            this.webView = webView2;
            this.edittext = mEditText;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            String address;
            super.onPageStarted(view, url, favicon);
            String newloc = "";
            if (url.startsWith("http:") || url.startsWith("https:") || url.startsWith("file:")) {
                newloc = url;
            } else if (url.startsWith("tel:")) {
                try {
                    Intent intent = new Intent("android.intent.action.DIAL");
                    intent.setData(Uri.parse(url));
                    InAppBrowser.this.cordova.getActivity().startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    LOG.e(InAppBrowser.LOG_TAG, "Error dialing " + url + ": " + e.toString());
                }
            } else if (url.startsWith("geo:") || url.startsWith("mailto:") || url.startsWith("market:")) {
                try {
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    intent2.setData(Uri.parse(url));
                    InAppBrowser.this.cordova.getActivity().startActivity(intent2);
                } catch (ActivityNotFoundException e2) {
                    LOG.e(InAppBrowser.LOG_TAG, "Error with " + url + ": " + e2.toString());
                }
            } else if (url.startsWith("sms:")) {
                try {
                    Intent intent3 = new Intent("android.intent.action.VIEW");
                    int parmIndex = url.indexOf(63);
                    if (parmIndex == -1) {
                        address = url.substring(4);
                    } else {
                        address = url.substring(4, parmIndex);
                        String query = Uri.parse(url).getQuery();
                        if (query != null && query.startsWith("body=")) {
                            intent3.putExtra("sms_body", query.substring(5));
                        }
                    }
                    intent3.setData(Uri.parse("sms:" + address));
                    intent3.putExtra("address", address);
                    intent3.setType("vnd.android-dir/mms-sms");
                    InAppBrowser.this.cordova.getActivity().startActivity(intent3);
                } catch (ActivityNotFoundException e3) {
                    LOG.e(InAppBrowser.LOG_TAG, "Error sending sms " + url + ":" + e3.toString());
                }
            } else {
                newloc = "http://" + url;
            }
            if (!newloc.equals(this.edittext.getText().toString())) {
                this.edittext.setText(newloc);
            }
            try {
                JSONObject obj = new JSONObject();
                obj.put(Globalization.TYPE, InAppBrowser.LOAD_START_EVENT);
                obj.put("url", newloc);
                InAppBrowser.this.sendUpdate(obj, true);
            } catch (JSONException e4) {
                Log.d(InAppBrowser.LOG_TAG, "Should never happen");
            }
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            try {
                JSONObject obj = new JSONObject();
                obj.put(Globalization.TYPE, InAppBrowser.LOAD_STOP_EVENT);
                obj.put("url", url);
                InAppBrowser.this.sendUpdate(obj, true);
            } catch (JSONException e) {
                Log.d(InAppBrowser.LOG_TAG, "Should never happen");
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            try {
                JSONObject obj = new JSONObject();
                obj.put(Globalization.TYPE, InAppBrowser.LOAD_ERROR_EVENT);
                obj.put("url", failingUrl);
                obj.put("code", errorCode);
                obj.put("message", description);
                InAppBrowser.this.sendUpdate(obj, true, PluginResult.Status.ERROR);
            } catch (JSONException e) {
                Log.d(InAppBrowser.LOG_TAG, "Should never happen");
            }
        }
    }
}
