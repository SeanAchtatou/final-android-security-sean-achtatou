package org.apache.cordova;

import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.apache.cordova.file.EncodingException;
import org.apache.cordova.file.FileExistsException;
import org.apache.cordova.file.InvalidModificationException;
import org.apache.cordova.file.NoModificationAllowedException;
import org.apache.cordova.file.TypeMismatchException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FileUtils extends CordovaPlugin {
    public static int ABORT_ERR = 3;
    public static int APPLICATION = 3;
    public static int ENCODING_ERR = 5;
    public static int INVALID_MODIFICATION_ERR = 9;
    public static int INVALID_STATE_ERR = 7;
    private static final String LOG_TAG = "FileUtils";
    public static int NOT_FOUND_ERR = 1;
    public static int NOT_READABLE_ERR = 4;
    public static int NO_MODIFICATION_ALLOWED_ERR = 6;
    public static int PATH_EXISTS_ERR = 12;
    public static int PERSISTENT = 1;
    public static int QUOTA_EXCEEDED_ERR = 10;
    public static int RESOURCE = 2;
    public static int SECURITY_ERR = 2;
    public static int SYNTAX_ERR = 8;
    public static int TEMPORARY = 0;
    public static int TYPE_MISMATCH_ERR = 11;

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        try {
            if (action.equals("testSaveLocationExists")) {
                CallbackContext callbackContext2 = callbackContext;
                callbackContext2.sendPluginResult(new PluginResult(PluginResult.Status.OK, DirectoryManager.testSaveLocationExists()));
            } else if (action.equals("getFreeDiskSpace")) {
                CallbackContext callbackContext3 = callbackContext;
                callbackContext3.sendPluginResult(new PluginResult(PluginResult.Status.OK, (float) DirectoryManager.getFreeDiskSpace(false)));
            } else if (action.equals("testFileExists")) {
                CallbackContext callbackContext4 = callbackContext;
                callbackContext4.sendPluginResult(new PluginResult(PluginResult.Status.OK, DirectoryManager.testFileExists(args.getString(0))));
            } else if (action.equals("testDirectoryExists")) {
                CallbackContext callbackContext5 = callbackContext;
                callbackContext5.sendPluginResult(new PluginResult(PluginResult.Status.OK, DirectoryManager.testFileExists(args.getString(0))));
            } else if (action.equals("readAsText")) {
                String encoding = args.getString(1);
                readFileAs(args.getString(0), args.getInt(2), args.getInt(3), callbackContext, encoding, 1);
            } else if (action.equals("readAsDataURL")) {
                readFileAs(args.getString(0), args.getInt(1), args.getInt(2), callbackContext, null, -1);
            } else if (action.equals("readAsArrayBuffer")) {
                readFileAs(args.getString(0), args.getInt(1), args.getInt(2), callbackContext, null, 6);
            } else if (action.equals("readAsBinaryString")) {
                readFileAs(args.getString(0), args.getInt(1), args.getInt(2), callbackContext, null, 7);
            } else if (action.equals("write")) {
                long fileSize = write(args.getString(0), args.getString(1), args.getInt(2), args.getBoolean(3));
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, (float) fileSize));
            } else if (action.equals("truncate")) {
                long fileSize2 = truncateFile(args.getString(0), args.getLong(1));
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, (float) fileSize2));
            } else if (action.equals("requestFileSystem")) {
                long size = args.optLong(1);
                if (size == 0 || size <= DirectoryManager.getFreeDiskSpace(true) * 1024) {
                    callbackContext.success(requestFileSystem(args.getInt(0)));
                } else {
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, QUOTA_EXCEEDED_ERR));
                }
            } else if (action.equals("resolveLocalFileSystemURI")) {
                callbackContext.success(resolveLocalFileSystemURI(args.getString(0)));
            } else if (action.equals("getMetadata")) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, (float) getMetadata(args.getString(0))));
            } else if (action.equals("getFileMetadata")) {
                callbackContext.success(getFileMetadata(args.getString(0)));
            } else if (action.equals("getParent")) {
                callbackContext.success(getParent(args.getString(0)));
            } else if (action.equals("getDirectory")) {
                callbackContext.success(getFile(args.getString(0), args.getString(1), args.optJSONObject(2), true));
            } else if (action.equals("getFile")) {
                callbackContext.success(getFile(args.getString(0), args.getString(1), args.optJSONObject(2), false));
            } else if (action.equals("remove")) {
                if (remove(args.getString(0))) {
                    notifyDelete(args.getString(0));
                    callbackContext.success();
                } else {
                    callbackContext.error(NO_MODIFICATION_ALLOWED_ERR);
                }
            } else if (action.equals("removeRecursively")) {
                if (removeRecursively(args.getString(0))) {
                    callbackContext.success();
                } else {
                    callbackContext.error(NO_MODIFICATION_ALLOWED_ERR);
                }
            } else if (action.equals("moveTo")) {
                callbackContext.success(transferTo(args.getString(0), args.getString(1), args.getString(2), true));
            } else if (action.equals("copyTo")) {
                callbackContext.success(transferTo(args.getString(0), args.getString(1), args.getString(2), false));
            } else if (!action.equals("readEntries")) {
                return false;
            } else {
                callbackContext.success(readEntries(args.getString(0)));
            }
        } catch (FileNotFoundException e) {
            callbackContext.error(NOT_FOUND_ERR);
        } catch (FileExistsException e2) {
            callbackContext.error(PATH_EXISTS_ERR);
        } catch (NoModificationAllowedException e3) {
            callbackContext.error(NO_MODIFICATION_ALLOWED_ERR);
        } catch (InvalidModificationException e4) {
            callbackContext.error(INVALID_MODIFICATION_ERR);
        } catch (MalformedURLException e5) {
            callbackContext.error(ENCODING_ERR);
        } catch (IOException e6) {
            callbackContext.error(INVALID_MODIFICATION_ERR);
        } catch (EncodingException e7) {
            callbackContext.error(ENCODING_ERR);
        } catch (TypeMismatchException e8) {
            callbackContext.error(TYPE_MISMATCH_ERR);
        }
        return true;
    }

    private void notifyDelete(String filePath) {
        String newFilePath = FileHelper.getRealPath(filePath, this.cordova);
        try {
            this.cordova.getActivity().getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "_data = ?", new String[]{newFilePath});
        } catch (UnsupportedOperationException e) {
        }
    }

    private JSONObject resolveLocalFileSystemURI(String url) throws IOException, JSONException {
        File fp;
        String decoded = URLDecoder.decode(url, "UTF-8");
        if (decoded.startsWith("content:")) {
            Cursor cursor = this.cordova.getActivity().managedQuery(Uri.parse(decoded), new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            fp = new File(cursor.getString(column_index));
        } else {
            new URL(decoded);
            if (decoded.startsWith("file://")) {
                int questionMark = decoded.indexOf("?");
                if (questionMark < 0) {
                    fp = new File(decoded.substring(7, decoded.length()));
                } else {
                    fp = new File(decoded.substring(7, questionMark));
                }
            } else {
                fp = new File(decoded);
            }
        }
        if (!fp.exists()) {
            throw new FileNotFoundException();
        } else if (fp.canRead()) {
            return getEntry(fp);
        } else {
            throw new IOException();
        }
    }

    private JSONArray readEntries(String fileName) throws FileNotFoundException, JSONException {
        File fp = createFileObject(fileName);
        if (!fp.exists()) {
            throw new FileNotFoundException();
        }
        JSONArray entries = new JSONArray();
        if (fp.isDirectory()) {
            File[] files = fp.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].canRead()) {
                    entries.put(getEntry(files[i]));
                }
            }
        }
        return entries;
    }

    private JSONObject transferTo(String fileName, String newParent, String newName, boolean move) throws JSONException, NoModificationAllowedException, IOException, InvalidModificationException, EncodingException, FileExistsException {
        String newFileName = FileHelper.getRealPath(fileName, this.cordova);
        String newParent2 = FileHelper.getRealPath(newParent, this.cordova);
        if (newName == null || !newName.contains(":")) {
            File source = new File(newFileName);
            if (!source.exists()) {
                throw new FileNotFoundException("The source does not exist");
            }
            File destinationDir = new File(newParent2);
            if (!destinationDir.exists()) {
                throw new FileNotFoundException("The source does not exist");
            }
            File destination = createDestination(newName, source, destinationDir);
            if (source.getAbsolutePath().equals(destination.getAbsolutePath())) {
                throw new InvalidModificationException("Can't copy a file onto itself");
            } else if (source.isDirectory()) {
                if (move) {
                    return moveDirectory(source, destination);
                }
                return copyDirectory(source, destination);
            } else if (!move) {
                return copyFile(source, destination);
            } else {
                JSONObject moveFile = moveFile(source, destination);
                if (!fileName.startsWith("content://")) {
                    return moveFile;
                }
                notifyDelete(fileName);
                return moveFile;
            }
        } else {
            throw new EncodingException("Bad file name");
        }
    }

    private File createDestination(String newName, File fp, File destination) {
        if ("null".equals(newName) || "".equals(newName)) {
            newName = null;
        }
        if (newName != null) {
            return new File(destination.getAbsolutePath() + File.separator + newName);
        }
        return new File(destination.getAbsolutePath() + File.separator + fp.getName());
    }

    private JSONObject copyFile(File srcFile, File destFile) throws IOException, InvalidModificationException, JSONException {
        if (!destFile.exists() || !destFile.isDirectory()) {
            copyAction(srcFile, destFile);
            return getEntry(destFile);
        }
        throw new InvalidModificationException("Can't rename a file to a directory");
    }

    private void copyAction(File srcFile, File destFile) throws FileNotFoundException, IOException {
        FileInputStream istream = new FileInputStream(srcFile);
        FileOutputStream ostream = new FileOutputStream(destFile);
        FileChannel input = istream.getChannel();
        FileChannel output = ostream.getChannel();
        try {
            input.transferTo(0, input.size(), output);
        } finally {
            istream.close();
            ostream.close();
            input.close();
            output.close();
        }
    }

    private JSONObject copyDirectory(File srcDir, File destinationDir) throws JSONException, IOException, NoModificationAllowedException, InvalidModificationException {
        if (destinationDir.exists() && destinationDir.isFile()) {
            throw new InvalidModificationException("Can't rename a file to a directory");
        } else if (isCopyOnItself(srcDir.getAbsolutePath(), destinationDir.getAbsolutePath())) {
            throw new InvalidModificationException("Can't copy itself into itself");
        } else if (destinationDir.exists() || destinationDir.mkdir()) {
            for (File file : srcDir.listFiles()) {
                if (file.isDirectory()) {
                    copyDirectory(file, destinationDir);
                } else {
                    copyFile(file, new File(destinationDir.getAbsoluteFile() + File.separator + file.getName()));
                }
            }
            return getEntry(destinationDir);
        } else {
            throw new NoModificationAllowedException("Couldn't create the destination directory");
        }
    }

    private boolean isCopyOnItself(String src, String dest) {
        if (!dest.startsWith(src) || dest.indexOf(File.separator, src.length() - 1) == -1) {
            return false;
        }
        return true;
    }

    private JSONObject moveFile(File srcFile, File destFile) throws IOException, JSONException, InvalidModificationException {
        if (!destFile.exists() || !destFile.isDirectory()) {
            if (!srcFile.renameTo(destFile)) {
                copyAction(srcFile, destFile);
                if (destFile.exists()) {
                    srcFile.delete();
                } else {
                    throw new IOException("moved failed");
                }
            }
            return getEntry(destFile);
        }
        throw new InvalidModificationException("Can't rename a file to a directory");
    }

    private JSONObject moveDirectory(File srcDir, File destinationDir) throws IOException, JSONException, InvalidModificationException, NoModificationAllowedException, FileExistsException {
        if (destinationDir.exists() && destinationDir.isFile()) {
            throw new InvalidModificationException("Can't rename a file to a directory");
        } else if (isCopyOnItself(srcDir.getAbsolutePath(), destinationDir.getAbsolutePath())) {
            throw new InvalidModificationException("Can't move itself into itself");
        } else if (!destinationDir.exists() || destinationDir.list().length <= 0) {
            if (!srcDir.renameTo(destinationDir)) {
                copyDirectory(srcDir, destinationDir);
                if (destinationDir.exists()) {
                    removeDirRecursively(srcDir);
                } else {
                    throw new IOException("moved failed");
                }
            }
            return getEntry(destinationDir);
        } else {
            throw new InvalidModificationException("directory is not empty");
        }
    }

    private boolean removeRecursively(String filePath) throws FileExistsException {
        File fp = createFileObject(filePath);
        if (atRootDirectory(filePath)) {
            return false;
        }
        return removeDirRecursively(fp);
    }

    private boolean removeDirRecursively(File directory) throws FileExistsException {
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                removeDirRecursively(file);
            }
        }
        if (directory.delete()) {
            return true;
        }
        throw new FileExistsException("could not delete: " + directory.getName());
    }

    private boolean remove(String filePath) throws NoModificationAllowedException, InvalidModificationException {
        File fp = createFileObject(filePath);
        if (atRootDirectory(filePath)) {
            throw new NoModificationAllowedException("You can't delete the root directory");
        } else if (!fp.isDirectory() || fp.list().length <= 0) {
            return fp.delete();
        } else {
            throw new InvalidModificationException("You can't delete a directory that is not empty.");
        }
    }

    private JSONObject getFile(String dirPath, String fileName, JSONObject options, boolean directory) throws FileExistsException, IOException, TypeMismatchException, EncodingException, JSONException {
        boolean create = false;
        boolean exclusive = false;
        if (options != null && (create = options.optBoolean("create"))) {
            exclusive = options.optBoolean("exclusive");
        }
        if (fileName.contains(":")) {
            throw new EncodingException("This file has a : in it's name");
        }
        File fp = createFileObject(dirPath, fileName);
        if (create) {
            if (!exclusive || !fp.exists()) {
                if (directory) {
                    fp.mkdir();
                } else {
                    fp.createNewFile();
                }
                if (!fp.exists()) {
                    throw new FileExistsException("create fails");
                }
            } else {
                throw new FileExistsException("create/exclusive fails");
            }
        } else if (!fp.exists()) {
            throw new FileNotFoundException("path does not exist");
        } else if (directory) {
            if (fp.isFile()) {
                throw new TypeMismatchException("path doesn't exist or is file");
            }
        } else if (fp.isDirectory()) {
            throw new TypeMismatchException("path doesn't exist or is directory");
        }
        return getEntry(fp);
    }

    private File createFileObject(String dirPath, String fileName) {
        if (fileName.startsWith("/")) {
            return new File(fileName);
        }
        return new File(FileHelper.getRealPath(dirPath, this.cordova) + File.separator + fileName);
    }

    private JSONObject getParent(String filePath) throws JSONException {
        String filePath2 = FileHelper.getRealPath(filePath, this.cordova);
        if (atRootDirectory(filePath2)) {
            return getEntry(filePath2);
        }
        return getEntry(new File(filePath2).getParent());
    }

    private boolean atRootDirectory(String filePath) {
        String filePath2 = FileHelper.getRealPath(filePath, this.cordova);
        if (filePath2.equals(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + this.cordova.getActivity().getPackageName() + "/cache") || filePath2.equals(Environment.getExternalStorageDirectory().getAbsolutePath()) || filePath2.equals("/data/data/" + this.cordova.getActivity().getPackageName())) {
            return true;
        }
        return false;
    }

    private File createFileObject(String filePath) {
        return new File(FileHelper.getRealPath(filePath, this.cordova));
    }

    private long getMetadata(String filePath) throws FileNotFoundException {
        File file = createFileObject(filePath);
        if (file.exists()) {
            return file.lastModified();
        }
        throw new FileNotFoundException("Failed to find file in getMetadata");
    }

    private JSONObject getFileMetadata(String filePath) throws FileNotFoundException, JSONException {
        File file = createFileObject(filePath);
        if (!file.exists()) {
            throw new FileNotFoundException("File: " + filePath + " does not exist.");
        }
        JSONObject metadata = new JSONObject();
        metadata.put("size", file.length());
        metadata.put(Globalization.TYPE, FileHelper.getMimeType(filePath, this.cordova));
        metadata.put("name", file.getName());
        metadata.put("fullPath", filePath);
        metadata.put("lastModifiedDate", file.lastModified());
        return metadata;
    }

    private JSONObject requestFileSystem(int type) throws IOException, JSONException {
        JSONObject fs = new JSONObject();
        if (type == TEMPORARY) {
            fs.put("name", "temporary");
            if (Environment.getExternalStorageState().equals("mounted")) {
                new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + this.cordova.getActivity().getPackageName() + "/cache/").mkdirs();
                fs.put("root", getEntry(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + this.cordova.getActivity().getPackageName() + "/cache/"));
            } else {
                new File("/data/data/" + this.cordova.getActivity().getPackageName() + "/cache/").mkdirs();
                fs.put("root", getEntry("/data/data/" + this.cordova.getActivity().getPackageName() + "/cache/"));
            }
        } else if (type == PERSISTENT) {
            fs.put("name", "persistent");
            if (Environment.getExternalStorageState().equals("mounted")) {
                fs.put("root", getEntry(Environment.getExternalStorageDirectory()));
            } else {
                fs.put("root", getEntry("/data/data/" + this.cordova.getActivity().getPackageName()));
            }
        } else {
            throw new IOException("No filesystem of type requested");
        }
        return fs;
    }

    public static JSONObject getEntry(File file) throws JSONException {
        JSONObject entry = new JSONObject();
        entry.put("isFile", file.isFile());
        entry.put("isDirectory", file.isDirectory());
        entry.put("name", file.getName());
        entry.put("fullPath", "file://" + file.getAbsolutePath());
        return entry;
    }

    private JSONObject getEntry(String path) throws JSONException {
        return getEntry(new File(path));
    }

    public void readFileAs(String filename, int start, int end, CallbackContext callbackContext, String encoding, int resultType) {
        final String str = filename;
        final int i = start;
        final int i2 = end;
        final int i3 = resultType;
        final String str2 = encoding;
        final CallbackContext callbackContext2 = callbackContext;
        this.cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                PluginResult result;
                try {
                    byte[] bytes = FileUtils.this.readAsBinaryHelper(str, i, i2);
                    switch (i3) {
                        case 1:
                            result = new PluginResult(PluginResult.Status.OK, new String(bytes, str2));
                            break;
                        case 6:
                            result = new PluginResult(PluginResult.Status.OK, bytes);
                            break;
                        case 7:
                            result = new PluginResult(PluginResult.Status.OK, bytes, true);
                            break;
                        default:
                            result = new PluginResult(PluginResult.Status.OK, "data:" + FileHelper.getMimeType(str, FileUtils.this.cordova) + ";base64," + new String(Base64.encode(bytes, 0), "US-ASCII"));
                            break;
                    }
                    callbackContext2.sendPluginResult(result);
                } catch (FileNotFoundException e) {
                    callbackContext2.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, FileUtils.NOT_FOUND_ERR));
                } catch (IOException e2) {
                    Log.d(FileUtils.LOG_TAG, e2.getLocalizedMessage());
                    callbackContext2.sendPluginResult(new PluginResult(PluginResult.Status.IO_EXCEPTION, FileUtils.NOT_READABLE_ERR));
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public byte[] readAsBinaryHelper(String filename, int start, int end) throws IOException {
        int numBytesToRead = end - start;
        byte[] bytes = new byte[numBytesToRead];
        InputStream inputStream = FileHelper.getInputStreamFromUriString(filename, this.cordova);
        int numBytesRead = 0;
        if (start > 0) {
            inputStream.skip((long) start);
        }
        while (numBytesToRead > 0) {
            numBytesRead = inputStream.read(bytes, numBytesRead, numBytesToRead);
            if (numBytesRead < 0) {
                break;
            }
            numBytesToRead -= numBytesRead;
        }
        return bytes;
    }

    public long write(String filename, String data, int offset, boolean isBinary) throws FileNotFoundException, IOException, NoModificationAllowedException {
        byte[] rawData;
        if (filename.startsWith("content://")) {
            throw new NoModificationAllowedException("Couldn't write to file given its content URI");
        }
        String filename2 = FileHelper.getRealPath(filename, this.cordova);
        boolean append = false;
        if (offset > 0) {
            truncateFile(filename2, (long) offset);
            append = true;
        }
        if (isBinary) {
            rawData = Base64.decode(data, 0);
        } else {
            rawData = data.getBytes();
        }
        ByteArrayInputStream in = new ByteArrayInputStream(rawData);
        FileOutputStream out = new FileOutputStream(filename2, append);
        byte[] buff = new byte[rawData.length];
        in.read(buff, 0, buff.length);
        out.write(buff, 0, rawData.length);
        out.flush();
        out.close();
        return (long) rawData.length;
    }

    private long truncateFile(String filename, long size) throws FileNotFoundException, IOException, NoModificationAllowedException {
        if (filename.startsWith("content://")) {
            throw new NoModificationAllowedException("Couldn't truncate file given its content URI");
        }
        RandomAccessFile raf = new RandomAccessFile(FileHelper.getRealPath(filename, this.cordova), "rw");
        try {
            if (raf.length() >= size) {
                raf.getChannel().truncate(size);
            } else {
                size = raf.length();
            }
            return size;
        } finally {
            raf.close();
        }
    }
}
