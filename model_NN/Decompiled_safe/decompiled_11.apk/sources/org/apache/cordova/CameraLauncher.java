package org.apache.cordova;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.LOG;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class CameraLauncher extends CordovaPlugin implements MediaScannerConnection.MediaScannerConnectionClient {
    private static final int ALLMEDIA = 2;
    private static final int CAMERA = 1;
    private static final int DATA_URL = 0;
    private static final int FILE_URI = 1;
    private static final String GET_All = "Get All";
    private static final String GET_PICTURE = "Get Picture";
    private static final String GET_VIDEO = "Get Video";
    private static final int JPEG = 0;
    private static final String LOG_TAG = "CameraLauncher";
    private static final int NATIVE_URI = 2;
    private static final int PHOTOLIBRARY = 0;
    private static final int PICTURE = 0;
    private static final int PNG = 1;
    private static final int SAVEDPHOTOALBUM = 2;
    private static final int VIDEO = 1;
    public CallbackContext callbackContext;
    private MediaScannerConnection conn;
    private boolean correctOrientation;
    private int encodingType;
    private Uri imageUri;
    private int mQuality;
    private int mediaType;
    private int numPics;
    private boolean saveToPhotoAlbum;
    private Uri scanMe;
    private int targetHeight;
    private int targetWidth;

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext2) throws JSONException {
        this.callbackContext = callbackContext2;
        if (!action.equals("takePicture")) {
            return false;
        }
        this.saveToPhotoAlbum = false;
        this.targetHeight = 0;
        this.targetWidth = 0;
        this.encodingType = 0;
        this.mediaType = 0;
        this.mQuality = 80;
        this.mQuality = args.getInt(0);
        int destType = args.getInt(1);
        int srcType = args.getInt(2);
        this.targetWidth = args.getInt(3);
        this.targetHeight = args.getInt(4);
        this.encodingType = args.getInt(5);
        this.mediaType = args.getInt(6);
        this.correctOrientation = args.getBoolean(8);
        this.saveToPhotoAlbum = args.getBoolean(9);
        if (this.targetWidth < 1) {
            this.targetWidth = -1;
        }
        if (this.targetHeight < 1) {
            this.targetHeight = -1;
        }
        if (srcType == 1) {
            takePicture(destType, this.encodingType);
        } else if (srcType == 0 || srcType == 2) {
            getImage(srcType, destType);
        }
        PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
        r.setKeepCallback(true);
        callbackContext2.sendPluginResult(r);
        return true;
    }

    public void takePicture(int returnType, int encodingType2) {
        this.numPics = queryImgDB(whichContentStore()).getCount();
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo = createCaptureFile(encodingType2);
        intent.putExtra("output", Uri.fromFile(photo));
        this.imageUri = Uri.fromFile(photo);
        if (this.cordova != null) {
            this.cordova.startActivityForResult(this, intent, returnType + 32 + 1);
        }
    }

    private File createCaptureFile(int encodingType2) {
        if (encodingType2 == 0) {
            return new File(DirectoryManager.getTempDirectoryPath(this.cordova.getActivity()), ".Pic.jpg");
        }
        if (encodingType2 == 1) {
            return new File(DirectoryManager.getTempDirectoryPath(this.cordova.getActivity()), ".Pic.png");
        }
        throw new IllegalArgumentException("Invalid Encoding Type: " + encodingType2);
    }

    public void getImage(int srcType, int returnType) {
        Intent intent = new Intent();
        String title = GET_PICTURE;
        if (this.mediaType == 0) {
            intent.setType("image/*");
        } else if (this.mediaType == 1) {
            intent.setType("video/*");
            title = GET_VIDEO;
        } else if (this.mediaType == 2) {
            intent.setType("*/*");
            title = GET_All;
        }
        intent.setAction("android.intent.action.GET_CONTENT");
        intent.addCategory("android.intent.category.OPENABLE");
        if (this.cordova != null) {
            this.cordova.startActivityForResult(this, Intent.createChooser(intent, new String(title)), ((srcType + 1) * 16) + returnType + 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        int rotate;
        String exifPath;
        int srcType = (requestCode / 16) - 1;
        int destType = (requestCode % 16) - 1;
        int rotate2 = 0;
        if (srcType == 1) {
            if (resultCode == -1) {
                try {
                    ExifHelper exif = new ExifHelper();
                    try {
                        if (this.encodingType == 0) {
                            exif.createInFile(DirectoryManager.getTempDirectoryPath(this.cordova.getActivity()) + "/.Pic.jpg");
                            exif.readExifData();
                            rotate2 = exif.getOrientation();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Bitmap bitmap = null;
                    Uri uri = null;
                    if (destType == 0) {
                        bitmap = getScaledBitmap(FileHelper.stripFileProtocol(this.imageUri.toString()));
                        if (bitmap == null) {
                            bitmap = (Bitmap) intent.getExtras().get("data");
                        }
                        if (bitmap == null) {
                            Log.d(LOG_TAG, "I either have a null image path or bitmap");
                            failPicture("Unable to create bitmap!");
                            return;
                        }
                        if (rotate2 != 0) {
                            if (this.correctOrientation) {
                                bitmap = getRotatedBitmap(rotate2, bitmap, exif);
                            }
                        }
                        processPicture(bitmap);
                        checkForDuplicateImage(0);
                    } else if (destType == 1 || destType == 2) {
                        if (this.saveToPhotoAlbum) {
                            uri = Uri.fromFile(new File(FileHelper.getRealPath(getUriFromMediaStore(), this.cordova)));
                        } else {
                            uri = Uri.fromFile(new File(DirectoryManager.getTempDirectoryPath(this.cordova.getActivity()), System.currentTimeMillis() + ".jpg"));
                        }
                        if (uri == null) {
                            failPicture("Error capturing image - no media storage found.");
                        }
                        if (this.targetHeight == -1 && this.targetWidth == -1 && this.mQuality == 100 && !this.correctOrientation) {
                            writeUncompressedImage(uri);
                            this.callbackContext.success(uri.toString());
                        } else {
                            Bitmap bitmap2 = getScaledBitmap(FileHelper.stripFileProtocol(this.imageUri.toString()));
                            if (rotate2 != 0 && this.correctOrientation) {
                                bitmap2 = getRotatedBitmap(rotate2, bitmap2, exif);
                            }
                            OutputStream os = this.cordova.getActivity().getContentResolver().openOutputStream(uri);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, this.mQuality, os);
                            os.close();
                            if (this.encodingType == 0) {
                                if (this.saveToPhotoAlbum) {
                                    exifPath = FileHelper.getRealPath(uri, this.cordova);
                                } else {
                                    exifPath = uri.getPath();
                                }
                                exif.createOutFile(exifPath);
                                exif.writeExifData();
                            }
                        }
                        this.callbackContext.success(uri.toString());
                    }
                    cleanup(1, this.imageUri, uri, bitmap);
                } catch (IOException e2) {
                    e2.printStackTrace();
                    failPicture("Error capturing image.");
                }
            } else if (resultCode == 0) {
                failPicture("Camera cancelled.");
            } else {
                failPicture("Did not complete!");
            }
        } else if (srcType != 0 && srcType != 2) {
        } else {
            if (resultCode == -1) {
                Uri uri2 = intent.getData();
                if (this.mediaType != 0) {
                    this.callbackContext.success(uri2.toString());
                } else if (this.targetHeight == -1 && this.targetWidth == -1 && ((destType == 1 || destType == 2) && !this.correctOrientation)) {
                    this.callbackContext.success(uri2.toString());
                } else {
                    String uriString = uri2.toString();
                    String mimeType = FileHelper.getMimeType(uriString, this.cordova);
                    if ("image/jpeg".equalsIgnoreCase(mimeType) || "image/png".equalsIgnoreCase(mimeType)) {
                        Bitmap bitmap3 = null;
                        try {
                            bitmap3 = getScaledBitmap(uriString);
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        }
                        if (bitmap3 == null) {
                            Log.d(LOG_TAG, "I either have a null image path or bitmap");
                            failPicture("Unable to create bitmap!");
                            return;
                        }
                        if (this.correctOrientation && (rotate = getImageOrientation(uri2)) != 0) {
                            Matrix matrix = new Matrix();
                            matrix.setRotate((float) rotate);
                            bitmap3 = Bitmap.createBitmap(bitmap3, 0, 0, bitmap3.getWidth(), bitmap3.getHeight(), matrix, true);
                        }
                        if (destType == 0) {
                            processPicture(bitmap3);
                        } else if (destType == 1 || destType == 2) {
                            if (this.targetHeight <= 0 || this.targetWidth <= 0) {
                                this.callbackContext.success(uri2.toString());
                            } else {
                                try {
                                    String resizePath = DirectoryManager.getTempDirectoryPath(this.cordova.getActivity()) + "/resize.jpg";
                                    String realPath = FileHelper.getRealPath(uri2, this.cordova);
                                    ExifHelper exif2 = new ExifHelper();
                                    if (realPath != null && this.encodingType == 0) {
                                        try {
                                            exif2.createInFile(realPath);
                                            exif2.readExifData();
                                            int rotate3 = exif2.getOrientation();
                                        } catch (IOException e4) {
                                            e4.printStackTrace();
                                        }
                                    }
                                    OutputStream os2 = new FileOutputStream(resizePath);
                                    bitmap3.compress(Bitmap.CompressFormat.JPEG, this.mQuality, os2);
                                    os2.close();
                                    if (realPath != null && this.encodingType == 0) {
                                        exif2.createOutFile(resizePath);
                                        exif2.writeExifData();
                                    }
                                    this.callbackContext.success("file://" + resizePath + "?" + System.currentTimeMillis());
                                } catch (Exception e5) {
                                    e5.printStackTrace();
                                    failPicture("Error retrieving image.");
                                }
                            }
                        }
                        if (bitmap3 != null) {
                            bitmap3.recycle();
                        }
                        System.gc();
                        return;
                    }
                    Log.d(LOG_TAG, "I either have a null image path or bitmap");
                    failPicture("Unable to retrieve path to picture!");
                }
            } else if (resultCode == 0) {
                failPicture("Selection cancelled.");
            } else {
                failPicture("Selection did not complete!");
            }
        }
    }

    private int getImageOrientation(Uri uri) {
        Cursor cursor = this.cordova.getActivity().getContentResolver().query(uri, new String[]{"orientation"}, null, null, null);
        if (cursor == null) {
            return 0;
        }
        cursor.moveToPosition(0);
        int rotate = cursor.getInt(0);
        cursor.close();
        return rotate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap getRotatedBitmap(int rotate, Bitmap bitmap, ExifHelper exif) {
        Matrix matrix = new Matrix();
        if (rotate == 180) {
            matrix.setRotate((float) rotate);
        } else {
            matrix.setRotate((float) rotate, ((float) bitmap.getWidth()) / 2.0f, ((float) bitmap.getHeight()) / 2.0f);
        }
        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        exif.resetOrientation();
        return bitmap2;
    }

    private void writeUncompressedImage(Uri uri) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(FileHelper.stripFileProtocol(this.imageUri.toString()));
        OutputStream os = this.cordova.getActivity().getContentResolver().openOutputStream(uri);
        byte[] buffer = new byte[4096];
        while (true) {
            int len = fis.read(buffer);
            if (len != -1) {
                os.write(buffer, 0, len);
            } else {
                os.flush();
                os.close();
                fis.close();
                return;
            }
        }
    }

    private Uri getUriFromMediaStore() {
        ContentValues values = new ContentValues();
        values.put("mime_type", "image/jpeg");
        try {
            return this.cordova.getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } catch (UnsupportedOperationException e) {
            LOG.d(LOG_TAG, "Can't write to external media storage.");
            try {
                return this.cordova.getActivity().getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, values);
            } catch (UnsupportedOperationException e2) {
                LOG.d(LOG_TAG, "Can't write to internal media storage.");
                return null;
            }
        }
    }

    private Bitmap getScaledBitmap(String imageUrl) throws IOException {
        if (this.targetWidth <= 0 && this.targetHeight <= 0) {
            return BitmapFactory.decodeStream(FileHelper.getInputStreamFromUriString(imageUrl, this.cordova));
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(FileHelper.getInputStreamFromUriString(imageUrl, this.cordova), null, options);
        if (options.outWidth == 0 || options.outHeight == 0) {
            return null;
        }
        int[] widthHeight = calculateAspectRatio(options.outWidth, options.outHeight);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, this.targetWidth, this.targetHeight);
        Bitmap unscaledBitmap = BitmapFactory.decodeStream(FileHelper.getInputStreamFromUriString(imageUrl, this.cordova), null, options);
        if (unscaledBitmap != null) {
            return Bitmap.createScaledBitmap(unscaledBitmap, widthHeight[0], widthHeight[1], true);
        }
        return null;
    }

    public int[] calculateAspectRatio(int origWidth, int origHeight) {
        int newWidth = this.targetWidth;
        int newHeight = this.targetHeight;
        if (newWidth <= 0 && newHeight <= 0) {
            newWidth = origWidth;
            newHeight = origHeight;
        } else if (newWidth > 0 && newHeight <= 0) {
            newHeight = (newWidth * origHeight) / origWidth;
        } else if (newWidth > 0 || newHeight <= 0) {
            double newRatio = ((double) newWidth) / ((double) newHeight);
            double origRatio = ((double) origWidth) / ((double) origHeight);
            if (origRatio > newRatio) {
                newHeight = (newWidth * origHeight) / origWidth;
            } else if (origRatio < newRatio) {
                newWidth = (newHeight * origWidth) / origHeight;
            }
        } else {
            newWidth = (newHeight * origWidth) / origHeight;
        }
        return new int[]{newWidth, newHeight};
    }

    public static int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight) {
        if (((float) srcWidth) / ((float) srcHeight) > ((float) dstWidth) / ((float) dstHeight)) {
            return srcWidth / dstWidth;
        }
        return srcHeight / dstHeight;
    }

    private Cursor queryImgDB(Uri contentStore) {
        return this.cordova.getActivity().getContentResolver().query(contentStore, new String[]{"_id"}, null, null, null);
    }

    private void cleanup(int imageType, Uri oldImage, Uri newImage, Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
        }
        new File(FileHelper.stripFileProtocol(oldImage.toString())).delete();
        checkForDuplicateImage(imageType);
        if (this.saveToPhotoAlbum && newImage != null) {
            scanForGallery(newImage);
        }
        System.gc();
    }

    private void checkForDuplicateImage(int type) {
        int diff = 1;
        Uri contentStore = whichContentStore();
        Cursor cursor = queryImgDB(contentStore);
        int currentNumOfImages = cursor.getCount();
        if (type == 1 && this.saveToPhotoAlbum) {
            diff = 2;
        }
        if (currentNumOfImages - this.numPics == diff) {
            cursor.moveToLast();
            int id = Integer.valueOf(cursor.getString(cursor.getColumnIndex("_id"))).intValue();
            if (diff == 2) {
                id--;
            }
            this.cordova.getActivity().getContentResolver().delete(Uri.parse(contentStore + "/" + id), null, null);
        }
    }

    private Uri whichContentStore() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        }
        return MediaStore.Images.Media.INTERNAL_CONTENT_URI;
    }

    public void processPicture(Bitmap bitmap) {
        ByteArrayOutputStream jpeg_data = new ByteArrayOutputStream();
        try {
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, this.mQuality, jpeg_data)) {
                this.callbackContext.success(new String(Base64.encode(jpeg_data.toByteArray(), 0)));
            }
        } catch (Exception e) {
            failPicture("Error compressing image.");
        }
    }

    public void failPicture(String err) {
        this.callbackContext.error(err);
    }

    private void scanForGallery(Uri newImage) {
        this.scanMe = newImage;
        if (this.conn != null) {
            this.conn.disconnect();
        }
        this.conn = new MediaScannerConnection(this.cordova.getActivity().getApplicationContext(), this);
        this.conn.connect();
    }

    public void onMediaScannerConnected() {
        try {
            this.conn.scanFile(this.scanMe.toString(), "image/*");
        } catch (IllegalStateException e) {
            LOG.e(LOG_TAG, "Can't scan file in MediaScanner after taking picture");
        }
    }

    public void onScanCompleted(String path, Uri uri) {
        this.conn.disconnect();
    }
}
