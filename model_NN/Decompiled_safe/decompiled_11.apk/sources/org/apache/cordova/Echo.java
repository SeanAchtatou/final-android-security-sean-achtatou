package org.apache.cordova;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONException;

public class Echo extends CordovaPlugin {
    public boolean execute(String action, CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        final String result = null;
        if ("echo".equals(action)) {
            if (!args.isNull(0)) {
                result = args.getString(0);
            }
            callbackContext.success(result);
            return true;
        } else if ("echoAsync".equals(action)) {
            if (!args.isNull(0)) {
                result = args.getString(0);
            }
            this.cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    callbackContext.success(result);
                }
            });
            return true;
        } else if (!"echoArrayBuffer".equals(action)) {
            return false;
        } else {
            callbackContext.success(args.getArrayBuffer(0));
            return true;
        }
    }
}
