package org.apache.cordova;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebHistoryItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.squareup.okhttp.internal.spdy.SpdyStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.cordova.api.CordovaInterface;
import org.apache.cordova.api.LOG;
import org.apache.cordova.api.PluginManager;
import org.apache.cordova.api.PluginResult;

public class CordovaWebView extends WebView {
    static final FrameLayout.LayoutParams COVER_SCREEN_GRAVITY_CENTER = new FrameLayout.LayoutParams(-1, -1, 17);
    public static final String TAG = "CordovaWebView";
    private boolean bound;
    private CordovaChromeClient chromeClient;
    /* access modifiers changed from: private */
    public CordovaInterface cordova;
    ExposedJsApi exposedJsApi;
    private boolean handleButton = false;
    NativeToJsMessageQueue jsMessageQueue;
    private ArrayList<Integer> keyDownCodes = new ArrayList<>();
    private ArrayList<Integer> keyUpCodes = new ArrayList<>();
    private long lastMenuEventTime = 0;
    int loadUrlTimeout = 0;
    private View mCustomView;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    private ActivityResult mResult = null;
    private boolean paused;
    public PluginManager pluginManager;
    private BroadcastReceiver receiver;
    private String url;
    CordovaWebViewClient viewClient;

    class ActivityResult {
        Intent incoming;
        int request;
        int result;

        public ActivityResult(int req, int res, Intent intent) {
            this.request = req;
            this.result = res;
            this.incoming = intent;
        }
    }

    public CordovaWebView(Context context) {
        super(context);
        if (CordovaInterface.class.isInstance(context)) {
            this.cordova = (CordovaInterface) context;
        } else {
            Log.d(TAG, "Your activity must implement CordovaInterface to work");
        }
        loadConfiguration();
        setup();
    }

    public CordovaWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (CordovaInterface.class.isInstance(context)) {
            this.cordova = (CordovaInterface) context;
        } else {
            Log.d(TAG, "Your activity must implement CordovaInterface to work");
        }
        setWebChromeClient(new CordovaChromeClient(this.cordova, this));
        initWebViewClient(this.cordova);
        loadConfiguration();
        setup();
    }

    public CordovaWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (CordovaInterface.class.isInstance(context)) {
            this.cordova = (CordovaInterface) context;
        } else {
            Log.d(TAG, "Your activity must implement CordovaInterface to work");
        }
        setWebChromeClient(new CordovaChromeClient(this.cordova, this));
        loadConfiguration();
        setup();
    }

    @TargetApi(SpdyStream.RST_FRAME_TOO_LARGE)
    public CordovaWebView(Context context, AttributeSet attrs, int defStyle, boolean privateBrowsing) {
        super(context, attrs, defStyle, privateBrowsing);
        if (CordovaInterface.class.isInstance(context)) {
            this.cordova = (CordovaInterface) context;
        } else {
            Log.d(TAG, "Your activity must implement CordovaInterface to work");
        }
        setWebChromeClient(new CordovaChromeClient(this.cordova));
        initWebViewClient(this.cordova);
        loadConfiguration();
        setup();
    }

    private void initWebViewClient(CordovaInterface cordova2) {
        if (Build.VERSION.SDK_INT < 11 || Build.VERSION.SDK_INT > 17) {
            setWebViewClient(new CordovaWebViewClient(this.cordova, this));
        } else {
            setWebViewClient(new IceCreamCordovaWebViewClient(this.cordova, this));
        }
    }

    @SuppressLint({"NewApi"})
    private void setup() {
        setInitialScale(0);
        setVerticalScrollBarEnabled(false);
        requestFocusFromTouch();
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        Class<WebSettings> cls = WebSettings.class;
        try {
            Method gingerbread_getMethod = cls.getMethod("setNavDump", Boolean.TYPE);
            Log.d(TAG, "CordovaWebView is running on device made by: " + Build.MANUFACTURER);
            if (Build.VERSION.SDK_INT < 11 && Build.MANUFACTURER.contains("HTC")) {
                gingerbread_getMethod.invoke(settings, true);
            }
        } catch (NoSuchMethodException e) {
            Log.d(TAG, "We are on a modern version of Android, we will deprecate HTC 2.3 devices in 2.8");
        } catch (IllegalArgumentException e2) {
            Log.d(TAG, "Doing the NavDump failed with bad arguments");
        } catch (IllegalAccessException e3) {
            Log.d(TAG, "This should never happen: IllegalAccessException means this isn't Android anymore");
        } catch (InvocationTargetException e4) {
            Log.d(TAG, "This should never happen: InvocationTargetException means this isn't Android anymore.");
        }
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        if (Build.VERSION.SDK_INT > 15) {
            Level16Apis.enableUniversalAccess(settings);
        }
        String databasePath = this.cordova.getActivity().getApplicationContext().getDir("database", 0).getPath();
        settings.setDatabaseEnabled(true);
        settings.setDatabasePath(databasePath);
        settings.setGeolocationDatabasePath(databasePath);
        settings.setDomStorageEnabled(true);
        settings.setGeolocationEnabled(true);
        settings.setAppCacheMaxSize(5242880);
        settings.setAppCachePath(this.cordova.getActivity().getApplicationContext().getDir("database", 0).getPath());
        settings.setAppCacheEnabled(true);
        updateUserAgentString();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.CONFIGURATION_CHANGED");
        if (this.receiver == null) {
            this.receiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    CordovaWebView.this.updateUserAgentString();
                }
            };
            this.cordova.getActivity().registerReceiver(this.receiver, intentFilter);
        }
        this.pluginManager = new PluginManager(this, this.cordova);
        this.jsMessageQueue = new NativeToJsMessageQueue(this, this.cordova);
        this.exposedJsApi = new ExposedJsApi(this.pluginManager, this.jsMessageQueue);
        exposeJsInterface();
    }

    /* access modifiers changed from: private */
    public void updateUserAgentString() {
        getSettings().getUserAgentString();
    }

    private void exposeJsInterface() {
        int SDK_INT = Build.VERSION.SDK_INT;
        if ((SDK_INT >= 11 && SDK_INT <= 13) || SDK_INT < 9) {
            Log.i(TAG, "Disabled addJavascriptInterface() bridge since Android version is old.");
        } else if (SDK_INT >= 11 || !Build.MANUFACTURER.equals(NetworkManager.TYPE_UNKNOWN)) {
            addJavascriptInterface(this.exposedJsApi, "_cordovaNative");
        } else {
            Log.i(TAG, "Disabled addJavascriptInterface() bridge callback due to a bug on the 2.3 emulator");
        }
    }

    public void setWebViewClient(CordovaWebViewClient client) {
        this.viewClient = client;
        super.setWebViewClient((WebViewClient) client);
    }

    public void setWebChromeClient(CordovaChromeClient client) {
        this.chromeClient = client;
        super.setWebChromeClient((WebChromeClient) client);
    }

    public CordovaChromeClient getWebChromeClient() {
        return this.chromeClient;
    }

    public void loadUrl(String url2) {
        if (url2.equals("about:blank") || url2.startsWith("javascript:")) {
            loadUrlNow(url2);
            return;
        }
        String initUrl = getProperty("url", null);
        if (initUrl == null) {
            loadUrlIntoView(url2);
        } else {
            loadUrlIntoView(initUrl);
        }
    }

    public void loadUrl(String url2, int time) {
        String initUrl = getProperty("url", null);
        if (initUrl == null) {
            loadUrlIntoView(url2, time);
        } else {
            loadUrlIntoView(initUrl);
        }
    }

    public void loadUrlIntoView(final String url2) {
        LOG.d(TAG, ">>> loadUrl(" + url2 + ")");
        this.url = url2;
        this.pluginManager.init();
        final int currentLoadUrlTimeout = this.loadUrlTimeout;
        final int loadUrlTimeoutValue = Integer.parseInt(getProperty("loadUrlTimeoutValue", "20000"));
        final Runnable loadError = new Runnable() {
            public void run() {
                this.stopLoading();
                LOG.e(CordovaWebView.TAG, "CordovaWebView: TIMEOUT ERROR!");
                if (CordovaWebView.this.viewClient != null) {
                    CordovaWebView.this.viewClient.onReceivedError(this, -6, "The connection to the server was unsuccessful.", url2);
                }
            }
        };
        final Runnable timeoutCheck = new Runnable() {
            public void run() {
                try {
                    synchronized (this) {
                        wait((long) loadUrlTimeoutValue);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (this.loadUrlTimeout == currentLoadUrlTimeout) {
                    this.cordova.getActivity().runOnUiThread(loadError);
                }
            }
        };
        this.cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                new Thread(timeoutCheck).start();
                this.loadUrlNow(url2);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void loadUrlNow(String url2) {
        if (LOG.isLoggable(3) && !url2.startsWith("javascript:")) {
            LOG.d(TAG, ">>> loadUrlNow()");
        }
        if (url2.startsWith("file://") || url2.startsWith("javascript:") || Config.isUrlWhiteListed(url2)) {
            super.loadUrl(url2);
        }
    }

    public void loadUrlIntoView(String url2, int time) {
        if (!url2.startsWith("javascript:") && !canGoBack()) {
            LOG.d(TAG, "loadUrlIntoView(%s, %d)", url2, Integer.valueOf(time));
            postMessage("splashscreen", "show");
        }
        loadUrlIntoView(url2);
    }

    public void sendJavascript(String statement) {
        this.jsMessageQueue.addJavaScript(statement);
    }

    public void sendPluginResult(PluginResult result, String callbackId) {
        this.jsMessageQueue.addPluginResult(result, callbackId);
    }

    public void postMessage(String id, Object data) {
        if (this.pluginManager != null) {
            this.pluginManager.postMessage(id, data);
        }
    }

    public boolean backHistory() {
        if (!super.canGoBack()) {
            return false;
        }
        printBackForwardList();
        super.goBack();
        return true;
    }

    public void showWebPage(String url2, boolean openExternal, boolean clearHistory, HashMap<String, Object> hashMap) {
        LOG.d(TAG, "showWebPage(%s, %b, %b, HashMap", url2, Boolean.valueOf(openExternal), Boolean.valueOf(clearHistory));
        if (clearHistory) {
            clearHistory();
        }
        if (openExternal) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(url2));
                this.cordova.getActivity().startActivity(intent);
            } catch (ActivityNotFoundException e) {
                LOG.e(TAG, "Error loading url " + url2, e);
            }
        } else if (url2.startsWith("file://") || Config.isUrlWhiteListed(url2)) {
            loadUrl(url2);
        } else {
            LOG.w(TAG, "showWebPage: Cannot load URL into webview since it is not in white list.  Loading into browser instead. (URL=" + url2 + ")");
            try {
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.setData(Uri.parse(url2));
                this.cordova.getActivity().startActivity(intent2);
            } catch (ActivityNotFoundException e2) {
                LOG.e(TAG, "Error loading url " + url2, e2);
            }
        }
    }

    private void loadConfiguration() {
        if ("true".equals(getProperty("fullscreen", "false"))) {
            this.cordova.getActivity().getWindow().clearFlags(2048);
            this.cordova.getActivity().getWindow().setFlags(1024, 1024);
        }
    }

    public String getProperty(String name, String defaultValue) {
        Object p;
        Bundle bundle = this.cordova.getActivity().getIntent().getExtras();
        return (bundle == null || (p = bundle.get(name)) == null) ? defaultValue : p.toString();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean z = false;
        if (this.keyDownCodes.contains(Integer.valueOf(keyCode))) {
            if (keyCode == 25) {
                LOG.d(TAG, "Down Key Hit");
                loadUrl("javascript:cordova.fireDocumentEvent('volumedownbutton');");
                return true;
            } else if (keyCode != 24) {
                return super.onKeyDown(keyCode, event);
            } else {
                LOG.d(TAG, "Up Key Hit");
                loadUrl("javascript:cordova.fireDocumentEvent('volumeupbutton');");
                return true;
            }
        } else if (keyCode == 4) {
            if (!startOfHistory() || this.bound) {
                z = true;
            }
            return z;
        } else if (keyCode != 82) {
            return super.onKeyDown(keyCode, event);
        } else {
            View childView = getFocusedChild();
            if (childView == null) {
                return super.onKeyDown(keyCode, event);
            }
            ((InputMethodManager) this.cordova.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(childView.getWindowToken(), 0);
            this.cordova.getActivity().openOptionsMenu();
            return true;
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.mCustomView != null) {
                hideCustomView();
            } else if (this.bound) {
                loadUrl("javascript:cordova.fireDocumentEvent('backbutton');");
                return true;
            } else if (backHistory()) {
                return true;
            } else {
                this.cordova.getActivity().finish();
            }
        } else if (keyCode == 82) {
            if (this.lastMenuEventTime < event.getEventTime()) {
                loadUrl("javascript:cordova.fireDocumentEvent('menubutton');");
            }
            this.lastMenuEventTime = event.getEventTime();
            return super.onKeyUp(keyCode, event);
        } else if (keyCode == 84) {
            loadUrl("javascript:cordova.fireDocumentEvent('searchbutton');");
            return true;
        } else if (this.keyUpCodes.contains(Integer.valueOf(keyCode))) {
            return super.onKeyUp(keyCode, event);
        }
        return super.onKeyUp(keyCode, event);
    }

    public void bindButton(boolean override) {
        this.bound = override;
    }

    public void bindButton(String button, boolean override) {
        if (button.compareTo("volumeup") == 0) {
            this.keyDownCodes.add(24);
        } else if (button.compareTo("volumedown") == 0) {
            this.keyDownCodes.add(25);
        }
    }

    public void bindButton(int keyCode, boolean keyDown, boolean override) {
        if (keyDown) {
            this.keyDownCodes.add(Integer.valueOf(keyCode));
        } else {
            this.keyUpCodes.add(Integer.valueOf(keyCode));
        }
    }

    public boolean isBackButtonBound() {
        return this.bound;
    }

    public void handlePause(boolean keepRunning) {
        LOG.d(TAG, "Handle the pause");
        loadUrl("javascript:try{cordova.fireDocumentEvent('pause');}catch(e){console.log('exception firing pause event from native');};");
        if (this.pluginManager != null) {
            this.pluginManager.onPause(keepRunning);
        }
        if (!keepRunning) {
            pauseTimers();
        }
        this.paused = true;
    }

    public void handleResume(boolean keepRunning, boolean activityResultKeepRunning) {
        loadUrl("javascript:try{cordova.fireDocumentEvent('resume');}catch(e){console.log('exception firing resume event from native');};");
        if (this.pluginManager != null) {
            this.pluginManager.onResume(keepRunning);
        }
        resumeTimers();
        this.paused = false;
    }

    public void handleDestroy() {
        loadUrl("javascript:try{cordova.require('cordova/channel').onDestroy.fire();}catch(e){console.log('exception firing destroy event from native');};");
        loadUrl("about:blank");
        if (this.pluginManager != null) {
            this.pluginManager.onDestroy();
        }
        if (this.receiver != null) {
            try {
                this.cordova.getActivity().unregisterReceiver(this.receiver);
            } catch (Exception e) {
                Log.e(TAG, "Error unregistering configuration receiver: " + e.getMessage(), e);
            }
        }
    }

    public void onNewIntent(Intent intent) {
        if (this.pluginManager != null) {
            this.pluginManager.onNewIntent(intent);
        }
    }

    public boolean isPaused() {
        return this.paused;
    }

    public boolean hadKeyEvent() {
        return this.handleButton;
    }

    @TargetApi(16)
    private static class Level16Apis {
        private Level16Apis() {
        }

        static void enableUniversalAccess(WebSettings settings) {
            settings.setAllowUniversalAccessFromFileURLs(true);
        }
    }

    public void printBackForwardList() {
        WebBackForwardList currentList = copyBackForwardList();
        int currentSize = currentList.getSize();
        for (int i = 0; i < currentSize; i++) {
            LOG.d(TAG, "The URL at index: " + Integer.toString(i) + "is " + currentList.getItemAtIndex(i).getUrl());
        }
    }

    public boolean startOfHistory() {
        WebHistoryItem item = copyBackForwardList().getItemAtIndex(0);
        if (item == null) {
            return false;
        }
        String url2 = item.getUrl();
        String currentUrl = getUrl();
        LOG.d(TAG, "The current URL is: " + currentUrl);
        LOG.d(TAG, "The URL at item 0 is:" + url2);
        return currentUrl.equals(url2);
    }

    public void showCustomView(View view, WebChromeClient.CustomViewCallback callback) {
        Log.d(TAG, "showing Custom View");
        if (this.mCustomView != null) {
            callback.onCustomViewHidden();
            return;
        }
        this.mCustomView = view;
        this.mCustomViewCallback = callback;
        ViewGroup parent = (ViewGroup) getParent();
        parent.addView(view, COVER_SCREEN_GRAVITY_CENTER);
        setVisibility(8);
        parent.setVisibility(0);
        parent.bringToFront();
    }

    public void hideCustomView() {
        Log.d(TAG, "Hidding Custom View");
        if (this.mCustomView != null) {
            this.mCustomView.setVisibility(8);
            ((ViewGroup) getParent()).removeView(this.mCustomView);
            this.mCustomView = null;
            this.mCustomViewCallback.onCustomViewHidden();
            setVisibility(0);
        }
    }

    public boolean isCustomViewShowing() {
        return this.mCustomView != null;
    }

    public WebBackForwardList restoreState(Bundle savedInstanceState) {
        WebBackForwardList myList = super.restoreState(savedInstanceState);
        Log.d(TAG, "WebView restoration crew now restoring!");
        this.pluginManager.init();
        return myList;
    }

    public void storeResult(int requestCode, int resultCode, Intent intent) {
        this.mResult = new ActivityResult(requestCode, resultCode, intent);
    }
}
