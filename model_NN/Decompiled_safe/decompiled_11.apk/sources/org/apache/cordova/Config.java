package org.apache.cordova;

import android.app.Activity;
import android.content.res.XmlResourceParser;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.cordova.api.LOG;
import org.xmlpull.v1.XmlPullParserException;

public class Config {
    public static final String TAG = "Config";
    private static Config self = null;
    private String startUrl;
    private ArrayList<Pattern> whiteList = new ArrayList<>();
    private HashMap<String, Boolean> whiteListCache = new HashMap<>();

    public static void init(Activity action) {
        self = new Config(action);
    }

    public static void init() {
        if (self == null) {
            self = new Config();
        }
    }

    private Config() {
    }

    private Config(Activity action) {
        boolean z;
        if (action == null) {
            LOG.i("CordovaLog", "There is no activity. Is this on the lock screen?");
            return;
        }
        int id = action.getResources().getIdentifier("config", "xml", action.getPackageName());
        if (id == 0) {
            id = action.getResources().getIdentifier("cordova", "xml", action.getPackageName());
            LOG.i("CordovaLog", "config.xml missing, reverting to cordova.xml");
        }
        if (id == 0) {
            LOG.i("CordovaLog", "cordova.xml missing. Ignoring...");
            return;
        }
        XmlResourceParser xml = action.getResources().getXml(id);
        int eventType = -1;
        while (eventType != 1) {
            if (eventType == 2) {
                String strNode = xml.getName();
                if (strNode.equals("access")) {
                    String origin = xml.getAttributeValue(null, "origin");
                    String subdomains = xml.getAttributeValue(null, "subdomains");
                    if (origin != null) {
                        if (subdomains == null || subdomains.compareToIgnoreCase("true") != 0) {
                            z = false;
                        } else {
                            z = true;
                        }
                        _addWhiteListEntry(origin, z);
                    }
                } else if (strNode.equals("log")) {
                    String level = xml.getAttributeValue(null, "level");
                    Log.d(TAG, "The <log> tags is deprecated. Use <preference name=\"loglevel\" value=\"" + level + "\"/> instead.");
                    if (level != null) {
                        LOG.setLogLevel(level);
                    }
                } else if (strNode.equals("preference")) {
                    String name = xml.getAttributeValue(null, "name");
                    if (name.equals("loglevel")) {
                        LOG.setLogLevel(xml.getAttributeValue(null, "value"));
                    } else if (name.equals("splashscreen")) {
                        String value = xml.getAttributeValue(null, "value");
                        action.getIntent().putExtra(name, action.getResources().getIdentifier(value == null ? "splash" : value, "drawable", action.getPackageName()));
                    } else if (name.equals("backgroundColor")) {
                        action.getIntent().putExtra(name, xml.getAttributeIntValue(null, "value", -16777216));
                    } else if (name.equals("loadUrlTimeoutValue")) {
                        action.getIntent().putExtra(name, xml.getAttributeIntValue(null, "value", 20000));
                    } else if (name.equals("keepRunning")) {
                        action.getIntent().putExtra(name, xml.getAttributeValue(null, "value").equals("true"));
                    } else if (name.equals("InAppBrowserStorageEnabled")) {
                        action.getIntent().putExtra(name, xml.getAttributeValue(null, "value").equals("true"));
                    } else if (name.equals("disallowOverscroll")) {
                        action.getIntent().putExtra(name, xml.getAttributeValue(null, "value").equals("true"));
                    } else {
                        action.getIntent().putExtra(name, xml.getAttributeValue(null, "value"));
                    }
                } else if (strNode.equals("content")) {
                    String src = xml.getAttributeValue(null, "src");
                    LOG.i("CordovaLog", "Found start page location: %s", src);
                    if (src != null) {
                        if (Pattern.compile("^[a-z-]+://").matcher(src).find()) {
                            this.startUrl = src;
                        } else {
                            this.startUrl = "file:///android_asset/www/" + (src.charAt(0) == '/' ? src.substring(1) : src);
                        }
                    }
                }
            }
            try {
                eventType = xml.next();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void addWhiteListEntry(String origin, boolean subdomains) {
        if (self != null) {
            self._addWhiteListEntry(origin, subdomains);
        }
    }

    private void _addWhiteListEntry(String origin, boolean subdomains) {
        try {
            if (origin.compareTo("*") == 0) {
                LOG.d(TAG, "Unlimited access to network resources");
                this.whiteList.add(Pattern.compile(".*"));
                return;
            }
            Matcher matcher = Pattern.compile("^[a-z-]+://").matcher(origin);
            if (subdomains) {
                if (origin.startsWith("http")) {
                    this.whiteList.add(Pattern.compile(origin.replaceFirst("https?://", "^https?://(.*\\.)?")));
                } else if (matcher.find()) {
                    this.whiteList.add(Pattern.compile("^" + origin.replaceFirst("//", "//(.*\\.)?")));
                } else {
                    this.whiteList.add(Pattern.compile("^https?://(.*\\.)?" + origin));
                }
                LOG.d(TAG, "Origin to allow with subdomains: %s", origin);
                return;
            }
            if (origin.startsWith("http")) {
                this.whiteList.add(Pattern.compile(origin.replaceFirst("https?://", "^https?://")));
            } else if (matcher.find()) {
                this.whiteList.add(Pattern.compile("^" + origin));
            } else {
                this.whiteList.add(Pattern.compile("^https?://" + origin));
            }
            LOG.d(TAG, "Origin to allow: %s", origin);
        } catch (Exception e) {
            LOG.d(TAG, "Failed to add origin %s", origin);
        }
    }

    public static boolean isUrlWhiteListed(String url) {
        if (self == null) {
            return false;
        }
        if (self.whiteListCache.get(url) != null) {
            return true;
        }
        Iterator<Pattern> pit = self.whiteList.iterator();
        while (pit.hasNext()) {
            if (pit.next().matcher(url).find()) {
                self.whiteListCache.put(url, true);
                return true;
            }
        }
        return false;
    }

    public static String getStartUrl() {
        if (self == null || self.startUrl == null) {
            return "file:///android_asset/www/index.html";
        }
        return self.startUrl;
    }
}
