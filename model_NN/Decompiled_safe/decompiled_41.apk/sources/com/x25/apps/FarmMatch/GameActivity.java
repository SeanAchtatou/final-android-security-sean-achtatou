package com.x25.apps.FarmMatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.mobclick.android.MobclickAgent;
import com.mobclick.android.ReportPolicy;
import com.waps.AnimationType;

public class GameActivity extends Activity {
    public static final String MYPREFS = "X25";
    private Ads ads;
    private GameView gameView;
    public Sound music;
    /* access modifiers changed from: private */
    public Store store;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        MobclickAgent.onError(this);
        Display display = getWindowManager().getDefaultDisplay();
        Bundle extras = getIntent().getExtras();
        this.gameView = new GameView(this, display.getWidth(), display.getHeight(), extras.getInt("difficulty"));
        this.gameView.totalScore = extras.getInt("totalScore");
        this.gameView.pass = extras.getInt("pass");
        if (this.gameView.pass >= 20) {
            this.gameView.pass = 19;
        }
        GameView gameView2 = this.gameView;
        GameView gameView3 = this.gameView;
        int i = this.gameView.progressTime - (this.gameView.pass * 5000);
        gameView3.curProgress = i;
        gameView2.progressTime = i;
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(1);
        layout.setBackgroundColor(-16711681);
        LinearLayout pbLayout = new LinearLayout(this);
        this.gameView.pb = new ProgressBar(this, null, 16842872);
        this.gameView.pb.setLayoutParams(new LinearLayout.LayoutParams(-1, dipToPx(18)));
        this.gameView.pb.setMax(this.gameView.progressTime);
        this.gameView.pb.setProgress(this.gameView.curProgress);
        this.gameView.pb.setSecondaryProgress(0);
        pbLayout.addView(this.gameView.pb);
        layout.addView(pbLayout);
        LinearLayout gameLayout = new LinearLayout(this);
        gameLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        gameLayout.addView(this.gameView);
        layout.addView(gameLayout);
        LinearLayout ad_layout = new LinearLayout(this);
        ad_layout.setBackgroundResource(R.drawable.bottom);
        ad_layout.addView(new RelativeLayout(this), new RelativeLayout.LayoutParams(-1, -2));
        layout.addView(ad_layout);
        setContentView(layout);
        this.store = new Store(this);
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public int dipToPx(int dip) {
        return (int) ((((float) dip) * getDisplayMetrics().density) + 0.5f);
    }

    public DisplayMetrics getDisplayMetrics() {
        return getResources().getDisplayMetrics();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.gameView.removeCallbacks(this.gameView);
        this.music.destroyBg();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        loadPreferences();
        this.gameView.run();
        this.music = new Sound(this);
        if (this.store.load("music") == 0) {
            this.music.playBg();
        }
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, (int) R.string.newgame).setIcon((int) R.drawable.restart);
        menu.add(0, 1, 1, (int) R.string.pauseorplay).setIcon((int) R.drawable.pause);
        menu.add(0, 2, 2, (int) R.string.setting).setIcon((int) R.drawable.music);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case AnimationType.RANDOM:
                Intent intent = new Intent(this, GameActivity.class);
                intent.setFlags(67108864);
                intent.putExtra("difficulty", this.gameView.difficulty);
                intent.putExtra("pass", 1);
                intent.putExtra("totalScore", 0);
                startActivity(intent);
                finish();
                return false;
            case 1:
                int i = this.gameView.gameState;
                this.gameView.getClass();
                if (i == 0) {
                    GameView gameView2 = this.gameView;
                    this.gameView.getClass();
                    gameView2.toState(2);
                } else {
                    GameView gameView3 = this.gameView;
                    this.gameView.getClass();
                    gameView3.toState(0);
                }
                return false;
            case ReportPolicy.BATCH_AT_TERMINATE:
                int isMusic = this.store.load("music");
                int isEffect = this.store.load("effect");
                boolean musicOn = true;
                boolean effectOn = true;
                if (isMusic == 1) {
                    musicOn = false;
                }
                if (isEffect == 1) {
                    effectOn = false;
                }
                new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setMultiChoiceItems(new String[]{getResources().getString(R.string.music), getResources().getString(R.string.sound)}, new boolean[]{musicOn, effectOn}, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (which == 0) {
                            GameActivity.this.music.switchBg();
                            if (isChecked) {
                                GameActivity.this.store.save("music", 0);
                            } else {
                                GameActivity.this.store.save("music", 1);
                            }
                        } else if (isChecked) {
                            GameActivity.this.store.save("effect", 0);
                        } else {
                            GameActivity.this.store.save("effect", 1);
                        }
                    }
                }).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
                return false;
            default:
                return false;
        }
    }

    public void loadPreferences() {
        this.gameView.topScore = this.store.load("score" + this.gameView.difficulty);
    }
}
