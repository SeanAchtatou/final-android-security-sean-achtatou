package com.x25.apps.FarmMatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.mobclick.android.ReportPolicy;
import com.mt.airad.AirAD;
import com.waps.AnimationType;
import java.lang.reflect.Array;
import java.util.Random;

public class GameView extends View implements Runnable {
    public static int SCREEN_H = 455;
    public static int SCREEN_W = 320;
    int BODY_H = 9;
    final int BODY_W = 8;
    final int ST_PAUSE = 0;
    final int ST_PLAYING = 2;
    final int ST_STOP = 1;
    private int awardRate = 2;
    private int awardScore = 0;
    int beginDrawX;
    int beginDrawY;
    private Bitmap bg;
    private Bitmap[] block;
    int blockCount = 10;
    private Bitmap bmTop;
    int[][] body = ((int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H));
    Bomb bomb;
    final int caseWidth = 40;
    int[][] clear = ((int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H));
    int clearFrame;
    final int clearFrameMax = 8;
    final int clearW = 30;
    Context context;
    int curProgress = 100000;
    int currentX = -1;
    int currentY = -1;
    private Bitmap cursor1;
    private Bitmap cursor2;
    final int cursorW = 36;
    private int delay = 10;
    int difficulty = 0;
    int gameScore;
    int gameState = 2;
    final int goalScore = 2000;
    int iDisScore;
    private Bitmap imgNum01;
    private Bitmap imgScore;
    private Bitmap imgTopScore;
    boolean isClear;
    boolean isDown = true;
    boolean isExchange;
    boolean isReExchange;
    boolean isSelected;
    int moveFrame;
    final int moveFrameMax = 8;
    final int moveSpeed = 5;
    Paint paint = new Paint();
    int pass = 1;
    public ProgressBar pb;
    private Bitmap[] penguin = new Bitmap[10];
    int progressTime = 100000;
    Random random = new Random();
    int scoreSpace;
    int selectedX;
    int selectedY;
    private Store store;
    int[][] tempMove = ((int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H));
    int topScore;
    int topSpace;
    int totalScore;

    public GameView(Context con, int w, int h, int diff) {
        super(con);
        this.context = con;
        setFocusable(true);
        this.paint.setColor(-7829368);
        this.paint.setTextSize(22.0f);
        this.difficulty = diff;
        SCREEN_W = w;
        SCREEN_H = h;
        this.beginDrawX = (SCREEN_W - 320) >> 1;
        this.beginDrawY = (SCREEN_H - (this.BODY_H * 40)) >> 1;
        this.beginDrawY -= 10;
        loadFP();
        this.bomb = new Bomb(this.block, SCREEN_W, SCREEN_H);
        this.store = new Store(con);
    }

    public Bitmap createImage(Drawable tile, int w, int h) {
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        tile.setBounds(0, 0, w, h);
        tile.draw(canvas);
        return bitmap;
    }

    public void loadFP() {
        Resources r = getResources();
        this.bg = BitmapFactory.decodeResource(r, R.drawable.bg);
        this.bmTop = BitmapFactory.decodeResource(r, R.drawable.top);
        this.cursor1 = BitmapFactory.decodeResource(r, R.drawable.cursor1);
        this.cursor2 = BitmapFactory.decodeResource(r, R.drawable.cursor2);
        this.imgNum01 = BitmapFactory.decodeResource(r, R.drawable.num01);
        this.imgScore = BitmapFactory.decodeResource(r, R.drawable.imgscore);
        this.scoreSpace = this.imgScore.getWidth();
        this.imgTopScore = BitmapFactory.decodeResource(r, R.drawable.topscore);
        this.topSpace = this.imgTopScore.getWidth();
        this.penguin[0] = BitmapFactory.decodeResource(r, R.drawable.na);
        this.penguin[1] = BitmapFactory.decodeResource(r, R.drawable.nb);
        this.penguin[2] = BitmapFactory.decodeResource(r, R.drawable.nc);
        this.penguin[3] = BitmapFactory.decodeResource(r, R.drawable.nd);
        this.penguin[4] = BitmapFactory.decodeResource(r, R.drawable.ne);
        this.penguin[5] = BitmapFactory.decodeResource(r, R.drawable.nf);
        this.penguin[6] = BitmapFactory.decodeResource(r, R.drawable.ng);
        this.penguin[7] = BitmapFactory.decodeResource(r, R.drawable.nh);
        this.penguin[8] = BitmapFactory.decodeResource(r, R.drawable.ni);
        this.penguin[9] = BitmapFactory.decodeResource(r, R.drawable.nj);
        this.blockCount = (this.difficulty - 1) + 7;
        this.block = new Bitmap[this.blockCount];
        for (int i = 0; i < this.blockCount; i++) {
            this.block[i] = this.penguin[i];
        }
    }

    public void toState(int state) {
        this.gameState = state;
    }

    public void logic() {
        this.bomb.move();
        if (this.iDisScore < this.gameScore) {
            this.iDisScore += ((this.gameScore - this.iDisScore) / 2) + 1;
        }
        if (this.isClear) {
            this.clearFrame++;
            if (this.clearFrame >= 8) {
                this.clearFrame = 0;
                this.isClear = false;
                this.isDown = doDown();
            }
        }
        if (this.isDown || this.isExchange || this.isReExchange) {
            this.moveFrame++;
        }
        if (this.isDown && this.moveFrame >= 8) {
            this.moveFrame = 0;
            this.isDown = doDown();
            if (!this.isDown) {
                this.isClear = checkClear();
            }
        }
        if (this.isExchange && this.moveFrame >= 8) {
            this.moveFrame = 0;
            this.isExchange = false;
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
            this.isClear = checkClear();
            if (!this.isClear) {
                this.isReExchange = true;
                doExchange();
            }
        }
        if (this.isReExchange && this.moveFrame >= 8) {
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
            this.moveFrame = 0;
            this.isReExchange = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        switch (this.gameState) {
            case ReportPolicy.BATCH_AT_TERMINATE:
                paintPlaying(canvas);
                return;
            default:
                return;
        }
    }

    private void paintPlaying(Canvas canvas) {
        Bitmap bitmap;
        canvas.drawColor(-1);
        canvas.clipRect(0, 0, SCREEN_W, SCREEN_H);
        canvas.drawBitmap(this.bmTop, 0.0f, 0.0f, this.paint);
        canvas.drawBitmap(this.bmTop, 0.0f, (float) (SCREEN_H - this.bmTop.getHeight()), this.paint);
        canvas.drawBitmap(this.bg, (float) this.beginDrawX, (float) this.beginDrawY, this.paint);
        for (int i = 0; i < this.tempMove.length; i++) {
            for (int j = 0; j < this.tempMove[i].length; j++) {
                switch (this.tempMove[i][j]) {
                    case 1:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case ReportPolicy.BATCH_AT_TERMINATE:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case ReportPolicy.PUSH:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 4:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), j * 40);
                        break;
                    case AnimationType.ALPHA:
                    default:
                        if (this.clear[i][j] != 0) {
                            break;
                        } else {
                            paintBlock(canvas, this.body[i][j], i * 40, j * 40);
                            break;
                        }
                    case AnimationType.TRANSLATE_FROM_RIGHT:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), j * 40);
                        break;
                    case AnimationType.TRANSLATE_FROM_LEFT:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case AirAD.ANIMATION_FIXED:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 9:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                }
            }
        }
        canvas.drawBitmap(this.imgTopScore, 20.0f, 0.0f, this.paint);
        paintNumber(canvas, this.topScore, this.topSpace + 20, 10, 10, 16);
        canvas.drawBitmap(this.imgScore, 180.0f, 0.0f, this.paint);
        paintNumber(canvas, this.iDisScore + this.totalScore, this.scoreSpace + 180, 10, 10, 16);
        this.paint.setTextSize(16.0f);
        this.paint.setColor(-65536);
        canvas.drawText(String.valueOf(this.context.getResources().getString(R.string.level_1)) + " " + this.pass + " " + this.context.getResources().getString(R.string.level_2), 130.0f, 40.0f, this.paint);
        if (this.isSelected) {
            bitmap = this.cursor1;
        } else {
            bitmap = this.cursor2;
        }
        canvas.drawBitmap(bitmap, (float) (this.beginDrawX + (this.currentX * 40)), (float) (this.beginDrawY + (this.currentY * 40)), this.paint);
        this.bomb.paint(canvas, this.paint);
        if (this.awardScore > 0) {
            LinearLayout linearLayout = new LinearLayout(this.context);
            linearLayout.setGravity(1);
            linearLayout.setOrientation(0);
            ImageView add = new ImageView(this.context);
            add.setImageResource(R.drawable.red_add);
            linearLayout.addView(add);
            char[] awardChar = new StringBuilder(String.valueOf(this.awardScore)).toString().toCharArray();
            for (int i2 = 0; i2 < awardChar.length; i2++) {
                ImageView imageView = new ImageView(this.context);
                imageView.setImageResource(this.context.getResources().getIdentifier("red_" + awardChar[i2], "drawable", this.context.getPackageName()));
                linearLayout.addView(imageView);
            }
            Toast toast = new Toast(this.context);
            toast.setGravity(16, 0, 0);
            toast.setDuration(0);
            toast.setView(linearLayout);
            toast.show();
            this.awardScore = 0;
        }
    }

    private boolean doDown() {
        boolean isFull = false;
        for (int i = 0; i < this.body.length; i++) {
            int j = this.body[i].length - 1;
            while (true) {
                if (j < 0) {
                    break;
                }
                this.tempMove[i][j] = 0;
                if (this.body[i][j] == 0) {
                    isFull = true;
                    for (int k = j; k >= 0; k--) {
                        this.tempMove[i][k] = 2;
                        if (k == 0) {
                            this.body[i][k] = Math.abs(this.random.nextInt() % this.blockCount) + 1;
                        } else {
                            this.body[i][k] = this.body[i][k - 1];
                        }
                    }
                } else {
                    j--;
                }
            }
        }
        return isFull;
    }

    private void doExchange() {
        if (this.currentX - this.selectedX == -1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 9;
                this.tempMove[this.selectedX][this.selectedY] = 1;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 6;
                this.tempMove[this.selectedX][this.selectedY] = 4;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 3;
                this.tempMove[this.selectedX][this.selectedY] = 7;
            }
        } else if (this.currentX - this.selectedX == 0) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 8;
                this.tempMove[this.selectedX][this.selectedY] = 2;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 2;
                this.tempMove[this.selectedX][this.selectedY] = 8;
            }
        } else if (this.currentX - this.selectedX == 1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 7;
                this.tempMove[this.selectedX][this.selectedY] = 3;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 4;
                this.tempMove[this.selectedX][this.selectedY] = 6;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 1;
                this.tempMove[this.selectedX][this.selectedY] = 9;
            }
        }
        int temp = this.body[this.selectedX][this.selectedY];
        this.body[this.selectedX][this.selectedY] = this.body[this.currentX][this.currentY];
        this.body[this.currentX][this.currentY] = temp;
    }

    /* Debug info: failed to restart local var, previous not found, register: 14 */
    private boolean checkClear() {
        for (int i = 0; i < this.body.length; i++) {
            for (int j = 0; j < this.body[i].length; j++) {
                if (j > 0 && j < this.body[i].length - 1 && this.body[i][j] == this.body[i][j - 1] && this.body[i][j] == this.body[i][j + 1]) {
                    int[] iArr = this.clear[i];
                    this.clear[i][j + 1] = 1;
                    this.clear[i][j - 1] = 1;
                    iArr[j] = 1;
                }
                if (i > 0 && i < this.body.length - 1 && this.body[i][j] == this.body[i - 1][j] && this.body[i][j] == this.body[i + 1][j]) {
                    int[] iArr2 = this.clear[i];
                    int[] iArr3 = this.clear[i - 1];
                    this.clear[i + 1][j] = 1;
                    iArr3[j] = 1;
                    iArr2[j] = 1;
                }
            }
        }
        boolean clearBlock = false;
        int clearNum = 0;
        this.awardScore = 0;
        for (int i2 = 0; i2 < 8; i2++) {
            for (int j2 = 0; j2 < this.BODY_H; j2++) {
                if (this.clear[i2][j2] == 1) {
                    clearBlock = true;
                    this.bomb.add(this.beginDrawX + (i2 * 40), this.beginDrawY + (j2 * 40), this.body[i2][j2] - 1);
                    int[] iArr4 = this.clear[i2];
                    this.body[i2][j2] = 0;
                    iArr4[j2] = 0;
                    this.gameScore += 10;
                    clearNum++;
                }
            }
        }
        if (clearNum > 3) {
            for (int scoreNum = 1; scoreNum <= clearNum - 3; scoreNum++) {
                this.awardScore += this.awardRate * scoreNum;
            }
        }
        this.gameScore += this.awardScore;
        if (this.store.load("effect") == 0) {
            if (clearBlock) {
                ((GameActivity) this.context).music.yes();
            } else {
                ((GameActivity) this.context).music.no();
            }
        }
        return clearBlock;
    }

    private void paintBlock(Canvas canvas, int index, int x, int y) {
        int index2 = index - 1;
        if (index2 >= 0 && index2 < this.block.length) {
            canvas.save();
            canvas.clipRect(this.beginDrawX, this.beginDrawY, this.beginDrawX + 320, this.beginDrawY + (this.BODY_H * 40));
            canvas.drawBitmap(this.block[index2], (float) (this.beginDrawX + x), (float) (this.beginDrawY + y), this.paint);
            canvas.restore();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case AnimationType.RANDOM:
                if (!this.isDown && !this.isExchange && !this.isReExchange && this.moveFrame == 0) {
                    if (event.getX() > ((float) this.beginDrawX) && event.getX() < ((float) (this.beginDrawX + 320)) && event.getY() > ((float) this.beginDrawY) && event.getY() < ((float) (this.beginDrawY + (this.BODY_H * 40)))) {
                        if (this.store.load("effect") == 0) {
                            ((GameActivity) this.context).music.click();
                        }
                        if (!this.isSelected) {
                            this.isSelected = true;
                            this.currentX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            this.currentY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            this.selectedX = this.currentX;
                            this.selectedY = this.currentY;
                            break;
                        } else {
                            int tempX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            int tempY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            if (tempX <= this.selectedX) {
                                if (tempX != this.selectedX) {
                                    if (tempX < this.selectedX) {
                                        if (tempY <= this.selectedY) {
                                            if (tempY != this.selectedY) {
                                                if (tempY < this.selectedY) {
                                                    moveLeftUp();
                                                    break;
                                                }
                                            } else {
                                                moveLeft();
                                                break;
                                            }
                                        } else {
                                            moveLeftDown();
                                            break;
                                        }
                                    }
                                } else if (tempY <= this.selectedY) {
                                    if (tempY != this.selectedY) {
                                        if (tempY < this.selectedY) {
                                            moveUp();
                                            break;
                                        }
                                    } else {
                                        this.isSelected = false;
                                        break;
                                    }
                                } else {
                                    moveDown();
                                    break;
                                }
                            } else if (tempY <= this.selectedY) {
                                if (tempY != this.selectedY) {
                                    if (tempY < this.selectedY) {
                                        moveRightUp();
                                        break;
                                    }
                                } else {
                                    moveRight();
                                    break;
                                }
                            } else {
                                moveRightDown();
                                break;
                            }
                        }
                    }
                } else {
                    return super.onTouchEvent(event);
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (this.isDown || this.isExchange || this.isReExchange || this.moveFrame != 0) {
            return super.onKeyDown(keyCode, msg);
        }
        if (keyCode == 23) {
            if (this.isDown || this.isExchange || this.isReExchange) {
                return super.onKeyDown(keyCode, msg);
            }
            this.isSelected = !this.isSelected;
            this.selectedX = this.currentX;
            this.selectedY = this.currentY;
        } else if (this.isExchange || this.isReExchange) {
            return super.onKeyDown(keyCode, msg);
        } else {
            if (keyCode == 19) {
                moveUp();
            } else if (keyCode == 20) {
                moveDown();
            } else if (keyCode == 21) {
                moveLeft();
            } else if (keyCode == 22) {
                moveRight();
            }
        }
        return super.onKeyDown(keyCode, msg);
    }

    public void setExchange() {
        if (this.isSelected) {
            this.isExchange = true;
            this.isSelected = false;
            doExchange();
        }
    }

    private void moveRightDown() {
        if (this.currentX == 7 || this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % this.BODY_H;
        setExchange();
    }

    private void moveLeftDown() {
        if (this.currentX == 0 || this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % this.BODY_H;
        setExchange();
    }

    private void moveRightUp() {
        if (this.currentY == 0 || this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveLeftUp() {
        if (this.currentX == 0 || this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveUp() {
        if (this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentY - 1;
        this.currentY = i;
        this.currentY = (i + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveDown() {
        if (this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentY + 1;
        this.currentY = i;
        this.currentY = i % this.BODY_H;
        setExchange();
    }

    private void moveLeft() {
        if (this.currentX == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        setExchange();
    }

    private void moveRight() {
        if (this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        setExchange();
    }

    private void paintNumber(Canvas canvas, int num, int x, int y, int w, int h) {
        String tmpStr = new StringBuilder().append(Math.abs(num)).toString();
        for (int i = 0; i < tmpStr.length(); i++) {
            canvas.save();
            canvas.clipRect((i * w) + x, y, ((i + 1) * w) + x, y + h);
            canvas.drawBitmap(this.imgNum01, (float) (((i - Integer.parseInt(new StringBuilder(String.valueOf(tmpStr.charAt(i))).toString())) * w) + x), (float) y, this.paint);
            canvas.restore();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void run() {
        String result;
        if (this.gameState == 2) {
            int isEffect = this.store.load("effect");
            this.curProgress -= this.delay;
            if (this.curProgress <= 0) {
                toState(1);
                this.totalScore += this.gameScore;
                String result2 = this.context.getResources().getString(R.string.gameover);
                if (this.totalScore > this.topScore) {
                    this.store.save("score" + this.difficulty, this.totalScore);
                    result = String.valueOf(result2) + this.context.getResources().getString(R.string.text_1) + this.totalScore + this.context.getResources().getString(R.string.text_2);
                } else {
                    result = String.valueOf(result2) + this.context.getResources().getString(R.string.text_3) + this.totalScore + this.context.getResources().getString(R.string.text_4) + this.topScore + this.context.getResources().getString(R.string.text_5);
                }
                new AlertDialog.Builder(this.context).setTitle((int) R.string.app_name).setMessage(result).setPositiveButton((int) R.string.restart, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Intent intent = new Intent(GameView.this.context, GameActivity.class);
                        intent.setFlags(67108864);
                        intent.putExtra("difficulty", GameView.this.difficulty);
                        intent.putExtra("pass", 1);
                        intent.putExtra("totalScore", 0);
                        GameView.this.context.startActivity(intent);
                        ((Activity) GameView.this.context).finish();
                    }
                }).setNeutralButton((int) R.string.close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((Activity) GameView.this.context).finish();
                    }
                }).show();
                if (isEffect == 0) {
                    ((GameActivity) this.context).music.fail();
                }
            } else if (this.gameScore >= 2000) {
                this.totalScore += this.gameScore;
                new AlertDialog.Builder(this.context).setTitle((int) R.string.app_name).setMessage(String.valueOf(this.context.getResources().getString(R.string.text_6)) + this.pass + this.context.getResources().getString(R.string.text_7)).setPositiveButton((int) R.string.next, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Intent intent = new Intent(GameView.this.context, GameActivity.class);
                        intent.setFlags(67108864);
                        intent.putExtra("difficulty", GameView.this.difficulty);
                        GameView gameView = GameView.this;
                        int i2 = gameView.pass + 1;
                        gameView.pass = i2;
                        intent.putExtra("pass", i2);
                        intent.putExtra("totalScore", GameView.this.totalScore);
                        GameView.this.context.startActivity(intent);
                        ((Activity) GameView.this.context).finish();
                    }
                }).setNeutralButton((int) R.string.close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ((Activity) GameView.this.context).finish();
                    }
                }).show();
                if (isEffect == 0) {
                    ((GameActivity) this.context).music.pass();
                }
            } else {
                logic();
                invalidate();
                postDelayed(this, (long) this.delay);
                this.pb.setProgress(this.curProgress);
            }
        } else {
            invalidate();
            postDelayed(this, (long) this.delay);
        }
    }
}
