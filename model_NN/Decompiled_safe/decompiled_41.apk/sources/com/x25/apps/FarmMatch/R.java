package com.x25.apps.FarmMatch;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bg = 2130837504;
        public static final int bottom = 2130837505;
        public static final int button = 2130837506;
        public static final int button_bg = 2130837507;
        public static final int button_bg_sel = 2130837508;
        public static final int cursor1 = 2130837509;
        public static final int cursor2 = 2130837510;
        public static final int icon = 2130837511;
        public static final int imgscore = 2130837512;
        public static final int main_bg = 2130837513;
        public static final int music = 2130837514;
        public static final int na = 2130837515;
        public static final int nb = 2130837516;
        public static final int nc = 2130837517;
        public static final int nd = 2130837518;
        public static final int ne = 2130837519;
        public static final int nf = 2130837520;
        public static final int ng = 2130837521;
        public static final int nh = 2130837522;
        public static final int ni = 2130837523;
        public static final int nj = 2130837524;
        public static final int num01 = 2130837525;
        public static final int pause = 2130837526;
        public static final int red_0 = 2130837527;
        public static final int red_1 = 2130837528;
        public static final int red_2 = 2130837529;
        public static final int red_3 = 2130837530;
        public static final int red_4 = 2130837531;
        public static final int red_5 = 2130837532;
        public static final int red_6 = 2130837533;
        public static final int red_7 = 2130837534;
        public static final int red_8 = 2130837535;
        public static final int red_9 = 2130837536;
        public static final int red_add = 2130837537;
        public static final int res = 2130837538;
        public static final int restart = 2130837539;
        public static final int top = 2130837540;
        public static final int topscore = 2130837541;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131099654;
        public static final int OfferProgressBar = 2131099657;
        public static final int RelativeLayout01 = 2131099655;
        public static final int app = 2131099658;
        public static final int appIcon = 2131099659;
        public static final int description = 2131099663;
        public static final int easy = 2131099648;
        public static final int exit = 2131099653;
        public static final int hard = 2131099650;
        public static final int help = 2131099652;
        public static final int medium = 2131099649;
        public static final int notification = 2131099661;
        public static final int offersWebView = 2131099656;
        public static final int progress_bar = 2131099664;
        public static final int progress_text = 2131099660;
        public static final int setting = 2131099651;
        public static final int title = 2131099662;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int offers_web_view = 2130903041;
        public static final int umeng_download_notification = 2130903042;
    }

    public static final class raw {
        public static final int about = 2130968576;
        public static final int click = 2130968577;
        public static final int fail = 2130968578;
        public static final int music = 2130968579;
        public static final int no = 2130968580;
        public static final int pass = 2130968581;
        public static final int yes = 2130968582;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int close = 2131034121;
        public static final int easy = 2131034115;
        public static final int exit = 2131034120;
        public static final int gameover = 2131034130;
        public static final int hard = 2131034117;
        public static final int help = 2131034118;
        public static final int help_content = 2131034138;
        public static final int lang = 2131034112;
        public static final int level_1 = 2131034128;
        public static final int level_2 = 2131034129;
        public static final int medium = 2131034116;
        public static final int music = 2131034126;
        public static final int newgame = 2131034124;
        public static final int next = 2131034114;
        public static final int ok = 2131034122;
        public static final int pauseorplay = 2131034125;
        public static final int restart = 2131034123;
        public static final int setting = 2131034119;
        public static final int sound = 2131034127;
        public static final int text_1 = 2131034131;
        public static final int text_2 = 2131034132;
        public static final int text_3 = 2131034133;
        public static final int text_4 = 2131034134;
        public static final int text_5 = 2131034135;
        public static final int text_6 = 2131034136;
        public static final int text_7 = 2131034137;
    }
}
