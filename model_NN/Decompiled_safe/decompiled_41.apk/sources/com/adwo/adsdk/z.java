package com.adwo.adsdk;

import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;

final class z extends Thread {
    /* access modifiers changed from: private */
    public /* synthetic */ AdwoAdView a;

    z(AdwoAdView adwoAdView) {
        this.a = adwoAdView;
    }

    public final void run() {
        try {
            Context context = this.a.getContext();
            C0007f a2 = C0010i.a(context, AdwoAdView.n, (byte) 0);
            if (a2 != null) {
                synchronized (this) {
                    if (this.a.c == null || !a2.equals(this.a.c.d())) {
                        boolean z = this.a.c == null;
                        int c = z.super.getVisibility();
                        C0003b bVar = new C0003b(a2, context, AdwoAdView.a(), AdwoAdView.b(), (double) this.a.getResources().getDisplayMetrics().density);
                        bVar.setBackgroundColor(this.a.e());
                        bVar.a(this.a.d());
                        bVar.setVisibility(c);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(AdwoAdView.a(), AdwoAdView.b());
                        layoutParams.addRule(14);
                        bVar.setLayoutParams(layoutParams);
                        if (this.a.i != null) {
                            try {
                                this.a.i.onReceiveAd(this.a);
                            } catch (Exception e) {
                            }
                        }
                        AdwoAdView.k.post(new A(this, bVar, c, z));
                    } else {
                        this.a.a = false;
                        if (this.a.i != null) {
                            try {
                                this.a.i.onFailedToReceiveRefreshedAd(this.a);
                            } catch (Exception e2) {
                            }
                        }
                    }
                }
                return;
            }
            if (this.a.i != null) {
                try {
                    this.a.i.onFailedToReceiveAd(this.a);
                } catch (Exception e3) {
                    Log.w("Adwo SDK", e3.toString());
                }
            }
            this.a.a = false;
        } catch (Exception e4) {
            e4.printStackTrace();
            this.a.a = false;
        }
    }
}
