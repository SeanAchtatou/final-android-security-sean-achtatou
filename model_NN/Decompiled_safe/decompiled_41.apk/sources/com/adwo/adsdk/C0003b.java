package com.adwo.adsdk;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/* renamed from: com.adwo.adsdk.b  reason: case insensitive filesystem */
final class C0003b extends RelativeLayout implements Animation.AnimationListener, C0009h {
    private static final Typeface a = Typeface.create(Typeface.SANS_SERIF, 1);
    private static final Typeface b = Typeface.create(Typeface.SANS_SERIF, 0);
    private int c = -16777216;
    private int d = -1;
    private Drawable e;
    private Drawable f;
    private Drawable g;
    private Drawable h;
    /* access modifiers changed from: private */
    public C0007f i;
    private TextView j;
    private TextView k;
    /* access modifiers changed from: private */
    public R l = null;
    private ImageView m = null;
    /* access modifiers changed from: private */
    public ProgressBar n;
    /* access modifiers changed from: private */
    public boolean o;
    private C0002a p;

    private synchronized byte[] a(String str) {
        byte[] bArr;
        String lowerCase = str.substring(str.lastIndexOf("/") + 1, str.lastIndexOf(".")).toLowerCase();
        try {
            URL url = new URL(str);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestProperty("User-Agent", C0010i.g);
                if (C0010i.a != null) {
                    httpURLConnection.setRequestProperty("Cookie", C0010i.a);
                }
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                while (true) {
                    int read = inputStream.read();
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(read);
                }
                inputStream.close();
                bArr = byteArrayOutputStream.toByteArray();
                if (bArr == null || bArr.length <= 0) {
                    bArr = null;
                } else {
                    try {
                        this.p.a(lowerCase, bArr);
                    } catch (Exception e2) {
                    }
                }
            } catch (IOException e3) {
                bArr = null;
            }
        } catch (MalformedURLException e4) {
            bArr = null;
        }
        return bArr;
    }

    public final void a() {
        g();
        if (!(this.l == null || this.l.getBackground() == null)) {
            this.l.getBackground().setCallback(null);
            this.l.setBackgroundDrawable(null);
        }
        if (this.m != null && this.m.getBackground() != null) {
            this.m.getBackground().setCallback(null);
            this.m.setBackgroundDrawable(null);
        }
    }

    private void g() {
        if (this.e != null) {
            ((BitmapDrawable) this.e).getBitmap().recycle();
        }
        if (this.f != null) {
            ((BitmapDrawable) this.f).getBitmap().recycle();
        }
        if (this.g != null) {
            ((BitmapDrawable) this.g).getBitmap().recycle();
        }
        this.e = null;
        this.g = null;
        this.f = null;
        if (this.h != null) {
            ((BitmapDrawable) this.h).getBitmap().recycle();
        }
        this.h = null;
    }

    public final void b() {
        if (this.l != null) {
            this.l.a();
        }
    }

    public final void c() {
        if (this.l != null) {
            this.l.b();
            if (this.l.getBackground() != null) {
                this.l.getBackground().setCallback(null);
                this.l.setBackgroundDrawable(null);
            }
            this.l = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        r3 = (r13 - r4) / 2;
        r5.setMargins(r3 / 2, r3, 0, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x033b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x033c, code lost:
        r8 = r1;
        r1 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02ba, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        r1.printStackTrace();
        r9.l = null;
        r1 = android.graphics.BitmapFactory.decodeStream(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x02c5, code lost:
        if (r1 == null) goto L_0x02c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x02c7, code lost:
        r1 = android.graphics.BitmapFactory.decodeStream(getContext().getAssets().open("adwo_logo.png"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x02d9, code lost:
        if (r1 != null) goto L_0x02db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x02db, code lost:
        r3 = r1.getWidth();
        r4 = (int) (((double) r1.getHeight()) * r14);
        r5 = new android.widget.RelativeLayout.LayoutParams((int) (((double) r3) * r14), r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x02ee, code lost:
        if (r2 <= 0) goto L_0x02f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x02f0, code lost:
        r5.setMargins(0, 0, 0, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x02f7, code lost:
        r5.addRule(9);
        r9.m = new android.widget.ImageView(r11);
        r9.m.setLayoutParams(r5);
        r9.m.setBackgroundDrawable(new android.graphics.drawable.BitmapDrawable(r1));
        r9.m.setId(1);
        addView(r9.m);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x031f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0320, code lost:
        r8 = r1;
        r1 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x033b A[ExcHandler: all (r1v70 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:32:0x00db] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0341 A[SYNTHETIC, Splitter:B:107:0x0341] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0405  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x031f A[ExcHandler: Error (r1v72 'e' java.lang.Error A[CUSTOM_DECLARE]), Splitter:B:32:0x00db] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0328 A[SYNTHETIC, Splitter:B:98:0x0328] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:95:0x0323=Splitter:B:95:0x0323, B:77:0x02ad=Splitter:B:77:0x02ad} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C0003b(com.adwo.adsdk.C0007f r10, android.content.Context r11, int r12, int r13, double r14) {
        /*
            r9 = this;
            r9.<init>(r11)
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r9.c = r0
            r0 = -1
            r9.d = r0
            r0 = 0
            r9.l = r0
            r0 = 0
            r9.m = r0
            com.adwo.adsdk.a r0 = r9.p
            if (r0 != 0) goto L_0x001a
            com.adwo.adsdk.a r0 = com.adwo.adsdk.C0002a.a(r11)
            r9.p = r0
        L_0x001a:
            r9.i = r10
            r10.i = r9
            r0 = 0
            r9.e = r0
            r0 = 0
            r9.g = r0
            r0 = 0
            r9.f = r0
            r0 = 0
            r9.n = r0
            r0 = 0
            r9.o = r0
            if (r10 == 0) goto L_0x026d
            r0 = 1
            r9.setFocusable(r0)
            r0 = 1
            r9.setClickable(r0)
            r1 = 0
            r0 = 0
            java.lang.String r2 = r10.b()
            if (r2 == 0) goto L_0x0486
            java.lang.String r0 = r10.b()
            int r0 = r0.length()
            r2 = r0
        L_0x0048:
            java.lang.String r3 = r10.c()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r3 == 0) goto L_0x03ae
            com.adwo.adsdk.R r0 = r9.l     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r0 == 0) goto L_0x0079
            com.adwo.adsdk.R r0 = r9.l     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.Drawable r0 = r0.getBackground()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r0 == 0) goto L_0x0079
            com.adwo.adsdk.R r0 = r9.l     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.Drawable r0 = r0.getBackground()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.Bitmap r0 = r0.getBitmap()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r0.recycle()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            com.adwo.adsdk.R r0 = r9.l     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.Drawable r0 = r0.getBackground()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r4 = 0
            r0.setCallback(r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            com.adwo.adsdk.R r0 = r9.l     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r4 = 0
            r0.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
        L_0x0079:
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r0 == 0) goto L_0x00a4
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.Drawable r0 = r0.getBackground()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r0 == 0) goto L_0x00a4
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.Drawable r0 = r0.getBackground()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.Bitmap r0 = r0.getBitmap()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r0.recycle()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.Drawable r0 = r0.getBackground()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r4 = 0
            r0.setCallback(r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r4 = 0
            r0.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
        L_0x00a4:
            int r0 = r3.length()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r0 == 0) goto L_0x0288
            java.lang.String r0 = "/"
            int r0 = r3.lastIndexOf(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            int r0 = r0 + 1
            java.lang.String r4 = "."
            int r4 = r3.lastIndexOf(r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            java.lang.String r0 = r3.substring(r0, r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            com.adwo.adsdk.a r4 = r9.p     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            boolean r4 = r4.a(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r4 == 0) goto L_0x0277
            com.adwo.adsdk.a r4 = r9.p     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            byte[] r0 = r4.b(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r0 == 0) goto L_0x0299
            int r4 = r0.length     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r4 <= 0) goto L_0x0299
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r0 = r4
        L_0x00d9:
            java.lang.String r1 = ".gif"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            if (r1 != 0) goto L_0x00e9
            java.lang.String r1 = ".GIF"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            if (r1 == 0) goto L_0x0345
        L_0x00e9:
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r1.<init>(r12, r13)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            if (r2 > 0) goto L_0x029c
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r1.setMargins(r3, r4, r5, r6)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
        L_0x00f7:
            r3 = 9
            r1.addRule(r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            com.adwo.adsdk.R r3 = new com.adwo.adsdk.R     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3.<init>(r11)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r9.l = r3     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            com.adwo.adsdk.R r3 = r9.l     // Catch:{ Exception -> 0x02ba, Error -> 0x031f, all -> 0x033b }
            r3.a(r0)     // Catch:{ Exception -> 0x02ba, Error -> 0x031f, all -> 0x033b }
            com.adwo.adsdk.R r3 = r9.l     // Catch:{ Exception -> 0x02ba, Error -> 0x031f, all -> 0x033b }
            r3.setLayoutParams(r1)     // Catch:{ Exception -> 0x02ba, Error -> 0x031f, all -> 0x033b }
            com.adwo.adsdk.R r1 = r9.l     // Catch:{ Exception -> 0x02ba, Error -> 0x031f, all -> 0x033b }
            r3 = 1
            r1.setId(r3)     // Catch:{ Exception -> 0x02ba, Error -> 0x031f, all -> 0x033b }
            com.adwo.adsdk.R r1 = r9.l     // Catch:{ Exception -> 0x02ba, Error -> 0x031f, all -> 0x033b }
            r9.addView(r1)     // Catch:{ Exception -> 0x02ba, Error -> 0x031f, all -> 0x033b }
        L_0x0118:
            if (r0 == 0) goto L_0x011d
            r0.close()     // Catch:{ IOException -> 0x0477 }
        L_0x011d:
            r0 = 15
            if (r2 <= r0) goto L_0x0405
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r11)
            r9.j = r0
            android.widget.TextView r0 = r9.j
            java.lang.String r1 = r10.b()
            r0.setText(r1)
            android.widget.TextView r0 = r9.j
            android.graphics.Typeface r1 = com.adwo.adsdk.C0003b.a
            r0.setTypeface(r1)
            android.widget.TextView r0 = r9.j
            int r1 = r9.d
            r0.setTextColor(r1)
            r0 = 1096810496(0x41600000, float:14.0)
            int r1 = r13 / 5
            r3 = 10
            int r1 = r1 - r3
            float r1 = (float) r1
            float r0 = r0 + r1
            r1 = 60
            if (r13 <= r1) goto L_0x0155
            r0 = 1098907648(0x41800000, float:16.0)
            int r1 = r13 / 5
            r3 = 10
            int r1 = r1 - r3
            float r1 = (float) r1
            float r0 = r0 + r1
        L_0x0155:
            android.widget.TextView r1 = r9.j
            r1.setTextSize(r0)
            android.widget.TextView r0 = r9.j
            android.text.TextUtils$TruncateAt r1 = android.text.TextUtils.TruncateAt.END
            r0.setEllipsize(r1)
            android.widget.TextView r0 = r9.j
            r1 = 1
            r0.setSingleLine(r1)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r1 = -1
            r3 = -1
            r0.<init>(r1, r3)
            android.widget.ImageView r1 = r9.m
            if (r1 == 0) goto L_0x0177
            r1 = 1
            r3 = 1
            r0.addRule(r1, r3)
        L_0x0177:
            double r3 = (double) r13
            r5 = 4613937818241073152(0x4008000000000000, double:3.0)
            double r3 = r3 / r5
            int r1 = (int) r3
            r3 = 10
            int r1 = r1 - r3
            r3 = 6
            r4 = 0
            r5 = 0
            r0.setMargins(r3, r1, r4, r5)
            android.widget.TextView r1 = r9.j
            r1.setLayoutParams(r0)
            android.widget.TextView r0 = r9.j
            r1 = 2
            r0.setId(r1)
            android.widget.TextView r0 = r9.j
            r9.addView(r0)
        L_0x0195:
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r1 = 44
            r3 = 44
            r0.<init>(r1, r3)
            r1 = 44
            int r1 = r13 - r1
            int r1 = r1 / 2
            r3 = 0
            r0.setMargins(r1, r1, r3, r1)
            android.widget.ProgressBar r1 = new android.widget.ProgressBar
            r1.<init>(r11)
            r9.n = r1
            android.widget.ProgressBar r1 = r9.n
            r3 = 1
            r1.setIndeterminate(r3)
            android.widget.ProgressBar r1 = r9.n
            r1.setLayoutParams(r0)
            android.widget.ProgressBar r0 = r9.n
            r1 = 4
            r0.setVisibility(r1)
            android.widget.ProgressBar r0 = r9.n
            r9.addView(r0)
            if (r2 <= 0) goto L_0x026d
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r11)
            r9.k = r0
            android.widget.TextView r0 = r9.k
            r1 = 5
            r0.setGravity(r1)
            android.widget.TextView r0 = r9.k
            android.graphics.Typeface r1 = com.adwo.adsdk.C0003b.b
            r0.setTypeface(r1)
            android.widget.TextView r0 = r9.k
            int r1 = r9.d
            r0.setTextColor(r1)
            android.widget.TextView r0 = r9.k
            r1 = 1092616192(0x41200000, float:10.0)
            r0.setTextSize(r1)
            android.widget.TextView r0 = r9.k
            java.lang.String r1 = "安沃传媒"
            r0.setText(r1)
            android.widget.TextView r0 = r9.k
            r1 = 3
            r0.setId(r1)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r1 = -2
            r2 = -2
            r0.<init>(r1, r2)
            r1 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r1 = r1 * r14
            int r1 = (int) r1
            r2 = 0
            int r1 = r1 * 6
            int r1 = r13 - r1
            r3 = 6
            r4 = 0
            r0.setMargins(r2, r1, r3, r4)
            r1 = 11
            r0.addRule(r1)
            android.widget.TextView r1 = r9.k
            r1.setLayoutParams(r0)
            android.widget.TextView r0 = r9.k
            r9.addView(r0)
            android.widget.ImageView r0 = new android.widget.ImageView
            r0.<init>(r11)
            r1 = 0
            android.content.Context r2 = r9.getContext()     // Catch:{ IOException -> 0x046e }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ IOException -> 0x046e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x046e }
            java.lang.String r4 = "t"
            r3.<init>(r4)     // Catch:{ IOException -> 0x046e }
            byte r4 = r10.f     // Catch:{ IOException -> 0x046e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x046e }
            java.lang.String r4 = ".png"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x046e }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x046e }
            java.io.InputStream r2 = r2.open(r3)     // Catch:{ IOException -> 0x046e }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ IOException -> 0x046e }
        L_0x0247:
            if (r1 == 0) goto L_0x026d
            int r2 = r1.getHeight()
            int r3 = r13 - r2
            int r3 = r3 / 2
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams
            r4.<init>(r2, r2)
            r2 = 0
            r4.setMargins(r3, r3, r2, r3)
            r2 = 11
            r4.addRule(r2)
            r0.setLayoutParams(r4)
            android.graphics.drawable.BitmapDrawable r2 = new android.graphics.drawable.BitmapDrawable
            r2.<init>(r1)
            r0.setBackgroundDrawable(r2)
            r9.addView(r0)
        L_0x026d:
            r0 = -1
            r9.a(r0)
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r9.setBackgroundColor(r0)
            return
        L_0x0277:
            byte[] r0 = r9.a(r3)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r0 == 0) goto L_0x0299
            int r4 = r0.length     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r4 <= 0) goto L_0x0299
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r0 = r4
            goto L_0x00d9
        L_0x0288:
            android.content.Context r0 = r9.getContext()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            java.lang.String r4 = "adwo_logo.png"
            java.io.InputStream r0 = r0.open(r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
        L_0x0299:
            r0 = r1
            goto L_0x00d9
        L_0x029c:
            r3 = 0
            int r3 = r13 - r3
            int r3 = r3 / 2
            int r4 = r3 / 2
            r5 = 0
            r1.setMargins(r4, r3, r5, r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            goto L_0x00f7
        L_0x02a9:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x02ad:
            r0.printStackTrace()     // Catch:{ all -> 0x047a }
            if (r1 == 0) goto L_0x011d
            r1.close()     // Catch:{ IOException -> 0x02b7 }
            goto L_0x011d
        L_0x02b7:
            r0 = move-exception
            goto L_0x011d
        L_0x02ba:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r1 = 0
            r9.l = r1     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            if (r1 != 0) goto L_0x02d9
            android.content.Context r1 = r9.getContext()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            java.lang.String r3 = "adwo_logo.png"
            java.io.InputStream r1 = r1.open(r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
        L_0x02d9:
            if (r1 == 0) goto L_0x0118
            int r3 = r1.getWidth()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            int r4 = r1.getHeight()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            double r5 = (double) r3     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            double r5 = r5 * r14
            int r3 = (int) r5     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            double r4 = (double) r4     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            double r4 = r4 * r14
            int r4 = (int) r4     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.RelativeLayout$LayoutParams r5 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r5.<init>(r3, r4)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            if (r2 > 0) goto L_0x0330
            r3 = 0
            r4 = 0
            r6 = 0
            r7 = 0
            r5.setMargins(r3, r4, r6, r7)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
        L_0x02f7:
            r3 = 9
            r5.addRule(r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r3 = new android.widget.ImageView     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3.<init>(r11)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r9.m = r3     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3.setLayoutParams(r5)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r4.<init>(r1)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r1 = r9.m     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3 = 1
            r1.setId(r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r1 = r9.m     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r9.addView(r1)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            goto L_0x0118
        L_0x031f:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0323:
            r0.printStackTrace()     // Catch:{ all -> 0x047a }
            if (r1 == 0) goto L_0x011d
            r1.close()     // Catch:{ IOException -> 0x032d }
            goto L_0x011d
        L_0x032d:
            r0 = move-exception
            goto L_0x011d
        L_0x0330:
            int r3 = r13 - r4
            int r3 = r3 / 2
            int r4 = r3 / 2
            r6 = 0
            r5.setMargins(r4, r3, r6, r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            goto L_0x02f7
        L_0x033b:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x033f:
            if (r1 == 0) goto L_0x0344
            r1.close()     // Catch:{ IOException -> 0x0474 }
        L_0x0344:
            throw r0
        L_0x0345:
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            if (r1 != 0) goto L_0x035d
            android.content.Context r1 = r9.getContext()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            java.lang.String r3 = "adwo_logo.png"
            java.io.InputStream r1 = r1.open(r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
        L_0x035d:
            if (r1 == 0) goto L_0x0118
            int r3 = r1.getWidth()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            int r4 = r1.getHeight()     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            double r5 = (double) r3     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            double r5 = r5 * r14
            int r3 = (int) r5     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            double r4 = (double) r4     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            double r4 = r4 * r14
            int r4 = (int) r4     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.RelativeLayout$LayoutParams r5 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r5.<init>(r3, r4)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            if (r2 > 0) goto L_0x03a3
            r3 = 0
            r4 = 0
            r6 = 0
            r7 = 0
            r5.setMargins(r3, r4, r6, r7)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
        L_0x037b:
            r3 = 9
            r5.addRule(r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r3 = new android.widget.ImageView     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3.<init>(r11)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r9.m = r3     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3.setLayoutParams(r5)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r4.<init>(r1)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r1 = r9.m     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r3 = 1
            r1.setId(r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            android.widget.ImageView r1 = r9.m     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            r9.addView(r1)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            goto L_0x0118
        L_0x03a3:
            int r3 = r13 - r4
            int r3 = r3 / 2
            int r4 = r3 / 2
            r6 = 0
            r5.setMargins(r4, r3, r6, r3)     // Catch:{ Exception -> 0x02a9, Error -> 0x031f, all -> 0x033b }
            goto L_0x037b
        L_0x03ae:
            if (r2 <= 0) goto L_0x0483
            android.content.Context r0 = r9.getContext()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            java.lang.String r3 = "adwo_logo.png"
            java.io.InputStream r0 = r0.open(r3)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            if (r0 == 0) goto L_0x0483
            int r3 = r0.getWidth()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            int r4 = r0.getHeight()     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            double r5 = (double) r3     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            double r5 = r5 * r14
            int r3 = (int) r5     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            double r4 = (double) r4     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            double r4 = r4 * r14
            int r4 = (int) r4     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.widget.RelativeLayout$LayoutParams r5 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r5.<init>(r3, r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            int r3 = r13 - r4
            int r3 = r3 / 2
            int r4 = r3 / 2
            r6 = 0
            r5.setMargins(r4, r3, r6, r3)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.widget.ImageView r3 = new android.widget.ImageView     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r3.<init>(r11)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r9.m = r3     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r3.setLayoutParams(r5)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.widget.ImageView r3 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r3.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r3 = 1
            r0.setId(r3)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            android.widget.ImageView r0 = r9.m     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r9.addView(r0)     // Catch:{ Exception -> 0x0480, Error -> 0x047d }
            r0 = r1
            goto L_0x0118
        L_0x0405:
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r11)
            r9.j = r0
            android.widget.TextView r0 = r9.j
            java.lang.String r1 = r10.b()
            r0.setText(r1)
            android.widget.TextView r0 = r9.j
            android.graphics.Typeface r1 = com.adwo.adsdk.C0003b.a
            r0.setTypeface(r1)
            android.widget.TextView r0 = r9.j
            int r1 = r9.d
            r0.setTextColor(r1)
            r0 = 1098907648(0x41800000, float:16.0)
            int r1 = r13 / 5
            r3 = 10
            int r1 = r1 - r3
            float r1 = (float) r1
            float r0 = r0 + r1
            r1 = 60
            if (r13 <= r1) goto L_0x0433
            r1 = 1073741824(0x40000000, float:2.0)
            float r0 = r0 + r1
        L_0x0433:
            android.widget.TextView r1 = r9.j
            r1.setTextSize(r0)
            android.widget.TextView r0 = r9.j
            r1 = 1
            r0.setSingleLine(r1)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r1 = -2
            r3 = -2
            r0.<init>(r1, r3)
            android.widget.ImageView r1 = r9.m
            if (r1 == 0) goto L_0x044e
            r1 = 1
            r3 = 1
            r0.addRule(r1, r3)
        L_0x044e:
            double r3 = (double) r13
            r5 = 4613937818241073152(0x4008000000000000, double:3.0)
            double r3 = r3 / r5
            int r1 = (int) r3
            r3 = 10
            int r1 = r1 - r3
            r3 = 6
            r4 = 0
            r5 = 0
            r0.setMargins(r3, r1, r4, r5)
            android.widget.TextView r1 = r9.j
            r1.setLayoutParams(r0)
            android.widget.TextView r0 = r9.j
            r1 = 2
            r0.setId(r1)
            android.widget.TextView r0 = r9.j
            r9.addView(r0)
            goto L_0x0195
        L_0x046e:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0247
        L_0x0474:
            r1 = move-exception
            goto L_0x0344
        L_0x0477:
            r0 = move-exception
            goto L_0x011d
        L_0x047a:
            r0 = move-exception
            goto L_0x033f
        L_0x047d:
            r0 = move-exception
            goto L_0x0323
        L_0x0480:
            r0 = move-exception
            goto L_0x02ad
        L_0x0483:
            r0 = r1
            goto L_0x0118
        L_0x0486:
            r2 = r0
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0003b.<init>(com.adwo.adsdk.f, android.content.Context, int, int, double):void");
    }

    public final void a(int i2) {
        this.d = -16777216 | i2;
        this.j.setTextColor(this.d);
        postInvalidate();
    }

    public final void setBackgroundColor(int i2) {
        this.c = -16777216 | i2;
    }

    /* access modifiers changed from: protected */
    public final C0007f d() {
        return this.i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.b.a(android.graphics.Rect, boolean):android.graphics.drawable.Drawable
     arg types: [android.graphics.Rect, int]
     candidates:
      com.adwo.adsdk.b.a(com.adwo.adsdk.b, boolean):void
      com.adwo.adsdk.b.a(android.graphics.Rect, boolean):android.graphics.drawable.Drawable */
    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Typeface typeface = this.j.getTypeface();
        String b2 = this.i.b();
        if (b2 != null) {
            Paint paint = new Paint();
            paint.setTypeface(typeface);
            paint.setTextSize(this.j.getTextSize());
            paint.measureText(b2);
        }
        if (this.k != null) {
            this.k.setVisibility(0);
        }
        if (i2 != 0 && i3 != 0) {
            try {
                Rect rect = new Rect(0, 0, i2, i3);
                g();
                int i6 = this.c;
                this.e = a(rect, false);
                this.g = a(rect, false);
                int i7 = this.c;
                this.f = a(rect, true);
                setBackgroundDrawable(this.e);
            } catch (Error e2) {
            }
        }
    }

    private static Drawable a(Rect rect, boolean z) {
        Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        if (z) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(-1147097);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(3.0f);
            paint.setPathEffect(new CornerPathEffect(3.0f));
            Path path = new Path();
            path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
            canvas.drawPath(path, paint);
        }
        return new BitmapDrawable(createBitmap);
    }

    public final void e() {
        post(new C0004c(this));
    }

    public final void f() {
        post(new C0005d(this));
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 66 || i2 == 23) {
            h();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                h();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                h();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
        if (z) {
            setBackgroundDrawable(this.f);
        } else {
            setBackgroundDrawable(this.e);
        }
    }

    public final void setPressed(boolean z) {
        Drawable drawable;
        if ((!z || !this.o) && isPressed() != z) {
            Drawable drawable2 = this.e;
            int i2 = this.d;
            if (z) {
                this.h = getBackground();
                drawable = this.g;
                i2 = -16777216;
            } else {
                drawable = this.h;
            }
            setBackgroundDrawable(drawable);
            if (this.j != null) {
                this.j.setTextColor(i2);
            }
            if (this.k != null) {
                this.k.setTextColor(i2);
            }
            super.setPressed(z);
            invalidate();
        }
    }

    private void h() {
        float f2;
        float f3;
        if (this.i != null && isPressed()) {
            setPressed(false);
            if (!this.o) {
                this.o = true;
                if (this.l == null && this.m == null) {
                    this.i.a();
                    this.o = false;
                    return;
                }
                AnimationSet animationSet = new AnimationSet(true);
                float f4 = 20.0f;
                float f5 = 20.0f;
                if (this.l != null) {
                    f4 = ((float) this.l.getWidth()) / 2.0f;
                    f5 = ((float) this.l.getHeight()) / 2.0f;
                    this.l.b();
                }
                float f6 = f5;
                float f7 = f4;
                float f8 = f6;
                if (this.m != null) {
                    f2 = ((float) this.m.getHeight()) / 2.0f;
                    f3 = ((float) this.m.getWidth()) / 2.0f;
                } else {
                    f2 = f8;
                    f3 = f7;
                }
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, f3, f2);
                scaleAnimation.setDuration(200);
                animationSet.addAnimation(scaleAnimation);
                ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, f3, f2);
                scaleAnimation2.setDuration(299);
                scaleAnimation2.setStartOffset(200);
                scaleAnimation2.setAnimationListener(this);
                animationSet.addAnimation(scaleAnimation2);
                postDelayed(new C0006e(this), 500);
                if (this.l != null) {
                    this.l.startAnimation(animationSet);
                }
                if (this.m != null) {
                    this.m.startAnimation(animationSet);
                }
            }
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
    }
}
