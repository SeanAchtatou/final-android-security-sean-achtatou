package com.adwo.adsdk;

import android.content.Context;
import android.util.Log;

/* renamed from: com.adwo.adsdk.f  reason: case insensitive filesystem */
final class C0007f {
    Context a;
    protected int b = -1;
    protected String c = null;
    protected String d = null;
    protected String e = null;
    protected byte f;
    protected String g = null;
    protected String h = null;
    C0009h i;

    public static C0007f a(Context context, byte[] bArr) {
        C0007f a2 = N.a(bArr);
        if (a2 == null) {
            return null;
        }
        a2.a = context;
        if (a2.c == null && a2.e == null) {
            return null;
        }
        if (a2.c != null && a2.c.length() == 0) {
            return null;
        }
        if (a2.e != null && a2.e.length() == 0) {
            return null;
        }
        Log.d("Adwo SDK", "Get an ad from Adwo servers.");
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.d != null) {
            new C0008g(this).start();
        }
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.e;
    }

    public final String toString() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C0007f)) {
            return false;
        }
        C0007f fVar = (C0007f) obj;
        if (this.c != null && fVar.c != null && this.c.equals(fVar.c)) {
            return true;
        }
        if (this.e != null && fVar.e != null && this.e.equals(fVar.e)) {
            return true;
        }
        if (this.h == null || fVar.h == null || !this.h.equals(fVar.h)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
