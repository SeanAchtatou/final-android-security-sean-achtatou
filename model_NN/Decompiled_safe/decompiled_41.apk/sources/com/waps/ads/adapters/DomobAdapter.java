package com.waps.ads.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import cn.domob.android.ads.DomobAdListener;
import cn.domob.android.ads.DomobAdManager;
import cn.domob.android.ads.DomobAdView;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.g;

public class DomobAdapter extends a implements DomobAdListener {
    public DomobAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void handle() {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Into Domob");
        }
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            b bVar = adGroupLayout.d;
            int rgb = Color.rgb(bVar.e, bVar.f, bVar.g);
            int rgb2 = Color.rgb(bVar.a, bVar.b, bVar.c);
            Activity activity = (Activity) adGroupLayout.a.get();
            if (activity != null) {
                DomobAdView domobAdView = new DomobAdView(activity);
                DomobAdManager.setPublisherId(this.d.e);
                DomobAdManager.setIsTestMode(AdGroupTargeting.getTestMode());
                domobAdView.setAdListener(this);
                domobAdView.setBackgroundColor(rgb);
                domobAdView.setPrimaryTextColor(rgb2);
                domobAdView.requestFreshAd();
            }
        }
    }

    public void onFailedToReceiveFreshAd(DomobAdView domobAdView) {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Domob failure");
        }
        domobAdView.setAdListener((DomobAdListener) null);
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.j.resetRollover();
            adGroupLayout.rollover();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, cn.domob.android.ads.DomobAdView]
     candidates:
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void onReceivedFreshAd(DomobAdView domobAdView) {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Domob success");
        }
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new g(adGroupLayout, (ViewGroup) domobAdView));
            adGroupLayout.rotateThreadedDelayed();
        }
    }
}
