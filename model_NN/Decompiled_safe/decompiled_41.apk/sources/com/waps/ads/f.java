package com.waps.ads;

import java.lang.ref.WeakReference;

public class f implements Runnable {
    private WeakReference a;

    public f(AdGroupLayout adGroupLayout) {
        this.a = new WeakReference(adGroupLayout);
    }

    public void run() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.a.get();
        if (adGroupLayout != null) {
            adGroupLayout.rotateAd();
        }
    }
}
