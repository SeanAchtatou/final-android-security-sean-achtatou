package com.vpon.adon.android.main;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import com.vpon.adon.android.AdListener;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;

public class Test extends Activity {
    AdListener adListener = new AdListener() {
        public void onRecevieAd(AdView adView) {
            Log.i("adListenr", "OnRecevieAd");
        }

        public void onFailedToRecevieAd(AdView adView) {
            Log.i("adListenr", "OnFailesToRecevieAd");
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        setContentView(linearLayout);
        AdView adView = new AdView(this);
        adView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        adView.setAdListener(this.adListener);
        adView.setLicenseKey("ff80808129da8c680129ee69174a0008", AdOnPlatform.TW, true);
        linearLayout.addView(adView);
    }

    public void adonRotationHoriztion(int beganDegree, int endDegree, AdView view) {
        Rotate3dAnimation rotation = new Rotate3dAnimation((float) beganDegree, (float) endDegree, 160.0f, 24.0f, -24.0f, true);
        rotation.setDuration(1000);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.startAnimation(rotation);
    }
}
