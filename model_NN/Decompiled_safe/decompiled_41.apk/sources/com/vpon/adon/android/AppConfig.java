package com.vpon.adon.android;

public final class AppConfig {
    public static final boolean BETA = false;
    public static final boolean DEBUG = false;
    public static final String SDKVERSION = "2.1.1";
    public static final String vponTel = "88623698333";
}
