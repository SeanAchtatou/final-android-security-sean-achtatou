package com.vpon.adon.android.entity;

public class Ad extends Rsp {
    private int adHeight = 0;
    private String adHtml;
    private String adId;
    private AdRedirectPack adRedirectPack;
    private int adWidth = 0;
    private boolean bClick = false;
    private int distance = -1;
    private double mapLat;
    private double mapLon;
    private int refreshTime;

    public String getAdHtml() {
        return this.adHtml;
    }

    public void setAdHtml(String adHtml2) {
        this.adHtml = adHtml2;
    }

    public int getAdWidth() {
        return this.adWidth;
    }

    public void setAdWidth(int adWidth2) {
        this.adWidth = adWidth2;
    }

    public int getAdHeight() {
        return this.adHeight;
    }

    public void setAdHeight(int adHeight2) {
        this.adHeight = adHeight2;
    }

    public double getMapLon() {
        return this.mapLon;
    }

    public void setMapLon(double mapLon2) {
        this.mapLon = mapLon2;
    }

    public double getMapLat() {
        return this.mapLat;
    }

    public void setMapLat(double mapLat2) {
        this.mapLat = mapLat2;
    }

    public String getAdId() {
        return this.adId;
    }

    public void setAdId(String adId2) {
        this.adId = adId2;
    }

    public int getRefreshTime() {
        return this.refreshTime;
    }

    public void setRefreshTime(int refreshTime2) {
        this.refreshTime = refreshTime2;
    }

    public int getDistance() {
        return this.distance;
    }

    public void setDistance(int distance2) {
        this.distance = distance2;
    }

    public AdRedirectPack getAdRedirectPack() {
        return this.adRedirectPack;
    }

    public void setAdRedirectPack(AdRedirectPack adRedirectPack2) {
        this.adRedirectPack = adRedirectPack2;
    }

    public void setClicked() {
        this.bClick = true;
    }

    public boolean getClickStatus() {
        return this.bClick;
    }
}
