package hivestudio.choose;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.admob.android.ads.SimpleAdListener;
import hivestudio.choose.ChooseView;

public class Choose extends Activity implements AdListener {
    public static final String TAG = "Choose";
    private float _arrowAngle = -90.0f;
    /* access modifiers changed from: private */
    public ChooseView.ChooseThread _chooseThread;
    /* access modifiers changed from: private */
    public ChooseView _chooseView;
    private int _numPlayers = 6;
    private TextView[] _playerName = new TextView[10];
    private String[] _textViewNames = {String.valueOf(1), String.valueOf(2), String.valueOf(3), String.valueOf(4), String.valueOf(5), String.valueOf(6), String.valueOf(7), String.valueOf(8), String.valueOf(9), String.valueOf(10)};
    private View.OnClickListener mInfoListener = new View.OnClickListener() {
        public void onClick(View v) {
            int stateGame = Choose.this._chooseView.getThread().getStateGame();
            Choose.this._chooseView.getClass();
            if (stateGame != 0) {
                int stateGame2 = Choose.this._chooseView.getThread().getStateGame();
                Choose.this._chooseView.getClass();
                if (stateGame2 != 5) {
                    int stateGame3 = Choose.this._chooseView.getThread().getStateGame();
                    Choose.this._chooseView.getClass();
                    if (stateGame3 != 6) {
                        return;
                    }
                }
            }
            Choose.this.startActivity(new Intent(Choose.this, Info.class));
        }
    };
    private View.OnClickListener mLessListener = new View.OnClickListener() {
        public void onClick(View v) {
            Choose.this._chooseView.clickLess();
            Choose.this.switchTextViewCoordinates();
            Choose.this._chooseThread.reset();
        }
    };
    private View.OnClickListener mPlusListener = new View.OnClickListener() {
        public void onClick(View v) {
            Choose.this._chooseView.clickPlus();
            Choose.this.switchTextViewCoordinates();
            Choose.this._chooseThread.reset();
        }
    };
    private View.OnClickListener mResetListener = new View.OnClickListener() {
        public void onClick(View v) {
            Choose.this._chooseThread.reset();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        this._chooseView = (ChooseView) findViewById(R.id.ChooseView);
        this._chooseThread = this._chooseView.getThread();
        AdView ad = (AdView) findViewById(R.id.ad);
        ad.setAdListener(new ChooseAdListener(this, null));
        AdManager.setAllowUseOfLocation(true);
        this._chooseView.setTextView((TextView) findViewById(R.id.state), (TextView) findViewById(R.id.state), (TextView) findViewById(R.id.players), ad);
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this._numPlayers = this._chooseView.getNumPlayers();
        this._arrowAngle = this._chooseView.getArrowAngle();
        for (int i = 0; i < this._playerName.length; i++) {
            this._textViewNames[i] = String.valueOf(this._playerName[i].getText());
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        setContentView((int) R.layout.main);
        this._chooseView = (ChooseView) findViewById(R.id.ChooseView);
        this._chooseThread = this._chooseView.getThread();
        this._playerName[0] = (TextView) findViewById(R.id.player_name);
        this._playerName[1] = (TextView) findViewById(R.id.player_name2);
        this._playerName[2] = (TextView) findViewById(R.id.player_name3);
        this._playerName[3] = (TextView) findViewById(R.id.player_name4);
        this._playerName[4] = (TextView) findViewById(R.id.player_name5);
        this._playerName[5] = (TextView) findViewById(R.id.player_name6);
        this._playerName[6] = (TextView) findViewById(R.id.player_name7);
        this._playerName[7] = (TextView) findViewById(R.id.player_name8);
        this._playerName[8] = (TextView) findViewById(R.id.player_name9);
        this._playerName[9] = (TextView) findViewById(R.id.player_name10);
        AdView ad = (AdView) findViewById(R.id.ad);
        ad.setAdListener(new ChooseAdListener(this, null));
        this._chooseView.setTextView((TextView) findViewById(R.id.state), (TextView) findViewById(R.id.state), (TextView) findViewById(R.id.players), ad);
        ((ImageView) findViewById(R.id.InfoButton)).setOnClickListener(this.mInfoListener);
        ((Button) findViewById(R.id.ResetButton)).setOnClickListener(this.mResetListener);
        ((Button) findViewById(R.id.plusButton)).setOnClickListener(this.mPlusListener);
        ((Button) findViewById(R.id.lessButton)).setOnClickListener(this.mLessListener);
        this._chooseView.setNumPlayers(this._numPlayers);
        this._chooseView.setArrowAngle(this._arrowAngle);
        for (int i = 0; i < this._playerName.length; i++) {
            this._playerName[i].setText(this._textViewNames[i]);
        }
        switchTextViewCoordinates();
        this._chooseView.getThread().reset();
    }

    public void restoreInstanceState(Bundle savedState) {
        Log.v("choose", "RESTORE SAVED");
        ChooseView.ChooseThread thread = this._chooseView.getThread();
        this._chooseView.getClass();
        thread.setState(0);
        this._chooseView.setNumPlayers(savedState.getInt("players"));
        String[] aAux = savedState.getStringArray("player_names");
        for (int i = 0; i < this._playerName.length; i++) {
            this._playerName[i].setText(aAux[i]);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this._chooseThread.saveState(outState);
        String[] aPlayerNamesArray = new String[10];
        for (int i = 0; i < this._playerName.length; i++) {
            aPlayerNamesArray[i] = String.valueOf(this._playerName[i].getText());
        }
        outState.putStringArray("player_names", aPlayerNamesArray);
        Log.w(getClass().getName(), "SIS called");
    }

    /* access modifiers changed from: private */
    public void switchTextViewCoordinates() {
        for (int i = 0; i < this._playerName.length; i++) {
            String aAux = this._playerName[i].getText().toString();
            Character aLastChar = ' ';
            if (aAux.length() > 0) {
                aLastChar = Character.valueOf(aAux.charAt(aAux.length() - 1));
            }
            if (aLastChar.charValue() == '.') {
                aAux = (String) aAux.subSequence(0, aAux.length() - 2);
            }
            int aTextLength = aAux.length();
            if (aTextLength > 5 && this._chooseView.getNumPlayers() > 4) {
                this._playerName[i].setText(aAux.subSequence(0, 5));
                this._playerName[i].append("..");
            }
            if (aTextLength > 6 && this._chooseView.getNumPlayers() == 4) {
                this._playerName[i].setText(aAux.subSequence(0, 6));
                this._playerName[i].append("..");
            }
            if (aTextLength > 7 && this._chooseView.getNumPlayers() == 3) {
                this._playerName[i].setText(aAux.subSequence(0, 7));
                this._playerName[i].append("..");
            }
        }
        for (int i2 = 0; i2 < this._chooseView.getNumPlayers(); i2++) {
            this._playerName[i2].setLayoutParams(new AbsoluteLayout.LayoutParams(-2, -2, this._chooseView.getEditingPosX(i2, this._playerName[i2].length()), this._chooseView.getEditingPosY(i2)));
            this._playerName[i2].setVisibility(0);
        }
        for (int i3 = this._chooseView.getNumPlayers(); i3 < this._playerName.length; i3++) {
            this._playerName[i3].setVisibility(4);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case R.styleable.com_admob_android_ads_AdView_backgroundColor /*0*/:
                this._chooseThread.touchStart(x, y);
                break;
            case R.styleable.com_admob_android_ads_AdView_primaryTextColor /*1*/:
                this._chooseThread.touchUp();
                break;
            case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                this._chooseThread.touchMove(x, y);
                break;
        }
        int stateGame = this._chooseView.getThread().getStateGame();
        this._chooseView.getClass();
        if (stateGame != 6) {
            return true;
        }
        showKeyboard();
        return true;
    }

    private void showKeyboard() {
        InputMethodManager mgr = (InputMethodManager) getSystemService("input_method");
        mgr.showSoftInput(this._chooseView, 1);
        mgr.toggleSoftInput(2, 2);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        int stateGame = this._chooseView.getThread().getStateGame();
        this._chooseView.getClass();
        if (stateGame == 6) {
            KeyCharacterMap km = KeyCharacterMap.load(0);
            int aNumPlayer = this._chooseView.getPlayerEditing();
            String aAux = this._playerName[aNumPlayer].getText().toString();
            Character aLastChar = ' ';
            if (aAux.length() > 0) {
                aLastChar = Character.valueOf(aAux.charAt(aAux.length() - 1));
            }
            switch (keyCode) {
                case 67:
                    if (aLastChar.charValue() == '.' && aAux.length() > 1) {
                        aAux = (String) aAux.subSequence(0, aAux.length() - 2);
                    }
                    if (aAux.length() > 0) {
                        this._playerName[aNumPlayer].setText(aAux.subSequence(0, aAux.length() - 1));
                        break;
                    }
                    break;
                default:
                    if (this._chooseView.isFirstTimeEditing(aNumPlayer)) {
                        this._playerName[aNumPlayer].setText("");
                    }
                    if (aLastChar.charValue() != '.') {
                        if ((this._playerName[aNumPlayer].length() > 5 && this._chooseView.getNumPlayers() > 4) || ((this._playerName[aNumPlayer].length() > 6 && this._chooseView.getNumPlayers() == 4) || ((this._playerName[aNumPlayer].length() > 7 && this._chooseView.getNumPlayers() == 3) || this._playerName[aNumPlayer].length() > 9))) {
                            this._playerName[aNumPlayer].append("..");
                        } else if (Character.isLetterOrDigit(km.getDisplayLabel(keyCode))) {
                            this._playerName[aNumPlayer].append(Character.toString(km.getDisplayLabel(keyCode)));
                            this._playerName[aNumPlayer].setVisibility(0);
                        }
                        this._playerName[aNumPlayer].setLayoutParams(new AbsoluteLayout.LayoutParams(-2, -2, this._chooseView.getEditingPosX(this._chooseView.getPlayerEditing(), this._playerName[aNumPlayer].length()), this._chooseView.getEditingPosY(this._chooseView.getPlayerEditing())));
                        break;
                    }
                    break;
            }
            if (this._playerName[aNumPlayer].length() == 0) {
                this._playerName[aNumPlayer].setText(String.valueOf(aNumPlayer + 1));
                this._chooseView.setFirstTimeEditing(aNumPlayer);
                this._playerName[aNumPlayer].setLayoutParams(new AbsoluteLayout.LayoutParams(-2, -2, this._chooseView.getEditingPosX(this._chooseView.getPlayerEditing(), this._playerName[aNumPlayer].length()), this._chooseView.getEditingPosY(this._chooseView.getPlayerEditing())));
            }
        }
        if (keyCode == 4) {
            finish();
        }
        return false;
    }

    public void onFailedToReceiveAd(AdView arg0) {
    }

    public void onFailedToReceiveRefreshedAd(AdView arg0) {
    }

    public void onReceiveAd(AdView arg0) {
    }

    public void onReceiveRefreshedAd(AdView arg0) {
    }

    private class ChooseAdListener extends SimpleAdListener {
        private ChooseAdListener() {
        }

        /* synthetic */ ChooseAdListener(Choose choose, ChooseAdListener chooseAdListener) {
            this();
        }

        public void onFailedToReceiveAd(AdView adView) {
            super.onFailedToReceiveAd(adView);
        }

        public void onFailedToReceiveRefreshedAd(AdView adView) {
            super.onFailedToReceiveRefreshedAd(adView);
        }

        public void onReceiveAd(AdView adView) {
            super.onReceiveAd(adView);
        }

        public void onReceiveRefreshedAd(AdView adView) {
            super.onReceiveRefreshedAd(adView);
        }
    }
}
