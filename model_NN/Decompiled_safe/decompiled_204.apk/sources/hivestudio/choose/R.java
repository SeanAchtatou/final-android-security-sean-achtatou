package hivestudio.choose;

public final class R {

    public static final class anim {
        public static final int zoom_enter = 2130968576;
        public static final int zoom_exit = 2130968577;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int arrow = 2130837504;
        public static final int eight_players = 2130837505;
        public static final int eight_players_num0 = 2130837506;
        public static final int eight_players_num1 = 2130837507;
        public static final int eight_players_num2 = 2130837508;
        public static final int eight_players_num3 = 2130837509;
        public static final int eight_players_num4 = 2130837510;
        public static final int eight_players_num5 = 2130837511;
        public static final int eight_players_num6 = 2130837512;
        public static final int eight_players_num7 = 2130837513;
        public static final int eight_players_num8 = 2130837514;
        public static final int five_players = 2130837515;
        public static final int five_players_num0 = 2130837516;
        public static final int five_players_num1 = 2130837517;
        public static final int five_players_num2 = 2130837518;
        public static final int five_players_num3 = 2130837519;
        public static final int five_players_num4 = 2130837520;
        public static final int five_players_num5 = 2130837521;
        public static final int four_players = 2130837522;
        public static final int four_players_num0 = 2130837523;
        public static final int four_players_num1 = 2130837524;
        public static final int four_players_num2 = 2130837525;
        public static final int four_players_num3 = 2130837526;
        public static final int four_players_num4 = 2130837527;
        public static final int icon = 2130837528;
        public static final int info_icon = 2130837529;
        public static final int logo = 2130837530;
        public static final int nine_players = 2130837531;
        public static final int nine_players_num0 = 2130837532;
        public static final int nine_players_num1 = 2130837533;
        public static final int nine_players_num2 = 2130837534;
        public static final int nine_players_num3 = 2130837535;
        public static final int nine_players_num4 = 2130837536;
        public static final int nine_players_num5 = 2130837537;
        public static final int nine_players_num6 = 2130837538;
        public static final int nine_players_num7 = 2130837539;
        public static final int nine_players_num8 = 2130837540;
        public static final int nine_players_num9 = 2130837541;
        public static final int seven_players = 2130837542;
        public static final int seven_players_num0 = 2130837543;
        public static final int seven_players_num1 = 2130837544;
        public static final int seven_players_num2 = 2130837545;
        public static final int seven_players_num3 = 2130837546;
        public static final int seven_players_num4 = 2130837547;
        public static final int seven_players_num5 = 2130837548;
        public static final int seven_players_num6 = 2130837549;
        public static final int seven_players_num7 = 2130837550;
        public static final int six_players = 2130837551;
        public static final int six_players_num0 = 2130837552;
        public static final int six_players_num1 = 2130837553;
        public static final int six_players_num2 = 2130837554;
        public static final int six_players_num3 = 2130837555;
        public static final int six_players_num4 = 2130837556;
        public static final int six_players_num5 = 2130837557;
        public static final int six_players_num6 = 2130837558;
        public static final int ten_players = 2130837559;
        public static final int ten_players_num0 = 2130837560;
        public static final int ten_players_num1 = 2130837561;
        public static final int ten_players_num10 = 2130837562;
        public static final int ten_players_num2 = 2130837563;
        public static final int ten_players_num3 = 2130837564;
        public static final int ten_players_num4 = 2130837565;
        public static final int ten_players_num5 = 2130837566;
        public static final int ten_players_num6 = 2130837567;
        public static final int ten_players_num7 = 2130837568;
        public static final int ten_players_num8 = 2130837569;
        public static final int ten_players_num9 = 2130837570;
        public static final int three_players = 2130837571;
        public static final int three_players_num0 = 2130837572;
        public static final int three_players_num1 = 2130837573;
        public static final int three_players_num2 = 2130837574;
        public static final int three_players_num3 = 2130837575;
        public static final int two_players = 2130837576;
        public static final int two_players_num0 = 2130837577;
        public static final int two_players_num1 = 2130837578;
        public static final int two_players_num2 = 2130837579;
    }

    public static final class id {
        public static final int ChooseView = 2131099657;
        public static final int InfoButton = 2131099673;
        public static final int Logo = 2131099649;
        public static final int ResetButton = 2131099672;
        public static final int ad = 2131099668;

        /* renamed from: hivestudio  reason: collision with root package name */
        public static final int f0hivestudio = 2131099648;
        public static final int lessButton = 2131099669;
        public static final int player_name = 2131099658;
        public static final int player_name10 = 2131099667;
        public static final int player_name2 = 2131099659;
        public static final int player_name3 = 2131099660;
        public static final int player_name4 = 2131099661;
        public static final int player_name5 = 2131099662;
        public static final int player_name6 = 2131099663;
        public static final int player_name7 = 2131099664;
        public static final int player_name8 = 2131099665;
        public static final int player_name9 = 2131099666;
        public static final int players = 2131099670;
        public static final int plusButton = 2131099671;
        public static final int state = 2131099655;
        public static final int switcher = 2131099656;
        public static final int text1 = 2131099650;
        public static final int text2 = 2131099651;
        public static final int text3 = 2131099652;
        public static final int text4 = 2131099653;
        public static final int text5 = 2131099654;
    }

    public static final class layout {

        /* renamed from: hivestudio  reason: collision with root package name */
        public static final int f1hivestudio = 2130903040;
        public static final int link = 2130903041;
        public static final int main = 2130903042;
    }

    public static final class string {
        public static final int Instruction = 2131034124;
        public static final int Instruction_1 = 2131034125;
        public static final int Instruction_2 = 2131034126;
        public static final int Instruction_3 = 2131034127;
        public static final int Instruction_4 = 2131034128;
        public static final int app_name = 2131034112;
        public static final int hivestudio_link = 2131034130;
        public static final int info_button = 2131034114;
        public static final int less = 2131034121;
        public static final int players = 2131034122;
        public static final int plus = 2131034120;
        public static final int reset_button = 2131034113;
        public static final int speed = 2131034115;
        public static final int state_done = 2131034119;
        public static final int state_running = 2131034118;
        public static final int state_start = 2131034116;
        public static final int state_tracking = 2131034117;
        public static final int thanks = 2131034129;
        public static final int translation = 2131034123;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
