package hivestudio.choose;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;

public class Hivestudio extends Activity {
    protected boolean _active = true;
    protected int _splashTime = 800;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.f1hivestudio);
        new Thread() {
            public void run() {
                int waited = 0;
                while (Hivestudio.this._active && waited < Hivestudio.this._splashTime) {
                    try {
                        sleep(100);
                        if (Hivestudio.this._active) {
                            waited += 100;
                        }
                    } catch (InterruptedException e) {
                        return;
                    } finally {
                        Hivestudio.this.finish();
                        Hivestudio.this.startActivity(new Intent("hivestudio.choose.Choose"));
                        stop();
                    }
                }
            }
        }.start();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return true;
        }
        this._active = false;
        return true;
    }
}
