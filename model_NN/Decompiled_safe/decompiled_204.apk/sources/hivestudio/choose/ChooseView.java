package hivestudio.choose;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import com.admob.android.ads.AdView;

class ChooseView extends SurfaceView implements SurfaceHolder.Callback {
    public static final String TAG = "Choose";
    /* access modifiers changed from: private */
    public static Bitmap[] _background = new Bitmap[12];
    private final int CHOOSE_BAR_HEIGHT = 50;
    public final int STATE_DECIDING = 4;
    public final int STATE_EDITING = 6;
    public final int STATE_FINISHED = 5;
    public final int STATE_IDLE = 0;
    public final int STATE_RUNNING = 2;
    public final int STATE_START = -1;
    public final int STATE_TRACKING = 3;
    private final int TOP_BAR_HEIGHT = 62;
    /* access modifiers changed from: private */
    public AdView _ad;
    /* access modifiers changed from: private */
    public float _arrowAngle;
    /* access modifiers changed from: private */
    public Rect _arrowRect = new Rect();
    /* access modifiers changed from: private */
    public int _canvasHeight = 368;
    /* access modifiers changed from: private */
    public int _canvasWidth = 320;
    private int _editingPosX;
    private int _editingPosY;
    /* access modifiers changed from: private */
    public int _gameState;
    private boolean[] _isFirstTimeVector = new boolean[10];
    /* access modifiers changed from: private */
    public int _numberPlayers = 6;
    /* access modifiers changed from: private */
    public int _playerEditing;
    private int[] _playerPositionX10 = {(this._canvasWidth / 2) + 40, (this._canvasWidth / 2) + 85, (this._canvasWidth / 2) + 94, (this._canvasWidth / 2) + 85, (this._canvasWidth / 2) + 35, (this._canvasWidth / 2) - 50, (this._canvasWidth / 2) - 102, (this._canvasWidth / 2) - 115, (this._canvasWidth / 2) - 100, (this._canvasWidth / 2) - 60};
    private int[] _playerPositionX2 = {this._canvasWidth / 2, this._canvasWidth / 2};
    private int[] _playerPositionX3 = {(this._canvasWidth / 2) + 75, this._canvasWidth / 2, (this._canvasWidth / 2) - 85};
    private int[] _playerPositionX4 = {(this._canvasWidth / 2) + 85, (this._canvasWidth / 2) + 85, (this._canvasWidth / 2) - 85, (this._canvasWidth / 2) - 85};
    private int[] _playerPositionX5 = {(this._canvasWidth / 2) + 57, (this._canvasWidth / 2) + 85, this._canvasWidth / 2, (this._canvasWidth / 2) - 95, (this._canvasWidth / 2) - 75};
    private int[] _playerPositionX6 = {(this._canvasWidth / 2) + 47, (this._canvasWidth / 2) + 102, (this._canvasWidth / 2) + 47, (this._canvasWidth / 2) - 70, (this._canvasWidth / 2) - 115, (this._canvasWidth / 2) - 70};
    private int[] _playerPositionX7 = {(this._canvasWidth / 2) + 60, (this._canvasWidth / 2) + 100, (this._canvasWidth / 2) + 80, (this._canvasWidth / 2) - 5, (this._canvasWidth / 2) - 95, (this._canvasWidth / 2) - 115, (this._canvasWidth / 2) - 75};
    private int[] _playerPositionX8 = {(this._canvasWidth / 2) + 40, (this._canvasWidth / 2) + 95, (this._canvasWidth / 2) + 95, (this._canvasWidth / 2) + 35, (this._canvasWidth / 2) - 55, (this._canvasWidth / 2) - 110, (this._canvasWidth / 2) - 110, (this._canvasWidth / 2) - 65};
    private int[] _playerPositionX9 = {(this._canvasWidth / 2) + 40, (this._canvasWidth / 2) + 85, (this._canvasWidth / 2) + 94, (this._canvasWidth / 2) + 80, (this._canvasWidth / 2) - 5, (this._canvasWidth / 2) - 90, (this._canvasWidth / 2) - 110, (this._canvasWidth / 2) - 95, (this._canvasWidth / 2) - 55};
    private int[] _playerPositionY10 = {(this._canvasHeight / 2) - 130, (this._canvasHeight / 2) - 85, this._canvasHeight / 2, (this._canvasHeight / 2) + 80, (this._canvasHeight / 2) + 120, (this._canvasHeight / 2) + 120, (this._canvasHeight / 2) + 80, this._canvasHeight / 2, (this._canvasHeight / 2) - 85, (this._canvasHeight / 2) - 130};
    private int[] _playerPositionY2 = {(this._canvasHeight / 2) - 130, (this._canvasHeight / 2) + 100};
    private int[] _playerPositionY3 = {(this._canvasHeight / 2) - 110, (this._canvasHeight / 2) + 90, (this._canvasHeight / 2) - 110};
    private int[] _playerPositionY4 = {(this._canvasHeight / 2) - 130, (this._canvasHeight / 2) + 100, (this._canvasHeight / 2) + 100, (this._canvasHeight / 2) - 130};
    private int[] _playerPositionY5 = {(this._canvasHeight / 2) - 130, this._canvasHeight / 2, (this._canvasHeight / 2) + 100, this._canvasHeight / 2, (this._canvasHeight / 2) - 130};
    private int[] _playerPositionY6 = {(this._canvasHeight / 2) - 130, this._canvasHeight / 2, (this._canvasHeight / 2) + 100, (this._canvasHeight / 2) + 100, this._canvasHeight / 2, (this._canvasHeight / 2) - 130};
    private int[] _playerPositionY7 = {(this._canvasHeight / 2) - 130, (this._canvasHeight / 2) - 30, (this._canvasHeight / 2) + 70, (this._canvasHeight / 2) + 100, (this._canvasHeight / 2) + 70, (this._canvasHeight / 2) - 30, (this._canvasHeight / 2) - 130};
    private int[] _playerPositionY8 = {(this._canvasHeight / 2) - 130, (this._canvasHeight / 2) - 50, (this._canvasHeight / 2) + 50, (this._canvasHeight / 2) + 100, (this._canvasHeight / 2) + 100, (this._canvasHeight / 2) + 50, (this._canvasHeight / 2) - 50, (this._canvasHeight / 2) - 130};
    private int[] _playerPositionY9 = {(this._canvasHeight / 2) - 130, (this._canvasHeight / 2) - 70, (this._canvasHeight / 2) + 10, (this._canvasHeight / 2) + 80, (this._canvasHeight / 2) + 120, (this._canvasHeight / 2) + 80, (this._canvasHeight / 2) + 10, (this._canvasHeight / 2) - 70, (this._canvasHeight / 2) - 130};
    /* access modifiers changed from: private */
    public TextView _playersView;
    private TextView _speedView;
    /* access modifiers changed from: private */
    public TextView _stateView;
    private ChooseThread _thread;

    public ChooseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        Handler _handler = new Handler() {
            public void handleMessage(Message msg) {
                switch (ChooseView.this._gameState) {
                    case R.styleable.com_admob_android_ads_AdView_backgroundColor /*0*/:
                        ChooseView.this._stateView.setText((int) R.string.state_start);
                        break;
                    case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                        ChooseView.this._stateView.setText((int) R.string.state_running);
                        if (ChooseView.this._ad.getVisibility() != 0 || ChooseView.this._ad.getVisibility() == 8) {
                            ChooseView.this._ad.setVisibility(0);
                            AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
                            animation.setDuration(400);
                            animation.setFillAfter(true);
                            animation.setInterpolator(new AccelerateInterpolator());
                            ChooseView.this._ad.startAnimation(animation);
                            break;
                        }
                    case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                        ChooseView.this._stateView.setText((int) R.string.state_tracking);
                        break;
                    case 5:
                        ChooseView.this._ad.setVisibility(8);
                        ChooseView.this._stateView.setText((int) R.string.state_done);
                        break;
                }
                ChooseView.this._playersView.setText(new StringBuilder().append(ChooseView.this._numberPlayers).toString());
            }
        };
        for (int i = 0; i < this._isFirstTimeVector.length; i++) {
            this._isFirstTimeVector[i] = true;
        }
        this._thread = new ChooseThread(holder, context, _handler);
        setFocusable(true);
    }

    public ChooseThread getThread() {
        return this._thread;
    }

    public int getNumPlayers() {
        return this._numberPlayers;
    }

    public float getArrowAngle() {
        return this._arrowAngle;
    }

    public int getEditingPosX(int iPlayer, int iNumberOffset) {
        int aOffset = iNumberOffset * 6;
        switch (this._numberPlayers) {
            case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                return this._playerPositionX2[iPlayer] - aOffset;
            case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                if (this._playerEditing == 1) {
                    aOffset = iNumberOffset << 1;
                }
                return this._playerPositionX3[iPlayer] - aOffset;
            case R.styleable.com_admob_android_ads_AdView_refreshInterval /*4*/:
                return this._playerPositionX4[iPlayer] - aOffset;
            case 5:
                if (this._playerEditing <= 2) {
                    aOffset = iNumberOffset << 1;
                }
                return this._playerPositionX5[iPlayer] - aOffset;
            case 6:
                if (this._playerEditing <= 3) {
                    aOffset = iNumberOffset << 1;
                }
                return this._playerPositionX6[iPlayer] - aOffset;
            case 7:
                if (this._playerEditing <= 3) {
                    aOffset = iNumberOffset << 2;
                }
                return this._playerPositionX7[iPlayer] - aOffset;
            case 8:
                if (this._playerEditing <= 4) {
                    aOffset = iNumberOffset << 2;
                }
                return this._playerPositionX8[iPlayer] - aOffset;
            case 9:
                if (this._playerEditing <= 4) {
                    aOffset = iNumberOffset << 1;
                }
                return (this._playerPositionX9[iPlayer] - aOffset) - 1;
            case 10:
                if (this._playerEditing <= 5) {
                    aOffset = iNumberOffset << 1;
                }
                return (this._playerPositionX10[iPlayer] - aOffset) - 1;
            default:
                return this._editingPosX;
        }
    }

    public int getEditingPosY(int iPlayer) {
        switch (this._numberPlayers) {
            case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                return this._playerPositionY2[iPlayer];
            case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                return this._playerPositionY3[iPlayer];
            case R.styleable.com_admob_android_ads_AdView_refreshInterval /*4*/:
                return this._playerPositionY4[iPlayer];
            case 5:
                return this._playerPositionY5[iPlayer];
            case 6:
                return this._playerPositionY6[iPlayer];
            case 7:
                return this._playerPositionY7[iPlayer];
            case 8:
                return this._playerPositionY8[iPlayer];
            case 9:
                return this._playerPositionY9[iPlayer];
            case 10:
                return this._playerPositionY10[iPlayer];
            default:
                return this._editingPosY;
        }
    }

    public void setNumPlayers(int iNumPlayers) {
        this._numberPlayers = iNumPlayers;
        this._thread.switchPlayersBitmap();
    }

    public void setArrowAngle(float iArrowAngle) {
        this._arrowAngle = iArrowAngle;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this._thread.setRunning(true);
        this._thread.start();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this._thread.setSurfaceSize(width, height);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this._thread.setRunning(false);
        boolean retry = true;
        while (retry) {
            try {
                this._thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void setTextView(TextView speedView, TextView stateView, TextView playersView, AdView ad) {
        this._stateView = stateView;
        this._playersView = playersView;
        this._ad = ad;
    }

    public void clickPlus() {
        if ((this._thread.getStateGame() == 0 || this._thread.getStateGame() == 5 || this._thread.getStateGame() == 6) && this._numberPlayers < 10) {
            this._numberPlayers++;
            this._thread.switchPlayersBitmap();
        }
    }

    public void clickLess() {
        if ((this._thread.getStateGame() == 0 || this._thread.getStateGame() == 5 || this._thread.getStateGame() == 6) && this._numberPlayers > 2) {
            this._numberPlayers--;
            this._thread.switchPlayersBitmap();
        }
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
    }

    public int getPlayerEditing() {
        return this._playerEditing - 1;
    }

    public boolean isFirstTimeEditing(int iNumPlayer) {
        if (!this._isFirstTimeVector[iNumPlayer]) {
            return false;
        }
        this._isFirstTimeVector[iNumPlayer] = false;
        return true;
    }

    public void setFirstTimeEditing(int iNumPlayer) {
        this._isFirstTimeVector[iNumPlayer] = true;
    }

    class ChooseThread extends Thread {
        private final int ARROW_HEIGHT = 24;
        private final int ARROW_WIDTH = 170;
        private int ARROW_X;
        private int ARROW_Y;
        private final double FRICTION_COEF = 0.02d;
        private final double FRICTION_COEF_FINAL = 0.005d;
        private final int MAX_ARROW_VELOCITY = 48;
        private int _animOffset;
        private Bitmap _arrow;
        private double _arrowAngVelocity;
        private int _arrowPosition;
        private Handler _handler;
        private double _oldAng;
        Resources _res;
        private boolean _run = false;
        private SurfaceHolder _surfaceHolder;

        public ChooseThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
            this._surfaceHolder = surfaceHolder;
            this._handler = handler;
            this._res = context.getResources();
            this._arrow = BitmapFactory.decodeResource(this._res, R.drawable.arrow);
            this._animOffset = 0;
            ChooseView.this._arrowAngle = -90.0f;
            this._arrowPosition = 0;
            this._arrowAngVelocity = 0.0d;
            ChooseView.this._numberPlayers = 6;
            ChooseView.this._playerEditing = 0;
            ChooseView.this._gameState = 0;
            switchPlayersBitmap();
        }

        public void run() {
            this.ARROW_X = ChooseView.this._canvasWidth / 2;
            this.ARROW_Y = 181;
            while (this._run) {
                Canvas c = null;
                try {
                    c = this._surfaceHolder.lockCanvas(null);
                    switch (ChooseView.this._gameState) {
                        case R.styleable.com_admob_android_ads_AdView_backgroundColor /*0*/:
                            this._animOffset = 0;
                            this._oldAng = 90.0d;
                            break;
                        case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                            this._animOffset = 1;
                            calculateArrowPosition();
                            break;
                        case 5:
                            this._animOffset = 1;
                            break;
                    }
                    doDraw(c);
                } finally {
                    if (c != null) {
                        this._surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        private void calculateArrowPosition() {
            if (ChooseView.this._numberPlayers != 2) {
                int aModel = 360 / ChooseView.this._numberPlayers;
                double aNewBaseAngle = transformBase((double) Math.abs(ChooseView.this._arrowAngle));
                if (aNewBaseAngle != 0.0d) {
                    this._arrowPosition = (((int) aNewBaseAngle) / aModel) + 1;
                } else {
                    this._arrowPosition = 1;
                }
                if (this._arrowPosition > ChooseView.this._numberPlayers) {
                    this._arrowPosition = ChooseView.this._numberPlayers;
                }
            } else if (ChooseView.this._arrowAngle >= 0.0f || ChooseView.this._arrowAngle > -180.0f) {
                this._arrowPosition = 1;
            } else {
                this._arrowPosition = 2;
            }
            ChooseView chooseView = ChooseView.this;
            chooseView._arrowAngle = (float) (((double) chooseView._arrowAngle) + this._arrowAngVelocity);
            if (this._arrowAngVelocity > 0.0d) {
                ChooseView chooseView2 = ChooseView.this;
                chooseView2._arrowAngle = chooseView2._arrowAngle - 360.0f;
            }
            ChooseView chooseView3 = ChooseView.this;
            chooseView3._arrowAngle = chooseView3._arrowAngle % 360.0f;
            if (this._arrowAngVelocity < 0.0d) {
                this._arrowAngVelocity += ((-this._arrowAngVelocity) * 0.005d) + 0.02d;
                if (this._arrowAngVelocity > 0.0d) {
                    this._arrowAngVelocity = 0.0d;
                }
            } else {
                this._arrowAngVelocity -= (this._arrowAngVelocity * 0.005d) + 0.02d;
                if (this._arrowAngVelocity < 0.0d) {
                    this._arrowAngVelocity = 0.0d;
                }
            }
            if (this._arrowAngVelocity == 0.0d) {
                ChooseView.this._gameState = 5;
            }
        }

        private double transformBase(double iAngle) {
            if (iAngle < 0.0d || iAngle > 90.0d) {
                return 450.0d - iAngle;
            }
            return 90.0d - iAngle;
        }

        /* access modifiers changed from: private */
        public void switchPlayersBitmap() {
            switch (ChooseView.this._numberPlayers) {
                case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.two_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.two_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.two_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.two_players_num2);
                    return;
                case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.three_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.three_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.three_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.three_players_num2);
                    ChooseView._background[4] = BitmapFactory.decodeResource(this._res, R.drawable.three_players_num3);
                    return;
                case R.styleable.com_admob_android_ads_AdView_refreshInterval /*4*/:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.four_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.four_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.four_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.four_players_num2);
                    ChooseView._background[4] = BitmapFactory.decodeResource(this._res, R.drawable.four_players_num3);
                    ChooseView._background[5] = BitmapFactory.decodeResource(this._res, R.drawable.four_players_num4);
                    return;
                case 5:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.five_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.five_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.five_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.five_players_num2);
                    ChooseView._background[4] = BitmapFactory.decodeResource(this._res, R.drawable.five_players_num3);
                    ChooseView._background[5] = BitmapFactory.decodeResource(this._res, R.drawable.five_players_num4);
                    ChooseView._background[6] = BitmapFactory.decodeResource(this._res, R.drawable.five_players_num5);
                    return;
                case 6:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.six_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.six_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.six_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.six_players_num2);
                    ChooseView._background[4] = BitmapFactory.decodeResource(this._res, R.drawable.six_players_num3);
                    ChooseView._background[5] = BitmapFactory.decodeResource(this._res, R.drawable.six_players_num4);
                    ChooseView._background[6] = BitmapFactory.decodeResource(this._res, R.drawable.six_players_num5);
                    ChooseView._background[7] = BitmapFactory.decodeResource(this._res, R.drawable.six_players_num6);
                    return;
                case 7:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players_num2);
                    ChooseView._background[4] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players_num3);
                    ChooseView._background[5] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players_num4);
                    ChooseView._background[6] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players_num5);
                    ChooseView._background[7] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players_num6);
                    ChooseView._background[8] = BitmapFactory.decodeResource(this._res, R.drawable.seven_players_num7);
                    return;
                case 8:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num2);
                    ChooseView._background[4] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num3);
                    ChooseView._background[5] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num4);
                    ChooseView._background[6] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num5);
                    ChooseView._background[7] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num6);
                    ChooseView._background[8] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num7);
                    ChooseView._background[9] = BitmapFactory.decodeResource(this._res, R.drawable.eight_players_num8);
                    return;
                case 9:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num2);
                    ChooseView._background[4] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num3);
                    ChooseView._background[5] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num4);
                    ChooseView._background[6] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num5);
                    ChooseView._background[7] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num6);
                    ChooseView._background[8] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num7);
                    ChooseView._background[9] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num8);
                    ChooseView._background[10] = BitmapFactory.decodeResource(this._res, R.drawable.nine_players_num9);
                    return;
                case 10:
                    ChooseView._background[0] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players);
                    ChooseView._background[1] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num0);
                    ChooseView._background[2] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num1);
                    ChooseView._background[3] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num2);
                    ChooseView._background[4] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num3);
                    ChooseView._background[5] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num4);
                    ChooseView._background[6] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num5);
                    ChooseView._background[7] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num6);
                    ChooseView._background[8] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num7);
                    ChooseView._background[9] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num8);
                    ChooseView._background[10] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num9);
                    ChooseView._background[11] = BitmapFactory.decodeResource(this._res, R.drawable.ten_players_num10);
                    return;
                default:
                    return;
            }
        }

        private void doDraw(Canvas canvas) {
            canvas.drawBitmap(ChooseView._background[(this._animOffset + this._arrowPosition) % (ChooseView.this._numberPlayers + 2)], 0.0f, -62.0f, (Paint) null);
            canvas.save();
            canvas.rotate(ChooseView.this._arrowAngle, (float) this.ARROW_X, (float) this.ARROW_Y);
            canvas.drawBitmap(this._arrow, (float) (this.ARROW_X - 110), (float) (this.ARROW_Y - 15), (Paint) null);
            canvas.restore();
            this._handler.sendEmptyMessage(1);
        }

        public void setRunning(boolean b) {
            this._run = b;
        }

        public void setState(int iState) {
            ChooseView.this._gameState = iState;
        }

        public int getStateGame() {
            return ChooseView.this._gameState;
        }

        public void setSurfaceSize(int width, int height) {
            synchronized (this._surfaceHolder) {
                ChooseView.this._canvasWidth = width;
                ChooseView.this._canvasHeight = height;
            }
        }

        public void touchStart(float x, float y) {
            ChooseView.this._arrowRect.set(this.ARROW_X - 85, (this.ARROW_Y - 62) + 30, this.ARROW_X + 85, this.ARROW_Y + 170);
            if (!ChooseView.this._arrowRect.contains((int) x, (int) y)) {
                if (ChooseView.this._gameState == 0 || ChooseView.this._gameState == 5 || ChooseView.this._gameState == 6) {
                    ChooseView.this._gameState = 6;
                }
                calculatePlayerEditing(x, y);
            } else if (ChooseView.this._gameState == 0 || ChooseView.this._gameState == 6 || ChooseView.this._gameState == 5) {
                ChooseView.this._gameState = 3;
                ChooseView.this._arrowAngle = -((float) calculateAngle(x, 20.0f + y));
            }
        }

        private void calculatePlayerEditing(float x, float y) {
            float aPointAngle = (float) calculateAngle(x, 20.0f + y);
            if (ChooseView.this._numberPlayers != 2) {
                int aModel = 360 / ChooseView.this._numberPlayers;
                double aNewBaseAngle = transformBase((double) Math.abs(aPointAngle));
                if (aNewBaseAngle != 0.0d) {
                    ChooseView.this._playerEditing = (((int) aNewBaseAngle) / aModel) + 1;
                } else {
                    ChooseView.this._playerEditing = 1;
                }
            } else if (aPointAngle <= 0.0f || aPointAngle > 180.0f) {
                ChooseView.this._playerEditing = 2;
            } else {
                ChooseView.this._playerEditing = 1;
            }
        }

        public void touchMove(float x, float y) {
            if (ChooseView.this._gameState == 3) {
                double aNewAngle = calculateAngle(x, 20.0f + y);
                if (this._oldAng < 90.0d && aNewAngle > 270.0d) {
                    this._arrowAngVelocity = (this._oldAng - aNewAngle) + 360.0d;
                } else if (this._oldAng <= 270.0d || aNewAngle >= 90.0d) {
                    this._arrowAngVelocity = this._oldAng - aNewAngle;
                } else {
                    this._arrowAngVelocity = (this._oldAng - aNewAngle) - 360.0d;
                }
                this._oldAng = aNewAngle;
                ChooseView.this._arrowAngle = (float) (-aNewAngle);
                if (this._arrowAngVelocity > 48.0d) {
                    this._arrowAngVelocity = 48.0d;
                } else if (this._arrowAngVelocity < -48.0d) {
                    this._arrowAngVelocity = -48.0d;
                }
            }
        }

        private double calculateAngle(float x, float y) {
            double aXcoordinate = (double) (x - ((float) this.ARROW_X));
            double aYcoordinate = (double) ((((float) this.ARROW_Y) - y) + 85.0f);
            double aNewAngleRad = Math.atan2(aYcoordinate, aXcoordinate);
            if (aXcoordinate >= 0.0d && aYcoordinate >= 0.0d) {
                return Math.toDegrees(aNewAngleRad) % 360.0d;
            }
            if (aXcoordinate >= 0.0d && aYcoordinate <= 0.0d) {
                return (Math.toDegrees(aNewAngleRad) % 360.0d) + 360.0d;
            }
            if (aXcoordinate > 0.0d || aYcoordinate > 0.0d) {
                return Math.toDegrees(aNewAngleRad) % 360.0d;
            }
            return (Math.toDegrees(aNewAngleRad) % 360.0d) + 360.0d;
        }

        public void touchUp() {
            if (ChooseView.this._gameState == 3) {
                ChooseView.this._gameState = 2;
            }
        }

        public Bundle saveState(Bundle map) {
            synchronized (this._surfaceHolder) {
                if (map != null) {
                    map.putInt("players", Integer.valueOf(ChooseView.this._numberPlayers).intValue());
                }
            }
            return map;
        }

        public void reset() {
            if (ChooseView.this._gameState == 0 || ChooseView.this._gameState == 5 || ChooseView.this._gameState == 6) {
                this._animOffset = 0;
                ChooseView.this._arrowAngle = -90.0f;
                this._arrowPosition = 0;
                this._arrowAngVelocity = 0.0d;
                ChooseView.this._playerEditing = 0;
                ChooseView.this._gameState = 0;
            }
        }
    }
}
