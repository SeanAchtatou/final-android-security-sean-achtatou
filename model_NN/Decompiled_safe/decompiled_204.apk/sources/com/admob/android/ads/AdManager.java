package com.admob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;

public class AdManager {
    public static final String LOG = "AdMobSDK";
    public static final String SDK_VERSION = "20100527-ANDROID-3312276cc1406347";
    public static final String SDK_VERSION_DATE = "20100527";
    public static final String TEST_EMULATOR = "emulator";
    private static String a;
    private static int b;
    private static String c;
    private static String d = "url";
    private static String[] e = null;
    private static String f;
    /* access modifiers changed from: private */
    public static Location g;
    private static boolean h = false;
    private static boolean i = false;
    /* access modifiers changed from: private */
    public static long j;
    private static String k;
    private static GregorianCalendar l;
    private static Gender m;
    private static boolean n = false;

    public enum Gender {
        MALE,
        FEMALE
    }

    static {
        boolean z;
        Log.i(LOG, "AdMob SDK version is 20100527-ANDROID-3312276cc1406347");
        try {
            if (Class.forName("com.admob.android.ads.analytics.InstallReceiver") != null) {
                z = true;
            } else {
                z = false;
            }
            if (z) {
            }
        } catch (ClassNotFoundException e2) {
        } finally {
            Log.w(LOG, "Could not find InstallReceiver class so cannot track installs from AdMob ads.  Did you obfuscate this class away?");
        }
    }

    private AdManager() {
    }

    protected static void clientError(String str) {
        Log.e(LOG, str);
        throw new IllegalArgumentException(str);
    }

    protected static int getScreenWidth(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            return defaultDisplay.getWidth();
        }
        return 0;
    }

    static void a(Context context) {
        if (!n) {
            n = true;
            try {
                PackageManager packageManager = context.getPackageManager();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        String string = applicationInfo.metaData.getString("ADMOB_PUBLISHER_ID");
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Publisher ID read from AndroidManifest.xml is " + string);
                        }
                        if (c == null && string != null) {
                            if (!string.equals("a1496ced2842262") || (!context.getPackageName().equals("com.admob.android.ads") && !context.getPackageName().equals("com.example.admob.lunarlander"))) {
                                setPublisherId(string);
                            } else {
                                Log.i(LOG, "This is a sample application so allowing sample publisher ID.");
                                c = string;
                            }
                        }
                    }
                    if (!i) {
                        h = applicationInfo.metaData.getBoolean("ADMOB_ALLOW_LOCATION_FOR_ADS", false);
                    }
                    a = applicationInfo.packageName;
                    if (Log.isLoggable(LOG, 2)) {
                        Log.v(LOG, "Application's package name is " + a);
                    }
                }
                PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
                if (packageInfo != null) {
                    b = packageInfo.versionCode;
                    if (Log.isLoggable(LOG, 2)) {
                        Log.v(LOG, "Application's version number is " + b);
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    public static String getApplicationPackageName(Context context) {
        if (a == null) {
            a(context);
        }
        return a;
    }

    protected static int getApplicationVersion(Context context) {
        if (a == null) {
            a(context);
        }
        return b;
    }

    public static String getPublisherId(Context context) {
        if (c == null) {
            a(context);
        }
        if (c == null && Log.isLoggable(LOG, 6)) {
            Log.e(LOG, "getPublisherId returning null publisher id.  Please set the publisher id in AndroidManifest.xml or using AdManager.setPublisherId(String)");
        }
        return c;
    }

    public static void setPublisherId(String str) {
        if (str == null || str.length() != 15) {
            clientError("SETUP ERROR:  Incorrect AdMob publisher ID.  Should 15 [a-f,0-9] characters:  " + c);
        }
        if (str.equalsIgnoreCase("a1496ced2842262")) {
            clientError("SETUP ERROR:  Cannot use the sample publisher ID (a1496ced2842262).  Yours is available on www.admob.com.");
        }
        Log.i(LOG, "Publisher ID set to " + str);
        c = str;
    }

    public static String getTestAction() {
        return d;
    }

    public static void setTestDevices(String[] strArr) {
        if (strArr == null) {
            e = null;
            return;
        }
        String[] strArr2 = (String[]) strArr.clone();
        e = strArr2;
        Arrays.sort(strArr2);
    }

    static String[] getTestDevices() {
        return e;
    }

    public static boolean isTestDevice(Context context) {
        if (e == null) {
            return false;
        }
        String userId = getUserId(context);
        if (userId == null) {
            userId = TEST_EMULATOR;
        }
        if (Arrays.binarySearch(e, userId) >= 0) {
            return true;
        }
        return false;
    }

    public static void setTestAction(String str) {
        d = str;
    }

    private static boolean f() {
        return "unknown".equals(Build.BOARD) && "generic".equals(Build.DEVICE) && "generic".equals(Build.BRAND);
    }

    public static String getUserId(Context context) {
        if (f == null) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string == null || f()) {
                f = TEST_EMULATOR;
                Log.i(LOG, "To get test ads on the emulator use AdManager.setTestDevices( new String[] { AdManager.TEST_EMULATOR } )");
            } else {
                f = a(string);
                Log.i(LOG, "To get test ads on this device use AdManager.setTestDevices( new String[] { \"" + f + "\" } )");
            }
            if (Log.isLoggable(LOG, 3)) {
                Log.d(LOG, "The user ID is " + f);
            }
        }
        if (f == TEST_EMULATOR) {
            return null;
        }
        return f;
    }

    private static String a(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (Exception e2) {
            Log.d(LOG, "Could not generate hash of " + str, e2);
            return str.substring(0, 32);
        }
    }

    public static void setAllowUseOfLocation(boolean z) {
        i = true;
        h = z;
    }

    public static Location getCoordinates(Context context) {
        String str;
        final LocationManager locationManager;
        boolean z;
        if (f() && !h) {
            Log.i(LOG, "Location information is not being used for ad requests. Enable location");
            Log.i(LOG, "based ads with AdManager.setAllowUseOfLocation(true) or by setting ");
            Log.i(LOG, "meta-data ADMOB_ALLOW_LOCATION_FOR_ADS to true in AndroidManifest.xml");
        }
        if (h && context != null && (g == null || System.currentTimeMillis() > j + 900000)) {
            synchronized (context) {
                if (g == null || System.currentTimeMillis() > j + 900000) {
                    j = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from the network.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria, true);
                            z = true;
                        } else {
                            str = null;
                            z = true;
                        }
                    } else {
                        str = null;
                        locationManager = null;
                        z = false;
                    }
                    if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from GPS.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            z = true;
                        }
                    }
                    if (!z) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Cannot access user's location.  Permissions are not set.");
                        }
                    } else if (str != null) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Location provider setup successfully.");
                        }
                        locationManager.requestLocationUpdates(str, 0, 0.0f, new LocationListener() {
                            public final void onLocationChanged(Location location) {
                                Location unused = AdManager.g = location;
                                long unused2 = AdManager.j = System.currentTimeMillis();
                                locationManager.removeUpdates(this);
                                if (Log.isLoggable(AdManager.LOG, 3)) {
                                    Log.d(AdManager.LOG, "Acquired location " + AdManager.g.getLatitude() + "," + AdManager.g.getLongitude() + " at " + new Date(AdManager.j).toString() + ".");
                                }
                            }

                            public final void onProviderDisabled(String str) {
                            }

                            public final void onProviderEnabled(String str) {
                            }

                            public final void onStatusChanged(String str, int i, Bundle bundle) {
                            }
                        }, context.getMainLooper());
                    } else if (Log.isLoggable(LOG, 3)) {
                        Log.d(LOG, "No location providers are available.  Ads will not be geotargeted.");
                    }
                }
            }
        }
        return g;
    }

    static String b(Context context) {
        String str = null;
        Location coordinates = getCoordinates(context);
        if (coordinates != null) {
            str = coordinates.getLatitude() + "," + coordinates.getLongitude();
        }
        if (Log.isLoggable(LOG, 3)) {
            Log.d(LOG, "User coordinates are " + str);
        }
        return str;
    }

    static String a() {
        return String.valueOf(j / 1000);
    }

    public static String getPostalCode() {
        return k;
    }

    public static String getOrientation(Context context) {
        if (((WindowManager) context.getSystemService("window")).getDefaultDisplay().getOrientation() == 1) {
            return "l";
        }
        return "p";
    }

    public static void setPostalCode(String str) {
        k = str;
    }

    public static GregorianCalendar getBirthday() {
        return l;
    }

    static String b() {
        GregorianCalendar birthday = getBirthday();
        if (birthday == null) {
            return null;
        }
        return String.format("%04d%02d%02d", Integer.valueOf(birthday.get(1)), Integer.valueOf(birthday.get(2) + 1), Integer.valueOf(birthday.get(5)));
    }

    public static void setBirthday(GregorianCalendar gregorianCalendar) {
        l = gregorianCalendar;
    }

    public static void setBirthday(int i2, int i3, int i4) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(i2, i3 - 1, i4);
        setBirthday(gregorianCalendar);
    }

    public static Gender getGender() {
        return m;
    }

    static String c() {
        if (m == Gender.MALE) {
            return "m";
        }
        if (m == Gender.FEMALE) {
            return "f";
        }
        return null;
    }

    public static void setGender(Gender gender) {
        m = gender;
    }

    static void setEndpoint(String str) {
        u.a(str);
    }

    static String getEndpoint() {
        return u.a();
    }
}
