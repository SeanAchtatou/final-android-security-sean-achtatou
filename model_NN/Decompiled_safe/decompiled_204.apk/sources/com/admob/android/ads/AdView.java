package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admob.android.ads.d;
import java.lang.ref.WeakReference;

public class AdView extends RelativeLayout {
    static final Handler a = new Handler();
    private static Boolean b;
    /* access modifiers changed from: private */
    public g c;
    /* access modifiers changed from: private */
    public int d;
    private boolean e;
    private a f;
    private int g;
    private int h;
    private int i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public AdListener l;
    private boolean m;
    private boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public long p;
    private d.a q;

    static /* synthetic */ void a(AdView adView, d dVar) {
        if (adView.l == null) {
            return;
        }
        if (adView.c == null || adView.c.getParent() == null) {
            try {
                adView.l.onReceiveAd(adView);
            } catch (Exception e2) {
                Log.w(AdManager.LOG, "Unhandled exception raised in your AdListener.onReceiveAd.", e2);
            }
        } else {
            try {
                adView.l.onReceiveRefreshedAd(adView);
            } catch (Exception e3) {
                Log.w(AdManager.LOG, "Unhandled exception raised in your AdListener.onReceiveRefreshedAd.", e3);
            }
        }
    }

    static /* synthetic */ void a(AdView adView, g gVar) {
        adView.c = gVar;
        if (adView.m) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            adView.startAnimation(alphaAnimation);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.j.<init>(float, float, float, float, float, boolean):void
     arg types: [int, int, float, float, float, int]
     candidates:
      com.admob.android.ads.j.<init>(float[], float[], float, float, float, boolean):void
      com.admob.android.ads.j.<init>(float, float, float, float, float, boolean):void */
    static /* synthetic */ void b(AdView adView, final g gVar) {
        gVar.setVisibility(8);
        j jVar = new j(0.0f, -90.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), true);
        jVar.setDuration(700);
        jVar.setFillAfter(true);
        jVar.setInterpolator(new AccelerateInterpolator());
        jVar.setAnimationListener(new Animation.AnimationListener() {
            public final void onAnimationStart(Animation animation) {
            }

            public final void onAnimationEnd(Animation animation) {
                AdView.this.post(new c(gVar, AdView.this));
            }

            public final void onAnimationRepeat(Animation animation) {
            }
        });
        adView.startAnimation(jVar);
    }

    static /* synthetic */ void c(AdView adView) {
        if (adView.l != null) {
            a.post(new d(adView));
        }
    }

    static /* synthetic */ d.a d(AdView adView) {
        if (adView.q == null) {
            adView.q = new d.a(adView);
        }
        return adView.q;
    }

    public AdView(Activity activity) {
        this(activity, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        int i3;
        int i4;
        int i5;
        this.n = true;
        setDescendantFocusability(262144);
        setClickable(true);
        setLongClickable(false);
        setGravity(17);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            if (attributeSet.getAttributeBooleanValue(str, "testing", false)) {
                Log.w(AdManager.LOG, "AdView's \"testing\" XML attribute has been deprecated and will be ignored.  Please delete it from your XML layout and use AdManager.setTestDevices instead.");
            }
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -16777216);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            if (attributeUnsignedIntValue2 >= 0) {
                setTextColor(attributeUnsignedIntValue2);
            }
            int attributeUnsignedIntValue3 = attributeSet.getAttributeUnsignedIntValue(str, "primaryTextColor", -1);
            int attributeUnsignedIntValue4 = attributeSet.getAttributeUnsignedIntValue(str, "secondaryTextColor", -1);
            this.j = attributeSet.getAttributeValue(str, "keywords");
            setRequestInterval(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "isGoneWithoutAd", false);
            if (attributeBooleanValue) {
                setGoneWithoutAd(attributeBooleanValue);
            }
            i3 = attributeUnsignedIntValue4;
            int i6 = attributeUnsignedIntValue3;
            i5 = attributeUnsignedIntValue;
            i4 = i6;
        } else {
            i3 = -1;
            i4 = -1;
            i5 = -16777216;
        }
        setBackgroundColor(i5);
        setPrimaryTextColor(i4);
        setSecondaryTextColor(i3);
        this.c = null;
        this.q = null;
        if (b == null) {
            b = Boolean.valueOf(a(context));
        }
        if (b.booleanValue()) {
            TextView textView = new TextView(context, attributeSet, i2);
            textView.setBackgroundColor(getBackgroundColor());
            textView.setTextColor(getPrimaryTextColor());
            textView.setPadding(10, 10, 10, 10);
            textView.setTextSize(16.0f);
            textView.setGravity(16);
            textView.setText("Ads by AdMob");
            addView(textView, new RelativeLayout.LayoutParams(-1, -1));
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        d b2;
        super.onMeasure(i2, i3);
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i3);
        int mode2 = View.MeasureSpec.getMode(i3);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            i4 = size;
        } else {
            i4 = AdManager.getScreenWidth(getContext());
        }
        if (mode2 == 1073741824) {
            i5 = size2;
        } else if (this.c == null || (b2 = this.c.b()) == null) {
            i5 = 0;
        } else {
            int a2 = b2.a(b2.f());
            if (mode2 != Integer.MIN_VALUE || size2 >= a2) {
                i5 = a2;
            } else {
                Log.w(AdManager.LOG, "Cannot display ad because its container is too small.  The ad is " + a2 + " pixels tall but is only given " + size2 + " at most to draw into.  Please make your view containing AdView taller.");
                i5 = 0;
            }
        }
        setMeasuredDimension(i4, i5);
        if (Log.isLoggable(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "AdView.onMeasure:  widthSize " + size + " heightSize " + size2);
            Log.v(AdManager.LOG, "AdView.onMeasure:  measuredWidth " + i4 + " measuredHeight " + i5);
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (Log.isLoggable(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "AdView size is " + measuredWidth + " by " + measuredHeight);
        }
        if (this.n && !b.booleanValue()) {
            a();
        }
    }

    private static class d implements Runnable {
        private WeakReference<AdView> a;

        public d(AdView adView) {
            this.a = new WeakReference<>(adView);
        }

        public final void run() {
            AdView adView = this.a.get();
            if (adView == null) {
                return;
            }
            if (adView.c == null || adView.c.getParent() == null) {
                try {
                    adView.l.onFailedToReceiveAd(adView);
                } catch (Exception e) {
                    Log.w(AdManager.LOG, "Unhandled exception raised in your AdListener.onFailedToReceiveAd.", e);
                }
            } else {
                try {
                    adView.l.onFailedToReceiveRefreshedAd(adView);
                } catch (Exception e2) {
                    Log.w(AdManager.LOG, "Unhandled exception raised in your AdListener.onFailedToReceiveRefreshedAd.", e2);
                }
            }
        }
    }

    public void requestFreshAd() {
        if (!this.e) {
            long uptimeMillis = (SystemClock.uptimeMillis() - this.p) / 1000;
            if (uptimeMillis <= 0 || uptimeMillis >= 13) {
                if (c()) {
                    a();
                }
            } else if (Log.isLoggable(AdManager.LOG, 3)) {
                Log.d(AdManager.LOG, "Ignoring requestFreshAd.  Called " + uptimeMillis + " seconds since last refresh.  " + "Refreshes must be at least " + 13 + " apart.");
            }
        } else if (Log.isLoggable(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Request interval overridden by the server.  Ignoring requestFreshAd.");
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        t.a(getContext());
        if (!this.n && super.getVisibility() != 0) {
            Log.w(AdManager.LOG, "Cannot requestFreshAd() when the AdView is not visible.  Call AdView.setVisibility(View.VISIBLE) first.");
        } else if (!this.o) {
            this.o = true;
            this.p = SystemClock.uptimeMillis();
            new a(this).start();
        } else if (Log.isLoggable(AdManager.LOG, 5)) {
            Log.w(AdManager.LOG, "Ignoring requestFreshAd() because we are requesting an ad right now already.");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(d dVar, g gVar) {
        int visibility = super.getVisibility();
        double a2 = dVar.a();
        if (a2 >= 0.0d) {
            this.e = true;
            setRequestInterval((int) a2);
            a(true);
        } else {
            this.e = false;
        }
        boolean z = this.n;
        if (z) {
            this.n = false;
        }
        gVar.a(dVar);
        gVar.setVisibility(visibility);
        gVar.setGravity(17);
        dVar.a(gVar);
        gVar.setLayoutParams(new RelativeLayout.LayoutParams(dVar.a(dVar.e()), dVar.a(dVar.f())));
        a.post(new b(this, gVar, visibility, z));
    }

    private static class b implements Runnable {
        private WeakReference<AdView> a;
        private WeakReference<g> b;
        private int c;
        private boolean d;

        public b(AdView adView, g gVar, int i, boolean z) {
            this.a = new WeakReference<>(adView);
            this.b = new WeakReference<>(gVar);
            this.c = i;
            this.d = z;
        }

        public final void run() {
            try {
                AdView adView = this.a.get();
                g gVar = this.b.get();
                if (adView != null && gVar != null) {
                    adView.addView(gVar);
                    AdView.a(adView, gVar.b());
                    if (this.c != 0) {
                        g unused = adView.c = gVar;
                    } else if (this.d) {
                        AdView.a(adView, gVar);
                    } else {
                        AdView.b(adView, gVar);
                    }
                }
            } catch (Exception e) {
                Log.e(AdManager.LOG, "Unhandled exception placing AdContainer into AdView.", e);
            }
        }
    }

    public int getRequestInterval() {
        return this.d / 1000;
    }

    public void setRequestInterval(int i2) {
        int i3 = i2 * 1000;
        if (this.d != i3) {
            if (i2 > 0) {
                if (i2 < 13) {
                    Log.w(AdManager.LOG, "AdView.setRequestInterval(" + i2 + ") seconds must be >= " + 13);
                    i3 = 13000;
                } else if (i2 > 600) {
                    Log.w(AdManager.LOG, "AdView.setRequestInterval(" + i2 + ") seconds must be <= " + 600);
                    i3 = 600000;
                }
            }
            this.d = i3;
            if (i2 <= 0) {
                b();
            }
            Log.i(AdManager.LOG, "Requesting fresh ads every " + i2 + " seconds.");
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.d > 0 && getVisibility() == 0) {
                    int i2 = this.d;
                    b();
                    if (c()) {
                        this.f = new a(this);
                        a.postDelayed(this.f, (long) i2);
                        if (Log.isLoggable(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "Ad refresh scheduled for " + i2 + " from now.");
                        }
                    }
                }
            }
            if (!z || this.d == 0) {
                b();
            }
        }
    }

    private void b() {
        if (this.f != null) {
            this.f.a = true;
            this.f = null;
            if (Log.isLoggable(AdManager.LOG, 2)) {
                Log.v(AdManager.LOG, "Cancelled an ad refresh scheduled for the future.");
            }
        }
    }

    private boolean c() {
        d b2;
        if (this.c == null || (b2 = this.c.b()) == null || !b2.d() || this.c.g() >= 120) {
            return true;
        }
        if (Log.isLoggable(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Cannot refresh CPM ads.  Ignoring request to refresh the ad.");
        }
        return false;
    }

    private static class a implements Runnable {
        boolean a;
        private WeakReference<AdView> b;

        public a(AdView adView) {
            this.b = new WeakReference<>(adView);
        }

        public final void run() {
            try {
                AdView adView = this.b.get();
                if (!this.a && adView != null) {
                    if (Log.isLoggable(AdManager.LOG, 3)) {
                        int h = adView.d / 1000;
                        if (Log.isLoggable(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "Requesting a fresh ad because a request interval passed (" + h + " seconds).");
                        }
                    }
                    adView.a();
                }
            } catch (Exception e) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in RefreshHandler.run(), " + e.getMessage());
                }
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        a(z);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        a(i2 == 0);
    }

    @Deprecated
    public void setTextColor(int i2) {
        Log.w(AdManager.LOG, "Calling the deprecated method setTextColor!  Please use setPrimaryTextColor and setSecondaryTextColor instead.");
        setPrimaryTextColor(i2);
        setSecondaryTextColor(i2);
    }

    @Deprecated
    public int getTextColor() {
        Log.w(AdManager.LOG, "Calling the deprecated method getTextColor!  Please use getPrimaryTextColor and getSecondaryTextColor instead.");
        return getPrimaryTextColor();
    }

    public int getPrimaryTextColor() {
        return this.h;
    }

    public void setPrimaryTextColor(int i2) {
        this.h = -16777216 | i2;
    }

    public int getSecondaryTextColor() {
        return this.i;
    }

    public void setSecondaryTextColor(int i2) {
        this.i = -16777216 | i2;
    }

    public void setBackgroundColor(int i2) {
        this.g = -16777216 | i2;
        invalidate();
    }

    public int getBackgroundColor() {
        return this.g;
    }

    public String getKeywords() {
        return this.j;
    }

    public void setKeywords(String str) {
        this.j = str;
    }

    public String getSearchQuery() {
        return this.k;
    }

    public void setSearchQuery(String str) {
        this.k = str;
    }

    @Deprecated
    public void setGoneWithoutAd(boolean z) {
        Log.w(AdManager.LOG, "Deprecated method setGoneWithoutAd was called.  See JavaDoc for instructions to remove.");
    }

    @Deprecated
    public boolean isGoneWithoutAd() {
        Log.w(AdManager.LOG, "Deprecated method isGoneWithoutAd was called.  See JavaDoc for instructions to remove.");
        return false;
    }

    public void setVisibility(int i2) {
        boolean z;
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                invalidate();
            }
        }
        if (i2 == 0) {
            z = true;
        } else {
            z = false;
        }
        a(z);
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    public void setAdListener(AdListener adListener) {
        synchronized (this) {
            this.l = adListener;
        }
    }

    public AdListener getAdListener() {
        return this.l;
    }

    public boolean hasAd() {
        return (this.c == null || this.c.b() == null) ? false : true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.m = true;
        a(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.m = false;
        a(false);
        super.onDetachedFromWindow();
    }

    private static final class c implements Runnable {
        private WeakReference<AdView> a;
        private WeakReference<g> b;

        public c(g gVar, AdView adView) {
            this.b = new WeakReference<>(gVar);
            this.a = new WeakReference<>(adView);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.admob.android.ads.j.<init>(float, float, float, float, float, boolean):void
         arg types: [int, int, float, float, float, int]
         candidates:
          com.admob.android.ads.j.<init>(float[], float[], float, float, float, boolean):void
          com.admob.android.ads.j.<init>(float, float, float, float, float, boolean):void */
        public final void run() {
            try {
                final AdView adView = this.a.get();
                final g gVar = this.b.get();
                if (adView != null && gVar != null) {
                    final g a2 = adView.c;
                    if (a2 != null) {
                        a2.setVisibility(8);
                    }
                    gVar.setVisibility(0);
                    j jVar = new j(90.0f, 0.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), false);
                    jVar.setDuration(700);
                    jVar.setFillAfter(true);
                    jVar.setInterpolator(new DecelerateInterpolator());
                    jVar.setAnimationListener(new Animation.AnimationListener(this) {
                        public final void onAnimationStart(Animation animation) {
                        }

                        public final void onAnimationEnd(Animation animation) {
                            if (a2 != null) {
                                adView.removeView(a2);
                            }
                            g unused = adView.c = gVar;
                            if (a2 != null) {
                                a2.d();
                            }
                        }

                        public final void onAnimationRepeat(Animation animation) {
                        }
                    });
                    adView.startAnimation(jVar);
                }
            } catch (Exception e) {
                if (Log.isLoggable(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "exception caught in SwapViews.run(), " + e.getMessage());
                }
            }
        }
    }

    public void cleanup() {
        if (this.c != null) {
            this.c.d();
            this.c = null;
        }
    }

    private static boolean a(Context context) {
        try {
            if (Class.forName("org.json.JSONException") != null) {
                return false;
            }
        } catch (ClassNotFoundException e2) {
        }
        return AdManager.getUserId(context) == null;
    }
}
