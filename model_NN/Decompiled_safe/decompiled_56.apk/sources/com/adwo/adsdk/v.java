package com.adwo.adsdk;

import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

final class v extends WebChromeClient {
    private /* synthetic */ C0009j a;

    v(C0009j jVar) {
        this.a = jVar;
    }

    public final void onReceivedTitle(WebView webView, String str) {
        this.a.a().setTitle(str);
        super.onReceivedTitle(webView, str);
    }

    public final void onProgressChanged(WebView webView, int i) {
        this.a.a().setProgress(i * 100);
        super.onProgressChanged(webView, i);
    }

    public final void onGeolocationPermissionsHidePrompt() {
        super.onGeolocationPermissionsHidePrompt();
    }

    public final void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        super.onGeolocationPermissionsShowPrompt(str, callback);
        callback.invoke(str, true, false);
    }
}
