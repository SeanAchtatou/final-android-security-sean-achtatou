package com.adwo.adsdk;

import android.os.Handler;
import android.os.Message;
import com.mobclick.android.ReportPolicy;

/* renamed from: com.adwo.adsdk.k  reason: case insensitive filesystem */
final class C0010k extends Handler {
    private /* synthetic */ C0009j a;

    C0010k(C0009j jVar) {
        this.a = jVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.j.a(com.adwo.adsdk.j, boolean):void
     arg types: [com.adwo.adsdk.j, int]
     candidates:
      com.adwo.adsdk.j.a(java.lang.String, android.app.Activity):void
      com.adwo.adsdk.j.a(com.adwo.adsdk.j, boolean):void */
    public final void handleMessage(Message message) {
        switch (message.what) {
            case ReportPolicy.BATCH_AT_TERMINATE /*2*/:
                C0009j.a(this.a, true);
                return;
            default:
                return;
        }
    }
}
