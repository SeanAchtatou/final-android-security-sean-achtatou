package com.x25.apps.CarMark;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import com.mobclick.android.MobclickAgent;

public class Main extends Activity {
    private Ads ads;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        WebView wv = (WebView) findViewById(R.id.car_mark);
        wv.loadUrl("file:///android_asset/index.html");
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setBackgroundColor(0);
        MobclickAgent.update(this);
        this.ads = new Ads(this);
        this.ads.getAd().getWbs();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        new AlertDialog.Builder(this).setTitle("退出").setMessage("确定要退出[汽车标志大全]?").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Main.this.finish();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
