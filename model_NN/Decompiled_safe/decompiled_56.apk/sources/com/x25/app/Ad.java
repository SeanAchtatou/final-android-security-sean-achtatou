package com.x25.app;

import com.mobclick.android.ReportPolicy;

public class Ad {
    private static final char a = ((char) Integer.parseInt("00000011", 2));
    private static final char b = ((char) Integer.parseInt("00001111", 2));
    private static final char c = ((char) Integer.parseInt("00111111", 2));
    private static final char d = ((char) Integer.parseInt("11111100", 2));
    private static final char e = ((char) Integer.parseInt("11110000", 2));
    private static final char f = ((char) Integer.parseInt("11000000", 2));
    private static final char[] g = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    static String a(byte[] paramArrayOfByte) {
        StringBuffer localStringBuffer = new StringBuffer(((int) (((double) paramArrayOfByte.length) * 1.34d)) + 3);
        int i = 0;
        int j = 0;
        for (int k = 0; k < paramArrayOfByte.length; k++) {
            i %= 8;
            while (i < 8) {
                switch (i) {
                    case 0:
                        j = (char) (((char) (paramArrayOfByte[k] & d)) >>> 2);
                        break;
                    case ReportPolicy.BATCH_AT_TERMINATE:
                        j = (char) (paramArrayOfByte[k] & c);
                        break;
                    case ReportPolicy.DAILY:
                        j = (char) (((char) (paramArrayOfByte[k] & b)) << 2);
                        if (k + 1 >= paramArrayOfByte.length) {
                            break;
                        } else {
                            j = (char) (((paramArrayOfByte[k + 1] & f) >>> 6) | j);
                            break;
                        }
                    case 6:
                        j = (char) (((char) (paramArrayOfByte[k] & a)) << 4);
                        if (k + 1 >= paramArrayOfByte.length) {
                            break;
                        } else {
                            j = (char) (((paramArrayOfByte[k + 1] & e) >>> 4) | j);
                            break;
                        }
                }
                localStringBuffer.append(g[j]);
                i += 6;
            }
        }
        if (localStringBuffer.length() % 4 != 0) {
            for (int k2 = 4 - (localStringBuffer.length() % 4); k2 > 0; k2--) {
                localStringBuffer.append("=");
            }
        }
        return localStringBuffer.toString();
    }
}
