package com.vpon.adon.android;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.mobclick.android.ReportPolicy;
import com.mt.airad.AirAD;
import com.vpon.adon.android.entity.Ad;
import com.vpon.adon.android.utils.VPLog;

public final class AdView extends RelativeLayout {
    private AdListener adListener;
    /* access modifiers changed from: private */
    public Runnable adRefreshCount;
    /* access modifiers changed from: private */
    public Handler adRefreshHandler;
    /* access modifiers changed from: private */
    public Handler connectHandler;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public Ad currentAd;
    private AdDisplay currentAdDisplay;
    /* access modifiers changed from: private */
    public boolean isAutoRefreshAd;
    /* access modifiers changed from: private */
    public boolean isLive;
    /* access modifiers changed from: private */
    public boolean isPause;
    private String licenseKey;
    private Ad nextAd;
    private AdDisplay nextAdDisplay;
    private AdOnPlatform platform;

    public AdView(Context context2) {
        this(context2, null, 0);
    }

    public AdView(Context context2, AttributeSet attrs) {
        this(context2, attrs, 0);
    }

    public AdView(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        this.licenseKey = null;
        this.isLive = false;
        this.isPause = false;
        this.nextAd = null;
        this.currentAd = null;
        this.currentAdDisplay = null;
        this.nextAdDisplay = null;
        this.isAutoRefreshAd = false;
        this.adRefreshCount = new Runnable() {
            public void run() {
                if (AdView.this.isAutoRefreshAd && AdView.this.isLive && !AdView.this.isPause && AdView.this.currentAd != null && AdView.this.currentAd.getRefreshTime() > 0) {
                    int refresh = AdView.this.currentAd.getRefreshTime() - 1;
                    AdView.this.currentAd.setRefreshTime(refresh);
                    if (refresh == 0) {
                        AdManager.instance(AdView.this.context).performAdRequester(AdView.this, AdView.this.connectHandler);
                    }
                }
                if (AdView.this.adRefreshHandler != null) {
                    AdView.this.adRefreshHandler.postDelayed(AdView.this.adRefreshCount, 1000);
                }
            }
        };
        this.currentAd = null;
        this.nextAd = null;
        this.context = context2;
        this.connectHandler = new Handler();
        this.currentAdDisplay = new AdDisplay(context2, this);
        addView(this.currentAdDisplay);
        AdManager.instance(context2).addAdView(this);
    }

    public String getVersion() {
        return AppConfig.SDKVERSION;
    }

    public void setLicenseKey(String licenseKey2, AdOnPlatform platform2, boolean autoRefreshAd) {
        this.licenseKey = licenseKey2;
        this.isAutoRefreshAd = autoRefreshAd;
        this.platform = platform2;
        AdManager.instance(this.context).performAdRequester(this, this.connectHandler);
        if (this.adRefreshHandler == null) {
            this.adRefreshHandler = new Handler();
            this.adRefreshHandler.post(this.adRefreshCount);
        }
    }

    public void setAdListener(AdListener listener) {
        this.adListener = listener;
    }

    public void refreshAd() {
        this.isAutoRefreshAd = false;
        AdManager.instance(this.context).performAdRequester(this, this.connectHandler);
    }

    public void pauseAdAutoRefresh() {
        this.isPause = true;
    }

    public void restartAdAutoRefresh() {
        if (this.isAutoRefreshAd) {
            this.isPause = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void performAdManagerAddClickResp() {
        AdManager.instance(this.context).performClickRequester(this, this.currentAd);
    }

    /* access modifiers changed from: package-private */
    public void performAdManagerErrorRequester(String errorMessage) {
        AdManager.instance(this.context).performErrorRequester(this, errorMessage);
    }

    /* access modifiers changed from: package-private */
    public void displayNextAd(Ad ad) {
        VPLog.i("AdView", "displayNextAd");
        if (ad == null) {
            getAdError();
            return;
        }
        this.nextAd = ad;
        try {
            this.nextAdDisplay = new AdDisplay(this.context, this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.nextAd.getAdWidth(), this.nextAd.getAdHeight());
            layoutParams.addRule(13);
            this.nextAdDisplay.setLayoutParams(layoutParams);
            this.nextAdDisplay.displayNextAd(this.nextAd, this);
        } catch (Exception e) {
            AdManager.instance(this.context).performErrorRequester(this, e.getMessage());
            getAdError();
        }
    }

    private void getAdError() {
        if (this.adListener != null) {
            this.adListener.onFailedToRecevieAd(this);
        }
        if (this.currentAd != null) {
            this.currentAd.setRefreshTime(120);
            return;
        }
        Ad nextAd2 = new Ad();
        nextAd2.setRefreshTime(120);
        this.currentAd = null;
        this.currentAd = nextAd2;
    }

    /* access modifiers changed from: package-private */
    public void onRecivedAd() {
        removeView(this.currentAdDisplay);
        this.currentAd = this.nextAd;
        this.currentAdDisplay = this.nextAdDisplay;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.currentAd.getAdWidth(), this.currentAd.getAdHeight());
        layoutParams.addRule(13);
        addView(this.currentAdDisplay, layoutParams);
        VPLog.i("width", String.valueOf(this.currentAd.getAdWidth()));
        VPLog.i("width", String.valueOf(this.currentAd.getAdHeight()));
        if (this.adListener != null) {
            this.adListener.onRecevieAd(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int arg0) {
        switch (arg0) {
            case 0:
                this.isLive = true;
                return;
            case ReportPolicy.DAILY:
                this.isLive = false;
                return;
            case AirAD.ANIMATION_FIXED:
                this.isLive = false;
                return;
            default:
                return;
        }
    }

    public AdOnPlatform getPlatform() {
        return this.platform;
    }

    public String getLicenseKey() {
        return this.licenseKey;
    }

    public void setLicenseKey(String licenseKey2) {
        this.licenseKey = licenseKey2;
    }
}
