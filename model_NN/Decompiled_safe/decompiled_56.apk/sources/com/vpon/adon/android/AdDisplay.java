package com.vpon.adon.android;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.vpon.adon.android.entity.Ad;
import com.vpon.adon.android.utils.LocationUtil;
import com.vpon.adon.android.utils.VPLog;

public final class AdDisplay extends WebView implements LocationListener {
    public static final String vponUrl = "http://www.vpon.com/v/index.jsp";
    private Ad ad;
    private Context context;

    AdDisplay(Context context2, AdView adView) {
        super(context2, null, 0);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        getSettings().setSupportZoom(true);
        setWebChromeClient(new WebChromeClient());
        this.context = context2;
        LocationUtil.instance(getContext()).addLocationListener(this);
    }

    public void displayNextAd(Ad ad2, AdView adView) {
        VPLog.i("AdDisplay", "displayNextAd");
        VPLog.i("AdDisplay", "html: " + ad2.getAdHtml());
        this.ad = ad2;
        setWebViewClient(new AdDisplayWebViewClient(adView, ad2, this.context));
        String html = ad2.getAdHtml();
        if (ad2.getAdRedirectPack().getLocation() != null) {
            html = html.replace("<div id=\"distance\"></div>", getDistanceHtml(ad2.getAdRedirectPack().getLocation()));
        }
        loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
    }

    private String getDistanceHtml(Location location) {
        double Lat1r = 0.017453292519943295d * this.ad.getMapLat();
        double Long1r = 0.017453292519943295d * this.ad.getMapLon();
        double Lat2r = 0.017453292519943295d * location.getLatitude();
        double d = Math.acos((Math.sin(Lat1r) * Math.sin(Lat2r)) + (Math.cos(Lat1r) * Math.cos(Lat2r) * Math.cos((0.017453292519943295d * location.getLongitude()) - Long1r))) * 6371.0d;
        if (Double.isNaN(d)) {
            return "<div id=\"distance\"></div>";
        }
        int distanceMeter = (int) (1000.0d * d);
        if (distanceMeter < 1000) {
            return "<div id=\"distance\">" + String.valueOf(distanceMeter) + "<br />" + "m" + "</div>";
        }
        double distanceKiloMeter = (double) (distanceMeter / 1000);
        if (distanceKiloMeter >= 1000.0d) {
            return "<div id=\"distance\"></div>";
        }
        String distanceString = String.valueOf(distanceKiloMeter);
        if (distanceString.length() > 3) {
            distanceString = distanceString.substring(0, 3);
        }
        if (distanceString.substring(distanceString.length() - 1, distanceString.length()).equalsIgnoreCase(".")) {
            distanceString = distanceString.substring(0, distanceString.length() - 1);
        }
        return "<div id=\"distance\">" + distanceString + "<br />" + "km" + "</div>";
    }

    public void onLocationChanged(Location location) {
        if (location != null && this.ad != null && !Double.isNaN(this.ad.getMapLat()) && !Double.isNaN(this.ad.getMapLon())) {
            if (this.ad.getMapLat() != 0.0d || this.ad.getMapLon() != 0.0d) {
                loadDataWithBaseURL(null, this.ad.getAdHtml().replace("<div id=\"distance\"></div>", getDistanceHtml(location)), "text/html", "utf-8", null);
            }
        }
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
