package com.vpon.adon.android.entity;

import android.location.Location;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class AdRedirectPack implements Serializable {
    private static final long serialVersionUID = -7243498860564568402L;
    private String adId;
    private String cellId;
    private String imei;
    private String lac;
    private Double lat;
    private String licensekey;
    private Location location;
    private Double lon;
    private String mcc;
    private String mnc;

    public String toString() {
        JSONObject json = new JSONObject();
        try {
            json.put("imei", this.imei);
            json.put("licensekey", this.licensekey);
            json.put("adId", this.adId);
            json.put("lat", this.lat);
            json.put("lon", this.lon);
            json.put("cellId", this.cellId);
            json.put("lac", this.lac);
            json.put("mcc", this.mcc);
            json.put("mnc", this.mnc);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public String getAdId() {
        return this.adId;
    }

    public void setAdId(String adId2) {
        this.adId = adId2;
    }

    public String getLicensekey() {
        return this.licensekey;
    }

    public void setLicensekey(String licensekey2) {
        this.licensekey = licensekey2;
    }

    public String getImei() {
        return this.imei;
    }

    public void setImei(String imei2) {
        this.imei = imei2;
    }

    public Double getLat() {
        return this.lat;
    }

    public void setLat(Double lat2) {
        this.lat = lat2;
    }

    public Double getLon() {
        return this.lon;
    }

    public void setLon(Double lon2) {
        this.lon = lon2;
    }

    public String getMcc() {
        return this.mcc;
    }

    public void setMcc(String mcc2) {
        this.mcc = mcc2;
    }

    public String getMnc() {
        return this.mnc;
    }

    public void setMnc(String mnc2) {
        this.mnc = mnc2;
    }

    public String getLac() {
        return this.lac;
    }

    public void setLac(String lac2) {
        this.lac = lac2;
    }

    public String getCellId() {
        return this.cellId;
    }

    public void setCellId(String cellId2) {
        this.cellId = cellId2;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }
}
