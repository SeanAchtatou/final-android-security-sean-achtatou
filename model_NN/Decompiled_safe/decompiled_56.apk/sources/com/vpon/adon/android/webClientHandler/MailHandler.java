package com.vpon.adon.android.webClientHandler;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.vpon.adon.android.entity.Ad;

public class MailHandler extends WebClientHandler {
    public MailHandler(WebClientHandler next) {
        super(next);
    }

    public boolean handle(Context context, Ad ad, String url) {
        if (!url.startsWith("mailto:")) {
            return doNext(context, ad, url);
        }
        context.startActivity(new Intent("android.intent.action.SENDTO", Uri.parse(url)));
        return true;
    }
}
