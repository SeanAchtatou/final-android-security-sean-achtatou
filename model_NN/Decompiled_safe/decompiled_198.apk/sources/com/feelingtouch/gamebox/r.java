package com.feelingtouch.gamebox;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.feelingtouch.e.k;
import com.feelingtouch.imagelazyload.b;
import com.feelingtouch.imagelazyload.c;
import com.feelingtouch.shooting.R;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ScoreAdapter */
public final class r extends BaseAdapter {
    private ArrayList a = new ArrayList();
    private Activity b;
    private LayoutInflater c;
    private Drawable d;
    private b e;

    public r(Context context, ArrayList arrayList, Drawable drawable, b bVar) {
        this.b = (Activity) context;
        this.a = arrayList;
        this.c = (LayoutInflater) this.b.getSystemService("layout_inflater");
        this.d = drawable;
        this.e = bVar;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        s sVar;
        View view3;
        try {
            HashMap hashMap = (HashMap) this.a.get(i);
            if (view == null) {
                view2 = this.c.inflate((int) R.layout.gamebox_highscore_list_row, (ViewGroup) null);
                try {
                    s sVar2 = new s();
                    sVar2.a = (ImageView) view2.findViewById(R.id.flag);
                    sVar2.b = (ProgressBar) view2.findViewById(R.id.progress_bar);
                    sVar2.c = (TextView) view2.findViewById(R.id.name);
                    sVar2.d = (TextView) view2.findViewById(R.id.rank);
                    sVar2.e = (TextView) view2.findViewById(R.id.score);
                    sVar2.f = (ImageView) view2.findViewById(R.id.rank_img);
                    view2.setTag(sVar2);
                    sVar = sVar2;
                    view3 = view2;
                } catch (OutOfMemoryError e2) {
                    e = e2;
                    e.printStackTrace();
                    this.b.finish();
                    return view2;
                }
            } else {
                sVar = (s) view.getTag();
                view3 = view;
            }
            try {
                String str = (String) hashMap.get("flag_link");
                Drawable a2 = c.a(str);
                if (a2 != null) {
                    sVar.a.setImageDrawable(a2);
                    sVar.b.setVisibility(8);
                } else {
                    sVar.a.setImageDrawable(this.d);
                    if (k.a(str)) {
                        sVar.b.setVisibility(8);
                    } else {
                        sVar.b.setVisibility(0);
                        new c(str, this.e, sVar.a, this.d, sVar.b, "/sdcard/.gamebox/");
                    }
                }
                int intValue = ((Integer) hashMap.get("rank")).intValue();
                sVar.c.setText((String) hashMap.get("name"));
                sVar.d.setText("Rank:" + intValue);
                sVar.e.setText("Score:" + ((Integer) hashMap.get("score")).intValue());
                if (intValue <= 0 || intValue >= 4) {
                    sVar.f.setVisibility(8);
                    return view3;
                }
                sVar.f.setVisibility(0);
                if (intValue == 1) {
                    sVar.f.setImageResource(R.drawable.gamebox_rank1);
                }
                if (intValue == 2) {
                    sVar.f.setImageResource(R.drawable.gamebox_rank2);
                }
                if (intValue != 3) {
                    return view3;
                }
                sVar.f.setImageResource(R.drawable.gamebox_rank3);
                return view3;
            } catch (OutOfMemoryError e3) {
                e = e3;
                view2 = view3;
                e.printStackTrace();
                this.b.finish();
                return view2;
            }
        } catch (OutOfMemoryError e4) {
            e = e4;
            view2 = view;
            e.printStackTrace();
            this.b.finish();
            return view2;
        }
    }
}
