package com.feelingtouch.gamebox;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.feelingtouch.c.c;
import com.feelingtouch.d.d.a.a;
import com.feelingtouch.imagelazyload.b;
import com.feelingtouch.shooting.R;
import java.util.List;

/* compiled from: GameBoxAdapter */
public final class g extends BaseAdapter {
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public a[] b;
    private b c;
    private LayoutInflater d;
    private Drawable e;

    public g(Context context, List list, b bVar, Drawable drawable) {
        this.a = context;
        if (list == null) {
            c.a.a(getClass(), "games==null!!!!!!!!!!!!!!!!!!!");
        } else {
            this.b = (a[]) list.toArray(new a[0]);
        }
        this.c = bVar;
        this.e = drawable;
        this.d = (LayoutInflater) this.a.getSystemService("layout_inflater");
    }

    public final int getCount() {
        return this.b.length;
    }

    public final Object getItem(int i) {
        return this.b[i];
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        i iVar;
        View view2;
        String str = this.b[i].a;
        String str2 = this.b[i].c;
        String str3 = String.valueOf(this.b[i].h) + ", downloads";
        String str4 = this.b[i].e;
        c.a.a(getClass(), "url:", str2);
        if (view == null) {
            View inflate = this.d.inflate((int) R.layout.gamebox_list_row, (ViewGroup) null);
            i iVar2 = new i();
            iVar2.a = (ImageView) inflate.findViewById(R.id.icon);
            iVar2.b = (ProgressBar) inflate.findViewById(R.id.progress_bar);
            iVar2.c = (TextView) inflate.findViewById(R.id.name);
            iVar2.d = (TextView) inflate.findViewById(R.id.downloads);
            iVar2.e = (TextView) inflate.findViewById(R.id.desc);
            iVar2.f = (Button) inflate.findViewById(R.id.download_button);
            inflate.setTag(iVar2);
            iVar = iVar2;
            view2 = inflate;
        } else {
            iVar = (i) view.getTag();
            view2 = view;
        }
        Drawable a2 = c.a(str2);
        if (a2 != null) {
            iVar.a.setImageDrawable(a2);
            iVar.b.setVisibility(8);
            iVar.a.setVisibility(0);
        } else {
            iVar.a.setImageDrawable(this.e);
            new com.feelingtouch.imagelazyload.c(str2, this.c, iVar.a, this.e, iVar.b, "/sdcard/.gamebox/");
        }
        iVar.c.setText(str);
        iVar.d.setText(str3);
        iVar.e.setText(str4);
        iVar.f.setOnClickListener(new h(this, i));
        return view2;
    }
}
