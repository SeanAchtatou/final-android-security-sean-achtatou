package com.feelingtouch.gamebox;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.widget.Button;
import com.feelingtouch.e.b;
import com.feelingtouch.imagelazyload.ListImageActivity;
import com.feelingtouch.shooting.R;

public class GameBox extends ListImageActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.gamebox);
        b.a(this);
        if (HighScore.a != null) {
            setListAdapter(new g(this, HighScore.a, this, new BitmapDrawable(getResources().openRawResource(R.drawable.gamebox_default_game))));
            getListView().setOnItemClickListener(new e(this));
        }
        ((Button) findViewById(R.id.back)).setOnClickListener(new f(this));
        System.gc();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (HighScore.a == null) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.gc();
        System.gc();
        System.gc();
    }
}
