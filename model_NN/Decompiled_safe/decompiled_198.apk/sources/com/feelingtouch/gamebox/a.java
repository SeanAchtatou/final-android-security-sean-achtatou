package com.feelingtouch.gamebox;

import android.content.Context;
import com.feelingtouch.gamebox.a.c;

/* compiled from: AnalysisUtil */
public class a {
    protected static a b;
    protected Context a;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (b == null) {
                b = new a();
            }
            aVar = b;
        }
        return aVar;
    }

    public final void a(Context context) {
        com.feelingtouch.gamebox.a.a.a(context);
        c cVar = new c();
        cVar.b = System.currentTimeMillis();
        cVar.c = 1;
        com.feelingtouch.gamebox.a.a.a(cVar);
        this.a = context;
        new b(this).execute(null);
    }

    public static void b(Context context) {
        com.feelingtouch.gamebox.a.a.a(context);
        c cVar = new c();
        cVar.b = System.currentTimeMillis();
        cVar.c = 2;
        cVar.d = "";
        com.feelingtouch.gamebox.a.a.a(cVar);
    }
}
