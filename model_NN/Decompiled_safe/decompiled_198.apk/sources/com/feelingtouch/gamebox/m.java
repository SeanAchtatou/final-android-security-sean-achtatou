package com.feelingtouch.gamebox;

import android.view.View;

/* compiled from: HighScore */
final class m implements View.OnClickListener {
    final /* synthetic */ HighScore a;

    m(HighScore highScore) {
        this.a = highScore;
    }

    public final void onClick(View view) {
        HighScore highScore = this.a;
        highScore.n = highScore.n - 8;
        if (this.a.n < 0) {
            this.a.n = 0;
        }
        HighScore.g(this.a);
    }
}
