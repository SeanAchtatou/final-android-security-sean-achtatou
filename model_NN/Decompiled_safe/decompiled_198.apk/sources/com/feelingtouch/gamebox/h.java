package com.feelingtouch.gamebox;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.feelingtouch.d.d.a;

/* compiled from: GameBoxAdapter */
final class h implements View.OnClickListener {
    final /* synthetic */ g a;
    private final /* synthetic */ int b;

    h(g gVar, int i) {
        this.a = gVar;
        this.b = i;
    }

    public final void onClick(View view) {
        try {
            a.c.a(this.a.b[this.b].b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.a.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:" + this.a.b[this.b].b)));
    }
}
