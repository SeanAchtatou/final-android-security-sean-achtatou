package com.feelingtouch.gamebox;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.feelingtouch.c.c;
import com.feelingtouch.d.d.a;
import com.feelingtouch.imagelazyload.ListImageActivity;
import com.feelingtouch.shooting.R;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HighScore extends ListImageActivity {
    public static List a = null;
    TextView b;
    TextView c;
    protected Handler d = new k(this);
    private List e = null;
    private View f;
    /* access modifiers changed from: private */
    public ProgressBar g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public Button i;
    private Button j;
    private Button k;
    private ArrayList l = new ArrayList();
    private String m;
    /* access modifiers changed from: private */
    public int n = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.gamebox_highscore);
            this.m = getIntent().getStringExtra("package_name");
            new File("/sdcard/.gamebox/").mkdirs();
            this.j = (Button) findViewById(R.id.prev);
            this.k = (Button) findViewById(R.id.next);
            this.j.setEnabled(false);
            this.k.setEnabled(false);
            this.b = (TextView) findViewById(R.id.your_rank);
            this.c = (TextView) findViewById(R.id.myrank);
            this.f = LayoutInflater.from(this).inflate((int) R.layout.gamebox_loading, (ViewGroup) null);
            getListView().addHeaderView(this.f);
            this.g = (ProgressBar) this.f.findViewById(R.id.progress);
            this.h = (TextView) this.f.findViewById(R.id.loading);
            this.i = (Button) this.f.findViewById(R.id.retry);
            this.i.setOnClickListener(new l(this));
            a();
            new o(this).execute(null);
            this.j.setOnClickListener(new m(this));
            this.k.setOnClickListener(new n(this));
            System.gc();
            System.gc();
        } catch (OutOfMemoryError e2) {
            finish();
        }
    }

    static /* synthetic */ void g(HighScore highScore) {
        highScore.l.clear();
        highScore.a();
        highScore.h.setVisibility(0);
        highScore.h.setText((int) R.string.gamebox_loading);
        highScore.g.setVisibility(0);
        highScore.j.setEnabled(false);
        highScore.k.setEnabled(false);
        new o(highScore).execute(null);
    }

    static /* synthetic */ void b(HighScore highScore) {
        try {
            highScore.e = a.c.b(highScore.m, highScore.n);
            int b2 = com.feelingtouch.e.a.a.b(highScore, "high_score", 0);
            p pVar = new p(highScore);
            if (b2 > 0) {
                pVar.b = b2;
                pVar.a = a.c.a(d.a(highScore), b2);
                pVar.c = com.feelingtouch.e.a.a.a(highScore, "high_score_name", "");
            }
            if (a == null) {
                a = a.c.a();
            }
            highScore.d.sendMessage(highScore.d.obtainMessage(1, pVar));
        } catch (Exception e2) {
            e2.printStackTrace();
            c.a.a(highScore.getClass(), e2.getMessage());
            if (highScore.d != null) {
                highScore.d.sendEmptyMessage(2);
            }
        }
    }

    private void a() {
        BitmapDrawable bitmapDrawable;
        try {
            bitmapDrawable = new BitmapDrawable(getResources().openRawResource(R.drawable.gamebox_default_flag));
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            bitmapDrawable = null;
        }
        setListAdapter(new r(this, this.l, bitmapDrawable, this));
    }

    static /* synthetic */ void a(HighScore highScore, p pVar) {
        for (int i2 = 0; i2 < highScore.e.size(); i2++) {
            HashMap hashMap = new HashMap();
            hashMap.put("rank", Integer.valueOf(i2 + 1 + highScore.n));
            hashMap.put("name", ((com.feelingtouch.d.d.a.c) highScore.e.get(i2)).a);
            hashMap.put("score", Integer.valueOf(((com.feelingtouch.d.d.a.c) highScore.e.get(i2)).c));
            hashMap.put("flag_link", ((com.feelingtouch.d.d.a.c) highScore.e.get(i2)).e);
            highScore.l.add(hashMap);
        }
        if (pVar.b != 0) {
            highScore.b.setVisibility(0);
            highScore.c.setVisibility(0);
            highScore.c.setText(new StringBuilder().append(pVar.a).toString());
        } else {
            highScore.b.setVisibility(8);
            highScore.c.setVisibility(8);
        }
        highScore.a();
        highScore.i.setVisibility(8);
        highScore.h.setVisibility(8);
        highScore.g.setVisibility(8);
        if (highScore.n == 0) {
            highScore.j.setEnabled(false);
        } else {
            highScore.j.setEnabled(true);
        }
        if (highScore.e.size() < 8) {
            highScore.k.setEnabled(false);
        } else {
            highScore.k.setEnabled(true);
        }
    }

    static /* synthetic */ void a(HighScore highScore) {
        highScore.i.setVisibility(0);
        highScore.h.setText((int) R.string.gamebox_no_connection);
        highScore.g.setVisibility(8);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.gc();
        System.gc();
        System.gc();
    }
}
