package com.feelingtouch.gamebox;

import android.view.View;
import com.feelingtouch.shooting.R;

/* compiled from: HighScore */
final class l implements View.OnClickListener {
    final /* synthetic */ HighScore a;

    l(HighScore highScore) {
        this.a = highScore;
    }

    public final void onClick(View view) {
        this.a.i.setVisibility(8);
        this.a.h.setText((int) R.string.gamebox_loading);
        this.a.g.setVisibility(0);
        new o(this.a).execute(null);
    }
}
