package com.feelingtouch.gamebox;

import android.graphics.drawable.Drawable;
import com.feelingtouch.e.d;
import java.io.File;

/* compiled from: Cache */
public final class c {
    public static final Drawable a(String str) {
        String str2 = "/sdcard/.gamebox/" + str.substring(str.lastIndexOf(47) + 1, str.length());
        File file = new File(str2);
        d.a(file);
        if (file.exists()) {
            return Drawable.createFromPath(str2);
        }
        return null;
    }
}
