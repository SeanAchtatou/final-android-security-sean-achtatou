package com.feelingtouch.gamebox;

import android.os.AsyncTask;

/* compiled from: AnalysisUtil */
public final class b extends AsyncTask {
    final /* synthetic */ a a;

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    public b(a aVar) {
        this.a = aVar;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Void a() {
        /*
            r14 = this;
            com.feelingtouch.c.b r0 = com.feelingtouch.c.c.a
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r1 = 0
            java.lang.String r2 = "POST DELAY THIS ANALYTIC REQUEST"
            r0[r1] = r2
            java.lang.Class<com.feelingtouch.gamebox.a> r9 = com.feelingtouch.gamebox.a.class
            monitor-enter(r9)
            com.feelingtouch.gamebox.a r0 = r14.a     // Catch:{ all -> 0x005f }
            android.content.Context r0 = r0.a     // Catch:{ all -> 0x005f }
            java.lang.String r1 = r0.getPackageName()     // Catch:{ all -> 0x005f }
            com.feelingtouch.gamebox.a r0 = r14.a     // Catch:{ all -> 0x005f }
            android.content.Context r0 = r0.a     // Catch:{ all -> 0x005f }
            java.lang.String r2 = com.feelingtouch.e.a.a(r0)     // Catch:{ all -> 0x005f }
            java.util.Calendar r0 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x005f }
            java.util.TimeZone r0 = r0.getTimeZone()     // Catch:{ all -> 0x005f }
            java.lang.String r3 = r0.getDisplayName()     // Catch:{ all -> 0x005f }
            com.feelingtouch.d.d.a r0 = com.feelingtouch.d.d.a.c     // Catch:{ all -> 0x005f }
            java.util.List r4 = com.feelingtouch.gamebox.a.a.a()     // Catch:{ all -> 0x005f }
            r5 = 0
            java.util.Iterator r10 = r4.iterator()     // Catch:{ all -> 0x005f }
            r11 = r5
        L_0x0035:
            boolean r4 = r10.hasNext()     // Catch:{ all -> 0x005f }
            if (r4 != 0) goto L_0x003e
            monitor-exit(r9)     // Catch:{ all -> 0x005f }
            r0 = 0
        L_0x003d:
            return r0
        L_0x003e:
            java.lang.Object r14 = r10.next()     // Catch:{ all -> 0x005f }
            com.feelingtouch.gamebox.a.c r14 = (com.feelingtouch.gamebox.a.c) r14     // Catch:{ all -> 0x005f }
            int r4 = r14.c     // Catch:{ all -> 0x005f }
            r5 = 1
            if (r4 != r5) goto L_0x0052
            if (r11 == 0) goto L_0x0050
            long r4 = r11.a     // Catch:{ all -> 0x005f }
            com.feelingtouch.gamebox.a.a.a(r4)     // Catch:{ all -> 0x005f }
        L_0x0050:
            r11 = r14
            goto L_0x0035
        L_0x0052:
            int r4 = r14.c     // Catch:{ all -> 0x005f }
            r5 = 2
            if (r4 != r5) goto L_0x0035
            if (r11 != 0) goto L_0x0062
            long r4 = r14.a     // Catch:{ all -> 0x005f }
            com.feelingtouch.gamebox.a.a.a(r4)     // Catch:{ all -> 0x005f }
            goto L_0x0035
        L_0x005f:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0062:
            long r6 = r11.b     // Catch:{ all -> 0x005f }
            long r4 = r14.b     // Catch:{ all -> 0x005f }
            long r12 = r11.b     // Catch:{ all -> 0x005f }
            long r4 = r4 - r12
            java.lang.String r8 = r14.d     // Catch:{ Exception -> 0x007b }
            r0.a(r1, r2, r3, r4, r6, r8)     // Catch:{ Exception -> 0x007b }
            long r4 = r11.a     // Catch:{ Exception -> 0x007b }
            com.feelingtouch.gamebox.a.a.a(r4)     // Catch:{ Exception -> 0x007b }
            long r4 = r14.a     // Catch:{ Exception -> 0x007b }
            com.feelingtouch.gamebox.a.a.a(r4)     // Catch:{ Exception -> 0x007b }
            r4 = 0
            r11 = r4
            goto L_0x0035
        L_0x007b:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x005f }
            r0 = 0
            monitor-exit(r9)     // Catch:{ all -> 0x005f }
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.gamebox.b.a():java.lang.Void");
    }
}
