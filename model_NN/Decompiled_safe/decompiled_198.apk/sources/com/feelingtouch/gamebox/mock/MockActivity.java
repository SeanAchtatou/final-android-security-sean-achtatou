package com.feelingtouch.gamebox.mock;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.feelingtouch.gamebox.HighScore;

public class MockActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = new Intent(this, HighScore.class);
        intent.putExtra("package_name", "com.feelingtouch.physical");
        startActivity(intent);
    }
}
