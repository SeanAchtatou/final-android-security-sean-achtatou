package com.feelingtouch.gamebox;

import android.os.Handler;
import android.os.Message;

/* compiled from: HighScore */
final class k extends Handler {
    final /* synthetic */ HighScore a;

    k(HighScore highScore) {
        this.a = highScore;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 1:
                HighScore.a(this.a, (p) message.obj);
                return;
            case 2:
                HighScore.a(this.a);
                return;
            default:
                return;
        }
    }
}
