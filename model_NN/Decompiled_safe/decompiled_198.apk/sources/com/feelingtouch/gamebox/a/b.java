package com.feelingtouch.gamebox.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/* compiled from: GameBoxDBHelper */
final class b extends SQLiteOpenHelper {
    public b(Context context) {
        super(context, "gamebox.db", (SQLiteDatabase.CursorFactory) null, 2);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table usage (_id INTEGER PRIMARY KEY AUTOINCREMENT,dateTime INTEGER,recordType INTEGER,paramString TEXT )");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.e("GameBoxDBHelper", "Upgrading database from version " + i + " to " + i2 + ", which will integrade all old data.");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS usage");
        onCreate(sQLiteDatabase);
    }
}
