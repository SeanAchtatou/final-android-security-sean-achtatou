package com.feelingtouch.gamebox.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.LinkedList;
import java.util.List;

/* compiled from: GameBoxDBHelper */
public final class a {
    protected static Context a;
    private static SQLiteDatabase b = null;

    public static void a(Context context) {
        a = context;
        if (b == null) {
            b = new b(context).getWritableDatabase();
        }
    }

    public static List a() {
        Cursor cursor;
        LinkedList linkedList = new LinkedList();
        try {
            Cursor query = b.query("usage", null, null, null, null, null, "_id asc");
            while (query.moveToNext()) {
                try {
                    long j = query.getLong(query.getColumnIndex("_id"));
                    long j2 = query.getLong(query.getColumnIndex("dateTime"));
                    int i = query.getInt(query.getColumnIndex("recordType"));
                    String string = query.getString(query.getColumnIndex("paramString"));
                    c cVar = new c();
                    cVar.a = j;
                    cVar.b = j2;
                    cVar.c = i;
                    cVar.d = string;
                    linkedList.add(cVar);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = query;
                    th = th2;
                }
            }
            if (query != null) {
                query.close();
            }
            return linkedList;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        if (cursor != null) {
            cursor.close();
        }
        throw th;
    }

    public static boolean a(long j) {
        return 1 == b.delete("usage", "_id=?", new String[]{new StringBuilder().append(j).toString()});
    }

    public static long a(c cVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("dateTime", Long.valueOf(cVar.b));
        contentValues.put("recordType", Integer.valueOf(cVar.c));
        contentValues.put("paramString", cVar.d);
        return b.insert("usage", null, contentValues);
    }
}
