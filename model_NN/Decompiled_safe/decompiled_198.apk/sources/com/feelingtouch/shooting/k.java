package com.feelingtouch.shooting;

import android.view.SurfaceHolder;

/* compiled from: MainMenuView */
final class k extends Thread {
    SurfaceHolder a;
    boolean b = true;
    final /* synthetic */ MainMenuView c;

    public k(MainMenuView mainMenuView, SurfaceHolder surfaceHolder) {
        this.c = mainMenuView;
        this.a = surfaceHolder;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r7 = this;
            r1 = 0
        L_0x0001:
            com.feelingtouch.shooting.MainMenuView r0 = r7.c
            boolean r0 = r0.af
            if (r0 == 0) goto L_0x0001
            r0 = r1
        L_0x000a:
            boolean r1 = r7.b
            if (r1 != 0) goto L_0x000f
            return
        L_0x000f:
            com.feelingtouch.shooting.MainMenuView r1 = r7.c
            boolean r1 = r1.ae
            if (r1 != 0) goto L_0x000a
            android.view.SurfaceHolder r1 = r7.a     // Catch:{ Exception -> 0x00c4, all -> 0x00bf }
            monitor-enter(r1)     // Catch:{ Exception -> 0x00c4, all -> 0x00bf }
            android.view.SurfaceHolder r2 = r7.a     // Catch:{ all -> 0x0095 }
            android.graphics.Canvas r0 = r2.lockCanvas()     // Catch:{ all -> 0x0095 }
            r2 = 255(0xff, float:3.57E-43)
            r3 = 255(0xff, float:3.57E-43)
            r4 = 255(0xff, float:3.57E-43)
            r5 = 255(0xff, float:3.57E-43)
            r0.drawARGB(r2, r3, r4, r5)     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView r2 = r7.c     // Catch:{ all -> 0x0095 }
            android.graphics.Bitmap r2 = r2.g     // Catch:{ all -> 0x0095 }
            r3 = 0
            r4 = 0
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x0095 }
            boolean r2 = com.feelingtouch.e.b.a()     // Catch:{ all -> 0x0095 }
            if (r2 != 0) goto L_0x0049
            com.feelingtouch.shooting.MainMenuView r2 = r7.c     // Catch:{ all -> 0x0095 }
            android.graphics.Bitmap r2 = r2.m     // Catch:{ all -> 0x0095 }
            r3 = 0
            r4 = 0
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x0095 }
        L_0x0049:
            com.feelingtouch.shooting.MainMenuView r2 = r7.c     // Catch:{ all -> 0x0095 }
            android.graphics.Bitmap r2 = r2.r     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView r3 = r7.c     // Catch:{ all -> 0x0095 }
            android.graphics.Rect r3 = r3.ac     // Catch:{ all -> 0x0095 }
            int r3 = r3.left     // Catch:{ all -> 0x0095 }
            float r3 = (float) r3     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView r4 = r7.c     // Catch:{ all -> 0x0095 }
            android.graphics.Rect r4 = r4.ac     // Catch:{ all -> 0x0095 }
            int r4 = r4.top     // Catch:{ all -> 0x0095 }
            float r4 = (float) r4     // Catch:{ all -> 0x0095 }
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView r2 = r7.c     // Catch:{ all -> 0x0095 }
            android.graphics.Bitmap r2 = r2.s     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView r3 = r7.c     // Catch:{ all -> 0x0095 }
            android.graphics.Rect r3 = r3.ad     // Catch:{ all -> 0x0095 }
            int r3 = r3.left     // Catch:{ all -> 0x0095 }
            float r3 = (float) r3     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView r4 = r7.c     // Catch:{ all -> 0x0095 }
            android.graphics.Rect r4 = r4.ad     // Catch:{ all -> 0x0095 }
            int r4 = r4.top     // Catch:{ all -> 0x0095 }
            float r4 = (float) r4     // Catch:{ all -> 0x0095 }
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView r2 = r7.c     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView.a(r2, r0)     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView r2 = r7.c     // Catch:{ all -> 0x0095 }
            com.feelingtouch.shooting.MainMenuView.b(r2, r0)     // Catch:{ all -> 0x0095 }
            monitor-exit(r1)     // Catch:{ all -> 0x0095 }
            if (r0 == 0) goto L_0x000a
            android.view.SurfaceHolder r1 = r7.a
            r1.unlockCanvasAndPost(r0)
            goto L_0x000a
        L_0x0095:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            monitor-exit(r1)     // Catch:{ Exception -> 0x009b, all -> 0x00b3 }
            throw r0     // Catch:{ Exception -> 0x009b, all -> 0x00b3 }
        L_0x009b:
            r0 = move-exception
            r1 = r2
        L_0x009d:
            r0.printStackTrace()     // Catch:{ all -> 0x00bd }
            com.feelingtouch.c.b r2 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x00bd }
            java.lang.Class r3 = r7.getClass()     // Catch:{ all -> 0x00bd }
            r2.a(r3, r0)     // Catch:{ all -> 0x00bd }
            if (r1 == 0) goto L_0x00c9
            android.view.SurfaceHolder r0 = r7.a
            r0.unlockCanvasAndPost(r1)
            r0 = r1
            goto L_0x000a
        L_0x00b3:
            r0 = move-exception
            r1 = r2
        L_0x00b5:
            if (r1 == 0) goto L_0x00bc
            android.view.SurfaceHolder r2 = r7.a
            r2.unlockCanvasAndPost(r1)
        L_0x00bc:
            throw r0
        L_0x00bd:
            r0 = move-exception
            goto L_0x00b5
        L_0x00bf:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00b5
        L_0x00c4:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x009d
        L_0x00c9:
            r0 = r1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.shooting.k.run():void");
    }
}
