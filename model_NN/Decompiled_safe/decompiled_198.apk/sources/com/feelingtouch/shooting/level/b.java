package com.feelingtouch.shooting.level;

import android.view.SurfaceHolder;

/* compiled from: Level01view */
final class b extends Thread {
    SurfaceHolder a;
    boolean b = true;
    final /* synthetic */ Level01view c;

    public b(Level01view level01view, SurfaceHolder surfaceHolder) {
        this.c = level01view;
        this.a = surfaceHolder;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01cc  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01e7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r19 = this;
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 0
        L_0x0006:
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c
            r5 = r0
            boolean r5 = r5.a
            if (r5 == 0) goto L_0x0006
            r5 = 0
            r15 = r5
            r16 = r3
            r4 = r1
            r1 = r15
            r2 = r16
        L_0x0017:
            r0 = r19
            boolean r0 = r0.b
            r6 = r0
            if (r6 != 0) goto L_0x001f
            return
        L_0x001f:
            r6 = r4
        L_0x0020:
            r8 = 40
            long r8 = r8 + r4
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 < 0) goto L_0x00a2
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
            r4 = r0
            monitor-enter(r4)     // Catch:{ Exception -> 0x01da, all -> 0x01d5 }
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a     // Catch:{ all -> 0x00c0 }
            r5 = r0
            android.graphics.Canvas r1 = r5.lockCanvas()     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            android.graphics.Bitmap r5 = r5.k     // Catch:{ all -> 0x00c0 }
            r8 = 0
            r9 = 0
            r10 = 0
            r1.drawBitmap(r5, r8, r9, r10)     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.e.c.a(r1)     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.e.c.b(r1)     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            com.feelingtouch.shooting.level.Level01view.a(r5, r1)     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.f.a.a(r1)     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.d.c.a(r1)     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.f.a.b(r1)     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            com.feelingtouch.shooting.e.a r5 = r5.r     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r8 = r0
            float r8 = r8.m     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r9 = r0
            float r9 = r9.n     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r10 = r0
            int r10 = r10.q     // Catch:{ all -> 0x00c0 }
            r5.a(r1, r8, r9, r10)     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.c.c.a(r1)     // Catch:{ all -> 0x00c0 }
            r5 = 1
            com.feelingtouch.shooting.a.a.a(r1, r5)     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            int r5 = r5.b     // Catch:{ all -> 0x00c0 }
            switch(r5) {
                case 1: goto L_0x00a8;
                case 2: goto L_0x00e4;
                case 3: goto L_0x0094;
                case 4: goto L_0x0184;
                case 5: goto L_0x0094;
                case 6: goto L_0x0094;
                case 7: goto L_0x0094;
                case 8: goto L_0x0191;
                default: goto L_0x0094;
            }     // Catch:{ all -> 0x00c0 }
        L_0x0094:
            monitor-exit(r4)     // Catch:{ all -> 0x00c0 }
            if (r1 == 0) goto L_0x009f
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r4 = r0
            r4.unlockCanvasAndPost(r1)
        L_0x009f:
            r4 = r6
            goto L_0x0017
        L_0x00a2:
            long r6 = java.lang.System.currentTimeMillis()
            goto L_0x0020
        L_0x00a8:
            boolean r5 = com.feelingtouch.shooting.c.c.a     // Catch:{ all -> 0x00c0 }
            if (r5 == 0) goto L_0x0094
            r5 = 0
            com.feelingtouch.shooting.c.c.a = r5     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.d.c.b()     // Catch:{ all -> 0x00c0 }
            int r5 = com.feelingtouch.shooting.c.b.e     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.c.b.a(r5)     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            r8 = 2
            r5.b = r8     // Catch:{ all -> 0x00c0 }
            goto L_0x0094
        L_0x00c0:
            r5 = move-exception
            r8 = r2
            r2 = r1
            r1 = r5
            monitor-exit(r4)     // Catch:{ Exception -> 0x00c6 }
            throw r1     // Catch:{ Exception -> 0x00c6 }
        L_0x00c6:
            r1 = move-exception
            r3 = r8
        L_0x00c8:
            r1.printStackTrace()     // Catch:{ all -> 0x01c9 }
            com.feelingtouch.c.b r5 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x01c9 }
            java.lang.Class r8 = r19.getClass()     // Catch:{ all -> 0x01c9 }
            r5.a(r8, r1)     // Catch:{ all -> 0x01c9 }
            if (r2 == 0) goto L_0x01e7
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r1 = r0
            r1.unlockCanvasAndPost(r2)
            r1 = r2
            r15 = r3
            r2 = r15
            r4 = r6
            goto L_0x0017
        L_0x00e4:
            r8 = 40
            long r2 = r2 + r8
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            int r5 = r5.j     // Catch:{ all -> 0x00c0 }
            long r8 = (long) r5     // Catch:{ all -> 0x00c0 }
            int r5 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r5 <= 0) goto L_0x014b
            r2 = 0
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            java.util.Random r5 = r5.i     // Catch:{ all -> 0x00c0 }
            r8 = 3
            int r5 = r5.nextInt(r8)     // Catch:{ all -> 0x00c0 }
            int r5 = r5 + 1
            r8 = 10
            int[] r8 = new int[r8]     // Catch:{ all -> 0x00c0 }
            r9 = 1
            r10 = 1
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 2
            r10 = 2
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 3
            r10 = 3
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 4
            r10 = 4
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 5
            r10 = 5
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 6
            r10 = 6
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 7
            r10 = 7
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 8
            r10 = 8
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 9
            r10 = 9
            r8[r9] = r10     // Catch:{ all -> 0x00c0 }
            r9 = 10
            int[] r9 = new int[r9]     // Catch:{ all -> 0x00c0 }
            java.util.Random r10 = new java.util.Random     // Catch:{ all -> 0x00c0 }
            r10.<init>()     // Catch:{ all -> 0x00c0 }
            r11 = 0
        L_0x013d:
            int r12 = r8.length     // Catch:{ all -> 0x00c0 }
            if (r11 < r12) goto L_0x015c
            int[] r8 = new int[r5]     // Catch:{ all -> 0x00c0 }
            r10 = 0
            r11 = 0
            java.lang.System.arraycopy(r9, r10, r8, r11, r5)     // Catch:{ all -> 0x00c0 }
            r5 = 0
        L_0x0148:
            int r9 = r8.length     // Catch:{ all -> 0x00c0 }
            if (r5 < r9) goto L_0x0171
        L_0x014b:
            boolean r5 = com.feelingtouch.shooting.e.b.c()     // Catch:{ all -> 0x00c0 }
            if (r5 == 0) goto L_0x0094
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            r8 = 8
            r5.b = r8     // Catch:{ all -> 0x00c0 }
            goto L_0x0094
        L_0x015c:
            int r12 = r8.length     // Catch:{ all -> 0x00c0 }
            int r12 = r12 - r11
            int r12 = r10.nextInt(r12)     // Catch:{ all -> 0x00c0 }
            r13 = r8[r12]     // Catch:{ all -> 0x00c0 }
            r9[r11] = r13     // Catch:{ all -> 0x00c0 }
            int r13 = r8.length     // Catch:{ all -> 0x00c0 }
            r14 = 1
            int r13 = r13 - r14
            int r13 = r13 - r11
            r13 = r8[r13]     // Catch:{ all -> 0x00c0 }
            r8[r12] = r13     // Catch:{ all -> 0x00c0 }
            int r11 = r11 + 1
            goto L_0x013d
        L_0x0171:
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r9 = r0
            com.feelingtouch.shooting.target.e[] r9 = r9.l     // Catch:{ all -> 0x00c0 }
            r10 = r8[r5]     // Catch:{ all -> 0x00c0 }
            r9 = r9[r10]     // Catch:{ all -> 0x00c0 }
            r9.h()     // Catch:{ all -> 0x00c0 }
            int r5 = r5 + 1
            goto L_0x0148
        L_0x0184:
            r5 = 16
            com.feelingtouch.shooting.a.a.b = r5     // Catch:{ all -> 0x00c0 }
            boolean r5 = com.feelingtouch.shooting.a.a.a     // Catch:{ all -> 0x00c0 }
            if (r5 == 0) goto L_0x0094
            com.feelingtouch.shooting.a.a.c()     // Catch:{ all -> 0x00c0 }
            goto L_0x0094
        L_0x0191:
            boolean r5 = com.feelingtouch.shooting.e.b.d()     // Catch:{ all -> 0x00c0 }
            if (r5 == 0) goto L_0x01ba
            r5 = 256(0x100, float:3.59E-43)
            com.feelingtouch.shooting.a.a.b = r5     // Catch:{ all -> 0x00c0 }
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            boolean r5 = r5.p     // Catch:{ all -> 0x00c0 }
            if (r5 != 0) goto L_0x01b1
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            r5.p = true     // Catch:{ all -> 0x00c0 }
            com.feelingtouch.shooting.e.b.b()     // Catch:{ all -> 0x00c0 }
        L_0x01b1:
            boolean r5 = com.feelingtouch.shooting.a.a.a     // Catch:{ all -> 0x00c0 }
            if (r5 == 0) goto L_0x01bf
            com.feelingtouch.shooting.a.a.c()     // Catch:{ all -> 0x00c0 }
            goto L_0x0094
        L_0x01ba:
            r5 = 4096(0x1000, float:5.74E-42)
            com.feelingtouch.shooting.a.a.b = r5     // Catch:{ all -> 0x00c0 }
            goto L_0x01b1
        L_0x01bf:
            r0 = r19
            com.feelingtouch.shooting.level.Level01view r0 = r0.c     // Catch:{ all -> 0x00c0 }
            r5 = r0
            r8 = 1
            r5.b = r8     // Catch:{ all -> 0x00c0 }
            goto L_0x0094
        L_0x01c9:
            r1 = move-exception
        L_0x01ca:
            if (r2 == 0) goto L_0x01d4
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r3 = r0
            r3.unlockCanvasAndPost(r2)
        L_0x01d4:
            throw r1
        L_0x01d5:
            r2 = move-exception
            r15 = r2
            r2 = r1
            r1 = r15
            goto L_0x01ca
        L_0x01da:
            r4 = move-exception
            r15 = r4
            r16 = r1
            r1 = r15
            r17 = r2
            r3 = r17
            r2 = r16
            goto L_0x00c8
        L_0x01e7:
            r1 = r2
            r15 = r3
            r2 = r15
            r4 = r6
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.shooting.level.b.run():void");
    }
}
