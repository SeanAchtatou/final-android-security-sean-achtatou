package com.feelingtouch.shooting.level;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import com.feelingtouch.gamebox.a;
import com.feelingtouch.shooting.MissionActivity;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.c.b;
import com.feelingtouch.shooting.c.d;

public class Level01Activity extends Activity {
    private Level01view a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a().a(this);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.level_01_view);
        this.a = (Level01view) findViewById(R.id.level_01_view);
        Intent intent = getIntent();
        intent.putExtras(new Bundle());
        if (this.a.a) {
            setResult(-1, intent);
        }
        com.feelingtouch.shooting.f.a.a(this);
        com.feelingtouch.shooting.f.a.a(BitmapFactory.decodeResource(getResources(), R.drawable.font));
        com.feelingtouch.shooting.c.a.a(this);
        b.a(this);
        d.a(this);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        if (i == 4) {
            if (this.a.b == 2) {
                com.feelingtouch.shooting.e.b.a(true);
                this.a.b = 4;
            } else if (this.a.b == 4 || this.a.b == 1) {
                startActivity(new Intent(this, MissionActivity.class));
                finish();
            }
            return true;
        }
        if (i == 25 || i == 24) {
            b.b();
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a.a();
        a.b(this);
        System.gc();
        System.gc();
    }
}
