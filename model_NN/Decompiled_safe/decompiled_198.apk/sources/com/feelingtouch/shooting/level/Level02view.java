package com.feelingtouch.shooting.level;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.feelingtouch.b.a;
import com.feelingtouch.shooting.MissionActivity;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.d.e;
import com.feelingtouch.shooting.e.c;
import com.feelingtouch.shooting.f.b;

public class Level02view extends SurfaceView implements SurfaceHolder.Callback {
    public boolean a = false;
    public int b = 1;
    private Level02Activity c;
    private Resources d;
    private c e;
    private Thread f = null;
    private int g;
    private int h;
    /* access modifiers changed from: private */
    public int i = 50;
    /* access modifiers changed from: private */
    public Bitmap j = null;
    private float k;
    private float l;
    private Rect m;
    /* access modifiers changed from: private */
    public int n = 2;
    /* access modifiers changed from: private */
    public boolean o = false;
    private Context p;
    private int q = -1;

    public Level02view(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.p = context;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setKeepScreenOn(true);
        this.d = getResources();
        this.c = (Level02Activity) context;
        this.e = new c(this, holder);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        a.c(this.p);
        this.a = false;
        this.g = i3;
        this.h = i4;
        Bitmap decodeResource = BitmapFactory.decodeResource(this.d, com.feelingtouch.shooting.b.a.b[1]);
        this.j = Bitmap.createScaledBitmap(decodeResource, this.g, this.h, true);
        this.k = ((float) this.g) / ((float) decodeResource.getWidth());
        this.l = ((float) this.h) / ((float) decodeResource.getHeight());
        b.a(decodeResource);
        this.m = new Rect(0, 0, i3, (int) (400.0f * this.l));
        com.feelingtouch.shooting.a.a.b = 1;
        a.a = 2;
        a.a();
        com.feelingtouch.shooting.e.b.a(getContext(), a.a);
        c.a(this.d, i4, this.k, this.l, 2);
        e.a(this.d, i4, this.k, this.l);
        com.feelingtouch.shooting.target.c.a(this.d, this.g, this.h, this.k, this.l);
        com.feelingtouch.shooting.a.a.a(getResources(), i3, i4);
        if (!com.feelingtouch.shooting.c.c.a()) {
            Bitmap decodeResource2 = BitmapFactory.decodeResource(getResources(), R.drawable.go);
            com.feelingtouch.shooting.c.c.a(decodeResource2, (this.g - decodeResource2.getWidth()) / 2, (this.h - decodeResource2.getHeight()) / 2);
        }
        com.feelingtouch.shooting.f.a.a(this.d, this.k, this.l, (int) (255.0f * this.k), (int) (200.0f * this.l));
        this.a = true;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.f == null || !this.f.isAlive()) {
            this.e.b = true;
            this.f = new Thread(this.e);
            this.f.start();
            return;
        }
        com.feelingtouch.c.b bVar = com.feelingtouch.c.c.a;
        getClass();
        new Object[1][0] = "This Thread is already running.";
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        com.feelingtouch.shooting.a.a.a();
        this.e.b = false;
        boolean z = false;
        while (!z) {
            try {
                this.f.join();
                z = true;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
                com.feelingtouch.c.c.a.a(getClass(), e2);
            }
        }
        b.a(this.j);
        e.c();
        com.feelingtouch.shooting.target.c.d();
    }

    private void a() {
        if (com.feelingtouch.shooting.e.b.c <= 10) {
            if (this.q != 0) {
                this.q = 0;
                this.n = 2;
                this.i = 50;
                com.feelingtouch.shooting.target.c.a(2);
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 20) {
            if (this.q != 1) {
                this.q = 1;
                com.feelingtouch.shooting.f.a.a();
                this.n = 3;
                this.i = 50;
                com.feelingtouch.shooting.target.c.a(2);
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 30) {
            if (this.q != 2) {
                this.q = 2;
                com.feelingtouch.shooting.f.a.a();
                this.n = 4;
                this.i = 50;
                com.feelingtouch.shooting.target.c.a(2);
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 45) {
            if (this.q != 3) {
                this.q = 3;
                com.feelingtouch.shooting.f.a.a();
                this.n = 4;
                this.i = 60;
                com.feelingtouch.shooting.target.c.a(1);
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 60 && this.q != 4) {
            this.q = 4;
            com.feelingtouch.shooting.f.a.a();
            this.n = 5;
            this.i = 60;
            com.feelingtouch.shooting.target.c.a(1);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                if (com.feelingtouch.shooting.a.a.e) {
                    if (com.feelingtouch.shooting.a.a.c.contains(x, y)) {
                        com.feelingtouch.shooting.a.a.f = true;
                    } else if (com.feelingtouch.shooting.a.a.d.contains(x, y)) {
                        com.feelingtouch.shooting.a.a.g = true;
                    }
                }
                if (this.b == 2) {
                    if (this.m.contains(x, y)) {
                        e.a();
                        if (e.a) {
                            com.feelingtouch.shooting.target.c.a(x, y);
                            a();
                        }
                    } else if (e.a(x, y)) {
                        e.b();
                    } else if (c.a(x, y)) {
                        c.a = true;
                    }
                    if (c.b(x, y)) {
                        if (c.b) {
                            z = false;
                        } else {
                            z = true;
                        }
                        c.b = z;
                        if (z) {
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                        com.feelingtouch.shooting.c.b.a = z2;
                        break;
                    }
                }
                break;
            case 1:
                com.feelingtouch.shooting.a.a.f = false;
                com.feelingtouch.shooting.a.a.g = false;
                c.a = false;
                if (this.b == 2 && c.a(x, y)) {
                    com.feelingtouch.shooting.c.b.a();
                    com.feelingtouch.shooting.e.b.a(true);
                    this.b = 4;
                    a.a();
                }
                if (com.feelingtouch.shooting.a.a.e) {
                    if (!com.feelingtouch.shooting.a.a.c.contains(x, y)) {
                        if (com.feelingtouch.shooting.a.a.d.contains(x, y)) {
                            com.feelingtouch.shooting.c.b.a();
                            com.feelingtouch.shooting.e.b.b(getContext());
                            this.c.startActivity(new Intent(this.c, MissionActivity.class));
                            this.c.finish();
                            break;
                        }
                    } else {
                        com.feelingtouch.shooting.c.b.a();
                        switch (com.feelingtouch.shooting.a.a.b) {
                            case 1:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.a();
                                    a();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    break;
                                }
                                break;
                            case 16:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.a(false);
                                    this.b = 2;
                                    com.feelingtouch.shooting.a.a.b();
                                    break;
                                }
                                break;
                            case 256:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.b(getContext());
                                    com.feelingtouch.shooting.e.b.a();
                                    a();
                                    com.feelingtouch.shooting.target.c.c();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    break;
                                }
                                break;
                            case 4096:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.b(getContext());
                                    com.feelingtouch.shooting.e.b.a();
                                    a();
                                    com.feelingtouch.shooting.target.c.c();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    break;
                                }
                                break;
                        }
                    }
                }
                break;
        }
        return true;
    }
}
