package com.feelingtouch.shooting.level;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.feelingtouch.shooting.MissionActivity;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.e.a;
import com.feelingtouch.shooting.e.c;
import com.feelingtouch.shooting.f.b;
import com.feelingtouch.shooting.target.e;
import java.util.Random;

public class Level01view extends SurfaceView implements SurfaceHolder.Callback {
    public boolean a = false;
    public int b = 1;
    private Level01Activity c;
    private Resources d;
    private b e;
    private Thread f = null;
    private int g;
    private int h;
    /* access modifiers changed from: private */
    public Random i = new Random();
    /* access modifiers changed from: private */
    public int j = 2000;
    /* access modifiers changed from: private */
    public Bitmap k = null;
    /* access modifiers changed from: private */
    public e[] l = new e[10];
    /* access modifiers changed from: private */
    public float m;
    /* access modifiers changed from: private */
    public float n;
    private Rect o;
    /* access modifiers changed from: private */
    public boolean p = false;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public a r;
    private Context s;

    public Level01view(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setKeepScreenOn(true);
        this.s = context;
        this.d = getResources();
        this.c = (Level01Activity) context;
        this.e = new b(this, holder);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        com.feelingtouch.b.a.c(this.s);
        this.a = false;
        this.g = i3;
        this.h = i4;
        Bitmap decodeResource = BitmapFactory.decodeResource(this.d, com.feelingtouch.shooting.b.a.b[0]);
        this.k = Bitmap.createScaledBitmap(decodeResource, this.g, this.h, true);
        this.m = ((float) this.g) / ((float) decodeResource.getWidth());
        this.n = ((float) this.h) / ((float) decodeResource.getHeight());
        b.a(decodeResource);
        this.o = new Rect(0, 0, i3, (int) (355.0f * this.n));
        this.l[0] = new e(this.d, R.drawable.target, 76);
        this.l[0].a(this.m, this.n);
        this.l[1] = new e(this.l[0]);
        this.l[1].a(this.m, this.n);
        this.l[2] = new e(this.l[0]);
        this.l[2].a(this.m, this.n);
        this.l[3] = new e(this.l[0]);
        this.l[3].a(this.m, this.n);
        this.l[4] = new e(this.l[0]);
        this.l[4].a(this.m * 0.632f, this.n * 0.472f);
        this.l[5] = new e(this.d, R.drawable.target2, 82);
        this.l[5].a(this.m * 0.585f, this.n * 0.609f);
        this.l[6] = new e(this.l[0]);
        this.l[6].a(this.m * 0.474f, this.n * 0.352f);
        this.l[7] = new e(this.l[5]);
        this.l[7].a(this.m * 1.207f, this.n * 1.317f);
        this.l[8] = new e(this.d, R.drawable.target3, 83);
        this.l[8].a(this.m, this.n);
        this.l[9] = new e(this.d, R.drawable.target4, 88);
        this.l[9].a(this.m, this.n);
        int i5 = (int) (100.0f * this.n);
        this.l[0].a((int) (139.0f * this.m), i5);
        this.l[1].a((int) (238.0f * this.m), i5);
        this.l[2].a((int) (492.0f * this.m), i5);
        this.l[3].a((int) (572.0f * this.m), i5);
        this.l[4].a((int) (333.0f * this.m), (int) (122.0f * this.n));
        this.l[5].a((int) (390.0f * this.m), (int) (112.0f * this.n));
        this.l[6].a((int) (450.0f * this.m), (int) (124.0f * this.n));
        this.l[7].a((int) (716.0f * this.m), (int) (-60.0f * this.n));
        this.l[8].a((int) (461.0f * this.m), (int) (244.0f * this.n));
        this.l[9].a((int) (640.0f * this.m), (int) (163.0f * this.n));
        com.feelingtouch.shooting.a.a.b = 1;
        a.a = 1;
        a.a();
        com.feelingtouch.shooting.e.b.a(getContext(), a.a);
        c.a(this.d, i4, this.m, this.n, 1);
        com.feelingtouch.shooting.d.c.a(this.d, i4, this.m, this.n);
        com.feelingtouch.shooting.a.a.a(getResources(), i3, i4);
        if (!com.feelingtouch.shooting.c.c.a()) {
            Bitmap decodeResource2 = BitmapFactory.decodeResource(getResources(), R.drawable.go);
            com.feelingtouch.shooting.c.c.a(decodeResource2, (this.g - decodeResource2.getWidth()) / 2, (this.h - decodeResource2.getHeight()) / 2);
        }
        com.feelingtouch.shooting.f.a.a(this.d, this.m, this.n, (int) (255.0f * this.m), (int) (200.0f * this.n));
        this.r = new a(this.d);
        this.a = true;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.f == null || !this.f.isAlive()) {
            this.e.b = true;
            this.f = new Thread(this.e);
            this.f.start();
            return;
        }
        com.feelingtouch.c.b bVar = com.feelingtouch.c.c.a;
        getClass();
        new Object[1][0] = "This Thread is already running.";
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        com.feelingtouch.shooting.a.a.a();
        this.e.b = false;
        boolean z = false;
        while (!z) {
            try {
                this.f.join();
                z = true;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
                com.feelingtouch.c.c.a.a(getClass(), e2);
            }
        }
        b.a(this.k);
        for (e e3 : this.l) {
            e3.e();
        }
        com.feelingtouch.shooting.d.c.c();
    }

    static /* synthetic */ void a(Level01view level01view, Canvas canvas) {
        for (int i2 = 0; i2 < 10; i2++) {
            level01view.l[i2].a(canvas);
        }
    }

    private void a() {
        if (com.feelingtouch.shooting.e.b.c <= 15) {
            if (this.j != 2000) {
                this.j = 2000;
                for (int i2 = 0; i2 < 10; i2++) {
                    this.l[i2].b(30, 15);
                }
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 25) {
            if (this.j != 1500) {
                this.j = 1500;
                com.feelingtouch.shooting.f.a.a();
                for (int i3 = 0; i3 < 10; i3++) {
                    this.l[i3].b(25, 12);
                }
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 35) {
            if (this.j != 1300) {
                this.j = 1300;
                com.feelingtouch.shooting.f.a.a();
                for (int i4 = 0; i4 < 10; i4++) {
                    this.l[i4].b(23, 10);
                }
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 50) {
            if (this.j != 1300) {
                this.j = 1100;
                com.feelingtouch.shooting.f.a.a();
                for (int i5 = 0; i5 < 10; i5++) {
                    this.l[i5].b(20, 8);
                }
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 65) {
            if (this.j != 1000) {
                this.j = 1000;
                com.feelingtouch.shooting.f.a.a();
                for (int i6 = 0; i6 < 10; i6++) {
                    this.l[i6].b(17, 7);
                }
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 75) {
            if (this.j != 950) {
                this.j = 950;
                com.feelingtouch.shooting.f.a.a();
                for (int i7 = 0; i7 < 10; i7++) {
                    this.l[i7].b(15, 6);
                }
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 90 && this.j != 920) {
            this.j = 920;
            com.feelingtouch.shooting.f.a.a();
            for (int i8 = 0; i8 < 10; i8++) {
                this.l[i8].b(15, 6);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        boolean z3;
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                if (com.feelingtouch.shooting.a.a.e) {
                    if (com.feelingtouch.shooting.a.a.c.contains(x, y)) {
                        com.feelingtouch.shooting.a.a.f = true;
                    } else if (com.feelingtouch.shooting.a.a.d.contains(x, y)) {
                        com.feelingtouch.shooting.a.a.g = true;
                    }
                }
                if (this.b == 2) {
                    if (this.o.contains(x, y)) {
                        com.feelingtouch.shooting.d.c.a();
                        if (com.feelingtouch.shooting.d.c.a) {
                            int i2 = 0;
                            for (int i3 = 0; i3 < 10; i3++) {
                                if (!this.l[i3].b().contains(x, y) || !this.l[i3].u) {
                                    i2++;
                                } else {
                                    this.l[i3].i();
                                    if (i3 == 4 || i3 == 5) {
                                        com.feelingtouch.shooting.e.b.b(2);
                                        com.feelingtouch.shooting.f.a.a("+2", x, y);
                                    } else if (i3 == 6) {
                                        com.feelingtouch.shooting.e.b.b(3);
                                        com.feelingtouch.shooting.f.a.a("+3", x, y);
                                    } else {
                                        com.feelingtouch.shooting.e.b.b(1);
                                        com.feelingtouch.shooting.f.a.a("+1", x, y);
                                    }
                                    a();
                                }
                            }
                            if (i2 != 10) {
                                this.q++;
                                if (this.q >= 5) {
                                    if (this.q < 10) {
                                        com.feelingtouch.shooting.e.b.b(1);
                                    } else if (this.q < 20) {
                                        com.feelingtouch.shooting.e.b.b(2);
                                    } else if (this.q < 30) {
                                        com.feelingtouch.shooting.e.b.b(3);
                                    } else if (this.q < 40) {
                                        com.feelingtouch.shooting.e.b.b(4);
                                    } else if (this.q < 50) {
                                        com.feelingtouch.shooting.e.b.b(5);
                                    } else {
                                        com.feelingtouch.shooting.e.b.b(6);
                                    }
                                }
                            } else {
                                this.q = 0;
                            }
                        }
                    } else if (com.feelingtouch.shooting.d.c.a(x, y)) {
                        com.feelingtouch.shooting.d.c.b();
                    } else if (c.a(x, y)) {
                        c.a = true;
                    }
                    if (c.b(x, y)) {
                        if (c.b) {
                            z = false;
                        } else {
                            z = true;
                        }
                        c.b = z;
                        if (z) {
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                        com.feelingtouch.shooting.c.a.a = z2;
                        if (c.b) {
                            z3 = false;
                        } else {
                            z3 = true;
                        }
                        com.feelingtouch.shooting.c.b.a = z3;
                        com.feelingtouch.e.a.a.a(this.s, "bgMusic", com.feelingtouch.shooting.c.a.a);
                        com.feelingtouch.e.a.a.a(this.s, "effectMusic", com.feelingtouch.shooting.c.b.a);
                        break;
                    }
                }
                break;
            case 1:
                com.feelingtouch.shooting.a.a.f = false;
                com.feelingtouch.shooting.a.a.g = false;
                c.a = false;
                if (this.b == 2 && c.a(x, y)) {
                    com.feelingtouch.shooting.c.b.a();
                    com.feelingtouch.shooting.e.b.a(true);
                    this.b = 4;
                    com.feelingtouch.b.a.a();
                }
                if (com.feelingtouch.shooting.a.a.e) {
                    if (!com.feelingtouch.shooting.a.a.c.contains(x, y)) {
                        if (com.feelingtouch.shooting.a.a.d.contains(x, y)) {
                            com.feelingtouch.shooting.c.b.a();
                            com.feelingtouch.shooting.e.b.b(getContext());
                            this.c.startActivity(new Intent(this.c, MissionActivity.class));
                            this.c.finish();
                            break;
                        }
                    } else {
                        com.feelingtouch.shooting.c.b.a();
                        switch (com.feelingtouch.shooting.a.a.b) {
                            case 1:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.a();
                                    a();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    break;
                                }
                                break;
                            case 16:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.a(false);
                                    this.b = 2;
                                    com.feelingtouch.shooting.a.a.b();
                                    break;
                                }
                                break;
                            case 256:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    this.p = false;
                                    this.q = 0;
                                    com.feelingtouch.shooting.e.b.b(getContext());
                                    com.feelingtouch.shooting.e.b.a();
                                    a();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    break;
                                }
                                break;
                            case 4096:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    this.p = false;
                                    this.q = 0;
                                    com.feelingtouch.shooting.e.b.b(getContext());
                                    com.feelingtouch.shooting.e.b.a();
                                    a();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    break;
                                }
                                break;
                        }
                    }
                }
                break;
        }
        return true;
    }
}
