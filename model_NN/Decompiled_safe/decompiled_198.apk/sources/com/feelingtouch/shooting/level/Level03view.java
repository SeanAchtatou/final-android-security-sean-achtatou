package com.feelingtouch.shooting.level;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.feelingtouch.b.a;
import com.feelingtouch.shooting.MissionActivity;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.e.c;
import com.feelingtouch.shooting.f.b;

public class Level03view extends SurfaceView implements SurfaceHolder.Callback {
    public boolean a = false;
    public int b = 1;
    private Level03Activity c;
    private Resources d;
    private d e;
    private Thread f = null;
    private int g;
    private int h;
    /* access modifiers changed from: private */
    public Bitmap i = null;
    /* access modifiers changed from: private */
    public Bitmap j = null;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public int l;
    private float m;
    private float n;
    private Rect o;
    private long p = System.currentTimeMillis();
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public Bitmap r;
    /* access modifiers changed from: private */
    public Bitmap s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public boolean x = false;
    private Context y;

    public Level03view(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.y = context;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setKeepScreenOn(true);
        this.d = getResources();
        this.c = (Level03Activity) context;
        this.e = new d(this, holder);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        a.c(this.y);
        this.a = false;
        this.g = i3;
        this.h = i4;
        Bitmap decodeResource = BitmapFactory.decodeResource(this.d, com.feelingtouch.shooting.b.a.b[2]);
        this.i = Bitmap.createScaledBitmap(decodeResource, this.g, this.h, true);
        this.m = ((float) this.g) / ((float) decodeResource.getWidth());
        this.n = ((float) this.h) / ((float) decodeResource.getHeight());
        b.a(decodeResource);
        Bitmap decodeResource2 = BitmapFactory.decodeResource(this.d, R.drawable.level03_bg2);
        this.j = Bitmap.createScaledBitmap(decodeResource2, (int) (((float) decodeResource2.getWidth()) * this.m), (int) (((float) decodeResource2.getHeight()) * this.n), true);
        this.k = (int) (55.0f * this.m);
        this.l = (int) (93.0f * this.n);
        b.a(decodeResource2);
        this.o = new Rect(0, 0, i3, (int) (400.0f * this.n));
        Bitmap decodeResource3 = BitmapFactory.decodeResource(this.d, R.drawable.blood_bag);
        this.r = Bitmap.createBitmap(decodeResource3, 0, 0, 45, 81);
        this.s = Bitmap.createBitmap(decodeResource3, 45, 0, 45, 81);
        b.a(decodeResource3);
        this.t = this.s.getWidth();
        this.u = this.s.getHeight();
        this.v = (int) (this.m * 755.0f);
        this.w = (int) (this.n * 9.0f);
        com.feelingtouch.shooting.a.a.b = 1;
        a.a = 3;
        a.a();
        com.feelingtouch.shooting.e.b.a(getContext(), a.a);
        c.a(this.d, i4, this.m, this.n, 3);
        com.feelingtouch.shooting.d.a.a(this.d, i4, this.m, this.n);
        com.feelingtouch.shooting.target.b.a(this.d, i3, i4, this.m, this.n);
        this.q = 65;
        com.feelingtouch.shooting.a.a.a(getResources(), i3, i4);
        if (!com.feelingtouch.shooting.c.c.a()) {
            Bitmap decodeResource4 = BitmapFactory.decodeResource(getResources(), R.drawable.go);
            com.feelingtouch.shooting.c.c.a(decodeResource4, (this.g - decodeResource4.getWidth()) / 2, (this.h - decodeResource4.getHeight()) / 2);
        }
        com.feelingtouch.shooting.f.a.a(this.d, this.m, this.n, (int) (255.0f * this.m), (int) (200.0f * this.n));
        this.a = true;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.f == null || !this.f.isAlive()) {
            this.e.b = true;
            this.f = new Thread(this.e);
            this.f.start();
            return;
        }
        com.feelingtouch.c.b bVar = com.feelingtouch.c.c.a;
        getClass();
        new Object[1][0] = "This Thread is already running.";
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        com.feelingtouch.shooting.a.a.a();
        this.e.b = false;
        boolean z = false;
        while (!z) {
            try {
                this.f.join();
                z = true;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
                com.feelingtouch.c.c.a.a(getClass(), e2);
            }
        }
        b.a(this.i);
        b.a(this.j);
        b.a(this.s);
        b.a(this.r);
        com.feelingtouch.shooting.d.a.c();
        com.feelingtouch.shooting.target.b.b();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                if (com.feelingtouch.shooting.a.a.e) {
                    if (com.feelingtouch.shooting.a.a.c.contains(x2, y2)) {
                        com.feelingtouch.shooting.a.a.f = true;
                    } else if (com.feelingtouch.shooting.a.a.d.contains(x2, y2)) {
                        com.feelingtouch.shooting.a.a.g = true;
                    }
                }
                if (this.b == 2) {
                    if (com.feelingtouch.shooting.d.a.a(x2, y2)) {
                        com.feelingtouch.shooting.d.a.b();
                    } else if (c.a(x2, y2)) {
                        c.a = true;
                    } else if (c.b(x2, y2)) {
                        if (c.b) {
                            z = false;
                        } else {
                            z = true;
                        }
                        c.b = z;
                        if (z) {
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                        com.feelingtouch.shooting.c.b.a = z2;
                    }
                }
                if (this.b == 2 && this.o.contains(x2, y2) && System.currentTimeMillis() - this.p > 100) {
                    com.feelingtouch.shooting.d.a.a();
                    this.p = System.currentTimeMillis();
                    if (com.feelingtouch.shooting.d.a.a) {
                        com.feelingtouch.shooting.target.b.a(x2, y2);
                        break;
                    }
                }
                break;
            case 1:
                com.feelingtouch.shooting.a.a.f = false;
                com.feelingtouch.shooting.a.a.g = false;
                c.a = false;
                if (this.b == 2 && c.a(x2, y2)) {
                    com.feelingtouch.shooting.c.b.a();
                    com.feelingtouch.shooting.e.b.a(true);
                    this.b = 4;
                    a.a();
                }
                if (com.feelingtouch.shooting.a.a.e) {
                    if (!com.feelingtouch.shooting.a.a.c.contains(x2, y2)) {
                        if (com.feelingtouch.shooting.a.a.d.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            com.feelingtouch.shooting.e.b.b(getContext());
                            this.c.startActivity(new Intent(this.c, MissionActivity.class));
                            this.c.finish();
                            break;
                        }
                    } else {
                        com.feelingtouch.shooting.c.b.a();
                        switch (com.feelingtouch.shooting.a.a.b) {
                            case 1:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.a();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    break;
                                }
                                break;
                            case 16:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.a(false);
                                    this.b = 2;
                                    com.feelingtouch.shooting.a.a.b();
                                    break;
                                }
                                break;
                            case 256:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.b(getContext());
                                    com.feelingtouch.shooting.e.b.a();
                                    com.feelingtouch.shooting.target.b.a();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    a.a();
                                    break;
                                }
                                break;
                            case 4096:
                                if (!com.feelingtouch.shooting.a.a.a) {
                                    com.feelingtouch.shooting.e.b.b(getContext());
                                    com.feelingtouch.shooting.e.b.a();
                                    com.feelingtouch.shooting.target.b.a();
                                    this.b = 1;
                                    com.feelingtouch.shooting.a.a.b();
                                    com.feelingtouch.shooting.c.c.b();
                                    break;
                                }
                                break;
                        }
                    }
                }
                break;
            case 2:
                if (this.b == 2 && this.o.contains(x2, y2) && System.currentTimeMillis() - this.p > 100) {
                    com.feelingtouch.shooting.d.a.a();
                    this.p = System.currentTimeMillis();
                    if (com.feelingtouch.shooting.d.a.a) {
                        com.feelingtouch.shooting.target.b.a(x2, y2);
                        break;
                    }
                }
                break;
        }
        return true;
    }
}
