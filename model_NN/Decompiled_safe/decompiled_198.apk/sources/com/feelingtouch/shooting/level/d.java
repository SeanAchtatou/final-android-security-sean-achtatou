package com.feelingtouch.shooting.level;

import android.view.SurfaceHolder;

/* compiled from: Level03view */
final class d extends Thread {
    SurfaceHolder a;
    boolean b = true;
    final /* synthetic */ Level03view c;

    public d(Level03view level03view, SurfaceHolder surfaceHolder) {
        this.c = level03view;
        this.a = surfaceHolder;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x023d  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0258  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r19 = this;
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 0
        L_0x0006:
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c
            r5 = r0
            boolean r5 = r5.a
            if (r5 == 0) goto L_0x0006
            r5 = 0
            r15 = r5
            r16 = r3
            r4 = r1
            r1 = r15
            r2 = r16
        L_0x0017:
            r0 = r19
            boolean r0 = r0.b
            r6 = r0
            if (r6 != 0) goto L_0x001f
            return
        L_0x001f:
            r6 = r4
        L_0x0020:
            r8 = 40
            long r8 = r8 + r4
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 < 0) goto L_0x0144
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a     // Catch:{ Exception -> 0x024b, all -> 0x0246 }
            r4 = r0
            monitor-enter(r4)     // Catch:{ Exception -> 0x024b, all -> 0x0246 }
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a     // Catch:{ all -> 0x015d }
            r5 = r0
            android.graphics.Canvas r1 = r5.lockCanvas()     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            android.graphics.Bitmap r5 = r5.i     // Catch:{ all -> 0x015d }
            r8 = 0
            r9 = 0
            r10 = 0
            r1.drawBitmap(r5, r8, r9, r10)     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            android.graphics.Bitmap r5 = r5.r     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r8 = r0
            int r8 = r8.v     // Catch:{ all -> 0x015d }
            float r8 = (float) r8     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r9 = r0
            int r9 = r9.w     // Catch:{ all -> 0x015d }
            float r9 = (float) r9     // Catch:{ all -> 0x015d }
            r10 = 0
            r1.drawBitmap(r5, r8, r9, r10)     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            android.graphics.Bitmap r5 = r5.s     // Catch:{ all -> 0x015d }
            android.graphics.Rect r8 = new android.graphics.Rect     // Catch:{ all -> 0x015d }
            r9 = 0
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r10 = r0
            int r10 = r10.u     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r11 = r0
            int r11 = r11.q     // Catch:{ all -> 0x015d }
            int r10 = r10 - r11
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r11 = r0
            int r11 = r11.t     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r12 = r0
            int r12 = r12.u     // Catch:{ all -> 0x015d }
            r8.<init>(r9, r10, r11, r12)     // Catch:{ all -> 0x015d }
            android.graphics.Rect r9 = new android.graphics.Rect     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r10 = r0
            int r10 = r10.v     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r11 = r0
            int r11 = r11.w     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r12 = r0
            int r12 = r12.u     // Catch:{ all -> 0x015d }
            int r11 = r11 + r12
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r12 = r0
            int r12 = r12.q     // Catch:{ all -> 0x015d }
            int r11 = r11 - r12
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r12 = r0
            int r12 = r12.v     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r13 = r0
            int r13 = r13.t     // Catch:{ all -> 0x015d }
            int r12 = r12 + r13
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r13 = r0
            int r13 = r13.w     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r14 = r0
            int r14 = r14.u     // Catch:{ all -> 0x015d }
            int r13 = r13 + r14
            r9.<init>(r10, r11, r12, r13)     // Catch:{ all -> 0x015d }
            r10 = 0
            r1.drawBitmap(r5, r8, r9, r10)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.e.c.a(r1)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.e.c.b(r1)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.target.b.b(r1)     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            android.graphics.Bitmap r5 = r5.j     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r8 = r0
            int r8 = r8.k     // Catch:{ all -> 0x015d }
            float r8 = (float) r8     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r9 = r0
            int r9 = r9.l     // Catch:{ all -> 0x015d }
            float r9 = (float) r9     // Catch:{ all -> 0x015d }
            r10 = 0
            r1.drawBitmap(r5, r8, r9, r10)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.target.b.a(r1)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.f.a.a(r1)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.d.a.a(r1)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.c.c.a(r1)     // Catch:{ all -> 0x015d }
            r5 = 3
            com.feelingtouch.shooting.a.a.a(r1, r5)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.f.a.b(r1)     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            int r5 = r5.b     // Catch:{ all -> 0x015d }
            switch(r5) {
                case 1: goto L_0x014a;
                case 2: goto L_0x0181;
                case 3: goto L_0x0136;
                case 4: goto L_0x01f5;
                case 5: goto L_0x0136;
                case 6: goto L_0x0136;
                case 7: goto L_0x0136;
                case 8: goto L_0x0202;
                default: goto L_0x0136;
            }     // Catch:{ all -> 0x015d }
        L_0x0136:
            monitor-exit(r4)     // Catch:{ all -> 0x015d }
            if (r1 == 0) goto L_0x0141
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r4 = r0
            r4.unlockCanvasAndPost(r1)
        L_0x0141:
            r4 = r6
            goto L_0x0017
        L_0x0144:
            long r6 = java.lang.System.currentTimeMillis()
            goto L_0x0020
        L_0x014a:
            boolean r5 = com.feelingtouch.shooting.c.c.a     // Catch:{ all -> 0x015d }
            if (r5 == 0) goto L_0x0136
            r5 = 0
            com.feelingtouch.shooting.c.c.a = r5     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.d.a.b()     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 2
            r5.b = r8     // Catch:{ all -> 0x015d }
            goto L_0x0136
        L_0x015d:
            r5 = move-exception
            r8 = r2
            r2 = r1
            r1 = r5
            monitor-exit(r4)     // Catch:{ Exception -> 0x0163 }
            throw r1     // Catch:{ Exception -> 0x0163 }
        L_0x0163:
            r1 = move-exception
            r3 = r8
        L_0x0165:
            r1.printStackTrace()     // Catch:{ all -> 0x023a }
            com.feelingtouch.c.b r5 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x023a }
            java.lang.Class r8 = r19.getClass()     // Catch:{ all -> 0x023a }
            r5.a(r8, r1)     // Catch:{ all -> 0x023a }
            if (r2 == 0) goto L_0x0258
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r1 = r0
            r1.unlockCanvasAndPost(r2)
            r1 = r2
            r15 = r3
            r2 = r15
            r4 = r6
            goto L_0x0017
        L_0x0181:
            r8 = 40
            long r2 = r2 + r8
            com.feelingtouch.shooting.target.b.a(r2)     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.target.b.b(r2)     // Catch:{ all -> 0x015d }
            int r5 = com.feelingtouch.shooting.e.b.f     // Catch:{ all -> 0x015d }
            if (r5 != 0) goto L_0x01a6
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 10
            r5.q = r8     // Catch:{ all -> 0x015d }
        L_0x0198:
            int r5 = com.feelingtouch.shooting.e.b.f     // Catch:{ all -> 0x015d }
            if (r5 != 0) goto L_0x0136
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 8
            r5.b = r8     // Catch:{ all -> 0x015d }
            goto L_0x0136
        L_0x01a6:
            int r5 = com.feelingtouch.shooting.e.b.f     // Catch:{ all -> 0x015d }
            r8 = 20
            if (r5 > r8) goto L_0x01b7
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 20
            r5.q = r8     // Catch:{ all -> 0x015d }
            goto L_0x0198
        L_0x01b7:
            int r5 = com.feelingtouch.shooting.e.b.f     // Catch:{ all -> 0x015d }
            r8 = 40
            if (r5 > r8) goto L_0x01c8
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 30
            r5.q = r8     // Catch:{ all -> 0x015d }
            goto L_0x0198
        L_0x01c8:
            int r5 = com.feelingtouch.shooting.e.b.f     // Catch:{ all -> 0x015d }
            r8 = 60
            if (r5 > r8) goto L_0x01d9
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 40
            r5.q = r8     // Catch:{ all -> 0x015d }
            goto L_0x0198
        L_0x01d9:
            int r5 = com.feelingtouch.shooting.e.b.f     // Catch:{ all -> 0x015d }
            r8 = 80
            if (r5 > r8) goto L_0x01ea
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 50
            r5.q = r8     // Catch:{ all -> 0x015d }
            goto L_0x0198
        L_0x01ea:
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 65
            r5.q = r8     // Catch:{ all -> 0x015d }
            goto L_0x0198
        L_0x01f5:
            r5 = 16
            com.feelingtouch.shooting.a.a.b = r5     // Catch:{ all -> 0x015d }
            boolean r5 = com.feelingtouch.shooting.a.a.a     // Catch:{ all -> 0x015d }
            if (r5 == 0) goto L_0x0136
            com.feelingtouch.shooting.a.a.c()     // Catch:{ all -> 0x015d }
            goto L_0x0136
        L_0x0202:
            boolean r5 = com.feelingtouch.shooting.e.b.d()     // Catch:{ all -> 0x015d }
            if (r5 == 0) goto L_0x022b
            r5 = 256(0x100, float:3.59E-43)
            com.feelingtouch.shooting.a.a.b = r5     // Catch:{ all -> 0x015d }
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            boolean r5 = r5.x     // Catch:{ all -> 0x015d }
            if (r5 != 0) goto L_0x0222
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r5.x = true     // Catch:{ all -> 0x015d }
            com.feelingtouch.shooting.e.b.b()     // Catch:{ all -> 0x015d }
        L_0x0222:
            boolean r5 = com.feelingtouch.shooting.a.a.a     // Catch:{ all -> 0x015d }
            if (r5 == 0) goto L_0x0230
            com.feelingtouch.shooting.a.a.c()     // Catch:{ all -> 0x015d }
            goto L_0x0136
        L_0x022b:
            r5 = 4096(0x1000, float:5.74E-42)
            com.feelingtouch.shooting.a.a.b = r5     // Catch:{ all -> 0x015d }
            goto L_0x0222
        L_0x0230:
            r0 = r19
            com.feelingtouch.shooting.level.Level03view r0 = r0.c     // Catch:{ all -> 0x015d }
            r5 = r0
            r8 = 1
            r5.b = r8     // Catch:{ all -> 0x015d }
            goto L_0x0136
        L_0x023a:
            r1 = move-exception
        L_0x023b:
            if (r2 == 0) goto L_0x0245
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r3 = r0
            r3.unlockCanvasAndPost(r2)
        L_0x0245:
            throw r1
        L_0x0246:
            r2 = move-exception
            r15 = r2
            r2 = r1
            r1 = r15
            goto L_0x023b
        L_0x024b:
            r4 = move-exception
            r15 = r4
            r16 = r1
            r1 = r15
            r17 = r2
            r3 = r17
            r2 = r16
            goto L_0x0165
        L_0x0258:
            r1 = r2
            r15 = r3
            r2 = r15
            r4 = r6
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.shooting.level.d.run():void");
    }
}
