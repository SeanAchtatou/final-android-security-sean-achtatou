package com.feelingtouch.shooting.level;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import com.feelingtouch.gamebox.a;
import com.feelingtouch.shooting.MissionActivity;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.c.d;
import com.feelingtouch.shooting.f.b;
import java.util.Hashtable;

public class Level02Activity extends Activity {
    private Level02view a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a().a(this);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.level_02_view);
        this.a = (Level02view) findViewById(R.id.level_02_view);
        Intent intent = getIntent();
        intent.putExtras(new Bundle());
        if (this.a.a) {
            setResult(-1, intent);
        }
        com.feelingtouch.shooting.f.a.a(this);
        com.feelingtouch.shooting.f.a.a(BitmapFactory.decodeResource(getResources(), R.drawable.font));
        Hashtable hashtable = new Hashtable();
        b.a = hashtable;
        hashtable.put(0, 0);
        b.a.put(3, 5236);
        b.a.put(6, 10453);
        b.a.put(9, 15643);
        b.a.put(12, 20791);
        b.a.put(15, 25882);
        b.a.put(18, 30902);
        b.a.put(21, 35837);
        b.a.put(24, 40674);
        b.a.put(27, 45400);
        b.a.put(30, 50000);
        b.a.put(33, 54464);
        b.a.put(36, 58779);
        b.a.put(39, 62932);
        b.a.put(42, 66913);
        b.a.put(45, 70711);
        b.a.put(48, 74314);
        b.a.put(51, 77715);
        b.a.put(54, 80902);
        b.a.put(57, 83867);
        b.a.put(60, 86603);
        b.a.put(63, 89101);
        b.a.put(66, 91355);
        b.a.put(69, 93358);
        b.a.put(72, 95106);
        b.a.put(75, 96593);
        b.a.put(78, 97815);
        b.a.put(81, 98769);
        b.a.put(84, 99452);
        b.a.put(87, 99863);
        b.a.put(90, 100000);
        com.feelingtouch.shooting.c.a.a(this);
        com.feelingtouch.shooting.c.b.a(this);
        d.a(this);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        if (i == 4) {
            if (this.a.b == 2) {
                com.feelingtouch.shooting.e.b.a(true);
                this.a.b = 4;
            } else if (this.a.b == 4 || this.a.b == 1) {
                startActivity(new Intent(this, MissionActivity.class));
                finish();
            }
            return true;
        }
        if (i == 25 || i == 24) {
            com.feelingtouch.shooting.c.b.b();
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a.a();
        a.b(this);
        System.gc();
        System.gc();
    }
}
