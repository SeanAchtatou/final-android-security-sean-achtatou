package com.feelingtouch.shooting.level;

import android.view.SurfaceHolder;

/* compiled from: Level02view */
final class c extends Thread {
    SurfaceHolder a;
    boolean b = true;
    final /* synthetic */ Level02view c;

    public c(Level02view level02view, SurfaceHolder surfaceHolder) {
        this.c = level02view;
        this.a = surfaceHolder;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0169  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r19 = this;
            long r1 = java.lang.System.currentTimeMillis()
            r3 = 0
            r5 = 0
        L_0x0008:
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c
            r7 = r0
            boolean r7 = r7.a
            if (r7 == 0) goto L_0x0008
            r7 = 0
            r13 = r7
            r14 = r5
            r6 = r1
            r1 = r13
            r16 = r3
            r4 = r16
            r2 = r14
        L_0x001b:
            r0 = r19
            boolean r0 = r0.b
            r8 = r0
            if (r8 != 0) goto L_0x0023
            return
        L_0x0023:
            r8 = r6
        L_0x0024:
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c
            r10 = r0
            int r10 = r10.i
            long r10 = (long) r10
            long r10 = r10 + r6
            int r10 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r10 < 0) goto L_0x0081
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a     // Catch:{ Exception -> 0x015c, all -> 0x0157 }
            r6 = r0
            monitor-enter(r6)     // Catch:{ Exception -> 0x015c, all -> 0x0157 }
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a     // Catch:{ all -> 0x0099 }
            r7 = r0
            android.graphics.Canvas r1 = r7.lockCanvas()     // Catch:{ all -> 0x0099 }
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            android.graphics.Bitmap r7 = r7.j     // Catch:{ all -> 0x0099 }
            r10 = 0
            r11 = 0
            r12 = 0
            r1.drawBitmap(r7, r10, r11, r12)     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.target.c.a(r1)     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.e.c.a(r1)     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.e.c.b(r1)     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.f.a.a(r1)     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.d.e.a(r1)     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.c.c.a(r1)     // Catch:{ all -> 0x0099 }
            r7 = 2
            com.feelingtouch.shooting.a.a.a(r1, r7)     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.f.a.b(r1)     // Catch:{ all -> 0x0099 }
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            int r7 = r7.b     // Catch:{ all -> 0x0099 }
            switch(r7) {
                case 1: goto L_0x0086;
                case 2: goto L_0x00c2;
                case 3: goto L_0x0074;
                case 4: goto L_0x0106;
                case 5: goto L_0x0074;
                case 6: goto L_0x0074;
                case 7: goto L_0x0074;
                case 8: goto L_0x0113;
                default: goto L_0x0074;
            }     // Catch:{ all -> 0x0099 }
        L_0x0074:
            monitor-exit(r6)     // Catch:{ all -> 0x0099 }
            if (r1 == 0) goto L_0x007f
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r6 = r0
            r6.unlockCanvasAndPost(r1)
        L_0x007f:
            r6 = r8
            goto L_0x001b
        L_0x0081:
            long r8 = java.lang.System.currentTimeMillis()
            goto L_0x0024
        L_0x0086:
            boolean r7 = com.feelingtouch.shooting.c.c.a     // Catch:{ all -> 0x0099 }
            if (r7 == 0) goto L_0x0074
            r7 = 0
            com.feelingtouch.shooting.c.c.a = r7     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.d.e.b()     // Catch:{ all -> 0x0099 }
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            r10 = 2
            r7.b = r10     // Catch:{ all -> 0x0099 }
            goto L_0x0074
        L_0x0099:
            r7 = move-exception
            r10 = r4
            r13 = r1
            r1 = r7
            r14 = r2
            r3 = r14
            r2 = r13
            monitor-exit(r6)     // Catch:{ Exception -> 0x00a2 }
            throw r1     // Catch:{ Exception -> 0x00a2 }
        L_0x00a2:
            r1 = move-exception
            r5 = r10
        L_0x00a4:
            r1.printStackTrace()     // Catch:{ all -> 0x014b }
            com.feelingtouch.c.b r7 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x014b }
            java.lang.Class r10 = r19.getClass()     // Catch:{ all -> 0x014b }
            r7.a(r10, r1)     // Catch:{ all -> 0x014b }
            if (r2 == 0) goto L_0x0169
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r1 = r0
            r1.unlockCanvasAndPost(r2)
            r1 = r2
            r13 = r3
            r2 = r13
            r15 = r5
            r4 = r15
            r6 = r8
            goto L_0x001b
        L_0x00c2:
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            int r7 = r7.i     // Catch:{ all -> 0x0099 }
            long r10 = (long) r7     // Catch:{ all -> 0x0099 }
            long r4 = r4 + r10
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            int r7 = r7.i     // Catch:{ all -> 0x0099 }
            long r10 = (long) r7     // Catch:{ all -> 0x0099 }
            long r2 = r2 + r10
            r10 = 2000(0x7d0, double:9.88E-321)
            int r7 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r7 < 0) goto L_0x00f2
            com.feelingtouch.shooting.target.c.a(r4)     // Catch:{ all -> 0x0099 }
            int r7 = com.feelingtouch.shooting.target.c.a()     // Catch:{ all -> 0x0099 }
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r10 = r0
            int r10 = r10.n     // Catch:{ all -> 0x0099 }
            if (r7 < r10) goto L_0x00f2
            r2 = 0
        L_0x00f2:
            com.feelingtouch.shooting.target.c.b()     // Catch:{ all -> 0x0099 }
            boolean r7 = com.feelingtouch.shooting.e.b.c()     // Catch:{ all -> 0x0099 }
            if (r7 == 0) goto L_0x0074
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            r10 = 8
            r7.b = r10     // Catch:{ all -> 0x0099 }
            goto L_0x0074
        L_0x0106:
            r7 = 16
            com.feelingtouch.shooting.a.a.b = r7     // Catch:{ all -> 0x0099 }
            boolean r7 = com.feelingtouch.shooting.a.a.a     // Catch:{ all -> 0x0099 }
            if (r7 == 0) goto L_0x0074
            com.feelingtouch.shooting.a.a.c()     // Catch:{ all -> 0x0099 }
            goto L_0x0074
        L_0x0113:
            boolean r7 = com.feelingtouch.shooting.e.b.d()     // Catch:{ all -> 0x0099 }
            if (r7 == 0) goto L_0x013c
            r7 = 256(0x100, float:3.59E-43)
            com.feelingtouch.shooting.a.a.b = r7     // Catch:{ all -> 0x0099 }
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            boolean r7 = r7.o     // Catch:{ all -> 0x0099 }
            if (r7 != 0) goto L_0x0133
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            r7.o = true     // Catch:{ all -> 0x0099 }
            com.feelingtouch.shooting.e.b.b()     // Catch:{ all -> 0x0099 }
        L_0x0133:
            boolean r7 = com.feelingtouch.shooting.a.a.a     // Catch:{ all -> 0x0099 }
            if (r7 == 0) goto L_0x0141
            com.feelingtouch.shooting.a.a.c()     // Catch:{ all -> 0x0099 }
            goto L_0x0074
        L_0x013c:
            r7 = 4096(0x1000, float:5.74E-42)
            com.feelingtouch.shooting.a.a.b = r7     // Catch:{ all -> 0x0099 }
            goto L_0x0133
        L_0x0141:
            r0 = r19
            com.feelingtouch.shooting.level.Level02view r0 = r0.c     // Catch:{ all -> 0x0099 }
            r7 = r0
            r10 = 1
            r7.b = r10     // Catch:{ all -> 0x0099 }
            goto L_0x0074
        L_0x014b:
            r1 = move-exception
        L_0x014c:
            if (r2 == 0) goto L_0x0156
            r0 = r19
            android.view.SurfaceHolder r0 = r0.a
            r3 = r0
            r3.unlockCanvasAndPost(r2)
        L_0x0156:
            throw r1
        L_0x0157:
            r2 = move-exception
            r13 = r2
            r2 = r1
            r1 = r13
            goto L_0x014c
        L_0x015c:
            r6 = move-exception
            r13 = r6
            r14 = r1
            r1 = r13
            r15 = r2
            r2 = r14
            r17 = r4
            r5 = r17
            r3 = r15
            goto L_0x00a4
        L_0x0169:
            r1 = r2
            r13 = r3
            r2 = r13
            r15 = r5
            r4 = r15
            r6 = r8
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.shooting.level.c.run():void");
    }
}
