package com.feelingtouch.shooting;

import android.content.Context;
import com.feelingtouch.d.d.a;
import com.feelingtouch.e.f;

/* compiled from: GameboxUtil */
final class b implements f {
    final /* synthetic */ a a;
    private final /* synthetic */ boolean b = false;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ Context e;
    private final /* synthetic */ int f;

    b(a aVar, String str, String str2, Context context, int i) {
        this.a = aVar;
        this.c = str;
        this.d = str2;
        this.e = context;
        this.f = i;
    }

    public final void run() {
        try {
            if (this.b) {
                a.c.a(this.c, this.d);
            }
            a.c.a(this.d, this.e.getPackageName(), this.f, this.e.getResources().getConfiguration().locale.getCountry(), this.c);
            if (this.f > com.feelingtouch.e.a.a.b(this.e, "high_score", 0)) {
                com.feelingtouch.e.a.a.a(this.e, "high_score", this.f);
                com.feelingtouch.e.a.a.b(this.e, "high_score_name", this.d);
            }
            this.a.b.a().sendMessage(this.a.b.a().obtainMessage(999, Integer.valueOf(a.c.a(this.e.getPackageName(), this.f))));
        } catch (com.feelingtouch.d.c.a e2) {
            this.a.b.a().sendEmptyMessage(998);
        }
    }

    public final void a() {
        a.a = true;
    }
}
