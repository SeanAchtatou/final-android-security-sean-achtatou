package com.feelingtouch.shooting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.feelingtouch.shooting.c.b;
import com.feelingtouch.shooting.c.d;
import com.feelingtouch.shooting.f.a;

public class LoadingActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(new c(this));
        a.a(this);
        com.feelingtouch.shooting.c.a.a(this);
        b.a(this);
        d.a(this);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == com.feelingtouch.shooting.b.a.g && i2 == -1) {
            finish();
        }
    }
}
