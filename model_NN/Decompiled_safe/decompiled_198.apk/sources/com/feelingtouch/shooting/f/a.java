package com.feelingtouch.shooting.f;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import com.feelingtouch.shooting.R;

/* compiled from: FontUtil */
public final class a {
    private static int A;
    private static int B;
    public static Paint a = new Paint();
    public static Paint b;
    private static Paint c = new Paint();
    private static int d = 0;
    private static int e = 0;
    private static Bitmap f;
    private static Bitmap g;
    private static Bitmap h;
    private static Bitmap i;
    private static Bitmap j;
    private static Bitmap k;
    private static Bitmap l;
    private static Bitmap m;
    private static Bitmap n;
    private static Bitmap o;
    private static Bitmap p;
    private static Bitmap q;
    private static Bitmap r;
    private static long s;
    private static boolean t;
    private static String u;
    private static int v;
    private static int w;
    private static boolean x;
    private static long y;
    private static Bitmap z = null;

    public static void a(Bitmap bitmap) {
        if (d == 0) {
            d = 11;
            e = 11;
            q = Bitmap.createBitmap(bitmap, 0, 0, d, e);
            p = Bitmap.createBitmap(bitmap, d, 0, d, e);
            f = Bitmap.createBitmap(bitmap, d * 2, 0, d, e);
            g = Bitmap.createBitmap(bitmap, d * 3, 0, d, e);
            h = Bitmap.createBitmap(bitmap, d * 4, 0, d, e);
            i = Bitmap.createBitmap(bitmap, d * 5, 0, d, e);
            j = Bitmap.createBitmap(bitmap, d * 6, 0, d, e);
            k = Bitmap.createBitmap(bitmap, d * 7, 0, d, e);
            l = Bitmap.createBitmap(bitmap, d * 8, 0, d, e);
            m = Bitmap.createBitmap(bitmap, d * 9, 0, d, e);
            n = Bitmap.createBitmap(bitmap, d * 10, 0, d, e);
            o = Bitmap.createBitmap(bitmap, d * 11, 0, d, e);
        }
    }

    public static void a(Canvas canvas, String str, int i2, int i3) {
        char[] charArray = str.toCharArray();
        for (int i4 = 0; i4 < charArray.length; i4++) {
            if (charArray[i4] == '-') {
                canvas.drawBitmap(q, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '+') {
                canvas.drawBitmap(p, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '0') {
                canvas.drawBitmap(f, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '1') {
                canvas.drawBitmap(g, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '2') {
                canvas.drawBitmap(h, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '3') {
                canvas.drawBitmap(i, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '4') {
                canvas.drawBitmap(j, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '5') {
                canvas.drawBitmap(k, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '6') {
                canvas.drawBitmap(l, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '7') {
                canvas.drawBitmap(m, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '8') {
                canvas.drawBitmap(n, (float) ((d * i4) + i2), (float) i3, c);
            } else if (charArray[i4] == '9') {
                canvas.drawBitmap(o, (float) ((d * i4) + i2), (float) i3, c);
            }
        }
    }

    public static void a(String str, int i2, int i3) {
        u = str;
        v = i2;
        w = i3;
        r = Bitmap.createBitmap(u.toCharArray().length * d, e, Bitmap.Config.ARGB_8888);
        a(new Canvas(r), u, 0, 0);
        s = System.currentTimeMillis();
        t = true;
    }

    public static void a(Canvas canvas) {
        if (t) {
            if (System.currentTimeMillis() - s > 1000) {
                t = false;
            }
            int i2 = w - 1;
            w = i2;
            canvas.drawBitmap(r, (float) v, (float) i2, (Paint) null);
        }
    }

    public static void a(Context context) {
        a.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/StencilStd.otf"));
        a.setColor(Color.rgb(255, 247, 119));
        a.setAntiAlias(true);
        a.setTextSize(18.0f);
        Paint paint = new Paint();
        b = paint;
        paint.setAntiAlias(true);
        b.setColor(Color.rgb(255, 247, 119));
        b.setTextSize(14.0f);
    }

    public static void a(Resources resources, float f2, float f3, int i2, int i3) {
        if (z == null) {
            A = i2;
            B = i3;
            Bitmap decodeResource = BitmapFactory.decodeResource(resources, R.drawable.speedup);
            z = Bitmap.createScaledBitmap(decodeResource, (int) (((float) decodeResource.getWidth()) * f2), (int) (((float) decodeResource.getHeight()) * f3), true);
            decodeResource.recycle();
        }
    }

    public static void b(Canvas canvas) {
        if (!x) {
            return;
        }
        if (System.currentTimeMillis() - y > 1500) {
            x = false;
        } else {
            canvas.drawBitmap(z, (float) A, (float) B, (Paint) null);
        }
    }

    public static void a() {
        y = System.currentTimeMillis();
        x = true;
    }

    public static String a(String str, Paint paint) {
        if (paint.measureText(str) <= 60.0f) {
            return str;
        }
        char[] charArray = str.toCharArray();
        int i2 = 0;
        while (true) {
            if (i2 < charArray.length) {
                if (paint.measureText(charArray, 0, i2) > 60.0f) {
                    break;
                }
                i2++;
            } else {
                i2 = 0;
                break;
            }
        }
        char[] cArr = new char[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            cArr[i3] = charArray[i3];
        }
        return String.copyValueOf(cArr);
    }
}
