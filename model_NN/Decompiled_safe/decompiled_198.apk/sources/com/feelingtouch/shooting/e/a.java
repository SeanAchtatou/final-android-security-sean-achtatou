package com.feelingtouch.shooting.e;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.feelingtouch.shooting.R;

/* compiled from: Combo */
public final class a {
    private Paint a = new Paint();
    private int b = 22;
    private int c = 38;
    private Bitmap d;
    private Bitmap e;
    private Bitmap f;
    private Bitmap g;
    private Bitmap h;
    private Bitmap i;
    private Bitmap j;
    private Bitmap k;
    private Bitmap l;
    private Bitmap m;
    private Bitmap n;
    private int o = -1;
    private boolean p;
    private String q = null;
    private Bitmap r;
    private RectF s = new RectF();
    private Rect t = new Rect();
    private int u;

    public a(Resources resources) {
        this.n = BitmapFactory.decodeResource(resources, R.drawable.combo);
        Bitmap decodeResource = BitmapFactory.decodeResource(resources, R.drawable.combo_number);
        this.d = Bitmap.createBitmap(decodeResource, 0, 0, this.b, this.c);
        this.e = Bitmap.createBitmap(decodeResource, this.b, 0, this.b, this.c);
        this.f = Bitmap.createBitmap(decodeResource, this.b * 2, 0, this.b, this.c);
        this.g = Bitmap.createBitmap(decodeResource, this.b * 3, 0, this.b, this.c);
        this.h = Bitmap.createBitmap(decodeResource, this.b * 4, 0, this.b, this.c);
        this.i = Bitmap.createBitmap(decodeResource, this.b * 5, 0, this.b, this.c);
        this.j = Bitmap.createBitmap(decodeResource, this.b * 6, 0, this.b, this.c);
        this.k = Bitmap.createBitmap(decodeResource, this.b * 7, 0, this.b, this.c);
        this.l = Bitmap.createBitmap(decodeResource, this.b * 8, 0, this.b, this.c);
        this.m = Bitmap.createBitmap(decodeResource, this.b * 9, 0, this.b, this.c);
        decodeResource.recycle();
        this.a.setAntiAlias(true);
    }

    private void a(Canvas canvas, String str, int i2, int i3) {
        if (str != null) {
            char[] charArray = str.toCharArray();
            for (int i4 = 0; i4 < charArray.length; i4++) {
                if (charArray[i4] == '0') {
                    canvas.drawBitmap(this.m, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '1') {
                    canvas.drawBitmap(this.d, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '2') {
                    canvas.drawBitmap(this.e, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '3') {
                    canvas.drawBitmap(this.f, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '4') {
                    canvas.drawBitmap(this.g, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '5') {
                    canvas.drawBitmap(this.h, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '6') {
                    canvas.drawBitmap(this.i, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '7') {
                    canvas.drawBitmap(this.j, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '8') {
                    canvas.drawBitmap(this.k, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == '9') {
                    canvas.drawBitmap(this.l, (float) ((this.b * i4) + i2), (float) i3, this.a);
                } else if (charArray[i4] == 'c') {
                    canvas.drawBitmap(this.n, (float) ((this.b * i4) + i2), (float) (i3 + 23), this.a);
                }
            }
        }
    }

    public final void a(Canvas canvas, float f2, float f3, int i2) {
        if (i2 >= 5) {
            int i3 = (int) (10.0f * f2);
            int i4 = (int) (150.0f * f3);
            if (i2 == 5) {
                this.q = "5c";
            }
            if (i2 != this.o) {
                this.o = i2;
                String valueOf = String.valueOf(this.o);
                this.r = Bitmap.createBitmap(valueOf.toCharArray().length * this.b, this.c, Bitmap.Config.ARGB_8888);
                a(new Canvas(this.r), valueOf, 0, 0);
                this.p = true;
                this.t.set(0, 0, this.r.getWidth(), this.r.getHeight());
                this.u = 20;
            }
            if (!this.p) {
                this.q = String.valueOf(i2) + "c";
            }
            a(canvas, this.q, i3, i4);
            if (!this.p) {
                return;
            }
            if (this.u - 2 > 0) {
                this.u -= 2;
                this.s.set((float) (i3 - this.u), (float) (this.u + i4), (float) (i3 + this.t.width() + this.u), (float) (i4 + this.t.height() + (this.u * 3)));
                canvas.drawBitmap(this.r, this.t, this.s, (Paint) null);
                return;
            }
            this.p = false;
        }
    }

    public final void a() {
        this.d.recycle();
        this.e.recycle();
        this.f.recycle();
        this.g.recycle();
        this.h.recycle();
        this.i.recycle();
        this.j.recycle();
        this.k.recycle();
        this.l.recycle();
        this.m.recycle();
        this.n.recycle();
    }
}
