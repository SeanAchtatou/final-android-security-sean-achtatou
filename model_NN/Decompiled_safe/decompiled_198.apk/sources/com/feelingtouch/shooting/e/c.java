package com.feelingtouch.shooting.e;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.b.a;
import com.feelingtouch.shooting.f.b;

/* compiled from: ScoreBar */
public final class c {
    public static boolean a;
    public static boolean b;
    private static Bitmap c = null;
    private static Bitmap d = null;
    private static Bitmap e = null;
    private static Bitmap f = null;
    private static Rect g;
    private static Rect h;
    private static float i;
    private static float j;
    private static Bitmap k = null;
    private static int l;
    private static int m;
    private static int n;
    private static int o;
    private static int p = 0;

    public static void a(Resources resources, int i2, float f2, float f3, int i3) {
        i = f2;
        j = f3;
        p = i3;
        Bitmap decodeResource = BitmapFactory.decodeResource(resources, a.c[i3 - 1]);
        if (k != null) {
            k.recycle();
        }
        k = Bitmap.createScaledBitmap(decodeResource, (int) (((float) decodeResource.getWidth()) * i), (int) (((float) decodeResource.getHeight()) * j), true);
        b.a(decodeResource);
        l = (int) (108.0f * i);
        m = (int) (15.0f * j);
        n = l;
        o = (int) (59.0f * j);
        Bitmap decodeResource2 = BitmapFactory.decodeResource(resources, a.d[p - 1][0]);
        if (c != null) {
            c.recycle();
        }
        c = Bitmap.createScaledBitmap(decodeResource2, (int) (i * 64.0f), (int) (j * 65.0f), true);
        b.a(decodeResource2);
        Bitmap decodeResource3 = BitmapFactory.decodeResource(resources, a.d[p - 1][1]);
        if (d != null) {
            d.recycle();
        }
        d = Bitmap.createScaledBitmap(decodeResource3, (int) (i * 64.0f), (int) (j * 65.0f), true);
        b.a(decodeResource3);
        Bitmap decodeResource4 = BitmapFactory.decodeResource(resources, R.drawable.musicon);
        if (e != null) {
            e.recycle();
        }
        e = Bitmap.createScaledBitmap(decodeResource4, (int) (i * 64.0f), (int) (j * 65.0f), true);
        b.a(decodeResource4);
        Bitmap decodeResource5 = BitmapFactory.decodeResource(resources, R.drawable.musicoff);
        if (f != null) {
            f.recycle();
        }
        f = Bitmap.createScaledBitmap(decodeResource5, (int) (i * 64.0f), (int) (j * 65.0f), true);
        b.a(decodeResource5);
        g = new Rect(0, i2 - c.getHeight(), c.getWidth() + 0, (i2 - c.getHeight()) + c.getHeight());
        h = new Rect(0, (g.top - e.getHeight()) - 10, e.getWidth(), g.top - 10);
    }

    public static boolean a(int i2, int i3) {
        return g.contains(i2, i3);
    }

    public static boolean b(int i2, int i3) {
        return h.contains(i2, i3);
    }

    public static void a(Canvas canvas) {
        canvas.drawBitmap(k, 0.0f, 0.0f, (Paint) null);
        if (com.feelingtouch.shooting.level.a.a < 3) {
            com.feelingtouch.shooting.f.a.a(canvas, String.valueOf(b.d / 1000), l, m);
        } else {
            com.feelingtouch.shooting.f.a.a(canvas, String.valueOf(b.f), l, m);
        }
        com.feelingtouch.shooting.f.a.a(canvas, String.valueOf(b.c), n, o);
    }

    public static void b(Canvas canvas) {
        if (a) {
            canvas.drawBitmap(d, (float) g.left, (float) g.top, (Paint) null);
        } else {
            canvas.drawBitmap(c, (float) g.left, (float) g.top, (Paint) null);
        }
        if (com.feelingtouch.shooting.c.b.a) {
            canvas.drawBitmap(e, (float) h.left, (float) h.top, (Paint) null);
        } else {
            canvas.drawBitmap(f, (float) h.left, (float) h.top, (Paint) null);
        }
    }
}
