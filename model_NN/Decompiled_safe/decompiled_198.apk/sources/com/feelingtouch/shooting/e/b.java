package com.feelingtouch.shooting.e;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.feelingtouch.e.a.a;
import com.feelingtouch.shooting.R;

/* compiled from: Score */
public final class b {
    public static int a;
    public static int b;
    public static int c;
    public static long d;
    public static int e;
    public static int f;
    private static boolean g;
    private static StringBuffer h;
    private static Bitmap[] i = null;

    public static void a(Context context, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("LEVEL");
        stringBuffer.append(String.valueOf(i2));
        StringBuffer stringBuffer2 = new StringBuffer();
        h = stringBuffer2;
        stringBuffer2.append(stringBuffer);
        h.append("_SCORE");
        b = a.b(context, h.toString(), 0);
        a(context);
        c = 0;
        d = com.feelingtouch.shooting.level.a.c;
        f = 100;
        g = false;
    }

    public static void a(Context context) {
        e = a.b(context, "SKILL", 0);
    }

    public static void a() {
        f = 100;
        c = 0;
        d = com.feelingtouch.shooting.level.a.c;
        g = false;
    }

    public static void a(int i2) {
        c -= i2;
    }

    public static void b(int i2) {
        c += i2;
    }

    public static void b() {
        e += 2;
    }

    public static void b(Context context) {
        if (c > b) {
            a.a(context, h.toString(), c);
        }
        a.a(context, "SKILL", e);
    }

    public static boolean c() {
        if (d <= 0) {
            d = 0;
            return true;
        }
        if (!g) {
            d -= 40;
        }
        return false;
    }

    public static boolean d() {
        if (c >= com.feelingtouch.shooting.level.a.b) {
            return true;
        }
        return false;
    }

    public static void a(boolean z) {
        g = z;
    }

    public static void c(Context context) {
        int b2 = a.b(context, "LEVEL1_SCORE", 0) + a.b(context, "LEVEL2_SCORE", 0) + a.b(context, "LEVEL3_SCORE", 0);
        a = b2;
        a = (int) (((float) b2) * (1.0f + (((float) e) / 100.0f)));
        a.a(context, "GROWTHVALUE", a);
    }

    public static void a(Resources resources, float f2, float f3) {
        if (i == null) {
            i = new Bitmap[7];
            Bitmap decodeResource = BitmapFactory.decodeResource(resources, R.drawable.militaryranks);
            Bitmap createBitmap = Bitmap.createBitmap(decodeResource, 0, 0, 35, 34);
            i[0] = Bitmap.createScaledBitmap(createBitmap, (int) (35.0f * f2), (int) (34.0f * f3), true);
            com.feelingtouch.age.b.a.a(createBitmap);
            Bitmap createBitmap2 = Bitmap.createBitmap(decodeResource, 35, 0, 35, 34);
            i[1] = Bitmap.createScaledBitmap(createBitmap2, (int) (35.0f * f2), (int) (34.0f * f3), true);
            com.feelingtouch.age.b.a.a(createBitmap2);
            Bitmap createBitmap3 = Bitmap.createBitmap(decodeResource, 70, 0, 35, 34);
            i[2] = Bitmap.createScaledBitmap(createBitmap3, (int) (35.0f * f2), (int) (34.0f * f3), true);
            com.feelingtouch.age.b.a.a(createBitmap3);
            Bitmap createBitmap4 = Bitmap.createBitmap(decodeResource, 105, 0, 35, 34);
            i[3] = Bitmap.createScaledBitmap(createBitmap4, (int) (35.0f * f2), (int) (34.0f * f3), true);
            com.feelingtouch.age.b.a.a(createBitmap4);
            Bitmap createBitmap5 = Bitmap.createBitmap(decodeResource, 140, 0, 35, 34);
            i[4] = Bitmap.createScaledBitmap(createBitmap5, (int) (35.0f * f2), (int) (34.0f * f3), true);
            com.feelingtouch.age.b.a.a(createBitmap5);
            Bitmap createBitmap6 = Bitmap.createBitmap(decodeResource, 175, 0, 35, 34);
            i[5] = Bitmap.createScaledBitmap(createBitmap6, (int) (35.0f * f2), (int) (34.0f * f3), true);
            com.feelingtouch.age.b.a.a(createBitmap6);
            Bitmap createBitmap7 = Bitmap.createBitmap(decodeResource, 210, 0, 35, 34);
            i[6] = Bitmap.createScaledBitmap(createBitmap7, (int) (35.0f * f2), (int) (34.0f * f3), true);
            com.feelingtouch.age.b.a.a(createBitmap7);
            com.feelingtouch.age.b.a.a(decodeResource);
        }
    }

    public static void a(Canvas canvas, int i2, int i3, int i4) {
        if (i4 == 0 || i4 <= 3) {
            if (i4 == 0) {
                canvas.drawBitmap(i[6], (float) i2, (float) i3, (Paint) null);
            } else {
                canvas.drawBitmap(i[i4 - 1], (float) i2, (float) i3, (Paint) null);
            }
        } else if (i4 <= 10) {
            canvas.drawBitmap(i[3], (float) i2, (float) i3, (Paint) null);
        } else if (i4 <= 50) {
            canvas.drawBitmap(i[4], (float) i2, (float) i3, (Paint) null);
        } else if (i4 <= 100) {
            canvas.drawBitmap(i[5], (float) i2, (float) i3, (Paint) null);
        } else {
            canvas.drawBitmap(i[6], (float) i2, (float) i3, (Paint) null);
        }
    }
}
