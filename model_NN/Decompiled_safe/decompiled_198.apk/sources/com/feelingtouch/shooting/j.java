package com.feelingtouch.shooting;

import android.content.DialogInterface;
import android.widget.EditText;
import com.feelingtouch.e.k;
import com.feelingtouch.e.l;
import com.feelingtouch.shooting.f.a;

/* compiled from: MainMenuView */
final class j implements DialogInterface.OnClickListener {
    final /* synthetic */ MainMenuView a;
    private final /* synthetic */ EditText b;

    j(MainMenuView mainMenuView, EditText editText) {
        this.a = mainMenuView;
        this.b = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String editable = this.b.getText().toString();
        if (k.b(editable)) {
            String a2 = a.a(editable, a.b);
            com.feelingtouch.e.a.a.b(this.a.a, "profile_name", a2);
            this.a.aV = a2;
            this.a.ap = true;
            return;
        }
        l.a(this.a.a, R.string.gamebox_name_empty);
    }
}
