package com.feelingtouch.shooting.d;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.feelingtouch.age.b.a;

/* compiled from: Magazine */
public final class d {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private Rect g;
    private Bitmap[] h;
    private Paint i = new Paint();
    private int j = 255;
    private int k = 0;

    public d(Resources resources, int i2, int i3) {
        Bitmap decodeResource = BitmapFactory.decodeResource(resources, i2);
        this.c = decodeResource.getWidth() / (i3 + 1);
        this.d = decodeResource.getHeight();
        this.a = i3;
        this.e = 0;
        this.f = 0;
        this.g = new Rect(this.e, this.f, this.e + this.c, this.f + this.d);
        this.h = new Bitmap[(i3 + 1)];
        for (int i4 = 0; i4 < this.h.length; i4++) {
            this.h[i4] = Bitmap.createBitmap(decodeResource, this.c * i4, 0, this.c, this.d);
        }
    }

    public final void a() {
        this.b = this.a;
    }

    public final void b() {
        if (this.b > 0) {
            this.b--;
        }
    }

    public final boolean c() {
        return this.b == 0;
    }

    public final Rect d() {
        return this.g;
    }

    public final void a(float f2, float f3) {
        this.c = (int) (((float) this.c) * f2);
        this.d = (int) (((float) this.d) * f3);
        for (int i2 = 0; i2 < this.h.length; i2++) {
            Bitmap bitmap = this.h[i2];
            this.h[i2] = Bitmap.createScaledBitmap(bitmap, this.c, this.d, true);
            a.a(bitmap);
        }
        this.g.set(new Rect(this.e - 10, this.f - 30, this.e + this.c + 10, this.f + this.d + 10));
    }

    public final void a(int i2, int i3) {
        this.e = i2;
        this.f = i3;
        this.g.set(new Rect(this.e - 10, this.f - 30, this.e + this.c + 10, this.f + this.d + 10));
    }

    public final int e() {
        return this.c;
    }

    public final int f() {
        return this.d;
    }

    public final void a(Canvas canvas) {
        canvas.drawBitmap(this.h[this.b], (float) this.e, (float) this.f, this.i);
        if (this.b != 0) {
            this.j = 255;
        } else if (this.k == 0) {
            this.j = (int) (((double) this.j) * 0.9d);
            if (this.j < 100) {
                this.k = 1;
            }
        } else {
            this.j = (int) (((double) this.j) * 1.1d);
            if (this.j > 200) {
                this.k = 0;
            }
        }
        this.i.setAlpha(this.j);
    }

    public final void g() {
        for (Bitmap recycle : this.h) {
            recycle.recycle();
        }
    }
}
