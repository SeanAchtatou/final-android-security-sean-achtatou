package com.feelingtouch.shooting.d;

import android.content.res.Resources;
import android.graphics.Canvas;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.c.b;
import com.feelingtouch.shooting.c.d;

/* compiled from: Shotgun */
public final class e {
    public static boolean a = true;
    private static int b = 7;
    private static b c;
    private static d d;

    public static void a(Resources resources, int i, float f, float f2) {
        if (c != null) {
            c.e();
        }
        b bVar = new b(resources, R.drawable.shotgun, 353);
        c = bVar;
        bVar.a(f, f2);
        c.a((int) ((680.0f * f) - ((float) c.f())), i - c.g());
        if (d != null) {
            d.g();
        }
        d dVar = new d(resources, R.drawable.magazine02, b);
        d = dVar;
        dVar.a(f, f2);
        d.a((int) ((338.0f * f) - ((float) d.e())), (i - d.f()) - 5);
    }

    public static void a(Canvas canvas) {
        c.b(canvas);
        d.a(canvas);
    }

    public static boolean a(int i, int i2) {
        if (d.d().contains(i, i2)) {
            return true;
        }
        return false;
    }

    public static void a() {
        if (d.c()) {
            a = false;
            b.a(b.d);
            return;
        }
        c.h();
        d.b();
        b.a(b.g);
        d.b();
    }

    public static void b() {
        d.a();
        b.a(b.b);
        a = true;
    }

    public static void c() {
        c.e();
        d.g();
    }
}
