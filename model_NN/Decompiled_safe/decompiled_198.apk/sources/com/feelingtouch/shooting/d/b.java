package com.feelingtouch.shooting.d;

import android.content.res.Resources;
import com.feelingtouch.age.a.a;
import com.feelingtouch.age.a.c;

/* compiled from: Gun */
public final class b extends c {
    private a[] p = null;

    public b(Resources resources, int i, int i2) {
        b(com.feelingtouch.age.a.b.a(resources, i, i2, 0, 1));
        c();
        this.p = com.feelingtouch.age.a.b.a(resources, i, i2, 1, 1);
        this.a = 0;
        this.b = 0;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final void h() {
        a(this.p);
    }

    public final void a(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public final void a(float f, float f2) {
        super.a(f, f2);
    }

    public final void e() {
        super.e();
        a[] aVarArr = this.p;
        if (aVarArr != null) {
            for (a a : aVarArr) {
                a.a();
            }
        }
    }
}
