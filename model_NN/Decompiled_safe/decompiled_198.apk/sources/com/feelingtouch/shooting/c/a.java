package com.feelingtouch.shooting.c;

import android.content.Context;
import android.media.MediaPlayer;
import com.feelingtouch.shooting.R;

/* compiled from: BgMusicManager */
public final class a {
    public static boolean a = true;
    private static MediaPlayer b = null;
    private static Context c = null;

    public static void a(Context context) {
        if (b == null || c == null) {
            c = context;
            MediaPlayer create = MediaPlayer.create(context, (int) R.raw.bg);
            b = create;
            create.setLooping(true);
        }
        a = com.feelingtouch.e.a.a.a(context, "bgMusic").booleanValue();
    }

    public static void a() {
        if (a && b != null) {
            b.pause();
        }
    }

    public static void b() {
        if (a) {
            b.start();
        }
    }

    public static void c() {
        if (b != null) {
            b.stop();
            b.release();
            b = null;
        }
    }
}
