package com.feelingtouch.shooting.c;

import android.content.Context;
import android.os.Vibrator;
import com.feelingtouch.e.a.a;

/* compiled from: VibrateManager */
public final class d {
    public static boolean a = true;
    private static Context b = null;
    private static Vibrator c;

    public static void a(Context context) {
        b = context;
        a = a.a(context, "vibration").booleanValue();
        if (c == null) {
            c = (Vibrator) b.getSystemService("vibrator");
        }
    }

    public static void a() {
        if (a) {
            c.vibrate(new long[]{100, 20}, -1);
        }
    }

    public static void b() {
        if (a) {
            c.vibrate(new long[]{100, 80}, -1);
        }
    }
}
