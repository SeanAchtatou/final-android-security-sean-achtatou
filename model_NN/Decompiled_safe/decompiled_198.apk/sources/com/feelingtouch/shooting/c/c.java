package com.feelingtouch.shooting.c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/* compiled from: StampAnimation */
public final class c {
    public static boolean a;
    private static Paint b;
    private static int c;
    private static Bitmap d;
    private static Rect e;
    private static Rect f;
    private static int g;
    private static int h;
    private static int i;
    private static int j;
    private static int k;
    private static int l;
    private static int m;
    private static int n;
    private static boolean o;
    private static boolean p;
    private static long q;
    private static float r;
    private static int s;
    private static int t;

    public static void a(Bitmap bitmap, int i2, int i3) {
        if (d == null) {
            d = bitmap;
            s = bitmap.getWidth();
            t = d.getHeight();
            e = new Rect(0, 0, s, t);
            g = s * 2;
            h = t * 2;
            i = i2 - (s / 2);
            j = i3 - (t / 2);
            b = new Paint();
        }
    }

    public static boolean a() {
        return d != null;
    }

    public static void b() {
        o = true;
        a = false;
        p = false;
        c = 50;
        r = 1.0f;
    }

    public static void a(Canvas canvas) {
        if (o) {
            if (((double) r) > 0.5d) {
                r = (float) (((double) r) - 0.05d);
                k = (int) (((float) g) * r);
                l = (int) (((float) h) * r);
                m = i + ((g - k) / 2);
                n = j + ((h - l) / 2);
                if (c <= 255) {
                    c += 10;
                } else {
                    c = 255;
                }
            } else if (!p) {
                p = true;
                q = System.currentTimeMillis();
            }
            b.setAlpha(c);
            if (p && System.currentTimeMillis() - q > 1000) {
                o = false;
                a = true;
            }
            f = new Rect(m, n, m + k, n + l);
            canvas.drawBitmap(d, e, f, b);
        }
    }
}
