package com.feelingtouch.shooting.c;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import com.feelingtouch.e.a.a;
import com.feelingtouch.shooting.R;
import java.util.HashMap;

/* compiled from: EffectMusicManager */
public final class b {
    public static boolean a = true;
    public static int b = 0;
    public static int c = 1;
    public static int d = 2;
    public static int e = 3;
    public static int f = 4;
    public static int g = 5;
    private static Context h;
    private static int i;
    private static SoundPool j;
    private static HashMap k;

    public static void a(Context context) {
        if (h == null) {
            h = context;
            try {
                j = new SoundPool(100, 3, 100);
                k = new HashMap();
                i = ((AudioManager) h.getSystemService("audio")).getStreamVolume(3);
                a(R.raw.load_bullet, b);
                a(R.raw.bang, c);
                a(R.raw.block, d);
                a(R.raw.spin, e);
                a(R.raw.ak47_fire, f);
                a(R.raw.shotgun, g);
            } catch (Exception e2) {
            }
        }
        a = a.a(context, "effectMusic").booleanValue();
    }

    private static void a(int i2, int i3) {
        k.put(Integer.valueOf(i3), Integer.valueOf(j.load(h, i2, i3)));
    }

    private static void b(int i2) {
        try {
            j.play(((Integer) k.get(Integer.valueOf(i2))).intValue(), ((float) i) * 0.5f, ((float) i) * 0.5f, 1, 0, 1.0f);
        } catch (Exception e2) {
        }
    }

    public static void a(int i2) {
        if (a) {
            b(i2);
        }
    }

    public static void a() {
        if (a) {
            b(3);
        }
    }

    public static void b() {
        i = ((AudioManager) h.getSystemService("audio")).getStreamVolume(3);
    }
}
