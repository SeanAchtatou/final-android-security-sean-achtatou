package com.feelingtouch.shooting;

import android.view.SurfaceHolder;

/* compiled from: LoadingView */
final class d extends Thread {
    SurfaceHolder a;
    boolean b = true;
    final /* synthetic */ c c;

    public d(c cVar, SurfaceHolder surfaceHolder) {
        this.c = cVar;
        this.a = surfaceHolder;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0120  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r14 = this;
            long r0 = java.lang.System.currentTimeMillis()
            r2 = 0
        L_0x0006:
            com.feelingtouch.shooting.c r4 = r14.c
            boolean r4 = r4.i
            if (r4 == 0) goto L_0x0006
            r4 = 0
            r10 = r4
            r11 = r2
            r3 = r0
            r0 = r10
            r1 = r11
        L_0x0014:
            boolean r5 = r14.b
            if (r5 != 0) goto L_0x0019
            return
        L_0x0019:
            r5 = r3
        L_0x001a:
            r7 = 40
            long r7 = r7 + r3
            int r7 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r7 < 0) goto L_0x0094
            android.view.SurfaceHolder r3 = r14.a     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            monitor-enter(r3)     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            android.view.SurfaceHolder r4 = r14.a     // Catch:{ all -> 0x00b7 }
            android.graphics.Canvas r0 = r4.lockCanvas()     // Catch:{ all -> 0x00b7 }
            r4 = 255(0xff, float:3.57E-43)
            r7 = 19
            r8 = 12
            r9 = 14
            r0.drawARGB(r4, r7, r8, r9)     // Catch:{ all -> 0x00b7 }
            android.graphics.Bitmap[] r4 = com.feelingtouch.shooting.c.f     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r7 = r14.c     // Catch:{ all -> 0x00b7 }
            int r7 = r7.k     // Catch:{ all -> 0x00b7 }
            r4 = r4[r7]     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r7 = r14.c     // Catch:{ all -> 0x00b7 }
            int r7 = r7.g     // Catch:{ all -> 0x00b7 }
            float r7 = (float) r7     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r8 = r14.c     // Catch:{ all -> 0x00b7 }
            int r8 = r8.h     // Catch:{ all -> 0x00b7 }
            float r8 = (float) r8     // Catch:{ all -> 0x00b7 }
            r9 = 0
            r0.drawBitmap(r4, r7, r8, r9)     // Catch:{ all -> 0x00b7 }
            r7 = 40
            long r1 = r1 + r7
            r7 = 100
            int r4 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r4 <= 0) goto L_0x0079
            r1 = 0
            com.feelingtouch.shooting.c r4 = r14.c     // Catch:{ all -> 0x00b7 }
            int r7 = r4.k     // Catch:{ all -> 0x00b7 }
            int r7 = r7 + 1
            r4.k = r7     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r4 = r14.c     // Catch:{ all -> 0x00b7 }
            int r4 = r4.k     // Catch:{ all -> 0x00b7 }
            r7 = 12
            if (r4 != r7) goto L_0x0079
            com.feelingtouch.shooting.c r4 = r14.c     // Catch:{ all -> 0x00b7 }
            r7 = 0
            r4.k = r7     // Catch:{ all -> 0x00b7 }
        L_0x0079:
            com.feelingtouch.shooting.c r4 = r14.c     // Catch:{ all -> 0x00b7 }
            boolean r4 = r4.j     // Catch:{ all -> 0x00b7 }
            if (r4 == 0) goto L_0x008a
            com.feelingtouch.shooting.c r4 = r14.c     // Catch:{ all -> 0x00b7 }
            int r4 = r4.a     // Catch:{ all -> 0x00b7 }
            switch(r4) {
                case 1: goto L_0x0099;
                case 2: goto L_0x00d8;
                case 3: goto L_0x00f1;
                case 4: goto L_0x00b1;
                default: goto L_0x008a;
            }     // Catch:{ all -> 0x00b7 }
        L_0x008a:
            monitor-exit(r3)     // Catch:{ all -> 0x00b7 }
            if (r0 == 0) goto L_0x0092
            android.view.SurfaceHolder r3 = r14.a
            r3.unlockCanvasAndPost(r0)
        L_0x0092:
            r3 = r5
            goto L_0x0014
        L_0x0094:
            long r5 = java.lang.System.currentTimeMillis()
            goto L_0x001a
        L_0x0099:
            android.content.Intent r4 = new android.content.Intent     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r7 = r14.c     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.LoadingActivity r7 = r7.c     // Catch:{ all -> 0x00b7 }
            java.lang.Class<com.feelingtouch.shooting.level.Level01Activity> r8 = com.feelingtouch.shooting.level.Level01Activity.class
            r4.<init>(r7, r8)     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r7 = r14.c     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.LoadingActivity r7 = r7.c     // Catch:{ all -> 0x00b7 }
            int r8 = com.feelingtouch.shooting.b.a.g     // Catch:{ all -> 0x00b7 }
            r7.startActivityForResult(r4, r8)     // Catch:{ all -> 0x00b7 }
        L_0x00b1:
            com.feelingtouch.shooting.c r4 = r14.c     // Catch:{ all -> 0x00b7 }
            r4.j = false     // Catch:{ all -> 0x00b7 }
            goto L_0x008a
        L_0x00b7:
            r4 = move-exception
            r7 = r1
            r1 = r0
            r0 = r4
            monitor-exit(r3)     // Catch:{ Exception -> 0x00bd }
            throw r0     // Catch:{ Exception -> 0x00bd }
        L_0x00bd:
            r0 = move-exception
            r2 = r7
        L_0x00bf:
            r0.printStackTrace()     // Catch:{ all -> 0x010a }
            com.feelingtouch.c.b r4 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x010a }
            java.lang.Class r7 = r14.getClass()     // Catch:{ all -> 0x010a }
            r4.a(r7, r0)     // Catch:{ all -> 0x010a }
            if (r1 == 0) goto L_0x0120
            android.view.SurfaceHolder r0 = r14.a
            r0.unlockCanvasAndPost(r1)
            r0 = r1
            r10 = r2
            r1 = r10
            r3 = r5
            goto L_0x0014
        L_0x00d8:
            android.content.Intent r4 = new android.content.Intent     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r7 = r14.c     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.LoadingActivity r7 = r7.c     // Catch:{ all -> 0x00b7 }
            java.lang.Class<com.feelingtouch.shooting.level.Level02Activity> r8 = com.feelingtouch.shooting.level.Level02Activity.class
            r4.<init>(r7, r8)     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r7 = r14.c     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.LoadingActivity r7 = r7.c     // Catch:{ all -> 0x00b7 }
            int r8 = com.feelingtouch.shooting.b.a.g     // Catch:{ all -> 0x00b7 }
            r7.startActivityForResult(r4, r8)     // Catch:{ all -> 0x00b7 }
            goto L_0x00b1
        L_0x00f1:
            android.content.Intent r4 = new android.content.Intent     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r7 = r14.c     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.LoadingActivity r7 = r7.c     // Catch:{ all -> 0x00b7 }
            java.lang.Class<com.feelingtouch.shooting.level.Level03Activity> r8 = com.feelingtouch.shooting.level.Level03Activity.class
            r4.<init>(r7, r8)     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.c r7 = r14.c     // Catch:{ all -> 0x00b7 }
            com.feelingtouch.shooting.LoadingActivity r7 = r7.c     // Catch:{ all -> 0x00b7 }
            int r8 = com.feelingtouch.shooting.b.a.g     // Catch:{ all -> 0x00b7 }
            r7.startActivityForResult(r4, r8)     // Catch:{ all -> 0x00b7 }
            goto L_0x00b1
        L_0x010a:
            r0 = move-exception
        L_0x010b:
            if (r1 == 0) goto L_0x0112
            android.view.SurfaceHolder r2 = r14.a
            r2.unlockCanvasAndPost(r1)
        L_0x0112:
            throw r0
        L_0x0113:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x010b
        L_0x0118:
            r3 = move-exception
            r10 = r3
            r11 = r0
            r0 = r10
            r12 = r1
            r2 = r12
            r1 = r11
            goto L_0x00bf
        L_0x0120:
            r0 = r1
            r10 = r2
            r1 = r10
            r3 = r5
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.shooting.d.run():void");
    }
}
