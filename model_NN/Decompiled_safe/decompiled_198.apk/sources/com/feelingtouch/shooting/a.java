package com.feelingtouch.shooting;

import android.content.Context;
import com.feelingtouch.e.g;
import com.feelingtouch.e.k;
import com.feelingtouch.e.l;
import com.feelingtouch.gamebox.q;

/* compiled from: GameboxUtil */
public final class a {
    public static boolean a = false;
    /* access modifiers changed from: private */
    public q b = null;

    public a(q qVar) {
        this.b = qVar;
    }

    public final void a(Context context, int i) {
        String a2 = com.feelingtouch.e.a.a.a(context, "profile_name", "");
        if (k.b(a2)) {
            String a3 = com.feelingtouch.e.a.a(context);
            if (k.a(a3)) {
                l.a(context, R.string.gamebox_get_imei_error);
                return;
            }
            a = false;
            new g(context, new b(this, a3, a2, context, i), (byte) 0).a();
            return;
        }
        l.a(context, R.string.gamebox_name_empty);
    }
}
