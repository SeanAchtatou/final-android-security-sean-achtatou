package com.feelingtouch.shooting;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import com.feelingtouch.e.b;
import com.feelingtouch.gamebox.q;
import com.feelingtouch.shooting.c.d;
import com.feelingtouch.shooting.f.a;

public class MissionActivity extends Activity implements q {
    /* access modifiers changed from: private */
    public MissionView a;
    private Handler b = new l(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.mission_view);
        this.a = (MissionView) findViewById(R.id.mission_view);
        b.a(this);
        a.a(this);
        com.feelingtouch.shooting.c.a.a(this);
        com.feelingtouch.shooting.c.b.a(this);
        d.a(this);
        com.feelingtouch.b.a.b(this);
    }

    public final Handler a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        com.feelingtouch.shooting.c.a.b();
        this.a.b();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        com.feelingtouch.shooting.c.a.a();
        this.a.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        if (i == 25 || i == 24) {
            com.feelingtouch.shooting.c.b.b();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
