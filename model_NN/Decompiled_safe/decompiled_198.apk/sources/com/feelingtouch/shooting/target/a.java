package com.feelingtouch.shooting.target;

import android.content.res.Resources;
import android.graphics.Rect;
import com.feelingtouch.age.a.b;
import com.feelingtouch.age.a.c;
import java.util.Random;

/* compiled from: Duck */
public final class a extends c {
    protected int A;
    protected int B;
    protected boolean C = false;
    private int D;
    private int E;
    private int F;
    private int G;
    private int H;
    private int I;
    private long J;
    private int K = this.y.nextInt(90);
    private int L = this.y.nextInt(60);
    private int M = this.y.nextInt(50);
    private int N = this.y.nextInt(50);
    private int O;
    protected int p = 0;
    protected int q = 0;
    protected int r = 0;
    protected int s = 0;
    protected int t;
    protected int u;
    protected com.feelingtouch.age.a.a[] v = null;
    protected com.feelingtouch.age.a.a[] w = null;
    protected com.feelingtouch.age.a.a[] x = null;
    protected Random y = new Random();
    protected boolean z = false;

    public a(int i, int i2, Resources resources, int i3, int i4) {
        int i5;
        if (this.K > this.L) {
            i5 = this.K;
        } else {
            i5 = this.L;
        }
        this.O = i5;
        this.t = i;
        this.u = i2;
        this.v = b.a(resources, i3, i4, 0, 4, new int[]{4, 2, 2, 4});
        b(this.v);
        c();
        this.w = b.a(resources, i3, i4, 4, 4, new int[]{2, 2, 2, 1});
        this.x = b.a(this.w);
    }

    public a(a aVar) {
        super(aVar);
        int i;
        int i2;
        int i3;
        int i4;
        if (this.K > this.L) {
            i = this.K;
        } else {
            i = this.L;
        }
        this.O = i;
        this.w = aVar.w;
        this.x = aVar.x;
        this.t = aVar.t;
        this.u = aVar.u;
        this.A = aVar.A;
        this.B = aVar.B;
        if (com.feelingtouch.shooting.e.b.c <= 20) {
            this.D = 1;
            this.G = 1;
            this.E = 1;
            this.H = 1;
            this.F = 1;
            this.I = 1;
            a(new int[]{7, 4, 4, 7});
        } else if (com.feelingtouch.shooting.e.b.c <= 100) {
            this.D = 2;
            this.G = 1;
            this.E = 2;
            this.H = 1;
            this.F = 2;
            this.I = 1;
            a(new int[]{6, 3, 3, 6});
        } else if (com.feelingtouch.shooting.e.b.c <= 200) {
            this.D = 3;
            this.G = 2;
            this.E = 3;
            this.H = 1;
            this.F = 3;
            this.I = 1;
            a(new int[]{5, 3, 3, 5});
        } else if (com.feelingtouch.shooting.e.b.c <= 350) {
            this.D = 4;
            this.G = 2;
            this.E = 3;
            this.H = 2;
            this.F = 3;
            this.I = 2;
            a(new int[]{4, 2, 2, 4});
        } else if (com.feelingtouch.shooting.e.b.c <= 500) {
            this.D = 5;
            this.G = 2;
            this.E = 4;
            this.H = 2;
            this.F = 4;
            this.I = 2;
            a(new int[]{3, 2, 2, 3});
        } else if (com.feelingtouch.shooting.e.b.c <= 650) {
            this.D = 6;
            this.G = 3;
            this.E = 5;
            this.H = 2;
            this.F = 4;
            this.I = 2;
            a(new int[]{3, 2, 2, 3});
        } else if (com.feelingtouch.shooting.e.b.c <= 750) {
            this.D = 7;
            this.G = 3;
            this.E = 5;
            this.H = 2;
            this.F = 5;
            this.I = 2;
            a(new int[]{2, 2, 2, 2});
        } else {
            this.D = 8;
            this.G = 5;
            this.E = 6;
            this.H = 3;
            this.F = 5;
            this.I = 3;
            a(new int[]{2, 2, 2, 2});
        }
        if (this.A == 1) {
            if (this.y.nextBoolean()) {
                this.l = true;
                this.a = -f();
            } else {
                this.l = false;
                this.a = this.t;
            }
            this.b = this.y.nextInt(170);
            int nextInt = this.y.nextInt(6) + this.D;
            if (!this.l) {
                i4 = -1;
            } else {
                i4 = 1;
            }
            this.r = nextInt * i4;
            this.s = this.y.nextInt(2) + this.G;
        } else if (this.A == 2) {
            this.l = true;
            this.a = (int) (((float) (this.y.nextInt(348) + 68)) * ((((float) this.t) * 1.0f) / 854.0f));
            this.b = (int) (((float) (this.y.nextInt(99) + 181)) * ((((float) this.u) * 1.0f) / 485.0f));
            int nextInt2 = this.y.nextInt(6) + this.E;
            if (!this.l) {
                i3 = -1;
            } else {
                i3 = 1;
            }
            this.r = nextInt2 * i3;
            this.s = this.y.nextInt(1) + this.H;
        } else if (this.A == 3) {
            this.l = true;
            this.a = (int) (((float) (this.y.nextInt(348) + 68)) * ((((float) this.t) * 1.0f) / 854.0f));
            this.b = (int) (((float) (this.y.nextInt(99) + 181)) * ((((float) this.u) * 1.0f) / 485.0f));
            int nextInt3 = this.y.nextInt(6) + this.F;
            if (!this.l) {
                i2 = -1;
            } else {
                i2 = 1;
            }
            this.r = nextInt3 * i2;
            this.s = this.y.nextInt(1) + this.I;
        }
        this.C = true;
    }

    private void a(int[] iArr) {
        for (int i = 0; i < this.d.size(); i++) {
            ((com.feelingtouch.age.a.a) this.d.get(i)).b = iArr[i];
        }
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            ((com.feelingtouch.age.a.a) this.e.get(i2)).b = iArr[i2];
        }
    }

    public final void h() {
        if (this.l) {
            a(this.w);
        } else {
            a(this.x);
        }
        this.C = false;
        this.z = true;
    }

    public final boolean i() {
        int f = f();
        int g = g();
        if (this.a >= f * -2 || this.l) {
            if (this.a > (f * 2) + this.t && this.l) {
                if (this.B == 1) {
                    if (com.feelingtouch.shooting.e.b.f != 0) {
                        com.feelingtouch.shooting.e.b.f--;
                    }
                } else if (com.feelingtouch.shooting.e.b.f != 100) {
                    com.feelingtouch.shooting.e.b.f++;
                }
                return true;
            } else if (this.b >= g * -2 || this.l) {
                if (this.b > (g * 2) + this.t && this.l) {
                    if (this.B == 1) {
                        if (com.feelingtouch.shooting.e.b.f != 0) {
                            com.feelingtouch.shooting.e.b.f--;
                        }
                    } else if (com.feelingtouch.shooting.e.b.f != 100) {
                        com.feelingtouch.shooting.e.b.f++;
                    }
                    return true;
                } else if (!this.z || d() != 0) {
                    return false;
                } else {
                    if (this.B == 2 && com.feelingtouch.shooting.e.b.f != 0) {
                        com.feelingtouch.shooting.e.b.f--;
                    }
                    return true;
                }
            } else {
                if (this.B == 1) {
                    if (com.feelingtouch.shooting.e.b.f != 0) {
                        com.feelingtouch.shooting.e.b.f--;
                    }
                } else if (com.feelingtouch.shooting.e.b.f != 100) {
                    com.feelingtouch.shooting.e.b.f++;
                }
                return true;
            }
        } else {
            if (this.B == 1) {
                if (com.feelingtouch.shooting.e.b.f != 0) {
                    com.feelingtouch.shooting.e.b.f--;
                }
            } else if (com.feelingtouch.shooting.e.b.f != 100) {
                com.feelingtouch.shooting.e.b.f++;
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final Rect b() {
        return super.b();
    }

    public final void a(float f, float f2) {
        super.a(f, f2);
    }

    private static void c(com.feelingtouch.age.a.a[] aVarArr) {
        if (aVarArr != null) {
            for (com.feelingtouch.age.a.a a : aVarArr) {
                a.a();
            }
        }
    }

    public final void e() {
        super.e();
        c(this.v);
        c(this.w);
        c(this.x);
    }

    public final void a(long j) {
        if (j - this.J > 1000) {
            this.J = j;
            if (this.A == 2 || this.A == 3) {
                this.K = this.y.nextInt(80);
                this.L = this.y.nextInt(75);
                this.M = this.y.nextInt(70);
                this.N = this.y.nextInt(50);
            } else {
                this.K = this.y.nextInt(90);
                this.L = this.y.nextInt(60);
                this.M = this.y.nextInt(50);
                this.N = this.y.nextInt(50);
            }
            this.O = this.K > this.L ? this.K : this.L;
        }
        if (this.O == this.K) {
            this.q = 0;
        } else if (this.O == this.L) {
            if (this.M > this.N) {
                this.q = -this.s;
            } else if (this.A == 1) {
                this.q = this.s;
            }
        }
        this.p = this.r;
        if (!this.z) {
            this.a += this.p;
            this.b += this.q;
        }
    }
}
