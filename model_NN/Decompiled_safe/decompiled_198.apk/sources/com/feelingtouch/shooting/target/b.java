package com.feelingtouch.shooting.target;

import android.content.res.Resources;
import android.graphics.Canvas;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.e.a;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/* compiled from: DuckManager */
public final class b {
    private static Random a = new Random();
    private static long b;
    private static long c;
    private static long d;
    private static LinkedList e = new LinkedList();
    private static LinkedList f = new LinkedList();
    private static LinkedList g = new LinkedList();
    private static a h;
    private static a i;
    private static a j;
    private static a k;
    private static a l;
    private static a m;
    private static float n;
    private static float o;
    private static Object p = new Object();
    private static a q;
    private static int r = 0;
    private static int s = 0;

    public static void a(Resources resources, int i2, int i3, float f2, float f3) {
        n = f2;
        o = f3;
        q = new a(resources);
        if (h != null) {
            h.e();
        }
        a aVar = new a(i2, i3, resources, R.drawable.duck, 106);
        h = aVar;
        aVar.A = 1;
        h.B = 1;
        if (i != null) {
            i.e();
        }
        a aVar2 = new a(h);
        i = aVar2;
        aVar2.A = 2;
        i.B = 1;
        if (j != null) {
            j.e();
        }
        a aVar3 = new a(h);
        j = aVar3;
        aVar3.A = 3;
        j.B = 1;
        if (k != null) {
            k.e();
        }
        a aVar4 = new a(i2, i3, resources, R.drawable.pigeons, 78);
        k = aVar4;
        aVar4.A = 1;
        k.B = 2;
        if (l != null) {
            l.e();
        }
        a aVar5 = new a(k);
        l = aVar5;
        aVar5.A = 2;
        l.B = 2;
        if (m != null) {
            m.e();
        }
        a aVar6 = new a(k);
        m = aVar6;
        aVar6.A = 2;
        m.B = 2;
        b = 0;
        c = 0;
        d = 0;
        a();
    }

    private static void c() {
        if (com.feelingtouch.shooting.e.b.c <= 20) {
            return;
        }
        if (com.feelingtouch.shooting.e.b.c <= 100) {
            if (r != 1) {
                r = 1;
                com.feelingtouch.shooting.f.a.a();
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 200) {
            if (r != 2) {
                r = 2;
                com.feelingtouch.shooting.f.a.a();
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 350) {
            if (r != 3) {
                r = 3;
                com.feelingtouch.shooting.f.a.a();
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 500) {
            if (r != 4) {
                r = 4;
                com.feelingtouch.shooting.f.a.a();
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 650) {
            if (r != 5) {
                r = 5;
                com.feelingtouch.shooting.f.a.a();
            }
        } else if (com.feelingtouch.shooting.e.b.c <= 750) {
            if (r != 6) {
                r = 6;
                com.feelingtouch.shooting.f.a.a();
            }
        } else if (r != 7) {
            r = 7;
            com.feelingtouch.shooting.f.a.a();
        }
    }

    public static void a(int i2, int i3) {
        synchronized (p) {
            Iterator it = e.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                if (aVar.b().contains(i2, i3) && aVar.C) {
                    aVar.h();
                    c();
                    if (aVar.B == 1) {
                        com.feelingtouch.shooting.e.b.b(1);
                        com.feelingtouch.shooting.f.a.a("+1", i2, i3);
                        s++;
                    } else {
                        if (com.feelingtouch.shooting.e.b.c - 2 >= 0) {
                            com.feelingtouch.shooting.e.b.a(2);
                            com.feelingtouch.shooting.f.a.a("-2", i2, i3);
                        }
                        s = 0;
                    }
                }
            }
            Iterator it2 = f.iterator();
            while (it2.hasNext()) {
                a aVar2 = (a) it2.next();
                if (aVar2.b().contains(i2, i3) && aVar2.C) {
                    aVar2.h();
                    c();
                    if (aVar2.B == 1) {
                        com.feelingtouch.shooting.e.b.b(2);
                        com.feelingtouch.shooting.f.a.a("+2", i2, i3);
                        s++;
                    } else {
                        if (com.feelingtouch.shooting.e.b.c - 2 >= 0) {
                            com.feelingtouch.shooting.e.b.a(2);
                            com.feelingtouch.shooting.f.a.a("-2", i2, i3);
                        }
                        s = 0;
                    }
                }
            }
            Iterator it3 = g.iterator();
            while (it3.hasNext()) {
                a aVar3 = (a) it3.next();
                if (aVar3.b().contains(i2, i3) && aVar3.C) {
                    aVar3.h();
                    c();
                    if (aVar3.B == 1) {
                        com.feelingtouch.shooting.e.b.b(3);
                        com.feelingtouch.shooting.f.a.a("+3", i2, i3);
                        s++;
                    } else {
                        if (com.feelingtouch.shooting.e.b.c - 1 >= 0) {
                            com.feelingtouch.shooting.e.b.a(1);
                            com.feelingtouch.shooting.f.a.a("-1", i2, i3);
                        }
                        s = 0;
                    }
                }
            }
            if (s >= 5) {
                if (s < 10) {
                    com.feelingtouch.shooting.e.b.b(1);
                } else if (s < 30) {
                    com.feelingtouch.shooting.e.b.b(2);
                } else if (s < 50) {
                    com.feelingtouch.shooting.e.b.b(3);
                } else if (s < 70) {
                    com.feelingtouch.shooting.e.b.b(4);
                } else if (s < 100) {
                    com.feelingtouch.shooting.e.b.b(5);
                } else if (s < 150) {
                    com.feelingtouch.shooting.e.b.b(6);
                } else {
                    com.feelingtouch.shooting.e.b.b(7);
                }
            }
        }
    }

    public static void a(long j2) {
        if (e.size() < 4) {
            if (b == 0) {
                b = j2;
            }
            if (j2 > b + 80) {
                b = j2;
                if (a.nextInt(80) > a.nextInt(30)) {
                    a aVar = new a(h);
                    aVar.a(n, o);
                    e.add(aVar);
                } else {
                    a aVar2 = new a(k);
                    aVar2.a(n, o);
                    e.add(aVar2);
                }
            }
        }
        if (f.size() < 4) {
            if (c == 0) {
                c = j2;
            }
            if (j2 > c + 80) {
                c = j2;
                if (a.nextInt(80) > a.nextInt(30)) {
                    a aVar3 = new a(i);
                    aVar3.a(n * 0.7f, o * 0.7f);
                    f.add(aVar3);
                } else {
                    a aVar4 = new a(l);
                    aVar4.a(n * 0.7f, o * 0.7f);
                    f.add(aVar4);
                }
            }
        }
        if (g.size() < 4) {
            if (d == 0) {
                d = j2;
            }
            if (j2 > d + 80) {
                d = j2;
                if (a.nextInt(60) > a.nextInt(40)) {
                    a aVar5 = new a(j);
                    aVar5.a(n * 0.5f, o * 0.5f);
                    g.add(aVar5);
                    return;
                }
                a aVar6 = new a(m);
                aVar6.a(n * 0.5f, o * 0.5f);
                g.add(aVar6);
            }
        }
    }

    public static void b(long j2) {
        synchronized (p) {
            Iterator it = e.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                aVar.a(j2);
                if (aVar.i()) {
                    it.remove();
                }
            }
            Iterator it2 = f.iterator();
            while (it2.hasNext()) {
                a aVar2 = (a) it2.next();
                aVar2.a(j2);
                if (aVar2.i()) {
                    it2.remove();
                }
            }
            Iterator it3 = g.iterator();
            while (it3.hasNext()) {
                a aVar3 = (a) it3.next();
                aVar3.a(j2);
                if (aVar3.i()) {
                    it3.remove();
                }
            }
        }
    }

    public static void a(Canvas canvas) {
        synchronized (p) {
            Iterator it = e.iterator();
            while (it.hasNext()) {
                ((a) it.next()).b(canvas);
            }
            Iterator it2 = f.iterator();
            while (it2.hasNext()) {
                ((a) it2.next()).b(canvas);
            }
            q.a(canvas, n, o, s);
        }
    }

    public static void b(Canvas canvas) {
        synchronized (p) {
            Iterator it = g.iterator();
            while (it.hasNext()) {
                ((a) it.next()).b(canvas);
            }
        }
    }

    public static void a() {
        synchronized (p) {
            s = 0;
            e.clear();
            f.clear();
            g.clear();
        }
    }

    public static void b() {
        h.e();
        i.e();
        j.e();
        k.e();
        l.e();
        m.e();
        q.a();
    }
}
