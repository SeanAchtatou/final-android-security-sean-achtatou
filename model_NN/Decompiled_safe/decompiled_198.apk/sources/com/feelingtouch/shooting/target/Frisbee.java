package com.feelingtouch.shooting.target;

import android.content.res.Resources;
import android.graphics.Rect;
import com.feelingtouch.age.a.a;
import com.feelingtouch.age.a.b;
import com.feelingtouch.age.a.c;
import java.util.Hashtable;

public class Frisbee extends c {
    private int A;
    private int B;
    private int C;
    private int D;
    private int E = 0;
    private int F;
    protected int p = 3;
    protected int q;
    protected int r;
    protected a[] s = null;
    protected a[] t = null;
    protected a[] u = null;
    protected boolean v = false;
    protected boolean w = false;
    protected int x;
    private int y;
    private int z;

    private static native int caculategameRunY(int i, int i2, int i3);

    public Frisbee(int i, int i2, Resources resources, int i3, int i4) {
        this.q = i;
        this.r = i2;
        this.x = i4;
        this.s = b.a(resources, i3, 150, 0, 1);
        b(this.s);
        c();
        this.t = b.a(resources, i3, 150, 1, 3, new int[]{2, 2, 1});
        this.u = b.a(this.t);
    }

    public Frisbee(Frisbee frisbee) {
        super(frisbee);
        this.t = frisbee.t;
        this.u = frisbee.u;
        this.q = frisbee.q;
        this.r = frisbee.r;
        this.x = frisbee.x;
        this.p = frisbee.p;
    }

    public final void a(float f, float f2, int i, int i2, boolean z2, int i3) {
        a(f, f2);
        this.D = this.r - g();
        this.z = i;
        this.A = i2;
        this.l = z2;
        this.y = i3;
        if (!this.l) {
            this.y -= f();
        }
        this.a = this.y;
        this.b = this.D;
        int i4 = this.z;
        int intValue = ((Integer) com.feelingtouch.shooting.f.b.a.get(Integer.valueOf(i4 > 90 ? i4 - 90 : 90 - i4))).intValue();
        if (i4 > 90) {
            intValue = -intValue;
        }
        this.B = intValue * this.A;
        int i5 = this.z;
        Hashtable hashtable = com.feelingtouch.shooting.f.b.a;
        if (i5 > 90) {
            i5 = 180 - i5;
        }
        this.C = ((Integer) hashtable.get(Integer.valueOf(i5))).intValue() * this.A;
        this.w = true;
    }

    public final boolean h() {
        int f = f();
        int g = g();
        if (this.a < f * -2 && !this.l) {
            return true;
        }
        if (this.a > (f * 2) + this.q && this.l) {
            return true;
        }
        if (this.b < g * -2 && !this.l) {
            return true;
        }
        if (this.b > (g * 2) + this.q && this.l) {
            return true;
        }
        if (!this.v || d() != 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final void i() {
        if (!h()) {
            this.E++;
            if (this.E == this.p) {
                this.F++;
                this.E = 0;
                int i = this.B;
                int i2 = this.C;
                int i3 = this.F;
                int[] iArr = {(i * i3) / 100000, caculategameRunY(this.D, i2, i3)};
                this.a = iArr[0];
                if (!this.l) {
                    this.a = this.y - this.a;
                }
                this.b = iArr[1];
            }
        }
    }

    public final Rect b() {
        return super.b();
    }

    public final void j() {
        if (this.l) {
            a(this.t);
        } else {
            a(this.u);
        }
        this.w = false;
        this.v = true;
    }

    private static void c(a[] aVarArr) {
        if (aVarArr != null) {
            for (a a : aVarArr) {
                a.a();
            }
        }
    }

    public final void e() {
        super.e();
        c(this.t);
        c(this.u);
        c(this.s);
    }
}
