package com.feelingtouch.shooting.target;

import android.content.res.Resources;
import android.graphics.Canvas;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.e.a;
import com.feelingtouch.shooting.e.b;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/* compiled from: FrisbeeManager */
public final class c {
    private static Random a = new Random();
    private static d b;
    private static d c;
    private static LinkedList d = new LinkedList();
    private static Frisbee e;
    private static Frisbee f;
    private static long g;
    private static float h;
    private static float i;
    private static int j;
    private static int k;
    private static Object l = new Object();
    private static a m;
    private static int n = 0;

    public static void a(Resources resources, int i2, int i3, float f2, float f3) {
        j = i2;
        k = i3;
        h = f2;
        i = f3;
        m = new a(resources);
        if (b != null) {
            b.e();
        }
        if (c != null) {
            c.e();
        }
        b = new d(resources);
        c = new d(b);
        b.a(0.8f * f2, 0.8f * f3);
        c.a(0.8f * f2, 0.8f * f3);
        b.a(true, 0, i3);
        c.a(false, i2, i3);
        if (e != null) {
            e.e();
        }
        if (f != null) {
            f.e();
        }
        e = new Frisbee(i2, i3, resources, R.drawable.frisbee_red, 2);
        f = new Frisbee(i2, i3, resources, R.drawable.frisbee, 1);
        c();
    }

    public static void a(int i2, int i3) {
        int i4 = 0;
        synchronized (l) {
            Iterator it = d.iterator();
            while (it.hasNext()) {
                Frisbee frisbee = (Frisbee) it.next();
                if (!frisbee.b().contains(i2, i3) || !frisbee.w) {
                    i4++;
                } else {
                    frisbee.j();
                    if (frisbee.x == 1) {
                        b.b(1);
                        com.feelingtouch.shooting.f.a.a("+1", i2, i3);
                    } else {
                        if (b.c - 1 >= 0) {
                            b.a(1);
                            com.feelingtouch.shooting.f.a.a("-1", i2, i3);
                        }
                        i4++;
                    }
                }
            }
            if (i4 != d.size()) {
                int i5 = n + 1;
                n = i5;
                if (i5 >= 5) {
                    if (n < 10) {
                        b.b(1);
                    } else if (n < 20) {
                        b.b(2);
                    } else if (n < 30) {
                        b.b(3);
                    } else if (n < 40) {
                        b.b(4);
                    } else {
                        b.b(5);
                    }
                }
            } else {
                n = 0;
            }
        }
    }

    public static int a() {
        int size;
        synchronized (l) {
            size = d.size();
        }
        return size;
    }

    public static void a(long j2) {
        synchronized (l) {
            if (g == 0) {
                g = j2;
            }
            if (j2 > g + 500) {
                g = j2;
                int nextInt = (a.nextInt(9) * 3) + 30;
                int nextInt2 = (a.nextInt(11) * 3) + 30;
                if (a.nextInt(70) > a.nextInt(40)) {
                    Frisbee frisbee = new Frisbee(f);
                    if (a.nextBoolean()) {
                        b.h();
                        frisbee.a(h, i, nextInt2, nextInt, true, 10);
                        d.add(frisbee);
                    } else {
                        c.h();
                        frisbee.a(h, i, nextInt2, nextInt, false, j - 10);
                        d.add(frisbee);
                    }
                } else {
                    Frisbee frisbee2 = new Frisbee(e);
                    if (a.nextBoolean()) {
                        b.h();
                        frisbee2.a(h, i, nextInt2, nextInt, true, 10);
                        d.add(frisbee2);
                    } else {
                        c.h();
                        frisbee2.a(h, i, nextInt2, nextInt, false, j - 10);
                        d.add(frisbee2);
                    }
                }
            }
        }
    }

    public static void a(Canvas canvas) {
        synchronized (l) {
            Iterator it = d.iterator();
            while (it.hasNext()) {
                ((Frisbee) it.next()).b(canvas);
            }
            b.b(canvas);
            c.b(canvas);
            m.a(canvas, h, i, n);
        }
    }

    public static void b() {
        synchronized (l) {
            Iterator it = d.iterator();
            while (it.hasNext()) {
                Frisbee frisbee = (Frisbee) it.next();
                frisbee.i();
                if (frisbee.h()) {
                    it.remove();
                }
            }
        }
    }

    public static void a(int i2) {
        e.p = i2;
        f.p = i2;
    }

    public static void c() {
        synchronized (l) {
            n = 0;
            d.clear();
            g = 0;
        }
    }

    public static void d() {
        b.e();
        c.e();
        e.e();
        f.e();
        m.a();
    }
}
