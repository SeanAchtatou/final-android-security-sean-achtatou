package com.feelingtouch.shooting.target;

import android.content.res.Resources;
import android.graphics.Rect;
import com.feelingtouch.age.a.a;
import com.feelingtouch.age.a.b;
import com.feelingtouch.age.a.c;

/* compiled from: Target */
public final class e extends c {
    protected a[] p = null;
    protected a[] q = null;
    protected a[] r = null;
    protected a[] s = new a[3];
    protected a[] t = new a[2];
    public boolean u = false;

    public e(Resources resources, int i, int i2) {
        b(b.a(resources, i, i2, 0, 1));
        this.p = b.a(resources, i, i2, 1, 4);
        this.r = b.a(resources, i, i2, 2, 30);
        this.q = b.a(resources, i, i2, 3, 15);
        System.arraycopy(this.p, 0, this.s, 0, 1);
        System.arraycopy(this.r, 0, this.s, 1, 1);
        System.arraycopy(this.p, 0, this.s, 2, 1);
        System.arraycopy(this.q, 0, this.t, 0, 1);
        System.arraycopy(this.p, 0, this.t, 1, 1);
        this.a = 0;
        this.b = 0;
    }

    public e(e eVar) {
        super(eVar);
        this.p = eVar.p;
        this.q = eVar.q;
        this.r = eVar.r;
        this.s = eVar.s;
        this.t = eVar.t;
    }

    public final void a(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public final void h() {
        a(this.s);
        this.u = true;
    }

    public final void i() {
        a(this.t);
        this.u = false;
    }

    public final void b(int i, int i2) {
        for (a aVar : this.r) {
            aVar.b = i;
            aVar.c = i;
        }
        for (a aVar2 : this.q) {
            aVar2.b = i2;
            aVar2.c = i2;
        }
        System.arraycopy(this.p, 0, this.s, 0, 1);
        System.arraycopy(this.r, 0, this.s, 1, 1);
        System.arraycopy(this.p, 0, this.s, 2, 1);
        System.arraycopy(this.q, 0, this.t, 0, 1);
        System.arraycopy(this.p, 0, this.t, 1, 1);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (d() == 0) {
            this.u = false;
        }
    }

    public final Rect b() {
        return super.b();
    }

    public final void a(float f, float f2) {
        super.a(f, f2);
    }

    private static void c(a[] aVarArr) {
        if (aVarArr != null) {
            for (a a : aVarArr) {
                a.a();
            }
        }
    }

    public final void e() {
        super.e();
        c(this.p);
        c(this.q);
        c(this.r);
        c(this.s);
        c(this.t);
    }
}
