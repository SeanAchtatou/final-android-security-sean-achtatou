package com.feelingtouch.shooting.target;

import android.content.res.Resources;
import com.feelingtouch.age.a.a;
import com.feelingtouch.age.a.b;
import com.feelingtouch.age.a.c;
import com.feelingtouch.shooting.R;

/* compiled from: Launcher */
public final class d extends c {
    private a[] p = null;
    private a[] q = null;

    public d(Resources resources) {
        b(b.a(resources, R.drawable.launcher, 150, 0, 1));
        c();
        this.p = b.a(resources, R.drawable.launcher, 150, 1, 3);
        this.q = b.a(this.p);
        this.a = 0;
        this.b = 0;
    }

    public d(d dVar) {
        super(dVar);
        this.p = dVar.p;
        this.q = dVar.q;
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final void h() {
        if (this.l) {
            a(this.p);
        } else {
            a(this.q);
        }
    }

    public final void a(boolean z, int i, int i2) {
        this.a = i;
        if (!z) {
            this.a -= f();
        }
        this.b = i2 - g();
        this.l = z;
    }

    public final void a(float f, float f2) {
        super.a(f, f2);
    }

    private static void c(a[] aVarArr) {
        if (aVarArr != null) {
            for (a a : aVarArr) {
                a.a();
            }
        }
    }

    public final void e() {
        super.e();
        c(this.p);
        c(this.q);
    }
}
