package com.feelingtouch.shooting.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import com.feelingtouch.shooting.MainMenuActivity;
import com.feelingtouch.shooting.R;

public class ShootingWidget extends AppWidgetProvider {
    static final ComponentName a = new ComponentName("com.feelingtouch.shooting", "com.feelingtouch.shooting.widget.ShootingWidget");

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget);
        remoteViews.setOnClickPendingIntent(R.id.btn, PendingIntent.getActivity(context, 0, new Intent(context, MainMenuActivity.class), 268435456));
        AppWidgetManager.getInstance(context).updateAppWidget(a, remoteViews);
    }
}
