package com.feelingtouch.shooting;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.feelingtouch.b.a;
import com.feelingtouch.e.b;
import com.feelingtouch.e.k;

/* compiled from: MainMenuActivity */
final class i implements View.OnClickListener {
    final /* synthetic */ MainMenuActivity a;

    i(MainMenuActivity mainMenuActivity) {
        this.a = mainMenuActivity;
    }

    public final void onClick(View view) {
        Uri parse;
        String b = this.a.f.b();
        if (k.b(b)) {
            if (b.b) {
                parse = Uri.parse("market://search?q=pname:" + b);
            } else {
                parse = Uri.parse("http://www.feelingtouch.com/fgames/" + b + ".apk");
            }
            try {
                this.a.startActivity(new Intent("android.intent.action.VIEW", parse));
            } catch (Exception e) {
                if (b.b) {
                    this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.feelingtouch.com/fgames/" + b + ".apk")));
                }
            }
            this.a.getSharedPreferences("game_ad_version_preference", 1).edit().putBoolean("game_ad_isclicked", true).commit();
            a.a(this.a.getApplicationContext(), b);
        }
    }
}
