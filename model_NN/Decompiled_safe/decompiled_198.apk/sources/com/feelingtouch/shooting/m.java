package com.feelingtouch.shooting;

import android.view.SurfaceHolder;

/* compiled from: MissionView */
final class m extends Thread {
    SurfaceHolder a;
    boolean b = true;
    final /* synthetic */ MissionView c;

    public m(MissionView missionView, SurfaceHolder surfaceHolder) {
        this.c = missionView;
        this.a = surfaceHolder;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0113  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r7 = this;
            r1 = 0
        L_0x0001:
            com.feelingtouch.shooting.MissionView r0 = r7.c
            boolean r0 = r0.aj
            if (r0 == 0) goto L_0x0001
            r0 = r1
        L_0x000a:
            boolean r1 = r7.b
            if (r1 != 0) goto L_0x000f
            return
        L_0x000f:
            com.feelingtouch.shooting.MissionView r1 = r7.c
            boolean r1 = r1.ai
            if (r1 != 0) goto L_0x000a
            android.view.SurfaceHolder r1 = r7.a     // Catch:{ Exception -> 0x010e, all -> 0x0109 }
            monitor-enter(r1)     // Catch:{ Exception -> 0x010e, all -> 0x0109 }
            android.view.SurfaceHolder r2 = r7.a     // Catch:{ all -> 0x00c6 }
            android.graphics.Canvas r0 = r2.lockCanvas()     // Catch:{ all -> 0x00c6 }
            r2 = 255(0xff, float:3.57E-43)
            r3 = 255(0xff, float:3.57E-43)
            r4 = 255(0xff, float:3.57E-43)
            r5 = 255(0xff, float:3.57E-43)
            r0.drawARGB(r2, r3, r4, r5)     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            android.graphics.Bitmap r2 = r2.j     // Catch:{ all -> 0x00c6 }
            r3 = 0
            r4 = 0
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            android.graphics.Bitmap[] r2 = r2.W     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r3 = r7.c     // Catch:{ all -> 0x00c6 }
            int r3 = r3.X     // Catch:{ all -> 0x00c6 }
            r4 = 1
            int r3 = r3 - r4
            r2 = r2[r3]     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r3 = r7.c     // Catch:{ all -> 0x00c6 }
            int r3 = r3.Y     // Catch:{ all -> 0x00c6 }
            float r3 = (float) r3     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r4 = r7.c     // Catch:{ all -> 0x00c6 }
            int r4 = r4.Z     // Catch:{ all -> 0x00c6 }
            float r4 = (float) r4     // Catch:{ all -> 0x00c6 }
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            boolean r2 = r2.P     // Catch:{ all -> 0x00c6 }
            if (r2 != 0) goto L_0x00ad
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            android.graphics.Bitmap r2 = r2.K     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r3 = r7.c     // Catch:{ all -> 0x00c6 }
            int r3 = r3.M     // Catch:{ all -> 0x00c6 }
            float r3 = (float) r3     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r4 = r7.c     // Catch:{ all -> 0x00c6 }
            int r4 = r4.N     // Catch:{ all -> 0x00c6 }
            float r4 = (float) r4     // Catch:{ all -> 0x00c6 }
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x00c6 }
        L_0x0079:
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            boolean r2 = r2.V     // Catch:{ all -> 0x00c6 }
            if (r2 != 0) goto L_0x00e4
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            android.graphics.Bitmap r2 = r2.Q     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r3 = r7.c     // Catch:{ all -> 0x00c6 }
            int r3 = r3.S     // Catch:{ all -> 0x00c6 }
            float r3 = (float) r3     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r4 = r7.c     // Catch:{ all -> 0x00c6 }
            int r4 = r4.T     // Catch:{ all -> 0x00c6 }
            float r4 = (float) r4     // Catch:{ all -> 0x00c6 }
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x00c6 }
        L_0x0099:
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView.a(r2, r0)     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView.b(r2, r0)     // Catch:{ all -> 0x00c6 }
            monitor-exit(r1)     // Catch:{ all -> 0x00c6 }
            if (r0 == 0) goto L_0x000a
            android.view.SurfaceHolder r1 = r7.a
            r1.unlockCanvasAndPost(r0)
            goto L_0x000a
        L_0x00ad:
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            android.graphics.Bitmap r2 = r2.L     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r3 = r7.c     // Catch:{ all -> 0x00c6 }
            int r3 = r3.M     // Catch:{ all -> 0x00c6 }
            float r3 = (float) r3     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r4 = r7.c     // Catch:{ all -> 0x00c6 }
            int r4 = r4.N     // Catch:{ all -> 0x00c6 }
            float r4 = (float) r4     // Catch:{ all -> 0x00c6 }
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x00c6 }
            goto L_0x0079
        L_0x00c6:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            monitor-exit(r1)     // Catch:{ Exception -> 0x00cc, all -> 0x00fd }
            throw r0     // Catch:{ Exception -> 0x00cc, all -> 0x00fd }
        L_0x00cc:
            r0 = move-exception
            r1 = r2
        L_0x00ce:
            r0.printStackTrace()     // Catch:{ all -> 0x0107 }
            com.feelingtouch.c.b r2 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x0107 }
            java.lang.Class r3 = r7.getClass()     // Catch:{ all -> 0x0107 }
            r2.a(r3, r0)     // Catch:{ all -> 0x0107 }
            if (r1 == 0) goto L_0x0113
            android.view.SurfaceHolder r0 = r7.a
            r0.unlockCanvasAndPost(r1)
            r0 = r1
            goto L_0x000a
        L_0x00e4:
            com.feelingtouch.shooting.MissionView r2 = r7.c     // Catch:{ all -> 0x00c6 }
            android.graphics.Bitmap r2 = r2.R     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r3 = r7.c     // Catch:{ all -> 0x00c6 }
            int r3 = r3.S     // Catch:{ all -> 0x00c6 }
            float r3 = (float) r3     // Catch:{ all -> 0x00c6 }
            com.feelingtouch.shooting.MissionView r4 = r7.c     // Catch:{ all -> 0x00c6 }
            int r4 = r4.T     // Catch:{ all -> 0x00c6 }
            float r4 = (float) r4     // Catch:{ all -> 0x00c6 }
            r5 = 0
            r0.drawBitmap(r2, r3, r4, r5)     // Catch:{ all -> 0x00c6 }
            goto L_0x0099
        L_0x00fd:
            r0 = move-exception
            r1 = r2
        L_0x00ff:
            if (r1 == 0) goto L_0x0106
            android.view.SurfaceHolder r2 = r7.a
            r2.unlockCanvasAndPost(r1)
        L_0x0106:
            throw r0
        L_0x0107:
            r0 = move-exception
            goto L_0x00ff
        L_0x0109:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00ff
        L_0x010e:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00ce
        L_0x0113:
            r0 = r1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.shooting.m.run():void");
    }
}
