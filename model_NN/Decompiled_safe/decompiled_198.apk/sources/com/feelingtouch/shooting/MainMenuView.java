package com.feelingtouch.shooting;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.feelingtouch.age.b.a;
import com.feelingtouch.gamebox.HighScore;
import com.feelingtouch.shooting.c.c;
import com.feelingtouch.shooting.f.b;

public class MainMenuView extends SurfaceView implements SurfaceHolder.Callback {
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private int H;
    private int I;
    private boolean J;
    private long K = 0;
    private boolean L = false;
    private boolean M = false;
    private boolean N = false;
    private boolean O = false;
    private boolean P = false;
    private boolean Q = false;
    private boolean R = false;
    private int S;
    private int T;
    private int U;
    private Rect V;
    private Rect W;
    private Rect X;
    private Rect Y;
    private Rect Z;
    /* access modifiers changed from: private */
    public Context a;
    private Bitmap aA;
    private Bitmap aB;
    private Bitmap aC;
    private Bitmap aD;
    private Bitmap aE;
    private Rect aF = new Rect();
    private Rect aG = new Rect();
    private Rect aH = new Rect();
    private int aI = 1;
    private int aJ;
    private int aK = 0;
    private int aL;
    private int aM;
    private int aN;
    private int aO;
    private int aP;
    private int aQ;
    private int aR;
    private int aS;
    private int aT;
    private int aU;
    /* access modifiers changed from: private */
    public String aV;
    private int aW;
    private String aX;
    private String aY;
    private int aZ;
    private Rect aa;
    private Rect ab;
    /* access modifiers changed from: private */
    public Rect ac;
    /* access modifiers changed from: private */
    public Rect ad;
    /* access modifiers changed from: private */
    public boolean ae = false;
    /* access modifiers changed from: private */
    public boolean af = false;
    private boolean ag = true;
    private long ah = 0;
    private int ai = 0;
    private boolean aj = false;
    private boolean ak = false;
    private boolean al = false;
    private boolean am = false;
    private boolean an = true;
    private boolean ao = false;
    /* access modifiers changed from: private */
    public boolean ap = false;
    private boolean aq = false;
    private boolean ar = false;
    private boolean as = false;
    private boolean at = false;
    private boolean au = false;
    private Bitmap av;
    private Bitmap aw;
    private Bitmap ax;
    private Bitmap ay;
    private Bitmap az;
    private MainMenuActivity b;
    private int ba;
    private int bb;
    private int bc;
    private int bd;
    private int be;
    private int bf;
    private int bg;
    private k c;
    private Thread d = null;
    private int e;
    private int f;
    /* access modifiers changed from: private */
    public Bitmap g = null;
    private Bitmap h = null;
    private Bitmap i = null;
    private Bitmap j = null;
    private Bitmap k = null;
    private Bitmap l = null;
    /* access modifiers changed from: private */
    public Bitmap m = null;
    private Bitmap n = null;
    private Bitmap o = null;
    private Bitmap p = null;
    private Bitmap q = null;
    /* access modifiers changed from: private */
    public Bitmap r = null;
    /* access modifiers changed from: private */
    public Bitmap s = null;
    private float t;
    private float u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public MainMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setKeepScreenOn(true);
        this.a = context;
        this.b = (MainMenuActivity) context;
        this.c = new k(this, holder);
    }

    public MainMenuView(Context context) {
        super(context);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setKeepScreenOn(true);
        this.a = context;
        this.b = (MainMenuActivity) context;
        this.c = new k(this, holder);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.af = false;
        this.e = i3;
        this.f = i4;
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.mainmenu_background);
        this.g = Bitmap.createScaledBitmap(decodeResource, this.e, this.f, true);
        this.t = ((float) this.g.getWidth()) / ((float) decodeResource.getWidth());
        this.u = ((float) this.g.getHeight()) / ((float) decodeResource.getHeight());
        b.a(decodeResource);
        Bitmap decodeResource2 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_play_normal);
        this.i = Bitmap.createScaledBitmap(decodeResource2, (int) (this.t * ((float) decodeResource2.getWidth())), (int) (this.u * ((float) decodeResource2.getHeight())), true);
        b.a(decodeResource2);
        Bitmap decodeResource3 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_profile_press);
        this.k = Bitmap.createScaledBitmap(decodeResource3, (int) (this.t * ((float) decodeResource3.getWidth())), (int) (this.u * ((float) decodeResource3.getHeight())), true);
        b.a(decodeResource3);
        Bitmap decodeResource4 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_play_press);
        this.j = Bitmap.createScaledBitmap(decodeResource4, (int) (this.t * ((float) decodeResource4.getWidth())), (int) (this.u * ((float) decodeResource4.getHeight())), true);
        b.a(decodeResource4);
        Bitmap decodeResource5 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_more_normal);
        this.m = Bitmap.createScaledBitmap(decodeResource5, (int) (this.t * ((float) decodeResource5.getWidth())), (int) (this.u * ((float) decodeResource5.getHeight())), true);
        b.a(decodeResource5);
        Bitmap decodeResource6 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_more_press);
        this.l = Bitmap.createScaledBitmap(decodeResource6, (int) (this.t * ((float) decodeResource6.getWidth())), (int) (this.u * ((float) decodeResource6.getHeight())), true);
        b.a(decodeResource6);
        Bitmap decodeResource7 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_setting_press);
        this.n = Bitmap.createScaledBitmap(decodeResource7, (int) (this.t * ((float) decodeResource7.getWidth())), (int) (this.u * ((float) decodeResource7.getHeight())), true);
        b.a(decodeResource7);
        Bitmap decodeResource8 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_gamebox_press);
        this.o = Bitmap.createScaledBitmap(decodeResource8, (int) (this.t * ((float) decodeResource8.getWidth())), (int) (this.u * ((float) decodeResource8.getHeight())), true);
        b.a(decodeResource8);
        Bitmap decodeResource9 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_feedback_press);
        this.p = Bitmap.createScaledBitmap(decodeResource9, (int) (this.t * ((float) decodeResource9.getWidth())), (int) (this.u * ((float) decodeResource9.getHeight())), true);
        b.a(decodeResource9);
        Bitmap decodeResource10 = BitmapFactory.decodeResource(getResources(), R.drawable.menu_btn_about_press);
        this.q = Bitmap.createScaledBitmap(decodeResource10, (int) (this.t * ((float) decodeResource10.getWidth())), (int) (this.u * ((float) decodeResource10.getHeight())), true);
        b.a(decodeResource10);
        Bitmap decodeResource11 = BitmapFactory.decodeResource(getResources(), R.drawable.profilebg);
        this.av = Bitmap.createScaledBitmap(decodeResource11, (int) (this.t * ((float) decodeResource11.getWidth())), (int) (this.u * ((float) decodeResource11.getHeight())), true);
        b.a(decodeResource11);
        Bitmap decodeResource12 = BitmapFactory.decodeResource(getResources(), R.drawable.btn_ok_normal);
        this.az = Bitmap.createScaledBitmap(decodeResource12, (int) (this.t * ((float) decodeResource12.getWidth())), (int) (this.u * ((float) decodeResource12.getHeight())), true);
        b.a(decodeResource12);
        Bitmap decodeResource13 = BitmapFactory.decodeResource(getResources(), R.drawable.btn_ok_down);
        this.aA = Bitmap.createScaledBitmap(decodeResource13, (int) (this.t * ((float) decodeResource13.getWidth())), (int) (this.u * ((float) decodeResource13.getHeight())), true);
        b.a(decodeResource13);
        Bitmap decodeResource14 = BitmapFactory.decodeResource(getResources(), R.drawable.prev_normal_btn);
        this.aB = Bitmap.createScaledBitmap(decodeResource14, (int) (this.t * ((float) decodeResource14.getWidth())), (int) (this.u * ((float) decodeResource14.getHeight())), true);
        b.a(decodeResource14);
        Bitmap decodeResource15 = BitmapFactory.decodeResource(getResources(), R.drawable.prev_down_btn);
        this.aC = Bitmap.createScaledBitmap(decodeResource15, (int) (this.t * ((float) decodeResource15.getWidth())), (int) (this.u * ((float) decodeResource15.getHeight())), true);
        b.a(decodeResource15);
        Bitmap decodeResource16 = BitmapFactory.decodeResource(getResources(), R.drawable.next_normal_btn);
        this.aD = Bitmap.createScaledBitmap(decodeResource16, (int) (this.t * ((float) decodeResource16.getWidth())), (int) (this.u * ((float) decodeResource16.getHeight())), true);
        b.a(decodeResource16);
        Bitmap decodeResource17 = BitmapFactory.decodeResource(getResources(), R.drawable.next_down_btn);
        this.aE = Bitmap.createScaledBitmap(decodeResource17, (int) (this.t * ((float) decodeResource17.getWidth())), (int) (this.u * ((float) decodeResource17.getHeight())), true);
        b.a(decodeResource17);
        Bitmap decodeResource18 = BitmapFactory.decodeResource(getResources(), R.drawable.profile_people);
        Bitmap createBitmap = Bitmap.createBitmap(decodeResource18, 0, 0, 165, decodeResource18.getHeight());
        this.aw = Bitmap.createScaledBitmap(createBitmap, (int) (this.t * 165.0f), (int) (this.u * ((float) decodeResource18.getHeight())), true);
        a.a(createBitmap);
        Bitmap createBitmap2 = Bitmap.createBitmap(decodeResource18, 165, 0, 165, decodeResource18.getHeight());
        this.ax = Bitmap.createScaledBitmap(createBitmap2, (int) (this.t * 165.0f), (int) (this.u * ((float) decodeResource18.getHeight())), true);
        a.a(createBitmap2);
        Bitmap createBitmap3 = Bitmap.createBitmap(decodeResource18, 330, 0, 165, decodeResource18.getHeight());
        this.ay = Bitmap.createScaledBitmap(createBitmap3, (int) (this.t * 165.0f), (int) (this.u * ((float) decodeResource18.getHeight())), true);
        a.a(createBitmap3);
        b.a(decodeResource18);
        Bitmap decodeResource19 = BitmapFactory.decodeResource(getResources(), R.drawable.facebook);
        this.r = Bitmap.createScaledBitmap(decodeResource19, (int) (((float) decodeResource19.getWidth()) * 0.8f), (int) (((float) decodeResource19.getHeight()) * 0.8f), true);
        b.a(decodeResource19);
        Bitmap decodeResource20 = BitmapFactory.decodeResource(getResources(), R.drawable.twitter);
        this.s = Bitmap.createScaledBitmap(decodeResource20, (int) (((float) decodeResource20.getWidth()) * 0.8f), (int) (((float) decodeResource20.getHeight()) * 0.8f), true);
        b.a(decodeResource20);
        this.aW = com.feelingtouch.e.a.a.b(this.a, "RANK", 0);
        this.aX = String.valueOf(com.feelingtouch.e.a.a.b(this.a, "GROWTHVALUE", 0));
        this.aY = String.valueOf(this.aW);
        this.aI = com.feelingtouch.e.a.a.b(this.a, "PROFILE_ID", 1);
        this.aV = com.feelingtouch.e.a.a.a(getContext(), "profile_name", "");
        if (this.aV.equals("")) {
            this.an = true;
        } else {
            this.an = false;
            this.aV = com.feelingtouch.shooting.f.a.a(this.aV, com.feelingtouch.shooting.f.a.b);
            com.feelingtouch.e.a.a.b(this.a, "profile_name", this.aV);
        }
        this.aT = (int) (this.t * 47.0f);
        this.aU = (int) (this.u * 51.0f);
        this.aP = (int) (this.t * 37.0f);
        this.aQ = (int) (this.u * 383.0f);
        this.aR = (int) (this.t * 166.0f);
        this.aS = (int) (this.u * 383.0f);
        this.aN = (int) (this.t * 319.0f);
        this.aO = (int) (this.u * 405.0f);
        this.aL = -this.av.getWidth();
        this.aM = 0;
        this.aJ = this.aL;
        this.aK = 20;
        this.aF.top = this.aO;
        this.aF.bottom = this.aO + this.aA.getHeight();
        this.aG.top = this.aQ;
        this.aG.bottom = this.aQ + this.aB.getHeight();
        this.aH.top = this.aS;
        this.aH.bottom = this.aS + this.aD.getHeight();
        this.aZ = (int) (341.0f * this.t);
        this.ba = (int) (196.0f * this.u);
        this.bb = (int) (361.0f * this.t);
        this.bc = (int) (208.0f * this.u);
        this.bd = (int) (361.0f * this.t);
        this.be = (int) (280.0f * this.u);
        this.bf = (int) (361.0f * this.t);
        this.bg = (int) (324.0f * this.u);
        this.S = (int) (225.0f * this.t);
        this.T = this.f;
        this.U = (int) (214.0f * this.u);
        this.v = (int) (440.0f * this.t);
        this.w = (int) (255.0f * this.u);
        this.V = new Rect(this.v, this.U, this.v + this.j.getWidth(), this.U + this.i.getHeight());
        this.x = (int) (239.0f * this.t);
        this.y = (int) (233.0f * this.u);
        this.W = new Rect(this.S, this.U, this.S + this.k.getWidth(), this.V.bottom);
        this.z = (int) (0.0f * this.t);
        this.A = (int) (0.0f * this.u);
        this.X = new Rect(0, 0, this.l.getWidth(), this.l.getHeight());
        this.B = (int) (620.0f * this.t);
        this.C = (int) (169.0f * this.u);
        this.Y = new Rect(this.B, this.C, this.B + this.n.getWidth(), this.C + this.n.getHeight());
        this.ad = new Rect((this.e - 10) - this.s.getWidth(), 10, this.e - 10, this.s.getHeight() + 10);
        this.ac = new Rect((this.ad.left - 10) - this.r.getWidth(), 10, this.ad.left - 10, this.r.getHeight() + 10);
        this.D = (int) (658.0f * this.t);
        this.E = (int) (236.0f * this.u);
        this.Z = new Rect(this.D, this.E, this.D + this.o.getWidth(), this.E + this.o.getHeight());
        this.F = (int) (25.0f * this.t);
        this.G = (int) (408.0f * this.u);
        this.aa = new Rect(this.F, this.G, this.F + this.p.getWidth(), this.G + this.p.getHeight());
        this.H = (int) (746.0f * this.t);
        this.I = (int) (408.0f * this.u);
        this.ab = new Rect(this.H, this.I, this.H + this.q.getWidth(), this.I + this.q.getHeight());
        com.feelingtouch.shooting.a.a.a(getResources(), i3, i4);
        if (!c.a()) {
            Bitmap decodeResource21 = BitmapFactory.decodeResource(getResources(), R.drawable.go);
            c.a(decodeResource21, (this.e - decodeResource21.getWidth()) / 2, (this.f - decodeResource21.getHeight()) / 2);
        }
        com.feelingtouch.shooting.e.b.a(getResources(), this.t, this.u);
        this.c.b = true;
        this.J = true;
        this.af = true;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.d == null || !this.d.isAlive()) {
            com.feelingtouch.c.b bVar = com.feelingtouch.c.c.a;
            getClass();
            new Object[1][0] = "Creating new thread for the runnable.";
            this.c.b = true;
            this.d = new Thread(this.c);
            this.d.start();
            return;
        }
        com.feelingtouch.c.b bVar2 = com.feelingtouch.c.c.a;
        getClass();
        new Object[1][0] = "This Thread is already running.";
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.c.b = false;
        boolean z2 = false;
        while (!z2) {
            try {
                this.d.join();
                z2 = true;
            } catch (InterruptedException e2) {
                com.feelingtouch.c.c.a.a(getClass(), "Interrupted while waiting for thread to finish.", e2);
                return;
            }
        }
    }

    public final void a() {
        this.ae = true;
    }

    public final void b() {
        this.ae = false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        Uri parse;
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                if (!this.J) {
                    if (!this.ar) {
                        if (this.V.contains(x2, y2)) {
                            this.L = true;
                        }
                        if (this.W.contains(x2, y2)) {
                            this.M = true;
                        }
                        if (!com.feelingtouch.e.b.a() && this.X.contains(x2, y2)) {
                            this.N = true;
                        }
                        if (this.Y.contains(x2, y2)) {
                            this.O = true;
                        }
                        if (this.Z.contains(x2, y2)) {
                            this.P = true;
                        }
                        if (this.aa.contains(x2, y2)) {
                            this.Q = true;
                        }
                        if (this.ab.contains(x2, y2)) {
                            this.R = true;
                            break;
                        }
                    } else {
                        if (this.aF.contains(x2, y2)) {
                            this.as = true;
                        }
                        if (this.aG.contains(x2, y2)) {
                            this.at = true;
                        }
                        if (this.aH.contains(x2, y2)) {
                            this.au = true;
                            break;
                        }
                    }
                }
                break;
            case 1:
                this.L = false;
                this.M = false;
                this.N = false;
                this.O = false;
                this.P = false;
                this.Q = false;
                this.R = false;
                this.as = false;
                this.at = false;
                this.au = false;
                if (!this.J) {
                    if (!this.ar) {
                        if (this.V.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            if (this.an) {
                                c();
                            } else {
                                this.b.startActivity(new Intent(this.b, MissionActivity.class));
                            }
                        }
                        if (this.W.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            c();
                        }
                        if (!com.feelingtouch.e.b.a() && this.X.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            if (com.feelingtouch.e.b.b) {
                                parse = Uri.parse("market://search?q=pub:\"Feelingtouch Inc.\"");
                            } else {
                                parse = Uri.parse("http://www.feelingtouch.com/product.html");
                            }
                            this.b.startActivity(new Intent("android.intent.action.VIEW", parse));
                        }
                        if (this.Y.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            this.b.startActivity(new Intent(this.b, SettingPreferenceActivity.class));
                        }
                        if (this.Z.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            Intent intent = new Intent(this.b, HighScore.class);
                            intent.putExtra("package_name", this.b.getPackageName());
                            this.b.startActivity(intent);
                        }
                        if (this.aa.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            MainMenuActivity mainMenuActivity = this.b;
                            com.feelingtouch.e.a.a(mainMenuActivity, new String[]{"feedback@feelingtouch.com"}, "[EAGLE NEST] Feedback", "\n\n\n\n\n\n\n*************************\n" + mainMenuActivity.getString(R.string.feedback_hint) + "\nOS version: " + Build.VERSION.RELEASE + "\nDevice model: " + Build.MODEL + "\nSW version: " + mainMenuActivity.getString(R.string.version) + "\n*************************\n\n");
                        }
                        if (this.ab.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            this.b.startActivity(new Intent(this.b, AboutActivity.class));
                        }
                        if (this.ac.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.facebook.com/pages/Feelingtouch-Inc/166596376692142")));
                        }
                        if (this.ad.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://twitter.com/feelingtouch")));
                            break;
                        }
                    } else {
                        if (this.aF.contains(x2, y2)) {
                            com.feelingtouch.shooting.c.b.a();
                            if (this.an) {
                                com.feelingtouch.e.a.a.a(this.a, "PROFILE_ID", this.aI);
                                this.an = false;
                            }
                            this.aq = true;
                        }
                        if (this.an && this.aG.contains(x2, y2) && this.aI - 1 > 0) {
                            this.aI--;
                        }
                        if (this.an && this.aH.contains(x2, y2) && this.aI + 1 <= 3) {
                            this.aI++;
                            break;
                        }
                    }
                }
                break;
        }
        return true;
    }

    static /* synthetic */ void a(MainMenuView mainMenuView, Canvas canvas) {
        if (mainMenuView.J) {
            long currentTimeMillis = System.currentTimeMillis();
            if (mainMenuView.K == 0) {
                mainMenuView.K = currentTimeMillis;
            }
            if (currentTimeMillis - mainMenuView.K > 20) {
                if (mainMenuView.T - 8 > mainMenuView.U) {
                    mainMenuView.T -= 8;
                } else if (mainMenuView.T - 1 > mainMenuView.U) {
                    mainMenuView.T--;
                } else {
                    mainMenuView.T = mainMenuView.U;
                    mainMenuView.J = false;
                }
            }
        }
        canvas.drawBitmap(mainMenuView.i, (float) mainMenuView.S, (float) mainMenuView.T, (Paint) null);
        if (mainMenuView.L) {
            canvas.drawBitmap(mainMenuView.j, (float) mainMenuView.v, (float) mainMenuView.w, (Paint) null);
        }
        if (mainMenuView.M) {
            canvas.drawBitmap(mainMenuView.k, (float) mainMenuView.x, (float) mainMenuView.y, (Paint) null);
        }
        if (mainMenuView.N) {
            canvas.drawBitmap(mainMenuView.l, (float) mainMenuView.z, (float) mainMenuView.A, (Paint) null);
        }
        if (mainMenuView.O) {
            canvas.drawBitmap(mainMenuView.n, (float) mainMenuView.B, (float) mainMenuView.C, (Paint) null);
        }
        if (mainMenuView.P) {
            canvas.drawBitmap(mainMenuView.o, (float) mainMenuView.D, (float) mainMenuView.E, (Paint) null);
        }
        if (mainMenuView.Q) {
            canvas.drawBitmap(mainMenuView.p, (float) mainMenuView.F, (float) mainMenuView.G, (Paint) null);
        }
        if (mainMenuView.R) {
            canvas.drawBitmap(mainMenuView.q, (float) mainMenuView.H, (float) mainMenuView.I, (Paint) null);
        }
    }

    static /* synthetic */ void b(MainMenuView mainMenuView, Canvas canvas) {
        if (!mainMenuView.ao) {
            if (mainMenuView.ap) {
                if (mainMenuView.aJ + 10 < mainMenuView.aM) {
                    mainMenuView.aJ += 10;
                } else if (mainMenuView.aJ + 1 < mainMenuView.aM) {
                    mainMenuView.aJ++;
                } else {
                    mainMenuView.ao = true;
                    mainMenuView.ap = false;
                    mainMenuView.ar = true;
                }
            }
        } else if (mainMenuView.aq) {
            if (mainMenuView.aJ - 10 > mainMenuView.aL) {
                mainMenuView.aJ -= 10;
            } else if (mainMenuView.aJ - 1 > mainMenuView.aL) {
                mainMenuView.aJ--;
            } else {
                mainMenuView.ao = false;
                mainMenuView.aq = false;
                if (mainMenuView.ar) {
                    mainMenuView.ar = false;
                }
            }
        }
        if (mainMenuView.ao || mainMenuView.ap) {
            mainMenuView.aF.left = mainMenuView.aJ + mainMenuView.aN;
            mainMenuView.aF.right = mainMenuView.aF.left + mainMenuView.az.getWidth();
            mainMenuView.aG.left = mainMenuView.aJ + mainMenuView.aP;
            mainMenuView.aG.right = mainMenuView.aG.left + mainMenuView.aB.getWidth();
            mainMenuView.aH.left = mainMenuView.aJ + mainMenuView.aR;
            mainMenuView.aH.right = mainMenuView.aH.left + mainMenuView.aD.getWidth();
            canvas.drawBitmap(mainMenuView.av, (float) mainMenuView.aJ, (float) mainMenuView.aK, (Paint) null);
            if (!mainMenuView.as) {
                canvas.drawBitmap(mainMenuView.az, (float) mainMenuView.aF.left, (float) mainMenuView.aF.top, (Paint) null);
            } else {
                canvas.drawBitmap(mainMenuView.aA, (float) mainMenuView.aF.left, (float) mainMenuView.aF.top, (Paint) null);
            }
            if (mainMenuView.an) {
                if (!mainMenuView.at) {
                    canvas.drawBitmap(mainMenuView.aB, (float) mainMenuView.aG.left, (float) mainMenuView.aG.top, (Paint) null);
                } else {
                    canvas.drawBitmap(mainMenuView.aC, (float) mainMenuView.aG.left, (float) mainMenuView.aG.top, (Paint) null);
                }
                if (!mainMenuView.au) {
                    canvas.drawBitmap(mainMenuView.aD, (float) mainMenuView.aH.left, (float) mainMenuView.aH.top, (Paint) null);
                } else {
                    canvas.drawBitmap(mainMenuView.aE, (float) mainMenuView.aH.left, (float) mainMenuView.aH.top, (Paint) null);
                }
            }
            switch (mainMenuView.aI) {
                case 1:
                    canvas.drawBitmap(mainMenuView.aw, (float) (mainMenuView.aJ + mainMenuView.aT), (float) mainMenuView.aU, (Paint) null);
                    break;
                case 2:
                    canvas.drawBitmap(mainMenuView.ax, (float) (mainMenuView.aJ + mainMenuView.aT), (float) mainMenuView.aU, (Paint) null);
                    break;
                case 3:
                    canvas.drawBitmap(mainMenuView.ay, (float) (mainMenuView.aJ + mainMenuView.aT), (float) mainMenuView.aU, (Paint) null);
                    break;
            }
            canvas.drawText(mainMenuView.aV, (float) (mainMenuView.aJ + mainMenuView.aZ), (float) mainMenuView.ba, com.feelingtouch.shooting.f.a.b);
            com.feelingtouch.shooting.e.b.a(canvas, mainMenuView.aJ + mainMenuView.bb, mainMenuView.bc, mainMenuView.aW);
            canvas.drawText(mainMenuView.aX, (float) (mainMenuView.aJ + mainMenuView.bd), (float) mainMenuView.be, com.feelingtouch.shooting.f.a.a);
            canvas.drawText(mainMenuView.aY, (float) (mainMenuView.aJ + mainMenuView.bf), (float) mainMenuView.bg, com.feelingtouch.shooting.f.a.a);
        }
    }

    private void c() {
        View inflate = ((LayoutInflater) this.a.getSystemService("layout_inflater")).inflate((int) R.layout.gamebox_input_text, (ViewGroup) null);
        EditText editText = (EditText) inflate.findViewById(R.id.input);
        String a2 = com.feelingtouch.e.a.a.a(getContext(), "profile_name", "");
        if (a2.equals("")) {
            new AlertDialog.Builder(this.a).setTitle((int) R.string.app_name).setView(inflate).setMessage((int) R.string.gamebox_input_name).setPositiveButton((int) R.string.gamebox_ok, new j(this, editText)).setNegativeButton((int) R.string.gamebox_cancel, (DialogInterface.OnClickListener) null).show();
            return;
        }
        this.aW = com.feelingtouch.e.a.a.b(this.a, "RANK", 0);
        this.aX = String.valueOf(com.feelingtouch.e.a.a.b(this.a, "GROWTHVALUE", 0));
        this.aY = String.valueOf(this.aW);
        this.aV = a2;
        this.ap = true;
    }
}
