package com.feelingtouch.shooting;

import android.app.Activity;
import android.os.Bundle;
import com.feelingtouch.e.b;

public class AboutActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.about);
        b.a(this);
    }
}
