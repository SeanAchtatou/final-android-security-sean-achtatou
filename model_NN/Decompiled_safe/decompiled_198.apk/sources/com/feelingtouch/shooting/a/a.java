package com.feelingtouch.shooting.a;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.feelingtouch.shooting.R;
import com.feelingtouch.shooting.f.b;

/* compiled from: MissionDoor */
public final class a {
    private static Bitmap A;
    private static int B;
    private static int C;
    private static boolean D;
    private static Bitmap[] E = new Bitmap[3];
    private static Bitmap F;
    private static Bitmap G;
    private static Bitmap H;
    private static Bitmap I;
    private static Bitmap J;
    private static Bitmap K;
    private static Bitmap L;
    private static Bitmap M;
    private static Bitmap N;
    private static Bitmap O;
    private static Bitmap P;
    private static int Q;
    private static int R;
    private static float S;
    private static float T;
    private static float U;
    private static float V;
    private static int W;
    private static int X;
    private static int Y;
    private static int Z;
    public static boolean a;
    private static int aa;
    private static int ab;
    private static Paint ac = new Paint(com.feelingtouch.shooting.f.a.a);
    public static int b = 1;
    public static Rect c = new Rect();
    public static Rect d = new Rect();
    public static boolean e;
    public static boolean f;
    public static boolean g;
    private static Bitmap h;
    private static Bitmap i;
    private static Bitmap j;
    private static Bitmap k;
    private static Bitmap l;
    private static Bitmap m;
    private static Bitmap n;
    private static int o;
    private static int p;
    private static int q;
    private static int r;
    private static int s;
    private static int t;
    private static boolean u;
    private static boolean v;
    private static boolean w;
    private static boolean x;
    private static Bitmap y;
    private static Bitmap z;

    public static void a(Resources resources, int i2, int i3) {
        if (h == null) {
            ac.setTextSize(30.0f);
            j = Bitmap.createBitmap(681, 480, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(j);
            Bitmap decodeResource = BitmapFactory.decodeResource(resources, R.drawable.dis_l2);
            canvas.drawBitmap(decodeResource, 0.0f, 0.0f, (Paint) null);
            b.a(decodeResource);
            Bitmap decodeResource2 = BitmapFactory.decodeResource(resources, R.drawable.dis_l);
            canvas.drawBitmap(decodeResource2, 634.0f, 0.0f, (Paint) null);
            b.a(decodeResource2);
            h = Bitmap.createScaledBitmap(j, (int) (0.817d * ((double) i2)), i3, true);
            b.a(j);
            k = Bitmap.createBitmap(227, 480, Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(k);
            Bitmap decodeResource3 = BitmapFactory.decodeResource(resources, R.drawable.dis_r2);
            canvas2.drawBitmap(decodeResource3, 0.0f, 0.0f, (Paint) null);
            b.a(decodeResource3);
            Bitmap decodeResource4 = BitmapFactory.decodeResource(resources, R.drawable.dis_r);
            canvas2.drawBitmap(decodeResource4, 40.0f, 0.0f, (Paint) null);
            b.a(decodeResource4);
            S = (0.248f * ((float) i2)) / ((float) k.getWidth());
            T = (((float) i3) * 1.0f) / ((float) k.getHeight());
            i = Bitmap.createScaledBitmap(k, (int) (S * ((float) k.getWidth())), (int) (T * ((float) k.getHeight())), true);
            b.a(k);
            int width = h.getWidth();
            int height = h.getHeight();
            q = -width;
            r = i2;
            s = 0;
            t = i2 - i.getWidth();
            o = s;
            p = t;
            a = false;
            u = false;
            v = false;
            w = false;
            x = false;
            int i4 = (int) (0.785d * ((double) width));
            int i5 = (int) (0.794d * ((double) height));
            Bitmap decodeResource5 = BitmapFactory.decodeResource(resources, R.drawable.dis_1);
            E[0] = Bitmap.createScaledBitmap(decodeResource5, i4, i5, true);
            U = ((float) i4) / ((float) decodeResource5.getWidth());
            V = ((float) i5) / ((float) decodeResource5.getHeight());
            b.a(decodeResource5);
            Bitmap decodeResource6 = BitmapFactory.decodeResource(resources, R.drawable.dis_2);
            E[1] = Bitmap.createScaledBitmap(decodeResource6, i4, i5, true);
            b.a(decodeResource6);
            Bitmap decodeResource7 = BitmapFactory.decodeResource(resources, R.drawable.dis_3);
            E[2] = Bitmap.createScaledBitmap(decodeResource7, i4, i5, true);
            b.a(decodeResource7);
            Bitmap decodeResource8 = BitmapFactory.decodeResource(resources, R.drawable.play);
            I = Bitmap.createScaledBitmap(decodeResource8, (int) (S * ((float) decodeResource8.getWidth())), (int) (T * ((float) decodeResource8.getHeight())), true);
            b.a(decodeResource8);
            Bitmap decodeResource9 = BitmapFactory.decodeResource(resources, R.drawable.play_2);
            J = Bitmap.createScaledBitmap(decodeResource9, (int) (S * ((float) decodeResource9.getWidth())), (int) (T * ((float) decodeResource9.getHeight())), true);
            b.a(decodeResource9);
            Bitmap decodeResource10 = BitmapFactory.decodeResource(resources, R.drawable.back);
            O = Bitmap.createScaledBitmap(decodeResource10, (int) (S * ((float) decodeResource10.getWidth())), (int) (T * ((float) decodeResource10.getHeight())), true);
            b.a(decodeResource10);
            Bitmap decodeResource11 = BitmapFactory.decodeResource(resources, R.drawable.back_1);
            P = Bitmap.createScaledBitmap(decodeResource11, (int) (S * ((float) decodeResource11.getWidth())), (int) (T * ((float) decodeResource11.getHeight())), true);
            b.a(decodeResource11);
            l = Bitmap.createBitmap(479, 361, Bitmap.Config.ARGB_8888);
            Canvas canvas3 = new Canvas(l);
            m = Bitmap.createBitmap(479, 361, Bitmap.Config.ARGB_8888);
            Canvas canvas4 = new Canvas(m);
            n = Bitmap.createBitmap(479, 361, Bitmap.Config.ARGB_8888);
            Canvas canvas5 = new Canvas(n);
            Bitmap decodeResource12 = BitmapFactory.decodeResource(resources, R.drawable.cpanel_win);
            canvas3.drawBitmap(decodeResource12, 0.0f, 0.0f, (Paint) null);
            canvas4.drawBitmap(decodeResource12, 0.0f, 0.0f, (Paint) null);
            canvas5.drawBitmap(decodeResource12, 0.0f, 0.0f, (Paint) null);
            b.a(decodeResource12);
            Bitmap decodeResource13 = BitmapFactory.decodeResource(resources, R.drawable.winword);
            canvas3.drawBitmap(decodeResource13, 105.0f, 64.0f, (Paint) null);
            b.a(decodeResource13);
            G = Bitmap.createScaledBitmap(l, i4, i5, true);
            b.a(decodeResource13);
            b.a(l);
            Bitmap decodeResource14 = BitmapFactory.decodeResource(resources, R.drawable.replay);
            M = Bitmap.createScaledBitmap(decodeResource14, (int) (S * ((float) decodeResource14.getWidth())), (int) (T * ((float) decodeResource14.getHeight())), true);
            b.a(decodeResource14);
            Bitmap decodeResource15 = BitmapFactory.decodeResource(resources, R.drawable.replay_l);
            N = Bitmap.createScaledBitmap(decodeResource15, (int) (S * ((float) decodeResource15.getWidth())), (int) (T * ((float) decodeResource15.getHeight())), true);
            b.a(decodeResource15);
            Bitmap decodeResource16 = BitmapFactory.decodeResource(resources, R.drawable.loseword);
            canvas4.drawBitmap(decodeResource16, 105.0f, 54.0f, (Paint) null);
            b.a(decodeResource16);
            H = Bitmap.createScaledBitmap(m, i4, i5, true);
            b.a(decodeResource16);
            b.a(m);
            Bitmap decodeResource17 = BitmapFactory.decodeResource(resources, R.drawable.pauseword);
            canvas5.drawBitmap(decodeResource17, 105.0f, 54.0f, (Paint) null);
            F = Bitmap.createScaledBitmap(n, i4, i5, true);
            b.a(decodeResource17);
            b.a(n);
            Bitmap decodeResource18 = BitmapFactory.decodeResource(resources, R.drawable.resume);
            K = Bitmap.createScaledBitmap(decodeResource18, (int) (S * ((float) decodeResource18.getWidth())), (int) (T * ((float) decodeResource18.getHeight())), true);
            b.a(decodeResource18);
            Bitmap decodeResource19 = BitmapFactory.decodeResource(resources, R.drawable.resume_1);
            L = Bitmap.createScaledBitmap(decodeResource19, (int) (S * ((float) decodeResource19.getWidth())), (int) (T * ((float) decodeResource19.getHeight())), true);
            b.a(decodeResource19);
            aa = (int) (325.0f * U);
            ab = (int) (276.0f * V);
            W = (int) (61.0f * S);
            X = (int) (241.0f * T);
            Y = (int) (75.0f * S);
            Z = (int) (76.0f * T);
            R = K.getWidth();
            Q = ((int) (0.093d * ((double) width))) + 4;
            C = ((int) (0.11d * ((double) height))) + 4;
            c.top = X;
            c.bottom = X + K.getHeight();
            d.top = Z;
            d.bottom = Z + K.getHeight();
            e = false;
            f = false;
            g = false;
        }
    }

    public static void a() {
        a = false;
        o = s;
        p = t;
        u = false;
        v = false;
    }

    public static void b() {
        w = true;
    }

    public static void c() {
        x = true;
    }

    public static void a(Canvas canvas, int i2) {
        if (!a) {
            if (w) {
                if (!u) {
                    if (o - 20 > q) {
                        o -= 20;
                    } else if (o - 5 > q) {
                        o -= 5;
                    } else if (o - 1 > q) {
                        o--;
                    } else {
                        u = true;
                    }
                }
                if (!v) {
                    if (p + 5 < r) {
                        p += 5;
                    } else if (p + 1 < r) {
                        p++;
                    } else {
                        v = true;
                    }
                }
                if (u && v) {
                    a = true;
                    w = false;
                }
            }
        } else if (x) {
            if (u) {
                if (o + 20 < s) {
                    o += 20;
                } else if (o + 5 < s) {
                    o += 5;
                } else if (o + 1 < s) {
                    o++;
                } else {
                    u = false;
                }
            }
            if (v) {
                if (p - 5 > t) {
                    p -= 5;
                } else if (p - 1 > t) {
                    p--;
                } else {
                    v = false;
                }
            }
            if (!u && !v) {
                a = false;
                x = false;
            }
        }
        if (!a || x) {
            canvas.drawBitmap(i, (float) p, 0.0f, (Paint) null);
            canvas.drawBitmap(h, (float) o, 0.0f, (Paint) null);
            switch (b) {
                case 1:
                    D = false;
                    y = E[i2 - 1];
                    if (!f) {
                        z = I;
                    } else {
                        z = J;
                    }
                    if (g) {
                        A = P;
                        break;
                    } else {
                        A = O;
                        break;
                    }
                case 16:
                    D = true;
                    y = F;
                    if (!f) {
                        z = K;
                    } else {
                        z = L;
                    }
                    if (g) {
                        A = P;
                        break;
                    } else {
                        A = O;
                        break;
                    }
                case 256:
                    D = true;
                    y = G;
                    if (!f) {
                        z = M;
                    } else {
                        z = N;
                    }
                    if (g) {
                        A = P;
                        break;
                    } else {
                        A = O;
                        break;
                    }
                case 4096:
                    D = true;
                    y = H;
                    if (!f) {
                        z = M;
                    } else {
                        z = N;
                    }
                    if (g) {
                        A = P;
                        break;
                    } else {
                        A = O;
                        break;
                    }
            }
            B = o + Q;
            canvas.drawBitmap(y, (float) B, (float) C, (Paint) null);
            if (D) {
                canvas.drawText(String.valueOf(com.feelingtouch.shooting.e.b.c), (float) (B + aa), (float) (C + ab), ac);
            }
            c.left = p + W;
            c.right = c.left + R;
            d.left = p + Y;
            d.right = d.left + R;
            canvas.drawBitmap(z, (float) c.left, (float) c.top, (Paint) null);
            canvas.drawBitmap(A, (float) d.left, (float) d.top, (Paint) null);
            if (a || w) {
                e = false;
            } else {
                e = true;
            }
        }
    }
}
