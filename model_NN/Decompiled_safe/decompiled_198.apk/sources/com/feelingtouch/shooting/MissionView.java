package com.feelingtouch.shooting;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.feelingtouch.c.c;
import com.feelingtouch.e.a.a;
import com.feelingtouch.shooting.f.b;

public class MissionView extends SurfaceView implements SurfaceHolder.Callback {
    private int A;
    private int B;
    private int C;
    private int D;
    private Rect E;
    private Rect F;
    private Rect G;
    private boolean H = false;
    private boolean I = false;
    private boolean J = false;
    /* access modifiers changed from: private */
    public Bitmap K = null;
    /* access modifiers changed from: private */
    public Bitmap L = null;
    /* access modifiers changed from: private */
    public int M;
    /* access modifiers changed from: private */
    public int N;
    private Rect O;
    /* access modifiers changed from: private */
    public boolean P = false;
    /* access modifiers changed from: private */
    public Bitmap Q = null;
    /* access modifiers changed from: private */
    public Bitmap R = null;
    /* access modifiers changed from: private */
    public int S;
    /* access modifiers changed from: private */
    public int T;
    private Rect U;
    /* access modifiers changed from: private */
    public boolean V = false;
    /* access modifiers changed from: private */
    public Bitmap[] W = new Bitmap[3];
    /* access modifiers changed from: private */
    public int X;
    /* access modifiers changed from: private */
    public int Y;
    /* access modifiers changed from: private */
    public int Z;
    public int a;
    private String aa;
    private String ab;
    private int ac;
    private int ad;
    private int ae;
    private int af;
    private float ag;
    private float ah;
    /* access modifiers changed from: private */
    public boolean ai = false;
    /* access modifiers changed from: private */
    public boolean aj = false;
    private int ak = 0;
    private String al;
    private String am;
    private int an;
    private int ao;
    private int ap;
    private int aq;
    private int ar;
    private int as;
    private int at;
    private int au;
    public String b;
    private Resources c;
    private Context d;
    private MissionActivity e;
    private m f;
    private Thread g = null;
    private int h;
    private int i;
    /* access modifiers changed from: private */
    public Bitmap j = null;
    private Bitmap k = null;
    private Bitmap l = null;
    private Bitmap m = null;
    private Bitmap n = null;
    private Bitmap o = null;
    private int p;
    private int q;
    private int r = 1;
    private Bitmap s = null;
    private Bitmap t = null;
    private Bitmap u = null;
    private Bitmap v = null;
    private Bitmap w = null;
    private Bitmap x = null;
    private int y;
    private int z;

    public MissionView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setKeepScreenOn(true);
        this.c = getResources();
        this.d = context;
        this.e = (MissionActivity) context;
        this.f = new m(this, holder);
        this.r = a.b(context, "page", 1);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.aj = false;
        this.h = i3;
        this.i = i4;
        Bitmap decodeResource = BitmapFactory.decodeResource(this.c, R.drawable.mission_bg);
        this.j = Bitmap.createScaledBitmap(decodeResource, this.h, this.i, true);
        this.ag = ((float) this.j.getWidth()) / ((float) decodeResource.getWidth());
        this.ah = ((float) this.j.getHeight()) / ((float) decodeResource.getHeight());
        b.a(decodeResource);
        Bitmap decodeResource2 = BitmapFactory.decodeResource(this.c, R.drawable.mission_update_btn_normal);
        this.Q = Bitmap.createScaledBitmap(decodeResource2, (int) (((float) decodeResource2.getWidth()) * this.ag), (int) (((float) decodeResource2.getHeight()) * this.ah), true);
        b.a(decodeResource2);
        Bitmap decodeResource3 = BitmapFactory.decodeResource(this.c, R.drawable.mission_update_btn_down);
        this.R = Bitmap.createScaledBitmap(decodeResource3, (int) (((float) decodeResource3.getWidth()) * this.ag), (int) (((float) decodeResource3.getHeight()) * this.ah), true);
        b.a(decodeResource3);
        this.S = (int) (this.ag * 64.0f);
        this.T = (int) (this.ah * 405.0f);
        this.U = new Rect(this.S, this.T, this.S + this.Q.getWidth(), this.T + this.Q.getHeight());
        Bitmap decodeResource4 = BitmapFactory.decodeResource(this.c, R.drawable.prev_normal_btn);
        this.s = Bitmap.createScaledBitmap(decodeResource4, (int) (((float) decodeResource4.getWidth()) * this.ag), (int) (((float) decodeResource4.getHeight()) * this.ah), true);
        b.a(decodeResource4);
        Bitmap decodeResource5 = BitmapFactory.decodeResource(this.c, R.drawable.prev_down_btn);
        this.t = Bitmap.createScaledBitmap(decodeResource5, (int) (((float) decodeResource5.getWidth()) * this.ag), (int) (((float) decodeResource5.getHeight()) * this.ah), true);
        b.a(decodeResource5);
        this.y = (int) (270.0f * this.ag);
        this.z = (int) (173.0f * this.ah);
        Bitmap decodeResource6 = BitmapFactory.decodeResource(this.c, R.drawable.next_normal_btn);
        this.u = Bitmap.createScaledBitmap(decodeResource6, (int) (((float) decodeResource6.getWidth()) * this.ag), (int) (((float) decodeResource6.getHeight()) * this.ah), true);
        b.a(decodeResource6);
        Bitmap decodeResource7 = BitmapFactory.decodeResource(this.c, R.drawable.next_down_btn);
        this.v = Bitmap.createScaledBitmap(decodeResource7, (int) (((float) decodeResource7.getWidth()) * this.ag), (int) (((float) decodeResource7.getHeight()) * this.ah), true);
        b.a(decodeResource7);
        this.A = (int) (731.0f * this.ag);
        this.B = (int) (173.0f * this.ah);
        Bitmap decodeResource8 = BitmapFactory.decodeResource(this.c, R.drawable.play_now_normal_btn);
        this.w = Bitmap.createScaledBitmap(decodeResource8, (int) (((float) decodeResource8.getWidth()) * this.ag), (int) (((float) decodeResource8.getHeight()) * this.ah), true);
        b.a(decodeResource8);
        Bitmap decodeResource9 = BitmapFactory.decodeResource(this.c, R.drawable.play_now_down_btn);
        this.x = Bitmap.createScaledBitmap(decodeResource9, (int) (((float) decodeResource9.getWidth()) * this.ag), (int) (((float) decodeResource9.getHeight()) * this.ah), true);
        b.a(decodeResource9);
        this.C = (int) (443.0f * this.ag);
        this.D = (int) (338.0f * this.ah);
        this.G = new Rect(this.C, this.D, this.C + this.w.getWidth(), this.D + this.w.getHeight());
        this.E = new Rect(this.y - 10, this.z - 10, this.y + this.s.getWidth() + 10, this.z + this.s.getHeight() + 10);
        this.F = new Rect(this.A - 10, this.B - 10, this.A + this.u.getWidth() + 10, this.B + this.u.getHeight() + 10);
        Bitmap decodeResource10 = BitmapFactory.decodeResource(this.c, com.feelingtouch.shooting.b.a.a[0]);
        this.l = Bitmap.createScaledBitmap(decodeResource10, (int) (((float) decodeResource10.getWidth()) * this.ag), (int) (((float) decodeResource10.getHeight()) * this.ah), true);
        b.a(decodeResource10);
        Bitmap decodeResource11 = BitmapFactory.decodeResource(this.c, com.feelingtouch.shooting.b.a.a[1]);
        this.m = Bitmap.createScaledBitmap(decodeResource11, (int) (((float) decodeResource11.getWidth()) * this.ag), (int) (((float) decodeResource11.getHeight()) * this.ah), true);
        b.a(decodeResource11);
        Bitmap decodeResource12 = BitmapFactory.decodeResource(this.c, com.feelingtouch.shooting.b.a.a[2]);
        this.n = Bitmap.createScaledBitmap(decodeResource12, (int) (((float) decodeResource12.getWidth()) * this.ag), (int) (((float) decodeResource12.getHeight()) * this.ah), true);
        b.a(decodeResource12);
        Bitmap decodeResource13 = BitmapFactory.decodeResource(this.c, com.feelingtouch.shooting.b.a.a[3]);
        this.o = Bitmap.createScaledBitmap(decodeResource13, (int) (((float) decodeResource13.getWidth()) * this.ag), (int) (((float) decodeResource13.getHeight()) * this.ah), true);
        b.a(decodeResource13);
        this.p = (int) (374.0f * this.ag);
        this.q = (int) (89.0f * this.ah);
        Bitmap decodeResource14 = BitmapFactory.decodeResource(this.c, R.drawable.mission_back_normal);
        this.K = Bitmap.createScaledBitmap(decodeResource14, (int) (((float) decodeResource14.getWidth()) * this.ag), (int) (((float) decodeResource14.getHeight()) * this.ah), true);
        b.a(decodeResource14);
        this.M = (int) (750.0f * this.ag);
        this.N = (int) (391.0f * this.ah);
        this.O = new Rect(this.M, this.N, this.M + this.K.getWidth(), this.N + this.K.getHeight());
        Bitmap decodeResource15 = BitmapFactory.decodeResource(this.c, R.drawable.mission_back_pressed);
        this.L = Bitmap.createScaledBitmap(decodeResource15, (int) (((float) decodeResource15.getWidth()) * this.ag), (int) (((float) decodeResource15.getHeight()) * this.ah), true);
        b.a(decodeResource15);
        Bitmap decodeResource16 = BitmapFactory.decodeResource(this.c, R.drawable.profile_avator);
        this.W[0] = Bitmap.createScaledBitmap(Bitmap.createBitmap(decodeResource16, 0, 0, 115, decodeResource16.getHeight()), (int) (this.ag * 115.0f), (int) (((float) decodeResource16.getHeight()) * this.ah), true);
        this.W[1] = Bitmap.createScaledBitmap(Bitmap.createBitmap(decodeResource16, 115, 0, 115, decodeResource16.getHeight()), (int) (this.ag * 115.0f), (int) (((float) decodeResource16.getHeight()) * this.ah), true);
        this.W[2] = Bitmap.createScaledBitmap(Bitmap.createBitmap(decodeResource16, 230, 0, 115, decodeResource16.getHeight()), (int) (this.ag * 115.0f), (int) (((float) decodeResource16.getHeight()) * this.ah), true);
        this.X = a.b(this.d, "PROFILE_ID", 1);
        b.a(decodeResource16);
        this.Y = (int) (this.ag * 62.0f);
        this.Z = (int) (this.ah * 32.0f);
        this.al = a.a(this.d, "profile_name", "");
        this.a = a.b(this.d, "RANK", 0);
        com.feelingtouch.shooting.e.b.a(this.d);
        com.feelingtouch.shooting.e.b.c(getContext());
        this.am = String.valueOf(a.b(this.d, "GROWTHVALUE", 0));
        this.b = String.valueOf(this.a);
        com.feelingtouch.shooting.e.b.a(getResources(), this.ag, this.ah);
        this.an = (int) (114.0f * this.ag);
        this.ao = (int) (285.0f * this.ah);
        this.ap = (int) (131.0f * this.ag);
        this.aq = (int) (300.0f * this.ah);
        this.ar = (int) (131.0f * this.ag);
        this.as = (int) (359.0f * this.ah);
        this.at = (int) (131.0f * this.ag);
        this.au = (int) (397.0f * this.ah);
        this.aa = String.valueOf(com.feelingtouch.shooting.b.a.f[this.r - 1]);
        this.ab = String.valueOf(a.b(this.d, "LEVEL" + this.r + "_SCORE", 0));
        this.ac = (int) (475.0f * this.ag);
        this.ad = (int) (335.0f * this.ah);
        this.ae = (int) (675.0f * this.ag);
        this.af = (int) (335.0f * this.ah);
        this.f.b = true;
        this.aj = true;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.g == null || !this.g.isAlive()) {
            com.feelingtouch.c.b bVar = c.a;
            getClass();
            new Object[1][0] = "Creating new thread for the runnable.";
            this.f.b = true;
            this.g = new Thread(this.f);
            this.g.start();
            return;
        }
        com.feelingtouch.c.b bVar2 = c.a;
        getClass();
        new Object[1][0] = "This Thread is already running.";
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.f.b = false;
        boolean z2 = false;
        while (!z2) {
            try {
                this.g.join();
                z2 = true;
            } catch (InterruptedException e2) {
                c.a.a(getClass(), "Interrupted while waiting for thread to finish.", e2);
            }
        }
        a.a(this.d, "PAGE", this.r);
    }

    public final void a() {
        this.ai = true;
    }

    public final void b() {
        this.ai = false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 0:
                if (this.O.contains(x2, y2)) {
                    this.P = true;
                }
                if (this.E.contains(x2, y2)) {
                    this.I = true;
                }
                if (this.F.contains(x2, y2)) {
                    this.J = true;
                }
                if (this.G.contains(x2, y2)) {
                    this.H = true;
                }
                if (this.U.contains(x2, y2)) {
                    this.V = true;
                    break;
                }
                break;
            case 1:
                this.P = false;
                this.I = false;
                this.J = false;
                this.H = false;
                this.V = false;
                if (this.O.contains(x2, y2)) {
                    com.feelingtouch.shooting.c.b.a();
                    this.e.startActivity(new Intent(this.e, MainMenuActivity.class));
                }
                if (this.E.contains(x2, y2)) {
                    com.feelingtouch.shooting.c.b.a();
                    if (this.r > 1) {
                        this.r--;
                        this.aa = String.valueOf(com.feelingtouch.shooting.b.a.f[this.r - 1]);
                        this.ab = String.valueOf(a.b(this.d, "LEVEL" + this.r + "_SCORE", 0));
                    }
                }
                if (this.F.contains(x2, y2)) {
                    com.feelingtouch.shooting.c.b.a();
                    if (this.r < 4) {
                        this.r++;
                        this.aa = String.valueOf(com.feelingtouch.shooting.b.a.f[this.r - 1]);
                        this.ab = String.valueOf(a.b(this.d, "LEVEL" + this.r + "_SCORE", 0));
                    }
                }
                if (this.G.contains(x2, y2)) {
                    com.feelingtouch.shooting.c.b.a();
                    switch (this.r) {
                        case 1:
                            a.a(this.d, "page", this.r);
                            this.e.startActivity(new Intent(this.e, LoadingActivity.class));
                            break;
                        case 2:
                            a.a(this.d, "page", this.r);
                            this.e.startActivity(new Intent(this.e, LoadingActivity.class));
                            break;
                        case 3:
                            a.a(this.d, "page", this.r);
                            this.e.startActivity(new Intent(this.e, LoadingActivity.class));
                            break;
                    }
                }
                if (this.U.contains(x2, y2)) {
                    com.feelingtouch.shooting.c.b.a();
                    com.feelingtouch.shooting.e.b.c(getContext());
                    this.am = String.valueOf(com.feelingtouch.shooting.e.b.a);
                    new a(this.e).a(this.e, a.b(this.d, "GROWTHVALUE", 0));
                    break;
                }
                break;
        }
        return true;
    }

    static /* synthetic */ void a(MissionView missionView, Canvas canvas) {
        if (!missionView.I) {
            canvas.drawBitmap(missionView.s, (float) missionView.y, (float) missionView.z, (Paint) null);
        } else {
            canvas.drawBitmap(missionView.t, (float) missionView.y, (float) missionView.z, (Paint) null);
        }
        if (!missionView.J) {
            canvas.drawBitmap(missionView.u, (float) missionView.A, (float) missionView.B, (Paint) null);
        } else {
            canvas.drawBitmap(missionView.v, (float) missionView.A, (float) missionView.B, (Paint) null);
        }
        if (!missionView.H) {
            canvas.drawBitmap(missionView.w, (float) missionView.C, (float) missionView.D, (Paint) null);
        } else {
            canvas.drawBitmap(missionView.x, (float) missionView.C, (float) missionView.D, (Paint) null);
        }
        if (missionView.ak != missionView.r) {
            missionView.ak = missionView.r;
            switch (missionView.r) {
                case 1:
                    missionView.k = missionView.l;
                    break;
                case 2:
                    missionView.k = missionView.m;
                    break;
                case 3:
                    missionView.k = missionView.n;
                    break;
                case 4:
                    missionView.k = missionView.o;
                    break;
            }
        }
        canvas.drawBitmap(missionView.k, (float) missionView.p, (float) missionView.q, (Paint) null);
        canvas.drawText(missionView.aa, (float) missionView.ac, (float) missionView.ad, com.feelingtouch.shooting.f.a.a);
        canvas.drawText(missionView.ab, (float) missionView.ae, (float) missionView.af, com.feelingtouch.shooting.f.a.a);
    }

    static /* synthetic */ void b(MissionView missionView, Canvas canvas) {
        canvas.drawText(missionView.al, (float) missionView.an, (float) missionView.ao, com.feelingtouch.shooting.f.a.b);
        com.feelingtouch.shooting.e.b.a(canvas, missionView.ap, missionView.aq, missionView.a);
        canvas.drawText(missionView.am, (float) missionView.ar, (float) missionView.as, com.feelingtouch.shooting.f.a.a);
        canvas.drawText(missionView.b, (float) missionView.at, (float) missionView.au, com.feelingtouch.shooting.f.a.a);
    }
}
