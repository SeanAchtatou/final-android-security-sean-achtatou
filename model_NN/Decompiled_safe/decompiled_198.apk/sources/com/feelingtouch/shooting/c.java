package com.feelingtouch.shooting;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.feelingtouch.e.a.a;
import com.feelingtouch.shooting.f.b;

/* compiled from: LoadingView */
public final class c extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public static Bitmap[] f = null;
    /* access modifiers changed from: private */
    public int a;
    private Resources b;
    /* access modifiers changed from: private */
    public LoadingActivity c;
    private d d;
    private Thread e = null;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = true;
    /* access modifiers changed from: private */
    public int k = 0;

    public c(Context context) {
        super(context);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setKeepScreenOn(true);
        this.b = getResources();
        this.c = (LoadingActivity) context;
        this.d = new d(this, holder);
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.i = false;
        if (f == null) {
            Bitmap decodeResource = BitmapFactory.decodeResource(this.b, R.drawable.loading);
            f = new Bitmap[12];
            for (int i5 = 0; i5 < 12; i5++) {
                f[i5] = Bitmap.createBitmap(decodeResource, i5 * 120, 0, 120, 37);
            }
            b.a(decodeResource);
        }
        this.g = (i3 - f[0].getWidth()) / 2;
        this.h = (i4 - f[0].getHeight()) / 2;
        this.a = a.b(getContext(), "page", 1);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e2) {
            com.feelingtouch.c.c.a.a(getClass(), "Interrupted while doing initial sleeping.");
            e2.printStackTrace();
        }
        this.i = true;
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.e == null || !this.e.isAlive()) {
            this.d.b = true;
            this.e = new Thread(this.d);
            this.e.start();
            return;
        }
        com.feelingtouch.c.b bVar = com.feelingtouch.c.c.a;
        getClass();
        new Object[1][0] = "This Thread is already running.";
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.d.b = false;
        boolean z = false;
        while (!z) {
            try {
                this.e.join();
                z = true;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
                com.feelingtouch.c.c.a.a(getClass(), e2);
                return;
            }
        }
    }
}
