package com.feelingtouch.shooting.b;

import com.feelingtouch.shooting.R;

/* compiled from: Constant */
public final class a {
    public static final int[] a = {R.drawable.mission_level01, R.drawable.mission_level02, R.drawable.mission_level03, R.drawable.mission_level04};
    public static final int[] b = {R.drawable.level01_bg, R.drawable.level02_bg, R.drawable.level03_bg};
    public static final int[] c = {R.drawable.level_01_scorebar, R.drawable.level_02_scorebar, R.drawable.level_03_scorebar};
    public static final int[][] d = {new int[]{R.drawable.pause, R.drawable.pausepress}, new int[]{R.drawable.pause, R.drawable.pausepress}, new int[]{R.drawable.pause, R.drawable.pausepress}};
    public static final long[] e = {90000, 90000, 72000, 72000};
    public static final int[] f = {110, 65, 700, 30};
    public static int g = 0;
}
