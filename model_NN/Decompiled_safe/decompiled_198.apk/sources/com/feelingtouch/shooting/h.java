package com.feelingtouch.shooting;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* compiled from: MainMenuActivity */
final class h implements View.OnClickListener {
    final /* synthetic */ MainMenuActivity a;

    h(MainMenuActivity mainMenuActivity) {
        this.a = mainMenuActivity;
    }

    public final void onClick(View view) {
        this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://twitter.com/feelingtouch")));
    }
}
