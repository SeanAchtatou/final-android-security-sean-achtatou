package com.feelingtouch.shooting;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.feelingtouch.e.a.a;
import com.feelingtouch.gamebox.j;

/* compiled from: MissionActivity */
final class l extends Handler {
    final /* synthetic */ MissionActivity a;

    l(MissionActivity missionActivity) {
        this.a = missionActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (!j.a) {
            if (message.what == 999) {
                int intValue = ((Integer) message.obj).intValue();
                this.a.a.a = intValue;
                this.a.a.b = String.valueOf(intValue);
                a.a(this.a, "RANK", intValue);
                Toast.makeText(this.a, String.valueOf(this.a.getString(R.string.submit_successful)) + "\nYour rank in global: " + intValue, 0).show();
            } else if (message.what == 998) {
                com.feelingtouch.e.l.a(this.a, R.string.submit_failed);
            }
        }
    }
}
