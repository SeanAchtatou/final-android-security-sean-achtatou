package com.feelingtouch.shooting;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.feelingtouch.a.c;
import com.feelingtouch.b.a;
import com.feelingtouch.e.b;
import com.feelingtouch.shooting.c.d;

public class MainMenuActivity extends Activity {
    /* access modifiers changed from: private */
    public MainMenuView a;
    /* access modifiers changed from: private */
    public RelativeLayout b;
    /* access modifiers changed from: private */
    public ImageView c;
    private Button d;
    /* access modifiers changed from: private */
    public AlphaAnimation e;
    /* access modifiers changed from: private */
    public c f;
    private Handler g = new Handler();

    private native void jniPassHander(Activity activity);

    static {
        System.loadLibrary("feelingtouchshooting");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.mainmenu_view);
        a.a((Context) this);
        a.a((Activity) this);
        this.f = new c(this);
        this.e = new AlphaAnimation(1.0f, 0.0f);
        this.e.setRepeatCount(0);
        this.e.setDuration(4000);
        this.d = (Button) findViewById(R.id.game_download);
        this.b = (RelativeLayout) findViewById(R.id.first_page);
        this.c = (ImageView) findViewById(R.id.first_page_image);
        if (!b.a() && this.f.a() != null) {
            this.d.setVisibility(0);
            this.c.setBackgroundDrawable(this.f.a());
        }
        this.g.postDelayed(new e(this), 2000);
        this.e.setAnimationListener(new f(this));
        ((Button) findViewById(R.id.facebook)).setOnClickListener(new g(this));
        ((Button) findViewById(R.id.twitter)).setOnClickListener(new h(this));
        this.d.setOnClickListener(new i(this));
        this.a = new MainMenuView(this);
        com.feelingtouch.shooting.f.a.a(this);
        jniPassHander(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        com.feelingtouch.shooting.c.a.a(this);
        com.feelingtouch.shooting.c.b.a(this);
        d.a(this);
        com.feelingtouch.shooting.c.a.b();
        this.a.b();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        com.feelingtouch.shooting.c.a.a();
        this.a.a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.feelingtouch.shooting.c.a.c();
        System.gc();
        System.gc();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        if (i == 25 || i == 24) {
            com.feelingtouch.shooting.c.b.b();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
