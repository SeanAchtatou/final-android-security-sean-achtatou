package com.feelingtouch.b;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.feelingtouch.d.a.a.c;
import com.feelingtouch.d.a.a.d;
import com.feelingtouch.e.k;
import com.feelingtouch.shooting.R;

/* compiled from: MsgDialog */
public final class m extends Dialog {
    /* access modifiers changed from: private */
    public Activity a;
    /* access modifiers changed from: private */
    public int b = 0;
    /* access modifiers changed from: private */
    public c c;
    /* access modifiers changed from: private */
    public d d;
    private TextView e;
    private TextView f;
    private TextView g;
    private RelativeLayout h;
    private Button i;
    private Button j;

    public m(Context context, Object obj) {
        super(context, R.style.customized_dialog);
        this.a = (Activity) context;
        setContentView((int) R.layout.msg_dialog);
        if (obj instanceof d) {
            this.b = 2;
            this.d = (d) obj;
        } else if (obj instanceof c) {
            this.b = 1;
            this.c = (c) obj;
        }
        this.h = (RelativeLayout) findViewById(R.id.layout_msg_root);
        if (l.e != null) {
            this.h.setBackgroundDrawable(l.e);
        }
        this.e = (TextView) findViewById(R.id.msg_notice_title);
        this.f = (TextView) findViewById(R.id.msg_title);
        this.g = (TextView) findViewById(R.id.msg_text);
        this.j = (Button) findViewById(R.id.msg_action);
        if (l.c != null) {
            this.j.setBackgroundDrawable(l.c);
        }
        if (this.b == 2) {
            this.e.setText((int) R.string.upgrade);
            if (this.d != null) {
                if (k.b(this.d.a)) {
                    this.f.setText(this.d.a);
                }
                if (k.b(this.d.b)) {
                    this.g.setText(this.d.b);
                }
                this.j.setText((int) R.string.upgrade);
            } else {
                this.j.setVisibility(8);
            }
        } else {
            this.e.setText((int) R.string.message);
            if (this.c != null) {
                if (k.b(this.c.a)) {
                    this.f.setText(this.c.a);
                }
                if (k.b(this.c.b)) {
                    this.g.setText(this.c.b);
                }
                this.j.setText((int) R.string.msg_ok);
            }
        }
        this.j.setOnClickListener(new n(this));
        this.i = (Button) findViewById(R.id.ok);
        this.i.setOnClickListener(new o(this));
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (this.b == 2 && this.d.f) {
            return true;
        }
        dismiss();
        l.a();
        this.d = null;
        this.c = null;
        return true;
    }
}
