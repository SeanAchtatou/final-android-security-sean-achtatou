package com.feelingtouch.b;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.feelingtouch.shooting.R;
import java.util.List;

/* compiled from: BannerAdapter */
public final class i extends BaseAdapter {
    /* access modifiers changed from: private */
    public List a;
    private LayoutInflater b;
    /* access modifiers changed from: private */
    public Context c;

    public i(Context context, List list) {
        this.a = list;
        this.b = LayoutInflater.from(context);
        this.c = context;
    }

    public final int getCount() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        if (this.a == null) {
            return null;
        }
        if (view != null) {
            return view;
        }
        try {
            k kVar = (k) this.a.get(i);
            View inflate = this.b.inflate((int) R.layout.banner_list_item, viewGroup, false);
            try {
                ((RelativeLayout) inflate.findViewById(R.id.layout)).setBackgroundDrawable(l.d);
                ImageView imageView = (ImageView) inflate.findViewById(R.id.game_icon);
                if (kVar.a != null) {
                    imageView.setImageBitmap(kVar.a);
                }
                ((TextView) inflate.findViewById(R.id.game_desc)).setText(kVar.b.b);
                ((TextView) inflate.findViewById(R.id.game_name)).setText(kVar.b.e);
                Button button = (Button) inflate.findViewById(R.id.download);
                button.setBackgroundDrawable(l.b);
                button.setOnClickListener(new j(this, i));
                return inflate;
            } catch (Exception e) {
                e = e;
                view2 = inflate;
                e.printStackTrace();
                return view2;
            }
        } catch (Exception e2) {
            e = e2;
            view2 = null;
            e.printStackTrace();
            return view2;
        }
    }
}
