package com.feelingtouch.b;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.feelingtouch.d.a.a.b;
import com.feelingtouch.shooting.R;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/* compiled from: BannerAdDialog */
public final class d extends Dialog {
    /* access modifiers changed from: private */
    public List a = null;
    /* access modifiers changed from: private */
    public List b = new LinkedList();
    private Handler c = new Handler();
    private ListView d;
    private Button e;
    /* access modifiers changed from: private */
    public i f = null;
    /* access modifiers changed from: private */
    public Context g;
    private RelativeLayout h;

    public d(Context context) {
        super(context, R.style.customized_dialog);
        this.g = context;
        setContentView((int) R.layout.banner_dialog);
        a();
    }

    public final void a(List list) {
        this.a = new LinkedList();
        if (list.size() < 2) {
            this.a.add((b) list.get(0));
        } else {
            this.a.add((b) list.get(0));
            this.a.add((b) list.get(1));
        }
        if (this.a != null && this.a.size() > 0) {
            for (b bVar : this.a) {
                k kVar = new k();
                kVar.b = bVar;
                this.b.add(kVar);
            }
            this.c.post(new e(this));
        }
        a();
    }

    private void a() {
        this.h = (RelativeLayout) findViewById(R.id.layout_root);
        this.h.setBackgroundDrawable(l.a);
        this.d = (ListView) findViewById(R.id.game_list);
        if (this.f != null) {
            this.d.setAdapter((ListAdapter) this.f);
        }
        this.d.setOnItemClickListener(new f(this));
        this.e = (Button) findViewById(R.id.ok);
        this.e.setOnClickListener(new g(this));
    }

    public final void show() {
        if (this.f != null) {
            this.d.setAdapter((ListAdapter) this.f);
        }
        super.show();
    }

    public static Bitmap a(String str) {
        URL url;
        Bitmap bitmap = null;
        try {
            url = new URL(str);
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            url = null;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            return bitmap;
        } catch (IOException e3) {
            e3.printStackTrace();
            return bitmap;
        }
    }
}
