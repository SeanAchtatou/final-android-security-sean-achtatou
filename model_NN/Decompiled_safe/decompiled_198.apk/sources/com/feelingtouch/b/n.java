package com.feelingtouch.b;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.feelingtouch.e.k;

/* compiled from: MsgDialog */
final class n implements View.OnClickListener {
    final /* synthetic */ m a;

    n(m mVar) {
        this.a = mVar;
    }

    public final void onClick(View view) {
        this.a.dismiss();
        if (this.a.b == 2) {
            h.a(this.a.a, this.a.d.c, this.a.d.d);
            System.exit(0);
        } else if (this.a.b == 1 && k.b(this.a.c.c)) {
            this.a.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.a.c.c)));
            this.a.a.finish();
        }
    }
}
