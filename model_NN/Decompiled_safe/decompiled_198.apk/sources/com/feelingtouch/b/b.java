package com.feelingtouch.b;

import android.content.Context;
import android.os.Build;
import com.feelingtouch.d.a.a.a;
import com.feelingtouch.d.a.a.c;
import com.feelingtouch.d.a.a.d;
import java.util.Locale;

/* compiled from: BannerAd */
final class b extends Thread {
    private final /* synthetic */ Context a;

    b(Context context) {
        this.a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.feelingtouch.e.a.a.a(android.content.Context, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.feelingtouch.e.a.a.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.feelingtouch.e.a.a.a(android.content.Context, java.lang.String, int):void
      com.feelingtouch.e.a.a.a(android.content.Context, java.lang.String, boolean):void */
    public final void run() {
        try {
            a a2 = com.feelingtouch.d.a.a.c.a(com.feelingtouch.e.a.a(this.a), Locale.getDefault().getDisplayLanguage(), Locale.getDefault().getCountry(), Build.VERSION.SDK_INT, this.a.getPackageName());
            a.d = a2.a;
            a.g = a2.b;
            a.f = a2.d;
            a.e = a2.c;
            if (a.d != null) {
                a.d(this.a);
            }
        } catch (com.feelingtouch.d.c.a e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        a.i = a.f(this.a);
        a.h = com.feelingtouch.e.a.a.b(this.a, "banner_ad_msg_version", 0);
        if (com.feelingtouch.e.a.a.a(this.a, "banner_ad_first_run").booleanValue()) {
            try {
                if (a.f != null) {
                    a.i = a.f.g;
                    a.f = (d) null;
                }
                com.feelingtouch.d.a.a.c.a(this.a.getPackageName(), com.feelingtouch.e.a.a(this.a));
                com.feelingtouch.e.a.a.a(this.a, "banner_ad_first_run", false);
            } catch (com.feelingtouch.d.c.a e3) {
                e3.printStackTrace();
            }
        }
        if (a.f != null) {
            if (a.f.g > a.i) {
                l.a(this.a);
                a.e = (c) null;
            } else {
                a.f = (d) null;
            }
        }
        if (a.e == null) {
            return;
        }
        if (a.e.d > a.h) {
            l.a(this.a);
        } else {
            a.e = (c) null;
        }
    }
}
