package com.feelingtouch.b;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.feelingtouch.d.a.a.c;
import com.feelingtouch.d.a.a.d;
import com.feelingtouch.e.b;
import java.util.List;
import java.util.Random;

/* compiled from: BannerAd */
public final class a {
    private static boolean a = false;
    private static d b;
    private static m c;
    /* access modifiers changed from: private */
    public static List d;
    /* access modifiers changed from: private */
    public static c e;
    /* access modifiers changed from: private */
    public static d f;
    /* access modifiers changed from: private */
    public static int g = 0;
    /* access modifiers changed from: private */
    public static int h = 0;
    /* access modifiers changed from: private */
    public static int i = 0;

    public static void a(Context context) {
        if (!b.a()) {
            new b(context).start();
        }
    }

    /* access modifiers changed from: private */
    public static int f(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            return 1;
        }
    }

    static /* synthetic */ void d(Context context) {
        for (int size = d.size() - 1; size >= 0; size--) {
            try {
                if (context.getPackageManager().getPackageInfo(((com.feelingtouch.d.a.a.b) d.get(size)).f, 0) != null) {
                    d.remove(size);
                }
            } catch (PackageManager.NameNotFoundException | Exception e2) {
            }
        }
    }

    public static void b(Context context) {
        try {
            l.b(context);
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            l.b();
            g = 0;
            a = false;
        } catch (Exception e3) {
            l.b();
            g = 0;
            a = false;
        }
    }

    public static void c(Context context) {
        if (!b.a() && d != null && d.size() > 0) {
            try {
                d dVar = new d(context);
                b = dVar;
                dVar.a(d);
                a = true;
            } catch (Exception e2) {
                e2.printStackTrace();
                l.b();
                a = false;
                g = 0;
            }
            Log.e("", "========" + a);
        }
    }

    public static void a(Activity activity) {
        if (!b.a()) {
            try {
                if (l.e == null) {
                    return;
                }
                if (f != null) {
                    if (f.g > i) {
                        i = f.g;
                        m mVar = new m(activity, f);
                        c = mVar;
                        mVar.show();
                    }
                } else if (e != null && e.d > h) {
                    com.feelingtouch.e.a.a.a(activity.getApplicationContext(), "banner_ad_msg_version", e.d);
                    h = e.d;
                    m mVar2 = new m(activity, e);
                    c = mVar2;
                    mVar2.show();
                }
            } catch (Exception e2) {
            }
        }
    }

    public static void a() {
        if (!b.a() && d != null && d.size() > 0 && a) {
            if (g - new Random().nextInt(100) > 0) {
                b.show();
            }
        }
    }

    public static void a(Context context, String str) {
        if (!b.a()) {
            new Thread(new c(context, str)).start();
        }
    }
}
