package com.feelingtouch.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import com.feelingtouch.shooting.R;

/* compiled from: ImageManager */
public final class l {
    public static BitmapDrawable a;
    public static BitmapDrawable b;
    public static BitmapDrawable c;
    public static BitmapDrawable d;
    public static BitmapDrawable e;
    private static Bitmap f;
    private static Bitmap g;
    private static Bitmap h;
    private static Bitmap i;
    private static Bitmap j;

    public static void a(Context context) {
        try {
            j = BitmapFactory.decodeResource(context.getResources(), R.drawable.msg_dialog);
            h = BitmapFactory.decodeResource(context.getResources(), R.drawable.button_bg);
            e = new BitmapDrawable(j);
            c = new BitmapDrawable(h);
        } catch (OutOfMemoryError e2) {
            e = null;
            c = null;
        }
    }

    public static void b(Context context) {
        f = BitmapFactory.decodeResource(context.getResources(), R.drawable.dialog_bg);
        g = BitmapFactory.decodeResource(context.getResources(), R.drawable.button_bg);
        i = BitmapFactory.decodeResource(context.getResources(), R.drawable.item_bg);
        a = new BitmapDrawable(f);
        b = new BitmapDrawable(g);
        d = new BitmapDrawable(i);
    }

    public static void a() {
        if (j != null) {
            e = null;
            j.recycle();
        }
        if (h != null) {
            c = null;
            h.recycle();
        }
    }

    public static void b() {
        if (f != null) {
            f.recycle();
        }
        if (g != null) {
            g.recycle();
        }
        if (i != null) {
            i.recycle();
        }
    }
}
