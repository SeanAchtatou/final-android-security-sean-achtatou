package com.feelingtouch.b;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.feelingtouch.e.k;

/* compiled from: BannerAdUtil */
public final class h {
    public static void a(Context context, String str, String str2) {
        if (k.b(str)) {
            try {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            } catch (Exception e) {
                try {
                    if (k.b(str2)) {
                        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str2)));
                    }
                } catch (Exception e2) {
                }
            }
        }
    }
}
