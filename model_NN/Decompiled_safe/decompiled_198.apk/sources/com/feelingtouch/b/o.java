package com.feelingtouch.b;

import android.view.View;

/* compiled from: MsgDialog */
final class o implements View.OnClickListener {
    final /* synthetic */ m a;

    o(m mVar) {
        this.a = mVar;
    }

    public final void onClick(View view) {
        if (this.a.b != 2 || !this.a.d.f) {
            this.a.dismiss();
            l.a();
            this.a.d = null;
            this.a.c = null;
            return;
        }
        h.a(this.a.a, this.a.d.c, this.a.d.d);
        System.exit(0);
    }
}
