package com.feelingtouch.d.e;

import com.feelingtouch.c.b;
import com.feelingtouch.d.b.c;
import java.io.IOException;
import java.net.URI;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.NoConnectionReuseStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

/* compiled from: RpcHttpChannel */
public final class a implements com.feelingtouch.d.a {
    protected DefaultHttpClient a;
    protected String b;
    protected c c;

    public a(c cVar) {
        this.c = cVar;
        a();
    }

    private void a() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, this.c.c());
        c cVar = this.c;
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 0);
        HttpConnectionParams.setTcpNoDelay(basicHttpParams, true);
        this.b = this.c.a();
        this.a = new DefaultHttpClient(basicHttpParams);
        this.a.setReuseStrategy(new NoConnectionReuseStrategy());
        b bVar = com.feelingtouch.c.c.a;
        new Object[1][0] = "create rpc http channel instance,base url:" + this.b;
    }

    public final String a(String str, String str2) {
        URI uri = new URI(String.valueOf(this.b) + str);
        b bVar = com.feelingtouch.c.c.a;
        new Object[1][0] = "invoke json rpc call to url:" + uri.toString();
        HttpPost httpPost = new HttpPost(uri);
        httpPost.setEntity(new StringEntity(str2, this.c.c()));
        httpPost.setHeader("Content-Type", this.c.b());
        httpPost.setHeader("Content-Encoding", this.c.c());
        if (this.a == null) {
            a();
        }
        HttpResponse execute = this.a.execute(httpPost);
        if (execute.getStatusLine().getStatusCode() == 200) {
            return EntityUtils.toString(execute.getEntity(), this.c.c());
        }
        httpPost.abort();
        b bVar2 = com.feelingtouch.c.c.a;
        new Object[1][0] = "invoke json rpc call failed:" + execute.getStatusLine() + ",Error Code:" + execute.getStatusLine().getStatusCode();
        throw new IOException("Network Error:" + execute.getStatusLine() + ",Error Code:" + execute.getStatusLine().getStatusCode());
    }
}
