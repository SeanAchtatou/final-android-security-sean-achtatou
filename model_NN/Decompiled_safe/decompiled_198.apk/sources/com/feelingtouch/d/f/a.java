package com.feelingtouch.d.f;

import com.feelingtouch.e.e;

/* compiled from: TokenGenerator */
public abstract class a {
    public static String a(String str, String str2) {
        String str3;
        if (str == null) {
            str3 = "";
        } else {
            str3 = str;
        }
        if (str2 == null || str2.length() <= 2) {
            return "";
        }
        return e.a(String.valueOf(str3) + ((Object) str2.subSequence(2, str2.length() / 2)));
    }
}
