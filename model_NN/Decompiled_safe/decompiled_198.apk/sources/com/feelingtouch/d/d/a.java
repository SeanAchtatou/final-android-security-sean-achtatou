package com.feelingtouch.d.d;

import com.feelingtouch.d.b.b;
import com.feelingtouch.d.d.a.c;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: GameBoxTransport */
public final class a {
    public static a c = new a(new b());
    protected b a;
    protected com.feelingtouch.d.a b;

    private a(b bVar) {
        this.a = bVar;
        try {
            this.b = new com.feelingtouch.d.e.a(this.a);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public final void a(String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject();
            String date = new Date().toString();
            String a2 = com.feelingtouch.d.f.a.a("", date);
            jSONObject.put("username", str);
            jSONObject.put("labelName", str2);
            jSONObject.put("timestamp", date);
            jSONObject.put("token", a2);
            this.b.a("creategameuser", jSONObject.toString());
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }

    public final void a(String str, String str2, int i, String str3, String str4) {
        try {
            JSONObject jSONObject = new JSONObject();
            String date = new Date().toString();
            String a2 = com.feelingtouch.d.f.a.a("", date);
            jSONObject.put("imei", str4);
            jSONObject.put("timestamp", date);
            jSONObject.put("token", a2);
            jSONObject.put("userName", str);
            jSONObject.put("packageName", str2);
            jSONObject.put("score", i);
            jSONObject.put("dateTime", System.currentTimeMillis());
            jSONObject.put("locale", str3);
            this.b.a("submitgamescore", jSONObject.toString());
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }

    public final List a() {
        LinkedList linkedList = new LinkedList();
        try {
            JSONArray jSONArray = new JSONObject(this.b.a("getgamelist", "")).getJSONArray("results");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                com.feelingtouch.d.d.a.a aVar = new com.feelingtouch.d.d.a.a();
                aVar.a = jSONObject.getString("labelName");
                aVar.b = jSONObject.getString("packageName");
                aVar.c = jSONObject.getString("iconLink");
                aVar.d = jSONObject.getString("downloadURI");
                aVar.e = jSONObject.getString("desc");
                aVar.g = jSONObject.getBoolean("isHot");
                aVar.f = jSONObject.getBoolean("isOwn");
                aVar.h = jSONObject.getInt("downloadCnt");
                aVar.i = jSONObject.getInt("clickCnt");
                linkedList.add(aVar);
            }
            return linkedList;
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }

    public final int a(String str, int i) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("packageName", str);
            jSONObject.put("score", i);
            return new JSONObject(this.b.a("getrank", jSONObject.toString())).getInt("rank");
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }

    public final List b(String str, int i) {
        LinkedList linkedList = new LinkedList();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("gameName", str);
            jSONObject.put("length", 8);
            jSONObject.put("offset", i);
            JSONArray jSONArray = new JSONObject(this.b.a("getgametopscores", jSONObject.toString())).getJSONArray("results");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                c cVar = new c();
                cVar.a = jSONObject2.getString("userName");
                cVar.b = jSONObject2.getString("packageName");
                cVar.c = jSONObject2.getInt("score");
                cVar.d = jSONObject2.getLong("dateTime");
                cVar.e = jSONObject2.getString("locale");
                linkedList.add(cVar);
            }
            return linkedList;
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }

    public final void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("packageName", str);
            this.b.a("increasegameclickcnt", jSONObject.toString());
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }

    public final com.feelingtouch.d.d.a.b a(boolean z) {
        com.feelingtouch.d.d.a.b bVar = new com.feelingtouch.d.d.a.b();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("isPortrait", z);
            JSONObject jSONObject2 = new JSONObject(this.b.a("loadads", jSONObject.toString()));
            bVar.a = jSONObject2.getInt("version");
            if (bVar.a != 0) {
                bVar.b = jSONObject2.getInt("numberofimages");
                bVar.c = jSONObject2.getInt("fps");
                bVar.e = jSONObject2.getString("marketlink");
                bVar.d = new LinkedList();
                JSONArray jSONArray = jSONObject2.getJSONArray("imagelinks");
                for (int i = 0; i < jSONArray.length(); i++) {
                    bVar.d.add(jSONArray.getJSONObject(i).getString("imagelink"));
                }
            }
            return bVar;
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }

    public final void a(String str, String str2, String str3, long j, long j2, String str4) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("packageName", str);
            jSONObject.put("imei", str2);
            jSONObject.put("gmt", str3);
            jSONObject.put("duration", j);
            jSONObject.put("startTime", j2);
            jSONObject.put("paramString", str4);
            this.b.a("submitAnalyticRecords", jSONObject.toString());
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }
}
