package com.feelingtouch.d.a.a;

import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: GameAdBanner */
public final class b {
    public String a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;

    public static List a(JSONArray jSONArray) {
        LinkedList linkedList = new LinkedList();
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            if (jSONObject != null) {
                b bVar = new b();
                bVar.a = jSONObject.getString("iconLink");
                bVar.b = jSONObject.getString("desc");
                bVar.c = jSONObject.getString("marketLink");
                bVar.d = jSONObject.getString("httpLink");
                bVar.e = jSONObject.getString("gameName");
                bVar.f = jSONObject.getString("packageName");
                linkedList.add(bVar);
            }
        }
        return linkedList;
    }
}
