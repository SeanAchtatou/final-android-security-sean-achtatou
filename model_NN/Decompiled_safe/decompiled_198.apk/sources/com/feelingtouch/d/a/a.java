package com.feelingtouch.d.a;

import com.feelingtouch.d.a.a.b;
import com.feelingtouch.d.a.a.c;
import com.feelingtouch.d.a.a.d;
import java.io.IOException;
import java.net.URISyntaxException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AdsTransport */
public final class a {
    public static a c = new a(new com.feelingtouch.d.b.a());
    protected com.feelingtouch.d.b.a a;
    protected com.feelingtouch.d.a b;

    private a(com.feelingtouch.d.b.a aVar) {
        this.a = aVar;
        try {
            this.b = new com.feelingtouch.d.e.a(this.a);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public final com.feelingtouch.d.a.a.a a(String str, String str2, String str3, int i, String str4) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("imei", str);
            jSONObject.put("lan", str2);
            jSONObject.put("con", str3);
            jSONObject.put("os", i);
            jSONObject.put("pname", str4);
            JSONObject jSONObject2 = new JSONObject(this.b.a("loadBannerads", jSONObject.toString()));
            JSONArray jSONArray = jSONObject2.getJSONArray("ads");
            com.feelingtouch.d.a.a.a aVar = new com.feelingtouch.d.a.a.a();
            aVar.a = b.a(jSONArray);
            aVar.b = jSONObject2.getInt("percentage");
            try {
                JSONObject jSONObject3 = jSONObject2.getJSONObject("adMessage");
                c cVar = new c();
                cVar.a = jSONObject3.getString("title");
                cVar.b = jSONObject3.getString("desc");
                cVar.c = jSONObject3.getString("httpLink");
                cVar.d = jSONObject3.getInt("msgVersion");
                cVar.e = jSONObject3.getString("ownerPackageName");
                aVar.c = cVar;
            } catch (Exception e) {
            }
            try {
                JSONObject jSONObject4 = jSONObject2.getJSONObject("adUpgrade");
                d dVar = new d();
                dVar.a = jSONObject4.getString("title");
                dVar.b = jSONObject4.getString("desc");
                dVar.c = jSONObject4.getString("marketLink");
                dVar.d = jSONObject4.getString("httpLink");
                dVar.e = jSONObject4.getString("ownerPackageName");
                dVar.f = jSONObject4.getBoolean("isForceUpgrade");
                dVar.g = jSONObject4.getInt("upVersion");
                aVar.d = dVar;
            } catch (Exception e2) {
            }
            return aVar;
        } catch (URISyntaxException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        } catch (IOException e4) {
            throw new com.feelingtouch.d.c.a(e4);
        } catch (JSONException e5) {
            throw new com.feelingtouch.d.c.a(e5);
        }
    }

    public final void a(String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("packageName", str);
            jSONObject.put("imei", str2);
            this.b.a("submitInstall", jSONObject.toString());
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }

    public final void a(String str, String str2, String str3, int i) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("fromPackageName", str);
            jSONObject.put("imei", str2);
            jSONObject.put("toPackageName", str3);
            jSONObject.put("type", i);
            this.b.a("submitClick", jSONObject.toString());
        } catch (URISyntaxException e) {
            throw new com.feelingtouch.d.c.a(e);
        } catch (IOException e2) {
            throw new com.feelingtouch.d.c.a(e2);
        } catch (JSONException e3) {
            throw new com.feelingtouch.d.c.a(e3);
        }
    }
}
