package com.feelingtouch.imagelazyload;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.ProgressBar;

/* compiled from: ImageDisplayer */
public final class a implements Runnable {
    protected ProgressBar a;
    protected ImageView b;
    protected Drawable c;
    protected Drawable d;

    public a(ImageView imageView, Drawable drawable, ProgressBar progressBar) {
        this.b = imageView;
        this.c = drawable;
        this.a = progressBar;
    }

    public final void run() {
        this.a.setVisibility(8);
        if (this.c != null) {
            this.b.setImageDrawable(this.c);
        } else {
            this.b.setImageDrawable(this.d);
        }
    }
}
