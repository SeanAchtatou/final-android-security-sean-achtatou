package com.feelingtouch.imagelazyload;

import android.app.ListActivity;
import android.widget.ImageView;

public class ListImageActivity extends ListActivity implements b {
    public final void a(a aVar) {
        runOnUiThread(aVar);
    }

    public final void a(ImageView imageView) {
        imageView.setBackgroundDrawable(null);
    }
}
