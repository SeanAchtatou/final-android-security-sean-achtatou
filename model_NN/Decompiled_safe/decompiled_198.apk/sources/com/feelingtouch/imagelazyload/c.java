package com.feelingtouch.imagelazyload;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.feelingtouch.c.b;
import java.util.HashMap;

/* compiled from: ImageReceiver */
public final class c extends Thread {
    protected static HashMap e = new HashMap();
    protected static Drawable f;
    protected String a;
    protected b b;
    protected ImageView c;
    protected ProgressBar d;
    protected String g;

    public c(String str, b bVar, ImageView imageView, Drawable drawable, ProgressBar progressBar, String str2) {
        this.a = str;
        this.b = bVar;
        this.c = imageView;
        this.d = progressBar;
        f = drawable;
        this.g = str2;
        if (e.containsKey(this.a)) {
            ImageView imageView2 = this.c;
            Drawable drawable2 = f;
            this.b.a(new a(imageView2, (Drawable) e.get(this.a), this.d));
            return;
        }
        b bVar2 = com.feelingtouch.c.c.a;
        getClass();
        Object[] objArr = {"download", this.a};
        this.b.a(this.c);
        start();
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:9:0x0071=Splitter:B:9:0x0071, B:13:0x00a2=Splitter:B:13:0x00a2, B:17:0x00d3=Splitter:B:17:0x00d3} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r9 = this;
            r7 = 2
            r6 = 1
            r5 = 0
            r1 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x00a1, NullPointerException -> 0x00d2 }
            java.lang.String r2 = r9.a     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x00a1, NullPointerException -> 0x00d2 }
            r0.<init>(r2)     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x00a1, NullPointerException -> 0x00d2 }
            java.lang.Object r0 = r0.getContent()     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x00a1, NullPointerException -> 0x00d2 }
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x00a1, NullPointerException -> 0x00d2 }
            java.lang.String r2 = "src"
            android.graphics.drawable.Drawable r0 = android.graphics.drawable.Drawable.createFromStream(r0, r2)     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x00a1, NullPointerException -> 0x00d2 }
            java.lang.String r1 = r9.g     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            if (r1 == 0) goto L_0x0049
            java.lang.String r1 = r9.a     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            r2 = 47
            int r1 = r1.lastIndexOf(r2)     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            java.lang.String r2 = r9.a     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            int r1 = r1 + 1
            java.lang.String r3 = r9.a     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            int r3 = r3.length()     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            java.lang.String r1 = r2.substring(r1, r3)     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            java.lang.String r3 = r9.g     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            java.lang.String r1 = r1.toString()     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            java.lang.String r2 = r9.a     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
            com.feelingtouch.e.c.a(r2, r1)     // Catch:{ MalformedURLException -> 0x013c, IOException -> 0x0136, NullPointerException -> 0x0131, all -> 0x012c }
        L_0x0049:
            java.util.HashMap r1 = com.feelingtouch.imagelazyload.c.e
            java.lang.String r2 = r9.a
            r1.put(r2, r0)
            com.feelingtouch.imagelazyload.a r1 = new com.feelingtouch.imagelazyload.a
            android.widget.ImageView r2 = r9.c
            android.graphics.drawable.Drawable r3 = com.feelingtouch.imagelazyload.c.f
            android.widget.ProgressBar r3 = r9.d
            r1.<init>(r2, r0, r3)
            com.feelingtouch.imagelazyload.b r0 = r9.b
            r0.a(r1)
            com.feelingtouch.c.b r0 = com.feelingtouch.c.c.a
            r9.getClass()
            java.lang.Object[] r0 = new java.lang.Object[r7]
            java.lang.String r1 = "download finish,"
            r0[r5] = r1
            java.lang.String r1 = r9.a
            r0[r6] = r1
        L_0x006f:
            return
        L_0x0070:
            r0 = move-exception
        L_0x0071:
            com.feelingtouch.c.b r2 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x0104 }
            java.lang.Class r3 = r9.getClass()     // Catch:{ all -> 0x0104 }
            r2.a(r3, r0)     // Catch:{ all -> 0x0104 }
            java.util.HashMap r0 = com.feelingtouch.imagelazyload.c.e
            java.lang.String r2 = r9.a
            r0.put(r2, r1)
            com.feelingtouch.imagelazyload.a r0 = new com.feelingtouch.imagelazyload.a
            android.widget.ImageView r2 = r9.c
            android.graphics.drawable.Drawable r3 = com.feelingtouch.imagelazyload.c.f
            android.widget.ProgressBar r3 = r9.d
            r0.<init>(r2, r1, r3)
            com.feelingtouch.imagelazyload.b r1 = r9.b
            r1.a(r0)
            com.feelingtouch.c.b r0 = com.feelingtouch.c.c.a
            r9.getClass()
            java.lang.Object[] r0 = new java.lang.Object[r7]
            java.lang.String r1 = "download finish,"
            r0[r5] = r1
            java.lang.String r1 = r9.a
            r0[r6] = r1
            goto L_0x006f
        L_0x00a1:
            r0 = move-exception
        L_0x00a2:
            com.feelingtouch.c.b r2 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x0104 }
            java.lang.Class r3 = r9.getClass()     // Catch:{ all -> 0x0104 }
            r2.a(r3, r0)     // Catch:{ all -> 0x0104 }
            java.util.HashMap r0 = com.feelingtouch.imagelazyload.c.e
            java.lang.String r2 = r9.a
            r0.put(r2, r1)
            com.feelingtouch.imagelazyload.a r0 = new com.feelingtouch.imagelazyload.a
            android.widget.ImageView r2 = r9.c
            android.graphics.drawable.Drawable r3 = com.feelingtouch.imagelazyload.c.f
            android.widget.ProgressBar r3 = r9.d
            r0.<init>(r2, r1, r3)
            com.feelingtouch.imagelazyload.b r1 = r9.b
            r1.a(r0)
            com.feelingtouch.c.b r0 = com.feelingtouch.c.c.a
            r9.getClass()
            java.lang.Object[] r0 = new java.lang.Object[r7]
            java.lang.String r1 = "download finish,"
            r0[r5] = r1
            java.lang.String r1 = r9.a
            r0[r6] = r1
            goto L_0x006f
        L_0x00d2:
            r0 = move-exception
        L_0x00d3:
            com.feelingtouch.c.b r2 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x0104 }
            java.lang.Class r3 = r9.getClass()     // Catch:{ all -> 0x0104 }
            r2.a(r3, r0)     // Catch:{ all -> 0x0104 }
            java.util.HashMap r0 = com.feelingtouch.imagelazyload.c.e
            java.lang.String r2 = r9.a
            r0.put(r2, r1)
            com.feelingtouch.imagelazyload.a r0 = new com.feelingtouch.imagelazyload.a
            android.widget.ImageView r2 = r9.c
            android.graphics.drawable.Drawable r3 = com.feelingtouch.imagelazyload.c.f
            android.widget.ProgressBar r3 = r9.d
            r0.<init>(r2, r1, r3)
            com.feelingtouch.imagelazyload.b r1 = r9.b
            r1.a(r0)
            com.feelingtouch.c.b r0 = com.feelingtouch.c.c.a
            r9.getClass()
            java.lang.Object[] r0 = new java.lang.Object[r7]
            java.lang.String r1 = "download finish,"
            r0[r5] = r1
            java.lang.String r1 = r9.a
            r0[r6] = r1
            goto L_0x006f
        L_0x0104:
            r0 = move-exception
        L_0x0105:
            java.util.HashMap r2 = com.feelingtouch.imagelazyload.c.e
            java.lang.String r3 = r9.a
            r2.put(r3, r1)
            com.feelingtouch.imagelazyload.a r2 = new com.feelingtouch.imagelazyload.a
            android.widget.ImageView r3 = r9.c
            android.graphics.drawable.Drawable r4 = com.feelingtouch.imagelazyload.c.f
            android.widget.ProgressBar r4 = r9.d
            r2.<init>(r3, r1, r4)
            com.feelingtouch.imagelazyload.b r1 = r9.b
            r1.a(r2)
            com.feelingtouch.c.b r1 = com.feelingtouch.c.c.a
            r9.getClass()
            java.lang.Object[] r1 = new java.lang.Object[r7]
            java.lang.String r2 = "download finish,"
            r1[r5] = r2
            java.lang.String r2 = r9.a
            r1[r6] = r2
            throw r0
        L_0x012c:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0105
        L_0x0131:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00d3
        L_0x0136:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00a2
        L_0x013c:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.imagelazyload.c.run():void");
    }
}
