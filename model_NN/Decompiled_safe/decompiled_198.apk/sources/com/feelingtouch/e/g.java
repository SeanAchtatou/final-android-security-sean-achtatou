package com.feelingtouch.e;

import android.app.ProgressDialog;
import android.content.Context;
import com.feelingtouch.shooting.R;

/* compiled from: ProgressableTask */
public final class g {
    private final Context a;
    private ProgressDialog b;
    private final int c;
    private final CharSequence d;
    /* access modifiers changed from: private */
    public final f e;
    private Thread f;
    private int g;
    private volatile boolean h;

    public g(Context context, f fVar, byte b2) {
        this(context, fVar);
    }

    private g(Context context, f fVar) {
        this.g = -1;
        this.a = context;
        this.c = R.string.gamebox_submit;
        this.d = context.getString(R.string.gamebox_submitting);
        this.g = R.string.gamebox_cancel;
        this.e = fVar;
    }

    public final void a() {
        this.b = new ProgressDialog(this.a);
        if (this.c != -1) {
            this.b.setTitle(this.c);
        }
        this.b.setMessage(this.d);
        if (this.g != -1) {
            this.b.setButton(this.a.getText(this.g), new i(this));
            this.b.setOnCancelListener(new j(this));
        }
        this.b.show();
        this.f = new h(this, "ProgressableTask");
        this.f.start();
    }

    public final void b() {
        this.h = true;
        c();
        this.f.interrupt();
        this.e.a();
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.b.isShowing()) {
            this.b.dismiss();
        }
    }
}
