package com.feelingtouch.e;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: MD5 */
public final class e {
    private static MessageDigest a;

    static {
        try {
            a = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new Error(e);
        }
    }

    private e() {
    }

    private static synchronized String a(byte[] bArr) {
        String a2;
        synchronized (e.class) {
            a.update(bArr, 0, bArr.length);
            a2 = k.a(a.digest());
        }
        return a2;
    }

    public static String a(String str) {
        return a(k.c(str)).substring(8, 24);
    }
}
