package com.feelingtouch.e;

import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;

/* compiled from: StringUtil */
public final class k {
    public static final Pattern a = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*");
    private static final char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    private k() {
    }

    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean b(String str) {
        return str != null && str.length() > 0;
    }

    public static byte[] c(String str) {
        try {
            return str.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new Error(e);
        }
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            byte b3 = b2 & 255;
            sb.append(b[b3 >> 4]);
            sb.append(b[b3 & 15]);
        }
        return sb.toString();
    }
}
