package com.feelingtouch.e;

import java.io.File;

/* compiled from: IOUtil */
public final class d {
    private d() {
    }

    public static void a(File file) {
        File parentFile = file.getAbsoluteFile().getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
    }
}
