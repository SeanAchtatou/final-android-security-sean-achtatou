package com.feelingtouch.e;

/* compiled from: FileDownloader */
public class c {
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x007c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r8, java.lang.String r9) {
        /*
            r2 = 1
            r5 = 0
            r4 = 0
            com.feelingtouch.c.b r0 = com.feelingtouch.c.c.a
            java.lang.Class<com.feelingtouch.e.c> r1 = com.feelingtouch.e.c.class
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r3 = "download start"
            r2[r4] = r3
            r0.a(r1, r2)
            java.net.URL r0 = new java.net.URL     // Catch:{ all -> 0x00a7 }
            r0.<init>(r8)     // Catch:{ all -> 0x00a7 }
            java.net.URLConnection r8 = r0.openConnection()     // Catch:{ all -> 0x00a7 }
            java.net.HttpURLConnection r8 = (java.net.HttpURLConnection) r8     // Catch:{ all -> 0x00a7 }
            r0 = 5000(0x1388, float:7.006E-42)
            r8.setConnectTimeout(r0)     // Catch:{ all -> 0x0087 }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x0080 }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ FileNotFoundException -> 0x0080 }
            java.io.InputStream r2 = r8.getInputStream()     // Catch:{ FileNotFoundException -> 0x0080 }
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0080 }
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0080 }
            r8.getContentLength()     // Catch:{ all -> 0x00ac }
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x00ac }
            r1.<init>(r9)     // Catch:{ all -> 0x00ac }
            java.io.File r2 = r1.getAbsoluteFile()     // Catch:{ all -> 0x00ac }
            java.io.File r2 = r2.getParentFile()     // Catch:{ all -> 0x00ac }
            boolean r3 = r2.exists()     // Catch:{ all -> 0x00ac }
            if (r3 != 0) goto L_0x0047
            r2.mkdirs()     // Catch:{ all -> 0x00ac }
        L_0x0047:
            boolean r2 = r1.exists()     // Catch:{ all -> 0x00ac }
            if (r2 == 0) goto L_0x0050
            r1.delete()     // Catch:{ all -> 0x00ac }
        L_0x0050:
            r1.createNewFile()     // Catch:{ all -> 0x00ac }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ all -> 0x00ac }
            r2.<init>(r1)     // Catch:{ all -> 0x00ac }
            r1 = 5120(0x1400, float:7.175E-42)
            byte[] r1 = new byte[r1]     // Catch:{ all -> 0x006a }
        L_0x005c:
            r3 = 0
            r4 = 5120(0x1400, float:7.175E-42)
            int r3 = r0.read(r1, r3, r4)     // Catch:{ all -> 0x006a }
            if (r3 < 0) goto L_0x008c
            r4 = 0
            r2.write(r1, r4, r3)     // Catch:{ all -> 0x006a }
            goto L_0x005c
        L_0x006a:
            r1 = move-exception
            r3 = r2
            r2 = r8
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x0070:
            if (r3 == 0) goto L_0x0075
            r3.close()
        L_0x0075:
            if (r1 == 0) goto L_0x007a
            r1.close()
        L_0x007a:
            if (r2 == 0) goto L_0x007f
            r2.disconnect()
        L_0x007f:
            throw r0
        L_0x0080:
            r0 = move-exception
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0087 }
            r0.<init>()     // Catch:{ all -> 0x0087 }
            throw r0     // Catch:{ all -> 0x0087 }
        L_0x0087:
            r0 = move-exception
            r1 = r5
            r2 = r8
            r3 = r5
            goto L_0x0070
        L_0x008c:
            com.feelingtouch.c.b r1 = com.feelingtouch.c.c.a     // Catch:{ all -> 0x006a }
            java.lang.Class<com.feelingtouch.e.c> r3 = com.feelingtouch.e.c.class
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x006a }
            r5 = 0
            java.lang.String r6 = "download finish"
            r4[r5] = r6     // Catch:{ all -> 0x006a }
            r1.a(r3, r4)     // Catch:{ all -> 0x006a }
            r2.close()
            r0.close()
            if (r8 == 0) goto L_0x00a6
            r8.disconnect()
        L_0x00a6:
            return
        L_0x00a7:
            r0 = move-exception
            r1 = r5
            r2 = r5
            r3 = r5
            goto L_0x0070
        L_0x00ac:
            r1 = move-exception
            r2 = r8
            r3 = r5
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feelingtouch.e.c.a(java.lang.String, java.lang.String):void");
    }
}
