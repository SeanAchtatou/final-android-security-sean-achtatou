package com.feelingtouch.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.feelingtouch.c.c;

/* compiled from: AndroidUtil */
public class a {
    public static final String a(Context context) {
        String b = b(context);
        if (!k.a(b)) {
            return b;
        }
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            String str = (String) cls.getMethod("get", String.class, String.class).invoke(cls, "ro.serialno", "unknown");
            if (str == null) {
                return "";
            }
            return str;
        } catch (Exception e) {
            return b;
        }
    }

    private static String b(Context context) {
        try {
            String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            if (deviceId == null) {
                return "";
            }
            return deviceId;
        } catch (Exception e) {
            c.a.a(a.class, "getIMEI error", e.getMessage());
            return "";
        }
    }

    public static void a(Activity activity, String[] strArr, String str, String str2) {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            if (strArr.length != 0) {
                intent.putExtra("android.intent.extra.EMAIL", strArr);
            }
            intent.putExtra("android.intent.extra.TEXT", str2);
            intent.putExtra("android.intent.extra.SUBJECT", str);
            intent.setType("message/rfc882");
            Intent.createChooser(intent, "Choose Email Client");
            activity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
