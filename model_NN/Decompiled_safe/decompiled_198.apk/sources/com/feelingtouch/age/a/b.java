package com.feelingtouch.age.a;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.feelingtouch.age.b.a;
import java.util.ArrayList;

/* compiled from: FrameEntryFactory */
public final class b {
    private static ArrayList a = new ArrayList();

    public static a[] a(Resources resources, int i, int i2, int i3, int i4, int[] iArr) {
        Bitmap decodeResource = BitmapFactory.decodeResource(resources, i);
        a[] aVarArr = new a[i4];
        int height = decodeResource.getHeight();
        for (int i5 = 0; i5 < i4; i5++) {
            Bitmap createBitmap = Bitmap.createBitmap(i2, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas();
            canvas.setBitmap(createBitmap);
            int i6 = (i3 + i5) * i2;
            canvas.drawBitmap(decodeResource, new Rect(i6, 0, i6 + i2, height), new Rect(0, 0, i2, height), (Paint) null);
            aVarArr[i5] = new a(createBitmap, iArr[i5]);
        }
        a.a(decodeResource);
        a.add(aVarArr);
        return aVarArr;
    }

    public static a[] a(Resources resources, int i, int i2, int i3, int i4) {
        int[] iArr = new int[1];
        for (int i5 = 0; i5 <= 0; i5++) {
            iArr[0] = i4;
        }
        return a(resources, i, i2, i3, 1, iArr);
    }

    private static ArrayList b(a[] aVarArr) {
        int length = aVarArr.length;
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            Bitmap bitmap = aVarArr[i].a;
            arrayList.add(new a(Bitmap.createScaledBitmap(bitmap, -bitmap.getWidth(), bitmap.getHeight(), true), aVarArr[i].b));
        }
        return arrayList;
    }

    public static a[] a(a[] aVarArr) {
        return (a[]) b(aVarArr).toArray(new a[0]);
    }

    public static ArrayList a(ArrayList arrayList) {
        return b((a[]) arrayList.toArray(new a[0]));
    }

    public static void a() {
        for (int i = 0; i < a.size(); i++) {
            a[] aVarArr = (a[]) a.get(i);
            for (a a2 : aVarArr) {
                a2.a();
            }
        }
        a.clear();
        System.gc();
    }
}
