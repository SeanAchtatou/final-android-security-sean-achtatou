package com.feelingtouch.age.a;

import android.graphics.Bitmap;

/* compiled from: FrameEntry */
public final class a {
    public Bitmap a;
    public int b;
    public int c;

    public a() {
        this.a = null;
        this.a = null;
        this.b = 0;
    }

    public a(Bitmap bitmap, int i) {
        this.a = null;
        this.a = bitmap;
        this.b = i;
    }

    public final void a() {
        if (this.a != null) {
            this.a.recycle();
            this.a = null;
        }
    }
}
