package com.feelingtouch.age.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.feelingtouch.age.b.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: Sprite */
public abstract class c {
    protected int a = 0;
    protected int b = 0;
    protected int c = 1;
    protected ArrayList d = new ArrayList();
    protected ArrayList e = new ArrayList();
    protected ArrayList f = new ArrayList();
    protected int g = -1;
    protected int h = -1;
    protected float i = 1.0f;
    protected float j = 1.0f;
    protected LinkedList k = new LinkedList();
    protected boolean l = true;
    protected int m = 0;
    protected Rect n = new Rect();
    protected Paint o = new Paint();
    private boolean p = false;
    private boolean q = false;
    private int r;
    private long s;
    private long t;
    private int u;

    /* access modifiers changed from: protected */
    public abstract void a();

    public c() {
        this.o.setAntiAlias(true);
        this.o.setFilterBitmap(true);
    }

    public c(c cVar) {
        this.d = cVar.d;
        this.e = cVar.e;
        this.f = cVar.f;
        this.o.setAntiAlias(true);
        this.o.setFilterBitmap(true);
    }

    public Rect b() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.e = b.a(this.d);
    }

    /* access modifiers changed from: protected */
    public final void a(a[] aVarArr) {
        synchronized (this.k) {
            this.k.clear();
            for (a aVar : aVarArr) {
                aVar.c = aVar.b;
                this.k.add(aVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final int d() {
        int size;
        synchronized (this.k) {
            size = this.k.size();
        }
        return size;
    }

    public final void a(Canvas canvas) {
        a(canvas, (Paint) null);
    }

    public final void b(Canvas canvas) {
        a(canvas, this.o);
    }

    private void a(Canvas canvas, Paint paint) {
        boolean z;
        if (this.d.size() != 0 || this.k.size() != 0) {
            a();
            if (this.q) {
                if (System.currentTimeMillis() - this.s > this.t) {
                    this.q = false;
                } else {
                    if ((this.m - this.r) % this.u == 0) {
                        if (this.p) {
                            z = false;
                        } else {
                            z = true;
                        }
                        this.p = z;
                    }
                    if (this.p) {
                        this.m++;
                        return;
                    }
                }
            }
            int f2 = f();
            int g2 = g();
            this.n.left = this.a;
            this.n.top = this.b;
            this.n.right = f2 + this.a;
            this.n.bottom = g2 + this.b;
            synchronized (this.k) {
                if (this.k.size() == 0) {
                    int intValue = ((Integer) this.f.get(this.m % this.f.size())).intValue();
                    if (this.l) {
                        a.a(canvas, ((a) this.d.get(intValue)).a, this.n, paint);
                    } else {
                        a.a(canvas, ((a) this.e.get(intValue)).a, this.n, paint);
                    }
                } else {
                    a aVar = (a) this.k.removeFirst();
                    a.a(canvas, aVar.a, this.n, paint);
                    aVar.c--;
                    if (aVar.c > 0) {
                        this.k.addFirst(aVar);
                    }
                }
            }
            this.m++;
        }
    }

    /* access modifiers changed from: protected */
    public final void b(a[] aVarArr) {
        for (a aVar : aVarArr) {
            this.d.add(aVar);
            int i2 = aVar.b * this.c;
            for (int i3 = 0; i3 < i2; i3++) {
                this.f.add(Integer.valueOf(this.d.size() - 1));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        Iterator it = this.d.iterator();
        while (it.hasNext()) {
            ((a) it.next()).a();
        }
        Iterator it2 = this.e.iterator();
        while (it2.hasNext()) {
            ((a) it2.next()).a();
        }
        Iterator it3 = this.k.iterator();
        while (it3.hasNext()) {
            ((a) it3.next()).a();
        }
    }

    /* access modifiers changed from: protected */
    public void a(float f2, float f3) {
        this.i = f2;
        this.j = f3;
        this.g = -1;
        this.h = -1;
    }

    public final int f() {
        if (this.g == -1 && this.d.size() > 0) {
            Bitmap bitmap = ((a) this.d.get(0)).a;
            this.g = (int) (((float) bitmap.getWidth()) * this.i);
            this.h = (int) (((float) bitmap.getHeight()) * this.j);
        }
        return this.g;
    }

    public final int g() {
        if (this.h == -1 && this.d.size() > 0) {
            Bitmap bitmap = ((a) this.d.get(0)).a;
            this.g = (int) (((float) bitmap.getWidth()) * this.i);
            this.h = (int) (((float) bitmap.getHeight()) * this.j);
        }
        return this.h;
    }
}
