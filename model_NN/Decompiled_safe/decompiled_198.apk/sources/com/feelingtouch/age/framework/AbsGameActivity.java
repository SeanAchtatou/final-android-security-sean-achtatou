package com.feelingtouch.age.framework;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import com.feelingtouch.age.framework.a.a;
import com.feelingtouch.age.framework.view.GameView;

public abstract class AbsGameActivity extends Activity {
    protected GameView a;
    protected a b;
    private boolean c = true;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String str = com.feelingtouch.age.framework.b.a.g;
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.a != null && this.b.d()) {
            this.a.a().c();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.a != null) {
            this.a.c();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.a != null) {
            this.a.a().d();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.gc();
        System.gc();
        System.exit(0);
    }

    public final void a() {
        if (this.a != null) {
            this.a.c();
            this.a = null;
        }
        if (this.c) {
            new a(this).start();
        }
        finish();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (this.a != null) {
            this.a.b();
        }
        return true;
    }
}
