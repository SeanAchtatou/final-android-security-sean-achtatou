package com.feelingtouch.age.framework;

import android.os.Handler;
import android.os.Message;

/* compiled from: AbsLoadingActivity */
final class b extends Handler {
    final /* synthetic */ AbsLoadingActivity a;

    b(AbsLoadingActivity absLoadingActivity) {
        this.a = absLoadingActivity;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 1:
                this.a.b();
                this.a.finish();
                return;
            default:
                return;
        }
    }
}
