package com.feelingtouch.age.framework.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.feelingtouch.age.framework.d;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    protected Thread a;
    protected d b;
    protected SurfaceHolder c;
    protected boolean d = false;

    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setKeepScreenOn(true);
        this.c = getHolder();
        this.c.addCallback(this);
        setFocusable(true);
        setKeepScreenOn(true);
    }

    public final d a() {
        return this.b;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        if (!this.d) {
            this.b.a();
            this.d = true;
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.a == null || !this.a.isAlive()) {
            this.a = new Thread(this.b);
            this.a.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean z = false;
        this.b.b();
        while (!z) {
            try {
                this.a.join();
                z = true;
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.b.e();
    }

    public final void b() {
        this.b.f();
    }

    public final void c() {
        boolean z = false;
        this.b.b();
        while (!z) {
            try {
                if (this.a != null) {
                    this.a.join();
                }
                z = true;
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
