package com.feelingtouch.age.framework;

import android.graphics.Canvas;
import android.graphics.PaintFlagsDrawFilter;
import android.view.SurfaceHolder;
import com.feelingtouch.age.framework.a.a;

/* compiled from: GameRunnable */
public final class d implements Runnable {
    protected SurfaceHolder a;
    protected boolean b;
    protected boolean c;
    protected a d;
    private PaintFlagsDrawFilter e;

    public final void a() {
        this.b = false;
        a aVar = this.d;
        a aVar2 = this.d;
        this.b = true;
    }

    public final void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        long currentTimeMillis = System.currentTimeMillis();
        do {
        } while (!this.b);
        while (this.c) {
            long j = currentTimeMillis;
            while (j < 40 + currentTimeMillis) {
                j = System.currentTimeMillis();
            }
            synchronized (this.a) {
                this.d.a();
                try {
                    Canvas lockCanvas = this.a.lockCanvas();
                    if (lockCanvas != null) {
                        lockCanvas.setDrawFilter(this.e);
                        this.d.b();
                        this.a.unlockCanvasAndPost(lockCanvas);
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                    System.exit(0);
                }
            }
            currentTimeMillis = j;
        }
        return;
    }

    public final void b() {
        this.c = false;
    }

    public final void c() {
        this.d.c();
    }

    public final void d() {
        this.c = true;
    }

    public final boolean e() {
        if (!this.b) {
            return true;
        }
        a aVar = this.d;
        return true;
    }

    public final void f() {
        if (this.b) {
            this.d.e();
        }
    }
}
