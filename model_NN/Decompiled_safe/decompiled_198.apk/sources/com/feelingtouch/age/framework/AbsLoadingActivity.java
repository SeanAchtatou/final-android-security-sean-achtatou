package com.feelingtouch.age.framework;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import com.feelingtouch.age.framework.b.a;

public abstract class AbsLoadingActivity extends Activity {
    protected Handler a = new b(this);

    /* access modifiers changed from: protected */
    public abstract boolean a();

    /* access modifiers changed from: protected */
    public abstract void b();

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        int width = getWindowManager().getDefaultDisplay().getWidth();
        int height = getWindowManager().getDefaultDisplay().getHeight();
        if (width < height) {
            a.a(width, height, false);
        } else {
            a.a(width, height, true);
        }
        new c(this).start();
    }

    public final void c() {
        if (this.a != null) {
            this.a.sendEmptyMessage(1);
        }
    }
}
