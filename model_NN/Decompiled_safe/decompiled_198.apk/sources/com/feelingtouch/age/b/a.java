package com.feelingtouch.age.b;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: AgeUtil */
public final class a {
    private static ArrayList a = new ArrayList();

    public static void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }

    public static void a() {
        Iterator it = a.iterator();
        while (it.hasNext()) {
            a((Bitmap) it.next());
        }
        a.clear();
        System.gc();
    }

    public static void a(Canvas canvas, Bitmap bitmap, Rect rect, Paint paint) {
        if (bitmap != null && !bitmap.isRecycled()) {
            canvas.drawBitmap(bitmap, (Rect) null, rect, paint);
        }
    }
}
