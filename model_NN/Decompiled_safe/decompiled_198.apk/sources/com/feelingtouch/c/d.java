package com.feelingtouch.c;

import java.io.PrintStream;

/* compiled from: SystemOutLogger */
public final class d extends a {
    public final void b(Class cls, Object... objArr) {
        String sb;
        PrintStream printStream = System.err;
        if (objArr.length == 1) {
            sb = String.valueOf(a.a(cls)) + objArr[0];
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(a.a(cls));
            for (Object append : objArr) {
                sb2.append(append);
                sb2.append(' ');
            }
            sb = sb2.toString();
        }
        printStream.println(sb);
    }

    public final void b(Class cls, Throwable th) {
        System.err.print(a(cls));
        th.printStackTrace();
    }
}
