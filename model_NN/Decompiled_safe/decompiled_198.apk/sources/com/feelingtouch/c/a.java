package com.feelingtouch.c;

/* compiled from: AbstractLogger */
public abstract class a implements b {
    /* access modifiers changed from: protected */
    public abstract void b(Class cls, Throwable th);

    /* access modifiers changed from: protected */
    public abstract void b(Class cls, Object... objArr);

    public final void a(Class cls, Object... objArr) {
        b(cls, objArr);
    }

    public final void a(Class cls, Throwable th) {
        b(cls, th);
    }

    public static String a(Class cls) {
        return "[" + cls.getSimpleName() + "]:";
    }
}
