package com.feelingtouch.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import com.feelingtouch.e.d;
import java.io.File;

/* compiled from: GameADShow */
public final class c {
    private SharedPreferences a;
    private Context b;
    private Drawable c = null;
    private a d;
    private boolean e;

    public c(Context context) {
        this.b = context;
        this.e = false;
        this.d = new a(this.b);
        this.a = this.b.getSharedPreferences("game_ad_version_preference", 2);
        if (this.a.getInt("game_ad_version", -1) > 0) {
            String str = this.e ? String.valueOf("/sdcard/.gameAd/") + "AD_PORTRAIT.jpg" : String.valueOf("/sdcard/.gameAd/") + "AD_LANDSCAPE.jpg";
            File file = new File(str);
            d.a(file);
            this.c = file.exists() ? Drawable.createFromPath(str) : null;
        }
    }

    public final Drawable a() {
        if (this.a.getBoolean("game_ad_isclicked", false) || this.b.getPackageName().equals(this.a.getString("game_ad_marketlink", ""))) {
            return null;
        }
        return this.c;
    }

    public final String b() {
        return this.a.getString("game_ad_marketlink", "");
    }
}
