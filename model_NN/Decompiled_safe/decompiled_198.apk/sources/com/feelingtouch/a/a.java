package com.feelingtouch.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.feelingtouch.d.d.a.b;
import com.feelingtouch.e.c;
import java.io.IOException;
import java.util.List;

/* compiled from: GameADLoader */
public final class a {
    private Context a;
    /* access modifiers changed from: private */
    public b b;
    private SharedPreferences c = this.a.getSharedPreferences("game_ad_version_preference", 2);
    /* access modifiers changed from: private */
    public boolean d = false;

    public a(Context context) {
        this.a = context;
        new b(this).execute(null);
    }

    static /* synthetic */ boolean b(a aVar) {
        return aVar.c.getInt("game_ad_version", -1) == aVar.b.a;
    }

    static /* synthetic */ void d(a aVar) {
        aVar.c.edit().putInt("game_ad_version", aVar.b.a).commit();
        aVar.c.edit().putString("game_ad_marketlink", aVar.b.e).commit();
        aVar.c.edit().putBoolean("game_ad_isclicked", false).commit();
    }

    static /* synthetic */ void c(a aVar) {
        int i = aVar.b.b;
        List list = aVar.b.d;
        for (int i2 = 0; i2 < i; i2++) {
            String str = (String) list.get(i2);
            String str2 = aVar.d ? String.valueOf("/sdcard/.gameAd/") + "AD_PORTRAIT.jpg" : String.valueOf("/sdcard/.gameAd/") + "AD_LANDSCAPE.jpg";
            try {
                c.a(str, str2);
            } catch (IOException e) {
                com.feelingtouch.c.c.a.a(aVar.getClass(), "Load Images " + str2 + " Exception");
                e.printStackTrace();
            }
        }
    }
}
