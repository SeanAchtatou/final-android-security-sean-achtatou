package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;
import org.json.JSONArray;

/* compiled from: Ad */
final class bk implements Runnable {
    private WeakReference a;
    private JSONArray b;

    public bk(bg bgVar, JSONArray jSONArray) {
        this.a = new WeakReference(bgVar);
        this.b = jSONArray;
    }

    public final void run() {
        try {
            bg bgVar = (bg) this.a.get();
            if (bgVar != null) {
                bg.a(bgVar, this.b);
            }
        } catch (Exception e) {
            if (s.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in Ad$ViewAdd.run(), " + e.getMessage());
                e.printStackTrace();
            }
            bg bgVar2 = (bg) this.a.get();
            if (bgVar2 != null) {
                bgVar2.o();
            }
        }
    }
}
