package com.admob.android.ads;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;

/* compiled from: AdMobVideoViewNative */
public final class ag implements View.OnClickListener {
    private WeakReference a;

    public ag(y yVar) {
        this.a = new WeakReference(yVar);
    }

    public final void onClick(View view) {
        y yVar = (y) this.a.get();
        if (yVar != null) {
            yVar.f.a("replay", (Map) null);
            if (yVar.d != null) {
                y.b(yVar.d);
            }
            yVar.b(false);
            yVar.h = true;
            yVar.a(yVar.getContext());
        }
    }
}
