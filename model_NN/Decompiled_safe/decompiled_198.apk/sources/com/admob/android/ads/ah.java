package com.admob.android.ads;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;

/* compiled from: AdMobVideoViewNative */
public final class ah implements View.OnClickListener {
    private WeakReference a;
    private boolean b;

    public ah(y yVar, boolean z) {
        this.a = new WeakReference(yVar);
        this.b = z;
    }

    public final void onClick(View view) {
        y yVar = (y) this.a.get();
        if (yVar != null) {
            if (this.b) {
                yVar.f.a("skip", (Map) null);
            }
            yVar.c();
        }
    }
}
