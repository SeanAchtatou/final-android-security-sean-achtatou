package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;

/* compiled from: AdContainer */
final class bm implements Runnable {
    private WeakReference a;

    public bm(bl blVar) {
        this.a = new WeakReference(blVar);
    }

    public final void run() {
        try {
            bl blVar = (bl) this.a.get();
            if (blVar != null) {
                blVar.addView(blVar.c);
            }
        } catch (Exception e) {
            if (s.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in AdContainer post run(), " + e.getMessage());
            }
        }
    }
}
