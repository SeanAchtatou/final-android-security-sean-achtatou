package com.admob.android.ads;

import android.util.Log;

/* compiled from: TestLog */
public final class s implements br {
    o a;

    public static boolean a(String str, int i) {
        return (i >= 5) || Log.isLoggable(str, i);
    }

    public final void a() {
        if (this.a != null) {
            this.a.c();
        }
    }

    public final void a(bg bgVar) {
        if (this.a != null) {
            this.a.g = bgVar;
            this.a.a();
        }
    }
}
