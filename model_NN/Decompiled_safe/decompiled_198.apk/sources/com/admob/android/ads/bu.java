package com.admob.android.ads;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

/* compiled from: MovieInfo */
public final class bu implements bs {
    public String a;
    public String b;
    public int c;
    public int d;
    public String e;
    public String f;
    public double g;
    public String h;
    public String i;
    public boolean j = false;
    public String k;
    public String l;
    public Vector m = new Vector();

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("u", this.a);
        bundle.putString("t", this.b);
        bundle.putInt("c", this.c);
        bundle.putInt("msm", this.d);
        bundle.putString("s", this.e);
        bundle.putString("sin", this.f);
        bundle.putDouble("sd", this.g);
        bundle.putString("skd", this.h);
        bundle.putString("sku", this.i);
        bundle.putByte("nosk", this.j ? (byte) 1 : 0);
        bundle.putString("rd", this.k);
        bundle.putString("ru", this.l);
        bundle.putParcelableArrayList("b", g.a(this.m));
        return bundle;
    }

    public final boolean a(Bundle bundle) {
        boolean z;
        if (bundle == null) {
            return false;
        }
        this.a = bundle.getString("u");
        this.b = bundle.getString("t");
        this.c = bundle.getInt("c");
        this.d = bundle.getInt("msm");
        this.e = bundle.getString("s");
        this.f = bundle.getString("sin");
        this.g = bundle.getDouble("sd");
        this.h = bundle.getString("skd");
        this.i = bundle.getString("sku");
        this.j = bz.a(bundle.getByte("nosk"));
        this.k = bundle.getString("rd");
        this.l = bundle.getString("ru");
        this.m = null;
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("b");
        if (parcelableArrayList != null) {
            Vector vector = new Vector();
            Iterator it = parcelableArrayList.iterator();
            while (it.hasNext()) {
                Bundle bundle2 = (Bundle) it.next();
                if (bundle2 != null) {
                    bt btVar = new bt();
                    if (bundle2 == null) {
                        z = false;
                    } else {
                        btVar.a = bundle2.getString("ad");
                        btVar.b = bundle2.getString("au");
                        btVar.c = bundle2.getString("t");
                        if (!btVar.d.a(bundle2.getBundle("oi"))) {
                            z = false;
                        } else {
                            btVar.e = bundle2.getString("ap");
                            btVar.f = bundle2.getString("json");
                            z = true;
                        }
                    }
                    if (z) {
                        vector.add(btVar);
                    }
                }
            }
            this.m = vector;
        }
        return true;
    }

    public final boolean b() {
        return this.c == 0 || this.m == null || this.m.size() == 0;
    }

    public final boolean c() {
        return this.f != null && this.f.length() > 0 && this.g > 0.0d;
    }
}
