package com.admob.android.ads;

import android.webkit.WebView;
import java.lang.ref.WeakReference;

/* compiled from: AdMobCanvasView */
public final class ck extends ai {
    private /* synthetic */ cj b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ck(cj cjVar, cj cjVar2, WeakReference weakReference) {
        super(cjVar2, weakReference);
        this.b = cjVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        if ("http://mm.admob.com/static/android/canvas.html".equals(str) && this.b.d) {
            this.b.d = false;
            this.b.loadUrl("javascript:cb('" + this.b.a + "','" + this.b.c + "')");
        }
    }
}
