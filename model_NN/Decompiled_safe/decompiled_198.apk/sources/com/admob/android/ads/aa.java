package com.admob.android.ads;

import java.lang.ref.WeakReference;

/* compiled from: AdMobVideoViewNative */
final class aa implements Runnable {
    private WeakReference a;

    public aa(y yVar) {
        this.a = new WeakReference(yVar);
    }

    public final void run() {
        y yVar = (y) this.a.get();
        if (yVar != null) {
            yVar.g();
            yVar.d();
        }
    }
}
