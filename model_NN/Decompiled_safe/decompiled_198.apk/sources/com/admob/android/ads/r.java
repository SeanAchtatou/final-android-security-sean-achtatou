package com.admob.android.ads;

import java.lang.ref.WeakReference;

/* compiled from: InterstitialAd */
final class r implements Runnable {
    private WeakReference a;

    public r(o oVar) {
        this.a = new WeakReference(oVar);
    }

    public final void run() {
        o oVar = (o) this.a.get();
        if (oVar != null) {
            oVar.d();
        }
    }
}
