package com.admob.android.ads;

import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

/* compiled from: MyParcelableUtil */
public final class g implements br {
    private WeakReference a;

    public g() {
    }

    public static ArrayList a(Vector vector) {
        if (vector == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = vector.iterator();
        while (it.hasNext()) {
            bs bsVar = (bs) it.next();
            if (bsVar == null) {
                arrayList.add(null);
            } else {
                arrayList.add(bsVar.a());
            }
        }
        return arrayList;
    }

    public static Bundle a(bs bsVar) {
        if (bsVar == null) {
            return null;
        }
        return bsVar.a();
    }

    public g(AdView adView) {
        this.a = new WeakReference(adView);
    }

    public final void a() {
        AdView adView = (AdView) this.a.get();
        if (adView != null) {
            AdView.f(adView);
        }
    }

    public final void a(bg bgVar) {
        AdView adView = (AdView) this.a.get();
        if (adView != null) {
            synchronized (adView) {
                if (adView.b == null || !bgVar.equals(adView.b.d())) {
                    if (s.a("AdMobSDK", 4)) {
                        Log.i("AdMobSDK", "Ad returned (" + (SystemClock.uptimeMillis() - adView.o) + " ms):  " + bgVar);
                    }
                    adView.getContext();
                    adView.a(bgVar, bgVar.b());
                } else if (s.a("AdMobSDK", 3)) {
                    Log.d("AdMobSDK", "Received the same ad we already had.  Discarding it.");
                }
            }
        }
    }
}
