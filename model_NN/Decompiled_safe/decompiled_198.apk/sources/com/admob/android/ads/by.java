package com.admob.android.ads;

/* compiled from: AdMobOpener */
public enum by {
    PORTRAIT("p"),
    LANDSCAPE("l"),
    ANY("a");
    
    private String d;

    private by(String str) {
        this.d = str;
    }

    public final String toString() {
        return this.d;
    }

    public static by a(int i) {
        by byVar = ANY;
        by byVar2 = byVar;
        for (by byVar3 : values()) {
            if (byVar3.ordinal() == i) {
                byVar2 = byVar3;
            }
        }
        return byVar2;
    }
}
