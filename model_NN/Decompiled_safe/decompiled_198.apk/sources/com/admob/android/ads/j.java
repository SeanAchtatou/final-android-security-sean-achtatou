package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;

/* compiled from: AdView */
final class j implements Runnable {
    boolean a;
    private WeakReference b;

    public j(AdView adView) {
        this.b = new WeakReference(adView);
    }

    public final void run() {
        try {
            AdView adView = (AdView) this.b.get();
            if (!this.a && adView != null) {
                if (s.a("AdMobSDK", 3)) {
                    int k = adView.c / 1000;
                    if (s.a("AdMobSDK", 3)) {
                        Log.d("AdMobSDK", "Requesting a fresh ad because a request interval passed (" + k + " seconds).");
                    }
                }
                adView.f();
            }
        } catch (Exception e) {
            if (s.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in RefreshHandler.run(), " + e.getMessage());
            }
        }
    }
}
