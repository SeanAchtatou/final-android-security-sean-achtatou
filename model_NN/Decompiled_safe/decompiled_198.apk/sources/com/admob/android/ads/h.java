package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;

/* compiled from: AdView */
final class h extends Thread {
    private WeakReference a;

    public h(AdView adView) {
        this.a = new WeakReference(adView);
    }

    public final void run() {
        AdView adView = (AdView) this.a.get();
        if (adView != null) {
            try {
                Context context = adView.getContext();
                if (aw.a(AdView.c(adView), context, adView.i, adView.j, adView.a(), adView.b(), adView.c(), new bl(context, adView), (int) (((float) adView.getMeasuredWidth()) / bl.e()), adView.d(), null, adView.e()) == null) {
                    AdView.f(adView);
                }
            } catch (Exception e) {
                if (s.a("AdMobSDK", 6)) {
                    Log.e("AdMobSDK", "Unhandled exception requesting a fresh ad.", e);
                }
                AdView.f(adView);
            } finally {
                boolean unused = adView.n = false;
                adView.a(true);
            }
        }
    }
}
