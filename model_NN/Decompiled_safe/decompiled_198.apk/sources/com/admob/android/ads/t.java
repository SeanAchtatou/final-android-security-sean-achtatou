package com.admob.android.ads;

import java.lang.ref.WeakReference;

/* compiled from: InterstitialAd */
final class t implements Runnable {
    private WeakReference a;

    public t(o oVar) {
        this.a = new WeakReference(oVar);
    }

    public final void run() {
        o oVar = (o) this.a.get();
        if (oVar != null) {
            oVar.b();
        }
    }
}
