package com.admob.android.ads;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AdMobOpenerInfo */
public final class bz implements bs {
    public bh a = bh.CLICK_TO_BROWSER;
    public String b = "";
    public Vector c = new Vector();
    public String d = null;
    public by e = by.ANY;
    public boolean f = false;
    public Point g = new Point(4, 4);
    public bu h = null;
    public String i = null;
    public String j = null;
    public Bundle k = new Bundle();
    public boolean l = false;
    private boolean m = false;
    private Point n = new Point(0, 0);
    private String o = null;

    public final void a(String str, boolean z) {
        if (str != null && !"".equals(str)) {
            this.c.add(new ch(str, z));
        }
    }

    public final Hashtable b() {
        Set<String> keySet = this.k.keySet();
        Hashtable hashtable = new Hashtable();
        for (String next : keySet) {
            Parcelable parcelable = this.k.getParcelable(next);
            if (parcelable instanceof Bitmap) {
                hashtable.put(next, (Bitmap) parcelable);
            }
        }
        return hashtable;
    }

    public final Bundle a() {
        byte b2;
        Bundle bundle = new Bundle();
        bundle.putString("a", this.a.toString());
        bundle.putString("t", this.b);
        bundle.putParcelableArrayList("c", g.a(this.c));
        bundle.putString("u", this.d);
        bundle.putInt("or", this.e.ordinal());
        bundle.putByte("tr", this.m ? (byte) 1 : 0);
        if (this.f) {
            b2 = 1;
        } else {
            b2 = 0;
        }
        bundle.putByte("sc", b2);
        bundle.putIntArray("cbo", a(this.g));
        bundle.putIntArray("cs", a(this.n));
        bundle.putBundle("mi", g.a(this.h));
        bundle.putString("su", this.i);
        bundle.putString("si", this.j);
        bundle.putString("json", this.o);
        bundle.putBundle("$", this.k);
        bundle.putByte("int", this.l ? (byte) 1 : 0);
        return bundle;
    }

    public final boolean a(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        this.a = bh.a(bundle.getString("a"));
        this.b = bundle.getString("t");
        this.c = new Vector();
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("c");
        if (parcelableArrayList != null) {
            Iterator it = parcelableArrayList.iterator();
            while (it.hasNext()) {
                Bundle bundle2 = (Bundle) it.next();
                if (bundle2 != null) {
                    ch chVar = new ch();
                    chVar.a = bundle2.getString("u");
                    chVar.b = bundle2.getBoolean("p", false);
                    this.c.add(chVar);
                }
            }
        }
        this.d = bundle.getString("u");
        this.e = by.a(bundle.getInt("or"));
        this.m = a(bundle.getByte("tr"));
        this.f = a(bundle.getByte("sc"));
        this.g = a(bundle.getIntArray("cbo"));
        if (this.g == null) {
            this.g = new Point(4, 4);
        }
        this.n = a(bundle.getIntArray("cs"));
        bu buVar = new bu();
        if (buVar.a(bundle.getBundle("mi"))) {
            this.h = buVar;
        } else {
            this.h = null;
        }
        this.i = bundle.getString("su");
        this.j = bundle.getString("si");
        this.o = bundle.getString("json");
        this.k = bundle.getBundle("$");
        this.l = a(bundle.getByte("int"));
        return true;
    }

    public final void a(JSONObject jSONObject, ce ceVar, String str) {
        boolean z;
        this.a = bh.a(jSONObject.optString("a"));
        a(jSONObject.optString("au"), true);
        a(jSONObject.optString("tu"), false);
        JSONObject optJSONObject = jSONObject.optJSONObject("stats");
        if (optJSONObject != null) {
            this.i = optJSONObject.optString("url");
            this.j = optJSONObject.optString("id");
        }
        String optString = jSONObject.optString("or");
        if (optString != null && !optString.equals("")) {
            if ("l".equals(optString)) {
                this.e = by.LANDSCAPE;
            } else {
                this.e = by.PORTRAIT;
            }
        }
        if (jSONObject.opt("t") != null) {
            z = true;
        } else {
            z = false;
        }
        this.m = z;
        this.b = jSONObject.optString("title");
        if (this.a == bh.CLICK_TO_INTERACTIVE_VIDEO) {
            this.h = new bu();
            JSONObject optJSONObject2 = jSONObject.optJSONObject("$");
            if (ceVar != null) {
                try {
                    ceVar.a(optJSONObject2, str);
                } catch (JSONException e2) {
                }
            }
            this.h.a = jSONObject.optString("u");
            this.h.b = jSONObject.optString("title");
            this.h.c = jSONObject.optInt("mc", 2);
            this.h.d = jSONObject.optInt("msm", 0);
            this.h.e = jSONObject.optString("stats");
            this.h.f = jSONObject.optString("splash");
            this.h.g = jSONObject.optDouble("splash_duration", 1.5d);
            this.h.h = jSONObject.optString("skip_down");
            this.h.i = jSONObject.optString("skip_up");
            this.h.j = jSONObject.optBoolean("no_splash_skip");
            this.h.k = jSONObject.optString("replay_down");
            this.h.l = jSONObject.optString("replay_up");
            JSONArray optJSONArray = jSONObject.optJSONArray("buttons");
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i2 = 0; i2 < length; i2++) {
                    JSONObject optJSONObject3 = optJSONArray.optJSONObject(i2);
                    bt btVar = new bt();
                    btVar.a = optJSONObject3.optString("$");
                    btVar.b = optJSONObject3.optString("h");
                    btVar.c = optJSONObject3.optString("x");
                    btVar.e = optJSONObject3.optString("analytics_page_name");
                    btVar.d.a(optJSONObject3.optJSONObject("o"), ceVar, str);
                    btVar.f = optJSONObject3.optJSONObject("o").toString();
                    this.h.m.add(btVar);
                }
            }
        }
        this.f = jSONObject.optInt("sc", 0) != 0;
        JSONArray optJSONArray2 = jSONObject.optJSONArray("co");
        if (optJSONArray2 != null && optJSONArray2.length() >= 2) {
            this.g = new Point(optJSONArray2.optInt(0), optJSONArray2.optInt(1));
        }
        this.o = jSONObject.toString();
    }

    private static int[] a(Point point) {
        if (point == null) {
            return null;
        }
        return new int[]{point.x, point.y};
    }

    private static Point a(int[] iArr) {
        if (iArr == null || iArr.length == 2) {
            return null;
        }
        return new Point(iArr[0], iArr[1]);
    }

    public static boolean a(byte b2) {
        return b2 == 1;
    }
}
