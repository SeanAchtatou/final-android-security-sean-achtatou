package com.admob.android.ads;

import android.media.MediaPlayer;
import android.util.Log;
import java.lang.ref.WeakReference;

/* compiled from: AdMobVideoViewNative */
final class z implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private WeakReference a;

    public z(y yVar) {
        this.a = new WeakReference(yVar);
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        y yVar = (y) this.a.get();
        if (yVar != null) {
            yVar.a();
        }
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        y yVar = (y) this.a.get();
        if (yVar != null) {
            yVar.i = true;
            yVar.f();
            yVar.a(true);
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        if (s.a("AdMobSDK", 6)) {
            Log.e("AdMobSDK", "error playing video, what: " + i + ", extra: " + i2);
        }
        y yVar = (y) this.a.get();
        if (yVar == null) {
            return false;
        }
        yVar.c();
        return true;
    }
}
