package com.admob.android.ads;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import java.util.Date;

/* compiled from: AdManager */
final class c implements LocationListener {
    private /* synthetic */ LocationManager a;

    c(LocationManager locationManager) {
        this.a = locationManager;
    }

    public final void onLocationChanged(Location location) {
        Location unused = b.h = location;
        long unused2 = b.k = System.currentTimeMillis();
        this.a.removeUpdates(this);
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Acquired location " + b.h.getLatitude() + "," + b.h.getLongitude() + " at " + new Date(b.k).toString() + ".");
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
