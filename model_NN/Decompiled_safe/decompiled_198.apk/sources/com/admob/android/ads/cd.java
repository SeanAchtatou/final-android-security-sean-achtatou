package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

/* compiled from: AdMobLocalizer */
public final class cd implements be {
    private static cd a = null;
    private static Context b = null;
    private static Thread c = null;
    private static String d = null;
    private Properties e = null;
    private Context f;

    public static void a(Context context) {
        if (b == null && context != null) {
            b = context.getApplicationContext();
        }
        if (a == null) {
            a = new cd(b);
        }
    }

    public static String a(String str) {
        a(b);
        cd cdVar = a;
        cdVar.b();
        if (cdVar.e == null) {
            return str;
        }
        String property = cdVar.e.getProperty(str);
        return (property == null || property.equals("")) ? str : property;
    }

    private cd(Context context) {
        this.f = context;
        d = a();
        if (a != null) {
            a.e = null;
        }
        if (!b() && c == null) {
            Thread thread = new Thread(bd.a("http://mm.admob.com/static/android/i18n/20101109" + "/" + d + ".properties", b.g(this.f), this));
            c = thread;
            thread.start();
        }
    }

    public static String a() {
        if (d == null) {
            String language = Locale.getDefault().getLanguage();
            d = language;
            if (language == null) {
                d = "en";
            }
        }
        return d;
    }

    private boolean b() {
        if (this.e == null) {
            try {
                Properties properties = new Properties();
                File a2 = a(this.f, d);
                if (a2.exists()) {
                    properties.load(new FileInputStream(a2));
                    this.e = properties;
                }
            } catch (IOException e2) {
                this.e = null;
            }
        }
        return this.e != null;
    }

    private static File a(Context context, String str) {
        File file = new File(context.getCacheDir(), "admob_cache");
        if (!file.exists()) {
            file.mkdir();
        }
        File file2 = new File(file, "20101109");
        if (!file2.exists()) {
            file2.mkdir();
        }
        return new File(file2, str + ".properties");
    }

    public final void a(bb bbVar, Exception exc) {
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Could not get localized strings from the AdMob servers.");
        }
    }

    public final void a(bb bbVar) {
        try {
            byte[] a2 = bbVar.a();
            if (a2 != null) {
                FileOutputStream fileOutputStream = new FileOutputStream(a(this.f, d));
                fileOutputStream.write(a2);
                fileOutputStream.close();
            }
        } catch (Exception e2) {
            if (s.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "Could not store localized strings to cache file.");
            }
        }
    }
}
