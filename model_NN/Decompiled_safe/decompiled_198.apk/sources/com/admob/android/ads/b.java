package com.admob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.GregorianCalendar;

/* compiled from: AdManager */
public final class b {
    private static String a;
    private static int b;
    private static String c;
    private static String d;
    private static String e = bh.CLICK_TO_BROWSER.toString();
    private static String[] f = null;
    private static String g;
    /* access modifiers changed from: private */
    public static Location h;
    private static boolean i = false;
    private static boolean j = false;
    /* access modifiers changed from: private */
    public static long k;
    private static String l;
    private static GregorianCalendar m;
    private static d n;
    private static boolean o = false;
    private static Boolean p = null;

    static {
        if (s.a("AdMobSDK", 4)) {
            Log.i("AdMobSDK", "AdMob SDK version is 20101109-ANDROID-3312276cc1406347");
        }
    }

    private b() {
    }

    protected static void a(String str) {
        if (s.a("AdMobSDK", 6)) {
            Log.e("AdMobSDK", str);
        }
        throw new IllegalArgumentException(str);
    }

    static void a(Context context) {
        if (!o) {
            o = true;
            try {
                PackageManager packageManager = context.getPackageManager();
                String packageName = context.getPackageName();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 128);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        String a2 = a(applicationInfo.metaData, "ADMOB_PUBLISHER_ID", c);
                        if (a2 != null) {
                            b(a2);
                            if (s.a("AdMobSDK", 4)) {
                                Log.i("AdMobSDK", "Publisher ID set to " + a2);
                            }
                            c = a2;
                        }
                        String a3 = a(applicationInfo.metaData, "ADMOB_INTERSTITIAL_PUBLISHER_ID", d);
                        if (a3 != null) {
                            b(a3);
                            if (s.a("AdMobSDK", 4)) {
                                Log.i("AdMobSDK", "Interstitial Publisher ID set to " + a3);
                            }
                            d = a3;
                        }
                        if (!j) {
                            i = applicationInfo.metaData.getBoolean("ADMOB_ALLOW_LOCATION_FOR_ADS", false);
                        }
                    }
                    a = applicationInfo.packageName;
                    if (c != null) {
                        b(c);
                    }
                    if (d != null) {
                        b(d);
                    }
                    if (s.a("AdMobSDK", 2)) {
                        Log.v("AdMobSDK", "Application's package name is " + a);
                    }
                }
                PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
                if (packageInfo != null) {
                    b = packageInfo.versionCode;
                    if (s.a("AdMobSDK", 2)) {
                        Log.v("AdMobSDK", "Application's version number is " + b);
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    private static String a(Bundle bundle, String str, String str2) {
        String string = bundle.getString(str);
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Publisher ID read from AndroidManifest.xml is " + string);
        }
        if (str2 != null || string == null) {
            return null;
        }
        return string;
    }

    public static String b(Context context) {
        if (a == null) {
            a(context);
        }
        return a;
    }

    protected static int c(Context context) {
        if (a == null) {
            a(context);
        }
        return b;
    }

    public static String d(Context context) {
        if (c == null) {
            a(context);
        }
        if (c == null && s.a("AdMobSDK", 6)) {
            Log.e("AdMobSDK", "getPublisherId returning null publisher id.  Please set the publisher id in AndroidManifest.xml or using AdManager.setPublisherId(String)");
        }
        return c;
    }

    public static String e(Context context) {
        if (d == null) {
            a(context);
        }
        if (d == null && s.a("AdMobSDK", 6)) {
            Log.e("AdMobSDK", "getInterstitialPublisherId returning null publisher id.  Please set the publisher id in AndroidManifest.xml or using AdManager.setPublisherId(String)");
        }
        return d;
    }

    private static void b(String str) {
        if (str == null || str.length() != 15) {
            a("SETUP ERROR:  Incorrect AdMob publisher ID.  Should 15 [a-f,0-9] characters:  " + c);
        }
        if (a != null && str.equalsIgnoreCase("a1496ced2842262") && !"com.admob.android.ads".equals(a) && !"com.example.admob.lunarlander".equals(a)) {
            a("SETUP ERROR:  Cannot use the sample publisher ID (a1496ced2842262).  Yours is available on www.admob.com.");
        }
    }

    public static String a() {
        return e;
    }

    public static boolean f(Context context) {
        if (f == null) {
            return false;
        }
        String g2 = g(context);
        if (g2 == null) {
            g2 = "emulator";
        }
        if (Arrays.binarySearch(f, g2) >= 0) {
            return true;
        }
        return false;
    }

    public static boolean b() {
        return "unknown".equals(Build.BOARD) && "generic".equals(Build.DEVICE) && "generic".equals(Build.BRAND);
    }

    public static String g(Context context) {
        if (g == null) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string == null || b()) {
                g = "emulator";
                Log.i("AdMobSDK", "To get test ads on the emulator use AdManager.setTestDevices( new String[] { AdManager.TEST_EMULATOR } )");
            } else {
                g = c(string);
                Log.i("AdMobSDK", "To get test ads on this device use AdManager.setTestDevices( new String[] { \"" + g + "\" } )");
            }
            if (s.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "The user ID is " + g);
            }
        }
        if (g == "emulator") {
            return null;
        }
        return g;
    }

    private static String c(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (Exception e2) {
            if (s.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "Could not generate hash of " + str, e2);
            }
            return str.substring(0, 32);
        }
    }

    private static Location j(Context context) {
        String str;
        LocationManager locationManager;
        boolean z;
        if (b() && !i && s.a("AdMobSDK", 4)) {
            Log.i("AdMobSDK", "Location information is not being used for ad requests. Enable location");
            Log.i("AdMobSDK", "based ads with AdManager.setAllowUseOfLocation(true) or by setting ");
            Log.i("AdMobSDK", "meta-data ADMOB_ALLOW_LOCATION_FOR_ADS to true in AndroidManifest.xml");
        }
        if (i && context != null && (h == null || System.currentTimeMillis() > k + 900000)) {
            synchronized (context) {
                if (h == null || System.currentTimeMillis() > k + 900000) {
                    k = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        if (s.a("AdMobSDK", 3)) {
                            Log.d("AdMobSDK", "Trying to get locations from the network.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria, true);
                            z = true;
                        } else {
                            str = null;
                            z = true;
                        }
                    } else {
                        str = null;
                        locationManager = null;
                        z = false;
                    }
                    if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        if (s.a("AdMobSDK", 3)) {
                            Log.d("AdMobSDK", "Trying to get locations from GPS.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            z = true;
                        }
                    }
                    if (!z) {
                        if (s.a("AdMobSDK", 3)) {
                            Log.d("AdMobSDK", "Cannot access user's location.  Permissions are not set.");
                        }
                    } else if (str != null) {
                        if (s.a("AdMobSDK", 3)) {
                            Log.d("AdMobSDK", "Location provider setup successfully.");
                        }
                        locationManager.requestLocationUpdates(str, 0, 0.0f, new c(locationManager), context.getMainLooper());
                    } else if (s.a("AdMobSDK", 3)) {
                        Log.d("AdMobSDK", "No location providers are available.  Ads will not be geotargeted.");
                    }
                }
            }
        }
        return h;
    }

    static String h(Context context) {
        String str = null;
        Location j2 = j(context);
        if (j2 != null) {
            str = j2.getLatitude() + "," + j2.getLongitude();
        }
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "User coordinates are " + str);
        }
        return str;
    }

    static String c() {
        return String.valueOf(k / 1000);
    }

    public static String d() {
        return l;
    }

    public static String i(Context context) {
        if (((WindowManager) context.getSystemService("window")).getDefaultDisplay().getOrientation() == 1) {
            return "l";
        }
        return "p";
    }

    static String e() {
        GregorianCalendar gregorianCalendar = m;
        if (gregorianCalendar == null) {
            return null;
        }
        return String.format("%04d%02d%02d", Integer.valueOf(gregorianCalendar.get(1)), Integer.valueOf(gregorianCalendar.get(2) + 1), Integer.valueOf(gregorianCalendar.get(5)));
    }

    static String f() {
        if (n == d.MALE) {
            return "m";
        }
        if (n == d.FEMALE) {
            return "f";
        }
        return null;
    }

    static v a(cg cgVar) {
        int mode = cgVar.a.getMode();
        if (b()) {
            return v.EMULATOR;
        }
        if (cgVar.a.isMusicActive() || cgVar.a.isSpeakerphoneOn() || mode == 2 || mode == 1) {
            return v.VIBRATE;
        }
        int ringerMode = cgVar.a.getRingerMode();
        if (ringerMode == 0 || ringerMode == 1) {
            return v.VIBRATE;
        }
        return v.SPEAKER;
    }
}
