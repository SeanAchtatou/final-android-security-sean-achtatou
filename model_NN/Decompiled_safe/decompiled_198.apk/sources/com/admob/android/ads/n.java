package com.admob.android.ads;

import android.view.animation.Animation;

/* compiled from: AdView */
final class n implements Animation.AnimationListener {
    private /* synthetic */ bl a;
    private /* synthetic */ AdView b;
    private /* synthetic */ bl c;

    n(bl blVar, AdView adView, bl blVar2) {
        this.a = blVar;
        this.b = adView;
        this.c = blVar2;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        if (this.a != null) {
            this.b.removeView(this.a);
        }
        bl unused = this.b.b = this.c;
        if (this.a != null) {
            this.a.f();
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }
}
