package com.admob.android.ads;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AdView extends RelativeLayout {
    private static Boolean a;
    private static Handler s = null;
    /* access modifiers changed from: private */
    public bl b;
    /* access modifiers changed from: private */
    public int c;
    private boolean d;
    private j e;
    private int f;
    private int g;
    private int h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public a k;
    private boolean l;
    private boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public long o;
    private g p;
    private bi q;
    private l r;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.as.<init>(float, float, float, float, float, boolean):void
     arg types: [int, int, float, float, float, int]
     candidates:
      com.admob.android.ads.as.<init>(float[], float[], float, float, float, boolean):void
      com.admob.android.ads.as.<init>(float, float, float, float, float, boolean):void */
    static /* synthetic */ void b(AdView adView, bl blVar) {
        blVar.setVisibility(8);
        as asVar = new as(0.0f, -90.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), true);
        asVar.setDuration(700);
        asVar.setFillAfter(true);
        asVar.setInterpolator(new AccelerateInterpolator());
        asVar.setAnimationListener(new f(adView, blVar));
        adView.startAnimation(asVar);
    }

    static /* synthetic */ g c(AdView adView) {
        if (adView.p == null) {
            adView.p = new g(adView);
        }
        return adView.p;
    }

    static /* synthetic */ void f(AdView adView) {
        if (adView.k != null) {
            s.post(new i(adView));
        }
    }

    static /* synthetic */ void j(AdView adView) {
        if (adView.k == null) {
            return;
        }
        if (adView.b == null || adView.b.getParent() == null) {
            try {
                a aVar = adView.k;
            } catch (Exception e2) {
                Log.w("AdMobSDK", "Unhandled exception raised in your AdListener.onReceiveAd.", e2);
            }
        } else {
            try {
                a aVar2 = adView.k;
            } catch (Exception e3) {
                Log.w("AdMobSDK", "Unhandled exception raised in your AdListener.onReceiveRefreshedAd.", e3);
            }
        }
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, l.a);
    }

    private AdView(Context context, AttributeSet attributeSet, int i2, l lVar) {
        super(context, attributeSet, i2);
        int i3;
        int i4;
        int i5;
        this.m = true;
        if (a == null) {
            a = new Boolean(isInEditMode());
        }
        if (s == null && !a.booleanValue()) {
            Handler handler = new Handler();
            s = handler;
            bg.a(handler);
        }
        this.r = lVar;
        if (lVar != l.a) {
            this.q = bi.VIEW;
        }
        setDescendantFocusability(262144);
        setClickable(true);
        setLongClickable(false);
        setGravity(17);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            if (attributeSet.getAttributeBooleanValue(str, "testing", false) && s.a("AdMobSDK", 5)) {
                Log.w("AdMobSDK", "AdView's \"testing\" XML attribute has been deprecated and will be ignored.  Please delete it from your XML layout and use AdManager.setTestDevices instead.");
            }
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -16777216);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            if (attributeUnsignedIntValue2 >= 0) {
                if (s.a("AdMobSDK", 5)) {
                    Log.w("AdMobSDK", "Calling the deprecated method setTextColor!  Please use setPrimaryTextColor and setSecondaryTextColor instead.");
                }
                b(attributeUnsignedIntValue2);
                c(attributeUnsignedIntValue2);
            }
            int attributeUnsignedIntValue3 = attributeSet.getAttributeUnsignedIntValue(str, "primaryTextColor", -1);
            int attributeUnsignedIntValue4 = attributeSet.getAttributeUnsignedIntValue(str, "secondaryTextColor", -1);
            this.i = attributeSet.getAttributeValue(str, "keywords");
            a(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            if (attributeSet.getAttributeBooleanValue(str, "isGoneWithoutAd", false) && s.a("AdMobSDK", 5)) {
                Log.w("AdMobSDK", "Deprecated method setGoneWithoutAd was called.  See JavaDoc for instructions to remove.");
            }
            i3 = attributeUnsignedIntValue4;
            int i6 = attributeUnsignedIntValue3;
            i5 = attributeUnsignedIntValue;
            i4 = i6;
        } else {
            i3 = -1;
            i4 = -1;
            i5 = -16777216;
        }
        setBackgroundColor(i5);
        b(i4);
        c(i3);
        this.b = null;
        this.p = null;
        if (a.booleanValue()) {
            TextView textView = new TextView(context, attributeSet, i2);
            textView.setBackgroundColor(this.f);
            textView.setTextColor(this.g);
            textView.setPadding(10, 10, 10, 10);
            textView.setTextSize(16.0f);
            textView.setGravity(16);
            textView.setText("Ads by AdMob");
            addView(textView, new RelativeLayout.LayoutParams(-1, -1));
            return;
        }
        f();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "AdView size is " + measuredWidth + " by " + measuredHeight);
        }
        if (a.booleanValue()) {
            return;
        }
        if (((float) ((int) (((float) measuredWidth) / bl.e()))) <= 310.0f) {
            if (s.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "We need to have a minimum width of 320 device independent pixels to show an ad.");
            }
            try {
                this.b.setVisibility(8);
            } catch (NullPointerException e2) {
            }
        } else {
            try {
                int visibility = this.b.getVisibility();
                this.b.setVisibility(super.getVisibility());
                if (visibility != 0 && this.b.getVisibility() == 0) {
                    a(this.b);
                }
            } catch (NullPointerException e3) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        ay.a(getContext());
        if (this.m || super.getVisibility() == 0) {
            if (!this.n) {
                this.n = true;
                this.o = SystemClock.uptimeMillis();
                new h(this).start();
            } else if (s.a("AdMobSDK", 5)) {
                Log.w("AdMobSDK", "Ignoring requestFreshAd() because we are requesting an ad right now already.");
            }
        } else if (s.a("AdMobSDK", 5)) {
            Log.w("AdMobSDK", "Cannot requestFreshAd() when the AdView is not visible.  Call AdView.setVisibility(View.VISIBLE) first.");
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(bg bgVar, bl blVar) {
        int visibility = super.getVisibility();
        double a2 = bgVar.a();
        if (a2 >= 0.0d) {
            this.d = true;
            a((int) a2);
            a(true);
        } else {
            this.d = false;
        }
        boolean z = this.m;
        if (z) {
            this.m = false;
        }
        blVar.a(bgVar);
        blVar.setVisibility(visibility);
        blVar.setGravity(17);
        bgVar.a(blVar);
        blVar.setLayoutParams(new RelativeLayout.LayoutParams(bgVar.a(bgVar.e()), bgVar.a(bgVar.f())));
        s.post(new k(this, blVar, visibility, z));
    }

    private void a(int i2) {
        int i3 = i2 * 1000;
        if (this.c != i3) {
            if (i2 > 0) {
                if (i2 < 13) {
                    if (s.a("AdMobSDK", 5)) {
                        Log.w("AdMobSDK", "AdView.setRequestInterval(" + i2 + ") seconds must be >= " + 13);
                    }
                    i3 = 13000;
                } else if (i2 > 600) {
                    if (s.a("AdMobSDK", 5)) {
                        Log.w("AdMobSDK", "AdView.setRequestInterval(" + i2 + ") seconds must be <= " + 600);
                    }
                    i3 = 600000;
                }
            }
            this.c = i3;
            if (i2 <= 0) {
                g();
            }
            if (s.a("AdMobSDK", 4)) {
                Log.i("AdMobSDK", "Requesting fresh ads every " + i2 + " seconds.");
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        boolean z2;
        bg d2;
        synchronized (this) {
            if (z) {
                if (this.c > 0 && getVisibility() == 0) {
                    int i2 = this.c;
                    g();
                    if (this.b == null || (d2 = this.b.d()) == null || !d2.d() || this.b.h() >= 120) {
                        z2 = true;
                    } else {
                        if (s.a("AdMobSDK", 3)) {
                            Log.d("AdMobSDK", "Cannot refresh CPM ads.  Ignoring request to refresh the ad.");
                        }
                        z2 = false;
                    }
                    if (z2) {
                        this.e = new j(this);
                        s.postDelayed(this.e, (long) i2);
                        if (s.a("AdMobSDK", 3)) {
                            Log.d("AdMobSDK", "Ad refresh scheduled for " + i2 + " from now.");
                        }
                    }
                }
            }
            if (!z || this.c == 0) {
                g();
            }
        }
    }

    private void g() {
        if (this.e != null) {
            this.e.a = true;
            this.e = null;
            if (s.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "Cancelled an ad refresh scheduled for the future.");
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        a(z);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        a(i2 == 0);
    }

    public final int a() {
        return this.g;
    }

    private void b(int i2) {
        this.g = -16777216 | i2;
    }

    public final int b() {
        return this.h;
    }

    private void c(int i2) {
        this.h = -16777216 | i2;
    }

    public void setBackgroundColor(int i2) {
        this.f = -16777216 | i2;
        invalidate();
    }

    public final int c() {
        return this.f;
    }

    public void setVisibility(int i2) {
        boolean z;
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                invalidate();
            }
        }
        if (i2 == 0) {
            z = true;
        } else {
            z = false;
        }
        a(z);
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.l = true;
        a(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.l = false;
        a(false);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void a(bl blVar) {
        this.b = blVar;
        if (this.l) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            startAnimation(alphaAnimation);
        }
    }

    /* access modifiers changed from: package-private */
    public final bi d() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public final l e() {
        return this.r;
    }
}
