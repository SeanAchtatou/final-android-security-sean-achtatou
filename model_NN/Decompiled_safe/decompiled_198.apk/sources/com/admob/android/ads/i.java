package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;

/* compiled from: AdView */
final class i implements Runnable {
    private WeakReference a;

    public i(AdView adView) {
        this.a = new WeakReference(adView);
    }

    public final void run() {
        AdView adView = (AdView) this.a.get();
        if (adView == null) {
            return;
        }
        if ((adView.b == null || adView.b.getParent() == null) && adView.k != null) {
            try {
                a unused = adView.k;
            } catch (Exception e) {
                Log.w("AdMobSDK", "Unhandled exception raised in your AdListener.onFailedToReceiveAd.", e);
            }
        } else {
            try {
                a unused2 = adView.k;
            } catch (Exception e2) {
                Log.w("AdMobSDK", "Unhandled exception raised in your AdListener.onFailedToReceiveRefreshedAd.", e2);
            }
        }
    }
}
