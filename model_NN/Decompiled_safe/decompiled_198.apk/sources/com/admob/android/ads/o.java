package com.admob.android.ads;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.Timer;

/* compiled from: InterstitialAd */
public final class o {
    private static Handler a = null;
    private static Timer b = null;
    private static q c = null;
    private p d;
    private WeakReference e;
    private boolean f;
    /* access modifiers changed from: private */
    public bg g;
    private String h;
    private String i;
    private s j;
    private long k;

    /* access modifiers changed from: package-private */
    public final void a() {
        if (a != null) {
            a.post(new t(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (b != null) {
            b.cancel();
            b = null;
        }
        if (this.k != -1 && s.a("AdMobSDK", 2)) {
            Log.v("AdMobSDK", "total request time: " + (SystemClock.uptimeMillis() - this.k));
        }
        this.f = true;
        c = null;
        if (((u) this.e.get()) != null) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (a != null) {
            a.post(new r(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        c = null;
        if (((u) this.e.get()) != null) {
        }
    }

    /* access modifiers changed from: package-private */
    public final p e() {
        return this.d;
    }

    public final String f() {
        return this.i;
    }

    public final String g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final s h() {
        return this.j;
    }
}
