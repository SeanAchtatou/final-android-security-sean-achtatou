package com.admob.android.ads;

import android.view.MotionEvent;
import android.view.View;
import java.lang.ref.WeakReference;

/* compiled from: AdMobVideoViewNative */
public final class ac implements View.OnTouchListener {
    private WeakReference a;

    public ac(y yVar) {
        this.a = new WeakReference(yVar);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        y yVar = (y) this.a.get();
        if (yVar == null) {
            return false;
        }
        yVar.b(false);
        y.a(yVar, motionEvent);
        return false;
    }
}
