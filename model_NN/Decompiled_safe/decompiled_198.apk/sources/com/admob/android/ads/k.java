package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;

/* compiled from: AdView */
final class k implements Runnable {
    private WeakReference a;
    private WeakReference b;
    private int c;
    private boolean d;

    public k(AdView adView, bl blVar, int i, boolean z) {
        this.a = new WeakReference(adView);
        this.b = new WeakReference(blVar);
        this.c = i;
        this.d = z;
    }

    public final void run() {
        try {
            AdView adView = (AdView) this.a.get();
            bl blVar = (bl) this.b.get();
            if (adView != null && blVar != null) {
                adView.addView(blVar);
                blVar.d();
                AdView.j(adView);
                if (this.c != 0) {
                    bl unused = adView.b = blVar;
                } else if (this.d) {
                    adView.a(blVar);
                } else {
                    AdView.b(adView, blVar);
                }
            }
        } catch (Exception e) {
            if (s.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "Unhandled exception placing AdContainer into AdView.", e);
            }
        }
    }
}
