package com.admob.android.ads;

import android.util.Log;

/* compiled from: AdMobOpener */
final class bw implements be {
    bw() {
    }

    public final void a(bb bbVar) {
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Click processed at " + bbVar.c());
        }
    }

    public final void a(bb bbVar, Exception exc) {
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Click processing failed at " + bbVar.c(), exc);
        }
    }
}
