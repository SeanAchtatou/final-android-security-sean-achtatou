package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: DeveloperNotice */
final class ay {
    private static boolean a = false;

    ay() {
    }

    public static void a(Context context) {
        byte[] a2;
        String string;
        if (!a) {
            a = true;
            if (b.b()) {
                try {
                    String a3 = aw.a(context);
                    bb a4 = bd.a("http://api.admob.com/v1/pubcode/android_sdk_emulator_notice" + "?" + a3, "developer_message", b.g(context));
                    if (a4.d() && (a2 = a4.a()) != null && (string = new JSONObject(new JSONTokener(new String(a2))).getString("data")) != null && !string.equals("")) {
                        Log.w("AdMobSDK", string);
                    }
                } catch (Exception e) {
                    if (s.a("AdMobSDK", 2)) {
                        Log.v("AdMobSDK", "Unhandled exception retrieving developer message.", e);
                    }
                }
            }
        }
    }
}
