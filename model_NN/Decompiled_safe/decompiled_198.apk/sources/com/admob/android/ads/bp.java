package com.admob.android.ads;

import android.view.View;
import java.util.Comparator;

/* compiled from: AdContainer */
final class bp implements Comparator {
    bp() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        float a = am.a((View) obj);
        float a2 = am.a((View) obj2);
        if (a < a2) {
            return -1;
        }
        return a > a2 ? 1 : 0;
    }
}
