package com.admob.android.ads;

import android.os.Bundle;

/* compiled from: MovieButton */
public final class bt implements bs {
    public String a;
    public String b;
    public String c;
    public bz d = new bz();
    public String e;
    public String f;

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("ad", this.a);
        bundle.putString("au", this.b);
        bundle.putString("t", this.c);
        bundle.putBundle("oi", g.a(this.d));
        bundle.putString("ap", this.e);
        bundle.putString("json", this.f);
        return bundle;
    }
}
