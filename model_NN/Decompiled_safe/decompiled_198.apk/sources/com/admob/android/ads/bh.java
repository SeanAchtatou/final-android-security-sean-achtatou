package com.admob.android.ads;

/* compiled from: Ad */
public enum bh {
    CLICK_TO_MAP("map"),
    CLICK_TO_VIDEO("video"),
    CLICK_TO_APP("app"),
    CLICK_TO_BROWSER("url"),
    CLICK_TO_CALL("call"),
    CLICK_TO_MUSIC("itunes"),
    CLICK_TO_CANVAS("canvas"),
    CLICK_TO_CONTACT("contact"),
    CLICK_TO_INTERACTIVE_VIDEO("movie"),
    CLICK_TO_FULLSCREEN_BROWSER("screen");
    
    private String k;

    private bh(String str) {
        this.k = str;
    }

    public final String toString() {
        return this.k;
    }

    public static bh a(String str) {
        for (bh bhVar : values()) {
            if (bhVar.toString().equals(str)) {
                return bhVar;
            }
        }
        return null;
    }
}
