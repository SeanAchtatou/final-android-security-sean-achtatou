package com.admob.android.ads;

import android.os.Bundle;

/* compiled from: ClickURL */
public final class ch implements bs {
    public String a;
    public boolean b;

    public ch() {
        this.a = null;
        this.b = false;
    }

    public ch(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("u", this.a);
        bundle.putBoolean("p", this.b);
        return bundle;
    }

    public final int hashCode() {
        if (this.a != null) {
            return this.a.hashCode();
        }
        return super.hashCode();
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (!(obj instanceof ch)) {
            return false;
        }
        ch chVar = (ch) obj;
        boolean z3 = this.a == null && chVar.a != null;
        if (this.a == null || this.a.equals(chVar.a)) {
            z = false;
        } else {
            z = true;
        }
        if (this.b != chVar.b) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z3 || z || z2) {
            return false;
        }
        return true;
    }
}
