package com.admob.android.ads;

import android.app.Activity;
import android.view.KeyEvent;
import com.admob.android.ads.a.a;
import java.lang.ref.WeakReference;

/* compiled from: AdMobCanvasView */
public final class cj extends a {
    String c;
    boolean d = true;
    private bv e;

    public cj(Activity activity, String str, bv bvVar) {
        super(activity, false, new WeakReference(activity));
        this.c = str;
        this.e = bvVar;
    }

    /* access modifiers changed from: protected */
    public final ai a(WeakReference weakReference) {
        return new ck(this, this, weakReference);
    }

    public final void b(String str) {
        this.a = str + "#sdk";
    }

    public final void a() {
        if (this.e != null) {
            this.e.a();
        }
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        a();
        return true;
    }
}
