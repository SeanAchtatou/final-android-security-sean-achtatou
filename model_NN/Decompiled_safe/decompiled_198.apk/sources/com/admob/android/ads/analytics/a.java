package com.admob.android.ads.analytics;

import android.util.Log;
import com.admob.android.ads.bb;
import com.admob.android.ads.be;
import com.admob.android.ads.s;

/* compiled from: InstallReceiver */
final class a implements be {
    a() {
    }

    public final void a(bb bbVar) {
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Recorded install from an AdMob ad.");
        }
    }

    public final void a(bb bbVar, Exception exc) {
        if (s.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Failed to record install from an AdMob ad.", exc);
        }
    }
}
