package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

/* compiled from: AdContainer */
public final class bn extends Thread {
    private JSONObject a;
    private WeakReference b;

    public bn(JSONObject jSONObject, bl blVar) {
        this.a = jSONObject;
        this.b = new WeakReference(blVar);
    }

    public final void run() {
        try {
            bl blVar = (bl) this.b.get();
            if (blVar != null && blVar.a != null) {
                blVar.a.a(this.a);
                if (blVar.b != null) {
                    blVar.b.performClick();
                }
            }
        } catch (Exception e) {
            if (s.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in AdClickThread.run(), ", e);
            }
        }
    }
}
