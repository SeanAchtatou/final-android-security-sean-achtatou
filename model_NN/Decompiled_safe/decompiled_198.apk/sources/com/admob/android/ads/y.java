package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/* compiled from: AdMobVideoViewNative */
public final class y extends x implements aj {
    ViewGroup d;
    VideoView e;
    az f;
    int g;
    boolean h;
    boolean i = false;
    boolean j = false;
    w k;
    private long l;
    private Button m;
    private Runnable n;
    private boolean o;
    private aa p;
    private WeakReference q;
    private MediaController r;

    static /* synthetic */ void a(y yVar, MotionEvent motionEvent) {
        Log.v("AdMobSDK", "fadeBars()");
        if (yVar.e() && yVar.k != null) {
            if (yVar.g == 2) {
                yVar.a.removeCallbacks(yVar.n);
                if (!yVar.k.b) {
                    yVar.k.b();
                }
                yVar.a.postDelayed(yVar.n, 3000);
            } else if (motionEvent.getAction() != 0) {
            } else {
                if (yVar.k.b) {
                    yVar.k.a();
                } else {
                    yVar.k.b();
                }
            }
        }
    }

    public y(Context context, WeakReference weakReference) {
        super(context);
        this.q = weakReference;
        this.n = new ab(this);
        this.h = false;
        this.i = false;
        this.j = false;
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        bu buVar = this.c.h;
        this.e = new VideoView(context);
        z zVar = new z(this);
        this.e.setOnPreparedListener(zVar);
        this.e.setOnCompletionListener(zVar);
        this.e.setVideoPath(buVar.a);
        this.e.setBackgroundDrawable(null);
        this.e.setOnErrorListener(zVar);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        addView(this.e, layoutParams);
        if (this.k != null) {
            this.k.b();
        }
    }

    public final void a() {
        if (this.h) {
            this.a.post(new ae(this));
            return;
        }
        af afVar = new af(this);
        long currentTimeMillis = System.currentTimeMillis() - this.l;
        long j2 = (long) ((int) (this.c.h.g * 1000.0d));
        if (j2 > currentTimeMillis) {
            this.a.postDelayed(afVar, j2 - currentTimeMillis);
        } else {
            this.a.post(afVar);
        }
    }

    public static void a(View view) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(1000);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
        view.invalidate();
    }

    public static void b(View view) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1000);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
        view.invalidate();
    }

    public final void b() {
        if (this.d != null) {
            b(this.d);
        }
        if (this.m != null) {
            b(this.m);
        }
        if (this.k != null && !this.k.b) {
            this.k.b();
        }
        if (this.k != null) {
            w wVar = this.k;
            if (wVar.a != null) {
                b(wVar.a);
            }
        }
        invalidate();
        if (this.g == 2 && this.k != null && this.k.b) {
            this.a.postDelayed(this.n, 3000);
        }
        this.a.postDelayed(new ae(this), 1000);
    }

    public final void a(boolean z) {
        this.a.removeCallbacks(this.n);
        if (this.d == null) {
            h();
        }
        if (this.d != null) {
            a(this.d);
        }
        if (this.k != null) {
            w wVar = this.k;
            Context context = getContext();
            bz bzVar = this.c;
            float f2 = this.b;
            if (wVar.a == null) {
                RelativeLayout relativeLayout = new RelativeLayout(context);
                Button button = new Button(context);
                button.setTextColor(-1);
                button.setOnClickListener(new ag(this));
                BitmapDrawable bitmapDrawable = new BitmapDrawable((Bitmap) bzVar.b().get(bzVar.h.l));
                bitmapDrawable.setBounds(0, 0, (int) (134.0f * f2), (int) (134.0f * f2));
                button.setWidth((int) (134.0f * f2));
                button.setHeight(134);
                button.setBackgroundDrawable(bitmapDrawable);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (134.0f * f2), (int) (134.0f * f2));
                layoutParams.addRule(13);
                relativeLayout.addView(button, layoutParams);
                relativeLayout.setOnClickListener(new ag(this));
                TextView textView = new TextView(context);
                textView.setTextColor(-1);
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                textView.setText("Replay");
                textView.setPadding(0, 0, 0, (int) (14.0f * f2));
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(12);
                layoutParams2.addRule(14);
                relativeLayout.addView(textView, layoutParams2);
                wVar.a = new ci(context, relativeLayout, 134, 134, (Bitmap) bzVar.b().get(bzVar.h.k));
                wVar.a.setOnClickListener(new ag(this));
                wVar.a.setVisibility(4);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams((int) (134.0f * f2), (int) (134.0f * f2));
                layoutParams3.addRule(13);
                addView(wVar.a, layoutParams3);
            }
            if (z) {
                w wVar2 = this.k;
                if (wVar2.a != null) {
                    wVar2.a.bringToFront();
                    a(wVar2.a);
                }
            }
            if (!this.k.b) {
                this.k.b();
            }
        }
        if (this.o && this.p == null) {
            this.p = new aa(this);
            this.a.postDelayed(this.p, 7500);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        this.o = z;
        if (!z) {
            g();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.p != null) {
            this.a.removeCallbacks(this.p);
            this.p = null;
        }
    }

    public final void c() {
        f();
        HashMap hashMap = null;
        if (this.i) {
            hashMap = new HashMap();
            hashMap.put("event", "completed");
        }
        this.f.a("done", hashMap);
        d();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        Activity activity;
        if (this.q != null && (activity = (Activity) this.q.get()) != null) {
            activity.finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        this.p = null;
        if (this.c != null) {
            b(this.c.l);
            bu buVar = this.c.h;
            if (buVar != null) {
                Context context = getContext();
                if (b.i(context) == "l") {
                    this.g = 2;
                } else {
                    this.g = 1;
                }
                this.f = new az(this.c.j, b.d(context), this.c.i, b.g(context));
                this.f.a("video", (Map) null);
                a(context);
                String a = cd.a(this.c.l ? "Skip" : "Done");
                if (buVar.c()) {
                    h();
                    if (this.d != null) {
                        a(this.d);
                    }
                    if (!buVar.j || !buVar.c()) {
                        this.m = new Button(context);
                        this.m.setOnClickListener(new ah(this, true));
                        this.m.setBackgroundResource(17301509);
                        this.m.setTextSize(13.0f);
                        this.m.setText(a);
                        this.m.setVisibility(4);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (54.0f * this.b), (int) (36.0f * this.b));
                        layoutParams.addRule(11);
                        layoutParams.addRule(12);
                        layoutParams.setMargins(0, 0, (int) (2.0f * this.b), (int) (8.0f * this.b));
                        addView(this.m, layoutParams);
                        a(this.m);
                    }
                }
                if (buVar.c != 2 || buVar.m == null || buVar.m.size() <= 0) {
                    boolean z = buVar.c == 0;
                    Activity activity = (Activity) this.q.get();
                    if (activity != null && this.e != null) {
                        this.r = new MediaController(activity, z);
                        this.r.setAnchorView(this.e);
                        this.e.setMediaController(this.r);
                        return;
                    }
                    return;
                }
                this.k = new w();
                this.k.a(context, a, buVar, this.b, this, this.c, this.q);
            } else if (s.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "movieInfo is null");
            }
        } else if (s.a("AdMobSDK", 6)) {
            Log.e("AdMobSDK", "openerInfo is null");
        }
    }

    public final void a(Configuration configuration) {
        this.g = configuration.orientation;
        if (this.k == null || !e()) {
            this.a.removeCallbacks(this.n);
        } else if (this.g == 2 && this.k.b) {
            this.k.a();
        } else if (!this.k.b && this.g == 1) {
            this.k.b();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return this.e != null && this.e.isPlaying();
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        if (this.e != null) {
            this.e.stopPlayback();
            this.e.setVisibility(4);
            removeView(this.e);
            this.e = null;
        }
    }

    private void h() {
        Bitmap bitmap;
        if (this.c.h.c()) {
            Context context = getContext();
            this.d = new RelativeLayout(context);
            ImageView imageView = new ImageView(context);
            Hashtable b = this.c.b();
            if (!(b == null || (bitmap = (Bitmap) b.get(this.c.h.f)) == null)) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
                float f2 = getResources().getDisplayMetrics().density;
                imageView.setImageDrawable(bitmapDrawable);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(bg.a(bitmap.getWidth(), (double) f2), bg.a(bitmap.getHeight(), (double) f2));
                layoutParams.addRule(13);
                this.d.addView(imageView, layoutParams);
                this.d.setBackgroundColor(0);
                this.d.setVisibility(4);
                addView(this.d, new RelativeLayout.LayoutParams(-1, -1));
            }
            this.l = System.currentTimeMillis();
        }
    }
}
