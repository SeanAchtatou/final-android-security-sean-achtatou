package com.admob.android.ads.a;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.admob.android.ads.ai;
import com.admob.android.ads.bc;
import com.admob.android.ads.bg;
import com.admob.android.ads.s;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: AdMobWebView */
public class a extends WebView implements View.OnClickListener {
    public String a;
    protected ai b;
    private boolean c;
    private WeakReference d;

    public static View a(Context context, String str, boolean z, Point point, float f, WeakReference weakReference) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setGravity(17);
        a aVar = new a(context, z, weakReference);
        aVar.setBackgroundColor(0);
        relativeLayout.addView(aVar, new RelativeLayout.LayoutParams(-1, -1));
        if (z) {
            ImageButton imageButton = new ImageButton(context);
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundDrawable(null);
            imageButton.setPadding(0, 0, 0, 0);
            imageButton.setOnClickListener(aVar);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(bg.a(point.x, (double) f), bg.a(point.y, (double) f), 0, 0);
            relativeLayout.addView(imageButton, layoutParams);
        }
        aVar.a = str;
        aVar.loadUrl(str);
        return relativeLayout;
    }

    public a(Context context, boolean z, WeakReference weakReference) {
        super(context);
        this.c = z;
        this.d = weakReference;
        WebSettings settings = getSettings();
        settings.setLoadsImagesAutomatically(true);
        settings.setPluginsEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        settings.setUserAgentString(bc.i());
        this.b = a(weakReference);
        setWebViewClient(this.b);
    }

    public void loadUrl(String str) {
        String str2;
        if (this.c) {
            str2 = str + "#sdk_close";
        } else {
            str2 = str;
        }
        super.loadUrl(str2);
    }

    public final void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public ai a(WeakReference weakReference) {
        return new ai(this, weakReference);
    }

    public void onClick(View view) {
        a();
    }

    public void a() {
        Activity activity;
        if (this.d != null && (activity = (Activity) this.d.get()) != null) {
            activity.finish();
        }
    }

    public final void a(String str, Object... objArr) {
        String str2 = "";
        Iterator it = Arrays.asList(objArr).iterator();
        while (it.hasNext()) {
            str2 = str2.concat(a(it.next()));
            if (it.hasNext()) {
                str2 = str2.concat(",");
            }
        }
        String str3 = "javascript:admob.".concat(str) + "(" + str2 + ");";
        if (s.a("AdMobSDK", 3)) {
            Log.w("AdMobSDK", "Sending url to webView: " + str3);
        }
    }

    private String a(Object obj) {
        if (obj == null) {
            return "{}";
        }
        if ((obj instanceof Integer) || (obj instanceof Double)) {
            return obj.toString();
        }
        if (obj instanceof String) {
            return "'" + ((String) obj) + "'";
        } else if (obj instanceof Map) {
            String str = "{";
            Iterator it = ((Map) obj).entrySet().iterator();
            while (true) {
                String str2 = str;
                if (!it.hasNext()) {
                    return str2.concat("}");
                }
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                Object value = entry.getValue();
                String a2 = a(key);
                str = str2.concat(a2 + ":" + a(value));
                if (it.hasNext()) {
                    str = str.concat(",");
                }
            }
        } else if (obj instanceof JSONObject) {
            return ((JSONObject) obj).toString();
        } else {
            if (s.a("AdMobSDK", 5)) {
                Log.w("AdMobSDK", "Unable to create JSON from object: " + obj);
            }
            return "";
        }
    }
}
