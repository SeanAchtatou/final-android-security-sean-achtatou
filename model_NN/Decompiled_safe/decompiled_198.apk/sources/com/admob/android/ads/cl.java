package com.admob.android.ads;

import android.content.Context;
import com.admob.android.ads.a.a;
import org.json.JSONObject;

/* compiled from: AdMobFlexWebView */
public final class cl extends a {
    public JSONObject c = null;
    public JSONObject d = null;
    private boolean e = false;

    public cl(Context context, bg bgVar) {
        super(context, false, null);
        this.b = new cm(this, bgVar);
        setWebViewClient(this.b);
    }

    public final void b() {
        if ((this.b instanceof cm) && ((cm) this.b).c && !this.e) {
            this.e = true;
            a("init", this.c == null ? "null" : this.c, this.d == null ? "null" : this.d);
        }
    }
}
