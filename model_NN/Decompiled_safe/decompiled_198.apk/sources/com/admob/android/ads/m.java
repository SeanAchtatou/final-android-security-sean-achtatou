package com.admob.android.ads;

import android.util.Log;
import android.view.animation.DecelerateInterpolator;
import java.lang.ref.WeakReference;

/* compiled from: AdView */
final class m implements Runnable {
    private WeakReference a;
    private WeakReference b;

    public m(bl blVar, AdView adView) {
        this.b = new WeakReference(blVar);
        this.a = new WeakReference(adView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.as.<init>(float, float, float, float, float, boolean):void
     arg types: [int, int, float, float, float, int]
     candidates:
      com.admob.android.ads.as.<init>(float[], float[], float, float, float, boolean):void
      com.admob.android.ads.as.<init>(float, float, float, float, float, boolean):void */
    public final void run() {
        try {
            AdView adView = (AdView) this.a.get();
            bl blVar = (bl) this.b.get();
            if (adView != null && blVar != null) {
                bl a2 = adView.b;
                if (a2 != null) {
                    a2.setVisibility(8);
                }
                blVar.setVisibility(0);
                as asVar = new as(90.0f, 0.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), false);
                asVar.setDuration(700);
                asVar.setFillAfter(true);
                asVar.setInterpolator(new DecelerateInterpolator());
                asVar.setAnimationListener(new n(a2, adView, blVar));
                adView.startAnimation(asVar);
            }
        } catch (Exception e) {
            if (s.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in SwapViews.run(), " + e.getMessage());
            }
        }
    }
}
