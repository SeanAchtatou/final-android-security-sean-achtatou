package com.admob.android.ads;

import java.lang.ref.WeakReference;

/* compiled from: AdContainer */
final class bo implements Runnable {
    private WeakReference a;

    public bo(bl blVar) {
        this.a = new WeakReference(blVar);
    }

    public final void run() {
        try {
            bl blVar = (bl) this.a.get();
            if (blVar != null) {
                blVar.g();
            }
        } catch (Exception e) {
        }
    }
}
