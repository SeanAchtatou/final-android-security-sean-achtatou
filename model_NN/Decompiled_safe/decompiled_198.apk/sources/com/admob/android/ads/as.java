package com.admob.android.ads;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: Rotate3dAnimation */
public final class as extends Animation {
    private final float[] a;
    private final float[] b;
    private final float c;
    private final float d;
    private final float e;
    private final boolean f;
    private Camera g;

    public as(float f2, float f3, float f4, float f5, float f6, boolean z) {
        this(new float[]{0.0f, f2, 0.0f}, new float[]{0.0f, f3, 0.0f}, f4, f5, f6, z);
    }

    public as(float[] fArr, float[] fArr2, float f2, float f3, float f4, boolean z) {
        this.a = fArr;
        this.b = fArr2;
        this.c = f2;
        this.d = f3;
        this.e = f4;
        this.f = z;
    }

    public final void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.g = new Camera();
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f2, Transformation transformation) {
        if (((double) f2) < 0.0d || ((double) f2) > 1.0d) {
            transformation.setTransformationType(Transformation.TYPE_IDENTITY);
            return;
        }
        float[] fArr = this.a;
        float[] fArr2 = this.b;
        float[] fArr3 = new float[3];
        for (int i = 0; i < 3; i++) {
            fArr3[i] = fArr[i] + ((fArr2[i] - fArr[i]) * f2);
        }
        float f3 = this.c;
        float f4 = this.d;
        Camera camera = this.g;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.f) {
            camera.translate(0.0f, 0.0f, this.e * f2);
        } else {
            camera.translate(0.0f, 0.0f, this.e * (1.0f - f2));
        }
        camera.rotateX(fArr3[0]);
        camera.rotateY(fArr3[1]);
        camera.rotateZ(fArr3[2]);
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f3, -f4);
        matrix.postTranslate(f3, f4);
        transformation.setTransformationType(Transformation.TYPE_MATRIX);
    }
}
