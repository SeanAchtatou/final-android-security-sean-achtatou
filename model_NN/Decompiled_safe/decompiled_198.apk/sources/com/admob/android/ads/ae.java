package com.admob.android.ads;

import java.lang.ref.WeakReference;

/* compiled from: AdMobVideoViewNative */
final class ae implements Runnable {
    private WeakReference a;

    public ae(y yVar) {
        this.a = new WeakReference(yVar);
    }

    public final void run() {
        y yVar = (y) this.a.get();
        if (yVar != null && yVar.e != null) {
            yVar.e.setVisibility(0);
            yVar.e.requestLayout();
            yVar.e.requestFocus();
            yVar.e.start();
        }
    }
}
