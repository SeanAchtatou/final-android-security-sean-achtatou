package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AdMobVideoViewNative */
public final class ad implements View.OnClickListener {
    private WeakReference a;
    private WeakReference b;
    private WeakReference c;

    public ad(y yVar, bt btVar, WeakReference weakReference) {
        this.a = new WeakReference(yVar);
        this.b = new WeakReference(btVar);
        this.c = weakReference;
    }

    public final void onClick(View view) {
        HashMap hashMap;
        Activity activity;
        y yVar = (y) this.a.get();
        if (yVar != null) {
            yVar.b(false);
            bt btVar = (bt) this.b.get();
            if (btVar != null) {
                Context context = yVar.getContext();
                if (!yVar.j) {
                    yVar.j = true;
                    hashMap = new HashMap();
                    hashMap.put("event", "interaction");
                } else {
                    hashMap = null;
                }
                yVar.f.a(btVar.e, hashMap);
                boolean e = yVar.e();
                if (e) {
                    yVar.f();
                }
                yVar.a(e);
                bv bvVar = new bv();
                try {
                    bvVar.a(context, new JSONObject(btVar.f), (ce) null);
                } catch (JSONException e2) {
                    if (s.a("AdMobSDK", 6)) {
                        Log.e("AdMobSDK", "Could not create JSONObject from button click", e2);
                    }
                }
                bvVar.b();
                if (this.c != null && (activity = (Activity) this.c.get()) != null) {
                    bvVar.a(activity, yVar);
                }
            }
        }
    }
}
