package com.admob.android.ads;

/* compiled from: AdView */
final class l {
    public static final l a = new l(320, 48);
    private int b;
    private int c;

    private l(int i, int i2) {
        this.b = i;
        this.c = i2;
    }

    public final String toString() {
        return String.valueOf(this.b) + "x" + String.valueOf(this.c);
    }

    static {
        new l(320, 270);
        new l(748, 110);
        new l(488, 80);
    }
}
