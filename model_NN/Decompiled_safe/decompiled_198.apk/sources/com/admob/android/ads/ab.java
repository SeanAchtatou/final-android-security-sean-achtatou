package com.admob.android.ads;

import java.lang.ref.WeakReference;

/* compiled from: AdMobVideoViewNative */
final class ab implements Runnable {
    private WeakReference a;

    public ab(y yVar) {
        this.a = new WeakReference(yVar);
    }

    public final void run() {
        y yVar = (y) this.a.get();
        if (yVar != null && yVar.e() && yVar.g == 2 && yVar.k != null) {
            yVar.k.a();
        }
    }
}
