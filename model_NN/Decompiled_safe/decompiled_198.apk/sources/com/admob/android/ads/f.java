package com.admob.android.ads;

import android.view.animation.Animation;

/* compiled from: AdView */
final class f implements Animation.AnimationListener {
    private /* synthetic */ bl a;
    private /* synthetic */ AdView b;

    f(AdView adView, bl blVar) {
        this.b = adView;
        this.a = blVar;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        this.b.post(new m(this.a, this.b));
    }

    public final void onAnimationRepeat(Animation animation) {
    }
}
