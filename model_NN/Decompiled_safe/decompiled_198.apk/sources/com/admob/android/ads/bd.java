package com.admob.android.ads;

import org.json.JSONObject;

/* compiled from: AdMobConnectorFactory */
public final class bd {
    private static boolean a = false;

    public static bb a(String str, String str2, String str3, be beVar, int i, String str4) {
        return new bf(str, str2, str3, beVar, i, str4);
    }

    public static bb a(String str, String str2, String str3, JSONObject jSONObject, be beVar) {
        bb a2 = a(str, str2, str3, beVar, 5000, jSONObject == null ? null : jSONObject.toString());
        a2.a("application/json");
        return a2;
    }

    public static bb a(String str, String str2, String str3, be beVar) {
        return a(str, str2, str3, beVar, 5000, null);
    }

    public static bb a(String str, String str2, be beVar) {
        bb a2 = a(str, null, str2, beVar, 5000, null);
        if (a2 != null) {
            a2.g();
        }
        return a2;
    }

    public static bb a(String str, String str2, String str3) {
        return a(str, str2, str3, null);
    }
}
