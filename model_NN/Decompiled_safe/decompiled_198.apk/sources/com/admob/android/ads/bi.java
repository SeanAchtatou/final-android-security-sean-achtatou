package com.admob.android.ads;

/* compiled from: Ad */
public enum bi {
    VIEW("view"),
    INTERSTITIAL("full_screen"),
    BAR("bar");
    
    private String d;

    private bi(String str) {
        this.d = str;
    }

    public final String toString() {
        return this.d;
    }
}
