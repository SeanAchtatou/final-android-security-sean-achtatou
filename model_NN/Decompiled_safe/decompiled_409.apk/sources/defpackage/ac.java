package defpackage;

import java.util.LinkedList;

/* renamed from: ac  reason: default package */
final class ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final c f41a;
    private final LinkedList b;
    private final int c;
    private /* synthetic */ f d;

    public ac(f fVar, c cVar, LinkedList linkedList, int i) {
        this.d = fVar;
        this.f41a = cVar;
        this.b = linkedList;
        this.c = i;
    }

    public final void run() {
        this.f41a.a(this.b);
        this.f41a.a(this.c);
        this.f41a.p();
    }
}
