package defpackage;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.AdSize;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;

/* renamed from: b  reason: default package */
public final class b extends WebView {

    /* renamed from: a  reason: collision with root package name */
    private AdActivity f42a;
    private AdSize b;

    public b(Context context, AdSize adSize) {
        super(context);
        this.b = adSize;
        setBackgroundColor(0);
        AdUtil.a(this);
        getSettings().setJavaScriptEnabled(true);
        setScrollBarStyle(0);
    }

    public final void a() {
        if (this.f42a != null) {
            this.f42a.finish();
        }
    }

    public final void a(AdActivity adActivity) {
        this.f42a = adActivity;
    }

    public final AdActivity b() {
        return this.f42a;
    }

    public final void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        try {
            super.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } catch (Exception e) {
            a.a("An error occurred while loading data in AdWebView:", e);
        }
    }

    public final void loadUrl(String str) {
        try {
            super.loadUrl(str);
        } catch (Exception e) {
            a.a("An error occurred while loading a URL in AdWebView:", e);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        if (isInEditMode()) {
            super.onMeasure(i, i2);
        } else if (this.b == null) {
            super.onMeasure(i, i2);
        } else {
            int mode = View.MeasureSpec.getMode(i);
            int size = View.MeasureSpec.getSize(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int size2 = View.MeasureSpec.getSize(i2);
            float f = getContext().getResources().getDisplayMetrics().density;
            int a2 = (int) (((float) this.b.a()) * f);
            int b2 = (int) (((float) this.b.b()) * f);
            if (mode == 0 || mode2 == 0) {
                super.onMeasure(i, i2);
            } else if (((float) a2) - (6.0f * f) > ((float) size) || b2 > size2) {
                a.e("Not enough space to show ad! Wants: <" + a2 + ", " + b2 + ">, Has: <" + size + ", " + size2 + ">");
                setVisibility(8);
                setMeasuredDimension(0, 0);
            } else {
                super.onMeasure(i, i2);
            }
        }
    }
}
