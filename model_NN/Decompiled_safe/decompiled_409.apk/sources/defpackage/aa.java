package defpackage;

import android.webkit.WebView;

/* renamed from: aa  reason: default package */
final class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final String f39a;
    private final String b;
    private final WebView c;
    private /* synthetic */ f d;

    public aa(f fVar, WebView webView, String str, String str2) {
        this.d = fVar;
        this.c = webView;
        this.f39a = str;
        this.b = str2;
    }

    public final void run() {
        this.c.loadDataWithBaseURL(this.f39a, this.b, "text/html", "utf-8", null);
    }
}
