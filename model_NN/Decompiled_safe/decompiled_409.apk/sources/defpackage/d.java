package defpackage;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

/* renamed from: d  reason: default package */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private String f173a;
    private HashMap b;

    public d(Bundle bundle) {
        this.f173a = bundle.getString("action");
        Serializable serializable = bundle.getSerializable("params");
        this.b = serializable instanceof HashMap ? (HashMap) serializable : null;
    }

    private d(String str) {
        this.f173a = str;
    }

    public d(String str, HashMap hashMap) {
        this(str);
        this.b = hashMap;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("action", this.f173a);
        bundle.putSerializable("params", this.b);
        return bundle;
    }

    public final String b() {
        return this.f173a;
    }

    public final HashMap c() {
        return this.b;
    }
}
