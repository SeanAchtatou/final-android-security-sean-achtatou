package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.Ad;
import com.google.ads.AdActivity;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: c  reason: default package */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f43a = new Object();
    private WeakReference b;
    private Ad c;
    private AdListener d = null;
    private f e = null;
    private AdRequest f = null;
    private AdSize g;
    private a h = new a();
    private String i;
    private b j;
    private n k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private x r;
    private boolean s = false;
    private LinkedList t;
    private LinkedList u;
    private int v;

    public c(Activity activity, Ad ad, AdSize adSize, String str) {
        this.b = new WeakReference(activity);
        this.c = ad;
        this.g = adSize;
        this.i = str;
        synchronized (f43a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            this.m = 60000;
        }
        this.r = new x(this);
        this.t = new LinkedList();
        this.u = new LinkedList();
        a();
        AdUtil.g(activity.getApplicationContext());
    }

    private synchronized void A() {
        if (this.e != null) {
            this.e.a();
            this.e = null;
        }
        this.j.stopLoading();
    }

    private synchronized boolean w() {
        return this.e != null;
    }

    private synchronized void x() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            a.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new p((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized void y() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            a.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new p((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized void z() {
        this.d = null;
    }

    public final synchronized void a() {
        Activity e2 = e();
        if (e2 == null) {
            a.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new b(e2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new n(this, g.b, true, false);
            } else {
                this.k = new n(this, g.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final void a(long j2) {
        synchronized (f43a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.e = null;
        if (this.c instanceof InterstitialAd) {
            if (errorCode == AdRequest.ErrorCode.NO_FILL) {
                this.h.n();
            } else if (errorCode == AdRequest.ErrorCode.NETWORK_ERROR) {
                this.h.l();
            }
        }
        a.c("onFailedToReceiveAd(" + errorCode + ")");
    }

    public final synchronized void a(AdRequest adRequest) {
        if (w()) {
            a.e("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.c()) {
            a.e("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity e2 = e();
            if (e2 == null) {
                a.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(e2.getApplicationContext()) && AdUtil.b(e2.getApplicationContext())) {
                this.n = false;
                this.t.clear();
                this.f = adRequest;
                this.e = new f(this);
                this.e.a(adRequest);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        a.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList linkedList) {
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            a.a("Adding a click tracking URL: " + ((String) it.next()));
        }
        this.u = linkedList;
    }

    public final synchronized void b() {
        z();
        A();
        this.j.destroy();
    }

    public final synchronized void c() {
        if (this.o) {
            a.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            a.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void d() {
        if (!(this.c instanceof AdView)) {
            a.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            a.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            a.a("Refreshing is already enabled.");
        }
    }

    public final Activity e() {
        return (Activity) this.b.get();
    }

    public final Ad f() {
        return this.c;
    }

    public final synchronized f g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.i;
    }

    public final synchronized b i() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized n j() {
        return this.k;
    }

    public final AdSize k() {
        return this.g;
    }

    public final a l() {
        return this.h;
    }

    public final synchronized int m() {
        return this.v;
    }

    public final long n() {
        return this.m;
    }

    public final synchronized boolean o() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void p() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            x();
        }
        a.c("onReceiveAd()");
    }

    public final synchronized void q() {
        this.h.o();
        a.c("onDismissScreen()");
    }

    public final synchronized void r() {
        a.c("onPresentScreen()");
    }

    public final synchronized void s() {
        a.c("onLeaveApplication()");
    }

    public final void t() {
        this.h.b();
        y();
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean u() {
        return !this.u.isEmpty();
    }

    public final synchronized void v() {
        if (this.f == null) {
            a.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.d()) {
                a.a("Not refreshing because the ad is not visible.");
            } else {
                a.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            a.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }
}
