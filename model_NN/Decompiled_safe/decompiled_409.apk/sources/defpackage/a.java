package defpackage;

import android.os.SystemClock;
import java.util.LinkedList;

/* renamed from: a  reason: default package */
public final class a {
    private static long e = 0;

    /* renamed from: a  reason: collision with root package name */
    private LinkedList f0a = new LinkedList();
    private long b;
    private long c;
    private LinkedList d = new LinkedList();
    private String f;
    private boolean g = false;
    private boolean h = false;
    private String i;

    a() {
        a();
    }

    static long i() {
        return e;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.f0a.clear();
        this.b = 0;
        this.c = 0;
        this.d.clear();
        this.f = null;
        this.g = false;
        this.h = false;
    }

    public final void a(String str) {
        com.google.ads.util.a.d("Prior ad identifier = " + str);
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        com.google.ads.util.a.d("Ad clicked.");
        this.f0a.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }

    public final void b(String str) {
        com.google.ads.util.a.d("Prior impression ticket = " + str);
        this.i = str;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        com.google.ads.util.a.d("Ad request loaded.");
        this.b = SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        com.google.ads.util.a.d("Ad request started.");
        this.c = SystemClock.elapsedRealtime();
        e++;
    }

    /* access modifiers changed from: package-private */
    public final long e() {
        if (this.f0a.size() != this.d.size()) {
            return -1;
        }
        return (long) this.f0a.size();
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        if (this.f0a.isEmpty() || this.f0a.size() != this.d.size()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.f0a.size()) {
                return sb.toString();
            }
            if (i3 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(((Long) this.d.get(i3)).longValue() - ((Long) this.f0a.get(i3)).longValue()));
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        if (this.f0a.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.f0a.size()) {
                return sb.toString();
            }
            if (i3 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(((Long) this.f0a.get(i3)).longValue() - this.b));
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final long h() {
        return this.b - this.c;
    }

    /* access modifiers changed from: package-private */
    public final String j() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final boolean k() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        com.google.ads.util.a.d("Interstitial network error.");
        this.g = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean m() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final void n() {
        com.google.ads.util.a.d("Interstitial no fill.");
        this.h = true;
    }

    public final void o() {
        com.google.ads.util.a.d("Landing page dismissed.");
        this.d.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }

    /* access modifiers changed from: package-private */
    public final String p() {
        return this.i;
    }
}
