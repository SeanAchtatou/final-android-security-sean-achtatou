package defpackage;

import android.webkit.WebView;
import com.google.ads.AdRequest;

/* renamed from: y  reason: default package */
final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final c f195a;
    private final WebView b;
    private final e c;
    private final AdRequest.ErrorCode d;
    private final boolean e;
    private /* synthetic */ f f;

    public y(f fVar, c cVar, WebView webView, e eVar, AdRequest.ErrorCode errorCode, boolean z) {
        this.f = fVar;
        this.f195a = cVar;
        this.b = webView;
        this.c = eVar;
        this.d = errorCode;
        this.e = z;
    }

    public final void run() {
        this.b.stopLoading();
        this.b.destroy();
        this.c.a();
        if (this.e) {
            b i = this.f195a.i();
            i.stopLoading();
            i.setVisibility(8);
        }
        this.f195a.a(this.d);
    }
}
