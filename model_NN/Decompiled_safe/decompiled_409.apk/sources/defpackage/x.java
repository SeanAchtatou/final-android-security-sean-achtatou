package defpackage;

import com.google.ads.util.a;
import java.lang.ref.WeakReference;

/* renamed from: x  reason: default package */
public final class x implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f194a;

    public x(c cVar) {
        this.f194a = new WeakReference(cVar);
    }

    public final void run() {
        c cVar = (c) this.f194a.get();
        if (cVar == null) {
            a.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            cVar.v();
        }
    }
}
