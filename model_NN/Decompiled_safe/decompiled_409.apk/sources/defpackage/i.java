package defpackage;

import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.util.a;
import java.util.HashMap;

/* renamed from: i  reason: default package */
public final class i implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        a.e("Invalid " + ((String) hashMap.get("type")) + " request error: " + ((String) hashMap.get("errors")));
        f g = cVar.g();
        if (g != null) {
            g.a(AdRequest.ErrorCode.INVALID_REQUEST);
        }
    }
}
