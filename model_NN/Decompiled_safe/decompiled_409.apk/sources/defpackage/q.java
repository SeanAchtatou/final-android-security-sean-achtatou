package defpackage;

import android.app.Activity;
import android.webkit.WebView;
import android.widget.VideoView;
import com.google.ads.AdActivity;
import com.google.ads.util.a;
import java.util.HashMap;

/* renamed from: q  reason: default package */
public final class q implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("action");
        if (webView instanceof b) {
            AdActivity b = ((b) webView).b();
            if (b == null) {
                a.a("Could not get adActivity to create the video.");
            } else if (str.equals("load")) {
                String str2 = (String) hashMap.get("url");
                Activity e = cVar.e();
                if (e == null) {
                    a.e("activity was null while loading a video.");
                    return;
                }
                VideoView videoView = new VideoView(e);
                videoView.setVideoPath(str2);
                b.a(videoView);
            } else {
                VideoView a2 = b.a();
                if (a2 == null) {
                    a.e("Could not get the VideoView for a video GMSG.");
                } else if (str.equals("play")) {
                    a2.setVisibility(0);
                    a2.start();
                    a.d("Video is now playing.");
                    g.a(webView, "onVideoEvent", "{'event': 'playing'}");
                } else if (str.equals("pause")) {
                    a2.pause();
                } else if (str.equals("stop")) {
                    a2.stopPlayback();
                } else if (str.equals("remove")) {
                    a2.setVisibility(8);
                } else if (str.equals("replay")) {
                    a2.setVisibility(0);
                    a2.seekTo(0);
                    a2.start();
                } else if (str.equals("currentTime")) {
                    String str3 = (String) hashMap.get("time");
                    if (str3 == null) {
                        a.e("No \"time\" parameter!");
                    } else {
                        a2.seekTo((int) (Float.valueOf(str3).floatValue() * 1000.0f));
                    }
                } else if (!str.equals("position")) {
                    a.e("Unknown movie action: " + str);
                }
            }
        } else {
            a.a("Could not get adWebView to create the video.");
        }
    }
}
