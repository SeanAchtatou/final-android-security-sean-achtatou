package com.openfeint.internal.request.multipart;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class FilePartSource implements PartSource {

    /* renamed from: a  reason: collision with root package name */
    private File f114a;
    private String b;

    public FilePartSource(File file) {
        this.f114a = null;
        this.b = null;
        this.f114a = file;
        if (file == null) {
            return;
        }
        if (!file.isFile()) {
            throw new FileNotFoundException("File is not a normal file.");
        } else if (!file.canRead()) {
            throw new FileNotFoundException("File is not readable.");
        } else {
            this.b = file.getName();
        }
    }

    public FilePartSource(String str, File file) {
        this(file);
        if (str != null) {
            this.b = str;
        }
    }

    public InputStream createInputStream() {
        return this.f114a != null ? new FileInputStream(this.f114a) : new ByteArrayInputStream(new byte[0]);
    }

    public String getFileName() {
        return this.b == null ? "noname" : this.b;
    }

    public long getLength() {
        if (this.f114a != null) {
            return this.f114a.length();
        }
        return 0;
    }
}
