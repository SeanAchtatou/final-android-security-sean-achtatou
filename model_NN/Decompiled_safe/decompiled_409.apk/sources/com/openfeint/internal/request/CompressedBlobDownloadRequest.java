package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import hamon05.speed.pac.R;
import java.io.IOException;

public abstract class CompressedBlobDownloadRequest extends DownloadRequest {
    /* access modifiers changed from: protected */
    public final void onSuccess(byte[] bArr) {
        try {
            onSuccessDecompress(Compression.decompress(bArr));
        } catch (IOException e) {
            onFailure(OpenFeintInternal.getRString(R.string.of_io_exception_on_download));
        }
    }

    /* access modifiers changed from: protected */
    public abstract void onSuccessDecompress(byte[] bArr);
}
