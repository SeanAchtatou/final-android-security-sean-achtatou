package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;

public class CacheFileRequest extends CacheRequest {
    private String b;
    private String c;

    public CacheFileRequest(String str, String str2, String str3) {
        super(str3);
        this.b = str;
        this.c = str2;
    }

    public void onResponse(int i, byte[] bArr) {
        if (i == 200) {
            try {
                Util.saveFile(bArr, this.b);
                super.on200Response();
            } catch (Exception e) {
                OpenFeintInternal.log("CacheFile", e.toString());
            }
        }
    }

    public String path() {
        return this.c;
    }
}
