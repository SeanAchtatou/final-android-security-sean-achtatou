package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.resource.ServerException;
import hamon05.speed.pac.R;
import org.apache.http.client.methods.HttpUriRequest;

public abstract class JSONContentRequest extends BaseRequest {
    public JSONContentRequest() {
    }

    public JSONContentRequest(OrderedArgList orderedArgList) {
        super(orderedArgList);
    }

    protected static ServerException notJSONError(int i) {
        return new ServerException("ServerError", String.format(OpenFeintInternal.getRString(R.string.of_server_error_code_format), Integer.valueOf(i)));
    }

    /* access modifiers changed from: protected */
    public HttpUriRequest generateRequest() {
        HttpUriRequest generateRequest = super.generateRequest();
        generateRequest.addHeader("Accept", "application/json");
        return generateRequest;
    }

    /* access modifiers changed from: protected */
    public boolean isResponseJSON() {
        String responseType = getResponseType();
        return responseType != null && responseType.startsWith("application/json;");
    }
}
