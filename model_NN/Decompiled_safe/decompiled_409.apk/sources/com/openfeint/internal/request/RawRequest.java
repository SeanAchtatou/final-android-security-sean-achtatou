package com.openfeint.internal.request;

public abstract class RawRequest extends JSONRequest {
    private IRawRequestDelegate b;

    public RawRequest() {
    }

    public RawRequest(OrderedArgList orderedArgList) {
        super(orderedArgList);
    }

    public void onResponse(int i, byte[] bArr) {
        try {
            super.onResponse(i, bArr);
            if (this.b != null) {
                this.b.onResponse(i, !isResponseJSON() ? notJSONError(i).generate() : new String(bArr));
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void setDelegate(IRawRequestDelegate iRawRequestDelegate) {
        this.b = iRawRequestDelegate;
    }
}
