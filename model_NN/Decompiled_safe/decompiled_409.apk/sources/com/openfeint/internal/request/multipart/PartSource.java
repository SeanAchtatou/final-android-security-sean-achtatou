package com.openfeint.internal.request.multipart;

import java.io.InputStream;

public interface PartSource {
    InputStream createInputStream();

    String getFileName();

    long getLength();
}
