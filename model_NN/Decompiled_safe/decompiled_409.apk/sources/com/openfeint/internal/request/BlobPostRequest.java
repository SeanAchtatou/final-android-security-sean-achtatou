package com.openfeint.internal.request;

import com.openfeint.internal.request.multipart.FilePart;
import com.openfeint.internal.request.multipart.MultipartHttpEntity;
import com.openfeint.internal.request.multipart.Part;
import com.openfeint.internal.request.multipart.PartSource;
import com.openfeint.internal.request.multipart.StringPart;
import com.openfeint.internal.resource.BlobUploadParameters;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;

public class BlobPostRequest extends BaseRequest {
    private BlobUploadParameters b;
    private PartSource c;
    private String d;
    private IRawRequestDelegate e;

    public BlobPostRequest(BlobUploadParameters blobUploadParameters, PartSource partSource, String str) {
        super(null);
        this.b = blobUploadParameters;
        this.c = partSource;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public HttpUriRequest generateRequest() {
        if (this.c == null) {
            return null;
        }
        HttpPost httpPost = new HttpPost(url());
        httpPost.setEntity(new MultipartHttpEntity(new Part[]{new StringPart("AWSAccessKeyId", this.b.c), new StringPart("acl", this.b.d), new StringPart("key", this.b.b), new StringPart("policy", this.b.e), new StringPart("signature", this.b.f), new FilePart("file", this.c, this.d, (String) null)}));
        addParams(httpPost);
        return httpPost;
    }

    public String method() {
        return "POST";
    }

    public void onResponse(int i, byte[] bArr) {
        if (this.e != null) {
            this.e.onResponse(i, new String(bArr));
        }
    }

    public String path() {
        return "";
    }

    public void setDelegate(IRawRequestDelegate iRawRequestDelegate) {
        this.e = iRawRequestDelegate;
    }

    public void sign() {
    }

    public boolean signed() {
        return false;
    }

    public String url() {
        return this.b.f118a;
    }
}
