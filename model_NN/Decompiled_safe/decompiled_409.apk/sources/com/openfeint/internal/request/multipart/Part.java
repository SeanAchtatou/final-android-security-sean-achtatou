package com.openfeint.internal.request.multipart;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public abstract class Part {

    /* renamed from: a  reason: collision with root package name */
    protected static final byte[] f116a = EncodingUtil.getAsciiBytes("\"");
    private static byte[] b;
    private static final byte[] c;
    private static byte[] d = EncodingUtil.getAsciiBytes("\r\n");
    private static byte[] e = EncodingUtil.getAsciiBytes("--");
    private static byte[] f = EncodingUtil.getAsciiBytes("Content-Disposition: form-data; name=");
    private static byte[] g = EncodingUtil.getAsciiBytes("Content-Type: ");
    private static byte[] h = EncodingUtil.getAsciiBytes("; charset=");
    private static byte[] i = EncodingUtil.getAsciiBytes("Content-Transfer-Encoding: ");
    private byte[] j;

    static {
        byte[] asciiBytes = EncodingUtil.getAsciiBytes("----------------ULTRASONIC_CUPCAKES___-__-");
        b = asciiBytes;
        c = asciiBytes;
    }

    public static String getBoundary() {
        return "----------------ULTRASONIC_CUPCAKES___-__-";
    }

    public static long getLengthOfParts(Part[] partArr) {
        return getLengthOfParts(partArr, c);
    }

    public static long getLengthOfParts(Part[] partArr, byte[] bArr) {
        if (partArr == null) {
            throw new IllegalArgumentException("Parts may not be null");
        }
        long j2 = 0;
        for (int i2 = 0; i2 < partArr.length; i2++) {
            partArr[i2].setPartBoundary(bArr);
            long length = partArr[i2].length();
            if (length < 0) {
                return -1;
            }
            j2 += length;
        }
        return j2 + ((long) e.length) + ((long) bArr.length) + ((long) e.length) + ((long) d.length);
    }

    public static void sendParts(OutputStream outputStream, Part[] partArr) {
        sendParts(outputStream, partArr, c);
    }

    public static void sendParts(OutputStream outputStream, Part[] partArr, byte[] bArr) {
        if (partArr == null) {
            throw new IllegalArgumentException("Parts may not be null");
        } else if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("partBoundary may not be empty");
        } else {
            for (int i2 = 0; i2 < partArr.length; i2++) {
                partArr[i2].setPartBoundary(bArr);
                partArr[i2].send(outputStream);
            }
            outputStream.write(e);
            outputStream.write(bArr);
            outputStream.write(e);
            outputStream.write(d);
        }
    }

    public abstract String getCharSet();

    public abstract String getContentType();

    public abstract String getName();

    /* access modifiers changed from: protected */
    public byte[] getPartBoundary() {
        return this.j == null ? c : this.j;
    }

    public abstract String getTransferEncoding();

    public boolean isRepeatable() {
        return true;
    }

    public long length() {
        if (lengthOfData() < 0) {
            return -1;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        sendStart(byteArrayOutputStream);
        sendDispositionHeader(byteArrayOutputStream);
        sendContentTypeHeader(byteArrayOutputStream);
        sendTransferEncodingHeader(byteArrayOutputStream);
        sendEndOfHeader(byteArrayOutputStream);
        sendEnd(byteArrayOutputStream);
        return ((long) byteArrayOutputStream.size()) + lengthOfData();
    }

    /* access modifiers changed from: protected */
    public abstract long lengthOfData();

    public void send(OutputStream outputStream) {
        sendStart(outputStream);
        sendDispositionHeader(outputStream);
        sendContentTypeHeader(outputStream);
        sendTransferEncodingHeader(outputStream);
        sendEndOfHeader(outputStream);
        sendData(outputStream);
        sendEnd(outputStream);
    }

    /* access modifiers changed from: protected */
    public void sendContentTypeHeader(OutputStream outputStream) {
        String contentType = getContentType();
        if (contentType != null) {
            outputStream.write(d);
            outputStream.write(g);
            outputStream.write(EncodingUtil.getAsciiBytes(contentType));
            String charSet = getCharSet();
            if (charSet != null) {
                outputStream.write(h);
                outputStream.write(EncodingUtil.getAsciiBytes(charSet));
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void sendData(OutputStream outputStream);

    /* access modifiers changed from: protected */
    public void sendDispositionHeader(OutputStream outputStream) {
        outputStream.write(f);
        outputStream.write(f116a);
        outputStream.write(EncodingUtil.getAsciiBytes(getName()));
        outputStream.write(f116a);
    }

    /* access modifiers changed from: protected */
    public void sendEnd(OutputStream outputStream) {
        outputStream.write(d);
    }

    /* access modifiers changed from: protected */
    public void sendEndOfHeader(OutputStream outputStream) {
        outputStream.write(d);
        outputStream.write(d);
    }

    /* access modifiers changed from: protected */
    public void sendStart(OutputStream outputStream) {
        outputStream.write(e);
        outputStream.write(getPartBoundary());
        outputStream.write(d);
    }

    /* access modifiers changed from: protected */
    public void sendTransferEncodingHeader(OutputStream outputStream) {
        String transferEncoding = getTransferEncoding();
        if (transferEncoding != null) {
            outputStream.write(d);
            outputStream.write(i);
            outputStream.write(EncodingUtil.getAsciiBytes(transferEncoding));
        }
    }

    /* access modifiers changed from: package-private */
    public void setPartBoundary(byte[] bArr) {
        this.j = bArr;
    }

    public String toString() {
        return getName();
    }
}
