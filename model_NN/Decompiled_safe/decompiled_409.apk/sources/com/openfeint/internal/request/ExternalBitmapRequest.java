package com.openfeint.internal.request;

public class ExternalBitmapRequest extends BitmapRequest {
    private String b;

    public ExternalBitmapRequest(String str) {
        this.b = str;
    }

    public String path() {
        return "";
    }

    public boolean signed() {
        return false;
    }

    public String url() {
        return this.b;
    }
}
