package com.openfeint.internal.request;

import java.util.Map;
import org.apache.http.params.HttpConnectionParams;

public class GenericRequest extends JSONContentRequest {
    private String b;
    private String c;
    private IRawRequestDelegate d;
    private long e = super.timeout();
    private int f = super.numRetries();

    public GenericRequest(String str, String str2, Map map, Map map2, IRawRequestDelegate iRawRequestDelegate) {
        if (map2 != null) {
            for (Map.Entry entry : map2.entrySet()) {
                String str3 = (String) entry.getKey();
                int parseInt = Integer.parseInt(entry.getValue().toString());
                if (str3.equals("connectionTimeout")) {
                    HttpConnectionParams.setConnectionTimeout(getHttpParams(), parseInt);
                } else if (str3.equals("socketTimeout")) {
                    HttpConnectionParams.setSoTimeout(getHttpParams(), parseInt);
                } else if (str3.equals("lingerTimeout")) {
                    HttpConnectionParams.setLinger(getHttpParams(), parseInt);
                } else if (str3.equals("timeout")) {
                    this.e = (long) parseInt;
                } else if (str3.equals("retries")) {
                    this.f = parseInt;
                }
            }
        }
        OrderedArgList orderedArgList = new OrderedArgList(map);
        orderedArgList.put("format", "json");
        setArgs(orderedArgList);
        this.b = str2;
        this.c = str;
        setDelegate(iRawRequestDelegate);
    }

    public String method() {
        return this.b;
    }

    public int numRetries() {
        return this.f;
    }

    public void onResponse(int i, byte[] bArr) {
        try {
            String generate = !isResponseJSON() ? notJSONError(i).generate() : bArr != null ? new String(bArr) : "";
            if (this.d != null) {
                this.d.onResponse(i, generate);
            }
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        }
    }

    public String path() {
        return this.c;
    }

    public void setDelegate(IRawRequestDelegate iRawRequestDelegate) {
        this.d = iRawRequestDelegate;
    }

    public long timeout() {
        return this.e;
    }
}
