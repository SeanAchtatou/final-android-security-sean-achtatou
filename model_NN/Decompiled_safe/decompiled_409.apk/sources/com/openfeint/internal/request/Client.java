package com.openfeint.internal.request;

import android.os.Bundle;
import android.os.Handler;
import com.openfeint.internal.CookieStore;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.SyncedStore;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.HttpContext;

public class Client extends DefaultHttpClient {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Signer f100a;
    /* access modifiers changed from: private */
    public Handler b;
    private CookieStore c;
    /* access modifiers changed from: private */
    public boolean d;
    private ExecutorService e = new Executor();

    final class Executor extends ThreadPoolExecutor {
        Executor() {
            super(2, 4, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), new RejectedExecutionHandler() {
                public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                    OpenFeintInternal.log("HTTPClient", "Can't submit runnable " + runnable.toString());
                }
            });
        }
    }

    class GzipDecompressingEntity extends HttpEntityWrapper {
        public GzipDecompressingEntity(HttpEntity httpEntity) {
            super(httpEntity);
        }

        public InputStream getContent() {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public long getContentLength() {
            return -1;
        }
    }

    public Client(String str, String str2, SyncedStore syncedStore) {
        super(makeCCM(), new BasicHttpParams());
        this.f100a = new Signer(str, str2);
        this.b = new Handler();
        this.c = new CookieStore(syncedStore);
        setCookieStore(this.c);
        addRequestInterceptor(new HttpRequestInterceptor() {
            public void process(HttpRequest httpRequest, HttpContext httpContext) {
                if (!httpRequest.containsHeader("Accept-Encoding")) {
                    httpRequest.addHeader("Accept-Encoding", "gzip");
                }
            }
        });
        addResponseInterceptor(new HttpResponseInterceptor() {
            public void process(HttpResponse httpResponse, HttpContext httpContext) {
                Header contentEncoding;
                HeaderElement[] elements;
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null && (contentEncoding = entity.getContentEncoding()) != null && (elements = contentEncoding.getElements()) != null) {
                    for (HeaderElement name : elements) {
                        if (name.getName().equalsIgnoreCase("gzip")) {
                            httpResponse.setEntity(new GzipDecompressingEntity(httpResponse.getEntity()));
                            return;
                        }
                    }
                }
            }
        });
    }

    static final ClientConnectionManager makeCCM() {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        return new ThreadSafeClientConnManager(new BasicHttpParams(), schemeRegistry);
    }

    public final void makeRequest(BaseRequest baseRequest) {
        makeRequest(baseRequest, baseRequest.timeout());
    }

    public final void makeRequest(final BaseRequest baseRequest, long j) {
        final AnonymousClass3 r6 = new Runnable() {
            public void run() {
                baseRequest.onResponse();
            }
        };
        final AnonymousClass4 r2 = new Runnable() {
            public void run() {
                if (baseRequest.getResponse() == null && baseRequest.getFuture().cancel(true)) {
                    baseRequest.postTimeoutCleanup();
                    Client.this.b.post(r6);
                }
            }
        };
        final long j2 = j;
        final BaseRequest baseRequest2 = baseRequest;
        baseRequest.setFuture(this.e.submit(new Runnable() {
            public void run() {
                Client.this.b.postDelayed(r2, j2);
                baseRequest2.sign(Client.this.f100a);
                baseRequest2.exec(Client.this.d);
                Client.this.b.removeCallbacks(r2);
                Client.this.b.post(r6);
            }
        }));
    }

    public void restoreInstanceState(Bundle bundle) {
        this.c.restoreInstanceState(bundle);
        this.d = bundle.getBoolean("mForceOffline");
    }

    public void saveInstanceState(Bundle bundle) {
        this.c.saveInstanceState(bundle);
        bundle.putBoolean("mForceOffline", this.d);
    }

    public boolean toggleForceOffline() {
        if (this.d) {
            this.d = false;
            OpenFeintInternal.log("HTTPClient", "forceOffline = FALSE");
        } else {
            this.d = true;
            OpenFeintInternal.log("HTTPClient", "forceOffline = TRUE");
        }
        return this.d;
    }
}
