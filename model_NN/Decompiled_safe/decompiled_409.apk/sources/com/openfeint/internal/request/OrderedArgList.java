package com.openfeint.internal.request;

import com.google.api.client.escape.PercentEscaper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderedArgList {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList f110a = new ArrayList();

    class NVPComparator implements Comparator {
        NVPComparator() {
        }

        public int compare(NameValuePair nameValuePair, NameValuePair nameValuePair2) {
            int compareTo = nameValuePair.getName().compareTo(nameValuePair2.getName());
            return compareTo == 0 ? nameValuePair.getValue().compareTo(nameValuePair2.getValue()) : compareTo;
        }
    }

    public OrderedArgList() {
    }

    public OrderedArgList(OrderedArgList orderedArgList) {
        this.f110a.addAll(orderedArgList.getArgs());
    }

    public OrderedArgList(Map map) {
        accumulate(map, (String) null);
    }

    public OrderedArgList(JSONObject jSONObject) {
        try {
            accumulate(jSONObject, (String) null);
        } catch (JSONException e) {
        }
    }

    private void accumulate(Map map, String str) {
        for (Map.Entry entry : map.entrySet()) {
            String str2 = (String) entry.getKey();
            Object value = entry.getValue();
            if (str != null) {
                str2 = String.valueOf(str) + "[" + str2 + "]";
            }
            if (value instanceof Map) {
                accumulate(stringObjectMap(value), str2);
            } else {
                put(str2, value.toString());
            }
        }
    }

    private void accumulate(JSONArray jSONArray, String str) {
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            Object obj = jSONArray.get(i);
            if (obj instanceof JSONObject) {
                accumulate((JSONObject) obj, str);
            } else if (obj instanceof JSONArray) {
                accumulate((JSONArray) obj, str);
            } else {
                put(str, obj.toString());
            }
        }
    }

    private void accumulate(final JSONObject jSONObject, String str) {
        for (String str2 : new Iterable() {
            public Iterator iterator() {
                return jSONObject.keys();
            }
        }) {
            Object obj = jSONObject.get(str2);
            String str3 = str == null ? str2 : String.valueOf(str) + "[" + str2 + "]";
            if (obj instanceof JSONObject) {
                accumulate((JSONObject) obj, str3);
            } else if (obj instanceof JSONArray) {
                accumulate((JSONArray) obj, str3);
            } else {
                put(str3, obj.toString());
            }
        }
    }

    private static String getArgString(List list) {
        PercentEscaper percentEscaper = new PercentEscaper("-_.*", true);
        Iterator it = list.iterator();
        StringBuilder sb = null;
        while (it.hasNext()) {
            NameValuePair nameValuePair = (NameValuePair) it.next();
            if (sb == null) {
                sb = new StringBuilder();
            } else {
                sb.append('&');
            }
            sb.append(percentEscaper.a(nameValuePair.getName()));
            sb.append('=');
            if (nameValuePair.getValue() != null) {
                sb.append(percentEscaper.a(nameValuePair.getValue()));
            }
        }
        if (sb == null) {
            return null;
        }
        return sb.toString();
    }

    private final Map stringObjectMap(Object obj) {
        return (Map) obj;
    }

    public String getArgString() {
        return getArgString(getArgs());
    }

    public String getArgStringSorted() {
        return getArgString(getArgsSorted());
    }

    public List getArgs() {
        return this.f110a;
    }

    public List getArgsSorted() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.f110a);
        Collections.sort(arrayList, new NVPComparator());
        return arrayList;
    }

    public void put(String str, String str2) {
        this.f110a.add(new BasicNameValuePair(str, str2));
    }

    public NameValuePair remove(String str) {
        Iterator it = this.f110a.iterator();
        while (it.hasNext()) {
            NameValuePair nameValuePair = (NameValuePair) it.next();
            if (nameValuePair.getName().equals(str)) {
                this.f110a.remove(nameValuePair);
                return nameValuePair;
            }
        }
        return null;
    }
}
