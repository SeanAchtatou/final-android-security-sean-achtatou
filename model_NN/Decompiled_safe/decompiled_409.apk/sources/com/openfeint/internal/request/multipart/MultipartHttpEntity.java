package com.openfeint.internal.request.multipart;

import com.openfeint.internal.OpenFeintInternal;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

public class MultipartHttpEntity implements HttpEntity {

    /* renamed from: a  reason: collision with root package name */
    private static byte[] f115a = EncodingUtil.getAsciiBytes("-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
    private Part[] b;
    private byte[] c;

    public MultipartHttpEntity(Part[] partArr) {
        if (partArr == null) {
            throw new IllegalArgumentException("parts cannot be null");
        }
        this.b = partArr;
    }

    private static byte[] generateMultipartBoundary() {
        Random random = new Random();
        byte[] bArr = new byte[(random.nextInt(11) + 30)];
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = f115a[random.nextInt(f115a.length)];
        }
        return bArr;
    }

    public void consumeContent() {
        throw new UnsupportedOperationException();
    }

    public InputStream getContent() {
        throw new UnsupportedOperationException();
    }

    public Header getContentEncoding() {
        return new BasicHeader("Content-Encoding", "text/html; charset=UTF-8");
    }

    public long getContentLength() {
        try {
            return Part.getLengthOfParts(this.b, getMultipartBoundary());
        } catch (Exception e) {
            OpenFeintInternal.log("MultipartRequestEntity", "An exception occurred while getting the length of the parts");
            return 0;
        }
    }

    public Header getContentType() {
        StringBuffer stringBuffer = new StringBuffer("multipart/form-data");
        stringBuffer.append("; boundary=");
        stringBuffer.append(EncodingUtil.getAsciiString(getMultipartBoundary()));
        return new BasicHeader("Content-Type", stringBuffer.toString());
    }

    /* access modifiers changed from: protected */
    public byte[] getMultipartBoundary() {
        if (this.c == null) {
            this.c = generateMultipartBoundary();
        }
        return this.c;
    }

    public boolean isChunked() {
        return getContentLength() < 0;
    }

    public boolean isRepeatable() {
        for (Part isRepeatable : this.b) {
            if (!isRepeatable.isRepeatable()) {
                return false;
            }
        }
        return true;
    }

    public boolean isStreaming() {
        return false;
    }

    public void writeTo(OutputStream outputStream) {
        Part.sendParts(outputStream, this.b, getMultipartBoundary());
    }
}
