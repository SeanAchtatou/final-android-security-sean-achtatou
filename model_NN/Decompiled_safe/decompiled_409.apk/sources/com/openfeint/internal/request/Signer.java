package com.openfeint.internal.request;

import a.b.a.a.a.b;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Signer {

    /* renamed from: a  reason: collision with root package name */
    private String f112a;
    private String b;
    private String c = (String.valueOf(this.b) + "&");
    private String d;

    public Signer(String str, String str2) {
        this.f112a = str;
        this.b = str2;
    }

    public String getKey() {
        return this.f112a;
    }

    public void setAccessToken(String str, String str2) {
        this.d = str;
        this.c = String.valueOf(this.b) + "&" + str2;
    }

    public String sign(String str, String str2, long j, OrderedArgList orderedArgList) {
        if (this.d != null) {
            orderedArgList.put("token", this.d);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append('+');
        sb.append(this.b);
        sb.append('+');
        sb.append(str2);
        sb.append('+');
        String argString = orderedArgList.getArgString();
        if (argString == null) {
            argString = "";
        }
        sb.append(argString);
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(this.c.getBytes("UTF-8"), "HmacSHA1");
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(secretKeySpec);
            return new String(b.a(instance.doFinal(sb.toString().getBytes("UTF-8")))).replace("\r\n", "");
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException e) {
            return null;
        }
    }
}
