package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class Compression {

    /* renamed from: a  reason: collision with root package name */
    private static String f108a = "Compression";
    private static final byte[] b = "OFZLHDR0".getBytes();
    private static /* synthetic */ int[] c;

    enum CompressionMethod {
        Default,
        Uncompressed,
        LegacyHeaderless
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod() {
        int[] iArr = c;
        if (iArr == null) {
            iArr = new int[CompressionMethod.values().length];
            try {
                iArr[CompressionMethod.Default.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[CompressionMethod.LegacyHeaderless.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[CompressionMethod.Uncompressed.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            c = iArr;
        }
        return iArr;
    }

    private static byte[] _compress(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream);
        deflaterOutputStream.write(bArr);
        deflaterOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] compress(byte[] bArr) {
        try {
            switch ($SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod()[compressionMethod().ordinal()]) {
                case 1:
                    byte[] _compress = _compress(bArr);
                    byte[] integerToLittleEndianByteArray = integerToLittleEndianByteArray(bArr.length);
                    int length = _compress.length + b.length + integerToLittleEndianByteArray.length;
                    if (length < bArr.length) {
                        byte[] bArr2 = new byte[length];
                        System.arraycopy(b, 0, bArr2, 0, b.length);
                        System.arraycopy(integerToLittleEndianByteArray, 0, bArr2, b.length, integerToLittleEndianByteArray.length);
                        System.arraycopy(_compress, 0, bArr2, b.length + 4, _compress.length);
                        OpenFeintInternal.log(f108a, String.format("Using Default strategy: orig %d bytes, compressed %d bytes (%.2f%% of original size)", Integer.valueOf(bArr.length), Integer.valueOf(length), Float.valueOf((((float) length) / ((float) bArr.length)) * 100.0f)));
                        return bArr2;
                    }
                    OpenFeintInternal.log(f108a, "Using Default strategy: compression declined");
                    return bArr;
                case 2:
                default:
                    OpenFeintInternal.log(f108a, "Using Uncompressed strategy");
                    return bArr;
                case 3:
                    byte[] _compress2 = _compress(bArr);
                    OpenFeintInternal.log(f108a, String.format("Using Default strategy: orig %d bytes, compressed %d bytes (%.2f%% of original size)", Integer.valueOf(bArr.length), Integer.valueOf(_compress2.length), Float.valueOf((((float) _compress2.length) / ((float) bArr.length)) * 100.0f)));
                    return _compress2;
            }
        } catch (IOException e) {
            return null;
        }
    }

    private static CompressionMethod compressionMethod() {
        String str = (String) OpenFeintInternal.getInstance().getSettings().get("SettingCloudStorageCompressionStrategy");
        if (str != null) {
            if (str.equals("CloudStorageCompressionStrategyLegacyHeaderlessCompression")) {
                return CompressionMethod.LegacyHeaderless;
            }
            if (str.equals("CloudStorageCompressionStrategyNoCompression")) {
                return CompressionMethod.Uncompressed;
            }
        }
        return CompressionMethod.Default;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static byte[] decompress(byte[] bArr) {
        switch ($SWITCH_TABLE$com$openfeint$internal$request$Compression$CompressionMethod()[compressionMethod().ordinal()]) {
            case 1:
                int i = 0;
                if (b.length < bArr.length) {
                    while (i < b.length && b[i] == bArr[i]) {
                        i++;
                    }
                }
                if (i == b.length) {
                    int length = b.length + 4;
                    return Util.toByteArray(new InflaterInputStream(new ByteArrayInputStream(bArr, length, bArr.length - length)));
                }
                break;
            case 3:
                return Util.toByteArray(new InflaterInputStream(new ByteArrayInputStream(bArr)));
        }
        return bArr;
    }

    private static byte[] integerToLittleEndianByteArray(int i) {
        return new byte[]{(byte) (i >> 0), (byte) (i >> 8), (byte) (i >> 16), (byte) (i >> 24)};
    }
}
