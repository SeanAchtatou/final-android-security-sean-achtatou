package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.resource.ServerException;
import hamon05.speed.pac.R;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Future;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

public abstract class BaseRequest {

    /* renamed from: a  reason: collision with root package name */
    protected static String f97a = "Request";
    private static int b = 2;
    private static long c = 20000;
    private static String k = null;
    private OrderedArgList d;
    /* access modifiers changed from: private */
    public HttpUriRequest e;
    /* access modifiers changed from: private */
    public byte[] f;
    private boolean g = false;
    /* access modifiers changed from: private */
    public String h = null;
    /* access modifiers changed from: private */
    public String i = null;
    /* access modifiers changed from: private */
    public int j;
    private long l;
    private String m = null;
    private String n = null;
    private int o = 0;
    /* access modifiers changed from: private */
    public String p = null;
    private Future q = null;
    private HttpParams r = null;
    /* access modifiers changed from: private */
    public HttpResponse s;

    public BaseRequest() {
    }

    public BaseRequest(OrderedArgList orderedArgList) {
        setArgs(orderedArgList);
    }

    private void fakeServerException(ServerException serverException) {
        this.j = 0;
        this.f = serverException.generate().getBytes();
        this.i = "application/json;";
    }

    /* access modifiers changed from: protected */
    public final void addParams(HttpUriRequest httpUriRequest) {
        if (this.r != null) {
            httpUriRequest.setParams(this.r);
        }
    }

    /* access modifiers changed from: protected */
    public String currentURL() {
        return this.p != null ? this.p : url();
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 124 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void exec(boolean r8) {
        /*
            r7 = this;
            r6 = 1
            r5 = 0
            r4 = 0
            org.apache.http.client.methods.HttpUriRequest r0 = r7.generateRequest()
            r7.e = r0
            int r0 = r7.numRetries()
            r7.o = r0
            r7.f = r4
            r7.j = r5
            r7.s = r4
            org.apache.http.client.methods.HttpUriRequest r0 = r7.e
            java.net.URI r0 = r0.getURI()
            java.lang.String r0 = r0.getPath()
            java.lang.String r1 = "//"
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x00c4
            com.openfeint.internal.resource.ServerException r0 = new com.openfeint.internal.resource.ServerException
            r0.<init>()
            java.lang.String r1 = "RequestError"
            r0.f125a = r1
            r1 = 2131034154(0x7f05002a, float:1.7678818E38)
            java.lang.String r1 = com.openfeint.internal.OpenFeintInternal.getRString(r1)
            r0.b = r1
            r0.c = r6
            r7.fakeServerException(r0)
        L_0x003e:
            int r0 = r7.j
            byte[] r1 = r7.f
            r7.onResponseOffMainThread(r0, r1)
            return
        L_0x0046:
            if (r8 == 0) goto L_0x00aa
            r0 = 0
            r7.o = r0     // Catch:{ Exception -> 0x0053 }
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0053 }
            java.lang.String r1 = "Forced failure"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0053 }
            throw r0     // Catch:{ Exception -> 0x0053 }
        L_0x0053:
            r0 = move-exception
            java.lang.String r1 = com.openfeint.internal.request.BaseRequest.f97a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Error executing request '"
            r2.<init>(r3)
            java.lang.String r3 = r7.path()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "'."
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.openfeint.internal.OpenFeintInternal.log(r1, r2)
            java.io.PrintStream r1 = java.lang.System.err
            r0.printStackTrace(r1)
            r7.f = r4
            r7.j = r5
            r7.s = r4
            int r1 = r7.o
            int r1 = r1 - r6
            r7.o = r1
            if (r1 >= 0) goto L_0x00c4
            com.openfeint.internal.resource.ServerException r1 = new com.openfeint.internal.resource.ServerException
            r1.<init>()
            java.lang.Class r2 = r0.getClass()
            java.lang.String r2 = r2.getName()
            r1.f125a = r2
            java.lang.String r0 = r0.getMessage()
            r1.b = r0
            java.lang.String r0 = r1.b
            if (r0 != 0) goto L_0x00a6
            r0 = 2131034117(0x7f050005, float:1.7678742E38)
            java.lang.String r0 = com.openfeint.internal.OpenFeintInternal.getRString(r0)
            r1.b = r0
        L_0x00a6:
            r7.fakeServerException(r1)
            goto L_0x003e
        L_0x00aa:
            com.openfeint.internal.OpenFeintInternal r0 = com.openfeint.internal.OpenFeintInternal.getInstance()     // Catch:{ Exception -> 0x0053 }
            org.apache.http.impl.client.AbstractHttpClient r0 = r0.getClient()     // Catch:{ Exception -> 0x0053 }
            org.apache.http.protocol.BasicHttpContext r1 = new org.apache.http.protocol.BasicHttpContext     // Catch:{ Exception -> 0x0053 }
            r1.<init>()     // Catch:{ Exception -> 0x0053 }
            com.openfeint.internal.request.BaseRequest$1 r2 = new com.openfeint.internal.request.BaseRequest$1     // Catch:{ Exception -> 0x0053 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0053 }
            org.apache.http.client.methods.HttpUriRequest r3 = r7.e     // Catch:{ Exception -> 0x0053 }
            r0.execute(r3, r2, r1)     // Catch:{ Exception -> 0x0053 }
            r0 = 0
            r7.e = r0     // Catch:{ Exception -> 0x0053 }
        L_0x00c4:
            byte[] r0 = r7.f
            if (r0 == 0) goto L_0x0046
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.request.BaseRequest.exec(boolean):void");
    }

    /* access modifiers changed from: protected */
    public HttpEntity genEntity() {
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(this.d.getArgs(), "UTF-8");
        urlEncodedFormEntity.setContentType("application/x-www-form-urlencoded; charset=" + "UTF-8");
        return urlEncodedFormEntity;
    }

    /* access modifiers changed from: protected */
    public HttpUriRequest generateRequest() {
        HttpPost httpPost = null;
        String method = method();
        if (method.equals("GET") || method.equals("DELETE")) {
            String url = url();
            String argString = this.d.getArgString();
            if (argString != null) {
                url = String.valueOf(url) + "?" + argString;
            }
            if (method.equals("GET")) {
                httpPost = new HttpGet(url);
            } else if (method.equals("DELETE")) {
                httpPost = new HttpDelete(url);
            }
        } else {
            if (method.equals("POST")) {
                httpPost = new HttpPost(url());
            } else if (method.equals("PUT")) {
                httpPost = new HttpPut(url());
            } else {
                throw new RuntimeException("Unsupported HTTP method: " + method);
            }
            try {
                httpPost.setEntity(genEntity());
            } catch (UnsupportedEncodingException e2) {
                OpenFeintInternal.log(f97a, "Unable to encode request.");
                e2.printStackTrace(System.err);
            }
        }
        if (!(!signed() || this.m == null || this.n == null)) {
            httpPost.addHeader("X-OF-Signature", this.m);
            httpPost.addHeader("X-OF-Key", this.n);
        }
        addParams(httpPost);
        return httpPost;
    }

    public Future getFuture() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public HttpParams getHttpParams() {
        if (this.r == null) {
            this.r = new BasicHttpParams();
        }
        return this.r;
    }

    public HttpResponse getResponse() {
        return this.s;
    }

    /* access modifiers changed from: protected */
    public String getResponseEncoding() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public String getResponseType() {
        return this.i;
    }

    public void launch() {
        OpenFeintInternal.makeRequest(this);
    }

    public abstract String method();

    public boolean needsDeviceSession() {
        return signed();
    }

    public int numRetries() {
        return b;
    }

    public final void onResponse() {
        if (!this.g) {
            this.g = true;
            if (this.f == null) {
                this.j = 0;
                ServerException serverException = new ServerException();
                serverException.f125a = "Unknown";
                serverException.b = OpenFeintInternal.getRString(R.string.of_unknown_server_error);
                fakeServerException(serverException);
            }
            onResponse(this.j, this.f);
            this.s = null;
        }
    }

    public abstract void onResponse(int i2, byte[] bArr);

    /* access modifiers changed from: protected */
    public void onResponseOffMainThread(int i2, byte[] bArr) {
    }

    public abstract String path();

    public void postTimeoutCleanup() {
        final HttpUriRequest httpUriRequest = this.e;
        this.e = null;
        if (httpUriRequest != null) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        httpUriRequest.abort();
                    } catch (UnsupportedOperationException e) {
                    }
                }
            }).start();
        }
        ServerException serverException = new ServerException();
        serverException.f125a = "Timeout";
        serverException.b = OpenFeintInternal.getRString(R.string.of_timeout);
        fakeServerException(serverException);
    }

    public final void setArgs(OrderedArgList orderedArgList) {
        this.d = orderedArgList;
    }

    public void setFuture(Future future) {
        this.q = future;
    }

    /* access modifiers changed from: protected */
    public boolean shouldRedirect(String str) {
        return true;
    }

    public final void sign(Signer signer) {
        if (this.d == null) {
            this.d = new OrderedArgList();
        }
        if (signed()) {
            this.l = System.currentTimeMillis() / 1000;
            this.m = signer.sign(path(), method(), this.l, this.d);
            this.n = signer.getKey();
        }
    }

    public boolean signed() {
        return true;
    }

    public long timeout() {
        return c;
    }

    public String url() {
        if (k == null) {
            k = OpenFeintInternal.getInstance().getServerUrl();
        }
        return String.valueOf(k) + path();
    }

    public boolean wantsLogin() {
        return false;
    }
}
