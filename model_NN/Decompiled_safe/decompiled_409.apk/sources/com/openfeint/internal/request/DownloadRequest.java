package com.openfeint.internal.request;

import a.a.a.n;
import com.openfeint.internal.JsonResourceParser;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.resource.ServerException;
import hamon05.speed.pac.R;
import java.io.IOException;

public abstract class DownloadRequest extends BaseRequest {
    public DownloadRequest() {
    }

    public DownloadRequest(OrderedArgList orderedArgList) {
        super(orderedArgList);
    }

    public String method() {
        return "GET";
    }

    public void onFailure(String str) {
        OpenFeintInternal.log("ServerException", str);
    }

    public void onResponse(int i, byte[] bArr) {
        String rString;
        String rString2 = OpenFeintInternal.getRString(R.string.of_unknown_server_error);
        if (200 > i || i >= 300 || bArr == null) {
            if (404 == i) {
                rString = OpenFeintInternal.getRString(R.string.of_file_not_found);
            } else {
                try {
                    Object parse = new JsonResourceParser(new n((byte) 0).a(bArr)).parse();
                    if (parse == null || !(parse instanceof ServerException)) {
                        rString = rString2;
                    } else {
                        ServerException serverException = (ServerException) parse;
                        rString = String.valueOf(serverException.f125a) + ": " + serverException.b;
                    }
                } catch (IOException e) {
                    rString = OpenFeintInternal.getRString(R.string.of_error_parsing_error_message);
                }
            }
            onFailure(rString);
            return;
        }
        onSuccess(bArr);
    }

    /* access modifiers changed from: protected */
    public abstract void onSuccess(byte[] bArr);
}
