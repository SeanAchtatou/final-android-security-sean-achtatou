package com.openfeint.internal.request.multipart;

import java.io.OutputStream;

public class StringPart extends PartBase {
    private byte[] b;
    private String c;

    public StringPart(String str, String str2) {
        this(str, str2, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StringPart(String str, String str2, String str3) {
        super(str, "text/html", str3 == null ? "UTF-8" : str3, "8bit");
        if (str2 == null) {
            throw new IllegalArgumentException("Value may not be null");
        } else if (str2.indexOf(0) != -1) {
            throw new IllegalArgumentException("NULs may not be present in string parts");
        } else {
            this.c = str2;
        }
    }

    private byte[] getContent() {
        if (this.b == null) {
            this.b = EncodingUtil.getBytes(this.c, getCharSet());
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    public long lengthOfData() {
        return (long) getContent().length;
    }

    /* access modifiers changed from: protected */
    public void sendData(OutputStream outputStream) {
        outputStream.write(getContent());
    }

    public void setCharSet(String str) {
        super.setCharSet(str);
        this.b = null;
    }
}
