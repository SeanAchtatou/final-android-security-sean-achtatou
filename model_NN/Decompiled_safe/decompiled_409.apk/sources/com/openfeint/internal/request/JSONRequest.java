package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.resource.ServerException;
import hamon05.speed.pac.R;

public abstract class JSONRequest extends JSONContentRequest {
    public JSONRequest() {
    }

    public JSONRequest(OrderedArgList orderedArgList) {
        super(orderedArgList);
    }

    /* access modifiers changed from: protected */
    public void onFailure(Object obj) {
        String rString = OpenFeintInternal.getRString(R.string.of_unknown_server_error);
        if (obj != null && (obj instanceof ServerException)) {
            ServerException serverException = (ServerException) obj;
            rString = serverException.b;
            if (serverException.c) {
                OpenFeintInternal.log("ServerException", rString);
                OpenFeintInternal.getInstance().displayErrorDialog(rString);
            }
        }
        onFailure(rString);
    }

    public void onFailure(String str) {
    }

    /* access modifiers changed from: protected */
    public void onResponse(int i, Object obj) {
        if (200 > i || i >= 300 || (obj != null && (obj instanceof ServerException))) {
            onFailure(obj);
        } else {
            onSuccess(obj);
        }
    }

    public void onResponse(int i, byte[] bArr) {
        if (bArr.length == 0 || ((bArr.length == 1 && bArr[0] == 32) || isResponseJSON())) {
            onResponse(i, parseJson(bArr));
        } else {
            onResponse(i, notJSONError(i));
        }
    }

    public void onSuccess(Object obj) {
    }

    /* access modifiers changed from: protected */
    public Object parseJson(byte[] bArr) {
        return Util.getObjFromJson(bArr);
    }
}
