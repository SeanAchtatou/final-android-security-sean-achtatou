package com.openfeint.internal.request.multipart;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public class FilePart extends PartBase {
    private static final byte[] b = EncodingUtil.getAsciiBytes("; filename=");
    private PartSource c;

    public FilePart(String str, PartSource partSource) {
        this(str, partSource, (String) null, (String) null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FilePart(String str, PartSource partSource, String str2, String str3) {
        super(str, str2 == null ? "application/octet-stream" : str2, str3 == null ? "ISO-8859-1" : str3, "binary");
        if (partSource == null) {
            throw new IllegalArgumentException("Source may not be null");
        }
        this.c = partSource;
    }

    public FilePart(String str, File file) {
        this(str, new FilePartSource(file), (String) null, (String) null);
    }

    public FilePart(String str, File file, String str2, String str3) {
        this(str, new FilePartSource(file), str2, str3);
    }

    public FilePart(String str, String str2, File file) {
        this(str, new FilePartSource(str2, file), (String) null, (String) null);
    }

    public FilePart(String str, String str2, File file, String str3, String str4) {
        this(str, new FilePartSource(str2, file), str3, str4);
    }

    /* access modifiers changed from: protected */
    public PartSource getSource() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public long lengthOfData() {
        return this.c.getLength();
    }

    /* access modifiers changed from: protected */
    public void sendData(OutputStream outputStream) {
        if (lengthOfData() != 0) {
            byte[] bArr = new byte[4096];
            InputStream createInputStream = this.c.createInputStream();
            while (true) {
                try {
                    int read = createInputStream.read(bArr);
                    if (read >= 0) {
                        outputStream.write(bArr, 0, read);
                    } else {
                        return;
                    }
                } finally {
                    createInputStream.close();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void sendDispositionHeader(OutputStream outputStream) {
        super.sendDispositionHeader(outputStream);
        String fileName = this.c.getFileName();
        if (fileName != null) {
            outputStream.write(b);
            outputStream.write(f116a);
            outputStream.write(EncodingUtil.getAsciiBytes(fileName));
            outputStream.write(f116a);
        }
    }
}
