package com.openfeint.internal.request.multipart;

public abstract class PartBase extends Part {
    private String b;
    private String c;
    private String d;
    private String e;

    public PartBase(String str, String str2, String str3, String str4) {
        if (str == null) {
            throw new IllegalArgumentException("Name must not be null");
        }
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    public String getCharSet() {
        return this.d;
    }

    public String getContentType() {
        return this.c;
    }

    public String getName() {
        return this.b;
    }

    public String getTransferEncoding() {
        return this.e;
    }

    public void setCharSet(String str) {
        this.d = str;
    }

    public void setContentType(String str) {
        this.c = str;
    }

    public void setName(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Name must not be null");
        }
        this.b = str;
    }

    public void setTransferEncoding(String str) {
        this.e = str;
    }
}
