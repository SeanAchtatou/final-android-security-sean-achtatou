package com.openfeint.internal.request;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.db.DB;
import com.openfeint.internal.ui.WebViewCache;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

public abstract class CacheRequest extends BaseRequest {
    private String b;

    public CacheRequest(String str) {
        this.b = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getLastModified() {
        /*
            r6 = this;
            r4 = 0
            com.openfeint.internal.db.DB$DataStorageHelperX r0 = com.openfeint.internal.db.DB.f88a     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            r1 = 1
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            r2 = 0
            java.lang.String r3 = r6.b     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            r1[r2] = r3     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            java.lang.String r2 = "SELECT VALUE FROM store where id=?"
            android.database.Cursor r1 = r0.rawQuery(r2, r1)     // Catch:{ SQLiteDiskIOException -> 0x002d, Exception -> 0x0034 }
            int r2 = r1.getCount()     // Catch:{ SQLiteDiskIOException -> 0x004c, Exception -> 0x0043 }
            if (r2 <= 0) goto L_0x0052
            r1.moveToFirst()     // Catch:{ SQLiteDiskIOException -> 0x004c, Exception -> 0x0043 }
            r2 = 0
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteDiskIOException -> 0x004c, Exception -> 0x0043 }
        L_0x0023:
            r0.close()     // Catch:{ SQLiteDiskIOException -> 0x004f, Exception -> 0x0047 }
            r0 = r2
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()
        L_0x002c:
            return r0
        L_0x002d:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x0030:
            com.openfeint.internal.ui.WebViewCache.diskError()
            goto L_0x0027
        L_0x0034:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0037:
            java.lang.String r3 = "CacheRequest"
            java.lang.String r0 = r0.getMessage()
            com.openfeint.internal.OpenFeintInternal.log(r3, r0)
            r0 = r1
            r1 = r2
            goto L_0x0027
        L_0x0043:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x0037
        L_0x0047:
            r0 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0037
        L_0x004c:
            r0 = move-exception
            r0 = r4
            goto L_0x0030
        L_0x004f:
            r0 = move-exception
            r0 = r2
            goto L_0x0030
        L_0x0052:
            r2 = r4
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.request.CacheRequest.getLastModified():java.lang.String");
    }

    private void storeLastModified(String str) {
        if (str != null) {
            String[] strArr = {this.b, str};
            try {
                SQLiteDatabase writableDatabase = DB.f88a.getWritableDatabase();
                writableDatabase.execSQL("INSERT OR REPLACE INTO store VALUES(?, ?)", strArr);
                writableDatabase.close();
            } catch (SQLiteDiskIOException e) {
                WebViewCache.diskError();
            } catch (Exception e2) {
                OpenFeintInternal.log("CacheRequest", e2.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public HttpUriRequest generateRequest() {
        HttpUriRequest generateRequest = super.generateRequest();
        String lastModified = getLastModified();
        if (lastModified != null) {
            generateRequest.addHeader("If-Modified-Since", lastModified);
        }
        return generateRequest;
    }

    public String method() {
        return "GET";
    }

    public void on200Response() {
        updateLastModifiedFromResponse(getResponse());
    }

    /* access modifiers changed from: protected */
    public void updateLastModifiedFromResponse(HttpResponse httpResponse) {
        Header firstHeader = httpResponse != null ? httpResponse.getFirstHeader("Last-Modified") : null;
        if (firstHeader != null) {
            storeLastModified(firstHeader.getValue());
        }
    }
}
