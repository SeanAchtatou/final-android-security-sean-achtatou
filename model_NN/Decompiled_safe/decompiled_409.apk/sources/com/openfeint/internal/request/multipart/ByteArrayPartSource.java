package com.openfeint.internal.request.multipart;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class ByteArrayPartSource implements PartSource {

    /* renamed from: a  reason: collision with root package name */
    private String f113a;
    private byte[] b;

    public ByteArrayPartSource(String str, byte[] bArr) {
        this.f113a = str;
        this.b = bArr;
    }

    public InputStream createInputStream() {
        return new ByteArrayInputStream(this.b);
    }

    public String getFileName() {
        return this.f113a;
    }

    public long getLength() {
        return (long) this.b.length;
    }
}
