package com.openfeint.internal.request;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.openfeint.internal.OpenFeintInternal;
import hamon05.speed.pac.R;

public abstract class BitmapRequest extends DownloadRequest {
    public void onSuccess(Bitmap bitmap) {
    }

    /* access modifiers changed from: protected */
    public void onSuccess(byte[] bArr) {
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        if (decodeByteArray != null) {
            onSuccess(decodeByteArray);
        } else {
            onFailure(OpenFeintInternal.getRString(R.string.of_bitmap_decode_error));
        }
    }
}
