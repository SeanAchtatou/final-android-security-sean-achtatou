package com.openfeint.internal.offline;

import a.b.a.a.a.c;
import android.content.Context;
import com.openfeint.api.Notification;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.notifications.SimpleNotification;
import com.openfeint.internal.request.BaseRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.DateResourceProperty;
import hamon05.speed.pac.R;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public class OfflineSupport {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static DB f92a = null;
    private static String b = null;
    /* access modifiers changed from: private */
    public static AtomicBoolean c = new AtomicBoolean(false);

    public class DB {

        /* renamed from: a  reason: collision with root package name */
        public ArrayList f94a = new ArrayList();
        public ArrayList b = new ArrayList();

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0060 A[SYNTHETIC, Splitter:B:15:0x0060] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00dd A[SYNTHETIC, Splitter:B:32:0x00dd] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static com.openfeint.internal.offline.OfflineSupport.DB load(java.lang.String r8) {
            /*
                com.openfeint.internal.offline.OfflineSupport$DB r0 = new com.openfeint.internal.offline.OfflineSupport$DB
                r0.<init>()
                r1 = 0
                if (r8 == 0) goto L_0x0063
                com.openfeint.internal.OpenFeintInternal r2 = com.openfeint.internal.OpenFeintInternal.getInstance()     // Catch:{ Exception -> 0x00eb, all -> 0x00da }
                android.content.Context r2 = r2.getContext()     // Catch:{ Exception -> 0x00eb, all -> 0x00da }
                java.io.FileInputStream r2 = r2.openFileInput(r8)     // Catch:{ Exception -> 0x00eb, all -> 0x00da }
                javax.crypto.CipherInputStream r2 = com.openfeint.internal.Encryption.decryptionWrap(r2)     // Catch:{ Exception -> 0x00eb, all -> 0x00da }
                java.io.ObjectInputStream r3 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x00eb, all -> 0x00da }
                r3.<init>(r2)     // Catch:{ Exception -> 0x00eb, all -> 0x00da }
                int r1 = r3.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                switch(r1) {
                    case 0: goto L_0x0064;
                    default: goto L_0x0024;
                }     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
            L_0x0024:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r4 = "Unrecognized stream version %d"
                r5 = 1
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r6 = 0
                java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r5[r6] = r1     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r1 = java.lang.String.format(r4, r5)     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.<init>(r1)     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                throw r2     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
            L_0x003a:
                r1 = move-exception
                r2 = r3
            L_0x003c:
                java.lang.String r3 = "OfflineSupport"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e8 }
                java.lang.String r5 = "Couldn't load offline achievements - "
                r4.<init>(r5)     // Catch:{ all -> 0x00e8 }
                java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00e8 }
                java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x00e8 }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e8 }
                com.openfeint.internal.OpenFeintInternal.log(r3, r1)     // Catch:{ all -> 0x00e8 }
                java.util.ArrayList r1 = r0.f94a     // Catch:{ all -> 0x00e8 }
                r1.clear()     // Catch:{ all -> 0x00e8 }
                java.util.ArrayList r1 = r0.b     // Catch:{ all -> 0x00e8 }
                r1.clear()     // Catch:{ all -> 0x00e8 }
                if (r2 == 0) goto L_0x0063
                r2.close()     // Catch:{ IOException -> 0x00e1 }
            L_0x0063:
                return r0
            L_0x0064:
                int r1 = r3.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
            L_0x0068:
                if (r1 > 0) goto L_0x0076
                int r1 = r3.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
            L_0x006e:
                if (r1 > 0) goto L_0x009f
                r3.close()     // Catch:{ IOException -> 0x0074 }
                goto L_0x0063
            L_0x0074:
                r1 = move-exception
                goto L_0x0063
            L_0x0076:
                com.openfeint.internal.offline.OfflineSupport$OfflineAchievement r2 = new com.openfeint.internal.offline.OfflineSupport$OfflineAchievement     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.f95a = r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                float r4 = r3.readFloat()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.b = r4     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                float r4 = r3.readFloat()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.c = r4     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.d = r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.util.ArrayList r4 = r0.f94a     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r4.add(r2)     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                int r1 = r1 + -1
                goto L_0x0068
            L_0x009f:
                com.openfeint.internal.offline.OfflineSupport$OfflineScore r2 = new com.openfeint.internal.offline.OfflineSupport$OfflineScore     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.f96a = r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                long r4 = r3.readLong()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.b = r4     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.c = r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.d = r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.e = r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.Object r8 = r3.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r2.f = r8     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                java.util.ArrayList r4 = r0.b     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                r4.add(r2)     // Catch:{ Exception -> 0x003a, all -> 0x00e5 }
                int r1 = r1 + -1
                goto L_0x006e
            L_0x00da:
                r0 = move-exception
            L_0x00db:
                if (r1 == 0) goto L_0x00e0
                r1.close()     // Catch:{ IOException -> 0x00e3 }
            L_0x00e0:
                throw r0
            L_0x00e1:
                r1 = move-exception
                goto L_0x0063
            L_0x00e3:
                r1 = move-exception
                goto L_0x00e0
            L_0x00e5:
                r0 = move-exception
                r1 = r3
                goto L_0x00db
            L_0x00e8:
                r0 = move-exception
                r1 = r2
                goto L_0x00db
            L_0x00eb:
                r2 = move-exception
                r7 = r2
                r2 = r1
                r1 = r7
                goto L_0x003c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.offline.OfflineSupport.DB.load(java.lang.String):com.openfeint.internal.offline.OfflineSupport$DB");
        }

        public void clear() {
            this.f94a.clear();
            this.b.clear();
        }

        public DB dup() {
            DB db = new DB();
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                db.b.add(((OfflineScore) it.next()).dup());
            }
            Iterator it2 = this.f94a.iterator();
            while (it2.hasNext()) {
                db.f94a.add(((OfflineAchievement) it2.next()).dup());
            }
            return db;
        }

        public OfflineAchievement findAchievement(String str) {
            Iterator it = this.f94a.iterator();
            while (it.hasNext()) {
                OfflineAchievement offlineAchievement = (OfflineAchievement) it.next();
                if (offlineAchievement.f95a.equals(str)) {
                    return offlineAchievement;
                }
            }
            return null;
        }

        public void merge(DB db) {
            Iterator it = db.f94a.iterator();
            while (it.hasNext()) {
                OfflineAchievement offlineAchievement = (OfflineAchievement) it.next();
                OfflineAchievement findAchievement = findAchievement(offlineAchievement.f95a);
                if (findAchievement == null) {
                    this.f94a.add(offlineAchievement.dup());
                } else {
                    if (findAchievement.b < offlineAchievement.b) {
                        findAchievement.b = offlineAchievement.b;
                        findAchievement.d = offlineAchievement.d;
                    }
                    findAchievement.c = Math.max(findAchievement.c, offlineAchievement.c);
                }
            }
            this.b.addAll(db.b);
        }

        public void removeReferencedBlobs() {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                OfflineSupport.deleteDataFile(((OfflineScore) it.next()).e);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4 A[SYNTHETIC, Splitter:B:34:0x00a4] */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00a9 A[SYNTHETIC, Splitter:B:37:0x00a9] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void save(java.lang.String r6) {
            /*
                r5 = this;
                r3 = 0
                com.openfeint.internal.OpenFeintInternal r0 = com.openfeint.internal.OpenFeintInternal.getInstance()     // Catch:{ Exception -> 0x00be, all -> 0x00b7 }
                android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x00be, all -> 0x00b7 }
                r1 = 0
                java.io.FileOutputStream r0 = r0.openFileOutput(r6, r1)     // Catch:{ Exception -> 0x00be, all -> 0x00b7 }
                javax.crypto.CipherOutputStream r1 = com.openfeint.internal.Encryption.encryptionWrap(r0)     // Catch:{ Exception -> 0x00be, all -> 0x00b7 }
                java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00c2, all -> 0x00bb }
                r2.<init>(r1)     // Catch:{ Exception -> 0x00c2, all -> 0x00bb }
                r0 = 0
                r2.writeInt(r0)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.util.ArrayList r0 = r5.f94a     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                int r0 = r0.size()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeInt(r0)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.util.ArrayList r0 = r5.f94a     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
            L_0x002a:
                boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                if (r0 != 0) goto L_0x0051
                java.util.ArrayList r0 = r5.b     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                int r0 = r0.size()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeInt(r0)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.util.ArrayList r0 = r5.b     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
            L_0x003f:
                boolean r3 = r0.hasNext()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                if (r3 != 0) goto L_0x007c
                r2.close()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.close()     // Catch:{ IOException -> 0x00b3 }
            L_0x004b:
                if (r1 == 0) goto L_0x0050
                r1.close()     // Catch:{ IOException -> 0x00b5 }
            L_0x0050:
                return
            L_0x0051:
                java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                com.openfeint.internal.offline.OfflineSupport$OfflineAchievement r0 = (com.openfeint.internal.offline.OfflineSupport.OfflineAchievement) r0     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.lang.String r4 = r0.f95a     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeObject(r4)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                float r4 = r0.b     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeFloat(r4)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                float r4 = r0.c     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeFloat(r4)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.lang.String r0 = r0.d     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeObject(r0)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                goto L_0x002a
            L_0x006c:
                r0 = move-exception
                r0 = r1
                r1 = r2
            L_0x006f:
                if (r1 == 0) goto L_0x0074
                r1.close()     // Catch:{ IOException -> 0x00ad }
            L_0x0074:
                if (r0 == 0) goto L_0x0050
                r0.close()     // Catch:{ IOException -> 0x007a }
                goto L_0x0050
            L_0x007a:
                r0 = move-exception
                goto L_0x0050
            L_0x007c:
                java.lang.Object r5 = r0.next()     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                com.openfeint.internal.offline.OfflineSupport$OfflineScore r5 = (com.openfeint.internal.offline.OfflineSupport.OfflineScore) r5     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.lang.String r3 = r5.f96a     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeObject(r3)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                long r3 = r5.b     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeLong(r3)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.lang.String r3 = r5.c     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeObject(r3)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.lang.String r3 = r5.d     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeObject(r3)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.lang.String r3 = r5.e     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeObject(r3)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                java.lang.String r3 = r5.f     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                r2.writeObject(r3)     // Catch:{ Exception -> 0x006c, all -> 0x00a1 }
                goto L_0x003f
            L_0x00a1:
                r0 = move-exception
            L_0x00a2:
                if (r2 == 0) goto L_0x00a7
                r2.close()     // Catch:{ IOException -> 0x00af }
            L_0x00a7:
                if (r1 == 0) goto L_0x00ac
                r1.close()     // Catch:{ IOException -> 0x00b1 }
            L_0x00ac:
                throw r0
            L_0x00ad:
                r1 = move-exception
                goto L_0x0074
            L_0x00af:
                r2 = move-exception
                goto L_0x00a7
            L_0x00b1:
                r1 = move-exception
                goto L_0x00ac
            L_0x00b3:
                r0 = move-exception
                goto L_0x004b
            L_0x00b5:
                r0 = move-exception
                goto L_0x0050
            L_0x00b7:
                r0 = move-exception
                r1 = r3
                r2 = r3
                goto L_0x00a2
            L_0x00bb:
                r0 = move-exception
                r2 = r3
                goto L_0x00a2
            L_0x00be:
                r0 = move-exception
                r0 = r3
                r1 = r3
                goto L_0x006f
            L_0x00c2:
                r0 = move-exception
                r0 = r1
                r1 = r3
                goto L_0x006f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.offline.OfflineSupport.DB.save(java.lang.String):void");
        }

        public void updateOnUpload(DB db) {
            boolean z;
            Iterator it = db.f94a.iterator();
            while (it.hasNext()) {
                OfflineAchievement offlineAchievement = (OfflineAchievement) it.next();
                OfflineAchievement findAchievement = findAchievement(offlineAchievement.f95a);
                if (findAchievement == null) {
                    findAchievement = offlineAchievement.dup();
                    this.f94a.add(findAchievement);
                }
                findAchievement.c = Math.max(findAchievement.c, offlineAchievement.b);
            }
            ArrayList arrayList = this.b;
            this.b = new ArrayList();
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                OfflineScore offlineScore = (OfflineScore) it2.next();
                if (offlineScore.e != null) {
                    this.b.add(offlineScore);
                } else {
                    Iterator it3 = db.b.iterator();
                    while (true) {
                        if (it3.hasNext()) {
                            if (offlineScore.eq((OfflineScore) it3.next())) {
                                z = true;
                                break;
                            }
                        } else {
                            z = false;
                            break;
                        }
                    }
                    if (!z) {
                        this.b.add(offlineScore);
                    }
                }
            }
        }
    }

    public class OfflineAchievement {

        /* renamed from: a  reason: collision with root package name */
        public String f95a;
        public float b;
        public float c;
        public String d;

        public OfflineAchievement dup() {
            OfflineAchievement offlineAchievement = new OfflineAchievement();
            offlineAchievement.f95a = this.f95a;
            offlineAchievement.b = this.b;
            offlineAchievement.c = this.c;
            offlineAchievement.d = this.d;
            return offlineAchievement;
        }

        public boolean eq(OfflineAchievement offlineAchievement) {
            return OfflineSupport.streq(this.f95a, offlineAchievement.f95a) && this.b == offlineAchievement.b && this.c == offlineAchievement.c && OfflineSupport.streq(this.d, offlineAchievement.d);
        }
    }

    public class OfflineScore {

        /* renamed from: a  reason: collision with root package name */
        public String f96a;
        public long b;
        public String c;
        public String d;
        public String e;
        public String f;

        public OfflineScore dup() {
            OfflineScore offlineScore = new OfflineScore();
            offlineScore.f96a = this.f96a;
            offlineScore.b = this.b;
            offlineScore.c = this.c;
            offlineScore.d = this.d;
            offlineScore.e = this.e;
            offlineScore.f = this.f;
            return offlineScore;
        }

        public boolean eq(OfflineScore offlineScore) {
            return OfflineSupport.streq(this.f96a, offlineScore.f96a) && this.b == offlineScore.b && OfflineSupport.streq(this.c, offlineScore.c) && OfflineSupport.streq(this.d, offlineScore.d) && OfflineSupport.streq(this.e, offlineScore.e) && OfflineSupport.streq(this.f, offlineScore.f);
        }
    }

    /* access modifiers changed from: private */
    public static void deleteDataFile(String str) {
        try {
            Context context = OpenFeintInternal.getInstance().getContext();
            context.deleteFile(context.getFileStreamPath(str).getPath());
        } catch (Exception e) {
        }
    }

    private static String filename(String str) {
        if (str == null) {
            return null;
        }
        String appID = OpenFeintInternal.getInstance().getAppID();
        if (appID == null) {
            return null;
        }
        return "of.offline." + str + "." + appID;
    }

    private static String fullPath(String str) {
        return OpenFeintInternal.getInstance().getContext().getFileStreamPath(str).getPath();
    }

    public static float getClientCompletionPercentage(String str) {
        OfflineAchievement findAchievement = f92a.findAchievement(str);
        if (findAchievement == null) {
            return 0.0f;
        }
        return findAchievement.b;
    }

    private static boolean isUserTemporary() {
        return "0".equals(b);
    }

    private static String now() {
        return DateResourceProperty.f119a.format(new Date());
    }

    public static void postOfflineScore(Score score, Leaderboard leaderboard) {
        OfflineScore offlineScore;
        if (b != null) {
            OfflineScore offlineScore2 = new OfflineScore();
            offlineScore2.f96a = leaderboard.resourceID();
            offlineScore2.b = score.b;
            offlineScore2.c = score.e;
            offlineScore2.d = score.f;
            offlineScore2.f = now();
            if (score.i != null) {
                Iterator it = f92a.b.iterator();
                while (true) {
                    if (it.hasNext()) {
                        offlineScore = (OfflineScore) it.next();
                        if (offlineScore2.f96a.equals(offlineScore.f96a)) {
                            break;
                        }
                    } else {
                        offlineScore = null;
                        break;
                    }
                }
                if (offlineScore != null) {
                    if (leaderboard.d || ((leaderboard.c && offlineScore.b < score.b) || (!leaderboard.c && offlineScore.b > score.b))) {
                        deleteDataFile(offlineScore.e);
                        f92a.b.remove(offlineScore);
                    } else {
                        return;
                    }
                }
                String str = "unknown.blob";
                try {
                    MessageDigest instance = MessageDigest.getInstance("SHA1");
                    instance.update(score.i);
                    str = String.format("%s.blob", new String(c.a(instance.digest())));
                } catch (NoSuchAlgorithmException e) {
                }
                try {
                    Util.saveFile(score.i, fullPath(str));
                    offlineScore2.e = str;
                } catch (IOException e2) {
                    return;
                }
            }
            f92a.b.add(offlineScore2);
            save();
            if (!isUserTemporary()) {
                SimpleNotification.show(OpenFeintInternal.getRString(R.string.of_score_submitted_notification), "@drawable/of_icon_highscore_notification", Notification.Category.HighScore, Notification.Type.Success);
            }
        }
    }

    static final void removeAndUploadNext(OfflineScore offlineScore) {
        f92a.b.remove(offlineScore);
        save();
        uploadScoresWithBlobs();
    }

    private static void removeDBForUser(String str) {
        deleteDataFile(filename(str));
    }

    /* access modifiers changed from: private */
    public static void save() {
        String filename = filename(b);
        if (filename != null) {
            f92a.save(filename);
        }
    }

    public static void setUserDeclined() {
        setUserID(null);
    }

    public static void setUserID(String str) {
        if (str == null || !str.equals(b)) {
            DB load = DB.load(filename(str));
            if (isUserTemporary()) {
                f92a.merge(load);
                deleteDataFile(filename("0"));
            } else {
                f92a = load;
            }
            b = str;
            trySubmitOfflineData();
        }
    }

    public static void setUserTemporary() {
        setUserID("0");
    }

    /* access modifiers changed from: private */
    public static boolean streq(String str, String str2) {
        return str == null ? str2 == null : str.equals(str2);
    }

    public static void trySubmitOfflineData() {
        if (b != null && !b.equals("0") && OpenFeint.isUserLoggedIn()) {
            updateToServer();
        }
    }

    public static void updateClientCompletionPercentage(String str, float f) {
        if (b != null) {
            OfflineAchievement findAchievement = f92a.findAchievement(str);
            if (findAchievement == null) {
                OfflineAchievement offlineAchievement = new OfflineAchievement();
                offlineAchievement.f95a = str;
                offlineAchievement.c = 0.0f;
                offlineAchievement.b = f;
                offlineAchievement.d = now();
                f92a.f94a.add(offlineAchievement);
                save();
            } else if (findAchievement.b < f) {
                findAchievement.b = f;
                findAchievement.d = now();
                save();
            }
        }
    }

    public static void updateServerCompletionPercentage(String str, float f) {
        if (b != null) {
            OfflineAchievement findAchievement = f92a.findAchievement(str);
            if (findAchievement == null) {
                findAchievement = new OfflineAchievement();
                findAchievement.f95a = str;
                findAchievement.b = f;
                f92a.f94a.add(findAchievement);
            }
            findAchievement.c = f;
            findAchievement.d = now();
            save();
        }
    }

    private static synchronized void updateToServer() {
        synchronized (OfflineSupport.class) {
            if (c.compareAndSet(false, true)) {
                final DB dup = f92a.dup();
                OrderedArgList orderedArgList = new OrderedArgList();
                Iterator it = dup.f94a.iterator();
                int i = 0;
                while (it.hasNext()) {
                    OfflineAchievement offlineAchievement = (OfflineAchievement) it.next();
                    if (offlineAchievement.b != offlineAchievement.c) {
                        OpenFeintInternal.log("OfflineSupport", String.format("Updating achievement %s from known %f to %f completion", offlineAchievement.f95a, Float.valueOf(offlineAchievement.c), Float.valueOf(offlineAchievement.b)));
                        orderedArgList.put(String.format("achievements[%d][id]", Integer.valueOf(i)), offlineAchievement.f95a);
                        orderedArgList.put(String.format("achievements[%d][percent_complete]", Integer.valueOf(i)), Float.toString(offlineAchievement.b));
                        orderedArgList.put(String.format("achievements[%d][timestamp]", Integer.valueOf(i)), offlineAchievement.d);
                        i++;
                    }
                }
                Iterator it2 = dup.b.iterator();
                int i2 = 0;
                while (it2.hasNext()) {
                    OfflineScore offlineScore = (OfflineScore) it2.next();
                    if (offlineScore.e == null) {
                        OpenFeintInternal.log("OfflineSupport", String.format("Posting score %d to leaderboard %s", Long.valueOf(offlineScore.b), offlineScore.f96a));
                        orderedArgList.put(String.format("high_scores[%d][leaderboard_id]", Integer.valueOf(i2)), offlineScore.f96a);
                        orderedArgList.put(String.format("high_scores[%d][score]", Integer.valueOf(i2)), Long.toString(offlineScore.b));
                        if (offlineScore.c != null) {
                            orderedArgList.put(String.format("high_scores[%d][display_text]", Integer.valueOf(i2)), offlineScore.c);
                        }
                        if (offlineScore.d != null) {
                            orderedArgList.put(String.format("high_scores[%d][custom_data]", Integer.valueOf(i2)), offlineScore.d);
                        }
                        orderedArgList.put(String.format("high_scores[%d][timestamp]", Integer.valueOf(i2)), offlineScore.f);
                        i2++;
                    }
                }
                if (i == 0 && i2 == 0) {
                    uploadScoresWithBlobs();
                } else {
                    final String str = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/offline_syncs";
                    new BaseRequest(orderedArgList) {
                        public String method() {
                            return "POST";
                        }

                        public void onResponse(int i, byte[] bArr) {
                        }

                        public void onResponseOffMainThread(int i, byte[] bArr) {
                            if (200 > i || i >= 300) {
                                if (i != 0 && 500 > i) {
                                    OfflineSupport.f92a.removeReferencedBlobs();
                                    OfflineSupport.f92a.clear();
                                    OfflineSupport.save();
                                }
                                OfflineSupport.c.set(false);
                                return;
                            }
                            OfflineSupport.f92a.updateOnUpload(dup);
                            OfflineSupport.save();
                            OfflineSupport.uploadScoresWithBlobs();
                        }

                        public String path() {
                            return str;
                        }
                    }.launch();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void uploadScoresWithBlobs() {
        Iterator it = f92a.b.iterator();
        while (it.hasNext()) {
            final OfflineScore offlineScore = (OfflineScore) it.next();
            if (offlineScore.e != null) {
                Score score = new Score(offlineScore.b);
                score.f = offlineScore.d;
                score.e = offlineScore.c;
                try {
                    score.i = Util.readWholeFile(fullPath(offlineScore.e));
                } catch (IOException e) {
                }
                if (score.i == null) {
                    removeAndUploadNext(offlineScore);
                    return;
                } else {
                    score.submitToFromOffline(new Leaderboard(offlineScore.f96a), offlineScore.f, new Score.SubmitToCB() {
                        public void onFailure(String str) {
                            OfflineSupport.c.set(false);
                        }

                        public void onSuccess(boolean z) {
                            OfflineSupport.removeAndUploadNext(OfflineScore.this);
                        }
                    });
                    return;
                }
            }
        }
        c.set(false);
    }
}
