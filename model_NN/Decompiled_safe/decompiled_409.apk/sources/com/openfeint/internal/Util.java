package com.openfeint.internal;

import a.a.a.n;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.openfeint.internal.resource.ResourceClass;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Util {

    /* renamed from: a  reason: collision with root package name */
    private static int f87a = Integer.valueOf(Build.VERSION.SDK).intValue();

    public static void copyDirectory(File file, File file2) {
        if (file.isDirectory()) {
            if (!file2.exists()) {
                file2.mkdir();
            }
            String[] list = file.list();
            for (int i = 0; i < list.length; i++) {
                copyDirectory(new File(file, list[i]), new File(file2, list[i]));
            }
            return;
        }
        copyFile(file, file2);
    }

    public static void copyFile(File file, File file2) {
        copyStream(new FileInputStream(file), new FileOutputStream(file2));
    }

    public static void copyStream(InputStream inputStream, OutputStream outputStream) {
        copyStreamAndLeaveInputOpen(inputStream, outputStream);
        inputStream.close();
    }

    public static void copyStreamAndLeaveInputOpen(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[16384];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                outputStream.close();
                return;
            }
            outputStream.write(bArr, 0, read);
        }
    }

    public static void createSymbolic(String str, String str2) {
        run("ln -s " + str + " " + str2);
    }

    public static void deleteFiles(File file) {
        if (file.isDirectory()) {
            for (String file2 : file.list()) {
                deleteFiles(new File(file, file2));
            }
        }
        file.delete();
    }

    public static DisplayMetrics getDisplayMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) OpenFeintInternal.getInstance().getContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static String getDpiName(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.density >= 2.0f ? "udpi" : ((double) displayMetrics.density) >= 1.5d ? "hdpi" : "mdpi";
    }

    public static Object getObjFromJson(byte[] bArr) {
        try {
            return new JsonResourceParser(new n((byte) 0).a(bArr)).parse();
        } catch (Exception e) {
            OpenFeintInternal.log("Util", e.getMessage());
            return null;
        }
    }

    public static Object getObjFromJson(byte[] bArr, ResourceClass resourceClass) {
        n nVar = new n((byte) 0);
        OpenFeintInternal.log("Util", new String(bArr));
        try {
            return new JsonResourceParser(nVar.a(bArr)).parse(resourceClass);
        } catch (Exception e) {
            OpenFeintInternal.log("Util", String.valueOf(e.getMessage()) + "json error");
            return null;
        }
    }

    public static Object getObjFromJsonFile(String str) {
        try {
            return getObjFromJsonStream(new FileInputStream(new File(str)));
        } catch (Exception e) {
            return null;
        }
    }

    public static Object getObjFromJsonStream(InputStream inputStream) {
        Object parse = new JsonResourceParser(new n((byte) 0).a(inputStream)).parse();
        inputStream.close();
        return parse;
    }

    public static boolean isPadVersion() {
        return f87a >= 11;
    }

    public static boolean isSymblic(File file) {
        try {
            return !file.getCanonicalPath().equals(file.getAbsolutePath());
        } catch (IOException e) {
            return false;
        }
    }

    public static void moveWebCache(Context context) {
        if (noSdcardPermission(context)) {
            File file = new File(context.getCacheDir(), "webviewCache");
            if (!isSymblic(file) && "mounted".equals(Environment.getExternalStorageState())) {
                File file2 = new File(Environment.getExternalStorageDirectory(), "openfeint/cache");
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                deleteFiles(file);
                createSymbolic(file2.getAbsolutePath(), file.getAbsolutePath());
            }
        }
    }

    public static boolean noPermission(String str, Context context) {
        return -1 == context.getPackageManager().checkPermission(str, context.getPackageName());
    }

    public static boolean noSdcardPermission() {
        return noSdcardPermission(OpenFeintInternal.getInstance().getContext());
    }

    public static boolean noSdcardPermission(Context context) {
        return noPermission("android.permission.WRITE_EXTERNAL_STORAGE", context);
    }

    public static byte[] readWholeFile(String str) {
        File file = new File(str);
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bArr = new byte[((int) file.length())];
        fileInputStream.read(bArr);
        fileInputStream.close();
        return bArr;
    }

    public static void run(String str) {
        try {
            Runtime.getRuntime().exec(str);
            OpenFeintInternal.log("Util", str);
        } catch (Exception e) {
            OpenFeintInternal.log("Util", e.getMessage());
        }
    }

    public static void saveFile(byte[] bArr, String str) {
        File file = new File(str);
        file.getParentFile().mkdirs();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(bArr);
        fileOutputStream.close();
    }

    public static void saveStream(InputStream inputStream, String str) {
        saveStreamAndLeaveInputOpen(inputStream, str);
        inputStream.close();
    }

    public static void saveStreamAndLeaveInputOpen(InputStream inputStream, String str) {
        File file = new File(str);
        file.getParentFile().mkdirs();
        copyStreamAndLeaveInputOpen(inputStream, new FileOutputStream(file));
    }

    public static boolean sdcardReady(Context context) {
        if (!noSdcardPermission(context)) {
            return false;
        }
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static void setOrientation(Activity activity) {
        Integer num = (Integer) OpenFeintInternal.getInstance().getSettings().get("RequestedOrientation");
        if (num != null) {
            activity.setRequestedOrientation(num.intValue());
        }
    }

    public static final byte[] toByteArray(InputStream inputStream) {
        byte[] bArr = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                byteArrayOutputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }
}
