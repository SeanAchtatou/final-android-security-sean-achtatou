package com.openfeint.internal.ui;

public abstract class WebViewCacheCallback {
    public abstract void failLoaded();

    public void onTrackingNeeded() {
    }

    public abstract void pathLoaded(String str);
}
