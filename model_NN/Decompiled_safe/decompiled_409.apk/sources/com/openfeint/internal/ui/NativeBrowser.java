package com.openfeint.internal.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.concurrent.atomic.AtomicBoolean;

public class NativeBrowser extends NestedWindow {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Handler f129a;
    /* access modifiers changed from: private */
    public Runnable b;
    /* access modifiers changed from: private */
    public AtomicBoolean c = new AtomicBoolean(false);

    public final class JSInterface {
        public JSInterface() {
        }

        public final void returnValue(final String str) {
            NativeBrowser.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (NativeBrowser.this.c.compareAndSet(false, true)) {
                        Intent intent = new Intent();
                        if (str != null) {
                            intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.result", str);
                        }
                        NativeBrowser.this.setResult(-1, intent);
                        NativeBrowser.this.finish();
                    }
                }
            });
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        String string = extras.getString("com.openfeint.internal.ui.NativeBrowser.argument.src");
        String string2 = extras.getString("com.openfeint.internal.ui.NativeBrowser.argument.timeout");
        this.f.getSettings().setJavaScriptEnabled(true);
        this.f.addJavascriptInterface(new JSInterface(), "NativeBrowser");
        this.f.setWebViewClient(new WebViewClient() {
            private void clearTimeout() {
                if (NativeBrowser.this.f129a != null && NativeBrowser.this.b != null) {
                    NativeBrowser.this.f129a.removeCallbacks(NativeBrowser.this.b);
                    NativeBrowser.this.f129a = null;
                    NativeBrowser.this.b = null;
                }
            }

            public void onPageFinished(WebView webView, String str) {
                clearTimeout();
                super.onPageFinished(webView, str);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onReceivedError(WebView webView, int i, String str, String str2) {
                clearTimeout();
                super.onReceivedError(webView, i, str, str2);
                Intent intent = new Intent();
                intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failed", true);
                intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_code", i);
                intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_desc", str);
                NativeBrowser.this.setResult(-1, intent);
                NativeBrowser.this.finish();
            }
        });
        this.f.setWebChromeClient(new WebChromeClient());
        this.f.loadUrl(string);
        if (string2 != null) {
            this.f129a = new Handler();
            this.b = new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                public void run() {
                    if (NativeBrowser.this.c.compareAndSet(false, true)) {
                        Intent intent = new Intent();
                        intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failed", true);
                        intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_code", 0);
                        intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_desc", "Timeout");
                        NativeBrowser.this.setResult(-1, intent);
                        NativeBrowser.this.finish();
                    }
                }
            };
            this.f129a.postDelayed(this.b, (long) Integer.parseInt(string2));
        }
        fade(true);
    }
}
