package com.openfeint.internal.ui;

import a.a.a.n;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.api.resource.Score;
import com.openfeint.api.ui.Dashboard;
import com.openfeint.internal.ImagePicker;
import com.openfeint.internal.JsonResourceParser;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.resource.ScoreBlobDelegate;
import hamon05.speed.pac.R;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebNav extends NestedWindow {

    /* renamed from: a  reason: collision with root package name */
    private WebNavClient f139a;
    boolean b = false;
    boolean c = false;
    protected int d;
    protected ArrayList e = new ArrayList();
    private ActionHandler g;
    private Dialog h;
    private boolean i = true;
    /* access modifiers changed from: private */
    public Map j = null;

    public class ActionHandler {

        /* renamed from: a  reason: collision with root package name */
        WebNav f148a;
        private List b = new ArrayList();

        public ActionHandler(WebNav webNav) {
            this.f148a = webNav;
            populateActionList(this.b);
        }

        private Map parseQueryString(Uri uri) {
            return parseQueryString(uri.getEncodedQuery());
        }

        private Map parseQueryString(String str) {
            HashMap hashMap = new HashMap();
            if (str != null) {
                for (String split : str.split("&")) {
                    String[] split2 = split.split("=");
                    if (split2.length == 2) {
                        hashMap.put(split2[0], Uri.decode(split2[1]));
                    } else {
                        hashMap.put(split2[0], null);
                    }
                }
            }
            return hashMap;
        }

        public void alert(Map map) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.f148a);
            builder.setTitle((CharSequence) map.get("title"));
            builder.setMessage((CharSequence) map.get("message"));
            builder.setNegativeButton(OpenFeintInternal.getRString(R.string.of_ok), (DialogInterface.OnClickListener) null);
            builder.show();
        }

        public void apiRequest(Map map) {
            final String str = (String) map.get("request_id");
            OpenFeintInternal.genericRequest((String) map.get("path"), (String) map.get("method"), parseQueryString((String) map.get("params")), parseQueryString((String) map.get("httpParams")), new IRawRequestDelegate() {
                public void onResponse(int i, String str) {
                    String trim = str.trim();
                    if (trim.length() == 0) {
                        trim = "{}";
                    }
                    ActionHandler.this.f148a.executeJavascript(String.format("OF.api.completeRequest(\"%s\", \"%d\", %s)", str, Integer.valueOf(i), trim));
                }
            });
        }

        public void back(Map map) {
            this.f148a.fade(false);
            String str = (String) map.get("root");
            if (str != null && !str.equals("false")) {
                this.f148a.d = 1;
            }
            if (this.f148a.d > 1) {
                this.f148a.d--;
            }
        }

        public void contentLoaded(Map map) {
            if (map.get("keepLoader") == null || !((String) map.get("keepLoader")).equals("true")) {
                hideLoader(null);
                WebNav.this.setTitle((CharSequence) map.get("title"));
            }
            this.f148a.fade(true);
            WebNav.this.dismissDialog();
        }

        public void dashboard(Map map) {
            Dashboard.openFromSpotlight();
        }

        public void dismiss(Map map) {
            WebNav.this.finish();
        }

        public void dispatch(Uri uri) {
            if (uri.getHost().equals("action")) {
                Map parseQueryString = parseQueryString(uri);
                String replaceFirst = uri.getPath().replaceFirst("/", "");
                if (!replaceFirst.equals("log")) {
                    HashMap hashMap = new HashMap(parseQueryString);
                    String str = (String) parseQueryString.get("params");
                    if (str != null && str.contains("password")) {
                        hashMap.put("params", "---FILTERED---");
                    }
                    OpenFeintInternal.log("WebUI", "ACTION: " + replaceFirst + " " + hashMap.toString());
                }
                if (this.b.contains(replaceFirst)) {
                    try {
                        getClass().getMethod(replaceFirst, Map.class).invoke(this, parseQueryString);
                    } catch (NoSuchMethodException e) {
                        OpenFeintInternal.log("WebUI", "mActionList contains this method, but it is not implemented: " + replaceFirst);
                    } catch (Exception e2) {
                        OpenFeintInternal.log("WebUI", "Unhandled Exception: " + e2.toString() + "   " + e2.getCause());
                    }
                } else {
                    OpenFeintInternal.log("WebUI", "UNHANDLED ACTION: " + replaceFirst);
                }
            } else {
                OpenFeintInternal.log("WebUI", "UNHANDLED MESSAGE TYPE: " + uri.getHost());
            }
        }

        public void downloadBlob(Map map) {
            String str = (String) map.get("score");
            final String str2 = (String) map.get("onError");
            final String str3 = (String) map.get("onSuccess");
            try {
                Object parse = new JsonResourceParser(new n((byte) 0).a(new StringReader(str))).parse();
                if (parse != null && (parse instanceof Score)) {
                    final Score score = (Score) parse;
                    score.downloadBlob(new Score.DownloadBlobCB() {
                        public void onFailure(String str) {
                            if (str2 != null) {
                                WebNav.this.executeJavascript(String.format("%s(%s)", str2, str));
                            }
                        }

                        public void onSuccess() {
                            if (str3 != null) {
                                WebNav.this.executeJavascript(String.format("%s()", str3));
                            }
                            ScoreBlobDelegate.notifyBlobDownloaded(score);
                        }
                    });
                }
            } catch (Exception e) {
                if (str2 != null) {
                    WebNav.this.executeJavascript(String.format("%s(%s)", str2, e.getLocalizedMessage()));
                }
            }
        }

        /* access modifiers changed from: protected */
        public List getActionList() {
            return this.b;
        }

        public void hideLoader(Map map) {
        }

        public void isApplicationInstalled(Map map) {
            List<ApplicationInfo> installedApplications = this.f148a.getPackageManager().getInstalledApplications(0);
            String str = (String) map.get("package_name");
            boolean z = false;
            for (ApplicationInfo applicationInfo : installedApplications) {
                if (applicationInfo.packageName.equals(str)) {
                    z = true;
                }
            }
            WebNav webNav = WebNav.this;
            Object[] objArr = new Object[2];
            objArr[0] = map.get("callback");
            objArr[1] = z ? "true" : "false";
            webNav.executeJavascript(String.format("%s(%s)", objArr));
        }

        public void log(Map map) {
            if (((String) map.get("message")) != null) {
                OpenFeintInternal.log("WebUI", "WEBLOG: " + ((String) map.get("message")));
            }
        }

        public void openBrowser(Map map) {
            Intent intent = new Intent(this.f148a, NativeBrowser.class);
            WebNav.this.j = new HashMap();
            for (String str : new String[]{"src", "callback", "on_cancel", "on_failure", "timeout"}) {
                String str2 = (String) map.get(str);
                if (str2 != null) {
                    WebNav.this.j.put(str, str2);
                    intent.putExtra("com.openfeint.internal.ui.NativeBrowser.argument." + str, str2);
                }
            }
            WebNav.this.startActivityForResult(intent, 25565);
        }

        public void openMarket(Map map) {
            this.f148a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + ((String) map.get("package_name")))));
        }

        public void openYoutubePlayer(Map map) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("vnd.youtube:" + ((String) map.get("video_id"))));
            if (WebNav.this.getPackageManager().queryIntentActivities(intent, 65536).size() == 0) {
                Toast.makeText(this.f148a, OpenFeintInternal.getRString(R.string.of_no_video), 0).show();
            } else {
                this.f148a.startActivity(intent);
            }
        }

        /* access modifiers changed from: protected */
        public void populateActionList(List list) {
            list.add("log");
            list.add("apiRequest");
            list.add("contentLoaded");
            list.add("startLoading");
            list.add("back");
            list.add("showLoader");
            list.add("hideLoader");
            list.add("alert");
            list.add("dismiss");
            list.add("openMarket");
            list.add("isApplicationInstalled");
            list.add("openYoutubePlayer");
            list.add("profilePicture");
            list.add("openBrowser");
            list.add("downloadBlob");
            list.add("dashboard");
            list.add("readSetting");
            list.add("writeSetting");
        }

        public final void profilePicture(Map map) {
            ImagePicker.show(WebNav.this);
        }

        public void readSetting(Map map) {
            String str = (String) map.get("key");
            String str2 = (String) map.get("callback");
            if (str2 != null) {
                String string = OpenFeintInternal.getInstance().getContext().getSharedPreferences("OFWebUI", 0).getString(str != null ? "OFWebUISetting_" + str : null, null);
                OpenFeintInternal.log("WebUI", String.format("readSetting(%s) => %s", str, string));
                WebNav webNav = WebNav.this;
                Object[] objArr = new Object[2];
                objArr[0] = str2;
                objArr[1] = string != null ? string : "null";
                webNav.executeJavascript(String.format("%s(%s)", objArr));
            }
        }

        public void showLoader(Map map) {
        }

        public void startLoading(Map map) {
            this.f148a.fade(false);
            showLoader(null);
            WebViewCache.trackPath((String) map.get("path"), new WebViewCacheCallback() {
                public void failLoaded() {
                    WebNav.this.closeForDiskError();
                }

                public void onTrackingNeeded() {
                    WebNav.this.showDialog();
                }

                public void pathLoaded(String str) {
                    WebNav.this.executeJavascript("OF.navigateToUrlCallback()");
                }
            });
            this.f148a.d++;
        }

        public void writeSetting(Map map) {
            String str = (String) map.get("key");
            String str2 = (String) map.get("value");
            if (str != null && str2 != null) {
                SharedPreferences.Editor edit = OpenFeintInternal.getInstance().getContext().getSharedPreferences("OFWebUI", 0).edit();
                edit.putString("OFWebUISetting_" + str, str2);
                edit.commit();
            }
        }
    }

    class WebNavChromeClient extends WebChromeClient {
        private WebNavChromeClient() {
        }

        /* synthetic */ WebNavChromeClient(WebNav webNav, WebNavChromeClient webNavChromeClient) {
            this();
        }

        public void onConsoleMessage(String str, int i, String str2) {
            if (!WebNav.this.c) {
                WebNav.this.e.add(String.format("%s at %s:%d)", str, str2, Integer.valueOf(i)));
            }
        }

        public boolean onJsAlert(WebView webView, String str, String str2, final JsResult jsResult) {
            new AlertDialog.Builder(webView.getContext()).setMessage(str2).setNegativeButton(OpenFeintInternal.getRString(R.string.of_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsResult.cancel();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    jsResult.cancel();
                }
            }).show();
            return true;
        }

        public boolean onJsConfirm(WebView webView, String str, String str2, final JsResult jsResult) {
            new AlertDialog.Builder(webView.getContext()).setMessage(str2).setPositiveButton(OpenFeintInternal.getRString(R.string.of_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsResult.confirm();
                }
            }).setNegativeButton(OpenFeintInternal.getRString(R.string.of_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsResult.cancel();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    jsResult.cancel();
                }
            }).show();
            return true;
        }
    }

    public class WebNavClient extends WebViewClient {

        /* renamed from: a  reason: collision with root package name */
        private ActionHandler f158a;

        public WebNavClient(ActionHandler actionHandler) {
            this.f158a = actionHandler;
        }

        /* access modifiers changed from: protected */
        public void attemptRecovery(WebView webView, String str) {
            if (WebViewCache.recover()) {
                WebNav.this.load(true);
                new AlertDialog.Builder(webView.getContext()).setMessage(OpenFeintInternal.getRString(R.string.of_crash_report_query)).setNegativeButton(OpenFeintInternal.getRString(R.string.of_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        WebNav.this.finish();
                    }
                }).setPositiveButton(OpenFeintInternal.getRString(R.string.of_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        WebNavClient.this.submitCrashReport();
                    }
                }).show();
            } else if (!WebViewCache.isDiskError()) {
                WebNav.this.finish();
            }
        }

        public void loadInitialContent() {
            OpenFeintInternal instance = OpenFeintInternal.getInstance();
            CurrentUser currentUser = instance.getCurrentUser();
            int i = WebNav.this.getResources().getConfiguration().orientation;
            HashMap hashMap = new HashMap();
            if (currentUser != null) {
                hashMap.put("id", currentUser.resourceID());
                hashMap.put("name", currentUser.f69a);
            }
            HashMap hashMap2 = new HashMap();
            hashMap2.put("id", instance.getAppID());
            hashMap2.put("name", instance.getAppName());
            hashMap2.put("version", Integer.toString(instance.getAppVersion()));
            Map deviceParams = OpenFeintInternal.getInstance().getDeviceParams();
            HashMap hashMap3 = new HashMap();
            hashMap3.put("platform", "android");
            hashMap3.put("clientVersion", instance.getOFVersion());
            hashMap3.put("hasNativeInterface", true);
            hashMap3.put("dpi", Util.getDpiName(WebNav.this));
            hashMap3.put("locale", WebNav.this.getResources().getConfiguration().locale.toString());
            hashMap3.put("user", new JSONObject(hashMap));
            hashMap3.put("game", new JSONObject(hashMap2));
            hashMap3.put("device", new JSONObject(deviceParams));
            hashMap3.put("actions", new JSONArray((Collection) WebNav.this.getActionHandler().getActionList()));
            hashMap3.put("orientation", i == 2 ? "landscape" : "portrait");
            hashMap3.put("serverUrl", instance.getServerUrl());
            WebNav.this.executeJavascript(String.format("OF.init.clientBoot(%s)", new JSONObject(hashMap3).toString()));
            this.f158a.f148a.loadInitialContent();
        }

        public void onPageFinished(WebView webView, String str) {
            if (!WebNav.this.b) {
                WebNav.this.b = true;
                if (WebNav.this.c) {
                    loadInitialContent();
                } else {
                    attemptRecovery(webView, str);
                }
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            this.f158a.hideLoader(null);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Uri parse = Uri.parse(str);
            if (parse.getScheme().equals("http") || parse.getScheme().equals("https")) {
                webView.loadUrl(str);
                return true;
            } else if (parse.getScheme().equals("openfeint")) {
                this.f158a.dispatch(parse);
                return true;
            } else {
                OpenFeintInternal.log("WebUI", "UNHANDLED PROTOCOL: " + parse.getScheme());
                return true;
            }
        }

        /* access modifiers changed from: protected */
        public void submitCrashReport() {
            HashMap hashMap = new HashMap();
            hashMap.put("console", new JSONArray((Collection) WebNav.this.e));
            JSONObject jSONObject = new JSONObject(hashMap);
            HashMap hashMap2 = new HashMap();
            hashMap2.put("crash_report", jSONObject.toString());
            OpenFeintInternal.genericRequest("/webui/crash_report", "POST", hashMap2, null, null);
        }
    }

    /* access modifiers changed from: private */
    public void closeForDiskError() {
        runOnUiThread(new Runnable() {
            public void run() {
                WebNav.this.dismissDialog();
                new AlertDialog.Builder(WebNav.this).setMessage(String.format(OpenFeintInternal.getRString(R.string.of_nodisk), Util.sdcardReady(WebNav.this) ? OpenFeintInternal.getRString(R.string.of_sdcard) : OpenFeintInternal.getRString(R.string.of_device))).setPositiveButton(OpenFeintInternal.getRString(R.string.of_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        WebNav.this.finish();
                    }
                }).show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void dismissDialog() {
        if (this.h.isShowing()) {
            this.h.dismiss();
        }
    }

    private static final String jsQuotedStringLiteral(String str) {
        return str == null ? "''" : "'" + str.replace("\\", "\\\\").replace("'", "\\'") + "'";
    }

    /* access modifiers changed from: private */
    public void showDialog() {
        if (!this.h.isShowing()) {
            this.h.show();
        }
    }

    /* access modifiers changed from: protected */
    public ActionHandler createActionHandler(WebNav webNav) {
        return new ActionHandler(webNav);
    }

    /* access modifiers changed from: protected */
    public WebNavClient createWebNavClient(ActionHandler actionHandler) {
        return new WebNavClient(actionHandler);
    }

    public void executeJavascript(String str) {
        if (this.f != null) {
            this.f.loadUrl("javascript:" + str);
        }
    }

    public ActionHandler getActionHandler() {
        return this.g;
    }

    public Dialog getLaunchLoadingView() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public String initialContentPath() {
        String stringExtra = getIntent().getStringExtra("content_path");
        if (stringExtra != null) {
            return stringExtra;
        }
        throw new RuntimeException("WebNav intent requires extra value 'content_path'");
    }

    /* access modifiers changed from: protected */
    public void load(final boolean z) {
        this.b = false;
        WebViewCache.trackPath(rootPage(), new WebViewCacheCallback() {
            public void failLoaded() {
                WebNav.this.closeForDiskError();
            }

            public void pathLoaded(String str) {
                if (WebNav.this.f != null) {
                    String itemUri = WebViewCache.getItemUri(str);
                    OpenFeintInternal.log("WebUI", "Loading URL: " + itemUri);
                    if (z) {
                        WebNav.this.f.reload();
                    } else {
                        WebNav.this.f.loadUrl(itemUri);
                    }
                }
            }
        });
    }

    public void loadInitialContent() {
        String initialContentPath = initialContentPath();
        if (initialContentPath.contains("?")) {
            initialContentPath = initialContentPath.split("\\?")[0];
        }
        if (!initialContentPath.endsWith(".json")) {
            initialContentPath = String.valueOf(initialContentPath) + ".json";
        }
        WebViewCache.trackPath(initialContentPath, new WebViewCacheCallback() {
            public void failLoaded() {
                WebNav.this.closeForDiskError();
            }

            public void pathLoaded(String str) {
                WebNav.this.executeJavascript("OF.navigateToUrl('" + WebNav.this.initialContentPath() + "')");
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Bitmap onImagePickerActivityResult;
        super.onActivityResult(i2, i3, intent);
        if (this.j != null && i2 == 25565) {
            if (i3 != 0) {
                this.i = false;
                if (intent.getBooleanExtra("com.openfeint.internal.ui.NativeBrowser.argument.failed", false)) {
                    String str = (String) this.j.get("on_failure");
                    if (str != null) {
                        executeJavascript(String.format("%s(%d, %s)", str, Integer.valueOf(intent.getIntExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_code", 0)), jsQuotedStringLiteral(intent.getStringExtra("com.openfeint.internal.ui.NativeBrowser.argument.failure_desc"))));
                    }
                } else {
                    String str2 = (String) this.j.get("callback");
                    if (str2 != null) {
                        String stringExtra = intent.getStringExtra("com.openfeint.internal.ui.NativeBrowser.argument.result");
                        Object[] objArr = new Object[2];
                        objArr[0] = str2;
                        objArr[1] = stringExtra != null ? stringExtra : "";
                        executeJavascript(String.format("%s(%s)", objArr));
                    }
                }
            } else {
                String str3 = (String) this.j.get("on_cancel");
                if (str3 != null) {
                    executeJavascript(String.format("%s()", str3));
                }
            }
            this.j = null;
        } else if (ImagePicker.isImagePickerActivityResult(i2) && (onImagePickerActivityResult = ImagePicker.onImagePickerActivityResult(this, i3, 152, intent)) != null) {
            ImagePicker.compressAndUpload(onImagePickerActivityResult, "/xp/users/" + OpenFeintInternal.getInstance().getCurrentUser().resourceID() + "/profile_picture", new OpenFeintInternal.IUploadDelegate() {
                public void fileUploadedTo(String str, boolean z) {
                    if (z) {
                        WebNav.this.executeJavascript("try { OF.page.onProfilePictureChanged('" + str + "'); } catch (e) {}");
                    }
                }
            });
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        executeJavascript(String.format("OF.setOrientation('%s');", configuration.orientation == 2 ? "landscape" : "portrait"));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        OpenFeintInternal.log("WebUI", "--- WebUI Bootup ---");
        this.d = 0;
        this.f.getSettings().setJavaScriptEnabled(true);
        this.f.getSettings().setPluginsEnabled(false);
        this.f.setScrollBarStyle(33554432);
        this.f.getSettings().setCacheMode(2);
        this.h = new Dialog(this, R.style.OFLoading);
        this.h.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                WebNav.this.finish();
            }
        });
        this.h.setCancelable(true);
        this.h.setContentView((int) R.layout.of_native_loader);
        ProgressBar progressBar = (ProgressBar) this.h.findViewById(R.id.progress);
        progressBar.setIndeterminate(true);
        progressBar.setIndeterminateDrawable(OpenFeintInternal.getInstance().getContext().getResources().getDrawable(R.drawable.of_native_loader_progress));
        this.g = createActionHandler(this);
        this.f139a = createWebNavClient(this.g);
        this.f.setWebViewClient(this.f139a);
        this.f.setWebChromeClient(new WebNavChromeClient(this, null));
        this.f.addJavascriptInterface(new Object() {
            public void action(final String str) {
                WebNav.this.runOnUiThread(new Runnable() {
                    public void run() {
                        WebNav.this.getActionHandler().dispatch(Uri.parse(str));
                    }
                });
            }

            public void frameworkLoaded() {
                WebNav.this.setFrameworkLoaded(true);
            }
        }, "NativeInterface");
        String initialContentPath = initialContentPath();
        if (initialContentPath.contains("?")) {
            initialContentPath = initialContentPath.split("\\?")[0];
        }
        if (!initialContentPath.endsWith(".json")) {
            initialContentPath = String.valueOf(initialContentPath) + ".json";
        }
        WebViewCache.prioritize(initialContentPath);
        load(false);
        this.h.show();
    }

    public void onDestroy() {
        this.f.destroy();
        this.f = null;
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 84) {
            executeJavascript(String.format("OF.menu('%s')", "search"));
            return true;
        } else if (i2 != 4 || this.d <= 1) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            executeJavascript("OF.goBack()");
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        OpenFeintInternal.restoreInstanceState(bundle);
    }

    public void onResume() {
        super.onResume();
        CurrentUser currentUser = OpenFeintInternal.getInstance().getCurrentUser();
        if (currentUser != null && this.c) {
            executeJavascript(String.format("if (OF.user) { OF.user.name = %s; OF.user.id = '%s'; }", jsQuotedStringLiteral(currentUser.f69a), currentUser.resourceID()));
            if (this.i) {
                executeJavascript("if (OF.page) OF.refresh();");
            }
        }
        this.i = true;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        OpenFeintInternal.saveInstanceState(bundle);
    }

    public void onStop() {
        super.onStop();
        dismissDialog();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        OpenFeintDelegate delegate = OpenFeintInternal.getInstance().getDelegate();
        if (delegate == null) {
            return;
        }
        if (z) {
            delegate.onDashboardAppear();
        } else {
            delegate.onDashboardDisappear();
        }
    }

    /* access modifiers changed from: protected */
    public String rootPage() {
        return "index.html";
    }

    /* access modifiers changed from: protected */
    public void setFrameworkLoaded(boolean z) {
        this.c = z;
    }
}
