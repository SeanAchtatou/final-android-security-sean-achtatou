package com.openfeint.internal.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;
import com.openfeint.api.OpenFeint;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.ui.WebNav;
import hamon05.speed.pac.R;
import java.util.List;
import java.util.Map;

public class Settings extends WebNav {

    /* renamed from: a  reason: collision with root package name */
    private String f136a;

    class SettingsActionHandler extends WebNav.ActionHandler {
        public SettingsActionHandler(WebNav webNav) {
            super(webNav);
        }

        public void apiRequest(Map map) {
            super.apiRequest(map);
            OpenFeint.getCurrentUser().load(null);
        }

        public final void introFlow(Map map) {
            Settings.this.startActivity(new Intent(Settings.this, IntroFlow.class).putExtra("content_name", "login?mode=switch"));
        }

        public final void logout(Map map) {
            OpenFeintInternal.getInstance().logoutUser(new IRawRequestDelegate() {
                public void onResponse(int i, String str) {
                    Settings.this.finish();
                }
            });
        }

        /* access modifiers changed from: protected */
        public void populateActionList(List list) {
            super.populateActionList(list);
            list.add("logout");
            list.add("introFlow");
        }
    }

    public static void open() {
        Context context = OpenFeintInternal.getInstance().getContext();
        Intent intent = new Intent(context, Settings.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public WebNav.ActionHandler createActionHandler(WebNav webNav) {
        return new SettingsActionHandler(webNav);
    }

    /* access modifiers changed from: protected */
    public String initialContentPath() {
        return "settings/index";
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 10009 && intent != null) {
            Toast.makeText(this, OpenFeintInternal.getRString(R.string.of_profile_pic_changed), 0).show();
        }
    }

    public void onResume() {
        if (this.f136a == null) {
            this.f136a = OpenFeint.getCurrentUser().resourceID();
        } else if (!this.f136a.equals(OpenFeint.getCurrentUser().resourceID())) {
            new AlertDialog.Builder(this).setTitle(OpenFeintInternal.getRString(R.string.of_switched_accounts)).setMessage(String.format(OpenFeintInternal.getRString(R.string.of_now_logged_in_as_format), OpenFeint.getCurrentUser().f69a)).setNegativeButton(OpenFeintInternal.getRString(R.string.of_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Settings.this.finish();
                }
            }).show();
        }
        super.onResume();
    }
}
