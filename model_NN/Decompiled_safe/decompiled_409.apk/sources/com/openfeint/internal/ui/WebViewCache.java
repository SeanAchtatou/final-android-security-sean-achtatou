package com.openfeint.internal.ui;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.openfeint.api.ui.Dashboard;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.db.DB;
import com.openfeint.internal.request.BaseRequest;
import com.openfeint.internal.request.CacheRequest;
import com.openfeint.internal.request.OrderedArgList;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class WebViewCache {

    /* renamed from: a  reason: collision with root package name */
    static WebViewCache f161a;
    private static URI g;
    private static String h;
    private static String i;
    /* access modifiers changed from: private */
    public static String j;
    private static boolean s = false;
    Handler b;
    boolean c = false;
    ManifestData d;
    Map e;
    Context f;
    private Handler k;
    private Set l;
    private Map m;
    private boolean n = false;
    private Set o;
    private Set p;
    private boolean q = false;
    private URI r = getServerURI();

    class ItemAndCallback {

        /* renamed from: a  reason: collision with root package name */
        public final ManifestItem f166a;
        public final WebViewCacheCallback b;

        public ItemAndCallback(ManifestItem manifestItem, WebViewCacheCallback webViewCacheCallback) {
            this.f166a = manifestItem;
            this.b = webViewCacheCallback;
        }
    }

    class ManifestData {

        /* renamed from: a  reason: collision with root package name */
        Set f167a = new HashSet();
        Map b = new HashMap();

        ManifestData(SQLiteDatabase sQLiteDatabase) {
            Cursor cursor;
            Cursor cursor2;
            ManifestItem manifestItem;
            try {
                cursor = sQLiteDatabase.rawQuery("SELECT path, hash, is_global FROM server_manifest", null);
                try {
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        do {
                            String string = cursor.getString(0);
                            String string2 = cursor.getString(1);
                            boolean z = cursor.getInt(2) != 0;
                            this.b.put(string, new ManifestItem(string, string2));
                            if (z) {
                                this.f167a.add(string);
                            }
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                    Cursor cursor3 = cursor;
                    for (String str : this.b.keySet()) {
                        try {
                            cursor3 = sQLiteDatabase.rawQuery("SELECT has_dependency FROM dependencies WHERE path = ?", new String[]{str});
                            if (cursor3.getCount() <= 0 || (manifestItem = (ManifestItem) this.b.get(str)) == null) {
                                cursor3.close();
                            } else {
                                Set set = manifestItem.c;
                                cursor3.moveToFirst();
                                do {
                                    set.add(cursor3.getString(0));
                                } while (cursor3.moveToNext());
                                cursor3.close();
                            }
                        } catch (SQLiteDiskIOException e) {
                            cursor = cursor3;
                        } catch (Exception e2) {
                            e = e2;
                            cursor2 = cursor3;
                            try {
                                OpenFeintInternal.log("WebViewCache", "SQLite exception. " + e.toString());
                                try {
                                    cursor2.close();
                                } catch (Exception e3) {
                                    return;
                                }
                            } catch (Throwable th) {
                                th = th;
                                try {
                                    cursor2.close();
                                } catch (Exception e4) {
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            cursor2 = cursor3;
                            cursor2.close();
                            throw th;
                        }
                    }
                    try {
                        cursor3.close();
                    } catch (Exception e5) {
                    }
                } catch (SQLiteDiskIOException e6) {
                    try {
                        WebViewCache.diskError();
                        try {
                            cursor.close();
                        } catch (Exception e7) {
                        }
                    } catch (Throwable th3) {
                        Throwable th4 = th3;
                        cursor2 = cursor;
                        th = th4;
                        cursor2.close();
                        throw th;
                    }
                } catch (Exception e8) {
                    Exception exc = e8;
                    cursor2 = cursor;
                    e = exc;
                    OpenFeintInternal.log("WebViewCache", "SQLite exception. " + e.toString());
                    cursor2.close();
                }
            } catch (SQLiteDiskIOException e9) {
                cursor = null;
            } catch (Exception e10) {
                e = e10;
                cursor2 = null;
                OpenFeintInternal.log("WebViewCache", "SQLite exception. " + e.toString());
                cursor2.close();
            } catch (Throwable th5) {
                th = th5;
                cursor2 = null;
                cursor2.close();
                throw th;
            }
        }

        ManifestData(byte[] bArr) {
            String str;
            ManifestItem manifestItem = null;
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bArr)), 8192);
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        String trim = readLine.trim();
                        if (trim.length() != 0) {
                            switch (trim.charAt(0)) {
                                case '#':
                                    continue;
                                case '-':
                                    if (manifestItem != null) {
                                        manifestItem.c.add(trim.substring(1).trim());
                                        continue;
                                    } else {
                                        throw new Exception("Manifest Syntax Error: Dependency without an item");
                                    }
                                default:
                                    String[] split = trim.split(" ");
                                    if (split.length >= 2) {
                                        if (split[0].charAt(0) == '@') {
                                            str = split[0].substring(1);
                                            this.f167a.add(str);
                                        } else {
                                            str = split[0];
                                        }
                                        ManifestItem manifestItem2 = new ManifestItem(str, split[1]);
                                        this.b.put(str, manifestItem2);
                                        manifestItem = manifestItem2;
                                        continue;
                                    } else {
                                        throw new Exception("Manifest Syntax Error: Extra items in line");
                                    }
                            }
                        }
                    } else {
                        return;
                    }
                }
            } catch (Exception e) {
                throw new Exception(e);
            }
        }

        /* access modifiers changed from: package-private */
        public void saveTo(SQLiteDatabase sQLiteDatabase) {
            try {
                sQLiteDatabase.beginTransaction();
                sQLiteDatabase.execSQL("DELETE FROM server_manifest;");
                sQLiteDatabase.execSQL("DELETE FROM dependencies;");
                SQLiteStatement compileStatement = sQLiteDatabase.compileStatement("INSERT INTO server_manifest(path, hash, is_global) VALUES(?, ?, ?)");
                SQLiteStatement compileStatement2 = sQLiteDatabase.compileStatement("INSERT INTO dependencies(path, has_dependency) VALUES(?, ?)");
                for (String str : this.b.keySet()) {
                    ManifestItem manifestItem = (ManifestItem) this.b.get(str);
                    compileStatement.bindString(1, str);
                    compileStatement.bindString(2, manifestItem.b);
                    compileStatement.bindLong(3, (long) (this.f167a.contains(str) ? 1 : 0));
                    compileStatement.execute();
                    compileStatement2.bindString(1, str);
                    for (String bindString : manifestItem.c) {
                        compileStatement2.bindString(2, bindString);
                        compileStatement2.execute();
                    }
                }
                sQLiteDatabase.setTransactionSuccessful();
                try {
                    sQLiteDatabase.endTransaction();
                } catch (Exception e) {
                }
            } catch (SQLiteDiskIOException e2) {
                WebViewCache.diskError();
                try {
                    sQLiteDatabase.endTransaction();
                } catch (Exception e3) {
                }
            } catch (Exception e4) {
                OpenFeintInternal.log("WebViewCache", "SQLite exception. " + e4.toString());
                try {
                    sQLiteDatabase.endTransaction();
                } catch (Exception e5) {
                }
            } catch (Throwable th) {
                try {
                    sQLiteDatabase.endTransaction();
                } catch (Exception e6) {
                }
                throw th;
            }
        }
    }

    class ManifestItem {

        /* renamed from: a  reason: collision with root package name */
        public String f168a;
        public String b;
        public Set c;

        ManifestItem(ManifestItem manifestItem) {
            this.f168a = manifestItem.f168a;
            this.c = new HashSet(manifestItem.c);
        }

        ManifestItem(String str, String str2) {
            this.f168a = str;
            this.b = str2;
            this.c = new HashSet();
        }
    }

    class ManifestRequest extends CacheRequest {
        /* access modifiers changed from: private */
        public ManifestData b = null;

        public ManifestRequest(String str) {
            super(str);
        }

        /* access modifiers changed from: private */
        public void finishManifest() {
            if (this.b != null) {
                try {
                    this.b.saveTo(DB.f88a.getWritableDatabase());
                } catch (Exception e) {
                    OpenFeintInternal.log("WebViewCache", e.toString());
                }
                Message.obtain(WebViewCache.this.b, 0, this.b).sendToTarget();
                return;
            }
            WebViewCache.this.finishWithoutLoading();
        }

        public void onResponse(int i, byte[] bArr) {
        }

        public void onResponseOffMainThread(int i, byte[] bArr) {
            if (i == 200) {
                try {
                    this.b = new ManifestData(bArr);
                } catch (Exception e) {
                    OpenFeintInternal.log("WebViewCache", e.toString());
                }
            } else {
                try {
                    this.b = new ManifestData(DB.f88a.getReadableDatabase());
                } catch (Exception e2) {
                    OpenFeintInternal.log("WebViewCache", e2.toString());
                }
            }
            if (this.b == null || this.b.b.isEmpty()) {
                this.b = null;
                new BaseRequest() {
                    public String method() {
                        return "GET";
                    }

                    public void onResponse(int i, byte[] bArr) {
                    }

                    public void onResponseOffMainThread(int i, byte[] bArr) {
                        if (200 == i) {
                            try {
                                ManifestRequest.this.b = new ManifestData(bArr);
                                ManifestRequest.this.finishManifest();
                                ManifestRequest.this.updateLastModifiedFromResponse(getResponse());
                            } catch (Exception e) {
                            }
                        } else {
                            OpenFeintInternal.log(f97a, "finishWithoutLoading " + i);
                            WebViewCache.this.finishWithoutLoading();
                        }
                    }

                    public String path() {
                        return ManifestRequest.this.path();
                    }
                }.launch();
                return;
            }
            finishManifest();
            updateLastModifiedFromResponse(getResponse());
        }

        public String path() {
            return WebViewCache.getManifestPath(WebViewCache.this.f);
        }

        public boolean signed() {
            return false;
        }
    }

    class PathAndCallback {

        /* renamed from: a  reason: collision with root package name */
        public final String f169a;
        public final WebViewCacheCallback b;

        public PathAndCallback(String str, WebViewCacheCallback webViewCacheCallback) {
            this.f169a = str;
            this.b = webViewCacheCallback;
        }
    }

    class SaxHandler extends DefaultHandler {

        /* renamed from: a  reason: collision with root package name */
        private String f170a;
        private String b;
        private Map c;

        private SaxHandler() {
            this.c = new HashMap();
        }

        /* synthetic */ SaxHandler(WebViewCache webViewCache, SaxHandler saxHandler) {
            this();
        }

        public void characters(char[] cArr, int i, int i2) {
            this.f170a = new String(cArr).substring(i, i + i2);
        }

        public void endElement(String str, String str2, String str3) {
            String trim = str2.trim();
            if (trim.equals("key")) {
                this.b = this.f170a;
            } else if (trim.equals("string")) {
                this.c.put(this.b, this.f170a);
                DB.setClientManifest(this.b, this.f170a);
            }
        }

        public Map getOutputMap() {
            return this.c;
        }

        public void startElement(String str, String str2, String str3, Attributes attributes) {
            this.f170a = "";
        }
    }

    public class TestOnlyManifestItem {

        /* renamed from: a  reason: collision with root package name */
        public String f171a;
        private String b;
        private String c;

        public enum Status {
            NotYetDownloaded,
            NotOnServer,
            UpToDate,
            OutOfDate
        }

        public TestOnlyManifestItem(String str, String str2, String str3) {
            this.f171a = str;
            this.b = str2;
            this.c = str3;
        }

        public static void syncAndOpenDashboard() {
            if (!WebViewCache.f161a.c) {
                WebViewCache.f161a.d = null;
                WebViewCache.f161a.sync();
            }
            Dashboard.open();
        }

        public void invalidate() {
            DB.setClientManifest(this.f171a, "INVALID");
            WebViewCache.f161a.e.put(this.f171a, "INVALID");
            Util.deleteFiles(new File(String.valueOf(WebViewCache.j) + this.f171a));
            WebViewCache.f161a.markSyncRequired();
        }

        public Status status() {
            return this.b == null ? Status.NotYetDownloaded : this.c == null ? Status.NotOnServer : this.c.equals(this.b) ? Status.UpToDate : Status.OutOfDate;
        }
    }

    private WebViewCache(Context context) {
        this.f = context;
        this.l = new HashSet();
        this.m = new HashMap();
        this.o = new HashSet();
        this.p = new HashSet();
        this.k = new Handler();
        this.b = new Handler() {
            public void dispatchMessage(Message message) {
                switch (message.what) {
                    case 0:
                        OpenFeintInternal.log("WebViewCache", "kServerManifestReady");
                        WebViewCache.this.d = (ManifestData) message.obj;
                        WebViewCache.this.triggerUpdates();
                        return;
                    case 1:
                        WebViewCache.this.finishItem((String) message.obj, message.arg1 > 0);
                        return;
                    case 2:
                        WebViewCache.this.finishItems((Set) message.obj, message.arg1 > 0, false);
                        return;
                    case 3:
                        WebViewCache.this.e = (Map) message.obj;
                        WebViewCache.this.triggerUpdates();
                        return;
                    default:
                        return;
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void batchFetch(String str, String str2, int i2, Set set) {
        final String str3 = str;
        final String str4 = str2;
        final int i3 = i2;
        final Set set2 = set;
        new BaseRequest() {
            public String method() {
                return "GET";
            }

            public void onResponse(int i, byte[] bArr) {
            }

            public void onResponseOffMainThread(int i, byte[] bArr) {
                WebViewCache.this.handleBatchBody(i, bArr, str3, str4, i3, set2);
            }

            public String path() {
                return "";
            }

            public boolean signed() {
                return false;
            }

            public String url() {
                return str3;
            }
        }.launch();
    }

    private void batchRequest(Set set) {
        batchRequest(set, 3);
    }

    /* access modifiers changed from: private */
    public void batchRequest(final Set set, final int i2) {
        OpenFeintInternal.log("WebViewCache", String.format("Syncing %d items", Integer.valueOf(set.size())));
        OrderedArgList orderedArgList = new OrderedArgList();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            ManifestItem manifestItem = (ManifestItem) this.d.b.get((String) it.next());
            orderedArgList.put("files[][path]", manifestItem.f168a);
            orderedArgList.put("files[][hash]", manifestItem.b);
        }
        new BaseRequest(orderedArgList) {
            public String method() {
                return "POST";
            }

            public void onResponse(int i, byte[] bArr) {
            }

            /* access modifiers changed from: protected */
            public void onResponseOffMainThread(int i, byte[] bArr) {
                WebViewCache.this.handleBatchBody(i, bArr, url(), currentURL(), i2, set);
            }

            public String path() {
                return "/webui/assets";
            }

            public boolean signed() {
                return false;
            }
        }.launch();
    }

    /* access modifiers changed from: private */
    public void clientManifestReady() {
        Map defaultClientManifest = getDefaultClientManifest();
        Message obtain = Message.obtain(this.b, 3);
        obtain.obj = defaultClientManifest;
        obtain.sendToTarget();
    }

    /* access modifiers changed from: private */
    public void copyDefaultBackground(File file) {
        HashSet hashSet = new HashSet();
        gatherDefaultItems("webui", hashSet);
        Set<String> stripUnused = stripUnused(hashSet);
        copySpecific(file, "webui/manifest.plist", stripUnused);
        copyDirectory(file, "webui/javascripts/", stripUnused);
        copyDirectory(file, "webui/stylesheets/", stripUnused);
        copyDirectory(file, "webui/intro/", stripUnused);
        if (Util.getDpiName(this.f).equals("mdpi")) {
            copySpecific(file, "webui/images/space.grid.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/button.gray.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/button.gray.hit.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/button.green.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/button.green.hit.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/logo.small.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/header_bg.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/loading.spinner.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/input.text.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/frame.small.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/icon.leaf.gray.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/tab.divider.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/tab.active_indicator.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/logo.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/header_bg.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/loading.spinner.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/icon.user.male.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/intro.leaderboards.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/intro.friends.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/intro.achievements.mdpi.png", stripUnused);
            copySpecific(file, "webui/images/intro.games.mdpi.png", stripUnused);
        } else {
            copySpecific(file, "webui/images/space.grid.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/button.gray.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/button.gray.hit.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/button.green.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/button.green.hit.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/logo.small.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/header_bg.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/loading.spinner.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/input.text.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/frame.small.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/icon.leaf.gray.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/tab.divider.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/tab.active_indicator.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/logo.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/header_bg.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/loading.spinner.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/icon.user.male.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/intro.leaderboards.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/intro.friends.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/intro.achievements.hdpi.png", stripUnused);
            copySpecific(file, "webui/images/intro.games.hdpi.png", stripUnused);
        }
        clientManifestReady();
        for (String copySingleItem : stripUnused) {
            copySingleItem(file, copySingleItem);
        }
    }

    private void copyDirectory(File file, String str, Set set) {
        HashSet<String> hashSet = new HashSet<>();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            if (str2.startsWith(str)) {
                hashSet.add(str2);
            }
        }
        for (String copySpecific : hashSet) {
            copySpecific(file, copySpecific, set);
        }
    }

    private void copySingleItem(File file, String str) {
        try {
            File file2 = new File(file, str);
            DataInputStream dataInputStream = new DataInputStream(this.f.getAssets().open(str));
            file2.getParentFile().mkdirs();
            Util.copyStream(dataInputStream, new DataOutputStream(new FileOutputStream(file2)));
        } catch (Exception e2) {
            OpenFeintInternal.log("WebViewCache", e2.toString());
        }
    }

    private void copySpecific(File file, String str, Set set) {
        if (set.contains(str)) {
            copySingleItem(file, str);
            set.remove(str);
        }
    }

    /* access modifiers changed from: private */
    public void deleteAll() {
        Util.deleteFiles(new File(this.f.getFilesDir(), "webui"));
        this.f.getDatabasePath("manifest.db").delete();
    }

    public static void diskError() {
        s = true;
        for (PathAndCallback pathAndCallback : f161a.l) {
            pathAndCallback.b.failLoaded();
        }
        f161a.l.clear();
        f161a.finishWithoutLoading();
    }

    private void finishGlobals() {
        ManifestItem manifestItem;
        for (PathAndCallback pathAndCallback : this.l) {
            if (!this.o.contains(pathAndCallback.f169a)) {
                pathAndCallback.b.pathLoaded(pathAndCallback.f169a);
            } else {
                ManifestItem manifestItem2 = new ManifestItem((ManifestItem) this.d.b.get(pathAndCallback.f169a));
                manifestItem2.c.retainAll(this.o);
                this.m.put(pathAndCallback.f169a, new ItemAndCallback(manifestItem2, pathAndCallback.b));
            }
        }
        this.l.clear();
        HashSet hashSet = new HashSet();
        for (String str : this.p) {
            if (this.o.contains(str) && (manifestItem = (ManifestItem) this.d.b.get(str)) != null) {
                hashSet.addAll(manifestItem.c);
            }
        }
        hashSet.retainAll(this.o);
        this.p.addAll(hashSet);
        this.n = true;
    }

    /* access modifiers changed from: private */
    public void finishItem(String str, boolean z) {
        HashSet hashSet = new HashSet(1);
        hashSet.add(str);
        finishItems(hashSet, z, true);
    }

    private void finishItems(Set set, boolean z) {
        finishItems(set, z, false);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.String} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void finishItems(java.util.Set r9, boolean r10, boolean r11) {
        /*
            r8 = this;
            com.openfeint.internal.ui.WebViewCache$ManifestData r1 = r8.d
            if (r1 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            if (r10 != 0) goto L_0x0010
            if (r11 != 0) goto L_0x0010
            r1 = 1
            r8.q = r1
        L_0x000c:
            r8.loadNextItem()
            goto L_0x0004
        L_0x0010:
            java.util.Map r1 = r8.m
            java.util.Collection r1 = r1.values()
            java.util.Iterator r2 = r1.iterator()
        L_0x001a:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x006d
            java.util.Set r1 = r8.o
            r1.removeAll(r9)
            com.openfeint.internal.ui.WebViewCache$ManifestData r1 = r8.d
            java.util.Set r1 = r1.f167a
            r1.removeAll(r9)
            java.util.Set r1 = r8.p
            r1.removeAll(r9)
            boolean r1 = r8.n
            if (r1 == 0) goto L_0x0054
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>()
            java.util.Map r1 = r8.m
            java.util.Collection r1 = r1.values()
            java.util.Iterator r3 = r1.iterator()
        L_0x0044:
            boolean r1 = r3.hasNext()
            if (r1 != 0) goto L_0x007b
            java.util.Iterator r2 = r2.iterator()
        L_0x004e:
            boolean r1 = r2.hasNext()
            if (r1 != 0) goto L_0x00a8
        L_0x0054:
            int r1 = r9.size()
            java.lang.String[] r3 = new java.lang.String[r1]
            int r1 = r3.length
            java.lang.String[] r4 = new java.lang.String[r1]
            r1 = 0
            java.util.Iterator r5 = r9.iterator()
            r6 = r1
        L_0x0063:
            boolean r1 = r5.hasNext()
            if (r1 != 0) goto L_0x00b4
            com.openfeint.internal.db.DB.setClientManifestBatch(r3, r4)
            goto L_0x000c
        L_0x006d:
            java.lang.Object r1 = r2.next()
            com.openfeint.internal.ui.WebViewCache$ItemAndCallback r1 = (com.openfeint.internal.ui.WebViewCache.ItemAndCallback) r1
            com.openfeint.internal.ui.WebViewCache$ManifestItem r1 = r1.f166a
            java.util.Set r1 = r1.c
            r1.removeAll(r9)
            goto L_0x001a
        L_0x007b:
            java.lang.Object r1 = r3.next()
            com.openfeint.internal.ui.WebViewCache$ItemAndCallback r1 = (com.openfeint.internal.ui.WebViewCache.ItemAndCallback) r1
            java.util.Set r4 = r8.o
            com.openfeint.internal.ui.WebViewCache$ManifestItem r5 = r1.f166a
            java.lang.String r5 = r5.f168a
            boolean r4 = r4.contains(r5)
            if (r4 != 0) goto L_0x0044
            com.openfeint.internal.ui.WebViewCache$ManifestItem r4 = r1.f166a
            java.util.Set r4 = r4.c
            int r4 = r4.size()
            if (r4 != 0) goto L_0x0044
            com.openfeint.internal.ui.WebViewCache$ManifestItem r4 = r1.f166a
            java.lang.String r4 = r4.f168a
            r2.add(r4)
            com.openfeint.internal.ui.WebViewCacheCallback r4 = r1.b
            com.openfeint.internal.ui.WebViewCache$ManifestItem r1 = r1.f166a
            java.lang.String r1 = r1.f168a
            r4.pathLoaded(r1)
            goto L_0x0044
        L_0x00a8:
            java.lang.Object r1 = r2.next()
            java.lang.String r1 = (java.lang.String) r1
            java.util.Map r3 = r8.m
            r3.remove(r1)
            goto L_0x004e
        L_0x00b4:
            java.lang.Object r1 = r5.next()
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0
            r2 = r0
            if (r10 == 0) goto L_0x00d6
            com.openfeint.internal.ui.WebViewCache$ManifestData r1 = r8.d
            java.util.Map r1 = r1.b
            java.lang.Object r1 = r1.get(r2)
            com.openfeint.internal.ui.WebViewCache$ManifestItem r1 = (com.openfeint.internal.ui.WebViewCache.ManifestItem) r1
            java.lang.String r1 = r1.b
        L_0x00ca:
            r3[r6] = r2
            r4[r6] = r1
            int r6 = r6 + 1
            java.util.Map r7 = r8.e
            r7.put(r2, r1)
            goto L_0x0063
        L_0x00d6:
            java.lang.String r1 = "INVALID"
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.ui.WebViewCache.finishItems(java.util.Set, boolean, boolean):void");
    }

    private void finishLoading() {
        DB.f88a.close();
        this.c = true;
    }

    /* access modifiers changed from: private */
    public void finishWithoutLoading() {
        OpenFeintInternal.log("WebViewCache", "finishWithoutLoading");
        for (PathAndCallback pathAndCallback : this.l) {
            pathAndCallback.b.pathLoaded(pathAndCallback.f169a);
        }
        this.l.clear();
        this.p.clear();
        this.d.f167a.clear();
        this.o.clear();
        finishLoading();
    }

    private static String fullOuterJoin(String str, String str2, String str3, String str4) {
        return String.format("%s UNION %s;", String.format("SELECT %s from %s LEFT OUTER JOIN %s ON %s", str, str2, str3, str4), String.format("SELECT %s from %s LEFT OUTER JOIN %s ON %s", str, str3, str2, str4));
    }

    private void gatherDefaultItems(String str, Set set) {
        try {
            String[] list = this.f.getAssets().list(str);
            int length = list.length;
            for (int i2 = 0; i2 < length; i2++) {
                String str2 = String.valueOf(str) + "/" + list[i2];
                try {
                    InputStream open = this.f.getAssets().open(str2);
                    set.add(str2);
                    open.close();
                } catch (IOException e2) {
                    gatherDefaultItems(str2, set);
                }
            }
        } catch (IOException e3) {
            OpenFeintInternal.log("WebViewCache", e3.toString());
        }
    }

    private Map getDefaultClientManifest() {
        Cursor cursor;
        Cursor cursor2;
        try {
            cursor2 = DB.f88a.getReadableDatabase().rawQuery("SELECT * FROM manifest", null);
            try {
                if (cursor2.getCount() > 0) {
                    HashMap hashMap = new HashMap();
                    cursor2.moveToFirst();
                    do {
                        hashMap.put(cursor2.getString(0), cursor2.getString(1));
                    } while (cursor2.moveToNext());
                    cursor2.close();
                    OpenFeintInternal.log("WebViewCache", "create client Manifest from db");
                    try {
                        cursor2.close();
                    } catch (Exception e2) {
                    }
                    return hashMap;
                }
                try {
                    cursor2.close();
                } catch (Exception e3) {
                }
                return getDefaultClientManifestFromAsset();
            } catch (SQLiteDiskIOException e4) {
                try {
                    diskError();
                    try {
                        cursor2.close();
                    } catch (Exception e5) {
                    }
                    return getDefaultClientManifestFromAsset();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = cursor2;
                    th = th2;
                    try {
                        cursor.close();
                    } catch (Exception e6) {
                    }
                    throw th;
                }
            } catch (Exception e7) {
                Exception exc = e7;
                cursor = cursor2;
                e = exc;
                try {
                    OpenFeintInternal.log("WebViewCache", "SQLite exception. " + e.toString());
                    try {
                        cursor.close();
                    } catch (Exception e8) {
                    }
                    return getDefaultClientManifestFromAsset();
                } catch (Throwable th3) {
                    th = th3;
                    cursor.close();
                    throw th;
                }
            }
        } catch (SQLiteDiskIOException e9) {
            cursor2 = null;
        } catch (Exception e10) {
            e = e10;
            cursor = null;
            OpenFeintInternal.log("WebViewCache", "SQLite exception. " + e.toString());
            cursor.close();
            return getDefaultClientManifestFromAsset();
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            cursor.close();
            throw th;
        }
    }

    private Map getDefaultClientManifestFromAsset() {
        File file = new File(j, "manifest.plist");
        if (file.isFile()) {
            try {
                XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
                SaxHandler saxHandler = new SaxHandler(this, null);
                xMLReader.setContentHandler(saxHandler);
                xMLReader.parse(new InputSource(new FileInputStream(file.getPath())));
                return saxHandler.getOutputMap();
            } catch (Exception e2) {
                OpenFeintInternal.log("WebViewCache", e2.toString());
            }
        }
        return new HashMap();
    }

    public static final String getItemUri(String str) {
        return String.valueOf(i) + str;
    }

    /* access modifiers changed from: private */
    public static final String getManifestPath(Context context) {
        return String.format("/webui/manifest/%s.%s.%s", "android", h != null ? h : "embed", Util.getDpiName(context));
    }

    private static final URI getServerURI() {
        try {
            return g != null ? g : new URI(OpenFeintInternal.getInstance().getServerUrl());
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void handleBatchBody(int i2, byte[] bArr, String str, String str2, int i3, Set set) {
        if (200 <= i2 && i2 < 300) {
            processBatch(set, bArr);
        } else if (302 == i2 || 303 == i2) {
            batchFetch(str, str2, i3, set);
        } else if (i2 != 0 && (400 > i2 || i2 >= 500)) {
            Message.obtain(this.b, 2, 0, 0, set).sendToTarget();
        } else if (i3 > 0) {
            final String str3 = str;
            final String str4 = str2;
            final Set set2 = set;
            final int i4 = i3;
            this.k.postDelayed(new Runnable() {
                public void run() {
                    if (str3.equals(str4)) {
                        WebViewCache.this.batchRequest(set2, i4 - 1);
                    } else {
                        WebViewCache.this.batchFetch(str3, str4, i4 - 1, set2);
                    }
                }
            }, 5000);
        } else {
            Message.obtain(this.b, 2, 0, 0, set).sendToTarget();
        }
    }

    public static WebViewCache initialize(Context context) {
        if (f161a != null) {
            f161a.finishWithoutLoading();
        }
        WebViewCache webViewCache = new WebViewCache(context);
        f161a = webViewCache;
        return webViewCache;
    }

    public static boolean isDiskError() {
        return s;
    }

    public static boolean isLoaded(String str) {
        return f161a.isLoadedInner(str);
    }

    private boolean isLoadedInner(String str) {
        return this.d == null ? this.c : !this.o.contains(str);
    }

    private void loadNextItem() {
        OpenFeintInternal.log("WebViewCache", "loadNextItem");
        this.d.f167a.retainAll(this.o);
        if (!this.n && this.d.f167a.isEmpty()) {
            finishGlobals();
        }
        this.p.retainAll(this.o);
        int size = this.d.f167a.size() + this.p.size();
        if (!this.q && size > 1) {
            HashSet hashSet = new HashSet();
            hashSet.addAll(this.d.f167a);
            hashSet.addAll(this.p);
            batchRequest(hashSet);
        } else if (this.d.f167a.size() > 0) {
            singleRequest((String) this.d.f167a.iterator().next());
        } else if (this.p.size() > 0) {
            singleRequest((String) this.p.iterator().next());
        } else if (!this.q && this.o.size() > 1) {
            batchRequest(this.o);
        } else if (this.o.size() > 0) {
            singleRequest((String) this.o.iterator().next());
        } else {
            finishLoading();
        }
    }

    public static void prioritize(String str) {
        f161a.prioritizeInner(str);
    }

    private void prioritizeInner(String str) {
        ManifestItem manifestItem;
        if (!this.c) {
            this.p.add(str);
            if (this.d != null && (manifestItem = (ManifestItem) this.d.b.get(str)) != null) {
                HashSet hashSet = new HashSet(manifestItem.c);
                hashSet.retainAll(this.o);
                this.p.addAll(hashSet);
                OpenFeintInternal.log("WebViewCache", "Prioritizing " + str + " deps:" + hashSet.toString());
            }
        }
    }

    private void processBatch(Set set, byte[] bArr) {
        HashSet hashSet = new HashSet();
        ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(bArr));
        while (true) {
            try {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    break;
                } else if (!nextEntry.isDirectory()) {
                    String name = nextEntry.getName();
                    Util.saveStreamAndLeaveInputOpen(zipInputStream, String.valueOf(j) + name);
                    hashSet.add(name);
                }
            } catch (Exception e2) {
                OpenFeintInternal.log("WebViewCache", e2.getMessage());
            }
        }
        if (!hashSet.isEmpty()) {
            Message.obtain(this.b, 2, 1, 0, hashSet).sendToTarget();
        } else {
            Message.obtain(this.b, 2, 0, 0, set).sendToTarget();
        }
    }

    public static boolean recover() {
        if (s) {
            return false;
        }
        return f161a.recoverInternal();
    }

    private final void singleRequest(final String str) {
        OpenFeintInternal.log("WebViewCache", "Syncing item: " + str);
        new BaseRequest() {
            public String method() {
                return "GET";
            }

            public void onResponse(int i, byte[] bArr) {
                if (i != 200) {
                    Message.obtain(WebViewCache.this.b, 1, 0, 0, str).sendToTarget();
                    return;
                }
                try {
                    Util.saveFile(bArr, String.valueOf(WebViewCache.j) + str);
                    Message.obtain(WebViewCache.this.b, 1, 1, 0, str).sendToTarget();
                } catch (Exception e) {
                    Message.obtain(WebViewCache.this.b, 1, 0, 0, str).sendToTarget();
                }
            }

            public String path() {
                return "/webui/" + str;
            }

            public boolean signed() {
                return false;
            }
        }.launch();
    }

    public static void start() {
        f161a.updateExternalStorageState();
        f161a.sync();
    }

    private Set stripUnused(Set set) {
        String str = Util.getDpiName(this.f).equals("mdpi") ? ".hdpi." : ".mdpi.";
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            if (!str2.contains(str)) {
                hashSet.add(str2);
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: private */
    public void sync() {
        OpenFeintInternal.log("WebViewCache", "--- WebViewCache Sync ---");
        new ManifestRequest("manifest").launch();
    }

    public static TestOnlyManifestItem[] testOnlyManifestItems() {
        Cursor cursor;
        Cursor cursor2;
        SQLiteDatabase readableDatabase = DB.f88a.getReadableDatabase();
        ArrayList arrayList = new ArrayList();
        try {
            cursor2 = readableDatabase.rawQuery(fullOuterJoin("server_manifest.path, server_manifest.hash, manifest.hash", "server_manifest", "manifest", "server_manifest.path = manifest.path"), null);
            try {
                if (cursor2.getCount() > 0) {
                    cursor2.moveToFirst();
                    do {
                        String string = cursor2.getString(0);
                        if (string != null) {
                            arrayList.add(new TestOnlyManifestItem(string, cursor2.getString(2), cursor2.getString(1)));
                        }
                    } while (cursor2.moveToNext());
                }
                try {
                    cursor2.close();
                } catch (Exception e2) {
                }
            } catch (Exception e3) {
                try {
                    cursor2.close();
                } catch (Exception e4) {
                }
                TestOnlyManifestItem[] testOnlyManifestItemArr = (TestOnlyManifestItem[]) arrayList.toArray(new TestOnlyManifestItem[0]);
                Arrays.sort(testOnlyManifestItemArr, new Comparator() {
                    public int compare(TestOnlyManifestItem testOnlyManifestItem, TestOnlyManifestItem testOnlyManifestItem2) {
                        return testOnlyManifestItem.f171a.compareTo(testOnlyManifestItem2.f171a);
                    }
                });
                return testOnlyManifestItemArr;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = cursor2;
                th = th2;
                try {
                    cursor.close();
                } catch (Exception e5) {
                }
                throw th;
            }
        } catch (Exception e6) {
            cursor2 = null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            cursor.close();
            throw th;
        }
        TestOnlyManifestItem[] testOnlyManifestItemArr2 = (TestOnlyManifestItem[]) arrayList.toArray(new TestOnlyManifestItem[0]);
        Arrays.sort(testOnlyManifestItemArr2, new Comparator() {
            public int compare(TestOnlyManifestItem testOnlyManifestItem, TestOnlyManifestItem testOnlyManifestItem2) {
                return testOnlyManifestItem.f171a.compareTo(testOnlyManifestItem2.f171a);
            }
        });
        return testOnlyManifestItemArr2;
    }

    public static boolean trackPath(String str, WebViewCacheCallback webViewCacheCallback) {
        return f161a.trackPathInner(str, webViewCacheCallback);
    }

    private boolean trackPathInner(String str, WebViewCacheCallback webViewCacheCallback) {
        if (this.c) {
            webViewCacheCallback.pathLoaded(str);
            return false;
        } else if (this.d == null) {
            webViewCacheCallback.onTrackingNeeded();
            this.l.add(new PathAndCallback(str, webViewCacheCallback));
            return true;
        } else {
            ManifestItem manifestItem = (ManifestItem) this.d.b.get(str);
            if (manifestItem != null) {
                webViewCacheCallback.onTrackingNeeded();
                ManifestItem manifestItem2 = new ManifestItem(manifestItem);
                manifestItem2.c.retainAll(this.o);
                this.m.put(str, new ItemAndCallback(manifestItem2, webViewCacheCallback));
                return true;
            }
            webViewCacheCallback.pathLoaded(str);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void triggerUpdates() {
        OpenFeintInternal.log("WebViewCache", "loadedManifest");
        if (this.d != null && this.e != null) {
            for (ManifestItem manifestItem : this.d.b.values()) {
                if (!manifestItem.b.equals(this.e.get(manifestItem.f168a))) {
                    this.o.add(manifestItem.f168a);
                }
            }
            loadNextItem();
        }
    }

    private void updateExternalStorageState() {
        if (Util.noSdcardPermission()) {
            OpenFeintInternal.log("WebViewCache", "no sdcard permission");
            setRootUriInternal();
            return;
        }
        String externalStorageState = Environment.getExternalStorageState();
        if ("mounted".equals(externalStorageState)) {
            setRootUriSdcard(new File(Environment.getExternalStorageDirectory(), "openfeint"));
            return;
        }
        OpenFeintInternal.log("WebViewCache", externalStorageState);
        setRootUriInternal();
    }

    /* access modifiers changed from: package-private */
    public void markSyncRequired() {
        this.c = false;
        this.n = false;
    }

    /* access modifiers changed from: package-private */
    public boolean recoverInternal() {
        boolean recover = DB.recover(this.f);
        this.d = null;
        if (recover) {
            this.e = getDefaultClientManifestFromAsset();
            recover = !this.e.isEmpty();
        }
        markSyncRequired();
        sync();
        return recover;
    }

    public final void setRootUriInternal() {
        OpenFeintInternal.log("WebViewCache", "can't use sdcard");
        final File filesDir = this.f.getFilesDir();
        j = String.valueOf(new File(filesDir, "webui").getAbsolutePath()) + "/";
        i = "file://" + j;
        if (!new File(filesDir, "webui").isDirectory()) {
            new Thread(new Runnable() {
                public void run() {
                    WebViewCache.this.copyDefaultBackground(filesDir);
                }
            }).start();
        } else {
            clientManifestReady();
        }
    }

    public final void setRootUriSdcard(File file) {
        final File file2 = new File(file, "webui");
        boolean z = !file2.exists();
        if (z) {
            try {
                new File(file, ".nomedia").createNewFile();
            } catch (IOException e2) {
            }
            if (!file2.mkdirs()) {
                setRootUriInternal();
                return;
            }
        }
        j = String.valueOf(file2.getAbsolutePath()) + "/";
        i = "file://" + j;
        if (z) {
            final File filesDir = this.f.getFilesDir();
            final File file3 = new File(filesDir, "webui");
            if (file3.isDirectory()) {
                try {
                    Util.copyFile(this.f.getDatabasePath("manifest.db"), new File(file2, "manifest.db"));
                } catch (IOException e3) {
                }
            }
            new Thread(new Runnable() {
                public void run() {
                    try {
                        if (file3.isDirectory()) {
                            Util.copyDirectory(file3, file2);
                            WebViewCache.this.deleteAll();
                            OpenFeintInternal.log("WebViewCache", "copy in phone data finish");
                            WebViewCache.this.clientManifestReady();
                            return;
                        }
                        OpenFeintInternal.log("WebViewCache", "copy from asset");
                        WebViewCache.this.copyDefaultBackground(filesDir);
                    } catch (IOException e) {
                        OpenFeintInternal.log("WebViewCache", e.getMessage());
                        WebViewCache.this.setRootUriInternal();
                    }
                }
            }).start();
            return;
        }
        clientManifestReady();
        deleteAll();
    }
}
