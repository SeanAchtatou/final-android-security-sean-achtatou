package com.openfeint.internal.ui;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.webkit.WebView;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import hamon05.speed.pac.R;

public class NestedWindow extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private View f134a;
    private boolean b = false;
    private final String c = "NestedWindow";
    protected WebView f;

    private boolean isBigScreen() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        if (height <= width || width < 800 || height < 1000) {
            return width >= 1000 && height >= 800;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void beforeSetContentView() {
        if (isBigScreen()) {
            getWindow().requestFeature(1);
        }
    }

    public void fade(boolean z) {
        float f2 = 0.0f;
        if (this.f != null && this.b != z) {
            this.b = z;
            float f3 = z ? 0.0f : 1.0f;
            if (z) {
                f2 = 1.0f;
            }
            AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f2);
            alphaAnimation.setDuration((long) (z ? 200 : 0));
            alphaAnimation.setFillAfter(true);
            this.f.startAnimation(alphaAnimation);
            if (this.f.getVisibility() == 4) {
                this.f.setVisibility(0);
                findViewById(R.id.frameLayout).setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int layoutResource() {
        return R.layout.of_nested_window;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Util.setOrientation(this);
        beforeSetContentView();
        setContentView(layoutResource());
        this.f = (WebView) findViewById(R.id.web_view);
        this.f134a = findViewById(R.id.of_ll_logo_image);
        OpenFeintInternal.log("NestedWindow", "onCreate");
    }
}
