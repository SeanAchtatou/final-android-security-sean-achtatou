package com.openfeint.internal.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class PadLogoImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private final String f135a = "ImageView2";

    public PadLogoImageView(Context context) {
        super(context);
    }

    public PadLogoImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PadLogoImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        Log.e("ImageView2", String.valueOf(z));
        Log.e("ImageView2", String.valueOf(i2));
        if (!z) {
            return;
        }
        if (i2 < 350) {
            setVisibility(4);
        } else {
            setVisibility(0);
        }
    }
}
