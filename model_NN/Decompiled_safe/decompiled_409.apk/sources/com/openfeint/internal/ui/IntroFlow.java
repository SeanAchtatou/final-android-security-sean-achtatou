package com.openfeint.internal.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import com.openfeint.api.OpenFeint;
import com.openfeint.internal.ImagePicker;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util5;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.ui.WebNav;
import java.util.List;
import java.util.Map;

public class IntroFlow extends WebNav {

    /* renamed from: a  reason: collision with root package name */
    Bitmap f126a;

    class IntroFlowActionHandler extends WebNav.ActionHandler {
        public IntroFlowActionHandler(WebNav webNav) {
            super(webNav);
        }

        public final void cacheImage(Map map) {
            ImagePicker.show(IntroFlow.this);
        }

        public final void clearImage(Map map) {
            IntroFlow.this.f126a = null;
        }

        public final void createUser(final Map map) {
            OpenFeintInternal.getInstance().createUser((String) map.get("name"), (String) map.get("email"), (String) map.get("password"), (String) map.get("password_confirmation"), new IRawRequestDelegate() {
                public void onResponse(int i, String str) {
                    IntroFlowActionHandler.this.f148a.executeJavascript(String.format("%s('%d', %s)", map.get("callback"), Integer.valueOf(i), str.trim()));
                }
            });
        }

        public void decline(Map map) {
            OpenFeint.userDeclinedFeint();
            IntroFlow.this.finish();
        }

        public void getEmail(Map map) {
            String accountNameEclair = Util5.getAccountNameEclair(IntroFlow.this);
            if (accountNameEclair != null) {
                IntroFlow.this.executeJavascript(String.format("%s('%s');", map.get("callback"), accountNameEclair));
            }
        }

        public final void loginUser(final Map map) {
            OpenFeintInternal.getInstance().loginUser((String) map.get("email"), (String) map.get("password"), (String) map.get("user_id"), new IRawRequestDelegate() {
                public void onResponse(int i, String str) {
                    IntroFlowActionHandler.this.f148a.executeJavascript(String.format("%s('%d', %s)", map.get("callback"), Integer.valueOf(i), str.trim()));
                }
            });
        }

        /* access modifiers changed from: protected */
        public void populateActionList(List list) {
            super.populateActionList(list);
            list.add("createUser");
            list.add("loginUser");
            list.add("cacheImage");
            list.add("uploadImage");
            list.add("clearImage");
            list.add("decline");
            list.add("getEmail");
        }

        public final void uploadImage(Map map) {
            if (IntroFlow.this.f126a != null) {
                ImagePicker.compressAndUpload(IntroFlow.this.f126a, "/xp/users/" + OpenFeintInternal.getInstance().getCurrentUser().resourceID() + "/profile_picture", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public WebNav.ActionHandler createActionHandler(WebNav webNav) {
        return new IntroFlowActionHandler(webNav);
    }

    /* access modifiers changed from: protected */
    public String initialContentPath() {
        String stringExtra = getIntent().getStringExtra("content_name");
        return stringExtra != null ? "intro/" + stringExtra : "intro/index";
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (ImagePicker.isImagePickerActivityResult(i)) {
            this.f126a = ImagePicker.onImagePickerActivityResult(this, i2, 152, intent);
        }
    }
}
