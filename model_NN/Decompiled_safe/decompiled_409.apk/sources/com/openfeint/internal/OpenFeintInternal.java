package com.openfeint.internal;

import a.a.a.n;
import a.b.a.a.a.c;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.CookieManager;
import com.openfeint.api.Notification;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.api.resource.User;
import com.openfeint.internal.SyncedStore;
import com.openfeint.internal.db.DB;
import com.openfeint.internal.notifications.SimpleNotification;
import com.openfeint.internal.notifications.TwoLineNotification;
import com.openfeint.internal.offline.OfflineSupport;
import com.openfeint.internal.request.BaseRequest;
import com.openfeint.internal.request.BlobPostRequest;
import com.openfeint.internal.request.Client;
import com.openfeint.internal.request.GenericRequest;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.request.RawRequest;
import com.openfeint.internal.request.multipart.ByteArrayPartSource;
import com.openfeint.internal.request.multipart.FilePartSource;
import com.openfeint.internal.request.multipart.PartSource;
import com.openfeint.internal.resource.BlobUploadParameters;
import com.openfeint.internal.resource.ServerException;
import com.openfeint.internal.ui.IntroFlow;
import com.openfeint.internal.ui.WebViewCache;
import hamon05.speed.pac.R;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import org.apache.http.impl.client.AbstractHttpClient;

public class OpenFeintInternal {
    private static OpenFeintInternal c;

    /* renamed from: a  reason: collision with root package name */
    Client f75a;
    Handler b;
    private CurrentUser d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public boolean k;
    private OpenFeintDelegate l;
    private OpenFeintSettings m;
    private Analytics n;
    private SyncedStore o;
    /* access modifiers changed from: private */
    public Runnable p;
    /* access modifiers changed from: private */
    public List q;
    /* access modifiers changed from: private */
    public Runnable r;
    /* access modifiers changed from: private */
    public List s;
    private String t;
    private int u = -1;
    private Properties v;
    private String w;
    private Context x;
    private LoginDelegate y;

    public interface IUploadDelegate {
        void fileUploadedTo(String str, boolean z);
    }

    public interface LoginDelegate {
        void login(User user);
    }

    /* JADX INFO: finally extract failed */
    private OpenFeintInternal(OpenFeintSettings openFeintSettings, Context context) {
        c = this;
        this.x = context;
        this.m = openFeintSettings;
        SyncedStore.Reader read = getPrefs().read();
        try {
            this.j = read.getString(new StringBuilder(String.valueOf(getContext().getPackageName())).append(".of_declined").toString(), null) != null;
            read.complete();
            this.b = new Handler();
            this.v = new Properties();
            this.v.put("server-url", "https://api.openfeint.com");
            this.v.put("of-version", "1.9.2");
            loadPropertiesFromXMLResource(this.v, getResource("@xml/openfeint_internal_settings"));
            Log.i("OpenFeint", "Using OpenFeint version " + this.v.get("of-version") + " (" + this.v.get("server-url") + ")");
            Properties properties = new Properties();
            loadPropertiesFromXMLResource(properties, getResource("@xml/openfeint_app_settings"));
            this.m.applyOverrides(properties);
            this.m.verify();
            if (!Encryption.initialized()) {
                Encryption.init(this.m.c);
            }
            this.f75a = new Client(this.m.b, this.m.c, getPrefs());
            Util.moveWebCache(context);
            WebViewCache.initialize(context);
            DB.createDB(context);
            WebViewCache.start();
            this.n = new Analytics();
        } catch (Throwable th) {
            read.complete();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public final void _makeRequest(final BaseRequest baseRequest) {
        if (!isUserLoggedIn() && baseRequest.wantsLogin() && lastLoggedInUser() != null && isFeintServerReachable()) {
            login(false);
            if (this.s == null) {
                this.s = new ArrayList();
            }
            this.s.add(new Runnable() {
                public void run() {
                    OpenFeintInternal.this.f75a.makeRequest(baseRequest);
                }
            });
        } else if (this.g || !baseRequest.needsDeviceSession()) {
            this.f75a.makeRequest(baseRequest);
        } else {
            createDeviceSession();
            if (this.q == null) {
                this.q = new ArrayList();
            }
            this.q.add(new Runnable() {
                public void run() {
                    OpenFeintInternal.this.f75a.makeRequest(baseRequest);
                }
            });
        }
    }

    private void _restoreInstanceState(Bundle bundle) {
        if (!this.k && bundle != null) {
            this.d = (CurrentUser) userFromString(bundle.getString("mCurrentUser"));
            if (this.f75a != null) {
                this.f75a.restoreInstanceState(bundle);
            }
            this.e = bundle.getBoolean("mCurrentlyLoggingIn");
            this.f = bundle.getBoolean("mCreatingDeviceSession");
            this.g = bundle.getBoolean("mDeviceSessionCreated");
            this.h = bundle.getBoolean("mBanned");
            this.i = bundle.getBoolean("mApproved");
            this.j = bundle.getBoolean("mDeclined");
            this.k = true;
        }
    }

    private void _saveInstanceState(Bundle bundle) {
        if (this.d != null) {
            bundle.putString("mCurrentUser", this.d.generate());
        }
        if (this.f75a != null) {
            this.f75a.saveInstanceState(bundle);
        }
        bundle.putBoolean("mCurrentlyLoggingIn", this.e);
        bundle.putBoolean("mCreatingDeviceSession", this.f);
        bundle.putBoolean("mDeviceSessionCreated", this.g);
        bundle.putBoolean("mBanned", this.h);
        bundle.putBoolean("mApproved", this.i);
        bundle.putBoolean("mDeclined", this.j);
    }

    private static List cat(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(str)), 8192);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                arrayList.add(readLine);
            }
            bufferedReader.close();
        } catch (Exception e2) {
        }
        return arrayList;
    }

    private boolean checkBan() {
        if (!this.h) {
            return false;
        }
        displayErrorDialog(getContext().getText(R.string.of_banned_dialog));
        return true;
    }

    /* access modifiers changed from: private */
    public void clearPrefs() {
        SyncedStore.Editor edit = getPrefs().edit();
        try {
            edit.remove("last_logged_in_server");
            edit.remove("last_logged_in_user_name");
            edit.remove("last_logged_in_user");
        } finally {
            edit.commit();
        }
    }

    private void clearUser(SyncedStore.Editor editor) {
        editor.remove("last_logged_in_user");
    }

    private String findUDID() {
        String string = Settings.Secure.getString(getContext().getContentResolver(), "android_id");
        if (string != null && !string.equals("9774d56d682e549c")) {
            return "android-id-" + string;
        }
        SyncedStore.Reader read = getPrefs().read();
        try {
            String string2 = read.getString("udid", null);
            if (string2 != null) {
                return string2;
            }
            byte[] bArr = new byte[16];
            new Random().nextBytes(bArr);
            String str = "android-emu-" + new String(c.a(bArr)).replace("\r\n", "");
            SyncedStore.Editor edit = getPrefs().edit();
            try {
                edit.putString("udid", str);
                return str;
            } finally {
                edit.commit();
            }
        } finally {
            read.complete();
        }
    }

    public static void genericRequest(String str, String str2, Map map, Map map2, IRawRequestDelegate iRawRequestDelegate) {
        makeRequest(new GenericRequest(str, str2, map, map2, iRawRequestDelegate));
    }

    public static OpenFeintInternal getInstance() {
        return c;
    }

    public static String getModelString() {
        return "p(" + Build.PRODUCT + ")/m(" + Build.MODEL + ")";
    }

    public static String getOSVersionString() {
        return "v" + Build.VERSION.RELEASE + " (" + Build.VERSION.INCREMENTAL + ")";
    }

    public static String getProcessorInfo() {
        String str;
        try {
            Iterator it = cat("/proc/cpuinfo").iterator();
            while (true) {
                if (it.hasNext()) {
                    String str2 = (String) it.next();
                    if (str2.startsWith("Processor\t")) {
                        str = str2.split(":")[1].trim();
                        break;
                    }
                } else {
                    str = "unknown";
                    break;
                }
            }
            return String.format("family(%s) min(%s) max(%s)", str, cat("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq").get(0), cat("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq").get(0));
        } catch (Exception e2) {
            return "family(unknown) min(unknown) max(unknown)";
        }
    }

    public static String getRString(int i2) {
        return getInstance().getContext().getResources().getString(i2);
    }

    public static String getScreenInfo() {
        DisplayMetrics displayMetrics = Util.getDisplayMetrics();
        return String.format("%dx%d (%f dpi)", Integer.valueOf(displayMetrics.widthPixels), Integer.valueOf(displayMetrics.heightPixels), Float.valueOf(displayMetrics.density));
    }

    public static void initialize(Context context, OpenFeintSettings openFeintSettings, OpenFeintDelegate openFeintDelegate) {
        initializeWithoutLoggingIn(context, openFeintSettings, openFeintDelegate);
        OpenFeintInternal instance = getInstance();
        if (instance != null) {
            instance.login(false);
        }
    }

    public static void initializeWithoutLoggingIn(Context context, OpenFeintSettings openFeintSettings, OpenFeintDelegate openFeintDelegate) {
        validateManifest(context);
        if (c == null) {
            c = new OpenFeintInternal(openFeintSettings, context);
        }
        c.l = openFeintDelegate;
        if (!c.j) {
            String userID = c.getUserID();
            if (userID == null) {
                OfflineSupport.setUserTemporary();
            } else {
                OfflineSupport.setUserID(userID);
            }
            c.createDeviceSession();
            return;
        }
        OfflineSupport.setUserDeclined();
    }

    /* access modifiers changed from: private */
    public final User lastLoggedInUser() {
        User loadUser = loadUser();
        SyncedStore.Reader read = getPrefs().read();
        try {
            URL url = new URL(getServerUrl());
            URL url2 = new URL(read.getString("last_logged_in_server", ""));
            if (loadUser != null && url.equals(url2)) {
                read.complete();
                return loadUser;
            }
        } catch (MalformedURLException e2) {
        } catch (Throwable th) {
            read.complete();
            throw th;
        }
        read.complete();
        return null;
    }

    private void loadPropertiesFromXMLResource(Properties properties, int i2) {
        XmlResourceParser xmlResourceParser;
        String str;
        String str2 = null;
        try {
            xmlResourceParser = getContext().getResources().getXml(i2);
        } catch (Exception e2) {
            xmlResourceParser = null;
        }
        if (xmlResourceParser != null) {
            try {
                int eventType = xmlResourceParser.getEventType();
                while (xmlResourceParser.getEventType() != 1) {
                    if (eventType == 2) {
                        str = xmlResourceParser.getName();
                    } else {
                        if (xmlResourceParser.getEventType() == 4) {
                            properties.setProperty(str2, xmlResourceParser.getText());
                        }
                        str = str2;
                    }
                    xmlResourceParser.next();
                    str2 = str;
                    eventType = xmlResourceParser.getEventType();
                }
                xmlResourceParser.close();
            } catch (Exception e3) {
                throw new RuntimeException(e3);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    private User loadUser() {
        SyncedStore.Reader read = getPrefs().read();
        try {
            String string = read.getString("last_logged_in_user", null);
            read.complete();
            return userFromString(string);
        } catch (Throwable th) {
            read.complete();
            throw th;
        }
    }

    public static void log(String str, String str2) {
    }

    public static void makeRequest(BaseRequest baseRequest) {
        OpenFeintInternal instance = getInstance();
        if (instance == null) {
            ServerException serverException = new ServerException();
            serverException.f125a = "NoFeint";
            serverException.b = "OpenFeint has not been initialized.";
            baseRequest.onResponse(0, serverException.generate().getBytes());
            return;
        }
        instance._makeRequest(baseRequest);
    }

    public static void restoreInstanceState(Bundle bundle) {
        getInstance()._restoreInstanceState(bundle);
    }

    public static void saveInstanceState(Bundle bundle) {
        getInstance()._saveInstanceState(bundle);
    }

    private void saveUser(SyncedStore.Editor editor, User user) {
        editor.putString("last_logged_in_user", user.generate());
    }

    private void saveUserApproval(SyncedStore.Editor editor) {
        editor.remove(String.valueOf(getContext().getPackageName()) + ".of_declined");
    }

    /* access modifiers changed from: private */
    public void showOfflineNotification(int i2, Object obj) {
        Resources resources = getContext().getResources();
        String string = resources.getString(R.string.of_offline_notification_line2);
        if (i2 != 0) {
            if (403 == i2) {
                this.h = true;
            }
            if (obj instanceof ServerException) {
                string = ((ServerException) obj).b;
            }
        }
        TwoLineNotification.show(resources.getString(R.string.of_offline_notification), string, Notification.Category.Foreground, Notification.Type.NetworkOffline);
        log("Reachability", "Unable to launch IntroFlow because: " + string);
    }

    private static User userFromString(String str) {
        if (str == null) {
            return null;
        }
        try {
            Object parse = new JsonResourceParser(new n((byte) 0).a(new ByteArrayInputStream(str.getBytes()))).parse();
            if (parse != null && (parse instanceof User)) {
                return (User) parse;
            }
        } catch (IOException e2) {
        }
        return null;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public void userLoggedIn(User user) {
        this.d = new CurrentUser();
        this.d.shallowCopyAncestorType(user);
        User lastLoggedInUser = lastLoggedInUser();
        if (lastLoggedInUser == null || !lastLoggedInUser.resourceID().equals(user.resourceID())) {
            CookieManager.getInstance().removeAllCookie();
        }
        SyncedStore.Editor edit = getPrefs().edit();
        try {
            edit.putString("last_logged_in_server", getServerUrl());
            saveUserApproval(edit);
            saveUser(edit, user);
            edit.commit();
            if (this.l != null) {
                this.l.userLoggedIn(this.d);
            }
            if (this.y != null) {
                this.y.login(this.d);
            }
            if (this.r != null) {
                this.b.post(this.r);
                this.r = null;
            }
            getAnalytics().markSessionOpen(true);
            OfflineSupport.setUserID(user.resourceID());
        } catch (Throwable th) {
            edit.commit();
            throw th;
        }
    }

    private void userLoggedOut() {
        CurrentUser currentUser = this.d;
        this.d = null;
        this.g = false;
        clearPrefs();
        if (this.l != null) {
            this.l.userLoggedOut(currentUser);
        }
        getAnalytics().markSessionClose();
        OfflineSupport.setUserDeclined();
    }

    private static boolean validateManifest(Context context) {
        boolean z;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 1);
            for (String str : new String[]{"com.openfeint.api.ui.Dashboard", "com.openfeint.internal.ui.IntroFlow", "com.openfeint.internal.ui.Settings", "com.openfeint.internal.ui.NativeBrowser"}) {
                ActivityInfo[] activityInfoArr = packageInfo.activities;
                int length = activityInfoArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z = false;
                        break;
                    }
                    ActivityInfo activityInfo = activityInfoArr[i2];
                    if (!activityInfo.name.equals(str)) {
                        i2++;
                    } else if ((activityInfo.configChanges & 128) == 0) {
                        Log.v("OpenFeint", String.format("ActivityInfo for %s has the wrong configChanges.\nPlease consult README.txt for the correct configuration.", str));
                        return false;
                    } else {
                        z = true;
                    }
                }
                if (!z) {
                    Log.v("OpenFeint", String.format("Couldn't find ActivityInfo for %s.\nPlease consult README.txt for the correct configuration.", str));
                    return false;
                }
            }
            for (String noPermission : new String[]{"android.permission.INTERNET"}) {
                if (Util.noPermission(noPermission, context)) {
                    return false;
                }
            }
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.v("OpenFeint", String.format("Couldn't find PackageInfo for %s.\nPlease initialize OF with an Activity that lives in your root package.", context.getPackageName()));
            return false;
        }
    }

    public void createDeviceSession() {
        if (!this.f && !this.g) {
            HashMap hashMap = new HashMap();
            hashMap.put("platform", "android");
            hashMap.put("device", getDeviceParams());
            hashMap.put("of-version", getOFVersion());
            hashMap.put("game_version", Integer.toString(getAppVersion()));
            hashMap.put("protocol_version", "1.0");
            OrderedArgList orderedArgList = new OrderedArgList(hashMap);
            this.f = true;
            _makeRequest(new RawRequest(orderedArgList) {
                public String method() {
                    return "POST";
                }

                public boolean needsDeviceSession() {
                    return false;
                }

                public void onResponse(int i, Object obj) {
                    OpenFeintInternal.this.f = false;
                    if (200 > i || i >= 300) {
                        OpenFeintInternal.this.r = null;
                        OpenFeintInternal.this.showOfflineNotification(i, obj);
                    } else {
                        OpenFeintInternal.this.g = true;
                        if (OpenFeintInternal.this.p != null) {
                            OpenFeintInternal.log(f97a, "Launching post-device-session runnable now.");
                            OpenFeintInternal.this.b.post(OpenFeintInternal.this.p);
                        }
                    }
                    if (OpenFeintInternal.this.q != null) {
                        for (Runnable post : OpenFeintInternal.this.q) {
                            OpenFeintInternal.this.b.post(post);
                        }
                    }
                    OpenFeintInternal.this.p = null;
                    OpenFeintInternal.this.q = null;
                }

                public String path() {
                    return "/xp/devices";
                }
            });
        }
    }

    public void createUser(String str, String str2, String str3, String str4, IRawRequestDelegate iRawRequestDelegate) {
        OrderedArgList orderedArgList = new OrderedArgList();
        orderedArgList.put("user[name]", str);
        orderedArgList.put("user[http_basic_credential_attributes][email]", str2);
        orderedArgList.put("user[http_basic_credential_attributes][password]", str3);
        orderedArgList.put("user[http_basic_credential_attributes][password_confirmation]", str4);
        AnonymousClass1 r1 = new RawRequest(orderedArgList) {
            public String method() {
                return "POST";
            }

            public void onSuccess(Object obj) {
                OpenFeintInternal.this.userLoggedIn((User) obj);
            }

            public String path() {
                return "/xp/users.json";
            }
        };
        r1.setDelegate(iRawRequestDelegate);
        _makeRequest(r1);
    }

    public boolean currentlyLoggingIn() {
        return this.e || this.f;
    }

    public void displayErrorDialog(CharSequence charSequence) {
        SimpleNotification.show(charSequence.toString(), Notification.Category.Foreground, Notification.Type.Error);
    }

    public Analytics getAnalytics() {
        return this.n;
    }

    public String getAppID() {
        return this.m.d;
    }

    public String getAppName() {
        return this.m.f60a;
    }

    public int getAppVersion() {
        if (this.u == -1) {
            Context context = getContext();
            try {
                this.u = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            } catch (Exception e2) {
                this.u = 0;
            }
        }
        return this.u;
    }

    public AbstractHttpClient getClient() {
        return this.f75a;
    }

    public Context getContext() {
        return this.x;
    }

    public CurrentUser getCurrentUser() {
        return this.d;
    }

    public OpenFeintDelegate getDelegate() {
        return this.l;
    }

    public Map getDeviceParams() {
        HashMap hashMap = new HashMap();
        hashMap.put("identifier", getUDID());
        hashMap.put("hardware", getModelString());
        hashMap.put("os", getOSVersionString());
        hashMap.put("screen_resolution", getScreenInfo());
        hashMap.put("processor", getProcessorInfo());
        return hashMap;
    }

    public Properties getInternalProperties() {
        return this.v;
    }

    public String getOFVersion() {
        return getInternalProperties().getProperty("of-version");
    }

    public SyncedStore getPrefs() {
        if (this.o == null) {
            this.o = new SyncedStore(getContext());
        }
        return this.o;
    }

    public int getResource(String str) {
        return getContext().getResources().getIdentifier(str, null, getContext().getPackageName());
    }

    public String getServerUrl() {
        if (this.w == null) {
            String trim = getInternalProperties().getProperty("server-url").toLowerCase().trim();
            if (trim.endsWith("/")) {
                this.w = trim.substring(0, trim.length() - 1);
            } else {
                this.w = trim;
            }
        }
        return this.w;
    }

    public Map getSettings() {
        return this.m.e;
    }

    public String getUDID() {
        if (this.t == null) {
            this.t = findUDID();
        }
        return this.t;
    }

    public String getUserID() {
        CurrentUser currentUser = getCurrentUser();
        if (currentUser != null) {
            return currentUser.userID();
        }
        User lastLoggedInUser = lastLoggedInUser();
        if (lastLoggedInUser != null) {
            return lastLoggedInUser.userID();
        }
        return null;
    }

    public boolean isFeintServerReachable() {
        if (Util.noPermission("android.permission.ACCESS_NETWORK_STATE", getContext())) {
            return true;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean isUserLoggedIn() {
        return getCurrentUser() != null;
    }

    public void launchIntroFlow(final boolean z) {
        if (!checkBan()) {
            if (isFeintServerReachable()) {
                OpenFeintDelegate delegate = getDelegate();
                if (this.i || delegate == null || !delegate.showCustomApprovalFlow(getContext())) {
                    AnonymousClass11 r0 = new Runnable() {
                        public void run() {
                            Intent intent = new Intent(OpenFeintInternal.this.getContext(), IntroFlow.class);
                            if (OpenFeintInternal.this.i && z) {
                                intent.putExtra("content_name", "index?preapproved=true&spotlight=true");
                            } else if (z) {
                                intent.putExtra("content_name", "index?spotlight=true");
                            } else if (OpenFeintInternal.this.i) {
                                intent.putExtra("content_name", "index?preapproved=true");
                            }
                            intent.addFlags(268435456);
                            OpenFeintInternal.this.getContext().startActivity(intent);
                        }
                    };
                    if (this.f || !this.g) {
                        if (!this.f) {
                            createDeviceSession();
                        }
                        this.p = r0;
                        return;
                    }
                    r0.run();
                    return;
                }
                return;
            }
            showOfflineNotification(0, "");
        }
    }

    public void login(final boolean z) {
        this.b.post(new Runnable() {
            public void run() {
                if (!OpenFeintInternal.this.j && !OpenFeintInternal.this.e && !OpenFeintInternal.this.isUserLoggedIn()) {
                    OpenFeintInternal.this.k = true;
                    final User access$17 = OpenFeintInternal.this.lastLoggedInUser();
                    if (access$17 != null) {
                        OpenFeintInternal.log("OpenFeint", "Logging in last known user: " + access$17.f69a);
                        OpenFeintInternal openFeintInternal = OpenFeintInternal.this;
                        final boolean z = z;
                        openFeintInternal.loginUser(null, null, null, new IRawRequestDelegate() {
                            public void onResponse(int i, String str) {
                                if (200 <= i && i < 300) {
                                    SimpleNotification.show("Welcome back " + access$17.f69a, Notification.Category.Login, Notification.Type.Success);
                                } else if (403 == i) {
                                    OpenFeintInternal.this.h = true;
                                } else {
                                    OpenFeintInternal.this.launchIntroFlow(z);
                                }
                            }
                        });
                        return;
                    }
                    OpenFeintInternal.log("OpenFeint", "No last user, launch intro flow");
                    OpenFeintInternal.this.clearPrefs();
                    OpenFeintInternal.this.launchIntroFlow(z);
                }
            }
        });
    }

    public void loginUser(String str, String str2, String str3, IRawRequestDelegate iRawRequestDelegate) {
        final boolean z;
        if (!checkBan()) {
            if (this.f || !this.g) {
                if (!this.f) {
                    createDeviceSession();
                }
                final String str4 = str;
                final String str5 = str2;
                final String str6 = str3;
                final IRawRequestDelegate iRawRequestDelegate2 = iRawRequestDelegate;
                this.p = new Runnable() {
                    public void run() {
                        OpenFeintInternal.this.loginUser(str4, str5, str6, iRawRequestDelegate2);
                    }
                };
                return;
            }
            OrderedArgList orderedArgList = new OrderedArgList();
            if (str == null || str2 == null) {
                z = true;
            } else {
                orderedArgList.put("login", str);
                orderedArgList.put("password", str2);
                z = false;
            }
            if (!(str3 == null || str2 == null)) {
                orderedArgList.put("user_id", str3);
                orderedArgList.put("password", str2);
                z = false;
            }
            this.e = true;
            AnonymousClass4 r2 = new RawRequest(orderedArgList) {
                public String method() {
                    return "POST";
                }

                public void onResponse(int i, Object obj) {
                    OpenFeintInternal.this.e = false;
                    if (200 <= i && i < 300) {
                        OpenFeintInternal.this.userLoggedIn((User) obj);
                        if (OpenFeintInternal.this.r != null) {
                            OpenFeintInternal.log(f97a, "Launching post-login runnable now.");
                            OpenFeintInternal.this.b.post(OpenFeintInternal.this.r);
                        }
                    } else if (z) {
                        OpenFeintInternal.this.showOfflineNotification(i, obj);
                    }
                    if (OpenFeintInternal.this.s != null) {
                        for (Runnable post : OpenFeintInternal.this.s) {
                            OpenFeintInternal.this.b.post(post);
                        }
                    }
                    OpenFeintInternal.this.r = null;
                    OpenFeintInternal.this.s = null;
                }

                public String path() {
                    return "/xp/sessions.json";
                }
            };
            r2.setDelegate(iRawRequestDelegate);
            _makeRequest(r2);
        }
    }

    public void logoutUser(IRawRequestDelegate iRawRequestDelegate) {
        OrderedArgList orderedArgList = new OrderedArgList();
        orderedArgList.put("platform", "android");
        AnonymousClass6 r1 = new RawRequest(orderedArgList) {
            public String method() {
                return "DELETE";
            }

            public String path() {
                return "/xp/sessions.json";
            }
        };
        r1.setDelegate(iRawRequestDelegate);
        _makeRequest(r1);
        userLoggedOut();
    }

    public final void runOnUiThread(Runnable runnable) {
        this.b.post(runnable);
    }

    public void setDelegate(OpenFeintDelegate openFeintDelegate) {
        this.l = openFeintDelegate;
    }

    public void setLoginDelegate(LoginDelegate loginDelegate) {
        this.y = loginDelegate;
    }

    public void submitIntent(final Intent intent, boolean z) {
        this.j = false;
        AnonymousClass5 r0 = new Runnable() {
            public void run() {
                intent.addFlags(268435456);
                OpenFeintInternal.this.getContext().startActivity(intent);
            }
        };
        if (!isUserLoggedIn()) {
            log("OpenFeint", "Not logged in yet - queueing intent " + intent.toString() + " for now.");
            this.r = r0;
            if (!currentlyLoggingIn()) {
                login(z);
                return;
            }
            return;
        }
        this.b.post(r0);
    }

    public void uploadFile(String str, PartSource partSource, String str2, IUploadDelegate iUploadDelegate) {
        final String str3 = str;
        final PartSource partSource2 = partSource;
        final String str4 = str2;
        final IUploadDelegate iUploadDelegate2 = iUploadDelegate;
        _makeRequest(new JSONRequest() {
            public String method() {
                return "POST";
            }

            public void onFailure(String str) {
                if (iUploadDelegate2 != null) {
                    iUploadDelegate2.fileUploadedTo("", false);
                }
            }

            public void onSuccess(Object obj) {
                final BlobUploadParameters blobUploadParameters = (BlobUploadParameters) obj;
                BlobPostRequest blobPostRequest = new BlobPostRequest(blobUploadParameters, partSource2, str4);
                if (iUploadDelegate2 != null) {
                    final IUploadDelegate iUploadDelegate = iUploadDelegate2;
                    blobPostRequest.setDelegate(new IRawRequestDelegate() {
                        public void onResponse(int i, String str) {
                            iUploadDelegate.fileUploadedTo(String.valueOf(blobUploadParameters.f118a) + blobUploadParameters.b, 200 <= i && i < 300);
                        }
                    });
                }
                OpenFeintInternal.this._makeRequest(blobPostRequest);
            }

            public String path() {
                return str3;
            }

            public boolean wantsLogin() {
                return true;
            }
        });
    }

    public void uploadFile(String str, String str2, String str3, IUploadDelegate iUploadDelegate) {
        try {
            String[] split = str2.split("/");
            uploadFile(str, new FilePartSource(split.length > 0 ? split[split.length - 1] : str2, new File(str2)), str3, iUploadDelegate);
        } catch (FileNotFoundException e2) {
            iUploadDelegate.fileUploadedTo("", false);
        }
    }

    public void uploadFile(String str, String str2, byte[] bArr, String str3, IUploadDelegate iUploadDelegate) {
        uploadFile(str, new ByteArrayPartSource(str2, bArr), str3, iUploadDelegate);
    }

    /* JADX INFO: finally extract failed */
    public void userApprovedFeint() {
        this.i = true;
        this.j = false;
        SyncedStore.Editor edit = getPrefs().edit();
        try {
            saveUserApproval(edit);
            edit.commit();
            launchIntroFlow(false);
            OfflineSupport.setUserTemporary();
        } catch (Throwable th) {
            edit.commit();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public void userDeclinedFeint() {
        this.i = false;
        this.j = true;
        SyncedStore.Editor edit = getPrefs().edit();
        try {
            edit.putString(String.valueOf(getContext().getPackageName()) + ".of_declined", "sadly");
            edit.commit();
            OfflineSupport.setUserDeclined();
        } catch (Throwable th) {
            edit.commit();
            throw th;
        }
    }
}
