package com.openfeint.internal;

import android.content.SharedPreferences;
import java.util.Date;

public class Analytics {

    /* renamed from: a  reason: collision with root package name */
    private int f71a;
    private long b;
    private int c;
    private long d;
    private int e;
    private Date f;
    private Date g;

    public Analytics() {
        SharedPreferences sharedPreferences = OpenFeintInternal.getInstance().getContext().getSharedPreferences("FeintAnalytics", 0);
        sharedPreferences.getInt("dashboardLaunches", this.c);
        sharedPreferences.getInt("sessionLaunches", this.f71a);
        sharedPreferences.getInt("onlineSessions", this.e);
        sharedPreferences.getLong("sessionMilliseconds", this.b);
        sharedPreferences.getLong("dashboardMilliseconds", this.d);
    }

    private void update() {
        SharedPreferences.Editor edit = OpenFeintInternal.getInstance().getContext().getSharedPreferences("FeintAnalytics", 0).edit();
        edit.putInt("dashboardLaunches", this.c);
        edit.putInt("sessionLaunches", this.f71a);
        edit.putInt("onlineSessions", this.e);
        edit.putLong("sessionMilliseconds", this.b);
        edit.putLong("dashboardMilliseconds", this.d);
        edit.commit();
    }

    public void markDashboardClose() {
        if (this.f != null) {
            this.d += new Date().getTime() - this.f.getTime();
            this.f = null;
            update();
            return;
        }
        OpenFeintInternal.log("Analytics", "Dashboard closed without known starting time");
    }

    public void markDashboardOpen() {
        this.c++;
        this.f = new Date();
        update();
    }

    public void markSessionClose() {
        if (this.g != null) {
            this.b += new Date().getTime() - this.g.getTime();
            this.g = null;
            update();
            return;
        }
        OpenFeintInternal.log("Analytics", "Session closed without known starting time");
    }

    public void markSessionOpen(boolean z) {
        this.f71a++;
        if (z) {
            this.e++;
        }
        this.g = new Date();
        update();
    }
}
