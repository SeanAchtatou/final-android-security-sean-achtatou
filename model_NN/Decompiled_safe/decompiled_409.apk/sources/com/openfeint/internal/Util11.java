package com.openfeint.internal;

import android.content.Context;
import android.content.res.Configuration;

public class Util11 {
    public static boolean isPad(Context context) {
        try {
            int i = Configuration.class.getField("SCREENLAYOUT_SIZE_XLARGE").getInt(null);
            return (context.getResources().getConfiguration().screenLayout & i) == i;
        } catch (Exception e) {
            return false;
        }
    }
}
