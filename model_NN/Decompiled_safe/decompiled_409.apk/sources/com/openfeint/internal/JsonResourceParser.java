package com.openfeint.internal;

import a.a.a.c;
import a.a.a.h;
import a.a.a.m;
import com.openfeint.internal.resource.ArrayResourceProperty;
import com.openfeint.internal.resource.HashIntResourceProperty;
import com.openfeint.internal.resource.NestedResourceProperty;
import com.openfeint.internal.resource.PrimitiveResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.ResourceProperty;
import java.util.ArrayList;
import java.util.HashMap;

public class JsonResourceParser {

    /* renamed from: a  reason: collision with root package name */
    private h f74a;

    public JsonResourceParser(h hVar) {
        this.f74a = hVar;
    }

    private ArrayList parseArray() {
        m i = this.f74a.i();
        if (i == m.VALUE_NULL) {
            return null;
        }
        if (i == m.START_ARRAY) {
            return parseOpenedArray();
        }
        throw new c("Wanted START_ARRAY", this.f74a.l());
    }

    private HashMap parseHash() {
        m i = this.f74a.i();
        if (i == m.VALUE_NULL) {
            return null;
        }
        if (i == m.START_OBJECT) {
            return parseOpenedHash();
        }
        throw new c("Expected START_OBJECT", this.f74a.l());
    }

    private ArrayList parseOpenedArray() {
        ArrayList arrayList = new ArrayList();
        while (this.f74a.i() != m.END_ARRAY) {
            arrayList.add(parseResource());
        }
        return arrayList;
    }

    private HashMap parseOpenedHash() {
        HashMap hashMap = new HashMap();
        while (this.f74a.i() == m.FIELD_NAME) {
            String m = this.f74a.m();
            this.f74a.i();
            hashMap.put(m, Integer.valueOf(this.f74a.d()));
        }
        return hashMap;
    }

    private Resource parseOpenedResource(ResourceClass resourceClass) {
        Resource factory = resourceClass.factory();
        while (this.f74a.i() == m.FIELD_NAME) {
            ResourceProperty resourceProperty = (ResourceProperty) resourceClass.b.get(this.f74a.m());
            if (resourceProperty == null) {
                this.f74a.i();
                this.f74a.j();
            } else if (resourceProperty instanceof PrimitiveResourceProperty) {
                this.f74a.i();
                ((PrimitiveResourceProperty) resourceProperty).parse(factory, this.f74a);
            } else if (resourceProperty instanceof NestedResourceProperty) {
                NestedResourceProperty nestedResourceProperty = (NestedResourceProperty) resourceProperty;
                ResourceClass klass = Resource.getKlass(nestedResourceProperty.getType());
                if (klass != null) {
                    nestedResourceProperty.set(factory, parseResource(klass));
                } else {
                    OpenFeintInternal.log("JsonResourceParser", "unknown " + nestedResourceProperty.getType());
                }
            } else if (resourceProperty instanceof ArrayResourceProperty) {
                ((ArrayResourceProperty) resourceProperty).set(factory, parseArray());
            } else if (resourceProperty instanceof HashIntResourceProperty) {
                ((HashIntResourceProperty) resourceProperty).set(factory, parseHash());
            } else {
                OpenFeintInternal.log("JsonResourceParser", "Totally don't know what to do about this property");
                this.f74a.j();
            }
        }
        if (this.f74a.q() == m.END_OBJECT) {
            return factory;
        }
        throw new c("Expected END_OBJECT of " + resourceClass.c, this.f74a.l());
    }

    private Resource parseResource() {
        if (this.f74a.i() != m.FIELD_NAME) {
            throw new c("Couldn't find wrapper object.", this.f74a.k());
        }
        String m = this.f74a.m();
        ResourceClass klass = Resource.getKlass(m);
        if (klass == null) {
            throw new c("Don't know class '" + m + "'.", this.f74a.k());
        }
        Resource parseResource = parseResource(klass);
        if (this.f74a.i() == m.END_OBJECT) {
            return parseResource;
        }
        throw new c("Expected only one k/v in wrapper object.", this.f74a.k());
    }

    private Resource parseResource(ResourceClass resourceClass) {
        m i = this.f74a.i();
        if (i == m.VALUE_NULL) {
            return null;
        }
        if (i == m.START_OBJECT) {
            return parseOpenedResource(resourceClass);
        }
        throw new c("Expected START_OBJECT of " + resourceClass.c, this.f74a.l());
    }

    public Object parse() {
        Object parseOpenedResource;
        m i = this.f74a.i();
        if (i == null) {
            return null;
        }
        if (i != m.START_OBJECT) {
            throw new c("Couldn't find toplevel wrapper object.", this.f74a.k());
        } else if (this.f74a.i() != m.FIELD_NAME) {
            throw new c("Couldn't find toplevel wrapper object.", this.f74a.k());
        } else {
            String m = this.f74a.m();
            m i2 = this.f74a.i();
            if (i2 == m.START_ARRAY) {
                parseOpenedResource = parseOpenedArray();
            } else if (i2 == m.START_OBJECT) {
                ResourceClass klass = Resource.getKlass(m);
                if (klass == null) {
                    throw new c("Unknown toplevel class '" + m + "'.", this.f74a.k());
                }
                parseOpenedResource = parseOpenedResource(klass);
            } else {
                throw new c("Expected object or array at top level.", this.f74a.k());
            }
            if (this.f74a.i() == m.END_OBJECT) {
                return parseOpenedResource;
            }
            throw new c("Expected only one k/v in toplevel wrapper object.", this.f74a.k());
        }
    }

    public Object parse(ResourceClass resourceClass) {
        m i = this.f74a.i();
        if (i == null) {
            return null;
        }
        if (i == m.START_OBJECT) {
            return parseOpenedResource(resourceClass);
        }
        throw new c("Couldn't find toplevel wrapper object.", this.f74a.k());
    }
}
