package com.openfeint.internal;

import android.content.Context;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SyncedStore {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public HashMap f84a = new HashMap();
    private Context b;
    /* access modifiers changed from: private */
    public ReentrantReadWriteLock c = new ReentrantReadWriteLock();

    public class Editor {
        public Editor() {
        }

        public void commit() {
            SyncedStore.this.save();
            SyncedStore.this.c.writeLock().unlock();
        }

        public Set keySet() {
            return new HashSet(SyncedStore.this.f84a.keySet());
        }

        public void putString(String str, String str2) {
            SyncedStore.this.f84a.put(str, str2);
        }

        public void remove(String str) {
            SyncedStore.this.f84a.remove(str);
        }
    }

    public class Reader {
        public Reader() {
        }

        public void complete() {
            SyncedStore.this.c.readLock().unlock();
        }

        public String getString(String str, String str2) {
            String str3 = (String) SyncedStore.this.f84a.get(str);
            return str3 != null ? str3 : str2;
        }

        public Set keySet() {
            return SyncedStore.this.f84a.keySet();
        }
    }

    public SyncedStore(Context context) {
        this.b = context;
        load();
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0031 A[SYNTHETIC, Splitter:B:22:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0036 A[Catch:{ IOException -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0051 A[SYNTHETIC, Splitter:B:35:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0070 A[SYNTHETIC, Splitter:B:48:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x008f A[SYNTHETIC, Splitter:B:61:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00a7 A[SYNTHETIC, Splitter:B:71:0x00a7] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap mapFromStore(java.io.File r7) {
        /*
            r6 = this;
            r4 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0025, StreamCorruptedException -> 0x0045, IOException -> 0x0064, ClassNotFoundException -> 0x0083, all -> 0x00a2 }
            r0.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0025, StreamCorruptedException -> 0x0045, IOException -> 0x0064, ClassNotFoundException -> 0x0083, all -> 0x00a2 }
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x00f7, StreamCorruptedException -> 0x00ec, IOException -> 0x00e2, ClassNotFoundException -> 0x00d9, all -> 0x00c9 }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00f7, StreamCorruptedException -> 0x00ec, IOException -> 0x00e2, ClassNotFoundException -> 0x00d9, all -> 0x00c9 }
            java.lang.Object r6 = r1.readObject()     // Catch:{ FileNotFoundException -> 0x00fc, StreamCorruptedException -> 0x00f1, IOException -> 0x00e6, ClassNotFoundException -> 0x00dd, all -> 0x00ce }
            if (r6 == 0) goto L_0x00ba
            boolean r2 = r6 instanceof java.util.HashMap     // Catch:{ FileNotFoundException -> 0x00fc, StreamCorruptedException -> 0x00f1, IOException -> 0x00e6, ClassNotFoundException -> 0x00dd, all -> 0x00ce }
            if (r2 == 0) goto L_0x00ba
            java.util.HashMap r6 = (java.util.HashMap) r6     // Catch:{ FileNotFoundException -> 0x00fc, StreamCorruptedException -> 0x00f1, IOException -> 0x00e6, ClassNotFoundException -> 0x00dd, all -> 0x00ce }
            r1.close()     // Catch:{ IOException -> 0x001c }
        L_0x001a:
            r0 = r6
        L_0x001b:
            return r0
        L_0x001c:
            r0 = move-exception
            java.lang.String r0 = "DistributedPrefs"
            java.lang.String r1 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r0, r1)
            goto L_0x001a
        L_0x0025:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x0028:
            java.lang.String r2 = "DistributedPrefs"
            java.lang.String r3 = "Couldn't open of_prefs"
            com.openfeint.internal.OpenFeintInternal.log(r2, r3)     // Catch:{ all -> 0x00d3 }
            if (r0 == 0) goto L_0x0036
            r0.close()     // Catch:{ IOException -> 0x003c }
        L_0x0034:
            r0 = r4
            goto L_0x001b
        L_0x0036:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x003c }
            goto L_0x0034
        L_0x003c:
            r0 = move-exception
            java.lang.String r0 = "DistributedPrefs"
            java.lang.String r1 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r0, r1)
            goto L_0x0034
        L_0x0045:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x0048:
            java.lang.String r2 = "DistributedPrefs"
            java.lang.String r3 = "StreamCorruptedException"
            com.openfeint.internal.OpenFeintInternal.log(r2, r3)     // Catch:{ all -> 0x00d3 }
            if (r0 == 0) goto L_0x005e
            r0.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0034
        L_0x0055:
            r0 = move-exception
            java.lang.String r0 = "DistributedPrefs"
            java.lang.String r1 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r0, r1)
            goto L_0x0034
        L_0x005e:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0034
        L_0x0064:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x0067:
            java.lang.String r2 = "DistributedPrefs"
            java.lang.String r3 = "IOException while reading"
            com.openfeint.internal.OpenFeintInternal.log(r2, r3)     // Catch:{ all -> 0x00d3 }
            if (r0 == 0) goto L_0x007d
            r0.close()     // Catch:{ IOException -> 0x0074 }
            goto L_0x0034
        L_0x0074:
            r0 = move-exception
            java.lang.String r0 = "DistributedPrefs"
            java.lang.String r1 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r0, r1)
            goto L_0x0034
        L_0x007d:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0074 }
            goto L_0x0034
        L_0x0083:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x0086:
            java.lang.String r2 = "DistributedPrefs"
            java.lang.String r3 = "ClassNotFoundException"
            com.openfeint.internal.OpenFeintInternal.log(r2, r3)     // Catch:{ all -> 0x00d3 }
            if (r0 == 0) goto L_0x009c
            r0.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x0034
        L_0x0093:
            r0 = move-exception
            java.lang.String r0 = "DistributedPrefs"
            java.lang.String r1 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r0, r1)
            goto L_0x0034
        L_0x009c:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x0034
        L_0x00a2:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x00a5:
            if (r1 == 0) goto L_0x00ab
            r1.close()     // Catch:{ IOException -> 0x00b1 }
        L_0x00aa:
            throw r0
        L_0x00ab:
            if (r2 == 0) goto L_0x00aa
            r2.close()     // Catch:{ IOException -> 0x00b1 }
            goto L_0x00aa
        L_0x00b1:
            r1 = move-exception
            java.lang.String r1 = "DistributedPrefs"
            java.lang.String r2 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r1, r2)
            goto L_0x00aa
        L_0x00ba:
            r1.close()     // Catch:{ IOException -> 0x00bf }
            goto L_0x0034
        L_0x00bf:
            r0 = move-exception
            java.lang.String r0 = "DistributedPrefs"
            java.lang.String r1 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r0, r1)
            goto L_0x0034
        L_0x00c9:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x00a5
        L_0x00ce:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x00a5
        L_0x00d3:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x00a5
        L_0x00d9:
            r1 = move-exception
            r1 = r0
            r0 = r4
            goto L_0x0086
        L_0x00dd:
            r2 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0086
        L_0x00e2:
            r1 = move-exception
            r1 = r0
            r0 = r4
            goto L_0x0067
        L_0x00e6:
            r2 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0067
        L_0x00ec:
            r1 = move-exception
            r1 = r0
            r0 = r4
            goto L_0x0048
        L_0x00f1:
            r2 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0048
        L_0x00f7:
            r1 = move-exception
            r1 = r0
            r0 = r4
            goto L_0x0028
        L_0x00fc:
            r2 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.SyncedStore.mapFromStore(java.io.File):java.util.HashMap");
    }

    /* access modifiers changed from: package-private */
    public Editor edit() {
        this.c.writeLock().lock();
        return new Editor();
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x006f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void load() {
        /*
            r12 = this;
            r9 = 0
            r8 = 0
            r12.f84a = r9
            long r1 = java.lang.System.currentTimeMillis()
            android.content.Context r0 = r12.b
            java.lang.String r3 = "of_prefs"
            java.io.File r3 = r0.getFileStreamPath(r3)
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r12.c
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r0 = r0.writeLock()
            r0.lock()
            android.content.Context r0 = r12.b     // Catch:{ IOException -> 0x00d0 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ IOException -> 0x00d0 }
            r4 = 0
            java.util.List r4 = r0.getInstalledApplications(r4)     // Catch:{ IOException -> 0x00d0 }
            java.util.Iterator r5 = r4.iterator()     // Catch:{ IOException -> 0x00d0 }
        L_0x0028:
            boolean r0 = r5.hasNext()     // Catch:{ IOException -> 0x00d0 }
            if (r0 != 0) goto L_0x009c
            r0 = r9
        L_0x002f:
            java.lang.String r5 = r3.getCanonicalPath()     // Catch:{ IOException -> 0x00d0 }
            if (r0 == 0) goto L_0x00f3
            java.lang.String r6 = r0.dataDir     // Catch:{ IOException -> 0x00d0 }
            boolean r6 = r5.startsWith(r6)     // Catch:{ IOException -> 0x00d0 }
            if (r6 == 0) goto L_0x00f3
            java.lang.String r0 = r0.dataDir     // Catch:{ IOException -> 0x00d0 }
            int r0 = r0.length()     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r5 = r5.substring(r0)     // Catch:{ IOException -> 0x00d0 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ IOException -> 0x00d0 }
            r6 = r8
        L_0x004c:
            boolean r0 = r4.hasNext()     // Catch:{ IOException -> 0x00ee }
            if (r0 != 0) goto L_0x00b2
            java.util.HashMap r0 = r12.mapFromStore(r3)     // Catch:{ IOException -> 0x00ee }
            r12.f84a = r0     // Catch:{ IOException -> 0x00ee }
            r0 = r6
        L_0x0059:
            java.util.HashMap r3 = r12.f84a     // Catch:{ IOException -> 0x00f1 }
            if (r3 != 0) goto L_0x0064
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ IOException -> 0x00f1 }
            r3.<init>()     // Catch:{ IOException -> 0x00f1 }
            r12.f84a = r3     // Catch:{ IOException -> 0x00f1 }
        L_0x0064:
            java.util.concurrent.locks.ReentrantReadWriteLock r3 = r12.c
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r3 = r3.writeLock()
            r3.unlock()
        L_0x006d:
            if (r0 == 0) goto L_0x0072
            r12.save()
        L_0x0072:
            long r3 = java.lang.System.currentTimeMillis()
            long r0 = r3 - r1
            java.lang.String r2 = "DistributedPrefs"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Loading prefs took "
            r3.<init>(r4)
            java.lang.Long r4 = new java.lang.Long
            r4.<init>(r0)
            java.lang.String r0 = r4.toString()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r1 = " millis"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.openfeint.internal.OpenFeintInternal.log(r2, r0)
            return
        L_0x009c:
            java.lang.Object r0 = r5.next()     // Catch:{ IOException -> 0x00d0 }
            android.content.pm.ApplicationInfo r0 = (android.content.pm.ApplicationInfo) r0     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r6 = r0.packageName     // Catch:{ IOException -> 0x00d0 }
            android.content.Context r7 = r12.b     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ IOException -> 0x00d0 }
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x00d0 }
            if (r6 == 0) goto L_0x0028
            goto L_0x002f
        L_0x00b2:
            java.lang.Object r0 = r4.next()     // Catch:{ IOException -> 0x00ee }
            android.content.pm.ApplicationInfo r0 = (android.content.pm.ApplicationInfo) r0     // Catch:{ IOException -> 0x00ee }
            java.io.File r7 = new java.io.File     // Catch:{ IOException -> 0x00ee }
            java.lang.String r0 = r0.dataDir     // Catch:{ IOException -> 0x00ee }
            r7.<init>(r0, r5)     // Catch:{ IOException -> 0x00ee }
            long r8 = r3.lastModified()     // Catch:{ IOException -> 0x00ee }
            long r10 = r7.lastModified()     // Catch:{ IOException -> 0x00ee }
            int r0 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x004c
            r0 = 1
            r3 = r7
            r6 = r0
            goto L_0x004c
        L_0x00d0:
            r0 = move-exception
            r0 = r8
        L_0x00d2:
            java.lang.String r3 = "DistributedPrefs"
            java.lang.String r4 = "broken"
            com.openfeint.internal.OpenFeintInternal.log(r3, r4)     // Catch:{ all -> 0x00e3 }
            java.util.concurrent.locks.ReentrantReadWriteLock r3 = r12.c
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r3 = r3.writeLock()
            r3.unlock()
            goto L_0x006d
        L_0x00e3:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r12.c
            java.util.concurrent.locks.ReentrantReadWriteLock$WriteLock r1 = r1.writeLock()
            r1.unlock()
            throw r0
        L_0x00ee:
            r0 = move-exception
            r0 = r6
            goto L_0x00d2
        L_0x00f1:
            r3 = move-exception
            goto L_0x00d2
        L_0x00f3:
            r0 = r8
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.SyncedStore.load():void");
    }

    /* access modifiers changed from: package-private */
    public Reader read() {
        this.c.readLock().lock();
        return new Reader();
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036 A[SYNTHETIC, Splitter:B:16:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x006b A[SYNTHETIC, Splitter:B:33:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void save() {
        /*
            r5 = this;
            r3 = 0
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.lock()
            android.content.Context r0 = r5.b     // Catch:{ IOException -> 0x002a, all -> 0x0066 }
            java.lang.String r1 = "of_prefs"
            r2 = 1
            java.io.FileOutputStream r0 = r0.openFileOutput(r1, r2)     // Catch:{ IOException -> 0x002a, all -> 0x0066 }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x00c9, all -> 0x00b9 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x00c9, all -> 0x00b9 }
            java.util.HashMap r2 = r5.f84a     // Catch:{ IOException -> 0x00ce, all -> 0x00be }
            r1.writeObject(r2)     // Catch:{ IOException -> 0x00ce, all -> 0x00be }
            r1.close()     // Catch:{ IOException -> 0x009b }
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
        L_0x0029:
            return
        L_0x002a:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x002d:
            java.lang.String r2 = "DistributedPrefs"
            java.lang.String r3 = "Couldn't open of_prefs for writing"
            com.openfeint.internal.OpenFeintInternal.log(r2, r3)     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x0043
            r0.close()     // Catch:{ IOException -> 0x0049 }
        L_0x0039:
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            goto L_0x0029
        L_0x0043:
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x0049 }
            goto L_0x0039
        L_0x0049:
            r0 = move-exception
            java.lang.String r0 = "DistributedPrefs"
            java.lang.String r1 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r0, r1)     // Catch:{ all -> 0x005b }
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            goto L_0x0029
        L_0x005b:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
            throw r0
        L_0x0066:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0069:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x007e }
        L_0x006e:
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
        L_0x0077:
            throw r0
        L_0x0078:
            if (r2 == 0) goto L_0x006e
            r2.close()     // Catch:{ IOException -> 0x007e }
            goto L_0x006e
        L_0x007e:
            r1 = move-exception
            java.lang.String r1 = "DistributedPrefs"
            java.lang.String r2 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r1, r2)     // Catch:{ all -> 0x0090 }
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
            goto L_0x0077
        L_0x0090:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
            throw r0
        L_0x009b:
            r0 = move-exception
            java.lang.String r0 = "DistributedPrefs"
            java.lang.String r1 = "IOException while cleaning up"
            com.openfeint.internal.OpenFeintInternal.log(r0, r1)     // Catch:{ all -> 0x00ae }
            java.util.concurrent.locks.ReentrantReadWriteLock r0 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r0 = r0.readLock()
            r0.unlock()
            goto L_0x0029
        L_0x00ae:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantReadWriteLock r1 = r5.c
            java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock r1 = r1.readLock()
            r1.unlock()
            throw r0
        L_0x00b9:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0069
        L_0x00be:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0069
        L_0x00c3:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r0
            r0 = r4
            goto L_0x0069
        L_0x00c9:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x002d
        L_0x00ce:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.SyncedStore.save():void");
    }
}
