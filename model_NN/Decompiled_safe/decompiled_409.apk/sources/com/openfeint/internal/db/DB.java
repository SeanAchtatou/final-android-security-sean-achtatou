package com.openfeint.internal.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.ui.WebViewCache;
import java.io.File;

public class DB {

    /* renamed from: a  reason: collision with root package name */
    public static DataStorageHelperX f88a;

    public class DataStorageHelper extends SQLiteOpenHelper {
        DataStorageHelper(Context context) {
            super(context, "manifest.db", (SQLiteDatabase.CursorFactory) null, 3);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            DB.onUpgrade(sQLiteDatabase, 0, 3);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            DB.onUpgrade(sQLiteDatabase, i, i2);
        }
    }

    public class DataStorageHelperX extends SQLiteOpenHelperX {
        DataStorageHelperX(Context context) {
            super(new DataStorageHelper(context));
        }

        DataStorageHelperX(String str) {
            super(str, 3);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            DB.onUpgrade(sQLiteDatabase, 0, 3);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            DB.onUpgrade(sQLiteDatabase, i, i2);
        }
    }

    public static void createDB(Context context) {
        if (Util.noSdcardPermission()) {
            f88a = new DataStorageHelperX(context);
        } else if ("mounted".equals(Environment.getExternalStorageState())) {
            f88a = new DataStorageHelperX(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/openfeint/webui/manifest.db");
        } else {
            f88a = new DataStorageHelperX(context);
        }
    }

    private static void onCreate(SQLiteDatabase sQLiteDatabase) {
        onUpgrade(sQLiteDatabase, 0, 3);
    }

    /* access modifiers changed from: private */
    public static void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        int i3;
        if (i == 0) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS manifest (path TEXT PRIMARY KEY, hash TEXT);");
            i3 = i + 1;
        } else {
            i3 = i;
        }
        if (i3 == 1) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS store (ID TEXT PRIMARY KEY, VALUE TEXT);");
            i3++;
        }
        if (i3 == 2) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS server_manifest (path TEXT PRIMARY KEY NOT NULL, hash TEXT DEFAULT NULL, is_global INTEGER DEFAULT 0);");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS dependencies (path TEXT NOT NULL, has_dependency TEXT NOT NULL);");
            i3++;
        }
        if (i3 != i2) {
            OpenFeintInternal.log("SQL", String.format("Unable to upgrade DB from %d to %d.", Integer.valueOf(i3), Integer.valueOf(i2)));
        }
    }

    public static boolean recover(Context context) {
        if (f88a != null) {
            f88a.close();
        }
        boolean removeDB = removeDB(context);
        if (!removeDB) {
            return removeDB;
        }
        createDB(context);
        return f88a != null;
    }

    private static boolean removeDB(Context context) {
        return Util.noSdcardPermission() ? context.getDatabasePath("manifest.db").delete() : "mounted".equals(Environment.getExternalStorageState()) ? new File(Environment.getExternalStorageDirectory(), "/openfeint/webui/manifest.db").delete() : context.getDatabasePath("manifest.db").delete();
    }

    public static void setClientManifest(String str, String str2) {
        try {
            f88a.getWritableDatabase().execSQL("INSERT OR REPLACE INTO manifest(path, hash) VALUES(?,?)", new String[]{str, str2});
        } catch (SQLiteDiskIOException e) {
            WebViewCache.diskError();
        } catch (Exception e2) {
            OpenFeintInternal.log("SQL", e2.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        com.openfeint.internal.ui.WebViewCache.diskError();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r0.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x005d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005e, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0064, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0065, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0034 A[ExcHandler: SQLiteDiskIOException (e android.database.sqlite.SQLiteDiskIOException), Splitter:B:2:0x0005] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void setClientManifestBatch(java.lang.String[] r6, java.lang.String[] r7) {
        /*
            r0 = 0
            int r1 = r6.length
            int r2 = r7.length
            if (r1 != r2) goto L_0x0021
            com.openfeint.internal.db.DB$DataStorageHelperX r1 = com.openfeint.internal.db.DB.f88a     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x003e, all -> 0x0051 }
            android.database.sqlite.SQLiteDatabase r0 = r1.getWritableDatabase()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x003e, all -> 0x0051 }
            java.lang.String r1 = "INSERT OR REPLACE INTO manifest(path, hash) VALUES(?,?)"
            android.database.sqlite.SQLiteStatement r1 = r0.compileStatement(r1)     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            r0.beginTransaction()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            r2 = 0
        L_0x0015:
            int r3 = r6.length     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            if (r2 < r3) goto L_0x0022
            r1.close()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            r0.setTransactionSuccessful()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            r0.endTransaction()     // Catch:{ Exception -> 0x005b }
        L_0x0021:
            return
        L_0x0022:
            r3 = 1
            r4 = r6[r2]     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            r1.bindString(r3, r4)     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            r3 = 2
            r4 = r7[r2]     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            r1.bindString(r3, r4)     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            r1.execute()     // Catch:{ SQLiteDiskIOException -> 0x0034, Exception -> 0x0064 }
            int r2 = r2 + 1
            goto L_0x0015
        L_0x0034:
            r1 = move-exception
            com.openfeint.internal.ui.WebViewCache.diskError()     // Catch:{ all -> 0x005d }
            r0.endTransaction()     // Catch:{ Exception -> 0x003c }
            goto L_0x0021
        L_0x003c:
            r0 = move-exception
            goto L_0x0021
        L_0x003e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0042:
            java.lang.String r2 = "SQL"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0062 }
            com.openfeint.internal.OpenFeintInternal.log(r2, r0)     // Catch:{ all -> 0x0062 }
            r1.endTransaction()     // Catch:{ Exception -> 0x004f }
            goto L_0x0021
        L_0x004f:
            r0 = move-exception
            goto L_0x0021
        L_0x0051:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0055:
            r1.endTransaction()     // Catch:{ Exception -> 0x0059 }
        L_0x0058:
            throw r0
        L_0x0059:
            r1 = move-exception
            goto L_0x0058
        L_0x005b:
            r0 = move-exception
            goto L_0x0021
        L_0x005d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0055
        L_0x0062:
            r0 = move-exception
            goto L_0x0055
        L_0x0064:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.db.DB.setClientManifestBatch(java.lang.String[], java.lang.String[]):void");
    }
}
