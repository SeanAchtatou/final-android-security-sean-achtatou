package com.openfeint.internal.notifications;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.openfeint.api.Notification;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.BitmapRequest;
import hamon05.speed.pac.R;
import java.util.Map;

public class SimpleNotification extends NotificationBase {
    protected SimpleNotification(String str, String str2, Notification.Category category, Notification.Type type, Map map) {
        super(str, str2, category, type, map);
    }

    public static void show(String str, Notification.Category category, Notification.Type type) {
        show(str, null, category, type);
    }

    public static void show(String str, String str2, Notification.Category category, Notification.Type type) {
        new SimpleNotification(str, str2, category, type, null).checkDelegateAndView();
    }

    /* access modifiers changed from: protected */
    public boolean createView() {
        this.b = ((LayoutInflater) OpenFeintInternal.getInstance().getContext().getSystemService("layout_inflater")).inflate((int) R.layout.of_simple_notification, (ViewGroup) null);
        ((TextView) this.b.findViewById(R.id.of_text)).setText(getText());
        final ImageView imageView = (ImageView) this.b.findViewById(R.id.of_icon);
        if (this.f90a != null) {
            Drawable resourceDrawable = getResourceDrawable(this.f90a);
            if (resourceDrawable == null) {
                new BitmapRequest() {
                    public void onFailure(String str) {
                        OpenFeintInternal.log("NotificationImage", "Failed to load image " + SimpleNotification.this.f90a + ":" + str);
                        imageView.setVisibility(4);
                        SimpleNotification.this.showToast();
                    }

                    public void onSuccess(Bitmap bitmap) {
                        imageView.setImageDrawable(new BitmapDrawable(bitmap));
                        SimpleNotification.this.showToast();
                    }

                    public String path() {
                        return SimpleNotification.this.f90a;
                    }
                }.launch();
                return false;
            }
            imageView.setImageDrawable(resourceDrawable);
        } else {
            imageView.setVisibility(4);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void drawView(Canvas canvas) {
        this.b.draw(canvas);
    }
}
