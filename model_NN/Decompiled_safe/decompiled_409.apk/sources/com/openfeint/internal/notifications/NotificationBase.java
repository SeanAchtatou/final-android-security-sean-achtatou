package com.openfeint.internal.notifications;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Toast;
import com.openfeint.api.Notification;
import com.openfeint.internal.OpenFeintInternal;
import java.util.HashMap;
import java.util.Map;

public abstract class NotificationBase extends Notification {
    private static Map h = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    protected String f90a;
    View b;
    Toast c;
    private String d;
    private Notification.Category e;
    private Notification.Type f;
    private Map g;

    protected NotificationBase(String str, String str2, Notification.Category category, Notification.Type type, Map map) {
        this.d = str;
        this.f90a = str2;
        this.e = category;
        this.f = type;
        this.g = map;
    }

    static Drawable getResourceDrawable(String str) {
        if (!h.containsKey(str)) {
            OpenFeintInternal instance = OpenFeintInternal.getInstance();
            int resource = instance.getResource(str);
            if (resource == 0) {
                h.put(str, null);
            } else {
                h.put(str, instance.getContext().getResources().getDrawable(resource));
            }
        }
        return (Drawable) h.get(str);
    }

    /* access modifiers changed from: protected */
    public void checkDelegateAndView() {
        if (getDelegate().canShowNotification(this)) {
            getDelegate().notificationWillShow(this);
            if (createView()) {
                showToast();
                return;
            }
            return;
        }
        getDelegate().displayNotification(this);
    }

    /* access modifiers changed from: protected */
    public String clippedText(Paint paint, String str, int i) {
        int breakText = paint.breakText(str, true, (float) i, null);
        return breakText < str.length() ? String.valueOf(str.substring(0, breakText - 1)) + "..." : str;
    }

    /* access modifiers changed from: protected */
    public abstract boolean createView();

    /* access modifiers changed from: protected */
    public abstract void drawView(Canvas canvas);

    public Notification.Category getCategory() {
        return this.e;
    }

    public String getText() {
        return this.d;
    }

    public Notification.Type getType() {
        return this.f;
    }

    public Map getUserData() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void showToast() {
        OpenFeintInternal.getInstance().runOnUiThread(new Runnable() {
            public void run() {
                Context context = OpenFeintInternal.getInstance().getContext();
                NotificationBase.this.c = new Toast(context);
                NotificationBase.this.c.setGravity(80, 0, 0);
                NotificationBase.this.c.setDuration(1);
                NotificationBase.this.c.setView(NotificationBase.this.b);
                NotificationBase.this.c.show();
            }
        });
    }
}
