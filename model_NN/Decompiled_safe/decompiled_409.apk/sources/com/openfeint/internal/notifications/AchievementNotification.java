package com.openfeint.internal.notifications;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.openfeint.api.Notification;
import com.openfeint.api.resource.Achievement;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.ExternalBitmapRequest;
import hamon05.speed.pac.R;
import java.util.HashMap;
import java.util.Map;

public class AchievementNotification extends NotificationBase {
    protected AchievementNotification(Achievement achievement, Map map) {
        super(OpenFeintInternal.getRString(R.string.of_achievement_unlocked), null, Notification.Category.Achievement, Notification.Type.Success, map);
    }

    public static void showStatus(Achievement achievement) {
        HashMap hashMap = new HashMap();
        hashMap.put("achievement", achievement);
        new AchievementNotification(achievement, hashMap).checkDelegateAndView();
    }

    /* access modifiers changed from: protected */
    public boolean createView() {
        final Achievement achievement = (Achievement) getUserData().get("achievement");
        this.b = ((LayoutInflater) OpenFeintInternal.getInstance().getContext().getSystemService("layout_inflater")).inflate((int) R.layout.of_achievement_notification, (ViewGroup) null);
        if (achievement.f) {
            this.b.findViewById(R.id.of_achievement_progress_icon).setVisibility(4);
            if (achievement.c == 0) {
                this.b.findViewById(R.id.of_achievement_score_icon).setVisibility(4);
                this.b.findViewById(R.id.of_achievement_score).setVisibility(4);
            }
        } else {
            this.b.findViewById(R.id.of_achievement_score_icon).setVisibility(4);
        }
        ((TextView) this.b.findViewById(R.id.of_achievement_text)).setText((achievement.f62a == null || achievement.f62a.length() <= 0) ? OpenFeintInternal.getRString(R.string.of_achievement_unlocked) : achievement.f62a);
        ((TextView) this.b.findViewById(R.id.of_achievement_score)).setText(achievement.f ? Integer.toString(achievement.c) : String.format("%d%%", Integer.valueOf((int) achievement.g)));
        if (achievement.d != null) {
            Drawable resourceDrawable = getResourceDrawable(achievement.d);
            if (resourceDrawable == null) {
                new ExternalBitmapRequest(achievement.d) {
                    public void onFailure(String str) {
                        OpenFeintInternal.log("NotificationImage", "Failed to load image " + achievement.d + ":" + str);
                        AchievementNotification.this.showToast();
                    }

                    public void onSuccess(Bitmap bitmap) {
                        ((ImageView) AchievementNotification.this.b.findViewById(R.id.of_achievement_icon)).setImageDrawable(new BitmapDrawable(bitmap));
                        AchievementNotification.this.showToast();
                    }
                }.launch();
                return false;
            }
            ((ImageView) this.b.findViewById(R.id.of_achievement_icon)).setImageDrawable(resourceDrawable);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void drawView(Canvas canvas) {
        this.b.draw(canvas);
    }

    public void loadedImage(Bitmap bitmap) {
        this.b.invalidate();
    }
}
