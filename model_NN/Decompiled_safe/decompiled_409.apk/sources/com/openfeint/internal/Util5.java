package com.openfeint.internal;

import android.content.Context;

public class Util5 {
    public static String getAccountNameEclair(Context context) {
        try {
            Class<?> cls = Class.forName("android.accounts.AccountManager");
            Object invoke = cls.getMethod("get", Context.class).invoke(cls, context);
            return (String) Class.forName("android.accounts.Account").getField("name").get(((Object[]) cls.getMethod("getAccountsByType", String.class).invoke(invoke, "com.google"))[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
