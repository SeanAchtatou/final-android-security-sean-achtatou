package com.openfeint.internal;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.provider.MediaStore;
import android.widget.Toast;
import com.openfeint.internal.OpenFeintInternal;
import hamon05.speed.pac.R;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ImagePicker {
    public static void compressAndUpload(Bitmap bitmap, String str, OpenFeintInternal.IUploadDelegate iUploadDelegate) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        upload(str, byteArrayOutputStream, iUploadDelegate);
    }

    public static boolean isImagePickerActivityResult(int i) {
        return i == 10009;
    }

    public static Bitmap onImagePickerActivityResult(Activity activity, int i, int i2, Intent intent) {
        if (i == -1) {
            String[] strArr = {"_data", "orientation"};
            Cursor query = activity.getContentResolver().query(intent.getData(), strArr, null, null, null);
            if (query != null) {
                query.moveToFirst();
                String string = query.getString(query.getColumnIndex(strArr[0]));
                int i3 = query.getInt(query.getColumnIndex(strArr[1]));
                query.close();
                Bitmap resize = resize(string, i2, i3);
                OpenFeintInternal.log("ImagePicker", "image! " + resize.getWidth() + "x" + resize.getHeight());
                return resize;
            }
            Toast.makeText(OpenFeintInternal.getInstance().getContext(), OpenFeintInternal.getRString(R.string.of_profile_picture_download_failed), 1).show();
        }
        return null;
    }

    private static Bitmap preScaleImage(String str, int i) {
        File file = new File(str);
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file), null, options);
            int min = Math.min(options.outWidth, options.outHeight);
            int i2 = 1;
            while (min / 2 > i) {
                min /= 2;
                i2++;
            }
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inSampleSize = i2;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, options2);
        } catch (FileNotFoundException e) {
            OpenFeintInternal.log("ImagePicker", e.toString());
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap resize(String str, int i, int i2) {
        Bitmap preScaleImage = preScaleImage(str, i);
        int width = preScaleImage.getWidth();
        int height = preScaleImage.getHeight();
        boolean z = height > width;
        int i3 = z ? 0 : (width - height) / 2;
        int i4 = z ? (height - width) / 2 : 0;
        if (!z) {
            width = height;
        }
        float f = ((float) i) / ((float) width);
        Matrix matrix = new Matrix();
        matrix.postScale(f, f);
        matrix.postRotate((float) i2);
        return Bitmap.createBitmap(preScaleImage, i3, i4, width, width, matrix, false);
    }

    public static void show(Activity activity) {
        ((ActivityManager) activity.getSystemService("activity")).getMemoryInfo(new ActivityManager.MemoryInfo());
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        activity.startActivityForResult(intent, 10009);
    }

    private static void upload(String str, ByteArrayOutputStream byteArrayOutputStream, OpenFeintInternal.IUploadDelegate iUploadDelegate) {
        OpenFeintInternal.getInstance().uploadFile(str, "profile.png", byteArrayOutputStream.toByteArray(), "image/png", iUploadDelegate);
    }
}
