package com.openfeint.internal;

import android.os.Bundle;
import com.openfeint.internal.SyncedStore;
import java.util.Date;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;

public class CookieStore extends BasicCookieStore {

    /* renamed from: a  reason: collision with root package name */
    private static int f72a = "_of_cookie_".length();
    private static int b = "value".length();
    private SyncedStore c;

    public CookieStore(SyncedStore syncedStore) {
        this.c = syncedStore;
        SyncedStore.Reader read = this.c.read();
        try {
            for (String str : read.keySet()) {
                if (str.startsWith("_of_cookie_") && str.endsWith("value")) {
                    CookieStore.super.addCookie(cookieFromPrefs(read, str.substring(f72a, str.length() - b)));
                }
            }
        } finally {
            read.complete();
        }
    }

    private BasicClientCookie cookieFromBundle(Bundle bundle, String str) {
        String str2 = "_of_cookie_" + str;
        String string = bundle.getString(String.valueOf(str2) + "value");
        if (string == null) {
            return null;
        }
        String string2 = bundle.getString(String.valueOf(str2) + "path");
        String string3 = bundle.getString(String.valueOf(str2) + "domain");
        String string4 = bundle.getString(String.valueOf(str2) + "expiry");
        BasicClientCookie basicClientCookie = new BasicClientCookie(str, string);
        basicClientCookie.setPath(string2);
        basicClientCookie.setDomain(string3);
        if (string4 != null) {
            basicClientCookie.setExpiryDate(dateFromString(string4));
        }
        return basicClientCookie;
    }

    private BasicClientCookie cookieFromPrefs(SyncedStore.Reader reader, String str) {
        String str2 = "_of_cookie_" + str;
        String string = reader.getString(String.valueOf(str2) + "value", null);
        if (string == null) {
            return null;
        }
        String string2 = reader.getString(String.valueOf(str2) + "path", null);
        String string3 = reader.getString(String.valueOf(str2) + "domain", null);
        String string4 = reader.getString(String.valueOf(str2) + "expiry", null);
        BasicClientCookie basicClientCookie = new BasicClientCookie(str, string);
        basicClientCookie.setPath(string2);
        basicClientCookie.setDomain(string3);
        basicClientCookie.setExpiryDate(dateFromString(string4));
        return basicClientCookie;
    }

    private void cookieToBundle(Cookie cookie, Bundle bundle) {
        String name = cookie.getName();
        bundle.putString("_of_cookie_" + name + "value", cookie.getValue());
        bundle.putString("_of_cookie_" + name + "path", cookie.getPath());
        bundle.putString("_of_cookie_" + name + "domain", cookie.getDomain());
        Date expiryDate = cookie.getExpiryDate();
        if (expiryDate != null) {
            bundle.putString("_of_cookie_" + name + "expiry", stringFromDate(expiryDate));
        }
    }

    private void cookieToPrefs(Cookie cookie, SyncedStore.Editor editor) {
        String name = cookie.getName();
        editor.putString("_of_cookie_" + name + "value", cookie.getValue());
        editor.putString("_of_cookie_" + name + "path", cookie.getPath());
        editor.putString("_of_cookie_" + name + "domain", cookie.getDomain());
        editor.putString("_of_cookie_" + name + "expiry", stringFromDate(cookie.getExpiryDate()));
    }

    private static final Date dateFromString(String str) {
        try {
            return DateUtils.parseDate(str);
        } catch (DateParseException e) {
            OpenFeintInternal.log("CookieStore", "Couldn't parse date: '" + str + "'");
            return null;
        }
    }

    private static final String stringFromDate(Date date) {
        return DateUtils.formatDate(date);
    }

    public synchronized void addCookie(Cookie cookie) {
        SyncedStore.Editor edit;
        CookieStore.super.addCookie(cookie);
        String name = cookie.getName();
        SyncedStore.Reader read = this.c.read();
        try {
            BasicClientCookie cookieFromPrefs = cookieFromPrefs(read, name);
            read.complete();
            if (cookieFromPrefs == null || !cookieFromPrefs.getValue().equals(cookie.getValue()) || !cookieFromPrefs.getPath().equals(cookie.getPath()) || !cookieFromPrefs.getDomain().equals(cookie.getDomain()) || !cookieFromPrefs.getExpiryDate().equals(cookie.getExpiryDate())) {
                edit = this.c.edit();
                String str = "_of_cookie_" + name;
                for (String str2 : edit.keySet()) {
                    if (str2.startsWith(str)) {
                        edit.remove(str2);
                    }
                }
                if (cookie.getExpiryDate() != null) {
                    cookieToPrefs(cookie, edit);
                }
                edit.commit();
            }
        } catch (Throwable th) {
            read.complete();
            throw th;
        }
    }

    public synchronized void clearCookies(SyncedStore.Editor editor) {
        for (String str : editor.keySet()) {
            if (str.startsWith("_of_cookie_")) {
                editor.remove(str);
            }
        }
        CookieStore.super.clear();
    }

    public synchronized void restoreInstanceState(Bundle bundle) {
        for (String next : bundle.keySet()) {
            if (next.startsWith("_of_cookie_") && next.endsWith("value")) {
                CookieStore.super.addCookie(cookieFromBundle(bundle, next.substring(f72a, next.length() - b)));
            }
        }
    }

    public synchronized void saveInstanceState(Bundle bundle) {
        for (Cookie cookieToBundle : CookieStore.super.getCookies()) {
            cookieToBundle(cookieToBundle, bundle);
        }
    }
}
