package com.openfeint.internal.resource;

public abstract class NestedResourceProperty extends ResourceProperty {

    /* renamed from: a  reason: collision with root package name */
    private Class f121a;

    public NestedResourceProperty(Class cls) {
        this.f121a = cls;
    }

    public abstract Resource get(Resource resource);

    public Class getType() {
        return this.f121a;
    }

    public abstract void set(Resource resource, Resource resource2);
}
