package com.openfeint.internal.resource;

import a.a.a.f;
import a.a.a.h;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public abstract class DateResourceProperty extends PrimitiveResourceProperty {

    /* renamed from: a  reason: collision with root package name */
    public static DateFormat f119a = makeDateParser();

    static DateFormat makeDateParser() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    public void copy(Resource resource, Resource resource2) {
        set(resource, get(resource2));
    }

    public void generate(Resource resource, f fVar, String str) {
        Date date = get(resource);
        if (date != null) {
            fVar.a(str);
            fVar.b(f119a.format(date));
        }
    }

    public abstract Date get(Resource resource);

    public void parse(Resource resource, h hVar) {
        String m = hVar.m();
        if (m.equals("null")) {
            set(resource, null);
            return;
        }
        try {
            set(resource, f119a.parse(m));
        } catch (ParseException e) {
            set(resource, null);
        }
    }

    public abstract void set(Resource resource, Date date);
}
