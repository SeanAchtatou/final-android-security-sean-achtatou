package com.openfeint.internal.resource;

import a.a.a.f;
import a.a.a.h;
import a.a.a.m;

public abstract class StringResourceProperty extends PrimitiveResourceProperty {
    public void copy(Resource resource, Resource resource2) {
        set(resource, get(resource2));
    }

    public void generate(Resource resource, f fVar, String str) {
        String str2 = get(resource);
        if (str2 != null) {
            fVar.a(str);
            fVar.b(str2);
        }
    }

    public abstract String get(Resource resource);

    public void parse(Resource resource, h hVar) {
        if (hVar.q() == m.VALUE_NULL) {
            set(resource, null);
        } else {
            set(resource, hVar.m());
        }
    }

    public abstract void set(Resource resource, String str);
}
