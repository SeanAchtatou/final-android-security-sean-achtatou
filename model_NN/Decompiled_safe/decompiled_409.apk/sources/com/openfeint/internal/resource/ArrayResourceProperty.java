package com.openfeint.internal.resource;

import java.util.List;

public abstract class ArrayResourceProperty extends ResourceProperty {

    /* renamed from: a  reason: collision with root package name */
    private Class f117a;

    public ArrayResourceProperty(Class cls) {
        this.f117a = cls;
    }

    public Class elementType() {
        return this.f117a;
    }

    public abstract List get(Resource resource);

    public abstract void set(Resource resource, List list);
}
