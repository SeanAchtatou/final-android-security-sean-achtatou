package com.openfeint.internal.resource;

import a.a.a.f;
import a.a.a.h;

public abstract class EnumResourceProperty extends PrimitiveResourceProperty {

    /* renamed from: a  reason: collision with root package name */
    private Class f120a;

    public EnumResourceProperty(Class cls) {
        this.f120a = cls;
    }

    public void copy(Resource resource, Resource resource2) {
        set(resource, get(resource2));
    }

    public void generate(Resource resource, f fVar, String str) {
        Enum enumR = get(resource);
        fVar.a(str);
        fVar.b(enumR.toString());
    }

    public abstract Enum get(Resource resource);

    public void parse(Resource resource, h hVar) {
        set(resource, Enum.valueOf(this.f120a, hVar.m()));
    }

    public abstract void set(Resource resource, Enum enumR);
}
