package com.openfeint.internal.resource;

import java.util.HashMap;

public abstract class ResourceClass {

    /* renamed from: a  reason: collision with root package name */
    public Class f123a;
    public HashMap b = new HashMap();
    public String c;

    public ResourceClass(Class cls, String str) {
        this.f123a = cls;
        this.c = str;
        mixinParentProperties(cls);
    }

    private void mixinParentProperties(Class cls) {
        if (cls != Resource.class) {
            Class superclass = cls.getSuperclass();
            mixinParentProperties(superclass);
            ResourceClass klass = Resource.getKlass(superclass);
            for (String str : klass.b.keySet()) {
                this.b.put(str, (ResourceProperty) klass.b.get(str));
            }
        }
    }

    public abstract Resource factory();
}
