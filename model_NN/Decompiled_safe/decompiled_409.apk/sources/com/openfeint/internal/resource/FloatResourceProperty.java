package com.openfeint.internal.resource;

import a.a.a.c;
import a.a.a.f;
import a.a.a.h;

public abstract class FloatResourceProperty extends PrimitiveResourceProperty {
    public void copy(Resource resource, Resource resource2) {
        set(resource, get(resource2));
    }

    public void generate(Resource resource, f fVar, String str) {
        fVar.a(str);
        fVar.a(get(resource));
    }

    public abstract float get(Resource resource);

    public void parse(Resource resource, h hVar) {
        try {
            set(resource, hVar.f());
        } catch (c e) {
        }
    }

    public abstract void set(Resource resource, float f);
}
