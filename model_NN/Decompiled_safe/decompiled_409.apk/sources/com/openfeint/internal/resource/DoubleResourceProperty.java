package com.openfeint.internal.resource;

import a.a.a.f;
import a.a.a.h;

public abstract class DoubleResourceProperty extends PrimitiveResourceProperty {
    public void copy(Resource resource, Resource resource2) {
        set(resource, get(resource2));
    }

    public void generate(Resource resource, f fVar, String str) {
        fVar.a(str);
        fVar.a(get(resource));
    }

    public abstract double get(Resource resource);

    public void parse(Resource resource, h hVar) {
        set(resource, hVar.g());
    }

    public abstract void set(Resource resource, double d);
}
