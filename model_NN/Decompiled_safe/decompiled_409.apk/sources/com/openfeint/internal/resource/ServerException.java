package com.openfeint.internal.resource;

public class ServerException extends Resource {

    /* renamed from: a  reason: collision with root package name */
    public String f125a;
    public String b;
    public boolean c;

    public ServerException() {
    }

    public ServerException(String str, String str2) {
        this.f125a = str;
        this.b = str2;
    }

    public static ResourceClass getResourceClass() {
        AnonymousClass1 r0 = new ResourceClass(ServerException.class, "exception") {
            public Resource factory() {
                return new ServerException();
            }
        };
        r0.b.put("class", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((ServerException) resource).f125a;
            }

            public void set(Resource resource, String str) {
                ((ServerException) resource).f125a = str;
            }
        });
        r0.b.put("message", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((ServerException) resource).b;
            }

            public void set(Resource resource, String str) {
                ((ServerException) resource).b = str;
            }
        });
        r0.b.put("needs_developer_attention", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((ServerException) resource).c;
            }

            public void set(Resource resource, boolean z) {
                ((ServerException) resource).c = z;
            }
        });
        return r0;
    }
}
