package com.openfeint.internal.resource;

import a.a.a.b;
import a.a.a.f;
import a.a.a.n;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.resource.ServerTimestamp;
import com.openfeint.api.resource.User;
import com.openfeint.internal.OpenFeintInternal;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Resource {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap f122a = new HashMap();
    private static HashMap b = new HashMap();
    String l;

    static {
        registerSubclass(getResourceClass());
        registerSubclass(ServerException.getResourceClass());
        registerSubclass(ServerTimestamp.getResourceClass());
        registerSubclass(BlobUploadParameters.getResourceClass());
        registerSubclass(Achievement.getResourceClass());
        registerSubclass(Leaderboard.getResourceClass());
        registerSubclass(Score.getResourceClass());
        registerSubclass(User.getResourceClass());
        registerSubclass(CurrentUser.getResourceClass());
    }

    public static ResourceClass getKlass(Class cls) {
        return (ResourceClass) f122a.get(cls);
    }

    public static ResourceClass getKlass(String str) {
        return (ResourceClass) b.get(str);
    }

    public static ResourceClass getResourceClass() {
        AnonymousClass1 r0 = new ResourceClass(Resource.class, null) {
            public Resource factory() {
                return null;
            }
        };
        r0.b.put("id", new StringResourceProperty() {
            public String get(Resource resource) {
                return resource.l;
            }

            public void set(Resource resource, String str) {
                resource.l = str;
            }
        });
        return r0;
    }

    public static void registerSubclass(ResourceClass resourceClass) {
        f122a.put(resourceClass.f123a, resourceClass);
        if (resourceClass.c != null) {
            b.put(resourceClass.c, resourceClass);
        }
    }

    private final void unguardedShallowCopy(Resource resource) {
        for (Map.Entry value : getKlass(resource.getClass()).b.entrySet()) {
            ResourceProperty resourceProperty = (ResourceProperty) value.getValue();
            if (resourceProperty instanceof PrimitiveResourceProperty) {
                ((PrimitiveResourceProperty) resourceProperty).copy(this, resource);
            } else if (resourceProperty instanceof ArrayResourceProperty) {
                ((ArrayResourceProperty) resourceProperty).set(this, ((ArrayResourceProperty) resourceProperty).get(resource));
            } else if (resourceProperty instanceof NestedResourceProperty) {
                ((NestedResourceProperty) resourceProperty).set(this, ((NestedResourceProperty) resourceProperty).get(resource));
            }
        }
    }

    public final String generate() {
        StringWriter stringWriter = new StringWriter();
        try {
            f a2 = new n((byte) 0).a(stringWriter);
            generate(a2);
            a2.close();
            return stringWriter.toString();
        } catch (IOException e) {
            OpenFeintInternal.log("Resource", e.getMessage());
            return null;
        }
    }

    public final void generate(f fVar) {
        HashMap hashMap;
        ResourceClass klass = getKlass(getClass());
        fVar.c();
        fVar.a(klass.c);
        fVar.c();
        for (Map.Entry entry : klass.b.entrySet()) {
            ResourceProperty resourceProperty = (ResourceProperty) entry.getValue();
            if (resourceProperty instanceof PrimitiveResourceProperty) {
                ((PrimitiveResourceProperty) resourceProperty).generate(this, fVar, (String) entry.getKey());
            } else if (resourceProperty instanceof ArrayResourceProperty) {
                ArrayResourceProperty arrayResourceProperty = (ArrayResourceProperty) resourceProperty;
                List<Resource> list = arrayResourceProperty.get(this);
                if (list != null) {
                    fVar.a((String) entry.getKey());
                    ResourceClass klass2 = getKlass(arrayResourceProperty.elementType());
                    fVar.c();
                    fVar.a(String.valueOf(klass2.c) + "s");
                    fVar.a();
                    for (Resource generate : list) {
                        generate.generate(fVar);
                    }
                    fVar.b();
                    fVar.d();
                }
            } else if (resourceProperty instanceof NestedResourceProperty) {
                Resource resource = ((NestedResourceProperty) resourceProperty).get(this);
                if (resource != null) {
                    fVar.a((String) entry.getKey());
                    resource.generate(fVar);
                }
            } else if ((resourceProperty instanceof HashIntResourceProperty) && (hashMap = ((HashIntResourceProperty) resourceProperty).get(this)) != null && hashMap.size() > 0) {
                fVar.a((String) entry.getKey());
                fVar.c();
                for (Map.Entry entry2 : hashMap.entrySet()) {
                    fVar.a((String) entry2.getKey());
                    fVar.a(((Integer) entry2.getValue()).intValue());
                }
                fVar.d();
            }
        }
        fVar.d();
        fVar.d();
    }

    public final void generateToStream(OutputStream outputStream) {
        f a2 = new n((byte) 0).a(outputStream, b.UTF8);
        generate(a2);
        a2.close();
    }

    public String resourceID() {
        return this.l;
    }

    public void setResourceID(String str) {
        this.l = str;
    }

    public final void shallowCopy(Resource resource) {
        if (resource.getClass() != getClass()) {
            throw new UnsupportedOperationException("You can only shallowCopy the same type of resource");
        }
        unguardedShallowCopy(resource);
    }

    public final void shallowCopyAncestorType(Resource resource) {
        Class<?> cls = getClass();
        Class<?> cls2 = resource.getClass();
        if (cls2 != Resource.class) {
            while (cls != cls2 && cls != Resource.class) {
                cls = cls.getSuperclass();
            }
            if (cls == Resource.class) {
                throw new UnsupportedOperationException(String.valueOf(cls2.getName()) + " is not a superclass of " + getClass().getName());
            }
        }
        unguardedShallowCopy(resource);
    }
}
