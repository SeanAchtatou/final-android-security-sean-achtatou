package com.openfeint.internal.resource;

import a.a.a.f;
import a.a.a.h;

public abstract class PrimitiveResourceProperty extends ResourceProperty {
    public abstract void copy(Resource resource, Resource resource2);

    public abstract void generate(Resource resource, f fVar, String str);

    public abstract void parse(Resource resource, h hVar);
}
