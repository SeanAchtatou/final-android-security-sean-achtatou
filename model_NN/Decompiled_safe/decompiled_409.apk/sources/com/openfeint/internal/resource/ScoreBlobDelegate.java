package com.openfeint.internal.resource;

import com.openfeint.api.resource.Score;

public final class ScoreBlobDelegate {

    /* renamed from: a  reason: collision with root package name */
    public static Score.BlobDownloadedDelegate f124a = null;

    public static final void notifyBlobDownloaded(Score score) {
        if (f124a != null && score.i != null) {
            f124a.blobDownloadedForScore(score);
        }
    }
}
