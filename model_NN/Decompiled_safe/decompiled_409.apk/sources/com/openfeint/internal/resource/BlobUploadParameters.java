package com.openfeint.internal.resource;

public class BlobUploadParameters extends Resource {

    /* renamed from: a  reason: collision with root package name */
    public String f118a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;

    public static ResourceClass getResourceClass() {
        AnonymousClass1 r0 = new ResourceClass(BlobUploadParameters.class, "blob_upload_parameters") {
            public Resource factory() {
                return new BlobUploadParameters();
            }
        };
        r0.b.put("action", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((BlobUploadParameters) resource).f118a;
            }

            public void set(Resource resource, String str) {
                ((BlobUploadParameters) resource).f118a = str;
            }
        });
        r0.b.put("key", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((BlobUploadParameters) resource).b;
            }

            public void set(Resource resource, String str) {
                ((BlobUploadParameters) resource).b = str;
            }
        });
        r0.b.put("AWSAccessKeyId", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((BlobUploadParameters) resource).c;
            }

            public void set(Resource resource, String str) {
                ((BlobUploadParameters) resource).c = str;
            }
        });
        r0.b.put("acl", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((BlobUploadParameters) resource).d;
            }

            public void set(Resource resource, String str) {
                ((BlobUploadParameters) resource).d = str;
            }
        });
        r0.b.put("policy", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((BlobUploadParameters) resource).e;
            }

            public void set(Resource resource, String str) {
                ((BlobUploadParameters) resource).e = str;
            }
        });
        r0.b.put("signature", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((BlobUploadParameters) resource).f;
            }

            public void set(Resource resource, String str) {
                ((BlobUploadParameters) resource).f = str;
            }
        });
        return r0;
    }
}
