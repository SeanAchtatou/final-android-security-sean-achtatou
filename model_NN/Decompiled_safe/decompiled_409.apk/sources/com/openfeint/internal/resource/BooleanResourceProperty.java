package com.openfeint.internal.resource;

import a.a.a.f;
import a.a.a.h;
import a.a.a.m;

public abstract class BooleanResourceProperty extends PrimitiveResourceProperty {
    public void copy(Resource resource, Resource resource2) {
        set(resource, get(resource2));
    }

    public void generate(Resource resource, f fVar, String str) {
        fVar.a(str);
        fVar.a(get(resource));
    }

    public abstract boolean get(Resource resource);

    public void parse(Resource resource, h hVar) {
        if (hVar.q() == m.VALUE_TRUE || hVar.m().equalsIgnoreCase("true") || hVar.m().equalsIgnoreCase("1") || hVar.m().equalsIgnoreCase("YES")) {
            set(resource, true);
        } else {
            set(resource, false);
        }
    }

    public abstract void set(Resource resource, boolean z);
}
