package com.openfeint.internal.resource;

import a.a.a.f;
import a.a.a.h;

public abstract class LongResourceProperty extends PrimitiveResourceProperty {
    public void copy(Resource resource, Resource resource2) {
        set(resource, get(resource2));
    }

    public void generate(Resource resource, f fVar, String str) {
        fVar.a(str);
        fVar.a(get(resource));
    }

    public abstract long get(Resource resource);

    public void parse(Resource resource, h hVar) {
        set(resource, hVar.e());
    }

    public abstract void set(Resource resource, long j);

    public void set(Resource resource, String str) {
        set(resource, Long.valueOf(str).longValue());
    }
}
