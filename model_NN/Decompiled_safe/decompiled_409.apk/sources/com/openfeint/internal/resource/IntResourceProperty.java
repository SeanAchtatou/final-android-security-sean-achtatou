package com.openfeint.internal.resource;

import a.a.a.f;
import a.a.a.h;

public abstract class IntResourceProperty extends PrimitiveResourceProperty {
    public void copy(Resource resource, Resource resource2) {
        set(resource, get(resource2));
    }

    public void generate(Resource resource, f fVar, String str) {
        fVar.a(str);
        fVar.a(get(resource));
    }

    public abstract int get(Resource resource);

    public void parse(Resource resource, h hVar) {
        int i;
        try {
            i = hVar.d();
        } catch (Exception e) {
            i = 0;
        }
        set(resource, i);
    }

    public abstract void set(Resource resource, int i);
}
