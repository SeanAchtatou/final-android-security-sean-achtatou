package com.openfeint.internal;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public class Encryption {

    /* renamed from: a  reason: collision with root package name */
    private static SecretKey f73a;

    public static InputStream decrypt(File file) {
        return decryptionWrap(new FileInputStream(file));
    }

    public static byte[] decrypt(byte[] bArr) {
        try {
            return Util.toByteArray(decryptionWrap(new ByteArrayInputStream(bArr)));
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] decryptFile(String str) {
        return Util.toByteArray(decrypt(new File(str)));
    }

    public static CipherInputStream decryptionWrap(InputStream inputStream) {
        try {
            byte[] bArr = new byte[10];
            if (inputStream.read(bArr) != 10) {
                throw new Exception("Couldn't read entire salt");
            }
            PBEParameterSpec pBEParameterSpec = new PBEParameterSpec(bArr, 10);
            Cipher instance = Cipher.getInstance("PBEWithSHA256And256BitAES-CBC-BC");
            instance.init(2, f73a, pBEParameterSpec);
            return new CipherInputStream(inputStream, instance);
        } catch (Exception e) {
            OpenFeintInternal.log("Encryption", e.getMessage());
            return null;
        }
    }

    public static OutputStream encrypt(String str) {
        return encryptionWrap(new FileOutputStream(new File(str)));
    }

    public static boolean encrypt(byte[] bArr, String str) {
        try {
            OutputStream encrypt = encrypt(str);
            encrypt.write(bArr);
            encrypt.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static byte[] encrypt(byte[] bArr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            CipherOutputStream encryptionWrap = encryptionWrap(byteArrayOutputStream);
            encryptionWrap.write(bArr);
            encryptionWrap.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            return null;
        }
    }

    public static CipherOutputStream encryptionWrap(OutputStream outputStream) {
        try {
            byte[] bArr = new byte[10];
            new SecureRandom().nextBytes(bArr);
            outputStream.write(bArr);
            PBEParameterSpec pBEParameterSpec = new PBEParameterSpec(bArr, 10);
            Cipher instance = Cipher.getInstance("PBEWithSHA256And256BitAES-CBC-BC");
            instance.init(1, f73a, pBEParameterSpec);
            return new CipherOutputStream(outputStream, instance);
        } catch (Exception e) {
            OpenFeintInternal.log("Encryption", e.getMessage());
            return null;
        }
    }

    public static boolean init(String str) {
        try {
            f73a = SecretKeyFactory.getInstance("PBEWithSHA256And256BitAES-CBC-BC").generateSecret(new PBEKeySpec(str.toCharArray()));
            byte[] bytes = "PBEWithSHA256And256BitAES-CBC-BC".getBytes();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            CipherOutputStream encryptionWrap = encryptionWrap(byteArrayOutputStream);
            encryptionWrap.write(bytes);
            encryptionWrap.close();
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (byteArray.length == 0) {
                throw new Exception();
            } else if (Arrays.equals(Util.toByteArray(decryptionWrap(new ByteArrayInputStream(byteArray))), bytes)) {
                return true;
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            f73a = null;
            return false;
        }
    }

    public static boolean initialized() {
        return f73a != null;
    }
}
