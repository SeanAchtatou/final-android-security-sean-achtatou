package com.openfeint.api;

import android.content.Context;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.api.resource.User;

public abstract class OpenFeintDelegate {
    public void onDashboardAppear() {
    }

    public void onDashboardDisappear() {
    }

    public boolean showCustomApprovalFlow(Context context) {
        return false;
    }

    public void userLoggedIn(CurrentUser currentUser) {
    }

    public void userLoggedOut(User user) {
    }
}
