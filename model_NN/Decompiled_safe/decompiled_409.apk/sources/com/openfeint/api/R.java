package com.openfeint.api;

public final class R {

    public final class attr {
    }

    public final class color {
    }

    public final class drawable {
    }

    public final class id {
    }

    public final class layout {
    }

    public final class menu {
    }

    public final class string {
    }

    public final class style {
    }

    public final class styleable {

        /* renamed from: a  reason: collision with root package name */
        private static int[] f61a = {hamon05.speed.pac.R.attr.testing, hamon05.speed.pac.R.attr.backgroundColor, hamon05.speed.pac.R.attr.textColor, hamon05.speed.pac.R.attr.keywords, hamon05.speed.pac.R.attr.refreshInterval, hamon05.speed.pac.R.attr.isGoneWithoutAd};
    }
}
