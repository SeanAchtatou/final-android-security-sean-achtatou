package com.openfeint.api.resource;

import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.ArrayResourceProperty;
import com.openfeint.internal.resource.BooleanResourceProperty;
import com.openfeint.internal.resource.NestedResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.ServerException;
import com.openfeint.internal.resource.StringResourceProperty;
import hamon05.speed.pac.R;
import java.util.List;

public class Leaderboard extends Resource {

    /* renamed from: a  reason: collision with root package name */
    public String f65a;
    public Score b;
    public boolean c = true;
    public boolean d;
    public List e;

    public abstract class GetScoresCB extends APICallback {
        public abstract void onSuccess(List list);
    }

    public abstract class GetUserScoreCB extends APICallback {
        public abstract void onSuccess(Score score);
    }

    public abstract class ListCB extends APICallback {
        public abstract void onSuccess(List list);
    }

    public Leaderboard() {
    }

    public Leaderboard(String str) {
        setResourceID(str);
    }

    public static ResourceClass getResourceClass() {
        AnonymousClass4 r0 = new ResourceClass(Leaderboard.class, "leaderboard") {
            public Resource factory() {
                return new Leaderboard();
            }
        };
        r0.b.put("name", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Leaderboard) resource).f65a;
            }

            public void set(Resource resource, String str) {
                ((Leaderboard) resource).f65a = str;
            }
        });
        r0.b.put("current_user_high_score", new NestedResourceProperty(Score.class) {
            public Resource get(Resource resource) {
                return ((Leaderboard) resource).b;
            }

            public void set(Resource resource, Resource resource2) {
                ((Leaderboard) resource).b = (Score) resource2;
            }
        });
        r0.b.put("descending_sort_order", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((Leaderboard) resource).c;
            }

            public void set(Resource resource, boolean z) {
                ((Leaderboard) resource).c = z;
            }
        });
        r0.b.put("allow_posting_lower_scores", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((Leaderboard) resource).d;
            }

            public void set(Resource resource, boolean z) {
                ((Leaderboard) resource).d = z;
            }
        });
        r0.b.put("high_scores", new ArrayResourceProperty(Score.class) {
            public List get(Resource resource) {
                return ((Leaderboard) resource).e;
            }

            public void set(Resource resource, List list) {
                ((Leaderboard) resource).e = list;
            }
        });
        return r0;
    }

    private void getScores(boolean z, GetScoresCB getScoresCB) {
        final String str = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/leaderboards/" + resourceID() + "/high_scores";
        OrderedArgList orderedArgList = new OrderedArgList();
        if (z) {
            orderedArgList.put("friends_leaderboard", "true");
        }
        final boolean z2 = z;
        final GetScoresCB getScoresCB2 = getScoresCB;
        new JSONRequest(orderedArgList) {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (getScoresCB2 != null) {
                    getScoresCB2.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (getScoresCB2 != null) {
                    getScoresCB2.onSuccess((List) obj);
                }
            }

            public String path() {
                return str;
            }

            public boolean wantsLogin() {
                return z2;
            }
        }.launch();
    }

    public static void list(final ListCB listCB) {
        final String str = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/leaderboards";
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (listCB != null) {
                    listCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (listCB != null) {
                    try {
                        listCB.onSuccess((List) obj);
                    } catch (Exception e) {
                        onFailure(OpenFeintInternal.getRString(R.string.of_unexpected_response_format));
                    }
                }
            }

            public String path() {
                return str;
            }
        }.launch();
    }

    public void getFriendScores(GetScoresCB getScoresCB) {
        getScores(true, getScoresCB);
    }

    public void getScores(GetScoresCB getScoresCB) {
        getScores(false, getScoresCB);
    }

    public void getUserScore(User user, final GetUserScoreCB getUserScoreCB) {
        final String str = "/xp/users/" + user.resourceID() + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/leaderboards/" + resourceID() + "/current_score";
        new JSONRequest(new OrderedArgList()) {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (getUserScoreCB != null) {
                    getUserScoreCB.onFailure(str);
                }
            }

            public void onResponse(int i, Object obj) {
                if (getUserScoreCB == null) {
                    return;
                }
                if (200 <= i && i < 300) {
                    getUserScoreCB.onSuccess((Score) obj);
                } else if (404 == i) {
                    getUserScoreCB.onSuccess(null);
                } else if (obj instanceof ServerException) {
                    onFailure(((ServerException) obj).b);
                } else {
                    onFailure(OpenFeintInternal.getRString(R.string.of_unknown_server_error));
                }
            }

            public String path() {
                return str;
            }
        }.launch();
    }
}
