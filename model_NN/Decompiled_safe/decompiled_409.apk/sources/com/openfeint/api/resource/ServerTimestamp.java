package com.openfeint.api.resource;

import com.openfeint.internal.APICallback;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.resource.DateResourceProperty;
import com.openfeint.internal.resource.LongResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import java.util.Date;

public class ServerTimestamp extends Resource {

    /* renamed from: a  reason: collision with root package name */
    public Date f68a;
    public long b;

    public abstract class GetCB extends APICallback {
        public abstract void onSuccess(ServerTimestamp serverTimestamp);
    }

    public static void get(final GetCB getCB) {
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (GetCB.this != null) {
                    GetCB.this.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (GetCB.this != null) {
                    GetCB.this.onSuccess((ServerTimestamp) obj);
                }
            }

            public String path() {
                return "/xp/server_timestamp";
            }
        }.launch();
    }

    public static ResourceClass getResourceClass() {
        AnonymousClass2 r0 = new ResourceClass(ServerTimestamp.class, "server_timestamp") {
            public Resource factory() {
                return new ServerTimestamp();
            }
        };
        r0.b.put("timestamp", new DateResourceProperty() {
            public Date get(Resource resource) {
                return ((ServerTimestamp) resource).f68a;
            }

            public void set(Resource resource, Date date) {
                ((ServerTimestamp) resource).f68a = date;
            }
        });
        r0.b.put("seconds_since_epoch", new LongResourceProperty() {
            public long get(Resource resource) {
                return ((ServerTimestamp) resource).b;
            }

            public void set(Resource resource, long j) {
                ((ServerTimestamp) resource).b = j;
            }
        });
        return r0;
    }
}
