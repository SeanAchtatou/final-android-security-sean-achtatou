package com.openfeint.api.resource;

import android.graphics.Bitmap;
import android.net.Uri;
import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.BitmapRequest;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.BooleanResourceProperty;
import com.openfeint.internal.resource.DoubleResourceProperty;
import com.openfeint.internal.resource.IntResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.StringResourceProperty;
import hamon05.speed.pac.R;
import java.util.List;

public class User extends Resource {

    /* renamed from: a  reason: collision with root package name */
    public String f69a;
    public String b;
    public String c;
    public boolean d;
    public String e;
    public String f;
    public boolean g;
    public boolean h;
    public int i;
    public boolean j;
    public double k;
    public double m;

    public abstract class DownloadProfilePictureCB extends APICallback {
        public abstract void onSuccess(Bitmap bitmap);
    }

    public abstract class FindCB extends APICallback {
        public abstract void onSuccess(User user);
    }

    public abstract class GetFriendsCB extends APICallback {
        public abstract void onSuccess(List list);
    }

    public abstract class LoadCB extends APICallback {
        public abstract void onSuccess();
    }

    public User() {
    }

    public User(String str) {
        setResourceID(str);
    }

    public static void findByID(final String str, final FindCB findCB) {
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (findCB != null) {
                    findCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (findCB != null) {
                    findCB.onSuccess((User) obj);
                }
            }

            public String path() {
                return "/xp/users/" + str;
            }
        }.launch();
    }

    public static void findByName(final String str, final FindCB findCB) {
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (findCB != null) {
                    findCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (findCB != null) {
                    findCB.onSuccess((User) obj);
                }
            }

            public String path() {
                return "/xp/users/" + Uri.encode(str);
            }
        }.launch();
    }

    public static ResourceClass getResourceClass() {
        AnonymousClass6 r0 = new ResourceClass(User.class, "user") {
            public Resource factory() {
                return new User();
            }
        };
        r0.b.put("name", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((User) resource).f69a;
            }

            public void set(Resource resource, String str) {
                ((User) resource).f69a = str;
            }
        });
        r0.b.put("profile_picture_url", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((User) resource).b;
            }

            public void set(Resource resource, String str) {
                ((User) resource).b = str;
            }
        });
        r0.b.put("profile_picture_source", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((User) resource).c;
            }

            public void set(Resource resource, String str) {
                ((User) resource).c = str;
            }
        });
        r0.b.put("uses_facebook_profile_picture", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((User) resource).d;
            }

            public void set(Resource resource, boolean z) {
                ((User) resource).d = z;
            }
        });
        r0.b.put("last_played_game_id", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((User) resource).e;
            }

            public void set(Resource resource, String str) {
                ((User) resource).e = str;
            }
        });
        r0.b.put("last_played_game_name", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((User) resource).f;
            }

            public void set(Resource resource, String str) {
                ((User) resource).f = str;
            }
        });
        r0.b.put("gamer_score", new IntResourceProperty() {
            public int get(Resource resource) {
                return ((User) resource).i;
            }

            public void set(Resource resource, int i) {
                ((User) resource).i = i;
            }
        });
        r0.b.put("following_local_user", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((User) resource).g;
            }

            public void set(Resource resource, boolean z) {
                ((User) resource).g = z;
            }
        });
        r0.b.put("followed_by_local_user", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((User) resource).h;
            }

            public void set(Resource resource, boolean z) {
                ((User) resource).h = z;
            }
        });
        r0.b.put("online", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((User) resource).j;
            }

            public void set(Resource resource, boolean z) {
                ((User) resource).j = z;
            }
        });
        r0.b.put("lat", new DoubleResourceProperty() {
            public double get(Resource resource) {
                return ((User) resource).k;
            }

            public void set(Resource resource, double d) {
                ((User) resource).k = d;
            }
        });
        r0.b.put("lng", new DoubleResourceProperty() {
            public double get(Resource resource) {
                return ((User) resource).m;
            }

            public void set(Resource resource, double d) {
                ((User) resource).m = d;
            }
        });
        return r0;
    }

    public void downloadProfilePicture(final DownloadProfilePictureCB downloadProfilePictureCB) {
        if (this.b != null) {
            new BitmapRequest() {
                public void onFailure(String str) {
                    if (downloadProfilePictureCB != null) {
                        downloadProfilePictureCB.onFailure(str);
                    }
                }

                public void onSuccess(Bitmap bitmap) {
                    if (downloadProfilePictureCB != null) {
                        downloadProfilePictureCB.onSuccess(bitmap);
                    }
                }

                public String path() {
                    return "";
                }

                public String url() {
                    return User.this.b;
                }
            }.launch();
        } else if (downloadProfilePictureCB != null) {
            downloadProfilePictureCB.onFailure(OpenFeintInternal.getRString(R.string.of_profile_url_null));
        }
    }

    public void getFriends(final GetFriendsCB getFriendsCB) {
        OrderedArgList orderedArgList = new OrderedArgList();
        orderedArgList.put("user_id", resourceID());
        new JSONRequest(orderedArgList) {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (getFriendsCB != null) {
                    getFriendsCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (getFriendsCB != null) {
                    try {
                        getFriendsCB.onSuccess((List) obj);
                    } catch (Exception e) {
                        onFailure(OpenFeintInternal.getRString(R.string.of_unexpected_response_format));
                    }
                }
            }

            public String path() {
                return "/xp/friends";
            }

            public boolean wantsLogin() {
                return true;
            }
        }.launch();
    }

    public void load(final LoadCB loadCB) {
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (loadCB != null) {
                    loadCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                User.this.shallowCopyAncestorType((User) obj);
                if (loadCB != null) {
                    loadCB.onSuccess();
                }
            }

            public String path() {
                return "/xp/users/" + User.this.resourceID();
            }
        }.launch();
    }

    public String userID() {
        return resourceID();
    }
}
