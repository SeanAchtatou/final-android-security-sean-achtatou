package com.openfeint.api.resource;

import android.graphics.Bitmap;
import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.notifications.AchievementNotification;
import com.openfeint.internal.offline.OfflineSupport;
import com.openfeint.internal.request.BitmapRequest;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.BooleanResourceProperty;
import com.openfeint.internal.resource.DateResourceProperty;
import com.openfeint.internal.resource.FloatResourceProperty;
import com.openfeint.internal.resource.IntResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.StringResourceProperty;
import hamon05.speed.pac.R;
import java.util.Date;
import java.util.List;

public class Achievement extends Resource {

    /* renamed from: a  reason: collision with root package name */
    public String f62a;
    public String b;
    public int c;
    public String d;
    public boolean e;
    public boolean f;
    public float g;
    public Date h;
    public String i;
    public String j;
    public int k;

    public abstract class DownloadIconCB extends APICallback {
        public abstract void onSuccess(Bitmap bitmap);
    }

    public abstract class ListCB extends APICallback {
        public abstract void onSuccess(List list);
    }

    public abstract class LoadCB extends APICallback {
        public abstract void onSuccess();
    }

    public abstract class UnlockCB extends APICallback {
        public abstract void onSuccess(boolean z);
    }

    public abstract class UpdateProgressionCB extends APICallback {
        public abstract void onSuccess(boolean z);
    }

    public Achievement() {
    }

    public Achievement(String str) {
        setResourceID(str);
    }

    public static ResourceClass getResourceClass() {
        AnonymousClass6 r0 = new ResourceClass(Achievement.class, "achievement") {
            public Resource factory() {
                return new Achievement();
            }
        };
        r0.b.put("title", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Achievement) resource).f62a;
            }

            public void set(Resource resource, String str) {
                ((Achievement) resource).f62a = str;
            }
        });
        r0.b.put("description", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Achievement) resource).b;
            }

            public void set(Resource resource, String str) {
                ((Achievement) resource).b = str;
            }
        });
        r0.b.put("gamerscore", new IntResourceProperty() {
            public int get(Resource resource) {
                return ((Achievement) resource).c;
            }

            public void set(Resource resource, int i) {
                ((Achievement) resource).c = i;
            }
        });
        r0.b.put("icon_url", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Achievement) resource).d;
            }

            public void set(Resource resource, String str) {
                ((Achievement) resource).d = str;
            }
        });
        r0.b.put("is_secret", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((Achievement) resource).e;
            }

            public void set(Resource resource, boolean z) {
                ((Achievement) resource).e = z;
            }
        });
        r0.b.put("is_unlocked", new BooleanResourceProperty() {
            public boolean get(Resource resource) {
                return ((Achievement) resource).f;
            }

            public void set(Resource resource, boolean z) {
                ((Achievement) resource).f = z;
            }
        });
        r0.b.put("percent_complete", new FloatResourceProperty() {
            public float get(Resource resource) {
                return ((Achievement) resource).g;
            }

            public void set(Resource resource, float f) {
                ((Achievement) resource).g = f;
            }
        });
        r0.b.put("unlocked_at", new DateResourceProperty() {
            public Date get(Resource resource) {
                return ((Achievement) resource).h;
            }

            public void set(Resource resource, Date date) {
                ((Achievement) resource).h = date;
            }
        });
        r0.b.put("position", new IntResourceProperty() {
            public int get(Resource resource) {
                return ((Achievement) resource).k;
            }

            public void set(Resource resource, int i) {
                ((Achievement) resource).k = i;
            }
        });
        r0.b.put("end_version", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Achievement) resource).i;
            }

            public void set(Resource resource, String str) {
                ((Achievement) resource).i = str;
            }
        });
        r0.b.put("start_version", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Achievement) resource).j;
            }

            public void set(Resource resource, String str) {
                ((Achievement) resource).j = str;
            }
        });
        return r0;
    }

    public static void list(final ListCB listCB) {
        final String str = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/achievements";
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (listCB != null) {
                    listCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (listCB != null) {
                    try {
                        listCB.onSuccess((List) obj);
                    } catch (Exception e) {
                        onFailure(OpenFeintInternal.getRString(R.string.of_unexpected_response_format));
                    }
                }
            }

            public String path() {
                return str;
            }
        }.launch();
    }

    public void downloadIcon(final DownloadIconCB downloadIconCB) {
        if (this.d != null) {
            new BitmapRequest() {
                public String method() {
                    return "GET";
                }

                public void onFailure(String str) {
                    if (downloadIconCB != null) {
                        downloadIconCB.onFailure(str);
                    }
                }

                public void onSuccess(Bitmap bitmap) {
                    if (downloadIconCB != null) {
                        downloadIconCB.onSuccess(bitmap);
                    }
                }

                public String path() {
                    return "";
                }

                public String url() {
                    return Achievement.this.d;
                }
            }.launch();
        } else if (downloadIconCB != null) {
            downloadIconCB.onFailure(OpenFeintInternal.getRString(R.string.of_null_icon_url));
        }
    }

    public void load(final LoadCB loadCB) {
        String resourceID = resourceID();
        if (resourceID != null) {
            final String str = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/achievements/" + resourceID;
            new JSONRequest() {
                public String method() {
                    return "GET";
                }

                public void onFailure(String str) {
                    super.onFailure(str);
                    if (loadCB != null) {
                        loadCB.onFailure(str);
                    }
                }

                public void onSuccess(Object obj) {
                    Achievement.this.shallowCopy((Achievement) obj);
                    if (loadCB != null) {
                        loadCB.onSuccess();
                    }
                }

                public String path() {
                    return str;
                }
            }.launch();
        } else if (loadCB != null) {
            loadCB.onFailure(OpenFeintInternal.getRString(R.string.of_achievement_load_null));
        }
    }

    public void unlock(final UnlockCB unlockCB) {
        AnonymousClass3 r0 = null;
        if (unlockCB != null) {
            r0 = new UpdateProgressionCB() {
                public void onFailure(String str) {
                    unlockCB.onFailure(str);
                }

                public void onSuccess(boolean z) {
                    unlockCB.onSuccess(z);
                }
            };
        }
        updateProgression(100.0f, r0);
    }

    public void updateProgression(float f2, UpdateProgressionCB updateProgressionCB) {
        final float f3 = f2 < 0.0f ? 0.0f : f2 > 100.0f ? 100.0f : f2;
        final String resourceID = resourceID();
        if (resourceID == null) {
            if (updateProgressionCB != null) {
                updateProgressionCB.onFailure(OpenFeintInternal.getRString(R.string.of_achievement_unlock_null));
            }
        } else if (f3 > OfflineSupport.getClientCompletionPercentage(resourceID)) {
            OfflineSupport.updateClientCompletionPercentage(resourceID, f3);
            if (OpenFeintInternal.getInstance().getUserID() == null) {
                this.g = OfflineSupport.getClientCompletionPercentage(resourceID);
                this.f = this.g == 100.0f;
                if (updateProgressionCB != null) {
                    updateProgressionCB.onSuccess(false);
                    return;
                }
                return;
            }
            final String str = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/achievements/" + resourceID + "/unlock";
            OrderedArgList orderedArgList = new OrderedArgList();
            orderedArgList.put("percent_complete", new Float(f3).toString());
            final UpdateProgressionCB updateProgressionCB2 = updateProgressionCB;
            new JSONRequest(orderedArgList) {
                public String method() {
                    return "PUT";
                }

                public void onFailure(String str) {
                    super.onFailure(str);
                    if (updateProgressionCB2 != null) {
                        updateProgressionCB2.onFailure(str);
                    }
                }

                /* access modifiers changed from: protected */
                public void onResponse(int i, Object obj) {
                    if (i >= 200 && i < 300) {
                        Achievement achievement = (Achievement) obj;
                        float f2 = Achievement.this.g;
                        Achievement.this.shallowCopy(achievement);
                        float f3 = Achievement.this.g;
                        OfflineSupport.updateServerCompletionPercentage(resourceID, f3);
                        if (201 == i || f3 > f2) {
                            AchievementNotification.showStatus(achievement);
                        }
                        if (updateProgressionCB2 != null) {
                            updateProgressionCB2.onSuccess(201 == i);
                        }
                    } else if (400 > i || i >= 500) {
                        if (100.0f == f3) {
                            Achievement.this.g = f3;
                            Achievement.this.f = true;
                            AchievementNotification.showStatus(Achievement.this);
                        }
                        if (updateProgressionCB2 != null) {
                            updateProgressionCB2.onSuccess(false);
                        }
                    } else {
                        onFailure(obj);
                    }
                }

                public String path() {
                    return str;
                }
            }.launch();
        } else if (updateProgressionCB != null) {
            updateProgressionCB.onSuccess(false);
        }
    }
}
