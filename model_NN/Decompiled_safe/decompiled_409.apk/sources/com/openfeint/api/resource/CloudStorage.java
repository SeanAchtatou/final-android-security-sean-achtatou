package com.openfeint.api.resource;

import a.a.a.c;
import a.a.a.h;
import a.a.a.m;
import a.a.a.n;
import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.request.CompressedBlobDownloadRequest;
import com.openfeint.internal.request.Compression;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.resource.ServerException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;

public class CloudStorage {

    /* renamed from: a  reason: collision with root package name */
    private static int f64a = 262144;
    private static Pattern b;

    public abstract class DeleteCB extends APICallback {
        public abstract void onSuccess();
    }

    public abstract class ListCB extends APICallback {
        public abstract void onSuccess(List list);
    }

    public abstract class LoadCB extends APICallback {
        public abstract void onSuccess(byte[] bArr);
    }

    public abstract class SaveCB extends APICallback {
        public abstract void onSuccess();
    }

    public static void delete(String str, final DeleteCB deleteCB) {
        String userID = OpenFeintInternal.getInstance().getUserID();
        if ((userID == null || userID.length() == 0) && deleteCB != null) {
            deleteCB.onFailure("The user who owns this CloudStorage blob is not logged in. The CloudStorage blob specified was not deleted.");
        }
        if (!isValidKey(str) && deleteCB != null) {
            deleteCB.onFailure("'" + (str == null ? "(null)" : str) + "' is not a valid CloudStorage key.");
        }
        final String str2 = "/xp/users/" + userID + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/save_cards/" + str;
        new JSONRequest() {
            public String method() {
                return "DELETE";
            }

            public void onFailure(String str) {
                if (deleteCB != null) {
                    deleteCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (deleteCB != null) {
                    deleteCB.onSuccess();
                }
            }

            public String path() {
                return str2;
            }

            public boolean wantsLogin() {
                return true;
            }
        }.launch();
    }

    public static boolean isValidKey(String str) {
        if (str == null) {
            return false;
        }
        if (b == null) {
            try {
                b = Pattern.compile("[a-zA-Z][a-zA-Z0-9-_]*");
            } catch (PatternSyntaxException e) {
                return false;
            }
        }
        return b.matcher(str).matches();
    }

    public static void list(final ListCB listCB) {
        String userID = OpenFeintInternal.getInstance().getUserID();
        if ((userID == null || userID.length() == 0) && listCB != null) {
            listCB.onFailure("A user must be logged in to list their persisted CloudStorage blobs.");
        }
        final String str = "/xp/users/" + userID + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/save_cards";
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public void onFailure(String str) {
                if (listCB != null) {
                    listCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (listCB != null) {
                    listCB.onSuccess((List) obj);
                }
            }

            /* access modifiers changed from: protected */
            public Object parseJson(byte[] bArr) {
                Object objFromJson = Util.getObjFromJson(bArr);
                if (objFromJson != null && (objFromJson instanceof ServerException)) {
                    return objFromJson;
                }
                try {
                    h a2 = new n((byte) 0).a(bArr);
                    if (a2.i() != m.START_OBJECT) {
                        throw new c("Couldn't find toplevel wrapper object.", a2.k());
                    } else if (a2.i() != m.FIELD_NAME) {
                        throw new c("Couldn't find toplevel wrapper object.", a2.k());
                    } else if (!a2.m().equals("save_cards")) {
                        throw new c("Couldn't find toplevel wrapper object.", a2.k());
                    } else if (a2.i() != m.START_ARRAY) {
                        throw new c("Couldn't find savecard array.", a2.k());
                    } else {
                        ArrayList arrayList = new ArrayList();
                        while (a2.i() != m.END_ARRAY) {
                            if (a2.q() != m.VALUE_STRING) {
                                throw new c("Unexpected non-string in savecard array.", a2.k());
                            }
                            arrayList.add(a2.m());
                        }
                        return arrayList;
                    }
                } catch (Exception e) {
                    OpenFeintInternal.log(f97a, e.getMessage());
                    return new ServerException("JSONError", "Unexpected response format");
                }
            }

            public String path() {
                return str;
            }

            public boolean wantsLogin() {
                return true;
            }
        }.launch();
    }

    public static void load(String str, final LoadCB loadCB) {
        String userID = OpenFeintInternal.getInstance().getUserID();
        if ((userID == null || userID.length() == 0) && loadCB != null) {
            loadCB.onFailure("A user must be logged in to load data from a CloudStorage blob.");
        }
        if (!isValidKey(str) && loadCB != null) {
            loadCB.onFailure("'" + (str == null ? "(null)" : str) + "' is not a valid CloudStorage key.");
        }
        final String str2 = "/xp/users/" + userID + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/save_cards/" + str;
        new CompressedBlobDownloadRequest() {
            public void onFailure(String str) {
                if (loadCB != null) {
                    loadCB.onFailure(str);
                }
            }

            public void onSuccessDecompress(byte[] bArr) {
                if (loadCB != null) {
                    loadCB.onSuccess(bArr);
                }
            }

            public String path() {
                return str2;
            }

            public boolean wantsLogin() {
                return true;
            }
        }.launch();
    }

    public static void save(String str, final byte[] bArr, final SaveCB saveCB) {
        String userID = OpenFeintInternal.getInstance().getUserID();
        if (userID == null || userID.length() == 0) {
            if (saveCB != null) {
                saveCB.onFailure("Cannot save because the owner of this CloudStorage blob is not logged in.");
            }
        } else if (!isValidKey(str)) {
            if (saveCB != null) {
                saveCB.onFailure("'" + (str == null ? "(null)" : str) + "' is not a valid CloudStorage key.");
            }
        } else if (bArr == null || bArr.length == 0) {
            if (saveCB != null) {
                saveCB.onFailure("data is empty.  data must be set before saving.");
            }
        } else if (f64a >= bArr.length) {
            final String str2 = "/xp/users/" + userID + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/save_cards/" + str;
            new JSONRequest() {
                /* access modifiers changed from: protected */
                public HttpUriRequest generateRequest() {
                    HttpPut httpPut = new HttpPut(url());
                    httpPut.setEntity(new ByteArrayEntity(Compression.compress(bArr)));
                    addParams(httpPut);
                    return httpPut;
                }

                public String method() {
                    return "PUT";
                }

                public void onFailure(String str) {
                    if (saveCB != null) {
                        saveCB.onFailure(str);
                    }
                }

                public void onSuccess(Object obj) {
                    if (saveCB != null) {
                        saveCB.onSuccess();
                    }
                }

                public String path() {
                    return str2;
                }

                public boolean wantsLogin() {
                    return true;
                }
            }.launch();
        } else if (saveCB != null) {
            saveCB.onFailure("You cannot exceed 256 kB per save card");
        }
    }
}
