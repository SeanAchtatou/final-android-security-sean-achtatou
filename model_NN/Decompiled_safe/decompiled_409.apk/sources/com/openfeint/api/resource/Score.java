package com.openfeint.api.resource;

import com.openfeint.api.Notification;
import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.notifications.SimpleNotification;
import com.openfeint.internal.offline.OfflineSupport;
import com.openfeint.internal.request.CompressedBlobDownloadRequest;
import com.openfeint.internal.request.CompressedBlobPostRequest;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.BlobUploadParameters;
import com.openfeint.internal.resource.DoubleResourceProperty;
import com.openfeint.internal.resource.IntResourceProperty;
import com.openfeint.internal.resource.LongResourceProperty;
import com.openfeint.internal.resource.NestedResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.ScoreBlobDelegate;
import com.openfeint.internal.resource.StringResourceProperty;
import hamon05.speed.pac.R;
import java.util.List;

public class Score extends Resource {

    /* renamed from: a  reason: collision with root package name */
    public User f66a;
    public long b;
    public int c;
    public int d;
    public String e;
    public String f;
    public double g;
    public double h;
    public byte[] i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public BlobUploadParameters k;

    public abstract class BlobDownloadedDelegate {
        public void blobDownloadedForScore(Score score) {
        }
    }

    public abstract class DownloadBlobCB extends APICallback {
        public abstract void onSuccess();
    }

    public abstract class SubmitToCB extends APICallback {
        public void onBlobUploadFailure(String str) {
        }

        public void onBlobUploadSuccess() {
        }

        public abstract void onSuccess(boolean z);
    }

    public Score() {
    }

    public Score(long j2) {
        this.b = j2;
    }

    public Score(long j2, String str) {
        this.b = j2;
        this.e = str;
    }

    public static ResourceClass getResourceClass() {
        AnonymousClass3 r0 = new ResourceClass(Score.class, "high_score") {
            public Resource factory() {
                return new Score();
            }
        };
        r0.b.put("score", new LongResourceProperty() {
            public long get(Resource resource) {
                return ((Score) resource).b;
            }

            public void set(Resource resource, long j) {
                ((Score) resource).b = j;
            }
        });
        r0.b.put("rank", new IntResourceProperty() {
            public int get(Resource resource) {
                return ((Score) resource).c;
            }

            public void set(Resource resource, int i) {
                ((Score) resource).c = i;
            }
        });
        r0.b.put("leaderboard_id", new IntResourceProperty() {
            public int get(Resource resource) {
                return ((Score) resource).d;
            }

            public void set(Resource resource, int i) {
                ((Score) resource).d = i;
            }
        });
        r0.b.put("display_text", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Score) resource).e;
            }

            public void set(Resource resource, String str) {
                ((Score) resource).e = str;
            }
        });
        r0.b.put("custom_data", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Score) resource).f;
            }

            public void set(Resource resource, String str) {
                ((Score) resource).f = str;
            }
        });
        r0.b.put("lat", new DoubleResourceProperty() {
            public double get(Resource resource) {
                return ((Score) resource).g;
            }

            public void set(Resource resource, double d) {
                ((Score) resource).g = d;
            }
        });
        r0.b.put("lng", new DoubleResourceProperty() {
            public double get(Resource resource) {
                return ((Score) resource).h;
            }

            public void set(Resource resource, double d) {
                ((Score) resource).h = d;
            }
        });
        r0.b.put("user", new NestedResourceProperty(User.class) {
            public Resource get(Resource resource) {
                return ((Score) resource).f66a;
            }

            public void set(Resource resource, Resource resource2) {
                ((Score) resource).f66a = (User) resource2;
            }
        });
        r0.b.put("blob_url", new StringResourceProperty() {
            public String get(Resource resource) {
                return ((Score) resource).j;
            }

            public void set(Resource resource, String str) {
                ((Score) resource).j = str;
            }
        });
        r0.b.put("blob_upload_parameters", new NestedResourceProperty(BlobUploadParameters.class) {
            public Resource get(Resource resource) {
                return ((Score) resource).k;
            }

            public void set(Resource resource, Resource resource2) {
                ((Score) resource).k = (BlobUploadParameters) resource2;
            }
        });
        return r0;
    }

    public static void setBlobDownloadedDelegate(BlobDownloadedDelegate blobDownloadedDelegate) {
        ScoreBlobDelegate.f124a = blobDownloadedDelegate;
    }

    private void submitToInternal(Leaderboard leaderboard, String str, SubmitToCB submitToCB, boolean z) {
        final boolean z2 = false;
        if (leaderboard == null || leaderboard.resourceID() == null || leaderboard.resourceID().length() == 0) {
            if (submitToCB != null) {
                submitToCB.onFailure("No leaderboard ID provided.  Please provide a leaderboard ID from the Dev Dashboard.");
            }
        } else if (!OpenFeintInternal.getInstance().isUserLoggedIn()) {
            OfflineSupport.postOfflineScore(this, leaderboard);
            if (submitToCB != null) {
                submitToCB.onSuccess(false);
            }
        } else {
            final String str2 = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/leaderboards/" + leaderboard.resourceID() + "/high_scores";
            OrderedArgList orderedArgList = new OrderedArgList();
            orderedArgList.put("high_score[score]", new Long(this.b).toString());
            if (this.e != null) {
                orderedArgList.put("high_score[display_text]", this.e);
            }
            if (this.i != null) {
                z2 = true;
            }
            orderedArgList.put("high_score[has_blob]", z2 ? "1" : "0");
            if (str != null) {
                orderedArgList.put("high_score[timestamp]", str);
            }
            final boolean z3 = z;
            final SubmitToCB submitToCB2 = submitToCB;
            final Leaderboard leaderboard2 = leaderboard;
            new JSONRequest(orderedArgList) {
                private final void perhapsUploadBlob(boolean z, Object obj) {
                    if (z && (obj instanceof List)) {
                        Score score = (Score) ((List) obj).get(0);
                        CompressedBlobPostRequest compressedBlobPostRequest = new CompressedBlobPostRequest(score.k, String.format("blob.%s.bin", score.resourceID()), Score.this.i);
                        if (submitToCB2 != null) {
                            final SubmitToCB submitToCB = submitToCB2;
                            compressedBlobPostRequest.setDelegate(new IRawRequestDelegate() {
                                public void onResponse(int i, String str) {
                                    if (200 > i || i >= 300) {
                                        submitToCB.onBlobUploadFailure(str);
                                    } else {
                                        submitToCB.onBlobUploadSuccess();
                                    }
                                }
                            });
                        }
                        compressedBlobPostRequest.launch();
                    }
                }

                public String method() {
                    return "POST";
                }

                public void onFailure(String str) {
                    super.onFailure(str);
                    if (submitToCB2 != null) {
                        submitToCB2.onFailure(str);
                    }
                }

                /* access modifiers changed from: protected */
                public void onResponse(int i, Object obj) {
                    if (201 == i) {
                        if (!z3) {
                            SimpleNotification.show(OpenFeintInternal.getInstance().getContext().getResources().getString(R.string.of_score_submitted_notification), "@drawable/of_icon_highscore_notification", Notification.Category.HighScore, Notification.Type.Success);
                        }
                        if (submitToCB2 != null) {
                            submitToCB2.onSuccess(true);
                        }
                        perhapsUploadBlob(z2, obj);
                    } else if (200 > i || i >= 300) {
                        if ((i == 0 || 500 <= i) && !z3) {
                            OfflineSupport.postOfflineScore(Score.this, leaderboard2);
                            if (submitToCB2 != null) {
                                submitToCB2.onSuccess(false);
                                return;
                            }
                            return;
                        }
                        onFailure(obj);
                    } else if (submitToCB2 != null) {
                        submitToCB2.onSuccess(false);
                    }
                }

                public String path() {
                    return str2;
                }
            }.launch();
        }
    }

    public void downloadBlob(final DownloadBlobCB downloadBlobCB) {
        if (hasBlob()) {
            new CompressedBlobDownloadRequest() {
                public void onFailure(String str) {
                    super.onFailure(str);
                    if (downloadBlobCB != null) {
                        downloadBlobCB.onFailure(str);
                    }
                }

                /* access modifiers changed from: protected */
                public void onSuccessDecompress(byte[] bArr) {
                    Score.this.i = bArr;
                    if (downloadBlobCB != null) {
                        downloadBlobCB.onSuccess();
                    }
                }

                public String path() {
                    return "";
                }

                public boolean signed() {
                    return false;
                }

                public String url() {
                    return Score.this.j;
                }
            }.launch();
        } else if (downloadBlobCB != null) {
            downloadBlobCB.onFailure(OpenFeintInternal.getRString(R.string.of_no_blob));
        }
    }

    public boolean hasBlob() {
        return this.j != null;
    }

    public void submitTo(Leaderboard leaderboard, SubmitToCB submitToCB) {
        submitToInternal(leaderboard, null, submitToCB, false);
    }

    public void submitToFromOffline(Leaderboard leaderboard, String str, SubmitToCB submitToCB) {
        submitToInternal(leaderboard, str, submitToCB, true);
    }
}
