package com.openfeint.api.resource;

import com.openfeint.internal.APICallback;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;

public class CurrentUser extends User {

    public abstract class BefriendCB extends APICallback {
        public abstract void onSuccess();
    }

    public static ResourceClass getResourceClass() {
        return new ResourceClass(CurrentUser.class, "current_user") {
            public Resource factory() {
                return new CurrentUser();
            }
        };
    }

    public void befriend(User user, final BefriendCB befriendCB) {
        OrderedArgList orderedArgList = new OrderedArgList();
        orderedArgList.put("friend_id", user.resourceID());
        new JSONRequest(orderedArgList) {
            public String method() {
                return "POST";
            }

            public void onFailure(String str) {
                super.onFailure(str);
                if (befriendCB != null) {
                    befriendCB.onFailure(str);
                }
            }

            public void onSuccess(Object obj) {
                if (befriendCB != null) {
                    befriendCB.onSuccess();
                }
            }

            public String path() {
                return "/xp/friend_requests";
            }

            public boolean wantsLogin() {
                return true;
            }
        }.launch();
    }
}
