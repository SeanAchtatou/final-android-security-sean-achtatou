package com.openfeint.api;

import android.content.Context;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.offline.OfflineSupport;

public class OpenFeint {
    public static CurrentUser getCurrentUser() {
        return OpenFeintInternal.getInstance().getCurrentUser();
    }

    public static void initialize(Context context, OpenFeintSettings openFeintSettings, OpenFeintDelegate openFeintDelegate) {
        OpenFeintInternal.initialize(context, openFeintSettings, openFeintDelegate);
    }

    public static void initializeWithoutLoggingIn(Context context, OpenFeintSettings openFeintSettings, OpenFeintDelegate openFeintDelegate) {
        OpenFeintInternal.initializeWithoutLoggingIn(context, openFeintSettings, openFeintDelegate);
    }

    public static boolean isNetworkConnected() {
        return OpenFeintInternal.getInstance().isFeintServerReachable();
    }

    public static boolean isUserLoggedIn() {
        return OpenFeintInternal.getInstance().isUserLoggedIn();
    }

    public static void login() {
        OpenFeintInternal.getInstance().login(false);
    }

    public static void logoutUser() {
        OpenFeintInternal.getInstance().logoutUser(null);
    }

    public static void setDelegate(OpenFeintDelegate openFeintDelegate) {
        OpenFeintInternal.getInstance().setDelegate(openFeintDelegate);
    }

    public static void trySubmitOfflineData() {
        OfflineSupport.trySubmitOfflineData();
    }

    public static void userApprovedFeint() {
        OpenFeintInternal.getInstance().userApprovedFeint();
    }

    public static void userDeclinedFeint() {
        OpenFeintInternal.getInstance().userDeclinedFeint();
    }
}
