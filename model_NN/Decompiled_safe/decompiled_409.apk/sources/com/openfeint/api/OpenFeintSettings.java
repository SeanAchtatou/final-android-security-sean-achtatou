package com.openfeint.api;

import com.openfeint.internal.OpenFeintInternal;
import hamon05.speed.pac.R;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class OpenFeintSettings {

    /* renamed from: a  reason: collision with root package name */
    public String f60a;
    public String b;
    public String c;
    public String d;
    public Map e;

    public OpenFeintSettings(String str, String str2, String str3, String str4) {
        this.f60a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = new HashMap();
    }

    public OpenFeintSettings(String str, String str2, String str3, String str4, Map map) {
        this.f60a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = map;
    }

    public void applyOverrides(Properties properties) {
        if (properties != null) {
            String property = properties.getProperty("app-id");
            if (property != null) {
                this.d = property;
            }
            String property2 = properties.getProperty("game-name");
            if (property2 != null) {
                this.f60a = property2;
            }
            String property3 = properties.getProperty("app-key");
            if (property3 != null) {
                this.b = property3;
            }
            String property4 = properties.getProperty("app-secret");
            if (property4 != null) {
                this.c = property4;
            }
        }
    }

    public void verify() {
        String str = null;
        if (this.b == null) {
            str = OpenFeintInternal.getRString(R.string.of_key_cannot_be_null);
        } else if (this.c == null) {
            str = OpenFeintInternal.getRString(R.string.of_secret_cannot_be_null);
        } else if (this.d == null) {
            str = OpenFeintInternal.getRString(R.string.of_id_cannot_be_null);
        } else if (this.f60a == null) {
            str = OpenFeintInternal.getRString(R.string.of_name_cannot_be_null);
        }
        if (str != null) {
            OpenFeintInternal.log("OpenFeintSettings", str);
            OpenFeintInternal.getInstance().displayErrorDialog(str);
        }
    }
}
