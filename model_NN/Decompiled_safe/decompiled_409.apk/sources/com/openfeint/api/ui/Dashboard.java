package com.openfeint.api.ui;

import android.content.Intent;
import android.content.res.Resources;
import android.view.Menu;
import android.view.MenuItem;
import com.openfeint.api.Notification;
import com.openfeint.api.OpenFeint;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.notifications.TwoLineNotification;
import com.openfeint.internal.ui.Settings;
import com.openfeint.internal.ui.WebNav;
import hamon05.speed.pac.R;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Dashboard extends WebNav {
    private static List g = new ArrayList();

    /* renamed from: a  reason: collision with root package name */
    private boolean f70a = true;

    class DashboardActionHandler extends WebNav.ActionHandler {
        public DashboardActionHandler(WebNav webNav) {
            super(webNav);
        }

        public final void openSettings(Map map) {
            Settings.open();
        }

        /* access modifiers changed from: protected */
        public void populateActionList(List list) {
            super.populateActionList(list);
            list.add("openSettings");
        }
    }

    public static void close() {
        for (Dashboard finish : g) {
            finish.finish();
        }
    }

    public static void open() {
        open(null, false);
    }

    private static void open(String str, boolean z) {
        OpenFeint.trySubmitOfflineData();
        OpenFeintInternal instance = OpenFeintInternal.getInstance();
        if (!instance.isFeintServerReachable()) {
            Resources resources = OpenFeintInternal.getInstance().getContext().getResources();
            TwoLineNotification.show(resources.getString(R.string.of_offline_notification), resources.getString(R.string.of_offline_notification_line2), Notification.Category.Foreground, Notification.Type.NetworkOffline);
            return;
        }
        instance.getAnalytics().markDashboardOpen();
        Intent intent = new Intent(instance.getContext(), Dashboard.class);
        if (str != null) {
            intent.putExtra("screenName", str);
        }
        instance.submitIntent(intent, z);
    }

    public static void openAchievements() {
        open("achievements", false);
    }

    public static void openFromSpotlight() {
        open("user.json?spotlight=true", true);
    }

    public static void openGameDetail(String str) {
        open("game?game_id=" + str, false);
    }

    public static void openLeaderboard(String str) {
        open("leaderboard?leaderboard_id=" + str, false);
    }

    public static void openLeaderboards() {
        open("leaderboards", false);
    }

    /* access modifiers changed from: protected */
    public WebNav.ActionHandler createActionHandler(WebNav webNav) {
        return new DashboardActionHandler(webNav);
    }

    /* access modifiers changed from: protected */
    public String initialContentPath() {
        String stringExtra = getIntent().getStringExtra("screenName");
        if (stringExtra == null) {
            return "dashboard/user";
        }
        this.f70a = false;
        return "dashboard/" + stringExtra;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.of_dashboard, menu);
        return true;
    }

    public void onDestroy() {
        super.onDestroy();
        g.remove(this);
        OpenFeintInternal.getInstance().getAnalytics().markDashboardClose();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str = null;
        if (menuItem.getItemId() == R.id.home) {
            str = "home";
            this.f70a = true;
        } else if (menuItem.getItemId() == R.id.settings) {
            str = "settings";
        } else if (menuItem.getItemId() == R.id.exit_feint) {
            str = "exit";
        }
        if (str == null) {
            return super.onOptionsItemSelected(menuItem);
        }
        executeJavascript(String.format("OF.menu('%s')", str));
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.home).setVisible(!this.f70a || this.d > 1);
        return true;
    }

    public void onResume() {
        super.onResume();
        if (!g.contains(this)) {
            g.add(this);
        }
        if (OpenFeintInternal.getInstance().getCurrentUser() == null) {
            finish();
        }
    }
}
