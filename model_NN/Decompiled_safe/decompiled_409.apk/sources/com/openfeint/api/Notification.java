package com.openfeint.api;

import java.util.Map;

public abstract class Notification {

    /* renamed from: a  reason: collision with root package name */
    private static Delegate f57a = new Delegate();

    public enum Category {
        Foreground,
        Login,
        Challenge,
        HighScore,
        Leaderboard,
        Achievement,
        SocialNotification,
        Presence,
        Multiplayer
    }

    public class Delegate {
        public boolean canShowNotification(Notification notification) {
            return true;
        }

        public void displayNotification(Notification notification) {
        }

        public void notificationWillShow(Notification notification) {
        }
    }

    public enum Type {
        None,
        Submitting,
        Downloading,
        Error,
        Success,
        NewResources,
        UserPresenceOnLine,
        UserPresenceOffline,
        NewMessage,
        Multiplayer,
        NetworkOffline
    }

    public static Delegate getDelegate() {
        return f57a;
    }

    public static void setDelegate(Delegate delegate) {
        f57a = delegate;
    }

    public abstract Category getCategory();

    public abstract String getText();

    public abstract Type getType();

    public abstract Map getUserData();
}
