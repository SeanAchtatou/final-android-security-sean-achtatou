package com.google.api.client.escape;

public final class CharEscapers {

    /* renamed from: a  reason: collision with root package name */
    private static final Escaper f54a = new PercentEscaper("-_.*", true);
    private static final Escaper b = new PercentEscaper("-_.!~*'()@:$&,;=", false);
    private static final Escaper c = new PercentEscaper("-_.!~*'()@:$,;/?:", false);

    private CharEscapers() {
    }
}
