package com.google.api.client.escape;

final class Platform {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadLocal f56a = new ThreadLocal() {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object initialValue() {
            return new char[1024];
        }
    };

    private Platform() {
    }

    static char[] a() {
        return (char[]) f56a.get();
    }
}
