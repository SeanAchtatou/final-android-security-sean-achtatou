package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdRequest {
    private static String h = AdUtil.a("emulator");

    /* renamed from: a  reason: collision with root package name */
    private Gender f45a = null;
    private String b = null;
    private Set c = null;
    private Map d = null;
    private Location e = null;
    private boolean f = false;
    private boolean g = false;
    private Set i = null;

    public enum ErrorCode {
        INVALID_REQUEST("Invalid Google Ad request."),
        NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
        NETWORK_ERROR("A network error occurred."),
        INTERNAL_ERROR("There was an internal error.");
        
        private String e;

        private ErrorCode(String str) {
            this.e = str;
        }

        public final String toString() {
            return this.e;
        }
    }

    public enum Gender {
        MALE("m"),
        FEMALE("f");
        
        private String c;

        private Gender(String str) {
            this.c = str;
        }

        public final String toString() {
            return this.c;
        }
    }

    public final Map a(Context context) {
        String a2;
        HashMap hashMap = new HashMap();
        if (this.c != null) {
            hashMap.put("kw", this.c);
        }
        if (this.f45a != null) {
            hashMap.put("cust_gender", this.f45a.toString());
        }
        if (this.b != null) {
            hashMap.put("cust_age", this.b);
        }
        if (this.e != null) {
            hashMap.put("uule", AdUtil.a(this.e));
        }
        if (this.f) {
            hashMap.put("testing", 1);
        }
        if ((this.i == null || (a2 = AdUtil.a(context)) == null || !this.i.contains(a2)) ? false : true) {
            hashMap.put("adtest", "on");
        } else if (!this.g) {
            a.c("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.c() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.g = true;
        }
        if (this.d != null) {
            hashMap.put("extras", this.d);
        }
        return hashMap;
    }

    public final void a(String str) {
        if (this.c == null) {
            this.c = new HashSet();
        }
        this.c.add(str);
    }
}
