package a.a.a;

import java.io.IOException;

public class k extends IOException {

    /* renamed from: a  reason: collision with root package name */
    private g f33a;

    protected k(String str, g gVar) {
        this(str, gVar, null);
    }

    protected k(String str, g gVar, Throwable th) {
        super(str);
        if (th != null) {
            initCause(th);
        }
        this.f33a = gVar;
    }

    public String getMessage() {
        String message = super.getMessage();
        if (message == null) {
            message = "N/A";
        }
        g gVar = this.f33a;
        if (gVar == null) {
            return message;
        }
        return message + 10 + " at " + gVar.toString();
    }

    public String toString() {
        return String.valueOf(getClass().getName()) + ": " + getMessage();
    }
}
