package a.a.a.a;

import java.math.BigDecimal;
import java.util.ArrayList;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f6a = new char[0];
    private final e b;
    private char[] c;
    private int d;
    private int e;
    private ArrayList f;
    private boolean g = false;
    private int h;
    private char[] i;
    private int j;
    private String k;
    private char[] l;

    public f(e eVar) {
        this.b = eVar;
    }

    private final char[] b(int i2) {
        return this.b.a(g.TEXT_BUFFER, i2);
    }

    private void c(int i2) {
        int i3 = this.e;
        this.e = 0;
        char[] cArr = this.c;
        this.c = null;
        int i4 = this.d;
        this.d = -1;
        int i5 = i3 + i2;
        if (this.i == null || i5 > this.i.length) {
            this.i = b(i5);
        }
        if (i3 > 0) {
            System.arraycopy(cArr, i4, this.i, 0, i3);
        }
        this.h = 0;
        this.j = i3;
    }

    private void d(int i2) {
        if (this.f == null) {
            this.f = new ArrayList();
        }
        char[] cArr = this.i;
        this.g = true;
        this.f.add(cArr);
        this.h += cArr.length;
        int length = cArr.length;
        int i3 = length >> 1;
        if (i3 < i2) {
            i3 = i2;
        }
        this.j = 0;
        this.i = new char[Math.min(262144, length + i3)];
    }

    private void m() {
        this.c = null;
        this.d = -1;
        this.e = 0;
        this.k = null;
        this.l = null;
        if (this.g) {
            n();
        }
        this.j = 0;
    }

    private final void n() {
        this.g = false;
        this.f.clear();
        this.h = 0;
        this.j = 0;
    }

    private char[] o() {
        int i2;
        char[] cArr = this.l;
        if (cArr == null) {
            if (this.k != null) {
                cArr = this.k.toCharArray();
            } else if (this.d < 0) {
                int b2 = b();
                if (b2 <= 0) {
                    cArr = f6a;
                } else {
                    char[] cArr2 = new char[b2];
                    if (this.f != null) {
                        int size = this.f.size();
                        int i3 = 0;
                        int i4 = 0;
                        while (i3 < size) {
                            char[] cArr3 = (char[]) this.f.get(i3);
                            int length = cArr3.length;
                            System.arraycopy(cArr3, 0, cArr2, i4, length);
                            i3++;
                            i4 += length;
                        }
                        i2 = i4;
                    } else {
                        i2 = 0;
                    }
                    System.arraycopy(this.i, 0, cArr2, i2, this.j);
                    cArr = cArr2;
                }
            } else if (this.e <= 0) {
                cArr = f6a;
            } else {
                cArr = new char[this.e];
                System.arraycopy(this.c, this.d, cArr, 0, this.e);
            }
            this.l = cArr;
        }
        return cArr;
    }

    public final void a() {
        if (this.b != null && this.i != null) {
            m();
            char[] cArr = this.i;
            this.i = null;
            this.b.a(g.TEXT_BUFFER, cArr);
        }
    }

    public final void a(int i2) {
        this.j = i2;
    }

    public final void a(char[] cArr, int i2, int i3) {
        this.k = null;
        this.l = null;
        this.c = cArr;
        this.d = i2;
        this.e = i3;
        if (this.g) {
            n();
        }
    }

    public final int b() {
        return this.d >= 0 ? this.e : this.h + this.j;
    }

    public final void b(char[] cArr, int i2, int i3) {
        int i4;
        int i5;
        this.c = null;
        this.d = -1;
        this.e = 0;
        this.k = null;
        this.l = null;
        if (this.g) {
            n();
        } else if (this.i == null) {
            this.i = b(i3);
        }
        this.h = 0;
        this.j = 0;
        if (this.d >= 0) {
            c(i3);
        }
        this.k = null;
        this.l = null;
        char[] cArr2 = this.i;
        int length = cArr2.length - this.j;
        if (length >= i3) {
            System.arraycopy(cArr, i2, cArr2, this.j, i3);
            this.j += i3;
            return;
        }
        if (length > 0) {
            System.arraycopy(cArr, i2, cArr2, this.j, length);
            int i6 = i3 - length;
            i5 = i2 + length;
            i4 = i6;
        } else {
            i4 = i3;
            i5 = i2;
        }
        d(i4);
        System.arraycopy(cArr, i5, this.i, 0, i4);
        this.j = i4;
    }

    public final int c() {
        if (this.d >= 0) {
            return this.d;
        }
        return 0;
    }

    public final char[] d() {
        return this.d >= 0 ? this.c : !this.g ? this.i : o();
    }

    public final String e() {
        if (this.k == null) {
            if (this.l != null) {
                this.k = new String(this.l);
            } else if (this.d < 0) {
                int i2 = this.h;
                int i3 = this.j;
                if (i2 == 0) {
                    this.k = i3 == 0 ? "" : new String(this.i, 0, i3);
                } else {
                    StringBuilder sb = new StringBuilder(i2 + i3);
                    if (this.f != null) {
                        int size = this.f.size();
                        for (int i4 = 0; i4 < size; i4++) {
                            char[] cArr = (char[]) this.f.get(i4);
                            sb.append(cArr, 0, cArr.length);
                        }
                    }
                    sb.append(this.i, 0, this.j);
                    this.k = sb.toString();
                }
            } else if (this.e <= 0) {
                this.k = "";
                return "";
            } else {
                this.k = new String(this.c, this.d, this.e);
            }
        }
        return this.k;
    }

    public final BigDecimal f() {
        return this.l != null ? new BigDecimal(this.l) : this.d >= 0 ? new BigDecimal(this.c, this.d, this.e) : this.h == 0 ? new BigDecimal(this.i, 0, this.j) : new BigDecimal(o());
    }

    public final double g() {
        return Double.parseDouble(e());
    }

    public final char[] h() {
        if (this.d >= 0) {
            c(1);
        } else {
            char[] cArr = this.i;
            if (cArr == null) {
                this.i = b(0);
            } else if (this.j >= cArr.length) {
                d(1);
            }
        }
        return this.i;
    }

    public final char[] i() {
        m();
        char[] cArr = this.i;
        if (cArr != null) {
            return cArr;
        }
        char[] b2 = b(0);
        this.i = b2;
        return b2;
    }

    public final int j() {
        return this.j;
    }

    public final char[] k() {
        if (this.f == null) {
            this.f = new ArrayList();
        }
        this.g = true;
        this.f.add(this.i);
        int length = this.i.length;
        this.h += length;
        char[] cArr = new char[Math.min(length + (length >> 1), 262144)];
        this.j = 0;
        this.i = cArr;
        return cArr;
    }

    public final char[] l() {
        char[] cArr = this.i;
        int length = cArr.length;
        this.i = new char[(length == 262144 ? 262145 : Math.min(262144, (length >> 1) + length))];
        System.arraycopy(cArr, 0, this.i, 0, length);
        return this.i;
    }

    public final String toString() {
        return e();
    }
}
