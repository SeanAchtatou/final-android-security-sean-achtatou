package a.a.a.a;

import java.util.LinkedHashMap;
import java.util.Map;

public final class c extends LinkedHashMap {

    /* renamed from: a  reason: collision with root package name */
    public static final c f3a = new c();

    private c() {
        super(192, 0.8f, true);
    }

    public final synchronized String a(String str) {
        String str2;
        str2 = (String) get(str);
        if (str2 == null) {
            str2 = str.intern();
            put(str2, str2);
        }
        return str2;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > 192;
    }
}
