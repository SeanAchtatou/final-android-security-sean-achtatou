package a.a.a.a;

public enum d {
    READ_IO_BUFFER(4000),
    WRITE_ENCODING_BUFFER(4000),
    WRITE_CONCAT_BUFFER(2000);
    
    private final int d;

    private d(int i) {
        this.d = i;
    }
}
