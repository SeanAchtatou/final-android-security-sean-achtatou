package a.a.a.b;

import a.a.a.c.a;
import java.io.Reader;

public abstract class g extends e {
    public g(a aVar, int i, Reader reader) {
        super(aVar, i, reader);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0077, code lost:
        r6 = r5;
        r5 = r7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x021b  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0223  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0270  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x01ec A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0199 A[ADDED_TO_REGION, EDGE_INSN: B:180:0x0199->B:108:0x0199 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011b  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x015a  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0168  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x016f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final a.a.a.m c(int r12) {
        /*
            r11 = this;
            r0 = 45
            if (r12 != r0) goto L_0x0053
            r0 = 1
        L_0x0005:
            int r1 = r11.c
            r2 = 1
            int r2 = r1 - r2
            int r3 = r11.d
            if (r0 == 0) goto L_0x0026
            int r4 = r11.d
            if (r1 >= r4) goto L_0x00cc
            char[] r4 = r11.f12a
            int r5 = r1 + 1
            char r1 = r4[r1]
            r4 = 57
            if (r1 > r4) goto L_0x0020
            r4 = 48
            if (r1 >= r4) goto L_0x0025
        L_0x0020:
            java.lang.String r4 = "expected digit (0-9) to follow minus sign, for valid numeric value"
            r11.a(r1, r4)
        L_0x0025:
            r1 = r5
        L_0x0026:
            r4 = 1
            r10 = r4
            r4 = r1
            r1 = r10
        L_0x002a:
            int r5 = r11.d
            if (r4 >= r5) goto L_0x00cc
            char[] r5 = r11.f12a
            int r6 = r4 + 1
            char r4 = r5[r4]
            r5 = 48
            if (r4 < r5) goto L_0x0055
            r5 = 57
            if (r4 > r5) goto L_0x0055
            int r1 = r1 + 1
            r4 = 2
            if (r1 != r4) goto L_0x0287
            char[] r4 = r11.f12a
            r5 = 2
            int r5 = r6 - r5
            char r4 = r4[r5]
            r5 = 48
            if (r4 != r5) goto L_0x0287
            java.lang.String r4 = "Leading zeroes not allowed"
            r11.a(r4)
            r4 = r6
            goto L_0x002a
        L_0x0053:
            r0 = 0
            goto L_0x0005
        L_0x0055:
            r5 = 0
            r7 = 46
            if (r4 != r7) goto L_0x0281
            r4 = r5
            r5 = r6
        L_0x005c:
            if (r5 >= r3) goto L_0x00cc
            char[] r6 = r11.f12a
            int r7 = r5 + 1
            char r5 = r6[r5]
            r6 = 48
            if (r5 < r6) goto L_0x0070
            r6 = 57
            if (r5 > r6) goto L_0x0070
            int r4 = r4 + 1
            r5 = r7
            goto L_0x005c
        L_0x0070:
            if (r4 != 0) goto L_0x0077
            java.lang.String r6 = "Decimal point not followed by a digit"
            r11.a(r5, r6)
        L_0x0077:
            r6 = r5
            r5 = r7
        L_0x0079:
            r7 = 0
            r8 = 101(0x65, float:1.42E-43)
            if (r6 == r8) goto L_0x0082
            r8 = 69
            if (r6 != r8) goto L_0x027e
        L_0x0082:
            if (r5 >= r3) goto L_0x00cc
            char[] r6 = r11.f12a
            int r8 = r5 + 1
            char r5 = r6[r5]
            r6 = 45
            if (r5 == r6) goto L_0x0092
            r6 = 43
            if (r5 != r6) goto L_0x0278
        L_0x0092:
            if (r8 >= r3) goto L_0x00cc
            char[] r5 = r11.f12a
            int r6 = r8 + 1
            char r5 = r5[r8]
            r10 = r7
            r7 = r5
            r5 = r10
        L_0x009d:
            r8 = 57
            if (r7 > r8) goto L_0x00a5
            r8 = 48
            if (r7 >= r8) goto L_0x00bf
        L_0x00a5:
            if (r5 != 0) goto L_0x00ac
            java.lang.String r3 = "Exponent indicator not followed by a digit"
            r11.a(r7, r3)
        L_0x00ac:
            r3 = r5
            r5 = r6
        L_0x00ae:
            int r5 = r5 + -1
            r11.c = r5
            int r5 = r5 - r2
            a.a.a.a.f r6 = r11.n
            char[] r7 = r11.f12a
            r6.a(r7, r2, r5)
            a.a.a.m r0 = r11.a(r0, r1, r4, r3)
        L_0x00be:
            return r0
        L_0x00bf:
            int r5 = r5 + 1
            if (r6 >= r3) goto L_0x00cc
            char[] r7 = r11.f12a
            int r8 = r6 + 1
            char r6 = r7[r6]
            r7 = r6
            r6 = r8
            goto L_0x009d
        L_0x00cc:
            if (r0 == 0) goto L_0x01b7
            int r1 = r2 + 1
        L_0x00d0:
            r11.c = r1
            a.a.a.a.f r1 = r11.n
            char[] r1 = r1.i()
            r2 = 0
            if (r0 == 0) goto L_0x00e2
            r3 = 0
            int r2 = r2 + 1
            r4 = 45
            r1[r3] = r4
        L_0x00e2:
            r3 = 0
            r4 = 0
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x00e7:
            int r5 = r11.c
            int r6 = r11.d
            if (r5 < r6) goto L_0x01ba
            boolean r5 = r11.a()
            if (r5 != 0) goto L_0x01ba
            r4 = 0
            r5 = 1
            r10 = r5
            r5 = r4
            r4 = r10
        L_0x00f8:
            if (r1 != 0) goto L_0x0116
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "Missing integer part (next char "
            r6.<init>(r7)
            java.lang.String r7 = b(r5)
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = ")"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r11.a(r6)
        L_0x0116:
            r6 = 0
            r7 = 46
            if (r5 != r7) goto L_0x0270
            int r7 = r2 + 1
            r3[r2] = r5
            r2 = r6
            r6 = r3
            r3 = r5
            r5 = r7
        L_0x0123:
            int r7 = r11.c
            int r8 = r11.d
            if (r7 < r8) goto L_0x01f3
            boolean r7 = r11.a()
            if (r7 != 0) goto L_0x01f3
            r4 = 1
            r10 = r4
            r4 = r3
            r3 = r10
        L_0x0133:
            if (r2 != 0) goto L_0x013a
            java.lang.String r7 = "Decimal point not followed by a digit"
            r11.a(r4, r7)
        L_0x013a:
            r7 = 0
            r8 = 101(0x65, float:1.42E-43)
            if (r4 == r8) goto L_0x0143
            r8 = 69
            if (r4 != r8) goto L_0x0267
        L_0x0143:
            int r8 = r6.length
            if (r5 < r8) goto L_0x0150
            a.a.a.a.f r5 = r11.n
            char[] r5 = r5.k()
            r6 = 0
            r10 = r6
            r6 = r5
            r5 = r10
        L_0x0150:
            int r8 = r5 + 1
            r6[r5] = r4
            int r4 = r11.c
            int r5 = r11.d
            if (r4 >= r5) goto L_0x021b
            char[] r4 = r11.f12a
            int r5 = r11.c
            int r9 = r5 + 1
            r11.c = r9
            char r4 = r4[r5]
        L_0x0164:
            r5 = 45
            if (r4 == r5) goto L_0x016c
            r5 = 43
            if (r4 != r5) goto L_0x0261
        L_0x016c:
            int r5 = r6.length
            if (r8 < r5) goto L_0x025e
            a.a.a.a.f r5 = r11.n
            char[] r5 = r5.k()
            r6 = 0
            r10 = r6
            r6 = r5
            r5 = r10
        L_0x0179:
            int r8 = r5 + 1
            r6[r5] = r4
            int r4 = r11.c
            int r5 = r11.d
            if (r4 >= r5) goto L_0x0223
            char[] r4 = r11.f12a
            int r5 = r11.c
            int r9 = r5 + 1
            r11.c = r9
            char r4 = r4[r5]
        L_0x018d:
            r5 = r4
            r4 = r7
            r7 = r6
            r6 = r8
        L_0x0191:
            r8 = 57
            if (r5 > r8) goto L_0x0199
            r8 = 48
            if (r5 >= r8) goto L_0x022b
        L_0x0199:
            r10 = r4
            r4 = r3
            r3 = r10
        L_0x019c:
            if (r3 != 0) goto L_0x01a3
            java.lang.String r7 = "Exponent indicator not followed by a digit"
            r11.a(r5, r7)
        L_0x01a3:
            r5 = r6
        L_0x01a4:
            if (r4 != 0) goto L_0x01ac
            int r4 = r11.c
            r6 = 1
            int r4 = r4 - r6
            r11.c = r4
        L_0x01ac:
            a.a.a.a.f r4 = r11.n
            r4.a(r5)
            a.a.a.m r0 = r11.a(r0, r1, r2, r3)
            goto L_0x00be
        L_0x01b7:
            r1 = r2
            goto L_0x00d0
        L_0x01ba:
            char[] r5 = r11.f12a
            int r6 = r11.c
            int r7 = r6 + 1
            r11.c = r7
            char r5 = r5[r6]
            r6 = 48
            if (r5 < r6) goto L_0x00f8
            r6 = 57
            if (r5 > r6) goto L_0x00f8
            int r1 = r1 + 1
            r6 = 2
            if (r1 != r6) goto L_0x01df
            r6 = 1
            int r6 = r2 - r6
            char r6 = r3[r6]
            r7 = 48
            if (r6 != r7) goto L_0x01df
            java.lang.String r6 = "Leading zeroes not allowed"
            r11.a(r6)
        L_0x01df:
            int r6 = r3.length
            if (r2 < r6) goto L_0x01ec
            a.a.a.a.f r2 = r11.n
            char[] r2 = r2.k()
            r3 = 0
            r10 = r3
            r3 = r2
            r2 = r10
        L_0x01ec:
            int r6 = r2 + 1
            r3[r2] = r5
            r2 = r6
            goto L_0x00e7
        L_0x01f3:
            char[] r3 = r11.f12a
            int r7 = r11.c
            int r8 = r7 + 1
            r11.c = r8
            char r3 = r3[r7]
            r7 = 48
            if (r3 < r7) goto L_0x026b
            r7 = 57
            if (r3 > r7) goto L_0x026b
            int r2 = r2 + 1
            int r7 = r6.length
            if (r5 < r7) goto L_0x0214
            a.a.a.a.f r5 = r11.n
            char[] r5 = r5.k()
            r6 = 0
            r10 = r6
            r6 = r5
            r5 = r10
        L_0x0214:
            int r7 = r5 + 1
            r6[r5] = r3
            r5 = r7
            goto L_0x0123
        L_0x021b:
            java.lang.String r4 = "expected a digit for number exponent"
            char r4 = r11.e(r4)
            goto L_0x0164
        L_0x0223:
            java.lang.String r4 = "expected a digit for number exponent"
            char r4 = r11.e(r4)
            goto L_0x018d
        L_0x022b:
            int r4 = r4 + 1
            int r8 = r7.length
            if (r6 < r8) goto L_0x023a
            a.a.a.a.f r6 = r11.n
            char[] r6 = r6.k()
            r7 = 0
            r10 = r7
            r7 = r6
            r6 = r10
        L_0x023a:
            int r8 = r6 + 1
            r7[r6] = r5
            int r6 = r11.c
            int r9 = r11.d
            if (r6 < r9) goto L_0x0251
            boolean r6 = r11.a()
            if (r6 != 0) goto L_0x0251
            r3 = 1
            r6 = r8
            r10 = r3
            r3 = r4
            r4 = r10
            goto L_0x019c
        L_0x0251:
            char[] r5 = r11.f12a
            int r6 = r11.c
            int r9 = r6 + 1
            r11.c = r9
            char r5 = r5[r6]
            r6 = r8
            goto L_0x0191
        L_0x025e:
            r5 = r8
            goto L_0x0179
        L_0x0261:
            r5 = r4
            r4 = r7
            r7 = r6
            r6 = r8
            goto L_0x0191
        L_0x0267:
            r4 = r3
            r3 = r7
            goto L_0x01a4
        L_0x026b:
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x0133
        L_0x0270:
            r10 = r6
            r6 = r3
            r3 = r4
            r4 = r5
            r5 = r2
            r2 = r10
            goto L_0x013a
        L_0x0278:
            r6 = r8
            r10 = r7
            r7 = r5
            r5 = r10
            goto L_0x009d
        L_0x027e:
            r3 = r7
            goto L_0x00ae
        L_0x0281:
            r10 = r5
            r5 = r6
            r6 = r4
            r4 = r10
            goto L_0x0079
        L_0x0287:
            r4 = r6
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.g.c(int):a.a.a.m");
    }
}
