package a.a.a.b;

import a.a.a.a.f;
import a.a.a.c.a;
import a.a.a.e;
import a.a.a.g;
import a.a.a.h;
import a.a.a.m;

public abstract class c extends h {
    private static /* synthetic */ int[] u;

    /* renamed from: a  reason: collision with root package name */
    private boolean f11a;
    protected final a b;
    protected int c = 0;
    protected int d = 0;
    protected long e = 0;
    protected int f = 1;
    protected int g = 0;
    protected long h = 0;
    protected int i = 1;
    protected int j = 0;
    protected o k;
    protected m l;
    protected boolean m = false;
    protected final f n;
    protected boolean o = false;
    protected byte[] p;
    private char[] s = null;
    private a.a.a.a.a t = null;

    protected c(a aVar, int i2) {
        this.b = aVar;
        this.q = i2;
        this.n = aVar.d();
        this.k = new o(null, 0, this.i, this.j);
    }

    protected static final String b(int i2) {
        char c2 = (char) i2;
        return Character.isISOControl(c2) ? "(CTRL-CHAR, code " + i2 + ")" : i2 > 255 ? "'" + c2 + "' (code " + i2 + " / 0x" + Integer.toHexString(i2) + ")" : "'" + c2 + "' (code " + i2 + ")";
    }

    protected static void p() {
        throw new RuntimeException("Internal error: this code path should never get executed");
    }

    private static /* synthetic */ int[] r() {
        int[] iArr = u;
        if (iArr == null) {
            iArr = new int[m.values().length];
            try {
                iArr[m.END_ARRAY.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[m.END_OBJECT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[m.FIELD_NAME.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[m.NOT_AVAILABLE.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[m.START_ARRAY.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[m.START_OBJECT.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[m.VALUE_EMBEDDED_OBJECT.ordinal()] = 7;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[m.VALUE_FALSE.ordinal()] = 12;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[m.VALUE_NULL.ordinal()] = 13;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[m.VALUE_NUMBER_FLOAT.ordinal()] = 10;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[m.VALUE_NUMBER_INT.ordinal()] = 9;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[m.VALUE_STRING.ordinal()] = 8;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[m.VALUE_TRUE.ordinal()] = 11;
            } catch (NoSuchFieldError e14) {
            }
            u = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        throw d("Illegal character (" + b((char) i2) + "): only regular white space (\\r, \\n, \\t) is allowed between tokens");
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, char c2) {
        throw d("Unexpected close marker '" + ((char) i2) + "': expected '" + c2 + "' (for " + this.k.e() + " starting at " + new StringBuilder().append(this.k.a(this.b.a())).toString() + ")");
    }

    /* access modifiers changed from: protected */
    public abstract boolean a();

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public final void b(int i2, String str) {
        String str2 = "Unexpected character (" + b(i2) + ")";
        if (str != null) {
            str2 = String.valueOf(str2) + ": " + str;
        }
        throw d(str2);
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        throw d("Unexpected end-of-input" + str);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.n.a();
        char[] cArr = this.s;
        if (cArr != null) {
            this.s = null;
            this.b.c(cArr);
        }
    }

    /* access modifiers changed from: protected */
    public final void c(int i2, String str) {
        if (!a(e.ALLOW_UNQUOTED_CONTROL_CHARS) || i2 >= 32) {
            throw d("Illegal unquoted character (" + b((char) i2) + "): has to be escaped using backslash to be included in " + str);
        }
    }

    /* access modifiers changed from: protected */
    public final void c(String str) {
        throw d(str);
    }

    public void close() {
        this.f11a = true;
        try {
            b();
        } finally {
            c();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void h();

    public abstract m i();

    public final h j() {
        if (this.r == m.START_OBJECT || this.r == m.START_ARRAY) {
            int i2 = 1;
            while (true) {
                m i3 = i();
                if (i3 != null) {
                    switch (r()[i3.ordinal()]) {
                        case 2:
                        case 4:
                            i2++;
                            break;
                        case 3:
                        case 5:
                            i2--;
                            if (i2 != 0) {
                                break;
                            } else {
                                return this;
                            }
                    }
                } else {
                    o();
                    return this;
                }
            }
        } else {
            return this;
        }
    }

    public final g k() {
        return new g(this.b.a(), this.h, this.i, this.j + 1, (byte) 0);
    }

    public final g l() {
        return new g(this.b.a(), (this.e + ((long) this.c)) - 1, this.f, (this.c - this.g) + 1, (byte) 0);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final String m() {
        if (this.r == null) {
            return null;
        }
        switch (r()[this.r.ordinal()]) {
            case 6:
                return this.k.a();
            case 7:
            default:
                return this.r.a();
            case 8:
                if (this.m) {
                    this.m = false;
                    h();
                    break;
                }
                break;
            case 9:
            case 10:
                break;
        }
        return this.n.e();
    }

    /* access modifiers changed from: protected */
    public final void n() {
        if (!a()) {
            b(" in " + this.r);
        }
    }

    /* access modifiers changed from: protected */
    public final void o() {
        if (!this.k.c()) {
            b(": expected close marker for " + this.k.e() + " (from " + this.k.a(this.b.a()) + ")");
        }
    }
}
