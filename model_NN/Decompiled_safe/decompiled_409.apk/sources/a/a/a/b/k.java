package a.a.a.b;

import a.a.a.a;
import a.a.a.d;
import a.a.a.f;
import a.a.a.j;

public abstract class k extends f {
    protected boolean b;
    protected h c = new l();
    private d d;
    private int e;
    private boolean f;

    protected k(int i, d dVar) {
        this.e = i;
        this.d = dVar;
        this.b = a(j.WRITE_NUMBERS_AS_STRINGS);
    }

    protected static void d(String str) {
        throw new a(str);
    }

    public final void a() {
        c("start an array");
        this.c = this.c.g();
        if (this.f30a == null) {
            f();
        }
    }

    public final void a(String str) {
        int a2 = this.c.a(str);
        if (a2 == 4) {
            d("Can not write a field name, expecting a value");
        }
        a(str, a2 == 1);
    }

    /* access modifiers changed from: protected */
    public abstract void a(String str, boolean z);

    public final boolean a(j jVar) {
        return (this.e & jVar.b()) != 0;
    }

    public final void b() {
        if (!this.c.b()) {
            d("Current context not an ARRAY but " + this.c.e());
        }
        if (this.f30a == null) {
            g();
        }
        this.c = this.c.i();
    }

    public final void c() {
        c("start an object");
        this.c = this.c.h();
        if (this.f30a == null) {
            h();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void c(String str);

    public void close() {
        this.f = true;
    }

    public final void d() {
        if (!this.c.d()) {
            d("Current context not an object but " + this.c.e());
        }
        this.c = this.c.i();
        if (this.f30a == null) {
            i();
        }
    }

    public final h e() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public abstract void f();

    /* access modifiers changed from: protected */
    public abstract void g();

    /* access modifiers changed from: protected */
    public abstract void h();

    /* access modifiers changed from: protected */
    public abstract void i();
}
