package a.a.a.b;

import a.a.a.c;
import a.a.a.c.a;
import a.a.a.c.e;
import a.a.a.m;
import java.math.BigDecimal;
import java.math.BigInteger;

public abstract class b extends c {

    /* renamed from: a  reason: collision with root package name */
    private static BigDecimal f10a = new BigDecimal(Long.MIN_VALUE);
    private static BigDecimal s = new BigDecimal(Long.MAX_VALUE);
    private static BigDecimal t = new BigDecimal(Long.MIN_VALUE);
    private static BigDecimal u = new BigDecimal(Long.MAX_VALUE);
    private BigDecimal A;
    private boolean B;
    private int C;
    private int D;
    private int E;
    private int v = 0;
    private int w;
    private long x;
    private double y;
    private BigInteger z;

    protected b(a aVar, int i) {
        super(aVar, i);
    }

    private void c(int i) {
        if (this.r == null || !this.r.c()) {
            c("Current token (" + this.r + ") not numeric, can not use numeric value accessors");
        }
        try {
            if (this.r == m.VALUE_NUMBER_INT) {
                char[] d = this.n.d();
                int c = this.n.c();
                int i2 = this.C;
                if (this.B) {
                    c++;
                }
                if (i2 <= 9) {
                    int a2 = e.a(d, c, i2);
                    if (this.B) {
                        a2 = -a2;
                    }
                    this.w = a2;
                    this.v = 1;
                } else if (i2 <= 18) {
                    long b = e.b(d, c, i2);
                    if (this.B) {
                        b = -b;
                    }
                    if (i2 == 10) {
                        if (this.B) {
                            if (b >= -2147483648L) {
                                this.w = (int) b;
                                this.v = 1;
                                return;
                            }
                        } else if (b <= 2147483647L) {
                            this.w = (int) b;
                            this.v = 1;
                            return;
                        }
                    }
                    this.x = b;
                    this.v = 2;
                } else {
                    String e = this.n.e();
                    if (e.a(d, c, i2, this.B)) {
                        this.x = Long.parseLong(e);
                        this.v = 2;
                        return;
                    }
                    this.z = new BigInteger(e);
                    this.v = 4;
                }
            } else if (i == 16) {
                this.A = this.n.f();
                this.v = 16;
            } else {
                this.y = this.n.g();
                this.v = 8;
            }
        } catch (NumberFormatException e2) {
            throw new c("Malformed numeric value '" + this.n.e() + "'", l(), e2);
        }
    }

    private void r() {
        c("Numeric value (" + m() + ") out of range of int (" + Integer.MIN_VALUE + " - " + Integer.MAX_VALUE + ")");
    }

    private void s() {
        c("Numeric value (" + m() + ") out of range of long (" + Long.MIN_VALUE + " - " + Long.MAX_VALUE + ")");
    }

    /* access modifiers changed from: protected */
    public final m a(boolean z2, int i, int i2, int i3) {
        this.B = z2;
        this.C = i;
        this.D = i2;
        this.E = i3;
        this.v = 0;
        return (i2 > 0 || i3 > 0) ? m.VALUE_NUMBER_FLOAT : m.VALUE_NUMBER_INT;
    }

    /* access modifiers changed from: protected */
    public final void a(int i, String str) {
        String str2 = "Unexpected character (" + b(i) + ") in numeric value";
        if (str != null) {
            str2 = String.valueOf(str2) + ": " + str;
        }
        c(str2);
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        c("Invalid numeric value: " + str);
    }

    public final int d() {
        if ((this.v & 1) == 0) {
            if (this.v == 0) {
                c(1);
            }
            if ((this.v & 1) == 0) {
                if ((this.v & 2) != 0) {
                    int i = (int) this.x;
                    if (((long) i) != this.x) {
                        c("Numeric value (" + m() + ") out of range of int");
                    }
                    this.w = i;
                } else if ((this.v & 4) != 0) {
                    this.w = this.z.intValue();
                } else if ((this.v & 8) != 0) {
                    if (this.y < -2.147483648E9d || this.y > 2.147483647E9d) {
                        r();
                    }
                    this.w = (int) this.y;
                } else if ((this.v & 16) != 0) {
                    if (t.compareTo(this.A) > 0 || u.compareTo(this.A) < 0) {
                        r();
                    }
                    this.w = this.A.intValue();
                } else {
                    p();
                }
                this.v |= 1;
            }
        }
        return this.w;
    }

    public final long e() {
        if ((this.v & 2) == 0) {
            if (this.v == 0) {
                c(2);
            }
            if ((this.v & 2) == 0) {
                if ((this.v & 1) != 0) {
                    this.x = (long) this.w;
                } else if ((this.v & 4) != 0) {
                    this.x = this.z.longValue();
                } else if ((this.v & 8) != 0) {
                    if (this.y < -9.223372036854776E18d || this.y > 9.223372036854776E18d) {
                        s();
                    }
                    this.x = (long) this.y;
                } else if ((this.v & 16) != 0) {
                    if (f10a.compareTo(this.A) > 0 || s.compareTo(this.A) < 0) {
                        s();
                    }
                    this.x = this.A.longValue();
                } else {
                    p();
                }
                this.v |= 2;
            }
        }
        return this.x;
    }

    public final float f() {
        return (float) g();
    }

    public final double g() {
        if ((this.v & 8) == 0) {
            if (this.v == 0) {
                c(8);
            }
            if ((this.v & 8) == 0) {
                if ((this.v & 16) != 0) {
                    this.y = this.A.doubleValue();
                } else if ((this.v & 4) != 0) {
                    this.y = this.z.doubleValue();
                } else if ((this.v & 2) != 0) {
                    this.y = (double) this.x;
                } else if ((this.v & 1) != 0) {
                    this.y = (double) this.w;
                } else {
                    p();
                }
                this.v |= 8;
            }
        }
        return this.y;
    }
}
