package a.a.a.b;

import a.a.a.e;
import java.io.IOException;
import java.io.InputStream;

public abstract class a extends b {

    /* renamed from: a  reason: collision with root package name */
    protected byte[] f9a;
    private InputStream s;
    private boolean t;

    protected a(a.a.a.c.a aVar, int i, InputStream inputStream, byte[] bArr, int i2, int i3, boolean z) {
        super(aVar, i);
        this.s = inputStream;
        this.f9a = bArr;
        this.c = i2;
        this.d = i3;
        this.t = z;
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        this.e += (long) this.d;
        this.g -= this.d;
        if (this.s != null) {
            int read = this.s.read(this.f9a, 0, this.f9a.length);
            if (read > 0) {
                this.c = 0;
                this.d = read;
                return true;
            }
            b();
            if (read == 0) {
                throw new IOException("Reader returned 0 characters when trying to read " + this.d);
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.s != null) {
            if (this.b.c() || a(e.AUTO_CLOSE_SOURCE)) {
                this.s.close();
            }
            this.s = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        byte[] bArr;
        super.c();
        if (this.t && (bArr = this.f9a) != null) {
            this.f9a = null;
            this.b.a(bArr);
        }
    }
}
