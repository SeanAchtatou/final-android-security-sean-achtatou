package a.a.a.b;

import a.a.a.a.b;
import a.a.a.c.a;
import a.a.a.d;
import a.a.a.d.f;
import a.a.a.d.h;
import a.a.a.e;
import a.a.a.m;
import java.io.InputStream;

public final class p extends j {
    private d s;
    private h t;
    private int[] u = new int[32];

    public p(a aVar, int i, InputStream inputStream, d dVar, h hVar, byte[] bArr, int i2, int i3, boolean z) {
        super(aVar, i, inputStream, bArr, i2, i3, z);
        this.s = dVar;
        this.t = hVar;
        if (!e.CANONICALIZE_FIELD_NAMES.a(i)) {
            p();
        }
    }

    private final f a(int i, int i2) {
        f a2 = this.t.a(i);
        if (a2 != null) {
            return a2;
        }
        this.u[0] = i;
        return a(this.u, 1, i2);
    }

    private final f a(int i, int i2, int i3) {
        return a(this.u, 0, i, i2, i3);
    }

    private final f a(int i, int i2, int i3, int i4) {
        this.u[0] = i;
        return a(this.u, 1, i2, i3, i4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x011e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final a.a.a.d.f a(int[] r12, int r13, int r14) {
        /*
            r11 = this;
            int r0 = r13 << 2
            r1 = 4
            int r0 = r0 - r1
            int r0 = r0 + r14
            r1 = 4
            if (r14 >= r1) goto L_0x003a
            r1 = 1
            int r1 = r13 - r1
            r1 = r12[r1]
            r2 = 1
            int r2 = r13 - r2
            r3 = 4
            int r3 = r3 - r14
            int r3 = r3 << 3
            int r3 = r1 << r3
            r12[r2] = r3
        L_0x0018:
            a.a.a.a.f r2 = r11.n
            char[] r2 = r2.i()
            r3 = 0
            r4 = 0
            r10 = r4
            r4 = r2
            r2 = r10
        L_0x0023:
            if (r2 < r0) goto L_0x003c
            java.lang.String r0 = new java.lang.String
            r2 = 0
            r0.<init>(r4, r2, r3)
            r2 = 4
            if (r14 >= r2) goto L_0x0033
            r2 = 1
            int r2 = r13 - r2
            r12[r2] = r1
        L_0x0033:
            a.a.a.d.h r1 = r11.t
            a.a.a.d.f r0 = r1.a(r0, r12, r13)
            return r0
        L_0x003a:
            r1 = 0
            goto L_0x0018
        L_0x003c:
            int r5 = r2 >> 2
            r5 = r12[r5]
            r6 = r2 & 3
            r7 = 3
            int r6 = r7 - r6
            int r6 = r6 << 3
            int r5 = r5 >> r6
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r2 = r2 + 1
            r6 = 127(0x7f, float:1.78E-43)
            if (r5 <= r6) goto L_0x0126
            r6 = r5 & 224(0xe0, float:3.14E-43)
            r7 = 192(0xc0, float:2.69E-43)
            if (r6 != r7) goto L_0x00fb
            r5 = r5 & 31
            r6 = 1
            r10 = r6
            r6 = r5
            r5 = r10
        L_0x005c:
            int r7 = r2 + r5
            if (r7 <= r0) goto L_0x0065
            java.lang.String r7 = " in field name"
            r11.b(r7)
        L_0x0065:
            int r7 = r2 >> 2
            r7 = r12[r7]
            r8 = r2 & 3
            r9 = 3
            int r8 = r9 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r2 = r2 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x007c
            r11.n(r7)
        L_0x007c:
            int r6 = r6 << 6
            r7 = r7 & 63
            r6 = r6 | r7
            r7 = 1
            if (r5 <= r7) goto L_0x0122
            int r7 = r2 >> 2
            r7 = r12[r7]
            r8 = r2 & 3
            r9 = 3
            int r8 = r9 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r2 = r2 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x009b
            r11.n(r7)
        L_0x009b:
            int r6 = r6 << 6
            r7 = r7 & 63
            r6 = r6 | r7
            r7 = 2
            if (r5 <= r7) goto L_0x0122
            int r7 = r2 >> 2
            r7 = r12[r7]
            r8 = r2 & 3
            r9 = 3
            int r8 = r9 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r2 = r2 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x00bc
            r8 = r7 & 255(0xff, float:3.57E-43)
            r11.n(r8)
        L_0x00bc:
            int r6 = r6 << 6
            r7 = r7 & 63
            r6 = r6 | r7
            r10 = r6
            r6 = r2
            r2 = r10
        L_0x00c4:
            r7 = 2
            if (r5 <= r7) goto L_0x011e
            r5 = 65536(0x10000, float:9.18355E-41)
            int r2 = r2 - r5
            int r5 = r4.length
            if (r3 < r5) goto L_0x00d3
            a.a.a.a.f r4 = r11.n
            char[] r4 = r4.l()
        L_0x00d3:
            int r5 = r3 + 1
            r7 = 55296(0xd800, float:7.7486E-41)
            int r8 = r2 >> 10
            int r7 = r7 + r8
            char r7 = (char) r7
            r4[r3] = r7
            r3 = 56320(0xdc00, float:7.8921E-41)
            r2 = r2 & 1023(0x3ff, float:1.434E-42)
            r2 = r2 | r3
            r3 = r6
            r10 = r5
            r5 = r4
            r4 = r10
        L_0x00e8:
            int r6 = r5.length
            if (r4 < r6) goto L_0x00f1
            a.a.a.a.f r5 = r11.n
            char[] r5 = r5.l()
        L_0x00f1:
            int r6 = r4 + 1
            char r2 = (char) r2
            r5[r4] = r2
            r2 = r3
            r4 = r5
            r3 = r6
            goto L_0x0023
        L_0x00fb:
            r6 = r5 & 240(0xf0, float:3.36E-43)
            r7 = 224(0xe0, float:3.14E-43)
            if (r6 != r7) goto L_0x0109
            r5 = r5 & 15
            r6 = 2
            r10 = r6
            r6 = r5
            r5 = r10
            goto L_0x005c
        L_0x0109:
            r6 = r5 & 248(0xf8, float:3.48E-43)
            r7 = 240(0xf0, float:3.36E-43)
            if (r6 != r7) goto L_0x0117
            r5 = r5 & 7
            r6 = 3
            r10 = r6
            r6 = r5
            r5 = r10
            goto L_0x005c
        L_0x0117:
            r11.m(r5)
            r5 = 1
            r6 = r5
            goto L_0x005c
        L_0x011e:
            r5 = r4
            r4 = r3
            r3 = r6
            goto L_0x00e8
        L_0x0122:
            r10 = r6
            r6 = r2
            r2 = r10
            goto L_0x00c4
        L_0x0126:
            r10 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r10
            goto L_0x00e8
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.p.a(int[], int, int):a.a.a.d.f");
    }

    private final f a(int[] iArr, int i, int i2, int i3) {
        int[] iArr2;
        if (i >= iArr.length) {
            iArr2 = a(iArr, iArr.length);
            this.u = iArr2;
        } else {
            iArr2 = iArr;
        }
        int i4 = i + 1;
        iArr2[i] = i2;
        f a2 = this.t.a(iArr2, i4);
        return a2 == null ? a(iArr2, i4, i3) : a2;
    }

    private f a(int[] iArr, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int[] iArr2;
        int[] iArr3;
        int i7;
        int i8;
        int[] iArr4;
        int[] iArr5;
        int[] iArr6;
        int[] iArr7;
        int[] a2 = b.a();
        int i9 = i4;
        byte b = i3;
        int i10 = i2;
        int i11 = i;
        int[] iArr8 = iArr;
        while (true) {
            if (a2[b] != 0) {
                if (b == 34) {
                    break;
                }
                if (b != 92) {
                    c(b, "name");
                } else {
                    b = t();
                }
                if (b > 127) {
                    if (i6 >= 4) {
                        if (i11 >= iArr8.length) {
                            iArr7 = a(iArr8, iArr8.length);
                            this.u = iArr7;
                        } else {
                            iArr7 = iArr8;
                        }
                        iArr7[i11] = i5;
                        i5 = 0;
                        i11++;
                        iArr8 = iArr7;
                        i6 = 0;
                    }
                    if (b < 2048) {
                        i5 = (i5 << 8) | (b >> 6) | 192;
                        i6++;
                    } else {
                        int i12 = (i5 << 8) | (b >> 12) | 224;
                        int i13 = i6 + 1;
                        if (i13 >= 4) {
                            if (i11 >= iArr8.length) {
                                iArr6 = a(iArr8, iArr8.length);
                                this.u = iArr6;
                            } else {
                                iArr6 = iArr8;
                            }
                            iArr6[i11] = i12;
                            i12 = 0;
                            i11++;
                            iArr8 = iArr6;
                            i13 = 0;
                        }
                        i5 = (i12 << 8) | ((b >> 6) & 63) | 128;
                        i6 = i13 + 1;
                    }
                    b = (b & 63) | 128;
                }
            }
            if (i6 < 4) {
                i9 = i6 + 1;
                b |= i5 << 8;
                i7 = i11;
                iArr3 = iArr8;
            } else {
                if (i11 >= iArr8.length) {
                    iArr2 = a(iArr8, iArr8.length);
                    this.u = iArr2;
                } else {
                    iArr2 = iArr8;
                }
                int i14 = i11 + 1;
                iArr2[i11] = i5;
                iArr3 = iArr2;
                i9 = 1;
                i7 = i14;
            }
            if (this.c >= this.d && !a()) {
                b(" in field name");
            }
            byte[] bArr = this.f9a;
            int i15 = this.c;
            this.c = i15 + 1;
            byte b2 = bArr[i15] & 255;
            iArr8 = iArr3;
            i11 = i7;
            i10 = b;
            b = b2;
        }
        if (i6 > 0) {
            if (i11 >= iArr8.length) {
                iArr5 = a(iArr8, iArr8.length);
                this.u = iArr5;
            } else {
                iArr5 = iArr8;
            }
            iArr5[i11] = i5;
            iArr4 = iArr5;
            i8 = i11 + 1;
        } else {
            i8 = i11;
            iArr4 = iArr8;
        }
        f a3 = this.t.a(iArr4, i8);
        return a3 == null ? a(iArr4, i8, i6) : a3;
    }

    private void a(m mVar) {
        byte[] b = mVar.b();
        int length = b.length;
        for (int i = 1; i < length; i++) {
            if (this.c >= this.d) {
                n();
            }
            if (b[i] != this.f9a[this.c]) {
                StringBuilder sb = new StringBuilder(mVar.a().substring(0, i));
                while (true) {
                    if (this.c >= this.d && !a()) {
                        break;
                    }
                    byte[] bArr = this.f9a;
                    int i2 = this.c;
                    this.c = i2 + 1;
                    char g = (char) g(bArr[i2]);
                    if (!Character.isJavaIdentifierPart(g)) {
                        break;
                    }
                    this.c++;
                    sb.append(g);
                }
                c("Unrecognized token '" + sb.toString() + "': was expecting 'null', 'true' or 'false'");
            }
            this.c++;
        }
    }

    private static int[] a(int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        int length = iArr.length;
        int[] iArr2 = new int[(length + i)];
        System.arraycopy(iArr, 0, iArr2, 0, length);
        return iArr2;
    }

    private final f b(int i, int i2, int i3) {
        f a2 = this.t.a(i, i2);
        if (a2 != null) {
            return a2;
        }
        this.u[0] = i;
        this.u[1] = i2;
        return a(this.u, 2, i3);
    }

    private void b(int i, int i2) {
        this.c = i2;
        n(i);
    }

    private f d(int i) {
        int[] a2 = b.a();
        int i2 = 2;
        byte b = i;
        while (this.d - this.c >= 4) {
            byte[] bArr = this.f9a;
            int i3 = this.c;
            this.c = i3 + 1;
            byte b2 = bArr[i3] & 255;
            if (a2[b2] != 0) {
                return b2 == 34 ? a(this.u, i2, b, 1) : a(this.u, i2, b, b2, 1);
            }
            byte b3 = (b << 8) | b2;
            byte[] bArr2 = this.f9a;
            int i4 = this.c;
            this.c = i4 + 1;
            byte b4 = bArr2[i4] & 255;
            if (a2[b4] != 0) {
                return b4 == 34 ? a(this.u, i2, b3, 2) : a(this.u, i2, b3, b4, 2);
            }
            byte b5 = (b3 << 8) | b4;
            byte[] bArr3 = this.f9a;
            int i5 = this.c;
            this.c = i5 + 1;
            byte b6 = bArr3[i5] & 255;
            if (a2[b6] != 0) {
                return b6 == 34 ? a(this.u, i2, b5, 3) : a(this.u, i2, b5, b6, 3);
            }
            int i6 = (b5 << 8) | b6;
            byte[] bArr4 = this.f9a;
            int i7 = this.c;
            this.c = i7 + 1;
            b = bArr4[i7] & 255;
            if (a2[b] != 0) {
                return b == 34 ? a(this.u, i2, i6, 4) : a(this.u, i2, i6, b, 4);
            }
            if (i2 >= this.u.length) {
                this.u = a(this.u, i2);
            }
            this.u[i2] = i6;
            i2++;
        }
        return a(this.u, i2, 0, b, 0);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private f e(int i) {
        f fVar;
        int[] iArr;
        int i2;
        int[] iArr2;
        int[] iArr3;
        int i3;
        int i4;
        int i5;
        int[] iArr4;
        int[] iArr5;
        char c;
        int[] iArr6;
        int[] iArr7;
        if (i != 39 || !a(e.ALLOW_SINGLE_QUOTES)) {
            if (!a(e.ALLOW_UNQUOTED_FIELD_NAMES)) {
                b(i, "was expecting double-quote to start field name");
            }
            int[] d = b.d();
            if (d[i] != 0) {
                b(i, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
            }
            int i6 = 0;
            int i7 = 0;
            int[] iArr8 = this.u;
            byte b = i;
            int i8 = 0;
            while (true) {
                if (i8 < 4) {
                    i8++;
                    i6 = (i6 << 8) | b;
                } else {
                    if (i7 >= iArr8.length) {
                        iArr = a(iArr8, iArr8.length);
                        this.u = iArr;
                    } else {
                        iArr = iArr8;
                    }
                    iArr[i7] = i6;
                    i6 = b;
                    i7++;
                    iArr8 = iArr;
                    i8 = 1;
                }
                if (this.c >= this.d && !a()) {
                    b(" in field name");
                }
                b = this.f9a[this.c] & 255;
                if (d[b] != 0) {
                    break;
                }
                this.c++;
            }
            if (i8 > 0) {
                if (i7 >= iArr8.length) {
                    iArr3 = a(iArr8, iArr8.length);
                    this.u = iArr3;
                } else {
                    iArr3 = iArr8;
                }
                iArr3[i7] = i6;
                iArr2 = iArr3;
                i2 = i7 + 1;
            } else {
                i2 = i7;
                iArr2 = iArr8;
            }
            fVar = this.t.a(iArr2, i2);
            if (fVar == null) {
                return a(iArr2, i2, i8);
            }
        } else {
            if (this.c >= this.d && !a()) {
                b(": was expecting closing ''' for name");
            }
            byte[] bArr = this.f9a;
            int i9 = this.c;
            this.c = i9 + 1;
            char c2 = bArr[i9] & 255;
            if (c2 == '\'') {
                return h.c();
            }
            int[] iArr9 = this.u;
            int[] a2 = b.a();
            int i10 = 0;
            int[] iArr10 = iArr9;
            char c3 = c2;
            int i11 = 0;
            int i12 = 0;
            while (c3 != '\'') {
                if (!(c3 == '\"' || a2[c3] == 0)) {
                    if (c3 != '\\') {
                        c(c3, "name");
                    } else {
                        c3 = t();
                    }
                    if (c3 > 127) {
                        if (i4 >= 4) {
                            if (i10 >= iArr10.length) {
                                iArr7 = a(iArr10, iArr10.length);
                                this.u = iArr7;
                            } else {
                                iArr7 = iArr10;
                            }
                            iArr7[i10] = i3;
                            i3 = 0;
                            i10++;
                            iArr10 = iArr7;
                            i4 = 0;
                        }
                        if (c3 < 2048) {
                            i3 = (i3 << 8) | (c3 >> 6) | 192;
                            i4++;
                        } else {
                            int i13 = (i3 << 8) | (c3 >> 12) | 224;
                            int i14 = i4 + 1;
                            if (i14 >= 4) {
                                if (i10 >= iArr10.length) {
                                    iArr6 = a(iArr10, iArr10.length);
                                    this.u = iArr6;
                                } else {
                                    iArr6 = iArr10;
                                }
                                iArr6[i10] = i13;
                                i13 = 0;
                                i10++;
                                iArr10 = iArr6;
                                i14 = 0;
                            }
                            i3 = (i13 << 8) | ((c3 >> 6) & 63) | 128;
                            i4 = i14 + 1;
                        }
                        c3 = (c3 & '?') | 128;
                    }
                }
                if (i4 < 4) {
                    i12 = i4 + 1;
                    c = (i3 << 8) | c3;
                } else {
                    if (i10 >= iArr10.length) {
                        iArr5 = a(iArr10, iArr10.length);
                        this.u = iArr5;
                    } else {
                        iArr5 = iArr10;
                    }
                    iArr5[i10] = i3;
                    c = c3;
                    i10++;
                    iArr10 = iArr5;
                    i12 = 1;
                }
                if (this.c >= this.d && !a()) {
                    b(" in field name");
                }
                byte[] bArr2 = this.f9a;
                int i15 = this.c;
                this.c = i15 + 1;
                c3 = bArr2[i15] & 255;
                i11 = c;
            }
            if (i4 > 0) {
                if (i10 >= iArr10.length) {
                    iArr4 = a(iArr10, iArr10.length);
                    this.u = iArr4;
                } else {
                    iArr4 = iArr10;
                }
                iArr4[i10] = i3;
                i5 = i10 + 1;
            } else {
                i5 = i10;
                iArr4 = iArr10;
            }
            fVar = this.t.a(iArr4, i5);
            if (fVar == null) {
                return a(iArr4, i5, i4);
            }
        }
        return fVar;
    }

    /* JADX WARN: Type inference failed for: r3v13, types: [int] */
    /* JADX WARN: Type inference failed for: r3v15, types: [int] */
    /* JADX WARN: Type inference failed for: r3v17, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private a.a.a.m f(int r12) {
        /*
            r11 = this;
            r9 = 39
            r8 = 0
            if (r12 != r9) goto L_0x000d
            a.a.a.e r0 = a.a.a.e.ALLOW_SINGLE_QUOTES
            boolean r0 = r11.a(r0)
            if (r0 != 0) goto L_0x0012
        L_0x000d:
            java.lang.String r0 = "expected a valid value (number, String, array, object, 'true', 'false' or 'null')"
            r11.b(r12, r0)
        L_0x0012:
            a.a.a.a.f r0 = r11.n
            char[] r0 = r0.i()
            int[] r1 = a.a.a.a.b.b()
            byte[] r2 = r11.f9a
            r3 = r8
        L_0x001f:
            int r4 = r11.c
            int r5 = r11.d
            if (r4 < r5) goto L_0x0028
            r11.n()
        L_0x0028:
            int r4 = r0.length
            if (r3 < r4) goto L_0x0032
            a.a.a.a.f r0 = r11.n
            char[] r0 = r0.k()
            r3 = r8
        L_0x0032:
            int r4 = r11.d
            int r5 = r11.c
            int r6 = r0.length
            int r6 = r6 - r3
            int r5 = r5 + r6
            if (r5 >= r4) goto L_0x00d9
            r4 = r3
            r3 = r5
        L_0x003d:
            int r5 = r11.c
            if (r5 < r3) goto L_0x0043
            r3 = r4
            goto L_0x001f
        L_0x0043:
            int r5 = r11.c
            int r6 = r5 + 1
            r11.c = r6
            byte r5 = r2[r5]
            r5 = r5 & 255(0xff, float:3.57E-43)
            if (r5 == r9) goto L_0x005a
            r6 = r1[r5]
            if (r6 != 0) goto L_0x005a
            int r6 = r4 + 1
            char r5 = (char) r5
            r0[r4] = r5
            r4 = r6
            goto L_0x003d
        L_0x005a:
            if (r5 == r9) goto L_0x00cf
            r3 = r1[r5]
            switch(r3) {
                case 1: goto L_0x0080;
                case 2: goto L_0x008c;
                case 3: goto L_0x0094;
                case 4: goto L_0x00ac;
                default: goto L_0x0061;
            }
        L_0x0061:
            r3 = 32
            if (r5 >= r3) goto L_0x006a
            java.lang.String r3 = "string value"
            r11.c(r5, r3)
        L_0x006a:
            r11.l(r5)
        L_0x006d:
            r3 = r4
            r4 = r5
        L_0x006f:
            int r5 = r0.length
            if (r3 < r5) goto L_0x0079
            a.a.a.a.f r0 = r11.n
            char[] r0 = r0.k()
            r3 = r8
        L_0x0079:
            int r5 = r3 + 1
            char r4 = (char) r4
            r0[r3] = r4
            r3 = r5
            goto L_0x001f
        L_0x0080:
            r3 = 34
            if (r5 == r3) goto L_0x006d
            char r3 = r11.t()
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x006f
        L_0x008c:
            int r3 = r11.h(r5)
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x006f
        L_0x0094:
            int r3 = r11.d
            int r6 = r11.c
            int r3 = r3 - r6
            r6 = 2
            if (r3 < r6) goto L_0x00a4
            int r3 = r11.j(r5)
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x006f
        L_0x00a4:
            int r3 = r11.i(r5)
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x006f
        L_0x00ac:
            int r3 = r11.k(r5)
            int r5 = r4 + 1
            r6 = 55296(0xd800, float:7.7486E-41)
            int r7 = r3 >> 10
            r6 = r6 | r7
            char r6 = (char) r6
            r0[r4] = r6
            int r4 = r0.length
            if (r5 < r4) goto L_0x00d7
            a.a.a.a.f r0 = r11.n
            char[] r0 = r0.k()
            r4 = r8
        L_0x00c5:
            r5 = 56320(0xdc00, float:7.8921E-41)
            r3 = r3 & 1023(0x3ff, float:1.434E-42)
            r3 = r3 | r5
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x006f
        L_0x00cf:
            a.a.a.a.f r0 = r11.n
            r0.a(r4)
            a.a.a.m r0 = a.a.a.m.VALUE_STRING
            return r0
        L_0x00d7:
            r4 = r5
            goto L_0x00c5
        L_0x00d9:
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.p.f(int):a.a.a.m");
    }

    private int g(int i) {
        int i2;
        char c;
        if (i >= 0) {
            return i;
        }
        if ((i & 224) == 192) {
            i2 = i & 31;
            c = 1;
        } else if ((i & 240) == 224) {
            i2 = i & 15;
            c = 2;
        } else if ((i & 248) == 240) {
            i2 = i & 7;
            c = 3;
        } else {
            m(i & 255);
            i2 = i;
            c = 1;
        }
        int w = w();
        if ((w & 192) != 128) {
            n(w & 255);
        }
        int i3 = (i2 << 6) | (w & 63);
        if (c <= 1) {
            return i3;
        }
        int w2 = w();
        if ((w2 & 192) != 128) {
            n(w2 & 255);
        }
        int i4 = (i3 << 6) | (w2 & 63);
        if (c <= 2) {
            return i4;
        }
        int w3 = w();
        if ((w3 & 192) != 128) {
            n(w3 & 255);
        }
        return (i4 << 6) | (w3 & 63);
    }

    private final int h(int i) {
        if (this.c >= this.d) {
            n();
        }
        byte[] bArr = this.f9a;
        int i2 = this.c;
        this.c = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            b(b & 255, this.c);
        }
        return (b & 63) | ((i & 31) << 6);
    }

    private final int i(int i) {
        if (this.c >= this.d) {
            n();
        }
        int i2 = i & 15;
        byte[] bArr = this.f9a;
        int i3 = this.c;
        this.c = i3 + 1;
        byte b = bArr[i3];
        if ((b & 192) != 128) {
            b(b & 255, this.c);
        }
        byte b2 = (i2 << 6) | (b & 63);
        if (this.c >= this.d) {
            n();
        }
        byte[] bArr2 = this.f9a;
        int i4 = this.c;
        this.c = i4 + 1;
        byte b3 = bArr2[i4];
        if ((b3 & 192) != 128) {
            b(b3 & 255, this.c);
        }
        return (b2 << 6) | (b3 & 63);
    }

    private final int j(int i) {
        int i2 = i & 15;
        byte[] bArr = this.f9a;
        int i3 = this.c;
        this.c = i3 + 1;
        byte b = bArr[i3];
        if ((b & 192) != 128) {
            b(b & 255, this.c);
        }
        byte b2 = (i2 << 6) | (b & 63);
        byte[] bArr2 = this.f9a;
        int i4 = this.c;
        this.c = i4 + 1;
        byte b3 = bArr2[i4];
        if ((b3 & 192) != 128) {
            b(b3 & 255, this.c);
        }
        return (b2 << 6) | (b3 & 63);
    }

    private final int k(int i) {
        if (this.c >= this.d) {
            n();
        }
        byte[] bArr = this.f9a;
        int i2 = this.c;
        this.c = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            b(b & 255, this.c);
        }
        byte b2 = (b & 63) | ((i & 7) << 6);
        if (this.c >= this.d) {
            n();
        }
        byte[] bArr2 = this.f9a;
        int i3 = this.c;
        this.c = i3 + 1;
        byte b3 = bArr2[i3];
        if ((b3 & 192) != 128) {
            b(b3 & 255, this.c);
        }
        byte b4 = (b2 << 6) | (b3 & 63);
        if (this.c >= this.d) {
            n();
        }
        byte[] bArr3 = this.f9a;
        int i4 = this.c;
        this.c = i4 + 1;
        byte b5 = bArr3[i4];
        if ((b5 & 192) != 128) {
            b(b5 & 255, this.c);
        }
        return ((b4 << 6) | (b5 & 63)) - 65536;
    }

    private void l(int i) {
        if (i < 32) {
            a(i);
        }
        m(i);
    }

    private void m(int i) {
        c("Invalid UTF-8 start byte 0x" + Integer.toHexString(i));
    }

    private void n(int i) {
        c("Invalid UTF-8 middle byte 0x" + Integer.toHexString(i));
    }

    private final int r() {
        while (true) {
            if (this.c < this.d || a()) {
                byte[] bArr = this.f9a;
                int i = this.c;
                this.c = i + 1;
                byte b = bArr[i] & 255;
                if (b > 32) {
                    if (b != 47) {
                        return b;
                    }
                    s();
                } else if (b != 32) {
                    if (b == 10) {
                        v();
                    } else if (b == 13) {
                        u();
                    } else if (b != 9) {
                        a(b);
                    }
                }
            } else {
                throw d("Unexpected end-of-input within/between " + this.k.e() + " entries");
            }
        }
    }

    private final void s() {
        if (!a(e.ALLOW_COMMENTS)) {
            b(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this.c >= this.d && !a()) {
            b(" in a comment");
        }
        byte[] bArr = this.f9a;
        int i = this.c;
        this.c = i + 1;
        byte b = bArr[i] & 255;
        if (b == 47) {
            int[] e = b.e();
            while (true) {
                if (this.c < this.d || a()) {
                    byte[] bArr2 = this.f9a;
                    int i2 = this.c;
                    this.c = i2 + 1;
                    byte b2 = bArr2[i2] & 255;
                    int i3 = e[b2];
                    if (i3 != 0) {
                        switch (i3) {
                            case 10:
                                v();
                                return;
                            case 13:
                                u();
                                return;
                            case 42:
                                break;
                            default:
                                l(b2);
                                continue;
                        }
                    }
                } else {
                    return;
                }
            }
        } else if (b == 42) {
            int[] e2 = b.e();
            while (true) {
                if (this.c < this.d || a()) {
                    byte[] bArr3 = this.f9a;
                    int i4 = this.c;
                    this.c = i4 + 1;
                    byte b3 = bArr3[i4] & 255;
                    int i5 = e2[b3];
                    if (i5 != 0) {
                        switch (i5) {
                            case 10:
                                v();
                                continue;
                            case 13:
                                u();
                                continue;
                            case 42:
                                if (this.f9a[this.c] == 47) {
                                    this.c++;
                                    return;
                                }
                                continue;
                            default:
                                l(b3);
                                continue;
                        }
                    }
                } else {
                    b(" in a comment");
                    return;
                }
            }
        } else {
            b(b, "was expecting either '*' or '/' for a comment");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private char t() {
        if (this.c >= this.d && !a()) {
            b(" in character escape sequence");
        }
        byte[] bArr = this.f9a;
        int i = this.c;
        this.c = i + 1;
        byte b = bArr[i];
        switch (b) {
            case 34:
            case 47:
            case 92:
                return (char) b;
            case 98:
                return 8;
            case 102:
                return 12;
            case 110:
                return 10;
            case 114:
                return 13;
            case 116:
                return 9;
            case 117:
                break;
            default:
                c("Unrecognized character escape (\\ followed by " + b(g(b)) + ")");
                break;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < 4; i3++) {
            if (this.c >= this.d && !a()) {
                b(" in character escape sequence");
            }
            byte[] bArr2 = this.f9a;
            int i4 = this.c;
            this.c = i4 + 1;
            byte b2 = bArr2[i4];
            int a2 = b.a(b2);
            if (a2 < 0) {
                b(b2, "expected a hex-digit for character escape sequence");
            }
            i2 = (i2 << 4) | a2;
        }
        return (char) i2;
    }

    private void u() {
        if ((this.c < this.d || a()) && this.f9a[this.c] == 10) {
            this.c++;
        }
        this.f++;
        this.g = this.c;
    }

    private void v() {
        this.f++;
        this.g = this.c;
    }

    private int w() {
        if (this.c >= this.d) {
            n();
        }
        byte[] bArr = this.f9a;
        int i = this.c;
        this.c = i + 1;
        return bArr[i] & 255;
    }

    public final void close() {
        super.close();
        this.t.b();
    }

    /* access modifiers changed from: protected */
    public final void h() {
        int i;
        int i2;
        int i3;
        int i4;
        char[] i5 = this.n.i();
        int[] b = b.b();
        byte[] bArr = this.f9a;
        int i6 = 0;
        while (true) {
            int i7 = this.c;
            if (i7 >= this.d) {
                n();
                i7 = this.c;
            }
            if (i6 >= i5.length) {
                i5 = this.n.k();
                i6 = 0;
            }
            int i8 = this.d;
            int length = (i5.length - i6) + i7;
            if (length < i8) {
                i = i6;
                i2 = length;
            } else {
                int i9 = i8;
                i = i6;
                i2 = i9;
            }
            while (true) {
                if (i7 >= i2) {
                    this.c = i7;
                    i6 = i;
                } else {
                    int i10 = i7 + 1;
                    byte b2 = bArr[i7] & 255;
                    if (b[b2] != 0) {
                        this.c = i10;
                        if (b2 != 34) {
                            switch (b[b2]) {
                                case 1:
                                    i4 = t();
                                    i3 = i;
                                    break;
                                case 2:
                                    i4 = h(b2);
                                    i3 = i;
                                    break;
                                case 3:
                                    if (this.d - this.c < 2) {
                                        i4 = i(b2);
                                        i3 = i;
                                        break;
                                    } else {
                                        i4 = j(b2);
                                        i3 = i;
                                        break;
                                    }
                                case 4:
                                    int k = k(b2);
                                    i3 = i + 1;
                                    i5[i] = (char) (55296 | (k >> 10));
                                    if (i3 >= i5.length) {
                                        i5 = this.n.k();
                                        i3 = 0;
                                    }
                                    i4 = (k & 1023) | 56320;
                                    break;
                                default:
                                    if (b2 >= 32) {
                                        l(b2);
                                        i4 = b2;
                                        i3 = i;
                                        break;
                                    } else {
                                        c(b2, "string value");
                                        i4 = b2;
                                        i3 = i;
                                        break;
                                    }
                            }
                            if (i3 >= i5.length) {
                                i5 = this.n.k();
                                i3 = 0;
                            }
                            i5[i3] = (char) i4;
                            i6 = i3 + 1;
                        } else {
                            this.n.a(i);
                            return;
                        }
                    } else {
                        i5[i] = (char) b2;
                        i7 = i10;
                        i++;
                    }
                }
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.b.c.a(int, char):void
     arg types: [int, int]
     candidates:
      a.a.a.b.p.a(int, int):a.a.a.d.f
      a.a.a.b.p.a(int[], int):int[]
      a.a.a.b.b.a(int, java.lang.String):void
      a.a.a.b.c.a(int, char):void */
    /* JADX WARNING: CFG modification limit reached, blocks count: 304 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final a.a.a.m i() {
        /*
            r12 = this;
            r10 = 2
            r7 = 128(0x80, float:1.794E-43)
            r9 = 1
            r2 = 0
            r8 = 34
            a.a.a.m r0 = r12.r
            a.a.a.m r1 = a.a.a.m.FIELD_NAME
            if (r0 != r1) goto L_0x0038
            r12.o = r2
            a.a.a.m r0 = r12.l
            r1 = 0
            r12.l = r1
            a.a.a.m r1 = a.a.a.m.START_ARRAY
            if (r0 != r1) goto L_0x0027
            a.a.a.b.o r1 = r12.k
            int r2 = r12.i
            int r3 = r12.j
            a.a.a.b.o r1 = r1.a(r2, r3)
            r12.k = r1
        L_0x0024:
            r12.r = r0
        L_0x0026:
            return r0
        L_0x0027:
            a.a.a.m r1 = a.a.a.m.START_OBJECT
            if (r0 != r1) goto L_0x0024
            a.a.a.b.o r1 = r12.k
            int r2 = r12.i
            int r3 = r12.j
            a.a.a.b.o r1 = r1.b(r2, r3)
            r12.k = r1
            goto L_0x0024
        L_0x0038:
            boolean r0 = r12.m
            if (r0 == 0) goto L_0x014f
            r12.m = r2
            int[] r0 = a.a.a.a.b.b()
            byte[] r1 = r12.f9a
        L_0x0044:
            int r3 = r12.c
            int r4 = r12.d
            if (r3 < r4) goto L_0x0408
            r12.n()
            int r3 = r12.c
            int r4 = r12.d
            r11 = r4
            r4 = r3
            r3 = r11
        L_0x0054:
            if (r4 < r3) goto L_0x0059
            r12.c = r4
            goto L_0x0044
        L_0x0059:
            int r5 = r4 + 1
            byte r4 = r1[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r6 = r0[r4]
            if (r6 == 0) goto L_0x0405
            r12.c = r5
            if (r4 == r8) goto L_0x014f
            r3 = r0[r4]
            switch(r3) {
                case 1: goto L_0x0076;
                case 2: goto L_0x007a;
                case 3: goto L_0x0099;
                case 4: goto L_0x00d7;
                default: goto L_0x006c;
            }
        L_0x006c:
            r3 = 32
            if (r4 >= r3) goto L_0x0129
            java.lang.String r3 = "string value"
            r12.c(r4, r3)
            goto L_0x0044
        L_0x0076:
            r12.t()
            goto L_0x0044
        L_0x007a:
            int r3 = r12.c
            int r4 = r12.d
            if (r3 < r4) goto L_0x0083
            r12.n()
        L_0x0083:
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r4 = r3 & 192(0xc0, float:2.69E-43)
            if (r4 == r7) goto L_0x0044
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r4 = r12.c
            r12.b(r3, r4)
            goto L_0x0044
        L_0x0099:
            int r3 = r12.c
            int r4 = r12.d
            if (r3 < r4) goto L_0x00a2
            r12.n()
        L_0x00a2:
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r4 = r3 & 192(0xc0, float:2.69E-43)
            if (r4 == r7) goto L_0x00b7
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r4 = r12.c
            r12.b(r3, r4)
        L_0x00b7:
            int r3 = r12.c
            int r4 = r12.d
            if (r3 < r4) goto L_0x00c0
            r12.n()
        L_0x00c0:
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r4 = r3 & 192(0xc0, float:2.69E-43)
            if (r4 == r7) goto L_0x0044
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r4 = r12.c
            r12.b(r3, r4)
            goto L_0x0044
        L_0x00d7:
            int r3 = r12.c
            int r4 = r12.d
            if (r3 < r4) goto L_0x00e0
            r12.n()
        L_0x00e0:
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r4 = r3 & 192(0xc0, float:2.69E-43)
            if (r4 == r7) goto L_0x00f5
            r4 = r3 & 255(0xff, float:3.57E-43)
            int r5 = r12.c
            r12.b(r4, r5)
        L_0x00f5:
            int r4 = r12.c
            int r5 = r12.d
            if (r4 < r5) goto L_0x00fe
            r12.n()
        L_0x00fe:
            r4 = r3 & 192(0xc0, float:2.69E-43)
            if (r4 == r7) goto L_0x0109
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r4 = r12.c
            r12.b(r3, r4)
        L_0x0109:
            int r3 = r12.c
            int r4 = r12.d
            if (r3 < r4) goto L_0x0112
            r12.n()
        L_0x0112:
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r4 = r3 & 192(0xc0, float:2.69E-43)
            if (r4 == r7) goto L_0x0044
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r4 = r12.c
            r12.b(r3, r4)
            goto L_0x0044
        L_0x0129:
            r12.l(r4)
            goto L_0x0044
        L_0x012e:
            byte[] r0 = r12.f9a
            int r1 = r12.c
            int r3 = r1 + 1
            r12.c = r3
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            r1 = 32
            if (r0 <= r1) goto L_0x0160
            r1 = 47
            if (r0 == r1) goto L_0x014c
        L_0x0142:
            if (r0 >= 0) goto L_0x017c
            r12.close()
            r0 = 0
            r12.r = r0
            goto L_0x0026
        L_0x014c:
            r12.s()
        L_0x014f:
            int r0 = r12.c
            int r1 = r12.d
            if (r0 < r1) goto L_0x012e
            boolean r0 = r12.a()
            if (r0 != 0) goto L_0x012e
            r12.o()
            r0 = -1
            goto L_0x0142
        L_0x0160:
            r1 = 32
            if (r0 == r1) goto L_0x014f
            r1 = 10
            if (r0 != r1) goto L_0x016c
            r12.v()
            goto L_0x014f
        L_0x016c:
            r1 = 13
            if (r0 != r1) goto L_0x0174
            r12.u()
            goto L_0x014f
        L_0x0174:
            r1 = 9
            if (r0 == r1) goto L_0x014f
            r12.a(r0)
            goto L_0x014f
        L_0x017c:
            long r3 = r12.e
            int r1 = r12.c
            long r5 = (long) r1
            long r3 = r3 + r5
            r5 = 1
            long r3 = r3 - r5
            r12.h = r3
            int r1 = r12.f
            r12.i = r1
            int r1 = r12.c
            int r3 = r12.g
            int r1 = r1 - r3
            int r1 = r1 - r9
            r12.j = r1
            r1 = 0
            r12.p = r1
            r1 = 93
            if (r0 != r1) goto L_0x01b5
            a.a.a.b.o r1 = r12.k
            boolean r1 = r1.b()
            if (r1 != 0) goto L_0x01a7
            r1 = 125(0x7d, float:1.75E-43)
            r12.a(r0, r1)
        L_0x01a7:
            a.a.a.b.o r0 = r12.k
            a.a.a.b.o r0 = r0.g()
            r12.k = r0
            a.a.a.m r0 = a.a.a.m.END_ARRAY
            r12.r = r0
            goto L_0x0026
        L_0x01b5:
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x01d4
            a.a.a.b.o r1 = r12.k
            boolean r1 = r1.d()
            if (r1 != 0) goto L_0x01c6
            r1 = 93
            r12.a(r0, r1)
        L_0x01c6:
            a.a.a.b.o r0 = r12.k
            a.a.a.b.o r0 = r0.g()
            r12.k = r0
            a.a.a.m r0 = a.a.a.m.END_OBJECT
            r12.r = r0
            goto L_0x0026
        L_0x01d4:
            a.a.a.b.o r1 = r12.k
            boolean r1 = r1.h()
            if (r1 == 0) goto L_0x0202
            r1 = 44
            if (r0 == r1) goto L_0x01fe
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "was expecting comma to separate "
            r1.<init>(r3)
            a.a.a.b.o r3 = r12.k
            java.lang.String r3 = r3.e()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = " entries"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r12.b(r0, r1)
        L_0x01fe:
            int r0 = r12.r()
        L_0x0202:
            a.a.a.b.o r1 = r12.k
            boolean r6 = r1.d()
            if (r6 == 0) goto L_0x022e
            if (r0 == r8) goto L_0x023d
            a.a.a.d.f r0 = r12.e(r0)
        L_0x0210:
            a.a.a.b.o r1 = r12.k
            java.lang.String r0 = r0.b()
            r1.a(r0)
            a.a.a.m r0 = a.a.a.m.FIELD_NAME
            r12.r = r0
            int r0 = r12.r()
            r1 = 58
            if (r0 == r1) goto L_0x022a
            java.lang.String r1 = "was expecting a colon to separate field name and value"
            r12.b(r0, r1)
        L_0x022a:
            int r0 = r12.r()
        L_0x022e:
            switch(r0) {
                case 34: goto L_0x03b1;
                case 45: goto L_0x03fb;
                case 48: goto L_0x03fb;
                case 49: goto L_0x03fb;
                case 50: goto L_0x03fb;
                case 51: goto L_0x03fb;
                case 52: goto L_0x03fb;
                case 53: goto L_0x03fb;
                case 54: goto L_0x03fb;
                case 55: goto L_0x03fb;
                case 56: goto L_0x03fb;
                case 57: goto L_0x03fb;
                case 91: goto L_0x03b7;
                case 93: goto L_0x03db;
                case 102: goto L_0x03e9;
                case 110: goto L_0x03f2;
                case 116: goto L_0x03e0;
                case 123: goto L_0x03c9;
                case 125: goto L_0x03db;
                default: goto L_0x0231;
            }
        L_0x0231:
            a.a.a.m r0 = r12.f(r0)
        L_0x0235:
            if (r6 == 0) goto L_0x0401
            r12.l = r0
            a.a.a.m r0 = r12.r
            goto L_0x0026
        L_0x023d:
            int r0 = r12.d
            int r1 = r12.c
            int r0 = r0 - r1
            r1 = 9
            if (r0 >= r1) goto L_0x0274
            int r0 = r12.c
            int r1 = r12.d
            if (r0 < r1) goto L_0x0257
            boolean r0 = r12.a()
            if (r0 != 0) goto L_0x0257
            java.lang.String r0 = ": was expecting closing '\"' for name"
            r12.b(r0)
        L_0x0257:
            byte[] r0 = r12.f9a
            int r1 = r12.c
            int r3 = r1 + 1
            r12.c = r3
            byte r0 = r0[r1]
            r4 = r0 & 255(0xff, float:3.57E-43)
            if (r4 != r8) goto L_0x026a
            a.a.a.d.f r0 = a.a.a.d.h.c()
            goto L_0x0210
        L_0x026a:
            int[] r1 = r12.u
            r0 = r12
            r3 = r2
            r5 = r2
            a.a.a.d.f r0 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0210
        L_0x0274:
            int[] r0 = a.a.a.a.b.a()
            byte[] r1 = r12.f9a
            int r3 = r12.c
            int r4 = r3 + 1
            r12.c = r4
            byte r1 = r1[r3]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r3 = r0[r1]
            if (r3 == 0) goto L_0x0295
            if (r1 != r8) goto L_0x028f
            a.a.a.d.f r0 = a.a.a.d.h.c()
            goto L_0x0210
        L_0x028f:
            a.a.a.d.f r0 = r12.a(r2, r1, r2)
            goto L_0x0210
        L_0x0295:
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r3 = r3 & 255(0xff, float:3.57E-43)
            r4 = r0[r3]
            if (r4 == 0) goto L_0x02b3
            if (r3 != r8) goto L_0x02ad
            a.a.a.d.f r0 = r12.a(r1, r9)
            goto L_0x0210
        L_0x02ad:
            a.a.a.d.f r0 = r12.a(r1, r3, r9)
            goto L_0x0210
        L_0x02b3:
            int r1 = r1 << 8
            r1 = r1 | r3
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r3 = r3 & 255(0xff, float:3.57E-43)
            r4 = r0[r3]
            if (r4 == 0) goto L_0x02d4
            if (r3 != r8) goto L_0x02ce
            a.a.a.d.f r0 = r12.a(r1, r10)
            goto L_0x0210
        L_0x02ce:
            a.a.a.d.f r0 = r12.a(r1, r3, r10)
            goto L_0x0210
        L_0x02d4:
            int r1 = r1 << 8
            r1 = r1 | r3
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r3 = r3 & 255(0xff, float:3.57E-43)
            r4 = r0[r3]
            if (r4 == 0) goto L_0x02f7
            if (r3 != r8) goto L_0x02f0
            r0 = 3
            a.a.a.d.f r0 = r12.a(r1, r0)
            goto L_0x0210
        L_0x02f0:
            r0 = 3
            a.a.a.d.f r0 = r12.a(r1, r3, r0)
            goto L_0x0210
        L_0x02f7:
            int r1 = r1 << 8
            r1 = r1 | r3
            byte[] r3 = r12.f9a
            int r4 = r12.c
            int r5 = r4 + 1
            r12.c = r5
            byte r3 = r3[r4]
            r3 = r3 & 255(0xff, float:3.57E-43)
            r0 = r0[r3]
            if (r0 == 0) goto L_0x031a
            if (r3 != r8) goto L_0x0313
            r0 = 4
            a.a.a.d.f r0 = r12.a(r1, r0)
            goto L_0x0210
        L_0x0313:
            r0 = 4
            a.a.a.d.f r0 = r12.a(r1, r3, r0)
            goto L_0x0210
        L_0x031a:
            int[] r0 = a.a.a.a.b.a()
            byte[] r4 = r12.f9a
            int r5 = r12.c
            int r7 = r5 + 1
            r12.c = r7
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = r0[r4]
            if (r5 == 0) goto L_0x033c
            if (r4 != r8) goto L_0x0336
            a.a.a.d.f r0 = r12.b(r1, r3, r9)
            goto L_0x0210
        L_0x0336:
            a.a.a.d.f r0 = r12.a(r1, r3, r4, r9)
            goto L_0x0210
        L_0x033c:
            int r3 = r3 << 8
            r3 = r3 | r4
            byte[] r4 = r12.f9a
            int r5 = r12.c
            int r7 = r5 + 1
            r12.c = r7
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = r0[r4]
            if (r5 == 0) goto L_0x035d
            if (r4 != r8) goto L_0x0357
            a.a.a.d.f r0 = r12.b(r1, r3, r10)
            goto L_0x0210
        L_0x0357:
            a.a.a.d.f r0 = r12.a(r1, r3, r4, r10)
            goto L_0x0210
        L_0x035d:
            int r3 = r3 << 8
            r3 = r3 | r4
            byte[] r4 = r12.f9a
            int r5 = r12.c
            int r7 = r5 + 1
            r12.c = r7
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = r0[r4]
            if (r5 == 0) goto L_0x0380
            if (r4 != r8) goto L_0x0379
            r0 = 3
            a.a.a.d.f r0 = r12.b(r1, r3, r0)
            goto L_0x0210
        L_0x0379:
            r0 = 3
            a.a.a.d.f r0 = r12.a(r1, r3, r4, r0)
            goto L_0x0210
        L_0x0380:
            int r3 = r3 << 8
            r3 = r3 | r4
            byte[] r4 = r12.f9a
            int r5 = r12.c
            int r7 = r5 + 1
            r12.c = r7
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r0 = r0[r4]
            if (r0 == 0) goto L_0x03a3
            if (r4 != r8) goto L_0x039c
            r0 = 4
            a.a.a.d.f r0 = r12.b(r1, r3, r0)
            goto L_0x0210
        L_0x039c:
            r0 = 4
            a.a.a.d.f r0 = r12.a(r1, r3, r4, r0)
            goto L_0x0210
        L_0x03a3:
            int[] r0 = r12.u
            r0[r2] = r1
            int[] r0 = r12.u
            r0[r9] = r3
            a.a.a.d.f r0 = r12.d(r4)
            goto L_0x0210
        L_0x03b1:
            r12.m = r9
            a.a.a.m r0 = a.a.a.m.VALUE_STRING
            goto L_0x0235
        L_0x03b7:
            if (r6 != 0) goto L_0x03c5
            a.a.a.b.o r0 = r12.k
            int r1 = r12.i
            int r2 = r12.j
            a.a.a.b.o r0 = r0.a(r1, r2)
            r12.k = r0
        L_0x03c5:
            a.a.a.m r0 = a.a.a.m.START_ARRAY
            goto L_0x0235
        L_0x03c9:
            if (r6 != 0) goto L_0x03d7
            a.a.a.b.o r0 = r12.k
            int r1 = r12.i
            int r2 = r12.j
            a.a.a.b.o r0 = r0.b(r1, r2)
            r12.k = r0
        L_0x03d7:
            a.a.a.m r0 = a.a.a.m.START_OBJECT
            goto L_0x0235
        L_0x03db:
            java.lang.String r1 = "expected a value"
            r12.b(r0, r1)
        L_0x03e0:
            a.a.a.m r0 = a.a.a.m.VALUE_TRUE
            r12.a(r0)
            a.a.a.m r0 = a.a.a.m.VALUE_TRUE
            goto L_0x0235
        L_0x03e9:
            a.a.a.m r0 = a.a.a.m.VALUE_FALSE
            r12.a(r0)
            a.a.a.m r0 = a.a.a.m.VALUE_FALSE
            goto L_0x0235
        L_0x03f2:
            a.a.a.m r0 = a.a.a.m.VALUE_NULL
            r12.a(r0)
            a.a.a.m r0 = a.a.a.m.VALUE_NULL
            goto L_0x0235
        L_0x03fb:
            a.a.a.m r0 = r12.c(r0)
            goto L_0x0235
        L_0x0401:
            r12.r = r0
            goto L_0x0026
        L_0x0405:
            r4 = r5
            goto L_0x0054
        L_0x0408:
            r11 = r4
            r4 = r3
            r3 = r11
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.p.i():a.a.a.m");
    }
}
