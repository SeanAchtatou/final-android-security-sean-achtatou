package a.a.a.b;

import a.a.a.c.a;
import a.a.a.m;
import java.io.InputStream;

public abstract class j extends a {
    public j(a aVar, int i, InputStream inputStream, byte[] bArr, int i2, int i3, boolean z) {
        super(aVar, i, inputStream, bArr, i2, i3, z);
    }

    /* access modifiers changed from: protected */
    public final m c(int i) {
        byte b;
        int i2;
        char[] cArr;
        int i3;
        int i4;
        boolean z;
        byte b2;
        char[] cArr2;
        int i5;
        boolean z2;
        int i6;
        int i7;
        boolean z3;
        int i8;
        byte b3;
        char[] cArr3;
        int i9;
        int i10;
        char[] i11 = this.n.i();
        int i12 = 0;
        boolean z4 = i == 45;
        if (z4) {
            i12 = 0 + 1;
            i11[0] = '-';
            if (this.c >= this.d) {
                n();
            }
            byte[] bArr = this.f9a;
            int i13 = this.c;
            this.c = i13 + 1;
            b = bArr[i13] & 255;
        } else {
            b = i;
        }
        byte b4 = b;
        char[] cArr4 = i11;
        int i14 = 0;
        while (true) {
            if (b4 >= 48 && b4 <= 57) {
                i14++;
                if (i14 == 2 && cArr4[i12 - 1] == '0') {
                    a("Leading zeroes not allowed");
                }
                if (i12 >= cArr4.length) {
                    cArr4 = this.n.k();
                    i12 = 0;
                }
                int i15 = i12 + 1;
                cArr4[i12] = (char) b4;
                if (this.c >= this.d && !a()) {
                    i2 = 0;
                    i4 = i14;
                    z = true;
                    cArr = cArr4;
                    i3 = i15;
                    break;
                }
                byte[] bArr2 = this.f9a;
                int i16 = this.c;
                this.c = i16 + 1;
                b4 = bArr2[i16] & 255;
                i12 = i15;
            } else {
                i2 = b4;
                cArr = cArr4;
                i3 = i12;
                i4 = i14;
                z = false;
            }
        }
        i2 = b4;
        cArr = cArr4;
        i3 = i12;
        i4 = i14;
        z = false;
        if (i4 == 0) {
            a("Missing integer part (next char " + b(i2) + ")");
        }
        if (i2 == 46) {
            int i17 = i3 + 1;
            cArr[i3] = (char) i2;
            int i18 = 0;
            b2 = i2;
            cArr2 = cArr;
            while (true) {
                i5 = i17;
                if (this.c >= this.d && !a()) {
                    z = true;
                    break;
                }
                byte[] bArr3 = this.f9a;
                int i19 = this.c;
                this.c = i19 + 1;
                b2 = bArr3[i19] & 255;
                if (b2 < 48 || b2 > 57) {
                    break;
                }
                i18++;
                if (i5 >= cArr2.length) {
                    cArr2 = this.n.k();
                    i5 = 0;
                }
                i17 = i5 + 1;
                cArr2[i5] = (char) b2;
            }
            if (i18 == 0) {
                a(b2, "Decimal point not followed by a digit");
            }
            int i20 = i18;
            z2 = z;
            i6 = i20;
        } else {
            b2 = i2;
            cArr2 = cArr;
            i5 = i3;
            z2 = z;
            i6 = 0;
        }
        if (b2 == 101 || b2 == 69) {
            if (i5 >= cArr2.length) {
                cArr2 = this.n.k();
                i5 = 0;
            }
            int i21 = i5 + 1;
            cArr2[i5] = (char) b2;
            if (this.c >= this.d) {
                n();
            }
            byte[] bArr4 = this.f9a;
            int i22 = this.c;
            this.c = i22 + 1;
            byte b5 = bArr4[i22] & 255;
            if (b5 == 45 || b5 == 43) {
                if (i21 >= cArr2.length) {
                    cArr3 = this.n.k();
                    i10 = 0;
                } else {
                    cArr3 = cArr2;
                    i10 = i21;
                }
                int i23 = i10 + 1;
                cArr3[i10] = (char) b5;
                if (this.c >= this.d) {
                    n();
                }
                byte[] bArr5 = this.f9a;
                int i24 = this.c;
                this.c = i24 + 1;
                i7 = i23;
                b3 = bArr5[i24] & 255;
                i9 = 0;
            } else {
                cArr3 = cArr2;
                i7 = i21;
                b3 = b5;
                i9 = 0;
            }
            while (true) {
                if (b3 <= 57 && b3 >= 48) {
                    i9++;
                    if (i7 >= cArr3.length) {
                        cArr3 = this.n.k();
                        i7 = 0;
                    }
                    int i25 = i7 + 1;
                    cArr3[i7] = (char) b3;
                    if (this.c >= this.d && !a()) {
                        i7 = i25;
                        i8 = i9;
                        z3 = true;
                        break;
                    }
                    byte[] bArr6 = this.f9a;
                    int i26 = this.c;
                    this.c = i26 + 1;
                    b3 = bArr6[i26] & 255;
                    i7 = i25;
                } else {
                    int i27 = i9;
                    z3 = z2;
                    i8 = i27;
                }
            }
            int i272 = i9;
            z3 = z2;
            i8 = i272;
            if (i8 == 0) {
                a(b3, "Exponent indicator not followed by a digit");
            }
        } else {
            i7 = i5;
            z3 = z2;
            i8 = 0;
        }
        if (!z3) {
            this.c--;
        }
        this.n.a(i7);
        return a(z4, i4, i6, i8);
    }
}
