package a.a.a;

import a.a.a.a.e;
import a.a.a.b.f;
import a.a.a.b.i;
import a.a.a.b.m;
import a.a.a.c.a;
import a.a.a.c.b;
import a.a.a.d.g;
import a.a.a.d.h;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.ref.SoftReference;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private static int f36a = e.a();
    private static int b = j.a();
    private static ThreadLocal c = new ThreadLocal();
    private g d;
    private h e;
    private d f;
    private int g;
    private int h;

    private n() {
        this.d = g.a();
        this.e = h.a();
        this.g = f36a;
        this.h = b;
        this.f = null;
    }

    public n(byte b2) {
        this();
    }

    private static a a(Object obj, boolean z) {
        e eVar;
        SoftReference softReference = (SoftReference) c.get();
        e eVar2 = softReference == null ? null : (e) softReference.get();
        if (eVar2 == null) {
            e eVar3 = new e();
            if (softReference == null) {
                c.set(new SoftReference(eVar3));
            }
            eVar = eVar3;
        } else {
            eVar = eVar2;
        }
        return new a(eVar, obj, z);
    }

    private f a(Writer writer, a aVar) {
        return new m(aVar, this.h, this.f, writer);
    }

    private boolean a(e eVar) {
        return (this.g & eVar.b()) != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a
     arg types: [java.io.OutputStream, int]
     candidates:
      a.a.a.n.a(java.io.Writer, a.a.a.c.a):a.a.a.f
      a.a.a.n.a(java.io.OutputStream, a.a.a.b):a.a.a.f
      a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a */
    public final f a(OutputStream outputStream, b bVar) {
        a a2 = a((Object) outputStream, false);
        a2.a(bVar);
        return a(bVar == b.UTF8 ? new b(a2, outputStream) : new OutputStreamWriter(outputStream, bVar.a()), a2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a
     arg types: [java.io.Writer, int]
     candidates:
      a.a.a.n.a(java.io.Writer, a.a.a.c.a):a.a.a.f
      a.a.a.n.a(java.io.OutputStream, a.a.a.b):a.a.a.f
      a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a */
    public final f a(Writer writer) {
        return a(writer, a((Object) writer, false));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a
     arg types: [java.io.InputStream, int]
     candidates:
      a.a.a.n.a(java.io.Writer, a.a.a.c.a):a.a.a.f
      a.a.a.n.a(java.io.OutputStream, a.a.a.b):a.a.a.f
      a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a */
    public final h a(InputStream inputStream) {
        return new f(a((Object) inputStream, false), inputStream).a(this.g, this.f, this.e, this.d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a
     arg types: [java.io.Reader, int]
     candidates:
      a.a.a.n.a(java.io.Writer, a.a.a.c.a):a.a.a.f
      a.a.a.n.a(java.io.OutputStream, a.a.a.b):a.a.a.f
      a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a */
    public final h a(Reader reader) {
        return new i(a((Object) reader, false), this.g, reader, this.f, this.d.a(a(e.CANONICALIZE_FIELD_NAMES), a(e.INTERN_FIELD_NAMES)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a
     arg types: [byte[], int]
     candidates:
      a.a.a.n.a(java.io.Writer, a.a.a.c.a):a.a.a.f
      a.a.a.n.a(java.io.OutputStream, a.a.a.b):a.a.a.f
      a.a.a.n.a(java.lang.Object, boolean):a.a.a.c.a */
    public final h a(byte[] bArr) {
        return new f(a((Object) bArr, true), bArr, bArr.length).a(this.g, this.f, this.e, this.d);
    }
}
