package a.a.a;

public enum j {
    AUTO_CLOSE_TARGET(true),
    AUTO_CLOSE_JSON_CONTENT(true),
    QUOTE_FIELD_NAMES(true),
    QUOTE_NON_NUMERIC_NUMBERS(true),
    WRITE_NUMBERS_AS_STRINGS(false);
    
    private boolean f;
    private int g = (1 << ordinal());

    private j(boolean z) {
        this.f = z;
    }

    public static int a() {
        int i = 0;
        for (j jVar : values()) {
            if (jVar.f) {
                i |= jVar.g;
            }
        }
        return i;
    }

    public final int b() {
        return this.g;
    }
}
