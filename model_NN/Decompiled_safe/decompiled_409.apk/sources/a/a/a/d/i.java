package a.a.a.d;

final class i {

    /* renamed from: a  reason: collision with root package name */
    private final String f28a;
    private final i b;

    public i(String str, i iVar) {
        this.f28a = str;
        this.b = iVar;
    }

    public final String a() {
        return this.f28a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0018  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(char[] r7, int r8, int r9) {
        /*
            r6 = this;
            java.lang.String r0 = r6.f28a
            a.a.a.d.i r1 = r6.b
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0007:
            int r2 = r1.length()
            if (r2 != r9) goto L_0x0020
            r2 = 0
        L_0x000e:
            char r3 = r1.charAt(r2)
            int r4 = r8 + r2
            char r4 = r7[r4]
            if (r3 != r4) goto L_0x001c
            int r2 = r2 + 1
            if (r2 < r9) goto L_0x000e
        L_0x001c:
            if (r2 != r9) goto L_0x0020
            r0 = r1
        L_0x001f:
            return r0
        L_0x0020:
            if (r0 == 0) goto L_0x0027
            java.lang.String r1 = r0.f28a
            a.a.a.d.i r0 = r0.b
            goto L_0x0007
        L_0x0027:
            r0 = 0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.d.i.a(char[], int, int):java.lang.String");
    }

    public final i b() {
        return this.b;
    }
}
