package a.a.a.d;

public final class a extends f {

    /* renamed from: a  reason: collision with root package name */
    private int[] f20a;
    private int b;

    a(String str, int i, int[] iArr, int i2) {
        super(str, i);
        if (i2 < 3) {
            throw new IllegalArgumentException("Qlen must >= 3");
        }
        this.f20a = iArr;
        this.b = i2;
    }

    public final boolean a(int i) {
        return false;
    }

    public final boolean a(int i, int i2) {
        return false;
    }

    public final boolean a(int[] iArr, int i) {
        if (i != this.b) {
            return false;
        }
        for (int i2 = 0; i2 < i; i2++) {
            if (iArr[i2] != this.f20a[i2]) {
                return false;
            }
        }
        return true;
    }
}
