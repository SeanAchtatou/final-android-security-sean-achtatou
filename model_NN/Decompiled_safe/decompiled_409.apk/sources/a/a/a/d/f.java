package a.a.a.d;

public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    private String f25a;
    private int b;

    protected f(String str, int i) {
        this.f25a = str;
        this.b = i;
    }

    public abstract boolean a(int i);

    public abstract boolean a(int i, int i2);

    public abstract boolean a(int[] iArr, int i);

    public final String b() {
        return this.f25a;
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public final int hashCode() {
        return this.b;
    }

    public String toString() {
        return this.f25a;
    }
}
