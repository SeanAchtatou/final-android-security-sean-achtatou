package a.a.a.d;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private h f27a;
    private boolean b;
    private int c;
    private int d;
    private int[] e;
    private f[] f;
    private e[] g;
    private int h;
    private int i;
    private transient boolean j;
    private boolean k;
    private boolean l;
    private boolean m;

    private h() {
        this.f27a = null;
        this.b = true;
        d();
    }

    private h(h hVar, boolean z) {
        this.f27a = hVar;
        this.b = z;
        this.c = hVar.c;
        this.d = hVar.d;
        this.e = hVar.e;
        this.f = hVar.f;
        this.g = hVar.g;
        this.h = hVar.h;
        this.i = hVar.i;
        this.j = false;
        this.k = true;
        this.l = true;
        this.m = true;
    }

    public static h a() {
        return new h();
    }

    private synchronized void a(h hVar) {
        if (hVar.c > this.c) {
            if (hVar.c > 6000) {
                d();
            } else {
                this.c = hVar.c;
                this.e = hVar.e;
                this.f = hVar.f;
                this.k = true;
                this.l = true;
                this.d = hVar.d;
                this.g = hVar.g;
                this.h = hVar.h;
                this.i = hVar.i;
            }
        }
    }

    private static int b(int[] iArr, int i2) {
        int i3 = iArr[0];
        for (int i4 = 1; i4 < i2; i4++) {
            i3 = (i3 * 31) + iArr[i4];
        }
        int i5 = (i3 >>> 16) ^ i3;
        return i5 ^ (i5 >>> 8);
    }

    public static f c() {
        return b.a();
    }

    private void d() {
        this.c = 0;
        this.e = new int[64];
        this.f = new f[64];
        this.k = false;
        this.l = false;
        this.d = 63;
        this.m = true;
        this.g = null;
        this.i = 0;
        this.j = false;
    }

    private int e() {
        e[] eVarArr = this.g;
        int i2 = -1;
        int i3 = this.i;
        int i4 = Integer.MAX_VALUE;
        for (int i5 = 0; i5 < i3; i5++) {
            int i6 = 1;
            for (e eVar = eVarArr[i5].b; eVar != null; eVar = eVar.b) {
                i6++;
            }
            if (i6 < i4) {
                if (i6 == 1) {
                    return i5;
                }
                i2 = i5;
                i4 = i6;
            }
        }
        return i2;
    }

    private void f() {
        e[] eVarArr = this.g;
        int length = eVarArr.length;
        this.g = new e[(length + length)];
        System.arraycopy(eVarArr, 0, this.g, 0, length);
    }

    public final f a(int i2) {
        e eVar;
        int i3 = (i2 >>> 16) ^ i2;
        int i4 = i3 ^ (i3 >>> 8);
        int i5 = this.d & i4;
        int i6 = this.e[i5];
        if ((((i6 >> 8) ^ i4) << 8) == 0) {
            f fVar = this.f[i5];
            if (fVar == null) {
                return null;
            }
            if (fVar.a(i2)) {
                return fVar;
            }
        } else if (i6 == 0) {
            return null;
        }
        int i7 = i6 & 255;
        if (i7 <= 0 || (eVar = this.g[i7 - 1]) == null) {
            return null;
        }
        return eVar.a(i4, i2, 0);
    }

    public final f a(int i2, int i3) {
        e eVar;
        int i4 = (i2 * 31) + i3;
        int i5 = i4 ^ (i4 >>> 16);
        int i6 = i5 ^ (i5 >>> 8);
        int i7 = this.d & i6;
        int i8 = this.e[i7];
        if ((((i8 >> 8) ^ i6) << 8) == 0) {
            f fVar = this.f[i7];
            if (fVar == null) {
                return null;
            }
            if (fVar.a(i2, i3)) {
                return fVar;
            }
        } else if (i8 == 0) {
            return null;
        }
        int i9 = i8 & 255;
        if (i9 <= 0 || (eVar = this.g[i9 - 1]) == null) {
            return null;
        }
        return eVar.a(i6, i2, i3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0293  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final a.a.a.d.f a(java.lang.String r20, int[] r21, int r22) {
        /*
            r19 = this;
            r0 = r19
            boolean r0 = r0.b
            r5 = r0
            if (r5 == 0) goto L_0x0338
            a.a.a.a.c r5 = a.a.a.a.c.f3a
            r0 = r5
            r1 = r20
            java.lang.String r5 = r0.a(r1)
            r6 = r5
        L_0x0011:
            int r7 = b(r21, r22)
            r5 = 4
            r0 = r22
            r1 = r5
            if (r0 >= r1) goto L_0x001e
            switch(r22) {
                case 1: goto L_0x0119;
                case 2: goto L_0x0123;
                case 3: goto L_0x0130;
                default: goto L_0x001e;
            }
        L_0x001e:
            r0 = r22
            int[] r0 = new int[r0]
            r5 = r0
            r8 = 0
        L_0x0024:
            r0 = r8
            r1 = r22
            if (r0 < r1) goto L_0x0140
            a.a.a.d.a r8 = new a.a.a.d.a
            r0 = r8
            r1 = r6
            r2 = r7
            r3 = r5
            r4 = r22
            r0.<init>(r1, r2, r3, r4)
            r5 = r8
        L_0x0035:
            r0 = r19
            boolean r0 = r0.k
            r6 = r0
            if (r6 == 0) goto L_0x005e
            r0 = r19
            int[] r0 = r0.e
            r6 = r0
            r0 = r19
            int[] r0 = r0.e
            r8 = r0
            int r8 = r8.length
            int[] r9 = new int[r8]
            r0 = r9
            r1 = r19
            r1.e = r0
            r9 = 0
            r0 = r19
            int[] r0 = r0.e
            r10 = r0
            r11 = 0
            java.lang.System.arraycopy(r6, r9, r10, r11, r8)
            r6 = 0
            r0 = r6
            r1 = r19
            r1.k = r0
        L_0x005e:
            r0 = r19
            boolean r0 = r0.j
            r6 = r0
            if (r6 == 0) goto L_0x00aa
            r6 = 0
            r0 = r6
            r1 = r19
            r1.j = r0
            r6 = 0
            r0 = r6
            r1 = r19
            r1.l = r0
            r0 = r19
            int[] r0 = r0.e
            r6 = r0
            int r6 = r6.length
            int r8 = r6 + r6
            r9 = 65536(0x10000, float:9.18355E-41)
            if (r8 <= r9) goto L_0x0148
            r6 = 0
            r0 = r6
            r1 = r19
            r1.c = r0
            r0 = r19
            int[] r0 = r0.e
            r6 = r0
            r8 = 0
            java.util.Arrays.fill(r6, r8)
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r6 = r0
            r8 = 0
            java.util.Arrays.fill(r6, r8)
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r6 = r0
            r8 = 0
            java.util.Arrays.fill(r6, r8)
            r6 = 0
            r0 = r6
            r1 = r19
            r1.h = r0
            r6 = 0
            r0 = r6
            r1 = r19
            r1.i = r0
        L_0x00aa:
            r0 = r19
            int r0 = r0.c
            r6 = r0
            int r6 = r6 + 1
            r0 = r6
            r1 = r19
            r1.c = r0
            r0 = r19
            int r0 = r0.d
            r6 = r0
            r6 = r6 & r7
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r8 = r0
            r8 = r8[r6]
            if (r8 != 0) goto L_0x0293
            r0 = r19
            int[] r0 = r0.e
            r8 = r0
            int r7 = r7 << 8
            r8[r6] = r7
            r0 = r19
            boolean r0 = r0.l
            r7 = r0
            if (r7 == 0) goto L_0x00f2
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r7 = r0
            int r8 = r7.length
            a.a.a.d.f[] r9 = new a.a.a.d.f[r8]
            r0 = r9
            r1 = r19
            r1.f = r0
            r9 = 0
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r10 = r0
            r11 = 0
            java.lang.System.arraycopy(r7, r9, r10, r11, r8)
            r7 = 0
            r0 = r7
            r1 = r19
            r1.l = r0
        L_0x00f2:
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r7 = r0
            r7[r6] = r5
        L_0x00f9:
            r0 = r19
            int[] r0 = r0.e
            r6 = r0
            int r6 = r6.length
            r0 = r19
            int r0 = r0.c
            r7 = r0
            int r8 = r6 >> 1
            if (r7 <= r8) goto L_0x0118
            int r7 = r6 >> 2
            r0 = r19
            int r0 = r0.c
            r8 = r0
            int r6 = r6 - r7
            if (r8 <= r6) goto L_0x0329
            r6 = 1
            r0 = r6
            r1 = r19
            r1.j = r0
        L_0x0118:
            return r5
        L_0x0119:
            a.a.a.d.b r5 = new a.a.a.d.b
            r8 = 0
            r8 = r21[r8]
            r5.<init>(r6, r7, r8)
            goto L_0x0035
        L_0x0123:
            a.a.a.d.c r5 = new a.a.a.d.c
            r8 = 0
            r8 = r21[r8]
            r9 = 1
            r9 = r21[r9]
            r5.<init>(r6, r7, r8, r9)
            goto L_0x0035
        L_0x0130:
            a.a.a.d.d r5 = new a.a.a.d.d
            r8 = 0
            r8 = r21[r8]
            r9 = 1
            r9 = r21[r9]
            r10 = 2
            r10 = r21[r10]
            r5.<init>(r6, r7, r8, r9, r10)
            goto L_0x0035
        L_0x0140:
            r9 = r21[r8]
            r5[r8] = r9
            int r8 = r8 + 1
            goto L_0x0024
        L_0x0148:
            int[] r9 = new int[r8]
            r0 = r9
            r1 = r19
            r1.e = r0
            r9 = 1
            int r9 = r8 - r9
            r0 = r9
            r1 = r19
            r1.d = r0
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r9 = r0
            a.a.a.d.f[] r8 = new a.a.a.d.f[r8]
            r0 = r8
            r1 = r19
            r1.f = r0
            r8 = 0
            r10 = 0
            r18 = r10
            r10 = r8
            r8 = r18
        L_0x016a:
            if (r8 < r6) goto L_0x01c0
            r0 = r19
            int r0 = r0.i
            r6 = r0
            if (r6 == 0) goto L_0x00aa
            r8 = 0
            r0 = r8
            r1 = r19
            r1.h = r0
            r8 = 0
            r0 = r8
            r1 = r19
            r1.i = r0
            r8 = 0
            r0 = r8
            r1 = r19
            r1.m = r0
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r8 = r0
            int r9 = r8.length
            a.a.a.d.e[] r9 = new a.a.a.d.e[r9]
            r0 = r9
            r1 = r19
            r1.g = r0
            r9 = 0
        L_0x0193:
            if (r9 < r6) goto L_0x01e3
            r0 = r19
            int r0 = r0.c
            r6 = r0
            if (r10 == r6) goto L_0x00aa
            java.lang.RuntimeException r5 = new java.lang.RuntimeException
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "Internal error: count after rehash "
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r7 = "; should be "
            java.lang.StringBuilder r6 = r6.append(r7)
            r0 = r19
            int r0 = r0.c
            r7 = r0
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.<init>(r6)
            throw r5
        L_0x01c0:
            r11 = r9[r8]
            if (r11 == 0) goto L_0x01e0
            int r10 = r10 + 1
            int r12 = r11.hashCode()
            r0 = r19
            int r0 = r0.d
            r13 = r0
            r13 = r13 & r12
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r14 = r0
            r14[r13] = r11
            r0 = r19
            int[] r0 = r0.e
            r11 = r0
            int r12 = r12 << 8
            r11[r13] = r12
        L_0x01e0:
            int r8 = r8 + 1
            goto L_0x016a
        L_0x01e3:
            r11 = r8[r9]
            r18 = r11
            r11 = r10
            r10 = r18
        L_0x01ea:
            if (r10 != 0) goto L_0x01f0
            int r9 = r9 + 1
            r10 = r11
            goto L_0x0193
        L_0x01f0:
            int r11 = r11 + 1
            a.a.a.d.f r12 = r10.f24a
            int r13 = r12.hashCode()
            r0 = r19
            int r0 = r0.d
            r14 = r0
            r14 = r14 & r13
            r0 = r19
            int[] r0 = r0.e
            r15 = r0
            r15 = r15[r14]
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r16 = r0
            r16 = r16[r14]
            if (r16 != 0) goto L_0x0222
            r0 = r19
            int[] r0 = r0.e
            r15 = r0
            int r13 = r13 << 8
            r15[r14] = r13
            r0 = r19
            a.a.a.d.f[] r0 = r0.f
            r13 = r0
            r13[r14] = r12
        L_0x021f:
            a.a.a.d.e r10 = r10.b
            goto L_0x01ea
        L_0x0222:
            r0 = r19
            int r0 = r0.h
            r13 = r0
            int r13 = r13 + 1
            r0 = r13
            r1 = r19
            r1.h = r0
            r13 = r15 & 255(0xff, float:3.57E-43)
            if (r13 != 0) goto L_0x0290
            r0 = r19
            int r0 = r0.i
            r13 = r0
            r16 = 254(0xfe, float:3.56E-43)
            r0 = r13
            r1 = r16
            if (r0 > r1) goto L_0x028b
            r0 = r19
            int r0 = r0.i
            r13 = r0
            r0 = r19
            int r0 = r0.i
            r16 = r0
            int r16 = r16 + 1
            r0 = r16
            r1 = r19
            r1.i = r0
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r16 = r0
            r0 = r16
            int r0 = r0.length
            r16 = r0
            r0 = r13
            r1 = r16
            if (r0 < r1) goto L_0x0264
            r19.f()
        L_0x0264:
            r0 = r19
            int[] r0 = r0.e
            r16 = r0
            r15 = r15 & -256(0xffffffffffffff00, float:NaN)
            int r17 = r13 + 1
            r15 = r15 | r17
            r16[r14] = r15
        L_0x0272:
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r14 = r0
            a.a.a.d.e r15 = new a.a.a.d.e
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r16 = r0
            r16 = r16[r13]
            r0 = r15
            r1 = r12
            r2 = r16
            r0.<init>(r1, r2)
            r14[r13] = r15
            goto L_0x021f
        L_0x028b:
            int r13 = r19.e()
            goto L_0x0264
        L_0x0290:
            int r13 = r13 + -1
            goto L_0x0272
        L_0x0293:
            r0 = r19
            boolean r0 = r0.m
            r7 = r0
            if (r7 == 0) goto L_0x02b0
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r7 = r0
            if (r7 != 0) goto L_0x030e
            r7 = 32
            a.a.a.d.e[] r7 = new a.a.a.d.e[r7]
            r0 = r7
            r1 = r19
            r1.g = r0
        L_0x02aa:
            r7 = 0
            r0 = r7
            r1 = r19
            r1.m = r0
        L_0x02b0:
            r0 = r19
            int r0 = r0.h
            r7 = r0
            int r7 = r7 + 1
            r0 = r7
            r1 = r19
            r1.h = r0
            r0 = r19
            int[] r0 = r0.e
            r7 = r0
            r7 = r7[r6]
            r8 = r7 & 255(0xff, float:3.57E-43)
            if (r8 != 0) goto L_0x0326
            r0 = r19
            int r0 = r0.i
            r8 = r0
            r9 = 254(0xfe, float:3.56E-43)
            if (r8 > r9) goto L_0x0321
            r0 = r19
            int r0 = r0.i
            r8 = r0
            r0 = r19
            int r0 = r0.i
            r9 = r0
            int r9 = r9 + 1
            r0 = r9
            r1 = r19
            r1.i = r0
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r9 = r0
            int r9 = r9.length
            if (r8 < r9) goto L_0x02ec
            r19.f()
        L_0x02ec:
            r0 = r19
            int[] r0 = r0.e
            r9 = r0
            r7 = r7 & -256(0xffffffffffffff00, float:NaN)
            int r10 = r8 + 1
            r7 = r7 | r10
            r9[r6] = r7
            r6 = r8
        L_0x02f9:
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r7 = r0
            a.a.a.d.e r8 = new a.a.a.d.e
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r9 = r0
            r9 = r9[r6]
            r8.<init>(r5, r9)
            r7[r6] = r8
            goto L_0x00f9
        L_0x030e:
            int r8 = r7.length
            a.a.a.d.e[] r9 = new a.a.a.d.e[r8]
            r0 = r9
            r1 = r19
            r1.g = r0
            r9 = 0
            r0 = r19
            a.a.a.d.e[] r0 = r0.g
            r10 = r0
            r11 = 0
            java.lang.System.arraycopy(r7, r9, r10, r11, r8)
            goto L_0x02aa
        L_0x0321:
            int r8 = r19.e()
            goto L_0x02ec
        L_0x0326:
            int r6 = r8 + -1
            goto L_0x02f9
        L_0x0329:
            r0 = r19
            int r0 = r0.h
            r6 = r0
            if (r6 < r7) goto L_0x0118
            r6 = 1
            r0 = r6
            r1 = r19
            r1.j = r0
            goto L_0x0118
        L_0x0338:
            r6 = r20
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.d.h.a(java.lang.String, int[], int):a.a.a.d.f");
    }

    public final f a(int[] iArr, int i2) {
        e eVar;
        int b2 = b(iArr, i2);
        int i3 = this.d & b2;
        int i4 = this.e[i3];
        if ((((i4 >> 8) ^ b2) << 8) == 0) {
            f fVar = this.f[i3];
            if (fVar == null || fVar.a(iArr, i2)) {
                return fVar;
            }
        } else if (i4 == 0) {
            return null;
        }
        int i5 = i4 & 255;
        if (i5 <= 0 || (eVar = this.g[i5 - 1]) == null) {
            return null;
        }
        if (eVar.f24a.hashCode() == b2 && eVar.f24a.a(iArr, i2)) {
            return eVar.f24a;
        }
        for (e eVar2 = eVar.b; eVar2 != null; eVar2 = eVar2.b) {
            f fVar2 = eVar2.f24a;
            if (fVar2.hashCode() == b2 && fVar2.a(iArr, i2)) {
                return fVar2;
            }
        }
        return null;
    }

    public final synchronized h a(boolean z) {
        return new h(this, z);
    }

    public final void b() {
        if ((!this.k) && this.f27a != null) {
            this.f27a.a(this);
            this.k = true;
            this.l = true;
            this.m = true;
        }
    }
}
