package a.a.a.c;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public final class b extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private a f15a;
    private OutputStream b;
    private byte[] c;
    private int d;
    private int e;
    private int f = 0;

    public b(a aVar, OutputStream outputStream) {
        this.f15a = aVar;
        this.b = outputStream;
        this.c = aVar.f();
        this.d = this.c.length - 4;
        this.e = 0;
    }

    private int a(int i) {
        int i2 = this.f;
        this.f = 0;
        if (i >= 56320 && i <= 57343) {
            return ((i2 - 55296) << 10) + 65536 + (i - 56320);
        }
        throw new IOException("Broken surrogate pair: first char 0x" + Integer.toHexString(i2) + ", second 0x" + Integer.toHexString(i) + "; illegal combination");
    }

    private static void b(int i) {
        if (i > 1114111) {
            throw new IOException("Illegal character point (0x" + Integer.toHexString(i) + ") to output; max is 0x10FFFF as per RFC 4627");
        } else if (i < 55296) {
            throw new IOException("Illegal character point (0x" + Integer.toHexString(i) + ") to output");
        } else if (i <= 56319) {
            throw new IOException("Unmatched first part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        } else {
            throw new IOException("Unmatched second part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        }
    }

    public final Writer append(char c2) {
        write(c2);
        return this;
    }

    public final void close() {
        if (this.b != null) {
            if (this.e > 0) {
                this.b.write(this.c, 0, this.e);
                this.e = 0;
            }
            OutputStream outputStream = this.b;
            this.b = null;
            byte[] bArr = this.c;
            if (bArr != null) {
                this.c = null;
                this.f15a.b(bArr);
            }
            outputStream.close();
            int i = this.f;
            this.f = 0;
            if (i > 0) {
                b(i);
            }
        }
    }

    public final void flush() {
        if (this.e > 0) {
            this.b.write(this.c, 0, this.e);
            this.e = 0;
        }
        this.b.flush();
    }

    public final void write(int i) {
        int i2;
        int i3;
        if (this.f > 0) {
            i2 = a(i);
        } else if (i < 55296 || i > 57343) {
            i2 = i;
        } else {
            if (i > 56319) {
                b(i);
            }
            this.f = i;
            return;
        }
        if (this.e >= this.d) {
            this.b.write(this.c, 0, this.e);
            this.e = 0;
        }
        if (i2 < 128) {
            byte[] bArr = this.c;
            int i4 = this.e;
            this.e = i4 + 1;
            bArr[i4] = (byte) i2;
            return;
        }
        int i5 = this.e;
        if (i2 < 2048) {
            int i6 = i5 + 1;
            this.c[i5] = (byte) ((i2 >> 6) | 192);
            this.c[i6] = (byte) ((i2 & 63) | 128);
            i3 = i6 + 1;
        } else if (i2 <= 65535) {
            int i7 = i5 + 1;
            this.c[i5] = (byte) ((i2 >> 12) | 224);
            int i8 = i7 + 1;
            this.c[i7] = (byte) (((i2 >> 6) & 63) | 128);
            this.c[i8] = (byte) ((i2 & 63) | 128);
            i3 = i8 + 1;
        } else {
            if (i2 > 1114111) {
                b(i2);
            }
            int i9 = i5 + 1;
            this.c[i5] = (byte) ((i2 >> 18) | 240);
            int i10 = i9 + 1;
            this.c[i9] = (byte) (((i2 >> 12) & 63) | 128);
            int i11 = i10 + 1;
            this.c[i10] = (byte) (((i2 >> 6) & 63) | 128);
            this.c[i11] = (byte) ((i2 & 63) | 128);
            i3 = i11 + 1;
        }
        this.e = i3;
    }

    public final void write(String str) {
        write(str, 0, str.length());
    }

    public final void write(String str, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        char c2;
        if (i2 >= 2) {
            if (this.f > 0) {
                write(a(str.charAt(i)));
                i4 = i + 1;
                i3 = i2 - 1;
            } else {
                i3 = i2;
                i4 = i;
            }
            int i7 = this.e;
            byte[] bArr = this.c;
            int i8 = this.d;
            int i9 = i3 + i4;
            int i10 = i7;
            int i11 = i4;
            int i12 = i10;
            while (true) {
                if (i11 < i9) {
                    if (i12 >= i8) {
                        this.b.write(bArr, 0, i12);
                        i12 = 0;
                    }
                    int i13 = i11 + 1;
                    char charAt = str.charAt(i11);
                    if (charAt < 128) {
                        int i14 = i12 + 1;
                        bArr[i12] = (byte) charAt;
                        int i15 = i9 - i13;
                        int i16 = i8 - i14;
                        if (i15 > i16) {
                            i15 = i16;
                        }
                        int i17 = i15 + i13;
                        i6 = i14;
                        while (i13 < i17) {
                            int i18 = i13 + 1;
                            char charAt2 = str.charAt(i13);
                            if (charAt2 < 128) {
                                bArr[i6] = (byte) charAt2;
                                i6++;
                                i13 = i18;
                            } else {
                                c2 = charAt2;
                                i13 = i18;
                            }
                        }
                        i12 = i6;
                        i11 = i13;
                    } else {
                        char c3 = charAt;
                        i6 = i12;
                        c2 = c3;
                    }
                    if (c2 >= 2048) {
                        if (c2 >= 55296 && c2 <= 57343) {
                            if (c2 > 56319) {
                                this.e = i6;
                                b(c2);
                            }
                            this.f = c2;
                            if (i13 >= i9) {
                                i5 = i6;
                                break;
                            }
                            int i19 = i13 + 1;
                            int a2 = a(str.charAt(i13));
                            if (a2 > 1114111) {
                                this.e = i6;
                                b(a2);
                            }
                            int i20 = i6 + 1;
                            bArr[i6] = (byte) ((a2 >> 18) | 240);
                            int i21 = i20 + 1;
                            bArr[i20] = (byte) (((a2 >> 12) & 63) | 128);
                            int i22 = i21 + 1;
                            bArr[i21] = (byte) (((a2 >> 6) & 63) | 128);
                            bArr[i22] = (byte) ((a2 & 63) | 128);
                            i11 = i19;
                            i12 = i22 + 1;
                        } else {
                            int i23 = i6 + 1;
                            bArr[i6] = (byte) ((c2 >> 12) | 224);
                            int i24 = i23 + 1;
                            bArr[i23] = (byte) (((c2 >> 6) & 63) | 128);
                            bArr[i24] = (byte) ((c2 & '?') | 128);
                            i12 = i24 + 1;
                            i11 = i13;
                        }
                    } else {
                        int i25 = i6 + 1;
                        bArr[i6] = (byte) ((c2 >> 6) | 192);
                        bArr[i25] = (byte) ((c2 & '?') | 128);
                        i12 = i25 + 1;
                        i11 = i13;
                    }
                } else {
                    i5 = i12;
                    break;
                }
            }
            this.e = i5;
        } else if (i2 == 1) {
            write(str.charAt(i));
        }
    }

    public final void write(char[] cArr) {
        write(cArr, 0, cArr.length);
    }

    public final void write(char[] cArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        char c2;
        if (i2 >= 2) {
            if (this.f > 0) {
                write(a(cArr[i]));
                i4 = i + 1;
                i3 = i2 - 1;
            } else {
                i3 = i2;
                i4 = i;
            }
            int i7 = this.e;
            byte[] bArr = this.c;
            int i8 = this.d;
            int i9 = i3 + i4;
            int i10 = i7;
            int i11 = i4;
            int i12 = i10;
            while (true) {
                if (i11 < i9) {
                    if (i12 >= i8) {
                        this.b.write(bArr, 0, i12);
                        i12 = 0;
                    }
                    int i13 = i11 + 1;
                    char c3 = cArr[i11];
                    if (c3 < 128) {
                        int i14 = i12 + 1;
                        bArr[i12] = (byte) c3;
                        int i15 = i9 - i13;
                        int i16 = i8 - i14;
                        if (i15 > i16) {
                            i15 = i16;
                        }
                        int i17 = i15 + i13;
                        i6 = i14;
                        while (i13 < i17) {
                            int i18 = i13 + 1;
                            char c4 = cArr[i13];
                            if (c4 < 128) {
                                bArr[i6] = (byte) c4;
                                i6++;
                                i13 = i18;
                            } else {
                                c2 = c4;
                                i13 = i18;
                            }
                        }
                        i12 = i6;
                        i11 = i13;
                    } else {
                        char c5 = c3;
                        i6 = i12;
                        c2 = c5;
                    }
                    if (c2 >= 2048) {
                        if (c2 >= 55296 && c2 <= 57343) {
                            if (c2 > 56319) {
                                this.e = i6;
                                b(c2);
                            }
                            this.f = c2;
                            if (i13 >= i9) {
                                i5 = i6;
                                break;
                            }
                            int i19 = i13 + 1;
                            int a2 = a(cArr[i13]);
                            if (a2 > 1114111) {
                                this.e = i6;
                                b(a2);
                            }
                            int i20 = i6 + 1;
                            bArr[i6] = (byte) ((a2 >> 18) | 240);
                            int i21 = i20 + 1;
                            bArr[i20] = (byte) (((a2 >> 12) & 63) | 128);
                            int i22 = i21 + 1;
                            bArr[i21] = (byte) (((a2 >> 6) & 63) | 128);
                            bArr[i22] = (byte) ((a2 & 63) | 128);
                            i11 = i19;
                            i12 = i22 + 1;
                        } else {
                            int i23 = i6 + 1;
                            bArr[i6] = (byte) ((c2 >> 12) | 224);
                            int i24 = i23 + 1;
                            bArr[i23] = (byte) (((c2 >> 6) & 63) | 128);
                            bArr[i24] = (byte) ((c2 & '?') | 128);
                            i12 = i24 + 1;
                            i11 = i13;
                        }
                    } else {
                        int i25 = i6 + 1;
                        bArr[i6] = (byte) ((c2 >> 6) | 192);
                        bArr[i25] = (byte) ((c2 & '?') | 128);
                        i12 = i25 + 1;
                        i11 = i13;
                    }
                } else {
                    i5 = i12;
                    break;
                }
            }
            this.e = i5;
        } else if (i2 == 1) {
            write(cArr[i]);
        }
    }
}
