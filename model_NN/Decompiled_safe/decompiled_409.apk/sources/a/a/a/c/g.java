package a.a.a.c;

import java.io.CharConversionException;
import java.io.InputStream;

public final class g extends c {
    private boolean e;
    private char f = 0;
    private int g = 0;
    private int h = 0;

    public g(a aVar, InputStream inputStream, byte[] bArr, int i, int i2, boolean z) {
        super(aVar, inputStream, bArr, i, i2);
        this.e = z;
    }

    private boolean a(int i) {
        this.h += this.d - i;
        if (i > 0) {
            if (this.c > 0) {
                for (int i2 = 0; i2 < i; i2++) {
                    this.b[i2] = this.b[this.c + i2];
                }
                this.c = 0;
            }
            this.d = i;
        } else {
            this.c = 0;
            int read = this.f16a.read(this.b);
            if (read <= 0) {
                this.d = 0;
                if (read < 0) {
                    a();
                    return false;
                }
                b();
            }
            this.d = read;
        }
        while (this.d < 4) {
            int read2 = this.f16a.read(this.b, this.d, this.b.length - this.d);
            if (read2 <= 0) {
                if (read2 < 0) {
                    a();
                    int i3 = this.d;
                    throw new CharConversionException("Unexpected EOF in the middle of a 4-byte UTF-32 char: got " + i3 + ", needed " + 4 + ", at char #" + this.g + ", byte #" + (this.h + i3) + ")");
                }
                b();
            }
            this.d = read2 + this.d;
        }
        return true;
    }

    public final /* bridge */ /* synthetic */ void close() {
        super.close();
    }

    public final /* bridge */ /* synthetic */ int read() {
        return super.read();
    }

    public final int read(char[] cArr, int i, int i2) {
        int i3;
        int i4;
        byte b;
        int i5;
        byte b2;
        if (this.b == null) {
            return -1;
        }
        if (i2 <= 0) {
            return i2;
        }
        if (i < 0 || i + i2 > cArr.length) {
            throw new ArrayIndexOutOfBoundsException("read(buf," + i + "," + i2 + "), cbuf[" + cArr.length + "]");
        }
        int i6 = i2 + i;
        if (this.f != 0) {
            i3 = i + 1;
            cArr[i] = this.f;
            this.f = 0;
        } else {
            int i7 = this.d - this.c;
            if (i7 < 4 && !a(i7)) {
                return -1;
            }
            i3 = i;
        }
        while (true) {
            if (i3 < i6) {
                int i8 = this.c;
                if (this.e) {
                    b = (this.b[i8 + 3] & 255) | (this.b[i8] << 24) | ((this.b[i8 + 1] & 255) << 16) | ((this.b[i8 + 2] & 255) << 8);
                } else {
                    b = (this.b[i8 + 3] << 24) | (this.b[i8] & 255) | ((this.b[i8 + 1] & 255) << 8) | ((this.b[i8 + 2] & 255) << 16);
                }
                this.c += 4;
                if (b <= 65535) {
                    byte b3 = b;
                    i5 = i3;
                    b2 = b3;
                } else if (b > 1114111) {
                    int i9 = i3 - i;
                    throw new CharConversionException("Invalid UTF-32 character 0x" + Integer.toHexString(b) + ("(above " + Integer.toHexString(1114111) + ") ") + " at char #" + (i9 + this.g) + ", byte #" + ((this.h + this.c) - 1) + ")");
                } else {
                    int i10 = b - 65536;
                    int i11 = i3 + 1;
                    cArr[i3] = (char) (55296 + (i10 >> 10));
                    b2 = 56320 | (i10 & 1023);
                    if (i11 >= i6) {
                        this.f = (char) b2;
                        i4 = i11;
                        break;
                    }
                    i5 = i11;
                }
                int i12 = i5 + 1;
                cArr[i5] = (char) b2;
                if (this.c >= this.d) {
                    i4 = i12;
                    break;
                }
                i3 = i12;
            } else {
                i4 = i3;
                break;
            }
        }
        int i13 = i4 - i;
        this.g += i13;
        return i13;
    }
}
