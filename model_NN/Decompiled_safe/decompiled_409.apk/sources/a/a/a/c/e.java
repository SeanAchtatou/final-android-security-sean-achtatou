package a.a.a.c;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static String f18a = String.valueOf(Long.MIN_VALUE).substring(1);
    private static String b = String.valueOf(Long.MAX_VALUE);

    public static final int a(char[] cArr, int i, int i2) {
        int i3 = cArr[i] - '0';
        int i4 = i2 + i;
        int i5 = i + 1;
        if (i5 >= i4) {
            return i3;
        }
        int i6 = (i3 * 10) + (cArr[i5] - '0');
        int i7 = i5 + 1;
        if (i7 >= i4) {
            return i6;
        }
        int i8 = (i6 * 10) + (cArr[i7] - '0');
        int i9 = i7 + 1;
        if (i9 >= i4) {
            return i8;
        }
        int i10 = (i8 * 10) + (cArr[i9] - '0');
        int i11 = i9 + 1;
        if (i11 >= i4) {
            return i10;
        }
        int i12 = (i10 * 10) + (cArr[i11] - '0');
        int i13 = i11 + 1;
        if (i13 >= i4) {
            return i12;
        }
        int i14 = (i12 * 10) + (cArr[i13] - '0');
        int i15 = i13 + 1;
        if (i15 >= i4) {
            return i14;
        }
        int i16 = (i14 * 10) + (cArr[i15] - '0');
        int i17 = i15 + 1;
        if (i17 >= i4) {
            return i16;
        }
        int i18 = (i16 * 10) + (cArr[i17] - '0');
        int i19 = i17 + 1;
        return i19 < i4 ? (i18 * 10) + (cArr[i19] - '0') : i18;
    }

    public static final boolean a(char[] cArr, int i, int i2, boolean z) {
        String str = z ? f18a : b;
        int length = str.length();
        if (i2 < length) {
            return true;
        }
        if (i2 > length) {
            return false;
        }
        for (int i3 = 0; i3 < length; i3++) {
            if (cArr[i + i3] > str.charAt(i3)) {
                return false;
            }
        }
        return true;
    }

    public static final long b(char[] cArr, int i, int i2) {
        int i3 = i2 - 9;
        return (((long) a(cArr, i, i3)) * 1000000000) + ((long) a(cArr, i3 + i, 9));
    }
}
