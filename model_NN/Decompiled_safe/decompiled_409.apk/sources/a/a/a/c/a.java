package a.a.a.c;

import a.a.a.a.d;
import a.a.a.a.e;
import a.a.a.a.f;
import a.a.a.a.g;
import a.a.a.b;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private Object f14a;
    private b b;
    private boolean c;
    private e d;
    private byte[] e = null;
    private byte[] f = null;
    private char[] g = null;
    private char[] h = null;
    private char[] i = null;

    public a(e eVar, Object obj, boolean z) {
        this.d = eVar;
        this.f14a = obj;
        this.c = z;
    }

    public final Object a() {
        return this.f14a;
    }

    public final void a(b bVar) {
        this.b = bVar;
    }

    public final void a(byte[] bArr) {
        if (bArr == null) {
            return;
        }
        if (bArr != this.e) {
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
        this.e = null;
        this.d.a(d.READ_IO_BUFFER, bArr);
    }

    public final void a(char[] cArr) {
        if (cArr == null) {
            return;
        }
        if (cArr != this.g) {
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
        this.g = null;
        this.d.a(g.TOKEN_BUFFER, cArr);
    }

    public final b b() {
        return this.b;
    }

    public final void b(byte[] bArr) {
        if (bArr == null) {
            return;
        }
        if (bArr != this.f) {
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
        this.f = null;
        this.d.a(d.WRITE_ENCODING_BUFFER, bArr);
    }

    public final void b(char[] cArr) {
        if (cArr == null) {
            return;
        }
        if (cArr != this.h) {
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
        this.h = null;
        this.d.a(g.CONCAT_BUFFER, cArr);
    }

    public final void c(char[] cArr) {
        if (cArr == null) {
            return;
        }
        if (cArr != this.i) {
            throw new IllegalArgumentException("Trying to release buffer not owned by the context");
        }
        this.i = null;
        this.d.a(g.NAME_COPY_BUFFER, cArr);
    }

    public final boolean c() {
        return this.c;
    }

    public final f d() {
        return new f(this.d);
    }

    public final byte[] e() {
        if (this.e != null) {
            throw new IllegalStateException("Trying to call allocReadIOBuffer() second time");
        }
        this.e = this.d.a(d.READ_IO_BUFFER);
        return this.e;
    }

    public final byte[] f() {
        if (this.f != null) {
            throw new IllegalStateException("Trying to call allocWriteEncodingBuffer() second time");
        }
        this.f = this.d.a(d.WRITE_ENCODING_BUFFER);
        return this.f;
    }

    public final char[] g() {
        if (this.g != null) {
            throw new IllegalStateException("Trying to call allocTokenBuffer() second time");
        }
        this.g = this.d.a(g.TOKEN_BUFFER);
        return this.g;
    }

    public final char[] h() {
        if (this.h != null) {
            throw new IllegalStateException("Trying to call allocConcatBuffer() second time");
        }
        this.h = this.d.a(g.CONCAT_BUFFER);
        return this.h;
    }
}
