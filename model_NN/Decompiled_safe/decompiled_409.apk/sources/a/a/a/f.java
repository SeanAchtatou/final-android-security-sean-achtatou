package a.a.a;

import java.io.Closeable;

public abstract class f implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    protected i f30a;

    protected f() {
    }

    public abstract void a();

    public abstract void a(double d);

    public abstract void a(float f);

    public abstract void a(int i);

    public abstract void a(long j);

    public abstract void a(String str);

    public abstract void a(boolean z);

    public abstract void b();

    public abstract void b(String str);

    public abstract void c();

    public abstract void close();

    public abstract void d();
}
