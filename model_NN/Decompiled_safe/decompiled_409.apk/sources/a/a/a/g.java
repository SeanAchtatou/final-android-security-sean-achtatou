package a.a.a;

import java.io.Serializable;

public final class g implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static g f31a = new g("N/A", -1, -1, -1);
    private long b;
    private long c;
    private int d;
    private int e;
    private Object f;

    private g(Object obj, long j, int i, int i2) {
        this.f = obj;
        this.b = -1;
        this.c = j;
        this.d = i;
        this.e = i2;
    }

    public g(Object obj, long j, int i, int i2, byte b2) {
        this(obj, j, i, i2);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        if (this.f == null) {
            if (gVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(gVar.f)) {
            return false;
        }
        return this.d == gVar.d && this.e == gVar.e && this.c == gVar.c && this.b == gVar.b;
    }

    public final int hashCode() {
        return ((((this.f == null ? 1 : this.f.hashCode()) ^ this.d) + this.e) ^ ((int) this.c)) + ((int) this.b);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(80);
        sb.append("[Source: ");
        if (this.f == null) {
            sb.append("UNKNOWN");
        } else {
            sb.append(this.f.toString());
        }
        sb.append("; line: ");
        sb.append(this.d);
        sb.append(", column: ");
        sb.append(this.e);
        sb.append(']');
        return sb.toString();
    }
}
