package a.a.a;

public abstract class l {

    /* renamed from: a  reason: collision with root package name */
    protected int f34a;
    protected int b = -1;

    public l(int i) {
        this.f34a = i;
    }

    public final boolean b() {
        return this.f34a == 1;
    }

    public final boolean c() {
        return this.f34a == 0;
    }

    public final boolean d() {
        return this.f34a == 2;
    }

    public final String e() {
        switch (this.f34a) {
            case 0:
                return "ROOT";
            case 1:
                return "ARRAY";
            case 2:
                return "OBJECT";
            default:
                return "?";
        }
    }

    public final int f() {
        if (this.b < 0) {
            return 0;
        }
        return this.b;
    }
}
