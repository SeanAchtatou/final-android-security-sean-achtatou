package hamon05.speed.pac;

import android.app.Application;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import java.util.HashMap;
import java.util.List;

public class OpenFeintApplication extends Application {

    /* renamed from: a  reason: collision with root package name */
    public static List f177a = null;
    public static List b = null;

    public void onCreate() {
        super.onCreate();
        HashMap hashMap = new HashMap();
        hashMap.put("SettingCloudStorageCompressionStrategy", "CloudStorageCompressionStrategyDefault");
        hashMap.put("RequestedOrientation", 1);
        OpenFeint.initialize(this, new OpenFeintSettings("SPEED", "TULIdlw3f6PDgUgrEPo3Pg", "tEyQ53jICcRhgBxau09s8v3SISCK29J78nRuUtSASo", "332893", hashMap), new OpenFeintDelegate() {
        });
        Achievement.list(new Achievement.ListCB() {
            public void onSuccess(List list) {
                OpenFeintApplication.f177a = list;
            }
        });
        Leaderboard.list(new Leaderboard.ListCB() {
            public void onSuccess(List list) {
                OpenFeintApplication.b = list;
            }
        });
    }
}
