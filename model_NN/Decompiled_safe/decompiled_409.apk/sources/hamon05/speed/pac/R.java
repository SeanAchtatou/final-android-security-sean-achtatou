package hamon05.speed.pac;

public final class R {

    public final class attr {
        /* added by JADX */
        public static final int testing = 2130771968;
        /* added by JADX */
        public static final int backgroundColor = 2130771969;
        /* added by JADX */
        public static final int textColor = 2130771970;
        /* added by JADX */
        public static final int keywords = 2130771971;
        /* added by JADX */
        public static final int refreshInterval = 2130771972;
        /* added by JADX */
        public static final int isGoneWithoutAd = 2130771973;
    }

    public final class color {
        /* added by JADX */
        public static final int of_transparent = 2130968576;
    }

    public final class drawable {
        /* added by JADX */
        public static final int bg = 2130837504;
        /* added by JADX */
        public static final int card_eff = 2130837505;
        /* added by JADX */
        public static final int card_nokori = 2130837506;
        /* added by JADX */
        public static final int card_s01 = 2130837507;
        /* added by JADX */
        public static final int frame_layout_shape = 2130837508;
        /* added by JADX */
        public static final int game_message = 2130837509;
        /* added by JADX */
        public static final int howto = 2130837510;
        /* added by JADX */
        public static final int icon = 2130837511;
        /* added by JADX */
        public static final int menu_message = 2130837512;
        /* added by JADX */
        public static final int miss = 2130837513;
        /* added by JADX */
        public static final int of_achievement_icon_frame = 2130837514;
        /* added by JADX */
        public static final int of_achievement_icon_locked = 2130837515;
        /* added by JADX */
        public static final int of_achievement_icon_unlocked = 2130837516;
        /* added by JADX */
        public static final int of_achievement_notification_bkg = 2130837517;
        /* added by JADX */
        public static final int of_achievement_notification_locked = 2130837518;
        /* added by JADX */
        public static final int of_feint_points_white = 2130837519;
        /* added by JADX */
        public static final int of_icon_dashboard_exit = 2130837520;
        /* added by JADX */
        public static final int of_icon_dashboard_home = 2130837521;
        /* added by JADX */
        public static final int of_icon_dashboard_settings = 2130837522;
        /* added by JADX */
        public static final int of_icon_highscore_notification = 2130837523;
        /* added by JADX */
        public static final int of_ll_logo = 2130837524;
        /* added by JADX */
        public static final int of_native_loader = 2130837525;
        /* added by JADX */
        public static final int of_native_loader_frame = 2130837526;
        /* added by JADX */
        public static final int of_native_loader_leaf = 2130837527;
        /* added by JADX */
        public static final int of_native_loader_progress = 2130837528;
        /* added by JADX */
        public static final int of_native_loader_progress_01 = 2130837529;
        /* added by JADX */
        public static final int of_native_loader_progress_02 = 2130837530;
        /* added by JADX */
        public static final int of_native_loader_progress_03 = 2130837531;
        /* added by JADX */
        public static final int of_native_loader_progress_04 = 2130837532;
        /* added by JADX */
        public static final int of_native_loader_progress_05 = 2130837533;
        /* added by JADX */
        public static final int of_native_loader_progress_06 = 2130837534;
        /* added by JADX */
        public static final int of_native_loader_progress_07 = 2130837535;
        /* added by JADX */
        public static final int of_native_loader_progress_08 = 2130837536;
        /* added by JADX */
        public static final int of_native_loader_progress_09 = 2130837537;
        /* added by JADX */
        public static final int of_native_loader_progress_10 = 2130837538;
        /* added by JADX */
        public static final int of_native_loader_progress_11 = 2130837539;
        /* added by JADX */
        public static final int of_native_loader_progress_12 = 2130837540;
        /* added by JADX */
        public static final int of_notification_bkg = 2130837541;
        /* added by JADX */
        public static final int player = 2130837542;
        /* added by JADX */
        public static final int title = 2130837543;
        /* added by JADX */
        public static final int touch = 2130837544;
    }

    public final class id {
        /* added by JADX */
        public static final int linearLayoutMain = 2131230720;
        /* added by JADX */
        public static final int linearLayoutAd = 2131230721;
        /* added by JADX */
        public static final int of_achievement_notification = 2131230722;
        /* added by JADX */
        public static final int of_achievement_icon = 2131230723;
        /* added by JADX */
        public static final int of_achievement_icon_frame = 2131230724;
        /* added by JADX */
        public static final int of_achievement_text = 2131230725;
        /* added by JADX */
        public static final int of_achievement_score = 2131230726;
        /* added by JADX */
        public static final int of_achievement_score_icon = 2131230727;
        /* added by JADX */
        public static final int of_achievement_progress_icon = 2131230728;
        /* added by JADX */
        public static final int progress = 2131230729;
        /* added by JADX */
        public static final int nested_window_root = 2131230730;
        /* added by JADX */
        public static final int frameLayout = 2131230731;
        /* added by JADX */
        public static final int web_view = 2131230732;
        /* added by JADX */
        public static final int of_ll_logo_image = 2131230733;
        /* added by JADX */
        public static final int of_icon = 2131230734;
        /* added by JADX */
        public static final int of_text = 2131230735;
        /* added by JADX */
        public static final int of_text1 = 2131230736;
        /* added by JADX */
        public static final int of_text2 = 2131230737;
        /* added by JADX */
        public static final int home = 2131230738;
        /* added by JADX */
        public static final int settings = 2131230739;
        /* added by JADX */
        public static final int exit_feint = 2131230740;
    }

    public final class layout {
        /* added by JADX */
        public static final int main = 2130903040;
        /* added by JADX */
        public static final int of_achievement_notification = 2130903041;
        /* added by JADX */
        public static final int of_native_loader = 2130903042;
        /* added by JADX */
        public static final int of_nested_window = 2130903043;
        /* added by JADX */
        public static final int of_simple_notification = 2130903044;
        /* added by JADX */
        public static final int of_two_line_notification = 2130903045;
    }

    public final class menu {
        /* added by JADX */
        public static final int of_dashboard = 2131165184;
    }

    public final class string {
        /* added by JADX */
        public static final int of_key_cannot_be_null = 2131034112;
        /* added by JADX */
        public static final int of_secret_cannot_be_null = 2131034113;
        /* added by JADX */
        public static final int of_id_cannot_be_null = 2131034114;
        /* added by JADX */
        public static final int of_name_cannot_be_null = 2131034115;
        /* added by JADX */
        public static final int of_unexpected_response_format = 2131034116;
        /* added by JADX */
        public static final int of_unknown_server_error = 2131034117;
        /* added by JADX */
        public static final int of_null_icon_url = 2131034118;
        /* added by JADX */
        public static final int of_achievement_unlock_null = 2131034119;
        /* added by JADX */
        public static final int of_achievement_load_null = 2131034120;
        /* added by JADX */
        public static final int of_profile_url_null = 2131034121;
        /* added by JADX */
        public static final int of_low_memory_profile_pic = 2131034122;
        /* added by JADX */
        public static final int of_profile_picture_download_failed = 2131034123;
        /* added by JADX */
        public static final int of_nodisk = 2131034124;
        /* added by JADX */
        public static final int of_sdcard = 2131034125;
        /* added by JADX */
        public static final int of_device = 2131034126;
        /* added by JADX */
        public static final int of_loading_feint = 2131034127;
        /* added by JADX */
        public static final int of_no = 2131034128;
        /* added by JADX */
        public static final int of_yes = 2131034129;
        /* added by JADX */
        public static final int of_ok = 2131034130;
        /* added by JADX */
        public static final int of_cancel = 2131034131;
        /* added by JADX */
        public static final int of_io_exception_on_download = 2131034132;
        /* added by JADX */
        public static final int of_cant_compress_blob = 2131034133;
        /* added by JADX */
        public static final int of_no_blob = 2131034134;
        /* added by JADX */
        public static final int of_achievement_unlocked = 2131034135;
        /* added by JADX */
        public static final int of_timeout = 2131034136;
        /* added by JADX */
        public static final int of_bitmap_decode_error = 2131034137;
        /* added by JADX */
        public static final int of_file_not_found = 2131034138;
        /* added by JADX */
        public static final int of_error_parsing_error_message = 2131034139;
        /* added by JADX */
        public static final int of_server_error_code_format = 2131034140;
        /* added by JADX */
        public static final int of_ioexception_reading_body = 2131034141;
        /* added by JADX */
        public static final int of_switched_accounts = 2131034142;
        /* added by JADX */
        public static final int of_now_logged_in_as_format = 2131034143;
        /* added by JADX */
        public static final int of_profile_pic_changed = 2131034144;
        /* added by JADX */
        public static final int of_crash_report_query = 2131034145;
        /* added by JADX */
        public static final int of_no_video = 2131034146;
        /* added by JADX */
        public static final int of_home = 2131034147;
        /* added by JADX */
        public static final int of_banned_dialog = 2131034148;
        /* added by JADX */
        public static final int of_settings = 2131034149;
        /* added by JADX */
        public static final int of_exit_feint = 2131034150;
        /* added by JADX */
        public static final int of_offline_notification = 2131034151;
        /* added by JADX */
        public static final int of_offline_notification_line2 = 2131034152;
        /* added by JADX */
        public static final int of_score_submitted_notification = 2131034153;
        /* added by JADX */
        public static final int of_malformed_request_error = 2131034154;
        /* added by JADX */
        public static final int app_name = 2131034155;
    }

    public final class style {
        /* added by JADX */
        public static final int OFLoading = 2131099648;
        /* added by JADX */
        public static final int OFNestedWindow = 2131099649;
    }

    public final class styleable {

        /* renamed from: a  reason: collision with root package name */
        private static int[] f181a = {R.attr.testing, R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval, R.attr.isGoneWithoutAd};
    }
}
