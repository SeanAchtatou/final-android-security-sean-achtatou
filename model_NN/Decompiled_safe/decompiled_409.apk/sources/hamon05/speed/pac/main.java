package hamon05.speed.pac;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import java.lang.Thread;
import java.util.Random;

public class main extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private AdView f182a;
    private HelloView b = null;
    private LinearLayout c;
    private SharedPreferences d;

    public class HelloView extends SurfaceView implements SurfaceHolder.Callback {
        /* access modifiers changed from: private */
        public Sprite[] A = new Sprite[10];
        /* access modifiers changed from: private */
        public Sprite[] B = new Sprite[2];
        /* access modifiers changed from: private */
        public Sprite[] C = new Sprite[2];
        /* access modifiers changed from: private */
        public Sprite[] D = new Sprite[2];
        /* access modifiers changed from: private */
        public Sprite E = new Sprite();
        /* access modifiers changed from: private */
        public Sprite[] F = new Sprite[4];
        /* access modifiers changed from: private */
        public Sprite G = new Sprite();
        /* access modifiers changed from: private */
        public Sprite[] H = new Sprite[5];
        /* access modifiers changed from: private */
        public Sprite I = new Sprite();
        /* access modifiers changed from: private */
        public Sprite[] J = new Sprite[4];
        /* access modifiers changed from: private */
        public int K;
        /* access modifiers changed from: private */
        public int L;
        private HelloThread M;
        /* access modifiers changed from: private */
        public Context N;
        /* access modifiers changed from: private */
        public Handler O;

        /* renamed from: a  reason: collision with root package name */
        SharedPreferences f183a;
        SharedPreferences.Editor b;
        Matrix c = new Matrix();
        int[] d;
        int[] e;
        int[] f;
        final float g;
        final float h;
        private Leaderboard i = new Leaderboard("848996");
        private Leaderboard j = new Leaderboard("849006");
        private Leaderboard k = new Leaderboard("849036");
        private Leaderboard l = new Leaderboard("849056");
        /* access modifiers changed from: private */
        public Random m = new Random();
        /* access modifiers changed from: private */
        public byte n = 0;
        /* access modifiers changed from: private */
        public Bitmap o;
        /* access modifiers changed from: private */
        public Bitmap p;
        /* access modifiers changed from: private */
        public Bitmap q;
        /* access modifiers changed from: private */
        public Bitmap r;
        /* access modifiers changed from: private */
        public Bitmap s;
        /* access modifiers changed from: private */
        public Bitmap t;
        /* access modifiers changed from: private */
        public Bitmap u;
        /* access modifiers changed from: private */
        public Bitmap v;
        /* access modifiers changed from: private */
        public Bitmap w;
        /* access modifiers changed from: private */
        public Bitmap x;
        /* access modifiers changed from: private */
        public Bitmap y;
        /* access modifiers changed from: private */
        public Bitmap z;

        class FPSTimer {

            /* renamed from: a  reason: collision with root package name */
            private int f189a;
            private double b;
            private double c;
            private long d;

            public FPSTimer(int i) {
                this.f189a = i;
                reset();
            }

            public boolean elapsed() {
                long currentTimeMillis = System.currentTimeMillis();
                long j = currentTimeMillis - this.d;
                this.d = currentTimeMillis;
                this.c += ((double) j) / 1000.0d;
                this.c -= this.b;
                if (this.c <= 0.0d) {
                    try {
                        Thread.sleep((long) ((-this.c) * 1000.0d));
                    } catch (InterruptedException e2) {
                    }
                    return true;
                } else if (this.c <= this.b) {
                    return false;
                } else {
                    reset();
                    return true;
                }
            }

            public void reset() {
                this.b = 1.0d / ((double) this.f189a);
                this.d = System.currentTimeMillis();
                this.c = 0.0d;
            }
        }

        class HelloThread extends Thread {
            /* access modifiers changed from: private */

            /* renamed from: a  reason: collision with root package name */
            public float f190a;
            /* access modifiers changed from: private */
            public float b;
            /* access modifiers changed from: private */
            public boolean c = false;
            private boolean d = false;
            private boolean e = false;
            private byte f;
            private byte g;
            private long h;
            private long i;
            private long j;
            private long k;
            private int l = 0;
            private long m;
            private int n;
            private SurfaceHolder o;
            private boolean p = false;

            public HelloThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
                HelloView.this.N = context.getApplicationContext();
                this.o = surfaceHolder;
                HelloView.this.O = handler;
            }

            public boolean checkCardOK(int i2, int i3) {
                if (i3 == i2 + 1 || i3 == i2 - 1 || i3 == i2 + 12 || i3 == i2 - 12 || i3 == i2 + 14 || i3 == i2 - 14 || i3 == i2 + 25 || i3 == i2 - 25 || i3 == i2 + 27 || i3 == i2 - 27 || i3 == i2 + 38 || i3 == i2 - 38 || i3 == i2 + 40 || i3 == i2 - 40) {
                    return true;
                }
                if (i3 == 0 && i2 == 51) {
                    return true;
                }
                return i3 == 51 && i2 == 0;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
             arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
             candidates:
              ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
              ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
            /* access modifiers changed from: protected */
            public void doDraw(Canvas canvas) {
                Paint paint = new Paint();
                HelloView.this.t = null;
                paint.setAlpha(255);
                canvas.drawBitmap(HelloView.this.o, new Rect(0, 0, HelloView.this.o.getWidth(), HelloView.this.o.getHeight()), new Rect(0, 0, (int) (320.0f * HelloView.this.g), (int) (432.0f * HelloView.this.g)), paint);
                for (int i2 = 0; i2 < 10; i2++) {
                    if (HelloView.this.A[i2].f191a) {
                        paint.setAlpha(HelloView.this.A[i2].r);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.p, (int) (((float) HelloView.this.A[i2].e) * HelloView.this.h), (int) (((float) HelloView.this.A[i2].f) * HelloView.this.h), (int) (((float) HelloView.this.A[i2].g) * HelloView.this.h), (int) (((float) HelloView.this.A[i2].h) * HelloView.this.h));
                        HelloView.this.c = new Matrix();
                        HelloView.this.c.postScale(HelloView.this.A[i2].u, HelloView.this.A[i2].u);
                        HelloView.this.c.postRotate((float) HelloView.this.A[i2].s);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                        canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.A[i2].j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.A[i2].l / 2)) * HelloView.this.g), ((((float) HelloView.this.A[i2].k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.A[i2].m / 2)) * HelloView.this.g), paint);
                    }
                }
                for (int i3 = 0; i3 < 4; i3++) {
                    if (HelloView.this.J[i3].f191a) {
                        paint.setAlpha(HelloView.this.J[i3].r);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.z, (int) (((float) HelloView.this.J[i3].e) * HelloView.this.h), (int) (((float) HelloView.this.J[i3].f) * HelloView.this.h), (int) (((float) HelloView.this.J[i3].g) * HelloView.this.h), (int) (((float) HelloView.this.J[i3].h) * HelloView.this.h));
                        HelloView.this.c = new Matrix();
                        HelloView.this.c.postScale(HelloView.this.J[i3].u, HelloView.this.J[i3].u);
                        HelloView.this.c.postRotate((float) HelloView.this.J[i3].s);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                        canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.J[i3].j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.J[i3].l / 2)) * HelloView.this.g), ((((float) HelloView.this.J[i3].k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.J[i3].m / 2)) * HelloView.this.g), paint);
                    }
                }
                for (int i4 = 0; i4 < 2; i4++) {
                    if (HelloView.this.B[i4].f191a) {
                        paint.setAlpha(HelloView.this.B[i4].r);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.q, (int) (((float) HelloView.this.B[i4].e) * HelloView.this.h), (int) (((float) HelloView.this.B[i4].f) * HelloView.this.h), (int) (((float) HelloView.this.B[i4].g) * HelloView.this.h), (int) (((float) HelloView.this.B[i4].h) * HelloView.this.h));
                        HelloView.this.c = new Matrix();
                        HelloView.this.c.postScale(HelloView.this.B[i4].u, HelloView.this.B[i4].u);
                        HelloView.this.c.postRotate((float) HelloView.this.B[i4].s);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                        canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.B[i4].j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.B[i4].l / 2)) * HelloView.this.g), ((((float) HelloView.this.B[i4].k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.B[i4].m / 2)) * HelloView.this.g), paint);
                    }
                }
                for (int i5 = 0; i5 < 2; i5++) {
                    if (HelloView.this.C[i5].f191a) {
                        paint.setAlpha(HelloView.this.C[i5].r);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.r, (int) (((float) HelloView.this.C[i5].e) * HelloView.this.h), (int) (((float) HelloView.this.C[i5].f) * HelloView.this.h), (int) (((float) HelloView.this.C[i5].g) * HelloView.this.h), (int) (((float) HelloView.this.C[i5].h) * HelloView.this.h));
                        HelloView.this.c = new Matrix();
                        HelloView.this.c.postScale(HelloView.this.C[i5].u, HelloView.this.C[i5].u);
                        HelloView.this.c.postRotate((float) HelloView.this.C[i5].s);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                        canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.C[i5].j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.C[i5].l / 2)) * HelloView.this.g), ((((float) HelloView.this.C[i5].k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.C[i5].m / 2)) * HelloView.this.g), paint);
                    }
                }
                for (int i6 = 0; i6 < 4; i6++) {
                    if (HelloView.this.F[i6].f191a) {
                        paint.setAlpha(HelloView.this.F[i6].r);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.v, (int) (((float) HelloView.this.F[i6].e) * HelloView.this.h), (int) (((float) HelloView.this.F[i6].f) * HelloView.this.h), (int) (((float) HelloView.this.F[i6].g) * HelloView.this.h), (int) (((float) HelloView.this.F[i6].h) * HelloView.this.h));
                        HelloView.this.c = new Matrix();
                        HelloView.this.c.postScale(HelloView.this.F[i6].u, HelloView.this.F[i6].u);
                        HelloView.this.c.postRotate((float) HelloView.this.F[i6].s);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                        canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.F[i6].j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.F[i6].l / 2)) * HelloView.this.g), ((((float) HelloView.this.F[i6].k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.F[i6].m / 2)) * HelloView.this.g), paint);
                    }
                }
                for (int i7 = 0; i7 < 2; i7++) {
                    if (HelloView.this.D[i7].f191a) {
                        paint.setAlpha(HelloView.this.D[i7].r);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.s, (int) (((float) HelloView.this.D[i7].e) * HelloView.this.h), (int) (((float) HelloView.this.D[i7].f) * HelloView.this.h), (int) (((float) HelloView.this.D[i7].g) * HelloView.this.h), (int) (((float) HelloView.this.D[i7].h) * HelloView.this.h));
                        HelloView.this.c = new Matrix();
                        HelloView.this.c.postScale(HelloView.this.D[i7].u, HelloView.this.D[i7].u);
                        HelloView.this.c.postRotate((float) HelloView.this.D[i7].s);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                        canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.D[i7].j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.D[i7].l / 2)) * HelloView.this.g), ((((float) HelloView.this.D[i7].k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.D[i7].m / 2)) * HelloView.this.g), paint);
                    }
                }
                if (HelloView.this.E.f191a) {
                    paint.setAlpha(HelloView.this.E.r);
                    HelloView.this.t = Bitmap.createBitmap(HelloView.this.u, (int) (((float) HelloView.this.E.e) * HelloView.this.h), (int) (((float) HelloView.this.E.f) * HelloView.this.h), (int) (((float) HelloView.this.E.g) * HelloView.this.h), (int) (((float) HelloView.this.E.h) * HelloView.this.h));
                    HelloView.this.c = new Matrix();
                    HelloView.this.c.postScale(HelloView.this.E.u, HelloView.this.E.u);
                    HelloView.this.c.postRotate((float) HelloView.this.E.s);
                    HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                    canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.E.j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.E.l / 2)) * HelloView.this.g), ((((float) HelloView.this.E.k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.E.m / 2)) * HelloView.this.g), paint);
                }
                for (int i8 = 0; i8 < 5; i8++) {
                    if (HelloView.this.H[i8].f191a) {
                        paint.setAlpha(HelloView.this.H[i8].r);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.w, (int) (((float) HelloView.this.H[i8].e) * HelloView.this.h), (int) (((float) HelloView.this.H[i8].f) * HelloView.this.h), (int) (((float) HelloView.this.H[i8].g) * HelloView.this.h), (int) (((float) HelloView.this.H[i8].h) * HelloView.this.h));
                        HelloView.this.c = new Matrix();
                        HelloView.this.c.postScale(HelloView.this.H[i8].u, HelloView.this.H[i8].u);
                        HelloView.this.c.postRotate((float) HelloView.this.H[i8].s);
                        HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                        canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.H[i8].j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.H[i8].l / 2)) * HelloView.this.g), ((((float) HelloView.this.H[i8].k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.H[i8].m / 2)) * HelloView.this.g), paint);
                    }
                }
                if (HelloView.this.G.f191a) {
                    paint.setAlpha(HelloView.this.G.r);
                    HelloView.this.t = Bitmap.createBitmap(HelloView.this.x, (int) (((float) HelloView.this.G.e) * HelloView.this.h), (int) (((float) HelloView.this.G.f) * HelloView.this.h), (int) (((float) HelloView.this.G.g) * HelloView.this.h), (int) (((float) HelloView.this.G.h) * HelloView.this.h));
                    HelloView.this.c = new Matrix();
                    HelloView.this.c.postScale(HelloView.this.G.u, HelloView.this.G.u);
                    HelloView.this.c.postRotate((float) HelloView.this.G.s);
                    HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                    canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.G.j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.G.l / 2)) * HelloView.this.g), ((((float) HelloView.this.G.k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.G.m / 2)) * HelloView.this.g), paint);
                }
                if (HelloView.this.I.f191a) {
                    paint.setAlpha(HelloView.this.I.r);
                    HelloView.this.t = Bitmap.createBitmap(HelloView.this.y, (int) (((float) HelloView.this.I.e) * HelloView.this.h), (int) (((float) HelloView.this.I.f) * HelloView.this.h), (int) (((float) HelloView.this.I.g) * HelloView.this.h), (int) (((float) HelloView.this.I.h) * HelloView.this.h));
                    HelloView.this.c = new Matrix();
                    HelloView.this.c.postScale(HelloView.this.I.u, HelloView.this.I.u);
                    HelloView.this.c.postRotate((float) HelloView.this.I.s);
                    HelloView.this.t = Bitmap.createBitmap(HelloView.this.t, 0, 0, HelloView.this.t.getWidth(), HelloView.this.t.getHeight(), HelloView.this.c, true);
                    canvas.drawBitmap(HelloView.this.t, ((((float) HelloView.this.I.j) * HelloView.this.g) - ((float) (HelloView.this.t.getWidth() / 2))) + (((float) (HelloView.this.I.l / 2)) * HelloView.this.g), ((((float) HelloView.this.I.k) * HelloView.this.g) - ((float) (HelloView.this.t.getHeight() / 2))) + (((float) (HelloView.this.I.m / 2)) * HelloView.this.g), paint);
                }
            }

            /* JADX WARN: Type inference failed for: r14v0, types: [hamon05.speed.pac.main$HelloView$HelloThread] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void run() {
                /*
                    r14 = this;
                    r13 = 2
                    r12 = 3
                    r11 = 0
                    r10 = 1
                    hamon05.speed.pac.main$HelloView$FPSTimer r0 = new hamon05.speed.pac.main$HelloView$FPSTimer
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this
                    r2 = 60
                    r0.<init>(r2)
                    r1 = r10
                    r2 = r10
                L_0x000f:
                    boolean r3 = r14.p
                    if (r3 != 0) goto L_0x0014
                    return
                L_0x0014:
                    r3 = 0
                    if (r2 == 0) goto L_0x0020
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this
                    byte r2 = r2.n
                    switch(r2) {
                        case 0: goto L_0x0025;
                        case 1: goto L_0x2a24;
                        case 2: goto L_0x2b58;
                        case 3: goto L_0x45c5;
                        case 4: goto L_0x71b2;
                        case 5: goto L_0x60da;
                        default: goto L_0x0020;
                    }
                L_0x0020:
                    boolean r2 = r0.elapsed()
                    goto L_0x000f
                L_0x0025:
                    int r2 = r14.l     // Catch:{ all -> 0x00fa }
                    switch(r2) {
                        case 0: goto L_0x0042;
                        case 1: goto L_0x0104;
                        case 2: goto L_0x039a;
                        case 3: goto L_0x0410;
                        case 4: goto L_0x0486;
                        case 5: goto L_0x04fc;
                        case 10: goto L_0x0574;
                        case 11: goto L_0x06e7;
                        case 12: goto L_0x0a35;
                        case 13: goto L_0x0aae;
                        case 14: goto L_0x0b27;
                        case 15: goto L_0x0ba0;
                        case 16: goto L_0x0c19;
                        case 20: goto L_0x0f4f;
                        case 21: goto L_0x0fd4;
                        case 22: goto L_0x1124;
                        case 23: goto L_0x119a;
                        case 30: goto L_0x120f;
                        case 31: goto L_0x1284;
                        case 32: goto L_0x132f;
                        case 40: goto L_0x13ae;
                        case 50: goto L_0x1b5a;
                        case 60: goto L_0x2299;
                        case 110: goto L_0x0c8e;
                        case 111: goto L_0x0d13;
                        case 112: goto L_0x0e63;
                        case 113: goto L_0x0ed9;
                        default: goto L_0x002a;
                    }     // Catch:{ all -> 0x00fa }
                L_0x002a:
                    android.view.SurfaceHolder r2 = r14.o     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    android.graphics.Canvas r2 = r2.lockCanvas(r4)     // Catch:{ all -> 0x00fa }
                    android.view.SurfaceHolder r3 = r14.o     // Catch:{ all -> 0x2a20 }
                    monitor-enter(r3)     // Catch:{ all -> 0x2a20 }
                    if (r2 == 0) goto L_0x0039
                    r14.doDraw(r2)     // Catch:{ all -> 0x2a1d }
                L_0x0039:
                    monitor-exit(r3)     // Catch:{ all -> 0x2a1d }
                    if (r2 == 0) goto L_0x0020
                    android.view.SurfaceHolder r3 = r14.o
                    r3.unlockCanvasAndPost(r2)
                    goto L_0x0020
                L_0x0042:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.G     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 144(0x90, float:2.02E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 720(0x2d0, float:1.009E-42)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 480(0x1e0, float:6.73E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    r2 = 1
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x00fa:
                    r0 = move-exception
                    r1 = r3
                L_0x00fc:
                    if (r1 == 0) goto L_0x0103
                    android.view.SurfaceHolder r2 = r14.o
                    r2.unlockCanvasAndPost(r1)
                L_0x0103:
                    throw r0
                L_0x0104:
                    boolean r2 = r14.c     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x01ac
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x01ac
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x01ac
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x01ac
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 2
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x01ac:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x0250
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x0250
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0250
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0250
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 3
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x0250:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x02f4
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x02f4
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x02f4
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x02f4
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 5
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x02f4:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 4
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x039a:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x03c4
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 10
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x03c4:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0410:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 2
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 2
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x043a
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 20
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x043a:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0486:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x04b0
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 30
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x04b0:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x04fc:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0528
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    com.openfeint.api.ui.Dashboard.open()     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x0528:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0574:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 288(0x120, float:4.04E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    android.content.SharedPreferences r2 = r2.f183a     // Catch:{ all -> 0x00fa }
                    java.lang.String r4 = "EASY"
                    r5 = 0
                    boolean r2 = r2.getBoolean(r4, r5)     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x05a9
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 528(0x210, float:7.4E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                L_0x05a9:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 336(0x150, float:4.71E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    android.content.SharedPreferences r2 = r2.f183a     // Catch:{ all -> 0x00fa }
                    java.lang.String r4 = "NORMAL"
                    r5 = 0
                    boolean r2 = r2.getBoolean(r4, r5)     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x05eb
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 576(0x240, float:8.07E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                L_0x05eb:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    android.content.SharedPreferences r2 = r2.f183a     // Catch:{ all -> 0x00fa }
                    java.lang.String r4 = "HARD"
                    r5 = 0
                    boolean r2 = r2.getBoolean(r4, r5)     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x062d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 624(0x270, float:8.74E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                L_0x062d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    android.content.SharedPreferences r2 = r2.f183a     // Catch:{ all -> 0x00fa }
                    java.lang.String r4 = "EASY"
                    r5 = 0
                    boolean r2 = r2.getBoolean(r4, r5)     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x06b0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    android.content.SharedPreferences r2 = r2.f183a     // Catch:{ all -> 0x00fa }
                    java.lang.String r4 = "NORMAL"
                    r5 = 0
                    boolean r2 = r2.getBoolean(r4, r5)     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x06b0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    android.content.SharedPreferences r2 = r2.f183a     // Catch:{ all -> 0x00fa }
                    java.lang.String r4 = "HARD"
                    r5 = 0
                    boolean r2 = r2.getBoolean(r4, r5)     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x06b0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 432(0x1b0, float:6.05E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    android.content.SharedPreferences r2 = r2.f183a     // Catch:{ all -> 0x00fa }
                    java.lang.String r4 = "NIGHTMARE"
                    r5 = 0
                    boolean r2 = r2.getBoolean(r4, r5)     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x06a3
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 672(0x2a0, float:9.42E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                L_0x06a3:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                L_0x06b0:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.G     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    r2 = 11
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x06e7:
                    boolean r2 = r14.c     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x0790
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x0790
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0790
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0790
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 12
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x0790:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x0835
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x0835
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0835
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0835
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 13
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x0835:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x08da
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x08da
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x08da
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x08da
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 14
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x08da:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x098e
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x098e
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x098e
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x098e
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x098e
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 15
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x098e:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 16
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0a35:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 0
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 0
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0a62
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.g = r2     // Catch:{ all -> 0x00fa }
                    r2 = 110(0x6e, float:1.54E-43)
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x0a62:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0aae:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0adb
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 1
                    r14.g = r2     // Catch:{ all -> 0x00fa }
                    r2 = 110(0x6e, float:1.54E-43)
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x0adb:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0b27:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 2
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 2
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0b54
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 2
                    r14.g = r2     // Catch:{ all -> 0x00fa }
                    r2 = 110(0x6e, float:1.54E-43)
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x0b54:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0ba0:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0bcd
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 3
                    r14.g = r2     // Catch:{ all -> 0x00fa }
                    r2 = 110(0x6e, float:1.54E-43)
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x0bcd:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0c19:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0c42
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x0c42:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0c8e:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 192(0xc0, float:2.69E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.G     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    r2 = 111(0x6f, float:1.56E-43)
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0d13:
                    boolean r2 = r14.c     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x0dbc
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x0dbc
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0dbc
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0dbc
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 112(0x70, float:1.57E-43)
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x0dbc:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 113(0x71, float:1.58E-43)
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0e63:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0e8d
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 40
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x0e8d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0ed9:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0f03
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 10
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x0f03:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0f4f:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 192(0xc0, float:2.69E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.G     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    r2 = 21
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x0fd4:
                    boolean r2 = r14.c     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x107d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x107d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x107d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x107d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 22
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                L_0x107d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 23
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x1124:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x114e
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 50
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x114e:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x119a:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x11c3
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x11c3:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x120f:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.I     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.G     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    r2 = 31
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x1284:
                    boolean r2 = r14.c     // Catch:{ all -> 0x00fa }
                    if (r2 == 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.j     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.g     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.k     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.h     // Catch:{ all -> 0x00fa }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    float r4 = r4.g     // Catch:{ all -> 0x00fa }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x00fa }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r2.c = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x00fa }
                    r2 = 32
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x132f:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.c     // Catch:{ all -> 0x00fa }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r2 = r2[r6]     // Catch:{ all -> 0x00fa }
                    long r6 = r2.d     // Catch:{ all -> 0x00fa }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x1362
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.I     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.l = r2     // Catch:{ all -> 0x00fa }
                L_0x1362:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    float r4 = r4.u     // Catch:{ all -> 0x00fa }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    int r2 = r2.r     // Catch:{ all -> 0x00fa }
                    r4 = 40
                    if (r2 <= r4) goto L_0x002a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x00fa }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.r     // Catch:{ all -> 0x00fa }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x13ae:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r2.shuffle(r4)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 128(0x80, float:1.794E-43)
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 272(0x110, float:3.81E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.t     // Catch:{ all -> 0x00fa }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.t     // Catch:{ all -> 0x00fa }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 0
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 0
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 1
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 2
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 2
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 3
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2.K = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 25
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 4
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 26
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 5
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 5
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 27
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 6
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 6
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 28
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 7
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 7
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 29
                    r2.L = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 50
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 51
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r14.m = r4     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.n = r2     // Catch:{ all -> 0x00fa }
                    r2 = 2
                    r14.f = r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.n = r4     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    byte r2 = r14.g     // Catch:{ all -> 0x00fa }
                    if (r2 != 0) goto L_0x1b34
                    r4 = 3000(0xbb8, double:1.482E-320)
                    r14.i = r4     // Catch:{ all -> 0x00fa }
                    r4 = 2000(0x7d0, double:9.88E-321)
                    r14.j = r4     // Catch:{ all -> 0x00fa }
                L_0x1b34:
                    byte r2 = r14.g     // Catch:{ all -> 0x00fa }
                    if (r2 != r10) goto L_0x1b40
                    r4 = 1500(0x5dc, double:7.41E-321)
                    r14.i = r4     // Catch:{ all -> 0x00fa }
                    r4 = 1000(0x3e8, double:4.94E-321)
                    r14.j = r4     // Catch:{ all -> 0x00fa }
                L_0x1b40:
                    byte r2 = r14.g     // Catch:{ all -> 0x00fa }
                    if (r2 != r13) goto L_0x1b4c
                    r4 = 1000(0x3e8, double:4.94E-321)
                    r14.i = r4     // Catch:{ all -> 0x00fa }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r14.j = r4     // Catch:{ all -> 0x00fa }
                L_0x1b4c:
                    byte r2 = r14.g     // Catch:{ all -> 0x00fa }
                    if (r2 != r12) goto L_0x002a
                    r4 = 500(0x1f4, double:2.47E-321)
                    r14.i = r4     // Catch:{ all -> 0x00fa }
                    r4 = 250(0xfa, double:1.235E-321)
                    r14.j = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x1b5a:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r2.shuffle(r4)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 64
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 272(0x110, float:3.81E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.t     // Catch:{ all -> 0x00fa }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.t     // Catch:{ all -> 0x00fa }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 0
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 0
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 1
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 2
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 2
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 3
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2.K = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 25
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 4
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 26
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 5
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 5
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 27
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 6
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 6
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 28
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 7
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 7
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 29
                    r2.L = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 50
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 51
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r14.m = r4     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.n = r2     // Catch:{ all -> 0x00fa }
                    r2 = 3
                    r14.f = r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.n = r4     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x2299:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r2.shuffle(r4)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r2.backDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r2.u = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.r = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 128(0x80, float:1.794E-43)
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 272(0x110, float:3.81E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.t     // Catch:{ all -> 0x00fa }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    int r4 = r4.t     // Catch:{ all -> 0x00fa }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 384(0x180, float:5.38E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.J     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 0
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 0
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 1
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 1
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 2
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 2
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 3
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 3
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2.K = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 25
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 4
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 4
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 26
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 5
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 5
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 27
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 6
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 6
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 28
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 7
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 7
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 29
                    r2.L = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 50
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.d     // Catch:{ all -> 0x00fa }
                    r5 = 51
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    r2.i = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r4 = r4.e     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x00fa }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    int r5 = r5.i     // Catch:{ all -> 0x00fa }
                    r4 = r4[r5]     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    int[] r5 = r5.f     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x00fa }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x00fa }
                    int r6 = r6.i     // Catch:{ all -> 0x00fa }
                    r5 = r5[r6]     // Catch:{ all -> 0x00fa }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x00fa }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r2.setDef()     // Catch:{ all -> 0x00fa }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fa }
                    r14.m = r4     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.n = r2     // Catch:{ all -> 0x00fa }
                    r2 = 6
                    r14.f = r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2.n = r4     // Catch:{ all -> 0x00fa }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x00fa }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x00fa }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x00fa }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x00fa }
                    r4 = 1500(0x5dc, double:7.41E-321)
                    r14.i = r4     // Catch:{ all -> 0x00fa }
                    r4 = 1000(0x3e8, double:4.94E-321)
                    r14.j = r4     // Catch:{ all -> 0x00fa }
                    goto L_0x002a
                L_0x2a1d:
                    r0 = move-exception
                    monitor-exit(r3)     // Catch:{ all -> 0x2a1d }
                    throw r0     // Catch:{ all -> 0x2a20 }
                L_0x2a20:
                    r0 = move-exception
                    r1 = r2
                    goto L_0x00fc
                L_0x2a24:
                    int r2 = r14.n     // Catch:{ all -> 0x2ab8 }
                    switch(r2) {
                        case 0: goto L_0x2a42;
                        case 1: goto L_0x2ac2;
                        default: goto L_0x2a29;
                    }     // Catch:{ all -> 0x2ab8 }
                L_0x2a29:
                    android.view.SurfaceHolder r2 = r14.o     // Catch:{ all -> 0x2ab8 }
                    r4 = 0
                    android.graphics.Canvas r2 = r2.lockCanvas(r4)     // Catch:{ all -> 0x2ab8 }
                    android.view.SurfaceHolder r3 = r14.o     // Catch:{ all -> 0x2b54 }
                    monitor-enter(r3)     // Catch:{ all -> 0x2b54 }
                    if (r2 == 0) goto L_0x2a38
                    r14.doDraw(r2)     // Catch:{ all -> 0x2b51 }
                L_0x2a38:
                    monitor-exit(r3)     // Catch:{ all -> 0x2b51 }
                    if (r2 == 0) goto L_0x0020
                    android.view.SurfaceHolder r3 = r14.o
                    r3.unlockCanvasAndPost(r2)
                    goto L_0x0020
                L_0x2a42:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x2ab8 }
                    long r6 = r14.m     // Catch:{ all -> 0x2ab8 }
                    long r4 = r4 - r6
                    r6 = 2000(0x7d0, double:9.88E-321)
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x2a6c
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    r2.backDef()     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    r4 = 288(0x120, float:4.04E-43)
                    r2.f = r4     // Catch:{ all -> 0x2ab8 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x2ab8 }
                    r14.m = r4     // Catch:{ all -> 0x2ab8 }
                    r2 = 1
                    r14.n = r2     // Catch:{ all -> 0x2ab8 }
                L_0x2a6c:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    float r2 = r2.u     // Catch:{ all -> 0x2ab8 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x2a93
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x2ab8 }
                    float r4 = r4.u     // Catch:{ all -> 0x2ab8 }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 - r5
                    r2.u = r4     // Catch:{ all -> 0x2ab8 }
                L_0x2a93:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    int r2 = r2.r     // Catch:{ all -> 0x2ab8 }
                    r4 = 245(0xf5, float:3.43E-43)
                    if (r2 >= r4) goto L_0x2a29
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x2ab8 }
                    int r4 = r4.r     // Catch:{ all -> 0x2ab8 }
                    int r4 = r4 + 5
                    r2.r = r4     // Catch:{ all -> 0x2ab8 }
                    goto L_0x2a29
                L_0x2ab8:
                    r0 = move-exception
                    r1 = r3
                L_0x2aba:
                    if (r1 == 0) goto L_0x2ac1
                    android.view.SurfaceHolder r2 = r14.o
                    r2.unlockCanvasAndPost(r1)
                L_0x2ac1:
                    throw r0
                L_0x2ac2:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x2ab8 }
                    long r6 = r14.m     // Catch:{ all -> 0x2ab8 }
                    long r4 = r4 - r6
                    r6 = 500(0x1f4, double:2.47E-321)
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x2b14
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    r2.backDef()     // Catch:{ all -> 0x2ab8 }
                    r2 = 2
                    r14.n = r2     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    byte r4 = r14.f     // Catch:{ all -> 0x2ab8 }
                    r2.n = r4     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x2ab8 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x2ab8 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x2ab8 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x2ab8 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x2ab8 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x2ab8 }
                    r14.k = r4     // Catch:{ all -> 0x2ab8 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x2ab8 }
                    r14.h = r4     // Catch:{ all -> 0x2ab8 }
                L_0x2b14:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x2ab8 }
                    float r4 = r4.u     // Catch:{ all -> 0x2ab8 }
                    r5 = 1045220557(0x3e4ccccd, float:0.2)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    int r2 = r2.r     // Catch:{ all -> 0x2ab8 }
                    r4 = 10
                    if (r2 <= r4) goto L_0x2a29
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x2ab8 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x2ab8 }
                    int r4 = r4.r     // Catch:{ all -> 0x2ab8 }
                    r5 = 20
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x2ab8 }
                    goto L_0x2a29
                L_0x2b51:
                    r0 = move-exception
                    monitor-exit(r3)     // Catch:{ all -> 0x2b51 }
                    throw r0     // Catch:{ all -> 0x2b54 }
                L_0x2b54:
                    r0 = move-exception
                    r1 = r2
                    goto L_0x2aba
                L_0x2b58:
                    boolean r2 = r14.c     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2b64
                    boolean r2 = r14.d     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x2b64
                    r2 = r11
                L_0x2b61:
                    r4 = 4
                    if (r2 < r4) goto L_0x34bf
                L_0x2b64:
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x2b70
                    r4 = 3000(0xbb8, double:1.482E-320)
                    r14.i = r4     // Catch:{ all -> 0x3ad7 }
                    r4 = 2000(0x7d0, double:9.88E-321)
                    r14.j = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2b70:
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != r10) goto L_0x2b7c
                    r4 = 1500(0x5dc, double:7.41E-321)
                    r14.i = r4     // Catch:{ all -> 0x3ad7 }
                    r4 = 1000(0x3e8, double:4.94E-321)
                    r14.j = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2b7c:
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != r13) goto L_0x2b88
                    r4 = 1000(0x3e8, double:4.94E-321)
                    r14.i = r4     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r14.j = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2b88:
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != r12) goto L_0x2b94
                    r4 = 500(0x1f4, double:2.47E-321)
                    r14.i = r4     // Catch:{ all -> 0x3ad7 }
                    r4 = 250(0xfa, double:1.235E-321)
                    r14.j = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2b94:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    long r6 = r14.h     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    long r6 = r14.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    java.util.Random r2 = r2.m     // Catch:{ all -> 0x3ad7 }
                    long r8 = r14.j     // Catch:{ all -> 0x3ad7 }
                    int r8 = (int) r8     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.nextInt(r8)     // Catch:{ all -> 0x3ad7 }
                    long r8 = (long) r2     // Catch:{ all -> 0x3ad7 }
                    long r6 = r6 + r8
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x2bb5
                    r2 = 4
                L_0x2bb1:
                    r4 = 8
                    if (r2 < r4) goto L_0x3afe
                L_0x2bb5:
                    boolean r2 = r14.d     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2c80
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x3ad7 }
                    long r6 = r2.c     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    r6 = 500(0x1f4, double:2.47E-321)
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x2be1
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    r2 = 0
                    r14.d = r2     // Catch:{ all -> 0x3ad7 }
                L_0x2be1:
                    float r2 = r14.b     // Catch:{ all -> 0x3ad7 }
                    r4 = 1129840640(0x43580000, float:216.0)
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    float r5 = r5.g     // Catch:{ all -> 0x3ad7 }
                    float r4 = r4 * r5
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x2bf1
                    r2 = 0
                    r14.b = r2     // Catch:{ all -> 0x3ad7 }
                L_0x2bf1:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x2c1a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.D     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r12
                    r2.j = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2c1a:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2c44
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.D     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 3
                    r2.j = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2c44:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.j     // Catch:{ all -> 0x3ad7 }
                    r4 = 54
                    if (r2 >= r4) goto L_0x2c62
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2c62:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.j     // Catch:{ all -> 0x3ad7 }
                    r4 = 74
                    if (r2 <= r4) goto L_0x2c80
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.b = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2c80:
                    r2 = r11
                L_0x2c81:
                    if (r2 < r13) goto L_0x4044
                    r2 = r13
                L_0x2c84:
                    r4 = 4
                    if (r2 < r4) goto L_0x40e2
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x2c8f
                    r2 = r11
                L_0x2c8c:
                    r4 = 4
                    if (r2 < r4) goto L_0x4195
                L_0x2c8f:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2cfd
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    long r6 = r2.c     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    long r6 = r2.d     // Catch:{ all -> 0x3ad7 }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x2cc2
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2cc2:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x3ad7 }
                    float r4 = r4.u     // Catch:{ all -> 0x3ad7 }
                    r5 = 1036831949(0x3dcccccd, float:0.1)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.r     // Catch:{ all -> 0x3ad7 }
                    r4 = 10
                    if (r2 <= r4) goto L_0x2cfd
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.r     // Catch:{ all -> 0x3ad7 }
                    r5 = 20
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x3ad7 }
                L_0x2cfd:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2d2d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2d2d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2d5d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2d5d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2d8d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2d8d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2dbd
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2dbd:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2ded
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2ded:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 5
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2e1d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2e1d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 6
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2e4d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2e4d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 7
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2e7d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2e7d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2ead
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2ead:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2edd
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2edd:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2f0d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2f0d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2f3d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2f3d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2f6d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2f6d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 5
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2f9d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2f9d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 6
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2fcd
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2fcd:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 7
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r2 == 0) goto L_0x2ffd
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                L_0x2ffd:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.t     // Catch:{ all -> 0x3ad7 }
                    if (r2 <= 0) goto L_0x41ff
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.K     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r2.i = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.K     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    r2.j = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.k     // Catch:{ all -> 0x3ad7 }
                    r2.k = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.l     // Catch:{ all -> 0x3ad7 }
                    r2.l = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.m     // Catch:{ all -> 0x3ad7 }
                    r2.m = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 255(0xff, float:3.57E-43)
                    r2.r = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r2.u = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r2.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r2.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r2.backDef()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.f = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r2.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r2.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                L_0x3191:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.t     // Catch:{ all -> 0x3ad7 }
                    if (r2 <= 0) goto L_0x43df
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.K     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r2.i = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.K     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    r2.j = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.k     // Catch:{ all -> 0x3ad7 }
                    r2.k = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.l     // Catch:{ all -> 0x3ad7 }
                    r2.l = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.m     // Catch:{ all -> 0x3ad7 }
                    r2.m = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 255(0xff, float:3.57E-43)
                    r2.r = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r2.u = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r2.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r2.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r2.backDef()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.f = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r2.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r2.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                L_0x3325:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x33a0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x33a0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x33a0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x33a0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r2.backDef()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 192(0xc0, float:2.69E-43)
                    r2.f = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r2.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 3000(0xbb8, double:1.482E-320)
                    r2.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    r4 = 4
                    r2.n = r4     // Catch:{ all -> 0x3ad7 }
                L_0x33a0:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x34a6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x34a6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x34a6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x34a6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r2.backDef()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 144(0x90, float:2.02E-43)
                    r2.f = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r2.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 3000(0xbb8, double:1.482E-320)
                    r2.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    r4 = 4
                    r2.n = r4     // Catch:{ all -> 0x3ad7 }
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x343c
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    android.content.SharedPreferences$Editor r2 = r2.b     // Catch:{ all -> 0x3ad7 }
                    java.lang.String r4 = "EASY"
                    r5 = 1
                    r2.putBoolean(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    long r6 = r14.k     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    r6 = 0
                    r2.scoreSubmit(r4, r6)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.unlockStageAchievement(r4)     // Catch:{ all -> 0x3ad7 }
                L_0x343c:
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != r10) goto L_0x345d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    android.content.SharedPreferences$Editor r2 = r2.b     // Catch:{ all -> 0x3ad7 }
                    java.lang.String r4 = "NORMAL"
                    r5 = 1
                    r2.putBoolean(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    long r6 = r14.k     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    r6 = 1
                    r2.scoreSubmit(r4, r6)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2.unlockStageAchievement(r4)     // Catch:{ all -> 0x3ad7 }
                L_0x345d:
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != r13) goto L_0x347e
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    android.content.SharedPreferences$Editor r2 = r2.b     // Catch:{ all -> 0x3ad7 }
                    java.lang.String r4 = "HARD"
                    r5 = 1
                    r2.putBoolean(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    long r6 = r14.k     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    r6 = 2
                    r2.scoreSubmit(r4, r6)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    r4 = 2
                    r2.unlockStageAchievement(r4)     // Catch:{ all -> 0x3ad7 }
                L_0x347e:
                    byte r2 = r14.g     // Catch:{ all -> 0x3ad7 }
                    if (r2 != r12) goto L_0x349f
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    android.content.SharedPreferences$Editor r2 = r2.b     // Catch:{ all -> 0x3ad7 }
                    java.lang.String r4 = "NIGHTMARE"
                    r5 = 1
                    r2.putBoolean(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    long r6 = r14.k     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    r6 = 3
                    r2.scoreSubmit(r4, r6)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r2.unlockStageAchievement(r4)     // Catch:{ all -> 0x3ad7 }
                L_0x349f:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    android.content.SharedPreferences$Editor r2 = r2.b     // Catch:{ all -> 0x3ad7 }
                    r2.commit()     // Catch:{ all -> 0x3ad7 }
                L_0x34a6:
                    android.view.SurfaceHolder r2 = r14.o     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    android.graphics.Canvas r2 = r2.lockCanvas(r4)     // Catch:{ all -> 0x3ad7 }
                    android.view.SurfaceHolder r3 = r14.o     // Catch:{ all -> 0x45c1 }
                    monitor-enter(r3)     // Catch:{ all -> 0x45c1 }
                    if (r2 == 0) goto L_0x34b5
                    r14.doDraw(r2)     // Catch:{ all -> 0x45be }
                L_0x34b5:
                    monitor-exit(r3)     // Catch:{ all -> 0x45be }
                    if (r2 == 0) goto L_0x0020
                    android.view.SurfaceHolder r3 = r14.o
                    r3.unlockCanvasAndPost(r2)
                    goto L_0x0020
                L_0x34bf:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    float r4 = (float) r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    float r5 = r5.g     // Catch:{ all -> 0x3ad7 }
                    float r4 = r4 * r5
                    float r5 = r14.f190a     // Catch:{ all -> 0x3ad7 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 >= 0) goto L_0x3ab6
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.k     // Catch:{ all -> 0x3ad7 }
                    float r4 = (float) r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    float r5 = r5.g     // Catch:{ all -> 0x3ad7 }
                    float r4 = r4 * r5
                    float r5 = r14.b     // Catch:{ all -> 0x3ad7 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 >= 0) goto L_0x3ab6
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.g     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + r5
                    float r4 = (float) r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    float r5 = r5.g     // Catch:{ all -> 0x3ad7 }
                    float r4 = r4 * r5
                    float r5 = r14.f190a     // Catch:{ all -> 0x3ad7 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 <= 0) goto L_0x3ab6
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.k     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.h     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + r5
                    float r4 = (float) r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    float r5 = r5.g     // Catch:{ all -> 0x3ad7 }
                    float r4 = r4 * r5
                    float r5 = r14.b     // Catch:{ all -> 0x3ad7 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 <= 0) goto L_0x3ab6
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x3ab6
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x37be
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    r1.i = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    r1.j = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.k     // Catch:{ all -> 0x3ad7 }
                    r1.k = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.l     // Catch:{ all -> 0x3ad7 }
                    r1.l = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.m     // Catch:{ all -> 0x3ad7 }
                    r1.m = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 255(0xff, float:3.57E-43)
                    r1.r = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r1.u = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r1.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    r1.j = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.k     // Catch:{ all -> 0x3ad7 }
                    r1.k = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.l     // Catch:{ all -> 0x3ad7 }
                    r1.l = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.m     // Catch:{ all -> 0x3ad7 }
                    r1.m = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1.r = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r1.u = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r1.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    int r1 = r1.t     // Catch:{ all -> 0x3ad7 }
                    if (r1 <= 0) goto L_0x3aba
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r1 = r1[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.K     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r1.i = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r1 = r1[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r4 = r1.K     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 1
                    r1.K = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r1.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r10
                    r1.t = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                L_0x37bd:
                    r1 = r11
                L_0x37be:
                    if (r1 == 0) goto L_0x3a37
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x3a37
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    r1.i = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    r1.j = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.k     // Catch:{ all -> 0x3ad7 }
                    r1.k = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.l     // Catch:{ all -> 0x3ad7 }
                    r1.l = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.m     // Catch:{ all -> 0x3ad7 }
                    r1.m = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 255(0xff, float:3.57E-43)
                    r1.r = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r1.u = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r1.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.j     // Catch:{ all -> 0x3ad7 }
                    r1.j = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.k     // Catch:{ all -> 0x3ad7 }
                    r1.k = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.l     // Catch:{ all -> 0x3ad7 }
                    r1.l = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.m     // Catch:{ all -> 0x3ad7 }
                    r1.m = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1.r = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r1.u = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r1.c = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x3ad7 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    int r1 = r1.t     // Catch:{ all -> 0x3ad7 }
                    if (r1 <= 0) goto L_0x3ae1
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r1 = r1[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.K     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r1.i = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r1 = r1[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r4 = r1.K     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 1
                    r1.K = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r1.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r10
                    r1.t = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                L_0x3a36:
                    r1 = r11
                L_0x3a37:
                    if (r1 == 0) goto L_0x3ab2
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    int r1 = r1.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r1 = r14.checkCardOK(r1, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r1 != 0) goto L_0x3ab2
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    int r1 = r1.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    boolean r1 = r14.checkCardOK(r1, r4)     // Catch:{ all -> 0x3ad7 }
                    if (r1 != 0) goto L_0x3ab2
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r1.backDef()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1.b = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r1.c = r4     // Catch:{ all -> 0x3ad7 }
                    r1 = 1
                    r14.d = r1     // Catch:{ all -> 0x3ad7 }
                L_0x3ab2:
                    r1 = 0
                    r14.c = r1     // Catch:{ all -> 0x3ad7 }
                    r1 = r10
                L_0x3ab6:
                    int r2 = r2 + 1
                    goto L_0x2b61
                L_0x3aba:
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    int r1 = r1.t     // Catch:{ all -> 0x3ad7 }
                    if (r1 != 0) goto L_0x37bd
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r1 = r1[r2]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    goto L_0x37bd
                L_0x3ad7:
                    r0 = move-exception
                    r1 = r3
                L_0x3ad9:
                    if (r1 == 0) goto L_0x3ae0
                    android.view.SurfaceHolder r2 = r14.o
                    r2.unlockCanvasAndPost(r1)
                L_0x3ae0:
                    throw r0
                L_0x3ae1:
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x3ad7 }
                    int r1 = r1.t     // Catch:{ all -> 0x3ad7 }
                    if (r1 != 0) goto L_0x3a36
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x3ad7 }
                    r1 = r1[r2]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r1.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    goto L_0x3a36
                L_0x3afe:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x4040
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x3da6
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4.i = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x3ad7 }
                    r8 = 8
                    r7 = r7[r8]     // Catch:{ all -> 0x3ad7 }
                    int r7 = r7.i     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    r7 = 64
                    r8 = 64
                    r4.setPic(r5, r6, r7, r8)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.j     // Catch:{ all -> 0x3ad7 }
                    r4.j = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.k     // Catch:{ all -> 0x3ad7 }
                    r4.k = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.l     // Catch:{ all -> 0x3ad7 }
                    r4.l = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.m     // Catch:{ all -> 0x3ad7 }
                    r4.m = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 255(0xff, float:3.57E-43)
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1065353216(0x3f800000, float:1.0)
                    r4.u = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.b = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r4.c = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 500(0x1f4, double:2.47E-321)
                    r4.d = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.j     // Catch:{ all -> 0x3ad7 }
                    r4.j = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.k     // Catch:{ all -> 0x3ad7 }
                    r4.k = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.l     // Catch:{ all -> 0x3ad7 }
                    r4.l = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.m     // Catch:{ all -> 0x3ad7 }
                    r4.m = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1073741824(0x40000000, float:2.0)
                    r4.u = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.b = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r4.c = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 500(0x1f4, double:2.47E-321)
                    r4.d = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    if (r4 <= 0) goto L_0x3d8a
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.d     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.K     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r4.i = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x3ad7 }
                    r2 = r7[r2]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    r2 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r4.setPic(r5, r2, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.K     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                L_0x3d82:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r14.h = r4     // Catch:{ all -> 0x3ad7 }
                    goto L_0x2bb5
                L_0x3d8a:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    if (r4 != 0) goto L_0x3d82
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r2 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    goto L_0x3d82
                L_0x3da6:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x4040
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4.i = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x3ad7 }
                    r8 = 9
                    r7 = r7[r8]     // Catch:{ all -> 0x3ad7 }
                    int r7 = r7.i     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    r7 = 64
                    r8 = 64
                    r4.setPic(r5, r6, r7, r8)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.j     // Catch:{ all -> 0x3ad7 }
                    r4.j = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.k     // Catch:{ all -> 0x3ad7 }
                    r4.k = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.l     // Catch:{ all -> 0x3ad7 }
                    r4.l = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.m     // Catch:{ all -> 0x3ad7 }
                    r4.m = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 255(0xff, float:3.57E-43)
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1065353216(0x3f800000, float:1.0)
                    r4.u = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.b = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r4.c = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 500(0x1f4, double:2.47E-321)
                    r4.d = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.j     // Catch:{ all -> 0x3ad7 }
                    r4.j = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.k     // Catch:{ all -> 0x3ad7 }
                    r4.k = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.l     // Catch:{ all -> 0x3ad7 }
                    r4.l = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.m     // Catch:{ all -> 0x3ad7 }
                    r4.m = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1073741824(0x40000000, float:2.0)
                    r4.u = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.b = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r4.c = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 500(0x1f4, double:2.47E-321)
                    r4.d = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    if (r4 <= 0) goto L_0x4024
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.d     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.K     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r4.i = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x3ad7 }
                    r2 = r7[r2]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    r2 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r4.setPic(r5, r2, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.K     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                L_0x401c:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r14.h = r4     // Catch:{ all -> 0x3ad7 }
                    goto L_0x2bb5
                L_0x4024:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    if (r4 != 0) goto L_0x401c
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r2 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    goto L_0x401c
                L_0x4040:
                    int r2 = r2 + 1
                    goto L_0x2bb1
                L_0x4044:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x40de
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.F     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    long r6 = r6.c     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.F     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    long r6 = r6.d     // Catch:{ all -> 0x3ad7 }
                    int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r4 <= 0) goto L_0x407f
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                L_0x407f:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.F     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    float r5 = r5.u     // Catch:{ all -> 0x3ad7 }
                    r6 = 1036831949(0x3dcccccd, float:0.1)
                    float r5 = r5 + r6
                    r4.u = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.r     // Catch:{ all -> 0x3ad7 }
                    r5 = 20
                    if (r4 <= r5) goto L_0x40c4
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.F     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.r     // Catch:{ all -> 0x3ad7 }
                    r6 = 20
                    int r5 = r5 - r6
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                L_0x40c4:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.r     // Catch:{ all -> 0x3ad7 }
                    if (r4 >= 0) goto L_0x40de
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                L_0x40de:
                    int r2 = r2 + 1
                    goto L_0x2c81
                L_0x40e2:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x4191
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.F     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    long r6 = r6.c     // Catch:{ all -> 0x3ad7 }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.F     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    long r6 = r6.d     // Catch:{ all -> 0x3ad7 }
                    int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r4 <= 0) goto L_0x411d
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                L_0x411d:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    float r4 = r4.u     // Catch:{ all -> 0x3ad7 }
                    r5 = 1066192077(0x3f8ccccd, float:1.1)
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 <= 0) goto L_0x414b
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.F     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    float r5 = r5.u     // Catch:{ all -> 0x3ad7 }
                    r6 = 1036831949(0x3dcccccd, float:0.1)
                    float r5 = r5 - r6
                    r4.u = r5     // Catch:{ all -> 0x3ad7 }
                L_0x414b:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.r     // Catch:{ all -> 0x3ad7 }
                    r5 = 255(0xff, float:3.57E-43)
                    if (r4 >= r5) goto L_0x4174
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.F     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.r     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5 + 20
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                L_0x4174:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.r     // Catch:{ all -> 0x3ad7 }
                    r5 = 255(0xff, float:3.57E-43)
                    if (r4 <= r5) goto L_0x4191
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r5 = 255(0xff, float:3.57E-43)
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                L_0x4191:
                    int r2 = r2 + 1
                    goto L_0x2c84
                L_0x4195:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.J     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x41fb
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    if (r4 != 0) goto L_0x41ef
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.i     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x41fb
                L_0x41ef:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.J     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                L_0x41fb:
                    int r2 = r2 + 1
                    goto L_0x2c8c
                L_0x41ff:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.t     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3191
                    r2 = 4
                L_0x420f:
                    r4 = 8
                    if (r2 >= r4) goto L_0x3191
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x43db
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4.i = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x3ad7 }
                    r8 = 8
                    r7 = r7[r8]     // Catch:{ all -> 0x3ad7 }
                    int r7 = r7.i     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    r7 = 64
                    r8 = 64
                    r4.setPic(r5, r6, r7, r8)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.j     // Catch:{ all -> 0x3ad7 }
                    r4.j = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.k     // Catch:{ all -> 0x3ad7 }
                    r4.k = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.l     // Catch:{ all -> 0x3ad7 }
                    r4.l = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.m     // Catch:{ all -> 0x3ad7 }
                    r4.m = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 255(0xff, float:3.57E-43)
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1065353216(0x3f800000, float:1.0)
                    r4.u = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.b = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r4.c = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 500(0x1f4, double:2.47E-321)
                    r4.d = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    if (r4 <= 0) goto L_0x43be
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.d     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.K     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r4.i = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x3ad7 }
                    r2 = r7[r2]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    r2 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r4.setPic(r5, r2, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.K     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    goto L_0x3191
                L_0x43be:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    if (r4 != 0) goto L_0x3191
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r2 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    goto L_0x3191
                L_0x43db:
                    int r2 = r2 + 1
                    goto L_0x420f
                L_0x43df:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.t     // Catch:{ all -> 0x3ad7 }
                    if (r2 != 0) goto L_0x3325
                    r2 = r11
                L_0x43ef:
                    r4 = 4
                    if (r2 >= r4) goto L_0x3325
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x3ad7 }
                    if (r4 == 0) goto L_0x45ba
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.i     // Catch:{ all -> 0x3ad7 }
                    r4.i = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x3ad7 }
                    r8 = 9
                    r7 = r7[r8]     // Catch:{ all -> 0x3ad7 }
                    int r7 = r7.i     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r7]     // Catch:{ all -> 0x3ad7 }
                    r7 = 64
                    r8 = 64
                    r4.setPic(r5, r6, r7, r8)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.j     // Catch:{ all -> 0x3ad7 }
                    r4.j = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.k     // Catch:{ all -> 0x3ad7 }
                    r4.k = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.l     // Catch:{ all -> 0x3ad7 }
                    r4.l = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r2]     // Catch:{ all -> 0x3ad7 }
                    int r5 = r5.m     // Catch:{ all -> 0x3ad7 }
                    r4.m = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 255(0xff, float:3.57E-43)
                    r4.r = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1065353216(0x3f800000, float:1.0)
                    r4.u = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.b = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x3ad7 }
                    r4.c = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 500(0x1f4, double:2.47E-321)
                    r4.d = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    if (r4 <= 0) goto L_0x459d
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.d     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.K     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    r4.i = r5     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r4 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x3ad7 }
                    r6 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    int r6 = r6.i     // Catch:{ all -> 0x3ad7 }
                    r5 = r5[r6]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x3ad7 }
                    r2 = r7[r2]     // Catch:{ all -> 0x3ad7 }
                    int r2 = r2.i     // Catch:{ all -> 0x3ad7 }
                    r2 = r6[r2]     // Catch:{ all -> 0x3ad7 }
                    r6 = 64
                    r7 = 64
                    r4.setPic(r5, r2, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.K     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r2.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x3ad7 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x3ad7 }
                    goto L_0x3325
                L_0x459d:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x3ad7 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x3ad7 }
                    int r4 = r4.t     // Catch:{ all -> 0x3ad7 }
                    if (r4 != 0) goto L_0x3325
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x3ad7 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x3ad7 }
                    r2 = r4[r2]     // Catch:{ all -> 0x3ad7 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x3ad7 }
                    goto L_0x3325
                L_0x45ba:
                    int r2 = r2 + 1
                    goto L_0x43ef
                L_0x45be:
                    r0 = move-exception
                    monitor-exit(r3)     // Catch:{ all -> 0x45be }
                    throw r0     // Catch:{ all -> 0x45c1 }
                L_0x45c1:
                    r0 = move-exception
                    r1 = r2
                    goto L_0x3ad9
                L_0x45c5:
                    boolean r2 = r14.c     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x45da
                    boolean r2 = r14.d     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x45d1
                    r2 = r11
                L_0x45ce:
                    r4 = 4
                    if (r2 < r4) goto L_0x4f41
                L_0x45d1:
                    boolean r2 = r14.e     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x45da
                    r2 = 4
                L_0x45d6:
                    r4 = 8
                    if (r2 < r4) goto L_0x5580
                L_0x45da:
                    boolean r2 = r14.d     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x46a5
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x5559 }
                    long r6 = r2.c     // Catch:{ all -> 0x5559 }
                    long r4 = r4 - r6
                    r6 = 500(0x1f4, double:2.47E-321)
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x4606
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                    r2 = 0
                    r14.d = r2     // Catch:{ all -> 0x5559 }
                L_0x4606:
                    float r2 = r14.b     // Catch:{ all -> 0x5559 }
                    r4 = 1129840640(0x43580000, float:216.0)
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x4616
                    r2 = 0
                    r14.b = r2     // Catch:{ all -> 0x5559 }
                L_0x4616:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x463f
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.D     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r12
                    r2.j = r4     // Catch:{ all -> 0x5559 }
                L_0x463f:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4669
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.D     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 3
                    r2.j = r4     // Catch:{ all -> 0x5559 }
                L_0x4669:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.j     // Catch:{ all -> 0x5559 }
                    r4 = 54
                    if (r2 >= r4) goto L_0x4687
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x5559 }
                L_0x4687:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.j     // Catch:{ all -> 0x5559 }
                    r4 = 74
                    if (r2 <= r4) goto L_0x46a5
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.b = r4     // Catch:{ all -> 0x5559 }
                L_0x46a5:
                    boolean r2 = r14.e     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4770
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r6 = 0
                    r2 = r2[r6]     // Catch:{ all -> 0x5559 }
                    long r6 = r2.c     // Catch:{ all -> 0x5559 }
                    long r4 = r4 - r6
                    r6 = 500(0x1f4, double:2.47E-321)
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x46d1
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                    r2 = 0
                    r14.e = r2     // Catch:{ all -> 0x5559 }
                L_0x46d1:
                    float r2 = r14.b     // Catch:{ all -> 0x5559 }
                    r4 = 1129840640(0x43580000, float:216.0)
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x46e1
                    r2 = 0
                    r14.b = r2     // Catch:{ all -> 0x5559 }
                L_0x46e1:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x470a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.D     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r12
                    r2.j = r4     // Catch:{ all -> 0x5559 }
                L_0x470a:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4734
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.D     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 3
                    r2.j = r4     // Catch:{ all -> 0x5559 }
                L_0x4734:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.j     // Catch:{ all -> 0x5559 }
                    r4 = 54
                    if (r2 >= r4) goto L_0x4752
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x5559 }
                L_0x4752:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.j     // Catch:{ all -> 0x5559 }
                    r4 = 74
                    if (r2 <= r4) goto L_0x4770
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.b = r4     // Catch:{ all -> 0x5559 }
                L_0x4770:
                    r2 = r11
                L_0x4771:
                    if (r2 < r13) goto L_0x5bc3
                    r2 = r13
                L_0x4774:
                    r4 = 4
                    if (r2 < r4) goto L_0x5c61
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x47e5
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    long r6 = r2.c     // Catch:{ all -> 0x5559 }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    long r6 = r2.d     // Catch:{ all -> 0x5559 }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x47aa
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                L_0x47aa:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x5559 }
                    float r4 = r4.u     // Catch:{ all -> 0x5559 }
                    r5 = 1036831949(0x3dcccccd, float:0.1)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    int r2 = r2.r     // Catch:{ all -> 0x5559 }
                    r4 = 10
                    if (r2 <= r4) goto L_0x47e5
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x5559 }
                    int r4 = r4.r     // Catch:{ all -> 0x5559 }
                    r5 = 20
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x5559 }
                L_0x47e5:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4815
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4815:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4845
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4845:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4875
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4875:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x48a5
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x48a5:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x48d5
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x48d5:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 5
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4905
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4905:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 6
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4935
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4935:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 7
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4965
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4965:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4995
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4995:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x49c5
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x49c5:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x49f5
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x49f5:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4a25
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4a25:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 4
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4a55
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4a55:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 5
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4a85
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4a85:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 6
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4ab5
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4ab5:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 7
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r2 = r14.checkCardOK(r2, r4)     // Catch:{ all -> 0x5559 }
                    if (r2 == 0) goto L_0x4ae5
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                L_0x4ae5:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.t     // Catch:{ all -> 0x5559 }
                    if (r2 <= 0) goto L_0x5d14
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r5 = r5.K     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r2.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r4 = r2.K     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r4 = r2.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r2.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r2.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r2.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r2.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 255(0xff, float:3.57E-43)
                    r2.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r2.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r2.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r2.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r2.backDef()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.f = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r2.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r2.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                L_0x4c79:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.t     // Catch:{ all -> 0x5559 }
                    if (r2 <= 0) goto L_0x5ef4
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r5 = r5.K     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r2.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r4 = r2.K     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r4 = r2.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r2.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r2.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r2.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r2.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 255(0xff, float:3.57E-43)
                    r2.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r2.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r2.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r2.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r2.backDef()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.f = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r2.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r2.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                L_0x4e0d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e9a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e9a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e9a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e9a
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r2.backDef()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 48
                    r2.f = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 255(0xff, float:3.57E-43)
                    r2.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r2.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 3000(0xbb8, double:1.482E-320)
                    r2.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.s = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    r4 = 4
                    r2.n = r4     // Catch:{ all -> 0x5559 }
                L_0x4e9a:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4f28
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4f28
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4f28
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x5559 }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    boolean r2 = r2.f191a     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4f28
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r2.backDef()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 96
                    r2.f = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 255(0xff, float:3.57E-43)
                    r2.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r2.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 3000(0xbb8, double:1.482E-320)
                    r2.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    r4 = 4
                    r2.n = r4     // Catch:{ all -> 0x5559 }
                L_0x4f28:
                    android.view.SurfaceHolder r2 = r14.o     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    android.graphics.Canvas r2 = r2.lockCanvas(r4)     // Catch:{ all -> 0x5559 }
                    android.view.SurfaceHolder r3 = r14.o     // Catch:{ all -> 0x60d6 }
                    monitor-enter(r3)     // Catch:{ all -> 0x60d6 }
                    if (r2 == 0) goto L_0x4f37
                    r14.doDraw(r2)     // Catch:{ all -> 0x60d3 }
                L_0x4f37:
                    monitor-exit(r3)     // Catch:{ all -> 0x60d3 }
                    if (r2 == 0) goto L_0x0020
                    android.view.SurfaceHolder r3 = r14.o
                    r3.unlockCanvasAndPost(r2)
                    goto L_0x0020
                L_0x4f41:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    float r4 = (float) r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    float r5 = r14.f190a     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 >= 0) goto L_0x5538
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    float r4 = (float) r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    float r5 = r14.b     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 >= 0) goto L_0x5538
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.g     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + r5
                    float r4 = (float) r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    float r5 = r14.f190a     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 <= 0) goto L_0x5538
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.h     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + r5
                    float r4 = (float) r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    float r5 = r14.b     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 <= 0) goto L_0x5538
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x5538
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x5240
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    r1.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r1.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r1.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r1.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r1.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 255(0xff, float:3.57E-43)
                    r1.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r1.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r1.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r1.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r1.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r1.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r1.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.t     // Catch:{ all -> 0x5559 }
                    if (r1 <= 0) goto L_0x553c
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r5 = r5.K     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r1.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r4 = r1.K     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 1
                    r1.K = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r4 = r1.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r10
                    r1.t = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                L_0x523f:
                    r1 = r11
                L_0x5240:
                    if (r1 == 0) goto L_0x54b9
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x54b9
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    r1.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r1.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r1.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r1.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r1.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 255(0xff, float:3.57E-43)
                    r1.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r1.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r1.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r1.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r1.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r1.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r1.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 3
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.t     // Catch:{ all -> 0x5559 }
                    if (r1 <= 0) goto L_0x5563
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r5 = r5.K     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r1.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r4 = r1.K     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 1
                    r1.K = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r4 = r1.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r10
                    r1.t = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                L_0x54b8:
                    r1 = r11
                L_0x54b9:
                    if (r1 == 0) goto L_0x5534
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r1 = r14.checkCardOK(r1, r4)     // Catch:{ all -> 0x5559 }
                    if (r1 != 0) goto L_0x5534
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r1 = r14.checkCardOK(r1, r4)     // Catch:{ all -> 0x5559 }
                    if (r1 != 0) goto L_0x5534
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r1.backDef()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    r1 = 1
                    r14.d = r1     // Catch:{ all -> 0x5559 }
                L_0x5534:
                    r1 = 0
                    r14.c = r1     // Catch:{ all -> 0x5559 }
                    r1 = r10
                L_0x5538:
                    int r2 = r2 + 1
                    goto L_0x45ce
                L_0x553c:
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.t     // Catch:{ all -> 0x5559 }
                    if (r1 != 0) goto L_0x523f
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    goto L_0x523f
                L_0x5559:
                    r0 = move-exception
                    r1 = r3
                L_0x555b:
                    if (r1 == 0) goto L_0x5562
                    android.view.SurfaceHolder r2 = r14.o
                    r2.unlockCanvasAndPost(r1)
                L_0x5562:
                    throw r0
                L_0x5563:
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.t     // Catch:{ all -> 0x5559 }
                    if (r1 != 0) goto L_0x54b8
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    goto L_0x54b8
                L_0x5580:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    float r4 = (float) r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    float r5 = r14.f190a     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 >= 0) goto L_0x5b85
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    float r4 = (float) r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    float r5 = r14.b     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 >= 0) goto L_0x5b85
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.g     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + r5
                    float r4 = (float) r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    float r5 = r14.f190a     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 <= 0) goto L_0x5b85
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.h     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + r5
                    float r4 = (float) r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    float r5 = r5.g     // Catch:{ all -> 0x5559 }
                    float r4 = r4 * r5
                    float r5 = r14.b     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 <= 0) goto L_0x5b85
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x5b85
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x587f
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    r1.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r1.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r1.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r1.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r1.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 255(0xff, float:3.57E-43)
                    r1.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r1.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r1.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r1.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r1.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r1.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r1.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.t     // Catch:{ all -> 0x5559 }
                    if (r1 <= 0) goto L_0x5b89
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r5 = r5.L     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r1.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r4 = r1.L     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 1
                    r1.L = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r4 = r1.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r10
                    r1.t = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                L_0x587e:
                    r1 = r11
                L_0x587f:
                    if (r1 == 0) goto L_0x5af8
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    boolean r4 = r14.checkCardOK(r4, r5)     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x5af8
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    r1.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r1.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r1.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r1.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r1.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 255(0xff, float:3.57E-43)
                    r1.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1065353216(0x3f800000, float:1.0)
                    r1.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.j     // Catch:{ all -> 0x5559 }
                    r1.j = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.k     // Catch:{ all -> 0x5559 }
                    r1.k = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.l     // Catch:{ all -> 0x5559 }
                    r1.l = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.m     // Catch:{ all -> 0x5559 }
                    r1.m = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.r = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r1.u = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 500(0x1f4, double:2.47E-321)
                    r1.d = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.F     // Catch:{ all -> 0x5559 }
                    r4 = 2
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.t     // Catch:{ all -> 0x5559 }
                    if (r1 <= 0) goto L_0x5ba6
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.d     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r5 = r5.L     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r1.i = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r4 = r4.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r4 = r1.L     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 1
                    r1.L = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r4 = r1.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r10
                    r1.t = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r1.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                L_0x5af7:
                    r1 = r11
                L_0x5af8:
                    if (r1 == 0) goto L_0x5b81
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 9
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r1 = r14.checkCardOK(r1, r4)     // Catch:{ all -> 0x5559 }
                    if (r1 != 0) goto L_0x5b81
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r4 = 8
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.i     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.i     // Catch:{ all -> 0x5559 }
                    boolean r1 = r14.checkCardOK(r1, r4)     // Catch:{ all -> 0x5559 }
                    if (r1 != 0) goto L_0x5b81
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r1.backDef()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 180(0xb4, float:2.52E-43)
                    r1.s = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.b = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.D     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r1.c = r4     // Catch:{ all -> 0x5559 }
                    r1 = 1
                    r14.e = r1     // Catch:{ all -> 0x5559 }
                L_0x5b81:
                    r1 = 0
                    r14.c = r1     // Catch:{ all -> 0x5559 }
                    r1 = r10
                L_0x5b85:
                    int r2 = r2 + 1
                    goto L_0x45d6
                L_0x5b89:
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.t     // Catch:{ all -> 0x5559 }
                    if (r1 != 0) goto L_0x587e
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    goto L_0x587e
                L_0x5ba6:
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1 = r1[r4]     // Catch:{ all -> 0x5559 }
                    int r1 = r1.t     // Catch:{ all -> 0x5559 }
                    if (r1 != 0) goto L_0x5af7
                    hamon05.speed.pac.main$HelloView r1 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r1 = r1.A     // Catch:{ all -> 0x5559 }
                    r1 = r1[r2]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r1.f191a = r4     // Catch:{ all -> 0x5559 }
                    goto L_0x5af7
                L_0x5bc3:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x5c5d
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.F     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    long r6 = r6.c     // Catch:{ all -> 0x5559 }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.F     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    long r6 = r6.d     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r4 <= 0) goto L_0x5bfe
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x5559 }
                L_0x5bfe:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.F     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    float r5 = r5.u     // Catch:{ all -> 0x5559 }
                    r6 = 1036831949(0x3dcccccd, float:0.1)
                    float r5 = r5 + r6
                    r4.u = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.r     // Catch:{ all -> 0x5559 }
                    r5 = 20
                    if (r4 <= r5) goto L_0x5c43
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.F     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.r     // Catch:{ all -> 0x5559 }
                    r6 = 20
                    int r5 = r5 - r6
                    r4.r = r5     // Catch:{ all -> 0x5559 }
                L_0x5c43:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.r     // Catch:{ all -> 0x5559 }
                    if (r4 >= 0) goto L_0x5c5d
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4.r = r5     // Catch:{ all -> 0x5559 }
                L_0x5c5d:
                    int r2 = r2 + 1
                    goto L_0x4771
                L_0x5c61:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x5d10
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.F     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    long r6 = r6.c     // Catch:{ all -> 0x5559 }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.F     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    long r6 = r6.d     // Catch:{ all -> 0x5559 }
                    int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r4 <= 0) goto L_0x5c9c
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x5559 }
                L_0x5c9c:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    float r4 = r4.u     // Catch:{ all -> 0x5559 }
                    r5 = 1066192077(0x3f8ccccd, float:1.1)
                    int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
                    if (r4 <= 0) goto L_0x5cca
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.F     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    float r5 = r5.u     // Catch:{ all -> 0x5559 }
                    r6 = 1036831949(0x3dcccccd, float:0.1)
                    float r5 = r5 - r6
                    r4.u = r5     // Catch:{ all -> 0x5559 }
                L_0x5cca:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.r     // Catch:{ all -> 0x5559 }
                    r5 = 255(0xff, float:3.57E-43)
                    if (r4 >= r5) goto L_0x5cf3
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.F     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.r     // Catch:{ all -> 0x5559 }
                    int r5 = r5 + 20
                    r4.r = r5     // Catch:{ all -> 0x5559 }
                L_0x5cf3:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.r     // Catch:{ all -> 0x5559 }
                    r5 = 255(0xff, float:3.57E-43)
                    if (r4 <= r5) goto L_0x5d10
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    r5 = 255(0xff, float:3.57E-43)
                    r4.r = r5     // Catch:{ all -> 0x5559 }
                L_0x5d10:
                    int r2 = r2 + 1
                    goto L_0x4774
                L_0x5d14:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.t     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4c79
                    r2 = 4
                L_0x5d24:
                    r4 = 8
                    if (r2 >= r4) goto L_0x4c79
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x5ef0
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4.i = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 8
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x5559 }
                    r8 = 8
                    r7 = r7[r8]     // Catch:{ all -> 0x5559 }
                    int r7 = r7.i     // Catch:{ all -> 0x5559 }
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    r7 = 64
                    r8 = 64
                    r4.setPic(r5, r6, r7, r8)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.j     // Catch:{ all -> 0x5559 }
                    r4.j = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.k     // Catch:{ all -> 0x5559 }
                    r4.k = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.l     // Catch:{ all -> 0x5559 }
                    r4.l = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.m     // Catch:{ all -> 0x5559 }
                    r4.m = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 255(0xff, float:3.57E-43)
                    r4.r = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 1065353216(0x3f800000, float:1.0)
                    r4.u = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4.b = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r4.c = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 500(0x1f4, double:2.47E-321)
                    r4.d = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    if (r4 <= 0) goto L_0x5ed3
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.d     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r6 = r6.K     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r4.i = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x5559 }
                    r2 = r7[r2]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    r2 = r6[r2]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r4.setPic(r5, r2, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r4 = r2.K     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r4 = r2.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    goto L_0x4c79
                L_0x5ed3:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    if (r4 != 0) goto L_0x4c79
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r2 = r4[r2]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                    goto L_0x4c79
                L_0x5ef0:
                    int r2 = r2 + 1
                    goto L_0x5d24
                L_0x5ef4:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.t     // Catch:{ all -> 0x5559 }
                    if (r2 != 0) goto L_0x4e0d
                    r2 = r11
                L_0x5f04:
                    r4 = 4
                    if (r2 >= r4) goto L_0x4e0d
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    boolean r4 = r4.f191a     // Catch:{ all -> 0x5559 }
                    if (r4 == 0) goto L_0x60cf
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.i     // Catch:{ all -> 0x5559 }
                    r4.i = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r5 = 9
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x5559 }
                    r8 = 9
                    r7 = r7[r8]     // Catch:{ all -> 0x5559 }
                    int r7 = r7.i     // Catch:{ all -> 0x5559 }
                    r6 = r6[r7]     // Catch:{ all -> 0x5559 }
                    r7 = 64
                    r8 = 64
                    r4.setPic(r5, r6, r7, r8)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.j     // Catch:{ all -> 0x5559 }
                    r4.j = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.k     // Catch:{ all -> 0x5559 }
                    r4.k = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.l     // Catch:{ all -> 0x5559 }
                    r4.l = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x5559 }
                    r5 = r5[r2]     // Catch:{ all -> 0x5559 }
                    int r5 = r5.m     // Catch:{ all -> 0x5559 }
                    r4.m = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 255(0xff, float:3.57E-43)
                    r4.r = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 1065353216(0x3f800000, float:1.0)
                    r4.u = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4.b = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x5559 }
                    r4.c = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 500(0x1f4, double:2.47E-321)
                    r4.d = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4.f191a = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    if (r4 <= 0) goto L_0x60b2
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.d     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r6 = r6.K     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    r4.i = r5     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r4 = r4[r2]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r5 = r5.e     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x5559 }
                    r6 = r6[r2]     // Catch:{ all -> 0x5559 }
                    int r6 = r6.i     // Catch:{ all -> 0x5559 }
                    r5 = r5[r6]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int[] r6 = r6.f     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r7 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r7 = r7.A     // Catch:{ all -> 0x5559 }
                    r2 = r7[r2]     // Catch:{ all -> 0x5559 }
                    int r2 = r2.i     // Catch:{ all -> 0x5559 }
                    r2 = r6[r2]     // Catch:{ all -> 0x5559 }
                    r6 = 64
                    r7 = 64
                    r4.setPic(r5, r2, r6, r7)     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    int r4 = r2.K     // Catch:{ all -> 0x5559 }
                    int r4 = r4 + 1
                    r2.K = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    int r4 = r2.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 - r10
                    r2.t = r4     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x5559 }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x5559 }
                    goto L_0x4e0d
                L_0x60b2:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x5559 }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x5559 }
                    int r4 = r4.t     // Catch:{ all -> 0x5559 }
                    if (r4 != 0) goto L_0x4e0d
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x5559 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x5559 }
                    r2 = r4[r2]     // Catch:{ all -> 0x5559 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x5559 }
                    goto L_0x4e0d
                L_0x60cf:
                    int r2 = r2 + 1
                    goto L_0x5f04
                L_0x60d3:
                    r0 = move-exception
                    monitor-exit(r3)     // Catch:{ all -> 0x60d3 }
                    throw r0     // Catch:{ all -> 0x60d6 }
                L_0x60d6:
                    r0 = move-exception
                    r1 = r2
                    goto L_0x555b
                L_0x60da:
                    int r2 = r14.l     // Catch:{ all -> 0x614c }
                    switch(r2) {
                        case 0: goto L_0x60f8;
                        case 1: goto L_0x6156;
                        case 2: goto L_0x629e;
                        case 3: goto L_0x7116;
                        default: goto L_0x60df;
                    }     // Catch:{ all -> 0x614c }
                L_0x60df:
                    android.view.SurfaceHolder r2 = r14.o     // Catch:{ all -> 0x614c }
                    r4 = 0
                    android.graphics.Canvas r2 = r2.lockCanvas(r4)     // Catch:{ all -> 0x614c }
                    android.view.SurfaceHolder r3 = r14.o     // Catch:{ all -> 0x71ae }
                    monitor-enter(r3)     // Catch:{ all -> 0x71ae }
                    if (r2 == 0) goto L_0x60ee
                    r14.doDraw(r2)     // Catch:{ all -> 0x71ab }
                L_0x60ee:
                    monitor-exit(r3)     // Catch:{ all -> 0x71ab }
                    if (r2 == 0) goto L_0x0020
                    android.view.SurfaceHolder r3 = r14.o
                    r3.unlockCanvasAndPost(r2)
                    goto L_0x0020
                L_0x60f8:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.backDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.f = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.backDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.f = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    r2 = 1
                    r14.l = r2     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x614c }
                    goto L_0x60df
                L_0x614c:
                    r0 = move-exception
                    r1 = r3
                L_0x614e:
                    if (r1 == 0) goto L_0x6155
                    android.view.SurfaceHolder r2 = r14.o
                    r2.unlockCanvasAndPost(r1)
                L_0x6155:
                    throw r0
                L_0x6156:
                    boolean r2 = r14.c     // Catch:{ all -> 0x614c }
                    if (r2 == 0) goto L_0x60df
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.j     // Catch:{ all -> 0x614c }
                    float r2 = (float) r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    float r4 = r4.g     // Catch:{ all -> 0x614c }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x614c }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x61fb
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.k     // Catch:{ all -> 0x614c }
                    float r2 = (float) r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    float r4 = r4.g     // Catch:{ all -> 0x614c }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x614c }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x61fb
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.j     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x614c }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.g     // Catch:{ all -> 0x614c }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    float r4 = r4.g     // Catch:{ all -> 0x614c }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x614c }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x61fb
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.k     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x614c }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.h     // Catch:{ all -> 0x614c }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    float r4 = r4.g     // Catch:{ all -> 0x614c }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x614c }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x61fb
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x614c }
                    r2.c = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x614c }
                    r2 = 2
                    r14.l = r2     // Catch:{ all -> 0x614c }
                L_0x61fb:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.j     // Catch:{ all -> 0x614c }
                    float r2 = (float) r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    float r4 = r4.g     // Catch:{ all -> 0x614c }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x614c }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x60df
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.k     // Catch:{ all -> 0x614c }
                    float r2 = (float) r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    float r4 = r4.g     // Catch:{ all -> 0x614c }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x614c }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 >= 0) goto L_0x60df
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.j     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x614c }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.g     // Catch:{ all -> 0x614c }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    float r4 = r4.g     // Catch:{ all -> 0x614c }
                    float r2 = r2 * r4
                    float r4 = r14.f190a     // Catch:{ all -> 0x614c }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x60df
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.k     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x614c }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.h     // Catch:{ all -> 0x614c }
                    int r2 = r2 + r4
                    float r2 = (float) r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    float r4 = r4.g     // Catch:{ all -> 0x614c }
                    float r2 = r2 * r4
                    float r4 = r14.b     // Catch:{ all -> 0x614c }
                    int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r2 <= 0) goto L_0x60df
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x614c }
                    r2.c = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 250(0xfa, double:1.235E-321)
                    r2.d = r4     // Catch:{ all -> 0x614c }
                    r2 = 3
                    r14.l = r2     // Catch:{ all -> 0x614c }
                    goto L_0x60df
                L_0x629e:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x614c }
                    long r6 = r2.c     // Catch:{ all -> 0x614c }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r6 = 1
                    r2 = r2[r6]     // Catch:{ all -> 0x614c }
                    long r6 = r2.d     // Catch:{ all -> 0x614c }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x70ca
                    byte r2 = r14.f     // Catch:{ all -> 0x614c }
                    if (r2 != r12) goto L_0x69db
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r2.shuffle(r4)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r2.backDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r2.u = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.r = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 64
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 272(0x110, float:3.81E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x614c }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.t     // Catch:{ all -> 0x614c }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x614c }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.t     // Catch:{ all -> 0x614c }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 0
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 0
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 1
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 1
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 2
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 2
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 3
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 3
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2.K = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 25
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 4
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 4
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 26
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 5
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 5
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 27
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 6
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 6
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 28
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 7
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 7
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    r4 = 29
                    r2.L = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 50
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 51
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x614c }
                    r14.m = r4     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.n = r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.l = r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.n = r4     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x614c }
                L_0x69db:
                    byte r2 = r14.f     // Catch:{ all -> 0x614c }
                    if (r2 != r13) goto L_0x70ca
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 21
                    r2.t = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r2.shuffle(r4)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r2.backDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.f = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r4 = 1073741824(0x40000000, float:2.0)
                    r2.u = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.r = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 128(0x80, float:1.794E-43)
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r5 = 0
                    r6 = 64
                    r7 = 32
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 272(0x110, float:3.81E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.C     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x614c }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.t     // Catch:{ all -> 0x614c }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 16
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 128(0x80, float:1.794E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x614c }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.t     // Catch:{ all -> 0x614c }
                    int r4 = r4 * 64
                    r5 = 0
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 240(0xf0, float:3.36E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.B     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 0
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 0
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 0
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 1
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 1
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 2
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 2
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 2
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 2
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 3
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 3
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 320(0x140, float:4.48E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2.K = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 25
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 4
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 4
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 32
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 4
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 26
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 5
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 5
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 5
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 27
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 6
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 6
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 6
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 28
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 7
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 7
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 224(0xe0, float:3.14E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 48
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 180(0xb4, float:2.52E-43)
                    r2.s = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 7
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    r4 = 29
                    r2.L = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 50
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 8
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 8
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 96
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 8
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.d     // Catch:{ all -> 0x614c }
                    r5 = 51
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    r2.i = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r4 = r4.e     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r5 = r5.A     // Catch:{ all -> 0x614c }
                    r6 = 9
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    int r5 = r5.i     // Catch:{ all -> 0x614c }
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r5 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    int[] r5 = r5.f     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r6 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r6 = r6.A     // Catch:{ all -> 0x614c }
                    r7 = 9
                    r6 = r6[r7]     // Catch:{ all -> 0x614c }
                    int r6 = r6.i     // Catch:{ all -> 0x614c }
                    r5 = r5[r6]     // Catch:{ all -> 0x614c }
                    r6 = 64
                    r7 = 64
                    r2.setPic(r4, r5, r6, r7)     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 160(0xa0, float:2.24E-43)
                    r2.j = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 184(0xb8, float:2.58E-43)
                    r2.k = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.A     // Catch:{ all -> 0x614c }
                    r4 = 9
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r2.setDef()     // Catch:{ all -> 0x614c }
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x614c }
                    r14.m = r4     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.n = r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.l = r2     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2.n = r4     // Catch:{ all -> 0x614c }
                L_0x70ca:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x614c }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    float r4 = r4.u     // Catch:{ all -> 0x614c }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.r     // Catch:{ all -> 0x614c }
                    r4 = 40
                    if (r2 <= r4) goto L_0x60df
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x614c }
                    r5 = 1
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.r     // Catch:{ all -> 0x614c }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x614c }
                    goto L_0x60df
                L_0x7116:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x614c }
                    long r6 = r2.c     // Catch:{ all -> 0x614c }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r6 = 3
                    r2 = r2[r6]     // Catch:{ all -> 0x614c }
                    long r6 = r2.d     // Catch:{ all -> 0x614c }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x715f
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 1
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.l = r2     // Catch:{ all -> 0x614c }
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    r4 = 0
                    r2.n = r4     // Catch:{ all -> 0x614c }
                L_0x715f:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x614c }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    float r4 = r4.u     // Catch:{ all -> 0x614c }
                    r5 = 1028443341(0x3d4ccccd, float:0.05)
                    float r4 = r4 + r5
                    r2.u = r4     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    int r2 = r2.r     // Catch:{ all -> 0x614c }
                    r4 = 40
                    if (r2 <= r4) goto L_0x60df
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r2 = r2.H     // Catch:{ all -> 0x614c }
                    r4 = 3
                    r2 = r2[r4]     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x614c }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.H     // Catch:{ all -> 0x614c }
                    r5 = 3
                    r4 = r4[r5]     // Catch:{ all -> 0x614c }
                    int r4 = r4.r     // Catch:{ all -> 0x614c }
                    r5 = 40
                    int r4 = r4 - r5
                    r2.r = r4     // Catch:{ all -> 0x614c }
                    goto L_0x60df
                L_0x71ab:
                    r0 = move-exception
                    monitor-exit(r3)     // Catch:{ all -> 0x71ab }
                    throw r0     // Catch:{ all -> 0x71ae }
                L_0x71ae:
                    r0 = move-exception
                    r1 = r2
                    goto L_0x614e
                L_0x71b2:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    long r6 = r2.c     // Catch:{ all -> 0x73e9 }
                    long r4 = r4 - r6
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    long r6 = r2.d     // Catch:{ all -> 0x73e9 }
                    int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                    if (r2 <= 0) goto L_0x71fa
                    r2 = 0
                    r14.l = r2     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    r2.backDef()     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    r4 = 0
                    r2.f191a = r4     // Catch:{ all -> 0x73e9 }
                    r2 = r11
                L_0x71e6:
                    if (r2 < r13) goto L_0x7388
                    r2 = r11
                L_0x71e9:
                    r4 = 10
                    if (r2 < r4) goto L_0x73b0
                    r2 = r11
                L_0x71ee:
                    r4 = 4
                    if (r2 < r4) goto L_0x73c0
                    r2 = 0
                    r14.c = r2     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    r4 = 5
                    r2.n = r4     // Catch:{ all -> 0x73e9 }
                L_0x71fa:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.f     // Catch:{ all -> 0x73e9 }
                    r4 = 48
                    if (r2 != r4) goto L_0x722b
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.s     // Catch:{ all -> 0x73e9 }
                    r4 = 360(0x168, float:5.04E-43)
                    if (r2 >= r4) goto L_0x722b
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x73e9 }
                    int r4 = r4.s     // Catch:{ all -> 0x73e9 }
                    int r4 = r4 + 10
                    r2.s = r4     // Catch:{ all -> 0x73e9 }
                L_0x722b:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.f     // Catch:{ all -> 0x73e9 }
                    r4 = 96
                    if (r2 != r4) goto L_0x725c
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.s     // Catch:{ all -> 0x73e9 }
                    r4 = 540(0x21c, float:7.57E-43)
                    if (r2 >= r4) goto L_0x725c
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x73e9 }
                    int r4 = r4.s     // Catch:{ all -> 0x73e9 }
                    int r4 = r4 + 10
                    r2.s = r4     // Catch:{ all -> 0x73e9 }
                L_0x725c:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.f     // Catch:{ all -> 0x73e9 }
                    r4 = 144(0x90, float:2.02E-43)
                    if (r2 != r4) goto L_0x72f0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.k     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x73e9 }
                    int r4 = r4.o     // Catch:{ all -> 0x73e9 }
                    r5 = 10
                    int r4 = r4 - r5
                    if (r2 >= r4) goto L_0x728d
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x73e9 }
                L_0x728d:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.k     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x73e9 }
                    int r4 = r4.o     // Catch:{ all -> 0x73e9 }
                    int r4 = r4 + 10
                    if (r2 <= r4) goto L_0x72af
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    r4 = 0
                    r2.b = r4     // Catch:{ all -> 0x73e9 }
                L_0x72af:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x73e9 }
                    if (r2 == 0) goto L_0x72d0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x73e9 }
                    int r4 = r4.k     // Catch:{ all -> 0x73e9 }
                    int r4 = r4 + 2
                    r2.k = r4     // Catch:{ all -> 0x73e9 }
                L_0x72d0:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x73e9 }
                    if (r2 != 0) goto L_0x72f0
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x73e9 }
                    int r4 = r4.k     // Catch:{ all -> 0x73e9 }
                    int r4 = r4 - r13
                    r2.k = r4     // Catch:{ all -> 0x73e9 }
                L_0x72f0:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.f     // Catch:{ all -> 0x73e9 }
                    r4 = 192(0xc0, float:2.69E-43)
                    if (r2 != r4) goto L_0x736f
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.s     // Catch:{ all -> 0x73e9 }
                    r4 = -20
                    if (r2 >= r4) goto L_0x7316
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    r4 = 1
                    r2.b = r4     // Catch:{ all -> 0x73e9 }
                L_0x7316:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    int r2 = r2.s     // Catch:{ all -> 0x73e9 }
                    r4 = 20
                    if (r2 <= r4) goto L_0x732e
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    r4 = 0
                    r2.b = r4     // Catch:{ all -> 0x73e9 }
                L_0x732e:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x73e9 }
                    if (r2 == 0) goto L_0x734f
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x73e9 }
                    int r4 = r4.s     // Catch:{ all -> 0x73e9 }
                    int r4 = r4 + 2
                    r2.s = r4     // Catch:{ all -> 0x73e9 }
                L_0x734f:
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    boolean r2 = r2.b     // Catch:{ all -> 0x73e9 }
                    if (r2 != 0) goto L_0x736f
                    hamon05.speed.pac.main$HelloView r2 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r2 = r2.E     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite r4 = r4.E     // Catch:{ all -> 0x73e9 }
                    int r4 = r4.s     // Catch:{ all -> 0x73e9 }
                    int r4 = r4 - r13
                    r2.s = r4     // Catch:{ all -> 0x73e9 }
                L_0x736f:
                    android.view.SurfaceHolder r2 = r14.o     // Catch:{ all -> 0x73e9 }
                    r4 = 0
                    android.graphics.Canvas r2 = r2.lockCanvas(r4)     // Catch:{ all -> 0x73e9 }
                    android.view.SurfaceHolder r3 = r14.o     // Catch:{ all -> 0x73df }
                    monitor-enter(r3)     // Catch:{ all -> 0x73df }
                    if (r2 == 0) goto L_0x737e
                    r14.doDraw(r2)     // Catch:{ all -> 0x73dc }
                L_0x737e:
                    monitor-exit(r3)     // Catch:{ all -> 0x73dc }
                    if (r2 == 0) goto L_0x0020
                    android.view.SurfaceHolder r3 = r14.o
                    r3.unlockCanvasAndPost(r2)
                    goto L_0x0020
                L_0x7388:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.C     // Catch:{ all -> 0x73e9 }
                    r4 = r4[r2]     // Catch:{ all -> 0x73e9 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.D     // Catch:{ all -> 0x73e9 }
                    r4 = r4[r2]     // Catch:{ all -> 0x73e9 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.B     // Catch:{ all -> 0x73e9 }
                    r4 = r4[r2]     // Catch:{ all -> 0x73e9 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x73e9 }
                    int r2 = r2 + 1
                    goto L_0x71e6
                L_0x73b0:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.A     // Catch:{ all -> 0x73e9 }
                    r4 = r4[r2]     // Catch:{ all -> 0x73e9 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x73e9 }
                    int r2 = r2 + 1
                    goto L_0x71e9
                L_0x73c0:
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.F     // Catch:{ all -> 0x73e9 }
                    r4 = r4[r2]     // Catch:{ all -> 0x73e9 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView r4 = hamon05.speed.pac.main.HelloView.this     // Catch:{ all -> 0x73e9 }
                    hamon05.speed.pac.main$HelloView$Sprite[] r4 = r4.J     // Catch:{ all -> 0x73e9 }
                    r4 = r4[r2]     // Catch:{ all -> 0x73e9 }
                    r5 = 0
                    r4.f191a = r5     // Catch:{ all -> 0x73e9 }
                    int r2 = r2 + 1
                    goto L_0x71ee
                L_0x73dc:
                    r0 = move-exception
                    monitor-exit(r3)     // Catch:{ all -> 0x73dc }
                    throw r0     // Catch:{ all -> 0x73df }
                L_0x73df:
                    r0 = move-exception
                    r1 = r2
                L_0x73e1:
                    if (r1 == 0) goto L_0x73e8
                    android.view.SurfaceHolder r2 = r14.o
                    r2.unlockCanvasAndPost(r1)
                L_0x73e8:
                    throw r0
                L_0x73e9:
                    r0 = move-exception
                    r1 = r3
                    goto L_0x73e1
                */
                throw new UnsupportedOperationException("Method not decompiled: hamon05.speed.pac.main.HelloView.HelloThread.run():void");
            }

            public void setRunning(boolean z) {
                this.p = z;
            }
        }

        class Sprite {
            /* access modifiers changed from: private */

            /* renamed from: a  reason: collision with root package name */
            public boolean f191a = false;
            /* access modifiers changed from: private */
            public boolean b = false;
            /* access modifiers changed from: private */
            public long c;
            /* access modifiers changed from: private */
            public long d;
            /* access modifiers changed from: private */
            public int e;
            /* access modifiers changed from: private */
            public int f;
            /* access modifiers changed from: private */
            public int g;
            /* access modifiers changed from: private */
            public int h;
            /* access modifiers changed from: private */
            public int i;
            /* access modifiers changed from: private */
            public int j;
            /* access modifiers changed from: private */
            public int k;
            /* access modifiers changed from: private */
            public int l;
            /* access modifiers changed from: private */
            public int m;
            private int n;
            /* access modifiers changed from: private */
            public int o;
            private int p;
            private int q;
            /* access modifiers changed from: private */
            public int r = 255;
            /* access modifiers changed from: private */
            public int s = 0;
            /* access modifiers changed from: private */
            public int t = 21;
            /* access modifiers changed from: private */
            public float u = 1.0f;

            Sprite() {
            }

            /* access modifiers changed from: private */
            public void backDef() {
                this.j = this.n;
                this.k = this.o;
                this.l = this.p;
                this.m = this.q;
                this.r = 255;
                this.u = 1.0f;
                this.s = 0;
                this.b = false;
            }

            /* access modifiers changed from: private */
            public void setDef() {
                this.n = this.j;
                this.o = this.k;
                this.p = this.l;
                this.q = this.m;
            }

            /* access modifiers changed from: private */
            public void setPic(int i2, int i3, int i4, int i5) {
                this.e = i2;
                this.f = i3;
                this.g = i4;
                this.h = i5;
                this.l = i4;
                this.m = i5;
            }
        }

        public HelloView(Context context, SharedPreferences sharedPreferences, float f2, float f3) {
            super(context);
            int[] iArr = new int[52];
            iArr[1] = 1;
            iArr[2] = 2;
            iArr[3] = 3;
            iArr[4] = 4;
            iArr[5] = 5;
            iArr[6] = 6;
            iArr[7] = 7;
            iArr[8] = 8;
            iArr[9] = 9;
            iArr[10] = 10;
            iArr[11] = 11;
            iArr[12] = 12;
            iArr[13] = 13;
            iArr[14] = 14;
            iArr[15] = 15;
            iArr[16] = 16;
            iArr[17] = 17;
            iArr[18] = 18;
            iArr[19] = 19;
            iArr[20] = 20;
            iArr[21] = 21;
            iArr[22] = 22;
            iArr[23] = 23;
            iArr[24] = 24;
            iArr[25] = 25;
            iArr[26] = 26;
            iArr[27] = 27;
            iArr[28] = 28;
            iArr[29] = 29;
            iArr[30] = 30;
            iArr[31] = 31;
            iArr[32] = 32;
            iArr[33] = 33;
            iArr[34] = 34;
            iArr[35] = 35;
            iArr[36] = 36;
            iArr[37] = 37;
            iArr[38] = 38;
            iArr[39] = 39;
            iArr[40] = 40;
            iArr[41] = 41;
            iArr[42] = 42;
            iArr[43] = 43;
            iArr[44] = 44;
            iArr[45] = 45;
            iArr[46] = 46;
            iArr[47] = 47;
            iArr[48] = 48;
            iArr[49] = 49;
            iArr[50] = 50;
            iArr[51] = 51;
            this.d = iArr;
            int[] iArr2 = new int[52];
            iArr2[1] = 64;
            iArr2[2] = 128;
            iArr2[3] = 192;
            iArr2[4] = 256;
            iArr2[5] = 320;
            iArr2[6] = 384;
            iArr2[7] = 448;
            iArr2[8] = 512;
            iArr2[9] = 576;
            iArr2[10] = 640;
            iArr2[11] = 704;
            iArr2[12] = 768;
            iArr2[14] = 64;
            iArr2[15] = 128;
            iArr2[16] = 192;
            iArr2[17] = 256;
            iArr2[18] = 320;
            iArr2[19] = 384;
            iArr2[20] = 448;
            iArr2[21] = 512;
            iArr2[22] = 576;
            iArr2[23] = 640;
            iArr2[24] = 704;
            iArr2[25] = 768;
            iArr2[27] = 64;
            iArr2[28] = 128;
            iArr2[29] = 192;
            iArr2[30] = 256;
            iArr2[31] = 320;
            iArr2[32] = 384;
            iArr2[33] = 448;
            iArr2[34] = 512;
            iArr2[35] = 576;
            iArr2[36] = 640;
            iArr2[37] = 704;
            iArr2[38] = 768;
            iArr2[40] = 64;
            iArr2[41] = 128;
            iArr2[42] = 192;
            iArr2[43] = 256;
            iArr2[44] = 320;
            iArr2[45] = 384;
            iArr2[46] = 448;
            iArr2[47] = 512;
            iArr2[48] = 576;
            iArr2[49] = 640;
            iArr2[50] = 704;
            iArr2[51] = 768;
            this.e = iArr2;
            int[] iArr3 = new int[52];
            iArr3[13] = 64;
            iArr3[14] = 64;
            iArr3[15] = 64;
            iArr3[16] = 64;
            iArr3[17] = 64;
            iArr3[18] = 64;
            iArr3[19] = 64;
            iArr3[20] = 64;
            iArr3[21] = 64;
            iArr3[22] = 64;
            iArr3[23] = 64;
            iArr3[24] = 64;
            iArr3[25] = 64;
            iArr3[26] = 128;
            iArr3[27] = 128;
            iArr3[28] = 128;
            iArr3[29] = 128;
            iArr3[30] = 128;
            iArr3[31] = 128;
            iArr3[32] = 128;
            iArr3[33] = 128;
            iArr3[34] = 128;
            iArr3[35] = 128;
            iArr3[36] = 128;
            iArr3[37] = 128;
            iArr3[38] = 128;
            iArr3[39] = 192;
            iArr3[40] = 192;
            iArr3[41] = 192;
            iArr3[42] = 192;
            iArr3[43] = 192;
            iArr3[44] = 192;
            iArr3[45] = 192;
            iArr3[46] = 192;
            iArr3[47] = 192;
            iArr3[48] = 192;
            iArr3[49] = 192;
            iArr3[50] = 192;
            iArr3[51] = 192;
            this.f = iArr3;
            this.M = null;
            SurfaceHolder holder = getHolder();
            holder.addCallback(this);
            this.M = new HelloThread(holder, context, new Handler());
            setFocusable(true);
            this.f183a = sharedPreferences;
            this.b = this.f183a.edit();
            this.g = f2;
            this.h = f3;
            shuffle(this.d);
            if (this.o == null) {
                this.o = BitmapFactory.decodeResource(getResources(), R.drawable.bg);
            }
            if (this.p == null) {
                this.p = BitmapFactory.decodeResource(getResources(), R.drawable.card_s01);
            }
            if (this.q == null) {
                this.q = BitmapFactory.decodeResource(getResources(), R.drawable.card_nokori);
            }
            if (this.r == null) {
                this.r = BitmapFactory.decodeResource(getResources(), R.drawable.player);
            }
            if (this.s == null) {
                this.s = BitmapFactory.decodeResource(getResources(), R.drawable.miss);
            }
            if (this.u == null) {
                this.u = BitmapFactory.decodeResource(getResources(), R.drawable.game_message);
            }
            if (this.v == null) {
                this.v = BitmapFactory.decodeResource(getResources(), R.drawable.card_eff);
            }
            if (this.w == null) {
                this.w = BitmapFactory.decodeResource(getResources(), R.drawable.menu_message);
            }
            if (this.x == null) {
                this.x = BitmapFactory.decodeResource(getResources(), R.drawable.title);
            }
            if (this.y == null) {
                this.y = BitmapFactory.decodeResource(getResources(), R.drawable.howto);
            }
            if (this.z == null) {
                this.z = BitmapFactory.decodeResource(getResources(), R.drawable.touch);
            }
            for (int i2 = 0; i2 < 10; i2++) {
                this.A[i2] = new Sprite();
            }
            for (int i3 = 0; i3 < 2; i3++) {
                this.B[i3] = new Sprite();
            }
            for (int i4 = 0; i4 < 5; i4++) {
                this.H[i4] = new Sprite();
            }
            for (int i5 = 0; i5 < 2; i5++) {
                this.C[i5] = new Sprite();
            }
            for (int i6 = 0; i6 < 2; i6++) {
                this.D[i6] = new Sprite();
            }
            for (int i7 = 0; i7 < 4; i7++) {
                this.J[i7] = new Sprite();
                this.J[i7].setPic(0, 0, 64, 32);
            }
            for (int i8 = 0; i8 < 4; i8++) {
                this.F[i8] = new Sprite();
                this.F[i8].setPic(0, 0, 64, 64);
                this.F[i8].j = 0;
                this.F[i8].k = 0;
                this.F[i8].setDef();
            }
            this.E.setPic(0, 0, 224, 48);
            this.E.j = 48;
            this.E.k = 192;
            this.E.setDef();
            this.G.setPic(0, 0, 256, 96);
            this.G.j = 32;
            this.G.k = 32;
            this.G.setDef();
            this.I.setPic(0, 0, 320, 336);
            this.I.j = 0;
            this.I.k = 0;
            this.I.setDef();
            this.H[0].setPic(0, 0, 288, 48);
            this.H[0].j = 16;
            this.H[0].k = 80;
            this.H[0].setDef();
            this.H[1].setPic(0, 0, 288, 48);
            this.H[1].j = 16;
            this.H[1].k = 144;
            this.H[1].setDef();
            this.H[2].setPic(0, 0, 288, 48);
            this.H[2].j = 16;
            this.H[2].k = 208;
            this.H[2].setDef();
            this.H[3].setPic(0, 0, 288, 48);
            this.H[3].j = 16;
            this.H[3].k = 272;
            this.H[3].setDef();
            this.H[4].setPic(0, 0, 288, 48);
            this.H[4].j = 16;
            this.H[4].k = 337;
            this.H[4].setDef();
            this.D[0].setPic(0, 0, 192, 64);
            this.D[0].j = 64;
            this.D[0].k = 48;
            this.D[0].setDef();
            this.D[0].s = 180;
            this.D[1].setPic(0, 0, 192, 64);
            this.D[1].j = 64;
            this.D[1].k = 320;
            this.D[1].setDef();
            this.D[1].f191a = false;
        }

        /* access modifiers changed from: private */
        public void scoreSubmit(long j2, byte b2) {
            Score score = new Score(j2, null);
            switch (b2) {
                case 0:
                    score.submitTo(this.i, new Score.SubmitToCB() {
                        public void onFailure(String str) {
                            main.this.setResult(0);
                        }

                        public void onSuccess(boolean z) {
                            main.this.setResult(-1);
                        }
                    });
                    return;
                case 1:
                    score.submitTo(this.j, new Score.SubmitToCB() {
                        public void onFailure(String str) {
                            main.this.setResult(0);
                        }

                        public void onSuccess(boolean z) {
                            main.this.setResult(-1);
                        }
                    });
                    return;
                case 2:
                    score.submitTo(this.k, new Score.SubmitToCB() {
                        public void onFailure(String str) {
                            main.this.setResult(0);
                        }

                        public void onSuccess(boolean z) {
                            main.this.setResult(-1);
                        }
                    });
                    return;
                case 3:
                    score.submitTo(this.l, new Score.SubmitToCB() {
                        public void onFailure(String str) {
                            main.this.setResult(0);
                        }

                        public void onSuccess(boolean z) {
                            main.this.setResult(-1);
                        }
                    });
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: private */
        public void unlockStageAchievement(short s2) {
            String str = "";
            switch (s2) {
                case 0:
                    str = "1126902";
                    break;
                case 1:
                    str = "1126912";
                    break;
                case 2:
                    str = "1126922";
                    break;
                case 3:
                    str = "1126932";
                    break;
            }
            new Achievement(str).unlock(new Achievement.UnlockCB() {
                public void onFailure(String str) {
                    main.this.setResult(0);
                }

                public void onSuccess(boolean z) {
                    main.this.setResult(-1);
                }
            });
        }

        public SharedPreferences GetPreference() {
            return this.f183a;
        }

        public HelloThread getThread() {
            return this.M;
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.M.f190a = motionEvent.getX();
                    this.M.b = motionEvent.getY();
                    this.M.c = true;
                    break;
                case 261:
                    this.M.f190a = motionEvent.getX(1);
                    this.M.b = motionEvent.getY(1);
                    this.M.c = true;
                    break;
            }
            return true;
        }

        public void shuffle(int[] iArr) {
            for (int length = iArr.length - 1; length > 0; length--) {
                int random = (int) (Math.random() * ((double) length));
                int i2 = iArr[length];
                iArr[length] = iArr[random];
                iArr[random] = i2;
            }
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            if (this.M.getState() == Thread.State.TERMINATED) {
                this.M = new HelloThread(surfaceHolder, this.N, this.O);
            }
            this.M.setRunning(true);
            this.M.start();
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            this.E.backDef();
            this.E.f191a = false;
            this.I.f191a = false;
            for (int i2 = 0; i2 < 5; i2++) {
                this.H[i2].f191a = false;
            }
            for (int i3 = 0; i3 < 2; i3++) {
                this.C[i3].f191a = false;
                this.D[i3].f191a = false;
                this.B[i3].f191a = false;
            }
            for (int i4 = 0; i4 < 10; i4++) {
                this.A[i4].f191a = false;
            }
            for (int i5 = 0; i5 < 4; i5++) {
                this.F[i5].f191a = false;
                this.J[i5].f191a = false;
            }
            this.G.f191a = false;
            this.n = 0;
            boolean z2 = true;
            this.M.setRunning(false);
            while (z2) {
                try {
                    this.M.join();
                    z2 = false;
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        this.d = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        this.c = (LinearLayout) findViewById(R.id.linearLayoutMain);
        if (this.b == null) {
            this.b = new HelloView(this, this.d, ((float) displayMetrics.widthPixels) / 320.0f, displayMetrics.density);
            this.c.addView(this.b);
        }
        AdView adView = new AdView(this, AdSize.f48a, "a14e145f0410190");
        ((LinearLayout) findViewById(R.id.linearLayoutAd)).addView(adView);
        adView.a(new AdRequest());
    }

    public void onDestroy() {
        if (this.f182a != null) {
            this.f182a.a();
        }
        super.onDestroy();
        this.b = null;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void onRestart() {
        super.onRestart();
    }
}
