package scala;

import scala.runtime.AbstractFunction1;

public final class Function1$$anonfun$andThen$1 extends AbstractFunction1<T1, A> implements Serializable {
    private final /* synthetic */ Function1 $outer;
    private final Function1 g$2;

    public Function1$$anonfun$andThen$1(Function1 function1, Function1<T1, R> function12) {
        if (function1 == null) {
            throw new NullPointerException();
        }
        this.$outer = function1;
        this.g$2 = function12;
    }

    public final A apply(T1 t1) {
        return this.g$2.apply(this.$outer.apply(t1));
    }
}
