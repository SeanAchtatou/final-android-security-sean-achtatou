package scala;

import scala.runtime.AbstractFunction0;
import scala.runtime.BoxesRunTime;

public final class Enumeration$$anonfun$scala$Enumeration$$nameOf$1 extends AbstractFunction0<String> implements Serializable {
    private final /* synthetic */ Enumeration $outer;
    private final int i$1;

    public Enumeration$$anonfun$scala$Enumeration$$nameOf$1(Enumeration enumeration, int i) {
        if (enumeration == null) {
            throw new NullPointerException();
        }
        this.$outer = enumeration;
        this.i$1 = i;
    }

    public final String apply() {
        this.$outer.scala$Enumeration$$populateNameMap();
        return this.$outer.scala$Enumeration$$nmap().apply(BoxesRunTime.boxToInteger(this.i$1));
    }
}
