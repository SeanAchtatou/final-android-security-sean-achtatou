package scala;

import scala.runtime.Nothing$;

public final class PartialFunction$ {
    public static final PartialFunction$ MODULE$ = null;
    private final PartialFunction<Object, Nothing$> empty_pf = new PartialFunction$$anon$1();
    public final Function1<Object, Object> scala$PartialFunction$$constFalse = new PartialFunction$$anonfun$2();
    public final PartialFunction<Object, Object> scala$PartialFunction$$fallback_pf = new PartialFunction$$anonfun$1();

    static {
        new PartialFunction$();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.PartialFunction$$anonfun$1, scala.PartialFunction<java.lang.Object, java.lang.Object>] */
    private PartialFunction$() {
        MODULE$ = this;
    }

    public final <A, B> PartialFunction<A, B> empty() {
        return this.empty_pf;
    }

    public final <B> PartialFunction<Object, B> scala$PartialFunction$$checkFallback() {
        return this.scala$PartialFunction$$fallback_pf;
    }

    public final <B> boolean scala$PartialFunction$$fallbackOccurred(B b) {
        return this.scala$PartialFunction$$fallback_pf == b;
    }
}
