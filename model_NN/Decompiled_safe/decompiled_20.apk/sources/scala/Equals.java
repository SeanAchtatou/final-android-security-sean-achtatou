package scala;

public interface Equals {
    boolean canEqual(Object obj);
}
