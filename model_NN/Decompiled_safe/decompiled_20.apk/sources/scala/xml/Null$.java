package scala.xml;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Function1;
import scala.Product;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.Seq;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;
import scala.runtime.ScalaRunTime$;

public final class Null$ extends MetaData implements Product {
    public static final Null$ MODULE$ = null;

    static {
        new Null$();
    }

    private Null$() {
        MODULE$ = this;
        Product.Cclass.$init$(this);
    }

    public final Seq<Object> basisForHashCode() {
        return Nil$.MODULE$;
    }

    public final StringBuilder buildString(StringBuilder stringBuilder) {
        return stringBuilder;
    }

    public final MetaData copy(MetaData metaData) {
        return metaData;
    }

    public final MetaData filter(Function1<MetaData, Object> function1) {
        return this;
    }

    public final Iterator<Nothing$> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public final scala.runtime.Null$ key() {
        return null;
    }

    public final int length() {
        return 0;
    }

    public final int length(int i) {
        return i;
    }

    public final scala.runtime.Null$ next() {
        return null;
    }

    public final int productArity() {
        return 0;
    }

    public final Object productElement(int i) {
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public final Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public final String productPrefix() {
        return "Null";
    }

    public final int size() {
        return 0;
    }

    public final boolean strict_$eq$eq(Equality equality) {
        return (equality instanceof MetaData) && ((MetaData) equality).length() == 0;
    }

    public final String toString() {
        return PoiTypeDef.All;
    }

    public final void toString1(StringBuilder stringBuilder) {
    }

    public final scala.runtime.Null$ value() {
        return null;
    }
}
