package scala.xml;

import scala.Serializable;
import scala.collection.Seq;
import scala.runtime.AbstractFunction1;

public final class NodeSeq$$anonfun$newBuilder$1 extends AbstractFunction1<Seq<Node>, NodeSeq> implements Serializable {
    public final /* bridge */ /* synthetic */ Object apply(Object obj) {
        return apply((Seq<Node>) ((Seq) obj));
    }

    public final NodeSeq apply(Seq<Node> seq) {
        return NodeSeq$.MODULE$.fromSeq(seq);
    }
}
