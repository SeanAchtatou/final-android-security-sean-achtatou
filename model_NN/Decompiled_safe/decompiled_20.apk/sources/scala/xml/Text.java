package scala.xml;

import scala.collection.mutable.StringBuilder;

public class Text extends Atom<String> {
    public StringBuilder buildString(StringBuilder stringBuilder) {
        return Utility$.MODULE$.escape((String) super.data(), stringBuilder);
    }
}
