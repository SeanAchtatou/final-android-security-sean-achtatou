package scala.xml;

public interface Attribute {
    boolean isPrefixed();

    String pre();
}
