package scala.xml;

import scala.Serializable;
import scala.runtime.AbstractFunction1;

public final class Elem$$anonfun$text$1 extends AbstractFunction1<Node, String> implements Serializable {
    public Elem$$anonfun$text$1(Elem elem) {
    }

    public final String apply(Node node) {
        return node.text();
    }
}
