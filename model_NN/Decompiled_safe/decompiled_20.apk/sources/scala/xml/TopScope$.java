package scala.xml;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.collection.mutable.StringBuilder;

public final class TopScope$ extends NamespaceBinding {
    public static final TopScope$ MODULE$ = null;

    static {
        new TopScope$();
    }

    private TopScope$() {
        super(null, null, null);
        MODULE$ = this;
    }

    public final void buildString(StringBuilder stringBuilder, NamespaceBinding namespaceBinding) {
    }

    public final String toString() {
        return PoiTypeDef.All;
    }
}
