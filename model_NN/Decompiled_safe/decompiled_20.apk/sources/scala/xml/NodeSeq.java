package scala.xml;

import scala.collection.AbstractSeq;
import scala.collection.Iterator;
import scala.collection.SeqLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.GenericCompanion;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Seq$;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Builder;
import scala.runtime.BoxesRunTime;
import scala.xml.Equality;

public abstract class NodeSeq extends AbstractSeq<Node> implements SeqLike<Node, NodeSeq>, Seq<Node> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.xml.NodeSeq, scala.collection.immutable.Iterable, scala.xml.Equality, scala.collection.immutable.Seq] */
    public NodeSeq() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Equality.Cclass.$init$(this);
    }

    public /* synthetic */ Object apply(Object obj) {
        return apply(BoxesRunTime.unboxToInt(obj));
    }

    public Node apply(int i) {
        return theSeq().apply(i);
    }

    public scala.collection.Seq<Object> basisForHashCode() {
        return theSeq();
    }

    public boolean canEqual(Object obj) {
        return obj instanceof NodeSeq;
    }

    public GenericCompanion<Seq> companion() {
        return Seq.Cclass.companion(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.xml.NodeSeq, scala.xml.Equality] */
    public boolean equals(Object obj) {
        return Equality.Cclass.equals(this, obj);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.xml.NodeSeq, scala.xml.Equality] */
    public int hashCode() {
        return Equality.Cclass.hashCode(this);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public Iterator<Node> iterator() {
        return theSeq().iterator();
    }

    public int length() {
        return theSeq().length();
    }

    public Builder<Node, NodeSeq> newBuilder() {
        return NodeSeq$.MODULE$.newBuilder();
    }

    public Seq<Node> seq() {
        return Seq.Cclass.seq(this);
    }

    public boolean strict_$eq$eq(Equality equality) {
        if (!(equality instanceof NodeSeq)) {
            return false;
        }
        NodeSeq nodeSeq = (NodeSeq) equality;
        return length() == nodeSeq.length() && theSeq().sameElements(nodeSeq.theSeq());
    }

    public String text() {
        return ((TraversableOnce) map(new NodeSeq$$anonfun$text$1(this), Seq$.MODULE$.canBuildFrom())).mkString();
    }

    public abstract scala.collection.Seq<Node> theSeq();

    public String toString() {
        return theSeq().mkString();
    }
}
