package scala.xml;

import scala.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;

public final class MetaData$$anonfun$asAttrMap$1 extends AbstractFunction1<MetaData, Tuple2<String, String>> implements Serializable {
    public MetaData$$anonfun$asAttrMap$1(MetaData metaData) {
    }

    public final Tuple2<String, String> apply(MetaData metaData) {
        return new Tuple2<>(metaData.prefixedKey(), NodeSeq$.MODULE$.seqToNodeSeq(metaData.value()).text());
    }
}
