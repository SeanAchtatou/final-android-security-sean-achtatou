package scala.xml;

import android.support.v4.view.MotionEventCompat;
import com.amap.api.search.route.Route;
import scala.Enumeration;
import scala.Function1;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxedUnit;
import scala.xml.parsing.TokenTests;

public final class Utility$ implements TokenTests {
    public static final Utility$ MODULE$ = null;

    static {
        new Utility$();
    }

    private Utility$() {
        MODULE$ = this;
        TokenTests.Cclass.$init$(this);
    }

    public final StringBuilder escape(String str, StringBuilder stringBuilder) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case MotionEventCompat.ACTION_HOVER_ENTER:
                    stringBuilder.append(9);
                    break;
                case 10:
                    stringBuilder.append(10);
                    break;
                case Route.DrivingNoFastRoad:
                    stringBuilder.append(13);
                    break;
                case '\"':
                    stringBuilder.append("&quot;");
                    break;
                case '&':
                    stringBuilder.append("&amp;");
                    break;
                case '<':
                    stringBuilder.append("&lt;");
                    break;
                case '>':
                    stringBuilder.append("&gt;");
                    break;
                default:
                    if (charAt < ' ') {
                        BoxedUnit boxedUnit = BoxedUnit.UNIT;
                        break;
                    } else {
                        stringBuilder.append(charAt);
                        break;
                    }
            }
        }
        return stringBuilder;
    }

    public final boolean isAtomAndNotText(Node node) {
        return node.isAtom() && !(node instanceof Text);
    }

    public final String sbToString(Function1<StringBuilder, BoxedUnit> function1) {
        StringBuilder stringBuilder = new StringBuilder();
        function1.apply(stringBuilder);
        return stringBuilder.toString();
    }

    public final void sequenceToXML(Seq<Node> seq, NamespaceBinding namespaceBinding, StringBuilder stringBuilder, boolean z, boolean z2, boolean z3, Enumeration.Value value) {
        if (!seq.isEmpty()) {
            if (seq.forall(new Utility$$anonfun$sequenceToXML$1())) {
                Iterator<Node> it = seq.iterator();
                serialize(it.next(), namespaceBinding, stringBuilder, z, z2, z3, value);
                while (it.hasNext()) {
                    stringBuilder.append(' ');
                    serialize(it.next(), namespaceBinding, stringBuilder, z, z2, z3, value);
                }
                return;
            }
            seq.foreach(new Utility$$anonfun$sequenceToXML$2(namespaceBinding, stringBuilder, z, z2, z3, value));
        }
    }

    public final boolean sequenceToXML$default$5() {
        return true;
    }

    public final boolean sequenceToXML$default$6() {
        return false;
    }

    public final Enumeration.Value sequenceToXML$default$7() {
        return MinimizeMode$.MODULE$.Default();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b6, code lost:
        if (r9.minimizeEmpty() != false) goto L_0x00a4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final scala.collection.mutable.StringBuilder serialize(scala.xml.Node r9, scala.xml.NamespaceBinding r10, scala.collection.mutable.StringBuilder r11, boolean r12, boolean r13, boolean r14, scala.Enumeration.Value r15) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof scala.xml.Comment
            if (r0 == 0) goto L_0x000e
            r0 = r9
            scala.xml.Comment r0 = (scala.xml.Comment) r0
            if (r12 != 0) goto L_0x000e
            scala.collection.mutable.StringBuilder r11 = r0.buildString(r11)
        L_0x000d:
            return r11
        L_0x000e:
            boolean r0 = r9 instanceof scala.xml.SpecialNode
            if (r0 == 0) goto L_0x0019
            scala.xml.SpecialNode r9 = (scala.xml.SpecialNode) r9
            scala.collection.mutable.StringBuilder r11 = r9.buildString(r11)
            goto L_0x000d
        L_0x0019:
            boolean r0 = r9 instanceof scala.xml.Group
            if (r0 == 0) goto L_0x002c
            scala.xml.Group r9 = (scala.xml.Group) r9
            scala.collection.Seq r0 = r9.nodes()
            scala.xml.Utility$$anonfun$serialize$1 r1 = new scala.xml.Utility$$anonfun$serialize$1
            r1.<init>(r11, r15, r9)
            r0.foreach(r1)
            goto L_0x000d
        L_0x002c:
            boolean r0 = r9 instanceof scala.xml.Elem
            if (r0 == 0) goto L_0x00b9
            scala.xml.Elem r9 = (scala.xml.Elem) r9
            r0 = 60
            r11.append(r0)
            r9.nameToString(r11)
            scala.xml.MetaData r0 = r9.attributes()
            if (r0 == 0) goto L_0x009b
            scala.xml.MetaData r0 = r9.attributes()
            r0.buildString(r11)
        L_0x0047:
            scala.xml.NamespaceBinding r0 = r9.scope()
            r0.buildString(r11, r10)
            scala.collection.Seq r0 = r9.child()
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x006c
            scala.xml.MinimizeMode$ r0 = scala.xml.MinimizeMode$.MODULE$
            scala.Enumeration$Value r0 = r0.Always()
            if (r15 != 0) goto L_0x009e
            if (r0 == 0) goto L_0x00a4
        L_0x0062:
            scala.xml.MinimizeMode$ r0 = scala.xml.MinimizeMode$.MODULE$
            scala.Enumeration$Value r0 = r0.Default()
            if (r15 != 0) goto L_0x00ac
            if (r0 == 0) goto L_0x00b2
        L_0x006c:
            r0 = 62
            r11.append(r0)
            scala.collection.Seq r1 = r9.child()
            scala.xml.NamespaceBinding r2 = r9.scope()
            boolean r5 = r8.sequenceToXML$default$5()
            boolean r6 = r8.sequenceToXML$default$6()
            scala.Enumeration$Value r7 = r8.sequenceToXML$default$7()
            r0 = r8
            r3 = r11
            r4 = r12
            r0.sequenceToXML(r1, r2, r3, r4, r5, r6, r7)
            java.lang.String r0 = "</"
            r11.append(r0)
            r9.nameToString(r11)
            r0 = 62
            scala.collection.mutable.StringBuilder r11 = r11.append(r0)
            goto L_0x000d
        L_0x009b:
            scala.runtime.BoxedUnit r0 = scala.runtime.BoxedUnit.UNIT
            goto L_0x0047
        L_0x009e:
            boolean r0 = r15.equals(r0)
            if (r0 == 0) goto L_0x0062
        L_0x00a4:
            java.lang.String r0 = "/>"
            scala.collection.mutable.StringBuilder r11 = r11.append(r0)
            goto L_0x000d
        L_0x00ac:
            boolean r0 = r15.equals(r0)
            if (r0 == 0) goto L_0x006c
        L_0x00b2:
            boolean r0 = r9.minimizeEmpty()
            if (r0 == 0) goto L_0x006c
            goto L_0x00a4
        L_0x00b9:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            scala.collection.mutable.StringBuilder r1 = new scala.collection.mutable.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Don't know how to serialize a "
            scala.collection.mutable.StringBuilder r1 = r1.append(r2)
            java.lang.Class r2 = r9.getClass()
            java.lang.String r2 = r2.getName()
            scala.collection.mutable.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.xml.Utility$.serialize(scala.xml.Node, scala.xml.NamespaceBinding, scala.collection.mutable.StringBuilder, boolean, boolean, boolean, scala.Enumeration$Value):scala.collection.mutable.StringBuilder");
    }

    public final NamespaceBinding serialize$default$2() {
        return TopScope$.MODULE$;
    }

    public final StringBuilder serialize$default$3() {
        return new StringBuilder();
    }

    public final boolean serialize$default$4() {
        return false;
    }

    public final boolean serialize$default$5() {
        return true;
    }

    public final boolean serialize$default$6() {
        return false;
    }

    public final Enumeration.Value serialize$default$7() {
        return MinimizeMode$.MODULE$.Default();
    }
}
