package scala.xml;

import scala.collection.Seq;

public final class NodeSeq$$anon$1 extends NodeSeq {
    private final Seq s$1;

    public NodeSeq$$anon$1(Seq seq) {
        this.s$1 = seq;
    }

    public final Seq<Node> theSeq() {
        return this.s$1;
    }
}
