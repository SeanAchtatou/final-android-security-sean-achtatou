package scala.xml;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public class Comment extends SpecialNode implements Product, Serializable {
    private final String commentText;

    public StringBuilder buildString(StringBuilder stringBuilder) {
        return stringBuilder.append("<!--").append(commentText()).append("-->");
    }

    public String commentText() {
        return this.commentText;
    }

    public String label() {
        return "#REM";
    }

    public int productArity() {
        return 1;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                break;
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
        return commentText();
    }

    public Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public String productPrefix() {
        return "Comment";
    }

    public String text() {
        return PoiTypeDef.All;
    }
}
