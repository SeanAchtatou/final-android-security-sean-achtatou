package scala.xml;

import scala.Predef$;
import scala.Serializable;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;

public class Atom<A> extends SpecialNode implements Serializable {
    private final A data;

    public Seq<Object> basisForHashCode() {
        return (Seq) Seq$.MODULE$.apply(Predef$.MODULE$.genericWrapArray(new Object[]{data()}));
    }

    public StringBuilder buildString(StringBuilder stringBuilder) {
        return Utility$.MODULE$.escape(data().toString(), stringBuilder);
    }

    public boolean canEqual(Object obj) {
        return obj instanceof Atom;
    }

    public A data() {
        return this.data;
    }

    public String label() {
        return "#PCDATA";
    }

    public boolean strict_$eq$eq(Equality equality) {
        if (!(equality instanceof Atom)) {
            return false;
        }
        Object data2 = data();
        Object data3 = ((Atom) equality).data();
        return data2 == data3 ? true : data2 == null ? false : data2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) data2, data3) : data2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) data2, data3) : data2.equals(data3);
    }

    public String text() {
        return data().toString();
    }
}
