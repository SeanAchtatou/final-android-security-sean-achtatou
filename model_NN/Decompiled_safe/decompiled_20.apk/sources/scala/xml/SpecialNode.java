package scala.xml;

import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;

public abstract class SpecialNode extends Node {
    public final Null$ attributes() {
        return Null$.MODULE$;
    }

    public abstract StringBuilder buildString(StringBuilder stringBuilder);

    public final Nil$ child() {
        return Nil$.MODULE$;
    }
}
