package scala.xml;

import scala.Enumeration;
import scala.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;

public final class Utility$$anonfun$sequenceToXML$2 extends AbstractFunction1<Node, StringBuilder> implements Serializable {
    private final boolean decodeEntities$1;
    private final Enumeration.Value minimizeTags$1;
    private final boolean preserveWhitespace$1;
    private final NamespaceBinding pscope$1;
    private final StringBuilder sb$1;
    private final boolean stripComments$1;

    public Utility$$anonfun$sequenceToXML$2(NamespaceBinding namespaceBinding, StringBuilder stringBuilder, boolean z, boolean z2, boolean z3, Enumeration.Value value) {
        this.pscope$1 = namespaceBinding;
        this.sb$1 = stringBuilder;
        this.stripComments$1 = z;
        this.decodeEntities$1 = z2;
        this.preserveWhitespace$1 = z3;
        this.minimizeTags$1 = value;
    }

    public final StringBuilder apply(Node node) {
        return Utility$.MODULE$.serialize(node, this.pscope$1, this.sb$1, this.stripComments$1, this.decodeEntities$1, this.preserveWhitespace$1, this.minimizeTags$1);
    }
}
