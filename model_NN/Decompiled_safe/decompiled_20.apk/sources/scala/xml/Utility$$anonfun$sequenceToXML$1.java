package scala.xml;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class Utility$$anonfun$sequenceToXML$1 extends AbstractFunction1<Node, Object> implements Serializable {
    public final /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply((Node) obj));
    }

    public final boolean apply(Node node) {
        return Utility$.MODULE$.isAtomAndNotText(node);
    }
}
