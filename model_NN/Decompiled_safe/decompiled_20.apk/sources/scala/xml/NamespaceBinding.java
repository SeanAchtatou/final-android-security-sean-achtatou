package scala.xml;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Predef$;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.List$;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;
import scala.xml.Equality;

public class NamespaceBinding implements Product, Serializable, Equality {
    private final NamespaceBinding parent;
    private final String prefix;
    private final String uri;

    public NamespaceBinding(String str, String str2, NamespaceBinding namespaceBinding) {
        this.prefix = str;
        this.uri = str2;
        this.parent = namespaceBinding;
        Equality.Cclass.$init$(this);
        Product.Cclass.$init$(this);
        if (str != null && str.equals(PoiTypeDef.All)) {
            throw new IllegalArgumentException("zero length prefix not allowed");
        }
    }

    public Seq<Object> basisForHashCode() {
        return List$.MODULE$.apply((Seq) Predef$.MODULE$.wrapRefArray(new Object[]{prefix(), uri(), parent()}));
    }

    public void buildString(StringBuilder stringBuilder, NamespaceBinding namespaceBinding) {
        if (this != namespaceBinding) {
            Predef$ predef$ = Predef$.MODULE$;
            StringOps stringOps = new StringOps(" xmlns%s=\"%s\"");
            Predef$ predef$2 = Predef$.MODULE$;
            Object[] objArr = new Object[2];
            objArr[0] = prefix() == null ? PoiTypeDef.All : new StringBuilder().append((Object) ":").append((Object) prefix()).toString();
            objArr[1] = uri() == null ? PoiTypeDef.All : uri();
            parent().buildString(stringBuilder.append(stringOps.format(predef$2.genericWrapArray(objArr))), namespaceBinding);
        }
    }

    public boolean canEqual(Object obj) {
        return obj instanceof NamespaceBinding;
    }

    public boolean equals(Object obj) {
        return Equality.Cclass.equals(this, obj);
    }

    public int hashCode() {
        return Equality.Cclass.hashCode(this);
    }

    public NamespaceBinding parent() {
        return this.parent;
    }

    public String prefix() {
        return this.prefix;
    }

    public int productArity() {
        return 3;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                return prefix();
            case 1:
                return uri();
            case 2:
                return parent();
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
    }

    public Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public String productPrefix() {
        return "NamespaceBinding";
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean strict_$eq$eq(scala.xml.Equality r4) {
        /*
            r3 = this;
            r0 = 0
            boolean r1 = r4 instanceof scala.xml.NamespaceBinding
            if (r1 == 0) goto L_0x0013
            scala.xml.NamespaceBinding r4 = (scala.xml.NamespaceBinding) r4
            java.lang.String r1 = r3.prefix()
            java.lang.String r2 = r4.prefix()
            if (r1 != 0) goto L_0x0014
            if (r2 == 0) goto L_0x001a
        L_0x0013:
            return r0
        L_0x0014:
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0013
        L_0x001a:
            java.lang.String r1 = r3.uri()
            java.lang.String r2 = r4.uri()
            if (r1 != 0) goto L_0x0034
            if (r2 != 0) goto L_0x0013
        L_0x0026:
            scala.xml.NamespaceBinding r1 = r3.parent()
            scala.xml.NamespaceBinding r2 = r4.parent()
            if (r1 != 0) goto L_0x003b
            if (r2 != 0) goto L_0x0013
        L_0x0032:
            r0 = 1
            goto L_0x0013
        L_0x0034:
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0013
            goto L_0x0026
        L_0x003b:
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0013
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.xml.NamespaceBinding.strict_$eq$eq(scala.xml.Equality):boolean");
    }

    public String toString() {
        return Utility$.MODULE$.sbToString(new NamespaceBinding$$anonfun$toString$1(this));
    }

    public String uri() {
        return this.uri;
    }
}
