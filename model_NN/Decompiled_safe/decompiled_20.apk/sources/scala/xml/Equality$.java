package scala.xml;

public final class Equality$ {
    public static final Equality$ MODULE$ = null;

    static {
        new Equality$();
    }

    private Equality$() {
        MODULE$ = this;
    }

    public final Object asRef(Object obj) {
        return obj;
    }

    public final boolean compareBlithely(Object obj, Object obj2) {
        if (obj == null || obj2 == null) {
            return obj == obj2;
        }
        if (obj2 instanceof String) {
            return compareBlithely(obj, (String) obj2);
        }
        if (obj2 instanceof Node) {
            return compareBlithely(obj, (Node) obj2);
        }
        return false;
    }

    public final boolean compareBlithely(Object obj, String str) {
        if (obj instanceof Atom) {
            Object data = ((Atom) obj).data();
            if (data == null) {
                if (str == null) {
                    return true;
                }
            } else if (data.equals(str)) {
                return true;
            }
            return false;
        } else if (!(obj instanceof NodeSeq)) {
            return false;
        } else {
            String text = ((NodeSeq) obj).text();
            if (text == null) {
                if (str == null) {
                    return true;
                }
            } else if (text.equals(str)) {
                return true;
            }
            return false;
        }
    }

    public final boolean compareBlithely(Object obj, Node node) {
        if (obj instanceof NodeSeq) {
            NodeSeq nodeSeq = (NodeSeq) obj;
            if (nodeSeq.length() == 1) {
                Node apply = nodeSeq.apply(0);
                if (node == null) {
                    if (apply == null) {
                        return true;
                    }
                } else if (node.equals(apply)) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }
}
