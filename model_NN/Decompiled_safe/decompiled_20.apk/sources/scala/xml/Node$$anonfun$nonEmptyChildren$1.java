package scala.xml;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class Node$$anonfun$nonEmptyChildren$1 extends AbstractFunction1<Node, Object> implements Serializable {
    public Node$$anonfun$nonEmptyChildren$1(Node node) {
    }

    public final /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply((Node) obj));
    }

    public final boolean apply(Node node) {
        String node2 = node.toString();
        return node2 != null && node2.equals(PoiTypeDef.All);
    }
}
