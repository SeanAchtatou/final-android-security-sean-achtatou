package scala.xml;

import scala.collection.Seq;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;

public final class NodeSeq$ {
    public static final NodeSeq$ MODULE$ = null;
    private final NodeSeq Empty = fromSeq(Nil$.MODULE$);

    static {
        new NodeSeq$();
    }

    private NodeSeq$() {
        MODULE$ = this;
    }

    public final NodeSeq fromSeq(Seq<Node> seq) {
        return new NodeSeq$$anon$1(seq);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer, scala.collection.mutable.Builder] */
    public final Builder<Node, NodeSeq> newBuilder() {
        return Builder.Cclass.mapResult(new ListBuffer(), new NodeSeq$$anonfun$newBuilder$1());
    }

    public final NodeSeq seqToNodeSeq(Seq<Node> seq) {
        return fromSeq(seq);
    }
}
