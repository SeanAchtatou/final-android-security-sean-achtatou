package scala.xml;

import scala.Predef$;
import scala.Product;
import scala.Serializable;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.StringOps;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;
import scala.runtime.ScalaRunTime$;

public final class Group extends Node implements Product, Serializable {
    private final Seq<Node> nodes;

    private Nothing$ fail(String str) {
        Predef$ predef$ = Predef$.MODULE$;
        throw new UnsupportedOperationException(new StringOps("class Group does not support method '%s'").format(Predef$.MODULE$.genericWrapArray(new Object[]{str})));
    }

    public final Nothing$ attributes() {
        return fail("attributes");
    }

    public final Seq<Node> basisForHashCode() {
        return nodes();
    }

    public final boolean canEqual(Object obj) {
        return obj instanceof Group;
    }

    public final Nothing$ child() {
        return fail("child");
    }

    public final Nothing$ label() {
        return fail("label");
    }

    public final Seq<Node> nodes() {
        return this.nodes;
    }

    public final int productArity() {
        return 1;
    }

    public final Object productElement(int i) {
        switch (i) {
            case 0:
                break;
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
        return nodes();
    }

    public final Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public final String productPrefix() {
        return "Group";
    }

    public final boolean strict_$eq$eq(Equality equality) {
        if (equality instanceof Group) {
            return nodes().sameElements(((Group) equality).nodes());
        }
        return false;
    }

    public final Seq<Node> theSeq() {
        return nodes();
    }
}
