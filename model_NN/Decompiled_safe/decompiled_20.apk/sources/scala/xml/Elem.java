package scala.xml;

import scala.Serializable;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.TraversableOnce;

public class Elem extends Node implements Serializable {
    private final MetaData attributes;
    private final Seq<Node> child;
    private final String label;
    private final boolean minimizeEmpty;
    private final String prefix;
    private final NamespaceBinding scope;

    public MetaData attributes() {
        return this.attributes;
    }

    public Seq<Object> basisForHashCode() {
        return child().toList().$colon$colon(attributes()).$colon$colon(label()).$colon$colon(prefix());
    }

    public Seq<Node> child() {
        return this.child;
    }

    public String label() {
        return this.label;
    }

    public boolean minimizeEmpty() {
        return this.minimizeEmpty;
    }

    public String prefix() {
        return this.prefix;
    }

    public NamespaceBinding scope() {
        return this.scope;
    }

    public String text() {
        return ((TraversableOnce) child().map(new Elem$$anonfun$text$1(this), Seq$.MODULE$.canBuildFrom())).mkString();
    }
}
