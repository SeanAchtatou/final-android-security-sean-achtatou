package scala.xml;

import scala.Serializable;
import scala.runtime.AbstractFunction1;

public final class NodeSeq$$anonfun$text$1 extends AbstractFunction1<Node, String> implements Serializable {
    public NodeSeq$$anonfun$text$1(NodeSeq nodeSeq) {
    }

    public final String apply(Node node) {
        return node.text();
    }
}
