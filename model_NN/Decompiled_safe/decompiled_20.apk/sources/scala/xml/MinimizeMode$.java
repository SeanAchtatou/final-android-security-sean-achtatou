package scala.xml;

import scala.Enumeration;

public final class MinimizeMode$ extends Enumeration {
    public static final MinimizeMode$ MODULE$ = null;
    private final Enumeration.Value Always = Value();
    private final Enumeration.Value Default = Value();
    private final Enumeration.Value Never = Value();

    static {
        new MinimizeMode$();
    }

    private MinimizeMode$() {
        MODULE$ = this;
    }

    public final Enumeration.Value Always() {
        return this.Always;
    }

    public final Enumeration.Value Default() {
        return this.Default;
    }
}
