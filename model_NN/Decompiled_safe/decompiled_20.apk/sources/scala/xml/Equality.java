package scala.xml;

import scala.Equals;
import scala.collection.Seq;
import scala.runtime.ScalaRunTime$;

public interface Equality extends Equals {

    /* renamed from: scala.xml.Equality$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Equality equality) {
        }

        private static boolean doComparison(Equality equality, Object obj, boolean z) {
            boolean z2;
            if ((obj instanceof Object) && equality == obj) {
                z2 = true;
            } else if (obj instanceof Equality) {
                Equality equality2 = (Equality) obj;
                z2 = equality2.canEqual(equality) && equality.strict_$eq$eq(equality2);
            } else {
                z2 = false;
            }
            return z2 || (z && Equality$.MODULE$.compareBlithely(equality, Equality$.MODULE$.asRef(obj)));
        }

        public static boolean equals(Equality equality, Object obj) {
            return doComparison(equality, obj, false);
        }

        public static int hashCode(Equality equality) {
            return ScalaRunTime$.MODULE$.hash(equality.basisForHashCode());
        }
    }

    Seq<Object> basisForHashCode();

    boolean canEqual(Object obj);

    boolean strict_$eq$eq(Equality equality);
}
