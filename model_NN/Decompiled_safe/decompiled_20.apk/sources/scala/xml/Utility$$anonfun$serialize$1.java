package scala.xml;

import scala.Enumeration;
import scala.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;

public final class Utility$$anonfun$serialize$1 extends AbstractFunction1<Node, StringBuilder> implements Serializable {
    private final Enumeration.Value minimizeTags$2;
    private final StringBuilder sb$2;
    private final Group x4$1;

    public Utility$$anonfun$serialize$1(StringBuilder stringBuilder, Enumeration.Value value, Group group) {
        this.sb$2 = stringBuilder;
        this.minimizeTags$2 = value;
        this.x4$1 = group;
    }

    public final StringBuilder apply(Node node) {
        NamespaceBinding scope = this.x4$1.scope();
        StringBuilder stringBuilder = this.sb$2;
        Enumeration.Value value = this.minimizeTags$2;
        return Utility$.MODULE$.serialize(node, scope, stringBuilder, Utility$.MODULE$.serialize$default$4(), Utility$.MODULE$.serialize$default$5(), Utility$.MODULE$.serialize$default$6(), value);
    }
}
