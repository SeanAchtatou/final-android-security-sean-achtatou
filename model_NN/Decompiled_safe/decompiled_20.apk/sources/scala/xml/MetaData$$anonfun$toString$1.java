package scala.xml;

import scala.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

public final class MetaData$$anonfun$toString$1 extends AbstractFunction1<StringBuilder, BoxedUnit> implements Serializable {
    private final /* synthetic */ MetaData $outer;

    public MetaData$$anonfun$toString$1(MetaData metaData) {
        if (metaData == null) {
            throw new NullPointerException();
        }
        this.$outer = metaData;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object obj) {
        apply((StringBuilder) obj);
        return BoxedUnit.UNIT;
    }

    public final void apply(StringBuilder stringBuilder) {
        this.$outer.buildString(stringBuilder);
    }
}
