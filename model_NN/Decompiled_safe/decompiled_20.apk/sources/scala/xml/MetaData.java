package scala.xml;

import scala.Function1;
import scala.Predef$;
import scala.Serializable;
import scala.collection.AbstractIterable;
import scala.collection.Iterable;
import scala.collection.Seq;
import scala.collection.immutable.List$;
import scala.collection.immutable.Map;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.xml.Equality;

public abstract class MetaData extends AbstractIterable<MetaData> implements Serializable, Iterable<MetaData> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.xml.MetaData, scala.xml.Equality] */
    public MetaData() {
        Equality.Cclass.$init$(this);
    }

    public Map<String, String> asAttrMap() {
        return iterator().map(new MetaData$$anonfun$asAttrMap$1(this)).toMap(Predef$.MODULE$.conforms());
    }

    public Seq<Object> basisForHashCode() {
        return List$.MODULE$.apply((Seq) Predef$.MODULE$.wrapRefArray((Object[]) new Map[]{asAttrMap()}));
    }

    public StringBuilder buildString(StringBuilder stringBuilder) {
        stringBuilder.append(' ');
        toString1(stringBuilder);
        return next().buildString(stringBuilder);
    }

    public boolean canEqual(Object obj) {
        return obj instanceof MetaData;
    }

    public abstract MetaData copy(MetaData metaData);

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.xml.MetaData, scala.xml.Equality] */
    public boolean equals(Object obj) {
        return Equality.Cclass.equals(this, obj);
    }

    public MetaData filter(Function1<MetaData, Object> function1) {
        return BoxesRunTime.unboxToBoolean(function1.apply(this)) ? copy(next().filter(function1)) : next().filter(function1);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.xml.MetaData, scala.xml.Equality] */
    public int hashCode() {
        return Equality.Cclass.hashCode(this);
    }

    public abstract String key();

    public int length() {
        return length(0);
    }

    public int length(int i) {
        return next().length(i + 1);
    }

    public abstract MetaData next();

    public String prefixedKey() {
        if (this instanceof Attribute) {
            Attribute attribute = (Attribute) this;
            if (attribute.isPrefixed()) {
                return new StringBuilder().append((Object) attribute.pre()).append((Object) ":").append((Object) key()).toString();
            }
        }
        return key();
    }

    public boolean strict_$eq$eq(Equality equality) {
        if (!(equality instanceof MetaData)) {
            return false;
        }
        Map<String, String> asAttrMap = asAttrMap();
        Map<String, String> asAttrMap2 = ((MetaData) equality).asAttrMap();
        if (asAttrMap != null) {
            return asAttrMap.equals(asAttrMap2);
        }
        if (asAttrMap2 != null) {
            return false;
        }
    }

    public String toString() {
        return Utility$.MODULE$.sbToString(new MetaData$$anonfun$toString$1(this));
    }

    public abstract void toString1(StringBuilder stringBuilder);

    public abstract Seq<Node> value();
}
