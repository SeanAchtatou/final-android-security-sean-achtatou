package scala.xml;

import scala.collection.Seq;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxedUnit;

public abstract class Node extends NodeSeq {
    public MetaData attributes() {
        return Null$.MODULE$;
    }

    public Seq<Object> basisForHashCode() {
        return nonEmptyChildren().toList().$colon$colon(attributes()).$colon$colon(label()).$colon$colon(prefix());
    }

    public String buildString(boolean z) {
        return Utility$.MODULE$.serialize(this, Utility$.MODULE$.serialize$default$2(), Utility$.MODULE$.serialize$default$3(), z, Utility$.MODULE$.serialize$default$5(), Utility$.MODULE$.serialize$default$6(), Utility$.MODULE$.serialize$default$7()).toString();
    }

    public boolean canEqual(Object obj) {
        return !(obj instanceof Group) && (obj instanceof Node);
    }

    public abstract Seq<Node> child();

    public boolean isAtom() {
        return this instanceof Atom;
    }

    public abstract String label();

    public StringBuilder nameToString(StringBuilder stringBuilder) {
        if (prefix() == null) {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        } else {
            stringBuilder.append(prefix());
            stringBuilder.append(':');
        }
        return stringBuilder.append(label());
    }

    public Seq<Node> nonEmptyChildren() {
        return (Seq) child().filterNot(new Node$$anonfun$nonEmptyChildren$1(this));
    }

    public String prefix() {
        return null;
    }

    public NamespaceBinding scope() {
        return TopScope$.MODULE$;
    }

    public boolean strict_$eq$eq(Equality equality) {
        if ((equality instanceof Group) || !(equality instanceof Node)) {
            return false;
        }
        Node node = (Node) equality;
        String prefix = prefix();
        String prefix2 = node.prefix();
        if (prefix == null) {
            if (prefix2 != null) {
                return false;
            }
        } else if (!prefix.equals(prefix2)) {
            return false;
        }
        String label = label();
        String label2 = node.label();
        if (label == null) {
            if (label2 != null) {
                return false;
            }
        } else if (!label.equals(label2)) {
            return false;
        }
        MetaData attributes = attributes();
        MetaData attributes2 = node.attributes();
        if (attributes == null) {
            if (attributes2 != null) {
                return false;
            }
        } else if (!attributes.equals(attributes2)) {
            return false;
        }
        return nonEmptyChildren().sameElements(node.nonEmptyChildren());
    }

    public String text() {
        return super.text();
    }

    public Seq<Node> theSeq() {
        return Nil$.MODULE$.$colon$colon(this);
    }

    public String toString() {
        return buildString(false);
    }
}
