package scala;

import scala.Product;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public class StringContext implements Product, Serializable {
    private final Seq<String> parts;

    public class InvalidEscapeException extends IllegalArgumentException {
        public InvalidEscapeException(String str, int i) {
            super(new StringBuilder().append((Object) "invalid escape character at index ").append(BoxesRunTime.boxToInteger(i)).append((Object) " in \"").append((Object) str).append((Object) "\"").toString());
        }
    }

    public StringContext(Seq<String> seq) {
        this.parts = seq;
        Product.Cclass.$init$(this);
    }

    public boolean canEqual(Object obj) {
        return obj instanceof StringContext;
    }

    public void checkLengths(Seq<Object> seq) {
        if (parts().length() != seq.length() + 1) {
            throw new IllegalArgumentException("wrong number of arguments for interpolated string");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r1 = 1
            r0 = 0
            if (r4 == r5) goto L_0x001c
            boolean r2 = r5 instanceof scala.StringContext
            if (r2 == 0) goto L_0x001e
            r2 = r1
        L_0x0009:
            if (r2 == 0) goto L_0x001d
            scala.StringContext r5 = (scala.StringContext) r5
            scala.collection.Seq r2 = r4.parts()
            scala.collection.Seq r3 = r5.parts()
            if (r2 != 0) goto L_0x0020
            if (r3 == 0) goto L_0x0026
        L_0x0019:
            r2 = r0
        L_0x001a:
            if (r2 == 0) goto L_0x001d
        L_0x001c:
            r0 = r1
        L_0x001d:
            return r0
        L_0x001e:
            r2 = r0
            goto L_0x0009
        L_0x0020:
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0019
        L_0x0026:
            boolean r2 = r5.canEqual(r4)
            if (r2 == 0) goto L_0x0019
            r2 = r1
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.StringContext.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public Seq<String> parts() {
        return this.parts;
    }

    public int productArity() {
        return 1;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                break;
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
        return parts();
    }

    public Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public String productPrefix() {
        return "StringContext";
    }

    public String s(Seq<Object> seq) {
        return standardInterpolator(new StringContext$$anonfun$s$1(this), seq);
    }

    public String standardInterpolator(Function1<String, String> function1, Seq<Object> seq) {
        checkLengths(seq);
        Iterator<String> it = parts().iterator();
        Iterator<Object> it2 = seq.iterator();
        StringBuilder sb = new StringBuilder(function1.apply(it.next()));
        while (it2.hasNext()) {
            sb.append(it2.next());
            sb.append(function1.apply(it.next()));
        }
        return sb.toString();
    }

    public String toString() {
        return ScalaRunTime$.MODULE$._toString(this);
    }
}
