package scala;

import scala.runtime.AbstractPartialFunction;

public final class PartialFunction$$anonfun$1 extends AbstractPartialFunction<Object, Object> implements Serializable {
    public final <A1, B1> B1 applyOrElse(A1 a1, Function1<A1, B1> function1) {
        return PartialFunction$.MODULE$.scala$PartialFunction$$fallback_pf;
    }

    public final boolean isDefinedAt(Object obj) {
        return true;
    }
}
