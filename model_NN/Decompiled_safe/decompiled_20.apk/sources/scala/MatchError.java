package scala;

import scala.collection.mutable.StringBuilder;

public final class MatchError extends RuntimeException {
    private volatile boolean bitmap$0;
    private final Object obj;
    private String objString;

    public MatchError(Object obj2) {
        this.obj = obj2;
    }

    private String objString() {
        return this.bitmap$0 ? this.objString : objString$lzycompute();
    }

    private String objString$lzycompute() {
        synchronized (this) {
            if (!this.bitmap$0) {
                this.objString = this.obj == null ? "null" : new StringBuilder().append((Object) this.obj.toString()).append((Object) " (of class ").append((Object) this.obj.getClass().getName()).append((Object) ")").toString();
                this.bitmap$0 = true;
            }
        }
        return this.objString;
    }

    public final String getMessage() {
        return objString();
    }
}
