package scala;

import scala.collection.generic.CanBuildFrom;
import scala.collection.mutable.StringBuilder;
import scala.collection.mutable.StringBuilder$;

public final class Predef$$anon$3 implements CanBuildFrom<String, Object, String> {
    public final StringBuilder apply() {
        return StringBuilder$.MODULE$.newBuilder();
    }

    public final StringBuilder apply(String str) {
        return apply();
    }
}
