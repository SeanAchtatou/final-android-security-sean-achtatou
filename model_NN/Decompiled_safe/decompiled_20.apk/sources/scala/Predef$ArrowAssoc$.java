package scala;

public class Predef$ArrowAssoc$ {
    public static final Predef$ArrowAssoc$ MODULE$ = null;

    static {
        new Predef$ArrowAssoc$();
    }

    public Predef$ArrowAssoc$() {
        MODULE$ = this;
    }

    public final <B, A> Tuple2<A, B> $minus$greater$extension(A a, B b) {
        return new Tuple2<>(a, b);
    }
}
