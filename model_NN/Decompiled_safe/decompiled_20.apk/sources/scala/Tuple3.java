package scala;

import scala.Product;
import scala.Product3;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.ScalaRunTime$;

public class Tuple3<T1, T2, T3> implements Product3<T1, T2, T3>, Product {
    private final T1 _1;
    private final T2 _2;
    private final T3 _3;

    public Tuple3(T1 t1, T2 t2, T3 t3) {
        this._1 = t1;
        this._2 = t2;
        this._3 = t3;
        Product.Cclass.$init$(this);
        Product3.Cclass.$init$(this);
    }

    public T1 _1() {
        return this._1;
    }

    public T2 _2() {
        return this._2;
    }

    public T3 _3() {
        return this._3;
    }

    public boolean canEqual(Object obj) {
        return obj instanceof Tuple3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003b, code lost:
        if (r0 == false) goto L_0x00a0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r2 = 1
            r1 = 0
            if (r5 == r6) goto L_0x003d
            boolean r0 = r6 instanceof scala.Tuple3
            if (r0 == 0) goto L_0x003f
            r0 = r2
        L_0x0009:
            if (r0 == 0) goto L_0x00a0
            scala.Tuple3 r6 = (scala.Tuple3) r6
            java.lang.Object r0 = r5._1()
            java.lang.Object r3 = r6._1()
            if (r0 != r3) goto L_0x0041
            r0 = r2
        L_0x0018:
            if (r0 == 0) goto L_0x009e
            java.lang.Object r0 = r5._2()
            java.lang.Object r3 = r6._2()
            if (r0 != r3) goto L_0x0060
            r0 = r2
        L_0x0025:
            if (r0 == 0) goto L_0x009e
            java.lang.Object r0 = r5._3()
            java.lang.Object r3 = r6._3()
            if (r0 != r3) goto L_0x007f
            r0 = r2
        L_0x0032:
            if (r0 == 0) goto L_0x009e
            boolean r0 = r6.canEqual(r5)
            if (r0 == 0) goto L_0x009e
            r0 = r2
        L_0x003b:
            if (r0 == 0) goto L_0x00a0
        L_0x003d:
            r0 = r2
        L_0x003e:
            return r0
        L_0x003f:
            r0 = r1
            goto L_0x0009
        L_0x0041:
            if (r0 != 0) goto L_0x0045
            r0 = r1
            goto L_0x0018
        L_0x0045:
            boolean r4 = r0 instanceof java.lang.Number
            if (r4 == 0) goto L_0x0050
            java.lang.Number r0 = (java.lang.Number) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
            goto L_0x0018
        L_0x0050:
            boolean r4 = r0 instanceof java.lang.Character
            if (r4 == 0) goto L_0x005b
            java.lang.Character r0 = (java.lang.Character) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
            goto L_0x0018
        L_0x005b:
            boolean r0 = r0.equals(r3)
            goto L_0x0018
        L_0x0060:
            if (r0 != 0) goto L_0x0064
            r0 = r1
            goto L_0x0025
        L_0x0064:
            boolean r4 = r0 instanceof java.lang.Number
            if (r4 == 0) goto L_0x006f
            java.lang.Number r0 = (java.lang.Number) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
            goto L_0x0025
        L_0x006f:
            boolean r4 = r0 instanceof java.lang.Character
            if (r4 == 0) goto L_0x007a
            java.lang.Character r0 = (java.lang.Character) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
            goto L_0x0025
        L_0x007a:
            boolean r0 = r0.equals(r3)
            goto L_0x0025
        L_0x007f:
            if (r0 != 0) goto L_0x0083
            r0 = r1
            goto L_0x0032
        L_0x0083:
            boolean r4 = r0 instanceof java.lang.Number
            if (r4 == 0) goto L_0x008e
            java.lang.Number r0 = (java.lang.Number) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
            goto L_0x0032
        L_0x008e:
            boolean r4 = r0 instanceof java.lang.Character
            if (r4 == 0) goto L_0x0099
            java.lang.Character r0 = (java.lang.Character) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
            goto L_0x0032
        L_0x0099:
            boolean r0 = r0.equals(r3)
            goto L_0x0032
        L_0x009e:
            r0 = r1
            goto L_0x003b
        L_0x00a0:
            r0 = r1
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.Tuple3.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public int productArity() {
        return Product3.Cclass.productArity(this);
    }

    public Object productElement(int i) {
        return Product3.Cclass.productElement(this, i);
    }

    public Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public String productPrefix() {
        return "Tuple3";
    }

    public String toString() {
        return new StringBuilder().append((Object) "(").append(_1()).append((Object) ",").append(_2()).append((Object) ",").append(_3()).append((Object) ")").toString();
    }
}
