package scala.sys;

public final class package$ {
    public static final package$ MODULE$ = null;

    static {
        new package$();
    }

    private package$() {
        MODULE$ = this;
    }

    public final SystemProperties props() {
        return new SystemProperties();
    }
}
