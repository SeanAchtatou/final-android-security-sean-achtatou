package scala.sys;

import scala.Option;
import scala.Option$;
import scala.Serializable;
import scala.runtime.AbstractFunction0;

public final class SystemProperties$$anonfun$get$1 extends AbstractFunction0<Option<String>> implements Serializable {
    private final String key$2;

    public SystemProperties$$anonfun$get$1(SystemProperties systemProperties, String str) {
        this.key$2 = str;
    }

    public final Option<String> apply() {
        return Option$.MODULE$.apply(System.getProperty(this.key$2));
    }
}
