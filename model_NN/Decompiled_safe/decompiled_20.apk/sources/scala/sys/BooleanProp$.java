package scala.sys;

import scala.sys.BooleanProp;

public final class BooleanProp$ {
    public static final BooleanProp$ MODULE$ = null;

    static {
        new BooleanProp$();
    }

    private BooleanProp$() {
        MODULE$ = this;
    }

    public final <T> BooleanProp keyExists(String str) {
        return new BooleanProp.BooleanPropImpl(str, new BooleanProp$$anonfun$keyExists$1());
    }

    public final <T> BooleanProp valueIsTrue(String str) {
        return new BooleanProp.BooleanPropImpl(str, new BooleanProp$$anonfun$valueIsTrue$1());
    }
}
