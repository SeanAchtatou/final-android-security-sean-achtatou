package scala.sys;

import java.security.AccessControlException;
import scala.Function0;
import scala.None$;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.MapLike;
import scala.collection.mutable.AbstractMap;
import scala.runtime.BoxesRunTime;

public class SystemProperties extends AbstractMap<String, String> {
    public SystemProperties $minus$eq(String str) {
        wrapAccess(new SystemProperties$$anonfun$$minus$eq$1(this, str));
        return this;
    }

    public SystemProperties $plus$eq(Tuple2<String, String> tuple2) {
        wrapAccess(new SystemProperties$$anonfun$$plus$eq$1(this, tuple2));
        return this;
    }

    public boolean contains(String str) {
        Option wrapAccess = wrapAccess(new SystemProperties$$anonfun$contains$1(this, str));
        return !wrapAccess.isEmpty() && BoxesRunTime.unboxToBoolean(wrapAccess.get());
    }

    /* renamed from: default  reason: not valid java name */
    public String m4default(String str) {
        return null;
    }

    public SystemProperties empty() {
        return new SystemProperties();
    }

    public Option<String> get(String str) {
        Option wrapAccess = wrapAccess(new SystemProperties$$anonfun$get$1(this, str));
        return wrapAccess.isEmpty() ? None$.MODULE$ : (Option) wrapAccess.get();
    }

    public Iterator<Tuple2<String, String>> iterator() {
        Option wrapAccess = wrapAccess(new SystemProperties$$anonfun$iterator$1(this));
        return (Iterator) (wrapAccess.isEmpty() ? Iterator$.MODULE$.empty() : wrapAccess.get());
    }

    public boolean scala$sys$SystemProperties$$super$contains(String str) {
        return MapLike.Cclass.contains(this, str);
    }

    public <T> Option<T> wrapAccess(Function0<T> function0) {
        try {
            return new Some(function0.apply());
        } catch (AccessControlException e) {
            return None$.MODULE$;
        }
    }
}
