package scala.sys;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class BooleanProp$$anonfun$valueIsTrue$1 extends AbstractFunction1<String, Object> implements Serializable {
    public final /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply((String) obj));
    }

    public final boolean apply(String str) {
        String lowerCase = str.toLowerCase();
        return lowerCase != null && lowerCase.equals("true");
    }
}
