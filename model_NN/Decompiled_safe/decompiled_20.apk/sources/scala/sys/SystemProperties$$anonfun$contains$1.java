package scala.sys;

import scala.Serializable;
import scala.runtime.AbstractFunction0$mcZ$sp;

public final class SystemProperties$$anonfun$contains$1 extends AbstractFunction0$mcZ$sp implements Serializable {
    public final /* synthetic */ SystemProperties $outer;
    public final String key$1;

    public SystemProperties$$anonfun$contains$1(SystemProperties systemProperties, String str) {
        if (systemProperties == null) {
            throw new NullPointerException();
        }
        this.$outer = systemProperties;
        this.key$1 = str;
    }

    public final boolean apply() {
        return this.$outer.scala$sys$SystemProperties$$super$contains(this.key$1);
    }
}
