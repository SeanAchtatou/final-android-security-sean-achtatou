package scala.sys;

import scala.collection.immutable.Nil$;
import scala.collection.mutable.Map;
import scala.collection.mutable.Map$;

public final class SystemProperties$ {
    public static final SystemProperties$ MODULE$ = null;
    private volatile byte bitmap$0;
    private BooleanProp noTraceSupression;
    private Map<String, String> propertyHelp;

    static {
        new SystemProperties$();
    }

    private SystemProperties$() {
        MODULE$ = this;
    }

    private <P extends Prop<?>> P addHelp(P p, String str) {
        propertyHelp().update(p.key(), str);
        return p;
    }

    private BooleanProp bool(String str, String str2) {
        return (BooleanProp) addHelp(str.startsWith("java.") ? BooleanProp$.MODULE$.valueIsTrue(str) : BooleanProp$.MODULE$.keyExists(str), str2);
    }

    private BooleanProp noTraceSupression$lzycompute() {
        synchronized (this) {
            if (((byte) (this.bitmap$0 & 16)) == 0) {
                this.noTraceSupression = bool("scala.control.noTraceSuppression", "scala should not suppress any stack trace creation");
                this.bitmap$0 = (byte) (this.bitmap$0 | 16);
            }
        }
        return this.noTraceSupression;
    }

    private Map<String, String> propertyHelp() {
        return ((byte) (this.bitmap$0 & 1)) == 0 ? propertyHelp$lzycompute() : this.propertyHelp;
    }

    private Map propertyHelp$lzycompute() {
        synchronized (this) {
            if (((byte) (this.bitmap$0 & 1)) == 0) {
                this.propertyHelp = (Map) Map$.MODULE$.apply(Nil$.MODULE$);
                this.bitmap$0 = (byte) (this.bitmap$0 | 1);
            }
        }
        return this.propertyHelp;
    }

    public final BooleanProp noTraceSupression() {
        return ((byte) (this.bitmap$0 & 16)) == 0 ? noTraceSupression$lzycompute() : this.noTraceSupression;
    }
}
