package scala.sys;

import scala.Function1;
import scala.runtime.BoxesRunTime;

public interface BooleanProp extends Prop<Object> {

    public class BooleanPropImpl extends PropImpl<Object> implements BooleanProp {
        public BooleanPropImpl(String str, Function1<String, Object> function1) {
            super(str, function1);
        }

        public /* synthetic */ boolean value() {
            return BoxesRunTime.unboxToBoolean(value());
        }
    }

    boolean value();
}
