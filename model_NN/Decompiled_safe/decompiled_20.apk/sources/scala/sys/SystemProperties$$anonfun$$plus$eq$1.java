package scala.sys;

import scala.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction0;

public final class SystemProperties$$anonfun$$plus$eq$1 extends AbstractFunction0<String> implements Serializable {
    private final Tuple2 kv$1;

    public SystemProperties$$anonfun$$plus$eq$1(SystemProperties systemProperties, Tuple2 tuple2) {
        this.kv$1 = tuple2;
    }

    public final String apply() {
        return System.setProperty((String) this.kv$1._1(), (String) this.kv$1._2());
    }
}
