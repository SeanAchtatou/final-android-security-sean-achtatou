package scala.sys;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Function1;
import scala.Predef$;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.Map;
import scala.collection.mutable.StringBuilder;

public class PropImpl<T> implements Prop<T> {
    private final String key;
    private final Function1<String, T> valueFn;

    public PropImpl(String str, Function1<String, T> function1) {
        this.key = str;
        this.valueFn = function1;
    }

    private String getString() {
        return isSet() ? new StringBuilder().append((Object) "currently: ").append((Object) get()).toString() : "unset";
    }

    public String get() {
        return isSet() ? (String) underlying().getOrElse(key(), new PropImpl$$anonfun$get$1(this)) : PoiTypeDef.All;
    }

    public boolean isSet() {
        return underlying().contains(key());
    }

    public String key() {
        return this.key;
    }

    public String toString() {
        Predef$ predef$ = Predef$.MODULE$;
        return new StringOps("%s (%s)").format(Predef$.MODULE$.genericWrapArray(new Object[]{key(), getString()}));
    }

    public Map<String, String> underlying() {
        return package$.MODULE$.props();
    }

    public T value() {
        return isSet() ? this.valueFn.apply(get()) : zero();
    }

    public T zero() {
        return null;
    }
}
