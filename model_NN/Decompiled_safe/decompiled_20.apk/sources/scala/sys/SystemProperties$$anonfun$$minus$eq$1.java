package scala.sys;

import scala.Serializable;
import scala.runtime.AbstractFunction0;

public final class SystemProperties$$anonfun$$minus$eq$1 extends AbstractFunction0<String> implements Serializable {
    private final String key$3;

    public SystemProperties$$anonfun$$minus$eq$1(SystemProperties systemProperties, String str) {
        this.key$3 = str;
    }

    public final String apply() {
        return System.clearProperty(this.key$3);
    }
}
