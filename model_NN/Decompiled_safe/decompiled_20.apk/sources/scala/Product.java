package scala;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.collection.Iterator;

public interface Product extends Equals {

    /* renamed from: scala.Product$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Product product) {
        }

        public static Iterator productIterator(Product product) {
            return new Product$$anon$1(product);
        }

        public static String productPrefix(Product product) {
            return PoiTypeDef.All;
        }
    }

    int productArity();

    Object productElement(int i);

    Iterator<Object> productIterator();

    String productPrefix();
}
