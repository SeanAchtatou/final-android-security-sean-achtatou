package scala;

import scala.collection.Iterable;

public final class Option$ implements Serializable {
    public static final Option$ MODULE$ = null;

    static {
        new Option$();
    }

    private Option$() {
        MODULE$ = this;
    }

    public final <A> Option<A> apply(A a) {
        return a == null ? None$.MODULE$ : new Some(a);
    }

    public final <A> Iterable<A> option2Iterable(Option<A> option) {
        return option.toList();
    }
}
