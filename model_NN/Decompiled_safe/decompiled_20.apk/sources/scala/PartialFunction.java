package scala;

import scala.Function1;
import scala.runtime.AbstractFunction1;

public interface PartialFunction<A, B> extends Function1<A, B> {

    public class AndThen<A, B, C> implements PartialFunction<A, C> {
        private final Function1<B, C> k;
        private final PartialFunction<A, B> pf;

        public AndThen(PartialFunction<A, B> partialFunction, Function1<B, C> function1) {
            this.pf = partialFunction;
            this.k = function1;
            Function1.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <C> PartialFunction<A, C> andThen(Function1<C, C> function1) {
            return Cclass.andThen(this, function1);
        }

        public C apply(A a) {
            return this.k.apply(this.pf.apply(a));
        }

        public <A1 extends A, C1> C1 applyOrElse(A1 a1, Function1<A1, C1> function1) {
            Object applyOrElse = this.pf.applyOrElse(a1, PartialFunction$.MODULE$.scala$PartialFunction$$checkFallback());
            return PartialFunction$.MODULE$.scala$PartialFunction$$fallbackOccurred(applyOrElse) ? function1.apply(a1) : this.k.apply(applyOrElse);
        }

        public <A> Function1<A, C> compose(Function1<A, A> function1) {
            return Function1.Cclass.compose(this, function1);
        }

        public boolean isDefinedAt(A a) {
            return this.pf.isDefinedAt(a);
        }

        public String toString() {
            return Function1.Cclass.toString(this);
        }
    }

    public class Lifted<A, B> extends AbstractFunction1<A, Option<B>> {
        private final PartialFunction<A, B> pf;

        public Lifted(PartialFunction<A, B> partialFunction) {
            this.pf = partialFunction;
        }

        public Option<B> apply(A a) {
            Object applyOrElse = pf().applyOrElse(a, PartialFunction$.MODULE$.scala$PartialFunction$$checkFallback());
            return PartialFunction$.MODULE$.scala$PartialFunction$$fallbackOccurred(applyOrElse) ? None$.MODULE$ : new Some(applyOrElse);
        }

        public PartialFunction<A, B> pf() {
            return this.pf;
        }
    }

    public class OrElse<A, B> implements PartialFunction<A, B> {
        private final PartialFunction<A, B> f1;
        private final PartialFunction<A, B> f2;

        public OrElse(PartialFunction<A, B> partialFunction, PartialFunction<A, B> partialFunction2) {
            this.f1 = partialFunction;
            this.f2 = partialFunction2;
            Function1.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <C> OrElse<A, C> andThen(Function1<B, C> function1) {
            return new OrElse<>(this.f1.andThen((Function1) function1), this.f2.andThen((Function1) function1));
        }

        public B apply(A a) {
            return this.f1.applyOrElse(a, this.f2);
        }

        public <A1 extends A, B1> B1 applyOrElse(A1 a1, Function1<A1, B1> function1) {
            B1 applyOrElse = this.f1.applyOrElse(a1, PartialFunction$.MODULE$.scala$PartialFunction$$checkFallback());
            return PartialFunction$.MODULE$.scala$PartialFunction$$fallbackOccurred(applyOrElse) ? this.f2.applyOrElse(a1, function1) : applyOrElse;
        }

        public <A> Function1<A, B> compose(Function1<A, A> function1) {
            return Function1.Cclass.compose(this, function1);
        }

        public boolean isDefinedAt(A a) {
            return this.f1.isDefinedAt(a) || this.f2.isDefinedAt(a);
        }

        public String toString() {
            return Function1.Cclass.toString(this);
        }
    }

    /* renamed from: scala.PartialFunction$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(PartialFunction partialFunction) {
        }

        public static PartialFunction andThen(PartialFunction partialFunction, Function1 function1) {
            return new AndThen(partialFunction, function1);
        }

        public static Object applyOrElse(PartialFunction partialFunction, Object obj, Function1 function1) {
            return partialFunction.isDefinedAt(obj) ? partialFunction.apply(obj) : function1.apply(obj);
        }

        public static Function1 lift(PartialFunction partialFunction) {
            return new Lifted(partialFunction);
        }

        public static PartialFunction orElse(PartialFunction partialFunction, PartialFunction partialFunction2) {
            return new OrElse(partialFunction, partialFunction2);
        }

        public static Function1 runWith(PartialFunction partialFunction, Function1 function1) {
            return new PartialFunction$$anonfun$runWith$1(partialFunction, function1);
        }
    }

    <C> PartialFunction<A, C> andThen(Function1 function1);

    <A1 extends A, B1> B1 applyOrElse(A1 a1, Function1<A1, B1> function1);

    boolean isDefinedAt(Object obj);
}
