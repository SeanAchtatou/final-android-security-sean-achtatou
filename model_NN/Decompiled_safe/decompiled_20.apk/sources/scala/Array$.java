package scala;

import scala.compat.Platform$;
import scala.runtime.ScalaRunTime$;

public final class Array$ extends FallbackArrayBuilding implements Serializable {
    public static final Array$ MODULE$ = null;
    private final boolean[] emptyBooleanArray = new boolean[0];
    private final byte[] emptyByteArray = new byte[0];
    private final char[] emptyCharArray = new char[0];
    private final double[] emptyDoubleArray = new double[0];
    private final float[] emptyFloatArray = new float[0];
    private final int[] emptyIntArray = new int[0];
    private final long[] emptyLongArray = new long[0];
    private final Object[] emptyObjectArray = new Object[0];
    private final short[] emptyShortArray = new short[0];

    static {
        new Array$();
    }

    private Array$() {
        MODULE$ = this;
    }

    private void slowcopy(Object obj, int i, Object obj2, int i2, int i3) {
        int i4 = i + i3;
        while (i < i4) {
            ScalaRunTime$.MODULE$.array_update(obj2, i2, ScalaRunTime$.MODULE$.array_apply(obj, i));
            i++;
            i2++;
        }
    }

    public final void copy(Object obj, int i, Object obj2, int i2, int i3) {
        Class<?> cls = obj.getClass();
        if (!cls.isArray() || !obj2.getClass().isAssignableFrom(cls)) {
            slowcopy(obj, i, obj2, i2, i3);
            return;
        }
        Platform$ platform$ = Platform$.MODULE$;
        System.arraycopy(obj, i, obj2, i2, i3);
    }
}
