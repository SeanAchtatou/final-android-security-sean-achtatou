package scala;

public interface Function2<T1, T2, R> {

    /* renamed from: scala.Function2$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Function2 function2) {
        }

        public static String toString(Function2 function2) {
            return "<function2>";
        }
    }

    R apply(T1 t1, T2 t2);
}
