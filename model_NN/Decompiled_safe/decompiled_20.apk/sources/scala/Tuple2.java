package scala;

import scala.Product;
import scala.Product2;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.ScalaRunTime$;

public class Tuple2<T1, T2> implements Product2<T1, T2>, Product {
    public final T1 _1;
    public final T2 _2;

    public Tuple2(T1 t1, T2 t2) {
        this._1 = t1;
        this._2 = t2;
        Product.Cclass.$init$(this);
        Product2.Cclass.$init$(this);
    }

    public T1 _1() {
        return this._1;
    }

    public T2 _2() {
        return this._2;
    }

    public boolean canEqual(Object obj) {
        return obj instanceof Tuple2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002e, code lost:
        if (r0 == false) goto L_0x0074;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r2 = 1
            r1 = 0
            if (r5 == r6) goto L_0x0030
            boolean r0 = r6 instanceof scala.Tuple2
            if (r0 == 0) goto L_0x0032
            r0 = r2
        L_0x0009:
            if (r0 == 0) goto L_0x0074
            scala.Tuple2 r6 = (scala.Tuple2) r6
            java.lang.Object r0 = r5._1()
            java.lang.Object r3 = r6._1()
            if (r0 != r3) goto L_0x0034
            r0 = r2
        L_0x0018:
            if (r0 == 0) goto L_0x0072
            java.lang.Object r0 = r5._2()
            java.lang.Object r3 = r6._2()
            if (r0 != r3) goto L_0x0053
            r0 = r2
        L_0x0025:
            if (r0 == 0) goto L_0x0072
            boolean r0 = r6.canEqual(r5)
            if (r0 == 0) goto L_0x0072
            r0 = r2
        L_0x002e:
            if (r0 == 0) goto L_0x0074
        L_0x0030:
            r0 = r2
        L_0x0031:
            return r0
        L_0x0032:
            r0 = r1
            goto L_0x0009
        L_0x0034:
            if (r0 != 0) goto L_0x0038
            r0 = r1
            goto L_0x0018
        L_0x0038:
            boolean r4 = r0 instanceof java.lang.Number
            if (r4 == 0) goto L_0x0043
            java.lang.Number r0 = (java.lang.Number) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
            goto L_0x0018
        L_0x0043:
            boolean r4 = r0 instanceof java.lang.Character
            if (r4 == 0) goto L_0x004e
            java.lang.Character r0 = (java.lang.Character) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
            goto L_0x0018
        L_0x004e:
            boolean r0 = r0.equals(r3)
            goto L_0x0018
        L_0x0053:
            if (r0 != 0) goto L_0x0057
            r0 = r1
            goto L_0x0025
        L_0x0057:
            boolean r4 = r0 instanceof java.lang.Number
            if (r4 == 0) goto L_0x0062
            java.lang.Number r0 = (java.lang.Number) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
            goto L_0x0025
        L_0x0062:
            boolean r4 = r0 instanceof java.lang.Character
            if (r4 == 0) goto L_0x006d
            java.lang.Character r0 = (java.lang.Character) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
            goto L_0x0025
        L_0x006d:
            boolean r0 = r0.equals(r3)
            goto L_0x0025
        L_0x0072:
            r0 = r1
            goto L_0x002e
        L_0x0074:
            r0 = r1
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.Tuple2.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public int productArity() {
        return Product2.Cclass.productArity(this);
    }

    public Object productElement(int i) {
        return Product2.Cclass.productElement(this, i);
    }

    public Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public String productPrefix() {
        return "Tuple2";
    }

    public String toString() {
        return new StringBuilder().append((Object) "(").append(_1()).append((Object) ",").append(_2()).append((Object) ")").toString();
    }
}
