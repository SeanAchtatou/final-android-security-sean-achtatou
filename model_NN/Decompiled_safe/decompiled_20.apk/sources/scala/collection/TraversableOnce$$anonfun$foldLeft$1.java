package scala.collection;

import scala.Function2;
import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.ObjectRef;

public final class TraversableOnce$$anonfun$foldLeft$1 extends AbstractFunction1<A, BoxedUnit> implements Serializable {
    private final Function2 op$1;
    private final ObjectRef result$2;

    public TraversableOnce$$anonfun$foldLeft$1(TraversableOnce traversableOnce, ObjectRef objectRef, Function2 function2) {
        this.result$2 = objectRef;
        this.op$1 = function2;
    }

    public final void apply(A a) {
        this.result$2.elem = this.op$1.apply(this.result$2.elem, a);
    }
}
