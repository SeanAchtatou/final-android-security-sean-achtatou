package scala.collection;

import scala.collection.SortedSet;
import scala.collection.SortedSetLike;
import scala.collection.generic.Sorted;
import scala.math.Ordering;

public interface SortedSetLike<A, This extends SortedSet<A> & SortedSetLike<A, This>> extends SetLike<A, This>, Sorted<A, This> {

    /* renamed from: scala.collection.SortedSetLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(SortedSetLike sortedSetLike) {
        }

        public static SortedSet keySet(SortedSetLike sortedSetLike) {
            return (SortedSet) sortedSetLike.repr();
        }

        public static boolean subsetOf(SortedSetLike sortedSetLike, GenSet genSet) {
            if (genSet instanceof SortedSet) {
                SortedSet sortedSet = (SortedSet) genSet;
                Ordering ordering = sortedSet.ordering();
                Ordering ordering2 = sortedSetLike.ordering();
                if (ordering != null ? ordering.equals(ordering2) : ordering2 == null) {
                    return sortedSet.hasAll(sortedSetLike.iterator());
                }
            }
            return sortedSetLike.scala$collection$SortedSetLike$$super$subsetOf(genSet);
        }
    }

    Ordering<A> ordering();

    boolean scala$collection$SortedSetLike$$super$subsetOf(GenSet<A> genSet);
}
