package scala.collection;

import scala.Equals;
import scala.Function1;
import scala.Predef$;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;
import scala.math.package$;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt$;
import scala.runtime.ScalaRunTime$;

public interface IterableLike<A, Repr> extends Equals, GenIterableLike<A, Repr>, TraversableLike<A, Repr> {

    /* renamed from: scala.collection.IterableLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(IterableLike iterableLike) {
        }

        public static boolean canEqual(IterableLike iterableLike, Object obj) {
            return true;
        }

        public static void copyToArray(IterableLike iterableLike, Object obj, int i, int i2) {
            RichInt$ richInt$ = RichInt$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            int min$extension = richInt$.min$extension(i + i2, ScalaRunTime$.MODULE$.array_length(obj));
            Iterator it = iterableLike.iterator();
            while (i < min$extension && it.hasNext()) {
                ScalaRunTime$.MODULE$.array_update(obj, i, it.next());
                i++;
            }
        }

        public static Object drop(IterableLike iterableLike, int i) {
            Builder newBuilder = iterableLike.newBuilder();
            newBuilder.sizeHint(iterableLike, -package$.MODULE$.max(0, i));
            Iterator it = iterableLike.iterator();
            for (int i2 = 0; i2 < i && it.hasNext(); i2++) {
                it.next();
            }
            return ((Builder) newBuilder.$plus$plus$eq(it)).result();
        }

        public static boolean exists(IterableLike iterableLike, Function1 function1) {
            return iterableLike.iterator().exists(function1);
        }

        public static boolean forall(IterableLike iterableLike, Function1 function1) {
            return iterableLike.iterator().forall(function1);
        }

        public static void foreach(IterableLike iterableLike, Function1 function1) {
            iterableLike.iterator().foreach(function1);
        }

        public static Object head(IterableLike iterableLike) {
            return iterableLike.iterator().next();
        }

        public static boolean isEmpty(IterableLike iterableLike) {
            return !iterableLike.iterator().hasNext();
        }

        public static boolean sameElements(IterableLike iterableLike, GenIterable genIterable) {
            boolean equals;
            Iterator it = iterableLike.iterator();
            Iterator it2 = genIterable.iterator();
            while (it.hasNext() && it2.hasNext()) {
                Object next = it.next();
                Object next2 = it2.next();
                if (next == next2) {
                    equals = true;
                    continue;
                } else if (next == null) {
                    equals = false;
                    continue;
                } else if (next instanceof Number) {
                    equals = BoxesRunTime.equalsNumObject((Number) next, next2);
                    continue;
                } else if (next instanceof Character) {
                    equals = BoxesRunTime.equalsCharObject((Character) next, next2);
                    continue;
                } else {
                    equals = next.equals(next2);
                    continue;
                }
                if (!equals) {
                    return false;
                }
            }
            return !it.hasNext() && !it2.hasNext();
        }

        public static Object slice(IterableLike iterableLike, int i, int i2) {
            int max = package$.MODULE$.max(i, 0);
            int i3 = i2 - max;
            Builder newBuilder = iterableLike.newBuilder();
            if (i3 > 0) {
                newBuilder.sizeHintBounded(i3, iterableLike);
                Iterator drop = iterableLike.iterator().drop(max);
                for (int i4 = 0; i4 < i3 && drop.hasNext(); i4++) {
                    newBuilder.$plus$eq(drop.next());
                }
            }
            return newBuilder.result();
        }

        public static Object take(IterableLike iterableLike, int i) {
            Builder newBuilder = iterableLike.newBuilder();
            if (i > 0) {
                newBuilder.sizeHintBounded(i, iterableLike);
                Iterator it = iterableLike.iterator();
                for (int i2 = 0; i2 < i && it.hasNext(); i2++) {
                    newBuilder.$plus$eq(it.next());
                }
            }
            return newBuilder.result();
        }

        public static Iterable thisCollection(IterableLike iterableLike) {
            return (Iterable) iterableLike;
        }

        public static Stream toStream(IterableLike iterableLike) {
            return iterableLike.iterator().toStream();
        }
    }

    boolean canEqual(Object obj);

    <B> void copyToArray(Object obj, int i, int i2);

    boolean exists(Function1<A, Object> function1);

    <U> void foreach(Function1<A, U> function1);

    A head();

    Iterator<A> iterator();

    Repr take(int i);

    Iterable<A> thisCollection();
}
