package scala.collection;

import scala.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

public final class BitSetLike$$anonfun$addString$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public final /* synthetic */ BitSetLike $outer;
    public final ObjectRef pre$1;
    public final StringBuilder sb$1;
    public final String sep$1;

    public BitSetLike$$anonfun$addString$1(BitSetLike bitSetLike, ObjectRef objectRef, StringBuilder stringBuilder, String str) {
        if (bitSetLike == null) {
            throw new NullPointerException();
        }
        this.$outer = bitSetLike;
        this.pre$1 = objectRef;
        this.sb$1 = stringBuilder;
        this.sep$1 = str;
    }

    public final /* synthetic */ Object apply(Object obj) {
        apply$mcVI$sp(BoxesRunTime.unboxToInt(obj));
        return BoxedUnit.UNIT;
    }

    public final void apply(int i) {
        apply$mcVI$sp(i);
    }

    public final void apply$mcVI$sp(int i) {
        if (this.$outer.contains(i)) {
            this.sb$1.append((String) this.pre$1.elem).append(i);
            this.pre$1.elem = this.sep$1;
        }
    }
}
