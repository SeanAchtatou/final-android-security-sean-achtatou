package scala.collection;

import scala.Function1;

public final class Iterator$$anon$11 extends AbstractIterator<B> {
    private final /* synthetic */ Iterator $outer;
    private final Function1 f$3;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.collection.Iterator<A>, scala.Function1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Iterator$$anon$11(scala.collection.Iterator r2, scala.collection.Iterator<A> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.f$3 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.Iterator$$anon$11.<init>(scala.collection.Iterator, scala.Function1):void");
    }

    public final boolean hasNext() {
        return this.$outer.hasNext();
    }

    public final B next() {
        return this.f$3.apply(this.$outer.next());
    }
}
