package scala.collection;

import scala.collection.generic.SetFactory;
import scala.collection.immutable.Set;
import scala.collection.mutable.Builder;

public final class Set$ extends SetFactory<Set> {
    public static final Set$ MODULE$ = null;

    static {
        new Set$();
    }

    private Set$() {
        MODULE$ = this;
    }

    public final <A> Set<A> empty() {
        return scala.collection.immutable.Set$.MODULE$.empty();
    }

    public final <A> Builder<A, Set<A>> newBuilder() {
        return scala.collection.immutable.Set$.MODULE$.newBuilder();
    }
}
