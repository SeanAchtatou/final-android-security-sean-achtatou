package scala.collection;

import scala.Function1;
import scala.collection.GenSet;
import scala.collection.GenSetLike;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.TraversableLike;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;

public abstract class AbstractSet<A> extends AbstractIterable<A> implements Set<A> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Subtractable, scala.collection.GenSetLike, scala.collection.SetLike, scala.collection.AbstractSet, scala.collection.GenSet, scala.Function1, scala.collection.generic.GenericSetTemplate, scala.collection.Set] */
    public AbstractSet() {
        Function1.Cclass.$init$(this);
        GenSetLike.Cclass.$init$(this);
        GenericSetTemplate.Cclass.$init$(this);
        GenSet.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        SetLike.Cclass.$init$(this);
        Set.Cclass.$init$(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.AbstractSet] */
    public Set<A> $plus$plus(GenTraversableOnce<A> genTraversableOnce) {
        return SetLike.Cclass.$plus$plus(this, genTraversableOnce);
    }

    public <A> Function1<A, A> andThen(Function1<Object, A> function1) {
        return Function1.Cclass.andThen(this, function1);
    }

    public boolean apply(A a) {
        return GenSetLike.Cclass.apply(this, a);
    }

    public GenericCompanion<Set> companion() {
        return Set.Cclass.companion(this);
    }

    public <A> Function1<A, Object> compose(Function1<A, A> function1) {
        return Function1.Cclass.compose(this, function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.GenSet, scala.collection.Set<A>] */
    public Set<A> empty() {
        return GenericSetTemplate.Cclass.empty(this);
    }

    public boolean equals(Object obj) {
        return GenSetLike.Cclass.equals(this, obj);
    }

    public int hashCode() {
        return GenSetLike.Cclass.hashCode(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.AbstractSet] */
    public boolean isEmpty() {
        return SetLike.Cclass.isEmpty(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.AbstractSet] */
    public <B, That> That map(Function1<A, B> function1, CanBuildFrom<Set<A>, B, That> canBuildFrom) {
        return SetLike.Cclass.map(this, function1, canBuildFrom);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.AbstractSet] */
    public Builder<A, Set<A>> newBuilder() {
        return SetLike.Cclass.newBuilder(this);
    }

    public Object scala$collection$SetLike$$super$map(Function1 function1, CanBuildFrom canBuildFrom) {
        return TraversableLike.Cclass.map(this, function1, canBuildFrom);
    }

    public Set<A> seq() {
        return Set.Cclass.seq(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.AbstractSet] */
    public String stringPrefix() {
        return SetLike.Cclass.stringPrefix(this);
    }

    public boolean subsetOf(GenSet<A> genSet) {
        return GenSetLike.Cclass.subsetOf(this, genSet);
    }

    public /* bridge */ /* synthetic */ Traversable thisCollection() {
        return thisCollection();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.AbstractSet] */
    public <A1> Buffer<A1> toBuffer() {
        return SetLike.Cclass.toBuffer(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.AbstractSet] */
    public String toString() {
        return SetLike.Cclass.toString(this);
    }
}
