package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;
import scala.runtime.BooleanRef;

public final class TraversableLike$$anonfun$isEmpty$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public final /* synthetic */ TraversableLike $outer;
    public final BooleanRef result$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.runtime.BooleanRef, scala.collection.TraversableLike<A, Repr>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TraversableLike$$anonfun$isEmpty$1(scala.collection.TraversableLike r2, scala.collection.TraversableLike<A, Repr> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.result$1 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.TraversableLike$$anonfun$isEmpty$1.<init>(scala.collection.TraversableLike, scala.runtime.BooleanRef):void");
    }

    public final void apply() {
        this.$outer.foreach(new TraversableLike$$anonfun$isEmpty$1$$anonfun$apply$mcV$sp$1(this));
    }

    public final void apply$mcV$sp() {
        this.$outer.foreach(new TraversableLike$$anonfun$isEmpty$1$$anonfun$apply$mcV$sp$1(this));
    }
}
