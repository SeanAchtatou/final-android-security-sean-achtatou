package scala.collection;

import scala.Function1;
import scala.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;
import scala.runtime.BooleanRef;

public final class TraversableLike$$anonfun$exists$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public final /* synthetic */ TraversableLike $outer;
    public final Function1 p$5;
    public final BooleanRef result$3;

    public TraversableLike$$anonfun$exists$1(TraversableLike traversableLike, BooleanRef booleanRef, Function1 function1) {
        if (traversableLike == null) {
            throw new NullPointerException();
        }
        this.$outer = traversableLike;
        this.result$3 = booleanRef;
        this.p$5 = function1;
    }

    public final void apply() {
        this.$outer.foreach(new TraversableLike$$anonfun$exists$1$$anonfun$apply$mcV$sp$3(this));
    }

    public final void apply$mcV$sp() {
        this.$outer.foreach(new TraversableLike$$anonfun$exists$1$$anonfun$apply$mcV$sp$3(this));
    }
}
