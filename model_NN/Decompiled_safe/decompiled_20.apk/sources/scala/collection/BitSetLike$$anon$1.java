package scala.collection;

import scala.runtime.BoxesRunTime;

public final class BitSetLike$$anon$1 extends AbstractIterator<Object> {
    private final /* synthetic */ BitSetLike $outer;
    private int current;
    private final int end;

    public BitSetLike$$anon$1(BitSetLike<This> bitSetLike) {
        if (bitSetLike == null) {
            throw new NullPointerException();
        }
        this.$outer = bitSetLike;
        this.current = 0;
        this.end = bitSetLike.nwords() * BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength();
    }

    private int current() {
        return this.current;
    }

    private void current_$eq(int i) {
        this.current = i;
    }

    private int end() {
        return this.end;
    }

    public final boolean hasNext() {
        while (current() < end() && !this.$outer.contains(current())) {
            this.current = current() + 1;
        }
        return current() < end();
    }

    public final int next() {
        if (!hasNext()) {
            return BoxesRunTime.unboxToInt(Iterator$.MODULE$.empty().next());
        }
        int current2 = current();
        this.current = current() + 1;
        return current2;
    }
}
