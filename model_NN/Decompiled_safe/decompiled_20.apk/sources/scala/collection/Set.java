package scala.collection;

import scala.Function1;
import scala.collection.generic.GenericCompanion;

public interface Set<A> extends Function1<A, Object>, GenSet<A>, Iterable<A> {

    /* renamed from: scala.collection.Set$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Set set) {
        }

        public static GenericCompanion companion(Set set) {
            return Set$.MODULE$;
        }

        public static Set seq(Set set) {
            return set;
        }
    }

    Set<A> seq();
}
