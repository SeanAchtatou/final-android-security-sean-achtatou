package scala.collection.generic;

import scala.Tuple2;
import scala.collection.GenMap;
import scala.collection.Seq;
import scala.collection.mutable.Builder;
import scala.collection.mutable.MapBuilder;

public abstract class GenMapFactory<CC extends GenMap<Object, Object>> {

    public class MapCanBuildFrom<A, B> implements CanBuildFrom<CC, Tuple2<A, B>, CC> {
        public final /* synthetic */ GenMapFactory $outer;

        public MapCanBuildFrom(GenMapFactory<CC> genMapFactory) {
            if (genMapFactory == null) {
                throw new NullPointerException();
            }
            this.$outer = genMapFactory;
        }

        public Builder<Tuple2<A, B>, CC> apply() {
            return scala$collection$generic$GenMapFactory$MapCanBuildFrom$$$outer().newBuilder();
        }

        public Builder<Tuple2<A, B>, CC> apply(CC cc) {
            return scala$collection$generic$GenMapFactory$MapCanBuildFrom$$$outer().newBuilder();
        }

        public /* synthetic */ GenMapFactory scala$collection$generic$GenMapFactory$MapCanBuildFrom$$$outer() {
            return this.$outer;
        }
    }

    public <A, B> CC apply(Seq<Tuple2<A, B>> seq) {
        return (GenMap) ((Builder) newBuilder().$plus$plus$eq(seq)).result();
    }

    public abstract <A, B> CC empty();

    public <A, B> Builder<Tuple2<A, B>, CC> newBuilder() {
        return new MapBuilder(empty());
    }
}
