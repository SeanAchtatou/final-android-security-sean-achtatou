package scala.collection.generic;

import scala.collection.GenTraversable;
import scala.collection.mutable.Builder;
import scala.runtime.Nothing$;

public abstract class GenTraversableFactory<CC extends GenTraversable<Object>> extends GenericCompanion<CC> {
    private GenTraversableFactory<CC>.GenericCanBuildFrom<Nothing$> ReusableCBF;
    private volatile boolean bitmap$0;

    public class GenericCanBuildFrom<A> implements CanBuildFrom<CC, A, CC> {
        public final /* synthetic */ GenTraversableFactory $outer;

        public GenericCanBuildFrom(GenTraversableFactory<CC> genTraversableFactory) {
            if (genTraversableFactory == null) {
                throw new NullPointerException();
            }
            this.$outer = genTraversableFactory;
        }

        public Builder<A, CC> apply() {
            return scala$collection$generic$GenTraversableFactory$GenericCanBuildFrom$$$outer().newBuilder();
        }

        public Builder<A, CC> apply(GenTraversable genTraversable) {
            return genTraversable.genericBuilder();
        }

        public /* synthetic */ GenTraversableFactory scala$collection$generic$GenTraversableFactory$GenericCanBuildFrom$$$outer() {
            return this.$outer;
        }
    }

    public class ReusableCBF extends GenTraversableFactory<CC>.GenericCanBuildFrom<Nothing$> {
        public ReusableCBF(GenTraversableFactory<CC> genTraversableFactory) {
            super(genTraversableFactory);
        }

        public Builder<Nothing$, CC> apply() {
            return scala$collection$generic$GenTraversableFactory$ReusableCBF$$$outer().newBuilder();
        }

        public /* synthetic */ GenTraversableFactory scala$collection$generic$GenTraversableFactory$ReusableCBF$$$outer() {
            return this.$outer;
        }
    }

    private GenericCanBuildFrom ReusableCBF$lzycompute() {
        synchronized (this) {
            if (!this.bitmap$0) {
                this.ReusableCBF = new ReusableCBF(this);
                this.bitmap$0 = true;
            }
        }
        return this.ReusableCBF;
    }

    public GenTraversableFactory<CC>.GenericCanBuildFrom<Nothing$> ReusableCBF() {
        return this.bitmap$0 ? this.ReusableCBF : ReusableCBF$lzycompute();
    }
}
