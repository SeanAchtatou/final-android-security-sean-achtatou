package scala.collection.generic;

import scala.collection.mutable.Builder;
import scala.collection.mutable.GrowingBuilder;
import scala.collection.mutable.Set;

public abstract class MutableSetFactory<CC extends Set<Object>> extends SetFactory<CC> {
    public <A> Builder<A, CC> newBuilder() {
        return new GrowingBuilder((Growable) empty());
    }
}
