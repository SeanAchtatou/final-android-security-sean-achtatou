package scala.collection.generic;

import scala.collection.TraversableOnce;

public interface Growable<A> {

    /* renamed from: scala.collection.generic.Growable$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Growable growable) {
        }

        public static Growable $plus$plus$eq(Growable growable, TraversableOnce traversableOnce) {
            traversableOnce.seq().foreach(new Growable$$anonfun$$plus$plus$eq$1(growable));
            return growable;
        }
    }

    Growable<A> $plus$eq(Object obj);

    Growable<A> $plus$plus$eq(TraversableOnce<A> traversableOnce);
}
