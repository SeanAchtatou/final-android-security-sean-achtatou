package scala.collection.generic;

import scala.collection.GenTraversable;
import scala.collection.Seq;
import scala.collection.mutable.Builder;

public abstract class GenericCompanion<CC extends GenTraversable<Object>> {
    public <A> CC apply(Seq<A> seq) {
        if (seq.isEmpty()) {
            return empty();
        }
        Builder newBuilder = newBuilder();
        newBuilder.$plus$plus$eq(seq);
        return (GenTraversable) newBuilder.result();
    }

    public <A> CC empty() {
        return (GenTraversable) newBuilder().result();
    }

    public abstract <A> Builder<A, CC> newBuilder();
}
