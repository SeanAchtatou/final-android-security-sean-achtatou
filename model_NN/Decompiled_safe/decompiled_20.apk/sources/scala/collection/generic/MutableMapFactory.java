package scala.collection.generic;

import scala.Tuple2;
import scala.collection.mutable.Builder;
import scala.collection.mutable.Map;

public abstract class MutableMapFactory<CC extends Map<Object, Object>> extends MapFactory<CC> {
    public <A, B> Builder<Tuple2<A, B>, CC> newBuilder() {
        return (Builder) empty();
    }
}
