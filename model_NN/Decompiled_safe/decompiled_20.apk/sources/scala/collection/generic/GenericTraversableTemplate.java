package scala.collection.generic;

import scala.collection.GenTraversable;
import scala.collection.mutable.Builder;

public interface GenericTraversableTemplate<A, CC extends GenTraversable<Object>> {

    /* renamed from: scala.collection.generic.GenericTraversableTemplate$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(GenericTraversableTemplate genericTraversableTemplate) {
        }

        public static Builder genericBuilder(GenericTraversableTemplate genericTraversableTemplate) {
            return genericTraversableTemplate.companion().newBuilder();
        }

        public static Builder newBuilder(GenericTraversableTemplate genericTraversableTemplate) {
            return genericTraversableTemplate.companion().newBuilder();
        }
    }

    GenericCompanion<CC> companion();

    <B> Builder<B, CC> genericBuilder();
}
