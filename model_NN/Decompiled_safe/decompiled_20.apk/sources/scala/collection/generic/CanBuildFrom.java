package scala.collection.generic;

import scala.collection.mutable.Builder;

public interface CanBuildFrom<From, Elem, To> {
    Builder<Elem, To> apply();

    Builder<Elem, To> apply(From from);
}
