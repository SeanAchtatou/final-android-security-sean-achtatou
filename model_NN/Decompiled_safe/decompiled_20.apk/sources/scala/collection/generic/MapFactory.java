package scala.collection.generic;

import scala.collection.Map;

public abstract class MapFactory<CC extends Map<Object, Object>> extends GenMapFactory<CC> {
    public abstract <A, B> CC empty();
}
