package scala.collection.generic;

import scala.collection.mutable.Builder;

public final class GenSetFactory$$anon$1 implements CanBuildFrom<CC, A, CC> {
    private final /* synthetic */ GenSetFactory $outer;

    public GenSetFactory$$anon$1(GenSetFactory<CC> genSetFactory) {
        if (genSetFactory == null) {
            throw new NullPointerException();
        }
        this.$outer = genSetFactory;
    }

    public final Builder<A, CC> apply() {
        return this.$outer.newBuilder();
    }

    public final Builder<A, CC> apply(CC cc) {
        return this.$outer.newBuilder();
    }
}
