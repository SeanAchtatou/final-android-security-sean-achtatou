package scala.collection.generic;

import scala.collection.GenSet;
import scala.collection.mutable.Builder;

public abstract class GenSetFactory<CC extends GenSet<Object>> extends GenericCompanion<CC> {
    public abstract <A> Builder<A, CC> newBuilder();

    public <A> Object setCanBuildFrom() {
        return new GenSetFactory$$anon$1(this);
    }
}
