package scala.collection.generic;

import scala.collection.immutable.Set;
import scala.collection.mutable.Builder;
import scala.collection.mutable.SetBuilder;

public abstract class ImmutableSetFactory<CC extends Set<Object>> extends SetFactory<CC> {
    public <A> Builder<A, CC> newBuilder() {
        return new SetBuilder((scala.collection.Set) empty());
    }
}
