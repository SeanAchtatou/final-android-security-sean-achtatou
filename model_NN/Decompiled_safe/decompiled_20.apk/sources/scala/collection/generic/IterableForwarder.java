package scala.collection.generic;

import scala.collection.GenIterable;
import scala.collection.Iterable;

public interface IterableForwarder<A> extends Iterable<A>, TraversableForwarder<A> {

    /* renamed from: scala.collection.generic.IterableForwarder$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(IterableForwarder iterableForwarder) {
        }

        public static boolean sameElements(IterableForwarder iterableForwarder, GenIterable genIterable) {
            return iterableForwarder.underlying().sameElements(genIterable);
        }
    }

    Iterable<A> underlying();
}
