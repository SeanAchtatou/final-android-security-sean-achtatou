package scala.collection.generic;

import scala.Function1;
import scala.Function2;
import scala.Predef$$less$colon$less;
import scala.collection.Traversable;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ClassTag;

public interface TraversableForwarder<A> extends Traversable<A> {

    /* renamed from: scala.collection.generic.TraversableForwarder$class  reason: invalid class name */
    public abstract class Cclass {
        public static Object $div$colon(TraversableForwarder traversableForwarder, Object obj, Function2 function2) {
            return traversableForwarder.underlying().$div$colon(obj, function2);
        }

        public static void $init$(TraversableForwarder traversableForwarder) {
        }

        public static StringBuilder addString(TraversableForwarder traversableForwarder, StringBuilder stringBuilder, String str, String str2, String str3) {
            return traversableForwarder.underlying().addString(stringBuilder, str, str2, str3);
        }

        public static void copyToArray(TraversableForwarder traversableForwarder, Object obj, int i) {
            traversableForwarder.underlying().copyToArray(obj, i);
        }

        public static void copyToArray(TraversableForwarder traversableForwarder, Object obj, int i, int i2) {
            traversableForwarder.underlying().copyToArray(obj, i, i2);
        }

        public static void copyToBuffer(TraversableForwarder traversableForwarder, Buffer buffer) {
            traversableForwarder.underlying().copyToBuffer(buffer);
        }

        public static boolean exists(TraversableForwarder traversableForwarder, Function1 function1) {
            return traversableForwarder.underlying().exists(function1);
        }

        public static Object foldLeft(TraversableForwarder traversableForwarder, Object obj, Function2 function2) {
            return traversableForwarder.underlying().foldLeft(obj, function2);
        }

        public static boolean forall(TraversableForwarder traversableForwarder, Function1 function1) {
            return traversableForwarder.underlying().forall(function1);
        }

        public static void foreach(TraversableForwarder traversableForwarder, Function1 function1) {
            traversableForwarder.underlying().foreach(function1);
        }

        public static Object head(TraversableForwarder traversableForwarder) {
            return traversableForwarder.underlying().head();
        }

        public static boolean isEmpty(TraversableForwarder traversableForwarder) {
            return traversableForwarder.underlying().isEmpty();
        }

        public static Object last(TraversableForwarder traversableForwarder) {
            return traversableForwarder.underlying().last();
        }

        public static String mkString(TraversableForwarder traversableForwarder) {
            return traversableForwarder.underlying().mkString();
        }

        public static String mkString(TraversableForwarder traversableForwarder, String str) {
            return traversableForwarder.underlying().mkString(str);
        }

        public static String mkString(TraversableForwarder traversableForwarder, String str, String str2, String str3) {
            return traversableForwarder.underlying().mkString(str, str2, str3);
        }

        public static boolean nonEmpty(TraversableForwarder traversableForwarder) {
            return traversableForwarder.underlying().nonEmpty();
        }

        public static Object toArray(TraversableForwarder traversableForwarder, ClassTag classTag) {
            return traversableForwarder.underlying().toArray(classTag);
        }

        public static Buffer toBuffer(TraversableForwarder traversableForwarder) {
            return traversableForwarder.underlying().toBuffer();
        }

        public static Map toMap(TraversableForwarder traversableForwarder, Predef$$less$colon$less predef$$less$colon$less) {
            return traversableForwarder.underlying().toMap(predef$$less$colon$less);
        }

        public static Set toSet(TraversableForwarder traversableForwarder) {
            return traversableForwarder.underlying().toSet();
        }

        public static Stream toStream(TraversableForwarder traversableForwarder) {
            return traversableForwarder.underlying().toStream();
        }
    }

    Traversable<A> underlying();
}
