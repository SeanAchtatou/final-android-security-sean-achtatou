package scala.collection.generic;

import scala.Serializable;
import scala.runtime.AbstractFunction1;

public final class Growable$$anonfun$$plus$plus$eq$1 extends AbstractFunction1<A, Growable<A>> implements Serializable {
    private final /* synthetic */ Growable $outer;

    public Growable$$anonfun$$plus$plus$eq$1(Growable<A> growable) {
        if (growable == null) {
            throw new NullPointerException();
        }
        this.$outer = growable;
    }

    public final Growable<A> apply(A a) {
        return this.$outer.$plus$eq(a);
    }
}
