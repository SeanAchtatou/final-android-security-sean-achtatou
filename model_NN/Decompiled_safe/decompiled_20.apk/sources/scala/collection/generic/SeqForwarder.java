package scala.collection.generic;

import scala.Function1;
import scala.Function2;
import scala.collection.GenSeq;
import scala.collection.Iterator;
import scala.collection.Seq;

public interface SeqForwarder<A> extends Seq<A>, IterableForwarder<A> {

    /* renamed from: scala.collection.generic.SeqForwarder$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(SeqForwarder seqForwarder) {
        }

        public static Object apply(SeqForwarder seqForwarder, int i) {
            return seqForwarder.underlying().apply(i);
        }

        public static boolean corresponds(SeqForwarder seqForwarder, GenSeq genSeq, Function2 function2) {
            return seqForwarder.underlying().corresponds(genSeq, function2);
        }

        public static boolean isDefinedAt(SeqForwarder seqForwarder, int i) {
            return seqForwarder.underlying().isDefinedAt(i);
        }

        public static int lengthCompare(SeqForwarder seqForwarder, int i) {
            return seqForwarder.underlying().lengthCompare(i);
        }

        public static int prefixLength(SeqForwarder seqForwarder, Function1 function1) {
            return seqForwarder.underlying().prefixLength(function1);
        }

        public static Iterator reverseIterator(SeqForwarder seqForwarder) {
            return seqForwarder.underlying().reverseIterator();
        }

        public static int segmentLength(SeqForwarder seqForwarder, Function1 function1, int i) {
            return seqForwarder.underlying().segmentLength(function1, i);
        }
    }

    boolean isDefinedAt(int i);

    Seq<A> underlying();
}
