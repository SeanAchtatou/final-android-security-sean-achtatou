package scala.collection.generic;

import scala.Function1;

public interface FilterMonadic<A, Repr> {
    <U> void foreach(Function1<A, U> function1);
}
