package scala.collection.generic;

import scala.collection.Iterator;
import scala.collection.SortedSet;
import scala.collection.generic.Sorted;
import scala.math.Ordering;

public interface Sorted<K, This extends Sorted<K, This>> {

    /* renamed from: scala.collection.generic.Sorted$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Sorted sorted) {
        }

        public static int compare(Sorted sorted, Object obj, Object obj2) {
            return sorted.ordering().compare(obj, obj2);
        }

        public static boolean hasAll(Sorted sorted, Iterator iterator) {
            boolean z;
            Iterator it = sorted.keySet().iterator();
            if (it.isEmpty()) {
                return iterator.isEmpty();
            }
            Object next = it.next();
            while (iterator.hasNext()) {
                Object next2 = iterator.next();
                while (true) {
                    int compare = sorted.compare(next2, next);
                    if (compare == 0) {
                        z = false;
                    } else if (compare < 0) {
                        return false;
                    } else {
                        if (!it.hasNext()) {
                            return false;
                        }
                        z = true;
                    }
                    if (z) {
                        next = it.next();
                    }
                }
            }
            return true;
        }
    }

    int compare(K k, K k2);

    boolean hasAll(Iterator<K> iterator);

    SortedSet<K> keySet();

    Ordering<K> ordering();
}
