package scala.collection.generic;

import scala.collection.GenSet;

public interface GenericSetTemplate<A, CC extends GenSet<Object>> extends GenericTraversableTemplate<A, CC> {

    /* renamed from: scala.collection.generic.GenericSetTemplate$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(GenericSetTemplate genericSetTemplate) {
        }

        public static GenSet empty(GenericSetTemplate genericSetTemplate) {
            return (GenSet) genericSetTemplate.companion().empty();
        }
    }

    CC empty();
}
