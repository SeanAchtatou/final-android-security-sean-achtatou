package scala.collection;

import scala.MatchError;
import scala.Predef$;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;
import scala.runtime.StringAdd$;

public final class MapLike$$anonfun$addString$1 extends AbstractFunction1<Tuple2<A, B>, String> implements Serializable {
    public MapLike$$anonfun$addString$1(MapLike<A, B, This> mapLike) {
    }

    public final String apply(Tuple2<A, B> tuple2) {
        if (tuple2 != null) {
            return new StringBuilder().append((Object) StringAdd$.MODULE$.$plus$extension(Predef$.MODULE$.any2stringadd(tuple2._1()), " -> ")).append((Object) tuple2._2()).toString();
        }
        throw new MatchError(tuple2);
    }
}
