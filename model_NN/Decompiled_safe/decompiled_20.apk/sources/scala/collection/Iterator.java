package scala.collection;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Function1;
import scala.Predef$;
import scala.StringContext;
import scala.collection.immutable.Stream;
import scala.collection.immutable.Stream$;
import scala.collection.immutable.Stream$Empty$;
import scala.collection.immutable.Stream$cons$;
import scala.collection.mutable.StringBuilder;
import scala.math.package$;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt$;
import scala.runtime.ScalaRunTime$;

public interface Iterator<A> extends TraversableOnce<A> {

    /* renamed from: scala.collection.Iterator$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Iterator iterator) {
        }

        public static void copyToArray(Iterator iterator, Object obj, int i, int i2) {
            Predef$ predef$ = Predef$.MODULE$;
            if (i >= 0 && i < ScalaRunTime$.MODULE$.array_length(obj)) {
                package$ package_ = package$.MODULE$;
                int min = Math.min(i2, ScalaRunTime$.MODULE$.array_length(obj) - i) + i;
                while (i < min && iterator.hasNext()) {
                    ScalaRunTime$.MODULE$.array_update(obj, i, iterator.next());
                    i++;
                }
                return;
            }
            throw new IllegalArgumentException(new StringBuilder().append((Object) "requirement failed: ").append((Object) new StringContext(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"start ", " out of range ", PoiTypeDef.All})).s(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(i), BoxesRunTime.boxToInteger(ScalaRunTime$.MODULE$.array_length(obj))}))).toString());
        }

        public static Iterator drop(Iterator iterator, int i) {
            return iterator.slice(i, Integer.MAX_VALUE);
        }

        public static boolean exists(Iterator iterator, Function1 function1) {
            boolean z = false;
            while (!z && iterator.hasNext()) {
                z = BoxesRunTime.unboxToBoolean(function1.apply(iterator.next()));
            }
            return z;
        }

        public static boolean forall(Iterator iterator, Function1 function1) {
            boolean z = true;
            while (z && iterator.hasNext()) {
                z = BoxesRunTime.unboxToBoolean(function1.apply(iterator.next()));
            }
            return z;
        }

        public static void foreach(Iterator iterator, Function1 function1) {
            while (iterator.hasNext()) {
                function1.apply(iterator.next());
            }
        }

        public static boolean isEmpty(Iterator iterator) {
            return !iterator.hasNext();
        }

        public static boolean isTraversableAgain(Iterator iterator) {
            return false;
        }

        public static Iterator map(Iterator iterator, Function1 function1) {
            return new Iterator$$anon$11(iterator, function1);
        }

        public static Iterator seq(Iterator iterator) {
            return iterator;
        }

        public static Iterator slice(Iterator iterator, int i, int i2) {
            RichInt$ richInt$ = RichInt$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            int max$extension = richInt$.max$extension(i, 0);
            for (int i3 = max$extension; i3 > 0 && iterator.hasNext(); i3--) {
                iterator.next();
            }
            return new Iterator$$anon$10(iterator, max$extension, i2);
        }

        public static Iterator take(Iterator iterator, int i) {
            return iterator.slice(0, i);
        }

        public static Stream toStream(Iterator iterator) {
            if (iterator.hasNext()) {
                Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
                return new Stream.Cons(iterator.next(), new Iterator$$anonfun$toStream$1(iterator));
            }
            Stream$ stream$ = Stream$.MODULE$;
            return Stream$Empty$.MODULE$;
        }

        public static String toString(Iterator iterator) {
            return new StringBuilder().append((Object) (iterator.hasNext() ? "non-empty" : "empty")).append((Object) " iterator").toString();
        }
    }

    Iterator<A> drop(int i);

    boolean exists(Function1<A, Object> function1);

    boolean forall(Function1<A, Object> function1);

    <U> void foreach(Function1<A, U> function1);

    boolean hasNext();

    boolean isEmpty();

    <B> Iterator<B> map(Function1<A, B> function1);

    A next();

    Iterator<A> seq();

    Iterator<A> slice(int i, int i2);

    Stream<A> toStream();
}
