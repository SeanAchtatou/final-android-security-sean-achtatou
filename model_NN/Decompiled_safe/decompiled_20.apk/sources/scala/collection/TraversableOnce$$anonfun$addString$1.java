package scala.collection;

import scala.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;
import scala.runtime.BooleanRef;
import scala.runtime.BoxedUnit;

public final class TraversableOnce$$anonfun$addString$1 extends AbstractFunction1<A, Object> implements Serializable {
    private final StringBuilder b$2;
    private final BooleanRef first$2;
    private final String sep$1;

    public TraversableOnce$$anonfun$addString$1(TraversableOnce traversableOnce, BooleanRef booleanRef, StringBuilder stringBuilder, String str) {
        this.first$2 = booleanRef;
        this.b$2 = stringBuilder;
        this.sep$1 = str;
    }

    public final Object apply(A a) {
        if (this.first$2.elem) {
            this.b$2.append((Object) a);
            this.first$2.elem = false;
            return BoxedUnit.UNIT;
        }
        this.b$2.append(this.sep$1);
        return this.b$2.append((Object) a);
    }
}
