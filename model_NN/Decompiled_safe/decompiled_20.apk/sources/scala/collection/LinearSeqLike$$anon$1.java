package scala.collection;

import scala.collection.immutable.List;

public final class LinearSeqLike$$anon$1 extends AbstractIterator<A> {
    private final /* synthetic */ LinearSeqLike $outer;
    private Repr these;

    public LinearSeqLike$$anon$1(Repr repr) {
        if (repr == null) {
            throw new NullPointerException();
        }
        this.$outer = repr;
        this.these = repr;
    }

    private Repr these() {
        return this.these;
    }

    private void these_$eq(Repr repr) {
        this.these = repr;
    }

    public final boolean hasNext() {
        return !these().isEmpty();
    }

    public final A next() {
        if (!hasNext()) {
            return Iterator$.MODULE$.empty().next();
        }
        A head = these().head();
        this.these = (LinearSeqLike) these().tail();
        return head;
    }

    public final List<A> toList() {
        List<A> list = these().toList();
        this.these = (LinearSeqLike) this.$outer.newBuilder().result();
        return list;
    }
}
