package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.Nothing$;

public final class TraversableLike$$anonfun$head$1$$anonfun$apply$mcV$sp$5 extends AbstractFunction1<A, Nothing$> implements Serializable {
    private final /* synthetic */ TraversableLike$$anonfun$head$1 $outer;

    public TraversableLike$$anonfun$head$1$$anonfun$apply$mcV$sp$5(TraversableLike<A, Repr>.1 r2) {
        if (r2 == null) {
            throw new NullPointerException();
        }
        this.$outer = r2;
    }

    public final Nothing$ apply(A a) {
        this.$outer.result$5.elem = new TraversableLike$$anonfun$head$1$$anonfun$apply$mcV$sp$5$$anonfun$apply$1(this, a);
        return Traversable$.MODULE$.breaks().m6break();
    }
}
