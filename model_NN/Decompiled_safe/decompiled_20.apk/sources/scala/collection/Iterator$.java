package scala.collection;

import scala.runtime.Nothing$;

public final class Iterator$ {
    public static final Iterator$ MODULE$ = null;
    private final Iterator<Nothing$> empty = new Iterator$$anon$2();

    static {
        new Iterator$();
    }

    private Iterator$() {
        MODULE$ = this;
    }

    public final <A> Iterator<A> apply(Seq<A> seq) {
        return seq.iterator();
    }

    public final Iterator<Nothing$> empty() {
        return this.empty;
    }
}
