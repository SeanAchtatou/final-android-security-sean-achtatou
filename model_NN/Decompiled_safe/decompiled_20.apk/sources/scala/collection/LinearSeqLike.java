package scala.collection;

import scala.Function2;
import scala.collection.LinearSeqLike;
import scala.runtime.BoxesRunTime;
import scala.util.hashing.MurmurHash3$;

public interface LinearSeqLike<A, Repr extends LinearSeqLike<A, Repr>> extends SeqLike<A, Repr> {

    /* renamed from: scala.collection.LinearSeqLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(LinearSeqLike linearSeqLike) {
        }

        public static final boolean corresponds(LinearSeqLike linearSeqLike, GenSeq genSeq, Function2 function2) {
            while (!linearSeqLike.isEmpty()) {
                if (!genSeq.nonEmpty() || !BoxesRunTime.unboxToBoolean(function2.apply(linearSeqLike.head(), genSeq.head()))) {
                    return false;
                }
                genSeq = (GenSeq) genSeq.tail();
                linearSeqLike = (LinearSeqLike) linearSeqLike.tail();
            }
            return genSeq.isEmpty();
        }

        public static int hashCode(LinearSeqLike linearSeqLike) {
            return MurmurHash3$.MODULE$.seqHash(linearSeqLike.seq());
        }

        public static Iterator iterator(LinearSeqLike linearSeqLike) {
            return new LinearSeqLike$$anon$1(linearSeqLike);
        }

        public static LinearSeq thisCollection(LinearSeqLike linearSeqLike) {
            return (LinearSeq) linearSeqLike;
        }

        public static LinearSeq toCollection(LinearSeqLike linearSeqLike, LinearSeqLike linearSeqLike2) {
            return (LinearSeq) linearSeqLike2;
        }
    }

    <B> boolean corresponds(GenSeq<B> genSeq, Function2<A, B, Object> function2);

    LinearSeq<A> seq();

    LinearSeq<A> thisCollection();

    LinearSeq<A> toCollection(LinearSeqLike linearSeqLike);
}
