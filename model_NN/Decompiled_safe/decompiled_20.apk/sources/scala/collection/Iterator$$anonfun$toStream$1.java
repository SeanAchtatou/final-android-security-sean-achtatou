package scala.collection;

import scala.Serializable;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction0;

public final class Iterator$$anonfun$toStream$1 extends AbstractFunction0<Stream<A>> implements Serializable {
    private final /* synthetic */ Iterator $outer;

    public Iterator$$anonfun$toStream$1(Iterator<A> iterator) {
        if (iterator == null) {
            throw new NullPointerException();
        }
        this.$outer = iterator;
    }

    public final Stream<A> apply() {
        return this.$outer.toStream();
    }
}
