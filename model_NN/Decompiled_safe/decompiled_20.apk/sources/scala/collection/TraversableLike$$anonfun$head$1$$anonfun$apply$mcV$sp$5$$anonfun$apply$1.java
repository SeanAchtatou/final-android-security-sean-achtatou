package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction0;

public final class TraversableLike$$anonfun$head$1$$anonfun$apply$mcV$sp$5$$anonfun$apply$1 extends AbstractFunction0<A> implements Serializable {
    private final Object x$5;

    public TraversableLike$$anonfun$head$1$$anonfun$apply$mcV$sp$5$$anonfun$apply$1(TraversableLike$$anonfun$head$1$$anonfun$apply$mcV$sp$5 traversableLike$$anonfun$head$1$$anonfun$apply$mcV$sp$5, TraversableLike<A, Repr>.5 r2) {
        this.x$5 = r2;
    }

    public final A apply() {
        return this.x$5;
    }
}
