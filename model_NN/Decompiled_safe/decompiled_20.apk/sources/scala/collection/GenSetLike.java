package scala.collection;

import scala.Equals;
import scala.Function1;
import scala.util.hashing.MurmurHash3$;

public interface GenSetLike<A, Repr> extends Equals, Function1<A, Object>, GenIterableLike<A, Repr> {

    /* renamed from: scala.collection.GenSetLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(GenSetLike genSetLike) {
        }

        public static boolean apply(GenSetLike genSetLike, Object obj) {
            return genSetLike.contains(obj);
        }

        public static boolean equals(GenSetLike genSetLike, Object obj) {
            if (!(obj instanceof GenSet)) {
                return false;
            }
            GenSet genSet = (GenSet) obj;
            return genSetLike == genSet || (genSet.canEqual(genSetLike) && genSetLike.size() == genSet.size() && liftedTree1$1(genSetLike, genSet));
        }

        public static int hashCode(GenSetLike genSetLike) {
            return MurmurHash3$.MODULE$.setHash(genSetLike.seq());
        }

        private static final boolean liftedTree1$1(GenSetLike genSetLike, GenSet genSet) {
            try {
                return genSetLike.subsetOf(genSet);
            } catch (ClassCastException e) {
                return false;
            }
        }

        public static boolean subsetOf(GenSetLike genSetLike, GenSet genSet) {
            return genSetLike.forall(genSet);
        }
    }

    boolean apply(Object obj);

    boolean contains(Object obj);

    Set<A> seq();

    boolean subsetOf(GenSet<A> genSet);
}
