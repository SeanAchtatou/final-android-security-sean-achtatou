package scala.collection;

import scala.collection.generic.GenericSetTemplate;

public interface GenSet<A> extends GenIterable<A>, GenSetLike<A, GenSet<A>>, GenericSetTemplate<A, GenSet> {

    /* renamed from: scala.collection.GenSet$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(GenSet genSet) {
        }
    }
}
