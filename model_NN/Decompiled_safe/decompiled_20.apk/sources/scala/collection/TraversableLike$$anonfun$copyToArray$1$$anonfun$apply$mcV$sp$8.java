package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.ScalaRunTime$;

public final class TraversableLike$$anonfun$copyToArray$1$$anonfun$apply$mcV$sp$8 extends AbstractFunction1<A, BoxedUnit> implements Serializable {
    private final /* synthetic */ TraversableLike$$anonfun$copyToArray$1 $outer;

    public TraversableLike$$anonfun$copyToArray$1$$anonfun$apply$mcV$sp$8(TraversableLike<A, Repr>.1 r2) {
        if (r2 == null) {
            throw new NullPointerException();
        }
        this.$outer = r2;
    }

    public final void apply(A a) {
        if (this.$outer.i$3.elem >= this.$outer.end$1) {
            throw Traversable$.MODULE$.breaks().m6break();
        }
        ScalaRunTime$.MODULE$.array_update(this.$outer.xs$1, this.$outer.i$3.elem, a);
        this.$outer.i$3.elem++;
    }
}
