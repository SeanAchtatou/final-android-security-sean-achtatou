package scala.collection;

import scala.Function1;
import scala.collection.immutable.Stream;

public interface GenTraversableOnce<A> {

    /* renamed from: scala.collection.GenTraversableOnce$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(GenTraversableOnce genTraversableOnce) {
        }
    }

    boolean forall(Function1<A, Object> function1);

    boolean isEmpty();

    boolean isTraversableAgain();

    boolean nonEmpty();

    TraversableOnce<A> seq();

    Stream<A> toStream();
}
