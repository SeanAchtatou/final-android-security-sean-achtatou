package scala.collection;

import java.util.Properties;
import scala.collection.convert.DecorateAsJava;
import scala.collection.convert.DecorateAsScala;
import scala.collection.convert.Decorators;
import scala.collection.mutable.Map;

public final class JavaConverters$ implements DecorateAsJava, DecorateAsScala {
    public static final JavaConverters$ MODULE$ = null;

    static {
        new JavaConverters$();
    }

    private JavaConverters$() {
        MODULE$ = this;
        DecorateAsJava.Cclass.$init$(this);
        DecorateAsScala.Cclass.$init$(this);
    }

    public final Decorators.AsScala<Map<String, String>> propertiesAsScalaMapConverter(Properties properties) {
        return DecorateAsScala.Cclass.propertiesAsScalaMapConverter(this, properties);
    }
}
