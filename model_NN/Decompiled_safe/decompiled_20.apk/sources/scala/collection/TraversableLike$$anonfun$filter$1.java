package scala.collection;

import scala.Function1;
import scala.Serializable;
import scala.collection.mutable.Builder;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class TraversableLike$$anonfun$filter$1 extends AbstractFunction1<A, Object> implements Serializable {
    private final Builder b$3;
    private final Function1 p$1;

    public TraversableLike$$anonfun$filter$1(TraversableLike traversableLike, Builder builder, Function1 function1) {
        this.b$3 = builder;
        this.p$1 = function1;
    }

    public final Object apply(A a) {
        return BoxesRunTime.unboxToBoolean(this.p$1.apply(a)) ? this.b$3.$plus$eq((Object) a) : BoxedUnit.UNIT;
    }
}
