package scala.collection;

import scala.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class GenMapLike$$anonfun$liftedTree1$1$1 extends AbstractFunction1<Tuple2<A, B>, Object> implements Serializable {
    private final GenMap x2$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.GenMapLike<A, B, Repr>, scala.collection.GenMap] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public GenMapLike$$anonfun$liftedTree1$1$1(scala.collection.GenMapLike r1, scala.collection.GenMapLike<A, B, Repr> r2) {
        /*
            r0 = this;
            r0.x2$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.GenMapLike$$anonfun$liftedTree1$1$1.<init>(scala.collection.GenMapLike, scala.collection.GenMap):void");
    }

    public final /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply((Tuple2) obj));
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: B
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final boolean apply(scala.Tuple2<A, B> r6) {
        /*
            r5 = this;
            r2 = 1
            r3 = 0
            if (r6 == 0) goto L_0x0045
            scala.collection.GenMap r0 = r5.x2$1
            java.lang.Object r1 = r6._1()
            scala.Option r0 = r0.get(r1)
            boolean r1 = r0 instanceof scala.Some
            if (r1 == 0) goto L_0x0043
            scala.Some r0 = (scala.Some) r0
            java.lang.Object r1 = r6._2()
            java.lang.Object r4 = r0.x()
            if (r1 != r4) goto L_0x0023
            r0 = r2
        L_0x001f:
            if (r0 == 0) goto L_0x0043
            r0 = r2
        L_0x0022:
            return r0
        L_0x0023:
            if (r1 != 0) goto L_0x0027
            r0 = r3
            goto L_0x001f
        L_0x0027:
            boolean r0 = r1 instanceof java.lang.Number
            if (r0 == 0) goto L_0x0033
            r0 = r1
            java.lang.Number r0 = (java.lang.Number) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r4)
            goto L_0x001f
        L_0x0033:
            boolean r0 = r1 instanceof java.lang.Character
            if (r0 == 0) goto L_0x003e
            java.lang.Character r1 = (java.lang.Character) r1
            boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r4)
            goto L_0x001f
        L_0x003e:
            boolean r0 = r1.equals(r4)
            goto L_0x001f
        L_0x0043:
            r0 = r3
            goto L_0x0022
        L_0x0045:
            scala.MatchError r0 = new scala.MatchError
            r0.<init>(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.GenMapLike$$anonfun$liftedTree1$1$1.apply(scala.Tuple2):boolean");
    }
}
