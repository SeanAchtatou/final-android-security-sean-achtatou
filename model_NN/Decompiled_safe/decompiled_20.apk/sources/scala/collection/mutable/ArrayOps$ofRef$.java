package scala.collection.mutable;

import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.ArrayOps;
import scala.collection.mutable.WrappedArray;
import scala.reflect.ClassTag$;
import scala.runtime.ScalaRunTime$;

public class ArrayOps$ofRef$ {
    public static final ArrayOps$ofRef$ MODULE$ = null;

    static {
        new ArrayOps$ofRef$();
    }

    public ArrayOps$ofRef$() {
        MODULE$ = this;
    }

    public final <T> T apply$extension(T[] tArr, int i) {
        return tArr[i];
    }

    public final <T> boolean equals$extension(T[] tArr, Object obj) {
        if (obj instanceof ArrayOps.ofRef) {
            if (tArr == (obj == null ? null : ((ArrayOps.ofRef) obj).repr())) {
                return true;
            }
        }
        return false;
    }

    public final <T> int hashCode$extension(T[] tArr) {
        return tArr.hashCode();
    }

    public final <T> int length$extension(T[] tArr) {
        return tArr.length;
    }

    public final <T> ArrayBuilder.ofRef<T> newBuilder$extension(T[] tArr) {
        return new ArrayBuilder.ofRef<>(ClassTag$.MODULE$.apply(ScalaRunTime$.MODULE$.arrayElementClass(tArr.getClass())));
    }

    public final <T> WrappedArray<T> thisCollection$extension(T[] tArr) {
        return new WrappedArray.ofRef(tArr);
    }

    public final <T> WrappedArray<T> toCollection$extension(T[] tArr, T[] tArr2) {
        return new WrappedArray.ofRef(tArr2);
    }
}
