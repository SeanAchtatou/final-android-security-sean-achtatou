package scala.collection.mutable;

import scala.Array$;
import scala.Serializable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;
import scala.collection.mutable.WrappedArray;
import scala.reflect.ClassTag;

public abstract class ArrayBuilder<T> implements Serializable, Builder<T, Object> {

    public class ofRef<T> extends ArrayBuilder<T> {
        private int capacity = 0;
        private T[] elems;
        private final ClassTag<T> evidence$2;
        private int size = 0;

        public ofRef(ClassTag<T> classTag) {
            this.evidence$2 = classTag;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private T[] elems() {
            return this.elems;
        }

        private void elems_$eq(T[] tArr) {
            this.elems = tArr;
        }

        private void ensureSize(int i) {
            if (capacity() < i || capacity() == 0) {
                int capacity2 = capacity() == 0 ? 16 : capacity() * 2;
                while (capacity2 < i) {
                    capacity2 *= 2;
                }
                resize(capacity2);
            }
        }

        private T[] mkArray(int i) {
            T[] tArr = (Object[]) this.evidence$2.newArray(i);
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, tArr, 0, size());
            }
            return tArr;
        }

        private void resize(int i) {
            this.elems = mkArray(i);
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        public ofRef<T> $plus$eq(T t) {
            ensureSize(size() + 1);
            elems()[size()] = t;
            this.size = size() + 1;
            return this;
        }

        public ofRef<T> $plus$plus$eq(TraversableOnce<T> traversableOnce) {
            if (!(traversableOnce instanceof WrappedArray.ofRef)) {
                return (ofRef) Growable.Cclass.$plus$plus$eq(this, traversableOnce);
            }
            WrappedArray.ofRef ofref = (WrappedArray.ofRef) traversableOnce;
            ensureSize(size() + ofref.length());
            Array$.MODULE$.copy(ofref.array(), 0, elems(), size(), ofref.length());
            this.size = size() + ofref.length();
            return this;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof ofRef)) {
                return false;
            }
            ofRef ofref = (ofRef) obj;
            return size() == ofref.size() && elems() == ofref.elems();
        }

        public T[] result() {
            return (capacity() == 0 || capacity() != size()) ? mkArray(size()) : elems();
        }

        public void sizeHint(int i) {
            if (capacity() < i) {
                resize(i);
            }
        }

        public String toString() {
            return "ArrayBuilder.ofRef";
        }
    }

    public ArrayBuilder() {
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
    }

    public Growable<T> $plus$plus$eq(TraversableOnce<T> traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public void sizeHint(int i) {
        Builder.Cclass.sizeHint(this, i);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }
}
