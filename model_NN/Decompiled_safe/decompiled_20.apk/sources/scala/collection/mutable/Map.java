package scala.collection.mutable;

import scala.Tuple2;

public interface Map<A, B> extends scala.collection.Map<A, B>, Iterable<Tuple2<A, B>>, MapLike<A, B, Map<A, B>> {

    /* renamed from: scala.collection.mutable.Map$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Map map) {
        }

        public static Map empty(Map map) {
            return Map$.MODULE$.empty();
        }

        public static Map seq(Map map) {
            return map;
        }
    }

    Map<A, B> empty();

    Map<A, B> seq();
}
