package scala.collection.mutable;

import scala.Serializable;

public final class StringBuilder$ implements Serializable {
    public static final StringBuilder$ MODULE$ = null;

    static {
        new StringBuilder$();
    }

    private StringBuilder$() {
        MODULE$ = this;
    }

    public final StringBuilder newBuilder() {
        return new StringBuilder();
    }
}
