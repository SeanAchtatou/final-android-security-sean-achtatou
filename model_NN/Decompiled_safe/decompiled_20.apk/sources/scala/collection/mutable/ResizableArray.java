package scala.collection.mutable;

import scala.Array$;
import scala.Function1;
import scala.Predef$;
import scala.collection.generic.GenericTraversableTemplate;
import scala.compat.Platform$;
import scala.math.package$;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt$;
import scala.runtime.ScalaRunTime$;

public interface ResizableArray<A> extends GenericTraversableTemplate<A, ResizableArray>, IndexedSeq<A> {

    /* renamed from: scala.collection.mutable.ResizableArray$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(ResizableArray resizableArray) {
            resizableArray.array_$eq(new Object[package$.MODULE$.max(resizableArray.initialSize(), 1)]);
            resizableArray.size0_$eq(0);
        }

        public static Object apply(ResizableArray resizableArray, int i) {
            if (i < resizableArray.size0()) {
                return resizableArray.array()[i];
            }
            throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }

        public static void copyToArray(ResizableArray resizableArray, Object obj, int i, int i2) {
            RichInt$ richInt$ = RichInt$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            RichInt$ richInt$2 = RichInt$.MODULE$;
            Predef$ predef$2 = Predef$.MODULE$;
            Array$.MODULE$.copy(resizableArray.array(), 0, obj, i, richInt$.min$extension(richInt$2.min$extension(i2, ScalaRunTime$.MODULE$.array_length(obj) - i), resizableArray.length()));
        }

        public static void ensureSize(ResizableArray resizableArray, int i) {
            if (i > resizableArray.array().length) {
                int length = resizableArray.array().length * 2;
                while (i > length) {
                    length *= 2;
                }
                Object[] objArr = new Object[length];
                Platform$ platform$ = Platform$.MODULE$;
                System.arraycopy(resizableArray.array(), 0, objArr, 0, resizableArray.size0());
                resizableArray.array_$eq(objArr);
            }
        }

        public static void foreach(ResizableArray resizableArray, Function1 function1) {
            int size = resizableArray.size();
            for (int i = 0; i < size; i++) {
                function1.apply(resizableArray.array()[i]);
            }
        }

        public static int length(ResizableArray resizableArray) {
            return resizableArray.size0();
        }
    }

    A apply(int i);

    Object[] array();

    void array_$eq(Object[] objArr);

    void ensureSize(int i);

    int initialSize();

    int length();

    int size0();

    void size0_$eq(int i);
}
