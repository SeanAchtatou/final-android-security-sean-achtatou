package scala.collection.mutable;

import scala.Array$;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;
import scala.collection.mutable.WrappedArray;
import scala.reflect.ClassTag;
import scala.runtime.BoxedUnit;
import scala.runtime.ScalaRunTime$;

public class WrappedArrayBuilder<A> implements Builder<A, WrappedArray<A>> {
    private int capacity = 0;
    private WrappedArray<A> elems;
    private final ClassTag<A> manifest;
    private int size = 0;
    private final ClassTag<A> tag;

    public WrappedArrayBuilder(ClassTag<A> classTag) {
        this.tag = classTag;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        this.manifest = classTag;
    }

    private int capacity() {
        return this.capacity;
    }

    private void capacity_$eq(int i) {
        this.capacity = i;
    }

    private WrappedArray<A> elems() {
        return this.elems;
    }

    private void elems_$eq(WrappedArray<A> wrappedArray) {
        this.elems = wrappedArray;
    }

    private void ensureSize(int i) {
        if (capacity() < i) {
            int capacity2 = capacity() == 0 ? 16 : capacity() * 2;
            while (capacity2 < i) {
                capacity2 *= 2;
            }
            resize(capacity2);
        }
    }

    private WrappedArray<A> mkArray(int i) {
        WrappedArray ofbyte;
        Class<?> arrayElementClass = ScalaRunTime$.MODULE$.arrayElementClass(this.tag);
        Class cls = Byte.TYPE;
        if (cls != null ? !cls.equals(arrayElementClass) : arrayElementClass != null) {
            Class cls2 = Short.TYPE;
            if (cls2 != null ? !cls2.equals(arrayElementClass) : arrayElementClass != null) {
                Class cls3 = Character.TYPE;
                if (cls3 != null ? !cls3.equals(arrayElementClass) : arrayElementClass != null) {
                    Class cls4 = Integer.TYPE;
                    if (cls4 != null ? !cls4.equals(arrayElementClass) : arrayElementClass != null) {
                        Class cls5 = Long.TYPE;
                        if (cls5 != null ? !cls5.equals(arrayElementClass) : arrayElementClass != null) {
                            Class cls6 = Float.TYPE;
                            if (cls6 != null ? !cls6.equals(arrayElementClass) : arrayElementClass != null) {
                                Class cls7 = Double.TYPE;
                                if (cls7 != null ? !cls7.equals(arrayElementClass) : arrayElementClass != null) {
                                    Class cls8 = Boolean.TYPE;
                                    if (cls8 != null ? !cls8.equals(arrayElementClass) : arrayElementClass != null) {
                                        Class cls9 = Void.TYPE;
                                        ofbyte = (cls9 != null ? !cls9.equals(arrayElementClass) : arrayElementClass != null) ? new WrappedArray.ofRef((Object[]) this.tag.newArray(i)) : new WrappedArray.ofUnit(new BoxedUnit[i]);
                                    } else {
                                        ofbyte = new WrappedArray.ofBoolean(new boolean[i]);
                                    }
                                } else {
                                    ofbyte = new WrappedArray.ofDouble(new double[i]);
                                }
                            } else {
                                ofbyte = new WrappedArray.ofFloat(new float[i]);
                            }
                        } else {
                            ofbyte = new WrappedArray.ofLong(new long[i]);
                        }
                    } else {
                        ofbyte = new WrappedArray.ofInt(new int[i]);
                    }
                } else {
                    ofbyte = new WrappedArray.ofChar(new char[i]);
                }
            } else {
                ofbyte = new WrappedArray.ofShort(new short[i]);
            }
        } else {
            ofbyte = new WrappedArray.ofByte(new byte[i]);
        }
        if (size() > 0) {
            Array$.MODULE$.copy(elems().array(), 0, ofbyte.array(), 0, size());
        }
        return ofbyte;
    }

    private void resize(int i) {
        this.elems = mkArray(i);
        this.capacity = i;
    }

    private int size() {
        return this.size;
    }

    private void size_$eq(int i) {
        this.size = i;
    }

    public WrappedArrayBuilder<A> $plus$eq(A a) {
        ensureSize(size() + 1);
        elems().update(size(), a);
        this.size = size() + 1;
        return this;
    }

    public Growable<A> $plus$plus$eq(TraversableOnce<A> traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public WrappedArray<A> result() {
        return (capacity() == 0 || capacity() != size()) ? mkArray(size()) : elems();
    }

    public void sizeHint(int i) {
        if (capacity() < i) {
            resize(i);
        }
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }
}
