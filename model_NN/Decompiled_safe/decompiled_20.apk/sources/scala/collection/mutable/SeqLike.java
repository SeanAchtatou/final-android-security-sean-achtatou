package scala.collection.mutable;

import scala.collection.Parallelizable;
import scala.collection.mutable.Seq;
import scala.collection.mutable.SeqLike;
import scala.collection.parallel.mutable.ParSeq;

public interface SeqLike<A, This extends SeqLike<A, This> & Seq<A>> extends Parallelizable<A, ParSeq<A>>, scala.collection.SeqLike<A, This> {

    /* renamed from: scala.collection.mutable.SeqLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(SeqLike seqLike) {
        }
    }
}
