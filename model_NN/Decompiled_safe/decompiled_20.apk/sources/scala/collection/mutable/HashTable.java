package scala.collection.mutable;

import java.util.Arrays;
import scala.Function1;
import scala.collection.Iterator;
import scala.collection.mutable.HashEntry;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;
import scala.util.hashing.package$;

public interface HashTable<A, Entry extends HashEntry<A, Entry>> extends HashUtils<A> {

    public class Contents<A, Entry extends HashEntry<A, Entry>> {
        private final int loadFactor;
        private final int seedvalue;
        private final int[] sizemap;
        private final HashEntry<A, Entry>[] table;
        private final int tableSize;
        private final int threshold;

        public int loadFactor() {
            return this.loadFactor;
        }

        public int seedvalue() {
            return this.seedvalue;
        }

        public int[] sizemap() {
            return this.sizemap;
        }

        public HashEntry<A, Entry>[] table() {
            return this.table;
        }

        public int tableSize() {
            return this.tableSize;
        }

        public int threshold() {
            return this.threshold;
        }
    }

    public interface HashUtils<KeyType> {

        /* renamed from: scala.collection.mutable.HashTable$HashUtils$class  reason: invalid class name */
        public abstract class Cclass {
            public static void $init$(HashUtils hashUtils) {
            }

            public static int elemHashCode(HashUtils hashUtils, Object obj) {
                return ScalaRunTime$.MODULE$.hash(obj);
            }

            public static final int improve(HashUtils hashUtils, int i, int i2) {
                int byteswap32 = package$.MODULE$.byteswap32(i);
                int i3 = i2 % 32;
                return (byteswap32 << (32 - i3)) | (byteswap32 >>> i3);
            }

            public static final int sizeMapBucketBitSize(HashUtils hashUtils) {
                return 5;
            }

            public static final int sizeMapBucketSize(HashUtils hashUtils) {
                return 1 << hashUtils.sizeMapBucketBitSize();
            }
        }

        int elemHashCode(KeyType keytype);

        int improve(int i, int i2);

        int sizeMapBucketBitSize();

        int sizeMapBucketSize();
    }

    /* renamed from: scala.collection.mutable.HashTable$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(HashTable hashTable) {
            hashTable._loadFactor_$eq(HashTable$.MODULE$.defaultLoadFactor());
            hashTable.table_$eq(new HashEntry[initialCapacity(hashTable)]);
            hashTable.tableSize_$eq(0);
            hashTable.threshold_$eq(initialThreshold(hashTable, hashTable._loadFactor()));
            hashTable.sizemap_$eq(null);
            hashTable.seedvalue_$eq(hashTable.tableSizeSeed());
        }

        public static boolean alwaysInitSizeMap(HashTable hashTable) {
            return false;
        }

        public static int calcSizeMapSize(HashTable hashTable, int i) {
            return (i >> hashTable.sizeMapBucketBitSize()) + 1;
        }

        public static boolean elemEquals(HashTable hashTable, Object obj, Object obj2) {
            return obj == obj2 ? true : obj == null ? false : obj instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj, obj2) : obj instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj, obj2) : obj.equals(obj2);
        }

        public static Iterator entriesIterator(HashTable hashTable) {
            return new HashTable$$anon$1(hashTable);
        }

        public static HashEntry findEntry(HashTable hashTable, Object obj) {
            return scala$collection$mutable$HashTable$$findEntry0(hashTable, obj, hashTable.index(hashTable.elemHashCode(obj)));
        }

        public static HashEntry findOrAddEntry(HashTable hashTable, Object obj, Object obj2) {
            int index = hashTable.index(hashTable.elemHashCode(obj));
            HashEntry scala$collection$mutable$HashTable$$findEntry0 = scala$collection$mutable$HashTable$$findEntry0(hashTable, obj, index);
            if (scala$collection$mutable$HashTable$$findEntry0 != null) {
                return scala$collection$mutable$HashTable$$findEntry0;
            }
            scala$collection$mutable$HashTable$$addEntry0(hashTable, hashTable.createNewEntry(obj, obj2), index);
            return null;
        }

        public static void foreachEntry(HashTable hashTable, Function1 function1) {
            HashEntry[] table = hashTable.table();
            int scala$collection$mutable$HashTable$$lastPopulatedIndex = scala$collection$mutable$HashTable$$lastPopulatedIndex(hashTable);
            HashEntry hashEntry = table[scala$collection$mutable$HashTable$$lastPopulatedIndex];
            while (hashEntry != null) {
                function1.apply(hashEntry);
                hashEntry = (HashEntry) hashEntry.next();
                while (hashEntry == null && scala$collection$mutable$HashTable$$lastPopulatedIndex > 0) {
                    scala$collection$mutable$HashTable$$lastPopulatedIndex--;
                    hashEntry = table[scala$collection$mutable$HashTable$$lastPopulatedIndex];
                }
            }
        }

        public static final int index(HashTable hashTable, int i) {
            int length = hashTable.table().length - 1;
            return length & (hashTable.improve(i, hashTable.seedvalue()) >> (32 - Integer.bitCount(length)));
        }

        public static void initWithContents(HashTable hashTable, Contents contents) {
            if (contents != null) {
                hashTable._loadFactor_$eq(contents.loadFactor());
                hashTable.table_$eq(contents.table());
                hashTable.tableSize_$eq(contents.tableSize());
                hashTable.threshold_$eq(contents.threshold());
                hashTable.seedvalue_$eq(contents.seedvalue());
                hashTable.sizemap_$eq(contents.sizemap());
            }
            if (hashTable.alwaysInitSizeMap() && hashTable.sizemap() == null) {
                hashTable.sizeMapInitAndRebuild();
            }
        }

        private static int initialCapacity(HashTable hashTable) {
            return HashTable$.MODULE$.capacity(hashTable.initialSize());
        }

        public static int initialSize(HashTable hashTable) {
            return 16;
        }

        private static int initialThreshold(HashTable hashTable, int i) {
            return HashTable$.MODULE$.newThreshold(i, initialCapacity(hashTable));
        }

        public static void nnSizeMapAdd(HashTable hashTable, int i) {
            if (hashTable.sizemap() != null) {
                int[] sizemap = hashTable.sizemap();
                int sizeMapBucketBitSize = i >> hashTable.sizeMapBucketBitSize();
                sizemap[sizeMapBucketBitSize] = sizemap[sizeMapBucketBitSize] + 1;
            }
        }

        public static void nnSizeMapRemove(HashTable hashTable, int i) {
            if (hashTable.sizemap() != null) {
                int[] sizemap = hashTable.sizemap();
                int sizeMapBucketBitSize = i >> hashTable.sizeMapBucketBitSize();
                sizemap[sizeMapBucketBitSize] = sizemap[sizeMapBucketBitSize] - 1;
            }
        }

        public static void nnSizeMapReset(HashTable hashTable, int i) {
            if (hashTable.sizemap() != null) {
                int calcSizeMapSize = hashTable.calcSizeMapSize(i);
                if (hashTable.sizemap().length != calcSizeMapSize) {
                    hashTable.sizemap_$eq(new int[calcSizeMapSize]);
                } else {
                    Arrays.fill(hashTable.sizemap(), 0);
                }
            }
        }

        public static HashEntry removeEntry(HashTable hashTable, Object obj) {
            int index = hashTable.index(hashTable.elemHashCode(obj));
            HashEntry hashEntry = hashTable.table()[index];
            if (hashEntry != null) {
                if (hashTable.elemEquals(hashEntry.key(), obj)) {
                    hashTable.table()[index] = (HashEntry) hashEntry.next();
                    hashTable.tableSize_$eq(hashTable.tableSize() - 1);
                    hashTable.nnSizeMapRemove(index);
                    return hashEntry;
                }
                HashEntry hashEntry2 = hashEntry;
                HashEntry hashEntry3 = (HashEntry) hashEntry.next();
                while (hashEntry3 != null && !hashTable.elemEquals(hashEntry3.key(), obj)) {
                    hashEntry2 = hashEntry3;
                    hashEntry3 = (HashEntry) hashEntry3.next();
                }
                if (hashEntry3 != null) {
                    hashEntry2.next_$eq(hashEntry3.next());
                    hashTable.tableSize_$eq(hashTable.tableSize() - 1);
                    hashTable.nnSizeMapRemove(index);
                    return hashEntry3;
                }
            }
            return null;
        }

        private static void resize(HashTable hashTable, int i) {
            HashEntry[] table = hashTable.table();
            hashTable.table_$eq(new HashEntry[i]);
            hashTable.nnSizeMapReset(hashTable.table().length);
            for (int length = table.length - 1; length >= 0; length--) {
                for (HashEntry hashEntry = table[length]; hashEntry != null; hashEntry = (HashEntry) hashEntry.next()) {
                    int index = hashTable.index(hashTable.elemHashCode(hashEntry.key()));
                    hashEntry.next_$eq(hashTable.table()[index]);
                    hashTable.table()[index] = hashEntry;
                    hashTable.nnSizeMapAdd(index);
                }
            }
            hashTable.threshold_$eq(HashTable$.MODULE$.newThreshold(hashTable._loadFactor(), i));
        }

        public static void scala$collection$mutable$HashTable$$addEntry0(HashTable hashTable, HashEntry hashEntry, int i) {
            hashEntry.next_$eq(hashTable.table()[i]);
            hashTable.table()[i] = hashEntry;
            hashTable.tableSize_$eq(hashTable.tableSize() + 1);
            hashTable.nnSizeMapAdd(i);
            if (hashTable.tableSize() > hashTable.threshold()) {
                resize(hashTable, hashTable.table().length * 2);
            }
        }

        public static HashEntry scala$collection$mutable$HashTable$$findEntry0(HashTable hashTable, Object obj, int i) {
            HashEntry hashEntry = hashTable.table()[i];
            while (hashEntry != null && !hashTable.elemEquals(hashEntry.key(), obj)) {
                hashEntry = (HashEntry) hashEntry.next();
            }
            return hashEntry;
        }

        public static int scala$collection$mutable$HashTable$$lastPopulatedIndex(HashTable hashTable) {
            int length = hashTable.table().length;
            while (true) {
                length--;
                if (hashTable.table()[length] != null || length <= 0) {
                    return length;
                }
            }
            return length;
        }

        public static void sizeMapInit(HashTable hashTable, int i) {
            hashTable.sizemap_$eq(new int[hashTable.calcSizeMapSize(i)]);
        }

        public static void sizeMapInitAndRebuild(HashTable hashTable) {
            hashTable.sizeMapInit(hashTable.table().length);
            HashEntry[] table = hashTable.table();
            int length = table.length < hashTable.sizeMapBucketSize() ? table.length : hashTable.sizeMapBucketSize();
            int i = hashTable.totalSizeMapBuckets();
            int i2 = length;
            int i3 = 0;
            int i4 = 0;
            while (i3 < i) {
                int i5 = 0;
                int i6 = i4;
                while (i6 < i2) {
                    for (HashEntry hashEntry = table[i6]; hashEntry != null; hashEntry = (HashEntry) hashEntry.next()) {
                        i5++;
                    }
                    i6++;
                }
                hashTable.sizemap()[i3] = i5;
                i2 = hashTable.sizeMapBucketSize() + i2;
                i3++;
                i4 = i6;
            }
        }

        public static int tableSizeSeed(HashTable hashTable) {
            return Integer.bitCount(hashTable.table().length - 1);
        }

        public static final int totalSizeMapBuckets(HashTable hashTable) {
            if (hashTable.sizeMapBucketSize() < hashTable.table().length) {
                return 1;
            }
            return hashTable.table().length / hashTable.sizeMapBucketSize();
        }
    }

    int _loadFactor();

    void _loadFactor_$eq(int i);

    boolean alwaysInitSizeMap();

    int calcSizeMapSize(int i);

    <B> Entry createNewEntry(Object obj, Object obj2);

    boolean elemEquals(A a, A a2);

    Iterator<Entry> entriesIterator();

    Entry findEntry(A a);

    <B> Entry findOrAddEntry(A a, B b);

    <U> void foreachEntry(Function1<Entry, U> function1);

    int index(int i);

    void initWithContents(Contents<A, Entry> contents);

    int initialSize();

    void nnSizeMapAdd(int i);

    void nnSizeMapRemove(int i);

    void nnSizeMapReset(int i);

    Entry removeEntry(A a);

    int seedvalue();

    void seedvalue_$eq(int i);

    void sizeMapInit(int i);

    void sizeMapInitAndRebuild();

    int[] sizemap();

    void sizemap_$eq(int[] iArr);

    HashEntry<A, Entry>[] table();

    int tableSize();

    int tableSizeSeed();

    void tableSize_$eq(int i);

    void table_$eq(HashEntry<A, Entry>[] hashEntryArr);

    int threshold();

    void threshold_$eq(int i);

    int totalSizeMapBuckets();
}
