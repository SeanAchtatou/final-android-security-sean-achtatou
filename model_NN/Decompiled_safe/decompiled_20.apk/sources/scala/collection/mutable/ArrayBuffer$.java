package scala.collection.mutable;

import scala.Serializable;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;

public final class ArrayBuffer$ extends SeqFactory<ArrayBuffer> implements Serializable {
    public static final ArrayBuffer$ MODULE$ = null;

    static {
        new ArrayBuffer$();
    }

    private ArrayBuffer$() {
        MODULE$ = this;
    }

    public final <A> CanBuildFrom<ArrayBuffer<?>, A, ArrayBuffer<A>> canBuildFrom() {
        return ReusableCBF();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder<A, scala.collection.mutable.ArrayBuffer<A>>, scala.collection.mutable.ArrayBuffer] */
    public final <A> Builder<A, ArrayBuffer<A>> newBuilder() {
        return new ArrayBuffer();
    }
}
