package scala.collection.mutable;

import scala.Function1;
import scala.Option;
import scala.Serializable;
import scala.collection.CustomParallelizable;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterator;
import scala.collection.Set;
import scala.collection.Traversable;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;
import scala.collection.mutable.FlatHashTable;
import scala.collection.parallel.mutable.ParHashSet;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public class HashSet<A> extends AbstractSet<A> implements Serializable, CustomParallelizable<A, ParHashSet<A>>, GenericSetTemplate<A, HashSet> {
    private transient int _loadFactor;
    private transient int seedvalue;
    private transient int[] sizemap;
    private transient Object[] table;
    private transient int tableSize;
    private transient int threshold;

    public HashSet() {
        this(null);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.FlatHashTable$HashUtils, scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable, scala.collection.CustomParallelizable] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public HashSet(scala.collection.mutable.FlatHashTable.Contents<A> r1) {
        /*
            r0 = this;
            r0.<init>()
            scala.collection.mutable.FlatHashTable.HashUtils.Cclass.$init$(r0)
            scala.collection.mutable.FlatHashTable.Cclass.$init$(r0)
            scala.collection.CustomParallelizable.Cclass.$init$(r0)
            r0.initWithContents(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.HashSet.<init>(scala.collection.mutable.FlatHashTable$Contents):void");
    }

    public /* bridge */ /* synthetic */ Set $minus(Object obj) {
        return $minus(obj);
    }

    public HashSet<A> $minus$eq(A a) {
        removeEntry(a);
        return this;
    }

    public /* bridge */ /* synthetic */ Set $plus(Object obj) {
        return $plus(obj);
    }

    public HashSet<A> $plus$eq(Object obj) {
        addEntry(obj);
        return this;
    }

    public /* bridge */ /* synthetic */ Set $plus$plus(GenTraversableOnce genTraversableOnce) {
        return $plus$plus(genTraversableOnce);
    }

    public int _loadFactor() {
        return this._loadFactor;
    }

    public void _loadFactor_$eq(int i) {
        this._loadFactor = i;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public boolean addEntry(A a) {
        return FlatHashTable.Cclass.addEntry(this, a);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public boolean alwaysInitSizeMap() {
        return FlatHashTable.Cclass.alwaysInitSizeMap(this);
    }

    public /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply(obj));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public int calcSizeMapSize(int i) {
        return FlatHashTable.Cclass.calcSizeMapSize(this, i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public int capacity(int i) {
        return FlatHashTable.Cclass.capacity(this, i);
    }

    public HashSet<A> clone() {
        return (HashSet) new HashSet().$plus$plus$eq(this);
    }

    public GenericCompanion<HashSet> companion() {
        return HashSet$.MODULE$;
    }

    public boolean contains(A a) {
        return containsEntry(a);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public boolean containsEntry(A a) {
        return FlatHashTable.Cclass.containsEntry(this, a);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.FlatHashTable$HashUtils, scala.collection.mutable.HashSet] */
    public int elemHashCode(A a) {
        return FlatHashTable.HashUtils.Cclass.elemHashCode(this, a);
    }

    public /* bridge */ /* synthetic */ Set empty() {
        return (Set) empty();
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public <U> void foreach(Function1<A, U> function1) {
        for (Object obj : table()) {
            if (obj != null) {
                function1.apply(obj);
            } else {
                BoxedUnit boxedUnit = BoxedUnit.UNIT;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.FlatHashTable$HashUtils, scala.collection.mutable.HashSet] */
    public final int improve(int i, int i2) {
        return FlatHashTable.HashUtils.Cclass.improve(this, i, i2);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public final int index(int i) {
        return FlatHashTable.Cclass.index(this, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public void initWithContents(FlatHashTable.Contents<A> contents) {
        FlatHashTable.Cclass.initWithContents(this, contents);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public int initialSize() {
        return FlatHashTable.Cclass.initialSize(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public Iterator<A> iterator() {
        return FlatHashTable.Cclass.iterator(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public void nnSizeMapAdd(int i) {
        FlatHashTable.Cclass.nnSizeMapAdd(this, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public void nnSizeMapRemove(int i) {
        FlatHashTable.Cclass.nnSizeMapRemove(this, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public void nnSizeMapReset(int i) {
        FlatHashTable.Cclass.nnSizeMapReset(this, i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public Option<A> removeEntry(A a) {
        return FlatHashTable.Cclass.removeEntry(this, a);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.SetLike, scala.collection.mutable.HashSet] */
    public /* bridge */ /* synthetic */ Object result() {
        return result();
    }

    public int seedvalue() {
        return this.seedvalue;
    }

    public void seedvalue_$eq(int i) {
        this.seedvalue = i;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public int size() {
        return tableSize();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.FlatHashTable$HashUtils, scala.collection.mutable.HashSet] */
    public final int sizeMapBucketBitSize() {
        return FlatHashTable.HashUtils.Cclass.sizeMapBucketBitSize(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.FlatHashTable$HashUtils, scala.collection.mutable.HashSet] */
    public final int sizeMapBucketSize() {
        return FlatHashTable.HashUtils.Cclass.sizeMapBucketSize(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public void sizeMapInit(int i) {
        FlatHashTable.Cclass.sizeMapInit(this, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public void sizeMapInitAndRebuild() {
        FlatHashTable.Cclass.sizeMapInitAndRebuild(this);
    }

    public int[] sizemap() {
        return this.sizemap;
    }

    public void sizemap_$eq(int[] iArr) {
        this.sizemap = iArr;
    }

    public Object[] table() {
        return this.table;
    }

    public int tableSize() {
        return this.tableSize;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public int tableSizeSeed() {
        return FlatHashTable.Cclass.tableSizeSeed(this);
    }

    public void tableSize_$eq(int i) {
        this.tableSize = i;
    }

    public void table_$eq(Object[] objArr) {
        this.table = objArr;
    }

    public /* bridge */ /* synthetic */ Traversable thisCollection() {
        return thisCollection();
    }

    public int threshold() {
        return this.threshold;
    }

    public void threshold_$eq(int i) {
        this.threshold = i;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.HashSet, scala.collection.mutable.FlatHashTable] */
    public final int totalSizeMapBuckets() {
        return FlatHashTable.Cclass.totalSizeMapBuckets(this);
    }
}
