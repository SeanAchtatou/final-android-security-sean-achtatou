package scala.collection.mutable;

import scala.Function1;
import scala.None$;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.CustomParallelizable;
import scala.collection.GenMap;
import scala.collection.Iterator;
import scala.collection.Map;
import scala.collection.Traversable;
import scala.collection.mutable.HashTable;
import scala.collection.parallel.mutable.ParHashMap;

public class HashMap<A, B> extends AbstractMap<A, B> implements Serializable, CustomParallelizable<Tuple2<A, B>, ParHashMap<A, B>>, HashTable<A, DefaultEntry<A, B>> {
    private transient int _loadFactor;
    private transient int seedvalue;
    private transient int[] sizemap;
    private transient HashEntry<Object, HashEntry>[] table;
    private transient int tableSize;
    private transient int threshold;

    public HashMap() {
        this(null);
    }

    public HashMap(HashTable.Contents<A, DefaultEntry<A, B>> contents) {
        HashTable.HashUtils.Cclass.$init$(this);
        HashTable.Cclass.$init$(this);
        CustomParallelizable.Cclass.$init$(this);
        initWithContents(contents);
    }

    public /* bridge */ /* synthetic */ Map $minus(Object obj) {
        return $minus(obj);
    }

    public HashMap<A, B> $minus$eq(A a) {
        removeEntry(a);
        return this;
    }

    public /* bridge */ /* synthetic */ GenMap $plus(Tuple2 tuple2) {
        return $plus(tuple2);
    }

    public HashMap<A, B> $plus$eq(Tuple2<A, B> tuple2) {
        DefaultEntry defaultEntry = (DefaultEntry) findOrAddEntry(tuple2._1(), tuple2._2());
        if (defaultEntry != null) {
            defaultEntry.value_$eq(tuple2._2());
        }
        return this;
    }

    public int _loadFactor() {
        return this._loadFactor;
    }

    public void _loadFactor_$eq(int i) {
        this._loadFactor = i;
    }

    public boolean alwaysInitSizeMap() {
        return HashTable.Cclass.alwaysInitSizeMap(this);
    }

    public B apply(A a) {
        DefaultEntry defaultEntry = (DefaultEntry) findEntry(a);
        return defaultEntry == null ? m2default(a) : defaultEntry.value();
    }

    public int calcSizeMapSize(int i) {
        return HashTable.Cclass.calcSizeMapSize(this, i);
    }

    public /* bridge */ /* synthetic */ Object clone() {
        return clone();
    }

    public boolean contains(A a) {
        return findEntry(a) != null;
    }

    public <B1> DefaultEntry<A, B> createNewEntry(A a, B1 b1) {
        return new DefaultEntry<>(a, b1);
    }

    public boolean elemEquals(A a, A a2) {
        return HashTable.Cclass.elemEquals(this, a, a2);
    }

    public int elemHashCode(A a) {
        return HashTable.HashUtils.Cclass.elemHashCode(this, a);
    }

    public HashMap<A, B> empty() {
        return HashMap$.MODULE$.empty();
    }

    public Iterator<DefaultEntry<A, B>> entriesIterator() {
        return HashTable.Cclass.entriesIterator(this);
    }

    public /* bridge */ /* synthetic */ Object filterNot(Function1 function1) {
        return filterNot(function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.DefaultEntry<A, B>, scala.collection.mutable.HashEntry] */
    public DefaultEntry<A, B> findEntry(A a) {
        return HashTable.Cclass.findEntry(this, a);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.DefaultEntry<A, B>, scala.collection.mutable.HashEntry] */
    public <B> DefaultEntry<A, B> findOrAddEntry(A a, B b) {
        return HashTable.Cclass.findOrAddEntry(this, a, b);
    }

    public <C> void foreach(Function1<Tuple2<A, B>, C> function1) {
        foreachEntry(new HashMap$$anonfun$foreach$1(this, function1));
    }

    public <U> void foreachEntry(Function1<DefaultEntry<A, B>, U> function1) {
        HashTable.Cclass.foreachEntry(this, function1);
    }

    public Option<B> get(A a) {
        DefaultEntry defaultEntry = (DefaultEntry) findEntry(a);
        return defaultEntry == null ? None$.MODULE$ : new Some(defaultEntry.value());
    }

    public final int improve(int i, int i2) {
        return HashTable.HashUtils.Cclass.improve(this, i, i2);
    }

    public final int index(int i) {
        return HashTable.Cclass.index(this, i);
    }

    public void initWithContents(HashTable.Contents<A, DefaultEntry<A, B>> contents) {
        HashTable.Cclass.initWithContents(this, contents);
    }

    public int initialSize() {
        return HashTable.Cclass.initialSize(this);
    }

    public Iterator<Tuple2<A, B>> iterator() {
        return entriesIterator().map(new HashMap$$anonfun$iterator$1(this));
    }

    public void nnSizeMapAdd(int i) {
        HashTable.Cclass.nnSizeMapAdd(this, i);
    }

    public void nnSizeMapRemove(int i) {
        HashTable.Cclass.nnSizeMapRemove(this, i);
    }

    public void nnSizeMapReset(int i) {
        HashTable.Cclass.nnSizeMapReset(this, i);
    }

    public Option<B> put(A a, B b) {
        DefaultEntry defaultEntry = (DefaultEntry) findOrAddEntry(a, b);
        if (defaultEntry == null) {
            return None$.MODULE$;
        }
        Object value = defaultEntry.value();
        defaultEntry.value_$eq(b);
        return new Some(value);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.DefaultEntry<A, B>, scala.collection.mutable.HashEntry] */
    public DefaultEntry<A, B> removeEntry(A a) {
        return HashTable.Cclass.removeEntry(this, a);
    }

    public /* bridge */ /* synthetic */ Object result() {
        return result();
    }

    public int seedvalue() {
        return this.seedvalue;
    }

    public void seedvalue_$eq(int i) {
        this.seedvalue = i;
    }

    public int size() {
        return tableSize();
    }

    public final int sizeMapBucketBitSize() {
        return HashTable.HashUtils.Cclass.sizeMapBucketBitSize(this);
    }

    public final int sizeMapBucketSize() {
        return HashTable.HashUtils.Cclass.sizeMapBucketSize(this);
    }

    public void sizeMapInit(int i) {
        HashTable.Cclass.sizeMapInit(this, i);
    }

    public void sizeMapInitAndRebuild() {
        HashTable.Cclass.sizeMapInitAndRebuild(this);
    }

    public int[] sizemap() {
        return this.sizemap;
    }

    public void sizemap_$eq(int[] iArr) {
        this.sizemap = iArr;
    }

    public HashEntry<A, DefaultEntry<A, B>>[] table() {
        return this.table;
    }

    public int tableSize() {
        return this.tableSize;
    }

    public int tableSizeSeed() {
        return HashTable.Cclass.tableSizeSeed(this);
    }

    public void tableSize_$eq(int i) {
        this.tableSize = i;
    }

    public void table_$eq(HashEntry<A, DefaultEntry<A, B>>[] hashEntryArr) {
        this.table = hashEntryArr;
    }

    public /* bridge */ /* synthetic */ Traversable thisCollection() {
        return thisCollection();
    }

    public int threshold() {
        return this.threshold;
    }

    public void threshold_$eq(int i) {
        this.threshold = i;
    }

    public final int totalSizeMapBuckets() {
        return HashTable.Cclass.totalSizeMapBuckets(this);
    }

    public void update(A a, B b) {
        put(a, b);
    }
}
