package scala.collection.mutable;

import scala.Cloneable;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;

public interface Buffer<A> extends Cloneable, GenericTraversableTemplate<A, Buffer> {

    /* renamed from: scala.collection.mutable.Buffer$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Buffer buffer) {
        }

        public static GenericCompanion companion(Buffer buffer) {
            return Buffer$.MODULE$;
        }
    }
}
