package scala.collection.mutable;

import scala.Predef$;
import scala.Serializable;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class FlatHashTable$$anonfun$checkConsistent$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public final /* synthetic */ FlatHashTable $outer;

    public FlatHashTable$$anonfun$checkConsistent$1(FlatHashTable<A> flatHashTable) {
        if (flatHashTable == null) {
            throw new NullPointerException();
        }
        this.$outer = flatHashTable;
    }

    public final /* synthetic */ Object apply(Object obj) {
        apply$mcVI$sp(BoxesRunTime.unboxToInt(obj));
        return BoxedUnit.UNIT;
    }

    public final void apply(int i) {
        apply$mcVI$sp(i);
    }

    public final void apply$mcVI$sp(int i) {
        if (this.$outer.table()[i] != null && !this.$outer.containsEntry(this.$outer.table()[i])) {
            Predef$ predef$ = Predef$.MODULE$;
            throw new AssertionError(new StringBuilder().append((Object) "assertion failed: ").append((Object) new FlatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1(this, i).apply()).toString());
        }
    }
}
