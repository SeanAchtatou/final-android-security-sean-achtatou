package scala.collection.mutable;

import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;

public interface Seq<A> extends scala.collection.Seq<A>, GenericTraversableTemplate<A, Seq>, Iterable<A> {

    /* renamed from: scala.collection.mutable.Seq$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Seq seq) {
        }

        public static GenericCompanion companion(Seq seq) {
            return Seq$.MODULE$;
        }

        public static Seq seq(Seq seq) {
            return seq;
        }
    }

    Seq<A> seq();
}
