package scala.collection.mutable;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Function1;
import scala.Function2;
import scala.Serializable;
import scala.collection.GenIterable;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.Growable;
import scala.collection.immutable.StringLike;
import scala.collection.mutable.Builder;
import scala.collection.mutable.IndexedSeq;
import scala.collection.mutable.IndexedSeqLike;
import scala.math.Ordered;
import scala.reflect.ClassTag;
import scala.runtime.BoxesRunTime;

public final class StringBuilder extends AbstractSeq<Object> implements CharSequence, Serializable, StringLike<StringBuilder>, Builder<Object, String>, IndexedSeq<Object> {
    private final StringBuilder underlying;

    public StringBuilder() {
        this(16, PoiTypeDef.All);
    }

    public StringBuilder(int i, String str) {
        this(new StringBuilder(str.length() + i).append(str));
    }

    public StringBuilder(StringBuilder sb) {
        this.underlying = sb;
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        Ordered.Cclass.$init$(this);
        StringLike.Cclass.$init$(this);
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
    }

    private StringBuilder underlying() {
        return this.underlying;
    }

    public final StringBuilder $plus$eq(char c) {
        append(c);
        return this;
    }

    public final Growable<Object> $plus$plus$eq(TraversableOnce<Object> traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public final StringBuilder append(char c) {
        underlying().append(c);
        return this;
    }

    public final StringBuilder append(int i) {
        underlying().append(i);
        return this;
    }

    public final StringBuilder append(Object obj) {
        underlying().append(String.valueOf(obj));
        return this;
    }

    public final StringBuilder append(String str) {
        underlying().append(str);
        return this;
    }

    public final char apply(int i) {
        return underlying().charAt(i);
    }

    public final /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToCharacter(apply(BoxesRunTime.unboxToInt(obj)));
    }

    public final char charAt(int i) {
        return underlying().charAt(i);
    }

    public final StringBuilder clone() {
        return new StringBuilder(new StringBuilder(underlying()));
    }

    public final GenericCompanion<IndexedSeq> companion() {
        return IndexedSeq.Cclass.companion(this);
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj) {
        return compare((String) obj);
    }

    public final int compare(String str) {
        return StringLike.Cclass.compare(this, str);
    }

    public final int compareTo(Object obj) {
        return Ordered.Cclass.compareTo(this, obj);
    }

    public final <B> void copyToArray(Object obj, int i, int i2) {
        IndexedSeqOptimized.Cclass.copyToArray(this, obj, i, i2);
    }

    public final Object drop(int i) {
        return IndexedSeqOptimized.Cclass.drop(this, i);
    }

    public final boolean exists(Function1<Object, Object> function1) {
        return IndexedSeqOptimized.Cclass.exists(this, function1);
    }

    public final <B> B foldLeft(B b, Function2<B, Object, B> function2) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, b, function2);
    }

    public final boolean forall(Function1<Object, Object> function1) {
        return IndexedSeqOptimized.Cclass.forall(this, function1);
    }

    public final <U> void foreach(Function1<Object, U> function1) {
        IndexedSeqOptimized.Cclass.foreach(this, function1);
    }

    public final String format(Seq<Object> seq) {
        return StringLike.Cclass.format(this, seq);
    }

    public final int hashCode() {
        return IndexedSeqLike.Cclass.hashCode(this);
    }

    public final Object head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public final /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public final boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final Iterator<Object> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public final Object last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public final int length() {
        return underlying().length();
    }

    public final int lengthCompare(int i) {
        return IndexedSeqOptimized.Cclass.lengthCompare(this, i);
    }

    public final String mkString() {
        return toString();
    }

    public final GrowingBuilder<Object, StringBuilder> newBuilder() {
        return new GrowingBuilder<>(new StringBuilder());
    }

    public final String result() {
        return toString();
    }

    public final StringBuilder reverse() {
        return new StringBuilder(new StringBuilder(underlying()).reverse());
    }

    public final Iterator<Object> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public final <B> boolean sameElements(GenIterable<B> genIterable) {
        return IndexedSeqOptimized.Cclass.sameElements(this, genIterable);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(GenIterable genIterable) {
        return IterableLike.Cclass.sameElements(this, genIterable);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final int segmentLength(Function1<Object, Object> function1, int i) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, function1, i);
    }

    public final IndexedSeq<Object> seq() {
        return IndexedSeq.Cclass.seq(this);
    }

    public final void sizeHint(int i) {
        Builder.Cclass.sizeHint(this, i);
    }

    public final void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public final void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public final void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }

    public final Object slice(int i, int i2) {
        return StringLike.Cclass.slice(this, i, i2);
    }

    public final String[] split(char c) {
        return StringLike.Cclass.split(this, c);
    }

    public final String stripSuffix(String str) {
        return StringLike.Cclass.stripSuffix(this, str);
    }

    public final CharSequence subSequence(int i, int i2) {
        return substring(i, i2);
    }

    public final String substring(int i, int i2) {
        return underlying().substring(i, i2);
    }

    public final Object tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public final Object take(int i) {
        return IndexedSeqOptimized.Cclass.take(this, i);
    }

    public final StringBuilder thisCollection() {
        return this;
    }

    public final <B> Object toArray(ClassTag<B> classTag) {
        return StringLike.Cclass.toArray(this, classTag);
    }

    public final <A1> Buffer<A1> toBuffer() {
        return IndexedSeqLike.Cclass.toBuffer(this);
    }

    public final IndexedSeq<Object> toCollection(IndexedSeq<Object> indexedSeq) {
        return IndexedSeqLike.Cclass.toCollection(this, indexedSeq);
    }

    public final StringBuilder toCollection(StringBuilder stringBuilder) {
        return stringBuilder;
    }

    public final String toString() {
        return underlying().toString();
    }
}
