package scala.collection.mutable;

import scala.Function1;
import scala.Proxy;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.mutable.Builder;

public final class Builder$$anon$1 implements Proxy, Builder<Elem, NewTo> {
    private final Function1 f$1;
    private final Builder<Elem, To> self;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.Function1, scala.collection.mutable.Builder<Elem, To>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Builder$$anon$1(scala.collection.mutable.Builder r1, scala.collection.mutable.Builder<Elem, To> r2) {
        /*
            r0 = this;
            r0.f$1 = r2
            r0.<init>()
            scala.collection.generic.Growable.Cclass.$init$(r0)
            scala.collection.mutable.Builder.Cclass.$init$(r0)
            scala.Proxy.Cclass.$init$(r0)
            r0.self = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.Builder$$anon$1.<init>(scala.collection.mutable.Builder, scala.Function1):void");
    }

    public final Builder<Elem, To>.1 $plus$eq(Elem elem) {
        self().$plus$eq((Object) elem);
        return this;
    }

    public final Builder<Elem, To>.1 $plus$plus$eq(TraversableOnce<Elem> traversableOnce) {
        self().$plus$plus$eq(traversableOnce);
        return this;
    }

    public final boolean equals(Object obj) {
        return Proxy.Cclass.equals(this, obj);
    }

    public final int hashCode() {
        return Proxy.Cclass.hashCode(this);
    }

    public final NewTo result() {
        return this.f$1.apply(self().result());
    }

    public final Builder<Elem, To> self() {
        return this.self;
    }

    public final void sizeHint(int i) {
        self().sizeHint(i);
    }

    public final void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public final void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public final void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        self().sizeHintBounded(i, traversableLike);
    }

    public final String toString() {
        return Proxy.Cclass.toString(this);
    }
}
