package scala.collection.mutable;

import scala.collection.generic.GenTraversableFactory;

public final class Iterable$ extends GenTraversableFactory<Iterable> {
    public static final Iterable$ MODULE$ = null;

    static {
        new Iterable$();
    }

    private Iterable$() {
        MODULE$ = this;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder<A, scala.collection.mutable.Iterable<A>>, scala.collection.mutable.ArrayBuffer] */
    public final <A> Builder<A, Iterable<A>> newBuilder() {
        return new ArrayBuffer();
    }
}
