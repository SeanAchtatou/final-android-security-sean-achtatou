package scala.collection.mutable;

import scala.collection.generic.SeqFactory;

public final class Buffer$ extends SeqFactory<Buffer> {
    public static final Buffer$ MODULE$ = null;

    static {
        new Buffer$();
    }

    private Buffer$() {
        MODULE$ = this;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder<A, scala.collection.mutable.Buffer<A>>, scala.collection.mutable.ArrayBuffer] */
    public final <A> Builder<A, Buffer<A>> newBuilder() {
        return new ArrayBuffer();
    }
}
