package scala.collection.mutable;

import scala.Cloneable;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.generic.Subtractable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.BufferLike;

public interface BufferLike<A, This extends BufferLike<A, This> & Buffer<A>> extends Cloneable, Growable<A>, Shrinkable<A>, Subtractable<A, This> {

    /* renamed from: scala.collection.mutable.BufferLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Buffer buffer) {
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.Buffer, scala.collection.TraversableOnce] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static scala.collection.mutable.Buffer clone(scala.collection.mutable.Buffer r1) {
            /*
                scala.collection.mutable.Builder r0 = r1.newBuilder()
                r0.$plus$plus$eq(r1)
                java.lang.Object r0 = r0.result()
                scala.collection.mutable.Buffer r0 = (scala.collection.mutable.Buffer) r0
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.BufferLike.Cclass.clone(scala.collection.mutable.Buffer):scala.collection.mutable.Buffer");
        }

        public static String stringPrefix(Buffer buffer) {
            return "Buffer";
        }
    }

    This clone();
}
