package scala.collection.mutable;

import java.util.NoSuchElementException;
import scala.collection.AbstractIterator;
import scala.collection.immutable.List;

public final class ListBuffer$$anon$1 extends AbstractIterator<A> {
    private final /* synthetic */ ListBuffer $outer;
    private List<A> cursor;
    private int delivered;

    public ListBuffer$$anon$1(ListBuffer<A> listBuffer) {
        if (listBuffer == null) {
            throw new NullPointerException();
        }
        this.$outer = listBuffer;
        this.cursor = null;
        this.delivered = 0;
    }

    private List<A> cursor() {
        return this.cursor;
    }

    private void cursor_$eq(List<A> list) {
        this.cursor = list;
    }

    private int delivered() {
        return this.delivered;
    }

    private void delivered_$eq(int i) {
        this.delivered = i;
    }

    public final boolean hasNext() {
        return delivered() < this.$outer.length();
    }

    public final A next() {
        if (hasNext()) {
            if (cursor() == null) {
                this.cursor = this.$outer.scala$collection$mutable$ListBuffer$$start();
            } else {
                this.cursor = (List) cursor().tail();
            }
            this.delivered = delivered() + 1;
            return cursor().head();
        }
        throw new NoSuchElementException("next on empty Iterator");
    }
}
