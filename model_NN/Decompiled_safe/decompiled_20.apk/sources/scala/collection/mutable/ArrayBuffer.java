package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.Serializable;
import scala.collection.CustomParallelizable;
import scala.collection.GenIterable;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.TraversableLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.mutable.Builder;
import scala.collection.mutable.IndexedSeq;
import scala.collection.mutable.IndexedSeqLike;
import scala.collection.mutable.ResizableArray;
import scala.collection.parallel.mutable.ParArray;
import scala.runtime.BoxesRunTime;

public class ArrayBuffer<A> extends AbstractBuffer<A> implements Serializable, CustomParallelizable<A, ParArray<A>>, GenericTraversableTemplate<A, ArrayBuffer>, Buffer<A> {
    private Object[] array;
    private final int initialSize;
    private int size0;

    public ArrayBuffer() {
        this(16);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeqLike, scala.collection.IndexedSeq, scala.collection.IndexedSeqOptimized, scala.collection.mutable.IndexedSeq, scala.collection.IndexedSeqLike, scala.collection.CustomParallelizable, scala.collection.mutable.Builder, scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer] */
    public ArrayBuffer(int i) {
        this.initialSize = i;
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        ResizableArray.Cclass.$init$(this);
        CustomParallelizable.Cclass.$init$(this);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer, scala.collection.mutable.ArrayBuffer<A>] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public scala.collection.mutable.ArrayBuffer<A> $plus$eq(A r3) {
        /*
            r2 = this;
            int r0 = r2.size0()
            int r0 = r0 + 1
            r2.ensureSize(r0)
            java.lang.Object[] r0 = r2.array()
            int r1 = r2.size0()
            r0[r1] = r3
            int r0 = r2.size0()
            int r0 = r0 + 1
            r2.size0_$eq(r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.ArrayBuffer.$plus$eq(java.lang.Object):scala.collection.mutable.ArrayBuffer");
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.collection.generic.Growable, scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer, scala.collection.mutable.ArrayBuffer<A>] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public scala.collection.mutable.ArrayBuffer<A> $plus$plus$eq(scala.collection.TraversableOnce<A> r4) {
        /*
            r3 = this;
            boolean r0 = r4 instanceof scala.collection.IndexedSeqLike
            if (r0 == 0) goto L_0x0026
            scala.collection.IndexedSeqLike r4 = (scala.collection.IndexedSeqLike) r4
            int r0 = r4.length()
            int r1 = r3.size0()
            int r1 = r1 + r0
            r3.ensureSize(r1)
            java.lang.Object[] r1 = r3.array()
            int r2 = r3.size0()
            r4.copyToArray(r1, r2, r0)
            int r1 = r3.size0()
            int r0 = r0 + r1
            r3.size0_$eq(r0)
        L_0x0025:
            return r3
        L_0x0026:
            scala.collection.generic.Growable r0 = scala.collection.generic.Growable.Cclass.$plus$plus$eq(r3, r4)
            scala.collection.mutable.ArrayBuffer r0 = (scala.collection.mutable.ArrayBuffer) r0
            r3 = r0
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.ArrayBuffer.$plus$plus$eq(scala.collection.TraversableOnce):scala.collection.mutable.ArrayBuffer");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer] */
    public A apply(int i) {
        return ResizableArray.Cclass.apply(this, i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public /* synthetic */ java.lang.Object apply(java.lang.Object r2) {
        /*
            r1 = this;
            int r0 = scala.runtime.BoxesRunTime.unboxToInt(r2)
            java.lang.Object r0 = r1.apply(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.ArrayBuffer.apply(java.lang.Object):java.lang.Object");
    }

    public Object[] array() {
        return this.array;
    }

    public void array_$eq(Object[] objArr) {
        this.array = objArr;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.BufferLike, scala.collection.mutable.ArrayBuffer] */
    public /* bridge */ /* synthetic */ Object clone() {
        return clone();
    }

    public GenericCompanion<ArrayBuffer> companion() {
        return ArrayBuffer$.MODULE$;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer] */
    public <B> void copyToArray(Object obj, int i, int i2) {
        ResizableArray.Cclass.copyToArray(this, obj, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> drop(int i) {
        return IndexedSeqOptimized.Cclass.drop(this, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer] */
    public void ensureSize(int i) {
        ResizableArray.Cclass.ensureSize(this, i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public boolean exists(Function1<A, Object> function1) {
        return IndexedSeqOptimized.Cclass.exists(this, function1);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public <B> B foldLeft(B b, Function2<B, A, B> function2) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, b, function2);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public boolean forall(Function1<A, Object> function1) {
        return IndexedSeqOptimized.Cclass.forall(this, function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer] */
    public <U> void foreach(Function1<A, U> function1) {
        ResizableArray.Cclass.foreach(this, function1);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqLike, scala.collection.mutable.ArrayBuffer] */
    public int hashCode() {
        return IndexedSeqLike.Cclass.hashCode(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public A head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public int initialSize() {
        return this.initialSize;
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqLike, scala.collection.mutable.ArrayBuffer] */
    public Iterator<A> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public A last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer] */
    public int length() {
        return ResizableArray.Cclass.length(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public int lengthCompare(int i) {
        return IndexedSeqOptimized.Cclass.lengthCompare(this, i);
    }

    public ArrayBuffer<A> result() {
        return this;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public Iterator<A> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public <B> boolean sameElements(GenIterable<B> genIterable) {
        return IndexedSeqOptimized.Cclass.sameElements(this, genIterable);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public boolean scala$collection$IndexedSeqOptimized$$super$sameElements(GenIterable genIterable) {
        return IterableLike.Cclass.sameElements(this, genIterable);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqOptimized, scala.collection.mutable.ArrayBuffer] */
    public int segmentLength(Function1<A, Object> function1, int i) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, function1, i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.IndexedSeq, scala.collection.mutable.ArrayBuffer] */
    public IndexedSeq<A> seq() {
        return IndexedSeq.Cclass.seq(this);
    }

    public int size0() {
        return this.size0;
    }

    public void size0_$eq(int i) {
        this.size0 = i;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [scala.collection.SeqLike, scala.collection.mutable.ResizableArray, scala.collection.mutable.ArrayBuffer] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void sizeHint(int r5) {
        /*
            r4 = this;
            r3 = 0
            int r0 = r4.size()
            if (r5 <= r0) goto L_0x001b
            if (r5 <= 0) goto L_0x001b
            java.lang.Object[] r0 = new java.lang.Object[r5]
            scala.compat.Platform$ r1 = scala.compat.Platform$.MODULE$
            java.lang.Object[] r1 = r4.array()
            int r2 = r4.size0()
            java.lang.System.arraycopy(r1, r3, r0, r3, r2)
            r4.array_$eq(r0)
        L_0x001b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.ArrayBuffer.sizeHint(int):void");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder, scala.collection.mutable.ArrayBuffer] */
    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint((Builder) this, traversableLike);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder, scala.collection.mutable.ArrayBuffer] */
    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder, scala.collection.mutable.ArrayBuffer] */
    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> slice(int i, int i2) {
        return IndexedSeqOptimized.Cclass.slice(this, i, i2);
    }

    public String stringPrefix() {
        return "ArrayBuffer";
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> take(int i) {
        return IndexedSeqOptimized.Cclass.take(this, i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.IndexedSeqLike, scala.collection.mutable.ArrayBuffer] */
    public IndexedSeq<A> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.IndexedSeqLike, scala.collection.mutable.ArrayBuffer] */
    public <A1> Buffer<A1> toBuffer() {
        return IndexedSeqLike.Cclass.toBuffer(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.IndexedSeqLike, scala.collection.mutable.ArrayBuffer] */
    public IndexedSeq<A> toCollection(ArrayBuffer<A> arrayBuffer) {
        return IndexedSeqLike.Cclass.toCollection(this, arrayBuffer);
    }
}
