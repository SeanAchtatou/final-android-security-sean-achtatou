package scala.collection.mutable;

import scala.Option;
import scala.Tuple2;
import scala.collection.Parallelizable;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.mutable.Map;
import scala.collection.mutable.MapLike;
import scala.collection.parallel.mutable.ParMap;

public interface MapLike<A, B, This extends MapLike<A, B, This> & Map<A, B>> extends scala.collection.MapLike<A, B, This>, Parallelizable<Tuple2<A, B>, ParMap<A, B>>, Growable<Tuple2<A, B>>, Shrinkable<A> {

    /* renamed from: scala.collection.mutable.MapLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(MapLike mapLike) {
        }

        public static Map $minus(MapLike mapLike, Object obj) {
            return (Map) mapLike.clone().$minus$eq(obj);
        }

        public static Map $plus(MapLike mapLike, Tuple2 tuple2) {
            return (Map) mapLike.clone().$plus$eq((Tuple2<Object, String>) tuple2);
        }

        public static Map clone(MapLike mapLike) {
            return (Map) ((Growable) mapLike.empty()).$plus$plus$eq((TraversableOnce) mapLike.repr());
        }

        public static Builder newBuilder(MapLike mapLike) {
            return (Builder) mapLike.empty();
        }

        public static Option put(MapLike mapLike, Object obj, Object obj2) {
            Option option = mapLike.get(obj);
            mapLike.update(obj, obj2);
            return option;
        }

        public static Map result(MapLike mapLike) {
            return (Map) mapLike.repr();
        }

        public static void update(MapLike mapLike, Object obj, Object obj2) {
            mapLike.$plus$eq((Tuple2<Object, String>) new Tuple2(obj, obj2));
        }
    }

    This $minus(A a);

    MapLike<A, B, This> $minus$eq(A a);

    <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2);

    MapLike<A, B, This> $plus$eq(Tuple2<Object, String> tuple2);

    This clone();

    This result();

    void update(A a, B b);
}
