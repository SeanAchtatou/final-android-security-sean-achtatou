package scala.collection.mutable;

import scala.MatchError;
import scala.collection.mutable.WrappedArray;
import scala.runtime.BoxedUnit;

public final class WrappedArray$ {
    public static final WrappedArray$ MODULE$ = null;
    private final WrappedArray.ofRef<Object> EmptyWrappedArray = new WrappedArray.ofRef<>(new Object[0]);

    static {
        new WrappedArray$();
    }

    private WrappedArray$() {
        MODULE$ = this;
    }

    private WrappedArray.ofRef<Object> EmptyWrappedArray() {
        return this.EmptyWrappedArray;
    }

    public final <T> WrappedArray<T> empty() {
        return EmptyWrappedArray();
    }

    public final <T> WrappedArray<T> make(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Object[]) {
            return new WrappedArray.ofRef((Object[]) obj);
        }
        if (obj instanceof int[]) {
            return new WrappedArray.ofInt((int[]) obj);
        }
        if (obj instanceof double[]) {
            return new WrappedArray.ofDouble((double[]) obj);
        }
        if (obj instanceof long[]) {
            return new WrappedArray.ofLong((long[]) obj);
        }
        if (obj instanceof float[]) {
            return new WrappedArray.ofFloat((float[]) obj);
        }
        if (obj instanceof char[]) {
            return new WrappedArray.ofChar((char[]) obj);
        }
        if (obj instanceof byte[]) {
            return new WrappedArray.ofByte((byte[]) obj);
        }
        if (obj instanceof short[]) {
            return new WrappedArray.ofShort((short[]) obj);
        }
        if (obj instanceof boolean[]) {
            return new WrappedArray.ofBoolean((boolean[]) obj);
        }
        if (obj instanceof BoxedUnit[]) {
            return new WrappedArray.ofUnit((BoxedUnit[]) obj);
        }
        throw new MatchError(obj);
    }
}
