package scala.collection.mutable;

import java.io.Serializable;
import scala.Function1;
import scala.Function2;
import scala.Predef$$less$colon$less;
import scala.Tuple2;
import scala.collection.GenIterable;
import scala.collection.GenSeq;
import scala.collection.GenSeqLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.IterableForwarder;
import scala.collection.generic.SeqForwarder;
import scala.collection.generic.TraversableForwarder;
import scala.collection.immutable.C$colon$colon;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;
import scala.reflect.ClassTag;
import scala.runtime.BoxesRunTime;

public final class ListBuffer<A> extends AbstractBuffer<A> implements Serializable, GenericTraversableTemplate<A, ListBuffer>, SeqForwarder<A> {
    private boolean exported = false;
    private C$colon$colon<A> last0;
    private int len = 0;
    private List<A> scala$collection$mutable$ListBuffer$$start = Nil$.MODULE$;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.mutable.ListBuffer, scala.collection.generic.SeqForwarder, scala.collection.generic.IterableForwarder, scala.collection.mutable.Builder, scala.collection.generic.TraversableForwarder] */
    public ListBuffer() {
        Builder.Cclass.$init$(this);
        TraversableForwarder.Cclass.$init$(this);
        IterableForwarder.Cclass.$init$(this);
        SeqForwarder.Cclass.$init$(this);
    }

    private void copy() {
        List tail = last0().tail();
        clear();
        for (List scala$collection$mutable$ListBuffer$$start2 = scala$collection$mutable$ListBuffer$$start(); scala$collection$mutable$ListBuffer$$start2 != tail; scala$collection$mutable$ListBuffer$$start2 = (List) scala$collection$mutable$ListBuffer$$start2.tail()) {
            $plus$eq(scala$collection$mutable$ListBuffer$$start2.head());
        }
    }

    private boolean exported() {
        return this.exported;
    }

    private void exported_$eq(boolean z) {
        this.exported = z;
    }

    private C$colon$colon<A> last0() {
        return this.last0;
    }

    private void last0_$eq(C$colon$colon<A> _colon_colon) {
        this.last0 = _colon_colon;
    }

    private int len() {
        return this.len;
    }

    private void len_$eq(int i) {
        this.len = i;
    }

    private void scala$collection$mutable$ListBuffer$$start_$eq(List<A> list) {
        this.scala$collection$mutable$ListBuffer$$start = list;
    }

    public final <B> B $div$colon(B b, Function2<B, A, B> function2) {
        return TraversableForwarder.Cclass.$div$colon(this, b, function2);
    }

    public final ListBuffer<A> $plus$eq(Object obj) {
        if (exported()) {
            copy();
        }
        if (scala$collection$mutable$ListBuffer$$start().isEmpty()) {
            this.last0 = new C$colon$colon<>(obj, Nil$.MODULE$);
            this.scala$collection$mutable$ListBuffer$$start = last0();
        } else {
            C$colon$colon last02 = last0();
            this.last0 = new C$colon$colon<>(obj, Nil$.MODULE$);
            last02.tl_$eq(last0());
        }
        this.len = len() + 1;
        return this;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.generic.Growable, scala.collection.mutable.ListBuffer, scala.collection.IterableLike] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final scala.collection.mutable.ListBuffer<A> $plus$plus$eq(scala.collection.TraversableOnce r2) {
        /*
            r1 = this;
        L_0x0000:
            if (r2 != r1) goto L_0x000e
            int r0 = r1.size()
            java.lang.Object r0 = r1.take(r0)
            scala.collection.TraversableOnce r0 = (scala.collection.TraversableOnce) r0
            r2 = r0
            goto L_0x0000
        L_0x000e:
            scala.collection.generic.Growable r0 = scala.collection.generic.Growable.Cclass.$plus$plus$eq(r1, r2)
            scala.collection.mutable.ListBuffer r0 = (scala.collection.mutable.ListBuffer) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.ListBuffer.$plus$plus$eq(scala.collection.TraversableOnce):scala.collection.mutable.ListBuffer");
    }

    public final StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
        return TraversableForwarder.Cclass.addString(this, stringBuilder, str, str2, str3);
    }

    public final A apply(int i) {
        if (i >= 0 && i < len()) {
            return SeqForwarder.Cclass.apply(this, i);
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public final /* synthetic */ Object apply(Object obj) {
        return apply(BoxesRunTime.unboxToInt(obj));
    }

    public final void clear() {
        this.scala$collection$mutable$ListBuffer$$start = Nil$.MODULE$;
        this.exported = false;
        this.len = 0;
    }

    public final ListBuffer<A> clone() {
        return new ListBuffer().$plus$plus$eq((TraversableOnce) this);
    }

    public final GenericCompanion<ListBuffer> companion() {
        return ListBuffer$.MODULE$;
    }

    public final <B> void copyToArray(Object obj, int i) {
        TraversableForwarder.Cclass.copyToArray(this, obj, i);
    }

    public final <B> void copyToArray(Object obj, int i, int i2) {
        TraversableForwarder.Cclass.copyToArray(this, obj, i, i2);
    }

    public final <B> void copyToBuffer(Buffer<B> buffer) {
        TraversableForwarder.Cclass.copyToBuffer(this, buffer);
    }

    public final <B> boolean corresponds(GenSeq<B> genSeq, Function2<A, B, Object> function2) {
        return SeqForwarder.Cclass.corresponds(this, genSeq, function2);
    }

    public final boolean equals(Object obj) {
        return obj instanceof ListBuffer ? readOnly().equals(((ListBuffer) obj).readOnly()) : GenSeqLike.Cclass.equals(this, obj);
    }

    public final boolean exists(Function1<A, Object> function1) {
        return TraversableForwarder.Cclass.exists(this, function1);
    }

    public final <B> B foldLeft(B b, Function2<B, A, B> function2) {
        return TraversableForwarder.Cclass.foldLeft(this, b, function2);
    }

    public final boolean forall(Function1<A, Object> function1) {
        return TraversableForwarder.Cclass.forall(this, function1);
    }

    public final <B> void foreach(Function1<A, B> function1) {
        TraversableForwarder.Cclass.foreach(this, function1);
    }

    public final A head() {
        return TraversableForwarder.Cclass.head(this);
    }

    public final boolean isDefinedAt(int i) {
        return SeqForwarder.Cclass.isDefinedAt(this, i);
    }

    public final /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public final boolean isEmpty() {
        return TraversableForwarder.Cclass.isEmpty(this);
    }

    public final Iterator<A> iterator() {
        return new ListBuffer$$anon$1(this);
    }

    public final A last() {
        return TraversableForwarder.Cclass.last(this);
    }

    public final int length() {
        return len();
    }

    public final int lengthCompare(int i) {
        return SeqForwarder.Cclass.lengthCompare(this, i);
    }

    public final String mkString() {
        return TraversableForwarder.Cclass.mkString(this);
    }

    public final String mkString(String str) {
        return TraversableForwarder.Cclass.mkString(this, str);
    }

    public final String mkString(String str, String str2, String str3) {
        return TraversableForwarder.Cclass.mkString(this, str, str2, str3);
    }

    public final boolean nonEmpty() {
        return TraversableForwarder.Cclass.nonEmpty(this);
    }

    public final int prefixLength(Function1<A, Object> function1) {
        return SeqForwarder.Cclass.prefixLength(this, function1);
    }

    public final List<A> prependToList(List<A> list) {
        if (scala$collection$mutable$ListBuffer$$start().isEmpty()) {
            return list;
        }
        if (exported()) {
            copy();
        }
        last0().tl_$eq(list);
        return toList();
    }

    public final List<A> readOnly() {
        return scala$collection$mutable$ListBuffer$$start();
    }

    public final List<A> result() {
        return toList();
    }

    public final Iterator<A> reverseIterator() {
        return SeqForwarder.Cclass.reverseIterator(this);
    }

    public final <B> boolean sameElements(GenIterable<B> genIterable) {
        return IterableForwarder.Cclass.sameElements(this, genIterable);
    }

    public final List<A> scala$collection$mutable$ListBuffer$$start() {
        return this.scala$collection$mutable$ListBuffer$$start;
    }

    public final int segmentLength(Function1<A, Object> function1, int i) {
        return SeqForwarder.Cclass.segmentLength(this, function1, i);
    }

    public final /* bridge */ /* synthetic */ Seq seq() {
        return seq();
    }

    public final int size() {
        return length();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer, scala.collection.mutable.Builder] */
    public final void sizeHint(int i) {
        Builder.Cclass.sizeHint((Builder) this, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer, scala.collection.mutable.Builder] */
    public final void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint((Builder) this, traversableLike);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer, scala.collection.mutable.Builder] */
    public final void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer, scala.collection.mutable.Builder] */
    public final void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }

    public final String stringPrefix() {
        return "ListBuffer";
    }

    public final <B> Object toArray(ClassTag<B> classTag) {
        return TraversableForwarder.Cclass.toArray(this, classTag);
    }

    public final <B> Buffer<B> toBuffer() {
        return TraversableForwarder.Cclass.toBuffer(this);
    }

    public final List<A> toList() {
        this.exported = !scala$collection$mutable$ListBuffer$$start().isEmpty();
        return scala$collection$mutable$ListBuffer$$start();
    }

    public final <T, U> Map<T, U> toMap(Predef$$less$colon$less<A, Tuple2<T, U>> predef$$less$colon$less) {
        return TraversableForwarder.Cclass.toMap(this, predef$$less$colon$less);
    }

    public final <B> Set<B> toSet() {
        return TraversableForwarder.Cclass.toSet(this);
    }

    public final Stream<A> toStream() {
        return TraversableForwarder.Cclass.toStream(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.List, scala.collection.immutable.Seq<A>] */
    public final scala.collection.immutable.Seq<A> underlying() {
        return scala$collection$mutable$ListBuffer$$start();
    }
}
