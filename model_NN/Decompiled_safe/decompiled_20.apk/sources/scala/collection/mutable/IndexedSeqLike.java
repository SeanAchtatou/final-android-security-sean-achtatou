package scala.collection.mutable;

public interface IndexedSeqLike<A, Repr> extends scala.collection.IndexedSeqLike<A, Repr> {

    /* renamed from: scala.collection.mutable.IndexedSeqLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(IndexedSeqLike indexedSeqLike) {
        }

        public static IndexedSeq thisCollection(IndexedSeqLike indexedSeqLike) {
            return (IndexedSeq) indexedSeqLike;
        }

        public static IndexedSeq toCollection(IndexedSeqLike indexedSeqLike, Object obj) {
            return (IndexedSeq) obj;
        }
    }

    IndexedSeq<A> thisCollection();

    IndexedSeq<A> toCollection(Object obj);
}
