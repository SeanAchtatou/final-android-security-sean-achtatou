package scala.collection.mutable;

import scala.Serializable;
import scala.collection.generic.MutableSetFactory;

public final class HashSet$ extends MutableSetFactory<HashSet> implements Serializable {
    public static final HashSet$ MODULE$ = null;

    static {
        new HashSet$();
    }

    private HashSet$() {
        MODULE$ = this;
    }

    public final <A> HashSet<A> empty() {
        return new HashSet<>();
    }
}
