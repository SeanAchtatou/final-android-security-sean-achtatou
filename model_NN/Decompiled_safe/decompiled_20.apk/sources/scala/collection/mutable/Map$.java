package scala.collection.mutable;

import scala.collection.generic.MutableMapFactory;

public final class Map$ extends MutableMapFactory<Map> {
    public static final Map$ MODULE$ = null;

    static {
        new Map$();
    }

    private Map$() {
        MODULE$ = this;
    }

    public final <A, B> Map<A, B> empty() {
        return new HashMap();
    }
}
