package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.Predef$;
import scala.Serializable;
import scala.collection.CustomParallelizable;
import scala.collection.GenIterable;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.GenericCompanion;
import scala.collection.mutable.ArrayLike;
import scala.collection.mutable.IndexedSeq;
import scala.collection.mutable.IndexedSeqLike;
import scala.collection.parallel.mutable.ParArray;
import scala.reflect.ClassTag;
import scala.reflect.ClassTag$;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public abstract class WrappedArray<T> extends AbstractSeq<T> implements CustomParallelizable<T, ParArray<T>>, ArrayLike<T, WrappedArray<T>>, IndexedSeq<T> {

    public final class ofBoolean extends WrappedArray<Object> implements Serializable {
        private final boolean[] array;

        public ofBoolean(boolean[] zArr) {
            this.array = zArr;
        }

        public final /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToBoolean(apply(BoxesRunTime.unboxToInt(obj)));
        }

        public final boolean apply(int i) {
            return apply$mcZI$sp(i);
        }

        public final boolean apply$mcZI$sp(int i) {
            return array()[i];
        }

        public final boolean[] array() {
            return this.array;
        }

        public final ClassTag<Object> elemTag() {
            return ClassTag$.MODULE$.Boolean();
        }

        public final int length() {
            return array().length;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = BoxesRunTime.unboxToBoolean(obj);
        }

        public final void update(int i, boolean z) {
            array()[i] = z;
        }
    }

    public final class ofByte extends WrappedArray<Object> implements Serializable {
        private final byte[] array;

        public ofByte(byte[] bArr) {
            this.array = bArr;
        }

        public final byte apply(int i) {
            return array()[i];
        }

        public final /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToByte(apply(BoxesRunTime.unboxToInt(obj)));
        }

        public final byte[] array() {
            return this.array;
        }

        public final ClassTag<Object> elemTag() {
            return ClassTag$.MODULE$.Byte();
        }

        public final int length() {
            return array().length;
        }

        public final void update(int i, byte b) {
            array()[i] = b;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = BoxesRunTime.unboxToByte(obj);
        }
    }

    public final class ofChar extends WrappedArray<Object> implements Serializable {
        private final char[] array;

        public ofChar(char[] cArr) {
            this.array = cArr;
        }

        public final char apply(int i) {
            return array()[i];
        }

        public final /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToCharacter(apply(BoxesRunTime.unboxToInt(obj)));
        }

        public final char[] array() {
            return this.array;
        }

        public final ClassTag<Object> elemTag() {
            return ClassTag$.MODULE$.Char();
        }

        public final int length() {
            return array().length;
        }

        public final void update(int i, char c) {
            array()[i] = c;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = BoxesRunTime.unboxToChar(obj);
        }
    }

    public final class ofDouble extends WrappedArray<Object> implements Serializable {
        private final double[] array;

        public ofDouble(double[] dArr) {
            this.array = dArr;
        }

        public final double apply(int i) {
            return apply$mcDI$sp(i);
        }

        public final /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToDouble(apply(BoxesRunTime.unboxToInt(obj)));
        }

        public final double apply$mcDI$sp(int i) {
            return array()[i];
        }

        public final double[] array() {
            return this.array;
        }

        public final ClassTag<Object> elemTag() {
            return ClassTag$.MODULE$.Double();
        }

        public final int length() {
            return array().length;
        }

        public final void update(int i, double d) {
            array()[i] = d;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = BoxesRunTime.unboxToDouble(obj);
        }
    }

    public final class ofFloat extends WrappedArray<Object> implements Serializable {
        private final float[] array;

        public ofFloat(float[] fArr) {
            this.array = fArr;
        }

        public final float apply(int i) {
            return apply$mcFI$sp(i);
        }

        public final /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToFloat(apply(BoxesRunTime.unboxToInt(obj)));
        }

        public final float apply$mcFI$sp(int i) {
            return array()[i];
        }

        public final float[] array() {
            return this.array;
        }

        public final ClassTag<Object> elemTag() {
            return ClassTag$.MODULE$.Float();
        }

        public final int length() {
            return array().length;
        }

        public final void update(int i, float f) {
            array()[i] = f;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = BoxesRunTime.unboxToFloat(obj);
        }
    }

    public final class ofInt extends WrappedArray<Object> implements Serializable {
        private final int[] array;

        public ofInt(int[] iArr) {
            this.array = iArr;
        }

        public final int apply(int i) {
            return apply$mcII$sp(i);
        }

        public final /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(obj)));
        }

        public final int apply$mcII$sp(int i) {
            return array()[i];
        }

        public final int[] array() {
            return this.array;
        }

        public final ClassTag<Object> elemTag() {
            return ClassTag$.MODULE$.Int();
        }

        public final int length() {
            return array().length;
        }

        public final void update(int i, int i2) {
            array()[i] = i2;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = BoxesRunTime.unboxToInt(obj);
        }
    }

    public final class ofLong extends WrappedArray<Object> implements Serializable {
        private final long[] array;

        public ofLong(long[] jArr) {
            this.array = jArr;
        }

        public final long apply(int i) {
            return apply$mcJI$sp(i);
        }

        public final /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToLong(apply(BoxesRunTime.unboxToInt(obj)));
        }

        public final long apply$mcJI$sp(int i) {
            return array()[i];
        }

        public final long[] array() {
            return this.array;
        }

        public final ClassTag<Object> elemTag() {
            return ClassTag$.MODULE$.Long();
        }

        public final int length() {
            return array().length;
        }

        public final void update(int i, long j) {
            array()[i] = j;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = BoxesRunTime.unboxToLong(obj);
        }
    }

    public final class ofRef<T> extends WrappedArray<T> implements Serializable {
        private final T[] array;
        private volatile boolean bitmap$0;
        private ClassTag<T> elemTag;

        public ofRef(T[] tArr) {
            this.array = tArr;
        }

        private ClassTag elemTag$lzycompute() {
            synchronized (this) {
                if (!this.bitmap$0) {
                    this.elemTag = ClassTag$.MODULE$.apply(ScalaRunTime$.MODULE$.arrayElementClass(array().getClass()));
                    this.bitmap$0 = true;
                }
            }
            return this.elemTag;
        }

        public final T apply(int i) {
            return array()[i];
        }

        public final /* synthetic */ Object apply(Object obj) {
            return apply(BoxesRunTime.unboxToInt(obj));
        }

        public final T[] array() {
            return this.array;
        }

        public final ClassTag<T> elemTag() {
            return this.bitmap$0 ? this.elemTag : elemTag$lzycompute();
        }

        public final int length() {
            return array().length;
        }

        public final void update(int i, T t) {
            array()[i] = t;
        }
    }

    public final class ofShort extends WrappedArray<Object> implements Serializable {
        private final short[] array;

        public ofShort(short[] sArr) {
            this.array = sArr;
        }

        public final /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToShort(apply(BoxesRunTime.unboxToInt(obj)));
        }

        public final short apply(int i) {
            return array()[i];
        }

        public final short[] array() {
            return this.array;
        }

        public final ClassTag<Object> elemTag() {
            return ClassTag$.MODULE$.Short();
        }

        public final int length() {
            return array().length;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = BoxesRunTime.unboxToShort(obj);
        }

        public final void update(int i, short s) {
            array()[i] = s;
        }
    }

    public final class ofUnit extends WrappedArray<BoxedUnit> implements Serializable {
        private final BoxedUnit[] array;

        public ofUnit(BoxedUnit[] boxedUnitArr) {
            this.array = boxedUnitArr;
        }

        public final /* synthetic */ Object apply(Object obj) {
            BoxesRunTime.unboxToInt(obj);
            array();
            return BoxedUnit.UNIT;
        }

        public final void apply(int i) {
            array();
        }

        public final void apply$mcVI$sp(int i) {
            array();
        }

        public final BoxedUnit[] array() {
            return this.array;
        }

        public final ClassTag<BoxedUnit> elemTag() {
            return ClassTag$.MODULE$.Unit();
        }

        public final int length() {
            return array().length;
        }

        public final /* synthetic */ void update(int i, Object obj) {
            array()[i] = (BoxedUnit) obj;
        }

        public final void update(int i, BoxedUnit boxedUnit) {
            array()[i] = boxedUnit;
        }
    }

    public WrappedArray() {
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        ArrayLike.Cclass.$init$(this);
        CustomParallelizable.Cclass.$init$(this);
    }

    private Class<?> elementClass() {
        return ScalaRunTime$.MODULE$.arrayElementClass(array().getClass());
    }

    public abstract Object array();

    public WrappedArray<T> clone() {
        return WrappedArray$.MODULE$.make(ScalaRunTime$.MODULE$.array_clone(array()));
    }

    public GenericCompanion<IndexedSeq> companion() {
        return IndexedSeq.Cclass.companion(this);
    }

    public <B> void copyToArray(Object obj, int i, int i2) {
        IndexedSeqOptimized.Cclass.copyToArray(this, obj, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> drop(int i) {
        return IndexedSeqOptimized.Cclass.drop(this, i);
    }

    public abstract ClassTag<T> elemTag();

    public boolean exists(Function1<T, Object> function1) {
        return IndexedSeqOptimized.Cclass.exists(this, function1);
    }

    public <B> B foldLeft(B b, Function2<B, T, B> function2) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, b, function2);
    }

    public boolean forall(Function1<T, Object> function1) {
        return IndexedSeqOptimized.Cclass.forall(this, function1);
    }

    public <U> void foreach(Function1<T, U> function1) {
        IndexedSeqOptimized.Cclass.foreach(this, function1);
    }

    public int hashCode() {
        return IndexedSeqLike.Cclass.hashCode(this);
    }

    public T head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public Iterator<T> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public T last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public int lengthCompare(int i) {
        return IndexedSeqOptimized.Cclass.lengthCompare(this, i);
    }

    public Builder<T, WrappedArray<T>> newBuilder() {
        return new WrappedArrayBuilder(elemTag());
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public Iterator<T> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(GenIterable<B> genIterable) {
        return IndexedSeqOptimized.Cclass.sameElements(this, genIterable);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public boolean scala$collection$IndexedSeqOptimized$$super$sameElements(GenIterable genIterable) {
        return IterableLike.Cclass.sameElements(this, genIterable);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public int segmentLength(Function1<T, Object> function1, int i) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, function1, i);
    }

    public IndexedSeq<T> seq() {
        return IndexedSeq.Cclass.seq(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> slice(int i, int i2) {
        return IndexedSeqOptimized.Cclass.slice(this, i, i2);
    }

    public String stringPrefix() {
        return "WrappedArray";
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> take(int i) {
        return IndexedSeqOptimized.Cclass.take(this, i);
    }

    public WrappedArray<T> thisCollection() {
        return this;
    }

    public <U> Object toArray(ClassTag<U> classTag) {
        ScalaRunTime$ scalaRunTime$ = ScalaRunTime$.MODULE$;
        Predef$ predef$ = Predef$.MODULE$;
        return elementClass() == scalaRunTime$.arrayElementClass(classTag) ? array() : TraversableOnce.Cclass.toArray(this, classTag);
    }

    public <A1> Buffer<A1> toBuffer() {
        return IndexedSeqLike.Cclass.toBuffer(this);
    }

    public WrappedArray<T> toCollection(WrappedArray<T> wrappedArray) {
        return wrappedArray;
    }

    public abstract void update(int i, T t);
}
