package scala.collection.mutable;

import scala.Array$;
import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.Predef$;
import scala.Predef$$less$colon$less;
import scala.Tuple2;
import scala.collection.CustomParallelizable;
import scala.collection.GenIterable;
import scala.collection.GenSeq;
import scala.collection.GenSeqLike;
import scala.collection.GenTraversableOnce;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Parallelizable;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.FilterMonadic;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.ArrayLike;
import scala.collection.mutable.IndexedSeqLike;
import scala.collection.parallel.mutable.ParArray;
import scala.math.package$;
import scala.reflect.ClassTag;
import scala.runtime.Nothing$;
import scala.runtime.RichInt$;
import scala.runtime.ScalaRunTime$;

public interface ArrayOps<T> extends CustomParallelizable<T, ParArray<T>>, ArrayLike<T, Object> {

    /* renamed from: scala.collection.mutable.ArrayOps$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(ArrayOps arrayOps) {
        }

        public static void copyToArray(ArrayOps arrayOps, Object obj, int i, int i2) {
            int min = package$.MODULE$.min(i2, ScalaRunTime$.MODULE$.array_length(arrayOps.repr()));
            if (ScalaRunTime$.MODULE$.array_length(obj) - i < min) {
                RichInt$ richInt$ = RichInt$.MODULE$;
                Predef$ predef$ = Predef$.MODULE$;
                min = richInt$.max$extension(ScalaRunTime$.MODULE$.array_length(obj) - i, 0);
            }
            Array$.MODULE$.copy(arrayOps.repr(), 0, obj, i, min);
        }

        private static Class elementClass(ArrayOps arrayOps) {
            return ScalaRunTime$.MODULE$.arrayElementClass(arrayOps.repr().getClass());
        }

        public static IndexedSeq seq(ArrayOps arrayOps) {
            return arrayOps.thisCollection();
        }

        public static Object toArray(ArrayOps arrayOps, ClassTag classTag) {
            ScalaRunTime$ scalaRunTime$ = ScalaRunTime$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            return elementClass(arrayOps) == scalaRunTime$.arrayElementClass(classTag) ? arrayOps.repr() : arrayOps.scala$collection$mutable$ArrayOps$$super$toArray(classTag);
        }
    }

    public final class ofRef<T> implements ArrayOps<T> {
        private final T[] repr;

        public ofRef(T[] tArr) {
            this.repr = tArr;
            GenTraversableOnce.Cclass.$init$(this);
            TraversableOnce.Cclass.$init$(this);
            Parallelizable.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            GenSeqLike.Cclass.$init$(this);
            SeqLike.Cclass.$init$(this);
            IndexedSeqLike.Cclass.$init$(this);
            IndexedSeqLike.Cclass.$init$(this);
            IndexedSeqOptimized.Cclass.$init$(this);
            ArrayLike.Cclass.$init$(this);
            CustomParallelizable.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public final <B> B $div$colon(B b, Function2<B, T, B> function2) {
            return TraversableOnce.Cclass.$div$colon(this, b, function2);
        }

        public final <B, That> That $plus$plus(GenTraversableOnce<B> genTraversableOnce, CanBuildFrom<T[], B, That> canBuildFrom) {
            return TraversableLike.Cclass.$plus$plus(this, genTraversableOnce, canBuildFrom);
        }

        public final StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
            return TraversableOnce.Cclass.addString(this, stringBuilder, str, str2, str3);
        }

        public final T apply(int i) {
            return ArrayOps$ofRef$.MODULE$.apply$extension(repr(), i);
        }

        public final boolean canEqual(Object obj) {
            return IterableLike.Cclass.canEqual(this, obj);
        }

        public final <B, That> That collect(PartialFunction<T, B> partialFunction, CanBuildFrom<T[], B, That> canBuildFrom) {
            return TraversableLike.Cclass.collect(this, partialFunction, canBuildFrom);
        }

        public final <B> void copyToArray(Object obj, int i) {
            TraversableOnce.Cclass.copyToArray(this, obj, i);
        }

        public final <U> void copyToArray(Object obj, int i, int i2) {
            Cclass.copyToArray(this, obj, i, i2);
        }

        public final <B> void copyToBuffer(Buffer<B> buffer) {
            TraversableOnce.Cclass.copyToBuffer(this, buffer);
        }

        public final <B> boolean corresponds(GenSeq<B> genSeq, Function2<T, B, Object> function2) {
            return SeqLike.Cclass.corresponds(this, genSeq, function2);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] drop(int i) {
            return IndexedSeqOptimized.Cclass.drop(this, i);
        }

        public final boolean equals(Object obj) {
            return ArrayOps$ofRef$.MODULE$.equals$extension(repr(), obj);
        }

        public final boolean exists(Function1<T, Object> function1) {
            return IndexedSeqOptimized.Cclass.exists(this, function1);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] filter(Function1<T, Object> function1) {
            return TraversableLike.Cclass.filter(this, function1);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] filterNot(Function1<T, Object> function1) {
            return TraversableLike.Cclass.filterNot(this, function1);
        }

        public final <B> B foldLeft(B b, Function2<B, T, B> function2) {
            return IndexedSeqOptimized.Cclass.foldLeft(this, b, function2);
        }

        public final boolean forall(Function1<T, Object> function1) {
            return IndexedSeqOptimized.Cclass.forall(this, function1);
        }

        public final <U> void foreach(Function1<T, U> function1) {
            IndexedSeqOptimized.Cclass.foreach(this, function1);
        }

        public final int hashCode() {
            return ArrayOps$ofRef$.MODULE$.hashCode$extension(repr());
        }

        public final T head() {
            return IndexedSeqOptimized.Cclass.head(this);
        }

        public final boolean isDefinedAt(int i) {
            return GenSeqLike.Cclass.isDefinedAt(this, i);
        }

        public final boolean isEmpty() {
            return IndexedSeqOptimized.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        public final Iterator<T> iterator() {
            return IndexedSeqLike.Cclass.iterator(this);
        }

        public final T last() {
            return IndexedSeqOptimized.Cclass.last(this);
        }

        public final int length() {
            return ArrayOps$ofRef$.MODULE$.length$extension(repr());
        }

        public final int lengthCompare(int i) {
            return IndexedSeqOptimized.Cclass.lengthCompare(this, i);
        }

        public final <B, That> That map(Function1<T, B> function1, CanBuildFrom<T[], B, That> canBuildFrom) {
            return TraversableLike.Cclass.map(this, function1, canBuildFrom);
        }

        public final String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public final String mkString(String str) {
            return TraversableOnce.Cclass.mkString(this, str);
        }

        public final String mkString(String str, String str2, String str3) {
            return TraversableOnce.Cclass.mkString(this, str, str2, str3);
        }

        public final /* synthetic */ Builder newBuilder() {
            return ArrayOps$ofRef$.MODULE$.newBuilder$extension(repr());
        }

        public final boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public final int prefixLength(Function1<T, Object> function1) {
            return GenSeqLike.Cclass.prefixLength(this, function1);
        }

        public final T[] repr() {
            return this.repr;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] reverse() {
            return IndexedSeqOptimized.Cclass.reverse(this);
        }

        public final Iterator<T> reverseIterator() {
            return IndexedSeqOptimized.Cclass.reverseIterator(this);
        }

        public final <B> boolean sameElements(GenIterable<B> genIterable) {
            return IndexedSeqOptimized.Cclass.sameElements(this, genIterable);
        }

        public final Object scala$collection$IndexedSeqOptimized$$super$head() {
            return IterableLike.Cclass.head(this);
        }

        public final Object scala$collection$IndexedSeqOptimized$$super$last() {
            return TraversableLike.Cclass.last(this);
        }

        public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(GenIterable genIterable) {
            return IterableLike.Cclass.sameElements(this, genIterable);
        }

        public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public final Object scala$collection$mutable$ArrayOps$$super$toArray(ClassTag classTag) {
            return TraversableOnce.Cclass.toArray(this, classTag);
        }

        public final int segmentLength(Function1<T, Object> function1, int i) {
            return IndexedSeqOptimized.Cclass.segmentLength(this, function1, i);
        }

        public final IndexedSeq<T> seq() {
            return Cclass.seq(this);
        }

        public final int size() {
            return SeqLike.Cclass.size(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] slice(int i, int i2) {
            return IndexedSeqOptimized.Cclass.slice(this, i, i2);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] sliceWithKnownBound(int i, int i2) {
            return TraversableLike.Cclass.sliceWithKnownBound(this, i, i2);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] sliceWithKnownDelta(int i, int i2, int i3) {
            return TraversableLike.Cclass.sliceWithKnownDelta(this, i, i2, i3);
        }

        public final String stringPrefix() {
            return TraversableLike.Cclass.stringPrefix(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] tail() {
            return IndexedSeqOptimized.Cclass.tail(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public final T[] take(int i) {
            return IndexedSeqOptimized.Cclass.take(this, i);
        }

        public final <Col> Col to(CanBuildFrom<Nothing$, T, Col> canBuildFrom) {
            return TraversableLike.Cclass.to(this, canBuildFrom);
        }

        public final <U> Object toArray(ClassTag<U> classTag) {
            return Cclass.toArray(this, classTag);
        }

        public final <A1> Buffer<A1> toBuffer() {
            return IndexedSeqLike.Cclass.toBuffer(this);
        }

        public final List<T> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public final <T, U> Map<T, U> toMap(Predef$$less$colon$less<T, Tuple2<T, U>> predef$$less$colon$less) {
            return TraversableOnce.Cclass.toMap(this, predef$$less$colon$less);
        }

        public final <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public final Stream<T> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public final String toString() {
            return SeqLike.Cclass.toString(this);
        }

        public final FilterMonadic<T, T[]> withFilter(Function1<T, Object> function1) {
            return TraversableLike.Cclass.withFilter(this, function1);
        }
    }

    <B> Object scala$collection$mutable$ArrayOps$$super$toArray(ClassTag<B> classTag);

    IndexedSeq<T> seq();
}
