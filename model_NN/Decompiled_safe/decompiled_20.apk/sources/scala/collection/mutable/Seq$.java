package scala.collection.mutable;

import scala.collection.generic.SeqFactory;

public final class Seq$ extends SeqFactory<Seq> {
    public static final Seq$ MODULE$ = null;

    static {
        new Seq$();
    }

    private Seq$() {
        MODULE$ = this;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder<A, scala.collection.mutable.Seq<A>>, scala.collection.mutable.ArrayBuffer] */
    public final <A> Builder<A, Seq<A>> newBuilder() {
        return new ArrayBuffer();
    }
}
