package scala.collection.mutable;

import scala.Function1;
import scala.collection.GenSet;
import scala.collection.GenSetLike;
import scala.collection.GenTraversableOnce;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.generic.Subtractable;
import scala.collection.mutable.Builder;
import scala.collection.mutable.Cloneable;
import scala.collection.mutable.Set;
import scala.collection.mutable.SetLike;

public abstract class AbstractSet<A> extends AbstractIterable<A> implements Set<A> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.GenSetLike, scala.collection.mutable.Cloneable, scala.Function1, scala.collection.generic.GenericSetTemplate, scala.collection.Set, scala.collection.mutable.AbstractSet, scala.collection.mutable.Set, scala.collection.generic.Subtractable, scala.collection.generic.Shrinkable, scala.collection.mutable.SetLike, scala.collection.generic.Growable, scala.collection.SetLike, scala.collection.GenSet, scala.collection.mutable.Builder] */
    public AbstractSet() {
        Function1.Cclass.$init$(this);
        GenSetLike.Cclass.$init$(this);
        GenericSetTemplate.Cclass.$init$(this);
        GenSet.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        SetLike.Cclass.$init$(this);
        Set.Cclass.$init$(this);
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        Shrinkable.Cclass.$init$(this);
        Cloneable.Cclass.$init$(this);
        SetLike.Cclass.$init$(this);
        Set.Cclass.$init$(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.SetLike, scala.collection.mutable.AbstractSet] */
    public Set<A> $minus(A a) {
        return SetLike.Cclass.$minus(this, a);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.SetLike, scala.collection.mutable.AbstractSet] */
    public Set<A> $plus(A a) {
        return SetLike.Cclass.$plus(this, a);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.SetLike, scala.collection.mutable.AbstractSet] */
    public Set<A> $plus$plus(GenTraversableOnce<A> genTraversableOnce) {
        return SetLike.Cclass.$plus$plus(this, genTraversableOnce);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.generic.Growable, scala.collection.mutable.AbstractSet] */
    public Growable<A> $plus$plus$eq(TraversableOnce<A> traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public <A> Function1<A, A> andThen(Function1<Object, A> function1) {
        return Function1.Cclass.andThen(this, function1);
    }

    public boolean apply(A a) {
        return GenSetLike.Cclass.apply(this, a);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.SetLike, scala.collection.mutable.AbstractSet] */
    public Set<A> clone() {
        return SetLike.Cclass.clone(this);
    }

    public GenericCompanion<Set> companion() {
        return Set.Cclass.companion(this);
    }

    public <A> Function1<A, Object> compose(Function1<A, A> function1) {
        return Function1.Cclass.compose(this, function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.GenSet, scala.collection.mutable.Set<A>] */
    public Set<A> empty() {
        return GenericSetTemplate.Cclass.empty(this);
    }

    public boolean equals(Object obj) {
        return GenSetLike.Cclass.equals(this, obj);
    }

    public int hashCode() {
        return GenSetLike.Cclass.hashCode(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.mutable.AbstractSet] */
    public boolean isEmpty() {
        return SetLike.Cclass.isEmpty(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.mutable.AbstractSet] */
    public <B, That> That map(Function1<A, B> function1, CanBuildFrom<Set<A>, B, That> canBuildFrom) {
        return SetLike.Cclass.map(this, function1, canBuildFrom);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.SetLike, scala.collection.mutable.AbstractSet] */
    public Builder<A, Set<A>> newBuilder() {
        return SetLike.Cclass.newBuilder(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.mutable.SetLike, scala.collection.mutable.AbstractSet] */
    public Set<A> result() {
        return SetLike.Cclass.result(this);
    }

    public Object scala$collection$SetLike$$super$map(Function1 function1, CanBuildFrom canBuildFrom) {
        return TraversableLike.Cclass.map(this, function1, canBuildFrom);
    }

    public Object scala$collection$mutable$Cloneable$$super$clone() {
        return super.clone();
    }

    public Set<A> seq() {
        return Set.Cclass.seq(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder, scala.collection.mutable.AbstractSet] */
    public void sizeHint(int i) {
        Builder.Cclass.sizeHint((Builder) this, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder, scala.collection.mutable.AbstractSet] */
    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint((Builder) this, traversableLike);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder, scala.collection.mutable.AbstractSet] */
    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder, scala.collection.mutable.AbstractSet] */
    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.mutable.AbstractSet] */
    public String stringPrefix() {
        return SetLike.Cclass.stringPrefix(this);
    }

    public boolean subsetOf(GenSet<A> genSet) {
        return GenSetLike.Cclass.subsetOf(this, genSet);
    }

    public /* bridge */ /* synthetic */ Traversable thisCollection() {
        return thisCollection();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.mutable.AbstractSet] */
    public <A1> Buffer<A1> toBuffer() {
        return SetLike.Cclass.toBuffer(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.SetLike, scala.collection.mutable.AbstractSet] */
    public String toString() {
        return SetLike.Cclass.toString(this);
    }
}
