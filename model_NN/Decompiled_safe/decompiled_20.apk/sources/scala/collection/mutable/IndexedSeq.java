package scala.collection.mutable;

import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;

public interface IndexedSeq<A> extends scala.collection.IndexedSeq<A>, GenericTraversableTemplate<A, IndexedSeq>, IndexedSeqLike<A, IndexedSeq<A>> {

    /* renamed from: scala.collection.mutable.IndexedSeq$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(IndexedSeq indexedSeq) {
        }

        public static GenericCompanion companion(IndexedSeq indexedSeq) {
            return IndexedSeq$.MODULE$;
        }

        public static IndexedSeq seq(IndexedSeq indexedSeq) {
            return indexedSeq;
        }
    }

    IndexedSeq<A> seq();
}
