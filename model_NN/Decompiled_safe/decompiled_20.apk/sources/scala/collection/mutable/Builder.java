package scala.collection.mutable;

import scala.Function1;
import scala.Predef$;
import scala.collection.IndexedSeqLike;
import scala.collection.TraversableLike;
import scala.collection.generic.Growable;
import scala.runtime.RichInt$;

public interface Builder<Elem, To> extends Growable<Elem> {

    /* renamed from: scala.collection.mutable.Builder$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Builder builder) {
        }

        public static Builder mapResult(Builder builder, Function1 function1) {
            return new Builder$$anon$1(builder, function1);
        }

        public static void sizeHint(Builder builder, int i) {
        }

        public static void sizeHint(Builder builder, TraversableLike traversableLike) {
            if (traversableLike instanceof IndexedSeqLike) {
                builder.sizeHint(traversableLike.size());
            }
        }

        public static void sizeHint(Builder builder, TraversableLike traversableLike, int i) {
            if (traversableLike instanceof IndexedSeqLike) {
                builder.sizeHint(traversableLike.size() + i);
            }
        }

        public static void sizeHintBounded(Builder builder, int i, TraversableLike traversableLike) {
            if (traversableLike instanceof IndexedSeqLike) {
                RichInt$ richInt$ = RichInt$.MODULE$;
                Predef$ predef$ = Predef$.MODULE$;
                builder.sizeHint(richInt$.min$extension(i, traversableLike.size()));
            }
        }
    }

    Builder<Elem, To> $plus$eq(Object obj);

    To result();

    void sizeHint(int i);

    void sizeHint(TraversableLike<?, ?> traversableLike);

    void sizeHint(TraversableLike<?, ?> traversableLike, int i);

    void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike);
}
