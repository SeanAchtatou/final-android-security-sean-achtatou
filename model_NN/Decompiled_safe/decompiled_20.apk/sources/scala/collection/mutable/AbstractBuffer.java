package scala.collection.mutable;

import scala.collection.Seq;
import scala.collection.TraversableOnce;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.generic.Subtractable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.BufferLike;
import scala.runtime.BoxesRunTime;

public abstract class AbstractBuffer<A> extends AbstractSeq<A> implements Buffer<A> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Subtractable, scala.collection.generic.Shrinkable, scala.collection.generic.Growable, scala.collection.mutable.Buffer, scala.collection.mutable.AbstractBuffer] */
    public AbstractBuffer() {
        Growable.Cclass.$init$(this);
        Shrinkable.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        BufferLike.Cclass.$init$(this);
        Buffer.Cclass.$init$(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.generic.Growable, scala.collection.mutable.AbstractBuffer] */
    public Growable<A> $plus$plus$eq(TraversableOnce traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public Buffer<A> clone() {
        return BufferLike.Cclass.clone(this);
    }

    public GenericCompanion<Buffer> companion() {
        return Buffer.Cclass.companion(this);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public /* bridge */ /* synthetic */ Seq seq() {
        return seq();
    }

    public String stringPrefix() {
        return BufferLike.Cclass.stringPrefix(this);
    }
}
