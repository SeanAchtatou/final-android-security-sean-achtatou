package scala.collection.mutable;

import scala.collection.GenTraversableOnce;
import scala.collection.Parallelizable;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.mutable.Set;
import scala.collection.mutable.SetLike;
import scala.collection.parallel.mutable.ParSet;

public interface SetLike<A, This extends SetLike<A, This> & Set<A>> extends Parallelizable<A, ParSet<A>>, scala.collection.SetLike<A, This>, Growable<A>, Shrinkable<A> {

    /* renamed from: scala.collection.mutable.SetLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(SetLike setLike) {
        }

        public static Set $minus(SetLike setLike, Object obj) {
            return (Set) setLike.clone().$minus$eq(obj);
        }

        public static Set $plus(SetLike setLike, Object obj) {
            return (Set) setLike.clone().$plus$eq(obj);
        }

        public static Set $plus$plus(SetLike setLike, GenTraversableOnce genTraversableOnce) {
            return (Set) setLike.clone().$plus$plus$eq(genTraversableOnce.seq());
        }

        public static Set clone(SetLike setLike) {
            return (Set) ((Growable) setLike.empty()).$plus$plus$eq(((Set) setLike.repr()).seq());
        }

        public static Builder newBuilder(SetLike setLike) {
            return (Builder) setLike.empty();
        }

        public static Set result(SetLike setLike) {
            return (Set) setLike.repr();
        }
    }

    This $minus(Object obj);

    SetLike<A, This> $minus$eq(A a);

    This $plus(Object obj);

    SetLike<A, This> $plus$eq(A a);

    This $plus$plus(GenTraversableOnce genTraversableOnce);

    This clone();

    This result();
}
