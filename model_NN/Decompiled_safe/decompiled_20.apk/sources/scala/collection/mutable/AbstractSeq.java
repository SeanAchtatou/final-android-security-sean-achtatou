package scala.collection.mutable;

import scala.collection.generic.GenericCompanion;
import scala.collection.mutable.Cloneable;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.Seq;
import scala.collection.mutable.SeqLike;
import scala.collection.mutable.Traversable;
import scala.runtime.BoxesRunTime;

public abstract class AbstractSeq<A> extends scala.collection.AbstractSeq<A> implements Seq<A> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.AbstractSeq, scala.collection.mutable.Cloneable, scala.collection.mutable.Seq, scala.collection.mutable.SeqLike, scala.collection.mutable.Traversable, scala.collection.mutable.Iterable] */
    public AbstractSeq() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Cloneable.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Seq<A>, java.lang.Object] */
    public Seq<A> clone() {
        return Cloneable.Cclass.clone(this);
    }

    public GenericCompanion<Seq> companion() {
        return Seq.Cclass.companion(this);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public Object scala$collection$mutable$Cloneable$$super$clone() {
        return super.clone();
    }

    public Seq<A> seq() {
        return Seq.Cclass.seq(this);
    }
}
