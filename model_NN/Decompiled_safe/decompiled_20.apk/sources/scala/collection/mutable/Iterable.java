package scala.collection.mutable;

import scala.collection.IterableLike;
import scala.collection.generic.GenericCompanion;

public interface Iterable<A> extends scala.collection.Iterable<A>, IterableLike<A, Iterable<A>> {

    /* renamed from: scala.collection.mutable.Iterable$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Iterable iterable) {
        }

        public static GenericCompanion companion(Iterable iterable) {
            return Iterable$.MODULE$;
        }

        public static Iterable seq(Iterable iterable) {
            return iterable;
        }
    }

    Iterable<A> seq();
}
