package scala.collection.mutable;

import scala.collection.generic.SeqFactory;

public final class IndexedSeq$ extends SeqFactory<IndexedSeq> {
    public static final IndexedSeq$ MODULE$ = null;

    static {
        new IndexedSeq$();
    }

    private IndexedSeq$() {
        MODULE$ = this;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder<A, scala.collection.mutable.IndexedSeq<A>>, scala.collection.mutable.ArrayBuffer] */
    public final <A> Builder<A, IndexedSeq<A>> newBuilder() {
        return new ArrayBuffer();
    }
}
