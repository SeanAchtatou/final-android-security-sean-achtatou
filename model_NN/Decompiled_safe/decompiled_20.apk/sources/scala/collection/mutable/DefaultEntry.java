package scala.collection.mutable;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Serializable;
import scala.collection.mutable.HashEntry;

public final class DefaultEntry<A, B> implements Serializable, HashEntry<A, DefaultEntry<A, B>> {
    private final A key;
    private Object next;
    private B value;

    public DefaultEntry(A a, B b) {
        this.key = a;
        this.value = b;
        HashEntry.Cclass.$init$(this);
    }

    public final String chainString() {
        return new StringBuilder().append((Object) "(kv: ").append(key()).append((Object) ", ").append(value()).append((Object) ")").append((Object) (next() == null ? PoiTypeDef.All : new StringBuilder().append((Object) " -> ").append((Object) ((DefaultEntry) next()).toString()).toString())).toString();
    }

    public final A key() {
        return this.key;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.DefaultEntry<A, B>, java.lang.Object] */
    public final DefaultEntry<A, B> next() {
        return this.next;
    }

    public final void next_$eq(DefaultEntry<A, B> defaultEntry) {
        this.next = defaultEntry;
    }

    public final String toString() {
        return chainString();
    }

    public final B value() {
        return this.value;
    }

    public final void value_$eq(B b) {
        this.value = b;
    }
}
