package scala.collection.mutable;

import scala.Serializable;
import scala.collection.generic.MutableMapFactory;

public final class HashMap$ extends MutableMapFactory<HashMap> implements Serializable {
    public static final HashMap$ MODULE$ = null;

    static {
        new HashMap$();
    }

    private HashMap$() {
        MODULE$ = this;
    }

    public final <A, B> HashMap<A, B> empty() {
        return new HashMap<>();
    }
}
