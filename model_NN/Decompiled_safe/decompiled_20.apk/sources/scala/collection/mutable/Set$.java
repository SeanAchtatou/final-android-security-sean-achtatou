package scala.collection.mutable;

import scala.collection.generic.MutableSetFactory;

public final class Set$ extends MutableSetFactory<Set> {
    public static final Set$ MODULE$ = null;

    static {
        new Set$();
    }

    private Set$() {
        MODULE$ = this;
    }

    public final <A> Set<A> empty() {
        return HashSet$.MODULE$.empty();
    }
}
