package scala.collection.mutable;

import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;

public interface Set<A> extends scala.collection.Set<A>, GenericSetTemplate<A, Set>, Iterable<A> {

    /* renamed from: scala.collection.mutable.Set$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Set set) {
        }

        public static GenericCompanion companion(Set set) {
            return Set$.MODULE$;
        }

        public static Set seq(Set set) {
            return set;
        }
    }

    Set<A> seq();
}
