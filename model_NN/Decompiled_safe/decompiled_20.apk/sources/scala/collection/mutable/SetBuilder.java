package scala.collection.mutable;

import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;

public class SetBuilder<A, Coll extends Set<A> & SetLike<A, Coll>> implements Builder<A, Coll> {
    private Coll elems;
    private final Coll empty;

    public SetBuilder(Coll coll) {
        this.empty = coll;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        this.elems = coll;
    }

    public SetBuilder<A, Coll> $plus$eq(A a) {
        elems_$eq(elems().$plus(a));
        return this;
    }

    public Growable<A> $plus$plus$eq(TraversableOnce<A> traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public Coll elems() {
        return this.elems;
    }

    public void elems_$eq(Coll coll) {
        this.elems = coll;
    }

    public Coll result() {
        return elems();
    }

    public void sizeHint(int i) {
        Builder.Cclass.sizeHint(this, i);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }
}
