package scala.collection.mutable;

import scala.Predef$;
import scala.collection.Seq;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.immutable.List$;
import scala.collection.mutable.Builder;

public abstract class LazyBuilder<Elem, To> implements Builder<Elem, To> {
    private ListBuffer<TraversableOnce<Elem>> parts = new ListBuffer<>();

    public LazyBuilder() {
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
    }

    public LazyBuilder<Elem, To> $plus$eq(Elem elem) {
        parts().$plus$eq((Object) List$.MODULE$.apply((Seq) Predef$.MODULE$.genericWrapArray(new Object[]{elem})));
        return this;
    }

    public LazyBuilder<Elem, To> $plus$plus$eq(TraversableOnce<Elem> traversableOnce) {
        parts().$plus$eq((Object) traversableOnce);
        return this;
    }

    public ListBuffer<TraversableOnce<Elem>> parts() {
        return this.parts;
    }

    public void sizeHint(int i) {
        Builder.Cclass.sizeHint(this, i);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }
}
