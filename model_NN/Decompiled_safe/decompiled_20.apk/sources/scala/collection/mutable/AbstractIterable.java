package scala.collection.mutable;

import scala.collection.generic.GenericCompanion;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.Traversable;

public abstract class AbstractIterable<A> extends scala.collection.AbstractIterable<A> implements Iterable<A> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.AbstractIterable, scala.collection.mutable.Traversable, scala.collection.mutable.Iterable] */
    public AbstractIterable() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public Iterable<A> seq() {
        return Iterable.Cclass.seq(this);
    }

    public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
        return thisCollection();
    }
}
