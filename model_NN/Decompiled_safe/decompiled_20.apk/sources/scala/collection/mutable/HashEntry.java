package scala.collection.mutable;

public interface HashEntry<A, E> {

    /* renamed from: scala.collection.mutable.HashEntry$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(HashEntry hashEntry) {
        }
    }

    A key();

    E next();

    void next_$eq(E e);
}
