package scala.collection.mutable;

import scala.Predef$;
import scala.Serializable;
import scala.runtime.AbstractFunction0;

public final class FlatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1 extends AbstractFunction0<String> implements Serializable {
    public final /* synthetic */ FlatHashTable$$anonfun$checkConsistent$1 $outer;
    public final int i$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [int, scala.collection.mutable.FlatHashTable<A>$1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FlatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1(scala.collection.mutable.FlatHashTable$$anonfun$checkConsistent$1 r2, scala.collection.mutable.FlatHashTable<A>.1 r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.i$1 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.FlatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1.<init>(scala.collection.mutable.FlatHashTable$$anonfun$checkConsistent$1, int):void");
    }

    public final String apply() {
        return new StringBuilder().append(this.i$1).append((Object) " ").append(this.$outer.$outer.table()[this.i$1]).append((Object) " ").append((Object) Predef$.MODULE$.refArrayOps(this.$outer.$outer.table()).mkString()).toString();
    }
}
