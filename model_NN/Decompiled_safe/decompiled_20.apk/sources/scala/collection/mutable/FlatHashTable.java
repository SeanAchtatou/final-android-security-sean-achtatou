package scala.collection.mutable;

import java.util.Arrays;
import scala.None$;
import scala.Option;
import scala.Predef$;
import scala.Some;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Range;
import scala.collection.immutable.Range$;
import scala.collection.mutable.ArrayOps;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt$;
import scala.util.hashing.package$;

public interface FlatHashTable<A> extends HashUtils<A> {

    public class Contents<A> {
        private final int loadFactor;
        private final int seedvalue;
        private final int[] sizemap;
        private final Object[] table;
        private final int tableSize;
        private final int threshold;

        public int loadFactor() {
            return this.loadFactor;
        }

        public int seedvalue() {
            return this.seedvalue;
        }

        public int[] sizemap() {
            return this.sizemap;
        }

        public Object[] table() {
            return this.table;
        }

        public int tableSize() {
            return this.tableSize;
        }

        public int threshold() {
            return this.threshold;
        }
    }

    public interface HashUtils<A> {

        /* renamed from: scala.collection.mutable.FlatHashTable$HashUtils$class  reason: invalid class name */
        public abstract class Cclass {
            public static void $init$(HashUtils hashUtils) {
            }

            public static int elemHashCode(HashUtils hashUtils, Object obj) {
                if (obj != null) {
                    return obj.hashCode();
                }
                throw new IllegalArgumentException("Flat hash tables cannot contain null elements.");
            }

            public static final int improve(HashUtils hashUtils, int i, int i2) {
                int byteswap32 = package$.MODULE$.byteswap32(i);
                int i3 = i2 % 32;
                return (byteswap32 << (32 - i3)) | (byteswap32 >>> i3);
            }

            public static final int sizeMapBucketBitSize(HashUtils hashUtils) {
                return 5;
            }

            public static final int sizeMapBucketSize(HashUtils hashUtils) {
                return 1 << hashUtils.sizeMapBucketBitSize();
            }
        }

        int elemHashCode(A a);

        int improve(int i, int i2);

        int sizeMapBucketBitSize();

        int sizeMapBucketSize();
    }

    /* renamed from: scala.collection.mutable.FlatHashTable$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(FlatHashTable flatHashTable) {
            flatHashTable._loadFactor_$eq(FlatHashTable$.MODULE$.defaultLoadFactor());
            flatHashTable.table_$eq(new Object[initialCapacity(flatHashTable)]);
            flatHashTable.tableSize_$eq(0);
            flatHashTable.threshold_$eq(FlatHashTable$.MODULE$.newThreshold(flatHashTable._loadFactor(), initialCapacity(flatHashTable)));
            flatHashTable.sizemap_$eq(null);
            flatHashTable.seedvalue_$eq(flatHashTable.tableSizeSeed());
        }

        public static boolean addEntry(FlatHashTable flatHashTable, Object obj) {
            int index = flatHashTable.index(flatHashTable.elemHashCode(obj));
            Object obj2 = flatHashTable.table()[index];
            while (obj2 != null) {
                if (obj2 == obj ? true : obj2 == null ? false : obj2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj2, obj) : obj2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj2, obj) : obj2.equals(obj)) {
                    return false;
                }
                index = (index + 1) % flatHashTable.table().length;
                obj2 = flatHashTable.table()[index];
            }
            flatHashTable.table()[index] = obj;
            flatHashTable.tableSize_$eq(flatHashTable.tableSize() + 1);
            flatHashTable.nnSizeMapAdd(index);
            if (flatHashTable.tableSize() >= flatHashTable.threshold()) {
                growTable(flatHashTable);
            }
            return true;
        }

        public static boolean alwaysInitSizeMap(FlatHashTable flatHashTable) {
            return false;
        }

        public static int calcSizeMapSize(FlatHashTable flatHashTable, int i) {
            return (i >> flatHashTable.sizeMapBucketBitSize()) + 1;
        }

        public static int capacity(FlatHashTable flatHashTable, int i) {
            if (i == 0) {
                return 1;
            }
            return HashTable$.MODULE$.powerOfTwo(i);
        }

        private static void checkConsistent(FlatHashTable flatHashTable) {
            Predef$ predef$ = Predef$.MODULE$;
            int length = flatHashTable.table().length;
            Range$ range$ = Range$.MODULE$;
            Range range = new Range(0, length, 1);
            FlatHashTable$$anonfun$checkConsistent$1 flatHashTable$$anonfun$checkConsistent$1 = new FlatHashTable$$anonfun$checkConsistent$1(flatHashTable);
            if (range.validateRangeBoundaries(flatHashTable$$anonfun$checkConsistent$1)) {
                int start = range.start();
                int terminalElement = range.terminalElement();
                int step = range.step();
                while (start != terminalElement) {
                    if (flatHashTable.table()[start] == null || flatHashTable.containsEntry(flatHashTable.table()[start])) {
                        start += step;
                    } else {
                        Predef$ predef$2 = Predef$.MODULE$;
                        FlatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1 flatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1 = new FlatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1(flatHashTable$$anonfun$checkConsistent$1, start);
                        StringBuilder append = new StringBuilder().append((Object) "assertion failed: ");
                        StringBuilder append2 = new StringBuilder().append(start).append((Object) " ").append(flatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1.$outer.$outer.table()[start]).append((Object) " ");
                        Predef$ predef$3 = Predef$.MODULE$;
                        throw new AssertionError(append.append((Object) append2.append((Object) TraversableOnce.Cclass.mkString(new ArrayOps.ofRef(flatHashTable$$anonfun$checkConsistent$1$$anonfun$apply$mcVI$sp$1.$outer.$outer.table()))).toString()).toString());
                    }
                }
            }
        }

        public static boolean containsEntry(FlatHashTable flatHashTable, Object obj) {
            return findEntryImpl(flatHashTable, obj) != null;
        }

        private static Object findEntryImpl(FlatHashTable flatHashTable, Object obj) {
            int index = flatHashTable.index(flatHashTable.elemHashCode(obj));
            Object obj2 = flatHashTable.table()[index];
            int i = index;
            while (obj2 != null) {
                if (obj2 == obj ? true : obj2 == null ? false : obj2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj2, obj) : obj2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj2, obj) : obj2.equals(obj)) {
                    break;
                }
                int length = (i + 1) % flatHashTable.table().length;
                obj2 = flatHashTable.table()[length];
                i = length;
            }
            return obj2;
        }

        private static void growTable(FlatHashTable flatHashTable) {
            Object[] table = flatHashTable.table();
            flatHashTable.table_$eq(new Object[(flatHashTable.table().length * 2)]);
            flatHashTable.tableSize_$eq(0);
            flatHashTable.nnSizeMapReset(flatHashTable.table().length);
            flatHashTable.seedvalue_$eq(flatHashTable.tableSizeSeed());
            flatHashTable.threshold_$eq(FlatHashTable$.MODULE$.newThreshold(flatHashTable._loadFactor(), flatHashTable.table().length));
            for (Object obj : table) {
                if (obj != null) {
                    flatHashTable.addEntry(obj);
                }
            }
            if (tableDebug(flatHashTable)) {
                checkConsistent(flatHashTable);
            }
        }

        public static final int index(FlatHashTable flatHashTable, int i) {
            int improve = flatHashTable.improve(i, flatHashTable.seedvalue());
            int length = flatHashTable.table().length - 1;
            return (improve >>> (32 - Integer.bitCount(length))) & length;
        }

        public static void initWithContents(FlatHashTable flatHashTable, Contents contents) {
            if (contents != null) {
                flatHashTable._loadFactor_$eq(contents.loadFactor());
                flatHashTable.table_$eq(contents.table());
                flatHashTable.tableSize_$eq(contents.tableSize());
                flatHashTable.threshold_$eq(contents.threshold());
                flatHashTable.seedvalue_$eq(contents.seedvalue());
                flatHashTable.sizemap_$eq(contents.sizemap());
            }
            if (flatHashTable.alwaysInitSizeMap() && flatHashTable.sizemap() == null) {
                flatHashTable.sizeMapInitAndRebuild();
            }
        }

        private static int initialCapacity(FlatHashTable flatHashTable) {
            return flatHashTable.capacity(flatHashTable.initialSize());
        }

        public static int initialSize(FlatHashTable flatHashTable) {
            return 32;
        }

        public static Iterator iterator(FlatHashTable flatHashTable) {
            return new FlatHashTable$$anon$1(flatHashTable);
        }

        public static void nnSizeMapAdd(FlatHashTable flatHashTable, int i) {
            if (flatHashTable.sizemap() != null) {
                int sizeMapBucketBitSize = i >> flatHashTable.sizeMapBucketBitSize();
                int[] sizemap = flatHashTable.sizemap();
                sizemap[sizeMapBucketBitSize] = sizemap[sizeMapBucketBitSize] + 1;
            }
        }

        public static void nnSizeMapRemove(FlatHashTable flatHashTable, int i) {
            if (flatHashTable.sizemap() != null) {
                int[] sizemap = flatHashTable.sizemap();
                int sizeMapBucketBitSize = i >> flatHashTable.sizeMapBucketBitSize();
                sizemap[sizeMapBucketBitSize] = sizemap[sizeMapBucketBitSize] - 1;
            }
        }

        public static void nnSizeMapReset(FlatHashTable flatHashTable, int i) {
            if (flatHashTable.sizemap() != null) {
                int calcSizeMapSize = flatHashTable.calcSizeMapSize(i);
                if (flatHashTable.sizemap().length != calcSizeMapSize) {
                    flatHashTable.sizemap_$eq(new int[calcSizeMapSize]);
                } else {
                    Arrays.fill(flatHashTable.sizemap(), 0);
                }
            }
        }

        private static final boolean precedes$1(FlatHashTable flatHashTable, int i, int i2) {
            int length = flatHashTable.table().length >> 1;
            return i <= i2 ? i2 - i < length : i - i2 > length;
        }

        public static Option removeEntry(FlatHashTable flatHashTable, Object obj) {
            if (tableDebug(flatHashTable)) {
                checkConsistent(flatHashTable);
            }
            int index = flatHashTable.index(flatHashTable.elemHashCode(obj));
            Object obj2 = flatHashTable.table()[index];
            while (obj2 != null) {
                if (obj2 == obj ? true : obj2 == null ? false : obj2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj2, obj) : obj2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj2, obj) : obj2.equals(obj)) {
                    int length = (index + 1) % flatHashTable.table().length;
                    int i = index;
                    while (flatHashTable.table()[length] != null) {
                        int index2 = flatHashTable.index(flatHashTable.elemHashCode(flatHashTable.table()[length]));
                        if (index2 != length && precedes$1(flatHashTable, index2, i)) {
                            flatHashTable.table()[i] = flatHashTable.table()[length];
                            i = length;
                        }
                        length = (length + 1) % flatHashTable.table().length;
                    }
                    flatHashTable.table()[i] = null;
                    flatHashTable.tableSize_$eq(flatHashTable.tableSize() - 1);
                    flatHashTable.nnSizeMapRemove(i);
                    if (tableDebug(flatHashTable)) {
                        checkConsistent(flatHashTable);
                    }
                    return new Some(obj2);
                }
                index = (index + 1) % flatHashTable.table().length;
                obj2 = flatHashTable.table()[index];
            }
            return None$.MODULE$;
        }

        public static void sizeMapInit(FlatHashTable flatHashTable, int i) {
            flatHashTable.sizemap_$eq(new int[flatHashTable.calcSizeMapSize(i)]);
        }

        public static void sizeMapInitAndRebuild(FlatHashTable flatHashTable) {
            flatHashTable.sizeMapInit(flatHashTable.table().length);
            int i = flatHashTable.totalSizeMapBuckets();
            Object[] table = flatHashTable.table();
            RichInt$ richInt$ = RichInt$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            int i2 = 0;
            int min$extension = richInt$.min$extension(flatHashTable.sizeMapBucketSize(), table.length);
            int i3 = 0;
            while (i2 < i) {
                int i4 = i3;
                int i5 = 0;
                while (i4 < min$extension) {
                    if (table[i4] != null) {
                        i5++;
                    }
                    i4++;
                }
                flatHashTable.sizemap()[i2] = i5;
                min$extension += flatHashTable.sizeMapBucketSize();
                i2++;
                i3 = i4;
            }
        }

        private static final boolean tableDebug(FlatHashTable flatHashTable) {
            return false;
        }

        public static int tableSizeSeed(FlatHashTable flatHashTable) {
            return Integer.bitCount(flatHashTable.table().length - 1);
        }

        public static final int totalSizeMapBuckets(FlatHashTable flatHashTable) {
            return ((flatHashTable.table().length - 1) / flatHashTable.sizeMapBucketSize()) + 1;
        }
    }

    int _loadFactor();

    void _loadFactor_$eq(int i);

    boolean addEntry(A a);

    boolean alwaysInitSizeMap();

    int calcSizeMapSize(int i);

    int capacity(int i);

    boolean containsEntry(A a);

    int index(int i);

    void initWithContents(Contents<A> contents);

    int initialSize();

    void nnSizeMapAdd(int i);

    void nnSizeMapRemove(int i);

    void nnSizeMapReset(int i);

    Option<A> removeEntry(A a);

    int seedvalue();

    void seedvalue_$eq(int i);

    void sizeMapInit(int i);

    void sizeMapInitAndRebuild();

    int[] sizemap();

    void sizemap_$eq(int[] iArr);

    Object[] table();

    int tableSize();

    int tableSizeSeed();

    void tableSize_$eq(int i);

    void table_$eq(Object[] objArr);

    int threshold();

    void threshold_$eq(int i);

    int totalSizeMapBuckets();
}
