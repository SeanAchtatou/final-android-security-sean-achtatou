package scala.collection.mutable;

import scala.collection.AbstractIterator;
import scala.collection.Iterator$;

public final class FlatHashTable$$anon$1 extends AbstractIterator<A> {
    private final /* synthetic */ FlatHashTable $outer;
    private int i;

    public FlatHashTable$$anon$1(FlatHashTable<A> flatHashTable) {
        if (flatHashTable == null) {
            throw new NullPointerException();
        }
        this.$outer = flatHashTable;
        this.i = 0;
    }

    private int i() {
        return this.i;
    }

    private void i_$eq(int i2) {
        this.i = i2;
    }

    public final boolean hasNext() {
        while (i() < this.$outer.table().length && this.$outer.table()[i()] == null) {
            this.i = i() + 1;
        }
        return i() < this.$outer.table().length;
    }

    public final A next() {
        if (!hasNext()) {
            return Iterator$.MODULE$.empty().next();
        }
        this.i = i() + 1;
        return this.$outer.table()[i() - 1];
    }
}
