package scala.collection.mutable;

public interface Cloneable<A> extends scala.Cloneable {

    /* renamed from: scala.collection.mutable.Cloneable$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Cloneable cloneable) {
        }

        public static Object clone(Cloneable cloneable) {
            return cloneable.scala$collection$mutable$Cloneable$$super$clone();
        }
    }

    Object scala$collection$mutable$Cloneable$$super$clone();
}
