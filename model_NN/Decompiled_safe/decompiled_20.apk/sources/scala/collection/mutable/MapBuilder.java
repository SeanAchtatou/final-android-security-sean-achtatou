package scala.collection.mutable;

import scala.Tuple2;
import scala.collection.GenMap;
import scala.collection.GenMapLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;

public class MapBuilder<A, B, Coll extends GenMap<A, B> & GenMapLike<A, B, Coll>> implements Builder<Tuple2<A, B>, Coll> {
    private Coll elems;
    private final Coll empty;

    public MapBuilder(Coll coll) {
        this.empty = coll;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        this.elems = coll;
    }

    public MapBuilder<A, B, Coll> $plus$eq(Tuple2<A, B> tuple2) {
        elems_$eq(elems().$plus(tuple2));
        return this;
    }

    public Growable<Tuple2<A, B>> $plus$plus$eq(TraversableOnce<Tuple2<A, B>> traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public Coll elems() {
        return this.elems;
    }

    public void elems_$eq(Coll coll) {
        this.elems = coll;
    }

    public Coll result() {
        return elems();
    }

    public void sizeHint(int i) {
        Builder.Cclass.sizeHint(this, i);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }
}
