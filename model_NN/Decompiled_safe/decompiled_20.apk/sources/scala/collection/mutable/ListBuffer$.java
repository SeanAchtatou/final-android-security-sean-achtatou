package scala.collection.mutable;

import scala.Serializable;
import scala.collection.generic.SeqFactory;

public final class ListBuffer$ extends SeqFactory<ListBuffer> implements Serializable {
    public static final ListBuffer$ MODULE$ = null;

    static {
        new ListBuffer$();
    }

    private ListBuffer$() {
        MODULE$ = this;
    }

    public final <A> Builder<A, ListBuffer<A>> newBuilder() {
        return new GrowingBuilder(new ListBuffer());
    }
}
