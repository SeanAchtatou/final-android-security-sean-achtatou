package scala.collection.mutable;

import scala.Function1;
import scala.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;

public final class HashMap$$anonfun$foreach$1 extends AbstractFunction1<DefaultEntry<A, B>, C> implements Serializable {
    private final Function1 f$1;

    public HashMap$$anonfun$foreach$1(HashMap hashMap, HashMap<A, B> hashMap2) {
        this.f$1 = hashMap2;
    }

    public final C apply(DefaultEntry<A, B> defaultEntry) {
        return this.f$1.apply(new Tuple2(defaultEntry.key(), defaultEntry.value()));
    }
}
