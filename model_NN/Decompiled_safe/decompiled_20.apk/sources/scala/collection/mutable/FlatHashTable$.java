package scala.collection.mutable;

import scala.Predef$;

public final class FlatHashTable$ {
    public static final FlatHashTable$ MODULE$ = null;

    static {
        new FlatHashTable$();
    }

    private FlatHashTable$() {
        MODULE$ = this;
    }

    public final int defaultLoadFactor() {
        return 450;
    }

    public final int newThreshold(int i, int i2) {
        Predef$ predef$ = Predef$.MODULE$;
        if (i < 500) {
            return (int) ((((long) i2) * ((long) i)) / 1000);
        }
        throw new AssertionError(new StringBuilder().append((Object) "assertion failed: ").append((Object) "loadFactor too large; must be < 0.5").toString());
    }
}
