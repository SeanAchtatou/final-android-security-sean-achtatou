package scala.collection.mutable;

import scala.Function1;
import scala.Option;
import scala.Tuple2;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.mutable.Builder;
import scala.collection.mutable.Cloneable;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.Map;
import scala.collection.mutable.MapLike;
import scala.collection.mutable.Traversable;

public abstract class AbstractMap<A, B> extends scala.collection.AbstractMap<A, B> implements Map<A, B> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Map, scala.collection.generic.Shrinkable, scala.collection.mutable.Cloneable, scala.collection.generic.Growable, scala.collection.mutable.AbstractMap, scala.collection.mutable.Traversable, scala.collection.mutable.Builder, scala.collection.mutable.MapLike, scala.collection.mutable.Iterable] */
    public AbstractMap() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        Shrinkable.Cclass.$init$(this);
        Cloneable.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
    }

    public Map<A, B> $minus(A a) {
        return MapLike.Cclass.$minus(this, a);
    }

    public <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2) {
        return MapLike.Cclass.$plus(this, tuple2);
    }

    public Growable<Tuple2<A, B>> $plus$plus$eq(TraversableOnce<Tuple2<A, B>> traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public Map<A, B> clone() {
        return MapLike.Cclass.clone(this);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public Map<A, B> empty() {
        return Map.Cclass.empty(this);
    }

    public /* bridge */ /* synthetic */ Object filterNot(Function1 function1) {
        return filterNot(function1);
    }

    public Builder<Tuple2<A, B>, Map<A, B>> newBuilder() {
        return MapLike.Cclass.newBuilder(this);
    }

    public Option<B> put(Object obj, Object obj2) {
        return MapLike.Cclass.put(this, obj, obj2);
    }

    public Map<A, B> result() {
        return MapLike.Cclass.result(this);
    }

    public Object scala$collection$mutable$Cloneable$$super$clone() {
        return super.clone();
    }

    public Map<A, B> seq() {
        return Map.Cclass.seq(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.AbstractMap, scala.collection.mutable.Builder] */
    public void sizeHint(int i) {
        Builder.Cclass.sizeHint((Builder) this, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.AbstractMap, scala.collection.mutable.Builder] */
    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint((Builder) this, traversableLike);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.AbstractMap, scala.collection.mutable.Builder] */
    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.AbstractMap, scala.collection.mutable.Builder] */
    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }

    public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
        return thisCollection();
    }

    public void update(Object obj, Object obj2) {
        MapLike.Cclass.update(this, obj, obj2);
    }
}
