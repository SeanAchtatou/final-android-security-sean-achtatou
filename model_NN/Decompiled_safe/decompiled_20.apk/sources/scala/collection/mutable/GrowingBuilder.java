package scala.collection.mutable;

import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;

public class GrowingBuilder<Elem, To extends Growable<Elem>> implements Builder<Elem, To> {
    private To elems;
    private final To empty;

    public GrowingBuilder(To to) {
        this.empty = to;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        this.elems = to;
    }

    public GrowingBuilder<Elem, To> $plus$eq(Elem elem) {
        elems().$plus$eq(elem);
        return this;
    }

    public Growable<Elem> $plus$plus$eq(TraversableOnce<Elem> traversableOnce) {
        return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public To elems() {
        return this.elems;
    }

    public To result() {
        return elems();
    }

    public void sizeHint(int i) {
        Builder.Cclass.sizeHint(this, i);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }
}
