package scala.collection.mutable;

import scala.collection.AbstractIterator;
import scala.collection.mutable.HashTable;

public final class HashTable$$anon$1 extends AbstractIterator<Entry> {
    private HashEntry<A, Entry> es = iterTable()[idx()];
    private int idx;
    private final HashEntry<A, Entry>[] iterTable;

    public HashTable$$anon$1(HashTable<A, Entry> hashTable) {
        this.iterTable = hashTable.table();
        this.idx = HashTable.Cclass.scala$collection$mutable$HashTable$$lastPopulatedIndex(hashTable);
    }

    private HashEntry<A, Entry> es() {
        return this.es;
    }

    private void es_$eq(HashEntry<A, Entry> hashEntry) {
        this.es = hashEntry;
    }

    private int idx() {
        return this.idx;
    }

    private void idx_$eq(int i) {
        this.idx = i;
    }

    private HashEntry<A, Entry>[] iterTable() {
        return this.iterTable;
    }

    public final boolean hasNext() {
        return es() != null;
    }

    public final Entry next() {
        Entry es2 = es();
        this.es = (HashEntry) es().next();
        while (es() == null && idx() > 0) {
            this.idx = idx() - 1;
            this.es = iterTable()[idx()];
        }
        return es2;
    }
}
