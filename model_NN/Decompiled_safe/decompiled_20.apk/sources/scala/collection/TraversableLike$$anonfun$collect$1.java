package scala.collection;

import scala.PartialFunction;
import scala.Serializable;
import scala.collection.mutable.Builder;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

public final class TraversableLike$$anonfun$collect$1 extends AbstractFunction1<A, Object> implements Serializable {
    private final Builder b$4;
    private final PartialFunction pf$1;

    public TraversableLike$$anonfun$collect$1(TraversableLike traversableLike, Builder builder, PartialFunction partialFunction) {
        this.b$4 = builder;
        this.pf$1 = partialFunction;
    }

    public final Object apply(A a) {
        return this.pf$1.isDefinedAt(a) ? this.b$4.$plus$eq(this.pf$1.apply(a)) : BoxedUnit.UNIT;
    }
}
