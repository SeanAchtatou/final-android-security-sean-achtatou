package scala.collection;

import java.util.NoSuchElementException;
import scala.Serializable;
import scala.runtime.AbstractFunction0;
import scala.runtime.Nothing$;

public final class TraversableLike$$anonfun$2 extends AbstractFunction0<Nothing$> implements Serializable {
    public TraversableLike$$anonfun$2(TraversableLike<A, Repr> traversableLike) {
    }

    public final Nothing$ apply() {
        throw new NoSuchElementException();
    }
}
