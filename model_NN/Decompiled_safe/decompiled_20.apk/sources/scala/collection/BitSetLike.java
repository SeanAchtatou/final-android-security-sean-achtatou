package scala.collection;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Function1;
import scala.Predef$;
import scala.collection.BitSetLike;
import scala.collection.SortedSet;
import scala.collection.immutable.Range;
import scala.collection.immutable.Range$;
import scala.collection.mutable.StringBuilder;
import scala.math.Ordering;
import scala.math.Ordering$Int$;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

public interface BitSetLike<This extends BitSetLike<This> & SortedSet<Object>> extends SortedSetLike<Object, This> {

    /* renamed from: scala.collection.BitSetLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(BitSetLike bitSetLike) {
        }

        public static StringBuilder addString(BitSetLike bitSetLike, StringBuilder stringBuilder, String str, String str2, String str3) {
            stringBuilder.append(str);
            ObjectRef objectRef = new ObjectRef(PoiTypeDef.All);
            Predef$ predef$ = Predef$.MODULE$;
            int nwords = bitSetLike.nwords() * BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength();
            Range$ range$ = Range$.MODULE$;
            Range range = new Range(0, nwords, 1);
            if (range.validateRangeBoundaries(new BitSetLike$$anonfun$addString$1(bitSetLike, objectRef, stringBuilder, str2))) {
                int start = range.start();
                int terminalElement = range.terminalElement();
                int step = range.step();
                for (int i = start; i != terminalElement; i += step) {
                    if (bitSetLike.contains(i)) {
                        stringBuilder.append((String) objectRef.elem).append(i);
                        objectRef.elem = str2;
                    }
                }
            }
            return stringBuilder.append(str3);
        }

        public static boolean contains(BitSetLike bitSetLike, int i) {
            return i >= 0 && (bitSetLike.word(i >> BitSetLike$.MODULE$.LogWL()) & (1 << i)) != 0;
        }

        public static void foreach(BitSetLike bitSetLike, Function1 function1) {
            Predef$ predef$ = Predef$.MODULE$;
            int nwords = bitSetLike.nwords();
            Range$ range$ = Range$.MODULE$;
            Range range = new Range(0, nwords, 1);
            BitSetLike$$anonfun$foreach$1 bitSetLike$$anonfun$foreach$1 = new BitSetLike$$anonfun$foreach$1(bitSetLike, function1);
            if (range.validateRangeBoundaries(bitSetLike$$anonfun$foreach$1)) {
                int terminalElement = range.terminalElement();
                int step = range.step();
                for (int start = range.start(); start != terminalElement; start += step) {
                    long word = bitSetLike.word(start);
                    Predef$ predef$2 = Predef$.MODULE$;
                    int scala$collection$BitSetLike$$WordLength = (start + 1) * BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength();
                    Range$ range$2 = Range$.MODULE$;
                    Range range2 = new Range(BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength() * start, scala$collection$BitSetLike$$WordLength, 1);
                    if (range2.validateRangeBoundaries(new BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1(bitSetLike$$anonfun$foreach$1, word))) {
                        int terminalElement2 = range2.terminalElement();
                        int step2 = range2.step();
                        for (int start2 = range2.start(); start2 != terminalElement2; start2 += step2) {
                            if (((1 << start2) & word) != 0) {
                                function1.apply(BoxesRunTime.boxToInteger(start2));
                            } else {
                                BoxedUnit boxedUnit = BoxedUnit.UNIT;
                            }
                        }
                    }
                }
            }
        }

        public static Iterator iterator(BitSetLike bitSetLike) {
            return new BitSetLike$$anon$1(bitSetLike);
        }

        public static Ordering ordering(BitSetLike bitSetLike) {
            return Ordering$Int$.MODULE$;
        }

        public static int size(BitSetLike bitSetLike) {
            int i = 0;
            int nwords = bitSetLike.nwords();
            while (nwords > 0) {
                nwords--;
                i += Long.bitCount(bitSetLike.word(nwords));
            }
            return i;
        }

        public static String stringPrefix(BitSetLike bitSetLike) {
            return "BitSet";
        }
    }

    boolean contains(int i);

    Iterator<Object> iterator();

    int nwords();

    long word(int i);
}
