package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.IntRef;

public final class TraversableOnce$$anonfun$size$1 extends AbstractFunction1<A, BoxedUnit> implements Serializable {
    private final IntRef result$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.runtime.IntRef, scala.collection.TraversableOnce<A>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TraversableOnce$$anonfun$size$1(scala.collection.TraversableOnce r1, scala.collection.TraversableOnce<A> r2) {
        /*
            r0 = this;
            r0.result$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.TraversableOnce$$anonfun$size$1.<init>(scala.collection.TraversableOnce, scala.runtime.IntRef):void");
    }

    public final void apply(A a) {
        this.result$1.elem++;
    }
}
