package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.Nothing$;

public final class TraversableLike$$anonfun$isEmpty$1$$anonfun$apply$mcV$sp$1 extends AbstractFunction1<A, Nothing$> implements Serializable {
    private final /* synthetic */ TraversableLike$$anonfun$isEmpty$1 $outer;

    public TraversableLike$$anonfun$isEmpty$1$$anonfun$apply$mcV$sp$1(TraversableLike<A, Repr>.1 r2) {
        if (r2 == null) {
            throw new NullPointerException();
        }
        this.$outer = r2;
    }

    public final Nothing$ apply(A a) {
        this.$outer.result$1.elem = false;
        return Traversable$.MODULE$.breaks().m6break();
    }
}
