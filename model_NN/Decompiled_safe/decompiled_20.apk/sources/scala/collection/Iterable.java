package scala.collection;

import scala.collection.generic.GenericCompanion;

public interface Iterable<A> extends GenIterable<A>, IterableLike<A, Iterable<A>>, Traversable<A> {

    /* renamed from: scala.collection.Iterable$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Iterable iterable) {
        }

        public static GenericCompanion companion(Iterable iterable) {
            return Iterable$.MODULE$;
        }

        public static Iterable seq(Iterable iterable) {
            return iterable;
        }
    }

    Iterable<A> seq();
}
