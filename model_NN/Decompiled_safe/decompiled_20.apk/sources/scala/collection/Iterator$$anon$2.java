package scala.collection;

import java.util.NoSuchElementException;
import scala.runtime.Nothing$;

public final class Iterator$$anon$2 extends AbstractIterator<Nothing$> {
    public final boolean hasNext() {
        return false;
    }

    public final Nothing$ next() {
        throw new NoSuchElementException("next on empty iterator");
    }
}
