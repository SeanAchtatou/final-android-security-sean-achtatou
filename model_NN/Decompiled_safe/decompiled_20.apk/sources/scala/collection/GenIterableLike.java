package scala.collection;

public interface GenIterableLike<A, Repr> extends GenTraversableLike<A, Repr> {
    Iterator<A> iterator();

    <A1> boolean sameElements(GenIterable<A1> genIterable);
}
