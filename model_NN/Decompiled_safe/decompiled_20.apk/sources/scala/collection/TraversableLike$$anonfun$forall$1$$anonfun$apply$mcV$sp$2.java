package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class TraversableLike$$anonfun$forall$1$$anonfun$apply$mcV$sp$2 extends AbstractFunction1<A, BoxedUnit> implements Serializable {
    private final /* synthetic */ TraversableLike$$anonfun$forall$1 $outer;

    public TraversableLike$$anonfun$forall$1$$anonfun$apply$mcV$sp$2(TraversableLike<A, Repr>.1 r2) {
        if (r2 == null) {
            throw new NullPointerException();
        }
        this.$outer = r2;
    }

    public final void apply(A a) {
        if (!BoxesRunTime.unboxToBoolean(this.$outer.p$4.apply(a))) {
            this.$outer.result$2.elem = false;
            throw Traversable$.MODULE$.breaks().m6break();
        }
    }
}
