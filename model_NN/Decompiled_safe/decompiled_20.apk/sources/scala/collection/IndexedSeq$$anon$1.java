package scala.collection;

import scala.collection.generic.GenTraversableFactory;
import scala.collection.mutable.Builder;
import scala.runtime.Nothing$;

public final class IndexedSeq$$anon$1 extends GenTraversableFactory<IndexedSeq>.GenericCanBuildFrom<Nothing$> {
    public IndexedSeq$$anon$1() {
        super(IndexedSeq$.MODULE$);
    }

    public final Builder<Nothing$, IndexedSeq<Nothing$>> apply() {
        return IndexedSeq$.MODULE$.newBuilder();
    }
}
