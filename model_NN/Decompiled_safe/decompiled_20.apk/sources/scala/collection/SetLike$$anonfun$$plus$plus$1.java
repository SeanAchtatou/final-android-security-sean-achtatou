package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction2;

public final class SetLike$$anonfun$$plus$plus$1 extends AbstractFunction2<This, A, This> implements Serializable {
    public SetLike$$anonfun$$plus$plus$1(SetLike<A, This> setLike) {
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: This
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final This apply(This r2, A r3) {
        /*
            r1 = this;
            scala.collection.Set r0 = r2.$plus(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.SetLike$$anonfun$$plus$plus$1.apply(scala.collection.Set, java.lang.Object):scala.collection.Set");
    }
}
