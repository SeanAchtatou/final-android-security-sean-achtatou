package scala.collection;

import scala.Function0;
import scala.Function1;
import scala.PartialFunction;
import scala.Tuple2;
import scala.collection.GenMapLike;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.generic.Subtractable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;

public abstract class AbstractMap<A, B> extends AbstractIterable<Tuple2<A, B>> implements Map<A, B> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Subtractable, scala.collection.GenMapLike, scala.PartialFunction, scala.collection.Map, scala.collection.MapLike, scala.Function1, scala.collection.AbstractMap] */
    public AbstractMap() {
        GenMapLike.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
    }

    public StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
        return MapLike.Cclass.addString(this, stringBuilder, str, str2, str3);
    }

    public <C> PartialFunction<A, C> andThen(Function1<B, C> function1) {
        return PartialFunction.Cclass.andThen(this, function1);
    }

    public B apply(A a) {
        return MapLike.Cclass.apply(this, a);
    }

    public <A1 extends A, B1> B1 applyOrElse(A1 a1, Function1<A1, B1> function1) {
        return PartialFunction.Cclass.applyOrElse(this, a1, function1);
    }

    public <A> Function1<A, B> compose(Function1<A, A> function1) {
        return Function1.Cclass.compose(this, function1);
    }

    public boolean contains(A a) {
        return MapLike.Cclass.contains(this, a);
    }

    /* renamed from: default  reason: not valid java name */
    public B m1default(A a) {
        return MapLike.Cclass.m3default(this, a);
    }

    public Map<A, B> empty() {
        return Map.Cclass.empty(this);
    }

    public boolean equals(Object obj) {
        return GenMapLike.Cclass.equals(this, obj);
    }

    public Map<A, B> filterNot(Function1<Tuple2<A, B>, Object> function1) {
        return MapLike.Cclass.filterNot(this, function1);
    }

    public <B1> B1 getOrElse(A a, Function0<B1> function0) {
        return MapLike.Cclass.getOrElse(this, a, function0);
    }

    public int hashCode() {
        return GenMapLike.Cclass.hashCode(this);
    }

    public boolean isDefinedAt(A a) {
        return MapLike.Cclass.isDefinedAt(this, a);
    }

    public boolean isEmpty() {
        return MapLike.Cclass.isEmpty(this);
    }

    public Builder<Tuple2<A, B>, Map<A, B>> newBuilder() {
        return MapLike.Cclass.newBuilder(this);
    }

    public Map<A, B> seq() {
        return Map.Cclass.seq(this);
    }

    public String stringPrefix() {
        return MapLike.Cclass.stringPrefix(this);
    }

    public /* bridge */ /* synthetic */ Traversable thisCollection() {
        return thisCollection();
    }

    public <C> Buffer<C> toBuffer() {
        return MapLike.Cclass.toBuffer(this);
    }

    public String toString() {
        return MapLike.Cclass.toString(this);
    }
}
