package scala.collection;

import scala.Function1;
import scala.Function2;
import scala.Predef$;
import scala.collection.mutable.Builder;
import scala.math.package$;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt$;
import scala.runtime.ScalaRunTime$;

public interface IndexedSeqOptimized<A, Repr> extends IndexedSeqLike<A, Repr> {

    /* renamed from: scala.collection.IndexedSeqOptimized$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(IndexedSeqOptimized indexedSeqOptimized) {
        }

        public static void copyToArray(IndexedSeqOptimized indexedSeqOptimized, Object obj, int i, int i2) {
            int i3 = 0;
            RichInt$ richInt$ = RichInt$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            RichInt$ richInt$2 = RichInt$.MODULE$;
            Predef$ predef$2 = Predef$.MODULE$;
            int min$extension = richInt$.min$extension(richInt$2.min$extension(indexedSeqOptimized.length(), i2), ScalaRunTime$.MODULE$.array_length(obj) - i);
            while (i3 < min$extension) {
                ScalaRunTime$.MODULE$.array_update(obj, i, indexedSeqOptimized.apply(i3));
                i3++;
                i++;
            }
        }

        public static Object drop(IndexedSeqOptimized indexedSeqOptimized, int i) {
            return indexedSeqOptimized.slice(i, indexedSeqOptimized.length());
        }

        public static boolean exists(IndexedSeqOptimized indexedSeqOptimized, Function1 function1) {
            return indexedSeqOptimized.prefixLength(new IndexedSeqOptimized$$anonfun$exists$1(indexedSeqOptimized, function1)) != indexedSeqOptimized.length();
        }

        public static Object foldLeft(IndexedSeqOptimized indexedSeqOptimized, Object obj, Function2 function2) {
            return foldl(indexedSeqOptimized, 0, indexedSeqOptimized.length(), obj, function2);
        }

        private static Object foldl(IndexedSeqOptimized indexedSeqOptimized, int i, int i2, Object obj, Function2 function2) {
            while (i != i2) {
                obj = function2.apply(obj, indexedSeqOptimized.apply(i));
                i++;
            }
            return obj;
        }

        public static boolean forall(IndexedSeqOptimized indexedSeqOptimized, Function1 function1) {
            return indexedSeqOptimized.prefixLength(new IndexedSeqOptimized$$anonfun$forall$1(indexedSeqOptimized, function1)) == indexedSeqOptimized.length();
        }

        public static void foreach(IndexedSeqOptimized indexedSeqOptimized, Function1 function1) {
            int length = indexedSeqOptimized.length();
            for (int i = 0; i < length; i++) {
                function1.apply(indexedSeqOptimized.apply(i));
            }
        }

        public static Object head(IndexedSeqOptimized indexedSeqOptimized) {
            return indexedSeqOptimized.isEmpty() ? indexedSeqOptimized.scala$collection$IndexedSeqOptimized$$super$head() : indexedSeqOptimized.apply(0);
        }

        public static boolean isEmpty(IndexedSeqOptimized indexedSeqOptimized) {
            return indexedSeqOptimized.length() == 0;
        }

        public static Object last(IndexedSeqOptimized indexedSeqOptimized) {
            return indexedSeqOptimized.length() > 0 ? indexedSeqOptimized.apply(indexedSeqOptimized.length() - 1) : indexedSeqOptimized.scala$collection$IndexedSeqOptimized$$super$last();
        }

        public static int lengthCompare(IndexedSeqOptimized indexedSeqOptimized, int i) {
            return indexedSeqOptimized.length() - i;
        }

        public static Object reverse(IndexedSeqOptimized indexedSeqOptimized) {
            Builder newBuilder = indexedSeqOptimized.newBuilder();
            newBuilder.sizeHint(indexedSeqOptimized.length());
            int length = indexedSeqOptimized.length();
            while (length > 0) {
                length--;
                newBuilder.$plus$eq(indexedSeqOptimized.apply(length));
            }
            return newBuilder.result();
        }

        public static Iterator reverseIterator(IndexedSeqOptimized indexedSeqOptimized) {
            return new IndexedSeqOptimized$$anon$1(indexedSeqOptimized);
        }

        public static boolean sameElements(IndexedSeqOptimized indexedSeqOptimized, GenIterable genIterable) {
            if (!(genIterable instanceof IndexedSeq)) {
                return indexedSeqOptimized.scala$collection$IndexedSeqOptimized$$super$sameElements(genIterable);
            }
            IndexedSeq indexedSeq = (IndexedSeq) genIterable;
            int length = indexedSeqOptimized.length();
            if (length == indexedSeq.length()) {
                int i = 0;
                while (i < length) {
                    Object apply = indexedSeqOptimized.apply(i);
                    Object apply2 = indexedSeq.apply(i);
                    if (!(apply == apply2 ? true : apply == null ? false : apply instanceof Number ? BoxesRunTime.equalsNumObject((Number) apply, apply2) : apply instanceof Character ? BoxesRunTime.equalsCharObject((Character) apply, apply2) : apply.equals(apply2))) {
                        break;
                    }
                    i++;
                }
                if (i == length) {
                    return true;
                }
            }
            return false;
        }

        public static int segmentLength(IndexedSeqOptimized indexedSeqOptimized, Function1 function1, int i) {
            int length = indexedSeqOptimized.length();
            int i2 = i;
            while (i2 < length && BoxesRunTime.unboxToBoolean(function1.apply(indexedSeqOptimized.apply(i2)))) {
                i2++;
            }
            return i2 - i;
        }

        public static Object slice(IndexedSeqOptimized indexedSeqOptimized, int i, int i2) {
            int max = package$.MODULE$.max(i, 0);
            int min = package$.MODULE$.min(package$.MODULE$.max(i2, 0), indexedSeqOptimized.length());
            int max2 = package$.MODULE$.max(min - max, 0);
            Builder newBuilder = indexedSeqOptimized.newBuilder();
            newBuilder.sizeHint(max2);
            while (max < min) {
                newBuilder.$plus$eq(indexedSeqOptimized.apply(max));
                max++;
            }
            return newBuilder.result();
        }

        public static Object tail(IndexedSeqOptimized indexedSeqOptimized) {
            return indexedSeqOptimized.isEmpty() ? indexedSeqOptimized.scala$collection$IndexedSeqOptimized$$super$tail() : indexedSeqOptimized.slice(1, indexedSeqOptimized.length());
        }

        public static Object take(IndexedSeqOptimized indexedSeqOptimized, int i) {
            return indexedSeqOptimized.slice(0, i);
        }
    }

    boolean isEmpty();

    A scala$collection$IndexedSeqOptimized$$super$head();

    A scala$collection$IndexedSeqOptimized$$super$last();

    <B> boolean scala$collection$IndexedSeqOptimized$$super$sameElements(GenIterable<B> genIterable);

    Repr scala$collection$IndexedSeqOptimized$$super$tail();

    Repr slice(int i, int i2);
}
