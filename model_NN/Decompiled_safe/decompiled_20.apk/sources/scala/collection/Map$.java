package scala.collection;

import scala.collection.generic.MapFactory;
import scala.collection.immutable.Map;

public final class Map$ extends MapFactory<Map> {
    public static final Map$ MODULE$ = null;

    static {
        new Map$();
    }

    private Map$() {
        MODULE$ = this;
    }

    public final <A, B> Map<A, B> empty() {
        return scala.collection.immutable.Map$.MODULE$.empty();
    }
}
