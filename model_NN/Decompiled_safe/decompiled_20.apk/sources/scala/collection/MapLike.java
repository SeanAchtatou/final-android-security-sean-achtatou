package scala.collection;

import java.util.NoSuchElementException;
import scala.Function0;
import scala.Function1;
import scala.MatchError;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.Some;
import scala.Tuple2;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.TraversableLike;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.MapBuilder;
import scala.collection.mutable.StringBuilder;
import scala.collection.parallel.ParMap;
import scala.runtime.ObjectRef;

public interface MapLike<A, B, This extends MapLike<A, B, This> & Map<A, B>> extends PartialFunction<A, B>, GenMapLike<A, B, This>, IterableLike<Tuple2<A, B>, This>, Parallelizable<Tuple2<A, B>, ParMap<A, B>> {

    /* renamed from: scala.collection.MapLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(MapLike mapLike) {
        }

        public static StringBuilder addString(MapLike mapLike, StringBuilder stringBuilder, String str, String str2, String str3) {
            return mapLike.iterator().map(new MapLike$$anonfun$addString$1(mapLike)).addString(stringBuilder, str, str2, str3);
        }

        public static Object apply(MapLike mapLike, Object obj) {
            Option option = mapLike.get(obj);
            None$ none$ = None$.MODULE$;
            if (none$ != null ? none$.equals(option) : option == null) {
                return mapLike.m2default(obj);
            }
            if (option instanceof Some) {
                return ((Some) option).x();
            }
            throw new MatchError(option);
        }

        public static boolean contains(MapLike mapLike, Object obj) {
            return mapLike.get(obj).isDefined();
        }

        /* renamed from: default  reason: not valid java name */
        public static Object m3default(MapLike mapLike, Object obj) {
            throw new NoSuchElementException(new StringBuilder().append((Object) "key not found: ").append(obj).toString());
        }

        public static Map filterNot(MapLike mapLike, Function1 function1) {
            ObjectRef objectRef = new ObjectRef((Map) mapLike.repr());
            mapLike.foreach(new MapLike$$anonfun$filterNot$1(mapLike, objectRef, function1));
            return (Map) objectRef.elem;
        }

        public static Object getOrElse(MapLike mapLike, Object obj, Function0 function0) {
            Option option = mapLike.get(obj);
            if (option instanceof Some) {
                return ((Some) option).x();
            }
            None$ none$ = None$.MODULE$;
            if (none$ != null ? none$.equals(option) : option == null) {
                return function0.apply();
            }
            throw new MatchError(option);
        }

        public static boolean isDefinedAt(MapLike mapLike, Object obj) {
            return mapLike.contains(obj);
        }

        public static boolean isEmpty(MapLike mapLike) {
            return mapLike.size() == 0;
        }

        public static Builder newBuilder(MapLike mapLike) {
            return new MapBuilder(mapLike.empty());
        }

        public static String stringPrefix(MapLike mapLike) {
            return "Map";
        }

        public static Buffer toBuffer(MapLike mapLike) {
            ArrayBuffer arrayBuffer = new ArrayBuffer(mapLike.size());
            mapLike.copyToBuffer(arrayBuffer);
            return arrayBuffer;
        }

        public static String toString(MapLike mapLike) {
            return TraversableLike.Cclass.toString(mapLike);
        }
    }

    This $minus(Object obj);

    B apply(A a);

    boolean contains(A a);

    /* renamed from: default  reason: not valid java name */
    B m2default(A a);

    This empty();

    This filterNot(Function1 function1);

    Option<B> get(A a);

    <B1> B1 getOrElse(A a, Function0<B1> function0);

    boolean isEmpty();

    Iterator<Tuple2<A, B>> iterator();
}
