package scala.collection;

import java.util.NoSuchElementException;
import scala.Function1;
import scala.Function2;
import scala.collection.LinearSeqOptimized;
import scala.runtime.BoxesRunTime;

public interface LinearSeqOptimized<A, Repr extends LinearSeqOptimized<A, Repr>> extends LinearSeqLike<A, Repr> {

    /* renamed from: scala.collection.LinearSeqOptimized$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(LinearSeqOptimized linearSeqOptimized) {
        }

        public static Object apply(LinearSeqOptimized linearSeqOptimized, int i) {
            LinearSeqOptimized drop = linearSeqOptimized.drop(i);
            if (i >= 0 && !drop.isEmpty()) {
                return drop.head();
            }
            throw new IndexOutOfBoundsException(String.valueOf(BoxesRunTime.boxToInteger(i)));
        }

        public static boolean exists(LinearSeqOptimized linearSeqOptimized, Function1 function1) {
            while (!linearSeqOptimized.isEmpty()) {
                if (BoxesRunTime.unboxToBoolean(function1.apply(linearSeqOptimized.head()))) {
                    return true;
                }
                linearSeqOptimized = (LinearSeqOptimized) linearSeqOptimized.tail();
            }
            return false;
        }

        public static Object foldLeft(LinearSeqOptimized linearSeqOptimized, Object obj, Function2 function2) {
            while (!linearSeqOptimized.isEmpty()) {
                obj = function2.apply(obj, linearSeqOptimized.head());
                linearSeqOptimized = (LinearSeqOptimized) linearSeqOptimized.tail();
            }
            return obj;
        }

        public static boolean forall(LinearSeqOptimized linearSeqOptimized, Function1 function1) {
            while (!linearSeqOptimized.isEmpty()) {
                if (!BoxesRunTime.unboxToBoolean(function1.apply(linearSeqOptimized.head()))) {
                    return false;
                }
                linearSeqOptimized = (LinearSeqOptimized) linearSeqOptimized.tail();
            }
            return true;
        }

        public static boolean isDefinedAt(LinearSeqOptimized linearSeqOptimized, int i) {
            return i >= 0 && linearSeqOptimized.lengthCompare(i) > 0;
        }

        public static Object last(LinearSeqOptimized linearSeqOptimized) {
            if (linearSeqOptimized.isEmpty()) {
                throw new NoSuchElementException();
            }
            LinearSeqOptimized linearSeqOptimized2 = (LinearSeqOptimized) linearSeqOptimized.tail();
            while (true) {
                LinearSeqOptimized linearSeqOptimized3 = linearSeqOptimized2;
                if (linearSeqOptimized3.isEmpty()) {
                    return linearSeqOptimized.head();
                }
                linearSeqOptimized2 = (LinearSeqOptimized) linearSeqOptimized3.tail();
                linearSeqOptimized = linearSeqOptimized3;
            }
        }

        public static int length(LinearSeqOptimized linearSeqOptimized) {
            int i = 0;
            while (!linearSeqOptimized.isEmpty()) {
                linearSeqOptimized = (LinearSeqOptimized) linearSeqOptimized.tail();
                i++;
            }
            return i;
        }

        public static int lengthCompare(LinearSeqOptimized linearSeqOptimized, int i) {
            if (i < 0) {
                return 1;
            }
            return loop$1(linearSeqOptimized, 0, linearSeqOptimized, i);
        }

        private static final int loop$1(LinearSeqOptimized linearSeqOptimized, int i, LinearSeqOptimized linearSeqOptimized2, int i2) {
            while (i != i2) {
                if (linearSeqOptimized2.isEmpty()) {
                    return -1;
                }
                i++;
                linearSeqOptimized2 = (LinearSeqOptimized) linearSeqOptimized2.tail();
            }
            return linearSeqOptimized2.isEmpty() ? 0 : 1;
        }

        public static boolean sameElements(LinearSeqOptimized linearSeqOptimized, GenIterable genIterable) {
            if (!(genIterable instanceof LinearSeq)) {
                return linearSeqOptimized.scala$collection$LinearSeqOptimized$$super$sameElements(genIterable);
            }
            LinearSeq linearSeq = (LinearSeq) genIterable;
            while (!linearSeqOptimized.isEmpty() && !linearSeq.isEmpty()) {
                Object head = linearSeqOptimized.head();
                Object head2 = linearSeq.head();
                if (!(head == head2 ? true : head == null ? false : head instanceof Number ? BoxesRunTime.equalsNumObject((Number) head, head2) : head instanceof Character ? BoxesRunTime.equalsCharObject((Character) head, head2) : head.equals(head2))) {
                    break;
                }
                linearSeq = (LinearSeq) linearSeq.tail();
                linearSeqOptimized = (LinearSeqOptimized) linearSeqOptimized.tail();
            }
            return linearSeqOptimized.isEmpty() && linearSeq.isEmpty();
        }

        public static int segmentLength(LinearSeqOptimized linearSeqOptimized, Function1 function1, int i) {
            int i2 = 0;
            LinearSeqOptimized drop = linearSeqOptimized.drop(i);
            while (!drop.isEmpty() && BoxesRunTime.unboxToBoolean(function1.apply(drop.head()))) {
                drop = (LinearSeqOptimized) drop.tail();
                i2++;
            }
            return i2;
        }
    }

    A apply(int i);

    Repr drop(int i);

    A head();

    boolean isDefinedAt(int i);

    boolean isEmpty();

    A last();

    int lengthCompare(int i);

    <B> boolean scala$collection$LinearSeqOptimized$$super$sameElements(GenIterable<B> genIterable);
}
