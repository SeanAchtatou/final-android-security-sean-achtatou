package scala.collection;

import scala.Function1;
import scala.Function2;
import scala.collection.TraversableLike;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.Builder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

public interface SeqLike<A, Repr> extends GenSeqLike<A, Repr>, IterableLike<A, Repr> {

    /* renamed from: scala.collection.SeqLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(SeqLike seqLike) {
        }

        public static boolean corresponds(SeqLike seqLike, GenSeq genSeq, Function2 function2) {
            Iterator it = seqLike.iterator();
            Iterator it2 = genSeq.iterator();
            while (it.hasNext() && it2.hasNext()) {
                if (!BoxesRunTime.unboxToBoolean(function2.apply(it.next(), it2.next()))) {
                    return false;
                }
            }
            return !it.hasNext() && !it2.hasNext();
        }

        public static boolean isEmpty(SeqLike seqLike) {
            return seqLike.lengthCompare(0) == 0;
        }

        public static int lengthCompare(SeqLike seqLike, int i) {
            if (i < 0) {
                return 1;
            }
            Iterator it = seqLike.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                if (i2 == i) {
                    return it.hasNext() ? 1 : 0;
                }
                it.next();
                i2++;
            }
            return i2 - i;
        }

        public static Object reverse(SeqLike seqLike) {
            ObjectRef objectRef = new ObjectRef(Nil$.MODULE$);
            seqLike.foreach(new SeqLike$$anonfun$reverse$1(seqLike, objectRef));
            Builder newBuilder = seqLike.newBuilder();
            newBuilder.sizeHint(seqLike);
            T t = objectRef.elem;
            while (true) {
                List list = (List) t;
                if (list.isEmpty()) {
                    return newBuilder.result();
                }
                newBuilder.$plus$eq(list.head());
                t = list.tail();
            }
        }

        public static Iterator reverseIterator(SeqLike seqLike) {
            return seqLike.toCollection(seqLike.reverse()).iterator();
        }

        public static int segmentLength(SeqLike seqLike, Function1 function1, int i) {
            int i2 = 0;
            Iterator drop = seqLike.iterator().drop(i);
            while (drop.hasNext() && BoxesRunTime.unboxToBoolean(function1.apply(drop.next()))) {
                i2++;
            }
            return i2;
        }

        public static int size(SeqLike seqLike) {
            return seqLike.length();
        }

        public static Seq thisCollection(SeqLike seqLike) {
            return (Seq) seqLike;
        }

        public static Seq toCollection(SeqLike seqLike, Object obj) {
            return (Seq) obj;
        }

        public static String toString(SeqLike seqLike) {
            return TraversableLike.Cclass.toString(seqLike);
        }
    }

    A apply(int i);

    boolean isEmpty();

    int length();

    int lengthCompare(int i);

    Repr reverse();

    Iterator<A> reverseIterator();

    int size();

    Seq<A> thisCollection();

    Seq<A> toCollection(Object obj);
}
