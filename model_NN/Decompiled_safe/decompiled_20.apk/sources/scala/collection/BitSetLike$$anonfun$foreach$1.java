package scala.collection;

import scala.Function1;
import scala.Predef$;
import scala.Serializable;
import scala.collection.immutable.Range;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt$;

public final class BitSetLike$$anonfun$foreach$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public final /* synthetic */ BitSetLike $outer;
    public final Function1 f$1;

    public BitSetLike$$anonfun$foreach$1(BitSetLike bitSetLike, BitSetLike<This> bitSetLike2) {
        if (bitSetLike == null) {
            throw new NullPointerException();
        }
        this.$outer = bitSetLike;
        this.f$1 = bitSetLike2;
    }

    public final /* synthetic */ Object apply(Object obj) {
        apply$mcVI$sp(BoxesRunTime.unboxToInt(obj));
        return BoxedUnit.UNIT;
    }

    public final void apply(int i) {
        apply$mcVI$sp(i);
    }

    public final void apply$mcVI$sp(int i) {
        long word = this.$outer.word(i);
        RichInt$ richInt$ = RichInt$.MODULE$;
        Predef$ predef$ = Predef$.MODULE$;
        Range until$extension0 = richInt$.until$extension0(BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength() * i, (i + 1) * BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength());
        BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1 bitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1 = new BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1(this, word);
        if (until$extension0.validateRangeBoundaries(bitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1)) {
            int terminalElement = until$extension0.terminalElement();
            int step = until$extension0.step();
            for (int start = until$extension0.start(); start != terminalElement; start += step) {
                bitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1.apply(start);
            }
        }
    }
}
