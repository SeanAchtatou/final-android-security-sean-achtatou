package scala.collection;

import scala.Function1;
import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class IndexedSeqOptimized$$anonfun$forall$1 extends AbstractFunction1<A, Object> implements Serializable {
    private final Function1 p$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.IndexedSeqOptimized<A, Repr>, scala.Function1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IndexedSeqOptimized$$anonfun$forall$1(scala.collection.IndexedSeqOptimized r1, scala.collection.IndexedSeqOptimized<A, Repr> r2) {
        /*
            r0 = this;
            r0.p$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.IndexedSeqOptimized$$anonfun$forall$1.<init>(scala.collection.IndexedSeqOptimized, scala.Function1):void");
    }

    public final boolean apply(A a) {
        return BoxesRunTime.unboxToBoolean(this.p$1.apply(a));
    }
}
