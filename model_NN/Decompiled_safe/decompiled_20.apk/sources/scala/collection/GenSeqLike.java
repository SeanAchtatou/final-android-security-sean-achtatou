package scala.collection;

import scala.Equals;
import scala.Function1;
import scala.Function2;
import scala.util.hashing.MurmurHash3$;

public interface GenSeqLike<A, Repr> extends Equals, GenIterableLike<A, Repr> {

    /* renamed from: scala.collection.GenSeqLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(GenSeqLike genSeqLike) {
        }

        public static boolean equals(GenSeqLike genSeqLike, Object obj) {
            if (!(obj instanceof GenSeq)) {
                return false;
            }
            GenSeq genSeq = (GenSeq) obj;
            return genSeq.canEqual(genSeqLike) && genSeqLike.sameElements(genSeq);
        }

        public static int hashCode(GenSeqLike genSeqLike) {
            return MurmurHash3$.MODULE$.seqHash(genSeqLike.seq());
        }

        public static boolean isDefinedAt(GenSeqLike genSeqLike, int i) {
            return i >= 0 && i < genSeqLike.length();
        }

        public static int prefixLength(GenSeqLike genSeqLike, Function1 function1) {
            return genSeqLike.segmentLength(function1, 0);
        }
    }

    A apply(int i);

    <B> boolean corresponds(GenSeq<B> genSeq, Function2<A, B, Object> function2);

    boolean equals(Object obj);

    boolean isDefinedAt(int i);

    int length();

    int prefixLength(Function1<A, Object> function1);

    int segmentLength(Function1<A, Object> function1, int i);

    Seq<A> seq();
}
