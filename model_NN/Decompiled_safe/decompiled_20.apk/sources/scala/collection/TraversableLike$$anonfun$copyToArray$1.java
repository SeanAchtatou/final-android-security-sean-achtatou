package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;
import scala.runtime.IntRef;

public final class TraversableLike$$anonfun$copyToArray$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public final /* synthetic */ TraversableLike $outer;
    public final int end$1;
    public final IntRef i$3;
    public final Object xs$1;

    public TraversableLike$$anonfun$copyToArray$1(TraversableLike traversableLike, IntRef intRef, int i, Object obj) {
        if (traversableLike == null) {
            throw new NullPointerException();
        }
        this.$outer = traversableLike;
        this.i$3 = intRef;
        this.end$1 = i;
        this.xs$1 = obj;
    }

    public final void apply() {
        this.$outer.foreach(new TraversableLike$$anonfun$copyToArray$1$$anonfun$apply$mcV$sp$8(this));
    }

    public final void apply$mcV$sp() {
        this.$outer.foreach(new TraversableLike$$anonfun$copyToArray$1$$anonfun$apply$mcV$sp$8(this));
    }
}
