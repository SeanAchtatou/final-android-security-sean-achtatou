package scala.collection;

import scala.PartialFunction;
import scala.collection.generic.GenericCompanion;

public interface Seq<A> extends PartialFunction<Object, A>, GenSeq<A>, Iterable<A>, SeqLike<A, Seq<A>> {

    /* renamed from: scala.collection.Seq$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Seq seq) {
        }

        public static GenericCompanion companion(Seq seq) {
            return Seq$.MODULE$;
        }

        public static Seq seq(Seq seq) {
            return seq;
        }
    }

    Seq<A> seq();
}
