package scala.collection;

import scala.Predef$;
import scala.Serializable;
import scala.collection.BufferedIterator;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.Buffer;
import scala.runtime.BoxedUnit;
import scala.runtime.RichInt$;
import scala.util.hashing.MurmurHash3$;

public interface IndexedSeqLike<A, Repr> extends SeqLike<A, Repr> {

    public class Elements extends AbstractIterator<A> implements Serializable, BufferedIterator<A> {
        public final /* synthetic */ IndexedSeqLike $outer;
        private final int end;
        private int index;
        private final int start;

        public Elements(IndexedSeqLike<A, Repr> indexedSeqLike, int i, int i2) {
            this.start = i;
            this.end = i2;
            if (indexedSeqLike == null) {
                throw new NullPointerException();
            }
            this.$outer = indexedSeqLike;
            BufferedIterator.Cclass.$init$(this);
            this.index = i;
        }

        private int available() {
            RichInt$ richInt$ = RichInt$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            return richInt$.max$extension(this.end - index(), 0);
        }

        private int index() {
            return this.index;
        }

        private void index_$eq(int i) {
            this.index = i;
        }

        public Iterator<A> drop(int i) {
            return i <= 0 ? new Elements(scala$collection$IndexedSeqLike$Elements$$$outer(), index(), this.end) : index() + i >= this.end ? new Elements(scala$collection$IndexedSeqLike$Elements$$$outer(), this.end, this.end) : new Elements(scala$collection$IndexedSeqLike$Elements$$$outer(), index() + i, this.end);
        }

        public boolean hasNext() {
            return index() < this.end;
        }

        public A next() {
            if (index() >= this.end) {
                Iterator$.MODULE$.empty().next();
            } else {
                BoxedUnit boxedUnit = BoxedUnit.UNIT;
            }
            A apply = scala$collection$IndexedSeqLike$Elements$$$outer().apply(index());
            this.index = index() + 1;
            return apply;
        }

        public /* synthetic */ IndexedSeqLike scala$collection$IndexedSeqLike$Elements$$$outer() {
            return this.$outer;
        }

        public Iterator<A> slice(int i, int i2) {
            return take(i2).drop(i);
        }

        public Iterator<A> take(int i) {
            return i <= 0 ? Iterator$.MODULE$.empty() : i <= available() ? new Elements(scala$collection$IndexedSeqLike$Elements$$$outer(), index(), index() + i) : new Elements(scala$collection$IndexedSeqLike$Elements$$$outer(), index(), this.end);
        }
    }

    /* renamed from: scala.collection.IndexedSeqLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(IndexedSeqLike indexedSeqLike) {
        }

        public static int hashCode(IndexedSeqLike indexedSeqLike) {
            return MurmurHash3$.MODULE$.seqHash(indexedSeqLike.seq());
        }

        public static Iterator iterator(IndexedSeqLike indexedSeqLike) {
            return new Elements(indexedSeqLike, 0, indexedSeqLike.length());
        }

        public static IndexedSeq thisCollection(IndexedSeqLike indexedSeqLike) {
            return (IndexedSeq) indexedSeqLike;
        }

        public static Buffer toBuffer(IndexedSeqLike indexedSeqLike) {
            ArrayBuffer arrayBuffer = new ArrayBuffer(indexedSeqLike.size());
            indexedSeqLike.copyToBuffer(arrayBuffer);
            return arrayBuffer;
        }

        public static IndexedSeq toCollection(IndexedSeqLike indexedSeqLike, Object obj) {
            return (IndexedSeq) obj;
        }
    }

    IndexedSeq<A> seq();

    IndexedSeq<A> thisCollection();

    IndexedSeq<A> toCollection(Object obj);
}
