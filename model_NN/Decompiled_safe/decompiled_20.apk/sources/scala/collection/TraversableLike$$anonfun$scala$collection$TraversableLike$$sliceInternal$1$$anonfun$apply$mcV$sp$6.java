package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

public final class TraversableLike$$anonfun$scala$collection$TraversableLike$$sliceInternal$1$$anonfun$apply$mcV$sp$6 extends AbstractFunction1<A, BoxedUnit> implements Serializable {
    private final /* synthetic */ TraversableLike$$anonfun$scala$collection$TraversableLike$$sliceInternal$1 $outer;

    public TraversableLike$$anonfun$scala$collection$TraversableLike$$sliceInternal$1$$anonfun$apply$mcV$sp$6(TraversableLike<A, Repr>.1 r2) {
        if (r2 == null) {
            throw new NullPointerException();
        }
        this.$outer = r2;
    }

    public final void apply(A a) {
        if (this.$outer.i$1.elem >= this.$outer.from$1) {
            this.$outer.b$9.$plus$eq((Object) a);
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        this.$outer.i$1.elem++;
        if (this.$outer.i$1.elem >= this.$outer.until$1) {
            throw Traversable$.MODULE$.breaks().m6break();
        }
    }
}
