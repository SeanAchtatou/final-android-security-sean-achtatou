package scala.collection;

import scala.Array$;
import scala.Predef$;

public final class BitSetLike$ {
    public static final BitSetLike$ MODULE$ = null;
    private final int LogWL = 6;
    private final int scala$collection$BitSetLike$$WordLength = 64;

    static {
        new BitSetLike$();
    }

    private BitSetLike$() {
        MODULE$ = this;
    }

    public final int LogWL() {
        return this.LogWL;
    }

    public final int scala$collection$BitSetLike$$WordLength() {
        return this.scala$collection$BitSetLike$$WordLength;
    }

    public final long[] updateArray(long[] jArr, int i, long j) {
        boolean z = false;
        int length = jArr.length;
        while (length > 0 && (jArr[length - 1] == 0 || (j == 0 && i == length - 1))) {
            length--;
        }
        int i2 = (i < length || j == 0) ? length : i + 1;
        long[] jArr2 = new long[i2];
        Array$.MODULE$.copy(jArr, 0, jArr2, 0, length);
        if (i < i2) {
            jArr2[i] = j;
        } else {
            Predef$ predef$ = Predef$.MODULE$;
            if (j == 0) {
                z = true;
            }
            predef$.m0assert(z);
        }
        return jArr2;
    }
}
