package scala.collection;

import scala.Function1;
import scala.collection.GenIterable;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.immutable.Stream;

public abstract class AbstractIterable<A> extends AbstractTraversable<A> implements Iterable<A> {
    public AbstractIterable() {
        GenIterable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
    }

    public boolean canEqual(Object obj) {
        return IterableLike.Cclass.canEqual(this, obj);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public <B> void copyToArray(Object obj, int i, int i2) {
        IterableLike.Cclass.copyToArray(this, obj, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Iterable<A>] */
    public Iterable<A> drop(int i) {
        return IterableLike.Cclass.drop(this, i);
    }

    public boolean exists(Function1<A, Object> function1) {
        return IterableLike.Cclass.exists(this, function1);
    }

    public boolean forall(Function1<A, Object> function1) {
        return IterableLike.Cclass.forall(this, function1);
    }

    public <U> void foreach(Function1<A, U> function1) {
        IterableLike.Cclass.foreach(this, function1);
    }

    public A head() {
        return IterableLike.Cclass.head(this);
    }

    public boolean isEmpty() {
        return IterableLike.Cclass.isEmpty(this);
    }

    public <B> boolean sameElements(GenIterable<B> genIterable) {
        return IterableLike.Cclass.sameElements(this, genIterable);
    }

    public Iterable<A> seq() {
        return Iterable.Cclass.seq(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Iterable<A>] */
    public Iterable<A> slice(int i, int i2) {
        return IterableLike.Cclass.slice(this, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Iterable<A>] */
    public Iterable<A> take(int i) {
        return IterableLike.Cclass.take(this, i);
    }

    public Iterable<A> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public Stream<A> toStream() {
        return IterableLike.Cclass.toStream(this);
    }
}
