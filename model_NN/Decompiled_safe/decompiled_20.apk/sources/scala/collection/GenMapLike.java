package scala.collection;

import scala.Equals;
import scala.Function0;
import scala.Option;
import scala.Predef$;
import scala.Tuple2;
import scala.util.hashing.MurmurHash3$;

public interface GenMapLike<A, B, Repr> extends Equals, GenIterableLike<Tuple2<A, B>, Repr> {

    /* renamed from: scala.collection.GenMapLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(GenMapLike genMapLike) {
        }

        public static boolean equals(GenMapLike genMapLike, Object obj) {
            if (!(obj instanceof GenMap)) {
                return false;
            }
            GenMap genMap = (GenMap) obj;
            return genMapLike == genMap || (genMap.canEqual(genMapLike) && genMapLike.size() == genMap.size() && liftedTree1$1(genMapLike, genMap));
        }

        public static int hashCode(GenMapLike genMapLike) {
            return MurmurHash3$.MODULE$.mapHash(genMapLike.seq());
        }

        private static final boolean liftedTree1$1(GenMapLike genMapLike, GenMap genMap) {
            try {
                return genMapLike.forall(new GenMapLike$$anonfun$liftedTree1$1$1(genMapLike, genMap));
            } catch (ClassCastException e) {
                Predef$.MODULE$.println("class cast ");
                return false;
            }
        }
    }

    <B1> GenMap<A, B1> $plus(Tuple2 tuple2);

    B apply(A a);

    boolean contains(A a);

    Option<B> get(A a);

    <B1> B1 getOrElse(A a, Function0<B1> function0);

    boolean isDefinedAt(A a);

    Map<A, B> seq();
}
