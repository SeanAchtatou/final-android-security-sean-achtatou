package scala.collection;

public final class Iterator$$anon$10 extends AbstractIterator<A> {
    private final /* synthetic */ Iterator $outer;
    private int remaining;

    public Iterator$$anon$10(Iterator iterator, int i, int i2) {
        if (iterator == null) {
            throw new NullPointerException();
        }
        this.$outer = iterator;
        this.remaining = i2 - i;
    }

    private int remaining() {
        return this.remaining;
    }

    private void remaining_$eq(int i) {
        this.remaining = i;
    }

    public final boolean hasNext() {
        return remaining() > 0 && this.$outer.hasNext();
    }

    public final A next() {
        if (remaining() <= 0) {
            return Iterator$.MODULE$.empty().next();
        }
        this.remaining = remaining() - 1;
        return this.$outer.next();
    }
}
