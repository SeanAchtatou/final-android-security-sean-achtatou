package scala.collection;

public final class IndexedSeqOptimized$$anon$1 extends AbstractIterator<A> {
    private final /* synthetic */ IndexedSeqOptimized $outer;
    private int i;

    public IndexedSeqOptimized$$anon$1(IndexedSeqOptimized<A, Repr> indexedSeqOptimized) {
        if (indexedSeqOptimized == null) {
            throw new NullPointerException();
        }
        this.$outer = indexedSeqOptimized;
        this.i = indexedSeqOptimized.length();
    }

    private int i() {
        return this.i;
    }

    private void i_$eq(int i2) {
        this.i = i2;
    }

    public final boolean hasNext() {
        return i() > 0;
    }

    public final A next() {
        if (i() <= 0) {
            return Iterator$.MODULE$.empty().next();
        }
        this.i = i() - 1;
        return this.$outer.apply(i());
    }
}
