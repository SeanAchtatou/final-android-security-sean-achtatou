package scala.collection.convert;

import java.util.Properties;
import scala.collection.convert.Wrappers;
import scala.collection.mutable.Map;

public interface WrapAsScala {

    /* renamed from: scala.collection.convert.WrapAsScala$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(WrapAsScala wrapAsScala) {
        }

        public static Map propertiesAsScalaMap(WrapAsScala wrapAsScala, Properties properties) {
            return new Wrappers.JPropertiesWrapper(Wrappers$.MODULE$, properties);
        }
    }

    Map<String, String> propertiesAsScalaMap(Properties properties);
}
