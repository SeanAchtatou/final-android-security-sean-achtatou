package scala.collection.convert;

import java.util.Iterator;
import java.util.Map;
import scala.Tuple2;
import scala.collection.AbstractIterator;
import scala.collection.convert.Wrappers;

public final class Wrappers$JPropertiesWrapper$$anon$3 extends AbstractIterator<Tuple2<String, String>> {
    private final Iterator<Map.Entry<Object, Object>> ui;

    public Wrappers$JPropertiesWrapper$$anon$3(Wrappers.JPropertiesWrapper jPropertiesWrapper) {
        this.ui = jPropertiesWrapper.underlying().entrySet().iterator();
    }

    private Iterator<Map.Entry<Object, Object>> ui() {
        return this.ui;
    }

    public final boolean hasNext() {
        return ui().hasNext();
    }

    public final Tuple2<String, String> next() {
        Map.Entry next = ui().next();
        return new Tuple2<>((String) next.getKey(), (String) next.getValue());
    }
}
