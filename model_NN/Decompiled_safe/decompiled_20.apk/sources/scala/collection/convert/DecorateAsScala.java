package scala.collection.convert;

import java.util.Properties;
import scala.collection.convert.Decorators;
import scala.collection.mutable.Map;

public interface DecorateAsScala {

    /* renamed from: scala.collection.convert.DecorateAsScala$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(DecorateAsScala decorateAsScala) {
        }

        public static Decorators.AsScala propertiesAsScalaMapConverter(DecorateAsScala decorateAsScala, Properties properties) {
            return new Decorators.AsScala(Decorators$.MODULE$, new DecorateAsScala$$anonfun$propertiesAsScalaMapConverter$1(decorateAsScala, properties));
        }
    }

    Decorators.AsScala<Map<String, String>> propertiesAsScalaMapConverter(Properties properties);
}
