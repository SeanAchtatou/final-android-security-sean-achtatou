package scala.collection.convert;

import java.util.Properties;
import scala.collection.convert.WrapAsScala;
import scala.collection.mutable.Map;

public final class WrapAsScala$ implements WrapAsScala {
    public static final WrapAsScala$ MODULE$ = null;

    static {
        new WrapAsScala$();
    }

    private WrapAsScala$() {
        MODULE$ = this;
        LowPriorityWrapAsScala$class.$init$(this);
        WrapAsScala.Cclass.$init$(this);
    }

    public final Map<String, String> propertiesAsScalaMap(Properties properties) {
        return WrapAsScala.Cclass.propertiesAsScalaMap(this, properties);
    }
}
