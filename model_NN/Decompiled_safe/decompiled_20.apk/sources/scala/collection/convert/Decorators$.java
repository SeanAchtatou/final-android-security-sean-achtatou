package scala.collection.convert;

import scala.collection.convert.Decorators;

public final class Decorators$ implements Decorators {
    public static final Decorators$ MODULE$ = null;

    static {
        new Decorators$();
    }

    private Decorators$() {
        MODULE$ = this;
        Decorators.Cclass.$init$(this);
    }
}
