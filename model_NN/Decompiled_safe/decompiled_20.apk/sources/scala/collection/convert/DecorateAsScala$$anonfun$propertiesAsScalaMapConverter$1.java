package scala.collection.convert;

import java.util.Properties;
import scala.Serializable;
import scala.collection.mutable.Map;
import scala.runtime.AbstractFunction0;

public final class DecorateAsScala$$anonfun$propertiesAsScalaMapConverter$1 extends AbstractFunction0<Map<String, String>> implements Serializable {
    private final Properties p$2;

    public DecorateAsScala$$anonfun$propertiesAsScalaMapConverter$1(DecorateAsScala decorateAsScala, Properties properties) {
        this.p$2 = properties;
    }

    public final Map<String, String> apply() {
        return WrapAsScala$.MODULE$.propertiesAsScalaMap(this.p$2);
    }
}
