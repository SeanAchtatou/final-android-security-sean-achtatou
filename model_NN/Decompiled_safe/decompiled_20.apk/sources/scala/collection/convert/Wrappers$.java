package scala.collection.convert;

import scala.Serializable;
import scala.collection.convert.Wrappers;

public final class Wrappers$ implements Serializable, Wrappers {
    public static final Wrappers$ MODULE$ = null;

    static {
        new Wrappers$();
    }

    private Wrappers$() {
        MODULE$ = this;
        Wrappers.Cclass.$init$(this);
    }
}
