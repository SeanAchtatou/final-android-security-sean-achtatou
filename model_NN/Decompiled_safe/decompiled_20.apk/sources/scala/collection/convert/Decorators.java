package scala.collection.convert;

import scala.Function0;

public interface Decorators {

    public class AsScala<A> {
        public final /* synthetic */ Decorators $outer;
        private final Function0<A> op;

        public AsScala(Decorators decorators, Function0<A> function0) {
            this.op = function0;
            if (decorators == null) {
                throw new NullPointerException();
            }
            this.$outer = decorators;
        }

        public A asScala() {
            return this.op.apply();
        }
    }

    /* renamed from: scala.collection.convert.Decorators$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Decorators decorators) {
        }
    }
}
