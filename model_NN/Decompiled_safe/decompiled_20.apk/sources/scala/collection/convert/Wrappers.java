package scala.collection.convert;

import java.util.Properties;
import scala.Function1;
import scala.None$;
import scala.Option;
import scala.Product;
import scala.Serializable;
import scala.Some;
import scala.Tuple2;
import scala.collection.GenMap;
import scala.collection.Iterator;
import scala.collection.Map;
import scala.collection.Traversable;
import scala.collection.mutable.AbstractMap;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public interface Wrappers {

    public class JPropertiesWrapper extends AbstractMap<String, String> implements Product, Serializable {
        public final /* synthetic */ Wrappers $outer;
        private final Properties underlying;

        public JPropertiesWrapper(Wrappers wrappers, Properties properties) {
            this.underlying = properties;
            if (wrappers == null) {
                throw new NullPointerException();
            }
            this.$outer = wrappers;
            Product.Cclass.$init$(this);
        }

        public /* bridge */ /* synthetic */ Map $minus(Object obj) {
            return $minus(obj);
        }

        public JPropertiesWrapper $minus$eq(String str) {
            underlying().remove(str);
            return this;
        }

        public /* bridge */ /* synthetic */ GenMap $plus(Tuple2 tuple2) {
            return $plus(tuple2);
        }

        public JPropertiesWrapper $plus$eq(Tuple2<String, String> tuple2) {
            underlying().put(tuple2._1(), tuple2._2());
            return this;
        }

        public /* bridge */ /* synthetic */ Object clone() {
            return clone();
        }

        public JPropertiesWrapper empty() {
            return new JPropertiesWrapper(scala$collection$convert$Wrappers$JPropertiesWrapper$$$outer(), new Properties());
        }

        public /* bridge */ /* synthetic */ Object filterNot(Function1 function1) {
            return filterNot(function1);
        }

        public Option<String> get(String str) {
            Object obj = underlying().get(str);
            return obj == null ? None$.MODULE$ : new Some((String) obj);
        }

        public Iterator<Tuple2<String, String>> iterator() {
            return new Wrappers$JPropertiesWrapper$$anon$3(this);
        }

        public int productArity() {
            return 1;
        }

        public Object productElement(int i) {
            switch (i) {
                case 0:
                    break;
                default:
                    throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
            }
            return underlying();
        }

        public Iterator<Object> productIterator() {
            return ScalaRunTime$.MODULE$.typedProductIterator(this);
        }

        public String productPrefix() {
            return "JPropertiesWrapper";
        }

        public Option<String> put(String str, String str2) {
            Object put = underlying().put(str, str2);
            return put == null ? None$.MODULE$ : new Some((String) put);
        }

        public /* bridge */ /* synthetic */ Object result() {
            return result();
        }

        public /* synthetic */ Wrappers scala$collection$convert$Wrappers$JPropertiesWrapper$$$outer() {
            return this.$outer;
        }

        public /* bridge */ /* synthetic */ Map seq() {
            return seq();
        }

        public int size() {
            return underlying().size();
        }

        public /* bridge */ /* synthetic */ Traversable thisCollection() {
            return thisCollection();
        }

        public Properties underlying() {
            return this.underlying;
        }

        public void update(String str, String str2) {
            underlying().put(str, str2);
        }
    }

    /* renamed from: scala.collection.convert.Wrappers$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Wrappers wrappers) {
        }
    }
}
