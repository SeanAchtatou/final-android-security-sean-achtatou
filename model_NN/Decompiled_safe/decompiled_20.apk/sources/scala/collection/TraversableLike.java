package scala.collection;

import scala.Function0;
import scala.Function1;
import scala.PartialFunction;
import scala.Predef$;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.FilterMonadic;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.collection.parallel.ParIterable;
import scala.math.package$;
import scala.runtime.BooleanRef;
import scala.runtime.IntRef;
import scala.runtime.ObjectRef;
import scala.runtime.RichInt$;
import scala.runtime.ScalaRunTime$;

public interface TraversableLike<A, Repr> extends GenTraversableLike<A, Repr>, Parallelizable<A, ParIterable<A>>, TraversableOnce<A> {

    public class WithFilter implements FilterMonadic<A, Repr> {
        public final /* synthetic */ TraversableLike $outer;
        public final Function1<A, Object> scala$collection$TraversableLike$WithFilter$$p;

        public WithFilter(TraversableLike<A, Repr> traversableLike, Function1<A, Object> function1) {
            this.scala$collection$TraversableLike$WithFilter$$p = function1;
            if (traversableLike == null) {
                throw new NullPointerException();
            }
            this.$outer = traversableLike;
        }

        public <U> void foreach(Function1<A, U> function1) {
            scala$collection$TraversableLike$WithFilter$$$outer().foreach(new TraversableLike$WithFilter$$anonfun$foreach$1(this, function1));
        }

        public /* synthetic */ TraversableLike scala$collection$TraversableLike$WithFilter$$$outer() {
            return this.$outer;
        }
    }

    /* renamed from: scala.collection.TraversableLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(TraversableLike traversableLike) {
        }

        public static Object $plus$plus(TraversableLike traversableLike, GenTraversableOnce genTraversableOnce, CanBuildFrom canBuildFrom) {
            Builder apply = canBuildFrom.apply(traversableLike.repr());
            if (genTraversableOnce instanceof IndexedSeqLike) {
                apply.sizeHint(traversableLike, genTraversableOnce.seq().size());
            }
            apply.$plus$plus$eq(traversableLike.thisCollection());
            apply.$plus$plus$eq(genTraversableOnce.seq());
            return apply.result();
        }

        private static final Builder builder$1(TraversableLike traversableLike, CanBuildFrom canBuildFrom) {
            Builder apply = canBuildFrom.apply(traversableLike.repr());
            apply.sizeHint(traversableLike);
            return apply;
        }

        private static final Builder builder$2(TraversableLike traversableLike, CanBuildFrom canBuildFrom) {
            return canBuildFrom.apply(traversableLike.repr());
        }

        public static Object collect(TraversableLike traversableLike, PartialFunction partialFunction, CanBuildFrom canBuildFrom) {
            Builder apply = canBuildFrom.apply(traversableLike.repr());
            traversableLike.foreach(new TraversableLike$$anonfun$collect$1(traversableLike, apply, partialFunction));
            return apply.result();
        }

        public static void copyToArray(TraversableLike traversableLike, Object obj, int i, int i2) {
            IntRef intRef = new IntRef(i);
            RichInt$ richInt$ = RichInt$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            Traversable$.MODULE$.breaks().breakable(new TraversableLike$$anonfun$copyToArray$1(traversableLike, intRef, richInt$.min$extension(i + i2, ScalaRunTime$.MODULE$.array_length(obj)), obj));
        }

        public static Object drop(TraversableLike traversableLike, int i) {
            if (i > 0) {
                return traversableLike.sliceWithKnownDelta(i, Integer.MAX_VALUE, -i);
            }
            Builder newBuilder = traversableLike.newBuilder();
            newBuilder.sizeHint(traversableLike);
            return ((Builder) newBuilder.$plus$plus$eq(traversableLike.thisCollection())).result();
        }

        public static boolean exists(TraversableLike traversableLike, Function1 function1) {
            BooleanRef booleanRef = new BooleanRef(false);
            Traversable$.MODULE$.breaks().breakable(new TraversableLike$$anonfun$exists$1(traversableLike, booleanRef, function1));
            return booleanRef.elem;
        }

        public static Object filter(TraversableLike traversableLike, Function1 function1) {
            Builder newBuilder = traversableLike.newBuilder();
            traversableLike.foreach(new TraversableLike$$anonfun$filter$1(traversableLike, newBuilder, function1));
            return newBuilder.result();
        }

        public static Object filterNot(TraversableLike traversableLike, Function1 function1) {
            return traversableLike.filter(new TraversableLike$$anonfun$filterNot$1(traversableLike, function1));
        }

        public static Object flatMap(TraversableLike traversableLike, Function1 function1, CanBuildFrom canBuildFrom) {
            Builder builder$2 = builder$2(traversableLike, canBuildFrom);
            traversableLike.foreach(new TraversableLike$$anonfun$flatMap$1(traversableLike, builder$2, function1));
            return builder$2.result();
        }

        public static boolean forall(TraversableLike traversableLike, Function1 function1) {
            BooleanRef booleanRef = new BooleanRef(true);
            Traversable$.MODULE$.breaks().breakable(new TraversableLike$$anonfun$forall$1(traversableLike, booleanRef, function1));
            return booleanRef.elem;
        }

        public static Object head(TraversableLike traversableLike) {
            ObjectRef objectRef = new ObjectRef(new TraversableLike$$anonfun$2(traversableLike));
            Traversable$.MODULE$.breaks().breakable(new TraversableLike$$anonfun$head$1(traversableLike, objectRef));
            return ((Function0) objectRef.elem).apply();
        }

        public static boolean isEmpty(TraversableLike traversableLike) {
            BooleanRef booleanRef = new BooleanRef(true);
            Traversable$.MODULE$.breaks().breakable(new TraversableLike$$anonfun$isEmpty$1(traversableLike, booleanRef));
            return booleanRef.elem;
        }

        public static final boolean isTraversableAgain(TraversableLike traversableLike) {
            return true;
        }

        public static Object last(TraversableLike traversableLike) {
            ObjectRef objectRef = new ObjectRef(traversableLike.head());
            traversableLike.foreach(new TraversableLike$$anonfun$last$1(traversableLike, objectRef));
            return objectRef.elem;
        }

        public static Object map(TraversableLike traversableLike, Function1 function1, CanBuildFrom canBuildFrom) {
            Builder builder$1 = builder$1(traversableLike, canBuildFrom);
            traversableLike.foreach(new TraversableLike$$anonfun$map$1(traversableLike, builder$1, function1));
            return builder$1.result();
        }

        public static Object repr(TraversableLike traversableLike) {
            return traversableLike;
        }

        public static Object scala$collection$TraversableLike$$sliceInternal(TraversableLike traversableLike, int i, int i2, Builder builder) {
            Traversable$.MODULE$.breaks().breakable(new TraversableLike$$anonfun$scala$collection$TraversableLike$$sliceInternal$1(traversableLike, i, i2, builder, new IntRef(0)));
            return builder.result();
        }

        public static Object slice(TraversableLike traversableLike, int i, int i2) {
            return traversableLike.sliceWithKnownBound(package$.MODULE$.max(i, 0), i2);
        }

        public static Object sliceWithKnownBound(TraversableLike traversableLike, int i, int i2) {
            Builder newBuilder = traversableLike.newBuilder();
            if (i2 <= i) {
                return newBuilder.result();
            }
            newBuilder.sizeHintBounded(i2 - i, traversableLike);
            return scala$collection$TraversableLike$$sliceInternal(traversableLike, i, i2, newBuilder);
        }

        public static Object sliceWithKnownDelta(TraversableLike traversableLike, int i, int i2, int i3) {
            Builder newBuilder = traversableLike.newBuilder();
            if (i2 <= i) {
                return newBuilder.result();
            }
            newBuilder.sizeHint(traversableLike, i3);
            return scala$collection$TraversableLike$$sliceInternal(traversableLike, i, i2, newBuilder);
        }

        public static String stringPrefix(TraversableLike traversableLike) {
            String name = traversableLike.repr().getClass().getName();
            int lastIndexOf = name.lastIndexOf(46);
            if (lastIndexOf != -1) {
                name = name.substring(lastIndexOf + 1);
            }
            int indexOf = name.indexOf(36);
            return indexOf != -1 ? name.substring(0, indexOf) : name;
        }

        public static Object tail(TraversableLike traversableLike) {
            if (!traversableLike.isEmpty()) {
                return traversableLike.drop(1);
            }
            throw new UnsupportedOperationException("empty.tail");
        }

        public static Object take(TraversableLike traversableLike, int i) {
            return traversableLike.slice(0, i);
        }

        public static Traversable thisCollection(TraversableLike traversableLike) {
            return (Traversable) traversableLike;
        }

        public static Object to(TraversableLike traversableLike, CanBuildFrom canBuildFrom) {
            Builder apply = canBuildFrom.apply();
            apply.sizeHint(traversableLike);
            apply.$plus$plus$eq(traversableLike.thisCollection());
            return apply.result();
        }

        public static Stream toStream(TraversableLike traversableLike) {
            return traversableLike.toBuffer().toStream();
        }

        public static String toString(TraversableLike traversableLike) {
            return traversableLike.mkString(new StringBuilder().append((Object) traversableLike.stringPrefix()).append((Object) "(").toString(), ", ", ")");
        }

        public static FilterMonadic withFilter(TraversableLike traversableLike, Function1 function1) {
            return new WithFilter(traversableLike, function1);
        }
    }

    <B, That> That $plus$plus(GenTraversableOnce<B> genTraversableOnce, CanBuildFrom<Repr, B, That> canBuildFrom);

    <B, That> That collect(PartialFunction<A, B> partialFunction, CanBuildFrom<Repr, B, That> canBuildFrom);

    <B> void copyToArray(Object obj, int i, int i2);

    Repr drop(int i);

    boolean exists(Function1<A, Object> function1);

    Repr filter(Function1 function1);

    Repr filterNot(Function1 function1);

    boolean forall(Function1<A, Object> function1);

    <U> void foreach(Function1<A, U> function1);

    A head();

    boolean isEmpty();

    A last();

    <B, That> That map(Function1<A, B> function1, CanBuildFrom<Repr, B, That> canBuildFrom);

    Builder<A, Repr> newBuilder();

    Repr repr();

    Repr slice(int i, int i2);

    Repr sliceWithKnownBound(int i, int i2);

    Repr sliceWithKnownDelta(int i, int i2, int i3);

    String stringPrefix();

    Repr tail();

    Traversable<A> thisCollection();

    Stream<A> toStream();

    FilterMonadic<A, Repr> withFilter(Function1 function1);
}
