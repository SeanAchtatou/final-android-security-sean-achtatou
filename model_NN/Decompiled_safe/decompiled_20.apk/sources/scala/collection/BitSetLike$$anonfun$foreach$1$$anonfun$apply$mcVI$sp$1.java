package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1 extends AbstractFunction1<Object, Object> implements Serializable {
    public final /* synthetic */ BitSetLike$$anonfun$foreach$1 $outer;
    public final long w$1;

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 1, insn: 0x0008: IPUT  
          (r2v0 ? I:scala.collection.BitSetLike$$anonfun$foreach$1)
          (r1 I:scala.collection.BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1)
         scala.collection.BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1.$outer scala.collection.BitSetLike$$anonfun$foreach$1, block:B:3:0x0008
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:165)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:137)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:55)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1(
/*
Method generation error in method: scala.collection.BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1.<init>(scala.collection.BitSetLike$$anonfun$foreach$1, scala.collection.BitSetLike<This>$1):void, dex: classes.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r3v0 ?
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:189)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:161)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:133)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:317)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
    	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
    	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
    	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
    	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
    	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
    	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
    	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
    	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
    	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
    	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
    	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:81)
    	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:45)
    	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:34)
    	at jadx.core.codegen.CodeGen.generate(CodeGen.java:22)
    	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
    	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
    	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
    
*/

    public final Object apply(int i) {
        return (this.w$1 & (1 << i)) != 0 ? this.$outer.f$1.apply(BoxesRunTime.boxToInteger(i)) : BoxedUnit.UNIT;
    }

    public final /* synthetic */ Object apply(Object obj) {
        return apply(BoxesRunTime.unboxToInt(obj));
    }
}
