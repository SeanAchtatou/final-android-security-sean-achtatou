package scala.collection;

import scala.Function1;
import scala.Function2;
import scala.Predef$$less$colon$less;
import scala.Tuple2;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ClassTag;
import scala.runtime.Nothing$;

public abstract class AbstractIterator<A> implements Iterator<A> {
    public AbstractIterator() {
        GenTraversableOnce.Cclass.$init$(this);
        TraversableOnce.Cclass.$init$(this);
        Iterator.Cclass.$init$(this);
    }

    public <B> B $div$colon(B b, Function2<B, A, B> function2) {
        return TraversableOnce.Cclass.$div$colon(this, b, function2);
    }

    public StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
        return TraversableOnce.Cclass.addString(this, stringBuilder, str, str2, str3);
    }

    public <B> void copyToArray(Object obj, int i) {
        TraversableOnce.Cclass.copyToArray(this, obj, i);
    }

    public <B> void copyToArray(Object obj, int i, int i2) {
        Iterator.Cclass.copyToArray(this, obj, i, i2);
    }

    public <B> void copyToBuffer(Buffer<B> buffer) {
        TraversableOnce.Cclass.copyToBuffer(this, buffer);
    }

    public Iterator<A> drop(int i) {
        return Iterator.Cclass.drop(this, i);
    }

    public boolean exists(Function1<A, Object> function1) {
        return Iterator.Cclass.exists(this, function1);
    }

    public <B> B foldLeft(B b, Function2<B, A, B> function2) {
        return TraversableOnce.Cclass.foldLeft(this, b, function2);
    }

    public boolean forall(Function1<A, Object> function1) {
        return Iterator.Cclass.forall(this, function1);
    }

    public <U> void foreach(Function1<A, U> function1) {
        Iterator.Cclass.foreach(this, function1);
    }

    public boolean isEmpty() {
        return Iterator.Cclass.isEmpty(this);
    }

    public boolean isTraversableAgain() {
        return Iterator.Cclass.isTraversableAgain(this);
    }

    public <B> Iterator<B> map(Function1<A, B> function1) {
        return Iterator.Cclass.map(this, function1);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String str) {
        return TraversableOnce.Cclass.mkString(this, str);
    }

    public String mkString(String str, String str2, String str3) {
        return TraversableOnce.Cclass.mkString(this, str, str2, str3);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public Iterator<A> seq() {
        return Iterator.Cclass.seq(this);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    public Iterator<A> slice(int i, int i2) {
        return Iterator.Cclass.slice(this, i, i2);
    }

    public Iterator<A> take(int i) {
        return Iterator.Cclass.take(this, i);
    }

    public <Col> Col to(CanBuildFrom<Nothing$, A, Col> canBuildFrom) {
        return TraversableOnce.Cclass.to(this, canBuildFrom);
    }

    public <B> Object toArray(ClassTag<B> classTag) {
        return TraversableOnce.Cclass.toArray(this, classTag);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <T, U> Map<T, U> toMap(Predef$$less$colon$less<A, Tuple2<T, U>> predef$$less$colon$less) {
        return TraversableOnce.Cclass.toMap(this, predef$$less$colon$less);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return Iterator.Cclass.toStream(this);
    }

    public String toString() {
        return Iterator.Cclass.toString(this);
    }
}
