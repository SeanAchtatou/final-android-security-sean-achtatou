package scala.collection;

import scala.collection.generic.GenTraversableFactory;
import scala.collection.mutable.Builder;

public final class Iterable$ extends GenTraversableFactory<Iterable> {
    public static final Iterable$ MODULE$ = null;

    static {
        new Iterable$();
    }

    private Iterable$() {
        MODULE$ = this;
    }

    public final <A> Builder<A, Iterable<A>> newBuilder() {
        return scala.collection.immutable.Iterable$.MODULE$.newBuilder();
    }
}
