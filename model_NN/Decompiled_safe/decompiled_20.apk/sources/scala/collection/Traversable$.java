package scala.collection;

import scala.collection.generic.GenTraversableFactory;
import scala.collection.mutable.Builder;
import scala.util.control.Breaks;

public final class Traversable$ extends GenTraversableFactory<Traversable> {
    public static final Traversable$ MODULE$ = null;
    private final Breaks breaks = new Breaks();

    static {
        new Traversable$();
    }

    private Traversable$() {
        MODULE$ = this;
    }

    public final Breaks breaks() {
        return this.breaks;
    }

    public final <A> Builder<A, Traversable<A>> newBuilder() {
        return scala.collection.immutable.Traversable$.MODULE$.newBuilder();
    }
}
