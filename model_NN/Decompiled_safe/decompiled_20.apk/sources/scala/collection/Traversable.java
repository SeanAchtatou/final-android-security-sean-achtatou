package scala.collection;

import scala.collection.generic.GenericCompanion;

public interface Traversable<A> extends GenTraversable<A>, TraversableLike<A, Traversable<A>> {

    /* renamed from: scala.collection.Traversable$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Traversable traversable) {
        }

        public static GenericCompanion companion(Traversable traversable) {
            return Traversable$.MODULE$;
        }

        public static Traversable seq(Traversable traversable) {
            return traversable;
        }
    }

    Traversable<A> seq();
}
