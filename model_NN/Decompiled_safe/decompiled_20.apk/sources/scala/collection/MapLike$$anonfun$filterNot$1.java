package scala.collection;

import scala.Function1;
import scala.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

public final class MapLike$$anonfun$filterNot$1 extends AbstractFunction1<Tuple2<A, B>, BoxedUnit> implements Serializable {
    private final Function1 p$1;
    private final ObjectRef res$1;

    public MapLike$$anonfun$filterNot$1(MapLike mapLike, ObjectRef objectRef, Function1 function1) {
        this.res$1 = objectRef;
        this.p$1 = function1;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object obj) {
        apply((Tuple2) obj);
        return BoxedUnit.UNIT;
    }

    public final void apply(Tuple2<A, B> tuple2) {
        if (BoxesRunTime.unboxToBoolean(this.p$1.apply(tuple2))) {
            this.res$1.elem = ((Map) this.res$1.elem).$minus(tuple2._1());
        }
    }
}
