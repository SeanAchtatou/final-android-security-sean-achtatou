package scala.collection;

import scala.Tuple2;

public interface GenMap<A, B> extends GenIterable<Tuple2<A, B>>, GenMapLike<A, B, GenMap<A, B>> {
}
