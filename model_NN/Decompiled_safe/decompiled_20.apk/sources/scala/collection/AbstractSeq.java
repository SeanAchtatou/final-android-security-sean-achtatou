package scala.collection;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.collection.GenSeq;
import scala.collection.GenSeqLike;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.generic.GenericCompanion;
import scala.runtime.BoxesRunTime;

public abstract class AbstractSeq<A> extends AbstractIterable<A> implements Seq<A> {
    public AbstractSeq() {
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        GenSeqLike.Cclass.$init$(this);
        GenSeq.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
    }

    public <C> PartialFunction<Object, C> andThen(Function1<A, C> function1) {
        return PartialFunction.Cclass.andThen(this, function1);
    }

    public double apply$mcDI$sp(int i) {
        return Function1.Cclass.apply$mcDI$sp(this, i);
    }

    public float apply$mcFI$sp(int i) {
        return Function1.Cclass.apply$mcFI$sp(this, i);
    }

    public int apply$mcII$sp(int i) {
        return Function1.Cclass.apply$mcII$sp(this, i);
    }

    public long apply$mcJI$sp(int i) {
        return Function1.Cclass.apply$mcJI$sp(this, i);
    }

    public void apply$mcVI$sp(int i) {
        Function1.Cclass.apply$mcVI$sp(this, i);
    }

    public boolean apply$mcZI$sp(int i) {
        return Function1.Cclass.apply$mcZI$sp(this, i);
    }

    public <A1, B1> B1 applyOrElse(A1 a1, Function1<A1, B1> function1) {
        return PartialFunction.Cclass.applyOrElse(this, a1, function1);
    }

    public GenericCompanion<Seq> companion() {
        return Seq.Cclass.companion(this);
    }

    public <A> Function1<A, A> compose(Function1<A, Object> function1) {
        return Function1.Cclass.compose(this, function1);
    }

    public <B> boolean corresponds(GenSeq<B> genSeq, Function2<A, B, Object> function2) {
        return SeqLike.Cclass.corresponds(this, genSeq, function2);
    }

    public boolean equals(Object obj) {
        return GenSeqLike.Cclass.equals(this, obj);
    }

    public int hashCode() {
        return GenSeqLike.Cclass.hashCode(this);
    }

    public boolean isDefinedAt(int i) {
        return GenSeqLike.Cclass.isDefinedAt(this, i);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public boolean isEmpty() {
        return SeqLike.Cclass.isEmpty(this);
    }

    public int lengthCompare(int i) {
        return SeqLike.Cclass.lengthCompare(this, i);
    }

    public int prefixLength(Function1<A, Object> function1) {
        return GenSeqLike.Cclass.prefixLength(this, function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public Seq<A> reverse() {
        return SeqLike.Cclass.reverse(this);
    }

    public Iterator<A> reverseIterator() {
        return SeqLike.Cclass.reverseIterator(this);
    }

    public int segmentLength(Function1<A, Object> function1, int i) {
        return SeqLike.Cclass.segmentLength(this, function1, i);
    }

    public Seq<A> seq() {
        return Seq.Cclass.seq(this);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    public Seq<A> thisCollection() {
        return SeqLike.Cclass.thisCollection(this);
    }

    public Seq<A> toCollection(Object obj) {
        return SeqLike.Cclass.toCollection(this, obj);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }
}
