package scala.collection;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Function1;
import scala.Function2;
import scala.Predef$$less$colon$less;
import scala.Tuple2;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.List;
import scala.collection.immutable.List$;
import scala.collection.immutable.Map;
import scala.collection.immutable.Map$;
import scala.collection.immutable.Set;
import scala.collection.immutable.Set$;
import scala.collection.mutable.ArrayBuffer$;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ClassTag;
import scala.runtime.BooleanRef;
import scala.runtime.IntRef;
import scala.runtime.Nothing$;
import scala.runtime.ObjectRef;
import scala.runtime.ScalaRunTime$;

public interface TraversableOnce<A> extends GenTraversableOnce<A> {

    /* renamed from: scala.collection.TraversableOnce$class  reason: invalid class name */
    public abstract class Cclass {
        public static Object $div$colon(TraversableOnce traversableOnce, Object obj, Function2 function2) {
            return traversableOnce.foldLeft(obj, function2);
        }

        public static void $init$(TraversableOnce traversableOnce) {
        }

        public static StringBuilder addString(TraversableOnce traversableOnce, StringBuilder stringBuilder, String str, String str2, String str3) {
            BooleanRef booleanRef = new BooleanRef(true);
            stringBuilder.append(str);
            traversableOnce.foreach(new TraversableOnce$$anonfun$addString$1(traversableOnce, booleanRef, stringBuilder, str2));
            stringBuilder.append(str3);
            return stringBuilder;
        }

        public static void copyToArray(TraversableOnce traversableOnce, Object obj, int i) {
            traversableOnce.copyToArray(obj, i, ScalaRunTime$.MODULE$.array_length(obj) - i);
        }

        public static void copyToBuffer(TraversableOnce traversableOnce, Buffer buffer) {
            buffer.$plus$plus$eq(traversableOnce.seq());
        }

        public static Object foldLeft(TraversableOnce traversableOnce, Object obj, Function2 function2) {
            ObjectRef objectRef = new ObjectRef(obj);
            traversableOnce.seq().foreach(new TraversableOnce$$anonfun$foldLeft$1(traversableOnce, objectRef, function2));
            return objectRef.elem;
        }

        public static String mkString(TraversableOnce traversableOnce) {
            return traversableOnce.mkString(PoiTypeDef.All);
        }

        public static String mkString(TraversableOnce traversableOnce, String str) {
            return traversableOnce.mkString(PoiTypeDef.All, str, PoiTypeDef.All);
        }

        public static String mkString(TraversableOnce traversableOnce, String str, String str2, String str3) {
            return traversableOnce.addString(new StringBuilder(), str, str2, str3).toString();
        }

        public static boolean nonEmpty(TraversableOnce traversableOnce) {
            return !traversableOnce.isEmpty();
        }

        public static int size(TraversableOnce traversableOnce) {
            IntRef intRef = new IntRef(0);
            traversableOnce.foreach(new TraversableOnce$$anonfun$size$1(traversableOnce, intRef));
            return intRef.elem;
        }

        public static Object to(TraversableOnce traversableOnce, CanBuildFrom canBuildFrom) {
            Builder apply = canBuildFrom.apply();
            apply.$plus$plus$eq(traversableOnce.seq());
            return apply.result();
        }

        public static Object toArray(TraversableOnce traversableOnce, ClassTag classTag) {
            if (!traversableOnce.isTraversableAgain()) {
                return traversableOnce.toBuffer().toArray(classTag);
            }
            Object newArray = classTag.newArray(traversableOnce.size());
            traversableOnce.copyToArray(newArray, 0);
            return newArray;
        }

        public static Buffer toBuffer(TraversableOnce traversableOnce) {
            return (Buffer) traversableOnce.to(ArrayBuffer$.MODULE$.canBuildFrom());
        }

        public static List toList(TraversableOnce traversableOnce) {
            return (List) traversableOnce.to(List$.MODULE$.canBuildFrom());
        }

        public static Map toMap(TraversableOnce traversableOnce, Predef$$less$colon$less predef$$less$colon$less) {
            Builder newBuilder = Map$.MODULE$.newBuilder();
            traversableOnce.foreach(new TraversableOnce$$anonfun$toMap$1(traversableOnce, newBuilder, predef$$less$colon$less));
            return (Map) newBuilder.result();
        }

        public static Set toSet(TraversableOnce traversableOnce) {
            return (Set) traversableOnce.to(Set$.MODULE$.canBuildFrom());
        }
    }

    <B> B $div$colon(B b, Function2<B, A, B> function2);

    StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3);

    <B> void copyToArray(Object obj, int i);

    <B> void copyToArray(Object obj, int i, int i2);

    <B> void copyToBuffer(Buffer<B> buffer);

    <B> B foldLeft(B b, Function2<B, A, B> function2);

    <U> void foreach(Function1<A, U> function1);

    boolean isEmpty();

    String mkString();

    String mkString(String str);

    String mkString(String str, String str2, String str3);

    boolean nonEmpty();

    TraversableOnce<A> seq();

    int size();

    <Col> Col to(CanBuildFrom<Nothing$, A, Col> canBuildFrom);

    <B> Object toArray(ClassTag<B> classTag);

    <B> Buffer<B> toBuffer();

    List<A> toList();

    <T, U> Map<T, U> toMap(Predef$$less$colon$less<A, Tuple2<T, U>> predef$$less$colon$less);

    <B> Set<B> toSet();
}
