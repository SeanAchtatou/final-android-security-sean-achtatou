package scala.collection;

import scala.Function1;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.TraversableLike;
import scala.collection.generic.CanBuildFrom;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.SetBuilder;
import scala.collection.parallel.ParSet;

public interface SetLike<A, This extends SetLike<A, This> & Set<A>> extends GenSetLike<A, This>, IterableLike<A, This>, Parallelizable<A, ParSet<A>> {

    /* renamed from: scala.collection.SetLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(SetLike setLike) {
        }

        public static Set $plus$plus(SetLike setLike, GenTraversableOnce genTraversableOnce) {
            return (Set) genTraversableOnce.seq().$div$colon((Set) setLike.repr(), new SetLike$$anonfun$$plus$plus$1(setLike));
        }

        public static boolean isEmpty(SetLike setLike) {
            return setLike.size() == 0;
        }

        public static Object map(SetLike setLike, Function1 function1, CanBuildFrom canBuildFrom) {
            return setLike.scala$collection$SetLike$$super$map(function1, canBuildFrom);
        }

        public static Builder newBuilder(SetLike setLike) {
            return new SetBuilder(setLike.empty());
        }

        public static String stringPrefix(SetLike setLike) {
            return "Set";
        }

        public static Buffer toBuffer(SetLike setLike) {
            ArrayBuffer arrayBuffer = new ArrayBuffer(setLike.size());
            setLike.copyToBuffer(arrayBuffer);
            return arrayBuffer;
        }

        public static String toString(SetLike setLike) {
            return TraversableLike.Cclass.toString(setLike);
        }
    }

    This $minus(Object obj);

    This $plus(Object obj);

    This $plus$plus(GenTraversableOnce<A> genTraversableOnce);

    This empty();

    boolean isEmpty();

    <B, That> That scala$collection$SetLike$$super$map(Function1<A, B> function1, CanBuildFrom<This, B, That> canBuildFrom);
}
