package scala.collection;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.Predef$$less$colon$less;
import scala.Tuple2;
import scala.collection.GenTraversable;
import scala.collection.GenTraversableOnce;
import scala.collection.Parallelizable;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.FilterMonadic;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ClassTag;
import scala.runtime.Nothing$;

public abstract class AbstractTraversable<A> implements Traversable<A> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.AbstractTraversable, scala.collection.generic.GenericTraversableTemplate, scala.collection.Parallelizable, scala.collection.TraversableLike, scala.collection.GenTraversable, scala.collection.GenTraversableOnce, scala.collection.TraversableOnce, scala.collection.Traversable] */
    public AbstractTraversable() {
        GenTraversableOnce.Cclass.$init$(this);
        TraversableOnce.Cclass.$init$(this);
        Parallelizable.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        GenTraversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
    }

    public <B> B $div$colon(B b, Function2<B, A, B> function2) {
        return TraversableOnce.Cclass.$div$colon(this, b, function2);
    }

    public <B, That> That $plus$plus(GenTraversableOnce<B> genTraversableOnce, CanBuildFrom<Traversable<A>, B, That> canBuildFrom) {
        return TraversableLike.Cclass.$plus$plus(this, genTraversableOnce, canBuildFrom);
    }

    public StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
        return TraversableOnce.Cclass.addString(this, stringBuilder, str, str2, str3);
    }

    public <B, That> That collect(PartialFunction<A, B> partialFunction, CanBuildFrom<Traversable<A>, B, That> canBuildFrom) {
        return TraversableLike.Cclass.collect(this, partialFunction, canBuildFrom);
    }

    public GenericCompanion<Traversable> companion() {
        return Traversable.Cclass.companion(this);
    }

    public <B> void copyToArray(Object obj, int i) {
        TraversableOnce.Cclass.copyToArray(this, obj, i);
    }

    public <B> void copyToArray(Object obj, int i, int i2) {
        TraversableLike.Cclass.copyToArray(this, obj, i, i2);
    }

    public <B> void copyToBuffer(Buffer<B> buffer) {
        TraversableOnce.Cclass.copyToBuffer(this, buffer);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> drop(int i) {
        return TraversableLike.Cclass.drop(this, i);
    }

    public boolean exists(Function1<A, Object> function1) {
        return TraversableLike.Cclass.exists(this, function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> filter(Function1<A, Object> function1) {
        return TraversableLike.Cclass.filter(this, function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> filterNot(Function1 function1) {
        return TraversableLike.Cclass.filterNot(this, function1);
    }

    public <B> B foldLeft(B b, Function2<B, A, B> function2) {
        return TraversableOnce.Cclass.foldLeft(this, b, function2);
    }

    public boolean forall(Function1<A, Object> function1) {
        return TraversableLike.Cclass.forall(this, function1);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.AbstractTraversable, scala.collection.generic.GenericTraversableTemplate] */
    public <B> Builder<B, Traversable<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public A head() {
        return TraversableLike.Cclass.head(this);
    }

    public boolean isEmpty() {
        return TraversableLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public A last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<A, B> function1, CanBuildFrom<Traversable<A>, B, That> canBuildFrom) {
        return TraversableLike.Cclass.map(this, function1, canBuildFrom);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String str) {
        return TraversableOnce.Cclass.mkString(this, str);
    }

    public String mkString(String str, String str2, String str3) {
        return TraversableOnce.Cclass.mkString(this, str, str2, str3);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.AbstractTraversable, scala.collection.generic.GenericTraversableTemplate] */
    public Builder<A, Traversable<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public Traversable<A> seq() {
        return Traversable.Cclass.seq(this);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> slice(int i, int i2) {
        return TraversableLike.Cclass.slice(this, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> sliceWithKnownBound(int i, int i2) {
        return TraversableLike.Cclass.sliceWithKnownBound(this, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> sliceWithKnownDelta(int i, int i2, int i3) {
        return TraversableLike.Cclass.sliceWithKnownDelta(this, i, i2, i3);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Traversable<A>] */
    public Traversable<A> take(int i) {
        return TraversableLike.Cclass.take(this, i);
    }

    public Traversable<A> thisCollection() {
        return TraversableLike.Cclass.thisCollection(this);
    }

    public <Col> Col to(CanBuildFrom<Nothing$, A, Col> canBuildFrom) {
        return TraversableLike.Cclass.to(this, canBuildFrom);
    }

    public <B> Object toArray(ClassTag<B> classTag) {
        return TraversableOnce.Cclass.toArray(this, classTag);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <T, U> Map<T, U> toMap(Predef$$less$colon$less<A, Tuple2<T, U>> predef$$less$colon$less) {
        return TraversableOnce.Cclass.toMap(this, predef$$less$colon$less);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return TraversableLike.Cclass.toStream(this);
    }

    public String toString() {
        return TraversableLike.Cclass.toString(this);
    }

    public FilterMonadic<A, Traversable<A>> withFilter(Function1<A, Object> function1) {
        return TraversableLike.Cclass.withFilter(this, function1);
    }
}
