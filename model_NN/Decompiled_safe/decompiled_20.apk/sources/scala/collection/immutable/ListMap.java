package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Function1;
import scala.None$;
import scala.Option;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Map;
import scala.collection.Traversable;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;

public class ListMap<A, B> extends AbstractMap<A, B> implements Map<A, B> {

    public class Node<B1> extends ListMap<A, B1> {
        public final /* synthetic */ ListMap $outer;
        private final A key;
        private final B1 value;

        public Node(ListMap<A, B> listMap, A a, B1 b1) {
            this.key = a;
            this.value = b1;
            if (listMap == null) {
                throw new NullPointerException();
            }
            this.$outer = listMap;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        private B1 apply0(scala.collection.immutable.ListMap<A, B1> r3, A r4) {
            /*
                r2 = this;
            L_0x0000:
                java.lang.Object r1 = r3.key()
                if (r4 != r1) goto L_0x000e
                r0 = 1
            L_0x0007:
                if (r0 == 0) goto L_0x002f
                java.lang.Object r0 = r3.value()
                return r0
            L_0x000e:
                if (r4 != 0) goto L_0x0012
                r0 = 0
                goto L_0x0007
            L_0x0012:
                boolean r0 = r4 instanceof java.lang.Number
                if (r0 == 0) goto L_0x001e
                r0 = r4
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r1)
                goto L_0x0007
            L_0x001e:
                boolean r0 = r4 instanceof java.lang.Character
                if (r0 == 0) goto L_0x002a
                r0 = r4
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r1)
                goto L_0x0007
            L_0x002a:
                boolean r0 = r4.equals(r1)
                goto L_0x0007
            L_0x002f:
                scala.collection.immutable.ListMap r3 = r3.tail()
                goto L_0x0000
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.ListMap.Node.apply0(scala.collection.immutable.ListMap, java.lang.Object):java.lang.Object");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        private scala.Option<B1> get0(scala.collection.immutable.ListMap<A, B1> r3, A r4) {
            /*
                r2 = this;
            L_0x0000:
                java.lang.Object r1 = r3.key()
                if (r4 != r1) goto L_0x0013
                r0 = 1
            L_0x0007:
                if (r0 == 0) goto L_0x0034
                scala.Some r0 = new scala.Some
                java.lang.Object r1 = r3.value()
                r0.<init>(r1)
            L_0x0012:
                return r0
            L_0x0013:
                if (r4 != 0) goto L_0x0017
                r0 = 0
                goto L_0x0007
            L_0x0017:
                boolean r0 = r4 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0023
                r0 = r4
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r1)
                goto L_0x0007
            L_0x0023:
                boolean r0 = r4 instanceof java.lang.Character
                if (r0 == 0) goto L_0x002f
                r0 = r4
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r1)
                goto L_0x0007
            L_0x002f:
                boolean r0 = r4.equals(r1)
                goto L_0x0007
            L_0x0034:
                scala.collection.immutable.ListMap r0 = r3.tail()
                boolean r0 = r0.nonEmpty()
                if (r0 == 0) goto L_0x0043
                scala.collection.immutable.ListMap r3 = r3.tail()
                goto L_0x0000
            L_0x0043:
                scala.None$ r0 = scala.None$.MODULE$
                goto L_0x0012
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.ListMap.Node.get0(scala.collection.immutable.ListMap, java.lang.Object):scala.Option");
        }

        private int size0(ListMap<A, B1> listMap, int i) {
            while (!listMap.isEmpty()) {
                listMap = listMap.tail();
                i++;
            }
            return i;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        public ListMap<A, B1> $minus(A a) {
            List list;
            List list2 = Nil$.MODULE$;
            this = this;
            while (true) {
                list = list2;
                if (!this.nonEmpty()) {
                    break;
                }
                A key2 = this.key();
                list2 = !(a == key2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, key2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, key2) : a.equals(key2)) ? list.$colon$colon(new Tuple2(this.key(), this.value())) : list;
                this = this.tail();
            }
            Node node = (ListMap) ListMap$.MODULE$.apply(Nil$.MODULE$);
            while (true) {
                Nil$ nil$ = Nil$.MODULE$;
                if (list == null) {
                    if (nil$ == null) {
                        break;
                    }
                } else if (list.equals(nil$)) {
                    break;
                }
                Tuple2 tuple2 = (Tuple2) list.head();
                Node node2 = new Node(node, tuple2._1(), tuple2._2());
                list = (List) list.tail();
                node = node2;
            }
            return node;
        }

        public B1 apply(A a) {
            return apply0(this, a);
        }

        public Option<B1> get(A a) {
            return get0(this, a);
        }

        public boolean isEmpty() {
            return false;
        }

        public A key() {
            return this.key;
        }

        public /* synthetic */ ListMap scala$collection$immutable$ListMap$Node$$$outer() {
            return this.$outer;
        }

        public int size() {
            return size0(this, 0);
        }

        public ListMap<A, B1> tail() {
            return scala$collection$immutable$ListMap$Node$$$outer();
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        public <B2> ListMap<A, B2> updated(A a, B2 b2) {
            boolean contains = contains(a);
            this = this;
            if (contains) {
                this = $minus((Object) a);
            }
            return new Node(this, a, b2);
        }

        public B1 value() {
            return this.value;
        }
    }

    public ListMap<A, B> $minus(Object obj) {
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: scala.collection.immutable.ListMap.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.ListMap<A, B1>
     arg types: [java.lang.Object, java.lang.Object]
     candidates:
      scala.collection.immutable.ListMap.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map
      scala.collection.immutable.AbstractMap.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map<A, B1>
      scala.collection.immutable.ListMap.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.ListMap<A, B1> */
    public <B1> ListMap<A, B1> $plus(Tuple2 tuple2) {
        return updated(tuple2._1(), tuple2._2());
    }

    public ListMap<A, Nothing$> empty() {
        return ListMap$.MODULE$.empty();
    }

    public /* bridge */ /* synthetic */ Object filterNot(Function1 function1) {
        return filterNot(function1);
    }

    public Option<B> get(A a) {
        return None$.MODULE$;
    }

    public Iterator<Tuple2<A, B>> iterator() {
        return new ListMap$$anon$1(this).toList().reverseIterator();
    }

    public A key() {
        throw new NoSuchElementException("empty map");
    }

    public /* bridge */ /* synthetic */ Map seq() {
        return seq();
    }

    public int size() {
        return 0;
    }

    public ListMap<A, B> tail() {
        throw new NoSuchElementException("empty map");
    }

    public /* bridge */ /* synthetic */ Traversable thisCollection() {
        return thisCollection();
    }

    public <B1> ListMap<A, B1> updated(Object obj, Object obj2) {
        return new Node(this, obj, obj2);
    }

    public B value() {
        throw new NoSuchElementException("empty map");
    }
}
