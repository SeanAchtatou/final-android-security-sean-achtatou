package scala.collection.immutable;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Function1;
import scala.Function2;
import scala.Predef$;
import scala.collection.AbstractSeq;
import scala.collection.GenIterable;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.TraversableLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Seq;
import scala.collection.immutable.StringLike;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.math.Ordered;
import scala.reflect.ClassTag;
import scala.runtime.BoxesRunTime;

public class WrappedString extends AbstractSeq<Object> implements IndexedSeq<Object>, StringLike<WrappedString> {
    private final String self;

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.math.Ordered, scala.collection.immutable.Traversable, scala.collection.immutable.StringLike, scala.collection.IndexedSeq, scala.collection.immutable.Iterable, scala.collection.IndexedSeqOptimized, scala.collection.immutable.WrappedString, scala.collection.IndexedSeqLike, scala.collection.immutable.Seq, scala.collection.immutable.IndexedSeq] */
    public WrappedString(String str) {
        this.self = str;
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        Ordered.Cclass.$init$(this);
        StringLike.Cclass.$init$(this);
    }

    public char apply(int i) {
        return StringLike.Cclass.apply(this, i);
    }

    public /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToCharacter(apply(BoxesRunTime.unboxToInt(obj)));
    }

    public GenericCompanion<IndexedSeq> companion() {
        return IndexedSeq.Cclass.companion(this);
    }

    public /* bridge */ /* synthetic */ int compare(Object obj) {
        return compare((String) obj);
    }

    public int compare(String str) {
        return StringLike.Cclass.compare(this, str);
    }

    public int compareTo(Object obj) {
        return Ordered.Cclass.compareTo(this, obj);
    }

    public <B> void copyToArray(Object obj, int i, int i2) {
        IndexedSeqOptimized.Cclass.copyToArray(this, obj, i, i2);
    }

    public Object drop(int i) {
        return IndexedSeqOptimized.Cclass.drop(this, i);
    }

    public boolean exists(Function1<Object, Object> function1) {
        return IndexedSeqOptimized.Cclass.exists(this, function1);
    }

    public <B> B foldLeft(B b, Function2<B, Object, B> function2) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, b, function2);
    }

    public boolean forall(Function1<Object, Object> function1) {
        return IndexedSeqOptimized.Cclass.forall(this, function1);
    }

    public <U> void foreach(Function1<Object, U> function1) {
        IndexedSeqOptimized.Cclass.foreach(this, function1);
    }

    public String format(scala.collection.Seq<Object> seq) {
        return StringLike.Cclass.format(this, seq);
    }

    public int hashCode() {
        return IndexedSeqLike.Cclass.hashCode(this);
    }

    public Object head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public Iterator<Object> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public Object last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public int length() {
        return self().length();
    }

    public int lengthCompare(int i) {
        return IndexedSeqOptimized.Cclass.lengthCompare(this, i);
    }

    public String mkString() {
        return StringLike.Cclass.mkString(this);
    }

    public Builder<Object, WrappedString> newBuilder() {
        return WrappedString$.MODULE$.newBuilder();
    }

    public Object reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public Iterator<Object> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(GenIterable<B> genIterable) {
        return IndexedSeqOptimized.Cclass.sameElements(this, genIterable);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public boolean scala$collection$IndexedSeqOptimized$$super$sameElements(GenIterable genIterable) {
        return IterableLike.Cclass.sameElements(this, genIterable);
    }

    public Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public int segmentLength(Function1<Object, Object> function1, int i) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, function1, i);
    }

    public String self() {
        return this.self;
    }

    public IndexedSeq<Object> seq() {
        return IndexedSeq.Cclass.seq(this);
    }

    public WrappedString slice(int i, int i2) {
        if (i < 0) {
            i = 0;
        }
        if (i2 <= i || i >= ((WrappedString) repr()).length()) {
            return new WrappedString(PoiTypeDef.All);
        }
        if (i2 > length()) {
            i2 = length();
        }
        return new WrappedString(Predef$.MODULE$.unwrapString((WrappedString) repr()).substring(i, i2));
    }

    public String[] split(char c) {
        return StringLike.Cclass.split(this, c);
    }

    public String stripSuffix(String str) {
        return StringLike.Cclass.stripSuffix(this, str);
    }

    public Object tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public Object take(int i) {
        return IndexedSeqOptimized.Cclass.take(this, i);
    }

    public WrappedString thisCollection() {
        return this;
    }

    public <B> Object toArray(ClassTag<B> classTag) {
        return StringLike.Cclass.toArray(this, classTag);
    }

    public <A1> Buffer<A1> toBuffer() {
        return IndexedSeqLike.Cclass.toBuffer(this);
    }

    public WrappedString toCollection(WrappedString wrappedString) {
        return wrappedString;
    }

    public String toString() {
        return self();
    }
}
