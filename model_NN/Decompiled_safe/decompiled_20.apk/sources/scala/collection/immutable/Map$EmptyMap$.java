package scala.collection.immutable;

import scala.None$;
import scala.Option;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.immutable.Map;
import scala.runtime.Nothing$;

public class Map$EmptyMap$ extends AbstractMap<Object, Nothing$> implements Map<Object, Nothing$> {
    public static final Map$EmptyMap$ MODULE$ = null;

    static {
        new Map$EmptyMap$();
    }

    public Map$EmptyMap$() {
        MODULE$ = this;
    }

    public Map<Object, Nothing$> $minus(Object obj) {
        return this;
    }

    public <B1> Map<Object, B1> $plus(Tuple2<Object, B1> tuple2) {
        return updated(tuple2._1(), tuple2._2());
    }

    public Option<Nothing$> get(Object obj) {
        return None$.MODULE$;
    }

    public Iterator<Tuple2<Object, Nothing$>> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public int size() {
        return 0;
    }

    public <B1> Map<Object, B1> updated(Object obj, B1 b1) {
        return new Map.Map1(obj, b1);
    }
}
