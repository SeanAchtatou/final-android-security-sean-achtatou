package scala.collection.immutable;

import scala.Tuple2;
import scala.collection.immutable.HashMap;

public final class HashMap$HashTrieMap$$anon$1 extends TrieIterator<Tuple2<A, B>> {
    public HashMap$HashTrieMap$$anon$1(HashMap.HashTrieMap<A, B> hashTrieMap) {
        super((Iterable[]) hashTrieMap.elems());
    }

    public final Tuple2<A, B> getElem(Object obj) {
        return ((HashMap.HashMap1) obj).ensurePair();
    }
}
