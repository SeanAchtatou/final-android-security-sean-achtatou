package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.Product;
import scala.collection.AbstractSeq;
import scala.collection.GenIterable;
import scala.collection.GenSeq;
import scala.collection.GenTraversableOnce;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.LinearSeq;
import scala.collection.LinearSeqLike;
import scala.collection.LinearSeqOptimized;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.LinearSeq;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Stream;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.ListBuffer;
import scala.math.package$;
import scala.runtime.BoxesRunTime;

public abstract class List<A> extends AbstractSeq<A> implements Product, LinearSeqOptimized<A, List<A>>, GenericTraversableTemplate<A, List> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.LinearSeqOptimized, scala.collection.immutable.List, scala.collection.immutable.Iterable, scala.collection.LinearSeq, scala.collection.immutable.Seq, scala.Product, scala.collection.LinearSeqLike, scala.collection.immutable.LinearSeq] */
    public List() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        LinearSeqLike.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        Product.Cclass.$init$(this);
        LinearSeqOptimized.Cclass.$init$(this);
    }

    public <B> List<B> $colon$colon(B b) {
        return new C$colon$colon(b, this);
    }

    public <B> List<B> $colon$colon$colon(List<B> list) {
        return isEmpty() ? list : list.isEmpty() ? this : new ListBuffer().$plus$plus$eq((TraversableOnce) list).prependToList(this);
    }

    public <B, That> That $plus$plus(GenTraversableOnce<B> genTraversableOnce, CanBuildFrom<List<A>, B, That> canBuildFrom) {
        return canBuildFrom.apply(this) instanceof ListBuffer ? genTraversableOnce.seq().toList().$colon$colon$colon(this) : TraversableLike.Cclass.$plus$plus(this, genTraversableOnce, canBuildFrom);
    }

    public A apply(int i) {
        return LinearSeqOptimized.Cclass.apply(this, i);
    }

    public /* synthetic */ Object apply(Object obj) {
        return apply(BoxesRunTime.unboxToInt(obj));
    }

    public GenericCompanion<List> companion() {
        return List$.MODULE$;
    }

    public final <B> boolean corresponds(GenSeq<B> genSeq, Function2<A, B, Object> function2) {
        return LinearSeqLike.Cclass.corresponds(this, genSeq, function2);
    }

    public List<A> drop(int i) {
        while (!this.isEmpty() && i > 0) {
            i--;
            this = (List) this.tail();
        }
        return this;
    }

    public boolean exists(Function1<A, Object> function1) {
        return LinearSeqOptimized.Cclass.exists(this, function1);
    }

    public <B> B foldLeft(B b, Function2<B, A, B> function2) {
        return LinearSeqOptimized.Cclass.foldLeft(this, b, function2);
    }

    public boolean forall(Function1<A, Object> function1) {
        return LinearSeqOptimized.Cclass.forall(this, function1);
    }

    public final <B> void foreach(Function1<A, B> function1) {
        while (!this.isEmpty()) {
            function1.apply(this.head());
            this = (List) this.tail();
        }
    }

    public int hashCode() {
        return LinearSeqLike.Cclass.hashCode(this);
    }

    public boolean isDefinedAt(int i) {
        return LinearSeqOptimized.Cclass.isDefinedAt(this, i);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public Iterator<A> iterator() {
        return LinearSeqLike.Cclass.iterator(this);
    }

    public A last() {
        return LinearSeqOptimized.Cclass.last(this);
    }

    public int length() {
        return LinearSeqOptimized.Cclass.length(this);
    }

    public int lengthCompare(int i) {
        return LinearSeqOptimized.Cclass.lengthCompare(this, i);
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return Product.Cclass.productPrefix(this);
    }

    public List<A> reverse() {
        List list = Nil$.MODULE$;
        while (!this.isEmpty()) {
            List $colon$colon = list.$colon$colon(this.head());
            this = (List) this.tail();
            list = $colon$colon;
        }
        return list;
    }

    public <B> boolean sameElements(GenIterable<B> genIterable) {
        return LinearSeqOptimized.Cclass.sameElements(this, genIterable);
    }

    public boolean scala$collection$LinearSeqOptimized$$super$sameElements(GenIterable genIterable) {
        return IterableLike.Cclass.sameElements(this, genIterable);
    }

    public int segmentLength(Function1<A, Object> function1, int i) {
        return LinearSeqOptimized.Cclass.segmentLength(this, function1, i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.List, scala.collection.immutable.LinearSeq] */
    public LinearSeq<A> seq() {
        return LinearSeq.Cclass.seq(this);
    }

    public List<A> slice(int i, int i2) {
        int max = package$.MODULE$.max(i, 0);
        return (i2 <= max || isEmpty()) ? Nil$.MODULE$ : drop(max).take(i2 - max);
    }

    public String stringPrefix() {
        return "List";
    }

    public /* bridge */ /* synthetic */ List tail() {
        return (List) tail();
    }

    public List<A> take(int i) {
        ListBuffer listBuffer = new ListBuffer();
        int i2 = 0;
        List list = this;
        while (!list.isEmpty() && i2 < i) {
            i2++;
            listBuffer.$plus$eq(list.head());
            list = (List) list.tail();
        }
        return list.isEmpty() ? this : listBuffer.toList();
    }

    public scala.collection.LinearSeq<A> thisCollection() {
        return LinearSeqLike.Cclass.thisCollection(this);
    }

    public scala.collection.LinearSeq<A> toCollection(List<A> list) {
        return LinearSeqLike.Cclass.toCollection(this, list);
    }

    public /* bridge */ /* synthetic */ scala.collection.Seq toCollection(Object obj) {
        return toCollection((LinearSeqLike) obj);
    }

    public List<A> toList() {
        return this;
    }

    public Stream<A> toStream() {
        return isEmpty() ? Stream$Empty$.MODULE$ : new Stream.Cons(head(), new List$$anonfun$toStream$1(this));
    }
}
