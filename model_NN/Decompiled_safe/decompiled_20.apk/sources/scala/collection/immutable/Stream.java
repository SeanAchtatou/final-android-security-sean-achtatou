package scala.collection.immutable;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.Predef$;
import scala.Serializable;
import scala.collection.AbstractSeq;
import scala.collection.GenIterable;
import scala.collection.GenSeq;
import scala.collection.GenTraversableOnce;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.LinearSeq;
import scala.collection.LinearSeqLike;
import scala.collection.LinearSeqOptimized;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenTraversableFactory;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.TraversableForwarder;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.LinearSeq;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.LazyBuilder;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;
import scala.runtime.RichInt$;

public abstract class Stream<A> extends AbstractSeq<A> implements LinearSeqOptimized<A, Stream<A>>, GenericTraversableTemplate<A, Stream> {

    public final class Cons<A> extends Stream<A> implements Serializable {
        private final A hd;
        private final Function0<Stream<A>> tl;
        private volatile Stream<A> tlVal;

        public Cons(A a, Function0<Stream<A>> function0) {
            this.hd = a;
            this.tl = function0;
        }

        public final A head() {
            return this.hd;
        }

        public final boolean isEmpty() {
            return false;
        }

        public final Stream<A> tail() {
            if (!tailDefined()) {
                synchronized (this) {
                    if (!tailDefined()) {
                        this.tlVal = this.tl.apply();
                    }
                }
            }
            return this.tlVal;
        }

        public final boolean tailDefined() {
            return this.tlVal != null;
        }
    }

    public class ConsWrapper<A> {
        private final Function0<Stream<A>> tl;

        public ConsWrapper(Function0<Stream<A>> function0) {
            this.tl = function0;
        }

        public Stream<A> $hash$colon$colon(A a) {
            Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
            return new Cons(a, this.tl);
        }
    }

    public class StreamBuilder<A> extends LazyBuilder<A, Stream<A>> {
        public Stream<A> result() {
            Object flatMap;
            Stream stream = TraversableForwarder.Cclass.toStream(parts());
            Stream$StreamBuilder$$anonfun$result$1 stream$StreamBuilder$$anonfun$result$1 = new Stream$StreamBuilder$$anonfun$result$1(this);
            Stream$ stream$ = Stream$.MODULE$;
            StreamCanBuildFrom streamCanBuildFrom = new StreamCanBuildFrom();
            if (!(streamCanBuildFrom.apply(stream.repr()) instanceof StreamBuilder)) {
                flatMap = TraversableLike.Cclass.flatMap(stream, stream$StreamBuilder$$anonfun$result$1, streamCanBuildFrom);
            } else if (stream.isEmpty()) {
                flatMap = Stream$Empty$.MODULE$;
            } else {
                ObjectRef objectRef = new ObjectRef(stream);
                Stream stream2 = ((TraversableOnce) ((Stream) objectRef.elem).head()).toStream().toStream();
                while (!((Stream) objectRef.elem).isEmpty() && stream2.isEmpty()) {
                    objectRef.elem = (Stream) ((Stream) objectRef.elem).tail();
                    if (!((Stream) objectRef.elem).isEmpty()) {
                        stream2 = ((TraversableOnce) ((Stream) objectRef.elem).head()).toStream().toStream();
                    }
                }
                if (((Stream) objectRef.elem).isEmpty()) {
                    Stream$ stream$2 = Stream$.MODULE$;
                    flatMap = Stream$Empty$.MODULE$;
                } else {
                    flatMap = stream2.append(new Stream$$anonfun$flatMap$1(stream, stream$StreamBuilder$$anonfun$result$1, objectRef));
                }
            }
            return (Stream) flatMap;
        }
    }

    public class StreamCanBuildFrom<A> extends GenTraversableFactory<Stream>.GenericCanBuildFrom<A> {
        public StreamCanBuildFrom() {
            super(Stream$.MODULE$);
        }
    }

    public final class StreamWithFilter extends TraversableLike<A, Stream<A>>.WithFilter {
        public final Function1<A, Object> scala$collection$immutable$Stream$StreamWithFilter$$p;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public StreamWithFilter(Stream<A> stream, Function1<A, Object> function1) {
            super(stream, function1);
            this.scala$collection$immutable$Stream$StreamWithFilter$$p = function1;
        }

        public final <B> void foreach(Function1<A, B> function1) {
            Object obj = this.$outer;
            while (true) {
                Stream stream = (Stream) obj;
                if (!stream.isEmpty()) {
                    Object head = stream.head();
                    if (BoxesRunTime.unboxToBoolean(this.scala$collection$immutable$Stream$StreamWithFilter$$p.apply(head))) {
                        function1.apply(head);
                    } else {
                        BoxedUnit boxedUnit = BoxedUnit.UNIT;
                    }
                    obj = stream.tail();
                } else {
                    return;
                }
            }
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.LinearSeqOptimized, scala.collection.immutable.Iterable, scala.collection.immutable.Stream, scala.collection.LinearSeq, scala.collection.immutable.Seq, scala.collection.LinearSeqLike, scala.collection.immutable.LinearSeq] */
    public Stream() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        LinearSeqLike.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeqOptimized.Cclass.$init$(this);
    }

    private final void loop$3(String str, Stream stream, StringBuilder stringBuilder, String str2, String str3) {
        while (!stream.isEmpty()) {
            stringBuilder.append(str).append(stream.head());
            if (stream.tailDefined()) {
                stream = (Stream) stream.tail();
                str = str2;
            } else {
                stringBuilder.append(str2).append("?").append(str3);
                return;
            }
        }
        stringBuilder.append(str3);
    }

    public <B, That> That $plus$plus(GenTraversableOnce<B> genTraversableOnce, CanBuildFrom<Stream<A>, B, That> canBuildFrom) {
        if (!(canBuildFrom.apply(repr()) instanceof StreamBuilder)) {
            return TraversableLike.Cclass.$plus$plus(this, genTraversableOnce, canBuildFrom);
        }
        if (isEmpty()) {
            return genTraversableOnce.toStream();
        }
        Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
        return new Cons(head(), new Stream$$anonfun$$plus$plus$1(this, genTraversableOnce));
    }

    public StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
        stringBuilder.append(str);
        loop$3(PoiTypeDef.All, this, stringBuilder, str2, str3);
        return stringBuilder;
    }

    public <B> Stream<B> append(Function0<TraversableOnce<B>> function0) {
        if (isEmpty()) {
            return function0.apply().toStream();
        }
        Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
        return new Cons(head(), new Stream$$anonfun$append$1(this, function0));
    }

    public A apply(int i) {
        return LinearSeqOptimized.Cclass.apply(this, i);
    }

    public /* synthetic */ Object apply(Object obj) {
        return apply(BoxesRunTime.unboxToInt(obj));
    }

    public final <B, That> That collect(PartialFunction<A, B> partialFunction, CanBuildFrom<Stream<A>, B, That> canBuildFrom) {
        if (!(canBuildFrom.apply(repr()) instanceof StreamBuilder)) {
            return TraversableLike.Cclass.collect(this, partialFunction, canBuildFrom);
        }
        while (this.nonEmpty() && !partialFunction.isDefinedAt(this.head())) {
            this = (Stream) this.tail();
        }
        return this.isEmpty() ? Stream$Empty$.MODULE$ : Stream$.MODULE$.collectedTail(this, partialFunction, canBuildFrom);
    }

    public GenericCompanion<Stream> companion() {
        return Stream$.MODULE$;
    }

    public final <B> boolean corresponds(GenSeq<B> genSeq, Function2<A, B, Object> function2) {
        return LinearSeqLike.Cclass.corresponds(this, genSeq, function2);
    }

    public final Stream<A> drop(int i) {
        while (i > 0 && !this.isEmpty()) {
            i--;
            this = (Stream) this.tail();
        }
        return this;
    }

    public boolean exists(Function1<A, Object> function1) {
        return LinearSeqOptimized.Cclass.exists(this, function1);
    }

    public Stream<A> filter(Function1<A, Object> function1) {
        while (!this.isEmpty() && !BoxesRunTime.unboxToBoolean(function1.apply(this.head()))) {
            this = (Stream) this.tail();
        }
        return this.nonEmpty() ? Stream$.MODULE$.filteredTail(this, function1) : Stream$Empty$.MODULE$;
    }

    public final <B> B foldLeft(B b, Function2<B, A, B> function2) {
        while (!this.isEmpty()) {
            b = function2.apply(b, this.head());
            this = (Stream) this.tail();
        }
        return b;
    }

    public boolean forall(Function1<A, Object> function1) {
        return LinearSeqOptimized.Cclass.forall(this, function1);
    }

    public Stream<A> force() {
        for (Stream stream = this; !stream.isEmpty(); stream = (Stream) stream.tail()) {
        }
        return this;
    }

    public final <B> void foreach(Function1<A, B> function1) {
        while (!this.isEmpty()) {
            function1.apply(this.head());
            this = (Stream) this.tail();
        }
    }

    public int hashCode() {
        return LinearSeqLike.Cclass.hashCode(this);
    }

    public boolean isDefinedAt(int i) {
        return LinearSeqOptimized.Cclass.isDefinedAt(this, i);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public Iterator<A> iterator() {
        return new StreamIterator(this);
    }

    public A last() {
        return LinearSeqOptimized.Cclass.last(this);
    }

    public int length() {
        int i = 0;
        while (!this.isEmpty()) {
            this = (Stream) this.tail();
            i++;
        }
        return i;
    }

    public int lengthCompare(int i) {
        return LinearSeqOptimized.Cclass.lengthCompare(this, i);
    }

    public final <B, That> That map(Function1<A, B> function1, CanBuildFrom<Stream<A>, B, That> canBuildFrom) {
        if (!(canBuildFrom.apply(repr()) instanceof StreamBuilder)) {
            return TraversableLike.Cclass.map(this, function1, canBuildFrom);
        }
        if (isEmpty()) {
            return Stream$Empty$.MODULE$;
        }
        Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
        return new Cons(function1.apply(head()), new Stream$$anonfun$map$1(this, function1));
    }

    public String mkString() {
        return mkString(PoiTypeDef.All);
    }

    public String mkString(String str) {
        return mkString(PoiTypeDef.All, str, PoiTypeDef.All);
    }

    public String mkString(String str, String str2, String str3) {
        force();
        return TraversableOnce.Cclass.mkString(this, str, str2, str3);
    }

    public Stream<A> reverse() {
        ObjectRef objectRef = new ObjectRef(Stream$Empty$.MODULE$);
        for (Stream stream = this; !stream.isEmpty(); stream = (Stream) stream.tail()) {
            Stream$ stream$ = Stream$.MODULE$;
            T $hash$colon$colon = new ConsWrapper(new Stream$$anonfun$1(this, objectRef)).$hash$colon$colon(stream.head());
            $hash$colon$colon.tail();
            objectRef.elem = $hash$colon$colon;
        }
        return (Stream) objectRef.elem;
    }

    public <B> boolean sameElements(GenIterable<B> genIterable) {
        return LinearSeqOptimized.Cclass.sameElements(this, genIterable);
    }

    public boolean scala$collection$LinearSeqOptimized$$super$sameElements(GenIterable genIterable) {
        return IterableLike.Cclass.sameElements(this, genIterable);
    }

    public int segmentLength(Function1<A, Object> function1, int i) {
        return LinearSeqOptimized.Cclass.segmentLength(this, function1, i);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Stream, scala.collection.immutable.LinearSeq] */
    public LinearSeq<A> seq() {
        return LinearSeq.Cclass.seq(this);
    }

    public Stream<A> slice(int i, int i2) {
        RichInt$ richInt$ = RichInt$.MODULE$;
        Predef$ predef$ = Predef$.MODULE$;
        int max$extension = richInt$.max$extension(i, 0);
        return (i2 <= max$extension || isEmpty()) ? Stream$.MODULE$.empty() : drop(max$extension).take(i2 - max$extension);
    }

    public String stringPrefix() {
        return "Stream";
    }

    public /* bridge */ /* synthetic */ Stream tail() {
        return (Stream) tail();
    }

    public abstract boolean tailDefined();

    public Stream<A> take(int i) {
        if (i <= 0 || isEmpty()) {
            Stream$ stream$ = Stream$.MODULE$;
            return Stream$Empty$.MODULE$;
        } else if (i == 1) {
            Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
            return new Cons(head(), new Stream$$anonfun$take$1(this));
        } else {
            Stream$cons$ stream$cons$2 = Stream$cons$.MODULE$;
            return new Cons(head(), new Stream$$anonfun$take$2(this, i));
        }
    }

    public scala.collection.LinearSeq<A> thisCollection() {
        return LinearSeqLike.Cclass.thisCollection(this);
    }

    public scala.collection.LinearSeq<A> toCollection(Stream<A> stream) {
        return LinearSeqLike.Cclass.toCollection(this, stream);
    }

    public /* bridge */ /* synthetic */ scala.collection.Seq toCollection(Object obj) {
        return toCollection((LinearSeqLike) obj);
    }

    public Stream<A> toStream() {
        return this;
    }

    public String toString() {
        return TraversableOnce.Cclass.mkString(this, new StringBuilder().append((Object) stringPrefix()).append((Object) "(").toString(), ", ", ")");
    }

    public final Stream<A>.StreamWithFilter withFilter(Function1<A, Object> function1) {
        return new StreamWithFilter(this, function1);
    }
}
