package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Product;
import scala.collection.GenSeq;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;
import scala.runtime.ScalaRunTime$;

public final class Nil$ extends List<Nothing$> implements Product {
    public static final Nil$ MODULE$ = null;

    static {
        new Nil$();
    }

    private Nil$() {
        MODULE$ = this;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof GenSeq) {
            return ((GenSeq) obj).isEmpty();
        }
        return false;
    }

    public final Nothing$ head() {
        throw new NoSuchElementException("head of empty list");
    }

    public final boolean isEmpty() {
        return true;
    }

    public final int productArity() {
        return 0;
    }

    public final Object productElement(int i) {
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public final Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public final String productPrefix() {
        return "Nil";
    }

    public final List<Nothing$> tail() {
        throw new UnsupportedOperationException("tail of empty list");
    }
}
