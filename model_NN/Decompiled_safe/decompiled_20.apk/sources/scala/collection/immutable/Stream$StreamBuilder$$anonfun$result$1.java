package scala.collection.immutable;

import scala.Serializable;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction1;

public final class Stream$StreamBuilder$$anonfun$result$1 extends AbstractFunction1<TraversableOnce<A>, Stream<A>> implements Serializable {
    public Stream$StreamBuilder$$anonfun$result$1(Stream.StreamBuilder<A> streamBuilder) {
    }

    public final Stream<A> apply(TraversableOnce<A> traversableOnce) {
        return traversableOnce.toStream();
    }
}
