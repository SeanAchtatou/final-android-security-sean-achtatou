package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Tuple2;
import scala.collection.AbstractIterator;

public final class ListMap$$anon$1 extends AbstractIterator<Tuple2<A, B>> {
    private ListMap<A, B> self;

    public ListMap$$anon$1(ListMap<A, B> listMap) {
        this.self = listMap;
    }

    public final boolean hasNext() {
        return !self().isEmpty();
    }

    public final Tuple2<A, B> next() {
        if (hasNext()) {
            Tuple2<A, B> tuple2 = new Tuple2<>(self().key(), self().value());
            this.self = self().tail();
            return tuple2;
        }
        throw new NoSuchElementException("next on empty iterator");
    }

    public final ListMap<A, B> self() {
        return this.self;
    }

    public final void self_$eq(ListMap<A, B> listMap) {
        this.self = listMap;
    }
}
