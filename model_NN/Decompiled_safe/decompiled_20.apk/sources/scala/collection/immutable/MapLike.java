package scala.collection.immutable;

import scala.Tuple2;
import scala.collection.immutable.Map;
import scala.collection.immutable.MapLike;

public interface MapLike<A, B, This extends MapLike<A, B, This> & Map<A, B>> extends scala.collection.MapLike<A, B, This> {

    /* renamed from: scala.collection.immutable.MapLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(MapLike mapLike) {
        }

        public static Map updated(MapLike mapLike, Object obj, Object obj2) {
            return mapLike.$plus(new Tuple2(obj, obj2));
        }
    }

    <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2);
}
