package scala.collection.immutable;

import scala.collection.AbstractIterator;
import scala.collection.Iterator$;

public final class Vector$$anon$1 extends AbstractIterator<A> {
    private final /* synthetic */ Vector $outer;
    private int i;

    public Vector$$anon$1(Vector<A> vector) {
        if (vector == null) {
            throw new NullPointerException();
        }
        this.$outer = vector;
        this.i = vector.length();
    }

    private int i() {
        return this.i;
    }

    private void i_$eq(int i2) {
        this.i = i2;
    }

    public final boolean hasNext() {
        return i() > 0;
    }

    public final A next() {
        if (i() <= 0) {
            return Iterator$.MODULE$.empty().next();
        }
        this.i = i() - 1;
        return this.$outer.apply(i());
    }
}
