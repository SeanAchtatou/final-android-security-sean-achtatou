package scala.collection.immutable;

import scala.Serializable;
import scala.collection.IndexedSeq$;
import scala.collection.generic.GenTraversableFactory;
import scala.collection.generic.SeqFactory;
import scala.collection.immutable.Vector;
import scala.collection.mutable.Builder;
import scala.runtime.Nothing$;

public final class Vector$ extends SeqFactory<Vector> implements Serializable {
    public static final Vector$ MODULE$ = null;
    private final Vector<Nothing$> NIL = new Vector<>(0, 0, 0);
    private GenTraversableFactory<Vector>.GenericCanBuildFrom<Nothing$> ReusableCBF;
    private final GenTraversableFactory<Vector>.GenericCanBuildFrom<Nothing$> VectorReusableCBF = new Vector.VectorReusableCBF();
    private volatile boolean bitmap$0;

    static {
        new Vector$();
    }

    private Vector$() {
        MODULE$ = this;
    }

    private GenTraversableFactory.GenericCanBuildFrom ReusableCBF$lzycompute() {
        synchronized (this) {
            if (!this.bitmap$0) {
                this.ReusableCBF = IndexedSeq$.MODULE$.ReusableCBF();
                this.bitmap$0 = true;
            }
        }
        return this.ReusableCBF;
    }

    public final Vector<Nothing$> NIL() {
        return this.NIL;
    }

    public final GenTraversableFactory<Vector>.GenericCanBuildFrom<Nothing$> ReusableCBF() {
        return this.bitmap$0 ? this.ReusableCBF : ReusableCBF$lzycompute();
    }

    public final <A> Vector<A> empty() {
        return NIL();
    }

    public final <A> Builder<A, Vector<A>> newBuilder() {
        return new VectorBuilder();
    }
}
