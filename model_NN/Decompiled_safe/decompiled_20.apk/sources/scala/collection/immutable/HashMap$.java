package scala.collection.immutable;

import scala.Serializable;
import scala.Tuple2;
import scala.collection.generic.BitOperations;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenMapFactory;
import scala.collection.generic.ImmutableMapFactory;
import scala.collection.immutable.HashMap;

public final class HashMap$ extends ImmutableMapFactory<HashMap> implements Serializable, BitOperations.Int {
    public static final HashMap$ MODULE$ = null;
    private final HashMap.Merger<Object, Object> defaultMerger = new HashMap$$anon$2(new HashMap$$anonfun$1());

    static {
        new HashMap$();
    }

    private HashMap$() {
        MODULE$ = this;
        BitOperations.Int.Cclass.$init$(this);
    }

    public final <A, B> CanBuildFrom<HashMap<?, ?>, Tuple2<A, B>, HashMap<A, B>> canBuildFrom() {
        return new GenMapFactory.MapCanBuildFrom(this);
    }

    public final <A, B> HashMap<A, B> empty() {
        return HashMap$EmptyHashMap$.MODULE$;
    }

    public final <A, B> HashMap.HashTrieMap<A, B> scala$collection$immutable$HashMap$$makeHashTrieMap(int i, HashMap<A, B> hashMap, int i2, HashMap<A, B> hashMap2, int i3, int i4) {
        int i5 = (i >>> i3) & 31;
        int i6 = (i2 >>> i3) & 31;
        if (i5 != i6) {
            int i7 = (1 << i5) | (1 << i6);
            HashMap[] hashMapArr = new HashMap[2];
            if (i5 < i6) {
                hashMapArr[0] = hashMap;
                hashMapArr[1] = hashMap2;
            } else {
                hashMapArr[0] = hashMap2;
                hashMapArr[1] = hashMap;
            }
            return new HashMap.HashTrieMap<>(i7, hashMapArr, i4);
        }
        return new HashMap.HashTrieMap<>(1 << i5, new HashMap[]{scala$collection$immutable$HashMap$$makeHashTrieMap(i, hashMap, i2, hashMap2, i3 + 5, i4)}, i4);
    }
}
