package scala.collection.immutable;

import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.ImmutableSetFactory;

public final class Set$ extends ImmutableSetFactory<Set> {
    public static final Set$ MODULE$ = null;

    static {
        new Set$();
    }

    private Set$() {
        MODULE$ = this;
    }

    public final <A> CanBuildFrom<Set<?>, A, Set<A>> canBuildFrom() {
        return setCanBuildFrom();
    }

    public final <A> Set<A> empty() {
        return Set$EmptySet$.MODULE$;
    }
}
