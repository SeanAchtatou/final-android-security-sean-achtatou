package scala.collection.immutable;

import scala.collection.AbstractIterator;
import scala.collection.Iterator$;

public final class ListSet$$anon$1 extends AbstractIterator<A> {
    private ListSet<A> that;

    public ListSet$$anon$1(ListSet<A> listSet) {
        this.that = listSet;
    }

    private ListSet<A> that() {
        return this.that;
    }

    private void that_$eq(ListSet<A> listSet) {
        this.that = listSet;
    }

    public final boolean hasNext() {
        return that().nonEmpty();
    }

    public final A next() {
        if (!hasNext()) {
            return Iterator$.MODULE$.empty().next();
        }
        A head = that().head();
        this.that = that().tail();
        return head;
    }
}
