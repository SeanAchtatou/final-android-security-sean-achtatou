package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Serializable;
import scala.collection.AbstractSet;
import scala.collection.GenTraversableOnce;
import scala.collection.Iterator;
import scala.collection.SetLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.Growable;
import scala.collection.generic.TraversableForwarder;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Set;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Builder;
import scala.collection.mutable.HashSet;
import scala.collection.mutable.ListBuffer;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public class ListSet<A> extends AbstractSet<A> implements Serializable, SetLike<A, ListSet<A>> {

    public class ListSetBuilder<Elem> implements Builder<Elem, ListSet<Elem>> {
        private final ListBuffer<Elem> elems;
        private final HashSet<Elem> seen;

        public ListSetBuilder() {
            this(ListSet$.MODULE$.empty());
        }

        public ListSetBuilder(ListSet<Elem> listSet) {
            Growable.Cclass.$init$(this);
            Builder.Cclass.$init$(this);
            this.elems = (ListBuffer) new ListBuffer().$plus$plus$eq((TraversableOnce) listSet).reverse();
            this.seen = (HashSet) new HashSet().$plus$plus$eq(listSet);
        }

        public ListSetBuilder<Elem> $plus$eq(Elem elem) {
            if (seen().apply((Object) elem)) {
                BoxedUnit boxedUnit = BoxedUnit.UNIT;
            } else {
                elems().$plus$eq((Object) elem);
                seen().$plus$eq((Object) elem);
            }
            return this;
        }

        public Growable<Elem> $plus$plus$eq(TraversableOnce<Elem> traversableOnce) {
            return Growable.Cclass.$plus$plus$eq(this, traversableOnce);
        }

        public ListBuffer<Elem> elems() {
            return this.elems;
        }

        public ListSet<Elem> result() {
            ListBuffer elems2 = elems();
            ListSet$ listSet$ = ListSet$.MODULE$;
            return (ListSet) TraversableForwarder.Cclass.foldLeft(elems2, ListSet$EmptyListSet$.MODULE$, new ListSet$ListSetBuilder$$anonfun$result$1(this));
        }

        public HashSet<Elem> seen() {
            return this.seen;
        }

        public void sizeHint(int i) {
            Builder.Cclass.sizeHint(this, i);
        }

        public void sizeHint(TraversableLike<?, ?> traversableLike) {
            Builder.Cclass.sizeHint(this, traversableLike);
        }

        public void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
            Builder.Cclass.sizeHint(this, traversableLike, i);
        }

        public void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
            Builder.Cclass.sizeHintBounded(this, i, traversableLike);
        }
    }

    public class Node extends ListSet<A> {
        public final /* synthetic */ ListSet $outer;
        private final A head;

        public Node(ListSet<A> listSet, A a) {
            this.head = a;
            if (listSet == null) {
                throw new NullPointerException();
            }
            this.$outer = listSet;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        private boolean containsInternal(scala.collection.immutable.ListSet<A> r5, A r6) {
            /*
                r4 = this;
                r2 = 1
                r1 = 0
            L_0x0002:
                boolean r0 = r5.isEmpty()
                if (r0 == 0) goto L_0x000a
                r0 = r1
            L_0x0009:
                return r0
            L_0x000a:
                java.lang.Object r0 = r5.head()
                if (r0 != r6) goto L_0x0015
                r0 = r2
            L_0x0011:
                if (r0 == 0) goto L_0x0034
                r0 = r2
                goto L_0x0009
            L_0x0015:
                if (r0 != 0) goto L_0x0019
                r0 = r1
                goto L_0x0011
            L_0x0019:
                boolean r3 = r0 instanceof java.lang.Number
                if (r3 == 0) goto L_0x0024
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r6)
                goto L_0x0011
            L_0x0024:
                boolean r3 = r0 instanceof java.lang.Character
                if (r3 == 0) goto L_0x002f
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r6)
                goto L_0x0011
            L_0x002f:
                boolean r0 = r0.equals(r6)
                goto L_0x0011
            L_0x0034:
                scala.collection.immutable.ListSet r5 = r5.scala$collection$immutable$ListSet$$unchecked_outer()
                goto L_0x0002
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.ListSet.Node.containsInternal(scala.collection.immutable.ListSet, java.lang.Object):boolean");
        }

        private int sizeInternal(ListSet<A> listSet, int i) {
            while (!listSet.isEmpty()) {
                listSet = listSet.scala$collection$immutable$ListSet$$unchecked_outer();
                i++;
            }
            return i;
        }

        public ListSet<A> $minus(A a) {
            A head2 = head();
            return a == head2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, head2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, head2) : a.equals(head2) ? scala$collection$immutable$ListSet$Node$$$outer() : new Node(scala$collection$immutable$ListSet$Node$$$outer().$minus((Object) a), head());
        }

        public ListSet<A> $plus(A a) {
            return contains(a) ? this : new Node(this, a);
        }

        public boolean contains(A a) {
            return containsInternal(this, a);
        }

        public A head() {
            return this.head;
        }

        public boolean isEmpty() {
            return false;
        }

        public ListSet<A> scala$collection$immutable$ListSet$$unchecked_outer() {
            return scala$collection$immutable$ListSet$Node$$$outer();
        }

        public /* synthetic */ ListSet scala$collection$immutable$ListSet$Node$$$outer() {
            return this.$outer;
        }

        public int size() {
            return sizeInternal(this, 0);
        }

        public ListSet<A> tail() {
            return scala$collection$immutable$ListSet$Node$$$outer();
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.immutable.Iterable, scala.collection.immutable.ListSet, scala.collection.immutable.Set] */
    public ListSet() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Set.Cclass.$init$(this);
    }

    public ListSet<A> $minus(Object obj) {
        return this;
    }

    public ListSet<A> $plus(Object obj) {
        return new Node(this, obj);
    }

    public ListSet<A> $plus$plus(GenTraversableOnce<A> genTraversableOnce) {
        return genTraversableOnce.isEmpty() ? this : ((ListSetBuilder) new ListSetBuilder(this).$plus$plus$eq(genTraversableOnce.seq())).result();
    }

    public /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply(obj));
    }

    public GenericCompanion<ListSet> companion() {
        return ListSet$.MODULE$;
    }

    public boolean contains(A a) {
        return false;
    }

    public /* bridge */ /* synthetic */ scala.collection.Set empty() {
        return (scala.collection.Set) empty();
    }

    public A head() {
        throw new NoSuchElementException("Set has no elements");
    }

    public boolean isEmpty() {
        return true;
    }

    public Iterator<A> iterator() {
        return new ListSet$$anon$1(this);
    }

    public ListSet<A> scala$collection$immutable$ListSet$$unchecked_$plus(A a) {
        return new Node(this, a);
    }

    public ListSet<A> scala$collection$immutable$ListSet$$unchecked_outer() {
        throw new NoSuchElementException("Empty ListSet has no outer pointer");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.ListSet, scala.collection.immutable.Set] */
    public Set<A> seq() {
        return Set.Cclass.seq(this);
    }

    public int size() {
        return 0;
    }

    public String stringPrefix() {
        return "ListSet";
    }

    public ListSet<A> tail() {
        throw new NoSuchElementException("Next of an empty set");
    }

    public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
        return thisCollection();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.ListSet, scala.collection.immutable.Set] */
    public <B> Set<B> toSet() {
        return Set.Cclass.toSet(this);
    }
}
