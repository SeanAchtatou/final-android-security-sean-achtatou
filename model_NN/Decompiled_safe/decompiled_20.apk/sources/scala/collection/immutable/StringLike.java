package scala.collection.immutable;

import scala.Predef$;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.TraversableOnce;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Ordered;
import scala.math.ScalaNumber;
import scala.reflect.ClassTag;
import scala.reflect.ClassTag$;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt$;

public interface StringLike<Repr> extends IndexedSeqOptimized<Object, Repr>, Ordered<String> {

    /* renamed from: scala.collection.immutable.StringLike$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(StringLike stringLike) {
        }

        public static char apply(StringLike stringLike, int i) {
            return stringLike.toString().charAt(i);
        }

        public static int compare(StringLike stringLike, String str) {
            return stringLike.toString().compareTo(str);
        }

        public static String format(StringLike stringLike, Seq seq) {
            return String.format(stringLike.toString(), (Object[]) ((TraversableOnce) seq.map(new StringLike$$anonfun$format$1(stringLike), Seq$.MODULE$.canBuildFrom())).toArray(ClassTag$.MODULE$.AnyRef()));
        }

        public static String mkString(StringLike stringLike) {
            return stringLike.toString();
        }

        public static String scala$collection$immutable$StringLike$$escape(StringLike stringLike, char c) {
            return new StringBuilder().append((Object) "\\Q").append(BoxesRunTime.boxToCharacter(c)).append((Object) "\\E").toString();
        }

        public static Object scala$collection$immutable$StringLike$$unwrapArg(StringLike stringLike, Object obj) {
            return obj instanceof ScalaNumber ? ((ScalaNumber) obj).underlying() : obj;
        }

        public static Object slice(StringLike stringLike, int i, int i2) {
            RichInt$ richInt$ = RichInt$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            int max$extension = richInt$.max$extension(i, 0);
            RichInt$ richInt$2 = RichInt$.MODULE$;
            Predef$ predef$2 = Predef$.MODULE$;
            int min$extension = richInt$2.min$extension(i2, stringLike.length());
            if (max$extension >= min$extension) {
                return stringLike.newBuilder().result();
            }
            Builder newBuilder = stringLike.newBuilder();
            Predef$ predef$3 = Predef$.MODULE$;
            return ((Builder) newBuilder.$plus$plus$eq(new StringOps(stringLike.toString().substring(max$extension, min$extension)))).result();
        }

        public static String[] split(StringLike stringLike, char c) {
            return stringLike.toString().split(scala$collection$immutable$StringLike$$escape(stringLike, c));
        }

        public static String stripSuffix(StringLike stringLike, String str) {
            return stringLike.toString().endsWith(str) ? stringLike.toString().substring(0, stringLike.toString().length() - str.length()) : stringLike.toString();
        }

        public static Object toArray(StringLike stringLike, ClassTag classTag) {
            return stringLike.toString().toCharArray();
        }
    }

    char apply(int i);

    int compare(String str);

    String format(Seq<Object> seq);

    int length();

    Builder<Object, Repr> newBuilder();

    String[] split(char c);

    String stripSuffix(String str);
}
