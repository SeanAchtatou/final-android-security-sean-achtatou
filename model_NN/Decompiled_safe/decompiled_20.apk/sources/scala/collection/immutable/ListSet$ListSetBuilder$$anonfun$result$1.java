package scala.collection.immutable;

import scala.Serializable;
import scala.collection.immutable.ListSet;
import scala.runtime.AbstractFunction2;

public final class ListSet$ListSetBuilder$$anonfun$result$1 extends AbstractFunction2<ListSet<Elem>, Elem, ListSet<Elem>> implements Serializable {
    public ListSet$ListSetBuilder$$anonfun$result$1(ListSet.ListSetBuilder<Elem> listSetBuilder) {
    }

    public final ListSet<Elem> apply(ListSet<Elem> listSet, Elem elem) {
        return listSet.scala$collection$immutable$ListSet$$unchecked_$plus(elem);
    }
}
