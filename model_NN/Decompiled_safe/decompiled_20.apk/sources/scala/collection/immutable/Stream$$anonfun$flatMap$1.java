package scala.collection.immutable;

import scala.Function1;
import scala.Serializable;
import scala.collection.GenTraversableOnce;
import scala.collection.TraversableLike;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction0;
import scala.runtime.ObjectRef;

public final class Stream$$anonfun$flatMap$1 extends AbstractFunction0<Stream<B>> implements Serializable {
    private final /* synthetic */ Stream $outer;
    private final Function1 f$2;
    private final ObjectRef nonEmptyPrefix$1;

    public Stream$$anonfun$flatMap$1(Stream stream, Function1 function1, ObjectRef objectRef) {
        if (stream == null) {
            throw new NullPointerException();
        }
        this.$outer = stream;
        this.f$2 = function1;
        this.nonEmptyPrefix$1 = objectRef;
    }

    public final Stream<B> apply() {
        Object flatMap;
        Stream stream = (Stream) ((Stream) this.nonEmptyPrefix$1.elem).tail();
        Function1 function1 = this.f$2;
        CanBuildFrom canBuildFrom = Stream$.MODULE$.canBuildFrom();
        if (!(canBuildFrom.apply(stream.repr()) instanceof Stream.StreamBuilder)) {
            flatMap = TraversableLike.Cclass.flatMap(stream, function1, canBuildFrom);
        } else if (stream.isEmpty()) {
            flatMap = Stream$Empty$.MODULE$;
        } else {
            ObjectRef objectRef = new ObjectRef(stream);
            Stream stream2 = ((GenTraversableOnce) function1.apply(((Stream) objectRef.elem).head())).toStream();
            while (!((Stream) objectRef.elem).isEmpty() && stream2.isEmpty()) {
                objectRef.elem = (Stream) ((Stream) objectRef.elem).tail();
                if (!((Stream) objectRef.elem).isEmpty()) {
                    stream2 = ((GenTraversableOnce) function1.apply(((Stream) objectRef.elem).head())).toStream();
                }
            }
            flatMap = ((Stream) objectRef.elem).isEmpty() ? Stream$.MODULE$.empty() : stream2.append(new Stream$$anonfun$flatMap$1(stream, function1, objectRef));
        }
        return (Stream) flatMap;
    }
}
