package scala.collection.immutable;

import scala.runtime.Nothing$;

public class ListMap$EmptyListMap$ extends ListMap<Object, Nothing$> {
    public static final ListMap$EmptyListMap$ MODULE$ = null;

    static {
        new ListMap$EmptyListMap$();
    }

    public ListMap$EmptyListMap$() {
        MODULE$ = this;
    }
}
