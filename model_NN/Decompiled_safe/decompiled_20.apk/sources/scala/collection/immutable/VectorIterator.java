package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.collection.AbstractIterator;
import scala.collection.Iterator;
import scala.collection.immutable.VectorPointer;
import scala.math.package$;

public class VectorIterator<A> extends AbstractIterator<A> implements Iterator<A> {
    private final int _endIndex;
    private boolean _hasNext;
    private int blockIndex;
    private int depth;
    private Object[] display0;
    private Object[] display1;
    private Object[] display2;
    private Object[] display3;
    private Object[] display4;
    private Object[] display5;
    private int endIndex;
    private int endLo = package$.MODULE$.min(endIndex() - blockIndex(), 32);
    private int lo;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public VectorIterator(int i, int i2) {
        this._endIndex = i2;
        VectorPointer.Cclass.$init$(this);
        this.blockIndex = i & -32;
        this.lo = i & 31;
        this.endIndex = i2;
        this._hasNext = blockIndex() + lo() < endIndex();
    }

    private boolean _hasNext() {
        return this._hasNext;
    }

    private void _hasNext_$eq(boolean z) {
        this._hasNext = z;
    }

    private int blockIndex() {
        return this.blockIndex;
    }

    private void blockIndex_$eq(int i) {
        this.blockIndex = i;
    }

    private int endIndex() {
        return this.endIndex;
    }

    private int endLo() {
        return this.endLo;
    }

    private void endLo_$eq(int i) {
        this.endLo = i;
    }

    private int lo() {
        return this.lo;
    }

    private void lo_$eq(int i) {
        this.lo = i;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public final Object[] copyOf(Object[] objArr) {
        return VectorPointer.Cclass.copyOf(this, objArr);
    }

    public int depth() {
        return this.depth;
    }

    public void depth_$eq(int i) {
        this.depth = i;
    }

    public Object[] display0() {
        return this.display0;
    }

    public void display0_$eq(Object[] objArr) {
        this.display0 = objArr;
    }

    public Object[] display1() {
        return this.display1;
    }

    public void display1_$eq(Object[] objArr) {
        this.display1 = objArr;
    }

    public Object[] display2() {
        return this.display2;
    }

    public void display2_$eq(Object[] objArr) {
        this.display2 = objArr;
    }

    public Object[] display3() {
        return this.display3;
    }

    public void display3_$eq(Object[] objArr) {
        this.display3 = objArr;
    }

    public Object[] display4() {
        return this.display4;
    }

    public void display4_$eq(Object[] objArr) {
        this.display4 = objArr;
    }

    public Object[] display5() {
        return this.display5;
    }

    public void display5_$eq(Object[] objArr) {
        this.display5 = objArr;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public final void gotoNextBlockStart(int i, int i2) {
        VectorPointer.Cclass.gotoNextBlockStart(this, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public final void gotoPos(int i, int i2) {
        VectorPointer.Cclass.gotoPos(this, i, i2);
    }

    public boolean hasNext() {
        return _hasNext();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public final <U> void initFrom(VectorPointer<U> vectorPointer) {
        VectorPointer.Cclass.initFrom(this, vectorPointer);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public final <U> void initFrom(VectorPointer<U> vectorPointer, int i) {
        VectorPointer.Cclass.initFrom(this, vectorPointer, i);
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public A next() {
        if (_hasNext()) {
            A a = display0()[lo()];
            this.lo = lo() + 1;
            if (lo() == endLo()) {
                if (blockIndex() + lo() < endIndex()) {
                    int blockIndex2 = blockIndex() + 32;
                    VectorPointer.Cclass.gotoNextBlockStart(this, blockIndex2, blockIndex() ^ blockIndex2);
                    this.blockIndex = blockIndex2;
                    this.endLo = package$.MODULE$.min(endIndex() - blockIndex(), 32);
                    this.lo = 0;
                } else {
                    this._hasNext = false;
                }
            }
            return a;
        }
        throw new NoSuchElementException("reached iterator end");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public final Object[] nullSlotAndCopy(Object[] objArr, int i) {
        return VectorPointer.Cclass.nullSlotAndCopy(this, objArr, i);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.VectorIterator, scala.collection.immutable.VectorPointer] */
    public final void stabilize(int i) {
        VectorPointer.Cclass.stabilize(this, i);
    }
}
