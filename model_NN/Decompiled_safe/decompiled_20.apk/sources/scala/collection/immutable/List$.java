package scala.collection.immutable;

import scala.collection.Seq;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;

public final class List$ extends SeqFactory<List> {
    public static final List$ MODULE$ = null;

    static {
        new List$();
    }

    private List$() {
        MODULE$ = this;
    }

    public final <A> List<A> apply(Seq<A> seq) {
        return seq.toList();
    }

    public final <A> CanBuildFrom<List<?>, A, List<A>> canBuildFrom() {
        return ReusableCBF();
    }

    public final <A> List<A> empty() {
        return Nil$.MODULE$;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer, scala.collection.mutable.Builder<A, scala.collection.immutable.List<A>>] */
    public final <A> Builder<A, List<A>> newBuilder() {
        return new ListBuffer();
    }
}
