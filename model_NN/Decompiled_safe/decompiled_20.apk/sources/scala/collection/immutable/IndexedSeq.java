package scala.collection.immutable;

import scala.collection.IndexedSeqLike;
import scala.collection.generic.GenericCompanion;

public interface IndexedSeq<A> extends scala.collection.IndexedSeq<A>, IndexedSeqLike<A, IndexedSeq<A>> {

    /* renamed from: scala.collection.immutable.IndexedSeq$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(IndexedSeq indexedSeq) {
        }

        public static GenericCompanion companion(IndexedSeq indexedSeq) {
            return IndexedSeq$.MODULE$;
        }

        public static IndexedSeq seq(IndexedSeq indexedSeq) {
            return indexedSeq;
        }
    }

    IndexedSeq<A> seq();
}
