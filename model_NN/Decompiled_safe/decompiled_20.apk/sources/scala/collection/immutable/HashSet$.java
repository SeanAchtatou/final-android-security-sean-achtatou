package scala.collection.immutable;

import scala.Serializable;
import scala.collection.generic.ImmutableSetFactory;
import scala.collection.immutable.HashSet;

public final class HashSet$ extends ImmutableSetFactory<HashSet> implements Serializable {
    public static final HashSet$ MODULE$ = null;

    static {
        new HashSet$();
    }

    private HashSet$() {
        MODULE$ = this;
    }

    public final <A> HashSet<A> empty() {
        return HashSet$EmptyHashSet$.MODULE$;
    }

    public final <A> HashSet.HashTrieSet<A> scala$collection$immutable$HashSet$$makeHashTrieSet(int i, HashSet<A> hashSet, int i2, HashSet<A> hashSet2, int i3) {
        int i4 = (i >>> i3) & 31;
        int i5 = (i2 >>> i3) & 31;
        if (i4 != i5) {
            int i6 = (1 << i4) | (1 << i5);
            HashSet[] hashSetArr = new HashSet[2];
            if (i4 < i5) {
                hashSetArr[0] = hashSet;
                hashSetArr[1] = hashSet2;
            } else {
                hashSetArr[0] = hashSet2;
                hashSetArr[1] = hashSet;
            }
            return new HashSet.HashTrieSet<>(i6, hashSetArr, hashSet.size() + hashSet2.size());
        }
        HashSet.HashTrieSet<A> scala$collection$immutable$HashSet$$makeHashTrieSet = scala$collection$immutable$HashSet$$makeHashTrieSet(i, hashSet, i2, hashSet2, i3 + 5);
        return new HashSet.HashTrieSet<>(1 << i4, new HashSet[]{scala$collection$immutable$HashSet$$makeHashTrieSet}, scala$collection$immutable$HashSet$$makeHashTrieSet.size());
    }
}
