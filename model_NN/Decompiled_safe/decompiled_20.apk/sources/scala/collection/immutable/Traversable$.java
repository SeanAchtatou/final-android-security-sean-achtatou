package scala.collection.immutable;

import scala.collection.generic.GenTraversableFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;

public final class Traversable$ extends GenTraversableFactory<Traversable> {
    public static final Traversable$ MODULE$ = null;

    static {
        new Traversable$();
    }

    private Traversable$() {
        MODULE$ = this;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Builder<A, scala.collection.immutable.Traversable<A>>, scala.collection.mutable.ListBuffer] */
    public final <A> Builder<A, Traversable<A>> newBuilder() {
        return new ListBuffer();
    }
}
