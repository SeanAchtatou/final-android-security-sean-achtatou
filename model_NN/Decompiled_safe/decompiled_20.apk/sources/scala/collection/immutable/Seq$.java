package scala.collection.immutable;

import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;

public final class Seq$ extends SeqFactory<Seq> {
    public static final Seq$ MODULE$ = null;

    static {
        new Seq$();
    }

    private Seq$() {
        MODULE$ = this;
    }

    public final <A> CanBuildFrom<Seq<?>, A, Seq<A>> canBuildFrom() {
        return ReusableCBF();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer, scala.collection.mutable.Builder<A, scala.collection.immutable.Seq<A>>] */
    public final <A> Builder<A, Seq<A>> newBuilder() {
        return new ListBuffer();
    }
}
