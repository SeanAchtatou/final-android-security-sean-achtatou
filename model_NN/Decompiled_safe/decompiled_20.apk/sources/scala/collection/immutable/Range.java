package scala.collection.immutable;

import scala.Function1;
import scala.Predef$;
import scala.Serializable;
import scala.collection.AbstractSeq;
import scala.collection.CustomParallelizable;
import scala.collection.GenSeqLike;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.Iterator;
import scala.collection.generic.GenericCompanion;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.collection.parallel.immutable.ParRange;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;

public class Range extends AbstractSeq<Object> implements Serializable, CustomParallelizable<Object, ParRange>, IndexedSeq<Object> {
    private final int end;
    private final boolean isEmpty;
    private final int lastElement;
    private final int numRangeElements;
    private final int start;
    private final int step;
    private final int terminalElement;

    public class Inclusive extends Range {
        public Inclusive(int i, int i2, int i3) {
            super(i, i2, i3);
        }

        public Range copy(int i, int i2, int i3) {
            return new Inclusive(i, i2, i3);
        }

        public boolean isInclusive() {
            return true;
        }
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [scala.collection.immutable.Traversable, scala.collection.IndexedSeq, scala.collection.immutable.Iterable, scala.collection.IndexedSeqLike, scala.collection.CustomParallelizable, scala.collection.immutable.Range, scala.collection.immutable.Seq, scala.collection.immutable.IndexedSeq] */
    public Range(int i, int i2, int i3) {
        int i4 = 0;
        this.start = i;
        this.end = i2;
        this.step = i3;
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        CustomParallelizable.Cclass.$init$(this);
        this.isEmpty = (i > i2 && i3 > 0) || (i < i2 && i3 < 0) || (i == i2 && !isInclusive());
        if (i3 == 0) {
            throw new IllegalArgumentException("step cannot be 0.");
        }
        if (!isEmpty()) {
            long longLength = longLength();
            i4 = longLength > 2147483647L ? -1 : (int) longLength;
        }
        this.numRangeElements = i4;
        this.lastElement = ((numRangeElements() - 1) * i3) + i;
        this.terminalElement = (numRangeElements() * i3) + i;
    }

    private String description() {
        Predef$ predef$ = Predef$.MODULE$;
        StringOps stringOps = new StringOps("%d %s %d by %s");
        Predef$ predef$2 = Predef$.MODULE$;
        Object[] objArr = new Object[4];
        objArr[0] = BoxesRunTime.boxToInteger(start());
        objArr[1] = isInclusive() ? "to" : "until";
        objArr[2] = BoxesRunTime.boxToInteger(end());
        objArr[3] = BoxesRunTime.boxToInteger(step());
        return stringOps.format(predef$2.genericWrapArray(objArr));
    }

    private Nothing$ fail() {
        throw new IllegalArgumentException(new StringBuilder().append((Object) description()).append((Object) ": seqs cannot contain more than Int.MaxValue elements.").toString());
    }

    private long gap() {
        return ((long) end()) - ((long) start());
    }

    private boolean hasStub() {
        return isInclusive() || !isExact();
    }

    private boolean isExact() {
        return gap() % ((long) step()) == 0;
    }

    private int locationAfterN(int i) {
        return start() + (step() * i);
    }

    private long longLength() {
        return (gap() / ((long) step())) + ((long) (hasStub() ? 1 : 0));
    }

    private Range newEmptyRange(int i) {
        return new Range(i, i, step());
    }

    public final int apply(int i) {
        return apply$mcII$sp(i);
    }

    public final /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(obj)));
    }

    public int apply$mcII$sp(int i) {
        scala$collection$immutable$Range$$validateMaxLength();
        if (i >= 0 && i < numRangeElements()) {
            return start() + (step() * i);
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public GenericCompanion<IndexedSeq> companion() {
        return IndexedSeq.Cclass.companion(this);
    }

    public Range copy(int i, int i2, int i3) {
        return new Range(i, i2, i3);
    }

    public final Range drop(int i) {
        return (i <= 0 || isEmpty()) ? this : i >= numRangeElements() ? newEmptyRange(end()) : copy(locationAfterN(i), end(), step());
    }

    public int end() {
        return this.end;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Range)) {
            return GenSeqLike.Cclass.equals(this, obj);
        }
        Range range = (Range) obj;
        return range.canEqual(this) && length() == range.length() && (isEmpty() || (start() == range.start() && last() == range.last()));
    }

    public final <U> void foreach(Function1<Object, U> function1) {
        if (validateRangeBoundaries(function1)) {
            int terminalElement2 = terminalElement();
            int step2 = step();
            for (int start2 = start(); start2 != terminalElement2; start2 += step2) {
                function1.apply(BoxesRunTime.boxToInteger(start2));
            }
        }
    }

    public int hashCode() {
        return IndexedSeqLike.Cclass.hashCode(this);
    }

    public /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public final boolean isEmpty() {
        return this.isEmpty;
    }

    public boolean isInclusive() {
        return false;
    }

    public Iterator<Object> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public int last() {
        return isEmpty() ? BoxesRunTime.unboxToInt(Nil$.MODULE$.last()) : lastElement();
    }

    public final int lastElement() {
        return this.lastElement;
    }

    public int length() {
        if (numRangeElements() >= 0) {
            return numRangeElements();
        }
        throw fail();
    }

    public final int numRangeElements() {
        return this.numRangeElements;
    }

    public final Range reverse() {
        return isEmpty() ? this : new Inclusive(last(), start(), -step());
    }

    public void scala$collection$immutable$Range$$validateMaxLength() {
        if (numRangeElements() < 0) {
            throw fail();
        }
    }

    public IndexedSeq<Object> seq() {
        return IndexedSeq.Cclass.seq(this);
    }

    public int size() {
        return length();
    }

    public int start() {
        return this.start;
    }

    public int step() {
        return this.step;
    }

    public final Range tail() {
        if (isEmpty()) {
            Nil$.MODULE$.tail();
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        return drop(1);
    }

    public final Range take(int i) {
        return (i <= 0 || isEmpty()) ? newEmptyRange(start()) : i < numRangeElements() ? new Inclusive(start(), locationAfterN(i - 1), step()) : this;
    }

    public final int terminalElement() {
        return this.terminalElement;
    }

    public scala.collection.IndexedSeq<Object> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    public <A1> Buffer<A1> toBuffer() {
        return IndexedSeqLike.Cclass.toBuffer(this);
    }

    public scala.collection.IndexedSeq<Object> toCollection(IndexedSeq<Object> indexedSeq) {
        return IndexedSeqLike.Cclass.toCollection(this, indexedSeq);
    }

    public String toString() {
        return take(Range$.MODULE$.MAX_PRINT()).mkString("Range(", ", ", numRangeElements() > Range$.MODULE$.MAX_PRINT() ? ", ... )" : ")");
    }

    public boolean validateRangeBoundaries(Function1<Object, Object> function1) {
        scala$collection$immutable$Range$$validateMaxLength();
        if (start() != Integer.MIN_VALUE || end() != Integer.MIN_VALUE) {
            return true;
        }
        int start2 = start();
        int i = 0;
        while (i < numRangeElements()) {
            function1.apply(BoxesRunTime.boxToInteger(start2));
            i++;
            start2 += step();
        }
        return false;
    }
}
