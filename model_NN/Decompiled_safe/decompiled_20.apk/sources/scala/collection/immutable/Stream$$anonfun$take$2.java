package scala.collection.immutable;

import scala.Serializable;
import scala.runtime.AbstractFunction0;

public final class Stream$$anonfun$take$2 extends AbstractFunction0<Stream<A>> implements Serializable {
    private final /* synthetic */ Stream $outer;
    private final int n$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [int, scala.collection.immutable.Stream<A>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Stream$$anonfun$take$2(scala.collection.immutable.Stream r2, scala.collection.immutable.Stream<A> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.n$1 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Stream$$anonfun$take$2.<init>(scala.collection.immutable.Stream, int):void");
    }

    public final Stream<A> apply() {
        return ((Stream) this.$outer.tail()).take(this.n$1 - 1);
    }
}
