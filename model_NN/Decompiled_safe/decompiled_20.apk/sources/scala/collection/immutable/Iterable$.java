package scala.collection.immutable;

import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenTraversableFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;

public final class Iterable$ extends GenTraversableFactory<Iterable> {
    public static final Iterable$ MODULE$ = null;

    static {
        new Iterable$();
    }

    private Iterable$() {
        MODULE$ = this;
    }

    public final <A> CanBuildFrom<Iterable<?>, A, Iterable<A>> canBuildFrom() {
        return ReusableCBF();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer, scala.collection.mutable.Builder<A, scala.collection.immutable.Iterable<A>>] */
    public final <A> Builder<A, Iterable<A>> newBuilder() {
        return new ListBuffer();
    }
}
