package scala.collection.immutable;

import scala.Serializable;
import scala.runtime.AbstractFunction0;
import scala.runtime.Nothing$;

public final class StreamIterator$$anonfun$toStream$1 extends AbstractFunction0<Stream<Nothing$>> implements Serializable {
    public StreamIterator$$anonfun$toStream$1(StreamIterator<A> streamIterator) {
    }

    public final Stream<Nothing$> apply() {
        return Stream$.MODULE$.empty();
    }
}
