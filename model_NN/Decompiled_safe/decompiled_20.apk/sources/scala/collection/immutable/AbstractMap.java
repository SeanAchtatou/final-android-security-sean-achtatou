package scala.collection.immutable;

import scala.Function1;
import scala.Predef$$less$colon$less;
import scala.Tuple2;
import scala.collection.generic.GenericCompanion;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Map;
import scala.collection.immutable.MapLike;
import scala.collection.immutable.Traversable;

public abstract class AbstractMap<A, B> extends scala.collection.AbstractMap<A, B> implements Map<A, B> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.immutable.MapLike, scala.collection.immutable.Iterable, scala.collection.immutable.Map, scala.collection.immutable.AbstractMap] */
    public AbstractMap() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public Map<A, B> empty() {
        return Map.Cclass.empty(this);
    }

    public /* bridge */ /* synthetic */ Object filterNot(Function1 function1) {
        return filterNot(function1);
    }

    public Map<A, B> seq() {
        return Map.Cclass.seq(this);
    }

    public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
        return thisCollection();
    }

    public <T, U> Map<T, U> toMap(Predef$$less$colon$less<Tuple2<A, B>, Tuple2<T, U>> predef$$less$colon$less) {
        return Map.Cclass.toMap(this, predef$$less$colon$less);
    }

    public <B1> Map<A, B1> updated(A a, B1 b1) {
        return MapLike.Cclass.updated(this, a, b1);
    }
}
