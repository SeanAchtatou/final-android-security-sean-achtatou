package scala.collection.immutable;

import scala.Serializable;
import scala.collection.generic.BitSetFactory;
import scala.collection.immutable.BitSet;

public final class BitSet$ implements Serializable, BitSetFactory<BitSet> {
    public static final BitSet$ MODULE$ = null;
    private final BitSet empty = new BitSet.BitSet1(0);

    static {
        new BitSet$();
    }

    private BitSet$() {
        MODULE$ = this;
        BitSetFactory.Cclass.$init$(this);
    }

    public final BitSet empty() {
        return this.empty;
    }

    public final BitSet fromBitMaskNoCopy(long[] jArr) {
        int length = jArr.length;
        return length == 0 ? empty() : length == 1 ? new BitSet.BitSet1(jArr[0]) : length == 2 ? new BitSet.BitSet2(jArr[0], jArr[1]) : new BitSet.BitSetN(jArr);
    }
}
