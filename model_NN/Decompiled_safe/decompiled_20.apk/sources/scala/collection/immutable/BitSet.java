package scala.collection.immutable;

import scala.Function1;
import scala.Predef$;
import scala.Serializable;
import scala.collection.AbstractSet;
import scala.collection.BitSet;
import scala.collection.BitSetLike;
import scala.collection.BitSetLike$;
import scala.collection.GenSet;
import scala.collection.GenSetLike;
import scala.collection.Iterator;
import scala.collection.SortedSet;
import scala.collection.SortedSetLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.Sorted;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Set;
import scala.collection.immutable.SortedSet;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.StringBuilder;
import scala.math.Ordering;
import scala.runtime.BoxesRunTime;

public abstract class BitSet extends AbstractSet<Object> implements Serializable, scala.collection.BitSet, BitSetLike<BitSet> {

    public class BitSet1 extends BitSet {
        private final long elems;

        public BitSet1(long j) {
            this.elems = j;
        }

        public long elems() {
            return this.elems;
        }

        public int nwords() {
            return 1;
        }

        public BitSet updateWord(int i, long j) {
            if (i == 0) {
                return new BitSet1(j);
            }
            if (i == 1) {
                return new BitSet2(elems(), j);
            }
            return fromBitMaskNoCopy(BitSetLike$.MODULE$.updateArray(new long[]{elems()}, i, j));
        }

        public long word(int i) {
            if (i == 0) {
                return elems();
            }
            return 0;
        }
    }

    public class BitSet2 extends BitSet {
        private final long elems0;
        private final long elems1;

        public BitSet2(long j, long j2) {
            this.elems0 = j;
            this.elems1 = j2;
        }

        public long elems0() {
            return this.elems0;
        }

        public int nwords() {
            return 2;
        }

        public BitSet updateWord(int i, long j) {
            if (i == 0) {
                return new BitSet2(j, this.elems1);
            }
            if (i == 1) {
                return new BitSet2(elems0(), j);
            }
            return fromBitMaskNoCopy(BitSetLike$.MODULE$.updateArray(new long[]{elems0(), this.elems1}, i, j));
        }

        public long word(int i) {
            if (i == 0) {
                return elems0();
            }
            if (i == 1) {
                return this.elems1;
            }
            return 0;
        }
    }

    public class BitSetN extends BitSet {
        private final long[] elems;

        public BitSetN(long[] jArr) {
            this.elems = jArr;
        }

        public long[] elems() {
            return this.elems;
        }

        public int nwords() {
            return elems().length;
        }

        public BitSet updateWord(int i, long j) {
            return fromBitMaskNoCopy(BitSetLike$.MODULE$.updateArray(elems(), i, j));
        }

        public long word(int i) {
            if (i < nwords()) {
                return elems()[i];
            }
            return 0;
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.SortedSet, scala.collection.immutable.SortedSet, scala.collection.generic.Sorted, scala.collection.immutable.Iterable, scala.collection.SortedSetLike, scala.collection.immutable.BitSet, scala.collection.BitSetLike, scala.collection.BitSet, scala.collection.immutable.Set] */
    public BitSet() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Set.Cclass.$init$(this);
        Sorted.Cclass.$init$(this);
        SortedSetLike.Cclass.$init$(this);
        SortedSet.Cclass.$init$(this);
        SortedSet.Cclass.$init$(this);
        BitSetLike.Cclass.$init$(this);
        BitSet.Cclass.$init$(this);
    }

    public /* synthetic */ scala.collection.Set $minus(Object obj) {
        return $minus(BoxesRunTime.unboxToInt(obj));
    }

    public BitSet $minus(int i) {
        Predef$ predef$ = Predef$.MODULE$;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(new StringBuilder().append((Object) "requirement failed: ").append((Object) "bitset element must be >= 0").toString());
        } else if (!contains(i)) {
            return this;
        } else {
            int LogWL = i >> BitSetLike$.MODULE$.LogWL();
            return updateWord(LogWL, word(LogWL) & ((1 << i) ^ -1));
        }
    }

    public /* synthetic */ scala.collection.Set $plus(Object obj) {
        return $plus(BoxesRunTime.unboxToInt(obj));
    }

    public BitSet $plus(int i) {
        Predef$ predef$ = Predef$.MODULE$;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(new StringBuilder().append((Object) "requirement failed: ").append((Object) "bitset element must be >= 0").toString());
        } else if (contains(i)) {
            return this;
        } else {
            int LogWL = i >> BitSetLike$.MODULE$.LogWL();
            return updateWord(LogWL, word(LogWL) | (1 << i));
        }
    }

    public StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
        return BitSetLike.Cclass.addString(this, stringBuilder, str, str2, str3);
    }

    public /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply(obj));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.BitSet, scala.collection.immutable.Set] */
    public GenericCompanion<Set> companion() {
        return Set.Cclass.companion(this);
    }

    public int compare(Object obj, Object obj2) {
        return Sorted.Cclass.compare(this, obj, obj2);
    }

    public boolean contains(int i) {
        return BitSetLike.Cclass.contains(this, i);
    }

    public /* synthetic */ boolean contains(Object obj) {
        return contains(BoxesRunTime.unboxToInt(obj));
    }

    public BitSet empty() {
        return BitSet$.MODULE$.empty();
    }

    public <B> void foreach(Function1<Object, B> function1) {
        BitSetLike.Cclass.foreach(this, function1);
    }

    public BitSet fromBitMaskNoCopy(long[] jArr) {
        return BitSet$.MODULE$.fromBitMaskNoCopy(jArr);
    }

    public boolean hasAll(Iterator<Object> iterator) {
        return Sorted.Cclass.hasAll(this, iterator);
    }

    public Iterator<Object> iterator() {
        return BitSetLike.Cclass.iterator(this);
    }

    public scala.collection.SortedSet keySet() {
        return SortedSetLike.Cclass.keySet(this);
    }

    public Ordering<Object> ordering() {
        return BitSetLike.Cclass.ordering(this);
    }

    public boolean scala$collection$SortedSetLike$$super$subsetOf(GenSet genSet) {
        return GenSetLike.Cclass.subsetOf(this, genSet);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.BitSet, scala.collection.immutable.Set] */
    public Set<Object> seq() {
        return Set.Cclass.seq(this);
    }

    public int size() {
        return BitSetLike.Cclass.size(this);
    }

    public String stringPrefix() {
        return BitSetLike.Cclass.stringPrefix(this);
    }

    public boolean subsetOf(GenSet<Object> genSet) {
        return SortedSetLike.Cclass.subsetOf(this, genSet);
    }

    public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
        return thisCollection();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.BitSet, scala.collection.immutable.Set] */
    public <B> Set<B> toSet() {
        return Set.Cclass.toSet(this);
    }

    public abstract BitSet updateWord(int i, long j);
}
