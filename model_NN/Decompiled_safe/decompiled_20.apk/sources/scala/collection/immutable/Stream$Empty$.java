package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Serializable;
import scala.runtime.Nothing$;

public class Stream$Empty$ extends Stream<Nothing$> implements Serializable {
    public static final Stream$Empty$ MODULE$ = null;

    static {
        new Stream$Empty$();
    }

    public Stream$Empty$() {
        MODULE$ = this;
    }

    public Nothing$ head() {
        throw new NoSuchElementException("head of empty stream");
    }

    public boolean isEmpty() {
        return true;
    }

    public Nothing$ tail() {
        throw new UnsupportedOperationException("tail of empty stream");
    }

    public boolean tailDefined() {
        return false;
    }
}
