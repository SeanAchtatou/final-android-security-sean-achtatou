package scala.collection.immutable;

import scala.Function2;
import scala.Tuple2;
import scala.collection.immutable.HashMap;

public final class HashMap$$anon$2 extends HashMap.Merger<A1, B1> {
    private final HashMap.Merger<A1, B1> invert = new HashMap$$anon$2$$anon$3(this);
    public final Function2 mergef$1;

    public HashMap$$anon$2(Function2 function2) {
        this.mergef$1 = function2;
    }

    public final Tuple2<A1, B1> apply(Tuple2<A1, B1> tuple2, Tuple2<A1, B1> tuple22) {
        return (Tuple2) this.mergef$1.apply(tuple2, tuple22);
    }
}
