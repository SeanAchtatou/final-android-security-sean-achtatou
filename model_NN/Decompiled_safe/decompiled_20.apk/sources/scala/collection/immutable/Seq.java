package scala.collection.immutable;

import scala.collection.Parallelizable;
import scala.collection.generic.GenericCompanion;
import scala.collection.parallel.immutable.ParSeq;

public interface Seq<A> extends Parallelizable<A, ParSeq<A>>, scala.collection.Seq<A> {

    /* renamed from: scala.collection.immutable.Seq$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Seq seq) {
        }

        public static GenericCompanion companion(Seq seq) {
            return Seq$.MODULE$;
        }

        public static Seq seq(Seq seq) {
            return seq;
        }
    }

    Seq<A> seq();
}
