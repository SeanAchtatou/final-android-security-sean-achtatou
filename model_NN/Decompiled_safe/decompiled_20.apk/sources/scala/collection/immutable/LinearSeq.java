package scala.collection.immutable;

import scala.collection.LinearSeqLike;

public interface LinearSeq<A> extends scala.collection.LinearSeq<A>, LinearSeqLike<A, LinearSeq<A>> {

    /* renamed from: scala.collection.immutable.LinearSeq$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(LinearSeq linearSeq) {
        }

        public static LinearSeq seq(LinearSeq linearSeq) {
            return linearSeq;
        }
    }

    LinearSeq<A> seq();
}
