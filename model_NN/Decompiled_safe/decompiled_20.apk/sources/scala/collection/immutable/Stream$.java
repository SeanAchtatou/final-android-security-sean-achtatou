package scala.collection.immutable;

import scala.Function1;
import scala.PartialFunction;
import scala.collection.Seq;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;

public final class Stream$ extends SeqFactory<Stream> {
    public static final Stream$ MODULE$ = null;

    static {
        new Stream$();
    }

    private Stream$() {
        MODULE$ = this;
    }

    public final <A> Stream<A> apply(Seq<A> seq) {
        return seq.toStream();
    }

    public final <A> CanBuildFrom<Stream<?>, A, Stream<A>> canBuildFrom() {
        return new Stream.StreamCanBuildFrom();
    }

    public final <A, B, That> Stream.Cons<B> collectedTail(Stream<A> stream, PartialFunction<A, B> partialFunction, CanBuildFrom<Stream<A>, B, That> canBuildFrom) {
        Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
        return new Stream.Cons<>(partialFunction.apply(stream.head()), new Stream$$anonfun$collectedTail$1(stream, partialFunction, canBuildFrom));
    }

    public final <A> Stream<A> empty() {
        return Stream$Empty$.MODULE$;
    }

    public final <A> Stream.Cons<A> filteredTail(Stream<A> stream, Function1<A, Object> function1) {
        Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
        return new Stream.Cons<>(stream.head(), new Stream$$anonfun$filteredTail$1(stream, function1));
    }

    public final <A> Builder<A, Stream<A>> newBuilder() {
        return new Stream.StreamBuilder();
    }
}
