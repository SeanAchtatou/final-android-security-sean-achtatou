package scala.collection.immutable;

import scala.Serializable;
import scala.collection.generic.ImmutableSetFactory;
import scala.collection.immutable.ListSet;
import scala.collection.mutable.Builder;

public final class ListSet$ extends ImmutableSetFactory<ListSet> implements Serializable {
    public static final ListSet$ MODULE$ = null;

    static {
        new ListSet$();
    }

    private ListSet$() {
        MODULE$ = this;
    }

    public final <A> ListSet<A> empty() {
        return ListSet$EmptyListSet$.MODULE$;
    }

    public final <A> Builder<A, ListSet<A>> newBuilder() {
        return new ListSet.ListSetBuilder();
    }
}
