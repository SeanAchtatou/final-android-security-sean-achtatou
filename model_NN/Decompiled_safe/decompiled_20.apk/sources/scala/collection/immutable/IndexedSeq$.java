package scala.collection.immutable;

import scala.collection.generic.GenTraversableFactory;
import scala.collection.generic.SeqFactory;
import scala.collection.mutable.Builder;
import scala.runtime.Nothing$;

public final class IndexedSeq$ extends SeqFactory<IndexedSeq> {
    public static final IndexedSeq$ MODULE$ = null;
    private GenTraversableFactory<IndexedSeq>.GenericCanBuildFrom<Nothing$> ReusableCBF;
    private volatile boolean bitmap$0;

    static {
        new IndexedSeq$();
    }

    private IndexedSeq$() {
        MODULE$ = this;
    }

    private GenTraversableFactory.GenericCanBuildFrom ReusableCBF$lzycompute() {
        synchronized (this) {
            if (!this.bitmap$0) {
                this.ReusableCBF = scala.collection.IndexedSeq$.MODULE$.ReusableCBF();
                this.bitmap$0 = true;
            }
        }
        return this.ReusableCBF;
    }

    public final GenTraversableFactory<IndexedSeq>.GenericCanBuildFrom<Nothing$> ReusableCBF() {
        return this.bitmap$0 ? this.ReusableCBF : ReusableCBF$lzycompute();
    }

    public final <A> Builder<A, IndexedSeq<A>> newBuilder() {
        return Vector$.MODULE$.newBuilder();
    }
}
