package scala.collection.immutable;

import scala.Serializable;
import scala.collection.GenTraversableOnce;
import scala.runtime.AbstractFunction0;

public final class Stream$$anonfun$$plus$plus$1 extends AbstractFunction0<Stream<A>> implements Serializable {
    private final /* synthetic */ Stream $outer;
    private final GenTraversableOnce that$1;

    public Stream$$anonfun$$plus$plus$1(Stream stream, Stream<A> stream2) {
        if (stream == null) {
            throw new NullPointerException();
        }
        this.$outer = stream;
        this.that$1 = stream2;
    }

    public final Stream<A> apply() {
        return (Stream) ((Stream) this.$outer.tail()).$plus$plus(this.that$1, Stream$.MODULE$.canBuildFrom());
    }
}
