package scala.collection.immutable;

import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.collection.mutable.StringBuilder$;

public final class WrappedString$ {
    public static final WrappedString$ MODULE$ = null;

    static {
        new WrappedString$();
    }

    private WrappedString$() {
        MODULE$ = this;
    }

    public final Builder<Object, WrappedString> newBuilder() {
        StringBuilder$ stringBuilder$ = StringBuilder$.MODULE$;
        return Builder.Cclass.mapResult(new StringBuilder(), new WrappedString$$anonfun$newBuilder$1());
    }
}
