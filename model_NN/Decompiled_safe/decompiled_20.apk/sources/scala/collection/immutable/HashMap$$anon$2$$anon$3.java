package scala.collection.immutable;

import scala.Tuple2;
import scala.collection.immutable.HashMap;

public final class HashMap$$anon$2$$anon$3 extends HashMap.Merger<A1, B1> {
    private final /* synthetic */ HashMap$$anon$2 $outer;

    public HashMap$$anon$2$$anon$3(HashMap$$anon$2 hashMap$$anon$2) {
        if (hashMap$$anon$2 == null) {
            throw new NullPointerException();
        }
        this.$outer = hashMap$$anon$2;
    }

    public final Tuple2<A1, B1> apply(Tuple2<A1, B1> tuple2, Tuple2<A1, B1> tuple22) {
        return (Tuple2) this.$outer.mergef$1.apply(tuple22, tuple2);
    }
}
