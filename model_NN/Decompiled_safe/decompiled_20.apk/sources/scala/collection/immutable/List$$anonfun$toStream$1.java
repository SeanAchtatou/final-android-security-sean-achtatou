package scala.collection.immutable;

import scala.Serializable;
import scala.runtime.AbstractFunction0;

public final class List$$anonfun$toStream$1 extends AbstractFunction0<Stream<A>> implements Serializable {
    private final /* synthetic */ List $outer;

    public List$$anonfun$toStream$1(List<A> list) {
        if (list == null) {
            throw new NullPointerException();
        }
        this.$outer = list;
    }

    public final Stream<A> apply() {
        return ((List) this.$outer.tail()).toStream();
    }
}
