package scala.collection.immutable;

import scala.Function1;
import scala.Predef$;
import scala.Serializable;
import scala.collection.AbstractSet;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.Parallelizable;
import scala.collection.generic.GenericCompanion;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Traversable;
import scala.collection.parallel.immutable.ParSet;
import scala.runtime.BoxesRunTime;

public interface Set<A> extends Parallelizable<A, ParSet<A>>, scala.collection.Set<A> {

    public class Set1<A> extends AbstractSet<A> implements Serializable, Set<A> {
        private final A elem1;

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.immutable.Iterable, scala.collection.immutable.Set$Set1, scala.collection.immutable.Set] */
        public Set1(A a) {
            this.elem1 = a;
            Traversable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public Set<A> $minus(A a) {
            A a2 = this.elem1;
            return a == a2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, a2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, a2) : a.equals(a2) ? Set$.MODULE$.empty() : this;
        }

        public Set<A> $plus(A a) {
            return contains(a) ? this : new Set2(this.elem1, a);
        }

        public /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToBoolean(apply(obj));
        }

        public GenericCompanion<Set> companion() {
            return Cclass.companion(this);
        }

        public boolean contains(A a) {
            A a2 = this.elem1;
            return a == a2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, a2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, a2) : a.equals(a2);
        }

        public /* bridge */ /* synthetic */ scala.collection.Set empty() {
            return (scala.collection.Set) empty();
        }

        public <U> void foreach(Function1<A, U> function1) {
            function1.apply(this.elem1);
        }

        public Iterator<A> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.genericWrapArray(new Object[]{this.elem1}));
        }

        public Set<A> seq() {
            return Cclass.seq(this);
        }

        public int size() {
            return 1;
        }

        public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
            return thisCollection();
        }

        public <B> Set<B> toSet() {
            return Cclass.toSet(this);
        }
    }

    public class Set2<A> extends AbstractSet<A> implements Serializable, Set<A> {
        private final A elem1;
        private final A elem2;

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.immutable.Iterable, scala.collection.immutable.Set, scala.collection.immutable.Set$Set2] */
        public Set2(A a, A a2) {
            this.elem1 = a;
            this.elem2 = a2;
            Traversable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Set<A> $minus(A r5) {
            /*
                r4 = this;
                r1 = 1
                r2 = 0
                A r3 = r4.elem1
                if (r5 != r3) goto L_0x0012
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0033
                scala.collection.immutable.Set$Set1 r0 = new scala.collection.immutable.Set$Set1
                A r1 = r4.elem2
                r0.<init>(r1)
                r4 = r0
            L_0x0011:
                return r4
            L_0x0012:
                if (r5 != 0) goto L_0x0016
                r0 = r2
                goto L_0x0007
            L_0x0016:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0022
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0022:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x002e
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x002e:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0033:
                A r0 = r4.elem2
                if (r5 != r0) goto L_0x0043
                r0 = r1
            L_0x0038:
                if (r0 == 0) goto L_0x0011
                scala.collection.immutable.Set$Set1 r0 = new scala.collection.immutable.Set$Set1
                A r1 = r4.elem1
                r0.<init>(r1)
                r4 = r0
                goto L_0x0011
            L_0x0043:
                if (r5 != 0) goto L_0x0047
                r0 = r2
                goto L_0x0038
            L_0x0047:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0052
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x0038
            L_0x0052:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x005d
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x0038
            L_0x005d:
                boolean r0 = r5.equals(r0)
                goto L_0x0038
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set2.$minus(java.lang.Object):scala.collection.immutable.Set");
        }

        public Set<A> $plus(A a) {
            return contains(a) ? this : new Set3(this.elem1, this.elem2, a);
        }

        public /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToBoolean(apply(obj));
        }

        public GenericCompanion<Set> companion() {
            return Cclass.companion(this);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(A r5) {
            /*
                r4 = this;
                r2 = 1
                r1 = 0
                A r3 = r4.elem1
                if (r5 != r3) goto L_0x0012
                r0 = r2
            L_0x0007:
                if (r0 != 0) goto L_0x0010
                A r0 = r4.elem2
                if (r5 != r0) goto L_0x0033
                r0 = r2
            L_0x000e:
                if (r0 == 0) goto L_0x0052
            L_0x0010:
                r0 = r2
            L_0x0011:
                return r0
            L_0x0012:
                if (r5 != 0) goto L_0x0016
                r0 = r1
                goto L_0x0007
            L_0x0016:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0022
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0022:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x002e
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x002e:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0033:
                if (r5 != 0) goto L_0x0037
                r0 = r1
                goto L_0x000e
            L_0x0037:
                boolean r3 = r5 instanceof java.lang.Number
                if (r3 == 0) goto L_0x0042
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x000e
            L_0x0042:
                boolean r3 = r5 instanceof java.lang.Character
                if (r3 == 0) goto L_0x004d
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x000e
            L_0x004d:
                boolean r0 = r5.equals(r0)
                goto L_0x000e
            L_0x0052:
                r0 = r1
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set2.contains(java.lang.Object):boolean");
        }

        public /* bridge */ /* synthetic */ scala.collection.Set empty() {
            return (scala.collection.Set) empty();
        }

        public <U> void foreach(Function1<A, U> function1) {
            function1.apply(this.elem1);
            function1.apply(this.elem2);
        }

        public Iterator<A> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.genericWrapArray(new Object[]{this.elem1, this.elem2}));
        }

        public Set<A> seq() {
            return Cclass.seq(this);
        }

        public int size() {
            return 2;
        }

        public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
            return thisCollection();
        }

        public <B> Set<B> toSet() {
            return Cclass.toSet(this);
        }
    }

    public class Set3<A> extends AbstractSet<A> implements Serializable, Set<A> {
        private final A elem1;
        private final A elem2;
        private final A elem3;

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.immutable.Iterable, scala.collection.immutable.Set$Set3, scala.collection.immutable.Set] */
        public Set3(A a, A a2, A a3) {
            this.elem1 = a;
            this.elem2 = a2;
            this.elem3 = a3;
            Traversable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Set<A> $minus(A r5) {
            /*
                r4 = this;
                r1 = 1
                r2 = 0
                A r3 = r4.elem1
                if (r5 != r3) goto L_0x0014
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0035
                scala.collection.immutable.Set$Set2 r0 = new scala.collection.immutable.Set$Set2
                A r1 = r4.elem2
                A r2 = r4.elem3
                r0.<init>(r1, r2)
                r4 = r0
            L_0x0013:
                return r4
            L_0x0014:
                if (r5 != 0) goto L_0x0018
                r0 = r2
                goto L_0x0007
            L_0x0018:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0024
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0024:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0030
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x0030:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0035:
                A r3 = r4.elem2
                if (r5 != r3) goto L_0x0047
                r0 = r1
            L_0x003a:
                if (r0 == 0) goto L_0x0068
                scala.collection.immutable.Set$Set2 r0 = new scala.collection.immutable.Set$Set2
                A r1 = r4.elem1
                A r2 = r4.elem3
                r0.<init>(r1, r2)
                r4 = r0
                goto L_0x0013
            L_0x0047:
                if (r5 != 0) goto L_0x004b
                r0 = r2
                goto L_0x003a
            L_0x004b:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0057
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x003a
            L_0x0057:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0063
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x003a
            L_0x0063:
                boolean r0 = r5.equals(r3)
                goto L_0x003a
            L_0x0068:
                A r0 = r4.elem3
                if (r5 != r0) goto L_0x007a
                r0 = r1
            L_0x006d:
                if (r0 == 0) goto L_0x0013
                scala.collection.immutable.Set$Set2 r0 = new scala.collection.immutable.Set$Set2
                A r1 = r4.elem1
                A r2 = r4.elem2
                r0.<init>(r1, r2)
                r4 = r0
                goto L_0x0013
            L_0x007a:
                if (r5 != 0) goto L_0x007e
                r0 = r2
                goto L_0x006d
            L_0x007e:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0089
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x006d
            L_0x0089:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0094
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x006d
            L_0x0094:
                boolean r0 = r5.equals(r0)
                goto L_0x006d
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set3.$minus(java.lang.Object):scala.collection.immutable.Set");
        }

        public Set<A> $plus(A a) {
            return contains(a) ? this : new Set4(this.elem1, this.elem2, this.elem3, a);
        }

        public /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToBoolean(apply(obj));
        }

        public GenericCompanion<Set> companion() {
            return Cclass.companion(this);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(A r5) {
            /*
                r4 = this;
                r2 = 1
                r1 = 0
                A r3 = r4.elem1
                if (r5 != r3) goto L_0x0019
                r0 = r2
            L_0x0007:
                if (r0 != 0) goto L_0x0017
                A r3 = r4.elem2
                if (r5 != r3) goto L_0x003a
                r0 = r2
            L_0x000e:
                if (r0 != 0) goto L_0x0017
                A r0 = r4.elem3
                if (r5 != r0) goto L_0x005b
                r0 = r2
            L_0x0015:
                if (r0 == 0) goto L_0x007a
            L_0x0017:
                r0 = r2
            L_0x0018:
                return r0
            L_0x0019:
                if (r5 != 0) goto L_0x001d
                r0 = r1
                goto L_0x0007
            L_0x001d:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0029
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0029:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0035
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x0035:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x003a:
                if (r5 != 0) goto L_0x003e
                r0 = r1
                goto L_0x000e
            L_0x003e:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x004a
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x000e
            L_0x004a:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0056
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x000e
            L_0x0056:
                boolean r0 = r5.equals(r3)
                goto L_0x000e
            L_0x005b:
                if (r5 != 0) goto L_0x005f
                r0 = r1
                goto L_0x0015
            L_0x005f:
                boolean r3 = r5 instanceof java.lang.Number
                if (r3 == 0) goto L_0x006a
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x0015
            L_0x006a:
                boolean r3 = r5 instanceof java.lang.Character
                if (r3 == 0) goto L_0x0075
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x0015
            L_0x0075:
                boolean r0 = r5.equals(r0)
                goto L_0x0015
            L_0x007a:
                r0 = r1
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set3.contains(java.lang.Object):boolean");
        }

        public /* bridge */ /* synthetic */ scala.collection.Set empty() {
            return (scala.collection.Set) empty();
        }

        public <U> void foreach(Function1<A, U> function1) {
            function1.apply(this.elem1);
            function1.apply(this.elem2);
            function1.apply(this.elem3);
        }

        public Iterator<A> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.genericWrapArray(new Object[]{this.elem1, this.elem2, this.elem3}));
        }

        public Set<A> seq() {
            return Cclass.seq(this);
        }

        public int size() {
            return 3;
        }

        public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
            return thisCollection();
        }

        public <B> Set<B> toSet() {
            return Cclass.toSet(this);
        }
    }

    public class Set4<A> extends AbstractSet<A> implements Serializable, Set<A> {
        private final A elem1;
        private final A elem2;
        private final A elem3;
        private final A elem4;

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Set$Set4, scala.collection.immutable.Traversable, scala.collection.immutable.Iterable, scala.collection.immutable.Set] */
        public Set4(A a, A a2, A a3, A a4) {
            this.elem1 = a;
            this.elem2 = a2;
            this.elem3 = a3;
            this.elem4 = a4;
            Traversable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Set<A> $minus(A r5) {
            /*
                r4 = this;
                r1 = 1
                r2 = 0
                A r3 = r4.elem1
                if (r5 != r3) goto L_0x0016
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0037
                scala.collection.immutable.Set$Set3 r0 = new scala.collection.immutable.Set$Set3
                A r1 = r4.elem2
                A r2 = r4.elem3
                A r3 = r4.elem4
                r0.<init>(r1, r2, r3)
                r4 = r0
            L_0x0015:
                return r4
            L_0x0016:
                if (r5 != 0) goto L_0x001a
                r0 = r2
                goto L_0x0007
            L_0x001a:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0026
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0026:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0032
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x0032:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0037:
                A r3 = r4.elem2
                if (r5 != r3) goto L_0x004b
                r0 = r1
            L_0x003c:
                if (r0 == 0) goto L_0x006c
                scala.collection.immutable.Set$Set3 r0 = new scala.collection.immutable.Set$Set3
                A r1 = r4.elem1
                A r2 = r4.elem3
                A r3 = r4.elem4
                r0.<init>(r1, r2, r3)
                r4 = r0
                goto L_0x0015
            L_0x004b:
                if (r5 != 0) goto L_0x004f
                r0 = r2
                goto L_0x003c
            L_0x004f:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x005b
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x003c
            L_0x005b:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0067
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x003c
            L_0x0067:
                boolean r0 = r5.equals(r3)
                goto L_0x003c
            L_0x006c:
                A r3 = r4.elem3
                if (r5 != r3) goto L_0x0080
                r0 = r1
            L_0x0071:
                if (r0 == 0) goto L_0x00a1
                scala.collection.immutable.Set$Set3 r0 = new scala.collection.immutable.Set$Set3
                A r1 = r4.elem1
                A r2 = r4.elem2
                A r3 = r4.elem4
                r0.<init>(r1, r2, r3)
                r4 = r0
                goto L_0x0015
            L_0x0080:
                if (r5 != 0) goto L_0x0084
                r0 = r2
                goto L_0x0071
            L_0x0084:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0090
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0071
            L_0x0090:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x009c
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0071
            L_0x009c:
                boolean r0 = r5.equals(r3)
                goto L_0x0071
            L_0x00a1:
                A r0 = r4.elem4
                if (r5 != r0) goto L_0x00b6
                r0 = r1
            L_0x00a6:
                if (r0 == 0) goto L_0x0015
                scala.collection.immutable.Set$Set3 r0 = new scala.collection.immutable.Set$Set3
                A r1 = r4.elem1
                A r2 = r4.elem2
                A r3 = r4.elem3
                r0.<init>(r1, r2, r3)
                r4 = r0
                goto L_0x0015
            L_0x00b6:
                if (r5 != 0) goto L_0x00ba
                r0 = r2
                goto L_0x00a6
            L_0x00ba:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x00c5
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x00a6
            L_0x00c5:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x00d0
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x00a6
            L_0x00d0:
                boolean r0 = r5.equals(r0)
                goto L_0x00a6
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set4.$minus(java.lang.Object):scala.collection.immutable.Set");
        }

        /* JADX WARN: Type inference failed for: r7v1, types: [scala.collection.immutable.HashSet, scala.collection.immutable.Set<A>] */
        public Set<A> $plus(A a) {
            if (contains(a)) {
                return this;
            }
            return new HashSet().$plus(this.elem1, this.elem2, Predef$.MODULE$.genericWrapArray(new Object[]{this.elem3, this.elem4, a}));
        }

        public /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToBoolean(apply(obj));
        }

        public GenericCompanion<Set> companion() {
            return Cclass.companion(this);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(A r5) {
            /*
                r4 = this;
                r2 = 1
                r1 = 0
                A r3 = r4.elem1
                if (r5 != r3) goto L_0x0020
                r0 = r2
            L_0x0007:
                if (r0 != 0) goto L_0x001e
                A r3 = r4.elem2
                if (r5 != r3) goto L_0x0041
                r0 = r2
            L_0x000e:
                if (r0 != 0) goto L_0x001e
                A r3 = r4.elem3
                if (r5 != r3) goto L_0x0062
                r0 = r2
            L_0x0015:
                if (r0 != 0) goto L_0x001e
                A r0 = r4.elem4
                if (r5 != r0) goto L_0x0083
                r0 = r2
            L_0x001c:
                if (r0 == 0) goto L_0x00a3
            L_0x001e:
                r0 = r2
            L_0x001f:
                return r0
            L_0x0020:
                if (r5 != 0) goto L_0x0024
                r0 = r1
                goto L_0x0007
            L_0x0024:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0030
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0030:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x003c
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x003c:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0041:
                if (r5 != 0) goto L_0x0045
                r0 = r1
                goto L_0x000e
            L_0x0045:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0051
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x000e
            L_0x0051:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x005d
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x000e
            L_0x005d:
                boolean r0 = r5.equals(r3)
                goto L_0x000e
            L_0x0062:
                if (r5 != 0) goto L_0x0066
                r0 = r1
                goto L_0x0015
            L_0x0066:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0072
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0015
            L_0x0072:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x007e
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0015
            L_0x007e:
                boolean r0 = r5.equals(r3)
                goto L_0x0015
            L_0x0083:
                if (r5 != 0) goto L_0x0087
                r0 = r1
                goto L_0x001c
            L_0x0087:
                boolean r3 = r5 instanceof java.lang.Number
                if (r3 == 0) goto L_0x0092
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x001c
            L_0x0092:
                boolean r3 = r5 instanceof java.lang.Character
                if (r3 == 0) goto L_0x009d
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x001c
            L_0x009d:
                boolean r0 = r5.equals(r0)
                goto L_0x001c
            L_0x00a3:
                r0 = r1
                goto L_0x001f
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set4.contains(java.lang.Object):boolean");
        }

        public /* bridge */ /* synthetic */ scala.collection.Set empty() {
            return (scala.collection.Set) empty();
        }

        public <U> void foreach(Function1<A, U> function1) {
            function1.apply(this.elem1);
            function1.apply(this.elem2);
            function1.apply(this.elem3);
            function1.apply(this.elem4);
        }

        public Iterator<A> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.genericWrapArray(new Object[]{this.elem1, this.elem2, this.elem3, this.elem4}));
        }

        public Set<A> seq() {
            return Cclass.seq(this);
        }

        public int size() {
            return 4;
        }

        public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
            return thisCollection();
        }

        public <B> Set<B> toSet() {
            return Cclass.toSet(this);
        }
    }

    /* renamed from: scala.collection.immutable.Set$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Set set) {
        }

        public static GenericCompanion companion(Set set) {
            return Set$.MODULE$;
        }

        public static Set seq(Set set) {
            return set;
        }

        public static Set toSet(Set set) {
            return set;
        }
    }

    Set<A> seq();
}
