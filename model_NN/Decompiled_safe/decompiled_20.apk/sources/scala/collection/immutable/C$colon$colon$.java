package scala.collection.immutable;

import scala.Serializable;

/* renamed from: scala.collection.immutable.$colon$colon$  reason: invalid class name */
public final class C$colon$colon$ implements Serializable {
    public static final C$colon$colon$ MODULE$ = null;

    static {
        new C$colon$colon$();
    }

    private C$colon$colon$() {
        MODULE$ = this;
    }

    public final String toString() {
        return "::";
    }
}
