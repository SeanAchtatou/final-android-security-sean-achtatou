package scala.collection.immutable;

import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.immutable.VectorPointer;
import scala.collection.mutable.Builder;

public final class VectorBuilder<A> implements VectorPointer<A>, Builder<A, Vector<A>> {
    private int blockIndex = 0;
    private int depth;
    private Object[] display0;
    private Object[] display1;
    private Object[] display2;
    private Object[] display3;
    private Object[] display4;
    private Object[] display5;
    private int lo = 0;

    public VectorBuilder() {
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        VectorPointer.Cclass.$init$(this);
        display0_$eq(new Object[32]);
        depth_$eq(1);
    }

    private int blockIndex() {
        return this.blockIndex;
    }

    private void blockIndex_$eq(int i) {
        this.blockIndex = i;
    }

    private int lo() {
        return this.lo;
    }

    private void lo_$eq(int i) {
        this.lo = i;
    }

    public final VectorBuilder<A> $plus$eq(A a) {
        if (lo() >= display0().length) {
            int blockIndex2 = blockIndex() + 32;
            VectorPointer.Cclass.gotoNextBlockStartWritable(this, blockIndex2, blockIndex() ^ blockIndex2);
            this.blockIndex = blockIndex2;
            this.lo = 0;
        }
        display0()[lo()] = a;
        this.lo = lo() + 1;
        return this;
    }

    public final VectorBuilder<A> $plus$plus$eq(TraversableOnce<A> traversableOnce) {
        return (VectorBuilder) Growable.Cclass.$plus$plus$eq(this, traversableOnce);
    }

    public final Object[] copyOf(Object[] objArr) {
        return VectorPointer.Cclass.copyOf(this, objArr);
    }

    public final int depth() {
        return this.depth;
    }

    public final void depth_$eq(int i) {
        this.depth = i;
    }

    public final Object[] display0() {
        return this.display0;
    }

    public final void display0_$eq(Object[] objArr) {
        this.display0 = objArr;
    }

    public final Object[] display1() {
        return this.display1;
    }

    public final void display1_$eq(Object[] objArr) {
        this.display1 = objArr;
    }

    public final Object[] display2() {
        return this.display2;
    }

    public final void display2_$eq(Object[] objArr) {
        this.display2 = objArr;
    }

    public final Object[] display3() {
        return this.display3;
    }

    public final void display3_$eq(Object[] objArr) {
        this.display3 = objArr;
    }

    public final Object[] display4() {
        return this.display4;
    }

    public final void display4_$eq(Object[] objArr) {
        this.display4 = objArr;
    }

    public final Object[] display5() {
        return this.display5;
    }

    public final void display5_$eq(Object[] objArr) {
        this.display5 = objArr;
    }

    public final void gotoNextBlockStartWritable(int i, int i2) {
        VectorPointer.Cclass.gotoNextBlockStartWritable(this, i, i2);
    }

    public final <U> void initFrom(VectorPointer<U> vectorPointer, int i) {
        VectorPointer.Cclass.initFrom(this, vectorPointer, i);
    }

    public final Object[] nullSlotAndCopy(Object[] objArr, int i) {
        return VectorPointer.Cclass.nullSlotAndCopy(this, objArr, i);
    }

    public final Vector<A> result() {
        int lo2 = lo() + blockIndex();
        if (lo2 == 0) {
            return Vector$.MODULE$.empty();
        }
        Vector<A> vector = new Vector<>(0, lo2, 0);
        vector.initFrom(this);
        if (depth() <= 1) {
            return vector;
        }
        vector.gotoPos(0, lo2 - 1);
        return vector;
    }

    public final void sizeHint(int i) {
        Builder.Cclass.sizeHint(this, i);
    }

    public final void sizeHint(TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHint(this, traversableLike);
    }

    public final void sizeHint(TraversableLike<?, ?> traversableLike, int i) {
        Builder.Cclass.sizeHint(this, traversableLike, i);
    }

    public final void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike) {
        Builder.Cclass.sizeHintBounded(this, i, traversableLike);
    }
}
