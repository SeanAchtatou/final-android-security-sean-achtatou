package scala.collection.immutable;

import scala.collection.immutable.HashSet;

public final class HashSet$HashTrieSet$$anon$1 extends TrieIterator<A> {
    public HashSet$HashTrieSet$$anon$1(HashSet.HashTrieSet<A> hashTrieSet) {
        super((Iterable[]) hashTrieSet.elems());
    }

    public final A getElem(Object obj) {
        return ((HashSet.HashSet1) obj).key();
    }
}
