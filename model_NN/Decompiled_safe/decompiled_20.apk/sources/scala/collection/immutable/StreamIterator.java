package scala.collection.immutable;

import scala.Function0;
import scala.collection.AbstractIterator;
import scala.collection.Iterator$;

public final class StreamIterator<A> extends AbstractIterator<A> {
    private StreamIterator<A>.LazyCell these;

    public class LazyCell {
        public final /* synthetic */ StreamIterator $outer;
        private volatile boolean bitmap$0;
        private final Function0<Stream<A>> st;
        private Stream<A> v;

        public LazyCell(StreamIterator<A> streamIterator, Function0<Stream<A>> function0) {
            this.st = function0;
            if (streamIterator == null) {
                throw new NullPointerException();
            }
            this.$outer = streamIterator;
        }

        private Stream v$lzycompute() {
            synchronized (this) {
                if (!this.bitmap$0) {
                    this.v = this.st.apply();
                    this.bitmap$0 = true;
                }
            }
            this.st = null;
            return this.v;
        }

        public Stream<A> v() {
            return this.bitmap$0 ? this.v : v$lzycompute();
        }
    }

    private StreamIterator() {
    }

    public StreamIterator(Stream<A> stream) {
        this();
        this.these = new LazyCell(this, new StreamIterator$$anonfun$$init$$1(this, stream));
    }

    private StreamIterator<A>.LazyCell these() {
        return this.these;
    }

    private void these_$eq(StreamIterator<A>.LazyCell lazyCell) {
        this.these = lazyCell;
    }

    public final boolean hasNext() {
        return these().v().nonEmpty();
    }

    public final A next() {
        if (isEmpty()) {
            return Iterator$.MODULE$.empty().next();
        }
        Stream v = these().v();
        A head = v.head();
        this.these = new LazyCell(this, new StreamIterator$$anonfun$next$1(this, v));
        return head;
    }

    public final List<A> toList() {
        return toStream().toList();
    }

    public final Stream<A> toStream() {
        Stream<A> v = these().v();
        this.these = new LazyCell(this, new StreamIterator$$anonfun$toStream$1(this));
        return v;
    }
}
