package scala.collection.immutable;

import scala.Function1;
import scala.Serializable;
import scala.runtime.AbstractFunction0;

public final class Stream$$anonfun$filteredTail$1 extends AbstractFunction0<Stream<A>> implements Serializable {
    private final Function1 p$1;
    private final Stream stream$2;

    public Stream$$anonfun$filteredTail$1(Stream stream, Function1 function1) {
        this.stream$2 = stream;
        this.p$1 = function1;
    }

    public final Stream<A> apply() {
        return ((Stream) this.stream$2.tail()).filter(this.p$1);
    }
}
