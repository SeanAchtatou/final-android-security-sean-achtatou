package scala.collection.immutable;

import scala.Serializable;
import scala.collection.generic.ImmutableMapFactory;

public final class ListMap$ extends ImmutableMapFactory<ListMap> implements Serializable {
    public static final ListMap$ MODULE$ = null;

    static {
        new ListMap$();
    }

    private ListMap$() {
        MODULE$ = this;
    }

    public final <A, B> ListMap<A, B> empty() {
        return ListMap$EmptyListMap$.MODULE$;
    }
}
