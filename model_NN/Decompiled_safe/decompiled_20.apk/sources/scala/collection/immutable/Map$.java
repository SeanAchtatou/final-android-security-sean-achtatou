package scala.collection.immutable;

import scala.collection.generic.ImmutableMapFactory;

public final class Map$ extends ImmutableMapFactory<Map> {
    public static final Map$ MODULE$ = null;

    static {
        new Map$();
    }

    private Map$() {
        MODULE$ = this;
    }

    public final <A, B> Map<A, B> empty() {
        return Map$EmptyMap$.MODULE$;
    }
}
