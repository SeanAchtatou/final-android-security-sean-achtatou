package scala.collection.immutable;

import scala.Function1;
import scala.None$;
import scala.Option;
import scala.Predef$;
import scala.Predef$$less$colon$less;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.runtime.BoxesRunTime;

public interface Map<A, B> extends scala.collection.Map<A, B>, Iterable<Tuple2<A, B>>, MapLike<A, B, Map<A, B>> {

    public class Map1<A, B> extends AbstractMap<A, B> implements Map<A, B> {
        private final A key1;
        private final B value1;

        public Map1(A a, B b) {
            this.key1 = a;
            this.value1 = b;
        }

        public Map<A, B> $minus(A a) {
            A a2 = this.key1;
            return a == a2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, a2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, a2) : a.equals(a2) ? Map$.MODULE$.empty() : this;
        }

        public <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2) {
            return updated(tuple2._1(), tuple2._2());
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> function1) {
            function1.apply(new Tuple2(this.key1, this.value1));
        }

        public Option<B> get(A a) {
            A a2 = this.key1;
            return a == a2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, a2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, a2) : a.equals(a2) ? new Some(this.value1) : None$.MODULE$;
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{new Tuple2(this.key1, this.value1)}));
        }

        public int size() {
            return 1;
        }

        public <B1> Map<A, B1> updated(A a, B1 b1) {
            A a2 = this.key1;
            return a == a2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, a2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, a2) : a.equals(a2) ? new Map1(this.key1, b1) : new Map2(this.key1, this.value1, a, b1);
        }
    }

    public class Map2<A, B> extends AbstractMap<A, B> implements Map<A, B> {
        private final A key1;
        private final A key2;
        private final B value1;
        private final B value2;

        public Map2(A a, B b, A a2, B b2) {
            this.key1 = a;
            this.value1 = b;
            this.key2 = a2;
            this.value2 = b2;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Map<A, B> $minus(A r5) {
            /*
                r4 = this;
                r1 = 1
                r2 = 0
                A r3 = r4.key1
                if (r5 != r3) goto L_0x0014
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0035
                scala.collection.immutable.Map$Map1 r0 = new scala.collection.immutable.Map$Map1
                A r1 = r4.key2
                B r2 = r4.value2
                r0.<init>(r1, r2)
                r4 = r0
            L_0x0013:
                return r4
            L_0x0014:
                if (r5 != 0) goto L_0x0018
                r0 = r2
                goto L_0x0007
            L_0x0018:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0024
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0024:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0030
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x0030:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0035:
                A r0 = r4.key2
                if (r5 != r0) goto L_0x0047
                r0 = r1
            L_0x003a:
                if (r0 == 0) goto L_0x0013
                scala.collection.immutable.Map$Map1 r0 = new scala.collection.immutable.Map$Map1
                A r1 = r4.key1
                B r2 = r4.value1
                r0.<init>(r1, r2)
                r4 = r0
                goto L_0x0013
            L_0x0047:
                if (r5 != 0) goto L_0x004b
                r0 = r2
                goto L_0x003a
            L_0x004b:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0056
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x003a
            L_0x0056:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0061
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x003a
            L_0x0061:
                boolean r0 = r5.equals(r0)
                goto L_0x003a
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map2.$minus(java.lang.Object):scala.collection.immutable.Map");
        }

        public <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2) {
            return updated(tuple2._1(), tuple2._2());
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> function1) {
            function1.apply(new Tuple2(this.key1, this.value1));
            function1.apply(new Tuple2(this.key2, this.value2));
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.Option<B> get(A r5) {
            /*
                r4 = this;
                r1 = 1
                r2 = 0
                A r3 = r4.key1
                if (r5 != r3) goto L_0x0011
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0032
                scala.Some r0 = new scala.Some
                B r1 = r4.value1
                r0.<init>(r1)
            L_0x0010:
                return r0
            L_0x0011:
                if (r5 != 0) goto L_0x0015
                r0 = r2
                goto L_0x0007
            L_0x0015:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0021
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0021:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x002d
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x002d:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0032:
                A r0 = r4.key2
                if (r5 != r0) goto L_0x0041
                r0 = r1
            L_0x0037:
                if (r0 == 0) goto L_0x0060
                scala.Some r0 = new scala.Some
                B r1 = r4.value2
                r0.<init>(r1)
                goto L_0x0010
            L_0x0041:
                if (r5 != 0) goto L_0x0045
                r0 = r2
                goto L_0x0037
            L_0x0045:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0050
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x0037
            L_0x0050:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x005b
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x0037
            L_0x005b:
                boolean r0 = r5.equals(r0)
                goto L_0x0037
            L_0x0060:
                scala.None$ r0 = scala.None$.MODULE$
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map2.get(java.lang.Object):scala.Option");
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{new Tuple2(this.key1, this.value1), new Tuple2(this.key2, this.value2)}));
        }

        public int size() {
            return 2;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.Map<A, B1> updated(A r8, B1 r9) {
            /*
                r7 = this;
                r1 = 1
                r2 = 0
                A r3 = r7.key1
                if (r8 != r3) goto L_0x0015
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0036
                scala.collection.immutable.Map$Map2 r0 = new scala.collection.immutable.Map$Map2
                A r1 = r7.key1
                A r2 = r7.key2
                B r3 = r7.value2
                r0.<init>(r1, r9, r2, r3)
            L_0x0014:
                return r0
            L_0x0015:
                if (r8 != 0) goto L_0x0019
                r0 = r2
                goto L_0x0007
            L_0x0019:
                boolean r0 = r8 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0025
                r0 = r8
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0025:
                boolean r0 = r8 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0031
                r0 = r8
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x0031:
                boolean r0 = r8.equals(r3)
                goto L_0x0007
            L_0x0036:
                A r3 = r7.key2
                if (r8 != r3) goto L_0x0049
                r0 = r1
            L_0x003b:
                if (r0 == 0) goto L_0x006a
                scala.collection.immutable.Map$Map2 r0 = new scala.collection.immutable.Map$Map2
                A r1 = r7.key1
                B r2 = r7.value1
                A r3 = r7.key2
                r0.<init>(r1, r2, r3, r9)
                goto L_0x0014
            L_0x0049:
                if (r8 != 0) goto L_0x004d
                r0 = r2
                goto L_0x003b
            L_0x004d:
                boolean r0 = r8 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0059
                r0 = r8
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x003b
            L_0x0059:
                boolean r0 = r8 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0065
                r0 = r8
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x003b
            L_0x0065:
                boolean r0 = r8.equals(r3)
                goto L_0x003b
            L_0x006a:
                scala.collection.immutable.Map$Map3 r0 = new scala.collection.immutable.Map$Map3
                A r1 = r7.key1
                B r2 = r7.value1
                A r3 = r7.key2
                B r4 = r7.value2
                r5 = r8
                r6 = r9
                r0.<init>(r1, r2, r3, r4, r5, r6)
                goto L_0x0014
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map2.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map");
        }
    }

    public class Map3<A, B> extends AbstractMap<A, B> implements Map<A, B> {
        private final A key1;
        private final A key2;
        private final A key3;
        private final B value1;
        private final B value2;
        private final B value3;

        public Map3(A a, B b, A a2, B b2, A a3, B b3) {
            this.key1 = a;
            this.value1 = b;
            this.key2 = a2;
            this.value2 = b2;
            this.key3 = a3;
            this.value3 = b3;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Map<A, B> $minus(A r6) {
            /*
                r5 = this;
                r1 = 1
                r2 = 0
                A r3 = r5.key1
                if (r6 != r3) goto L_0x0018
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0039
                scala.collection.immutable.Map$Map2 r0 = new scala.collection.immutable.Map$Map2
                A r1 = r5.key2
                B r2 = r5.value2
                A r3 = r5.key3
                B r4 = r5.value3
                r0.<init>(r1, r2, r3, r4)
                r5 = r0
            L_0x0017:
                return r5
            L_0x0018:
                if (r6 != 0) goto L_0x001c
                r0 = r2
                goto L_0x0007
            L_0x001c:
                boolean r0 = r6 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0028
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0028:
                boolean r0 = r6 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0034
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x0034:
                boolean r0 = r6.equals(r3)
                goto L_0x0007
            L_0x0039:
                A r3 = r5.key2
                if (r6 != r3) goto L_0x004f
                r0 = r1
            L_0x003e:
                if (r0 == 0) goto L_0x0070
                scala.collection.immutable.Map$Map2 r0 = new scala.collection.immutable.Map$Map2
                A r1 = r5.key1
                B r2 = r5.value1
                A r3 = r5.key3
                B r4 = r5.value3
                r0.<init>(r1, r2, r3, r4)
                r5 = r0
                goto L_0x0017
            L_0x004f:
                if (r6 != 0) goto L_0x0053
                r0 = r2
                goto L_0x003e
            L_0x0053:
                boolean r0 = r6 instanceof java.lang.Number
                if (r0 == 0) goto L_0x005f
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x003e
            L_0x005f:
                boolean r0 = r6 instanceof java.lang.Character
                if (r0 == 0) goto L_0x006b
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x003e
            L_0x006b:
                boolean r0 = r6.equals(r3)
                goto L_0x003e
            L_0x0070:
                A r0 = r5.key3
                if (r6 != r0) goto L_0x0086
                r0 = r1
            L_0x0075:
                if (r0 == 0) goto L_0x0017
                scala.collection.immutable.Map$Map2 r0 = new scala.collection.immutable.Map$Map2
                A r1 = r5.key1
                B r2 = r5.value1
                A r3 = r5.key2
                B r4 = r5.value2
                r0.<init>(r1, r2, r3, r4)
                r5 = r0
                goto L_0x0017
            L_0x0086:
                if (r6 != 0) goto L_0x008a
                r0 = r2
                goto L_0x0075
            L_0x008a:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0095
                java.lang.Number r6 = (java.lang.Number) r6
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r6, r0)
                goto L_0x0075
            L_0x0095:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x00a0
                java.lang.Character r6 = (java.lang.Character) r6
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r6, r0)
                goto L_0x0075
            L_0x00a0:
                boolean r0 = r6.equals(r0)
                goto L_0x0075
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map3.$minus(java.lang.Object):scala.collection.immutable.Map");
        }

        public <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2) {
            return updated(tuple2._1(), tuple2._2());
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> function1) {
            function1.apply(new Tuple2(this.key1, this.value1));
            function1.apply(new Tuple2(this.key2, this.value2));
            function1.apply(new Tuple2(this.key3, this.value3));
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.Option<B> get(A r5) {
            /*
                r4 = this;
                r1 = 1
                r2 = 0
                A r3 = r4.key1
                if (r5 != r3) goto L_0x0011
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0032
                scala.Some r0 = new scala.Some
                B r1 = r4.value1
                r0.<init>(r1)
            L_0x0010:
                return r0
            L_0x0011:
                if (r5 != 0) goto L_0x0015
                r0 = r2
                goto L_0x0007
            L_0x0015:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0021
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0021:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x002d
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x002d:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0032:
                A r3 = r4.key2
                if (r5 != r3) goto L_0x0041
                r0 = r1
            L_0x0037:
                if (r0 == 0) goto L_0x0062
                scala.Some r0 = new scala.Some
                B r1 = r4.value2
                r0.<init>(r1)
                goto L_0x0010
            L_0x0041:
                if (r5 != 0) goto L_0x0045
                r0 = r2
                goto L_0x0037
            L_0x0045:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0051
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0037
            L_0x0051:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x005d
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0037
            L_0x005d:
                boolean r0 = r5.equals(r3)
                goto L_0x0037
            L_0x0062:
                A r0 = r4.key3
                if (r5 != r0) goto L_0x0071
                r0 = r1
            L_0x0067:
                if (r0 == 0) goto L_0x0090
                scala.Some r0 = new scala.Some
                B r1 = r4.value3
                r0.<init>(r1)
                goto L_0x0010
            L_0x0071:
                if (r5 != 0) goto L_0x0075
                r0 = r2
                goto L_0x0067
            L_0x0075:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0080
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x0067
            L_0x0080:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x008b
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x0067
            L_0x008b:
                boolean r0 = r5.equals(r0)
                goto L_0x0067
            L_0x0090:
                scala.None$ r0 = scala.None$.MODULE$
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map3.get(java.lang.Object):scala.Option");
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{new Tuple2(this.key1, this.value1), new Tuple2(this.key2, this.value2), new Tuple2(this.key3, this.value3)}));
        }

        public int size() {
            return 3;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.Map<A, B1> updated(A r10, B1 r11) {
            /*
                r9 = this;
                r1 = 1
                r2 = 0
                A r3 = r9.key1
                if (r10 != r3) goto L_0x001a
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x003b
                scala.collection.immutable.Map$Map3 r0 = new scala.collection.immutable.Map$Map3
                A r1 = r9.key1
                A r3 = r9.key2
                B r4 = r9.value2
                A r5 = r9.key3
                B r6 = r9.value3
                r2 = r11
                r0.<init>(r1, r2, r3, r4, r5, r6)
            L_0x0019:
                return r0
            L_0x001a:
                if (r10 != 0) goto L_0x001e
                r0 = r2
                goto L_0x0007
            L_0x001e:
                boolean r0 = r10 instanceof java.lang.Number
                if (r0 == 0) goto L_0x002a
                r0 = r10
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x002a:
                boolean r0 = r10 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0036
                r0 = r10
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x0036:
                boolean r0 = r10.equals(r3)
                goto L_0x0007
            L_0x003b:
                A r3 = r9.key2
                if (r10 != r3) goto L_0x0053
                r0 = r1
            L_0x0040:
                if (r0 == 0) goto L_0x0074
                scala.collection.immutable.Map$Map3 r0 = new scala.collection.immutable.Map$Map3
                A r1 = r9.key1
                B r2 = r9.value1
                A r3 = r9.key2
                A r5 = r9.key3
                B r6 = r9.value3
                r4 = r11
                r0.<init>(r1, r2, r3, r4, r5, r6)
                goto L_0x0019
            L_0x0053:
                if (r10 != 0) goto L_0x0057
                r0 = r2
                goto L_0x0040
            L_0x0057:
                boolean r0 = r10 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0063
                r0 = r10
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0040
            L_0x0063:
                boolean r0 = r10 instanceof java.lang.Character
                if (r0 == 0) goto L_0x006f
                r0 = r10
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0040
            L_0x006f:
                boolean r0 = r10.equals(r3)
                goto L_0x0040
            L_0x0074:
                A r3 = r9.key3
                if (r10 != r3) goto L_0x008c
                r0 = r1
            L_0x0079:
                if (r0 == 0) goto L_0x00ad
                scala.collection.immutable.Map$Map3 r0 = new scala.collection.immutable.Map$Map3
                A r1 = r9.key1
                B r2 = r9.value1
                A r3 = r9.key2
                B r4 = r9.value2
                A r5 = r9.key3
                r6 = r11
                r0.<init>(r1, r2, r3, r4, r5, r6)
                goto L_0x0019
            L_0x008c:
                if (r10 != 0) goto L_0x0090
                r0 = r2
                goto L_0x0079
            L_0x0090:
                boolean r0 = r10 instanceof java.lang.Number
                if (r0 == 0) goto L_0x009c
                r0 = r10
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0079
            L_0x009c:
                boolean r0 = r10 instanceof java.lang.Character
                if (r0 == 0) goto L_0x00a8
                r0 = r10
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0079
            L_0x00a8:
                boolean r0 = r10.equals(r3)
                goto L_0x0079
            L_0x00ad:
                scala.collection.immutable.Map$Map4 r0 = new scala.collection.immutable.Map$Map4
                A r1 = r9.key1
                B r2 = r9.value1
                A r3 = r9.key2
                B r4 = r9.value2
                A r5 = r9.key3
                B r6 = r9.value3
                r7 = r10
                r8 = r11
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
                goto L_0x0019
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map3.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map");
        }
    }

    public class Map4<A, B> extends AbstractMap<A, B> implements Map<A, B> {
        private final A key1;
        private final A key2;
        private final A key3;
        private final A key4;
        private final B value1;
        private final B value2;
        private final B value3;
        private final B value4;

        public Map4(A a, B b, A a2, B b2, A a3, B b3, A a4, B b4) {
            this.key1 = a;
            this.value1 = b;
            this.key2 = a2;
            this.value2 = b2;
            this.key3 = a3;
            this.value3 = b3;
            this.key4 = a4;
            this.value4 = b4;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Map<A, B> $minus(A r8) {
            /*
                r7 = this;
                r1 = 1
                r2 = 0
                A r3 = r7.key1
                if (r8 != r3) goto L_0x001b
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x003c
                scala.collection.immutable.Map$Map3 r0 = new scala.collection.immutable.Map$Map3
                A r1 = r7.key2
                B r2 = r7.value2
                A r3 = r7.key3
                B r4 = r7.value3
                A r5 = r7.key4
                B r6 = r7.value4
                r0.<init>(r1, r2, r3, r4, r5, r6)
            L_0x001a:
                return r0
            L_0x001b:
                if (r8 != 0) goto L_0x001f
                r0 = r2
                goto L_0x0007
            L_0x001f:
                boolean r0 = r8 instanceof java.lang.Number
                if (r0 == 0) goto L_0x002b
                r0 = r8
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x002b:
                boolean r0 = r8 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0037
                r0 = r8
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x0037:
                boolean r0 = r8.equals(r3)
                goto L_0x0007
            L_0x003c:
                A r3 = r7.key2
                if (r8 != r3) goto L_0x0055
                r0 = r1
            L_0x0041:
                if (r0 == 0) goto L_0x0076
                scala.collection.immutable.Map$Map3 r0 = new scala.collection.immutable.Map$Map3
                A r1 = r7.key1
                B r2 = r7.value1
                A r3 = r7.key3
                B r4 = r7.value3
                A r5 = r7.key4
                B r6 = r7.value4
                r0.<init>(r1, r2, r3, r4, r5, r6)
                goto L_0x001a
            L_0x0055:
                if (r8 != 0) goto L_0x0059
                r0 = r2
                goto L_0x0041
            L_0x0059:
                boolean r0 = r8 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0065
                r0 = r8
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0041
            L_0x0065:
                boolean r0 = r8 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0071
                r0 = r8
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0041
            L_0x0071:
                boolean r0 = r8.equals(r3)
                goto L_0x0041
            L_0x0076:
                A r3 = r7.key3
                if (r8 != r3) goto L_0x008f
                r0 = r1
            L_0x007b:
                if (r0 == 0) goto L_0x00b0
                scala.collection.immutable.Map$Map3 r0 = new scala.collection.immutable.Map$Map3
                A r1 = r7.key1
                B r2 = r7.value1
                A r3 = r7.key2
                B r4 = r7.value2
                A r5 = r7.key4
                B r6 = r7.value4
                r0.<init>(r1, r2, r3, r4, r5, r6)
                goto L_0x001a
            L_0x008f:
                if (r8 != 0) goto L_0x0093
                r0 = r2
                goto L_0x007b
            L_0x0093:
                boolean r0 = r8 instanceof java.lang.Number
                if (r0 == 0) goto L_0x009f
                r0 = r8
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x007b
            L_0x009f:
                boolean r0 = r8 instanceof java.lang.Character
                if (r0 == 0) goto L_0x00ab
                r0 = r8
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x007b
            L_0x00ab:
                boolean r0 = r8.equals(r3)
                goto L_0x007b
            L_0x00b0:
                A r0 = r7.key4
                if (r8 != r0) goto L_0x00ca
                r0 = r1
            L_0x00b5:
                if (r0 == 0) goto L_0x00e9
                scala.collection.immutable.Map$Map3 r0 = new scala.collection.immutable.Map$Map3
                A r1 = r7.key1
                B r2 = r7.value1
                A r3 = r7.key2
                B r4 = r7.value2
                A r5 = r7.key3
                B r6 = r7.value3
                r0.<init>(r1, r2, r3, r4, r5, r6)
                goto L_0x001a
            L_0x00ca:
                if (r8 != 0) goto L_0x00ce
                r0 = r2
                goto L_0x00b5
            L_0x00ce:
                boolean r1 = r8 instanceof java.lang.Number
                if (r1 == 0) goto L_0x00d9
                java.lang.Number r8 = (java.lang.Number) r8
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r8, r0)
                goto L_0x00b5
            L_0x00d9:
                boolean r1 = r8 instanceof java.lang.Character
                if (r1 == 0) goto L_0x00e4
                java.lang.Character r8 = (java.lang.Character) r8
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r8, r0)
                goto L_0x00b5
            L_0x00e4:
                boolean r0 = r8.equals(r0)
                goto L_0x00b5
            L_0x00e9:
                r0 = r7
                goto L_0x001a
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map4.$minus(java.lang.Object):scala.collection.immutable.Map");
        }

        public <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2) {
            return updated(tuple2._1(), tuple2._2());
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> function1) {
            function1.apply(new Tuple2(this.key1, this.value1));
            function1.apply(new Tuple2(this.key2, this.value2));
            function1.apply(new Tuple2(this.key3, this.value3));
            function1.apply(new Tuple2(this.key4, this.value4));
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.Option<B> get(A r5) {
            /*
                r4 = this;
                r1 = 1
                r2 = 0
                A r3 = r4.key1
                if (r5 != r3) goto L_0x0011
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x0032
                scala.Some r0 = new scala.Some
                B r1 = r4.value1
                r0.<init>(r1)
            L_0x0010:
                return r0
            L_0x0011:
                if (r5 != 0) goto L_0x0015
                r0 = r2
                goto L_0x0007
            L_0x0015:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0021
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x0021:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x002d
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x002d:
                boolean r0 = r5.equals(r3)
                goto L_0x0007
            L_0x0032:
                A r3 = r4.key2
                if (r5 != r3) goto L_0x0041
                r0 = r1
            L_0x0037:
                if (r0 == 0) goto L_0x0062
                scala.Some r0 = new scala.Some
                B r1 = r4.value2
                r0.<init>(r1)
                goto L_0x0010
            L_0x0041:
                if (r5 != 0) goto L_0x0045
                r0 = r2
                goto L_0x0037
            L_0x0045:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0051
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0037
            L_0x0051:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x005d
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0037
            L_0x005d:
                boolean r0 = r5.equals(r3)
                goto L_0x0037
            L_0x0062:
                A r3 = r4.key3
                if (r5 != r3) goto L_0x0071
                r0 = r1
            L_0x0067:
                if (r0 == 0) goto L_0x0092
                scala.Some r0 = new scala.Some
                B r1 = r4.value3
                r0.<init>(r1)
                goto L_0x0010
            L_0x0071:
                if (r5 != 0) goto L_0x0075
                r0 = r2
                goto L_0x0067
            L_0x0075:
                boolean r0 = r5 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0081
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0067
            L_0x0081:
                boolean r0 = r5 instanceof java.lang.Character
                if (r0 == 0) goto L_0x008d
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0067
            L_0x008d:
                boolean r0 = r5.equals(r3)
                goto L_0x0067
            L_0x0092:
                A r0 = r4.key4
                if (r5 != r0) goto L_0x00a2
                r0 = r1
            L_0x0097:
                if (r0 == 0) goto L_0x00c1
                scala.Some r0 = new scala.Some
                B r1 = r4.value4
                r0.<init>(r1)
                goto L_0x0010
            L_0x00a2:
                if (r5 != 0) goto L_0x00a6
                r0 = r2
                goto L_0x0097
            L_0x00a6:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x00b1
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x0097
            L_0x00b1:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x00bc
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x0097
            L_0x00bc:
                boolean r0 = r5.equals(r0)
                goto L_0x0097
            L_0x00c1:
                scala.None$ r0 = scala.None$.MODULE$
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map4.get(java.lang.Object):scala.Option");
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{new Tuple2(this.key1, this.value1), new Tuple2(this.key2, this.value2), new Tuple2(this.key3, this.value3), new Tuple2(this.key4, this.value4)}));
        }

        public int size() {
            return 4;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.Map<A, B1> updated(A r11, B1 r12) {
            /*
                r10 = this;
                r1 = 1
                r2 = 0
                A r3 = r10.key1
                if (r11 != r3) goto L_0x001e
                r0 = r1
            L_0x0007:
                if (r0 == 0) goto L_0x003f
                scala.collection.immutable.Map$Map4 r0 = new scala.collection.immutable.Map$Map4
                A r1 = r10.key1
                A r3 = r10.key2
                B r4 = r10.value2
                A r5 = r10.key3
                B r6 = r10.value3
                A r7 = r10.key4
                B r8 = r10.value4
                r2 = r12
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            L_0x001d:
                return r0
            L_0x001e:
                if (r11 != 0) goto L_0x0022
                r0 = r2
                goto L_0x0007
            L_0x0022:
                boolean r0 = r11 instanceof java.lang.Number
                if (r0 == 0) goto L_0x002e
                r0 = r11
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0007
            L_0x002e:
                boolean r0 = r11 instanceof java.lang.Character
                if (r0 == 0) goto L_0x003a
                r0 = r11
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0007
            L_0x003a:
                boolean r0 = r11.equals(r3)
                goto L_0x0007
            L_0x003f:
                A r3 = r10.key2
                if (r11 != r3) goto L_0x005b
                r0 = r1
            L_0x0044:
                if (r0 == 0) goto L_0x007c
                scala.collection.immutable.Map$Map4 r0 = new scala.collection.immutable.Map$Map4
                A r1 = r10.key1
                B r2 = r10.value1
                A r3 = r10.key2
                A r5 = r10.key3
                B r6 = r10.value3
                A r7 = r10.key4
                B r8 = r10.value4
                r4 = r12
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
                goto L_0x001d
            L_0x005b:
                if (r11 != 0) goto L_0x005f
                r0 = r2
                goto L_0x0044
            L_0x005f:
                boolean r0 = r11 instanceof java.lang.Number
                if (r0 == 0) goto L_0x006b
                r0 = r11
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0044
            L_0x006b:
                boolean r0 = r11 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0077
                r0 = r11
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0044
            L_0x0077:
                boolean r0 = r11.equals(r3)
                goto L_0x0044
            L_0x007c:
                A r3 = r10.key3
                if (r11 != r3) goto L_0x0098
                r0 = r1
            L_0x0081:
                if (r0 == 0) goto L_0x00b9
                scala.collection.immutable.Map$Map4 r0 = new scala.collection.immutable.Map$Map4
                A r1 = r10.key1
                B r2 = r10.value1
                A r3 = r10.key2
                B r4 = r10.value2
                A r5 = r10.key3
                A r7 = r10.key4
                B r8 = r10.value4
                r6 = r12
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
                goto L_0x001d
            L_0x0098:
                if (r11 != 0) goto L_0x009c
                r0 = r2
                goto L_0x0081
            L_0x009c:
                boolean r0 = r11 instanceof java.lang.Number
                if (r0 == 0) goto L_0x00a8
                r0 = r11
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x0081
            L_0x00a8:
                boolean r0 = r11 instanceof java.lang.Character
                if (r0 == 0) goto L_0x00b4
                r0 = r11
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x0081
            L_0x00b4:
                boolean r0 = r11.equals(r3)
                goto L_0x0081
            L_0x00b9:
                A r3 = r10.key4
                if (r11 != r3) goto L_0x00d6
                r0 = r1
            L_0x00be:
                if (r0 == 0) goto L_0x00f7
                scala.collection.immutable.Map$Map4 r0 = new scala.collection.immutable.Map$Map4
                A r1 = r10.key1
                B r2 = r10.value1
                A r3 = r10.key2
                B r4 = r10.value2
                A r5 = r10.key3
                B r6 = r10.value3
                A r7 = r10.key4
                r8 = r12
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
                goto L_0x001d
            L_0x00d6:
                if (r11 != 0) goto L_0x00da
                r0 = r2
                goto L_0x00be
            L_0x00da:
                boolean r0 = r11 instanceof java.lang.Number
                if (r0 == 0) goto L_0x00e6
                r0 = r11
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
                goto L_0x00be
            L_0x00e6:
                boolean r0 = r11 instanceof java.lang.Character
                if (r0 == 0) goto L_0x00f2
                r0 = r11
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
                goto L_0x00be
            L_0x00f2:
                boolean r0 = r11.equals(r3)
                goto L_0x00be
            L_0x00f7:
                scala.collection.immutable.HashMap r3 = new scala.collection.immutable.HashMap
                r3.<init>()
                scala.Tuple2 r4 = new scala.Tuple2
                A r0 = r10.key1
                B r5 = r10.value1
                r4.<init>(r0, r5)
                scala.Tuple2 r5 = new scala.Tuple2
                A r0 = r10.key2
                B r6 = r10.value2
                r5.<init>(r0, r6)
                scala.Predef$ r6 = scala.Predef$.MODULE$
                r0 = 3
                scala.Tuple2[] r0 = new scala.Tuple2[r0]
                scala.Tuple2 r7 = new scala.Tuple2
                A r8 = r10.key3
                B r9 = r10.value3
                r7.<init>(r8, r9)
                r0[r2] = r7
                scala.Tuple2 r2 = new scala.Tuple2
                A r7 = r10.key4
                B r8 = r10.value4
                r2.<init>(r7, r8)
                r0[r1] = r2
                r1 = 2
                scala.Tuple2 r2 = new scala.Tuple2
                r2.<init>(r11, r12)
                r0[r1] = r2
                java.lang.Object[] r0 = (java.lang.Object[]) r0
                scala.collection.mutable.WrappedArray r0 = r6.wrapRefArray(r0)
                scala.collection.immutable.HashMap r0 = r3.$plus(r4, r5, r0)
                goto L_0x001d
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map4.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map");
        }
    }

    /* renamed from: scala.collection.immutable.Map$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Map map) {
        }

        public static Map empty(Map map) {
            return Map$.MODULE$.empty();
        }

        public static Map seq(Map map) {
            return map;
        }

        public static Map toMap(Map map, Predef$$less$colon$less predef$$less$colon$less) {
            return map;
        }
    }

    Map<A, B> empty();

    Map<A, B> seq();
}
