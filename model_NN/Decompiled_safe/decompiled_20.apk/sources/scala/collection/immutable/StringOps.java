package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.Predef$$less$colon$less;
import scala.Tuple2;
import scala.collection.GenIterable;
import scala.collection.GenSeq;
import scala.collection.GenSeqLike;
import scala.collection.GenTraversableOnce;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Parallelizable;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.FilterMonadic;
import scala.collection.immutable.StringLike;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Ordered;
import scala.reflect.ClassTag;
import scala.runtime.Nothing$;

public final class StringOps implements StringLike<String> {
    private final String repr;

    public StringOps(String str) {
        this.repr = str;
        GenTraversableOnce.Cclass.$init$(this);
        TraversableOnce.Cclass.$init$(this);
        Parallelizable.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        GenSeqLike.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        Ordered.Cclass.$init$(this);
        StringLike.Cclass.$init$(this);
    }

    public final <B> B $div$colon(B b, Function2<B, Object, B> function2) {
        return TraversableOnce.Cclass.$div$colon(this, b, function2);
    }

    public final <B, That> That $plus$plus(GenTraversableOnce<B> genTraversableOnce, CanBuildFrom<String, B, That> canBuildFrom) {
        return TraversableLike.Cclass.$plus$plus(this, genTraversableOnce, canBuildFrom);
    }

    public final StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
        return TraversableOnce.Cclass.addString(this, stringBuilder, str, str2, str3);
    }

    public final char apply(int i) {
        return StringOps$.MODULE$.apply$extension(repr(), i);
    }

    public final boolean canEqual(Object obj) {
        return IterableLike.Cclass.canEqual(this, obj);
    }

    public final <B, That> That collect(PartialFunction<Object, B> partialFunction, CanBuildFrom<String, B, That> canBuildFrom) {
        return TraversableLike.Cclass.collect(this, partialFunction, canBuildFrom);
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj) {
        return compare((String) obj);
    }

    public final int compare(String str) {
        return StringLike.Cclass.compare(this, str);
    }

    public final int compareTo(Object obj) {
        return Ordered.Cclass.compareTo(this, obj);
    }

    public final <B> void copyToArray(Object obj, int i) {
        TraversableOnce.Cclass.copyToArray(this, obj, i);
    }

    public final <B> void copyToArray(Object obj, int i, int i2) {
        IndexedSeqOptimized.Cclass.copyToArray(this, obj, i, i2);
    }

    public final <B> void copyToBuffer(Buffer<B> buffer) {
        TraversableOnce.Cclass.copyToBuffer(this, buffer);
    }

    public final <B> boolean corresponds(GenSeq<B> genSeq, Function2<Object, B, Object> function2) {
        return SeqLike.Cclass.corresponds(this, genSeq, function2);
    }

    public final Object drop(int i) {
        return IndexedSeqOptimized.Cclass.drop(this, i);
    }

    public final boolean equals(Object obj) {
        return StringOps$.MODULE$.equals$extension(repr(), obj);
    }

    public final boolean exists(Function1<Object, Object> function1) {
        return IndexedSeqOptimized.Cclass.exists(this, function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, java.lang.String] */
    public final String filter(Function1<Object, Object> function1) {
        return TraversableLike.Cclass.filter(this, function1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, java.lang.String] */
    public final String filterNot(Function1<Object, Object> function1) {
        return TraversableLike.Cclass.filterNot(this, function1);
    }

    public final <B> B foldLeft(B b, Function2<B, Object, B> function2) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, b, function2);
    }

    public final boolean forall(Function1<Object, Object> function1) {
        return IndexedSeqOptimized.Cclass.forall(this, function1);
    }

    public final <U> void foreach(Function1<Object, U> function1) {
        IndexedSeqOptimized.Cclass.foreach(this, function1);
    }

    public final String format(Seq<Object> seq) {
        return StringLike.Cclass.format(this, seq);
    }

    public final int hashCode() {
        return StringOps$.MODULE$.hashCode$extension(repr());
    }

    public final Object head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public final boolean isDefinedAt(int i) {
        return GenSeqLike.Cclass.isDefinedAt(this, i);
    }

    public final boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public final Iterator<Object> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public final Object last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public final int length() {
        return StringOps$.MODULE$.length$extension(repr());
    }

    public final int lengthCompare(int i) {
        return IndexedSeqOptimized.Cclass.lengthCompare(this, i);
    }

    public final <B, That> That map(Function1<Object, B> function1, CanBuildFrom<String, B, That> canBuildFrom) {
        return TraversableLike.Cclass.map(this, function1, canBuildFrom);
    }

    public final String mkString() {
        return StringLike.Cclass.mkString(this);
    }

    public final String mkString(String str) {
        return TraversableOnce.Cclass.mkString(this, str);
    }

    public final String mkString(String str, String str2, String str3) {
        return TraversableOnce.Cclass.mkString(this, str, str2, str3);
    }

    public final /* synthetic */ Builder newBuilder() {
        return StringOps$.MODULE$.newBuilder$extension(repr());
    }

    public final boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public final int prefixLength(Function1<Object, Object> function1) {
        return GenSeqLike.Cclass.prefixLength(this, function1);
    }

    public final String repr() {
        return this.repr;
    }

    public final Object reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public final Iterator<Object> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public final <B> boolean sameElements(GenIterable<B> genIterable) {
        return IndexedSeqOptimized.Cclass.sameElements(this, genIterable);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(GenIterable genIterable) {
        return IterableLike.Cclass.sameElements(this, genIterable);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final int segmentLength(Function1<Object, Object> function1, int i) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, function1, i);
    }

    public final int size() {
        return SeqLike.Cclass.size(this);
    }

    public final /* synthetic */ Object slice(int i, int i2) {
        return StringOps$.MODULE$.slice$extension(repr(), i, i2);
    }

    public final Object sliceWithKnownBound(int i, int i2) {
        return TraversableLike.Cclass.sliceWithKnownBound(this, i, i2);
    }

    public final Object sliceWithKnownDelta(int i, int i2, int i3) {
        return TraversableLike.Cclass.sliceWithKnownDelta(this, i, i2, i3);
    }

    public final String[] split(char c) {
        return StringLike.Cclass.split(this, c);
    }

    public final String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public final String stripSuffix(String str) {
        return StringLike.Cclass.stripSuffix(this, str);
    }

    public final Object tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public final Object take(int i) {
        return IndexedSeqOptimized.Cclass.take(this, i);
    }

    public final <Col> Col to(CanBuildFrom<Nothing$, Object, Col> canBuildFrom) {
        return TraversableLike.Cclass.to(this, canBuildFrom);
    }

    public final <B> Object toArray(ClassTag<B> classTag) {
        return StringLike.Cclass.toArray(this, classTag);
    }

    public final <A1> Buffer<A1> toBuffer() {
        return IndexedSeqLike.Cclass.toBuffer(this);
    }

    public final List<Object> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public final <T, U> Map<T, U> toMap(Predef$$less$colon$less<Object, Tuple2<T, U>> predef$$less$colon$less) {
        return TraversableOnce.Cclass.toMap(this, predef$$less$colon$less);
    }

    public final <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public final Stream<Object> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public final String toString() {
        return StringOps$.MODULE$.toString$extension(repr());
    }

    public final FilterMonadic<Object, String> withFilter(Function1<Object, Object> function1) {
        return TraversableLike.Cclass.withFilter(this, function1);
    }
}
