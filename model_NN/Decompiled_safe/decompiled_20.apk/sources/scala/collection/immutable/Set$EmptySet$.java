package scala.collection.immutable;

import scala.Function1;
import scala.Serializable;
import scala.collection.AbstractSet;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.generic.GenericCompanion;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Set;
import scala.collection.immutable.Traversable;
import scala.runtime.BoxesRunTime;

public class Set$EmptySet$ extends AbstractSet<Object> implements Serializable, Set<Object> {
    public static final Set$EmptySet$ MODULE$ = null;

    static {
        new Set$EmptySet$();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.immutable.Iterable, scala.collection.immutable.Set$EmptySet$, scala.collection.immutable.Set] */
    public Set$EmptySet$() {
        MODULE$ = this;
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Set.Cclass.$init$(this);
    }

    public Set<Object> $minus(Object obj) {
        return this;
    }

    public Set<Object> $plus(Object obj) {
        return new Set.Set1(obj);
    }

    public /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply(obj));
    }

    public GenericCompanion<Set> companion() {
        return Set.Cclass.companion(this);
    }

    public boolean contains(Object obj) {
        return false;
    }

    public /* bridge */ /* synthetic */ scala.collection.Set empty() {
        return (scala.collection.Set) empty();
    }

    public <U> void foreach(Function1<Object, U> function1) {
    }

    public Iterator<Object> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public Set<Object> seq() {
        return Set.Cclass.seq(this);
    }

    public int size() {
        return 0;
    }

    public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
        return thisCollection();
    }

    public <B> Set<B> toSet() {
        return Set.Cclass.toSet(this);
    }
}
