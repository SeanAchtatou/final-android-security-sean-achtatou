package scala.collection.immutable;

import scala.Function1;
import scala.Serializable;
import scala.collection.TraversableLike;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction0;

public final class Stream$$anonfun$map$1 extends AbstractFunction0<Stream<B>> implements Serializable {
    private final /* synthetic */ Stream $outer;
    private final Function1 f$1;

    public Stream$$anonfun$map$1(Stream stream, Stream<A> stream2) {
        if (stream == null) {
            throw new NullPointerException();
        }
        this.$outer = stream;
        this.f$1 = stream2;
    }

    public final Stream<B> apply() {
        Object map;
        Stream stream = (Stream) this.$outer.tail();
        Function1 function1 = this.f$1;
        CanBuildFrom canBuildFrom = Stream$.MODULE$.canBuildFrom();
        if (!(canBuildFrom.apply(stream.repr()) instanceof Stream.StreamBuilder)) {
            map = TraversableLike.Cclass.map(stream, function1, canBuildFrom);
        } else if (stream.isEmpty()) {
            map = Stream$Empty$.MODULE$;
        } else {
            Stream$cons$ stream$cons$ = Stream$cons$.MODULE$;
            map = new Stream.Cons(function1.apply(stream.head()), new Stream$$anonfun$map$1(stream, function1));
        }
        return (Stream) map;
    }
}
