package scala.collection.immutable;

import scala.MatchError;
import scala.collection.AbstractIterable;
import scala.collection.AbstractIterator;
import scala.collection.Iterator;
import scala.collection.immutable.HashMap;
import scala.collection.immutable.HashSet;

public abstract class TrieIterator<T> extends AbstractIterator<T> {
    private final Iterable<T>[] elems;
    public Iterable<T>[] scala$collection$immutable$TrieIterator$$arrayD = initArrayD();
    public Iterable<T>[][] scala$collection$immutable$TrieIterator$$arrayStack = initArrayStack();
    public int scala$collection$immutable$TrieIterator$$depth = initDepth();
    public int scala$collection$immutable$TrieIterator$$posD = initPosD();
    public int[] scala$collection$immutable$TrieIterator$$posStack = initPosStack();
    public Iterator<T> scala$collection$immutable$TrieIterator$$subIter = initSubIter();

    public TrieIterator(Iterable<T>[] iterableArr) {
        this.elems = iterableArr;
    }

    private Iterable<T>[] getElems(Iterable<T> iterable) {
        AbstractIterable[] abstractIterableArr;
        if (iterable instanceof HashMap.HashTrieMap) {
            abstractIterableArr = (AbstractIterable[]) ((HashMap.HashTrieMap) iterable).elems();
        } else if (iterable instanceof HashSet.HashTrieSet) {
            abstractIterableArr = (AbstractIterable[]) ((HashSet.HashTrieSet) iterable).elems();
        } else {
            throw new MatchError(iterable);
        }
        return (Iterable[]) abstractIterableArr;
    }

    private boolean isContainer(Object obj) {
        return obj instanceof HashMap.HashMap1 ? true : obj instanceof HashSet.HashSet1;
    }

    private boolean isTrie(Object obj) {
        return obj instanceof HashMap.HashTrieMap ? true : obj instanceof HashSet.HashTrieSet;
    }

    private T next0(Iterable<T>[] iterableArr, int i) {
        while (true) {
            if (i == iterableArr.length - 1) {
                this.scala$collection$immutable$TrieIterator$$depth--;
                if (this.scala$collection$immutable$TrieIterator$$depth >= 0) {
                    this.scala$collection$immutable$TrieIterator$$arrayD = this.scala$collection$immutable$TrieIterator$$arrayStack[this.scala$collection$immutable$TrieIterator$$depth];
                    this.scala$collection$immutable$TrieIterator$$posD = this.scala$collection$immutable$TrieIterator$$posStack[this.scala$collection$immutable$TrieIterator$$depth];
                    this.scala$collection$immutable$TrieIterator$$arrayStack[this.scala$collection$immutable$TrieIterator$$depth] = null;
                } else {
                    this.scala$collection$immutable$TrieIterator$$arrayD = null;
                    this.scala$collection$immutable$TrieIterator$$posD = 0;
                }
            } else {
                this.scala$collection$immutable$TrieIterator$$posD++;
            }
            Iterable<T> iterable = iterableArr[i];
            if (isContainer(iterable)) {
                return getElem(iterable);
            }
            if (isTrie(iterable)) {
                if (this.scala$collection$immutable$TrieIterator$$depth >= 0) {
                    this.scala$collection$immutable$TrieIterator$$arrayStack[this.scala$collection$immutable$TrieIterator$$depth] = this.scala$collection$immutable$TrieIterator$$arrayD;
                    this.scala$collection$immutable$TrieIterator$$posStack[this.scala$collection$immutable$TrieIterator$$depth] = this.scala$collection$immutable$TrieIterator$$posD;
                }
                this.scala$collection$immutable$TrieIterator$$depth++;
                this.scala$collection$immutable$TrieIterator$$arrayD = getElems(iterable);
                this.scala$collection$immutable$TrieIterator$$posD = 0;
                iterableArr = getElems(iterable);
                i = 0;
            } else {
                this.scala$collection$immutable$TrieIterator$$subIter = iterable.iterator();
                return next();
            }
        }
    }

    public abstract T getElem(Object obj);

    public boolean hasNext() {
        return this.scala$collection$immutable$TrieIterator$$subIter != null || this.scala$collection$immutable$TrieIterator$$depth >= 0;
    }

    public Iterable<T>[] initArrayD() {
        return this.elems;
    }

    public Iterable<T>[][] initArrayStack() {
        return new Iterable[6][];
    }

    public int initDepth() {
        return 0;
    }

    public int initPosD() {
        return 0;
    }

    public int[] initPosStack() {
        return new int[6];
    }

    public Iterator<T> initSubIter() {
        return null;
    }

    public T next() {
        if (this.scala$collection$immutable$TrieIterator$$subIter == null) {
            return next0(this.scala$collection$immutable$TrieIterator$$arrayD, this.scala$collection$immutable$TrieIterator$$posD);
        }
        T next = this.scala$collection$immutable$TrieIterator$$subIter.next();
        if (this.scala$collection$immutable$TrieIterator$$subIter.hasNext()) {
            return next;
        }
        this.scala$collection$immutable$TrieIterator$$subIter = null;
        return next;
    }
}
