package scala.collection.immutable;

import scala.Serializable;
import scala.runtime.AbstractFunction1;

public final class WrappedString$$anonfun$newBuilder$1 extends AbstractFunction1<String, WrappedString> implements Serializable {
    public final WrappedString apply(String str) {
        return new WrappedString(str);
    }
}
