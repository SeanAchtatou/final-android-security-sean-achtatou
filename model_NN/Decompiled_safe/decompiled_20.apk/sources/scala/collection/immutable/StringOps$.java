package scala.collection.immutable;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.collection.mutable.StringBuilder;
import scala.collection.mutable.StringBuilder$;

public final class StringOps$ {
    public static final StringOps$ MODULE$ = null;

    static {
        new StringOps$();
    }

    private StringOps$() {
        MODULE$ = this;
    }

    public final char apply$extension(String str, int i) {
        return str.charAt(i);
    }

    public final boolean equals$extension(String str, Object obj) {
        if (obj instanceof StringOps) {
            String repr = obj == null ? null : ((StringOps) obj).repr();
            if (str != null ? str.equals(repr) : repr == null) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode$extension(String str) {
        return str.hashCode();
    }

    public final int length$extension(String str) {
        return str.length();
    }

    public final StringBuilder newBuilder$extension(String str) {
        return StringBuilder$.MODULE$.newBuilder();
    }

    public final WrappedString seq$extension(String str) {
        return new WrappedString(str);
    }

    public final String slice$extension(String str, int i, int i2) {
        if (i < 0) {
            i = 0;
        }
        if (i2 <= i || i >= str.length()) {
            return PoiTypeDef.All;
        }
        if (i2 > length$extension(str)) {
            i2 = length$extension(str);
        }
        return str.substring(i, i2);
    }

    public final WrappedString thisCollection$extension(String str) {
        return new WrappedString(str);
    }

    public final WrappedString toCollection$extension(String str, String str2) {
        return new WrappedString(str2);
    }

    public final String toString$extension(String str) {
        return str;
    }
}
