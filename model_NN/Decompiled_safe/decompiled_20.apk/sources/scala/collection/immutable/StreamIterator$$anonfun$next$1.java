package scala.collection.immutable;

import scala.Serializable;
import scala.runtime.AbstractFunction0;

public final class StreamIterator$$anonfun$next$1 extends AbstractFunction0<Stream<A>> implements Serializable {
    private final Stream cur$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.immutable.StreamIterator<A>, scala.collection.immutable.Stream] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public StreamIterator$$anonfun$next$1(scala.collection.immutable.StreamIterator r1, scala.collection.immutable.StreamIterator<A> r2) {
        /*
            r0 = this;
            r0.cur$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.StreamIterator$$anonfun$next$1.<init>(scala.collection.immutable.StreamIterator, scala.collection.immutable.Stream):void");
    }

    public final Stream<A> apply() {
        return (Stream) this.cur$1.tail();
    }
}
