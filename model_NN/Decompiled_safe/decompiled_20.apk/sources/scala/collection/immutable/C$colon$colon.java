package scala.collection.immutable;

import scala.Product;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* renamed from: scala.collection.immutable.$colon$colon  reason: invalid class name */
public final class C$colon$colon<B> extends List<B> implements Product {
    private B scala$collection$immutable$$colon$colon$$hd;
    private List<B> tl;

    public C$colon$colon(B b, List<B> list) {
        this.scala$collection$immutable$$colon$colon$$hd = b;
        this.tl = list;
    }

    public final B hd$1() {
        return this.scala$collection$immutable$$colon$colon$$hd;
    }

    public final B head() {
        return scala$collection$immutable$$colon$colon$$hd();
    }

    public final boolean isEmpty() {
        return false;
    }

    public final int productArity() {
        return 2;
    }

    public final Object productElement(int i) {
        switch (i) {
            case 0:
                return hd$1();
            case 1:
                return tl$1();
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
    }

    public final Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public final String productPrefix() {
        return "::";
    }

    public final B scala$collection$immutable$$colon$colon$$hd() {
        return this.scala$collection$immutable$$colon$colon$$hd;
    }

    public final List<B> tail() {
        return tl();
    }

    public final List<B> tl() {
        return this.tl;
    }

    public final List<B> tl$1() {
        return this.tl;
    }

    public final void tl_$eq(List<B> list) {
        this.tl = list;
    }
}
