package scala.collection.immutable;

import scala.Array$;
import scala.Function1;
import scala.Predef$;
import scala.Serializable;
import scala.collection.AbstractSet;
import scala.collection.CustomParallelizable;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.Seq;
import scala.collection.SetLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Set;
import scala.collection.immutable.Traversable;
import scala.collection.parallel.immutable.ParHashSet;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public class HashSet<A> extends AbstractSet<A> implements Serializable, CustomParallelizable<A, ParHashSet<A>>, SetLike<A, HashSet<A>> {

    public class HashSet1<A> extends HashSet<A> {
        private final int hash;
        private final A key;

        public HashSet1(A a, int i) {
            this.key = a;
            this.hash = i;
        }

        public <U> void foreach(Function1<A, U> function1) {
            function1.apply(key());
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean get0(A r5, int r6, int r7) {
            /*
                r4 = this;
                r0 = 1
                r1 = 0
                int r2 = r4.hash()
                if (r6 != r2) goto L_0x0031
                java.lang.Object r2 = r4.key()
                if (r5 != r2) goto L_0x0012
                r2 = r0
            L_0x000f:
                if (r2 == 0) goto L_0x0031
            L_0x0011:
                return r0
            L_0x0012:
                if (r5 != 0) goto L_0x0016
                r2 = r1
                goto L_0x000f
            L_0x0016:
                boolean r3 = r5 instanceof java.lang.Number
                if (r3 == 0) goto L_0x0021
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r2 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r2)
                goto L_0x000f
            L_0x0021:
                boolean r3 = r5 instanceof java.lang.Character
                if (r3 == 0) goto L_0x002c
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r2 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r2)
                goto L_0x000f
            L_0x002c:
                boolean r2 = r5.equals(r2)
                goto L_0x000f
            L_0x0031:
                r0 = r1
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashSet.HashSet1.get0(java.lang.Object, int, int):boolean");
        }

        public int hash() {
            return this.hash;
        }

        public Iterator<A> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.genericWrapArray(new Object[]{key()}));
        }

        public A key() {
            return this.key;
        }

        public HashSet<A> removed0(A a, int i, int i2) {
            if (i != hash()) {
                return this;
            }
            A key2 = key();
            return a == key2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, key2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, key2) : a.equals(key2) ? HashSet$.MODULE$.empty() : this;
        }

        public int size() {
            return 1;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.HashSet<A> updated0(A r7, int r8, int r9) {
            /*
                r6 = this;
                int r0 = r6.hash()
                if (r8 != r0) goto L_0x0031
                java.lang.Object r1 = r6.key()
                if (r7 != r1) goto L_0x0010
                r0 = 1
            L_0x000d:
                if (r0 == 0) goto L_0x0031
            L_0x000f:
                return r6
            L_0x0010:
                if (r7 != 0) goto L_0x0014
                r0 = 0
                goto L_0x000d
            L_0x0014:
                boolean r0 = r7 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0020
                r0 = r7
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r1)
                goto L_0x000d
            L_0x0020:
                boolean r0 = r7 instanceof java.lang.Character
                if (r0 == 0) goto L_0x002c
                r0 = r7
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r1)
                goto L_0x000d
            L_0x002c:
                boolean r0 = r7.equals(r1)
                goto L_0x000d
            L_0x0031:
                int r0 = r6.hash()
                if (r8 == r0) goto L_0x004a
                scala.collection.immutable.HashSet$ r0 = scala.collection.immutable.HashSet$.MODULE$
                int r1 = r6.hash()
                scala.collection.immutable.HashSet$HashSet1 r4 = new scala.collection.immutable.HashSet$HashSet1
                r4.<init>(r7, r8)
                r2 = r6
                r3 = r8
                r5 = r9
                scala.collection.immutable.HashSet$HashTrieSet r6 = r0.scala$collection$immutable$HashSet$$makeHashTrieSet(r1, r2, r3, r4, r5)
                goto L_0x000f
            L_0x004a:
                scala.collection.immutable.HashSet$HashSetCollision1 r0 = new scala.collection.immutable.HashSet$HashSetCollision1
                scala.collection.immutable.ListSet$ r1 = scala.collection.immutable.ListSet$.MODULE$
                scala.collection.immutable.ListSet r1 = r1.empty()
                java.lang.Object r2 = r6.key()
                scala.collection.immutable.ListSet r1 = r1.$plus(r2)
                scala.collection.immutable.ListSet r1 = r1.$plus(r7)
                r0.<init>(r8, r1)
                r6 = r0
                goto L_0x000f
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashSet.HashSet1.updated0(java.lang.Object, int, int):scala.collection.immutable.HashSet");
        }
    }

    public class HashSetCollision1<A> extends HashSet<A> {
        private final int hash;
        private final ListSet<A> ks;

        public HashSetCollision1(int i, ListSet<A> listSet) {
            this.hash = i;
            this.ks = listSet;
        }

        public <U> void foreach(Function1<A, U> function1) {
            ks().foreach(function1);
        }

        public boolean get0(A a, int i, int i2) {
            if (i == hash()) {
                return ks().contains(a);
            }
            return false;
        }

        public int hash() {
            return this.hash;
        }

        public Iterator<A> iterator() {
            return ks().iterator();
        }

        public ListSet<A> ks() {
            return this.ks;
        }

        public HashSet<A> removed0(A a, int i, int i2) {
            if (i != hash()) {
                return this;
            }
            ListSet $minus = ks().$minus((Object) a);
            return $minus.isEmpty() ? HashSet$.MODULE$.empty() : $minus.tail().isEmpty() ? new HashSet1($minus.head(), i) : new HashSetCollision1(i, $minus);
        }

        public int size() {
            return ks().size();
        }

        public HashSet<A> updated0(A a, int i, int i2) {
            if (i == hash()) {
                return new HashSetCollision1(i, ks().$plus((Object) a));
            }
            return HashSet$.MODULE$.scala$collection$immutable$HashSet$$makeHashTrieSet(hash(), this, i, new HashSet1(a, i), i2);
        }
    }

    public class HashTrieSet<A> extends HashSet<A> {
        private final int bitmap;
        private final HashSet<A>[] elems;
        private final int size0;

        public HashTrieSet(int i, HashSet<A>[] hashSetArr, int i2) {
            this.bitmap = i;
            this.elems = hashSetArr;
            this.size0 = i2;
            Predef$.MODULE$.m0assert(Integer.bitCount(i) == hashSetArr.length);
        }

        private int bitmap() {
            return this.bitmap;
        }

        private int size0() {
            return this.size0;
        }

        public HashSet<A>[] elems() {
            return this.elems;
        }

        public <U> void foreach(Function1<A, U> function1) {
            for (HashSet foreach : elems()) {
                foreach.foreach(function1);
            }
        }

        public boolean get0(A a, int i, int i2) {
            int i3 = (i >>> i2) & 31;
            int i4 = 1 << i3;
            if (bitmap() == -1) {
                return elems()[i3 & 31].get0(a, i, i2 + 5);
            }
            if ((bitmap() & i4) == 0) {
                return false;
            }
            return elems()[Integer.bitCount(bitmap() & (i4 - 1))].get0(a, i, i2 + 5);
        }

        public TrieIterator<A> iterator() {
            return new HashSet$HashTrieSet$$anon$1(this);
        }

        public HashSet<A> removed0(A a, int i, int i2) {
            HashSet hashSet;
            HashSet removed0;
            int i3 = 1 << ((i >>> i2) & 31);
            int bitCount = Integer.bitCount(bitmap() & (i3 - 1));
            if ((bitmap() & i3) == 0 || hashSet == (removed0 = (hashSet = elems()[bitCount]).removed0(a, i, i2 + 5))) {
                return this;
            }
            if (removed0.isEmpty()) {
                int bitmap2 = bitmap() ^ i3;
                if (bitmap2 == 0) {
                    return HashSet$.MODULE$.empty();
                }
                HashSet<A>[] hashSetArr = new HashSet[(elems().length - 1)];
                Array$.MODULE$.copy(elems(), 0, hashSetArr, 0, bitCount);
                Array$.MODULE$.copy(elems(), bitCount + 1, hashSetArr, bitCount, (elems().length - bitCount) - 1);
                return (hashSetArr.length != 1 || (hashSetArr[0] instanceof HashTrieSet)) ? new HashTrieSet(bitmap2, hashSetArr, size() - hashSet.size()) : hashSetArr[0];
            }
            HashSet[] hashSetArr2 = new HashSet[elems().length];
            Array$.MODULE$.copy(elems(), 0, hashSetArr2, 0, elems().length);
            hashSetArr2[bitCount] = removed0;
            return new HashTrieSet(bitmap(), hashSetArr2, (removed0.size() - hashSet.size()) + size());
        }

        public int size() {
            return size0();
        }

        public HashSet<A> updated0(A a, int i, int i2) {
            int i3 = 1 << ((i >>> i2) & 31);
            int bitCount = Integer.bitCount(bitmap() & (i3 - 1));
            if ((bitmap() & i3) != 0) {
                HashSet hashSet = elems()[bitCount];
                HashSet updated0 = hashSet.updated0(a, i, i2 + 5);
                if (hashSet == updated0) {
                    return this;
                }
                HashSet[] hashSetArr = new HashSet[elems().length];
                Array$.MODULE$.copy(elems(), 0, hashSetArr, 0, elems().length);
                hashSetArr[bitCount] = updated0;
                return new HashTrieSet(bitmap(), hashSetArr, size() + (updated0.size() - hashSet.size()));
            }
            HashSet[] hashSetArr2 = new HashSet[(elems().length + 1)];
            Array$.MODULE$.copy(elems(), 0, hashSetArr2, 0, bitCount);
            hashSetArr2[bitCount] = new HashSet1(a, i);
            int i4 = bitCount;
            Array$.MODULE$.copy(elems(), i4, hashSetArr2, bitCount + 1, elems().length - bitCount);
            return new HashTrieSet(bitmap() | i3, hashSetArr2, size() + 1);
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Traversable, scala.collection.immutable.Iterable, scala.collection.immutable.HashSet, scala.collection.CustomParallelizable, scala.collection.immutable.Set] */
    public HashSet() {
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Set.Cclass.$init$(this);
        CustomParallelizable.Cclass.$init$(this);
    }

    public HashSet<A> $minus(A a) {
        return removed0(a, computeHash(a), 0);
    }

    public HashSet<A> $plus(A a) {
        return updated0(a, computeHash(a), 0);
    }

    public HashSet<A> $plus(A a, A a2, Seq<A> seq) {
        return (HashSet) $plus((Object) a).$plus((Object) a2).$plus$plus(seq);
    }

    public /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply(obj));
    }

    public GenericCompanion<HashSet> companion() {
        return HashSet$.MODULE$;
    }

    public int computeHash(A a) {
        return improve(elemHashCode(a));
    }

    public boolean contains(A a) {
        return get0(a, computeHash(a), 0);
    }

    public int elemHashCode(A a) {
        return ScalaRunTime$.MODULE$.hash((Object) a);
    }

    public HashSet<A> empty() {
        return HashSet$.MODULE$.empty();
    }

    public <U> void foreach(Function1<A, U> function1) {
    }

    public boolean get0(A a, int i, int i2) {
        return false;
    }

    public final int improve(int i) {
        int i2 = ((i << 9) ^ -1) + i;
        int i3 = i2 ^ (i2 >>> 14);
        int i4 = i3 + (i3 << 4);
        return i4 ^ (i4 >>> 10);
    }

    public Iterator<A> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public HashSet<A> removed0(A a, int i, int i2) {
        return this;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.HashSet, scala.collection.immutable.Set] */
    public Set<A> seq() {
        return Set.Cclass.seq(this);
    }

    public int size() {
        return 0;
    }

    public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
        return thisCollection();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.HashSet, scala.collection.immutable.Set] */
    public <B> Set<B> toSet() {
        return Set.Cclass.toSet(this);
    }

    public HashSet<A> updated0(A a, int i, int i2) {
        return new HashSet1(a, i);
    }
}
