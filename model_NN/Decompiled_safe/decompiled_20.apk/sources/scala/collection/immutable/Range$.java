package scala.collection.immutable;

import scala.Serializable;

public final class Range$ implements Serializable {
    public static final Range$ MODULE$ = null;
    private final int MAX_PRINT = 512;

    static {
        new Range$();
    }

    private Range$() {
        MODULE$ = this;
    }

    public final int MAX_PRINT() {
        return this.MAX_PRINT;
    }

    public final Range apply(int i, int i2) {
        return new Range(i, i2, 1);
    }
}
