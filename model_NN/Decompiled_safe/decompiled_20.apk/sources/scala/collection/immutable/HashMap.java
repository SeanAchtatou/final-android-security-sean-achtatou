package scala.collection.immutable;

import scala.Array$;
import scala.Function1;
import scala.None$;
import scala.Option;
import scala.Predef$;
import scala.Serializable;
import scala.Tuple2;
import scala.collection.CustomParallelizable;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.Seq;
import scala.collection.Traversable;
import scala.collection.parallel.immutable.ParHashMap;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public class HashMap<A, B> extends AbstractMap<A, B> implements Serializable, CustomParallelizable<Tuple2<A, B>, ParHashMap<A, B>> {

    public class HashMap1<A, B> extends HashMap<A, B> {
        private final int hash;
        private final A key;
        private Tuple2<A, B> kv;
        private final B value;

        public HashMap1(A a, int i, B b, Tuple2<A, B> tuple2) {
            this.key = a;
            this.hash = i;
            this.value = b;
            this.kv = tuple2;
        }

        public Tuple2<A, B> ensurePair() {
            if (kv() != null) {
                return kv();
            }
            kv_$eq(new Tuple2(key(), value()));
            return kv();
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> function1) {
            function1.apply(ensurePair());
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.Option<B> get0(A r3, int r4, int r5) {
            /*
                r2 = this;
                int r0 = r2.hash()
                if (r4 != r0) goto L_0x0038
                java.lang.Object r0 = r2.key()
                if (r3 != r0) goto L_0x0019
                r0 = 1
            L_0x000d:
                if (r0 == 0) goto L_0x0038
                scala.Some r0 = new scala.Some
                java.lang.Object r1 = r2.value()
                r0.<init>(r1)
            L_0x0018:
                return r0
            L_0x0019:
                if (r3 != 0) goto L_0x001d
                r0 = 0
                goto L_0x000d
            L_0x001d:
                boolean r1 = r3 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0028
                java.lang.Number r3 = (java.lang.Number) r3
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r3, r0)
                goto L_0x000d
            L_0x0028:
                boolean r1 = r3 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0033
                java.lang.Character r3 = (java.lang.Character) r3
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r3, r0)
                goto L_0x000d
            L_0x0033:
                boolean r0 = r3.equals(r0)
                goto L_0x000d
            L_0x0038:
                scala.None$ r0 = scala.None$.MODULE$
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashMap.HashMap1.get0(java.lang.Object, int, int):scala.Option");
        }

        public int hash() {
            return this.hash;
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Iterator$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{ensurePair()}));
        }

        public A key() {
            return this.key;
        }

        public Tuple2<A, B> kv() {
            return this.kv;
        }

        public void kv_$eq(Tuple2<A, B> tuple2) {
            this.kv = tuple2;
        }

        public HashMap<A, B> removed0(A a, int i, int i2) {
            if (i != hash()) {
                return this;
            }
            A key2 = key();
            return a == key2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, key2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, key2) : a.equals(key2) ? HashMap$.MODULE$.empty() : this;
        }

        public int size() {
            return 1;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.HashMap<A, B1> updated0(A r8, int r9, int r10, B1 r11, scala.Tuple2<A, B1> r12, scala.collection.immutable.HashMap.Merger<A, B1> r13) {
            /*
                r7 = this;
                int r0 = r7.hash()
                if (r9 != r0) goto L_0x0055
                java.lang.Object r1 = r7.key()
                if (r8 != r1) goto L_0x0018
                r0 = 1
            L_0x000d:
                if (r0 == 0) goto L_0x0055
                if (r13 != 0) goto L_0x003f
                java.lang.Object r0 = r7.value()
                if (r0 != r11) goto L_0x0039
            L_0x0017:
                return r7
            L_0x0018:
                if (r8 != 0) goto L_0x001c
                r0 = 0
                goto L_0x000d
            L_0x001c:
                boolean r0 = r8 instanceof java.lang.Number
                if (r0 == 0) goto L_0x0028
                r0 = r8
                java.lang.Number r0 = (java.lang.Number) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r1)
                goto L_0x000d
            L_0x0028:
                boolean r0 = r8 instanceof java.lang.Character
                if (r0 == 0) goto L_0x0034
                r0 = r8
                java.lang.Character r0 = (java.lang.Character) r0
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r1)
                goto L_0x000d
            L_0x0034:
                boolean r0 = r8.equals(r1)
                goto L_0x000d
            L_0x0039:
                scala.collection.immutable.HashMap$HashMap1 r7 = new scala.collection.immutable.HashMap$HashMap1
                r7.<init>(r8, r9, r11, r12)
                goto L_0x0017
            L_0x003f:
                scala.Tuple2 r0 = r7.kv()
                scala.Tuple2 r0 = r13.apply(r0, r12)
                scala.collection.immutable.HashMap$HashMap1 r7 = new scala.collection.immutable.HashMap$HashMap1
                java.lang.Object r1 = r0._1()
                java.lang.Object r2 = r0._2()
                r7.<init>(r1, r9, r2, r0)
                goto L_0x0017
            L_0x0055:
                int r0 = r7.hash()
                if (r9 == r0) goto L_0x006f
                scala.collection.immutable.HashMap$HashMap1 r4 = new scala.collection.immutable.HashMap$HashMap1
                r4.<init>(r8, r9, r11, r12)
                scala.collection.immutable.HashMap$ r0 = scala.collection.immutable.HashMap$.MODULE$
                int r1 = r7.hash()
                r6 = 2
                r2 = r7
                r3 = r9
                r5 = r10
                scala.collection.immutable.HashMap$HashTrieMap r7 = r0.scala$collection$immutable$HashMap$$makeHashTrieMap(r1, r2, r3, r4, r5, r6)
                goto L_0x0017
            L_0x006f:
                scala.collection.immutable.HashMap$HashMapCollision1 r0 = new scala.collection.immutable.HashMap$HashMapCollision1
                scala.collection.immutable.ListMap$ r1 = scala.collection.immutable.ListMap$.MODULE$
                scala.collection.immutable.ListMap r1 = r1.empty()
                java.lang.Object r2 = r7.key()
                java.lang.Object r3 = r7.value()
                scala.collection.immutable.ListMap r1 = r1.updated(r2, r3)
                scala.collection.immutable.ListMap r1 = r1.updated(r8, r11)
                r0.<init>(r9, r1)
                r7 = r0
                goto L_0x0017
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashMap.HashMap1.updated0(java.lang.Object, int, int, java.lang.Object, scala.Tuple2, scala.collection.immutable.HashMap$Merger):scala.collection.immutable.HashMap");
        }

        public B value() {
            return this.value;
        }
    }

    public class HashMapCollision1<A, B> extends HashMap<A, B> {
        private final int hash;
        private final ListMap<A, B> kvs;

        public HashMapCollision1(int i, ListMap<A, B> listMap) {
            this.hash = i;
            this.kvs = listMap;
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> function1) {
            kvs().foreach(function1);
        }

        public Option<B> get0(A a, int i, int i2) {
            return i == hash() ? kvs().get(a) : None$.MODULE$;
        }

        public int hash() {
            return this.hash;
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return kvs().iterator();
        }

        public ListMap<A, B> kvs() {
            return this.kvs;
        }

        public HashMap<A, B> removed0(A a, int i, int i2) {
            if (i != hash()) {
                return this;
            }
            ListMap $minus = kvs().$minus((Object) a);
            if ($minus.isEmpty()) {
                return HashMap$.MODULE$.empty();
            }
            if (!$minus.tail().isEmpty()) {
                return new HashMapCollision1(i, $minus);
            }
            Tuple2 tuple2 = (Tuple2) $minus.head();
            return new HashMap1(tuple2._1(), i, tuple2._2(), tuple2);
        }

        public int size() {
            return kvs().size();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: scala.collection.immutable.ListMap.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.ListMap<A, B1>
         arg types: [A, B1]
         candidates:
          scala.collection.immutable.ListMap.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map
          scala.collection.immutable.AbstractMap.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map<A, B1>
          scala.collection.immutable.ListMap.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.ListMap<A, B1> */
        public <B1> HashMap<A, B1> updated0(A a, int i, int i2, B1 b1, Tuple2<A, B1> tuple2, Merger<A, B1> merger) {
            if (i == hash()) {
                return (merger == null || !kvs().contains(a)) ? new HashMapCollision1(i, kvs().updated((Object) a, (Object) b1)) : new HashMapCollision1(i, kvs().$plus((Tuple2) merger.apply(new Tuple2(a, kvs().apply(a)), tuple2)));
            }
            return HashMap$.MODULE$.scala$collection$immutable$HashMap$$makeHashTrieMap(hash(), this, i, new HashMap1(a, i, b1, tuple2), i2, size() + 1);
        }
    }

    public class HashTrieMap<A, B> extends HashMap<A, B> {
        private final int bitmap;
        private final HashMap<A, B>[] elems;
        private final int size0;

        public HashTrieMap(int i, HashMap<A, B>[] hashMapArr, int i2) {
            this.bitmap = i;
            this.elems = hashMapArr;
            this.size0 = i2;
        }

        public int bitmap() {
            return this.bitmap;
        }

        public HashMap<A, B>[] elems() {
            return this.elems;
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> function1) {
            for (HashMap foreach : elems()) {
                foreach.foreach(function1);
            }
        }

        public Option<B> get0(A a, int i, int i2) {
            int i3 = (i >>> i2) & 31;
            int i4 = 1 << i3;
            if (bitmap() == -1) {
                return elems()[i3 & 31].get0(a, i, i2 + 5);
            }
            if ((bitmap() & i4) == 0) {
                return None$.MODULE$;
            }
            return elems()[Integer.bitCount(bitmap() & (i4 - 1))].get0(a, i, i2 + 5);
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return new HashMap$HashTrieMap$$anon$1(this);
        }

        public HashMap<A, B> removed0(A a, int i, int i2) {
            HashMap<A, B> hashMap;
            HashMap<A, B> removed0;
            int i3 = 1 << ((i >>> i2) & 31);
            int bitCount = Integer.bitCount(bitmap() & (i3 - 1));
            if ((bitmap() & i3) == 0 || (removed0 = (hashMap = elems()[bitCount]).removed0(a, i, i2 + 5)) == hashMap) {
                return this;
            }
            if (removed0.isEmpty()) {
                int bitmap2 = bitmap() ^ i3;
                if (bitmap2 == 0) {
                    return HashMap$.MODULE$.empty();
                }
                HashMap<A, B>[] hashMapArr = new HashMap[(elems().length - 1)];
                Array$.MODULE$.copy(elems(), 0, hashMapArr, 0, bitCount);
                Array$.MODULE$.copy(elems(), bitCount + 1, hashMapArr, bitCount, (elems().length - bitCount) - 1);
                return (hashMapArr.length != 1 || (hashMapArr[0] instanceof HashTrieMap)) ? new HashTrieMap(bitmap2, hashMapArr, size() - hashMap.size()) : hashMapArr[0];
            } else if (elems().length == 1 && !(removed0 instanceof HashTrieMap)) {
                return removed0;
            } else {
                HashMap[] hashMapArr2 = new HashMap[elems().length];
                Array$.MODULE$.copy(elems(), 0, hashMapArr2, 0, elems().length);
                hashMapArr2[bitCount] = removed0;
                return new HashTrieMap(bitmap(), hashMapArr2, size() + (removed0.size() - hashMap.size()));
            }
        }

        public int size() {
            return size0();
        }

        public int size0() {
            return this.size0;
        }

        public <B1> HashMap<A, B1> updated0(A a, int i, int i2, B1 b1, Tuple2<A, B1> tuple2, Merger<A, B1> merger) {
            int i3 = 1 << ((i >>> i2) & 31);
            int bitCount = Integer.bitCount(bitmap() & (i3 - 1));
            if ((bitmap() & i3) != 0) {
                HashMap<A, B1> hashMap = elems()[bitCount];
                HashMap<A, B1> updated0 = hashMap.updated0(a, i, i2 + 5, b1, tuple2, merger);
                if (updated0 == hashMap) {
                    return this;
                }
                HashMap[] hashMapArr = new HashMap[elems().length];
                Array$.MODULE$.copy(elems(), 0, hashMapArr, 0, elems().length);
                hashMapArr[bitCount] = updated0;
                return new HashTrieMap(bitmap(), hashMapArr, (updated0.size() - hashMap.size()) + size());
            }
            HashMap[] hashMapArr2 = new HashMap[(elems().length + 1)];
            Array$.MODULE$.copy(elems(), 0, hashMapArr2, 0, bitCount);
            hashMapArr2[bitCount] = new HashMap1(a, i, b1, tuple2);
            int i4 = bitCount;
            Array$.MODULE$.copy(elems(), i4, hashMapArr2, bitCount + 1, elems().length - bitCount);
            return new HashTrieMap(bitmap() | i3, hashMapArr2, size() + 1);
        }
    }

    public abstract class Merger<A, B> {
        public abstract Tuple2<A, B> apply(Tuple2<A, B> tuple2, Tuple2<A, B> tuple22);
    }

    public HashMap() {
        CustomParallelizable.Cclass.$init$(this);
    }

    public HashMap<A, B> $minus(A a) {
        return removed0(a, computeHash(a), 0);
    }

    public <B1> HashMap<A, B1> $plus(Tuple2<A, B1> tuple2) {
        return updated0(tuple2._1(), computeHash(tuple2._1()), 0, tuple2._2(), tuple2, null);
    }

    public <B1> HashMap<A, B1> $plus(Tuple2<A, B1> tuple2, Tuple2<A, B1> tuple22, Seq<Tuple2<A, B1>> seq) {
        return (HashMap) $plus((Tuple2) tuple2).$plus((Tuple2) tuple22).$plus$plus(seq, HashMap$.MODULE$.canBuildFrom());
    }

    public int computeHash(A a) {
        return improve(elemHashCode(a));
    }

    public int elemHashCode(A a) {
        return ScalaRunTime$.MODULE$.hash((Object) a);
    }

    public HashMap<A, B> empty() {
        return HashMap$.MODULE$.empty();
    }

    public /* bridge */ /* synthetic */ Object filterNot(Function1 function1) {
        return filterNot(function1);
    }

    public <U> void foreach(Function1<Tuple2<A, B>, U> function1) {
    }

    public Option<B> get(A a) {
        return get0(a, computeHash(a), 0);
    }

    public Option<B> get0(A a, int i, int i2) {
        return None$.MODULE$;
    }

    public final int improve(int i) {
        int i2 = ((i << 9) ^ -1) + i;
        int i3 = i2 ^ (i2 >>> 14);
        int i4 = i3 + (i3 << 4);
        return i4 ^ (i4 >>> 10);
    }

    public Iterator<Tuple2<A, B>> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public HashMap<A, B> removed0(A a, int i, int i2) {
        return this;
    }

    public int size() {
        return 0;
    }

    public /* bridge */ /* synthetic */ Traversable thisCollection() {
        return thisCollection();
    }

    public <B1> HashMap<A, B1> updated(A a, B1 b1) {
        return updated0(a, computeHash(a), 0, b1, null, null);
    }

    public <B1> HashMap<A, B1> updated0(A a, int i, int i2, B1 b1, Tuple2<A, B1> tuple2, Merger<A, B1> merger) {
        return new HashMap1(a, i, b1, tuple2);
    }
}
