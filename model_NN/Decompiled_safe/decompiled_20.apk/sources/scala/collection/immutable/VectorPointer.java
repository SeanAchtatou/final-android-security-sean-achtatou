package scala.collection.immutable;

import scala.MatchError;
import scala.Predef$;
import scala.compat.Platform$;
import scala.runtime.BoxesRunTime;

public interface VectorPointer<T> {

    /* renamed from: scala.collection.immutable.VectorPointer$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(VectorPointer vectorPointer) {
        }

        public static final Object[] copyOf(VectorPointer vectorPointer, Object[] objArr) {
            if (objArr == null) {
                Predef$.MODULE$.println("NULL");
            }
            Object[] objArr2 = new Object[objArr.length];
            Platform$ platform$ = Platform$.MODULE$;
            System.arraycopy(objArr, 0, objArr2, 0, objArr.length);
            return objArr2;
        }

        public static final Object getElem(VectorPointer vectorPointer, int i, int i2) {
            if (i2 < 32) {
                return vectorPointer.display0()[i & 31];
            }
            if (i2 < 1024) {
                return ((Object[]) vectorPointer.display1()[(i >> 5) & 31])[i & 31];
            }
            if (i2 < 32768) {
                return ((Object[]) ((Object[]) vectorPointer.display2()[(i >> 10) & 31])[(i >> 5) & 31])[i & 31];
            }
            if (i2 < 1048576) {
                return ((Object[]) ((Object[]) ((Object[]) vectorPointer.display3()[(i >> 15) & 31])[(i >> 10) & 31])[(i >> 5) & 31])[i & 31];
            }
            if (i2 < 33554432) {
                return ((Object[]) ((Object[]) ((Object[]) ((Object[]) vectorPointer.display4()[(i >> 20) & 31])[(i >> 15) & 31])[(i >> 10) & 31])[(i >> 5) & 31])[i & 31];
            }
            if (i2 < 1073741824) {
                return ((Object[]) ((Object[]) ((Object[]) ((Object[]) ((Object[]) vectorPointer.display5()[(i >> 25) & 31])[(i >> 20) & 31])[(i >> 15) & 31])[(i >> 10) & 31])[(i >> 5) & 31])[i & 31];
            }
            throw new IllegalArgumentException();
        }

        public static final void gotoNextBlockStart(VectorPointer vectorPointer, int i, int i2) {
            if (i2 < 1024) {
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[(i >> 5) & 31]);
            } else if (i2 < 32768) {
                vectorPointer.display1_$eq((Object[]) vectorPointer.display2()[(i >> 10) & 31]);
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[0]);
            } else if (i2 < 1048576) {
                vectorPointer.display2_$eq((Object[]) vectorPointer.display3()[(i >> 15) & 31]);
                vectorPointer.display1_$eq((Object[]) vectorPointer.display2()[0]);
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[0]);
            } else if (i2 < 33554432) {
                vectorPointer.display3_$eq((Object[]) vectorPointer.display4()[(i >> 20) & 31]);
                vectorPointer.display2_$eq((Object[]) vectorPointer.display3()[0]);
                vectorPointer.display1_$eq((Object[]) vectorPointer.display2()[0]);
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[0]);
            } else if (i2 < 1073741824) {
                vectorPointer.display4_$eq((Object[]) vectorPointer.display5()[(i >> 25) & 31]);
                vectorPointer.display3_$eq((Object[]) vectorPointer.display4()[0]);
                vectorPointer.display2_$eq((Object[]) vectorPointer.display3()[0]);
                vectorPointer.display1_$eq((Object[]) vectorPointer.display2()[0]);
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[0]);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public static final void gotoNextBlockStartWritable(VectorPointer vectorPointer, int i, int i2) {
            if (i2 < 1024) {
                if (vectorPointer.depth() == 1) {
                    vectorPointer.display1_$eq(new Object[32]);
                    vectorPointer.display1()[0] = vectorPointer.display0();
                    vectorPointer.depth_$eq(vectorPointer.depth() + 1);
                }
                vectorPointer.display0_$eq(new Object[32]);
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
            } else if (i2 < 32768) {
                if (vectorPointer.depth() == 2) {
                    vectorPointer.display2_$eq(new Object[32]);
                    vectorPointer.display2()[0] = vectorPointer.display1();
                    vectorPointer.depth_$eq(vectorPointer.depth() + 1);
                }
                vectorPointer.display0_$eq(new Object[32]);
                vectorPointer.display1_$eq(new Object[32]);
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
            } else if (i2 < 1048576) {
                if (vectorPointer.depth() == 3) {
                    vectorPointer.display3_$eq(new Object[32]);
                    vectorPointer.display3()[0] = vectorPointer.display2();
                    vectorPointer.depth_$eq(vectorPointer.depth() + 1);
                }
                vectorPointer.display0_$eq(new Object[32]);
                vectorPointer.display1_$eq(new Object[32]);
                vectorPointer.display2_$eq(new Object[32]);
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
            } else if (i2 < 33554432) {
                if (vectorPointer.depth() == 4) {
                    vectorPointer.display4_$eq(new Object[32]);
                    vectorPointer.display4()[0] = vectorPointer.display3();
                    vectorPointer.depth_$eq(vectorPointer.depth() + 1);
                }
                vectorPointer.display0_$eq(new Object[32]);
                vectorPointer.display1_$eq(new Object[32]);
                vectorPointer.display2_$eq(new Object[32]);
                vectorPointer.display3_$eq(new Object[32]);
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
                vectorPointer.display4()[(i >> 20) & 31] = vectorPointer.display3();
            } else if (i2 < 1073741824) {
                if (vectorPointer.depth() == 5) {
                    vectorPointer.display5_$eq(new Object[32]);
                    vectorPointer.display5()[0] = vectorPointer.display4();
                    vectorPointer.depth_$eq(vectorPointer.depth() + 1);
                }
                vectorPointer.display0_$eq(new Object[32]);
                vectorPointer.display1_$eq(new Object[32]);
                vectorPointer.display2_$eq(new Object[32]);
                vectorPointer.display3_$eq(new Object[32]);
                vectorPointer.display4_$eq(new Object[32]);
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
                vectorPointer.display4()[(i >> 20) & 31] = vectorPointer.display3();
                vectorPointer.display5()[(i >> 25) & 31] = vectorPointer.display4();
            } else {
                throw new IllegalArgumentException();
            }
        }

        public static final void gotoPos(VectorPointer vectorPointer, int i, int i2) {
            if (i2 < 32) {
                return;
            }
            if (i2 < 1024) {
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[(i >> 5) & 31]);
            } else if (i2 < 32768) {
                vectorPointer.display1_$eq((Object[]) vectorPointer.display2()[(i >> 10) & 31]);
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[(i >> 5) & 31]);
            } else if (i2 < 1048576) {
                vectorPointer.display2_$eq((Object[]) vectorPointer.display3()[(i >> 15) & 31]);
                vectorPointer.display1_$eq((Object[]) vectorPointer.display2()[(i >> 10) & 31]);
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[(i >> 5) & 31]);
            } else if (i2 < 33554432) {
                vectorPointer.display3_$eq((Object[]) vectorPointer.display4()[(i >> 20) & 31]);
                vectorPointer.display2_$eq((Object[]) vectorPointer.display3()[(i >> 15) & 31]);
                vectorPointer.display1_$eq((Object[]) vectorPointer.display2()[(i >> 10) & 31]);
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[(i >> 5) & 31]);
            } else if (i2 < 1073741824) {
                vectorPointer.display4_$eq((Object[]) vectorPointer.display5()[(i >> 25) & 31]);
                vectorPointer.display3_$eq((Object[]) vectorPointer.display4()[(i >> 20) & 31]);
                vectorPointer.display2_$eq((Object[]) vectorPointer.display3()[(i >> 15) & 31]);
                vectorPointer.display1_$eq((Object[]) vectorPointer.display2()[(i >> 10) & 31]);
                vectorPointer.display0_$eq((Object[]) vectorPointer.display1()[(i >> 5) & 31]);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public static final void gotoPosWritable0(VectorPointer vectorPointer, int i, int i2) {
            int depth = vectorPointer.depth() - 1;
            switch (depth) {
                case 0:
                    vectorPointer.display0_$eq(vectorPointer.copyOf(vectorPointer.display0()));
                    return;
                case 1:
                    vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                    vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i >> 5) & 31));
                    return;
                case 2:
                    vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                    vectorPointer.display1_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display2(), (i >> 10) & 31));
                    vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i >> 5) & 31));
                    return;
                case 3:
                    vectorPointer.display3_$eq(vectorPointer.copyOf(vectorPointer.display3()));
                    vectorPointer.display2_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display3(), (i >> 15) & 31));
                    vectorPointer.display1_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display2(), (i >> 10) & 31));
                    vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i >> 5) & 31));
                    return;
                case 4:
                    vectorPointer.display4_$eq(vectorPointer.copyOf(vectorPointer.display4()));
                    vectorPointer.display3_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display4(), (i >> 20) & 31));
                    vectorPointer.display2_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display3(), (i >> 15) & 31));
                    vectorPointer.display1_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display2(), (i >> 10) & 31));
                    vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i >> 5) & 31));
                    return;
                case 5:
                    vectorPointer.display5_$eq(vectorPointer.copyOf(vectorPointer.display5()));
                    vectorPointer.display4_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display5(), (i >> 25) & 31));
                    vectorPointer.display3_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display4(), (i >> 20) & 31));
                    vectorPointer.display2_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display3(), (i >> 15) & 31));
                    vectorPointer.display1_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display2(), (i >> 10) & 31));
                    vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i >> 5) & 31));
                    return;
                default:
                    throw new MatchError(BoxesRunTime.boxToInteger(depth));
            }
        }

        public static final void gotoPosWritable1(VectorPointer vectorPointer, int i, int i2, int i3) {
            if (i3 < 32) {
                vectorPointer.display0_$eq(vectorPointer.copyOf(vectorPointer.display0()));
            } else if (i3 < 1024) {
                vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i2 >> 5) & 31));
            } else if (i3 < 32768) {
                vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                vectorPointer.display1_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display2(), (i2 >> 10) & 31));
                vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i2 >> 5) & 31));
            } else if (i3 < 1048576) {
                vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                vectorPointer.display3_$eq(vectorPointer.copyOf(vectorPointer.display3()));
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
                vectorPointer.display2_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display3(), (i2 >> 15) & 31));
                vectorPointer.display1_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display2(), (i2 >> 10) & 31));
                vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i2 >> 5) & 31));
            } else if (i3 < 33554432) {
                vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                vectorPointer.display3_$eq(vectorPointer.copyOf(vectorPointer.display3()));
                vectorPointer.display4_$eq(vectorPointer.copyOf(vectorPointer.display4()));
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
                vectorPointer.display4()[(i >> 20) & 31] = vectorPointer.display3();
                vectorPointer.display3_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display4(), (i2 >> 20) & 31));
                vectorPointer.display2_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display3(), (i2 >> 15) & 31));
                vectorPointer.display1_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display2(), (i2 >> 10) & 31));
                vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i2 >> 5) & 31));
            } else if (i3 < 1073741824) {
                vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                vectorPointer.display3_$eq(vectorPointer.copyOf(vectorPointer.display3()));
                vectorPointer.display4_$eq(vectorPointer.copyOf(vectorPointer.display4()));
                vectorPointer.display5_$eq(vectorPointer.copyOf(vectorPointer.display5()));
                vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
                vectorPointer.display4()[(i >> 20) & 31] = vectorPointer.display3();
                vectorPointer.display5()[(i >> 25) & 31] = vectorPointer.display4();
                vectorPointer.display4_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display5(), (i2 >> 25) & 31));
                vectorPointer.display3_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display4(), (i2 >> 20) & 31));
                vectorPointer.display2_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display3(), (i2 >> 15) & 31));
                vectorPointer.display1_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display2(), (i2 >> 10) & 31));
                vectorPointer.display0_$eq(vectorPointer.nullSlotAndCopy(vectorPointer.display1(), (i2 >> 5) & 31));
            } else {
                throw new IllegalArgumentException();
            }
        }

        public static final void initFrom(VectorPointer vectorPointer, VectorPointer vectorPointer2) {
            vectorPointer.initFrom(vectorPointer2, vectorPointer2.depth());
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public static final void initFrom(VectorPointer vectorPointer, VectorPointer vectorPointer2, int i) {
            vectorPointer.depth_$eq(i);
            int i2 = i - 1;
            switch (i2) {
                case -1:
                    return;
                case 0:
                    break;
                case 1:
                    vectorPointer.display1_$eq(vectorPointer2.display1());
                    break;
                case 2:
                    vectorPointer.display2_$eq(vectorPointer2.display2());
                    vectorPointer.display1_$eq(vectorPointer2.display1());
                    break;
                case 3:
                    vectorPointer.display3_$eq(vectorPointer2.display3());
                    vectorPointer.display2_$eq(vectorPointer2.display2());
                    vectorPointer.display1_$eq(vectorPointer2.display1());
                    break;
                case 4:
                    vectorPointer.display4_$eq(vectorPointer2.display4());
                    vectorPointer.display3_$eq(vectorPointer2.display3());
                    vectorPointer.display2_$eq(vectorPointer2.display2());
                    vectorPointer.display1_$eq(vectorPointer2.display1());
                    break;
                case 5:
                    vectorPointer.display5_$eq(vectorPointer2.display5());
                    vectorPointer.display4_$eq(vectorPointer2.display4());
                    vectorPointer.display3_$eq(vectorPointer2.display3());
                    vectorPointer.display2_$eq(vectorPointer2.display2());
                    vectorPointer.display1_$eq(vectorPointer2.display1());
                    break;
                default:
                    throw new MatchError(BoxesRunTime.boxToInteger(i2));
            }
            vectorPointer.display0_$eq(vectorPointer2.display0());
        }

        public static final Object[] nullSlotAndCopy(VectorPointer vectorPointer, Object[] objArr, int i) {
            Object obj = objArr[i];
            objArr[i] = null;
            return vectorPointer.copyOf((Object[]) obj);
        }

        public static final void stabilize(VectorPointer vectorPointer, int i) {
            int depth = vectorPointer.depth() - 1;
            switch (depth) {
                case 0:
                    return;
                case 1:
                    vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                    vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                    return;
                case 2:
                    vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                    vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                    vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                    vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                    return;
                case 3:
                    vectorPointer.display3_$eq(vectorPointer.copyOf(vectorPointer.display3()));
                    vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                    vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                    vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
                    vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                    vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                    return;
                case 4:
                    vectorPointer.display4_$eq(vectorPointer.copyOf(vectorPointer.display4()));
                    vectorPointer.display3_$eq(vectorPointer.copyOf(vectorPointer.display3()));
                    vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                    vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                    vectorPointer.display4()[(i >> 20) & 31] = vectorPointer.display3();
                    vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
                    vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                    vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                    return;
                case 5:
                    vectorPointer.display5_$eq(vectorPointer.copyOf(vectorPointer.display5()));
                    vectorPointer.display4_$eq(vectorPointer.copyOf(vectorPointer.display4()));
                    vectorPointer.display3_$eq(vectorPointer.copyOf(vectorPointer.display3()));
                    vectorPointer.display2_$eq(vectorPointer.copyOf(vectorPointer.display2()));
                    vectorPointer.display1_$eq(vectorPointer.copyOf(vectorPointer.display1()));
                    vectorPointer.display5()[(i >> 25) & 31] = vectorPointer.display4();
                    vectorPointer.display4()[(i >> 20) & 31] = vectorPointer.display3();
                    vectorPointer.display3()[(i >> 15) & 31] = vectorPointer.display2();
                    vectorPointer.display2()[(i >> 10) & 31] = vectorPointer.display1();
                    vectorPointer.display1()[(i >> 5) & 31] = vectorPointer.display0();
                    return;
                default:
                    throw new MatchError(BoxesRunTime.boxToInteger(depth));
            }
        }
    }

    Object[] copyOf(Object[] objArr);

    int depth();

    void depth_$eq(int i);

    Object[] display0();

    void display0_$eq(Object[] objArr);

    Object[] display1();

    void display1_$eq(Object[] objArr);

    Object[] display2();

    void display2_$eq(Object[] objArr);

    Object[] display3();

    void display3_$eq(Object[] objArr);

    Object[] display4();

    void display4_$eq(Object[] objArr);

    Object[] display5();

    void display5_$eq(Object[] objArr);

    <U> void initFrom(VectorPointer<U> vectorPointer, int i);

    Object[] nullSlotAndCopy(Object[] objArr, int i);
}
