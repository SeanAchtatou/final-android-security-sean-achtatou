package scala.collection.immutable;

import scala.MatchError;
import scala.Serializable;
import scala.collection.AbstractSeq;
import scala.collection.CustomParallelizable;
import scala.collection.GenTraversableOnce;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.Iterator;
import scala.collection.TraversableLike;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenTraversableFactory;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Traversable;
import scala.collection.immutable.VectorPointer;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.parallel.immutable.ParVector;
import scala.compat.Platform$;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;

public final class Vector<A> extends AbstractSeq<A> implements Serializable, CustomParallelizable<A, ParVector<A>>, IndexedSeqLike<A, Vector<A>>, GenericTraversableTemplate<A, Vector> {
    private int depth;
    private boolean dirty = false;
    private Object[] display0;
    private Object[] display1;
    private Object[] display2;
    private Object[] display3;
    private Object[] display4;
    private Object[] display5;
    private final int endIndex;
    private final int focus;
    private final int startIndex;

    public class VectorReusableCBF extends GenTraversableFactory<Vector>.GenericCanBuildFrom<Nothing$> {
        public VectorReusableCBF() {
            super(Vector$.MODULE$);
        }

        public Builder<Nothing$, Vector<Nothing$>> apply() {
            return Vector$.MODULE$.newBuilder();
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.Traversable, scala.collection.IndexedSeq, scala.collection.immutable.Iterable, scala.collection.IndexedSeqLike, scala.collection.CustomParallelizable, scala.collection.immutable.Seq, scala.collection.immutable.VectorPointer, scala.collection.immutable.IndexedSeq] */
    public Vector(int i, int i2, int i3) {
        this.startIndex = i;
        this.endIndex = i2;
        this.focus = i3;
        Traversable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        VectorPointer.Cclass.$init$(this);
        CustomParallelizable.Cclass.$init$(this);
    }

    private int checkRangeConvert(int i) {
        int startIndex2 = startIndex() + i;
        if (i >= 0 && startIndex2 < endIndex()) {
            return startIndex2;
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    private void cleanLeftEdge(int i) {
        if (i < 32) {
            zeroLeft(display0(), i);
        } else if (i < 1024) {
            zeroLeft(display0(), i & 31);
            this.display1 = copyRight(display1(), i >>> 5);
        } else if (i < 32768) {
            zeroLeft(display0(), i & 31);
            this.display1 = copyRight(display1(), (i >>> 5) & 31);
            this.display2 = copyRight(display2(), i >>> 10);
        } else if (i < 1048576) {
            zeroLeft(display0(), i & 31);
            this.display1 = copyRight(display1(), (i >>> 5) & 31);
            this.display2 = copyRight(display2(), (i >>> 10) & 31);
            this.display3 = copyRight(display3(), i >>> 15);
        } else if (i < 33554432) {
            zeroLeft(display0(), i & 31);
            this.display1 = copyRight(display1(), (i >>> 5) & 31);
            this.display2 = copyRight(display2(), (i >>> 10) & 31);
            this.display3 = copyRight(display3(), (i >>> 15) & 31);
            this.display4 = copyRight(display4(), i >>> 20);
        } else if (i < 1073741824) {
            zeroLeft(display0(), i & 31);
            this.display1 = copyRight(display1(), (i >>> 5) & 31);
            this.display2 = copyRight(display2(), (i >>> 10) & 31);
            this.display3 = copyRight(display3(), (i >>> 15) & 31);
            this.display4 = copyRight(display4(), (i >>> 20) & 31);
            this.display5 = copyRight(display5(), i >>> 25);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private void cleanRightEdge(int i) {
        if (i <= 32) {
            zeroRight(display0(), i);
        } else if (i <= 1024) {
            zeroRight(display0(), ((i - 1) & 31) + 1);
            this.display1 = copyLeft(display1(), i >>> 5);
        } else if (i <= 32768) {
            zeroRight(display0(), ((i - 1) & 31) + 1);
            this.display1 = copyLeft(display1(), (((i - 1) >>> 5) & 31) + 1);
            this.display2 = copyLeft(display2(), i >>> 10);
        } else if (i <= 1048576) {
            zeroRight(display0(), ((i - 1) & 31) + 1);
            this.display1 = copyLeft(display1(), (((i - 1) >>> 5) & 31) + 1);
            this.display2 = copyLeft(display2(), (((i - 1) >>> 10) & 31) + 1);
            this.display3 = copyLeft(display3(), i >>> 15);
        } else if (i <= 33554432) {
            zeroRight(display0(), ((i - 1) & 31) + 1);
            this.display1 = copyLeft(display1(), (((i - 1) >>> 5) & 31) + 1);
            this.display2 = copyLeft(display2(), (((i - 1) >>> 10) & 31) + 1);
            this.display3 = copyLeft(display3(), (((i - 1) >>> 15) & 31) + 1);
            this.display4 = copyLeft(display4(), i >>> 20);
        } else if (i <= 1073741824) {
            zeroRight(display0(), ((i - 1) & 31) + 1);
            this.display1 = copyLeft(display1(), (((i - 1) >>> 5) & 31) + 1);
            this.display2 = copyLeft(display2(), (((i - 1) >>> 10) & 31) + 1);
            this.display3 = copyLeft(display3(), (((i - 1) >>> 15) & 31) + 1);
            this.display4 = copyLeft(display4(), (((i - 1) >>> 20) & 31) + 1);
            this.display5 = copyLeft(display5(), i >>> 25);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private Object[] copyLeft(Object[] objArr, int i) {
        Object[] objArr2 = new Object[objArr.length];
        Platform$ platform$ = Platform$.MODULE$;
        System.arraycopy(objArr, 0, objArr2, 0, i);
        return objArr2;
    }

    private Object[] copyRight(Object[] objArr, int i) {
        Object[] objArr2 = new Object[objArr.length];
        Platform$ platform$ = Platform$.MODULE$;
        System.arraycopy(objArr, i, objArr2, i, objArr2.length - i);
        return objArr2;
    }

    /* JADX WARN: Type inference failed for: r3v4, types: [scala.collection.immutable.Vector, scala.collection.immutable.Vector<A>, scala.collection.immutable.VectorPointer] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private scala.collection.immutable.Vector<A> dropBack0(int r8) {
        /*
            r7 = this;
            int r0 = r8 + -1
            r0 = r0 & -32
            int r1 = r7.startIndex()
            int r2 = r8 + -1
            r1 = r1 ^ r2
            int r1 = r7.requiredDepth(r1)
            int r2 = r7.startIndex()
            r3 = 1
            int r4 = r1 * 5
            int r3 = r3 << r4
            int r3 = r3 + -1
            r3 = r3 ^ -1
            r2 = r2 & r3
            scala.collection.immutable.Vector r3 = new scala.collection.immutable.Vector
            int r4 = r7.startIndex()
            int r4 = r4 - r2
            int r5 = r8 - r2
            int r6 = r0 - r2
            r3.<init>(r4, r5, r6)
            scala.collection.immutable.VectorPointer.Cclass.initFrom(r3, r7)
            boolean r4 = r7.dirty()
            r3.dirty = r4
            int r4 = r7.focus
            int r5 = r7.focus
            r5 = r5 ^ r0
            r3.gotoPosWritable(r4, r0, r5)
            r3.preClean(r1)
            int r0 = r8 - r2
            r3.cleanRightEdge(r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Vector.dropBack0(int):scala.collection.immutable.Vector");
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [scala.collection.immutable.Vector, scala.collection.immutable.Vector<A>, scala.collection.immutable.VectorPointer] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private scala.collection.immutable.Vector<A> dropFront0(int r8) {
        /*
            r7 = this;
            r0 = r8 & -32
            int r1 = r7.endIndex()
            int r1 = r1 + -1
            r1 = r1 ^ r8
            int r1 = r7.requiredDepth(r1)
            r2 = 1
            int r3 = r1 * 5
            int r2 = r2 << r3
            int r2 = r2 + -1
            r2 = r2 ^ -1
            r2 = r2 & r8
            scala.collection.immutable.Vector r3 = new scala.collection.immutable.Vector
            int r4 = r8 - r2
            int r5 = r7.endIndex()
            int r5 = r5 - r2
            int r6 = r0 - r2
            r3.<init>(r4, r5, r6)
            scala.collection.immutable.VectorPointer.Cclass.initFrom(r3, r7)
            boolean r4 = r7.dirty()
            r3.dirty = r4
            int r4 = r7.focus
            int r5 = r7.focus
            r5 = r5 ^ r0
            r3.gotoPosWritable(r4, r0, r5)
            r3.preClean(r1)
            int r0 = r8 - r2
            r3.cleanLeftEdge(r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Vector.dropFront0(int):scala.collection.immutable.Vector");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    private void gotoPosWritable(int i, int i2, int i3) {
        if (dirty()) {
            VectorPointer.Cclass.gotoPosWritable1(this, i, i2, i3);
            return;
        }
        VectorPointer.Cclass.gotoPosWritable0(this, i2, i3);
        this.dirty = true;
    }

    private void preClean(int i) {
        this.depth = i;
        int i2 = i - 1;
        switch (i2) {
            case 0:
                this.display1 = null;
                this.display2 = null;
                this.display3 = null;
                this.display4 = null;
                this.display5 = null;
                return;
            case 1:
                this.display2 = null;
                this.display3 = null;
                this.display4 = null;
                this.display5 = null;
                return;
            case 2:
                this.display3 = null;
                this.display4 = null;
                this.display5 = null;
                return;
            case 3:
                this.display4 = null;
                this.display5 = null;
                return;
            case 4:
                this.display5 = null;
                return;
            case 5:
                return;
            default:
                throw new MatchError(BoxesRunTime.boxToInteger(i2));
        }
    }

    private int requiredDepth(int i) {
        if (i < 32) {
            return 1;
        }
        if (i < 1024) {
            return 2;
        }
        if (i < 32768) {
            return 3;
        }
        if (i < 1048576) {
            return 4;
        }
        if (i < 33554432) {
            return 5;
        }
        if (i < 1073741824) {
            return 6;
        }
        throw new IllegalArgumentException();
    }

    private void zeroLeft(Object[] objArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
    }

    private void zeroRight(Object[] objArr, int i) {
        while (i < objArr.length) {
            objArr[i] = null;
            i++;
        }
    }

    public final <B, That> That $plus$plus(GenTraversableOnce<B> genTraversableOnce, CanBuildFrom<Vector<A>, B, That> canBuildFrom) {
        return TraversableLike.Cclass.$plus$plus(this, genTraversableOnce.seq(), canBuildFrom);
    }

    public final A apply(int i) {
        int checkRangeConvert = checkRangeConvert(i);
        return getElem(checkRangeConvert, this.focus ^ checkRangeConvert);
    }

    public final /* synthetic */ Object apply(Object obj) {
        return apply(BoxesRunTime.unboxToInt(obj));
    }

    public final GenericCompanion<Vector> companion() {
        return Vector$.MODULE$;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    public final Object[] copyOf(Object[] objArr) {
        return VectorPointer.Cclass.copyOf(this, objArr);
    }

    public final int depth() {
        return this.depth;
    }

    public final void depth_$eq(int i) {
        this.depth = i;
    }

    public final boolean dirty() {
        return this.dirty;
    }

    public final void dirty_$eq(boolean z) {
        this.dirty = z;
    }

    public final Object[] display0() {
        return this.display0;
    }

    public final void display0_$eq(Object[] objArr) {
        this.display0 = objArr;
    }

    public final Object[] display1() {
        return this.display1;
    }

    public final void display1_$eq(Object[] objArr) {
        this.display1 = objArr;
    }

    public final Object[] display2() {
        return this.display2;
    }

    public final void display2_$eq(Object[] objArr) {
        this.display2 = objArr;
    }

    public final Object[] display3() {
        return this.display3;
    }

    public final void display3_$eq(Object[] objArr) {
        this.display3 = objArr;
    }

    public final Object[] display4() {
        return this.display4;
    }

    public final void display4_$eq(Object[] objArr) {
        this.display4 = objArr;
    }

    public final Object[] display5() {
        return this.display5;
    }

    public final void display5_$eq(Object[] objArr) {
        this.display5 = objArr;
    }

    public final Vector<A> drop(int i) {
        return i <= 0 ? this : startIndex() + i < endIndex() ? dropFront0(startIndex() + i) : Vector$.MODULE$.empty();
    }

    public final int endIndex() {
        return this.endIndex;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    public final A getElem(int i, int i2) {
        return VectorPointer.Cclass.getElem(this, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    public final void gotoPos(int i, int i2) {
        VectorPointer.Cclass.gotoPos(this, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    public final void gotoPosWritable0(int i, int i2) {
        VectorPointer.Cclass.gotoPosWritable0(this, i, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    public final void gotoPosWritable1(int i, int i2, int i3) {
        VectorPointer.Cclass.gotoPosWritable1(this, i, i2, i3);
    }

    public final int hashCode() {
        return IndexedSeqLike.Cclass.hashCode(this);
    }

    public final A head() {
        if (!isEmpty()) {
            return apply(0);
        }
        throw new UnsupportedOperationException("empty.head");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    public final <U> void initFrom(VectorPointer<U> vectorPointer) {
        VectorPointer.Cclass.initFrom(this, vectorPointer);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    public final <U> void initFrom(VectorPointer<U> vectorPointer, int i) {
        VectorPointer.Cclass.initFrom(this, vectorPointer, i);
    }

    public final <B> void initIterator(VectorIterator<B> vectorIterator) {
        vectorIterator.initFrom(this);
        if (dirty()) {
            vectorIterator.stabilize(this.focus);
        }
        if (vectorIterator.depth() > 1) {
            vectorIterator.gotoPos(startIndex(), startIndex() ^ this.focus);
        }
    }

    public final /* synthetic */ boolean isDefinedAt(Object obj) {
        return isDefinedAt(BoxesRunTime.unboxToInt(obj));
    }

    public final VectorIterator<A> iterator() {
        VectorIterator<A> vectorIterator = new VectorIterator<>(startIndex(), endIndex());
        initIterator(vectorIterator);
        return vectorIterator;
    }

    public final A last() {
        if (!isEmpty()) {
            return apply(length() - 1);
        }
        throw new UnsupportedOperationException("empty.last");
    }

    public final int length() {
        return endIndex() - startIndex();
    }

    public final int lengthCompare(int i) {
        return length() - i;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.VectorPointer] */
    public final Object[] nullSlotAndCopy(Object[] objArr, int i) {
        return VectorPointer.Cclass.nullSlotAndCopy(this, objArr, i);
    }

    public final Iterator<A> reverseIterator() {
        return new Vector$$anon$1(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Vector, scala.collection.immutable.IndexedSeq] */
    public final IndexedSeq<A> seq() {
        return IndexedSeq.Cclass.seq(this);
    }

    public final Vector<A> slice(int i, int i2) {
        return take(i2).drop(i);
    }

    public final int startIndex() {
        return this.startIndex;
    }

    public final Vector<A> tail() {
        if (!isEmpty()) {
            return drop(1);
        }
        throw new UnsupportedOperationException("empty.tail");
    }

    public final Vector<A> take(int i) {
        return i <= 0 ? Vector$.MODULE$.empty() : startIndex() + i < endIndex() ? dropBack0(startIndex() + i) : this;
    }

    public final scala.collection.IndexedSeq<A> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    public final <A1> Buffer<A1> toBuffer() {
        return IndexedSeqLike.Cclass.toBuffer(this);
    }

    public final scala.collection.IndexedSeq<A> toCollection(Vector<A> vector) {
        return IndexedSeqLike.Cclass.toCollection(this, vector);
    }
}
