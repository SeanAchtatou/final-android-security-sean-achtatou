package scala.collection.immutable;

import scala.Serializable;
import scala.collection.immutable.StringLike;
import scala.runtime.AbstractFunction1;

public final class StringLike$$anonfun$format$1 extends AbstractFunction1<Object, Object> implements Serializable {
    private final /* synthetic */ StringLike $outer;

    public StringLike$$anonfun$format$1(StringLike<Repr> stringLike) {
        if (stringLike == null) {
            throw new NullPointerException();
        }
        this.$outer = stringLike;
    }

    public final Object apply(Object obj) {
        return StringLike.Cclass.scala$collection$immutable$StringLike$$unwrapArg(this.$outer, obj);
    }
}
