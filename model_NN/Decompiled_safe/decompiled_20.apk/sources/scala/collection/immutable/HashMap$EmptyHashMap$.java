package scala.collection.immutable;

import scala.runtime.Nothing$;

public class HashMap$EmptyHashMap$ extends HashMap<Object, Nothing$> {
    public static final HashMap$EmptyHashMap$ MODULE$ = null;

    static {
        new HashMap$EmptyHashMap$();
    }

    public HashMap$EmptyHashMap$() {
        MODULE$ = this;
    }
}
