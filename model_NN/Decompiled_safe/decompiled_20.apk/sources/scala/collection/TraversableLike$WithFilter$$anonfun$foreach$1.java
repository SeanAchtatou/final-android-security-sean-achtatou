package scala.collection;

import scala.Function1;
import scala.Serializable;
import scala.collection.TraversableLike;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class TraversableLike$WithFilter$$anonfun$foreach$1 extends AbstractFunction1<A, Object> implements Serializable {
    private final /* synthetic */ TraversableLike.WithFilter $outer;
    private final Function1 f$3;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.collection.TraversableLike<A, Repr>$WithFilter, scala.Function1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TraversableLike$WithFilter$$anonfun$foreach$1(scala.collection.TraversableLike.WithFilter r2, scala.collection.TraversableLike<A, Repr>.WithFilter r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.f$3 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.TraversableLike$WithFilter$$anonfun$foreach$1.<init>(scala.collection.TraversableLike$WithFilter, scala.Function1):void");
    }

    public final Object apply(A a) {
        return BoxesRunTime.unboxToBoolean(this.$outer.scala$collection$TraversableLike$WithFilter$$p.apply(a)) ? this.f$3.apply(a) : BoxedUnit.UNIT;
    }
}
