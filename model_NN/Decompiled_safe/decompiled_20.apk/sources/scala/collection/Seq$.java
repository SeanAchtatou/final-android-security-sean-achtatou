package scala.collection;

import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;
import scala.collection.mutable.Builder;

public final class Seq$ extends SeqFactory<Seq> {
    public static final Seq$ MODULE$ = null;

    static {
        new Seq$();
    }

    private Seq$() {
        MODULE$ = this;
    }

    public final <A> CanBuildFrom<Seq<?>, A, Seq<A>> canBuildFrom() {
        return ReusableCBF();
    }

    public final <A> Builder<A, Seq<A>> newBuilder() {
        return scala.collection.immutable.Seq$.MODULE$.newBuilder();
    }
}
