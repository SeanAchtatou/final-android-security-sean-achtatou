package scala.collection;

import scala.collection.parallel.ParIterable;

public interface GenTraversableLike<A, Repr> extends GenTraversableOnce<A>, Parallelizable<A, ParIterable<A>> {
    A head();

    int size();

    Repr tail();
}
