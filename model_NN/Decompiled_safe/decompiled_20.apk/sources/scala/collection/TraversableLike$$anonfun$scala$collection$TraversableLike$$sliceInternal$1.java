package scala.collection;

import scala.Serializable;
import scala.collection.mutable.Builder;
import scala.runtime.AbstractFunction0$mcV$sp;
import scala.runtime.IntRef;

public final class TraversableLike$$anonfun$scala$collection$TraversableLike$$sliceInternal$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public final /* synthetic */ TraversableLike $outer;
    public final Builder b$9;
    public final int from$1;
    public final IntRef i$1;
    public final int until$1;

    public TraversableLike$$anonfun$scala$collection$TraversableLike$$sliceInternal$1(TraversableLike traversableLike, int i, int i2, Builder builder, IntRef intRef) {
        if (traversableLike == null) {
            throw new NullPointerException();
        }
        this.$outer = traversableLike;
        this.from$1 = i;
        this.until$1 = i2;
        this.b$9 = builder;
        this.i$1 = intRef;
    }

    public final void apply() {
        this.$outer.seq().foreach(new TraversableLike$$anonfun$scala$collection$TraversableLike$$sliceInternal$1$$anonfun$apply$mcV$sp$6(this));
    }

    public final void apply$mcV$sp() {
        this.$outer.seq().foreach(new TraversableLike$$anonfun$scala$collection$TraversableLike$$sliceInternal$1$$anonfun$apply$mcV$sp$6(this));
    }
}
