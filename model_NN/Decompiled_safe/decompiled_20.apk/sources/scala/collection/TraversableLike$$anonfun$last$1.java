package scala.collection;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.ObjectRef;

public final class TraversableLike$$anonfun$last$1 extends AbstractFunction1<A, BoxedUnit> implements Serializable {
    private final ObjectRef lst$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.TraversableLike<A, Repr>, scala.runtime.ObjectRef] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TraversableLike$$anonfun$last$1(scala.collection.TraversableLike r1, scala.collection.TraversableLike<A, Repr> r2) {
        /*
            r0 = this;
            r0.lst$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.TraversableLike$$anonfun$last$1.<init>(scala.collection.TraversableLike, scala.runtime.ObjectRef):void");
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [A, T] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void apply(A r2) {
        /*
            r1 = this;
            scala.runtime.ObjectRef r0 = r1.lst$1
            r0.elem = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.TraversableLike$$anonfun$last$1.apply(java.lang.Object):void");
    }
}
