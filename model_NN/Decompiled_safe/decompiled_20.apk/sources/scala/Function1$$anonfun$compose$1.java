package scala;

import scala.runtime.AbstractFunction1;

public final class Function1$$anonfun$compose$1 extends AbstractFunction1<A, R> implements Serializable {
    private final /* synthetic */ Function1 $outer;
    private final Function1 g$1;

    public Function1$$anonfun$compose$1(Function1 function1, Function1<T1, R> function12) {
        if (function1 == null) {
            throw new NullPointerException();
        }
        this.$outer = function1;
        this.g$1 = function12;
    }

    public final R apply(A a) {
        return this.$outer.apply(this.g$1.apply(a));
    }
}
