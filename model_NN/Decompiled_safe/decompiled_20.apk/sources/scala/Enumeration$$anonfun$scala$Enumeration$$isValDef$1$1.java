package scala;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class Enumeration$$anonfun$scala$Enumeration$$isValDef$1$1 extends AbstractFunction1<Field, Object> implements Serializable {
    private final Method m$1;

    public Enumeration$$anonfun$scala$Enumeration$$isValDef$1$1(Enumeration enumeration, Method method) {
        this.m$1 = method;
    }

    public final /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply((Field) obj));
    }

    public final boolean apply(Field field) {
        String name = field.getName();
        String name2 = this.m$1.getName();
        if (name != null ? name.equals(name2) : name2 == null) {
            Class<?> type = field.getType();
            Class<?> returnType = this.m$1.getReturnType();
            return type != null ? type.equals(returnType) : returnType == null;
        }
    }
}
