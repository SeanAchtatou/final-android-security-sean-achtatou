package scala;

public interface Proxy {

    /* renamed from: scala.Proxy$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Proxy proxy) {
        }

        public static boolean equals(Proxy proxy, Object obj) {
            return obj != null && (obj == proxy || obj == proxy.self() || obj.equals(proxy.self()));
        }

        public static int hashCode(Proxy proxy) {
            return proxy.self().hashCode();
        }

        public static String toString(Proxy proxy) {
            return String.valueOf(proxy.self());
        }
    }

    Object self();
}
