package scala;

import org.apache.commons.httpclient.HttpStatus;
import scala.StringContext;
import scala.collection.immutable.StringOps$;
import scala.runtime.IntRef;
import scala.runtime.ObjectRef;
import scala.runtime.VolatileByteRef;

public final class StringContext$ implements Serializable {
    public static final StringContext$ MODULE$ = null;

    static {
        new StringContext$();
    }

    private StringContext$() {
        MODULE$ = this;
    }

    private final StringBuilder bldr$1(ObjectRef objectRef, VolatileByteRef volatileByteRef) {
        return ((byte) (volatileByteRef.elem & 1)) == 0 ? bldr$lzycompute$1(objectRef, volatileByteRef) : (StringBuilder) objectRef.elem;
    }

    private final StringBuilder bldr$lzycompute$1(ObjectRef objectRef, VolatileByteRef volatileByteRef) {
        synchronized (this) {
            if (((byte) (volatileByteRef.elem & 1)) == 0) {
                objectRef.elem = new StringBuilder();
                volatileByteRef.elem = (byte) (volatileByteRef.elem | 1);
            }
        }
        return (StringBuilder) objectRef.elem;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    private final void output$1(char c, String str, ObjectRef objectRef, IntRef intRef, IntRef intRef2, IntRef intRef3, VolatileByteRef volatileByteRef) {
        bldr$1(objectRef, volatileByteRef).append((CharSequence) str, intRef.elem, intRef2.elem);
        bldr$1(objectRef, volatileByteRef).append(c);
        intRef.elem = intRef3.elem;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public final String treatEscapes(String str) {
        char c;
        ObjectRef objectRef = new ObjectRef(null);
        VolatileByteRef volatileByteRef = new VolatileByteRef((byte) 0);
        int length = str.length();
        IntRef intRef = new IntRef(0);
        IntRef intRef2 = new IntRef(0);
        IntRef intRef3 = new IntRef(0);
        while (intRef3.elem < length) {
            intRef2.elem = intRef3.elem;
            StringOps$ stringOps$ = StringOps$.MODULE$;
            Predef$ predef$ = Predef$.MODULE$;
            if (stringOps$.apply$extension(str, intRef3.elem) == '\\') {
                intRef3.elem++;
                if (intRef3.elem >= length) {
                    throw new StringContext.InvalidEscapeException(str, intRef2.elem);
                }
                StringOps$ stringOps$2 = StringOps$.MODULE$;
                Predef$ predef$2 = Predef$.MODULE$;
                if ('0' <= stringOps$2.apply$extension(str, intRef3.elem)) {
                    StringOps$ stringOps$3 = StringOps$.MODULE$;
                    Predef$ predef$3 = Predef$.MODULE$;
                    if (stringOps$3.apply$extension(str, intRef3.elem) <= '7') {
                        StringOps$ stringOps$4 = StringOps$.MODULE$;
                        Predef$ predef$4 = Predef$.MODULE$;
                        char apply$extension = stringOps$4.apply$extension(str, intRef3.elem);
                        int i = apply$extension - '0';
                        intRef3.elem++;
                        if (intRef3.elem < length) {
                            StringOps$ stringOps$5 = StringOps$.MODULE$;
                            Predef$ predef$5 = Predef$.MODULE$;
                            if ('0' <= stringOps$5.apply$extension(str, intRef3.elem)) {
                                StringOps$ stringOps$6 = StringOps$.MODULE$;
                                Predef$ predef$6 = Predef$.MODULE$;
                                if (stringOps$6.apply$extension(str, intRef3.elem) <= '7') {
                                    StringOps$ stringOps$7 = StringOps$.MODULE$;
                                    Predef$ predef$7 = Predef$.MODULE$;
                                    i = ((i * 8) + stringOps$7.apply$extension(str, intRef3.elem)) - 48;
                                    intRef3.elem++;
                                    if (intRef3.elem < length && apply$extension <= '3') {
                                        StringOps$ stringOps$8 = StringOps$.MODULE$;
                                        Predef$ predef$8 = Predef$.MODULE$;
                                        if ('0' <= stringOps$8.apply$extension(str, intRef3.elem)) {
                                            StringOps$ stringOps$9 = StringOps$.MODULE$;
                                            Predef$ predef$9 = Predef$.MODULE$;
                                            if (stringOps$9.apply$extension(str, intRef3.elem) <= '7') {
                                                StringOps$ stringOps$10 = StringOps$.MODULE$;
                                                Predef$ predef$10 = Predef$.MODULE$;
                                                i = ((i * 8) + stringOps$10.apply$extension(str, intRef3.elem)) - 48;
                                                intRef3.elem++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        output$1((char) i, str, objectRef, intRef, intRef2, intRef3, volatileByteRef);
                    }
                }
                StringOps$ stringOps$11 = StringOps$.MODULE$;
                Predef$ predef$11 = Predef$.MODULE$;
                char apply$extension2 = stringOps$11.apply$extension(str, intRef3.elem);
                intRef3.elem++;
                switch (apply$extension2) {
                    case '\"':
                        c = '\"';
                        break;
                    case '\'':
                        c = '\'';
                        break;
                    case '\\':
                        c = '\\';
                        break;
                    case 'b':
                        c = 8;
                        break;
                    case HttpStatus.SC_PROCESSING:
                        c = 12;
                        break;
                    case 'n':
                        c = 10;
                        break;
                    case 'r':
                        c = 13;
                        break;
                    case 't':
                        c = 9;
                        break;
                    default:
                        throw new StringContext.InvalidEscapeException(str, intRef2.elem);
                }
                output$1(c, str, objectRef, intRef, intRef2, intRef3, volatileByteRef);
            } else {
                intRef3.elem++;
            }
        }
        return intRef.elem == 0 ? str : bldr$1(objectRef, volatileByteRef).append((CharSequence) str, intRef.elem, intRef3.elem).toString();
    }
}
