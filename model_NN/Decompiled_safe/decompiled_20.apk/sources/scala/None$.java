package scala;

import java.util.NoSuchElementException;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;
import scala.runtime.ScalaRunTime$;

public final class None$ extends Option<Nothing$> {
    public static final None$ MODULE$ = null;

    static {
        new None$();
    }

    private None$() {
        MODULE$ = this;
    }

    public final boolean canEqual(Object obj) {
        return obj instanceof None$;
    }

    public final Nothing$ get() {
        throw new NoSuchElementException("None.get");
    }

    public final int hashCode() {
        return 2433880;
    }

    public final boolean isEmpty() {
        return true;
    }

    public final int productArity() {
        return 0;
    }

    public final Object productElement(int i) {
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public final Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public final String productPrefix() {
        return "None";
    }

    public final String toString() {
        return "None";
    }
}
