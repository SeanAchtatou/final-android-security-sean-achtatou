package scala;

import scala.collection.C$colon$plus$;
import scala.collection.C$plus$colon$;
import scala.collection.IndexedSeq$;
import scala.collection.Iterable$;
import scala.collection.Iterator$;
import scala.collection.Seq$;
import scala.collection.Traversable$;
import scala.collection.immutable.C$colon$colon$;
import scala.collection.immutable.List$;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Range$;
import scala.collection.immutable.Stream$;
import scala.collection.immutable.Stream$$hash$colon$colon$;
import scala.collection.immutable.Vector$;
import scala.collection.mutable.StringBuilder$;
import scala.math.BigDecimal$;
import scala.math.BigInt$;
import scala.math.Equiv$;
import scala.math.Numeric$;
import scala.math.Ordered$;
import scala.math.Ordering$;
import scala.util.Either$;
import scala.util.Left$;
import scala.util.Right$;

public final class package$ {
    public static final package$ MODULE$ = null;
    private final C$colon$colon$ $colon$colon = C$colon$colon$.MODULE$;
    private final C$colon$plus$ $colon$plus = C$colon$plus$.MODULE$;
    private final Stream$$hash$colon$colon$ $hash$colon$colon = Stream$$hash$colon$colon$.MODULE$;
    private final C$plus$colon$ $plus$colon = C$plus$colon$.MODULE$;
    private final Specializable AnyRef = new package$$anon$1();
    private final BigDecimal$ BigDecimal = BigDecimal$.MODULE$;
    private final BigInt$ BigInt = BigInt$.MODULE$;
    private final Either$ Either = Either$.MODULE$;
    private final Equiv$ Equiv = Equiv$.MODULE$;
    private final IndexedSeq$ IndexedSeq = IndexedSeq$.MODULE$;
    private final Iterable$ Iterable = Iterable$.MODULE$;
    private final Iterator$ Iterator = Iterator$.MODULE$;
    private final Left$ Left = Left$.MODULE$;
    private final List$ List = List$.MODULE$;
    private final Nil$ Nil = Nil$.MODULE$;
    private final Numeric$ Numeric = Numeric$.MODULE$;
    private final Ordered$ Ordered = Ordered$.MODULE$;
    private final Ordering$ Ordering = Ordering$.MODULE$;
    private final Range$ Range = Range$.MODULE$;
    private final Right$ Right = Right$.MODULE$;
    private final Seq$ Seq = Seq$.MODULE$;
    private final Stream$ Stream = Stream$.MODULE$;
    private final StringBuilder$ StringBuilder = StringBuilder$.MODULE$;
    private final Traversable$ Traversable = Traversable$.MODULE$;
    private final Vector$ Vector = Vector$.MODULE$;

    static {
        new package$();
    }

    private package$() {
        MODULE$ = this;
    }
}
