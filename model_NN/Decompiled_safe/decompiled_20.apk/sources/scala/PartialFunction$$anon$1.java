package scala;

import scala.Function1;
import scala.PartialFunction;
import scala.runtime.Nothing$;

public final class PartialFunction$$anon$1 implements PartialFunction<Object, Nothing$> {
    private final Function1<Object, None$> lift = new PartialFunction$$anon$1$$anonfun$3(this);

    public PartialFunction$$anon$1() {
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
    }

    public final <C> PartialFunction$$anon$1 andThen(Function1<Nothing$, C> function1) {
        return this;
    }

    public final Nothing$ apply(Object obj) {
        throw new MatchError(obj);
    }

    public final <A1, B1> B1 applyOrElse(A1 a1, Function1<A1, B1> function1) {
        return PartialFunction.Cclass.applyOrElse(this, a1, function1);
    }

    public final <A> Function1<A, Nothing$> compose(Function1<A, Object> function1) {
        return Function1.Cclass.compose(this, function1);
    }

    public final boolean isDefinedAt(Object obj) {
        return false;
    }

    public final String toString() {
        return Function1.Cclass.toString(this);
    }
}
