package scala;

import java.lang.reflect.Method;
import scala.Enumeration;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

public final class Enumeration$$anonfun$scala$Enumeration$$populateNameMap$1 extends AbstractFunction1<Method, Object> implements Serializable {
    private final /* synthetic */ Enumeration $outer;

    public Enumeration$$anonfun$scala$Enumeration$$populateNameMap$1(Enumeration enumeration) {
        if (enumeration == null) {
            throw new NullPointerException();
        }
        this.$outer = enumeration;
    }

    public final Object apply(Method method) {
        String name = method.getName();
        Enumeration.Value value = (Enumeration.Value) method.invoke(this.$outer, new Object[0]);
        if (value.scala$Enumeration$$outerEnum() != this.$outer) {
            return BoxedUnit.UNIT;
        }
        return this.$outer.scala$Enumeration$$nmap().$plus$eq((Tuple2<Object, String>) new Tuple2(BoxesRunTime.boxToInteger(BoxesRunTime.unboxToInt(Enumeration.Val.class.getMethod("id", new Class[0]).invoke(value, new Object[0]))), name));
    }
}
