package scala;

public interface Function0<R> {

    /* renamed from: scala.Function0$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Function0 function0) {
        }

        public static void apply$mcV$sp(Function0 function0) {
            function0.apply();
        }

        public static String toString(Function0 function0) {
            return "<function0>";
        }
    }

    R apply();

    void apply$mcV$sp();
}
