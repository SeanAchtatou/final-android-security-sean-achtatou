package scala;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import scala.util.DynamicVariable;

public final class Console$ {
    public static final Console$ MODULE$ = null;
    private final DynamicVariable<PrintStream> errVar = new DynamicVariable<>(System.err);
    private final DynamicVariable<BufferedReader> inVar = new DynamicVariable<>(new BufferedReader(new InputStreamReader(System.in)));
    private final DynamicVariable<PrintStream> outVar = new DynamicVariable<>(System.out);

    static {
        new Console$();
    }

    private Console$() {
        MODULE$ = this;
    }

    private DynamicVariable<PrintStream> outVar() {
        return this.outVar;
    }

    public final PrintStream out() {
        return outVar().value();
    }

    public final void println(Object obj) {
        out().println(obj);
    }
}
