package scala.runtime;

import scala.collection.immutable.Range;
import scala.collection.immutable.Range$;

public final class RichInt$ {
    public static final RichInt$ MODULE$ = null;

    static {
        new RichInt$();
    }

    private RichInt$() {
        MODULE$ = this;
    }

    public final int max$extension(int i, int i2) {
        return i > i2 ? i : i2;
    }

    public final int min$extension(int i, int i2) {
        return i < i2 ? i : i2;
    }

    public final Range until$extension0(int i, int i2) {
        return Range$.MODULE$.apply(i, i2);
    }
}
