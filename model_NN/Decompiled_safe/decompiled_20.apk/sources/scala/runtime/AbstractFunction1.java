package scala.runtime;

import scala.Function1;

public abstract class AbstractFunction1<T1, R> implements Function1<T1, R> {
    public AbstractFunction1() {
        Function1.Cclass.$init$(this);
    }

    public <A> Function1<T1, A> andThen(Function1<R, A> function1) {
        return Function1.Cclass.andThen(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcDD$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcDD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcDF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcDF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcDI$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcDI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcDJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcDJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcFD$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcFD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcFF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcFF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcFI$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcFI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcFJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcFJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcID$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcID$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcIF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcIF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcII$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcII$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcIJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcIJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcJD$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcJD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcJF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcJF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcJI$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcJI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcJJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcJJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcVD$sp(Function1<BoxedUnit, A> function1) {
        return Function1.Cclass.andThen$mcVD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcVF$sp(Function1<BoxedUnit, A> function1) {
        return Function1.Cclass.andThen$mcVF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcVI$sp(Function1<BoxedUnit, A> function1) {
        return Function1.Cclass.andThen$mcVI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcVJ$sp(Function1<BoxedUnit, A> function1) {
        return Function1.Cclass.andThen$mcVJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcZD$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcZD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcZF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcZF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcZI$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcZI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcZJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcZJ$sp(this, function1);
    }

    public double apply$mcDD$sp(double d) {
        return Function1.Cclass.apply$mcDD$sp(this, d);
    }

    public double apply$mcDF$sp(float f) {
        return Function1.Cclass.apply$mcDF$sp(this, f);
    }

    public double apply$mcDI$sp(int i) {
        return Function1.Cclass.apply$mcDI$sp(this, i);
    }

    public double apply$mcDJ$sp(long j) {
        return Function1.Cclass.apply$mcDJ$sp(this, j);
    }

    public float apply$mcFD$sp(double d) {
        return Function1.Cclass.apply$mcFD$sp(this, d);
    }

    public float apply$mcFF$sp(float f) {
        return Function1.Cclass.apply$mcFF$sp(this, f);
    }

    public float apply$mcFI$sp(int i) {
        return Function1.Cclass.apply$mcFI$sp(this, i);
    }

    public float apply$mcFJ$sp(long j) {
        return Function1.Cclass.apply$mcFJ$sp(this, j);
    }

    public int apply$mcID$sp(double d) {
        return Function1.Cclass.apply$mcID$sp(this, d);
    }

    public int apply$mcIF$sp(float f) {
        return Function1.Cclass.apply$mcIF$sp(this, f);
    }

    public int apply$mcII$sp(int i) {
        return Function1.Cclass.apply$mcII$sp(this, i);
    }

    public int apply$mcIJ$sp(long j) {
        return Function1.Cclass.apply$mcIJ$sp(this, j);
    }

    public long apply$mcJD$sp(double d) {
        return Function1.Cclass.apply$mcJD$sp(this, d);
    }

    public long apply$mcJF$sp(float f) {
        return Function1.Cclass.apply$mcJF$sp(this, f);
    }

    public long apply$mcJI$sp(int i) {
        return Function1.Cclass.apply$mcJI$sp(this, i);
    }

    public long apply$mcJJ$sp(long j) {
        return Function1.Cclass.apply$mcJJ$sp(this, j);
    }

    public void apply$mcVD$sp(double d) {
        Function1.Cclass.apply$mcVD$sp(this, d);
    }

    public void apply$mcVF$sp(float f) {
        Function1.Cclass.apply$mcVF$sp(this, f);
    }

    public void apply$mcVI$sp(int i) {
        Function1.Cclass.apply$mcVI$sp(this, i);
    }

    public void apply$mcVJ$sp(long j) {
        Function1.Cclass.apply$mcVJ$sp(this, j);
    }

    public boolean apply$mcZD$sp(double d) {
        return Function1.Cclass.apply$mcZD$sp(this, d);
    }

    public boolean apply$mcZF$sp(float f) {
        return Function1.Cclass.apply$mcZF$sp(this, f);
    }

    public boolean apply$mcZI$sp(int i) {
        return Function1.Cclass.apply$mcZI$sp(this, i);
    }

    public boolean apply$mcZJ$sp(long j) {
        return Function1.Cclass.apply$mcZJ$sp(this, j);
    }

    public <A> Function1<A, R> compose(Function1<A, T1> function1) {
        return Function1.Cclass.compose(this, function1);
    }

    public <A> Function1<A, Object> compose$mcDD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcDD$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcDF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcDF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcDI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcDI$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcDJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcDJ$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcFD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcFD$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcFF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcFF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcFI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcFI$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcFJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcFJ$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcID$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcID$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcIF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcIF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcII$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcII$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcIJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcIJ$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcJD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcJD$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcJF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcJF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcJI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcJI$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcJJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcJJ$sp(this, function1);
    }

    public <A> Function1<A, BoxedUnit> compose$mcVD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcVD$sp(this, function1);
    }

    public <A> Function1<A, BoxedUnit> compose$mcVF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcVF$sp(this, function1);
    }

    public <A> Function1<A, BoxedUnit> compose$mcVI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcVI$sp(this, function1);
    }

    public <A> Function1<A, BoxedUnit> compose$mcVJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcVJ$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcZD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcZD$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcZF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcZF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcZI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcZI$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcZJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcZJ$sp(this, function1);
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }
}
