package scala.runtime;

import scala.Function0;

public abstract class AbstractFunction0<R> implements Function0<R> {
    public AbstractFunction0() {
        Function0.Cclass.$init$(this);
    }

    public void apply$mcV$sp() {
        Function0.Cclass.apply$mcV$sp(this);
    }

    public String toString() {
        return Function0.Cclass.toString(this);
    }
}
