package scala.runtime;

public final class RichFloat$ {
    public static final RichFloat$ MODULE$ = null;

    static {
        new RichFloat$();
    }

    private RichFloat$() {
        MODULE$ = this;
    }

    public final boolean isInfinity$extension(float f) {
        return Float.isInfinite(f);
    }
}
