package scala.runtime;

import java.io.Serializable;

public class VolatileByteRef implements Serializable {
    public volatile byte elem;

    public VolatileByteRef(byte b) {
        this.elem = b;
    }

    public String toString() {
        return Byte.toString(this.elem);
    }
}
