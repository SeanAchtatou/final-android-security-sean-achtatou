package scala.runtime;

import scala.MatchError;
import scala.Predef$;
import scala.Product;
import scala.StringContext;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Set;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ClassTag;
import scala.util.hashing.MurmurHash3$;

public final class ScalaRunTime$ {
    public static final ScalaRunTime$ MODULE$ = null;
    private final Set<String> tupleNames;

    static {
        new ScalaRunTime$();
    }

    private ScalaRunTime$() {
        MODULE$ = this;
        TraversableOnce traversableOnce = Nil$.MODULE$;
        for (int i = 22; i > 0; i--) {
            traversableOnce = traversableOnce.$colon$colon(new StringBuilder().append((Object) "scala.Tuple").append((Object) String.valueOf(i)).toString());
        }
        this.tupleNames = traversableOnce.toSet();
    }

    public final int _hashCode(Product product) {
        return MurmurHash3$.MODULE$.productHash(product);
    }

    public final String _toString(Product product) {
        return product.productIterator().mkString(new StringBuilder().append((Object) product.productPrefix()).append((Object) "(").toString(), ",", ")");
    }

    public final Class<?> arrayElementClass(Object obj) {
        if (obj instanceof Class) {
            return ((Class) obj).getComponentType();
        }
        if (obj instanceof ClassTag) {
            return ((ClassTag) obj).runtimeClass();
        }
        throw new UnsupportedOperationException(new StringContext(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"unsupported schematic ", " (", ")"})).s(Predef$.MODULE$.genericWrapArray(new Object[]{obj, obj.getClass()})));
    }

    public final Object array_apply(Object obj, int i) {
        if (obj instanceof Object[]) {
            return ((Object[]) obj)[i];
        }
        if (obj instanceof int[]) {
            return BoxesRunTime.boxToInteger(((int[]) obj)[i]);
        }
        if (obj instanceof double[]) {
            return BoxesRunTime.boxToDouble(((double[]) obj)[i]);
        }
        if (obj instanceof long[]) {
            return BoxesRunTime.boxToLong(((long[]) obj)[i]);
        }
        if (obj instanceof float[]) {
            return BoxesRunTime.boxToFloat(((float[]) obj)[i]);
        }
        if (obj instanceof char[]) {
            return BoxesRunTime.boxToCharacter(((char[]) obj)[i]);
        }
        if (obj instanceof byte[]) {
            return BoxesRunTime.boxToByte(((byte[]) obj)[i]);
        }
        if (obj instanceof short[]) {
            return BoxesRunTime.boxToShort(((short[]) obj)[i]);
        }
        if (obj instanceof boolean[]) {
            return BoxesRunTime.boxToBoolean(((boolean[]) obj)[i]);
        }
        if (obj instanceof BoxedUnit[]) {
            return ((BoxedUnit[]) obj)[i];
        }
        if (obj == null) {
            throw new NullPointerException();
        }
        throw new MatchError(obj);
    }

    public final Object array_clone(Object obj) {
        if (obj instanceof Object[]) {
            return ArrayRuntime.cloneArray((Object[]) obj);
        }
        if (obj instanceof int[]) {
            return ArrayRuntime.cloneArray((int[]) obj);
        }
        if (obj instanceof double[]) {
            return ArrayRuntime.cloneArray((double[]) obj);
        }
        if (obj instanceof long[]) {
            return ArrayRuntime.cloneArray((long[]) obj);
        }
        if (obj instanceof float[]) {
            return ArrayRuntime.cloneArray((float[]) obj);
        }
        if (obj instanceof char[]) {
            return ArrayRuntime.cloneArray((char[]) obj);
        }
        if (obj instanceof byte[]) {
            return ArrayRuntime.cloneArray((byte[]) obj);
        }
        if (obj instanceof short[]) {
            return ArrayRuntime.cloneArray((short[]) obj);
        }
        if (obj instanceof boolean[]) {
            return ArrayRuntime.cloneArray((boolean[]) obj);
        }
        if (obj instanceof BoxedUnit[]) {
            return (BoxedUnit[]) obj;
        }
        if (obj == null) {
            throw new NullPointerException();
        }
        throw new MatchError(obj);
    }

    public final int array_length(Object obj) {
        if (obj instanceof Object[]) {
            return ((Object[]) obj).length;
        }
        if (obj instanceof int[]) {
            return ((int[]) obj).length;
        }
        if (obj instanceof double[]) {
            return ((double[]) obj).length;
        }
        if (obj instanceof long[]) {
            return ((long[]) obj).length;
        }
        if (obj instanceof float[]) {
            return ((float[]) obj).length;
        }
        if (obj instanceof char[]) {
            return ((char[]) obj).length;
        }
        if (obj instanceof byte[]) {
            return ((byte[]) obj).length;
        }
        if (obj instanceof short[]) {
            return ((short[]) obj).length;
        }
        if (obj instanceof boolean[]) {
            return ((boolean[]) obj).length;
        }
        if (obj instanceof BoxedUnit[]) {
            return ((BoxedUnit[]) obj).length;
        }
        if (obj == null) {
            throw new NullPointerException();
        }
        throw new MatchError(obj);
    }

    public final void array_update(Object obj, int i, Object obj2) {
        if (obj instanceof Object[]) {
            ((Object[]) obj)[i] = obj2;
        } else if (obj instanceof int[]) {
            ((int[]) obj)[i] = BoxesRunTime.unboxToInt(obj2);
        } else if (obj instanceof double[]) {
            ((double[]) obj)[i] = BoxesRunTime.unboxToDouble(obj2);
        } else if (obj instanceof long[]) {
            ((long[]) obj)[i] = BoxesRunTime.unboxToLong(obj2);
        } else if (obj instanceof float[]) {
            ((float[]) obj)[i] = BoxesRunTime.unboxToFloat(obj2);
        } else if (obj instanceof char[]) {
            ((char[]) obj)[i] = BoxesRunTime.unboxToChar(obj2);
        } else if (obj instanceof byte[]) {
            ((byte[]) obj)[i] = BoxesRunTime.unboxToByte(obj2);
        } else if (obj instanceof short[]) {
            ((short[]) obj)[i] = BoxesRunTime.unboxToShort(obj2);
        } else if (obj instanceof boolean[]) {
            ((boolean[]) obj)[i] = BoxesRunTime.unboxToBoolean(obj2);
        } else if (obj instanceof BoxedUnit[]) {
            ((BoxedUnit[]) obj)[i] = (BoxedUnit) obj2;
        } else if (obj == null) {
            throw new NullPointerException();
        } else {
            throw new MatchError(obj);
        }
    }

    public final int hash(double d) {
        int i = (int) d;
        if (((double) i) == d) {
            return i;
        }
        long j = (long) d;
        if (((double) j) == d) {
            return BoxesRunTime.boxToLong(j).hashCode();
        }
        float f = (float) d;
        return ((double) f) == d ? BoxesRunTime.boxToFloat(f).hashCode() : BoxesRunTime.boxToDouble(d).hashCode();
    }

    public final int hash(long j) {
        int i = (int) j;
        return i ^ ((i >>> 31) + ((int) (j >>> 32)));
    }

    public final int hash(Number number) {
        return BoxesRunTime.hashFromNumber(number);
    }

    public final int hash(Object obj) {
        if (obj == null) {
            return 0;
        }
        return obj instanceof Number ? BoxesRunTime.hashFromNumber((Number) obj) : obj.hashCode();
    }

    public final <T> Iterator<T> typedProductIterator(Product product) {
        return new ScalaRunTime$$anon$1(product);
    }
}
