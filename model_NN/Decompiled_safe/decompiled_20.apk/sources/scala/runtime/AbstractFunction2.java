package scala.runtime;

import scala.Function2;

public abstract class AbstractFunction2<T1, T2, R> implements Function2<T1, T2, R> {
    public AbstractFunction2() {
        Function2.Cclass.$init$(this);
    }

    public String toString() {
        return Function2.Cclass.toString(this);
    }
}
