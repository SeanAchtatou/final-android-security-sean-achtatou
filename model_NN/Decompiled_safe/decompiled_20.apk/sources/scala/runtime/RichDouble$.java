package scala.runtime;

public final class RichDouble$ {
    public static final RichDouble$ MODULE$ = null;

    static {
        new RichDouble$();
    }

    private RichDouble$() {
        MODULE$ = this;
    }

    public final boolean isInfinity$extension(double d) {
        return Double.isInfinite(d);
    }
}
