package scala.runtime;

import scala.collection.mutable.StringBuilder;

public final class StringAdd$ {
    public static final StringAdd$ MODULE$ = null;

    static {
        new StringAdd$();
    }

    private StringAdd$() {
        MODULE$ = this;
    }

    public final String $plus$extension(Object obj, String str) {
        return new StringBuilder().append((Object) String.valueOf(obj)).append((Object) str).toString();
    }
}
