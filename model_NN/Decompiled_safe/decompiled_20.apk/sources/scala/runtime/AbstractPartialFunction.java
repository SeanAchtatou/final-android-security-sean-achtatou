package scala.runtime;

import scala.Function1;
import scala.Option;
import scala.PartialFunction;
import scala.PartialFunction$;

public abstract class AbstractPartialFunction<T1, R> implements Function1<T1, R> {
    /* JADX WARN: Type inference failed for: r0v0, types: [scala.PartialFunction, scala.Function1, scala.runtime.AbstractPartialFunction] */
    public AbstractPartialFunction() {
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.PartialFunction, scala.runtime.AbstractPartialFunction] */
    public <C> PartialFunction<T1, C> andThen(Function1<R, C> function1) {
        return PartialFunction.Cclass.andThen(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcDD$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcDD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcDF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcDF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcDI$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcDI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcDJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcDJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcFD$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcFD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcFF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcFF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcFI$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcFI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcFJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcFJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcID$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcID$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcIF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcIF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcII$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcII$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcIJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcIJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcJD$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcJD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcJF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcJF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcJI$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcJI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcJJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcJJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcVD$sp(Function1<BoxedUnit, A> function1) {
        return Function1.Cclass.andThen$mcVD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcVF$sp(Function1<BoxedUnit, A> function1) {
        return Function1.Cclass.andThen$mcVF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcVI$sp(Function1<BoxedUnit, A> function1) {
        return Function1.Cclass.andThen$mcVI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcVJ$sp(Function1<BoxedUnit, A> function1) {
        return Function1.Cclass.andThen$mcVJ$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcZD$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcZD$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcZF$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcZF$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcZI$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcZI$sp(this, function1);
    }

    public <A> Function1<Object, A> andThen$mcZJ$sp(Function1<Object, A> function1) {
        return Function1.Cclass.andThen$mcZJ$sp(this, function1);
    }

    public R apply(T1 t1) {
        return applyOrElse(t1, PartialFunction$.MODULE$.empty());
    }

    public double apply$mcDD$sp(double d) {
        return BoxesRunTime.unboxToDouble(apply(BoxesRunTime.boxToDouble(d)));
    }

    public double apply$mcDF$sp(float f) {
        return BoxesRunTime.unboxToDouble(apply(BoxesRunTime.boxToFloat(f)));
    }

    public double apply$mcDI$sp(int i) {
        return BoxesRunTime.unboxToDouble(apply(BoxesRunTime.boxToInteger(i)));
    }

    public double apply$mcDJ$sp(long j) {
        return BoxesRunTime.unboxToDouble(apply(BoxesRunTime.boxToLong(j)));
    }

    public double apply$mcDL$sp(T1 t1) {
        return BoxesRunTime.unboxToDouble(apply(t1));
    }

    public float apply$mcFD$sp(double d) {
        return BoxesRunTime.unboxToFloat(apply(BoxesRunTime.boxToDouble(d)));
    }

    public float apply$mcFF$sp(float f) {
        return BoxesRunTime.unboxToFloat(apply(BoxesRunTime.boxToFloat(f)));
    }

    public float apply$mcFI$sp(int i) {
        return BoxesRunTime.unboxToFloat(apply(BoxesRunTime.boxToInteger(i)));
    }

    public float apply$mcFJ$sp(long j) {
        return BoxesRunTime.unboxToFloat(apply(BoxesRunTime.boxToLong(j)));
    }

    public float apply$mcFL$sp(T1 t1) {
        return BoxesRunTime.unboxToFloat(apply(t1));
    }

    public int apply$mcID$sp(double d) {
        return BoxesRunTime.unboxToInt(apply(BoxesRunTime.boxToDouble(d)));
    }

    public int apply$mcIF$sp(float f) {
        return BoxesRunTime.unboxToInt(apply(BoxesRunTime.boxToFloat(f)));
    }

    public int apply$mcII$sp(int i) {
        return BoxesRunTime.unboxToInt(apply(BoxesRunTime.boxToInteger(i)));
    }

    public int apply$mcIJ$sp(long j) {
        return BoxesRunTime.unboxToInt(apply(BoxesRunTime.boxToLong(j)));
    }

    public int apply$mcIL$sp(T1 t1) {
        return BoxesRunTime.unboxToInt(apply(t1));
    }

    public long apply$mcJD$sp(double d) {
        return BoxesRunTime.unboxToLong(apply(BoxesRunTime.boxToDouble(d)));
    }

    public long apply$mcJF$sp(float f) {
        return BoxesRunTime.unboxToLong(apply(BoxesRunTime.boxToFloat(f)));
    }

    public long apply$mcJI$sp(int i) {
        return BoxesRunTime.unboxToLong(apply(BoxesRunTime.boxToInteger(i)));
    }

    public long apply$mcJJ$sp(long j) {
        return BoxesRunTime.unboxToLong(apply(BoxesRunTime.boxToLong(j)));
    }

    public long apply$mcJL$sp(T1 t1) {
        return BoxesRunTime.unboxToLong(apply(t1));
    }

    public R apply$mcLD$sp(double d) {
        return apply(BoxesRunTime.boxToDouble(d));
    }

    public R apply$mcLF$sp(float f) {
        return apply(BoxesRunTime.boxToFloat(f));
    }

    public R apply$mcLI$sp(int i) {
        return apply(BoxesRunTime.boxToInteger(i));
    }

    public R apply$mcLJ$sp(long j) {
        return apply(BoxesRunTime.boxToLong(j));
    }

    public void apply$mcVD$sp(double d) {
        apply(BoxesRunTime.boxToDouble(d));
    }

    public void apply$mcVF$sp(float f) {
        apply(BoxesRunTime.boxToFloat(f));
    }

    public void apply$mcVI$sp(int i) {
        apply(BoxesRunTime.boxToInteger(i));
    }

    public void apply$mcVJ$sp(long j) {
        apply(BoxesRunTime.boxToLong(j));
    }

    public void apply$mcVL$sp(T1 t1) {
        apply(t1);
    }

    public boolean apply$mcZD$sp(double d) {
        return BoxesRunTime.unboxToBoolean(apply(BoxesRunTime.boxToDouble(d)));
    }

    public boolean apply$mcZF$sp(float f) {
        return BoxesRunTime.unboxToBoolean(apply(BoxesRunTime.boxToFloat(f)));
    }

    public boolean apply$mcZI$sp(int i) {
        return BoxesRunTime.unboxToBoolean(apply(BoxesRunTime.boxToInteger(i)));
    }

    public boolean apply$mcZJ$sp(long j) {
        return BoxesRunTime.unboxToBoolean(apply(BoxesRunTime.boxToLong(j)));
    }

    public boolean apply$mcZL$sp(T1 t1) {
        return BoxesRunTime.unboxToBoolean(apply(t1));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.PartialFunction, scala.runtime.AbstractPartialFunction] */
    public <A1 extends T1, B1> B1 applyOrElse(A1 a1, Function1<A1, B1> function1) {
        return PartialFunction.Cclass.applyOrElse(this, a1, function1);
    }

    public <A> Function1<A, R> compose(Function1<A, T1> function1) {
        return Function1.Cclass.compose(this, function1);
    }

    public <A> Function1<A, Object> compose$mcDD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcDD$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcDF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcDF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcDI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcDI$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcDJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcDJ$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcFD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcFD$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcFF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcFF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcFI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcFI$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcFJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcFJ$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcID$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcID$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcIF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcIF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcII$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcII$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcIJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcIJ$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcJD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcJD$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcJF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcJF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcJI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcJI$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcJJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcJJ$sp(this, function1);
    }

    public <A> Function1<A, BoxedUnit> compose$mcVD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcVD$sp(this, function1);
    }

    public <A> Function1<A, BoxedUnit> compose$mcVF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcVF$sp(this, function1);
    }

    public <A> Function1<A, BoxedUnit> compose$mcVI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcVI$sp(this, function1);
    }

    public <A> Function1<A, BoxedUnit> compose$mcVJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcVJ$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcZD$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcZD$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcZF$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcZF$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcZI$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcZI$sp(this, function1);
    }

    public <A> Function1<A, Object> compose$mcZJ$sp(Function1<A, Object> function1) {
        return Function1.Cclass.compose$mcZJ$sp(this, function1);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.PartialFunction, scala.runtime.AbstractPartialFunction] */
    public Function1<T1, Option<R>> lift() {
        return PartialFunction.Cclass.lift(this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.PartialFunction, scala.runtime.AbstractPartialFunction] */
    public <A1 extends T1, B1> PartialFunction<A1, B1> orElse(PartialFunction<A1, B1> partialFunction) {
        return PartialFunction.Cclass.orElse(this, partialFunction);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.PartialFunction, scala.runtime.AbstractPartialFunction] */
    public <U> Function1<T1, Object> runWith(Function1<R, U> function1) {
        return PartialFunction.Cclass.runWith(this, function1);
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }
}
