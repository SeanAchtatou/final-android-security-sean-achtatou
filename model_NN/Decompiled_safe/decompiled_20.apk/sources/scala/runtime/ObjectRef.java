package scala.runtime;

import java.io.Serializable;

public class ObjectRef<T> implements Serializable {
    public T elem;

    public ObjectRef(T t) {
        this.elem = t;
    }

    public String toString() {
        return String.valueOf(this.elem);
    }
}
