package scala.runtime;

import com.amap.api.maps.model.BitmapDescriptorFactory;
import scala.math.ScalaNumber;

public final class BoxesRunTime {
    public static Boolean boxToBoolean(boolean z) {
        return Boolean.valueOf(z);
    }

    public static Byte boxToByte(byte b) {
        return Byte.valueOf(b);
    }

    public static Character boxToCharacter(char c) {
        return Character.valueOf(c);
    }

    public static Double boxToDouble(double d) {
        return Double.valueOf(d);
    }

    public static Float boxToFloat(float f) {
        return Float.valueOf(f);
    }

    public static Integer boxToInteger(int i) {
        return Integer.valueOf(i);
    }

    public static Long boxToLong(long j) {
        return Long.valueOf(j);
    }

    public static Short boxToShort(short s) {
        return Short.valueOf(s);
    }

    public static boolean equalsCharObject(Character ch, Object obj) {
        return obj instanceof Character ? ch.charValue() == ((Character) obj).charValue() : obj instanceof Number ? equalsNumChar((Number) obj, ch) : ch == null ? obj == null : ch.equals(obj);
    }

    private static boolean equalsNumChar(Number number, Character ch) {
        if (ch == null) {
            return number == null;
        }
        char charValue = ch.charValue();
        switch (typeCode(number)) {
            case 3:
                return number.intValue() == charValue;
            case 4:
                return number.longValue() == ((long) charValue);
            case 5:
                return number.floatValue() == ((float) charValue);
            case 6:
                return number.doubleValue() == ((double) charValue);
            default:
                return number.equals(ch);
        }
    }

    public static boolean equalsNumNum(Number number, Number number2) {
        int typeCode = typeCode(number);
        int typeCode2 = typeCode(number2);
        if (typeCode2 <= typeCode) {
            typeCode2 = typeCode;
        }
        switch (typeCode2) {
            case 3:
                return number.intValue() == number2.intValue();
            case 4:
                return number.longValue() == number2.longValue();
            case 5:
                return number.floatValue() == number2.floatValue();
            case 6:
                return number.doubleValue() == number2.doubleValue();
            default:
                return (!(number2 instanceof ScalaNumber) || (number instanceof ScalaNumber)) ? number == null ? number2 == null : number.equals(number2) : number2.equals(number);
        }
    }

    public static boolean equalsNumObject(Number number, Object obj) {
        return obj instanceof Number ? equalsNumNum(number, (Number) obj) : obj instanceof Character ? equalsNumChar(number, (Character) obj) : number == null ? obj == null : number.equals(obj);
    }

    public static int hashFromDouble(Double d) {
        int intValue = d.intValue();
        double doubleValue = d.doubleValue();
        if (((double) intValue) == doubleValue) {
            return intValue;
        }
        long longValue = d.longValue();
        if (((double) longValue) == doubleValue) {
            return Long.valueOf(longValue).hashCode();
        }
        float floatValue = d.floatValue();
        return ((double) floatValue) == doubleValue ? Float.valueOf(floatValue).hashCode() : d.hashCode();
    }

    public static int hashFromFloat(Float f) {
        int intValue = f.intValue();
        float floatValue = f.floatValue();
        if (((float) intValue) == floatValue) {
            return intValue;
        }
        long longValue = f.longValue();
        return ((float) longValue) == floatValue ? Long.valueOf(longValue).hashCode() : f.hashCode();
    }

    public static int hashFromLong(Long l) {
        int intValue = l.intValue();
        return ((long) intValue) == l.longValue() ? intValue : l.hashCode();
    }

    public static int hashFromNumber(Number number) {
        return number instanceof Long ? hashFromLong((Long) number) : number instanceof Double ? hashFromDouble((Double) number) : number instanceof Float ? hashFromFloat((Float) number) : number.hashCode();
    }

    private static int typeCode(Object obj) {
        if (obj instanceof Integer) {
            return 3;
        }
        if (obj instanceof Double) {
            return 6;
        }
        if (obj instanceof Long) {
            return 4;
        }
        if (obj instanceof Character) {
            return 0;
        }
        if (obj instanceof Float) {
            return 5;
        }
        return ((obj instanceof Byte) || (obj instanceof Short)) ? 3 : 7;
    }

    public static boolean unboxToBoolean(Object obj) {
        if (obj == null) {
            return false;
        }
        return ((Boolean) obj).booleanValue();
    }

    public static byte unboxToByte(Object obj) {
        if (obj == null) {
            return 0;
        }
        return ((Byte) obj).byteValue();
    }

    public static char unboxToChar(Object obj) {
        if (obj == null) {
            return 0;
        }
        return ((Character) obj).charValue();
    }

    public static double unboxToDouble(Object obj) {
        if (obj == null) {
            return 0.0d;
        }
        return ((Double) obj).doubleValue();
    }

    public static float unboxToFloat(Object obj) {
        return obj == null ? BitmapDescriptorFactory.HUE_RED : ((Float) obj).floatValue();
    }

    public static int unboxToInt(Object obj) {
        if (obj == null) {
            return 0;
        }
        return ((Integer) obj).intValue();
    }

    public static long unboxToLong(Object obj) {
        if (obj == null) {
            return 0;
        }
        return ((Long) obj).longValue();
    }

    public static short unboxToShort(Object obj) {
        if (obj == null) {
            return 0;
        }
        return ((Short) obj).shortValue();
    }
}
