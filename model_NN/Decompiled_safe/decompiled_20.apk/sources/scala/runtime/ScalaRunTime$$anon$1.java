package scala.runtime;

import scala.Product;
import scala.collection.AbstractIterator;

public final class ScalaRunTime$$anon$1 extends AbstractIterator<T> {
    private int c = 0;
    private final int cmax;
    private final Product x$2;

    public ScalaRunTime$$anon$1(Product product) {
        this.x$2 = product;
        this.cmax = product.productArity();
    }

    private int c() {
        return this.c;
    }

    private void c_$eq(int i) {
        this.c = i;
    }

    private int cmax() {
        return this.cmax;
    }

    public final boolean hasNext() {
        return c() < cmax();
    }

    public final T next() {
        T productElement = this.x$2.productElement(c());
        this.c = c() + 1;
        return productElement;
    }
}
