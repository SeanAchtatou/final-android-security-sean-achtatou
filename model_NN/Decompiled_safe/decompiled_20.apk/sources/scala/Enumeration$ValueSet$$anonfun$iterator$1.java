package scala;

import scala.Enumeration;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class Enumeration$ValueSet$$anonfun$iterator$1 extends AbstractFunction1<Object, Enumeration.Value> implements Serializable {
    private final /* synthetic */ Enumeration.ValueSet $outer;

    public Enumeration$ValueSet$$anonfun$iterator$1(Enumeration.ValueSet valueSet) {
        if (valueSet == null) {
            throw new NullPointerException();
        }
        this.$outer = valueSet;
    }

    public final /* synthetic */ Object apply(Object obj) {
        return apply(BoxesRunTime.unboxToInt(obj));
    }

    public final Enumeration.Value apply(int i) {
        return this.$outer.scala$Enumeration$ValueSet$$$outer().apply(this.$outer.scala$Enumeration$ValueSet$$$outer().scala$Enumeration$$bottomId() + i);
    }
}
