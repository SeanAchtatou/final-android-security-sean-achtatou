package scala;

import scala.Enumeration;
import scala.math.Ordering;
import scala.math.PartialOrdering;

public class Enumeration$ValueOrdering$ implements Ordering<Enumeration.Value> {
    private final /* synthetic */ Enumeration $outer;

    /* JADX WARN: Type inference failed for: r1v0, types: [scala.math.Ordering, scala.math.PartialOrdering, scala.Enumeration$ValueOrdering$] */
    public Enumeration$ValueOrdering$(Enumeration enumeration) {
        if (enumeration == null) {
            throw new NullPointerException();
        }
        this.$outer = enumeration;
        PartialOrdering.Cclass.$init$(this);
        Ordering.Cclass.$init$(this);
    }

    public int compare(Enumeration.Value value, Enumeration.Value value2) {
        return value.compare(value2);
    }
}
