package scala;

import scala.runtime.BoxesRunTime;

public interface Function1<T1, R> {

    /* renamed from: scala.Function1$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Function1 function1) {
        }

        public static Function1 andThen(Function1 function1, Function1 function12) {
            return new Function1$$anonfun$andThen$1(function1, function12);
        }

        public static Function1 andThen$mcDD$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcDF$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcDI$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcDJ$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcFD$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcFF$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcFI$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcFJ$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcID$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcIF$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcII$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcIJ$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcJD$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcJF$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcJI$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcJJ$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcVD$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcVF$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcVI$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcVJ$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcZD$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcZF$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcZI$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static Function1 andThen$mcZJ$sp(Function1 function1, Function1 function12) {
            return function1.andThen(function12);
        }

        public static double apply$mcDD$sp(Function1 function1, double d) {
            return BoxesRunTime.unboxToDouble(function1.apply(BoxesRunTime.boxToDouble(d)));
        }

        public static double apply$mcDF$sp(Function1 function1, float f) {
            return BoxesRunTime.unboxToDouble(function1.apply(BoxesRunTime.boxToFloat(f)));
        }

        public static double apply$mcDI$sp(Function1 function1, int i) {
            return BoxesRunTime.unboxToDouble(function1.apply(BoxesRunTime.boxToInteger(i)));
        }

        public static double apply$mcDJ$sp(Function1 function1, long j) {
            return BoxesRunTime.unboxToDouble(function1.apply(BoxesRunTime.boxToLong(j)));
        }

        public static float apply$mcFD$sp(Function1 function1, double d) {
            return BoxesRunTime.unboxToFloat(function1.apply(BoxesRunTime.boxToDouble(d)));
        }

        public static float apply$mcFF$sp(Function1 function1, float f) {
            return BoxesRunTime.unboxToFloat(function1.apply(BoxesRunTime.boxToFloat(f)));
        }

        public static float apply$mcFI$sp(Function1 function1, int i) {
            return BoxesRunTime.unboxToFloat(function1.apply(BoxesRunTime.boxToInteger(i)));
        }

        public static float apply$mcFJ$sp(Function1 function1, long j) {
            return BoxesRunTime.unboxToFloat(function1.apply(BoxesRunTime.boxToLong(j)));
        }

        public static int apply$mcID$sp(Function1 function1, double d) {
            return BoxesRunTime.unboxToInt(function1.apply(BoxesRunTime.boxToDouble(d)));
        }

        public static int apply$mcIF$sp(Function1 function1, float f) {
            return BoxesRunTime.unboxToInt(function1.apply(BoxesRunTime.boxToFloat(f)));
        }

        public static int apply$mcII$sp(Function1 function1, int i) {
            return BoxesRunTime.unboxToInt(function1.apply(BoxesRunTime.boxToInteger(i)));
        }

        public static int apply$mcIJ$sp(Function1 function1, long j) {
            return BoxesRunTime.unboxToInt(function1.apply(BoxesRunTime.boxToLong(j)));
        }

        public static long apply$mcJD$sp(Function1 function1, double d) {
            return BoxesRunTime.unboxToLong(function1.apply(BoxesRunTime.boxToDouble(d)));
        }

        public static long apply$mcJF$sp(Function1 function1, float f) {
            return BoxesRunTime.unboxToLong(function1.apply(BoxesRunTime.boxToFloat(f)));
        }

        public static long apply$mcJI$sp(Function1 function1, int i) {
            return BoxesRunTime.unboxToLong(function1.apply(BoxesRunTime.boxToInteger(i)));
        }

        public static long apply$mcJJ$sp(Function1 function1, long j) {
            return BoxesRunTime.unboxToLong(function1.apply(BoxesRunTime.boxToLong(j)));
        }

        public static void apply$mcVD$sp(Function1 function1, double d) {
            function1.apply(BoxesRunTime.boxToDouble(d));
        }

        public static void apply$mcVF$sp(Function1 function1, float f) {
            function1.apply(BoxesRunTime.boxToFloat(f));
        }

        public static void apply$mcVI$sp(Function1 function1, int i) {
            function1.apply(BoxesRunTime.boxToInteger(i));
        }

        public static void apply$mcVJ$sp(Function1 function1, long j) {
            function1.apply(BoxesRunTime.boxToLong(j));
        }

        public static boolean apply$mcZD$sp(Function1 function1, double d) {
            return BoxesRunTime.unboxToBoolean(function1.apply(BoxesRunTime.boxToDouble(d)));
        }

        public static boolean apply$mcZF$sp(Function1 function1, float f) {
            return BoxesRunTime.unboxToBoolean(function1.apply(BoxesRunTime.boxToFloat(f)));
        }

        public static boolean apply$mcZI$sp(Function1 function1, int i) {
            return BoxesRunTime.unboxToBoolean(function1.apply(BoxesRunTime.boxToInteger(i)));
        }

        public static boolean apply$mcZJ$sp(Function1 function1, long j) {
            return BoxesRunTime.unboxToBoolean(function1.apply(BoxesRunTime.boxToLong(j)));
        }

        public static Function1 compose(Function1 function1, Function1 function12) {
            return new Function1$$anonfun$compose$1(function1, function12);
        }

        public static Function1 compose$mcDD$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcDF$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcDI$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcDJ$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcFD$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcFF$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcFI$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcFJ$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcID$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcIF$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcII$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcIJ$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcJD$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcJF$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcJI$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcJJ$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcVD$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcVF$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcVI$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcVJ$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcZD$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcZF$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcZI$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static Function1 compose$mcZJ$sp(Function1 function1, Function1 function12) {
            return function1.compose(function12);
        }

        public static String toString(Function1 function1) {
            return "<function1>";
        }
    }

    <A> Function1<T1, A> andThen(Function1<R, A> function1);

    R apply(Object obj);

    <A> Function1<A, R> compose(Function1<A, T1> function1);
}
