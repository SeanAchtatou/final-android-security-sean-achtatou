package scala.compat;

import scala.util.Properties$;

public final class Platform$ {
    public static final Platform$ MODULE$ = null;
    private final String EOL = Properties$.MODULE$.lineSeparator();

    static {
        new Platform$();
    }

    private Platform$() {
        MODULE$ = this;
    }
}
