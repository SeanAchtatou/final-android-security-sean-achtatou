package scala.math;

public interface Ordered<A> extends Comparable<A> {

    /* renamed from: scala.math.Ordered$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Ordered ordered) {
        }

        public static int compareTo(Ordered ordered, Object obj) {
            return ordered.compare(obj);
        }
    }

    int compare(Object obj);
}
