package scala.math;

public final class package$ {
    public static final package$ MODULE$ = null;
    private final double E = 2.718281828459045d;
    private final double Pi = 3.141592653589793d;

    static {
        new package$();
    }

    private package$() {
        MODULE$ = this;
    }

    public final int max(int i, int i2) {
        return Math.max(i, i2);
    }

    public final int min(int i, int i2) {
        return Math.min(i, i2);
    }
}
