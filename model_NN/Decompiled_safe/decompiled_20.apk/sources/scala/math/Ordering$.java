package scala.math;

import scala.Serializable;
import scala.math.LowPriorityOrderingImplicits;

public final class Ordering$ implements Serializable, LowPriorityOrderingImplicits {
    public static final Ordering$ MODULE$ = null;

    static {
        new Ordering$();
    }

    private Ordering$() {
        MODULE$ = this;
        LowPriorityOrderingImplicits.Cclass.$init$(this);
    }
}
