package scala.math;

import scala.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;

public final class BigDecimal$$anonfun$isValidByte$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public final /* synthetic */ BigDecimal $outer;

    public BigDecimal$$anonfun$isValidByte$1(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            throw new NullPointerException();
        }
        this.$outer = bigDecimal;
    }

    public final void apply() {
        this.$outer.toByteExact();
    }

    public final void apply$mcV$sp() {
        this.$outer.toByteExact();
    }
}
