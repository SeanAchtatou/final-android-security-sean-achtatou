package scala.math;

import scala.Serializable;

public final class Equiv$ implements Serializable {
    public static final Equiv$ MODULE$ = null;

    static {
        new Equiv$();
    }

    private Equiv$() {
        MODULE$ = this;
        LowPriorityEquiv$class.$init$(this);
    }
}
