package scala.math;

import java.math.BigDecimal;
import java.math.MathContext;
import scala.Serializable;

public final class BigDecimal$ implements Serializable {
    public static final BigDecimal$ MODULE$ = null;
    private final BigDecimal MaxLong = new BigDecimal(BigDecimal.valueOf(Long.MAX_VALUE), defaultMathContext());
    private final BigDecimal MinLong = new BigDecimal(BigDecimal.valueOf(Long.MIN_VALUE), defaultMathContext());
    private volatile boolean bitmap$0;
    private BigDecimal[] cache;
    private final MathContext defaultMathContext = MathContext.DECIMAL128;
    private final int maxCached = 512;
    private final int minCached = -512;

    static {
        new BigDecimal$();
    }

    private BigDecimal$() {
        MODULE$ = this;
    }

    private BigDecimal[] cache() {
        return this.bitmap$0 ? this.cache : cache$lzycompute();
    }

    private BigDecimal[] cache$lzycompute() {
        synchronized (this) {
            if (!this.bitmap$0) {
                this.cache = new BigDecimal[((maxCached() - minCached()) + 1)];
                this.bitmap$0 = true;
            }
        }
        return this.cache;
    }

    private int maxCached() {
        return this.maxCached;
    }

    private int minCached() {
        return this.minCached;
    }

    public final BigDecimal apply(int i) {
        return apply(i, defaultMathContext());
    }

    public final BigDecimal apply(int i, MathContext mathContext) {
        MathContext defaultMathContext2 = defaultMathContext();
        if (mathContext != null ? mathContext.equals(defaultMathContext2) : defaultMathContext2 == null) {
            if (minCached() <= i && i <= maxCached()) {
                int minCached2 = i - minCached();
                BigDecimal bigDecimal = cache()[minCached2];
                if (bigDecimal != null) {
                    return bigDecimal;
                }
                BigDecimal bigDecimal2 = new BigDecimal(BigDecimal.valueOf((long) i), mathContext);
                cache()[minCached2] = bigDecimal2;
                return bigDecimal2;
            }
        }
        return new BigDecimal(BigDecimal.valueOf((long) i), mathContext);
    }

    public final MathContext defaultMathContext() {
        return this.defaultMathContext;
    }

    public final BigDecimal int2bigDecimal(int i) {
        return apply(i);
    }
}
