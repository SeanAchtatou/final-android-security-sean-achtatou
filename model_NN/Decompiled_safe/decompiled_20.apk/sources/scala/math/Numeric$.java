package scala.math;

import scala.Serializable;

public final class Numeric$ implements Serializable {
    public static final Numeric$ MODULE$ = null;

    static {
        new Numeric$();
    }

    private Numeric$() {
        MODULE$ = this;
    }
}
