package scala.math;

import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public interface ScalaNumericAnyConversions {

    /* renamed from: scala.math.ScalaNumericAnyConversions$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(ScalaNumericAnyConversions scalaNumericAnyConversions) {
        }

        public static byte toByte(ScalaNumericAnyConversions scalaNumericAnyConversions) {
            return scalaNumericAnyConversions.byteValue();
        }

        public static double toDouble(ScalaNumericAnyConversions scalaNumericAnyConversions) {
            return scalaNumericAnyConversions.doubleValue();
        }

        public static float toFloat(ScalaNumericAnyConversions scalaNumericAnyConversions) {
            return scalaNumericAnyConversions.floatValue();
        }

        public static int toInt(ScalaNumericAnyConversions scalaNumericAnyConversions) {
            return scalaNumericAnyConversions.intValue();
        }

        public static long toLong(ScalaNumericAnyConversions scalaNumericAnyConversions) {
            return scalaNumericAnyConversions.longValue();
        }

        public static short toShort(ScalaNumericAnyConversions scalaNumericAnyConversions) {
            return scalaNumericAnyConversions.shortValue();
        }

        public static boolean unifiedPrimitiveEquals(ScalaNumericAnyConversions scalaNumericAnyConversions, Object obj) {
            if (obj instanceof Character) {
                return scalaNumericAnyConversions.isValidChar() && scalaNumericAnyConversions.toInt() == BoxesRunTime.unboxToChar(obj);
            } else if (obj instanceof Byte) {
                return scalaNumericAnyConversions.isValidByte() && scalaNumericAnyConversions.toByte() == BoxesRunTime.unboxToByte(obj);
            } else if (obj instanceof Short) {
                return scalaNumericAnyConversions.isValidShort() && scalaNumericAnyConversions.toShort() == BoxesRunTime.unboxToShort(obj);
            } else if (obj instanceof Integer) {
                return scalaNumericAnyConversions.isValidInt() && scalaNumericAnyConversions.toInt() == BoxesRunTime.unboxToInt(obj);
            } else if (obj instanceof Long) {
                return scalaNumericAnyConversions.toLong() == BoxesRunTime.unboxToLong(obj);
            } else if (obj instanceof Float) {
                return scalaNumericAnyConversions.toFloat() == BoxesRunTime.unboxToFloat(obj);
            } else if (!(obj instanceof Double)) {
                return false;
            } else {
                return scalaNumericAnyConversions.toDouble() == BoxesRunTime.unboxToDouble(obj);
            }
        }

        public static int unifiedPrimitiveHashcode(ScalaNumericAnyConversions scalaNumericAnyConversions) {
            long j = scalaNumericAnyConversions.toLong();
            return (j < -2147483648L || j > 2147483647L) ? ScalaRunTime$.MODULE$.hash(j) : (int) j;
        }
    }

    byte byteValue();

    double doubleValue();

    float floatValue();

    int intValue();

    boolean isValidByte();

    boolean isValidChar();

    boolean isValidInt();

    boolean isValidShort();

    long longValue();

    short shortValue();

    byte toByte();

    double toDouble();

    float toFloat();

    int toInt();

    long toLong();

    short toShort();

    boolean unifiedPrimitiveEquals(Object obj);

    int unifiedPrimitiveHashcode();
}
