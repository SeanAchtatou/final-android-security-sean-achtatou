package scala.math;

import scala.math.Ordering;
import scala.math.PartialOrdering;
import scala.runtime.BoxesRunTime;

public class Ordering$Int$ implements Ordering.IntOrdering {
    public static final Ordering$Int$ MODULE$ = null;

    static {
        new Ordering$Int$();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.math.Ordering, scala.math.Ordering$Int$, scala.math.PartialOrdering, scala.math.Ordering$IntOrdering] */
    public Ordering$Int$() {
        MODULE$ = this;
        PartialOrdering.Cclass.$init$(this);
        Ordering.Cclass.$init$(this);
        Ordering.IntOrdering.Cclass.$init$(this);
    }

    public int compare(int i, int i2) {
        return Ordering.IntOrdering.Cclass.compare(this, i, i2);
    }

    public /* synthetic */ int compare(Object obj, Object obj2) {
        return compare(BoxesRunTime.unboxToInt(obj), BoxesRunTime.unboxToInt(obj2));
    }
}
