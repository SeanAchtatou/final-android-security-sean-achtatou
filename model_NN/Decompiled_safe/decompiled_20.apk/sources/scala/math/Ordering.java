package scala.math;

import java.util.Comparator;
import scala.Serializable;

public interface Ordering<T> extends Comparator<T>, Serializable {

    public interface IntOrdering extends Ordering<Object> {

        /* renamed from: scala.math.Ordering$IntOrdering$class  reason: invalid class name */
        public abstract class Cclass {
            public static void $init$(IntOrdering intOrdering) {
            }

            public static int compare(IntOrdering intOrdering, int i, int i2) {
                if (i < i2) {
                    return -1;
                }
                return i == i2 ? 0 : 1;
            }
        }

        int compare(int i, int i2);
    }

    /* renamed from: scala.math.Ordering$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Ordering ordering) {
        }
    }

    int compare(Object obj, Object obj2);
}
