package scala.math;

import java.math.BigInteger;
import scala.Option;
import scala.Serializable;
import scala.math.ScalaNumericAnyConversions;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public class BigInt extends ScalaNumber implements Serializable, ScalaNumericConversions {
    private final BigInteger bigInteger;

    public BigInt(BigInteger bigInteger2) {
        this.bigInteger = bigInteger2;
        ScalaNumericAnyConversions.Cclass.$init$(this);
    }

    private boolean bitLengthOverflow() {
        BigInteger shiftRight = bigInteger().shiftRight(Integer.MAX_VALUE);
        return shiftRight.signum() != 0 && !shiftRight.equals(BigInt$.MODULE$.scala$math$BigInt$$minusOne());
    }

    public boolean $greater$eq(BigInt bigInt) {
        return compare(bigInt) >= 0;
    }

    public boolean $less$eq(BigInt bigInt) {
        return compare(bigInt) <= 0;
    }

    public BigInteger bigInteger() {
        return this.bigInteger;
    }

    public int bitLength() {
        return bigInteger().bitLength();
    }

    public byte byteValue() {
        return (byte) intValue();
    }

    public int compare(BigInt bigInt) {
        return bigInteger().compareTo(bigInt.bigInteger());
    }

    public double doubleValue() {
        return bigInteger().doubleValue();
    }

    public boolean equals(Object obj) {
        if (obj instanceof BigInt) {
            return equals((BigInt) obj);
        }
        if (obj instanceof BigDecimal) {
            Option<BigInt> bigIntExact = ((BigDecimal) obj).toBigIntExact();
            return !bigIntExact.isEmpty() && equals(bigIntExact.get());
        } else if (obj instanceof Double) {
            return isValidDouble() && toDouble() == BoxesRunTime.unboxToDouble(obj);
        } else if (!(obj instanceof Float)) {
            return isValidLong() && unifiedPrimitiveEquals(obj);
        } else {
            return isValidFloat() && toFloat() == BoxesRunTime.unboxToFloat(obj);
        }
    }

    public boolean equals(BigInt bigInt) {
        return compare(bigInt) == 0;
    }

    public float floatValue() {
        return bigInteger().floatValue();
    }

    public int hashCode() {
        return isValidLong() ? unifiedPrimitiveHashcode() : ScalaRunTime$.MODULE$.hash((Number) bigInteger());
    }

    public int intValue() {
        return bigInteger().intValue();
    }

    public boolean isValidByte() {
        return $greater$eq(BigInt$.MODULE$.int2bigInt(-128)) && $less$eq(BigInt$.MODULE$.int2bigInt(127));
    }

    public boolean isValidChar() {
        return $greater$eq(BigInt$.MODULE$.int2bigInt(0)) && $less$eq(BigInt$.MODULE$.int2bigInt(65535));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
        if ((r2 <= 1024 && r3 >= r2 + -53 && r3 < 1024) != false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isValidDouble() {
        /*
            r5 = this;
            r4 = 1024(0x400, float:1.435E-42)
            r0 = 1
            r1 = 0
            int r2 = r5.bitLength()
            r3 = 53
            if (r2 <= r3) goto L_0x001b
            int r3 = r5.lowestSetBit()
            if (r2 > r4) goto L_0x0022
            int r2 = r2 + -53
            if (r3 < r2) goto L_0x0022
            if (r3 >= r4) goto L_0x0022
            r2 = r0
        L_0x0019:
            if (r2 == 0) goto L_0x0024
        L_0x001b:
            boolean r2 = r5.bitLengthOverflow()
            if (r2 != 0) goto L_0x0024
        L_0x0021:
            return r0
        L_0x0022:
            r2 = r1
            goto L_0x0019
        L_0x0024:
            r0 = r1
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.math.BigInt.isValidDouble():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
        if ((r2 <= 128 && r3 >= r2 + -24 && r3 < 128) != false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isValidFloat() {
        /*
            r5 = this;
            r4 = 128(0x80, float:1.794E-43)
            r0 = 1
            r1 = 0
            int r2 = r5.bitLength()
            r3 = 24
            if (r2 <= r3) goto L_0x001b
            int r3 = r5.lowestSetBit()
            if (r2 > r4) goto L_0x0022
            int r2 = r2 + -24
            if (r3 < r2) goto L_0x0022
            if (r3 >= r4) goto L_0x0022
            r2 = r0
        L_0x0019:
            if (r2 == 0) goto L_0x0024
        L_0x001b:
            boolean r2 = r5.bitLengthOverflow()
            if (r2 != 0) goto L_0x0024
        L_0x0021:
            return r0
        L_0x0022:
            r2 = r1
            goto L_0x0019
        L_0x0024:
            r0 = r1
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.math.BigInt.isValidFloat():boolean");
    }

    public boolean isValidInt() {
        return $greater$eq(BigInt$.MODULE$.int2bigInt(Integer.MIN_VALUE)) && $less$eq(BigInt$.MODULE$.int2bigInt(Integer.MAX_VALUE));
    }

    public boolean isValidLong() {
        return $greater$eq(BigInt$.MODULE$.long2bigInt(Long.MIN_VALUE)) && $less$eq(BigInt$.MODULE$.long2bigInt(Long.MAX_VALUE));
    }

    public boolean isValidShort() {
        return $greater$eq(BigInt$.MODULE$.int2bigInt(-32768)) && $less$eq(BigInt$.MODULE$.int2bigInt(32767));
    }

    public long longValue() {
        return bigInteger().longValue();
    }

    public int lowestSetBit() {
        return bigInteger().getLowestSetBit();
    }

    public short shortValue() {
        return (short) intValue();
    }

    public byte toByte() {
        return ScalaNumericAnyConversions.Cclass.toByte(this);
    }

    public double toDouble() {
        return ScalaNumericAnyConversions.Cclass.toDouble(this);
    }

    public float toFloat() {
        return ScalaNumericAnyConversions.Cclass.toFloat(this);
    }

    public int toInt() {
        return ScalaNumericAnyConversions.Cclass.toInt(this);
    }

    public long toLong() {
        return ScalaNumericAnyConversions.Cclass.toLong(this);
    }

    public short toShort() {
        return ScalaNumericAnyConversions.Cclass.toShort(this);
    }

    public String toString() {
        return bigInteger().toString();
    }

    public BigInteger underlying() {
        return bigInteger();
    }

    public boolean unifiedPrimitiveEquals(Object obj) {
        return ScalaNumericAnyConversions.Cclass.unifiedPrimitiveEquals(this, obj);
    }

    public int unifiedPrimitiveHashcode() {
        return ScalaNumericAnyConversions.Cclass.unifiedPrimitiveHashcode(this);
    }
}
