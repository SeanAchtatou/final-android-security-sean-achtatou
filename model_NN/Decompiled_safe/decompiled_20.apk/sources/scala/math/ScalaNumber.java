package scala.math;

public abstract class ScalaNumber extends Number {
    public abstract Object underlying();
}
