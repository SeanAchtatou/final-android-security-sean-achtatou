package scala.math;

import java.math.MathContext;
import scala.Function0;
import scala.None$;
import scala.Option;
import scala.Predef$;
import scala.Serializable;
import scala.Some;
import scala.math.ScalaNumericAnyConversions;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichDouble$;
import scala.runtime.RichFloat$;
import scala.runtime.ScalaRunTime$;

public class BigDecimal extends ScalaNumber implements Serializable, ScalaNumericConversions {
    private final java.math.BigDecimal bigDecimal;
    private final MathContext mc;

    public BigDecimal(java.math.BigDecimal bigDecimal2, MathContext mathContext) {
        this.bigDecimal = bigDecimal2;
        this.mc = mathContext;
        ScalaNumericAnyConversions.Cclass.$init$(this);
    }

    private BigDecimal bigdec2BigDecimal(java.math.BigDecimal bigDecimal2) {
        return new BigDecimal(bigDecimal2, mc());
    }

    private boolean noArithmeticException(Function0<BoxedUnit> function0) {
        try {
            function0.apply$mcV$sp();
            return true;
        } catch (ArithmeticException e) {
            return false;
        }
    }

    public java.math.BigDecimal bigDecimal() {
        return this.bigDecimal;
    }

    public byte byteValue() {
        return (byte) intValue();
    }

    public int compare(BigDecimal bigDecimal2) {
        return bigDecimal().compareTo(bigDecimal2.bigDecimal());
    }

    public double doubleValue() {
        return bigDecimal().doubleValue();
    }

    public boolean equals(Object obj) {
        if (obj instanceof BigDecimal) {
            return equals((BigDecimal) obj);
        }
        if (obj instanceof BigInt) {
            BigInt bigInt = (BigInt) obj;
            Option<BigInt> bigIntExact = toBigIntExact();
            return !bigIntExact.isEmpty() && bigInt.equals(bigIntExact.get());
        } else if (obj instanceof Double) {
            return isValidDouble() && toDouble() == BoxesRunTime.unboxToDouble(obj);
        } else if (!(obj instanceof Float)) {
            return isValidLong() && unifiedPrimitiveEquals(obj);
        } else {
            return isValidFloat() && toFloat() == BoxesRunTime.unboxToFloat(obj);
        }
    }

    public boolean equals(BigDecimal bigDecimal2) {
        return compare(bigDecimal2) == 0;
    }

    public float floatValue() {
        return bigDecimal().floatValue();
    }

    public int hashCode() {
        return isWhole() ? unifiedPrimitiveHashcode() : ScalaRunTime$.MODULE$.hash(doubleValue());
    }

    public int intValue() {
        return bigDecimal().intValue();
    }

    public boolean isValidByte() {
        return noArithmeticException(new BigDecimal$$anonfun$isValidByte$1(this));
    }

    public boolean isValidChar() {
        return isValidInt() && toIntExact() >= 0 && toIntExact() <= 65535;
    }

    public boolean isValidDouble() {
        double d = toDouble();
        RichDouble$ richDouble$ = RichDouble$.MODULE$;
        Predef$ predef$ = Predef$.MODULE$;
        return !richDouble$.isInfinity$extension(d) && bigDecimal().compareTo(new java.math.BigDecimal(d)) == 0;
    }

    public boolean isValidFloat() {
        float f = toFloat();
        RichFloat$ richFloat$ = RichFloat$.MODULE$;
        Predef$ predef$ = Predef$.MODULE$;
        return !richFloat$.isInfinity$extension(f) && bigDecimal().compareTo(new java.math.BigDecimal((double) f)) == 0;
    }

    public boolean isValidInt() {
        return noArithmeticException(new BigDecimal$$anonfun$isValidInt$1(this));
    }

    public boolean isValidLong() {
        return noArithmeticException(new BigDecimal$$anonfun$isValidLong$1(this));
    }

    public boolean isValidShort() {
        return noArithmeticException(new BigDecimal$$anonfun$isValidShort$1(this));
    }

    public boolean isWhole() {
        BigDecimal remainder = remainder(BigDecimal$.MODULE$.int2bigDecimal(1));
        BigDecimal apply = BigDecimal$.MODULE$.apply(0);
        return remainder == apply ? true : remainder == null ? false : remainder instanceof Number ? BoxesRunTime.equalsNumObject(remainder, apply) : remainder instanceof Character ? BoxesRunTime.equalsCharObject((Character) remainder, apply) : remainder.equals(apply);
    }

    public long longValue() {
        return bigDecimal().longValue();
    }

    public MathContext mc() {
        return this.mc;
    }

    public BigDecimal remainder(BigDecimal bigDecimal2) {
        return bigdec2BigDecimal(bigDecimal().remainder(bigDecimal2.bigDecimal()));
    }

    public short shortValue() {
        return (short) intValue();
    }

    public Option<BigInt> toBigIntExact() {
        try {
            return new Some(new BigInt(bigDecimal().toBigIntegerExact()));
        } catch (ArithmeticException e) {
            return None$.MODULE$;
        }
    }

    public byte toByte() {
        return ScalaNumericAnyConversions.Cclass.toByte(this);
    }

    public byte toByteExact() {
        return bigDecimal().byteValueExact();
    }

    public double toDouble() {
        return ScalaNumericAnyConversions.Cclass.toDouble(this);
    }

    public float toFloat() {
        return ScalaNumericAnyConversions.Cclass.toFloat(this);
    }

    public int toInt() {
        return ScalaNumericAnyConversions.Cclass.toInt(this);
    }

    public int toIntExact() {
        return bigDecimal().intValueExact();
    }

    public long toLong() {
        return ScalaNumericAnyConversions.Cclass.toLong(this);
    }

    public long toLongExact() {
        return bigDecimal().longValueExact();
    }

    public short toShort() {
        return ScalaNumericAnyConversions.Cclass.toShort(this);
    }

    public short toShortExact() {
        return bigDecimal().shortValueExact();
    }

    public String toString() {
        return bigDecimal().toString();
    }

    public java.math.BigDecimal underlying() {
        return bigDecimal();
    }

    public boolean unifiedPrimitiveEquals(Object obj) {
        return ScalaNumericAnyConversions.Cclass.unifiedPrimitiveEquals(this, obj);
    }

    public int unifiedPrimitiveHashcode() {
        return ScalaNumericAnyConversions.Cclass.unifiedPrimitiveHashcode(this);
    }
}
