package scala.math;

import java.math.BigInteger;
import scala.Serializable;

public final class BigInt$ implements Serializable {
    public static final BigInt$ MODULE$ = null;
    private final BigInt MaxLong = apply(Long.MAX_VALUE);
    private final BigInt MinLong = apply(Long.MIN_VALUE);
    private final BigInt[] cache = new BigInt[((maxCached() - minCached()) + 1)];
    private final int maxCached = 1024;
    private final int minCached = -1024;
    private final BigInteger scala$math$BigInt$$minusOne = BigInteger.valueOf(-1);

    static {
        new BigInt$();
    }

    private BigInt$() {
        MODULE$ = this;
    }

    private BigInt[] cache() {
        return this.cache;
    }

    private int maxCached() {
        return this.maxCached;
    }

    private int minCached() {
        return this.minCached;
    }

    public final BigInt apply(int i) {
        if (minCached() > i || i > maxCached()) {
            return new BigInt(BigInteger.valueOf((long) i));
        }
        int minCached2 = i - minCached();
        BigInt bigInt = cache()[minCached2];
        if (bigInt != null) {
            return bigInt;
        }
        BigInt bigInt2 = new BigInt(BigInteger.valueOf((long) i));
        cache()[minCached2] = bigInt2;
        return bigInt2;
    }

    public final BigInt apply(long j) {
        return (((long) minCached()) > j || j > ((long) maxCached())) ? new BigInt(BigInteger.valueOf(j)) : apply((int) j);
    }

    public final BigInt int2bigInt(int i) {
        return apply(i);
    }

    public final BigInt long2bigInt(long j) {
        return apply(j);
    }

    public final BigInteger scala$math$BigInt$$minusOne() {
        return this.scala$math$BigInt$$minusOne;
    }
}
