package scala;

import scala.runtime.BoxesRunTime;

public interface Product2<T1, T2> extends Product {

    /* renamed from: scala.Product2$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Product2 product2) {
        }

        public static int productArity(Product2 product2) {
            return 2;
        }

        public static Object productElement(Product2 product2, int i) {
            switch (i) {
                case 0:
                    return product2._1();
                case 1:
                    return product2._2();
                default:
                    throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
            }
        }
    }

    T1 _1();

    T2 _2();
}
