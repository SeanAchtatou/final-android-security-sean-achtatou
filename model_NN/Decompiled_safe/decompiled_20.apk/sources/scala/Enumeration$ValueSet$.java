package scala;

import scala.Enumeration;
import scala.collection.immutable.BitSet$;

public class Enumeration$ValueSet$ implements Serializable {
    private final /* synthetic */ Enumeration $outer;
    private final Enumeration.ValueSet empty;

    public Enumeration$ValueSet$(Enumeration enumeration) {
        if (enumeration == null) {
            throw new NullPointerException();
        }
        this.$outer = enumeration;
        this.empty = new Enumeration.ValueSet(enumeration, BitSet$.MODULE$.empty());
    }

    public Enumeration.ValueSet empty() {
        return this.empty;
    }
}
