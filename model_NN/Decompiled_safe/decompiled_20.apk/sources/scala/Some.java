package scala;

import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

public final class Some<A> extends Option<A> {
    private final A x;

    public Some(A a) {
        this.x = a;
    }

    public final boolean canEqual(Object obj) {
        return obj instanceof Some;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        if ((r0 == r3 ? true : r0 == null ? false : r0 instanceof java.lang.Number ? scala.runtime.BoxesRunTime.equalsNumObject((java.lang.Number) r0, r3) : r0 instanceof java.lang.Character ? scala.runtime.BoxesRunTime.equalsCharObject((java.lang.Character) r0, r3) : r0.equals(r3)) != false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r2 = 1
            r1 = 0
            if (r5 == r6) goto L_0x001d
            boolean r0 = r6 instanceof scala.Some
            if (r0 == 0) goto L_0x001f
            r0 = r2
        L_0x0009:
            if (r0 == 0) goto L_0x0042
            scala.Some r6 = (scala.Some) r6
            java.lang.Object r0 = r5.x()
            java.lang.Object r3 = r6.x()
            if (r0 != r3) goto L_0x0021
            r0 = r2
        L_0x0018:
            if (r0 == 0) goto L_0x0040
            r0 = r2
        L_0x001b:
            if (r0 == 0) goto L_0x0042
        L_0x001d:
            r0 = r2
        L_0x001e:
            return r0
        L_0x001f:
            r0 = r1
            goto L_0x0009
        L_0x0021:
            if (r0 != 0) goto L_0x0025
            r0 = r1
            goto L_0x0018
        L_0x0025:
            boolean r4 = r0 instanceof java.lang.Number
            if (r4 == 0) goto L_0x0030
            java.lang.Number r0 = (java.lang.Number) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r0, r3)
            goto L_0x0018
        L_0x0030:
            boolean r4 = r0 instanceof java.lang.Character
            if (r4 == 0) goto L_0x003b
            java.lang.Character r0 = (java.lang.Character) r0
            boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r0, r3)
            goto L_0x0018
        L_0x003b:
            boolean r0 = r0.equals(r3)
            goto L_0x0018
        L_0x0040:
            r0 = r1
            goto L_0x001b
        L_0x0042:
            r0 = r1
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.Some.equals(java.lang.Object):boolean");
    }

    public final A get() {
        return x();
    }

    public final int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public final boolean isEmpty() {
        return false;
    }

    public final int productArity() {
        return 1;
    }

    public final Object productElement(int i) {
        switch (i) {
            case 0:
                break;
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
        return x();
    }

    public final Iterator<Object> productIterator() {
        return ScalaRunTime$.MODULE$.typedProductIterator(this);
    }

    public final String productPrefix() {
        return "Some";
    }

    public final String toString() {
        return ScalaRunTime$.MODULE$._toString(this);
    }

    public final A x() {
        return this.x;
    }
}
