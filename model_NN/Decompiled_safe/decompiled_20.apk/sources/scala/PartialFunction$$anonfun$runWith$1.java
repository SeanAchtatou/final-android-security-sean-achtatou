package scala;

import scala.runtime.AbstractFunction1;

public final class PartialFunction$$anonfun$runWith$1 extends AbstractFunction1<A, Object> implements Serializable {
    private final /* synthetic */ PartialFunction $outer;
    private final Function1 action$1;

    public PartialFunction$$anonfun$runWith$1(PartialFunction partialFunction, PartialFunction<A, B> partialFunction2) {
        if (partialFunction == null) {
            throw new NullPointerException();
        }
        this.$outer = partialFunction;
        this.action$1 = partialFunction2;
    }

    public final boolean apply(A a) {
        Object applyOrElse = this.$outer.applyOrElse(a, PartialFunction$.MODULE$.scala$PartialFunction$$checkFallback());
        if (PartialFunction$.MODULE$.scala$PartialFunction$$fallbackOccurred(applyOrElse)) {
            return false;
        }
        this.action$1.apply(applyOrElse);
        return true;
    }
}
