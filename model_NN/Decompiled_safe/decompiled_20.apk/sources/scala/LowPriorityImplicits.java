package scala;

import scala.collection.immutable.WrappedString;
import scala.collection.mutable.WrappedArray;
import scala.collection.mutable.WrappedArray$;

public class LowPriorityImplicits {
    public <T> WrappedArray<T> genericWrapArray(Object obj) {
        if (obj == null) {
            return null;
        }
        return WrappedArray$.MODULE$.make(obj);
    }

    public int intWrapper(int i) {
        return i;
    }

    public String unwrapString(WrappedString wrappedString) {
        if (wrappedString != null) {
            return wrappedString.self();
        }
        return null;
    }

    public <T> WrappedArray<T> wrapRefArray(T[] tArr) {
        if (tArr == null) {
            return null;
        }
        return tArr.length == 0 ? WrappedArray$.MODULE$.empty() : new WrappedArray.ofRef(tArr);
    }
}
