package scala;

import scala.runtime.AbstractFunction1;

public final class StringContext$$anonfun$s$1 extends AbstractFunction1<String, String> implements Serializable {
    public StringContext$$anonfun$s$1(StringContext stringContext) {
    }

    public final String apply(String str) {
        return StringContext$.MODULE$.treatEscapes(str);
    }
}
