package scala.util;

public final class DynamicVariable$$anon$1 extends InheritableThreadLocal<T> {
    private final /* synthetic */ DynamicVariable $outer;

    public DynamicVariable$$anon$1(DynamicVariable<T> dynamicVariable) {
        if (dynamicVariable == null) {
            throw new NullPointerException();
        }
        this.$outer = dynamicVariable;
    }

    public final T initialValue() {
        return this.$outer.scala$util$DynamicVariable$$init;
    }
}
