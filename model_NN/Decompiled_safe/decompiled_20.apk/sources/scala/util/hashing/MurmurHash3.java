package scala.util.hashing;

import scala.Product;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.runtime.IntRef;
import scala.runtime.ScalaRunTime$;

public class MurmurHash3 {
    private final int avalanche(int i) {
        int i2 = ((i >>> 16) ^ i) * -2048144789;
        int i3 = (i2 ^ (i2 >>> 13)) * -1028477387;
        return i3 ^ (i3 >>> 16);
    }

    public final int finalizeHash(int i, int i2) {
        return avalanche(i ^ i2);
    }

    public final int listHash(List<?> list, int i) {
        int i2 = 0;
        while (!list.isEmpty()) {
            i = mix(i, ScalaRunTime$.MODULE$.hash(list.head()));
            i2++;
            list = (List) list.tail();
        }
        return finalizeHash(i, i2);
    }

    public final int mix(int i, int i2) {
        return (Integer.rotateLeft(mixLast(i, i2), 13) * 5) - 430675100;
    }

    public final int mixLast(int i, int i2) {
        return (Integer.rotateLeft(-862048943 * i2, 15) * 461845907) ^ i;
    }

    public final int orderedHash(TraversableOnce<Object> traversableOnce, int i) {
        IntRef intRef = new IntRef(0);
        IntRef intRef2 = new IntRef(i);
        traversableOnce.foreach(new MurmurHash3$$anonfun$orderedHash$1(this, intRef, intRef2));
        return finalizeHash(intRef2.elem, intRef.elem);
    }

    public final int productHash(Product product, int i) {
        int productArity = product.productArity();
        if (productArity == 0) {
            return product.productPrefix().hashCode();
        }
        for (int i2 = 0; i2 < productArity; i2++) {
            i = mix(i, ScalaRunTime$.MODULE$.hash(product.productElement(i2)));
        }
        return finalizeHash(i, productArity);
    }

    public final int unorderedHash(TraversableOnce<Object> traversableOnce, int i) {
        IntRef intRef = new IntRef(0);
        IntRef intRef2 = new IntRef(0);
        IntRef intRef3 = new IntRef(0);
        IntRef intRef4 = new IntRef(1);
        traversableOnce.foreach(new MurmurHash3$$anonfun$unorderedHash$1(this, intRef, intRef2, intRef3, intRef4));
        return finalizeHash(mixLast(mix(mix(i, intRef.elem), intRef2.elem), intRef4.elem), intRef3.elem);
    }
}
