package scala.util.hashing;

import scala.Product;
import scala.collection.Map;
import scala.collection.Seq;
import scala.collection.Set;
import scala.collection.immutable.List;

public final class MurmurHash3$ extends MurmurHash3 {
    public static final MurmurHash3$ MODULE$ = null;
    private final int mapSeed = "Map".hashCode();
    private final int seqSeed = "Seq".hashCode();
    private final int setSeed = "Set".hashCode();

    static {
        new MurmurHash3$();
    }

    private MurmurHash3$() {
        MODULE$ = this;
    }

    public final int mapHash(Map<?, ?> map) {
        return unorderedHash(map, mapSeed());
    }

    public final int mapSeed() {
        return this.mapSeed;
    }

    public final int productHash(Product product) {
        return productHash(product, -889275714);
    }

    public final int seqHash(Seq<?> seq) {
        return seq instanceof List ? listHash((List) seq, seqSeed()) : orderedHash(seq, seqSeed());
    }

    public final int seqSeed() {
        return this.seqSeed;
    }

    public final int setHash(Set<?> set) {
        return unorderedHash(set, setSeed());
    }

    public final int setSeed() {
        return this.setSeed;
    }
}
