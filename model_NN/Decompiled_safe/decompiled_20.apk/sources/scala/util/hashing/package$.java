package scala.util.hashing;

public final class package$ {
    public static final package$ MODULE$ = null;

    static {
        new package$();
    }

    private package$() {
        MODULE$ = this;
    }

    public final int byteswap32(int i) {
        return Integer.reverseBytes(i * -1640532531) * -1640532531;
    }
}
