package scala.util.hashing;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.IntRef;
import scala.runtime.ScalaRunTime$;

public final class MurmurHash3$$anonfun$orderedHash$1 extends AbstractFunction1<Object, BoxedUnit> implements Serializable {
    private final /* synthetic */ MurmurHash3 $outer;
    private final IntRef h$1;
    private final IntRef n$2;

    public MurmurHash3$$anonfun$orderedHash$1(MurmurHash3 murmurHash3, IntRef intRef, IntRef intRef2) {
        if (murmurHash3 == null) {
            throw new NullPointerException();
        }
        this.$outer = murmurHash3;
        this.n$2 = intRef;
        this.h$1 = intRef2;
    }

    public final void apply(Object obj) {
        this.h$1.elem = this.$outer.mix(this.h$1.elem, ScalaRunTime$.MODULE$.hash(obj));
        this.n$2.elem++;
    }
}
