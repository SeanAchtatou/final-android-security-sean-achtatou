package scala.util;

import scala.Serializable;

public final class Right$ implements Serializable {
    public static final Right$ MODULE$ = null;

    static {
        new Right$();
    }

    private Right$() {
        MODULE$ = this;
    }

    public final String toString() {
        return "Right";
    }
}
