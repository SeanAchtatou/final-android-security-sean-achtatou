package scala.util;

import java.io.InputStream;
import java.util.Properties;
import scala.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;

public final class PropertiesTrait$$anonfun$scalaProps$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public final Properties props$1;
    public final InputStream stream$1;

    public PropertiesTrait$$anonfun$scalaProps$1(PropertiesTrait propertiesTrait, Properties properties, InputStream inputStream) {
        this.props$1 = properties;
        this.stream$1 = inputStream;
    }

    public final void apply() {
        this.props$1.load(this.stream$1);
    }

    public final void apply$mcV$sp() {
        this.props$1.load(this.stream$1);
    }
}
