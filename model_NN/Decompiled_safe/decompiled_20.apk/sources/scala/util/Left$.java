package scala.util;

import scala.Serializable;

public final class Left$ implements Serializable {
    public static final Left$ MODULE$ = null;

    static {
        new Left$();
    }

    private Left$() {
        MODULE$ = this;
    }

    public final String toString() {
        return "Left";
    }
}
