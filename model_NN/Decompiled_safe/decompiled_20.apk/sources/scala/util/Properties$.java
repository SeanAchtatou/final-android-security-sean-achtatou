package scala.util;

import java.util.Properties;
import java.util.jar.Attributes;
import scala.Option;
import scala.util.PropertiesTrait;

public final class Properties$ implements PropertiesTrait {
    public static final Properties$ MODULE$ = null;
    private final Attributes.Name ScalaCompilerVersion = new Attributes.Name("Scala-Compiler-Version");
    private volatile boolean bitmap$0;
    private final String copyrightString;
    private final Option<String> developmentVersion;
    private final String propFilename;
    private final Option<String> releaseVersion;
    private final Properties scalaProps;
    private final String versionString;

    static {
        new Properties$();
    }

    private Properties$() {
        MODULE$ = this;
        PropertiesTrait.Cclass.$init$(this);
    }

    private Properties scalaProps$lzycompute() {
        synchronized (this) {
            if (!this.bitmap$0) {
                this.scalaProps = PropertiesTrait.Cclass.scalaProps(this);
                this.bitmap$0 = true;
            }
        }
        return this.scalaProps;
    }

    public final String lineSeparator() {
        return PropertiesTrait.Cclass.lineSeparator(this);
    }

    public final Class<Option<?>> pickJarBasedOn() {
        return Option.class;
    }

    public final String propCategory() {
        return "library";
    }

    public final String propFilename() {
        return this.propFilename;
    }

    public final String propOrElse(String str, String str2) {
        return PropertiesTrait.Cclass.propOrElse(this, str, str2);
    }

    public final void scala$util$PropertiesTrait$_setter_$copyrightString_$eq(String str) {
        this.copyrightString = str;
    }

    public final void scala$util$PropertiesTrait$_setter_$developmentVersion_$eq(Option option) {
        this.developmentVersion = option;
    }

    public final void scala$util$PropertiesTrait$_setter_$propFilename_$eq(String str) {
        this.propFilename = str;
    }

    public final void scala$util$PropertiesTrait$_setter_$releaseVersion_$eq(Option option) {
        this.releaseVersion = option;
    }

    public final void scala$util$PropertiesTrait$_setter_$versionString_$eq(String str) {
        this.versionString = str;
    }

    public final String scalaPropOrElse(String str, String str2) {
        return PropertiesTrait.Cclass.scalaPropOrElse(this, str, str2);
    }

    public final Option<String> scalaPropOrNone(String str) {
        return PropertiesTrait.Cclass.scalaPropOrNone(this, str);
    }

    public final Properties scalaProps() {
        return this.bitmap$0 ? this.scalaProps : scalaProps$lzycompute();
    }
}
