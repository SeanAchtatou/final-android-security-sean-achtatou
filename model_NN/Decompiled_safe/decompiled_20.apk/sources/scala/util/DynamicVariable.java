package scala.util;

import scala.collection.mutable.StringBuilder;

public class DynamicVariable<T> {
    public final T scala$util$DynamicVariable$$init;
    private final InheritableThreadLocal<T> tl = new DynamicVariable$$anon$1(this);

    public DynamicVariable(T t) {
        this.scala$util$DynamicVariable$$init = t;
    }

    private InheritableThreadLocal<T> tl() {
        return this.tl;
    }

    public String toString() {
        return new StringBuilder().append((Object) "DynamicVariable(").append(value()).append((Object) ")").toString();
    }

    public T value() {
        return tl().get();
    }
}
