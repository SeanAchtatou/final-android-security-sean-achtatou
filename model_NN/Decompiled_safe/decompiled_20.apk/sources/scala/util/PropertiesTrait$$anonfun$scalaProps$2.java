package scala.util;

import java.io.InputStream;
import scala.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;

public final class PropertiesTrait$$anonfun$scalaProps$2 extends AbstractFunction0$mcV$sp implements Serializable {
    public final InputStream stream$1;

    public PropertiesTrait$$anonfun$scalaProps$2(PropertiesTrait propertiesTrait, InputStream inputStream) {
        this.stream$1 = inputStream;
    }

    public final void apply() {
        this.stream$1.close();
    }

    public final void apply$mcV$sp() {
        this.stream$1.close();
    }
}
