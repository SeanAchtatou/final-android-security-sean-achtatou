package scala.util.control;

import scala.util.control.NoStackTrace;

public class BreakControl extends Throwable implements ControlThrowable {
    public BreakControl() {
        NoStackTrace.Cclass.$init$(this);
    }

    public Throwable fillInStackTrace() {
        return NoStackTrace.Cclass.fillInStackTrace(this);
    }

    public Throwable scala$util$control$NoStackTrace$$super$fillInStackTrace() {
        return super.fillInStackTrace();
    }
}
