package scala.util.control;

public interface NoStackTrace {

    /* renamed from: scala.util.control.NoStackTrace$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(NoStackTrace noStackTrace) {
        }

        public static Throwable fillInStackTrace(NoStackTrace noStackTrace) {
            return NoStackTrace$.MODULE$.noSuppression() ? noStackTrace.scala$util$control$NoStackTrace$$super$fillInStackTrace() : (Throwable) noStackTrace;
        }
    }

    Throwable scala$util$control$NoStackTrace$$super$fillInStackTrace();
}
