package scala.util.control;

import scala.Function0;
import scala.runtime.BoxedUnit;
import scala.runtime.Nothing$;

public class Breaks {
    private final BreakControl scala$util$control$Breaks$$breakException = new BreakControl();

    /* renamed from: break  reason: not valid java name */
    public Nothing$ m6break() {
        throw scala$util$control$Breaks$$breakException();
    }

    public void breakable(Function0<BoxedUnit> function0) {
        try {
            function0.apply$mcV$sp();
        } catch (BreakControl e) {
            if (e != scala$util$control$Breaks$$breakException()) {
                throw e;
            }
        }
    }

    public BreakControl scala$util$control$Breaks$$breakException() {
        return this.scala$util$control$Breaks$$breakException;
    }
}
