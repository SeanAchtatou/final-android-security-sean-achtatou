package scala.util;

import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

public final class PropertiesTrait$$anonfun$3 extends AbstractFunction1<String, Object> implements Serializable {
    public PropertiesTrait$$anonfun$3(PropertiesTrait propertiesTrait) {
    }

    public final /* synthetic */ Object apply(Object obj) {
        return BoxesRunTime.boxToBoolean(apply((String) obj));
    }

    public final boolean apply(String str) {
        return str.endsWith("-SNAPSHOT");
    }
}
