package scala.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.commons.httpclient.cookie.CookieSpec;
import scala.Function0;
import scala.Option;
import scala.Option$;
import scala.collection.mutable.StringBuilder;

public interface PropertiesTrait {

    /* renamed from: scala.util.PropertiesTrait$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(PropertiesTrait propertiesTrait) {
            propertiesTrait.scala$util$PropertiesTrait$_setter_$propFilename_$eq(new StringBuilder().append((Object) CookieSpec.PATH_DELIM).append((Object) propertiesTrait.propCategory()).append((Object) ".properties").toString());
            propertiesTrait.scala$util$PropertiesTrait$_setter_$releaseVersion_$eq(new Option.WithFilter(propertiesTrait.scalaPropOrNone("maven.version.number"), new PropertiesTrait$$anonfun$1(propertiesTrait)).map(new PropertiesTrait$$anonfun$2(propertiesTrait)));
            propertiesTrait.scala$util$PropertiesTrait$_setter_$developmentVersion_$eq(new Option.WithFilter(propertiesTrait.scalaPropOrNone("maven.version.number"), new PropertiesTrait$$anonfun$3(propertiesTrait)).flatMap(new PropertiesTrait$$anonfun$4(propertiesTrait)));
            propertiesTrait.scala$util$PropertiesTrait$_setter_$versionString_$eq(new StringBuilder().append((Object) "version ").append((Object) propertiesTrait.scalaPropOrElse("version.number", "(unknown)")).toString());
            propertiesTrait.scala$util$PropertiesTrait$_setter_$copyrightString_$eq(propertiesTrait.scalaPropOrElse("copyright.string", "Copyright 2002-2013, LAMP/EPFL"));
        }

        public static String lineSeparator(PropertiesTrait propertiesTrait) {
            return propertiesTrait.propOrElse("line.separator", "\n");
        }

        public static String propOrElse(PropertiesTrait propertiesTrait, String str, String str2) {
            return System.getProperty(str, str2);
        }

        private static void quietlyDispose(PropertiesTrait propertiesTrait, Function0 function0, Function0 function02) {
            try {
                function0.apply$mcV$sp();
            } finally {
                try {
                    function02.apply$mcV$sp();
                } catch (IOException e) {
                }
            }
        }

        public static String scalaPropOrElse(PropertiesTrait propertiesTrait, String str, String str2) {
            return propertiesTrait.scalaProps().getProperty(str, str2);
        }

        public static Option scalaPropOrNone(PropertiesTrait propertiesTrait, String str) {
            return Option$.MODULE$.apply(propertiesTrait.scalaProps().getProperty(str));
        }

        public static Properties scalaProps(PropertiesTrait propertiesTrait) {
            Properties properties = new Properties();
            InputStream resourceAsStream = propertiesTrait.pickJarBasedOn().getResourceAsStream(propertiesTrait.propFilename());
            if (resourceAsStream != null) {
                quietlyDispose(propertiesTrait, new PropertiesTrait$$anonfun$scalaProps$1(propertiesTrait, properties, resourceAsStream), new PropertiesTrait$$anonfun$scalaProps$2(propertiesTrait, resourceAsStream));
            }
            return properties;
        }
    }

    Class<?> pickJarBasedOn();

    String propCategory();

    String propFilename();

    String propOrElse(String str, String str2);

    void scala$util$PropertiesTrait$_setter_$copyrightString_$eq(String str);

    void scala$util$PropertiesTrait$_setter_$developmentVersion_$eq(Option option);

    void scala$util$PropertiesTrait$_setter_$propFilename_$eq(String str);

    void scala$util$PropertiesTrait$_setter_$releaseVersion_$eq(Option option);

    void scala$util$PropertiesTrait$_setter_$versionString_$eq(String str);

    String scalaPropOrElse(String str, String str2);

    Option<String> scalaPropOrNone(String str);

    Properties scalaProps();
}
