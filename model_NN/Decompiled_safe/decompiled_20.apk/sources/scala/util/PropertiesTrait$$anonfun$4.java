package scala.util;

import scala.None$;
import scala.Option;
import scala.Serializable;
import scala.Some;
import scala.runtime.AbstractFunction1;

public final class PropertiesTrait$$anonfun$4 extends AbstractFunction1<String, Option<String>> implements Serializable {
    private final /* synthetic */ PropertiesTrait $outer;

    public PropertiesTrait$$anonfun$4(PropertiesTrait propertiesTrait) {
        if (propertiesTrait == null) {
            throw new NullPointerException();
        }
        this.$outer = propertiesTrait;
    }

    public final Option<String> apply(String str) {
        Option<String> scalaPropOrNone = this.$outer.scalaPropOrNone("version.number");
        return scalaPropOrNone.isEmpty() ? None$.MODULE$ : new Some(scalaPropOrNone.get());
    }
}
