package scala;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;
import scala.collection.AbstractSet;
import scala.collection.GenSet;
import scala.collection.GenSetLike;
import scala.collection.Iterator;
import scala.collection.SortedSet;
import scala.collection.SortedSetLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.Sorted;
import scala.collection.immutable.BitSet;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Set;
import scala.collection.immutable.SortedSet;
import scala.collection.immutable.StringOps;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.HashMap;
import scala.collection.mutable.Map;
import scala.collection.mutable.StringBuilder;
import scala.math.Ordered;
import scala.math.Ordering;
import scala.reflect.NameTransformer$;
import scala.runtime.BoxesRunTime;
import scala.runtime.StringAdd$;

public abstract class Enumeration implements Serializable {
    private volatile Enumeration$ValueOrdering$ ValueOrdering$module;
    private volatile Enumeration$ValueSet$ ValueSet$module;
    private int nextId;
    private Iterator<String> nextName;
    private int scala$Enumeration$$bottomId;
    private final Map<Object, String> scala$Enumeration$$nmap;
    private int scala$Enumeration$$topId;
    private final Map<Object, Value> scala$Enumeration$$vmap;
    private volatile transient boolean scala$Enumeration$$vsetDefined;
    private transient ValueSet vset;

    public class Val extends Value {
        private final String name;
        public final int scala$Enumeration$Val$$i;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Val(Enumeration enumeration, int i, String str) {
            super(enumeration);
            this.scala$Enumeration$Val$$i = i;
            this.name = str;
            Predef$ predef$ = Predef$.MODULE$;
            if (!enumeration.scala$Enumeration$$vmap().isDefinedAt(BoxesRunTime.boxToInteger(i))) {
                enumeration.scala$Enumeration$$vmap().update(BoxesRunTime.boxToInteger(i), this);
                enumeration.scala$Enumeration$$vsetDefined_$eq(false);
                enumeration.nextId_$eq(i + 1);
                if (enumeration.nextId() > enumeration.scala$Enumeration$$topId()) {
                    enumeration.scala$Enumeration$$topId_$eq(enumeration.nextId());
                }
                if (i < enumeration.scala$Enumeration$$bottomId()) {
                    enumeration.scala$Enumeration$$bottomId_$eq(i);
                    return;
                }
                return;
            }
            throw new AssertionError(new StringBuilder().append((Object) "assertion failed: ").append((Object) new StringBuilder().append((Object) "Duplicate id: ").append(BoxesRunTime.boxToInteger(this.scala$Enumeration$Val$$i)).toString()).toString());
        }

        public int id() {
            return this.scala$Enumeration$Val$$i;
        }

        public /* synthetic */ Enumeration scala$Enumeration$Val$$$outer() {
            return this.$outer;
        }

        public String toString() {
            if (this.name != null) {
                return this.name;
            }
            try {
                return scala$Enumeration$Val$$$outer().scala$Enumeration$$nameOf(this.scala$Enumeration$Val$$i);
            } catch (NoSuchElementException e) {
                return new StringBuilder().append((Object) "<Invalid enum: no field for #").append(BoxesRunTime.boxToInteger(this.scala$Enumeration$Val$$i)).append((Object) ">").toString();
            }
        }
    }

    public abstract class Value implements Serializable, Ordered<Value> {
        public final /* synthetic */ Enumeration $outer;
        private final Enumeration scala$Enumeration$$outerEnum;

        public Value(Enumeration enumeration) {
            if (enumeration == null) {
                throw new NullPointerException();
            }
            this.$outer = enumeration;
            Ordered.Cclass.$init$(this);
            this.scala$Enumeration$$outerEnum = enumeration;
        }

        public int compare(Value value) {
            if (id() < value.id()) {
                return -1;
            }
            return id() == value.id() ? 0 : 1;
        }

        public int compareTo(Object obj) {
            return Ordered.Cclass.compareTo(this, obj);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Value)) {
                return false;
            }
            Value value = (Value) obj;
            return scala$Enumeration$$outerEnum() == value.scala$Enumeration$$outerEnum() && id() == value.id();
        }

        public int hashCode() {
            return id();
        }

        public abstract int id();

        public Enumeration scala$Enumeration$$outerEnum() {
            return this.scala$Enumeration$$outerEnum;
        }
    }

    public class ValueSet extends AbstractSet<Value> implements Serializable, SortedSetLike<Value, ValueSet> {
        public final /* synthetic */ Enumeration $outer;
        private BitSet nnIds;

        /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Traversable, scala.collection.SortedSet, scala.collection.immutable.SortedSet, scala.collection.generic.Sorted, scala.collection.immutable.Iterable, scala.collection.SortedSetLike, scala.collection.immutable.Set, scala.Enumeration$ValueSet] */
        public ValueSet(Enumeration enumeration, BitSet bitSet) {
            this.nnIds = bitSet;
            if (enumeration == null) {
                throw new NullPointerException();
            }
            this.$outer = enumeration;
            Traversable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Set.Cclass.$init$(this);
            Sorted.Cclass.$init$(this);
            SortedSetLike.Cclass.$init$(this);
            SortedSet.Cclass.$init$(this);
            SortedSet.Cclass.$init$(this);
        }

        public ValueSet $minus(Value value) {
            return new ValueSet(scala$Enumeration$ValueSet$$$outer(), this.nnIds.$minus(value.id() - scala$Enumeration$ValueSet$$$outer().scala$Enumeration$$bottomId()));
        }

        public ValueSet $plus(Value value) {
            return new ValueSet(scala$Enumeration$ValueSet$$$outer(), this.nnIds.$plus(value.id() - scala$Enumeration$ValueSet$$$outer().scala$Enumeration$$bottomId()));
        }

        public /* synthetic */ Object apply(Object obj) {
            return BoxesRunTime.boxToBoolean(apply(obj));
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Set, scala.Enumeration$ValueSet] */
        public GenericCompanion<Set> companion() {
            return Set.Cclass.companion(this);
        }

        public int compare(Object obj, Object obj2) {
            return Sorted.Cclass.compare(this, obj, obj2);
        }

        public boolean contains(Value value) {
            return this.nnIds.contains(value.id() - scala$Enumeration$ValueSet$$$outer().scala$Enumeration$$bottomId());
        }

        public ValueSet empty() {
            return scala$Enumeration$ValueSet$$$outer().ValueSet().empty();
        }

        public boolean hasAll(Iterator<Value> iterator) {
            return Sorted.Cclass.hasAll(this, iterator);
        }

        public Iterator<Value> iterator() {
            return this.nnIds.iterator().map(new Enumeration$ValueSet$$anonfun$iterator$1(this));
        }

        public scala.collection.SortedSet keySet() {
            return SortedSetLike.Cclass.keySet(this);
        }

        public Ordering<Value> ordering() {
            return scala$Enumeration$ValueSet$$$outer().ValueOrdering();
        }

        public /* synthetic */ Enumeration scala$Enumeration$ValueSet$$$outer() {
            return this.$outer;
        }

        public boolean scala$collection$SortedSetLike$$super$subsetOf(GenSet genSet) {
            return GenSetLike.Cclass.subsetOf(this, genSet);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Set, scala.Enumeration$ValueSet] */
        public Set<Value> seq() {
            return Set.Cclass.seq(this);
        }

        public String stringPrefix() {
            return StringAdd$.MODULE$.$plus$extension(Predef$.MODULE$.any2stringadd(scala$Enumeration$ValueSet$$$outer()), ".ValueSet");
        }

        public boolean subsetOf(GenSet<Value> genSet) {
            return SortedSetLike.Cclass.subsetOf(this, genSet);
        }

        public /* bridge */ /* synthetic */ scala.collection.Traversable thisCollection() {
            return thisCollection();
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [scala.collection.immutable.Set, scala.Enumeration$ValueSet] */
        public <B> Set<B> toSet() {
            return Set.Cclass.toSet(this);
        }
    }

    public Enumeration() {
        this(0);
    }

    public Enumeration(int i) {
        this.scala$Enumeration$$vmap = new HashMap();
        this.vset = null;
        this.scala$Enumeration$$vsetDefined = false;
        this.scala$Enumeration$$nmap = new HashMap();
        this.nextId = i;
        this.scala$Enumeration$$topId = i;
        this.scala$Enumeration$$bottomId = i >= 0 ? 0 : i;
    }

    private Enumeration$ValueOrdering$ ValueOrdering$lzycompute() {
        synchronized (this) {
            if (this.ValueOrdering$module == null) {
                this.ValueOrdering$module = new Enumeration$ValueOrdering$(this);
            }
        }
        return this.ValueOrdering$module;
    }

    private Enumeration$ValueSet$ ValueSet$lzycompute() {
        synchronized (this) {
            if (this.ValueSet$module == null) {
                this.ValueSet$module = new Enumeration$ValueSet$(this);
            }
        }
        return this.ValueSet$module;
    }

    public final Value Value() {
        return Value(nextId());
    }

    public final Value Value(int i) {
        return Value(i, scala$Enumeration$$nextNameOrNull());
    }

    public final Value Value(int i, String str) {
        return new Val(this, i, str);
    }

    public Enumeration$ValueOrdering$ ValueOrdering() {
        return this.ValueOrdering$module == null ? ValueOrdering$lzycompute() : this.ValueOrdering$module;
    }

    public Enumeration$ValueSet$ ValueSet() {
        return this.ValueSet$module == null ? ValueSet$lzycompute() : this.ValueSet$module;
    }

    public final Value apply(int i) {
        return scala$Enumeration$$vmap().apply(BoxesRunTime.boxToInteger(i));
    }

    public int nextId() {
        return this.nextId;
    }

    public void nextId_$eq(int i) {
        this.nextId = i;
    }

    public Iterator<String> nextName() {
        return this.nextName;
    }

    public int scala$Enumeration$$bottomId() {
        return this.scala$Enumeration$$bottomId;
    }

    public void scala$Enumeration$$bottomId_$eq(int i) {
        this.scala$Enumeration$$bottomId = i;
    }

    public final boolean scala$Enumeration$$isValDef$1(Method method, Field[] fieldArr) {
        return Predef$.MODULE$.refArrayOps((Object[]) fieldArr).exists(new Enumeration$$anonfun$scala$Enumeration$$isValDef$1$1(this, method));
    }

    public synchronized String scala$Enumeration$$nameOf(int i) {
        return (String) scala$Enumeration$$nmap().getOrElse(BoxesRunTime.boxToInteger(i), new Enumeration$$anonfun$scala$Enumeration$$nameOf$1(this, i));
    }

    public String scala$Enumeration$$nextNameOrNull() {
        if (nextName() == null || !nextName().hasNext()) {
            return null;
        }
        return nextName().next();
    }

    public Map<Object, String> scala$Enumeration$$nmap() {
        return this.scala$Enumeration$$nmap;
    }

    public void scala$Enumeration$$populateNameMap() {
        Predef$.MODULE$.refArrayOps((Object[]) ((Method[]) Predef$.MODULE$.refArrayOps((Object[]) getClass().getMethods()).filter(new Enumeration$$anonfun$1(this, getClass().getDeclaredFields())))).foreach(new Enumeration$$anonfun$scala$Enumeration$$populateNameMap$1(this));
    }

    public int scala$Enumeration$$topId() {
        return this.scala$Enumeration$$topId;
    }

    public void scala$Enumeration$$topId_$eq(int i) {
        this.scala$Enumeration$$topId = i;
    }

    public Map<Object, Value> scala$Enumeration$$vmap() {
        return this.scala$Enumeration$$vmap;
    }

    public void scala$Enumeration$$vsetDefined_$eq(boolean z) {
        this.scala$Enumeration$$vsetDefined = z;
    }

    public String toString() {
        Predef$ predef$ = Predef$.MODULE$;
        Predef$ predef$2 = Predef$.MODULE$;
        Predef$ predef$3 = Predef$.MODULE$;
        Predef$ predef$4 = Predef$.MODULE$;
        return (String) predef$.refArrayOps((Object[]) ((String) predef$2.refArrayOps((Object[]) new StringOps(new StringOps(getClass().getName()).stripSuffix(NameTransformer$.MODULE$.MODULE_SUFFIX_STRING())).split('.')).last()).split(Pattern.quote(NameTransformer$.MODULE$.NAME_JOIN_STRING()))).last();
    }
}
