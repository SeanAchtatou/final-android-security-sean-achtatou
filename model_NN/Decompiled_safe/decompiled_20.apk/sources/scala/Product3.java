package scala;

import scala.runtime.BoxesRunTime;

public interface Product3<T1, T2, T3> extends Product {

    /* renamed from: scala.Product3$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Product3 product3) {
        }

        public static int productArity(Product3 product3) {
            return 3;
        }

        public static Object productElement(Product3 product3, int i) {
            switch (i) {
                case 0:
                    return product3._1();
                case 1:
                    return product3._2();
                case 2:
                    return product3._3();
                default:
                    throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
            }
        }
    }

    T1 _1();

    T2 _2();

    T3 _3();
}
