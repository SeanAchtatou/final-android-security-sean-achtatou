package scala.reflect;

import scala.None$;
import scala.collection.immutable.Nil$;
import scala.runtime.BoxedUnit;
import scala.runtime.Nothing$;
import scala.runtime.Null$;

public final class ClassManifestFactory$ {
    public static final ClassManifestFactory$ MODULE$ = null;
    private final Manifest<Object> Any = ManifestFactory$.MODULE$.Any();
    private final Manifest<Object> AnyVal = ManifestFactory$.MODULE$.AnyVal();
    private final AnyValManifest<Object> Boolean = ManifestFactory$.MODULE$.Boolean();
    private final AnyValManifest<Object> Byte = ManifestFactory$.MODULE$.Byte();
    private final AnyValManifest<Object> Char = ManifestFactory$.MODULE$.Char();
    private final AnyValManifest<Object> Double = ManifestFactory$.MODULE$.Double();
    private final AnyValManifest<Object> Float = ManifestFactory$.MODULE$.Float();
    private final AnyValManifest<Object> Int = ManifestFactory$.MODULE$.Int();
    private final AnyValManifest<Object> Long = ManifestFactory$.MODULE$.Long();
    private final Manifest<Nothing$> Nothing = ManifestFactory$.MODULE$.Nothing();
    private final Manifest<Null$> Null = ManifestFactory$.MODULE$.Null();
    private final Manifest<Object> Object = ManifestFactory$.MODULE$.Object();
    private final AnyValManifest<Object> Short = ManifestFactory$.MODULE$.Short();
    private final AnyValManifest<BoxedUnit> Unit = ManifestFactory$.MODULE$.Unit();

    static {
        new ClassManifestFactory$();
    }

    private ClassManifestFactory$() {
        MODULE$ = this;
    }

    public final AnyValManifest<Object> Boolean() {
        return this.Boolean;
    }

    public final AnyValManifest<Object> Byte() {
        return this.Byte;
    }

    public final AnyValManifest<Object> Char() {
        return this.Char;
    }

    public final AnyValManifest<Object> Double() {
        return this.Double;
    }

    public final AnyValManifest<Object> Float() {
        return this.Float;
    }

    public final AnyValManifest<Object> Int() {
        return this.Int;
    }

    public final AnyValManifest<Object> Long() {
        return this.Long;
    }

    public final AnyValManifest<Object> Short() {
        return this.Short;
    }

    public final AnyValManifest<BoxedUnit> Unit() {
        return this.Unit;
    }

    public final <T> ClassTag<T> classType(Class<?> cls) {
        return new ClassTypeManifest(None$.MODULE$, cls, Nil$.MODULE$);
    }

    public final <T> ClassTag<T> fromClass(Class<T> cls) {
        Class cls2 = Byte.TYPE;
        if (cls2 != null ? cls2.equals(cls) : cls == null) {
            return Byte();
        }
        Class cls3 = Short.TYPE;
        if (cls3 != null ? cls3.equals(cls) : cls == null) {
            return Short();
        }
        Class cls4 = Character.TYPE;
        if (cls4 != null ? cls4.equals(cls) : cls == null) {
            return Char();
        }
        Class cls5 = Integer.TYPE;
        if (cls5 != null ? cls5.equals(cls) : cls == null) {
            return Int();
        }
        Class cls6 = Long.TYPE;
        if (cls6 != null ? cls6.equals(cls) : cls == null) {
            return Long();
        }
        Class cls7 = Float.TYPE;
        if (cls7 != null ? cls7.equals(cls) : cls == null) {
            return Float();
        }
        Class cls8 = Double.TYPE;
        if (cls8 != null ? cls8.equals(cls) : cls == null) {
            return Double();
        }
        Class cls9 = Boolean.TYPE;
        if (cls9 != null ? cls9.equals(cls) : cls == null) {
            return Boolean();
        }
        Class cls10 = Void.TYPE;
        return (cls10 != null ? !cls10.equals(cls) : cls != null) ? classType(cls) : Unit();
    }
}
