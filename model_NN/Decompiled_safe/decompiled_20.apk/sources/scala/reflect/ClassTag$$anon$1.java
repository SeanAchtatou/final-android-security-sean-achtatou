package scala.reflect;

import scala.collection.immutable.List;
import scala.reflect.ClassManifestDeprecatedApis;
import scala.reflect.ClassTag;

public final class ClassTag$$anon$1 implements ClassTag<T> {
    private final Class runtimeClass1$1;

    public ClassTag$$anon$1(Class cls) {
        this.runtimeClass1$1 = cls;
        ClassManifestDeprecatedApis.Cclass.$init$(this);
        ClassTag.Cclass.$init$(this);
    }

    public final boolean $less$colon$less(ClassTag<?> classTag) {
        return ClassManifestDeprecatedApis.Cclass.$less$colon$less(this, classTag);
    }

    public final String argString() {
        return ClassManifestDeprecatedApis.Cclass.argString(this);
    }

    public final boolean canEqual(Object obj) {
        return ClassTag.Cclass.canEqual(this, obj);
    }

    public final boolean equals(Object obj) {
        return ClassTag.Cclass.equals(this, obj);
    }

    public final Class<?> erasure() {
        return ClassManifestDeprecatedApis.Cclass.erasure(this);
    }

    public final int hashCode() {
        return ClassTag.Cclass.hashCode(this);
    }

    public final Object newArray(int i) {
        return ClassTag.Cclass.newArray(this, i);
    }

    public final Class<?> runtimeClass() {
        return this.runtimeClass1$1;
    }

    public final String toString() {
        return ClassTag.Cclass.toString(this);
    }

    public final List<OptManifest<?>> typeArguments() {
        return ClassManifestDeprecatedApis.Cclass.typeArguments(this);
    }
}
