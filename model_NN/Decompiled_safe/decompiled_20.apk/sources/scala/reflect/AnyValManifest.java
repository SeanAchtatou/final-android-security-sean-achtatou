package scala.reflect;

import scala.collection.immutable.List;
import scala.reflect.ClassManifestDeprecatedApis;
import scala.reflect.ClassTag;
import scala.reflect.Manifest;

public abstract class AnyValManifest<T> implements Manifest<T> {
    private final int hashCode = System.identityHashCode(this);
    private final String toString;

    public AnyValManifest(String str) {
        this.toString = str;
        ClassManifestDeprecatedApis.Cclass.$init$(this);
        ClassTag.Cclass.$init$(this);
        Manifest.Cclass.$init$(this);
    }

    public boolean $less$colon$less(ClassTag<?> classTag) {
        return classTag == this || classTag == package$.MODULE$.Manifest().Any() || classTag == package$.MODULE$.Manifest().AnyVal();
    }

    public String argString() {
        return ClassManifestDeprecatedApis.Cclass.argString(this);
    }

    public boolean canEqual(Object obj) {
        return obj instanceof AnyValManifest;
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public Class<?> erasure() {
        return ClassManifestDeprecatedApis.Cclass.erasure(this);
    }

    public int hashCode() {
        return this.hashCode;
    }

    public Object newArray(int i) {
        return ClassTag.Cclass.newArray(this, i);
    }

    public String toString() {
        return this.toString;
    }

    public List<Manifest<?>> typeArguments() {
        return Manifest.Cclass.typeArguments(this);
    }
}
