package scala.reflect;

public final class ManifestFactory$$anon$11 extends AnyValManifest<Object> {
    public ManifestFactory$$anon$11() {
        super("Float");
    }

    public final float[] newArray(int i) {
        return new float[i];
    }

    public final Class<Float> runtimeClass() {
        return Float.TYPE;
    }
}
