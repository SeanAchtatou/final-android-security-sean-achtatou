package scala.reflect;

public final class NameTransformer {

    public class OpCodes {
        private final String code;
        private final OpCodes next;
        private final char op;

        public OpCodes(char c, String str, OpCodes opCodes) {
            this.op = c;
            this.code = str;
            this.next = opCodes;
        }
    }
}
