package scala.reflect;

public final class ManifestFactory$$anon$6 extends AnyValManifest<Object> {
    public ManifestFactory$$anon$6() {
        super("Byte");
    }

    public final byte[] newArray(int i) {
        return new byte[i];
    }

    public final Class<Byte> runtimeClass() {
        return Byte.TYPE;
    }
}
