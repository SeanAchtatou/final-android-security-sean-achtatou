package scala.reflect;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.None$;
import scala.Option;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ClassManifestDeprecatedApis;
import scala.reflect.ClassTag;
import scala.reflect.Manifest;

public final class ManifestFactory {

    public class ClassTypeManifest<T> implements Manifest<T> {
        private final Option<Manifest<?>> prefix;
        private final Class<?> runtimeClass;
        private final List<Manifest<?>> typeArguments;

        public ClassTypeManifest(Option<Manifest<?>> option, Class<?> cls, List<Manifest<?>> list) {
            this.prefix = option;
            this.runtimeClass = cls;
            this.typeArguments = list;
            ClassManifestDeprecatedApis.Cclass.$init$(this);
            ClassTag.Cclass.$init$(this);
            Manifest.Cclass.$init$(this);
        }

        public boolean $less$colon$less(ClassTag<?> classTag) {
            return ClassManifestDeprecatedApis.Cclass.$less$colon$less(this, classTag);
        }

        public String argString() {
            return ClassManifestDeprecatedApis.Cclass.argString(this);
        }

        public boolean canEqual(Object obj) {
            return Manifest.Cclass.canEqual(this, obj);
        }

        public boolean equals(Object obj) {
            return Manifest.Cclass.equals(this, obj);
        }

        public Class<?> erasure() {
            return ClassManifestDeprecatedApis.Cclass.erasure(this);
        }

        public int hashCode() {
            return Manifest.Cclass.hashCode(this);
        }

        public Object newArray(int i) {
            return ClassTag.Cclass.newArray(this, i);
        }

        public Class<?> runtimeClass() {
            return this.runtimeClass;
        }

        /* JADX WARN: Type inference failed for: r3v0, types: [scala.reflect.ClassManifestDeprecatedApis, scala.reflect.ManifestFactory$ClassTypeManifest] */
        public String toString() {
            return new StringBuilder().append((Object) (this.prefix.isEmpty() ? PoiTypeDef.All : new StringBuilder().append((Object) this.prefix.get().toString()).append((Object) "#").toString())).append((Object) (erasure().isArray() ? "Array" : erasure().getName())).append((Object) argString()).toString();
        }

        public List<Manifest<?>> typeArguments() {
            return this.typeArguments;
        }
    }

    public abstract class PhantomManifest<T> extends ClassTypeManifest<T> {
        private final int hashCode = System.identityHashCode(this);
        private final String toString;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public PhantomManifest(Class<?> cls, String str) {
            super(None$.MODULE$, cls, Nil$.MODULE$);
            this.toString = str;
        }

        public boolean equals(Object obj) {
            return this == obj;
        }

        public int hashCode() {
            return this.hashCode;
        }

        public String toString() {
            return this.toString;
        }
    }
}
