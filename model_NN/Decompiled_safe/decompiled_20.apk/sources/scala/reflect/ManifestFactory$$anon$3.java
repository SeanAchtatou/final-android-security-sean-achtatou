package scala.reflect;

import scala.reflect.ManifestFactory;

public final class ManifestFactory$$anon$3 extends ManifestFactory.PhantomManifest<Object> {
    public ManifestFactory$$anon$3() {
        super(ManifestFactory$.MODULE$.scala$reflect$ManifestFactory$$ObjectTYPE(), "AnyVal");
    }

    public final boolean $less$colon$less(ClassTag<?> classTag) {
        return classTag == this || classTag == ManifestFactory$.MODULE$.Any();
    }

    public final Object[] newArray(int i) {
        return new Object[i];
    }
}
