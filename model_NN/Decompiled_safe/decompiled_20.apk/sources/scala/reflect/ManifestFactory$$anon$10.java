package scala.reflect;

public final class ManifestFactory$$anon$10 extends AnyValManifest<Object> {
    public ManifestFactory$$anon$10() {
        super("Long");
    }

    public final long[] newArray(int i) {
        return new long[i];
    }

    public final Class<Long> runtimeClass() {
        return Long.TYPE;
    }
}
