package scala.reflect;

import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.runtime.ScalaRunTime$;

public interface Manifest<T> extends ClassTag<T> {

    /* renamed from: scala.reflect.Manifest$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(Manifest manifest) {
        }

        public static boolean canEqual(Manifest manifest, Object obj) {
            return obj instanceof Manifest;
        }

        public static boolean equals(Manifest manifest, Object obj) {
            if (!(obj instanceof Manifest)) {
                return false;
            }
            Manifest manifest2 = (Manifest) obj;
            if (!manifest2.canEqual(manifest)) {
                return false;
            }
            Class erasure = manifest.erasure();
            Class erasure2 = manifest2.erasure();
            if (erasure == null) {
                if (erasure2 != null) {
                    return false;
                }
            } else if (!erasure.equals(erasure2)) {
                return false;
            }
            return manifest.$less$colon$less(manifest2) && manifest2.$less$colon$less(manifest);
        }

        public static int hashCode(Manifest manifest) {
            return ScalaRunTime$.MODULE$.hash(manifest.erasure());
        }

        public static List typeArguments(Manifest manifest) {
            return Nil$.MODULE$;
        }
    }

    boolean canEqual(Object obj);
}
