package scala.reflect;

import scala.runtime.BoxedUnit;
import scala.runtime.Nothing$;
import scala.runtime.Null$;

public final class ManifestFactory$ {
    public static final ManifestFactory$ MODULE$ = null;
    private final Manifest<Object> Any = new ManifestFactory$$anon$1();
    private final Manifest<Object> AnyRef = Object();
    private final Manifest<Object> AnyVal = new ManifestFactory$$anon$3();
    private final AnyValManifest<Object> Boolean = new ManifestFactory$$anon$13();
    private final AnyValManifest<Object> Byte = new ManifestFactory$$anon$6();
    private final AnyValManifest<Object> Char = new ManifestFactory$$anon$8();
    private final AnyValManifest<Object> Double = new ManifestFactory$$anon$12();
    private final AnyValManifest<Object> Float = new ManifestFactory$$anon$11();
    private final AnyValManifest<Object> Int = new ManifestFactory$$anon$9();
    private final AnyValManifest<Object> Long = new ManifestFactory$$anon$10();
    private final Manifest<Nothing$> Nothing = new ManifestFactory$$anon$5();
    private final Manifest<Null$> Null = new ManifestFactory$$anon$4();
    private final Manifest<Object> Object = new ManifestFactory$$anon$2();
    private final AnyValManifest<Object> Short = new ManifestFactory$$anon$7();
    private final AnyValManifest<BoxedUnit> Unit = new ManifestFactory$$anon$14();
    private final Class<Nothing$> scala$reflect$ManifestFactory$$NothingTYPE = Nothing$.class;
    private final Class<Null$> scala$reflect$ManifestFactory$$NullTYPE = Null$.class;
    private final Class<Object> scala$reflect$ManifestFactory$$ObjectTYPE = Object.class;

    static {
        new ManifestFactory$();
    }

    private ManifestFactory$() {
        MODULE$ = this;
    }

    public final Manifest<Object> Any() {
        return this.Any;
    }

    public final Manifest<Object> AnyRef() {
        return this.AnyRef;
    }

    public final Manifest<Object> AnyVal() {
        return this.AnyVal;
    }

    public final AnyValManifest<Object> Boolean() {
        return this.Boolean;
    }

    public final AnyValManifest<Object> Byte() {
        return this.Byte;
    }

    public final AnyValManifest<Object> Char() {
        return this.Char;
    }

    public final AnyValManifest<Object> Double() {
        return this.Double;
    }

    public final AnyValManifest<Object> Float() {
        return this.Float;
    }

    public final AnyValManifest<Object> Int() {
        return this.Int;
    }

    public final AnyValManifest<Object> Long() {
        return this.Long;
    }

    public final Manifest<Nothing$> Nothing() {
        return this.Nothing;
    }

    public final Manifest<Null$> Null() {
        return this.Null;
    }

    public final Manifest<Object> Object() {
        return this.Object;
    }

    public final AnyValManifest<Object> Short() {
        return this.Short;
    }

    public final AnyValManifest<BoxedUnit> Unit() {
        return this.Unit;
    }

    public final Class<Nothing$> scala$reflect$ManifestFactory$$NothingTYPE() {
        return this.scala$reflect$ManifestFactory$$NothingTYPE;
    }

    public final Class<Null$> scala$reflect$ManifestFactory$$NullTYPE() {
        return this.scala$reflect$ManifestFactory$$NullTYPE;
    }

    public final Class<Object> scala$reflect$ManifestFactory$$ObjectTYPE() {
        return this.scala$reflect$ManifestFactory$$ObjectTYPE;
    }
}
