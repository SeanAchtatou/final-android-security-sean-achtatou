package scala.reflect;

import scala.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

public final class ClassManifestDeprecatedApis$$anonfun$subargs$1 extends AbstractFunction2<OptManifest<?>, OptManifest<?>, Object> implements Serializable {
    public ClassManifestDeprecatedApis$$anonfun$subargs$1(ClassTag<T> classTag) {
    }

    public final /* synthetic */ Object apply(Object obj, Object obj2) {
        return BoxesRunTime.boxToBoolean(apply((OptManifest<?>) ((OptManifest) obj), (OptManifest<?>) ((OptManifest) obj2)));
    }

    public final boolean apply(OptManifest<?> optManifest, OptManifest<?> optManifest2) {
        Tuple2 tuple2 = new Tuple2(optManifest, optManifest2);
        if (tuple2._1() instanceof ClassTag) {
            ClassTag classTag = (ClassTag) tuple2._1();
            if (tuple2._2() instanceof ClassTag) {
                return classTag.$less$colon$less((ClassTag) tuple2._2());
            }
        }
        return tuple2._1() == NoManifest$.MODULE$ && tuple2._2() == NoManifest$.MODULE$;
    }
}
