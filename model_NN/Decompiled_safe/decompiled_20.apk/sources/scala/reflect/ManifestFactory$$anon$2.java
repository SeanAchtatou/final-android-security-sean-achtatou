package scala.reflect;

import scala.reflect.ManifestFactory;

public final class ManifestFactory$$anon$2 extends ManifestFactory.PhantomManifest<Object> {
    public ManifestFactory$$anon$2() {
        super(ManifestFactory$.MODULE$.scala$reflect$ManifestFactory$$ObjectTYPE(), "Object");
    }

    public final boolean $less$colon$less(ClassTag<?> classTag) {
        return classTag == this || classTag == ManifestFactory$.MODULE$.Any();
    }

    public final Object[] newArray(int i) {
        return new Object[i];
    }
}
