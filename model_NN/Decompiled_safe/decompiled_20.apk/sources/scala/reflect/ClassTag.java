package scala.reflect;

import java.lang.reflect.Array;
import scala.Equals;
import scala.Predef$;
import scala.Serializable;
import scala.StringContext;
import scala.runtime.BoxedUnit;
import scala.runtime.ScalaRunTime$;

public interface ClassTag<T> extends Equals, Serializable {

    /* renamed from: scala.reflect.ClassTag$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(ClassTag classTag) {
        }

        public static boolean canEqual(ClassTag classTag, Object obj) {
            return obj instanceof ClassTag;
        }

        public static boolean equals(ClassTag classTag, Object obj) {
            if (obj instanceof ClassTag) {
                Class<?> runtimeClass = classTag.runtimeClass();
                Class<?> runtimeClass2 = ((ClassTag) obj).runtimeClass();
                return runtimeClass != null ? runtimeClass.equals(runtimeClass2) : runtimeClass2 == null;
            }
        }

        public static int hashCode(ClassTag classTag) {
            return ScalaRunTime$.MODULE$.hash(classTag.runtimeClass());
        }

        public static Object newArray(ClassTag classTag, int i) {
            Class<?> runtimeClass = classTag.runtimeClass();
            Class cls = Byte.TYPE;
            if (cls != null ? cls.equals(runtimeClass) : runtimeClass == null) {
                return new byte[i];
            }
            Class cls2 = Short.TYPE;
            if (cls2 != null ? cls2.equals(runtimeClass) : runtimeClass == null) {
                return new short[i];
            }
            Class cls3 = Character.TYPE;
            if (cls3 != null ? cls3.equals(runtimeClass) : runtimeClass == null) {
                return new char[i];
            }
            Class cls4 = Integer.TYPE;
            if (cls4 != null ? cls4.equals(runtimeClass) : runtimeClass == null) {
                return new int[i];
            }
            Class cls5 = Long.TYPE;
            if (cls5 != null ? cls5.equals(runtimeClass) : runtimeClass == null) {
                return new long[i];
            }
            Class cls6 = Float.TYPE;
            if (cls6 != null ? cls6.equals(runtimeClass) : runtimeClass == null) {
                return new float[i];
            }
            Class cls7 = Double.TYPE;
            if (cls7 != null ? cls7.equals(runtimeClass) : runtimeClass == null) {
                return new double[i];
            }
            Class cls8 = Boolean.TYPE;
            if (cls8 != null ? cls8.equals(runtimeClass) : runtimeClass == null) {
                return new boolean[i];
            }
            Class cls9 = Void.TYPE;
            return (cls9 != null ? !cls9.equals(runtimeClass) : runtimeClass != null) ? Array.newInstance(classTag.runtimeClass(), i) : new BoxedUnit[i];
        }

        private static final String prettyprint$1(ClassTag classTag, Class cls) {
            if (!cls.isArray()) {
                return cls.getName();
            }
            return new StringContext(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"Array[", "]"})).s(Predef$.MODULE$.genericWrapArray(new Object[]{prettyprint$1(classTag, ScalaRunTime$.MODULE$.arrayElementClass(cls))}));
        }

        public static String toString(ClassTag classTag) {
            return prettyprint$1(classTag, classTag.runtimeClass());
        }
    }

    Object newArray(int i);

    Class<?> runtimeClass();

    String toString();
}
