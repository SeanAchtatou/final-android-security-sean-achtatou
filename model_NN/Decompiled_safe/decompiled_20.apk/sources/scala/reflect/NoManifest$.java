package scala.reflect;

import scala.runtime.Nothing$;

public final class NoManifest$ implements OptManifest<Nothing$> {
    public static final NoManifest$ MODULE$ = null;

    static {
        new NoManifest$();
    }

    private NoManifest$() {
        MODULE$ = this;
    }

    public final String toString() {
        return "<?>";
    }
}
