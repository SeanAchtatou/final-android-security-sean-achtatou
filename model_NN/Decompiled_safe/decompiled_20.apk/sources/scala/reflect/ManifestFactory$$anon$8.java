package scala.reflect;

public final class ManifestFactory$$anon$8 extends AnyValManifest<Object> {
    public ManifestFactory$$anon$8() {
        super("Char");
    }

    public final char[] newArray(int i) {
        return new char[i];
    }

    public final Class<Character> runtimeClass() {
        return Character.TYPE;
    }
}
