package scala.reflect;

import scala.reflect.ManifestFactory;
import scala.runtime.Null$;

public final class ManifestFactory$$anon$4 extends ManifestFactory.PhantomManifest<Null$> {
    public ManifestFactory$$anon$4() {
        super(ManifestFactory$.MODULE$.scala$reflect$ManifestFactory$$NullTYPE(), "Null");
    }

    public final boolean $less$colon$less(ClassTag<?> classTag) {
        return (classTag == null || classTag == ManifestFactory$.MODULE$.Nothing() || classTag.$less$colon$less(ManifestFactory$.MODULE$.AnyVal())) ? false : true;
    }

    public final Object[] newArray(int i) {
        return new Object[i];
    }
}
