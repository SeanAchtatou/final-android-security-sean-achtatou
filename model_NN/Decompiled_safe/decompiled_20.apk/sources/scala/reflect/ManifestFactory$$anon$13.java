package scala.reflect;

public final class ManifestFactory$$anon$13 extends AnyValManifest<Object> {
    public ManifestFactory$$anon$13() {
        super("Boolean");
    }

    public final boolean[] newArray(int i) {
        return new boolean[i];
    }

    public final Class<Boolean> runtimeClass() {
        return Boolean.TYPE;
    }
}
