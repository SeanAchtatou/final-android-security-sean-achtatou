package scala.reflect;

import scala.runtime.BoxedUnit;

public final class ManifestFactory$$anon$14 extends AnyValManifest<BoxedUnit> {
    public ManifestFactory$$anon$14() {
        super("Unit");
    }

    public final BoxedUnit[] newArray(int i) {
        return new BoxedUnit[i];
    }

    public final Class<Void> runtimeClass() {
        return Void.TYPE;
    }
}
