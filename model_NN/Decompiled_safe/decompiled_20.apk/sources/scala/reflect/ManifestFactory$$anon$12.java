package scala.reflect;

public final class ManifestFactory$$anon$12 extends AnyValManifest<Object> {
    public ManifestFactory$$anon$12() {
        super("Double");
    }

    public final double[] newArray(int i) {
        return new double[i];
    }

    public final Class<Double> runtimeClass() {
        return Double.TYPE;
    }
}
