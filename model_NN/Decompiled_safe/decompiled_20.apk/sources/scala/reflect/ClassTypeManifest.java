package scala.reflect;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Option;
import scala.collection.immutable.List;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ClassManifestDeprecatedApis;
import scala.reflect.ClassTag;

public class ClassTypeManifest<T> implements ClassTag<T> {
    private final Option<OptManifest<?>> prefix;
    private final Class<?> runtimeClass;
    private final List<OptManifest<?>> typeArguments;

    public ClassTypeManifest(Option<OptManifest<?>> option, Class<?> cls, List<OptManifest<?>> list) {
        this.prefix = option;
        this.runtimeClass = cls;
        this.typeArguments = list;
        ClassManifestDeprecatedApis.Cclass.$init$(this);
        ClassTag.Cclass.$init$(this);
    }

    public boolean $less$colon$less(ClassTag<?> classTag) {
        return ClassManifestDeprecatedApis.Cclass.$less$colon$less(this, classTag);
    }

    public String argString() {
        return ClassManifestDeprecatedApis.Cclass.argString(this);
    }

    public boolean canEqual(Object obj) {
        return ClassTag.Cclass.canEqual(this, obj);
    }

    public boolean equals(Object obj) {
        return ClassTag.Cclass.equals(this, obj);
    }

    public Class<?> erasure() {
        return ClassManifestDeprecatedApis.Cclass.erasure(this);
    }

    public int hashCode() {
        return ClassTag.Cclass.hashCode(this);
    }

    public Object newArray(int i) {
        return ClassTag.Cclass.newArray(this, i);
    }

    public Class<?> runtimeClass() {
        return this.runtimeClass;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.reflect.ClassManifestDeprecatedApis, scala.reflect.ClassTypeManifest] */
    public String toString() {
        return new StringBuilder().append((Object) (this.prefix.isEmpty() ? PoiTypeDef.All : new StringBuilder().append((Object) this.prefix.get().toString()).append((Object) "#").toString())).append((Object) (erasure().isArray() ? "Array" : erasure().getName())).append((Object) argString()).toString();
    }

    public List<OptManifest<?>> typeArguments() {
        return this.typeArguments;
    }
}
