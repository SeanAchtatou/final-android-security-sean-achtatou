package scala.reflect;

public final class ManifestFactory$$anon$9 extends AnyValManifest<Object> {
    public ManifestFactory$$anon$9() {
        super("Int");
    }

    public final int[] newArray(int i) {
        return new int[i];
    }

    public final Class<Integer> runtimeClass() {
        return Integer.TYPE;
    }
}
