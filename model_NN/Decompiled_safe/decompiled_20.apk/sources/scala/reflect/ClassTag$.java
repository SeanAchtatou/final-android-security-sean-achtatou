package scala.reflect;

import scala.Serializable;
import scala.runtime.BoxedUnit;
import scala.runtime.Nothing$;
import scala.runtime.Null$;

public final class ClassTag$ implements Serializable {
    public static final ClassTag$ MODULE$ = null;
    private final ClassTag<Object> Any = package$.MODULE$.Manifest().Any();
    private final ClassTag<Object> AnyRef = package$.MODULE$.Manifest().AnyRef();
    private final ClassTag<Object> AnyVal = package$.MODULE$.Manifest().AnyVal();
    private final ClassTag<Object> Boolean = package$.MODULE$.Manifest().Boolean();
    private final ClassTag<Object> Byte = package$.MODULE$.Manifest().Byte();
    private final ClassTag<Object> Char = package$.MODULE$.Manifest().Char();
    private final ClassTag<Object> Double = package$.MODULE$.Manifest().Double();
    private final ClassTag<Object> Float = package$.MODULE$.Manifest().Float();
    private final ClassTag<Object> Int = package$.MODULE$.Manifest().Int();
    private final ClassTag<Object> Long = package$.MODULE$.Manifest().Long();
    private final ClassTag<Nothing$> Nothing = package$.MODULE$.Manifest().Nothing();
    private final Class<Nothing$> NothingTYPE = Nothing$.class;
    private final ClassTag<Null$> Null = package$.MODULE$.Manifest().Null();
    private final Class<Null$> NullTYPE = Null$.class;
    private final ClassTag<Object> Object = package$.MODULE$.Manifest().Object();
    private final Class<Object> ObjectTYPE = Object.class;
    private final ClassTag<Object> Short = package$.MODULE$.Manifest().Short();
    private final ClassTag<BoxedUnit> Unit = package$.MODULE$.Manifest().Unit();

    static {
        new ClassTag$();
    }

    private ClassTag$() {
        MODULE$ = this;
    }

    private Class<Nothing$> NothingTYPE() {
        return this.NothingTYPE;
    }

    private Class<Null$> NullTYPE() {
        return this.NullTYPE;
    }

    private Class<Object> ObjectTYPE() {
        return this.ObjectTYPE;
    }

    public final ClassTag<Object> AnyRef() {
        return this.AnyRef;
    }

    public final ClassTag<Object> Boolean() {
        return this.Boolean;
    }

    public final ClassTag<Object> Byte() {
        return this.Byte;
    }

    public final ClassTag<Object> Char() {
        return this.Char;
    }

    public final ClassTag<Object> Double() {
        return this.Double;
    }

    public final ClassTag<Object> Float() {
        return this.Float;
    }

    public final ClassTag<Object> Int() {
        return this.Int;
    }

    public final ClassTag<Object> Long() {
        return this.Long;
    }

    public final ClassTag<Nothing$> Nothing() {
        return this.Nothing;
    }

    public final ClassTag<Null$> Null() {
        return this.Null;
    }

    public final ClassTag<Object> Object() {
        return this.Object;
    }

    public final ClassTag<Object> Short() {
        return this.Short;
    }

    public final ClassTag<BoxedUnit> Unit() {
        return this.Unit;
    }

    public final <T> ClassTag<T> apply(Class<?> cls) {
        Class cls2 = Byte.TYPE;
        if (cls2 != null ? cls2.equals(cls) : cls == null) {
            return Byte();
        }
        Class cls3 = Short.TYPE;
        if (cls3 != null ? cls3.equals(cls) : cls == null) {
            return Short();
        }
        Class cls4 = Character.TYPE;
        if (cls4 != null ? cls4.equals(cls) : cls == null) {
            return Char();
        }
        Class cls5 = Integer.TYPE;
        if (cls5 != null ? cls5.equals(cls) : cls == null) {
            return Int();
        }
        Class cls6 = Long.TYPE;
        if (cls6 != null ? cls6.equals(cls) : cls == null) {
            return Long();
        }
        Class cls7 = Float.TYPE;
        if (cls7 != null ? cls7.equals(cls) : cls == null) {
            return Float();
        }
        Class cls8 = Double.TYPE;
        if (cls8 != null ? cls8.equals(cls) : cls == null) {
            return Double();
        }
        Class cls9 = Boolean.TYPE;
        if (cls9 != null ? cls9.equals(cls) : cls == null) {
            return Boolean();
        }
        Class cls10 = Void.TYPE;
        if (cls10 != null ? cls10.equals(cls) : cls == null) {
            return Unit();
        }
        Class<Object> ObjectTYPE2 = ObjectTYPE();
        if (ObjectTYPE2 != null ? ObjectTYPE2.equals(cls) : cls == null) {
            return Object();
        }
        Class<Nothing$> NothingTYPE2 = NothingTYPE();
        if (NothingTYPE2 != null ? NothingTYPE2.equals(cls) : cls == null) {
            return Nothing();
        }
        Class<Null$> NullTYPE2 = NullTYPE();
        return (NullTYPE2 != null ? !NullTYPE2.equals(cls) : cls != null) ? new ClassTag$$anon$1(cls) : Null();
    }
}
