package scala.reflect;

import scala.reflect.ManifestFactory;

public final class ManifestFactory$$anon$1 extends ManifestFactory.PhantomManifest<Object> {
    public ManifestFactory$$anon$1() {
        super(ManifestFactory$.MODULE$.scala$reflect$ManifestFactory$$ObjectTYPE(), "Any");
    }

    public final boolean $less$colon$less(ClassTag<?> classTag) {
        return classTag == this;
    }

    public final Object[] newArray(int i) {
        return new Object[i];
    }
}
