package scala.reflect;

import com.amap.api.search.poisearch.PoiTypeDef;
import scala.Option$;
import scala.Predef$;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Set;
import scala.collection.mutable.StringBuilder;

public interface ClassManifestDeprecatedApis<T> extends OptManifest<T> {

    /* renamed from: scala.reflect.ClassManifestDeprecatedApis$class  reason: invalid class name */
    public abstract class Cclass {
        public static void $init$(ClassTag classTag) {
        }

        public static boolean $less$colon$less(ClassTag classTag, ClassTag classTag2) {
            if (!cannotMatch$1(classTag, classTag2)) {
                Class erasure = classTag.erasure();
                Class erasure2 = classTag2.erasure();
                if ((erasure != null ? !erasure.equals(erasure2) : erasure2 != null) ? classTag2.typeArguments().isEmpty() && subtype(classTag, classTag.erasure(), classTag2.erasure()) : subargs(classTag, classTag.typeArguments(), classTag2.typeArguments())) {
                    return true;
                }
            }
            return false;
        }

        public static String argString(ClassTag classTag) {
            return classTag.typeArguments().nonEmpty() ? classTag.typeArguments().mkString("[", ", ", "]") : classTag.erasure().isArray() ? new StringBuilder().append((Object) "[").append(package$.MODULE$.ClassManifest().fromClass(classTag.erasure().getComponentType())).append((Object) "]").toString() : PoiTypeDef.All;
        }

        private static final boolean cannotMatch$1(ClassTag classTag, ClassTag classTag2) {
            return (classTag2 instanceof AnyValManifest) || classTag2 == package$.MODULE$.Manifest().AnyVal() || classTag2 == package$.MODULE$.Manifest().Nothing() || classTag2 == package$.MODULE$.Manifest().Null();
        }

        public static Class erasure(ClassTag classTag) {
            return classTag.runtimeClass();
        }

        private static final boolean loop$1(ClassTag classTag, Set set, Set set2, Class cls) {
            while (set.nonEmpty()) {
                Class cls2 = (Class) set.head();
                Set set3 = (Set) Predef$.MODULE$.refArrayOps((Object[]) cls2.getInterfaces()).toSet().$plus$plus(Option$.MODULE$.option2Iterable(Option$.MODULE$.apply(cls2.getSuperclass())));
                if (set3.apply((Object) cls)) {
                    return true;
                }
                set2 = (Set) set2.$plus(cls2);
                set = (Set) ((Set) set.$plus$plus(set3).filterNot(set2)).$minus(cls2);
            }
            return false;
        }

        private static boolean subargs(ClassTag classTag, List list, List list2) {
            return list.corresponds(list2, new ClassManifestDeprecatedApis$$anonfun$subargs$1(classTag));
        }

        private static boolean subtype(ClassTag classTag, Class cls, Class cls2) {
            return loop$1(classTag, (Set) Predef$.MODULE$.Set().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Class[]{cls})), (Set) Predef$.MODULE$.Set().apply(Nil$.MODULE$), cls2);
        }

        public static List typeArguments(ClassTag classTag) {
            return Nil$.MODULE$;
        }
    }

    boolean $less$colon$less(ClassTag<?> classTag);

    String argString();

    Class<?> erasure();

    List<OptManifest<?>> typeArguments();
}
