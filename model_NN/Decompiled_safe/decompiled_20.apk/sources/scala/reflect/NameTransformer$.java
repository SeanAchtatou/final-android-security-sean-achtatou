package scala.reflect;

import scala.reflect.NameTransformer;
import scala.sys.package$;

public final class NameTransformer$ {
    public static final NameTransformer$ MODULE$ = null;
    private final String MODULE_INSTANCE_NAME = "MODULE$";
    private final String MODULE_SUFFIX_STRING = ((String) package$.MODULE$.props().getOrElse("SCALA_MODULE_SUFFIX_STRING", new NameTransformer$$anonfun$1()));
    private final String NAME_JOIN_STRING = ((String) package$.MODULE$.props().getOrElse("SCALA_NAME_JOIN_STRING", new NameTransformer$$anonfun$2()));
    private final NameTransformer.OpCodes[] code2op = new NameTransformer.OpCodes[ncodes()];
    private final int ncodes = 676;
    private final int nops = 128;
    private final String[] op2code = new String[nops()];

    static {
        new NameTransformer$();
    }

    private NameTransformer$() {
        MODULE$ = this;
        enterOp('~', "$tilde");
        enterOp('=', "$eq");
        enterOp('<', "$less");
        enterOp('>', "$greater");
        enterOp('!', "$bang");
        enterOp('#', "$hash");
        enterOp('%', "$percent");
        enterOp('^', "$up");
        enterOp('&', "$amp");
        enterOp('|', "$bar");
        enterOp('*', "$times");
        enterOp('/', "$div");
        enterOp('+', "$plus");
        enterOp('-', "$minus");
        enterOp(':', "$colon");
        enterOp('\\', "$bslash");
        enterOp('?', "$qmark");
        enterOp('@', "$at");
    }

    private NameTransformer.OpCodes[] code2op() {
        return this.code2op;
    }

    private void enterOp(char c, String str) {
        op2code()[c] = str;
        int charAt = (((str.charAt(1) - 'a') * 26) + str.charAt(2)) - 97;
        code2op()[charAt] = new NameTransformer.OpCodes(c, str, code2op()[charAt]);
    }

    private int ncodes() {
        return this.ncodes;
    }

    private int nops() {
        return this.nops;
    }

    private String[] op2code() {
        return this.op2code;
    }

    public final String MODULE_SUFFIX_STRING() {
        return this.MODULE_SUFFIX_STRING;
    }

    public final String NAME_JOIN_STRING() {
        return this.NAME_JOIN_STRING;
    }
}
