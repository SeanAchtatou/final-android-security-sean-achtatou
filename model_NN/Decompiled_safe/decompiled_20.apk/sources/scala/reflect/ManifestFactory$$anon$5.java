package scala.reflect;

import scala.reflect.ManifestFactory;
import scala.runtime.Nothing$;

public final class ManifestFactory$$anon$5 extends ManifestFactory.PhantomManifest<Nothing$> {
    public ManifestFactory$$anon$5() {
        super(ManifestFactory$.MODULE$.scala$reflect$ManifestFactory$$NothingTYPE(), "Nothing");
    }

    public final boolean $less$colon$less(ClassTag<?> classTag) {
        return classTag != null;
    }

    public final Object[] newArray(int i) {
        return new Object[i];
    }
}
