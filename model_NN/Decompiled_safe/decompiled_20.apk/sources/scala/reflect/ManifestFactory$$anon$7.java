package scala.reflect;

public final class ManifestFactory$$anon$7 extends AnyValManifest<Object> {
    public ManifestFactory$$anon$7() {
        super("Short");
    }

    public final short[] newArray(int i) {
        return new short[i];
    }

    public final Class<Short> runtimeClass() {
        return Short.TYPE;
    }
}
