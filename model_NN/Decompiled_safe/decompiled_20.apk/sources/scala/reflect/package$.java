package scala.reflect;

public final class package$ {
    public static final package$ MODULE$ = null;
    private final ClassManifestFactory$ ClassManifest = ClassManifestFactory$.MODULE$;
    private final ManifestFactory$ Manifest = ManifestFactory$.MODULE$;

    static {
        new package$();
    }

    private package$() {
        MODULE$ = this;
    }

    public final ClassManifestFactory$ ClassManifest() {
        return this.ClassManifest;
    }

    public final ManifestFactory$ Manifest() {
        return this.Manifest;
    }
}
