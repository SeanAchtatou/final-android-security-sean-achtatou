package scala;

import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.List$;
import scala.collection.immutable.Map$;
import scala.collection.immutable.Set$;
import scala.collection.mutable.ArrayOps;
import scala.reflect.ClassManifestFactory$;
import scala.reflect.ManifestFactory$;
import scala.reflect.NoManifest$;
import scala.reflect.package$;
import scala.xml.TopScope$;

public final class Predef$ extends LowPriorityImplicits {
    public static final Predef$ MODULE$ = null;
    private final TopScope$ $scope = TopScope$.MODULE$;
    private final ClassManifestFactory$ ClassManifest = package$.MODULE$.ClassManifest();
    private final ManifestFactory$ Manifest = package$.MODULE$.Manifest();
    private final Map$ Map = Map$.MODULE$;
    private final NoManifest$ NoManifest = NoManifest$.MODULE$;
    private final Set$ Set = Set$.MODULE$;
    private final CanBuildFrom<String, Object, String> StringCanBuildFrom = new Predef$$anon$3();
    public final Predef$$eq$colon$eq<Object, Object> scala$Predef$$singleton_$eq$colon$eq = new Predef$$anon$2();
    private final Predef$$less$colon$less<Object, Object> singleton_$less$colon$less = new Predef$$anon$1();

    static {
        new Predef$();
    }

    private Predef$() {
        MODULE$ = this;
        package$ package_ = package$.MODULE$;
        List$ list$ = List$.MODULE$;
    }

    public final Map$ Map() {
        return this.Map;
    }

    public final Set$ Set() {
        return this.Set;
    }

    public final <A> A any2ArrowAssoc(A a) {
        return a;
    }

    public final Object any2stringadd(Object obj) {
        return obj;
    }

    /* renamed from: assert  reason: not valid java name */
    public final void m0assert(boolean z) {
        if (!z) {
            throw new AssertionError("assertion failed");
        }
    }

    public final <A> Predef$$less$colon$less<A, A> conforms() {
        return this.singleton_$less$colon$less;
    }

    public final void println(Object obj) {
        Console$.MODULE$.println(obj);
    }

    public final <T> ArrayOps<T> refArrayOps(T[] tArr) {
        return new ArrayOps.ofRef(tArr);
    }

    public final void require(boolean z) {
        if (!z) {
            throw new IllegalArgumentException("requirement failed");
        }
    }
}
