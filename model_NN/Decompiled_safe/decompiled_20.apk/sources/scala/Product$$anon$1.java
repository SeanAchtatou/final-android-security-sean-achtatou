package scala;

import scala.collection.AbstractIterator;

public final class Product$$anon$1 extends AbstractIterator<Object> {
    private final /* synthetic */ Product $outer;
    private int c;
    private final int cmax;

    public Product$$anon$1(Product product) {
        if (product == null) {
            throw new NullPointerException();
        }
        this.$outer = product;
        this.c = 0;
        this.cmax = product.productArity();
    }

    private int c() {
        return this.c;
    }

    private void c_$eq(int i) {
        this.c = i;
    }

    private int cmax() {
        return this.cmax;
    }

    public final boolean hasNext() {
        return c() < cmax();
    }

    public final Object next() {
        Object productElement = this.$outer.productElement(c());
        this.c = c() + 1;
        return productElement;
    }
}
