package scala;

import scala.Product;
import scala.collection.Iterator;
import scala.collection.immutable.C$colon$colon;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.runtime.BoxesRunTime;

public abstract class Option<A> implements Product, Serializable {

    public class WithFilter {
        public final /* synthetic */ Option $outer;
        public final Function1<A, Object> scala$Option$WithFilter$$p;

        public WithFilter(Option<A> option, Function1<A, Object> function1) {
            this.scala$Option$WithFilter$$p = function1;
            if (option == null) {
                throw new NullPointerException();
            }
            this.$outer = option;
        }

        public <B> Option<B> flatMap(Function1<A, Option<B>> function1) {
            Option scala$Option$WithFilter$$$outer = scala$Option$WithFilter$$$outer();
            Function1<A, Object> function12 = this.scala$Option$WithFilter$$p;
            if (!scala$Option$WithFilter$$$outer.isEmpty() && !BoxesRunTime.unboxToBoolean(function12.apply(scala$Option$WithFilter$$$outer.get()))) {
                scala$Option$WithFilter$$$outer = None$.MODULE$;
            }
            return scala$Option$WithFilter$$$outer.isEmpty() ? None$.MODULE$ : function1.apply(scala$Option$WithFilter$$$outer.get());
        }

        public <B> Option<B> map(Function1<A, B> function1) {
            Option scala$Option$WithFilter$$$outer = scala$Option$WithFilter$$$outer();
            Function1<A, Object> function12 = this.scala$Option$WithFilter$$p;
            if (!scala$Option$WithFilter$$$outer.isEmpty() && !BoxesRunTime.unboxToBoolean(function12.apply(scala$Option$WithFilter$$$outer.get()))) {
                scala$Option$WithFilter$$$outer = None$.MODULE$;
            }
            return scala$Option$WithFilter$$$outer.isEmpty() ? None$.MODULE$ : new Some(function1.apply(scala$Option$WithFilter$$$outer.get()));
        }

        public /* synthetic */ Option scala$Option$WithFilter$$$outer() {
            return this.$outer;
        }
    }

    public Option() {
        Product.Cclass.$init$(this);
    }

    public abstract A get();

    public boolean isDefined() {
        return !isEmpty();
    }

    public abstract boolean isEmpty();

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return Product.Cclass.productPrefix(this);
    }

    public List<A> toList() {
        return isEmpty() ? Nil$.MODULE$ : new C$colon$colon(get(), Nil$.MODULE$);
    }
}
