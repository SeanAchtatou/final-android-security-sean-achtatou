package scala;

import scala.Function1;

public abstract class Predef$$less$colon$less<From, To> implements Function1<From, To>, Serializable {
    public Predef$$less$colon$less() {
        Function1.Cclass.$init$(this);
    }

    public <A> Function1<From, A> andThen(Function1<To, A> function1) {
        return Function1.Cclass.andThen(this, function1);
    }

    public <A> Function1<A, To> compose(Function1<A, From> function1) {
        return Function1.Cclass.compose(this, function1);
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }
}
