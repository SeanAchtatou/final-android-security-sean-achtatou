package com.unionpay.upomp.lthj.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.aq;
import com.lthj.unipay.plugin.at;
import com.lthj.unipay.plugin.cw;
import com.lthj.unipay.plugin.dy;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.q;
import com.lthj.unipay.plugin.w;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class ValidateCodeView extends RelativeLayout implements View.OnClickListener, UIResponseListener {
    private Context a;
    private AttributeSet b;
    /* access modifiers changed from: private */
    public ImageView c;
    /* access modifiers changed from: private */
    public ProgressBar d;
    private EditText e;
    private Button f;
    /* access modifiers changed from: private */
    public Handler g = new dy(this);

    public ValidateCodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context;
        this.b = attributeSet;
        e();
    }

    private void e() {
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this.a).inflate(PluginLink.getLayoutupomp_lthj_validatcodeview(), this);
        this.c = (ImageView) relativeLayout.findViewById(PluginLink.getIdupomp_lthj_validatecode_img());
        this.c.setOnClickListener(this);
        this.d = (ProgressBar) relativeLayout.findViewById(PluginLink.getIdupomp_lthj_validatecode_progress());
        this.f = (Button) relativeLayout.findViewById(PluginLink.getIdupomp_lthj_get_imgcode());
        this.f.setVisibility(8);
        this.f.setOnClickListener(this);
        this.e = (EditText) relativeLayout.findViewById(PluginLink.getIdupomp_lthj_validatecode_edit());
        if (this.b == null) {
        }
    }

    public ImageView a() {
        return this.c;
    }

    public ProgressBar b() {
        return this.d;
    }

    public EditText c() {
        return this.e;
    }

    public String d() {
        return this.e.getText().toString();
    }

    public void errorCallBack(String str) {
        j.a(this.a, str);
        this.c.setVisibility(8);
        this.d.setVisibility(8);
        this.f.setVisibility(0);
    }

    public void onClick(View view) {
        if (view == this.c) {
            j.a(this);
        } else if (view == this.f) {
            this.f.setVisibility(8);
            j.a(this);
        }
    }

    public void responseCallBack(w wVar) {
        if (wVar != null && wVar.m() != null) {
            int e2 = wVar.e();
            int parseInt = Integer.parseInt(wVar.m());
            switch (e2) {
                case 8201:
                    q qVar = (q) wVar;
                    at.a("VALIDATECODEVIEW", parseInt + " img");
                    if (parseInt == 0) {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append(ap.a().L.subSequence(0, ap.a().L.indexOf(CookieSpec.PATH_DELIM, 7) + 1));
                        stringBuffer.append(qVar.a());
                        stringBuffer.append("?sessionId=");
                        stringBuffer.append(aq.a);
                        new cw(this, stringBuffer.toString()).start();
                        return;
                    }
                    j.a(this.a, qVar.n(), parseInt);
                    this.c.setVisibility(0);
                    this.c.setBackgroundColor(-1);
                    this.c.setImageBitmap(null);
                    this.d.setVisibility(8);
                    return;
                default:
                    return;
            }
        }
    }
}
