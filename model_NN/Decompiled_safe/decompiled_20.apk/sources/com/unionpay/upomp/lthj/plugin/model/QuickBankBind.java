package com.unionpay.upomp.lthj.plugin.model;

public class QuickBankBind extends Data {
    public String cvn2;
    public String loginName;
    public String merchantId;
    public String merchantOrderId;
    public String merchantOrderTime;
    public String pan;
    public String panDate;
    public String pin;
}
