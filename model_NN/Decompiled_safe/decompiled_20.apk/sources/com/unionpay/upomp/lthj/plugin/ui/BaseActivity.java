package com.unionpay.upomp.lthj.plugin.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import com.lthj.unipay.plugin.at;
import com.lthj.unipay.plugin.bp;
import com.lthj.unipay.plugin.eh;
import com.lthj.unipay.plugin.ei;
import com.unionpay.upomp.lthj.link.PluginLink;

public class BaseActivity extends Activity {
    private String a;
    private View.OnClickListener b;
    /* access modifiers changed from: private */
    public bp c;

    public bp a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(String str, View.OnClickListener onClickListener) {
        this.a = str;
        this.b = onClickListener;
        at.a("baseactivity", str);
        this.c.changeTitleButton(str, onClickListener);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (getParent() instanceof bp) {
            this.c = (bp) getParent();
            a(null, null);
        }
        super.onCreate(bundle);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        quitNotice();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.c != null) {
            this.c.changeTitleButton(this.a, this.b);
        }
        this.c.aboutEnable(false);
        super.onResume();
    }

    public void quitNotice() {
        new AlertDialog.Builder(this).setTitle("退出交易").setMessage(PluginLink.getStringupomp_lthj_app_quitNotice_msg()).setNegativeButton(PluginLink.getStringupomp_lthj_cancel(), new ei(this)).setPositiveButton(PluginLink.getStringupomp_lthj_ok(), new eh(this)).show();
    }
}
