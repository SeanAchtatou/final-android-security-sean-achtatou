package com.unionpay.upomp.lthj.plugin.ui;

public final class R {

    public final class dimen {
        public static final int upomp_lthj_big_button_textsize = 2131099654;
        public static final int upomp_lthj_imgViewCheck_height = 2131099673;
        public static final int upomp_lthj_imgViewCheck_width = 2131099672;
        public static final int upomp_lthj_infobutton_h = 2131099665;
        public static final int upomp_lthj_infobutton_margin = 2131099666;
        public static final int upomp_lthj_infobutton_w = 2131099664;
        public static final int upomp_lthj_input_h = 2131099652;
        public static final int upomp_lthj_key_letter_btn_width = 2131099670;
        public static final int upomp_lthj_keyboard_fontsize = 2131099668;
        public static final int upomp_lthj_keyboard_padding = 2131099671;
        public static final int upomp_lthj_lineframe_h = 2131099648;
        public static final int upomp_lthj_lineframe_image_pad = 2131099650;
        public static final int upomp_lthj_lineframe_image_w = 2131099649;
        public static final int upomp_lthj_lineframe_text_pad = 2131099651;
        public static final int upomp_lthj_lineframe_textsize = 2131099656;
        public static final int upomp_lthj_listview_item_h = 2131099667;
        public static final int upomp_lthj_logo_w = 2131099653;
        public static final int upomp_lthj_margin_bottom = 2131099663;
        public static final int upomp_lthj_margin_left = 2131099661;
        public static final int upomp_lthj_margin_right = 2131099662;
        public static final int upomp_lthj_margin_top = 2131099660;
        public static final int upomp_lthj_num_sign_keyboard_padding = 2131099669;
        public static final int upomp_lthj_pad_left = 2131099657;
        public static final int upomp_lthj_pad_right = 2131099658;
        public static final int upomp_lthj_pad_top = 2131099659;
        public static final int upomp_lthj_progressBar_size = 2131099674;
        public static final int upomp_lthj_small_button_textsize = 2131099655;
        public static final int upomp_lthj_supportcard_padding = 2131099675;
        /* added by JADX */
        public static final int dialog_left_margin = 2131099676;
        /* added by JADX */
        public static final int dialog_top_margin = 2131099677;
        /* added by JADX */
        public static final int dialog_right_margin = 2131099678;
        /* added by JADX */
        public static final int dialog_bottom_margin = 2131099679;
        /* added by JADX */
        public static final int dialog_btn_close_right_margin = 2131099680;
        /* added by JADX */
        public static final int dialog_btn_close_top_margin = 2131099681;
        /* added by JADX */
        public static final int dialog_title_logo_left_margin = 2131099682;
        /* added by JADX */
        public static final int dialog_title_height = 2131099683;
        /* added by JADX */
        public static final int text_size_32 = 2131099684;
        /* added by JADX */
        public static final int text_size_31 = 2131099685;
        /* added by JADX */
        public static final int text_size_30 = 2131099686;
        /* added by JADX */
        public static final int text_size_29 = 2131099687;
        /* added by JADX */
        public static final int text_size_28 = 2131099688;
        /* added by JADX */
        public static final int text_size_27 = 2131099689;
        /* added by JADX */
        public static final int text_size_26 = 2131099690;
        /* added by JADX */
        public static final int text_size_25 = 2131099691;
        /* added by JADX */
        public static final int text_size_24 = 2131099692;
        /* added by JADX */
        public static final int text_size_23 = 2131099693;
        /* added by JADX */
        public static final int text_size_22 = 2131099694;
        /* added by JADX */
        public static final int text_size_21 = 2131099695;
        /* added by JADX */
        public static final int text_size_20 = 2131099696;
        /* added by JADX */
        public static final int text_size_19 = 2131099697;
        /* added by JADX */
        public static final int text_size_18 = 2131099698;
        /* added by JADX */
        public static final int text_size_17 = 2131099699;
        /* added by JADX */
        public static final int text_size_16 = 2131099700;
        /* added by JADX */
        public static final int text_size_15 = 2131099701;
        /* added by JADX */
        public static final int text_size_14 = 2131099702;
        /* added by JADX */
        public static final int text_size_13 = 2131099703;
        /* added by JADX */
        public static final int text_size_12 = 2131099704;
        /* added by JADX */
        public static final int text_size_11 = 2131099705;
        /* added by JADX */
        public static final int text_size_10 = 2131099706;
        /* added by JADX */
        public static final int text_size_9 = 2131099707;
        /* added by JADX */
        public static final int text_size_8 = 2131099708;
        /* added by JADX */
        public static final int text_size_7 = 2131099709;
        /* added by JADX */
        public static final int space_30 = 2131099710;
        /* added by JADX */
        public static final int space_29 = 2131099711;
        /* added by JADX */
        public static final int space_28 = 2131099712;
        /* added by JADX */
        public static final int space_27 = 2131099713;
        /* added by JADX */
        public static final int space_26 = 2131099714;
        /* added by JADX */
        public static final int space_25 = 2131099715;
        /* added by JADX */
        public static final int space_24 = 2131099716;
        /* added by JADX */
        public static final int space_23 = 2131099717;
        /* added by JADX */
        public static final int space_22 = 2131099718;
        /* added by JADX */
        public static final int space_21 = 2131099719;
        /* added by JADX */
        public static final int space_20 = 2131099720;
        /* added by JADX */
        public static final int space_19 = 2131099721;
        /* added by JADX */
        public static final int space_18 = 2131099722;
        /* added by JADX */
        public static final int space_17 = 2131099723;
        /* added by JADX */
        public static final int space_16 = 2131099724;
        /* added by JADX */
        public static final int space_15 = 2131099725;
        /* added by JADX */
        public static final int space_14 = 2131099726;
        /* added by JADX */
        public static final int space_13 = 2131099727;
        /* added by JADX */
        public static final int space_12 = 2131099728;
        /* added by JADX */
        public static final int space_11 = 2131099729;
        /* added by JADX */
        public static final int space_10 = 2131099730;
        /* added by JADX */
        public static final int space_9 = 2131099731;
        /* added by JADX */
        public static final int space_8 = 2131099732;
        /* added by JADX */
        public static final int space_7 = 2131099733;
        /* added by JADX */
        public static final int space_6 = 2131099734;
        /* added by JADX */
        public static final int space_5 = 2131099735;
        /* added by JADX */
        public static final int space_4 = 2131099736;
        /* added by JADX */
        public static final int space_3 = 2131099737;
        /* added by JADX */
        public static final int space_2 = 2131099738;
        /* added by JADX */
        public static final int space_1 = 2131099739;
        /* added by JADX */
        public static final int button_height = 2131099740;
        /* added by JADX */
        public static final int bottom_tab_font_size = 2131099741;
        /* added by JADX */
        public static final int bottom_tab_padding_up = 2131099742;
        /* added by JADX */
        public static final int bottom_tab_padding_drawable = 2131099743;
        /* added by JADX */
        public static final int divider_large_size = 2131099744;
        /* added by JADX */
        public static final int actions_spacing = 2131099745;
        /* added by JADX */
        public static final int webloading_progress_textsize = 2131099746;
        /* added by JADX */
        public static final int webloading_fail_textsize = 2131099747;
        /* added by JADX */
        public static final int commentary_edit_size = 2131099748;
        /* added by JADX */
        public static final int search_text_size = 2131099749;
        /* added by JADX */
        public static final int search_edit_size = 2131099750;
        /* added by JADX */
        public static final int search_text_loading = 2131099751;
        /* added by JADX */
        public static final int search_text_clear_history = 2131099752;
        /* added by JADX */
        public static final int web_title_height = 2131099753;
        /* added by JADX */
        public static final int dialog_TitleTextSize = 2131099754;
        /* added by JADX */
        public static final int LargeTextSize = 2131099755;
        /* added by JADX */
        public static final int updatebar_height = 2131099756;
        /* added by JADX */
        public static final int updatebar_content_height = 2131099757;
        /* added by JADX */
        public static final int updatebar_padding = 2131099758;
        /* added by JADX */
        public static final int activity_horizontal_margin = 2131099759;
        /* added by JADX */
        public static final int activity_vertical_margin = 2131099760;
    }

    public final class drawable {
        public static final int upomp_lthj_about_text_color = 2130837762;
        public static final int upomp_lthj_background = 2130837754;
        public static final int upomp_lthj_bottom_line = 2130837672;
        public static final int upomp_lthj_button_blue = 2130837673;
        public static final int upomp_lthj_button_blue_noselect = 2130837674;
        public static final int upomp_lthj_button_blue_select = 2130837675;
        public static final int upomp_lthj_button_drop = 2130837676;
        public static final int upomp_lthj_button_gray = 2130837677;
        public static final int upomp_lthj_button_gray_noselect = 2130837678;
        public static final int upomp_lthj_button_gray_select = 2130837679;
        public static final int upomp_lthj_button_green = 2130837680;
        public static final int upomp_lthj_button_green_noselect = 2130837681;
        public static final int upomp_lthj_button_green_select = 2130837682;
        public static final int upomp_lthj_button_lightblue = 2130837683;
        public static final int upomp_lthj_button_lightblue_noselect = 2130837684;
        public static final int upomp_lthj_button_lightblue_select = 2130837685;
        public static final int upomp_lthj_button_select = 2130837686;
        public static final int upomp_lthj_button_select_color = 2130837760;
        public static final int upomp_lthj_button_verifycode = 2130837687;
        public static final int upomp_lthj_button_verifycode_noselect = 2130837688;
        public static final int upomp_lthj_button_verifycode_select = 2130837689;
        public static final int upomp_lthj_card_icon = 2130837690;
        public static final int upomp_lthj_card_img = 2130837691;
        public static final int upomp_lthj_checkbox = 2130837692;
        public static final int upomp_lthj_checkbox_select = 2130837693;
        public static final int upomp_lthj_checkbox_unselect = 2130837694;
        public static final int upomp_lthj_common_drop = 2130837695;
        public static final int upomp_lthj_default_drop = 2130837696;
        public static final int upomp_lthj_default_icon = 2130837697;
        public static final int upomp_lthj_desc_icon = 2130837698;
        public static final int upomp_lthj_dialog_no_color = 2130837759;
        public static final int upomp_lthj_dialog_ok_color = 2130837758;
        public static final int upomp_lthj_fail_icon = 2130837699;
        public static final int upomp_lthj_faildialog_icon = 2130837700;
        public static final int upomp_lthj_filled_box = 2130837701;
        public static final int upomp_lthj_gray = 2130837751;
        public static final int upomp_lthj_gray_line = 2130837702;
        public static final int upomp_lthj_green = 2130837756;
        public static final int upomp_lthj_icon = 2130837703;
        public static final int upomp_lthj_info_down_btn = 2130837704;
        public static final int upomp_lthj_info_frame = 2130837705;
        public static final int upomp_lthj_info_up_btn = 2130837706;
        public static final int upomp_lthj_input_bg = 2130837707;
        public static final int upomp_lthj_input_help_btn = 2130837708;
        public static final int upomp_lthj_keyboard = 2130837709;
        public static final int upomp_lthj_keybtn = 2130837710;
        public static final int upomp_lthj_keybtn1 = 2130837711;
        public static final int upomp_lthj_keybtn_enlarge = 2130837712;
        public static final int upomp_lthj_keybtn_enlarge_1 = 2130837713;
        public static final int upomp_lthj_keypad_back = 2130837757;
        public static final int upomp_lthj_line_color = 2130837761;
        public static final int upomp_lthj_logo = 2130837714;
        public static final int upomp_lthj_logobar = 2130837715;
        public static final int upomp_lthj_mobile_icon = 2130837716;
        public static final int upomp_lthj_okdialog_icon = 2130837717;
        public static final int upomp_lthj_order_icon = 2130837718;
        public static final int upomp_lthj_pay_icon = 2130837719;
        public static final int upomp_lthj_progressbar = 2130837720;
        public static final int upomp_lthj_prompt_icon = 2130837721;
        public static final int upomp_lthj_red = 2130837755;
        public static final int upomp_lthj_refresh_icon = 2130837722;
        public static final int upomp_lthj_rightarrow_icon = 2130837723;
        public static final int upomp_lthj_row_line = 2130837724;
        public static final int upomp_lthj_set_default_icon = 2130837725;
        public static final int upomp_lthj_shield_icon = 2130837726;
        public static final int upomp_lthj_smalllogo = 2130837727;
        public static final int upomp_lthj_splash_logo = 2130837728;
        public static final int upomp_lthj_splash_progress = 2130837729;
        public static final int upomp_lthj_splash_thumb = 2130837730;
        public static final int upomp_lthj_success_icon = 2130837731;
        public static final int upomp_lthj_supportcard_title = 2130837732;
        public static final int upomp_lthj_text_black = 2130837752;
        public static final int upomp_lthj_text_blue = 2130837753;
        public static final int upomp_lthj_unbind_icon = 2130837733;
        public static final int upomp_lthj_user_icon = 2130837734;
        public static final int upomp_lthj_vertical_line = 2130837735;
        public static final int upomp_lthj_watermark = 2130837736;
        public static final int upomp_lthj_white = 2130837750;
        /* added by JADX */
        public static final int about = 2130837504;
        /* added by JADX */
        public static final int about_icon = 2130837505;
        /* added by JADX */
        public static final int about_icon_desc_sign = 2130837506;
        /* added by JADX */
        public static final int about_icon_map_sign = 2130837507;
        /* added by JADX */
        public static final int about_icon_phone_call = 2130837508;
        /* added by JADX */
        public static final int android_layout_bg = 2130837509;
        /* added by JADX */
        public static final int android_title_bg = 2130837510;
        /* added by JADX */
        public static final int android_title_bg_progress = 2130837511;
        /* added by JADX */
        public static final int arrow = 2130837512;
        /* added by JADX */
        public static final int arrow_blue = 2130837513;
        /* added by JADX */
        public static final int arrow_down = 2130837514;
        /* added by JADX */
        public static final int arrow_gray = 2130837515;
        /* added by JADX */
        public static final int arrow_gray_right = 2130837516;
        /* added by JADX */
        public static final int arrow_up = 2130837517;
        /* added by JADX */
        public static final int background_corners = 2130837518;
        /* added by JADX */
        public static final int bg_list_item = 2130837519;
        /* added by JADX */
        public static final int bg_list_item2 = 2130837520;
        /* added by JADX */
        public static final int bg_toolbar = 2130837521;
        /* added by JADX */
        public static final int bottom_bg = 2130837522;
        /* added by JADX */
        public static final int btn_close = 2130837523;
        /* added by JADX */
        public static final int btn_consultation = 2130837524;
        /* added by JADX */
        public static final int btn_consultation_check = 2130837525;
        /* added by JADX */
        public static final int btn_discuss = 2130837526;
        /* added by JADX */
        public static final int btn_discuss_check = 2130837527;
        /* added by JADX */
        public static final int btn_favorite = 2130837528;
        /* added by JADX */
        public static final int btn_favorite_check = 2130837529;
        /* added by JADX */
        public static final int btn_gray_btn = 2130837530;
        /* added by JADX */
        public static final int btn_gray_btn_click = 2130837531;
        /* added by JADX */
        public static final int btn_gray_title_bar = 2130837532;
        /* added by JADX */
        public static final int btn_gray_title_bar_click = 2130837533;
        /* added by JADX */
        public static final int btn_product_detail = 2130837534;
        /* added by JADX */
        public static final int btn_product_detail_check = 2130837535;
        /* added by JADX */
        public static final int btn_recommend = 2130837536;
        /* added by JADX */
        public static final int btn_recommend_check = 2130837537;
        /* added by JADX */
        public static final int btn_red_title_bar = 2130837538;
        /* added by JADX */
        public static final int btn_red_title_bar_click = 2130837539;
        /* added by JADX */
        public static final int btn_route_next = 2130837540;
        /* added by JADX */
        public static final int btn_route_pre = 2130837541;
        /* added by JADX */
        public static final int button = 2130837542;
        /* added by JADX */
        public static final int button_bg = 2130837543;
        /* added by JADX */
        public static final int cart_dialog_add_btn = 2130837544;
        /* added by JADX */
        public static final int cart_dialog_add_btn_bg = 2130837545;
        /* added by JADX */
        public static final int cart_dialog_add_btn_clicked = 2130837546;
        /* added by JADX */
        public static final int cart_dialog_bg = 2130837547;
        /* added by JADX */
        public static final int cart_dialog_edittext_bg = 2130837548;
        /* added by JADX */
        public static final int cart_dialog_sub_btn = 2130837549;
        /* added by JADX */
        public static final int cart_dialog_sub_btn_bg = 2130837550;
        /* added by JADX */
        public static final int cart_dialog_sub_btn_clicked = 2130837551;
        /* added by JADX */
        public static final int clearwords_icon = 2130837552;
        /* added by JADX */
        public static final int contact_listitem = 2130837553;
        /* added by JADX */
        public static final int custom_info_bubble = 2130837554;
        /* added by JADX */
        public static final int dialog_bg = 2130837555;
        /* added by JADX */
        public static final int empty_cart_bg = 2130837556;
        /* added by JADX */
        public static final int empty_list = 2130837557;
        /* added by JADX */
        public static final int feature_point = 2130837558;
        /* added by JADX */
        public static final int feature_point_cur = 2130837559;
        /* added by JADX */
        public static final int frame_button_bg_left = 2130837560;
        /* added by JADX */
        public static final int frame_button_bg_middle = 2130837561;
        /* added by JADX */
        public static final int frame_button_bg_right = 2130837562;
        /* added by JADX */
        public static final int frame_icon_search = 2130837563;
        /* added by JADX */
        public static final int free_register_bg = 2130837564;
        /* added by JADX */
        public static final int gallery_doc_check = 2130837565;
        /* added by JADX */
        public static final int gallery_dot = 2130837566;
        /* added by JADX */
        public static final int head_text_light = 2130837567;
        /* added by JADX */
        public static final int home_btn_bg = 2130837568;
        /* added by JADX */
        public static final int hot_goods_arrow_big = 2130837569;
        /* added by JADX */
        public static final int hot_goods_arrow_small = 2130837570;
        /* added by JADX */
        public static final int ic_action_search = 2130837571;
        /* added by JADX */
        public static final int ic_launcher = 2130837572;
        /* added by JADX */
        public static final int ic_pulltorefresh_arrow = 2130837573;
        /* added by JADX */
        public static final int ic_selected = 2130837574;
        /* added by JADX */
        public static final int ic_unselected = 2130837575;
        /* added by JADX */
        public static final int icon = 2130837576;
        /* added by JADX */
        public static final int icon_circle = 2130837577;
        /* added by JADX */
        public static final int icon_home_arrow = 2130837578;
        /* added by JADX */
        public static final int icon_hot_goods = 2130837579;
        /* added by JADX */
        public static final int icon_map_sign = 2130837580;
        /* added by JADX */
        public static final int icon_mendian = 2130837581;
        /* added by JADX */
        public static final int icon_new_goods = 2130837582;
        /* added by JADX */
        public static final int icon_phone_call = 2130837583;
        /* added by JADX */
        public static final int icon_phone_call_gray = 2130837584;
        /* added by JADX */
        public static final int icon_rebate_goods = 2130837585;
        /* added by JADX */
        public static final int icon_timebuy = 2130837586;
        /* added by JADX */
        public static final int image_default = 2130837587;
        /* added by JADX */
        public static final int list_item_bottom_bg = 2130837588;
        /* added by JADX */
        public static final int list_item_center_bg = 2130837589;
        /* added by JADX */
        public static final int list_item_one_bg = 2130837590;
        /* added by JADX */
        public static final int list_item_one_bg2 = 2130837591;
        /* added by JADX */
        public static final int list_item_one_bg3 = 2130837592;
        /* added by JADX */
        public static final int list_item_one_bg4 = 2130837593;
        /* added by JADX */
        public static final int list_item_top_bg = 2130837594;
        /* added by JADX */
        public static final int list_view_item_selector = 2130837595;
        /* added by JADX */
        public static final int listview_divider = 2130837596;
        /* added by JADX */
        public static final int loading_0 = 2130837597;
        /* added by JADX */
        public static final int loading_1 = 2130837598;
        /* added by JADX */
        public static final int loading_2 = 2130837599;
        /* added by JADX */
        public static final int loading_3 = 2130837600;
        /* added by JADX */
        public static final int loading_4 = 2130837601;
        /* added by JADX */
        public static final int loading_5 = 2130837602;
        /* added by JADX */
        public static final int loading_6 = 2130837603;
        /* added by JADX */
        public static final int loading_7 = 2130837604;
        /* added by JADX */
        public static final int login_button = 2130837605;
        /* added by JADX */
        public static final int login_button_press = 2130837606;
        /* added by JADX */
        public static final int loop_default = 2130837607;
        /* added by JADX */
        public static final int main_navigation_highlight_bg = 2130837608;
        /* added by JADX */
        public static final int map_arrow = 2130837609;
        /* added by JADX */
        public static final int map_bus = 2130837610;
        /* added by JADX */
        public static final int map_car = 2130837611;
        /* added by JADX */
        public static final int map_end = 2130837612;
        /* added by JADX */
        public static final int map_man = 2130837613;
        /* added by JADX */
        public static final int map_map_bus = 2130837614;
        /* added by JADX */
        public static final int map_next = 2130837615;
        /* added by JADX */
        public static final int map_next_disable = 2130837616;
        /* added by JADX */
        public static final int map_prev = 2130837617;
        /* added by JADX */
        public static final int map_prev_disable = 2130837618;
        /* added by JADX */
        public static final int map_start = 2130837619;
        /* added by JADX */
        public static final int mm_listitem_normal = 2130837620;
        /* added by JADX */
        public static final int mm_listitem_pressed = 2130837621;
        /* added by JADX */
        public static final int mm_listitem_simple = 2130837622;
        /* added by JADX */
        public static final int more_arrow = 2130837623;
        /* added by JADX */
        public static final int more_btn_big = 2130837624;
        /* added by JADX */
        public static final int more_btn_small = 2130837625;
        /* added by JADX */
        public static final int my_rating_bar_full = 2130837626;
        /* added by JADX */
        public static final int net_work = 2130837627;
        /* added by JADX */
        public static final int new_data_toast = 2130837628;
        /* added by JADX */
        public static final int no_image_default = 2130837629;
        /* added by JADX */
        public static final int no_photo = 2130837630;
        /* added by JADX */
        public static final int pref_icon_more = 2130837631;
        /* added by JADX */
        public static final int pro_cart_empty = 2130837632;
        /* added by JADX */
        public static final int pro_consult_empty = 2130837633;
        /* added by JADX */
        public static final int pro_desc_empty = 2130837634;
        /* added by JADX */
        public static final int pro_discuss_empty = 2130837635;
        /* added by JADX */
        public static final int pro_recommend_empty = 2130837636;
        /* added by JADX */
        public static final int product_list_bg = 2130837637;
        /* added by JADX */
        public static final int proggress_rotate = 2130837638;
        /* added by JADX */
        public static final int progress_bar_rotate = 2130837639;
        /* added by JADX */
        public static final int promotion_product_background = 2130837640;
        /* added by JADX */
        public static final int rb_focus = 2130837641;
        /* added by JADX */
        public static final int rb_normal = 2130837642;
        /* added by JADX */
        public static final int search = 2130837643;
        /* added by JADX */
        public static final int search_edit_bg_corners = 2130837644;
        /* added by JADX */
        public static final int search_history_bg = 2130837645;
        /* added by JADX */
        public static final int selector_consultation = 2130837646;
        /* added by JADX */
        public static final int selector_discuss = 2130837647;
        /* added by JADX */
        public static final int selector_gray_button = 2130837648;
        /* added by JADX */
        public static final int selector_listview_bg = 2130837649;
        /* added by JADX */
        public static final int selector_product_attrs_rbtn = 2130837650;
        /* added by JADX */
        public static final int selector_product_detail = 2130837651;
        /* added by JADX */
        public static final int selector_recommend = 2130837652;
        /* added by JADX */
        public static final int selector_red_button = 2130837653;
        /* added by JADX */
        public static final int selector_title_button = 2130837654;
        /* added by JADX */
        public static final int setting_arrow = 2130837655;
        /* added by JADX */
        public static final int shadow = 2130837656;
        /* added by JADX */
        public static final int shadow_no_bottom = 2130837657;
        /* added by JADX */
        public static final int start = 2130837658;
        /* added by JADX */
        public static final int tab_arrow = 2130837659;
        /* added by JADX */
        public static final int tab_arrow_selected = 2130837660;
        /* added by JADX */
        public static final int tab_bg_normal = 2130837661;
        /* added by JADX */
        public static final int tabbar_bg = 2130837662;
        /* added by JADX */
        public static final int tabbar_left_click_bg = 2130837663;
        /* added by JADX */
        public static final int tabbar_middle_click_bg = 2130837664;
        /* added by JADX */
        public static final int tabbar_right_click_bg = 2130837665;
        /* added by JADX */
        public static final int textview_round = 2130837666;
        /* added by JADX */
        public static final int textview_round_home = 2130837667;
        /* added by JADX */
        public static final int timebuy_title_bg = 2130837668;
        /* added by JADX */
        public static final int title_button_bg = 2130837669;
        /* added by JADX */
        public static final int title_button_click_bg = 2130837670;
        /* added by JADX */
        public static final int top = 2130837671;
        /* added by JADX */
        public static final int user_default_photo = 2130837737;
        /* added by JADX */
        public static final int welcome = 2130837738;
        /* added by JADX */
        public static final int widget_bar_bg_nor = 2130837739;
        /* added by JADX */
        public static final int widget_bar_bg_selector = 2130837740;
        /* added by JADX */
        public static final int widget_bar_cart_nor = 2130837741;
        /* added by JADX */
        public static final int widget_bar_cate_nor = 2130837742;
        /* added by JADX */
        public static final int widget_bar_home = 2130837743;
        /* added by JADX */
        public static final int widget_bar_home_nor = 2130837744;
        /* added by JADX */
        public static final int widget_bar_item_bg = 2130837745;
        /* added by JADX */
        public static final int widget_bar_more_nor = 2130837746;
        /* added by JADX */
        public static final int widget_bar_user_nor = 2130837747;
        /* added by JADX */
        public static final int widget_head_bg_gray = 2130837748;
        /* added by JADX */
        public static final int widget_head_bg_red = 2130837749;
        /* added by JADX */
        public static final int search_history_pressed = 2130837763;
        /* added by JADX */
        public static final int transparent_background = 2130837764;
        /* added by JADX */
        public static final int transparent = 2130837765;
        /* added by JADX */
        public static final int list_row_zeng = 2130837766;
        /* added by JADX */
        public static final int list_row_pressed = 2130837767;
    }

    public final class id {
        public static final int upomp_lthj_about_btn = 2131165353;
        public static final int upomp_lthj_account_mange_btn = 2131165230;
        public static final int upomp_lthj_add_bankcard_btn = 2131165255;
        public static final int upomp_lthj_auto_welcome_btn = 2131165358;
        public static final int upomp_lthj_bankcard_info_view = 2131165215;
        public static final int upomp_lthj_bankcard_listview = 2131165240;
        public static final int upomp_lthj_bankcard_listview_childicon = 2131165346;
        public static final int upomp_lthj_bankcard_listview_childtv = 2131165345;
        public static final int upomp_lthj_bind_number_view = 2131165231;
        public static final int upomp_lthj_bottom_line = 2131165186;
        public static final int upomp_lthj_build_no = 2131165354;
        public static final int upomp_lthj_button_bankcard_manage = 2131165234;
        public static final int upomp_lthj_button_cancel = 2131165211;
        public static final int upomp_lthj_button_change_mobile = 2131165233;
        public static final int upomp_lthj_button_change_pwd = 2131165232;
        public static final int upomp_lthj_button_ok = 2131165210;
        public static final int upomp_lthj_card_menu_drop = 2131165349;
        public static final int upomp_lthj_card_num_edit = 2131165206;
        public static final int upomp_lthj_confirm_pwd_input = 2131165235;
        public static final int upomp_lthj_container = 2131165185;
        public static final int upomp_lthj_cupsqid_tv = 2131165333;
        public static final int upomp_lthj_custominput_et = 2131165195;
        public static final int upomp_lthj_custominput_tv = 2131165194;
        public static final int upomp_lthj_cvn2_edit = 2131165207;
        public static final int upomp_lthj_cvn2_help = 2131165351;
        public static final int upomp_lthj_cvn2_layout = 2131165220;
        public static final int upomp_lthj_date_edit = 2131165209;
        public static final int upomp_lthj_date_help = 2131165352;
        public static final int upomp_lthj_date_layout = 2131165219;
        public static final int upomp_lthj_default_card = 2131165239;
        public static final int upomp_lthj_default_checkbox = 2131165344;
        public static final int upomp_lthj_desc_line = 2131165331;
        public static final int upomp_lthj_dialog_message = 2131165214;
        public static final int upomp_lthj_dialog_progress = 2131165213;
        public static final int upomp_lthj_error_desc = 2131165336;
        public static final int upomp_lthj_forget_pwd = 2131165225;
        public static final int upomp_lthj_get_imgcode = 2131165360;
        public static final int upomp_lthj_get_mac_btn = 2131165221;
        public static final int upomp_lthj_help_btn = 2131165248;
        public static final int upomp_lthj_homebackbutton = 2131165188;
        public static final int upomp_lthj_imgview_checkword = 2131165328;
        public static final int upomp_lthj_keyboardButtonNum = 2131165270;
        public static final int upomp_lthj_keyboard_button0 = 2131165266;
        public static final int upomp_lthj_keyboard_button1 = 2131165257;
        public static final int upomp_lthj_keyboard_button2 = 2131165258;
        public static final int upomp_lthj_keyboard_button3 = 2131165259;
        public static final int upomp_lthj_keyboard_button4 = 2131165260;
        public static final int upomp_lthj_keyboard_button5 = 2131165261;
        public static final int upomp_lthj_keyboard_button6 = 2131165262;
        public static final int upomp_lthj_keyboard_button7 = 2131165263;
        public static final int upomp_lthj_keyboard_button8 = 2131165264;
        public static final int upomp_lthj_keyboard_button9 = 2131165265;
        public static final int upomp_lthj_keyboard_buttonC = 2131165267;
        public static final int upomp_lthj_keyboard_buttonLetter = 2131165271;
        public static final int upomp_lthj_keyboard_buttonOK = 2131165268;
        public static final int upomp_lthj_keyboard_buttonSign = 2131165272;
        public static final int upomp_lthj_keyboard_button_a = 2131165273;
        public static final int upomp_lthj_keyboard_button_b = 2131165274;
        public static final int upomp_lthj_keyboard_button_c = 2131165275;
        public static final int upomp_lthj_keyboard_button_d = 2131165276;
        public static final int upomp_lthj_keyboard_button_e = 2131165277;
        public static final int upomp_lthj_keyboard_button_f = 2131165278;
        public static final int upomp_lthj_keyboard_button_g = 2131165279;
        public static final int upomp_lthj_keyboard_button_h = 2131165280;
        public static final int upomp_lthj_keyboard_button_i = 2131165281;
        public static final int upomp_lthj_keyboard_button_j = 2131165282;
        public static final int upomp_lthj_keyboard_button_k = 2131165283;
        public static final int upomp_lthj_keyboard_button_l = 2131165284;
        public static final int upomp_lthj_keyboard_button_m = 2131165285;
        public static final int upomp_lthj_keyboard_button_n = 2131165286;
        public static final int upomp_lthj_keyboard_button_o = 2131165287;
        public static final int upomp_lthj_keyboard_button_p = 2131165288;
        public static final int upomp_lthj_keyboard_button_q = 2131165289;
        public static final int upomp_lthj_keyboard_button_r = 2131165290;
        public static final int upomp_lthj_keyboard_button_s = 2131165291;
        public static final int upomp_lthj_keyboard_button_shift = 2131165299;
        public static final int upomp_lthj_keyboard_button_sign1 = 2131165300;
        public static final int upomp_lthj_keyboard_button_sign10 = 2131165309;
        public static final int upomp_lthj_keyboard_button_sign11 = 2131165310;
        public static final int upomp_lthj_keyboard_button_sign12 = 2131165311;
        public static final int upomp_lthj_keyboard_button_sign13 = 2131165312;
        public static final int upomp_lthj_keyboard_button_sign14 = 2131165313;
        public static final int upomp_lthj_keyboard_button_sign15 = 2131165314;
        public static final int upomp_lthj_keyboard_button_sign16 = 2131165315;
        public static final int upomp_lthj_keyboard_button_sign17 = 2131165316;
        public static final int upomp_lthj_keyboard_button_sign18 = 2131165317;
        public static final int upomp_lthj_keyboard_button_sign19 = 2131165318;
        public static final int upomp_lthj_keyboard_button_sign2 = 2131165301;
        public static final int upomp_lthj_keyboard_button_sign20 = 2131165319;
        public static final int upomp_lthj_keyboard_button_sign21 = 2131165320;
        public static final int upomp_lthj_keyboard_button_sign22 = 2131165321;
        public static final int upomp_lthj_keyboard_button_sign23 = 2131165322;
        public static final int upomp_lthj_keyboard_button_sign24 = 2131165323;
        public static final int upomp_lthj_keyboard_button_sign25 = 2131165324;
        public static final int upomp_lthj_keyboard_button_sign3 = 2131165302;
        public static final int upomp_lthj_keyboard_button_sign4 = 2131165303;
        public static final int upomp_lthj_keyboard_button_sign5 = 2131165304;
        public static final int upomp_lthj_keyboard_button_sign6 = 2131165305;
        public static final int upomp_lthj_keyboard_button_sign7 = 2131165306;
        public static final int upomp_lthj_keyboard_button_sign8 = 2131165307;
        public static final int upomp_lthj_keyboard_button_sign9 = 2131165308;
        public static final int upomp_lthj_keyboard_button_signnext = 2131165325;
        public static final int upomp_lthj_keyboard_button_t = 2131165292;
        public static final int upomp_lthj_keyboard_button_u = 2131165293;
        public static final int upomp_lthj_keyboard_button_v = 2131165294;
        public static final int upomp_lthj_keyboard_button_w = 2131165295;
        public static final int upomp_lthj_keyboard_button_x = 2131165296;
        public static final int upomp_lthj_keyboard_button_y = 2131165297;
        public static final int upomp_lthj_keyboard_button_z = 2131165298;
        public static final int upomp_lthj_keyboard_editText = 2131165269;
        public static final int upomp_lthj_keyboard_layoutRight = 2131165256;
        public static final int upomp_lthj_keyboard_title = 2131165326;
        public static final int upomp_lthj_keyboard_view = 2131165327;
        public static final int upomp_lthj_lineframe_image = 2131165189;
        public static final int upomp_lthj_lineframe_title = 2131165190;
        public static final int upomp_lthj_lineframe_tv = 2131165191;
        public static final int upomp_lthj_list_openview = 2131165242;
        public static final int upomp_lthj_macprompt = 2131165222;
        public static final int upomp_lthj_merchant_tv = 2131165196;
        public static final int upomp_lthj_mobile_num_edit = 2131165205;
        public static final int upomp_lthj_mobile_number_view = 2131165216;
        public static final int upomp_lthj_mobilemac_edit = 2131165212;
        public static final int upomp_lthj_myinfo_view = 2131165355;
        public static final int upomp_lthj_new_mobilenum_edit = 2131165238;
        public static final int upomp_lthj_new_pwd_input = 2131165236;
        public static final int upomp_lthj_next_btn = 2131165193;
        public static final int upomp_lthj_no_card_layout = 2131165347;
        public static final int upomp_lthj_old_pwd_input = 2131165237;
        public static final int upomp_lthj_orderamt_tv = 2131165197;
        public static final int upomp_lthj_orderno_row = 2131165204;
        public static final int upomp_lthj_orderno_tv = 2131165198;
        public static final int upomp_lthj_ordertime_tv = 2131165199;
        public static final int upomp_lthj_password_edit = 2131165227;
        public static final int upomp_lthj_pay_desc = 2131165332;
        public static final int upomp_lthj_pay_state = 2131165330;
        public static final int upomp_lthj_pin_edit = 2131165208;
        public static final int upomp_lthj_pin_layout = 2131165218;
        public static final int upomp_lthj_progress = 2131165329;
        public static final int upomp_lthj_quickpaycard_layout = 2131165348;
        public static final int upomp_lthj_refrush_btn = 2131165343;
        public static final int upomp_lthj_reg_pro_btn = 2131165356;
        public static final int upomp_lthj_reg_prompt = 2131165337;
        public static final int upomp_lthj_rel_above_bottom = 2131165359;
        public static final int upomp_lthj_relative_arrows = 2131165241;
        public static final int upomp_lthj_relative_toleft = 2131165361;
        public static final int upomp_lthj_safe_ask_drop = 2131165350;
        public static final int upomp_lthj_safe_ask_view = 2131165334;
        public static final int upomp_lthj_safe_asw_view = 2131165335;
        public static final int upomp_lthj_safe_prompt_view = 2131165217;
        public static final int upomp_lthj_savecard_listview = 2131165247;
        public static final int upomp_lthj_splash_seekbar = 2131165201;
        public static final int upomp_lthj_state_view = 2131165338;
        public static final int upomp_lthj_sup_bank_item = 2131165251;
        public static final int upomp_lthj_sup_credit_item = 2131165250;
        public static final int upomp_lthj_sup_debit_item = 2131165249;
        public static final int upomp_lthj_support_card_title1 = 2131165245;
        public static final int upomp_lthj_support_card_title2 = 2131165246;
        public static final int upomp_lthj_support_creditcard_tv = 2131165244;
        public static final int upomp_lthj_support_debitcard_tv = 2131165243;
        public static final int upomp_lthj_tableLayout = 2131165192;
        public static final int upomp_lthj_textview_time = 2131165200;
        public static final int upomp_lthj_tradeinfolayout = 2131165187;
        public static final int upomp_lthj_trantime_row = 2131165203;
        public static final int upomp_lthj_unfold_btn = 2131165202;
        public static final int upomp_lthj_use_card_pay = 2131165224;
        public static final int upomp_lthj_use_quick_pay = 2131165223;
        public static final int upomp_lthj_user_pro_box = 2131165357;
        public static final int upomp_lthj_username_edit = 2131165226;
        public static final int upomp_lthj_username_view = 2131165228;
        public static final int upomp_lthj_userprotocol_content = 2131165253;
        public static final int upomp_lthj_userprotocol_listview = 2131165254;
        public static final int upomp_lthj_userprotocol_title = 2131165252;
        public static final int upomp_lthj_validatecode_edit = 2131165340;
        public static final int upomp_lthj_validatecode_img = 2131165341;
        public static final int upomp_lthj_validatecode_layout = 2131165339;
        public static final int upomp_lthj_validatecode_progress = 2131165342;
        public static final int upomp_lthj_version = 2131165184;
        public static final int upomp_lthj_welcome_view = 2131165229;
        /* added by JADX */
        public static final int app_icon = 2131165362;
        /* added by JADX */
        public static final int about_company_name = 2131165363;
        /* added by JADX */
        public static final int view1 = 2131165364;
        /* added by JADX */
        public static final int about_company_address_layout = 2131165365;
        /* added by JADX */
        public static final int about_company_address_img = 2131165366;
        /* added by JADX */
        public static final int about_company_address = 2131165367;
        /* added by JADX */
        public static final int about_company_address_map = 2131165368;
        /* added by JADX */
        public static final int view2 = 2131165369;
        /* added by JADX */
        public static final int company_phone_layout = 2131165370;
        /* added by JADX */
        public static final int about_company_phone_img = 2131165371;
        /* added by JADX */
        public static final int about_company_img = 2131165372;
        /* added by JADX */
        public static final int company_phone = 2131165373;
        /* added by JADX */
        public static final int about_company_phone_call = 2131165374;
        /* added by JADX */
        public static final int view4 = 2131165375;
        /* added by JADX */
        public static final int company_desc_layout = 2131165376;
        /* added by JADX */
        public static final int about_company_desc_img = 2131165377;
        /* added by JADX */
        public static final int company_desc = 2131165378;
        /* added by JADX */
        public static final int about_company_desc_call = 2131165379;
        /* added by JADX */
        public static final int view3 = 2131165380;
        /* added by JADX */
        public static final int address_add_name = 2131165381;
        /* added by JADX */
        public static final int address_add_mobile = 2131165382;
        /* added by JADX */
        public static final int address_add_code = 2131165383;
        /* added by JADX */
        public static final int address_add_province = 2131165384;
        /* added by JADX */
        public static final int address_add_city = 2131165385;
        /* added by JADX */
        public static final int address_add_area = 2131165386;
        /* added by JADX */
        public static final int address_add_address = 2131165387;
        /* added by JADX */
        public static final int address_item = 2131165388;
        /* added by JADX */
        public static final int address_item_index = 2131165389;
        /* added by JADX */
        public static final int address_item_default_flag = 2131165390;
        /* added by JADX */
        public static final int address_item_name = 2131165391;
        /* added by JADX */
        public static final int address_item_mobile = 2131165392;
        /* added by JADX */
        public static final int address_item_address = 2131165393;
        /* added by JADX */
        public static final int address_listView = 2131165394;
        /* added by JADX */
        public static final int address_empty_list = 2131165395;
        /* added by JADX */
        public static final int attrs_name = 2131165396;
        /* added by JADX */
        public static final int attrs_image = 2131165397;
        /* added by JADX */
        public static final int itemattrs_name = 2131165398;
        /* added by JADX */
        public static final int itemattrs_id = 2131165399;
        /* added by JADX */
        public static final int order_info_scrollview = 2131165400;
        /* added by JADX */
        public static final int main_relativelayout_header = 2131165401;
        /* added by JADX */
        public static final int check_order_sub = 2131165402;
        /* added by JADX */
        public static final int check_order_address_item = 2131165403;
        /* added by JADX */
        public static final int check_order_address_title = 2131165404;
        /* added by JADX */
        public static final int check_order_address_username = 2131165405;
        /* added by JADX */
        public static final int check_order_address_tel = 2131165406;
        /* added by JADX */
        public static final int check_order_address_address = 2131165407;
        /* added by JADX */
        public static final int address_image_arrow = 2131165408;
        /* added by JADX */
        public static final int check_order_product_list = 2131165409;
        /* added by JADX */
        public static final int check_order_pay_item = 2131165410;
        /* added by JADX */
        public static final int check_order_pay_title = 2131165411;
        /* added by JADX */
        public static final int check_order_pay_content = 2131165412;
        /* added by JADX */
        public static final int check_order_ship_item = 2131165413;
        /* added by JADX */
        public static final int check_order_ship_title = 2131165414;
        /* added by JADX */
        public static final int check_order_ship_content = 2131165415;
        /* added by JADX */
        public static final int check_order_invoice_item_default = 2131165416;
        /* added by JADX */
        public static final int check_order_invoice_default = 2131165417;
        /* added by JADX */
        public static final int check_order_invoice_item = 2131165418;
        /* added by JADX */
        public static final int check_order_invoice_type = 2131165419;
        /* added by JADX */
        public static final int check_order_invoice_type_content = 2131165420;
        /* added by JADX */
        public static final int invoice_image_arrow = 2131165421;
        /* added by JADX */
        public static final int check_order_invoice_belongs_title = 2131165422;
        /* added by JADX */
        public static final int check_order_coupon_title = 2131165423;
        /* added by JADX */
        public static final int check_order_invoice_belongs_content = 2131165424;
        /* added by JADX */
        public static final int check_order_invoice_content_title = 2131165425;
        /* added by JADX */
        public static final int check_order_invoice_content_content = 2131165426;
        /* added by JADX */
        public static final int check_order_anount_title = 2131165427;
        /* added by JADX */
        public static final int check_order_anount = 2131165428;
        /* added by JADX */
        public static final int check_order_submit = 2131165429;
        /* added by JADX */
        public static final int empty_cart_view = 2131165430;
        /* added by JADX */
        public static final int search_relativelayout = 2131165431;
        /* added by JADX */
        public static final int search_editer = 2131165432;
        /* added by JADX */
        public static final int search_clearwords = 2131165433;
        /* added by JADX */
        public static final int search_cancel = 2131165434;
        /* added by JADX */
        public static final int search_ll = 2131165435;
        /* added by JADX */
        public static final int search_history = 2131165436;
        /* added by JADX */
        public static final int history_search_list = 2131165437;
        /* added by JADX */
        public static final int product_content = 2131165438;
        /* added by JADX */
        public static final int image_button_desc = 2131165439;
        /* added by JADX */
        public static final int image_button_recommend = 2131165440;
        /* added by JADX */
        public static final int cart_dialog_goods_count = 2131165441;
        /* added by JADX */
        public static final int cart_dialog_sub = 2131165442;
        /* added by JADX */
        public static final int cart_dialog_add = 2131165443;
        /* added by JADX */
        public static final int cart_dialog_update_count_positive = 2131165444;
        /* added by JADX */
        public static final int cart_dialog_cancle = 2131165445;
        /* added by JADX */
        public static final int cart_goods_item = 2131165446;
        /* added by JADX */
        public static final int cart_goods_item_checkbox = 2131165447;
        /* added by JADX */
        public static final int cart_goods_item_img = 2131165448;
        /* added by JADX */
        public static final int cart_goods_item_name = 2131165449;
        /* added by JADX */
        public static final int cart_goods_item_invalidtion = 2131165450;
        /* added by JADX */
        public static final int cart_goods_item_invalidation_msg = 2131165451;
        /* added by JADX */
        public static final int cart_goods_item_validation = 2131165452;
        /* added by JADX */
        public static final int cart_goods_item_amount_value = 2131165453;
        /* added by JADX */
        public static final int cart_mutiply = 2131165454;
        /* added by JADX */
        public static final int cart_goods_item_count_value = 2131165455;
        /* added by JADX */
        public static final int cart_go_to_the_goods_view = 2131165456;
        /* added by JADX */
        public static final int cate_item_list = 2131165457;
        /* added by JADX */
        public static final int cate_item_name = 2131165458;
        /* added by JADX */
        public static final int check_order_coupon_item = 2131165459;
        /* added by JADX */
        public static final int check_order_coupon_content = 2131165460;
        /* added by JADX */
        public static final int coupon_image_arrow = 2131165461;
        /* added by JADX */
        public static final int ll_data_loading = 2131165462;
        /* added by JADX */
        public static final int loading_progress_bar = 2131165463;
        /* added by JADX */
        public static final int loading_tip_txt = 2131165464;
        /* added by JADX */
        public static final int again_load = 2131165465;
        /* added by JADX */
        public static final int web_company_desc = 2131165466;
        /* added by JADX */
        public static final int coupon_item_list = 2131165467;
        /* added by JADX */
        public static final int copuon_item_name = 2131165468;
        /* added by JADX */
        public static final int copuon_item_time = 2131165469;
        /* added by JADX */
        public static final int empty_main = 2131165470;
        /* added by JADX */
        public static final int frame_cart = 2131165471;
        /* added by JADX */
        public static final int cart_cart_bar = 2131165472;
        /* added by JADX */
        public static final int cart_goods_item_checkbox_all = 2131165473;
        /* added by JADX */
        public static final int cart_goods_count = 2131165474;
        /* added by JADX */
        public static final int cart_goods_count_value = 2131165475;
        /* added by JADX */
        public static final int cart_amount = 2131165476;
        /* added by JADX */
        public static final int cart_amount_value = 2131165477;
        /* added by JADX */
        public static final int cart_goods_list = 2131165478;
        /* added by JADX */
        public static final int ll_product_cart_empty = 2131165479;
        /* added by JADX */
        public static final int go_to_the_promotions_btn = 2131165480;
        /* added by JADX */
        public static final int category_listView = 2131165481;
        /* added by JADX */
        public static final int frame_home_layout = 2131165482;
        /* added by JADX */
        public static final int lv_new_products_list = 2131165483;
        /* added by JADX */
        public static final int scrollview = 2131165484;
        /* added by JADX */
        public static final int alllinear = 2131165485;
        /* added by JADX */
        public static final int ll_kill_pro = 2131165486;
        /* added by JADX */
        public static final int tv_kill_pro_more = 2131165487;
        /* added by JADX */
        public static final int tv_kill_pro_hh = 2131165488;
        /* added by JADX */
        public static final int tv_kill_pro_mm = 2131165489;
        /* added by JADX */
        public static final int tv_kill_pro_time_suffix = 2131165490;
        /* added by JADX */
        public static final int gallery_kill_pro = 2131165491;
        /* added by JADX */
        public static final int ll_hot_pro = 2131165492;
        /* added by JADX */
        public static final int tv_hot_pro_more = 2131165493;
        /* added by JADX */
        public static final int gallery_hot_pro = 2131165494;
        /* added by JADX */
        public static final int ll_rebate_act = 2131165495;
        /* added by JADX */
        public static final int tv_rebate_act_more = 2131165496;
        /* added by JADX */
        public static final int gallery_rebate_act = 2131165497;
        /* added by JADX */
        public static final int tv_new_pro_more = 2131165498;
        /* added by JADX */
        public static final int timebuy_product_module = 2131165499;
        /* added by JADX */
        public static final int timebuy_goods_img = 2131165500;
        /* added by JADX */
        public static final int timebuy_info_area = 2131165501;
        /* added by JADX */
        public static final int timebuy_goods_title = 2131165502;
        /* added by JADX */
        public static final int timebuy_time_hh = 2131165503;
        /* added by JADX */
        public static final int timebuy_time_hh_title = 2131165504;
        /* added by JADX */
        public static final int timebuy_time_mm = 2131165505;
        /* added by JADX */
        public static final int timebuy_time_mm_title = 2131165506;
        /* added by JADX */
        public static final int timebuy_goods_name = 2131165507;
        /* added by JADX */
        public static final int timebuy_act_price_title = 2131165508;
        /* added by JADX */
        public static final int timebuy_act_price = 2131165509;
        /* added by JADX */
        public static final int timebuy_goods_price_title = 2131165510;
        /* added by JADX */
        public static final int timebuy_goods_price = 2131165511;
        /* added by JADX */
        public static final int hot_product_module = 2131165512;
        /* added by JADX */
        public static final int hot_goods_title_area = 2131165513;
        /* added by JADX */
        public static final int hot_goods_title = 2131165514;
        /* added by JADX */
        public static final int hot_goods_image1 = 2131165515;
        /* added by JADX */
        public static final int hot_goods_price_area1 = 2131165516;
        /* added by JADX */
        public static final int hot_goods_price1 = 2131165517;
        /* added by JADX */
        public static final int hot_goods_image2 = 2131165518;
        /* added by JADX */
        public static final int hot_goods_price_area2 = 2131165519;
        /* added by JADX */
        public static final int hot_goods_price2 = 2131165520;
        /* added by JADX */
        public static final int hot_goods_arrow_area = 2131165521;
        /* added by JADX */
        public static final int hot_goods_image3 = 2131165522;
        /* added by JADX */
        public static final int hot_goods_price_area3 = 2131165523;
        /* added by JADX */
        public static final int hot_goods_price3 = 2131165524;
        /* added by JADX */
        public static final int rebate_product_module = 2131165525;
        /* added by JADX */
        public static final int rebate_goods_title_area = 2131165526;
        /* added by JADX */
        public static final int rebate_goods_title = 2131165527;
        /* added by JADX */
        public static final int rebate_goods_image1 = 2131165528;
        /* added by JADX */
        public static final int rebate_goods_price_area1 = 2131165529;
        /* added by JADX */
        public static final int rebate_goods_price1 = 2131165530;
        /* added by JADX */
        public static final int rebate_goods_image2 = 2131165531;
        /* added by JADX */
        public static final int rebate_goods_price_area2 = 2131165532;
        /* added by JADX */
        public static final int rebate_goods_price2 = 2131165533;
        /* added by JADX */
        public static final int rebate_goods_arrow_area = 2131165534;
        /* added by JADX */
        public static final int rebate_goods_image3 = 2131165535;
        /* added by JADX */
        public static final int rebate_goods_price_area3 = 2131165536;
        /* added by JADX */
        public static final int rebate_goods_price3 = 2131165537;
        /* added by JADX */
        public static final int new_product_module = 2131165538;
        /* added by JADX */
        public static final int new_product_module_title = 2131165539;
        /* added by JADX */
        public static final int new_goods_image1 = 2131165540;
        /* added by JADX */
        public static final int new_goods_name1 = 2131165541;
        /* added by JADX */
        public static final int new_goods_price_title1 = 2131165542;
        /* added by JADX */
        public static final int new_goods_price1 = 2131165543;
        /* added by JADX */
        public static final int new_product_module_null = 2131165544;
        /* added by JADX */
        public static final int new_goods_image2 = 2131165545;
        /* added by JADX */
        public static final int new_goods_name2 = 2131165546;
        /* added by JADX */
        public static final int new_goods_price_title2 = 2131165547;
        /* added by JADX */
        public static final int new_goods_price2 = 2131165548;
        /* added by JADX */
        public static final int new_goods_image3 = 2131165549;
        /* added by JADX */
        public static final int new_goods_name3 = 2131165550;
        /* added by JADX */
        public static final int new_goods_price_title3 = 2131165551;
        /* added by JADX */
        public static final int new_goods_price3 = 2131165552;
        /* added by JADX */
        public static final int timebuy_product_module_navi = 2131165553;
        /* added by JADX */
        public static final int timebuy_title = 2131165554;
        /* added by JADX */
        public static final int timebuy_title_bottom_line = 2131165555;
        /* added by JADX */
        public static final int timebuy_time_ss = 2131165556;
        /* added by JADX */
        public static final int hot_product_module_navi = 2131165557;
        /* added by JADX */
        public static final int hot_title = 2131165558;
        /* added by JADX */
        public static final int hot_name = 2131165559;
        /* added by JADX */
        public static final int hot_title_line = 2131165560;
        /* added by JADX */
        public static final int hot_goods_image0 = 2131165561;
        /* added by JADX */
        public static final int hot_goods_price0 = 2131165562;
        /* added by JADX */
        public static final int hot_goods_image_area1 = 2131165563;
        /* added by JADX */
        public static final int hot_goods_image_area3 = 2131165564;
        /* added by JADX */
        public static final int hot_goods_image4 = 2131165565;
        /* added by JADX */
        public static final int hot_goods_price4 = 2131165566;
        /* added by JADX */
        public static final int hot_goods_image_area5 = 2131165567;
        /* added by JADX */
        public static final int hot_goods_image5 = 2131165568;
        /* added by JADX */
        public static final int hot_goods_price5 = 2131165569;
        /* added by JADX */
        public static final int hot_goods_image6 = 2131165570;
        /* added by JADX */
        public static final int hot_goods_price6 = 2131165571;
        /* added by JADX */
        public static final int rebate_product_module_navi = 2131165572;
        /* added by JADX */
        public static final int rebate_title = 2131165573;
        /* added by JADX */
        public static final int rebate_name = 2131165574;
        /* added by JADX */
        public static final int rebate_title_line = 2131165575;
        /* added by JADX */
        public static final int rebate_goods_image0 = 2131165576;
        /* added by JADX */
        public static final int rebate_goods_price0 = 2131165577;
        /* added by JADX */
        public static final int rebate_goods_image_area1 = 2131165578;
        /* added by JADX */
        public static final int rebate_goods_image_area3 = 2131165579;
        /* added by JADX */
        public static final int rebate_goods_image4 = 2131165580;
        /* added by JADX */
        public static final int rebate_goods_price4 = 2131165581;
        /* added by JADX */
        public static final int rebate_goods_image_area5 = 2131165582;
        /* added by JADX */
        public static final int rebate_goods_image5 = 2131165583;
        /* added by JADX */
        public static final int rebate_goods_price5 = 2131165584;
        /* added by JADX */
        public static final int rebate_goods_image6 = 2131165585;
        /* added by JADX */
        public static final int rebate_goods_price6 = 2131165586;
        /* added by JADX */
        public static final int new_product_module_navi = 2131165587;
        /* added by JADX */
        public static final int new_product_title = 2131165588;
        /* added by JADX */
        public static final int new_product_module_navi_value = 2131165589;
        /* added by JADX */
        public static final int new_product_name = 2131165590;
        /* added by JADX */
        public static final int new_product_title_line = 2131165591;
        /* added by JADX */
        public static final int icon_timebuy = 2131165592;
        /* added by JADX */
        public static final int mendian_module_navi = 2131165593;
        /* added by JADX */
        public static final int mendian_title = 2131165594;
        /* added by JADX */
        public static final int mendian_name = 2131165595;
        /* added by JADX */
        public static final int tv_mendian_more = 2131165596;
        /* added by JADX */
        public static final int mendian_module = 2131165597;
        /* added by JADX */
        public static final int mendian_image1 = 2131165598;
        /* added by JADX */
        public static final int mendian_name1 = 2131165599;
        /* added by JADX */
        public static final int mendian_image2 = 2131165600;
        /* added by JADX */
        public static final int mendian_name2 = 2131165601;
        /* added by JADX */
        public static final int mendian_image3 = 2131165602;
        /* added by JADX */
        public static final int mendian_name3 = 2131165603;
        /* added by JADX */
        public static final int member = 2131165604;
        /* added by JADX */
        public static final int login = 2131165605;
        /* added by JADX */
        public static final int register = 2131165606;
        /* added by JADX */
        public static final int more_clear_cache = 2131165607;
        /* added by JADX */
        public static final int tv_cache_size = 2131165608;
        /* added by JADX */
        public static final int more_check_version = 2131165609;
        /* added by JADX */
        public static final int about_version = 2131165610;
        /* added by JADX */
        public static final int more_about = 2131165611;
        /* added by JADX */
        public static final int head_contentLayout = 2131165612;
        /* added by JADX */
        public static final int head_arrowImageView = 2131165613;
        /* added by JADX */
        public static final int head_progressBar = 2131165614;
        /* added by JADX */
        public static final int head_tipsTextView = 2131165615;
        /* added by JADX */
        public static final int head_lastUpdatedTextView = 2131165616;
        /* added by JADX */
        public static final int iv_hot_pro_grid = 2131165617;
        /* added by JADX */
        public static final int tv_hot_pro_price = 2131165618;
        /* added by JADX */
        public static final int imagezoomdialog_view_switcher = 2131165619;
        /* added by JADX */
        public static final int imagezoomdialog_progress = 2131165620;
        /* added by JADX */
        public static final int imagezoomdialog_image = 2131165621;
        /* added by JADX */
        public static final int ad_imgs = 2131165622;
        /* added by JADX */
        public static final int point_linear = 2131165623;
        /* added by JADX */
        public static final int invoice_person_wraper = 2131165624;
        /* added by JADX */
        public static final int invoice_person = 2131165625;
        /* added by JADX */
        public static final int invoice_company_wraper = 2131165626;
        /* added by JADX */
        public static final int invoice_company = 2131165627;
        /* added by JADX */
        public static final int invoice_company_name = 2131165628;
        /* added by JADX */
        public static final int invoice_list = 2131165629;
        /* added by JADX */
        public static final int invoice_item_list = 2131165630;
        /* added by JADX */
        public static final int invoice_item_checkbox = 2131165631;
        /* added by JADX */
        public static final int title = 2131165632;
        /* added by JADX */
        public static final int iv_kill_pro_grid = 2131165633;
        /* added by JADX */
        public static final int tv_kill_pro_price = 2131165634;
        /* added by JADX */
        public static final int listview_foot_progress = 2131165635;
        /* added by JADX */
        public static final int listview_foot_more = 2131165636;
        /* added by JADX */
        public static final int ll_listview_header = 2131165637;
        /* added by JADX */
        public static final int load_more_parent = 2131165638;
        /* added by JADX */
        public static final int pb_load_more = 2131165639;
        /* added by JADX */
        public static final int btn_load_more = 2131165640;
        /* added by JADX */
        public static final int et_login_username = 2131165641;
        /* added by JADX */
        public static final int et_login_pwd = 2131165642;
        /* added by JADX */
        public static final int et_login_auto = 2131165643;
        /* added by JADX */
        public static final int rl_user_register = 2131165644;
        /* added by JADX */
        public static final int main_scrolllayout = 2131165645;
        /* added by JADX */
        public static final int main_linearlayout_footer = 2131165646;
        /* added by JADX */
        public static final int main_footbar_home = 2131165647;
        /* added by JADX */
        public static final int main_footbar_category = 2131165648;
        /* added by JADX */
        public static final int main_footbar_cart = 2131165649;
        /* added by JADX */
        public static final int main_footbar_member = 2131165650;
        /* added by JADX */
        public static final int main_footbar_setting = 2131165651;
        /* added by JADX */
        public static final int main_head_logo = 2131165652;
        /* added by JADX */
        public static final int main_head_title = 2131165653;
        /* added by JADX */
        public static final int main_head_progress = 2131165654;
        /* added by JADX */
        public static final int main_head_search = 2131165655;
        /* added by JADX */
        public static final int head_back_btn = 2131165656;
        /* added by JADX */
        public static final int head_right_btn = 2131165657;
        /* added by JADX */
        public static final int head_right_btn_ask_question = 2131165658;
        /* added by JADX */
        public static final int head_login_btn = 2131165659;
        /* added by JADX */
        public static final int head_logout_btn = 2131165660;
        /* added by JADX */
        public static final int cart_del_button = 2131165661;
        /* added by JADX */
        public static final int go_to_the_checkout = 2131165662;
        /* added by JADX */
        public static final int invoice_select_btn = 2131165663;
        /* added by JADX */
        public static final int address_add_btn = 2131165664;
        /* added by JADX */
        public static final int address_save_btn = 2131165665;
        /* added by JADX */
        public static final int head_cate_back_btn = 2131165666;
        /* added by JADX */
        public static final int search_hot = 2131165667;
        /* added by JADX */
        public static final int hot_keyword_search_list = 2131165668;
        /* added by JADX */
        public static final int relativeLayout1 = 2131165669;
        /* added by JADX */
        public static final int user_head_image = 2131165670;
        /* added by JADX */
        public static final int user_name = 2131165671;
        /* added by JADX */
        public static final int user_level_textview = 2131165672;
        /* added by JADX */
        public static final int user_level = 2131165673;
        /* added by JADX */
        public static final int user_unpay_order_tablerow = 2131165674;
        /* added by JADX */
        public static final int user_unpay_order_textview = 2131165675;
        /* added by JADX */
        public static final int user_unpay_order_num_textview = 2131165676;
        /* added by JADX */
        public static final int user_unpay_order_imageview = 2131165677;
        /* added by JADX */
        public static final int user_all_order_tablerow = 2131165678;
        /* added by JADX */
        public static final int user_all_order_textview = 2131165679;
        /* added by JADX */
        public static final int user_all_order_num_textview = 2131165680;
        /* added by JADX */
        public static final int user_all_order_imageview = 2131165681;
        /* added by JADX */
        public static final int tableRow4 = 2131165682;
        /* added by JADX */
        public static final int user_message_textview = 2131165683;
        /* added by JADX */
        public static final int user_message_num_textview = 2131165684;
        /* added by JADX */
        public static final int user_message_imageview = 2131165685;
        /* added by JADX */
        public static final int user_address_tablerow = 2131165686;
        /* added by JADX */
        public static final int user_address_textview = 2131165687;
        /* added by JADX */
        public static final int user_address_imageview = 2131165688;
        /* added by JADX */
        public static final int map = 2131165689;
        /* added by JADX */
        public static final int LinearLayoutLayout_index_bottom = 2131165690;
        /* added by JADX */
        public static final int pre_index = 2131165691;
        /* added by JADX */
        public static final int next_index = 2131165692;
        /* added by JADX */
        public static final int order_detail_amount_label = 2131165693;
        /* added by JADX */
        public static final int order_detail_amount_value = 2131165694;
        /* added by JADX */
        public static final int order_detail_ship = 2131165695;
        /* added by JADX */
        public static final int order_detail_list = 2131165696;
        /* added by JADX */
        public static final int order_detail_order_sn = 2131165697;
        /* added by JADX */
        public static final int order_detail_order_sn_label = 2131165698;
        /* added by JADX */
        public static final int order_detail_order_sn_value = 2131165699;
        /* added by JADX */
        public static final int order_detail_create_time = 2131165700;
        /* added by JADX */
        public static final int order_detail_create_time_label = 2131165701;
        /* added by JADX */
        public static final int order_detail_create_time_value = 2131165702;
        /* added by JADX */
        public static final int order_detail_receiver = 2131165703;
        /* added by JADX */
        public static final int order_detail_receiver_label = 2131165704;
        /* added by JADX */
        public static final int order_detail_receiver_value = 2131165705;
        /* added by JADX */
        public static final int order_detail_address = 2131165706;
        /* added by JADX */
        public static final int order_detail_address_label = 2131165707;
        /* added by JADX */
        public static final int order_detail_address_value = 2131165708;
        /* added by JADX */
        public static final int order_detail_coupon = 2131165709;
        /* added by JADX */
        public static final int order_detail_coupon_label = 2131165710;
        /* added by JADX */
        public static final int order_detail_coupon_value = 2131165711;
        /* added by JADX */
        public static final int order_detail_invoice_type = 2131165712;
        /* added by JADX */
        public static final int order_detail_invoice_title = 2131165713;
        /* added by JADX */
        public static final int order_detail_invoice_title_label = 2131165714;
        /* added by JADX */
        public static final int order_detail_invoice_title_value = 2131165715;
        /* added by JADX */
        public static final int order_detail_invoice_content = 2131165716;
        /* added by JADX */
        public static final int order_detail_invoice_content_label = 2131165717;
        /* added by JADX */
        public static final int order_detail_invoice_content_value = 2131165718;
        /* added by JADX */
        public static final int order_detail_item_img = 2131165719;
        /* added by JADX */
        public static final int order_detail_item_name = 2131165720;
        /* added by JADX */
        public static final int order_detail_item_price = 2131165721;
        /* added by JADX */
        public static final int order_detail_item_pic = 2131165722;
        /* added by JADX */
        public static final int order_detail_item_num = 2131165723;
        /* added by JADX */
        public static final int check_order_product_item = 2131165724;
        /* added by JADX */
        public static final int order_item_img = 2131165725;
        /* added by JADX */
        public static final int order_item_name = 2131165726;
        /* added by JADX */
        public static final int order_item_price = 2131165727;
        /* added by JADX */
        public static final int order_item_pre_count = 2131165728;
        /* added by JADX */
        public static final int order_item_count = 2131165729;
        /* added by JADX */
        public static final int imageView1 = 2131165730;
        /* added by JADX */
        public static final int product_attr = 2131165731;
        /* added by JADX */
        public static final int txt_tip_attr = 2131165732;
        /* added by JADX */
        public static final int list_stock_attr = 2131165733;
        /* added by JADX */
        public static final int strut = 2131165734;
        /* added by JADX */
        public static final int txt_attr = 2131165735;
        /* added by JADX */
        public static final int txt_attr_price = 2131165736;
        /* added by JADX */
        public static final int frame_expandable_listview_attrs = 2131165737;
        /* added by JADX */
        public static final int product_relativelayout_header = 2131165738;
        /* added by JADX */
        public static final int product_base_info = 2131165739;
        /* added by JADX */
        public static final int img_products = 2131165740;
        /* added by JADX */
        public static final int img_dots = 2131165741;
        /* added by JADX */
        public static final int txt_product_name = 2131165742;
        /* added by JADX */
        public static final int txt_product_price = 2131165743;
        /* added by JADX */
        public static final int txt_product_market_price = 2131165744;
        /* added by JADX */
        public static final int txt_product_price_discount = 2131165745;
        /* added by JADX */
        public static final int goods_split_attr = 2131165746;
        /* added by JADX */
        public static final int txt_split_attr = 2131165747;
        /* added by JADX */
        public static final int txt_split_val = 2131165748;
        /* added by JADX */
        public static final int stock_split_attr = 2131165749;
        /* added by JADX */
        public static final int txt_stock_attr = 2131165750;
        /* added by JADX */
        public static final int txt_stock_val = 2131165751;
        /* added by JADX */
        public static final int txt_freight = 2131165752;
        /* added by JADX */
        public static final int btn_add_cart = 2131165753;
        /* added by JADX */
        public static final int image_button_discuss = 2131165754;
        /* added by JADX */
        public static final int image_button_consult = 2131165755;
        /* added by JADX */
        public static final int gv_promotion_product_list = 2131165756;
        /* added by JADX */
        public static final int ll_loading_more = 2131165757;
        /* added by JADX */
        public static final int pb_loading = 2131165758;
        /* added by JADX */
        public static final int tv_loading_info = 2131165759;
        /* added by JADX */
        public static final int iv_promot_product = 2131165760;
        /* added by JADX */
        public static final int tv_promot_info = 2131165761;
        /* added by JADX */
        public static final int tv_price = 2131165762;
        /* added by JADX */
        public static final int tv_original_price = 2131165763;
        /* added by JADX */
        public static final int tv_discount_product = 2131165764;
        /* added by JADX */
        public static final int product_consult = 2131165765;
        /* added by JADX */
        public static final int ll_product_consult_empty = 2131165766;
        /* added by JADX */
        public static final int lv_product_consult_list = 2131165767;
        /* added by JADX */
        public static final int tv_product_consult_item_question = 2131165768;
        /* added by JADX */
        public static final int tv_product_consult_item_username = 2131165769;
        /* added by JADX */
        public static final int tv_product_consult_item_time = 2131165770;
        /* added by JADX */
        public static final int tv_product_consult_item_answer = 2131165771;
        /* added by JADX */
        public static final int pro_goods_desc = 2131165772;
        /* added by JADX */
        public static final int web_goods_desc = 2131165773;
        /* added by JADX */
        public static final int ll_product_desc_empty = 2131165774;
        /* added by JADX */
        public static final int product_discuss = 2131165775;
        /* added by JADX */
        public static final int ll_product_discuss_empty = 2131165776;
        /* added by JADX */
        public static final int lv_product_discuss_list = 2131165777;
        /* added by JADX */
        public static final int tv_product_discuss_item_username = 2131165778;
        /* added by JADX */
        public static final int rb_product_discuss_item_ratingBar = 2131165779;
        /* added by JADX */
        public static final int tv_product_discuss_item_time = 2131165780;
        /* added by JADX */
        public static final int tv_product_discuss_item_title = 2131165781;
        /* added by JADX */
        public static final int tv_product_discuss_item_content = 2131165782;
        /* added by JADX */
        public static final int viewer = 2131165783;
        /* added by JADX */
        public static final int search_tab_ll = 2131165784;
        /* added by JADX */
        public static final int search_product_sales = 2131165785;
        /* added by JADX */
        public static final int search_product_price = 2131165786;
        /* added by JADX */
        public static final int search_product_price_tv = 2131165787;
        /* added by JADX */
        public static final int search_product_price_image_up = 2131165788;
        /* added by JADX */
        public static final int search_product_price_image_down = 2131165789;
        /* added by JADX */
        public static final int search_product_time = 2131165790;
        /* added by JADX */
        public static final int frame_listview_product = 2131165791;
        /* added by JADX */
        public static final int product_img = 2131165792;
        /* added by JADX */
        public static final int product_title = 2131165793;
        /* added by JADX */
        public static final int product_price = 2131165794;
        /* added by JADX */
        public static final int product_promote = 2131165795;
        /* added by JADX */
        public static final int product_mprice = 2131165796;
        /* added by JADX */
        public static final int textView1 = 2131165797;
        /* added by JADX */
        public static final int order_act = 2131165798;
        /* added by JADX */
        public static final int txt_product_order_act = 2131165799;
        /* added by JADX */
        public static final int ll_product_recommend = 2131165800;
        /* added by JADX */
        public static final int gv_product_list = 2131165801;
        /* added by JADX */
        public static final int ll_product_recommend_empty = 2131165802;
        /* added by JADX */
        public static final int lv_seckill_list = 2131165803;
        /* added by JADX */
        public static final int tv_seckill_time = 2131165804;
        /* added by JADX */
        public static final int iv_seckill_product = 2131165805;
        /* added by JADX */
        public static final int tv_seckill_product_name = 2131165806;
        /* added by JADX */
        public static final int tv_seckill_price = 2131165807;
        /* added by JADX */
        public static final int btn_seckill_instance = 2131165808;
        /* added by JADX */
        public static final int tv_discount = 2131165809;
        /* added by JADX */
        public static final int iv_rebate_act_grid = 2131165810;
        /* added by JADX */
        public static final int tv_rebate_act_price = 2131165811;
        /* added by JADX */
        public static final int et_reg_username = 2131165812;
        /* added by JADX */
        public static final int et_reg_pwd1 = 2131165813;
        /* added by JADX */
        public static final int et_reg_pwd2 = 2131165814;
        /* added by JADX */
        public static final int btn_register = 2131165815;
        /* added by JADX */
        public static final int search_listitem_title = 2131165816;
        /* added by JADX */
        public static final int search_listitem_ll = 2131165817;
        /* added by JADX */
        public static final int search_listitem_author = 2131165818;
        /* added by JADX */
        public static final int search_listitem_date = 2131165819;
        /* added by JADX */
        public static final int success_title = 2131165820;
        /* added by JADX */
        public static final int tableRow1 = 2131165821;
        /* added by JADX */
        public static final int order_sn = 2131165822;
        /* added by JADX */
        public static final int tableRow2 = 2131165823;
        /* added by JADX */
        public static final int textView2 = 2131165824;
        /* added by JADX */
        public static final int order_amount = 2131165825;
        /* added by JADX */
        public static final int tableRow3 = 2131165826;
        /* added by JADX */
        public static final int textView3 = 2131165827;
        /* added by JADX */
        public static final int order_pay = 2131165828;
        /* added by JADX */
        public static final int order_warm_tips = 2131165829;
        /* added by JADX */
        public static final int online_pay_btn = 2131165830;
        /* added by JADX */
        public static final int LanchPayBtn = 2131165831;
        /* added by JADX */
        public static final int SubmitOrderBtn = 2131165832;
        /* added by JADX */
        public static final int update_progress = 2131165833;
        /* added by JADX */
        public static final int update_progress_text = 2131165834;
        /* added by JADX */
        public static final int order_all_after = 2131165835;
        /* added by JADX */
        public static final int order_all_before = 2131165836;
        /* added by JADX */
        public static final int user_all_order_listview = 2131165837;
        /* added by JADX */
        public static final int order_empty_list = 2131165838;
        /* added by JADX */
        public static final int product_imageview = 2131165839;
        /* added by JADX */
        public static final int order_sn_textview = 2131165840;
        /* added by JADX */
        public static final int order_time_textview = 2131165841;
        /* added by JADX */
        public static final int order_time = 2131165842;
        /* added by JADX */
        public static final int order_amount_textview = 2131165843;
        /* added by JADX */
        public static final int order_ship_amount = 2131165844;
        /* added by JADX */
        public static final int order_count = 2131165845;
        /* added by JADX */
        public static final int order_ship_btn = 2131165846;
        /* added by JADX */
        public static final int order_pay_btn = 2131165847;
        /* added by JADX */
        public static final int order_receiving_btn = 2131165848;
        /* added by JADX */
        public static final int user_unpay_order_listview = 2131165849;
        /* added by JADX */
        public static final int action_settings = 2131165850;
    }

    public final class layout {
        public static final int upomp_lthj_about = 2130903126;
        public static final int upomp_lthj_bankcard_item = 2130903127;
        public static final int upomp_lthj_bankcard_itemexpad = 2130903128;
        public static final int upomp_lthj_bankcard_list = 2130903129;
        public static final int upomp_lthj_bindcard_home = 2130903130;
        public static final int upomp_lthj_bindcard_next = 2130903131;
        public static final int upomp_lthj_cardinfo_tip = 2130903132;
        public static final int upomp_lthj_changemobile = 2130903133;
        public static final int upomp_lthj_changepassword = 2130903134;
        public static final int upomp_lthj_commonpay = 2130903135;
        public static final int upomp_lthj_custominput = 2130903136;
        public static final int upomp_lthj_findpwd_home = 2130903137;
        public static final int upomp_lthj_findpwd_next = 2130903138;
        public static final int upomp_lthj_homeaccount = 2130903139;
        public static final int upomp_lthj_homecardpay = 2130903140;
        public static final int upomp_lthj_hometradeinfo = 2130903141;
        public static final int upomp_lthj_index = 2130903142;
        public static final int upomp_lthj_keyboard = 2130903143;
        public static final int upomp_lthj_keyboard_letter = 2130903144;
        public static final int upomp_lthj_keyboard_num = 2130903145;
        public static final int upomp_lthj_keyboard_sign = 2130903146;
        public static final int upomp_lthj_lineframe = 2130903147;
        public static final int upomp_lthj_myinfo = 2130903148;
        public static final int upomp_lthj_onebtn_progress = 2130903149;
        public static final int upomp_lthj_quick_bind_result = 2130903150;
        public static final int upomp_lthj_quick_bindcard = 2130903151;
        public static final int upomp_lthj_quick_reg_confirm = 2130903152;
        public static final int upomp_lthj_quick_reg_result = 2130903153;
        public static final int upomp_lthj_quick_register = 2130903154;
        public static final int upomp_lthj_quickpay_hascard = 2130903155;
        public static final int upomp_lthj_quickpay_nocard = 2130903156;
        public static final int upomp_lthj_quickpay_userinfo = 2130903157;
        public static final int upomp_lthj_savecardpay = 2130903158;
        public static final int upomp_lthj_splash = 2130903159;
        public static final int upomp_lthj_supportcard = 2130903160;
        public static final int upomp_lthj_supportcard_bankitem = 2130903161;
        public static final int upomp_lthj_traderesult = 2130903162;
        public static final int upomp_lthj_twobtn_progress = 2130903163;
        public static final int upomp_lthj_user_protocol = 2130903164;
        public static final int upomp_lthj_userprotocal_item = 2130903165;
        public static final int upomp_lthj_validatcodeview = 2130903166;
        /* added by JADX */
        public static final int about = 2130903040;
        /* added by JADX */
        public static final int address_add = 2130903041;
        /* added by JADX */
        public static final int address_item = 2130903042;
        /* added by JADX */
        public static final int address_list = 2130903043;
        /* added by JADX */
        public static final int attrs_listitem = 2130903044;
        /* added by JADX */
        public static final int attrsitem_listitem = 2130903045;
        /* added by JADX */
        public static final int beta_check = 2130903046;
        /* added by JADX */
        public static final int beta_main_search = 2130903047;
        /* added by JADX */
        public static final int beta_product = 2130903048;
        /* added by JADX */
        public static final int beta_product_bottom = 2130903049;
        /* added by JADX */
        public static final int breakline = 2130903050;
        /* added by JADX */
        public static final int cart_dialog = 2130903051;
        /* added by JADX */
        public static final int cart_goods_item = 2130903052;
        /* added by JADX */
        public static final int category_item = 2130903053;
        /* added by JADX */
        public static final int check = 2130903054;
        /* added by JADX */
        public static final int click_load = 2130903055;
        /* added by JADX */
        public static final int company_desc = 2130903056;
        /* added by JADX */
        public static final int coupon_item = 2130903057;
        /* added by JADX */
        public static final int empty_main = 2130903058;
        /* added by JADX */
        public static final int frame_cart = 2130903059;
        /* added by JADX */
        public static final int frame_category = 2130903060;
        /* added by JADX */
        public static final int frame_home = 2130903061;
        /* added by JADX */
        public static final int frame_home_head = 2130903062;
        /* added by JADX */
        public static final int frame_home_headv2 = 2130903063;
        /* added by JADX */
        public static final int frame_home_headv3 = 2130903064;
        /* added by JADX */
        public static final int frame_home_headv4 = 2130903065;
        /* added by JADX */
        public static final int frame_member = 2130903066;
        /* added by JADX */
        public static final int frame_more = 2130903067;
        /* added by JADX */
        public static final int home_head = 2130903068;
        /* added by JADX */
        public static final int hot_pro_grid_item = 2130903069;
        /* added by JADX */
        public static final int image_zoom_dialog = 2130903070;
        /* added by JADX */
        public static final int imgs_looper = 2130903071;
        /* added by JADX */
        public static final int imgs_looperv2 = 2130903072;
        /* added by JADX */
        public static final int imgs_looperv3 = 2130903073;
        /* added by JADX */
        public static final int imgs_looperv4 = 2130903074;
        /* added by JADX */
        public static final int invoice = 2130903075;
        /* added by JADX */
        public static final int invoice_item = 2130903076;
        /* added by JADX */
        public static final int item = 2130903077;
        /* added by JADX */
        public static final int kill_pro_grid_item = 2130903078;
        /* added by JADX */
        public static final int listview_footer = 2130903079;
        /* added by JADX */
        public static final int listview_header = 2130903080;
        /* added by JADX */
        public static final int load_more = 2130903081;
        /* added by JADX */
        public static final int login = 2130903082;
        /* added by JADX */
        public static final int main = 2130903083;
        /* added by JADX */
        public static final int main_footer = 2130903084;
        /* added by JADX */
        public static final int main_header = 2130903085;
        /* added by JADX */
        public static final int main_search = 2130903086;
        /* added by JADX */
        public static final int main_search_listitem = 2130903087;
        /* added by JADX */
        public static final int member = 2130903088;
        /* added by JADX */
        public static final int mendian_map = 2130903089;
        /* added by JADX */
        public static final int order_detail = 2130903090;
        /* added by JADX */
        public static final int order_detail_item = 2130903091;
        /* added by JADX */
        public static final int order_product_item = 2130903092;
        /* added by JADX */
        public static final int preference = 2130903093;
        /* added by JADX */
        public static final int preference_icon_more = 2130903094;
        /* added by JADX */
        public static final int product = 2130903095;
        /* added by JADX */
        public static final int product_attr = 2130903096;
        /* added by JADX */
        public static final int product_attr_item = 2130903097;
        /* added by JADX */
        public static final int product_attrs = 2130903098;
        /* added by JADX */
        public static final int product_attrs_header = 2130903099;
        /* added by JADX */
        public static final int product_base = 2130903100;
        /* added by JADX */
        public static final int product_bottom = 2130903101;
        /* added by JADX */
        public static final int product_common_list = 2130903102;
        /* added by JADX */
        public static final int product_common_list_item = 2130903103;
        /* added by JADX */
        public static final int product_consult = 2130903104;
        /* added by JADX */
        public static final int product_consult_item = 2130903105;
        /* added by JADX */
        public static final int product_desc = 2130903106;
        /* added by JADX */
        public static final int product_discuss = 2130903107;
        /* added by JADX */
        public static final int product_discuss_item = 2130903108;
        /* added by JADX */
        public static final int product_imgs_gallery = 2130903109;
        /* added by JADX */
        public static final int product_list = 2130903110;
        /* added by JADX */
        public static final int product_listitem = 2130903111;
        /* added by JADX */
        public static final int product_listitem_v4 = 2130903112;
        /* added by JADX */
        public static final int product_order_act = 2130903113;
        /* added by JADX */
        public static final int product_recommend = 2130903114;
        /* added by JADX */
        public static final int product_seckill_list = 2130903115;
        /* added by JADX */
        public static final int product_seckill_list_item = 2130903116;
        /* added by JADX */
        public static final int progress_bar_rotate = 2130903117;
        /* added by JADX */
        public static final int pull_to_refresh_head = 2130903118;
        /* added by JADX */
        public static final int rebate_act_grid_item = 2130903119;
        /* added by JADX */
        public static final int register = 2130903120;
        /* added by JADX */
        public static final int search_listitem = 2130903121;
        /* added by JADX */
        public static final int start = 2130903122;
        /* added by JADX */
        public static final int success = 2130903123;
        /* added by JADX */
        public static final int test_upomp_pay = 2130903124;
        /* added by JADX */
        public static final int update_progress = 2130903125;
        /* added by JADX */
        public static final int user_order_all = 2130903167;
        /* added by JADX */
        public static final int user_order_item = 2130903168;
        /* added by JADX */
        public static final int user_order_unpay = 2130903169;
    }

    public final class raw {
        public static final int upomp_lthj_config_formal = 2131034114;
        public static final int upomp_lthj_config_test = 2131034115;
        /* added by JADX */
        public static final int newdatatoast = 2131034112;
        /* added by JADX */
        public static final int notificationsound = 2131034113;
    }

    public final class string {
        public static final int app_name = 2131230721;
        public static final int hello = 2131230720;
        public static final int upomp_lthj_about_copyright = 2131230725;
        public static final int upomp_lthj_account_manage = 2131230790;
        public static final int upomp_lthj_add_card = 2131230832;
        public static final int upomp_lthj_after_getmobilemacAgain = 2131230773;
        public static final int upomp_lthj_app_quitNotice_msg = 2131230819;
        public static final int upomp_lthj_back = 2131230729;
        public static final int upomp_lthj_back_pay = 2131230810;
        public static final int upomp_lthj_backcard_manage = 2131230804;
        public static final int upomp_lthj_backpage = 2131230739;
        public static final int upomp_lthj_backtomerchant = 2131230777;
        public static final int upomp_lthj_bind_fail = 2131230840;
        public static final int upomp_lthj_bind_now = 2131230811;
        public static final int upomp_lthj_bind_num = 2131230802;
        public static final int upomp_lthj_bind_result_prompt = 2131230784;
        public static final int upomp_lthj_bind_success = 2131230839;
        public static final int upomp_lthj_cancel = 2131230731;
        public static final int upomp_lthj_card_info = 2131230794;
        public static final int upomp_lthj_card_mobile_num = 2131230764;
        public static final int upomp_lthj_change_mobile = 2131230808;
        public static final int upomp_lthj_change_mobile_success = 2131230809;
        public static final int upomp_lthj_change_pwd = 2131230805;
        public static final int upomp_lthj_change_pwd_success = 2131230806;
        public static final int upomp_lthj_check_support = 2131230828;
        public static final int upomp_lthj_choose_pay_card = 2131230821;
        public static final int upomp_lthj_choose_safeask = 2131230824;
        public static final int upomp_lthj_close = 2131230820;
        public static final int upomp_lthj_company = 2131230722;
        public static final int upomp_lthj_confirm_pay = 2131230769;
        public static final int upomp_lthj_confirm_pwd = 2131230801;
        public static final int upomp_lthj_creditcard = 2131230838;
        public static final int upomp_lthj_custom_safeask = 2131230825;
        public static final int upomp_lthj_cvn = 2131230755;
        public static final int upomp_lthj_cvn2_hint = 2131230774;
        public static final int upomp_lthj_date_hint = 2131230775;
        public static final int upomp_lthj_debitcard = 2131230837;
        public static final int upomp_lthj_done = 2131230786;
        public static final int upomp_lthj_emailError_Format_prompt = 2131230851;
        public static final int upomp_lthj_ente_answer = 2131230816;
        public static final int upomp_lthj_ente_cardmobile = 2131230814;
        public static final int upomp_lthj_ente_confirm_pwd = 2131230817;
        public static final int upomp_lthj_ente_new_mobile = 2131230823;
        public static final int upomp_lthj_ente_new_pwd = 2131230815;
        public static final int upomp_lthj_ente_username = 2131230813;
        public static final int upomp_lthj_exit_pay = 2131230726;
        public static final int upomp_lthj_forget_password = 2131230795;
        public static final int upomp_lthj_get_mac = 2131230771;
        public static final int upomp_lthj_getting_mac = 2131230772;
        public static final int upomp_lthj_have_no_initfile = 2131230753;
        public static final int upomp_lthj_home = 2131230727;
        public static final int upomp_lthj_input_char_hint = 2131230767;
        public static final int upomp_lthj_keyboard_title = 2131230732;
        public static final int upomp_lthj_letter = 2131230733;
        public static final int upomp_lthj_login = 2131230730;
        public static final int upomp_lthj_loginpwd = 2131230789;
        public static final int upomp_lthj_logout_prompt = 2131230829;
        public static final int upomp_lthj_merchantId_Empty_prompt = 2131230741;
        public static final int upomp_lthj_merchantId_Length_prompt = 2131230742;
        public static final int upomp_lthj_merchantName_Empty_prompt = 2131230743;
        public static final int upomp_lthj_merchantOrderAmt_Digits_prompt = 2131230746;
        public static final int upomp_lthj_merchantOrderAmt_Empty_prompt = 2131230745;
        public static final int upomp_lthj_merchantOrderId_Empty_prompt = 2131230744;
        public static final int upomp_lthj_merchantOrderTime_Empty_prompt = 2131230747;
        public static final int upomp_lthj_merchantOrderTime_error_prompt = 2131230748;
        public static final int upomp_lthj_merchantXml_Format_prompt = 2131230749;
        public static final int upomp_lthj_merchantXml_HashFormat_prompt = 2131230751;
        public static final int upomp_lthj_merchantXml_MD5Error_prompt = 2131230752;
        public static final int upomp_lthj_merchantXml_ReadError_prompt = 2131230750;
        public static final int upomp_lthj_merchant_data_empty_prompt = 2131230740;
        public static final int upomp_lthj_mobile = 2131230796;
        public static final int upomp_lthj_mobileMacError_Length_prompt = 2131230850;
        public static final int upomp_lthj_mobile_mac_hint = 2131230770;
        public static final int upomp_lthj_multiple_user_login_tip = 2131230826;
        public static final int upomp_lthj_myinfo = 2131230803;
        public static final int upomp_lthj_new_pwd = 2131230799;
        public static final int upomp_lthj_next = 2131230728;
        public static final int upomp_lthj_nextpage = 2131230738;
        public static final int upomp_lthj_no_bind = 2131230812;
        public static final int upomp_lthj_no_save = 2131230793;
        public static final int upomp_lthj_no_support_card = 2131230827;
        public static final int upomp_lthj_num = 2131230734;
        public static final int upomp_lthj_ok = 2131230818;
        public static final int upomp_lthj_old_mobile = 2131230822;
        public static final int upomp_lthj_old_pwd = 2131230800;
        public static final int upomp_lthj_password = 2131230756;
        public static final int upomp_lthj_password_hint = 2131230766;
        public static final int upomp_lthj_pay_fail = 2131230783;
        public static final int upomp_lthj_pay_success = 2131230782;
        public static final int upomp_lthj_pin_hint = 2131230768;
        public static final int upomp_lthj_quick_bind_prompt = 2131230781;
        public static final int upomp_lthj_quick_reg_fail_prompt = 2131230780;
        public static final int upomp_lthj_quick_reg_prompt = 2131230778;
        public static final int upomp_lthj_rebind = 2131230841;
        public static final int upomp_lthj_reg_now = 2131230785;
        public static final int upomp_lthj_reg_safe_prompt = 2131230779;
        public static final int upomp_lthj_repay = 2131230776;
        public static final int upomp_lthj_reset_pwd_success = 2131230807;
        public static final int upomp_lthj_safe_ask = 2131230797;
        public static final int upomp_lthj_safe_ask_default = 2131230787;
        public static final int upomp_lthj_safe_asw = 2131230798;
        public static final int upomp_lthj_safe_prompt = 2131230760;
        public static final int upomp_lthj_save_account = 2131230792;
        public static final int upomp_lthj_secureinfo = 2131230757;
        public static final int upomp_lthj_sendMessageLose = 2131230759;
        public static final int upomp_lthj_sendMessageSuccess = 2131230758;
        public static final int upomp_lthj_session_timeout_tip = 2131230842;
        public static final int upomp_lthj_set_default = 2131230835;
        public static final int upomp_lthj_set_default_prompt = 2131230831;
        public static final int upomp_lthj_sign = 2131230735;
        public static final int upomp_lthj_str_no = 2131230736;
        public static final int upomp_lthj_str_ok = 2131230737;
        public static final int upomp_lthj_three_cardtype = 2131230763;
        public static final int upomp_lthj_two_card_type = 2131230833;
        public static final int upomp_lthj_unbind = 2131230836;
        public static final int upomp_lthj_unbind_prompt = 2131230830;
        public static final int upomp_lthj_use_card = 2131230762;
        public static final int upomp_lthj_use_quick = 2131230761;
        public static final int upomp_lthj_use_thecard = 2131230834;
        public static final int upomp_lthj_username = 2131230788;
        public static final int upomp_lthj_username_hint = 2131230765;
        public static final int upomp_lthj_validateBankPassWord_Length_prompt = 2131230864;
        public static final int upomp_lthj_validateCVN2_Empty_prompt = 2131230880;
        public static final int upomp_lthj_validateCVN2_Format_prompt = 2131230881;
        public static final int upomp_lthj_validateCardPanPassword_Empty_prompt = 2131230887;
        public static final int upomp_lthj_validateCardPan_Empty_prompt = 2131230886;
        public static final int upomp_lthj_validateChars_Format_prompt = 2131230845;
        public static final int upomp_lthj_validateChars_Format_prompt2 = 2131230846;
        public static final int upomp_lthj_validateEmail_Empty_prompt = 2131230872;
        public static final int upomp_lthj_validateEmail_Format_prompt = 2131230873;
        public static final int upomp_lthj_validateExpiryMonth_Empty_prompt = 2131230878;
        public static final int upomp_lthj_validateExpiryYear_Empty_prompt = 2131230879;
        public static final int upomp_lthj_validateImageCode_Empty_prompt = 2131230870;
        public static final int upomp_lthj_validateImageCode_Error_prompt = 2131230871;
        public static final int upomp_lthj_validateMobileMac_Empty_prompt = 2131230854;
        public static final int upomp_lthj_validateMobileMac_Length_prompt = 2131230855;
        public static final int upomp_lthj_validateMobileMac_Same_prompt = 2131230856;
        public static final int upomp_lthj_validateMobileNum_FromatError_prompt = 2131230858;
        public static final int upomp_lthj_validateMobileNum_LengthEmpty_prompt = 2131230857;
        public static final int upomp_lthj_validateNewMobileNum_Empty_prompt = 2131230844;
        public static final int upomp_lthj_validateNewPassWord_Empty_prompt = 2131230867;
        public static final int upomp_lthj_validateNewPassWord_Length_prompt = 2131230868;
        public static final int upomp_lthj_validateNewPassword_Same_prompt = 2131230848;
        public static final int upomp_lthj_validateNum_Length_prompt = 2131230849;
        public static final int upomp_lthj_validateOldMobileNum_Empty_prompt = 2131230843;
        public static final int upomp_lthj_validateOldPassWord_Empty_prompt = 2131230865;
        public static final int upomp_lthj_validateOldPassWord_Length_prompt = 2131230866;
        public static final int upomp_lthj_validatePanPassword_Empty_prompt = 2131230884;
        public static final int upomp_lthj_validatePanPassword_Length_prompt = 2131230885;
        public static final int upomp_lthj_validatePan_Empty_prompt = 2131230882;
        public static final int upomp_lthj_validatePan_Format_prompt = 2131230883;
        public static final int upomp_lthj_validatePassWord_Empty_prompt = 2131230862;
        public static final int upomp_lthj_validatePassWord_Length_prompt = 2131230863;
        public static final int upomp_lthj_validatePassWord_Same_prompt = 2131230869;
        public static final int upomp_lthj_validatePassword_complex_prompt = 2131230847;
        public static final int upomp_lthj_validatePassword_complex_prompt_kind = 2131230853;
        public static final int upomp_lthj_validateSafeAnswer_Empty_prompt = 2131230876;
        public static final int upomp_lthj_validateSafeQuestion_Empty_prompt = 2131230875;
        public static final int upomp_lthj_validateUserConcert_Empty_prompt = 2131230877;
        public static final int upomp_lthj_validateUserName_Empty_prompt = 2131230859;
        public static final int upomp_lthj_validateUserName_Length_prompt = 2131230861;
        public static final int upomp_lthj_validateUser_Protocal_prompt = 2131230860;
        public static final int upomp_lthj_validateWelcomeWord_Empty_prompt = 2131230874;
        public static final int upomp_lthj_version = 2131230724;
        public static final int upomp_lthj_version_hint = 2131230723;
        public static final int upomp_lthj_watting = 2131230754;
        public static final int upomp_lthj_welcome_word = 2131230791;
        public static final int upomp_lthj_welcome_wordError_Length_prompt = 2131230852;
        /* added by JADX */
        public static final int PIC_TAG = 2131230888;
        /* added by JADX */
        public static final int PIC_TEMP_PATH = 2131230889;
        /* added by JADX */
        public static final int UPDATE_PATH = 2131230890;
        /* added by JADX */
        public static final int LOG_PATH = 2131230891;
        /* added by JADX */
        public static final int IMAGE_DOMAIN = 2131230892;
        /* added by JADX */
        public static final int WEB_DOMAIN = 2131230893;
        /* added by JADX */
        public static final int API_URL = 2131230894;
        /* added by JADX */
        public static final int API_SIGN = 2131230895;
        /* added by JADX */
        public static final int network_not_connected = 2131230896;
        /* added by JADX */
        public static final int xml_parser_failed = 2131230897;
        /* added by JADX */
        public static final int io_exception_error = 2131230898;
        /* added by JADX */
        public static final int http_status_code_error = 2131230899;
        /* added by JADX */
        public static final int http_exception_error = 2131230900;
        /* added by JADX */
        public static final int socket_exception_error = 2131230901;
        /* added by JADX */
        public static final int server_exception_error = 2131230902;
        /* added by JADX */
        public static final int app_run_code_error = 2131230903;
        /* added by JADX */
        public static final int app_menu_surelogout = 2131230904;
        /* added by JADX */
        public static final int sure = 2131230905;
        /* added by JADX */
        public static final int cancle = 2131230906;
        /* added by JADX */
        public static final int app_error = 2131230907;
        /* added by JADX */
        public static final int app_error_message = 2131230908;
        /* added by JADX */
        public static final int submit_report = 2131230909;
        /* added by JADX */
        public static final int load_more = 2131230910;
        /* added by JADX */
        public static final int load_ing = 2131230911;
        /* added by JADX */
        public static final int load_full = 2131230912;
        /* added by JADX */
        public static final int load_empty = 2131230913;
        /* added by JADX */
        public static final int load_error = 2131230914;
        /* added by JADX */
        public static final int new_data_toast_message = 2131230915;
        /* added by JADX */
        public static final int new_data_toast_none = 2131230916;
        /* added by JADX */
        public static final int select = 2131230917;
        /* added by JADX */
        public static final int share = 2131230918;
        /* added by JADX */
        public static final int sharing = 2131230919;
        /* added by JADX */
        public static final int publishing = 2131230920;
        /* added by JADX */
        public static final int choose_image = 2131230921;
        /* added by JADX */
        public static final int delete_image = 2131230922;
        /* added by JADX */
        public static final int delete_tweet = 2131230923;
        /* added by JADX */
        public static final int delete_blog = 2131230924;
        /* added by JADX */
        public static final int republish_tweet = 2131230925;
        /* added by JADX */
        public static final int clearwords = 2131230926;
        /* added by JADX */
        public static final int msg_load_is_null = 2131230927;
        /* added by JADX */
        public static final int msg_login_email_error = 2131230928;
        /* added by JADX */
        public static final int msg_login_email_null = 2131230929;
        /* added by JADX */
        public static final int msg_login_pwd_null = 2131230930;
        /* added by JADX */
        public static final int msg_login_success = 2131230931;
        /* added by JADX */
        public static final int msg_login_fail = 2131230932;
        /* added by JADX */
        public static final int msg_login_error = 2131230933;
        /* added by JADX */
        public static final int msg_load_userface_fail = 2131230934;
        /* added by JADX */
        public static final int msg_load_image_fail = 2131230935;
        /* added by JADX */
        public static final int msg_read_detail_fail = 2131230936;
        /* added by JADX */
        public static final int msg_noaccess_delete = 2131230937;
        /* added by JADX */
        public static final int sinalogin_check_account = 2131230938;
        /* added by JADX */
        public static final int sinalogin_check_pass = 2131230939;
        /* added by JADX */
        public static final int sinalogin_check_server = 2131230940;
        /* added by JADX */
        public static final int OAUTH_ERROR = 2131230941;
        /* added by JADX */
        public static final int OAUTH_RequestToken_ACCESS = 2131230942;
        /* added by JADX */
        public static final int OAUTH_RequestToken_ERROR = 2131230943;
        /* added by JADX */
        public static final int OAUTH_AccessToken_ACCESS = 2131230944;
        /* added by JADX */
        public static final int OAUTH_AccessToken_ERROR = 2131230945;
        /* added by JADX */
        public static final int OAUTH_AccessToken_SXPIRED = 2131230946;
        /* added by JADX */
        public static final int Weibo_Message_NULL = 2131230947;
        /* added by JADX */
        public static final int Weibo_Message_LONG = 2131230948;
        /* added by JADX */
        public static final int Weibo_Share_Success = 2131230949;
        /* added by JADX */
        public static final int Weibo_Share_Error = 2131230950;
        /* added by JADX */
        public static final int Weibo_Share_Repeat = 2131230951;
        /* added by JADX */
        public static final int company_name = 2131230952;
        /* added by JADX */
        public static final int company_address = 2131230953;
        /* added by JADX */
        public static final int company_phone = 2131230954;
        /* added by JADX */
        public static final int company_desc = 2131230955;
        /* added by JADX */
        public static final int app_check_version = 2131230956;
        /* added by JADX */
        public static final int app_copy_rigth = 2131230957;
        /* added by JADX */
        public static final int main_navigation_home = 2131230958;
        /* added by JADX */
        public static final int main_navigation_catagory = 2131230959;
        /* added by JADX */
        public static final int main_navigation_car = 2131230960;
        /* added by JADX */
        public static final int main_navigation_member = 2131230961;
        /* added by JADX */
        public static final int main_navigation_more = 2131230962;
        /* added by JADX */
        public static final int back = 2131230963;
        /* added by JADX */
        public static final int sureBtn = 2131230964;
        /* added by JADX */
        public static final int login = 2131230965;
        /* added by JADX */
        public static final int logout = 2131230966;
        /* added by JADX */
        public static final int ask_question = 2131230967;
        /* added by JADX */
        public static final int action_settings = 2131230968;
        /* added by JADX */
        public static final int frame_title_search_result = 2131230969;
        /* added by JADX */
        public static final int btn_success = 2131230970;
        /* added by JADX */
        public static final int more_search_attrs_clear = 2131230971;
        /* added by JADX */
        public static final int more_search_attrs_screen = 2131230972;
        /* added by JADX */
        public static final int input_seach_key = 2131230973;
        /* added by JADX */
        public static final int search_search = 2131230974;
        /* added by JADX */
        public static final int search_cancel = 2131230975;
        /* added by JADX */
        public static final int search_history = 2131230976;
        /* added by JADX */
        public static final int search_hot = 2131230977;
        /* added by JADX */
        public static final int search_product_sales = 2131230978;
        /* added by JADX */
        public static final int search_product_price = 2131230979;
        /* added by JADX */
        public static final int screen = 2131230980;
        /* added by JADX */
        public static final int search_product_time = 2131230981;
        /* added by JADX */
        public static final int promotion_product_text = 2131230982;
        /* added by JADX */
        public static final int discount_product_text = 2131230983;
        /* added by JADX */
        public static final int recommend_product_text = 2131230984;
        /* added by JADX */
        public static final int seckill_product_text = 2131230985;
        /* added by JADX */
        public static final int seckill_product_info = 2131230986;
        /* added by JADX */
        public static final int new_product_text = 2131230987;
        /* added by JADX */
        public static final int success_order_head_title = 2131230988;
        /* added by JADX */
        public static final int success_order_text = 2131230989;
        /* added by JADX */
        public static final int order_sn = 2131230990;
        /* added by JADX */
        public static final int order_time = 2131230991;
        /* added by JADX */
        public static final int order_amount = 2131230992;
        /* added by JADX */
        public static final int order_other_amount = 2131230993;
        /* added by JADX */
        public static final int order_pay = 2131230994;
        /* added by JADX */
        public static final int order_pay_btn = 2131230995;
        /* added by JADX */
        public static final int order_receiving_btn = 2131230996;
        /* added by JADX */
        public static final int order_ship_btn = 2131230997;
        /* added by JADX */
        public static final int order_warm_tips = 2131230998;
        /* added by JADX */
        public static final int success_btn_online_pay = 2131230999;
        /* added by JADX */
        public static final int order_detail_title = 2131231000;
        /* added by JADX */
        public static final int order_detail_pay = 2131231001;
        /* added by JADX */
        public static final int order_unpay_head_title = 2131231002;
        /* added by JADX */
        public static final int order_all_head_title = 2131231003;
        /* added by JADX */
        public static final int order_all_tab_after_title = 2131231004;
        /* added by JADX */
        public static final int order_all_tab_before_title = 2131231005;
        /* added by JADX */
        public static final int loginActivity_username_name = 2131231006;
        /* added by JADX */
        public static final int loginActivity_pwd_pwd = 2131231007;
        /* added by JADX */
        public static final int loginActivity_username_des = 2131231008;
        /* added by JADX */
        public static final int loginActivity_pwd_des = 2131231009;
        /* added by JADX */
        public static final int loginActivity_login = 2131231010;
        /* added by JADX */
        public static final int loginActivity_input_name_pwd = 2131231011;
        /* added by JADX */
        public static final int loginActivity_input_name = 2131231012;
        /* added by JADX */
        public static final int loginActivity_input_pwd = 2131231013;
        /* added by JADX */
        public static final int loginActivity_auto_login = 2131231014;
        /* added by JADX */
        public static final int loginActivity_login_success = 2131231015;
        /* added by JADX */
        public static final int loginActivity_account_not_exist = 2131231016;
        /* added by JADX */
        public static final int loginActivity_account_pwd_error = 2131231017;
        /* added by JADX */
        public static final int loginActivity_account_state_error = 2131231018;
        /* added by JADX */
        public static final int loginActivity_session_error = 2131231019;
        /* added by JADX */
        public static final int user_center = 2131231020;
        /* added by JADX */
        public static final int user_level = 2131231021;
        /* added by JADX */
        public static final int user_unpay_order = 2131231022;
        /* added by JADX */
        public static final int user_all_order = 2131231023;
        /* added by JADX */
        public static final int user_message = 2131231024;
        /* added by JADX */
        public static final int user_address = 2131231025;
        /* added by JADX */
        public static final int register = 2131231026;
        /* added by JADX */
        public static final int register_success = 2131231027;
        /* added by JADX */
        public static final int registerActivity_username = 2131231028;
        /* added by JADX */
        public static final int registerActivity_pwd1 = 2131231029;
        /* added by JADX */
        public static final int registerActivity_pwd1_des = 2131231030;
        /* added by JADX */
        public static final int registerActivity_pwd2 = 2131231031;
        /* added by JADX */
        public static final int registerActivity_pwd2_des = 2131231032;
        /* added by JADX */
        public static final int registerActivity_email_des = 2131231033;
        /* added by JADX */
        public static final int registerActivity_register = 2131231034;
        /* added by JADX */
        public static final int email_no_empty = 2131231035;
        /* added by JADX */
        public static final int email_format_error = 2131231036;
        /* added by JADX */
        public static final int email_length_error = 2131231037;
        /* added by JADX */
        public static final int pwd_no_empty = 2131231038;
        /* added by JADX */
        public static final int pwd_length_error = 2131231039;
        /* added by JADX */
        public static final int twice_pwd_error = 2131231040;
        /* added by JADX */
        public static final int user_registering = 2131231041;
        /* added by JADX */
        public static final int email_already_exist = 2131231042;
        /* added by JADX */
        public static final int username_or_password_empty = 2131231043;
        /* added by JADX */
        public static final int product_loading = 2131231044;
        /* added by JADX */
        public static final int continue_shopping = 2131231045;
        /* added by JADX */
        public static final int go_to_cart = 2131231046;
        /* added by JADX */
        public static final int product_base_info = 2131231047;
        /* added by JADX */
        public static final int select_product_base_info = 2131231048;
        /* added by JADX */
        public static final int add_to_cart_success = 2131231049;
        /* added by JADX */
        public static final int product_desc = 2131231050;
        /* added by JADX */
        public static final int product_discuss = 2131231051;
        /* added by JADX */
        public static final int product_consult = 2131231052;
        /* added by JADX */
        public static final int product_recommend = 2131231053;
        /* added by JADX */
        public static final int product_discuss_loading = 2131231054;
        /* added by JADX */
        public static final int product_consult_loading = 2131231055;
        /* added by JADX */
        public static final int product_question = 2131231056;
        /* added by JADX */
        public static final int product_answer = 2131231057;
        /* added by JADX */
        public static final int product_discuss_empty = 2131231058;
        /* added by JADX */
        public static final int product_consult_empty = 2131231059;
        /* added by JADX */
        public static final int product_desc_empty = 2131231060;
        /* added by JADX */
        public static final int product_recommend_empty = 2131231061;
        /* added by JADX */
        public static final int cart_title = 2131231062;
        /* added by JADX */
        public static final int go_to_the_checkout = 2131231063;
        /* added by JADX */
        public static final int go_to_the_promotions_btn = 2131231064;
        /* added by JADX */
        public static final int cart_del_button = 2131231065;
        /* added by JADX */
        public static final int cart_del_btn_title = 2131231066;
        /* added by JADX */
        public static final int cart_del_btn_message = 2131231067;
        /* added by JADX */
        public static final int cart_del_btn_promotion_message = 2131231068;
        /* added by JADX */
        public static final int cart_dialog_del_title = 2131231069;
        /* added by JADX */
        public static final int cart_dialog_del_message = 2131231070;
        /* added by JADX */
        public static final int cart_dialog_title = 2131231071;
        /* added by JADX */
        public static final int cart_dialog_positive_button = 2131231072;
        /* added by JADX */
        public static final int cart_dialog_negative_button = 2131231073;
        /* added by JADX */
        public static final int cart_dialog_update_count_title = 2131231074;
        /* added by JADX */
        public static final int prompt_cart_empty_message = 2131231075;
        /* added by JADX */
        public static final int prompt_cart_empty_content = 2131231076;
        /* added by JADX */
        public static final int prompt_cart_is_full_stock = 2131231077;
        /* added by JADX */
        public static final int prompt_cart_is_invalid = 2131231078;
        /* added by JADX */
        public static final int cart_goods_count = 2131231079;
        /* added by JADX */
        public static final int cart_amount = 2131231080;
        /* added by JADX */
        public static final int RMB = 2131231081;
        /* added by JADX */
        public static final int desc_cart_goods_item_img = 2131231082;
        /* added by JADX */
        public static final int desc_cart_go_to_the_goods_view = 2131231083;
        /* added by JADX */
        public static final int cart_goods_item_separator = 2131231084;
        /* added by JADX */
        public static final int check_order_head_title = 2131231085;
        /* added by JADX */
        public static final int check_order_address_title = 2131231086;
        /* added by JADX */
        public static final int check_order_pay_title = 2131231087;
        /* added by JADX */
        public static final int check_order_ship_title = 2131231088;
        /* added by JADX */
        public static final int check_order_invoice_info = 2131231089;
        /* added by JADX */
        public static final int invoice_type = 2131231090;
        /* added by JADX */
        public static final int check_order_invoice_type = 2131231091;
        /* added by JADX */
        public static final int check_order_invoice_type_cont = 2131231092;
        /* added by JADX */
        public static final int check_order_invoice_title = 2131231093;
        /* added by JADX */
        public static final int invoice_title = 2131231094;
        /* added by JADX */
        public static final int invoice_title_personal = 2131231095;
        /* added by JADX */
        public static final int invoice_title_units = 2131231096;
        /* added by JADX */
        public static final int address = 2131231097;
        /* added by JADX */
        public static final int input_units_name = 2131231098;
        /* added by JADX */
        public static final int invoice_content = 2131231099;
        /* added by JADX */
        public static final int check_order_invoice_content = 2131231100;
        /* added by JADX */
        public static final int check_order_coupon_title = 2131231101;
        /* added by JADX */
        public static final int check_order_anount = 2131231102;
        /* added by JADX */
        public static final int check_order_submit = 2131231103;
        /* added by JADX */
        public static final int info_null = 2131231104;
        /* added by JADX */
        public static final int pay_platform_zfb = 2131231105;
        /* added by JADX */
        public static final int pay_platform_yl = 2131231106;
        /* added by JADX */
        public static final int pay_platform_hdfk = 2131231107;
        /* added by JADX */
        public static final int ship_type_ems = 2131231108;
        /* added by JADX */
        public static final int ship_type_delivery = 2131231109;
        /* added by JADX */
        public static final int ship_type_surface = 2131231110;
        /* added by JADX */
        public static final int address_list_title = 2131231111;
        /* added by JADX */
        public static final int address_add_title = 2131231112;
        /* added by JADX */
        public static final int address_edit_title = 2131231113;
        /* added by JADX */
        public static final int address_add_btn = 2131231114;
        /* added by JADX */
        public static final int address_save_btn = 2131231115;
        /* added by JADX */
        public static final int address_item_default_flag = 2131231116;
        /* added by JADX */
        public static final int address_add_lable_name = 2131231117;
        /* added by JADX */
        public static final int address_add_lable_mobile = 2131231118;
        /* added by JADX */
        public static final int address_add_lable_code = 2131231119;
        /* added by JADX */
        public static final int address_add_lable_province = 2131231120;
        /* added by JADX */
        public static final int address_add_lable_city = 2131231121;
        /* added by JADX */
        public static final int address_add_lable_area = 2131231122;
        /* added by JADX */
        public static final int address_add_lable_address = 2131231123;
        /* added by JADX */
        public static final int address_add_des_name = 2131231124;
        /* added by JADX */
        public static final int address_add_des_mobile = 2131231125;
        /* added by JADX */
        public static final int address_add_des_code = 2131231126;
        /* added by JADX */
        public static final int address_add_des_province = 2131231127;
        /* added by JADX */
        public static final int address_add_des_city = 2131231128;
        /* added by JADX */
        public static final int address_add_des_area = 2131231129;
        /* added by JADX */
        public static final int address_add_des_address = 2131231130;
        /* added by JADX */
        public static final int address_empty_list = 2131231131;
        /* added by JADX */
        public static final int order_empty_list = 2131231132;
        /* added by JADX */
        public static final int more_clear_cache = 2131231133;
        /* added by JADX */
        public static final int more_check_version = 2131231134;
        /* added by JADX */
        public static final int more_about = 2131231135;
        /* added by JADX */
        public static final int home_more_title = 2131231136;
        /* added by JADX */
        public static final int click_loading = 2131231137;
        /* added by JADX */
        public static final int click_load_error = 2131231138;
        /* added by JADX */
        public static final int again_load = 2131231139;
        /* added by JADX */
        public static final int pull_down_refresh = 2131231140;
        /* added by JADX */
        public static final int release_update = 2131231141;
        /* added by JADX */
        public static final int last_refresh = 2131231142;
        /* added by JADX */
        public static final int bein_refresh = 2131231143;
        /* added by JADX */
        public static final int loading = 2131231144;
        /* added by JADX */
        public static final int pull_load_more = 2131231145;
        /* added by JADX */
        public static final int has_load_all = 2131231146;
        /* added by JADX */
        public static final int market_price = 2131231147;
        /* added by JADX */
        public static final int price = 2131231148;
        /* added by JADX */
        public static final int kill_pro_price = 2131231149;
        /* added by JADX */
        public static final int default_price = 2131231150;
        /* added by JADX */
        public static final int coming = 2131231151;
        /* added by JADX */
        public static final int immediate_seckill = 2131231152;
        /* added by JADX */
        public static final int map_not_ready = 2131231153;
        /* added by JADX */
        public static final int mendian_text2 = 2131231154;
    }

    public final class style {
        public static final int upomp_lthj_about_item = 2131296266;
        public static final int upomp_lthj_about_textview = 2131296265;
        public static final int upomp_lthj_button_big_single = 2131296271;
        public static final int upomp_lthj_button_light_blue = 2131296281;
        public static final int upomp_lthj_checkbox = 2131296289;
        public static final int upomp_lthj_common_dialog = 2131296280;
        public static final int upomp_lthj_comstominput = 2131296272;
        public static final int upomp_lthj_dialog = 2131296288;
        public static final int upomp_lthj_dialog_button = 2131296278;
        public static final int upomp_lthj_dialog_content = 2131296276;
        public static final int upomp_lthj_dialog_icon = 2131296277;
        public static final int upomp_lthj_dialog_tv = 2131296287;
        public static final int upomp_lthj_drop_button = 2131296279;
        public static final int upomp_lthj_info_layout = 2131296263;
        public static final int upomp_lthj_info_textview = 2131296264;
        public static final int upomp_lthj_input_edittext = 2131296262;
        public static final int upomp_lthj_input_help_btn = 2131296269;
        public static final int upomp_lthj_input_layout = 2131296261;
        public static final int upomp_lthj_keyboard_dialog = 2131296286;
        public static final int upomp_lthj_keyboard_radio = 2131296285;
        public static final int upomp_lthj_layout_padding = 2131296260;
        public static final int upomp_lthj_layout_width_height_ff = 2131296256;
        public static final int upomp_lthj_layout_width_height_fw = 2131296257;
        public static final int upomp_lthj_layout_width_height_wf = 2131296259;
        public static final int upomp_lthj_layout_width_height_ww = 2131296258;
        public static final int upomp_lthj_light_blue_btn = 2131296274;
        public static final int upomp_lthj_lineframe_icon = 2131296283;
        public static final int upomp_lthj_right_arrows = 2131296282;
        public static final int upomp_lthj_rowline = 2131296273;
        public static final int upomp_lthj_scrollview_big = 2131296284;
        public static final int upomp_lthj_tablelayout = 2131296267;
        public static final int upomp_lthj_tablerow = 2131296268;
        public static final int upomp_lthj_verifycode_btn = 2131296270;
        public static final int upomp_lthj_watermark = 2131296275;
        /* added by JADX */
        public static final int ContentOverlay = 2131296290;
        /* added by JADX */
        public static final int Animation = 2131296291;
        /* added by JADX */
        public static final int Animation_Translucent = 2131296292;
        /* added by JADX */
        public static final int Animation_SlideTop = 2131296293;
        /* added by JADX */
        public static final int Animation_ZoomLight = 2131296294;
        /* added by JADX */
        public static final int Dialog = 2131296295;
        /* added by JADX */
        public static final int loading_small = 2131296296;
        /* added by JADX */
        public static final int App_Preference_CheckBox = 2131296297;
        /* added by JADX */
        public static final int App_Preference_TextAppearance_Large = 2131296298;
        /* added by JADX */
        public static final int App_Preference_TextAppearance_Small = 2131296299;
        /* added by JADX */
        public static final int Theme_NoTitleBar = 2131296300;
        /* added by JADX */
        public static final int Theme_HalfTranslucent = 2131296301;
        /* added by JADX */
        public static final int Theme_FullTranslucent = 2131296302;
        /* added by JADX */
        public static final int Theme_SlideTop = 2131296303;
        /* added by JADX */
        public static final int Theme_ZoomLight = 2131296304;
        /* added by JADX */
        public static final int Theme_ZoomLight_Fullscreen = 2131296305;
        /* added by JADX */
        public static final int footbar = 2131296306;
        /* added by JADX */
        public static final int user_center_footbar = 2131296307;
        /* added by JADX */
        public static final int main_footbar_radio = 2131296308;
        /* added by JADX */
        public static final int main_footbar_image = 2131296309;
        /* added by JADX */
        public static final int main_footbar_cutline = 2131296310;
        /* added by JADX */
        public static final int main_head_title = 2131296311;
        /* added by JADX */
        public static final int frame_button_left = 2131296312;
        /* added by JADX */
        public static final int frame_button_middle = 2131296313;
        /* added by JADX */
        public static final int frame_button_right = 2131296314;
        /* added by JADX */
        public static final int detail_head_title = 2131296315;
        /* added by JADX */
        public static final int Title = 2131296316;
        /* added by JADX */
        public static final int StyleListViewItem = 2131296317;
        /* added by JADX */
        public static final int main_tab_bottom = 2131296318;
        /* added by JADX */
        public static final int Transparent = 2131296319;
        /* added by JADX */
        public static final int layout_title = 2131296320;
        /* added by JADX */
        public static final int line1 = 2131296321;
        /* added by JADX */
        public static final int breakline = 2131296322;
        /* added by JADX */
        public static final int search_listview = 2131296323;
        /* added by JADX */
        public static final int product_gallery = 2131296324;
        /* added by JADX */
        public static final int dots_gallery = 2131296325;
        /* added by JADX */
        public static final int product_order_act = 2131296326;
        /* added by JADX */
        public static final int product_tool_button = 2131296327;
        /* added by JADX */
        public static final int title_base_button = 2131296328;
        /* added by JADX */
        public static final int title_button = 2131296329;
        /* added by JADX */
        public static final int title_button_right = 2131296330;
        /* added by JADX */
        public static final int title_imagebutton_right = 2131296331;
        /* added by JADX */
        public static final int MyRatingBar = 2131296332;
        /* added by JADX */
        public static final int CommonListviewStyle = 2131296333;
        /* added by JADX */
        public static final int AddressAddTextStyle = 2131296334;
        /* added by JADX */
        public static final int AddressAddEditTextStyle = 2131296335;
        /* added by JADX */
        public static final int AddressAddLinearLayoutStyle = 2131296336;
        /* added by JADX */
        public static final int AddressItemTableRowStyle = 2131296337;
        /* added by JADX */
        public static final int AddressTextStyle = 2131296338;
        /* added by JADX */
        public static final int AddressTextStyle15 = 2131296339;
        /* added by JADX */
        public static final int AddressItemTextStyle = 2131296340;
        /* added by JADX */
        public static final int BaseListTextStyle = 2131296341;
        /* added by JADX */
        public static final int AddressListTextStyle = 2131296342;
        /* added by JADX */
        public static final int OrderListTextStyle = 2131296343;
        /* added by JADX */
        public static final int CartEmptyBtnStyle = 2131296344;
        /* added by JADX */
        public static final int HomeTextStyle = 2131296345;
        /* added by JADX */
        public static final int MoreTextStyle = 2131296346;
        /* added by JADX */
        public static final int InvoiceTextStyle = 2131296347;
        /* added by JADX */
        public static final int CheckTextStyle = 2131296348;
        /* added by JADX */
        public static final int CheckTextStyle2 = 2131296349;
        /* added by JADX */
        public static final int MemberImageViewStyle = 2131296350;
        /* added by JADX */
        public static final int MemberTableRowStyle = 2131296351;
        /* added by JADX */
        public static final int OrderDetailTextStyle = 2131296352;
        /* added by JADX */
        public static final int UserOrderButtonStyle = 2131296353;
    }

    /* added by JADX */
    public static final class anim {
        /* added by JADX */
        public static final int loading = 2130968576;
        /* added by JADX */
        public static final int slide_in_up = 2130968577;
        /* added by JADX */
        public static final int slide_out_down = 2130968578;
        /* added by JADX */
        public static final int translucent_zoom_exit = 2130968579;
        /* added by JADX */
        public static final int translucent_zoom_in = 2130968580;
        /* added by JADX */
        public static final int translucent_zoom_out = 2130968581;
    }

    /* added by JADX */
    public static final class color {
        /* added by JADX */
        public static final int white = 2131361792;
        /* added by JADX */
        public static final int black = 2131361793;
        /* added by JADX */
        public static final int gray = 2131361794;
        /* added by JADX */
        public static final int red = 2131361795;
        /* added by JADX */
        public static final int gold = 2131361796;
        /* added by JADX */
        public static final int yellow = 2131361797;
        /* added by JADX */
        public static final int green = 2131361798;
        /* added by JADX */
        public static final int blue = 2131361799;
        /* added by JADX */
        public static final int purple = 2131361800;
        /* added by JADX */
        public static final int pink = 2131361801;
        /* added by JADX */
        public static final int orange = 2131361802;
        /* added by JADX */
        public static final int lemonyellow = 2131361803;
        /* added by JADX */
        public static final int graylight = 2131361804;
        /* added by JADX */
        public static final int graywhite = 2131361805;
        /* added by JADX */
        public static final int grayslate = 2131361806;
        /* added by JADX */
        public static final int lightblue = 2131361807;
        /* added by JADX */
        public static final int full_transparent = 2131361808;
        /* added by JADX */
        public static final int half_transparent = 2131361809;
        /* added by JADX */
        public static final int bright_foreground_light = 2131361810;
        /* added by JADX */
        public static final int bright_foreground_light_inverse = 2131361811;
        /* added by JADX */
        public static final int bright_foreground_light_disabled = 2131361812;
        /* added by JADX */
        public static final int dim_foreground_light = 2131361813;
        /* added by JADX */
        public static final int dim_foreground_light_disabled = 2131361814;
        /* added by JADX */
        public static final int dim_foreground_light_inverse = 2131361815;
        /* added by JADX */
        public static final int dim_foreground_light_inverse_disabled = 2131361816;
        /* added by JADX */
        public static final int head_text = 2131361817;
        /* added by JADX */
        public static final int author_text = 2131361818;
        /* added by JADX */
        public static final int face_bg = 2131361819;
        /* added by JADX */
        public static final int frame_button_text_nor = 2131361820;
        /* added by JADX */
        public static final int frame_button_text_select = 2131361821;
        /* added by JADX */
        public static final int listitem_transparent = 2131361822;
        /* added by JADX */
        public static final int listitem_black = 2131361823;
        /* added by JADX */
        public static final int listitem_white = 2131361824;
        /* added by JADX */
        public static final int listitem_blue = 2131361825;
        /* added by JADX */
        public static final int listitem_gray = 2131361826;
        /* added by JADX */
        public static final int listitem_bg_gray = 2131361827;
        /* added by JADX */
        public static final int listitem_group_gray = 2131361828;
        /* added by JADX */
        public static final int listitem_child_gray = 2131361829;
        /* added by JADX */
        public static final int listitem_green = 2131361830;
        /* added by JADX */
        public static final int listitem_greenyellow = 2131361831;
        /* added by JADX */
        public static final int listitem_yellow = 2131361832;
        /* added by JADX */
        public static final int listitem_pressed_yellow = 2131361833;
        /* added by JADX */
        public static final int main_search_black = 2131361834;
        /* added by JADX */
        public static final int main_head_title_shadow_color = 2131361835;
        /* added by JADX */
        public static final int detail_head_title_shadow_color = 2131361836;
        /* added by JADX */
        public static final int title_text_color = 2131361837;
        /* added by JADX */
        public static final int style_list_view_item_text_color = 2131361838;
        /* added by JADX */
        public static final int layout_title_text_color = 2131361839;
        /* added by JADX */
        public static final int line1_background = 2131361840;
        /* added by JADX */
        public static final int breakline_background = 2131361841;
        /* added by JADX */
        public static final int product_order_act_text_color = 2131361842;
        /* added by JADX */
        public static final int title_base_button_text_color = 2131361843;
        /* added by JADX */
        public static final int drawable_transparent_background = 2131361844;
        /* added by JADX */
        public static final int drawable_transparent = 2131361845;
        /* added by JADX */
        public static final int drawable_list_row_zeng = 2131361846;
        /* added by JADX */
        public static final int drawable_list_row_pressed = 2131361847;
        /* added by JADX */
        public static final int background_corners_solid_color = 2131361848;
        /* added by JADX */
        public static final int background_corners_stroke_color = 2131361849;
        /* added by JADX */
        public static final int bg_list_item_stroke_color = 2131361850;
        /* added by JADX */
        public static final int bg_list_item2_stroke_color = 2131361851;
        /* added by JADX */
        public static final int list_item_bottom_bg_1_gradient_end_color = 2131361852;
        /* added by JADX */
        public static final int list_item_bottom_bg_1_gradient_start_color = 2131361853;
        /* added by JADX */
        public static final int list_item_bottom_bg_1_stroke_color = 2131361854;
        /* added by JADX */
        public static final int list_item_bottom_bg_2_solid_color = 2131361855;
        /* added by JADX */
        public static final int list_item_bottom_bg_2_stroke_color = 2131361856;
        /* added by JADX */
        public static final int list_item_center_bg_1_gradient_end_color = 2131361857;
        /* added by JADX */
        public static final int list_item_center_bg_1_gradient_start_color = 2131361858;
        /* added by JADX */
        public static final int list_item_center_bg_1_stroke_color = 2131361859;
        /* added by JADX */
        public static final int list_item_center_bg_2_solid_color = 2131361860;
        /* added by JADX */
        public static final int list_item_center_bg_2_stroke_color = 2131361861;
        /* added by JADX */
        public static final int list_item_one_bg_1_gradient_end_color = 2131361862;
        /* added by JADX */
        public static final int list_item_one_bg_1_gradient_start_color = 2131361863;
        /* added by JADX */
        public static final int list_item_one_bg_1_stroke_color = 2131361864;
        /* added by JADX */
        public static final int list_item_one_bg_2_solid_color = 2131361865;
        /* added by JADX */
        public static final int list_item_one_bg_2_stroke_color = 2131361866;
        /* added by JADX */
        public static final int list_item_top_bg_1_gradient_end_color = 2131361867;
        /* added by JADX */
        public static final int list_item_top_bg_1_gradient_start_color = 2131361868;
        /* added by JADX */
        public static final int list_item_top_bg_1_stroke_color = 2131361869;
        /* added by JADX */
        public static final int list_item_top_bg_2_solid_color = 2131361870;
        /* added by JADX */
        public static final int list_item_top_bg_2_stroke_color = 2131361871;
        /* added by JADX */
        public static final int search_edit_bg_corners_solid_color = 2131361872;
        /* added by JADX */
        public static final int textview_round_solid_color = 2131361873;
        /* added by JADX */
        public static final int cart_goods_item_name_text_color = 2131361874;
        /* added by JADX */
        public static final int loading_tip_txt_text_color = 2131361875;
        /* added by JADX */
        public static final int cart_goods_count_text_color = 2131361876;
        /* added by JADX */
        public static final int cart_amount_text_color = 2131361877;
        /* added by JADX */
        public static final int head_tips_text_view_text_color = 2131361878;
        /* added by JADX */
        public static final int head_last_updated_text_view_text_color = 2131361879;
        /* added by JADX */
        public static final int empty_cart_view_background = 2131361880;
        /* added by JADX */
        public static final int order_item_price_text_color = 2131361881;
        /* added by JADX */
        public static final int order_item_count_text_color = 2131361882;
        /* added by JADX */
        public static final int product_consult_item_space_view_background = 2131361883;
        /* added by JADX */
        public static final int order_act_background = 2131361884;
        /* added by JADX */
        public static final int product_recommend_empty_text_color = 2131361885;
        /* added by JADX */
        public static final int lv_seckill_list_divider = 2131361886;
        /* added by JADX */
        public static final int rebate_act_grid_item_linear_layout_1_background = 2131361887;
        /* added by JADX */
        public static final int tv_rebate_act_price_text_color = 2131361888;
        /* added by JADX */
        public static final int background_color2 = 2131361889;
        /* added by JADX */
        public static final int background_color = 2131361890;
        /* added by JADX */
        public static final int divider = 2131361891;
        /* added by JADX */
        public static final int actions_bg = 2131361892;
        /* added by JADX */
        public static final int transparent = 2131361893;
        /* added by JADX */
        public static final int blue_light = 2131361894;
        /* added by JADX */
        public static final int actions_category_list_bg = 2131361895;
        /* added by JADX */
        public static final int login_btn_bg = 2131361896;
        /* added by JADX */
        public static final int login_btn_selector_bg = 2131361897;
        /* added by JADX */
        public static final int recommendfriend_font_bg = 2131361898;
        /* added by JADX */
        public static final int font_about_context = 2131361899;
        /* added by JADX */
        public static final int font_about_title = 2131361900;
        /* added by JADX */
        public static final int user_reply_aaaaaa = 2131361901;
        /* added by JADX */
        public static final int user_reply_78bbdd = 2131361902;
        /* added by JADX */
        public static final int user_reply_313131 = 2131361903;
        /* added by JADX */
        public static final int font_search_black = 2131361904;
        /* added by JADX */
        public static final int font_search_gray = 2131361905;
        /* added by JADX */
        public static final int title_fragment_bar_en_text_color = 2131361906;
        /* added by JADX */
        public static final int text_color_ff4a4a4a = 2131361907;
        /* added by JADX */
        public static final int text_color_4a4a4a = 2131361908;
        /* added by JADX */
        public static final int text_color_e40505 = 2131361909;
        /* added by JADX */
        public static final int text_color_989898 = 2131361910;
        /* added by JADX */
        public static final int text_color_ff2400 = 2131361911;
        /* added by JADX */
        public static final int text_color_a1a1a1 = 2131361912;
        /* added by JADX */
        public static final int text_color_a6a5a5 = 2131361913;
        /* added by JADX */
        public static final int text_color_818181 = 2131361914;
        /* added by JADX */
        public static final int text_color_f11111 = 2131361915;
        /* added by JADX */
        public static final int text_color_E1E1E1 = 2131361916;
        /* added by JADX */
        public static final int text_color_232323 = 2131361917;
        /* added by JADX */
        public static final int text_color_88ffffff = 2131361918;
        /* added by JADX */
        public static final int text_color_fe4e09 = 2131361919;
        /* added by JADX */
        public static final int text_color_fe4808 = 2131361920;
        /* added by JADX */
        public static final int text_color_787878 = 2131361921;
        /* added by JADX */
        public static final int text_color_d3d3d3 = 2131361922;
        /* added by JADX */
        public static final int text_color_2F2F2F = 2131361923;
        /* added by JADX */
        public static final int text_color_666666 = 2131361924;
        /* added by JADX */
        public static final int text_color_959595 = 2131361925;
        /* added by JADX */
        public static final int text_color_9c9c9c = 2131361926;
        /* added by JADX */
        public static final int text_color_b0b0b0 = 2131361927;
        /* added by JADX */
        public static final int font_white = 2131361928;
        /* added by JADX */
        public static final int listview_bg = 2131361929;
        /* added by JADX */
        public static final int list_item_bg_color_normal = 2131361930;
        /* added by JADX */
        public static final int list_item_bg_color_selector = 2131361931;
        /* added by JADX */
        public static final int all_bg_color = 2131361932;
        /* added by JADX */
        public static final int navigation_bar_color = 2131361933;
        /* added by JADX */
        public static final int subscribe_text_color = 2131361934;
        /* added by JADX */
        public static final int subscribe_gategory = 2131361935;
        /* added by JADX */
        public static final int color_srp_title = 2131361936;
        /* added by JADX */
        public static final int color_srp_content = 2131361937;
        /* added by JADX */
        public static final int favourite_btn_txtCol = 2131361938;
        /* added by JADX */
        public static final int cart_bg = 2131361939;
        /* added by JADX */
        public static final int text_product_price = 2131361940;
        /* added by JADX */
        public static final int main_head_title = 2131361941;
        /* added by JADX */
        public static final int main_topic_color = 2131361942;
        /* added by JADX */
        public static final int text_color_88717d83 = 2131361943;
        /* added by JADX */
        public static final int text_color_3e8ad6 = 2131361944;
        /* added by JADX */
        public static final int background_color_v4 = 2131361945;
        /* added by JADX */
        public static final int home_new_goods_color_v4 = 2131361946;
        /* added by JADX */
        public static final int frame_button_text_light = 2131361947;
        /* added by JADX */
        public static final int primary_text_light = 2131361948;
        /* added by JADX */
        public static final int secondary_text_light = 2131361949;
    }

    /* added by JADX */
    public static final class menu {
        /* added by JADX */
        public static final int goods_desc = 2131427328;
    }
}
