package com.unionpay.upomp.lthj.plugin.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.at;
import com.lthj.unipay.plugin.au;
import com.lthj.unipay.plugin.bb;
import com.lthj.unipay.plugin.bf;
import com.lthj.unipay.plugin.bg;
import com.lthj.unipay.plugin.bh;
import com.lthj.unipay.plugin.bi;
import com.lthj.unipay.plugin.bj;
import com.lthj.unipay.plugin.c;
import com.lthj.unipay.plugin.cc;
import com.lthj.unipay.plugin.cd;
import com.lthj.unipay.plugin.ce;
import com.lthj.unipay.plugin.cf;
import com.lthj.unipay.plugin.cg;
import com.lthj.unipay.plugin.ch;
import com.lthj.unipay.plugin.ci;
import com.lthj.unipay.plugin.cj;
import com.lthj.unipay.plugin.cl;
import com.lthj.unipay.plugin.cm;
import com.lthj.unipay.plugin.co;
import com.lthj.unipay.plugin.db;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.l;
import com.lthj.unipay.plugin.n;
import com.lthj.unipay.plugin.w;
import com.lthj.unipay.plugin.x;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.widget.LineFrameView;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;
import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends BaseActivity implements View.OnClickListener, UIResponseListener {
    public int a = 60;
    public TimerTask aaTimerTask;
    private final String b = "HomeActivity";
    private Button c;
    private Button d;
    private EditText e;
    private EditText f;
    private Button g;
    private EditText h;
    /* access modifiers changed from: private */
    public EditText i;
    private Button j;
    private ValidateCodeView k;
    private Button l;
    private Button m;
    private EditText n;
    /* access modifiers changed from: private */
    public Button o;
    private EditText p;
    private Button q;
    private View.OnClickListener r = new co(this);
    /* access modifiers changed from: private */
    public Handler s = new bj(this);
    private StringBuffer t;
    private EditText u;
    private String v;
    /* access modifiers changed from: private */
    public cm w;

    private void a(bb bbVar) {
        a(getString(PluginLink.getStringupomp_lthj_back()), new bh(this));
        setContentView(PluginLink.getLayoutupomp_lthj_findpwd_next());
        this.p = (EditText) findViewById(PluginLink.getIdupomp_lthj_safe_asw_view());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(bbVar.a());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(j.f(bbVar.c()));
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_safe_ask_view())).a(bbVar.d());
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_password_edit());
        x xVar = new x(8);
        this.f.setOnTouchListener(xVar);
        this.f.setOnFocusChangeListener(xVar);
        this.u = (EditText) findViewById(PluginLink.getIdupomp_lthj_confirm_pwd_input());
        x xVar2 = new x(7);
        this.u.setOnTouchListener(xVar2);
        this.u.setOnFocusChangeListener(xVar2);
        this.v = bbVar.a();
        this.q = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.q.setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void a(cm cmVar, boolean z) {
        if (z && !cmVar.a().equalsIgnoreCase(j.d(this))) {
            j.c(this, cmVar.a());
        }
        ap.a().z.setLength(0);
        ap.a().z.append(cmVar.a());
        ap.a().E = cmVar.d();
        ap.a().F = cmVar.o();
        ap.a().A.setLength(0);
        ap.a().A.append(cmVar.c());
        Intent intent = new Intent(this, PayActivity.class);
        intent.addFlags(67108864);
        intent.putExtra("isAcount", true);
        a().changeSubActivity(intent);
    }

    /* access modifiers changed from: private */
    public void b() {
        setContentView(PluginLink.getLayoutupomp_lthj_homecardpay());
        d();
        if (ap.a().b.b()) {
            this.k.setVisibility(0);
            j.a(this.k);
        }
        this.c = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.c.setOnClickListener(this);
        this.h = (EditText) findViewById(PluginLink.getIdupomp_lthj_card_num_edit());
        this.j = (Button) findViewById(PluginLink.getIdupomp_lthj_help_btn());
        this.j.setOnClickListener(this);
        this.i = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobile_num_edit());
        if (ap.a().w.length() != 0) {
            this.i.setText(j.f(ap.a().w.toString()));
        }
        if (ap.a().v.length() != 0) {
            this.h.setText(j.g(ap.a().v.toString()));
        }
        this.h.addTextChangedListener(new au(this.h));
        ((Button) findViewById(PluginLink.getIdupomp_lthj_use_quick_pay())).setOnClickListener(new cg(this));
    }

    /* access modifiers changed from: private */
    public void c() {
        setContentView(PluginLink.getLayoutupomp_lthj_homeaccountpay());
        d();
        if (ap.a().b.a()) {
            this.k.setVisibility(0);
            j.a(this.k);
        }
        this.d = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.d.setOnClickListener(this);
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_username_edit());
        this.e.setText(j.d(this));
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_password_edit());
        x xVar = new x(1);
        this.f.setOnFocusChangeListener(xVar);
        this.f.setOnTouchListener(xVar);
        ((Button) findViewById(PluginLink.getIdupomp_lthj_use_card_pay())).setOnClickListener(new cj(this));
        this.l = (Button) findViewById(PluginLink.getIdupomp_lthj_forget_pwd());
        this.l.setOnClickListener(this);
    }

    private void d() {
        this.k = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        a(getString(PluginLink.getStringupomp_lthj_app_quitNotice_msg()), this.r);
        this.g = (Button) findViewById(PluginLink.getIdupomp_lthj_unfold_btn());
        this.g.setOnClickListener(this);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_merchant_tv())).setText(ap.a().i);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_orderamt_tv())).setText(j.d(ap.a().m));
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_orderno_tv())).setText(ap.a().k);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_ordertime_tv())).setText(j.c(ap.a().l));
    }

    /* access modifiers changed from: private */
    public void e() {
        a(getString(PluginLink.getStringupomp_lthj_back()), new ci(this));
        setContentView(PluginLink.getLayoutupomp_lthj_findpwd_home());
        this.k = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        if (ap.a().b.e()) {
            this.k.setVisibility(0);
            j.a(this.k);
        }
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_username_edit());
        this.e.setText(j.d(this));
        this.i = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobile_num_edit());
        this.m = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.m.setOnClickListener(this);
        this.n = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        this.o = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.o.setOnClickListener(new cd(this));
    }

    public void errorCallBack(String str) {
        j.b(this, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.cl.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [com.unionpay.upomp.lthj.plugin.ui.HomeActivity, com.unionpay.upomp.lthj.plugin.ui.HomeActivity, java.lang.String, java.lang.String, int]
     candidates:
      com.lthj.unipay.plugin.cl.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, com.lthj.unipay.plugin.bt, java.lang.String):void
      com.lthj.unipay.plugin.cl.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, java.lang.String, boolean):void */
    public void onClick(View view) {
        if (view == this.c) {
            if (c.b(this, this.h, ap.a().v.toString()) && c.a(this, this.i, ap.a().w.toString()) && c.a(this, this.k)) {
                if (this.i.getText().toString().contains("*")) {
                    at.a("HomeActivity", "mobile has ****");
                } else {
                    ap.a().w.setLength(0);
                    ap.a().w.append(this.i.getText().toString());
                }
                this.t = new StringBuffer();
                if (this.h.getText().toString().contains("*")) {
                    this.t.append(ap.a().v);
                } else {
                    this.t.delete(0, this.t.length());
                    this.t.append(this.h.getText().toString());
                    for (int i2 = 0; i2 < this.t.length(); i2++) {
                        if (this.t.charAt(i2) == ' ') {
                            this.t.deleteCharAt(i2);
                        }
                    }
                }
                cl.a((UIResponseListener) this, (Context) this, this.t.toString(), this.k.c().getText().toString(), true);
            }
        } else if (view == this.d) {
            if (c.c(this, this.e) && c.d(this, this.f) && c.a(this, this.k)) {
                cl.d(this, this, this.e.getText().toString(), this.k.d());
                this.f.setText(PoiTypeDef.All);
            }
        } else if (view == this.g) {
            View findViewById = findViewById(PluginLink.getIdupomp_lthj_orderno_row());
            View findViewById2 = findViewById(PluginLink.getIdupomp_lthj_trantime_row());
            if (findViewById.getVisibility() == 8) {
                findViewById.setVisibility(0);
                findViewById2.setVisibility(0);
                view.setBackgroundResource(PluginLink.getDrawableupomp_lthj_info_up_btn());
                return;
            }
            findViewById.setVisibility(8);
            findViewById2.setVisibility(8);
            view.setBackgroundResource(PluginLink.getDrawableupomp_lthj_info_down_btn());
        } else if (view == this.j) {
            Intent intent = new Intent(this, SupportCardActivity.class);
            intent.putExtra("tranType", "1");
            intent.addFlags(67108864);
            a().changeSubActivity(intent);
        } else if (view == this.l) {
            e();
        } else if (view == this.m) {
            if (c.c(this, this.e) && c.b(this, this.i) && c.a(this, this.n) && c.a(this, this.k)) {
                cl.a(this, this, this.e.getText().toString(), this.i.getText().toString(), this.n.getText().toString(), this.k.d());
            }
        } else if (view == this.q && c.i(this, this.p) && c.d(this, this.f) && c.d(this, this.u) && c.a(this)) {
            cl.a(this, this, this.v, this.p.getText().toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a(getString(PluginLink.getStringupomp_lthj_backtomerchant()), new ch(this));
        if (ap.a().g) {
            c();
        } else {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (ap.a().w.length() != 0) {
            this.i.setText(j.f(ap.a().w.toString()));
        }
        if (ap.a().v.length() != 0) {
            this.h.setText(j.g(ap.a().v.toString()));
        }
        a().aboutEnable(true);
    }

    public void processRefreshConn() {
        if (this.aaTimerTask == null) {
            Timer timer = new Timer();
            this.aaTimerTask = new bi(this);
            timer.schedule(this.aaTimerTask, 0, 1000);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.unionpay.upomp.lthj.plugin.ui.HomeActivity.a(com.lthj.unipay.plugin.cm, boolean):void
     arg types: [com.lthj.unipay.plugin.cm, int]
     candidates:
      com.unionpay.upomp.lthj.plugin.ui.BaseActivity.a(java.lang.String, android.view.View$OnClickListener):void
      com.unionpay.upomp.lthj.plugin.ui.HomeActivity.a(com.lthj.unipay.plugin.cm, boolean):void */
    public void responseCallBack(w wVar) {
        if (wVar != null && wVar.m() != null && !isFinishing()) {
            int e2 = wVar.e();
            int parseInt = Integer.parseInt(wVar.m());
            switch (e2) {
                case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE:
                    this.w = (cm) wVar;
                    if (parseInt != 0) {
                        ap.a().b.a(true);
                        j.a(this, this.w.n(), parseInt);
                        break;
                    } else {
                        ap.a().b.a(false);
                        if (!j.d(this).equals(this.w.a())) {
                            l.a().a(this, getString(PluginLink.getStringupomp_lthj_save_account()), getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_no_save()), new ce(this), new bf(this));
                            break;
                        } else {
                            a(this.w, false);
                            break;
                        }
                    }
                case 8195:
                    if (parseInt == 0) {
                        l.a().a(this, getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_change_pwd_success()), new bg(this));
                    } else {
                        j.a(this, wVar.n(), parseInt);
                    }
                    ap.a().c.d.setLength(0);
                    break;
                case 8200:
                    n nVar = (n) wVar;
                    if (parseInt != 0) {
                        Toast makeText = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageLose()) + nVar.n() + ",错误码为：" + parseInt, 1);
                        makeText.setGravity(17, 0, 0);
                        makeText.show();
                        this.o.setEnabled(true);
                        this.o.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
                        break;
                    } else {
                        Toast makeText2 = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageSuccess()), 1);
                        makeText2.setGravity(17, 0, 0);
                        makeText2.show();
                        ap.a().x.delete(0, ap.a().x.length());
                        ap.a().x.append(nVar.c());
                        processRefreshConn();
                        break;
                    }
                case 8221:
                    bb bbVar = (bb) wVar;
                    if (parseInt != 0) {
                        ap.a().b.e(true);
                        j.a(this, wVar.n(), parseInt);
                        break;
                    } else {
                        ap.a().b.e(false);
                        a(bbVar);
                        break;
                    }
                case 8224:
                    db dbVar = (db) wVar;
                    if (parseInt != 0) {
                        ap.a().b.b(true);
                        if (!"5110".equals(wVar.m())) {
                            j.a(this, wVar.n(), parseInt);
                            break;
                        } else {
                            l.a().a(this, j.b(this, wVar.n(), parseInt), getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_check_support()), new cc(this), new cf(this));
                            break;
                        }
                    } else {
                        l.a().b();
                        ap.a().v.setLength(0);
                        ap.a().v.append(this.t.toString());
                        ap.a().c.a.setLength(0);
                        ap.a().c.a.append(this.t.toString());
                        ap.a().b.b(false);
                        Intent intent = new Intent(this, PayActivity.class);
                        intent.setFlags(67108864);
                        intent.putExtra("panType", dbVar.a());
                        intent.putExtra("panBank", dbVar.c());
                        intent.putExtra("mobilenumber", ap.a().w.toString());
                        a().changeSubActivity(intent);
                        break;
                    }
            }
            if (wVar.e() != 8200) {
                j.b(this.k);
            }
        }
    }
}
