package com.unionpay.upomp.lthj.plugin.model;

public class ModifyPassword extends Data {
    public String loginName;
    public String mobileMac;
    public String mobileNumber;
    public String newPassword;
    public String password;
}
