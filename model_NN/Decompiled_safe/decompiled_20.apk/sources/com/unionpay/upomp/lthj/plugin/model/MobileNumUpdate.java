package com.unionpay.upomp.lthj.plugin.model;

public class MobileNumUpdate extends Data {
    public String loginName;
    public String mobileMac;
    public String mobileNumber;
    public String newMobileNumber;
    public String password;
}
