package com.unionpay.upomp.lthj.plugin.model;

public class OrderQuery extends Data {
    public String commodityName;
    public String converRate;
    public String cupsQid;
    public String cupsRespCode;
    public String cupsTraceNum;
    public String cupsTraceTime;
    public String loginName;
    public String merchantId;
    public String merchantName;
    public String merchantOrderAmt;
    public String merchantOrderDesc;
    public String merchantOrderId;
    public String merchantOrderTime;
    public String merchantOrderTimeout;
    public String mobileNumber;
    public String setlAmt;
    public String setlCurrency;
    public String settleDate;
}
