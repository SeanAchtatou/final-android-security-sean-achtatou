package com.unionpay.upomp.lthj.plugin.model;

public class DeleteBankCard extends Data {
    public String bindId;
    public String isDefault;
    public String loginName;
    public String mobileNumber;
    public String pan;
}
