package com.unionpay.upomp.lthj.plugin.model;

public class OrderInfoCheck extends Data {
    public String activityId;
    public String backEndUrl;
    public String isPreAuth;
    public String merchantId;
    public String merchantName;
    public String merchantOrderAmt;
    public String merchantOrderDesc;
    public String merchantOrderId;
    public String merchantOrderTime;
    public String merchantPublicCert;
    public String sign;
    public String transTimeout;
}
