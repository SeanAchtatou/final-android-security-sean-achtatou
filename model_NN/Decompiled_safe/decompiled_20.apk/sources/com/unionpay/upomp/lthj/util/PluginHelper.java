package com.unionpay.upomp.lthj.util;

import android.app.Activity;
import android.os.Bundle;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.ef;

public class PluginHelper {
    public static Integer a = 0;

    public static void LaunchPlugin(Activity activity, Bundle bundle) {
        if (activity != null && bundle != null && !ap.a().f) {
            ap.a().f = true;
            new Thread(new ef(activity, bundle)).start();
        }
    }
}
