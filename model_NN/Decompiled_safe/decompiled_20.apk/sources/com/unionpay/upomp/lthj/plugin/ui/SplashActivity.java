package com.unionpay.upomp.lthj.plugin.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.SeekBar;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.lthj.unipay.plugin.aa;
import com.lthj.unipay.plugin.ab;
import com.lthj.unipay.plugin.ac;
import com.lthj.unipay.plugin.ad;
import com.lthj.unipay.plugin.af;
import com.lthj.unipay.plugin.ag;
import com.lthj.unipay.plugin.ah;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.aq;
import com.lthj.unipay.plugin.av;
import com.lthj.unipay.plugin.by;
import com.lthj.unipay.plugin.ck;
import com.lthj.unipay.plugin.cu;
import com.lthj.unipay.plugin.el;
import com.lthj.unipay.plugin.h;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.w;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.HeadData;
import com.unionpay.upomp.lthj.plugin.model.JNIInitBottomData;
import com.unionpay.upomp.lthj.util.PluginHelper;
import java.io.File;
import java.util.Calendar;
import java.util.Timer;

public class SplashActivity extends Activity implements UIResponseListener {
    public static Activity instance;
    Handler a = new ag(this);
    public int b;
    public Handler c = new af(this);
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public ah e;
    /* access modifiers changed from: private */
    public Handler f = new ab(this);
    /* access modifiers changed from: private */
    public SeekBar g;

    /* access modifiers changed from: private */
    public void a() {
        try {
            if (ap.a().a != null) {
                el elVar = new el(8198);
                elVar.a(HeadData.createHeadData("PluginInit.Req", this));
                elVar.b(ap.a().a.a());
                elVar.a(ap.a().h);
                ck.a().a(elVar, this, this, false, true);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        Message message = new Message();
        message.obj = str;
        this.a.sendMessage(message);
    }

    /* access modifiers changed from: private */
    public boolean a(byte[] bArr) {
        if (bArr == null) {
            a(getString(PluginLink.getStringupomp_lthj_merchantId_Empty_prompt()));
            return false;
        }
        try {
            ap.a().a(bArr);
            if (TextUtils.isEmpty(ap.a().h)) {
                a(getString(PluginLink.getStringupomp_lthj_merchantId_Empty_prompt()));
                return false;
            } else if (ap.a().h.length() < 15 || ap.a().h.length() > 24) {
                a(getString(PluginLink.getStringupomp_lthj_merchantId_Length_prompt()));
                return false;
            } else if (TextUtils.isEmpty(ap.a().k)) {
                a(getString(PluginLink.getStringupomp_lthj_merchantOrderId_Empty_prompt()));
                return false;
            } else if (TextUtils.isEmpty(ap.a().l)) {
                a(getString(PluginLink.getStringupomp_lthj_merchantOrderTime_Empty_prompt()));
                return false;
            } else {
                if (TextUtils.isEmpty(j.c(ap.a().l))) {
                    a(getString(PluginLink.getStringupomp_lthj_merchantOrderTime_error_prompt()));
                    return false;
                }
                return true;
            }
        } catch (Exception e2) {
            a(getString(PluginLink.getStringupomp_lthj_merchantXml_Format_prompt()));
        }
    }

    public void errorCallBack(String str) {
        if (!isFinishing()) {
            a(str);
        }
    }

    public void initBottomData(byte[] bArr) {
        boolean z = true;
        if (bArr == null) {
            File file = new File(this.d);
            this.e = new ah(this);
            if (!file.exists()) {
                byte[] a2 = ap.a().d ? this.e.a(PluginLink.getRawupomp_lthj_config_test()) : this.e.a(PluginLink.getRawupomp_lthj_config_formal());
                bArr = JniMethod.getJniMethod().decryptConfig(a2, a2.length);
            } else {
                byte[] a3 = this.e.a(this.d);
                bArr = JniMethod.getJniMethod().decryptConfig(a3, a3.length);
                z = false;
            }
        }
        if (bArr == null) {
            try {
                errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_ReadError_prompt()));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            ap.a().a.a(bArr);
            byte[] d2 = ap.a().a.d();
            byte[] e3 = ap.a().a.e();
            byte[] bArr2 = new byte[260];
            bArr2[0] = 0;
            bArr2[1] = 4;
            bArr2[2] = 0;
            bArr2[3] = 0;
            System.arraycopy(d2, 0, bArr2, 4, d2.length);
            System.arraycopy(e3, 0, bArr2, 257, e3.length);
            byte[] f2 = ap.a().a.f();
            byte[] g2 = ap.a().a.g();
            byte[] bArr3 = new byte[260];
            bArr3[0] = 0;
            bArr3[1] = 4;
            bArr3[2] = 0;
            bArr3[3] = 0;
            System.arraycopy(f2, 0, bArr3, 4, f2.length);
            System.arraycopy(g2, 0, bArr3, 257, g2.length);
            JNIInitBottomData jNIInitBottomData = new JNIInitBottomData();
            jNIInitBottomData.certVersion = ap.a().a.b();
            jNIInitBottomData.phoneIMEI = j.a(this);
            jNIInitBottomData.imsi = j.b(this);
            if (ap.a().d) {
                JniMethod.getJniMethod().initResource(1, jNIInitBottomData, bArr2, bArr3);
            } else {
                JniMethod.getJniMethod().initResource(0, jNIInitBottomData, bArr2, bArr3);
            }
            if (z) {
                this.e.a(this.d, JniMethod.getJniMethod().encryptConfig(bArr, bArr.length));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean
     arg types: [com.unionpay.upomp.lthj.plugin.ui.SplashActivity, int]
     candidates:
      com.lthj.unipay.plugin.j.a(android.content.Context, java.lang.String):void
      com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[]
      com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(PluginLink.getLayoutupomp_lthj_splash());
        instance = this;
        getWindow().setSoftInputMode(18);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_textview_time())).setText("©2002-" + Calendar.getInstance().get(1));
        this.g = (SeekBar) findViewById(PluginLink.getIdupomp_lthj_splash_seekbar());
        this.g.setEnabled(false);
        this.b = 0;
        this.g.setProgress(this.b);
        new Timer().schedule(new ad(this), 0, 100);
        aq.a = PoiTypeDef.All;
        ap.a().g = j.a((Context) this, false);
        new Thread(new ac(this)).start();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return 4 == i;
    }

    public void responseCallBack(w wVar) {
        if (!isFinishing() && wVar != null) {
            int parseInt = Integer.parseInt(wVar.m());
            switch (wVar.e()) {
                case 8198:
                    el elVar = (el) wVar;
                    if (parseInt != 0) {
                        a("初始化失败，" + elVar.n() + "，错误码为" + elVar.m());
                        return;
                    } else if (!elVar.d()) {
                        byte[] a2 = j.a(elVar.a().getBytes());
                        byte[] b2 = j.b((new String(a2) + j.a(this)).getBytes());
                        byte[] a3 = j.a(elVar.c().getBytes());
                        if (a3 == null || a3.length != 16) {
                            errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_HashFormat_prompt()));
                            return;
                        }
                        for (int i = 0; i < 16; i++) {
                            if (b2[i] != a3[i]) {
                                errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_MD5Error_prompt()));
                                return;
                            }
                        }
                        by byVar = new by(this);
                        byVar.a(a2);
                        byVar.start();
                        return;
                    } else {
                        String k = elVar.k();
                        String str = PoiTypeDef.All;
                        h hVar = new h(this);
                        cu a4 = hVar.a(1);
                        if (!(a4 == null || a4.b() == null)) {
                            str = a4.b();
                            byte[] bytes = str.getBytes();
                            byte[] decryptConfig = JniMethod.getJniMethod().decryptConfig(bytes, bytes.length);
                            if (decryptConfig != null) {
                                str = new String(decryptConfig);
                            }
                        }
                        if (!k.equals(str)) {
                            byte[] bytes2 = k.getBytes();
                            String str2 = new String(JniMethod.getJniMethod().encryptConfig(bytes2, bytes2.length));
                            if (a4 == null) {
                                cu cuVar = new cu();
                                cuVar.a(str2);
                                hVar.a(cuVar);
                            } else {
                                a4.a(str2);
                                hVar.b(a4);
                            }
                        }
                        ap.a().e = true;
                        av avVar = new av(8223);
                        avVar.a(HeadData.createHeadData("CheckOrder.Req", this));
                        avVar.a(ap.a().h);
                        avVar.b(ap.a().k);
                        avVar.c(ap.a().l);
                        avVar.d(ap.a().r);
                        try {
                            ck.a().a(avVar, this, this, false, true);
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                case 8223:
                    av avVar2 = (av) wVar;
                    if (parseInt == 0) {
                        ap.a().i = avVar2.a();
                        ap.a().j = avVar2.s();
                        ap.a().m = avVar2.c();
                        ap.a().n = avVar2.d();
                        ap.a().p = avVar2.o();
                        ap.a().q = avVar2.q();
                        ap.a().o = avVar2.r();
                        ap.a().K = avVar2.p();
                        synchronized (PluginHelper.a) {
                            PluginHelper.a.notify();
                        }
                        finish();
                        return;
                    }
                    a(avVar2.n() + "，错误码为" + avVar2.m());
                    return;
                default:
                    return;
            }
        }
    }

    public void showInitDialog(String str) {
        new AlertDialog.Builder(this).setTitle("提示").setMessage(str).setPositiveButton("确定", new aa(this)).show();
    }
}
