package com.unionpay.upomp.lthj.plugin.ui;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.lthj.unipay.plugin.al;
import com.lthj.unipay.plugin.an;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.at;
import com.lthj.unipay.plugin.bp;
import com.lthj.unipay.plugin.j;
import com.unionpay.upomp.lthj.link.PluginLink;

public class IndexActivityGroup extends ActivityGroup implements View.OnClickListener, bp {
    public static IndexActivityGroup instance;
    private Button a;
    private ImageView b;

    private void a() {
        changeSubActivity(new Intent(this, HomeActivity.class));
    }

    public void aboutEnable(boolean z) {
        if (this.b != null) {
            this.b.setEnabled(z);
        }
    }

    public void backToMerchant() {
        byte[] d = ap.a().d();
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putByteArray("xml", d);
        at.a("IndexActivityGroup", "back" + new String(d));
        intent.putExtras(bundle);
        setResult(2001, intent);
        finish();
        j.c();
    }

    public void changeSubActivity(Intent intent) {
        View decorView = getLocalActivityManager().startActivity(intent.getComponent().getShortClassName(), intent).getDecorView();
        decorView.clearFocus();
        ViewGroup viewGroup = (ViewGroup) findViewById(PluginLink.getIdupomp_lthj_container());
        viewGroup.removeAllViews();
        viewGroup.addView(decorView);
    }

    public void changeTitleButton(String str, View.OnClickListener onClickListener) {
        if (str == null || onClickListener == null) {
            this.a.setVisibility(8);
            return;
        }
        this.a.setVisibility(0);
        this.a.setText(str);
        this.a.setOnClickListener(onClickListener);
    }

    public void onClick(View view) {
        if (view == this.b) {
            changeSubActivity(new Intent(this, AboutActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(PluginLink.getLayoutupomp_lthj_index());
        this.a = (Button) findViewById(PluginLink.getIdupomp_lthj_homebackbutton());
        this.b = (ImageView) findViewById(PluginLink.getIdupomp_lthj_about_btn());
        this.b.setOnClickListener(this);
        a();
        instance = this;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return getCurrentActivity().onKeyDown(i, keyEvent);
    }

    public void quitNotice() {
        new AlertDialog.Builder(this).setTitle("退出交易").setMessage(PluginLink.getStringupomp_lthj_app_quitNotice_msg()).setNegativeButton(PluginLink.getStringupomp_lthj_cancel(), new al(this)).setPositiveButton(PluginLink.getStringupomp_lthj_ok(), new an(this)).show();
    }
}
