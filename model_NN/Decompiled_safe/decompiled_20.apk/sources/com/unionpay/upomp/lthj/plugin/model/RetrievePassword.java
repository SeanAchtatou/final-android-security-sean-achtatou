package com.unionpay.upomp.lthj.plugin.model;

public class RetrievePassword extends Data {
    public String email;
    public String loginName;
    public String newPassword;
    public String secureAnswer;
    public String validateCode;
}
