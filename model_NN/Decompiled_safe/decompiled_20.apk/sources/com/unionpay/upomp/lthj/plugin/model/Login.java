package com.unionpay.upomp.lthj.plugin.model;

public class Login extends Data {
    public String email;
    public String loginName;
    public String mobileNumber;
    public String password;
    public String validateCode;
    public String welcome;
}
