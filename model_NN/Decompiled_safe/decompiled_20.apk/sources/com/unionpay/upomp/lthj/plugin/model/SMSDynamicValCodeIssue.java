package com.unionpay.upomp.lthj.plugin.model;

public class SMSDynamicValCodeIssue extends Data {
    public String mobileMac;
    public String mobileNumber;
    public String secureInfo;
}
