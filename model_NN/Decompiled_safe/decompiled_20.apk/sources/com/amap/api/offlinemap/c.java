package com.amap.api.offlinemap;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Xml;
import com.amap.api.a.b.g;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class c {
    private static String g = null;
    private static Context h = null;
    public ArrayList<g> a = new ArrayList<>();
    public ArrayList<g> b = new ArrayList<>();
    public ArrayList<DownCity> c = new ArrayList<>();
    public ArrayList<DownCity> d = new ArrayList<>();
    Handler e;
    e f;

    public c(Context context, Handler handler) {
        h = context;
        this.e = handler;
        g = g.a(context);
        com.amap.api.a.b.c.b(h);
    }

    public static String a() {
        return g.b(h);
    }

    public void a(int i) {
        if (this.b != null && this.b.size() > 0) {
            g gVar = this.b.get(i);
            try {
                this.f = new e(new f(gVar.c(), a(), (gVar.b() + ".zip") + ".tmp", 5), this, gVar, h);
                this.f.start();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(g gVar) {
        gVar.a = 3;
        synchronized (this.b) {
            this.b.remove(gVar);
        }
        synchronized (this.a) {
            this.a.remove(gVar);
        }
        b(gVar.d());
        String a2 = gVar.a();
        String str = a2 + ".dt";
        if (a2 != null && a2.length() > 0) {
            try {
                new File(a2).delete();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (str != null && str.length() > 0) {
            try {
                new File(str).delete();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        String str2 = a2 + ".info";
        if (str2 != null && str2.length() > 0) {
            try {
                new File(str2).delete();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
    }

    public void a(g gVar, int i, int i2) {
        Bundle bundle = new Bundle();
        bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, i);
        bundle.putInt("completepercent", i2);
        Message obtainMessage = this.e.obtainMessage();
        obtainMessage.setData(bundle);
        this.e.sendMessage(obtainMessage);
        gVar.a = i;
        gVar.e();
    }

    public void b() {
        if (this.f != null) {
            this.f.b();
            this.f.interrupt();
            Bundle bundle = new Bundle();
            bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, 5);
            bundle.putInt("completepercent", 100);
            Message obtainMessage = this.e.obtainMessage();
            obtainMessage.setData(bundle);
            this.e.sendMessage(obtainMessage);
            this.b.clear();
            if (this.f != null) {
                this.f.b();
            }
        }
    }

    public void b(int i) {
        if (this.f != null) {
            this.f.b();
            this.f.interrupt();
            Bundle bundle = new Bundle();
            bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, 3);
            bundle.putInt("completepercent", 100);
            Message obtainMessage = this.e.obtainMessage();
            obtainMessage.setData(bundle);
            this.e.sendMessage(obtainMessage);
        }
    }

    public void b(g gVar) {
        String str = a() + gVar.b() + ".zip";
        gVar.e();
        File file = new File(str);
        new File(str + ".tmp").renameTo(file);
        Bundle bundle = new Bundle();
        bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, 1);
        bundle.putInt("completepercent", 100);
        Message obtainMessage = this.e.obtainMessage();
        obtainMessage.setData(bundle);
        this.e.sendMessage(obtainMessage);
        gVar.a = 1;
        gVar.e();
        if (file.exists()) {
            i.a(g + "vmap/", str);
            new File(str).delete();
            Bundle bundle2 = new Bundle();
            bundle2.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, 4);
            bundle2.putInt("completepercent", 100);
            Message obtainMessage2 = this.e.obtainMessage();
            obtainMessage.setData(bundle2);
            this.e.sendMessage(obtainMessage2);
        }
        gVar.a = 4;
        synchronized (this.b) {
            this.b.remove(gVar);
        }
        gVar.e();
    }

    public void c() {
        this.c.clear();
        try {
            InputStream open = h.getAssets().open("1010.png");
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(open, "utf-8");
            int eventType = newPullParser.getEventType();
            DownCity downCity = null;
            while (eventType != 1) {
                switch (eventType) {
                    case 2:
                        String name = newPullParser.getName();
                        if (name.equalsIgnoreCase("province")) {
                            String attributeValue = newPullParser.getAttributeValue(null, "code");
                            if (attributeValue == null || attributeValue.equals(PoiTypeDef.All)) {
                                eventType = newPullParser.next();
                                continue;
                            } else {
                                downCity = new DownCity();
                                downCity.setCode(attributeValue);
                            }
                        }
                        if (name.equalsIgnoreCase("city")) {
                            downCity = new DownCity();
                            downCity.setVersion("20130225");
                            downCity.setCode(newPullParser.getAttributeValue(null, "code"));
                        }
                        if (downCity != null) {
                            if (name.equalsIgnoreCase("name")) {
                                downCity.setCity(newPullParser.nextText());
                            } else if (name.equalsIgnoreCase("pinyin")) {
                                String nextText = newPullParser.nextText();
                                downCity.setPinyin(nextText);
                                downCity.setInitial(nextText.substring(0, 1));
                            } else if (name.equalsIgnoreCase("durl")) {
                                downCity.setDurl(newPullParser.nextText());
                            } else if (name.equalsIgnoreCase("size")) {
                                downCity.setSize(Long.parseLong(newPullParser.nextText()));
                            }
                        }
                        break;
                    case 3:
                        if (!(newPullParser.getName() == null || !newPullParser.getName().equalsIgnoreCase("size") || downCity == null)) {
                            this.d.add(downCity);
                            this.c.add(downCity);
                            downCity = null;
                            break;
                        }
                }
                eventType = newPullParser.next();
            }
            Collections.sort(this.c);
            Collections.sort(this.d);
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (XmlPullParserException e3) {
            e3.printStackTrace();
        }
    }
}
