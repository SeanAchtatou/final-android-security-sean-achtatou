package com.amap.api.offlinemap;

public class City implements Comparable {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;

    public int compareTo(Object obj) {
        String str = ((City) obj).d;
        if (str.charAt(0) > this.d.charAt(0)) {
            return -1;
        }
        return str.charAt(0) < this.d.charAt(0) ? 1 : 0;
    }

    public String getCity() {
        return this.b;
    }

    public String getCode() {
        return this.c;
    }

    public String getInitial() {
        return this.d;
    }

    public String getPinyin() {
        return this.e;
    }

    public String getProvince() {
        return this.a;
    }

    public void setCity(String str) {
        this.b = str;
    }

    public void setCode(String str) {
        this.c = str;
    }

    public void setInitial(String str) {
        this.d = str;
    }

    public void setPinyin(String str) {
        this.e = str;
    }

    public void setProvince(String str) {
        this.a = str;
    }
}
