package com.amap.api.offlinemap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class i {
    public static void a(String str, String str2) {
        try {
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(str2));
            File file = new File(str);
            file.mkdirs();
            a(zipInputStream, file);
            zipInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void a(ZipInputStream zipInputStream, File file) {
        ZipEntry nextEntry = zipInputStream.getNextEntry();
        while (nextEntry != null) {
            File file2 = new File(file.getAbsolutePath() + CookieSpec.PATH_DELIM + nextEntry.getName());
            if (nextEntry.isDirectory()) {
                file2.mkdirs();
                a(zipInputStream, file);
            } else {
                file2.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                byte[] bArr = new byte[2048];
                while (true) {
                    int read = zipInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                fileOutputStream.close();
            }
            nextEntry = zipInputStream.getNextEntry();
        }
    }
}
