package com.amap.api.offlinemap;

import android.os.Handler;
import android.os.Message;
import com.amap.api.location.LocationManagerProxy;

class d extends Handler {
    final /* synthetic */ OfflineMapManager a;

    d(OfflineMapManager offlineMapManager) {
        this.a = offlineMapManager;
    }

    public void handleMessage(Message message) {
        this.a.c.onDownload(message.getData().getInt(LocationManagerProxy.KEY_STATUS_CHANGED), message.getData().getInt("completepercent"));
    }
}
