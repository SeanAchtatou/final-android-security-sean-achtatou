package com.amap.api.offlinemap;

import android.content.Context;
import android.os.Handler;
import com.amap.api.a.b.c;
import com.amap.api.a.b.g;
import java.util.ArrayList;
import java.util.Iterator;

public final class OfflineMapManager {
    Handler a = new d(this);
    private c b;
    /* access modifiers changed from: private */
    public OfflineMapDownloadListener c;
    private ArrayList<City> d = new ArrayList<>();
    private Context e;

    public interface OfflineMapDownloadListener {
        void onDownload(int i, int i2);
    }

    public OfflineMapManager(Context context, OfflineMapDownloadListener offlineMapDownloadListener) {
        this.e = context;
        this.b = new c(context, this.a);
        c.a(context);
        this.c = offlineMapDownloadListener;
        this.b.c();
    }

    private void a(DownCity downCity) {
        this.d.add(downCity);
        g gVar = new g(downCity);
        gVar.a(this.b.b.size());
        gVar.a = 2;
        this.b.b.add(gVar);
        this.b.a(this.b.b.size() - 1);
    }

    public final boolean downloadByCityCode(String str) {
        DownCity itemByCityCode;
        if (!g.c(this.e) || (itemByCityCode = getItemByCityCode(str)) == null) {
            return false;
        }
        a(itemByCityCode);
        return true;
    }

    public final boolean downloadByCityName(String str) {
        DownCity itemByCityName;
        if (!g.c(this.e) || (itemByCityName = getItemByCityName(str)) == null) {
            return false;
        }
        a(itemByCityName);
        return true;
    }

    public final ArrayList<City> getDownloadingCityList() {
        return this.d;
    }

    public final DownCity getItemByCityCode(String str) {
        Iterator<DownCity> it = this.b.d.iterator();
        while (it.hasNext()) {
            DownCity next = it.next();
            if (next.getCode().equals(str)) {
                return next;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.amap.api.offlinemap.DownCity getItemByCityName(java.lang.String r5) {
        /*
            r4 = this;
            com.amap.api.offlinemap.c r0 = r4.b
            java.util.ArrayList<com.amap.api.offlinemap.DownCity> r0 = r0.d
            java.util.Iterator r1 = r0.iterator()
        L_0x0008:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0025
            java.lang.Object r0 = r1.next()
            com.amap.api.offlinemap.DownCity r0 = (com.amap.api.offlinemap.DownCity) r0
            java.lang.String r2 = r0.getCity()
            boolean r3 = r2.contains(r5)
            if (r3 != 0) goto L_0x0024
            boolean r2 = r5.contains(r2)
            if (r2 == 0) goto L_0x0008
        L_0x0024:
            return r0
        L_0x0025:
            r0 = 0
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.offlinemap.OfflineMapManager.getItemByCityName(java.lang.String):com.amap.api.offlinemap.DownCity");
    }

    public final ArrayList<DownCity> getOfflineCityList() {
        return this.b.d;
    }

    public final void pause() {
        this.b.b(0);
    }

    public final void remove(String str) {
        DownCity itemByCityName = getItemByCityName(str);
        if (itemByCityName != null) {
            this.b.a(new g(itemByCityName));
        }
    }

    public final void restart() {
        this.b.a(this.b.b.size() - 1);
    }

    public final void stop() {
        this.b.b();
    }
}
