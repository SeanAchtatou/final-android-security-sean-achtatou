package com.amap.api.offlinemap;

public class b extends Thread {
    String a;
    long b;
    long c;
    int d;
    boolean e = false;
    boolean f = false;
    a g = null;

    public b(String str, String str2, long j, long j2, int i) {
        this.a = str;
        this.b = j;
        this.c = j2;
        this.d = i;
        this.g = new a(str2, this.b);
    }

    public void a() {
        this.f = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        com.amap.api.offlinemap.h.a("Thread " + r7.d + " is over!");
        r7.e = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
        L_0x0000:
            long r0 = r7.b
            long r2 = r7.c
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x0094
            boolean r0 = r7.f
            if (r0 != 0) goto L_0x0094
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0070 }
            java.lang.String r1 = r7.a     // Catch:{ Exception -> 0x0070 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0070 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0070 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0070 }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = "text/xml;"
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0070 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0070 }
            java.lang.String r2 = "bytes="
            r1.<init>(r2)     // Catch:{ Exception -> 0x0070 }
            long r2 = r7.b     // Catch:{ Exception -> 0x0070 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r2 = "-"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0070 }
            java.lang.String r2 = "RANGE"
            r0.setRequestProperty(r2, r1)     // Catch:{ Exception -> 0x0070 }
            com.amap.api.offlinemap.h.a(r1)     // Catch:{ Exception -> 0x0070 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x0070 }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x0070 }
        L_0x004c:
            r2 = 0
            r3 = 1024(0x400, float:1.435E-42)
            int r2 = r0.read(r1, r2, r3)     // Catch:{ Exception -> 0x0070 }
            if (r2 <= 0) goto L_0x0075
            long r3 = r7.b     // Catch:{ Exception -> 0x0070 }
            long r5 = r7.c     // Catch:{ Exception -> 0x0070 }
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x0075
            boolean r3 = r7.f     // Catch:{ Exception -> 0x0070 }
            if (r3 != 0) goto L_0x0075
            long r3 = r7.b     // Catch:{ Exception -> 0x0070 }
            com.amap.api.offlinemap.a r5 = r7.g     // Catch:{ Exception -> 0x0070 }
            r6 = 0
            int r2 = r5.a(r1, r6, r2)     // Catch:{ Exception -> 0x0070 }
            long r5 = (long) r2     // Catch:{ Exception -> 0x0070 }
            long r2 = r3 + r5
            r7.b = r2     // Catch:{ Exception -> 0x0070 }
            goto L_0x004c
        L_0x0070:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0000
        L_0x0075:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0070 }
            java.lang.String r1 = "Thread "
            r0.<init>(r1)     // Catch:{ Exception -> 0x0070 }
            int r1 = r7.d     // Catch:{ Exception -> 0x0070 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r1 = " is over!"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0070 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0070 }
            com.amap.api.offlinemap.h.a(r0)     // Catch:{ Exception -> 0x0070 }
            r0 = 1
            r7.e = r0     // Catch:{ Exception -> 0x0070 }
            goto L_0x0000
        L_0x0094:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.offlinemap.b.run():void");
    }
}
