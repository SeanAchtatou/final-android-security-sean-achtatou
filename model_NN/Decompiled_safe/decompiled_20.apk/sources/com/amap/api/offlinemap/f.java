package com.amap.api.offlinemap;

import com.amap.api.search.poisearch.PoiTypeDef;

public class f {
    private String a;
    private String b;
    private String c;
    private int d;

    public f() {
        this(PoiTypeDef.All, PoiTypeDef.All, PoiTypeDef.All, 5);
    }

    public f(String str, String str2, String str3, int i) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = i;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public int d() {
        return this.d;
    }
}
