package com.amap.api.offlinemap;

import com.amap.api.search.poisearch.PoiTypeDef;

public class DownCity extends City {
    private String a = PoiTypeDef.All;
    private long b = 0;
    private int c = -1;
    private String d = PoiTypeDef.All;
    private String e = PoiTypeDef.All;

    public String getDurl() {
        return this.a;
    }

    public String getJianpin() {
        return this.e;
    }

    public long getSize() {
        return this.b;
    }

    public String getVersion() {
        return this.d;
    }

    public int getmState() {
        return this.c;
    }

    public void setDurl(String str) {
        this.a = str;
    }

    public void setJianpin(String str) {
        this.e = str;
    }

    public void setSize(long j) {
        this.b = j;
    }

    public void setVersion(String str) {
        this.d = str;
    }

    public void setmState(int i) {
        this.c = i;
    }
}
