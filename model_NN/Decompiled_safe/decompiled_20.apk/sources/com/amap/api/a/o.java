package com.amap.api.a;

import android.os.RemoteException;
import android.util.Log;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.microedition.khronos.opengles.GL10;

class o {
    private static int b = 0;
    a a = new a();
    private CopyOnWriteArrayList<t> c = new CopyOnWriteArrayList<>();

    class a implements Comparator<Object> {
        a() {
        }

        public int compare(Object obj, Object obj2) {
            t tVar = (t) obj;
            t tVar2 = (t) obj2;
            if (!(tVar == null || tVar2 == null)) {
                try {
                    if (tVar.d() > tVar2.d()) {
                        return 1;
                    }
                    if (tVar.d() < tVar2.d()) {
                        return -1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return 0;
        }
    }

    o() {
    }

    static String a(String str) {
        b++;
        return str + b;
    }

    private t c(String str) {
        Iterator<t> it = this.c.iterator();
        while (it.hasNext()) {
            t next = it.next();
            if (next != null && next.c().equals(str)) {
                return next;
            }
        }
        return null;
    }

    private void d() {
        Object[] array = this.c.toArray();
        Arrays.sort(array, this.a);
        this.c.clear();
        for (Object obj : array) {
            this.c.add((t) obj);
        }
    }

    public void a() {
        try {
            this.c.clear();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("amapApi", "GLOverlayLayer clear erro" + e.getMessage());
        }
    }

    public void a(t tVar) {
        b(tVar.c());
        this.c.add(tVar);
        d();
    }

    public void a(GL10 gl10) {
        int size = this.c.size();
        Iterator<t> it = this.c.iterator();
        while (it.hasNext()) {
            t next = it.next();
            try {
                if (next.e()) {
                    if (size <= 20) {
                        next.a(gl10);
                    } else if (next.a()) {
                        next.a(gl10);
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void b() {
        try {
            Iterator<t> it = this.c.iterator();
            while (it.hasNext()) {
                it.next().n();
            }
            a();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("amapApi", "GLOverlayLayer destory erro" + e.getMessage());
        }
    }

    public boolean b(String str) {
        t c2 = c(str);
        if (c2 != null) {
            return this.c.remove(c2);
        }
        return false;
    }

    public void c() {
        Iterator<t> it = this.c.iterator();
        while (it.hasNext()) {
            t next = it.next();
            if (next != null) {
                try {
                    next.g();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
