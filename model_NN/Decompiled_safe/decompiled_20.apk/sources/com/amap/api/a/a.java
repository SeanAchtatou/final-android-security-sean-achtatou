package com.amap.api.a;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.view.MotionEventCompat;
import com.amap.api.a.b.e;
import com.amap.api.a.b.g;
import com.amap.api.a.j;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.search.route.Route;
import com.autonavi.amap.mapcore.BaseMapCallImplement;
import com.autonavi.amap.mapcore.DPoint;
import com.autonavi.amap.mapcore.IPoint;
import com.autonavi.amap.mapcore.MapCore;
import com.autonavi.amap.mapcore.MapProjection;
import javax.microedition.khronos.opengles.GL10;

class a extends BaseMapCallImplement {
    IPoint a = new IPoint();
    float b;
    float c;
    float d;
    IPoint e = new IPoint();
    /* access modifiers changed from: private */
    public b f;
    private final Handler g = new Handler();
    private float h = -1.0f;

    /* renamed from: com.amap.api.a.a$2  reason: invalid class name */
    /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] a = new int[j.a.values().length];

        static {
            try {
                a[j.a.changeCenter.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[j.a.changeBearing.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[j.a.changeTilt.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[j.a.changeGeoCenterZoom.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[j.a.newCameraPosition.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[j.a.zoomIn.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[j.a.zoomOut.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[j.a.zoomTo.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[j.a.zoomBy.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[j.a.scrollBy.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                a[j.a.newLatLngBounds.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                a[j.a.newLatLngBoundsWithSize.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                a[j.a.changeGeoCenterZoomTiltBearing.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                a[j.a.STISET.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
        }
    }

    public a(b bVar) {
        this.f = bVar;
    }

    private void b(j jVar) {
        MapProjection c2 = this.f.c();
        boolean z = (c2.getMapAngle() == BitmapDescriptorFactory.HUE_RED && c2.getCameraHeaderAngle() == BitmapDescriptorFactory.HUE_RED) ? false : true;
        LatLngBounds latLngBounds = jVar.i;
        int i = jVar.k;
        int i2 = jVar.l;
        int i3 = jVar.j;
        if (!z) {
            IPoint iPoint = new IPoint();
            MapProjection.lonlat2Geo((latLngBounds.northeast.longitude + latLngBounds.southwest.longitude) / 2.0d, (latLngBounds.northeast.latitude + latLngBounds.southwest.latitude) / 2.0d, iPoint);
            float mapZoomer = c2.getMapZoomer();
            IPoint iPoint2 = new IPoint();
            IPoint iPoint3 = new IPoint();
            this.f.b(latLngBounds.northeast.latitude, latLngBounds.northeast.longitude, iPoint2);
            this.f.b(latLngBounds.southwest.latitude, latLngBounds.southwest.longitude, iPoint3);
            int i4 = iPoint2.x - iPoint3.x;
            int i5 = iPoint3.y - iPoint2.y;
            if (i4 != 0 || i5 != 0) {
                float max = Math.max(((float) i4) / ((float) (i - (i3 * 2))), ((float) i5) / ((float) (i2 - (i3 * 2))));
                float b2 = max > 1.0f ? g.b(mapZoomer - ((float) g.c(max))) : ((double) max) < 0.5d ? g.b((((float) g.c(1.0f / max)) + mapZoomer) - 1.0f) : mapZoomer;
                c2.setGeoCenter(iPoint.x, iPoint.y);
                c2.setMapZoomer(b2);
                return;
            }
            return;
        }
        jVar.q = false;
        Message message = new Message();
        message.obj = j.a(latLngBounds, i, i2, i3);
        message.what = 12;
        this.f.e.sendMessage(message);
    }

    public void OnMapDestory(GL10 gl10, MapCore mapCore) {
        super.OnMapDestory(mapCore);
    }

    public void OnMapLoaderError(int i) {
        e.a("MapCore", "OnMapLoaderError=" + i, 112);
    }

    public void OnMapProcessEvent(MapCore mapCore) {
        float z = this.f.z();
        a(mapCore);
        while (true) {
            ac a2 = this.f.a.a();
            if (a2 == null) {
                break;
            }
            if (a2.a == 2) {
                if (a2.a()) {
                    mapCore.setParameter(2010, 4, 0, 0, 0);
                } else {
                    mapCore.setParameter(2010, 0, 0, 0, 0);
                }
            }
            if (a2.a == 2011) {
                if (a2.b) {
                    mapCore.setParameter(2011, 3, 0, 0, 0);
                } else {
                    mapCore.setParameter(2011, 0, 0, 0, 0);
                }
            }
        }
        mapCore.setMapstate(this.f.c());
        if (this.b >= this.f.n() && this.h != z) {
            this.g.postDelayed(new Runnable() {
                public void run() {
                    if (a.this.f != null && a.this.f.b != null) {
                        a.this.f.b.a(a.this.f.z());
                    }
                }
            }, 0);
        }
        this.h = z;
    }

    public void OnMapReferencechanged(MapCore mapCore, String str, String str2) {
        try {
            if (this.f.u().c()) {
                this.f.f();
            }
            if (this.f.u().a()) {
                this.f.g();
            }
            this.f.E();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
        this.f.d.c();
    }

    public void OnMapSufaceChanged(GL10 gl10, MapCore mapCore, int i, int i2) {
    }

    public void OnMapSurfaceCreate(GL10 gl10, MapCore mapCore) {
        super.OnMapSurfaceCreate(mapCore);
    }

    public void OnMapSurfaceRenderer(GL10 gl10, MapCore mapCore, int i) {
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar) {
        MapCore a2 = this.f.a();
        MapProjection c2 = this.f.c();
        jVar.d = this.f.a(jVar.d);
        jVar.f = g.a(jVar.f);
        switch (AnonymousClass2.a[jVar.a.ordinal()]) {
            case 1:
                c2.setGeoCenter(jVar.p.x, jVar.p.y);
                break;
            case 2:
                c2.setMapAngle(jVar.g);
                break;
            case 3:
                c2.setCameraHeaderAngle(jVar.f);
                break;
            case 4:
                c2.setGeoCenter(jVar.p.x, jVar.p.y);
                c2.setMapZoomer(jVar.d);
                break;
            case 5:
                LatLng latLng = jVar.h.target;
                IPoint iPoint = new IPoint();
                MapProjection.lonlat2Geo(latLng.longitude, latLng.latitude, iPoint);
                float b2 = g.b(jVar.h.zoom);
                float f2 = jVar.h.bearing;
                float a3 = g.a(jVar.h.tilt);
                c2.setGeoCenter(iPoint.x, iPoint.y);
                c2.setMapZoomer(b2);
                c2.setMapAngle(f2);
                c2.setCameraHeaderAngle(a3);
                break;
            case 6:
                c2.setMapZoomer(this.f.a(c2.getMapZoomer() + 1.0f));
                break;
            case MotionEventCompat.ACTION_HOVER_MOVE:
                c2.setMapZoomer(this.f.a(c2.getMapZoomer() - 1.0f));
                break;
            case 8:
                c2.setMapZoomer(jVar.d);
                break;
            case MotionEventCompat.ACTION_HOVER_ENTER:
                float a4 = this.f.a(jVar.e + c2.getMapZoomer());
                Point point = jVar.m;
                float mapZoomer = a4 - c2.getMapZoomer();
                if (point != null) {
                    IPoint iPoint2 = new IPoint();
                    IPoint iPoint3 = new IPoint();
                    c2.getGeoCenter(iPoint3);
                    this.f.a(point.x, point.y, iPoint2);
                    int i = iPoint3.x - iPoint2.x;
                    int i2 = iPoint3.y - iPoint2.y;
                    c2.setGeoCenter((int) (((double) iPoint2.x) + (((double) i) / Math.pow(2.0d, (double) mapZoomer))), (int) ((((double) i2) / Math.pow(2.0d, (double) mapZoomer)) + ((double) iPoint2.y)));
                }
                c2.setMapZoomer(a4);
                a2.setMapstate(c2);
                return;
            case 10:
                float f3 = jVar.b;
                float f4 = jVar.c;
                float width = f3 + ((float) (this.f.getWidth() / 2));
                float height = f4 + ((float) (this.f.getHeight() / 2));
                IPoint iPoint4 = new IPoint();
                this.f.a((int) width, (int) height, iPoint4);
                c2.setGeoCenter(iPoint4.x, iPoint4.y);
                break;
            case Route.DrivingSaveMoney /*11*/:
                b(j.a(jVar.i, this.f.getWidth(), this.f.getHeight(), jVar.j));
                return;
            case Route.DrivingLeastDistance /*12*/:
                b(jVar);
                return;
            case Route.DrivingNoFastRoad /*13*/:
                c2.setGeoCenter(jVar.p.x, jVar.p.y);
                c2.setMapZoomer(jVar.d);
                c2.setMapAngle(jVar.g);
                c2.setCameraHeaderAngle(jVar.f);
                break;
            case 14:
                if (jVar.n) {
                    a2.setParameter(2011, 3, 0, 0, 0);
                } else {
                    a2.setParameter(2011, 0, 0, 0, 0);
                }
                a2.setMapstate(c2);
                return;
        }
        a2.setMapstate(c2);
    }

    /* access modifiers changed from: package-private */
    public void a(MapCore mapCore) {
        boolean z = false;
        MapProjection c2 = this.f.c();
        float mapZoomer = c2.getMapZoomer();
        float cameraHeaderAngle = c2.getCameraHeaderAngle();
        float mapAngle = c2.getMapAngle();
        c2.getGeoCenter(this.e);
        boolean z2 = false;
        while (true) {
            j c3 = this.f.a.c();
            if (c3 != null) {
                try {
                    a(c3);
                    z2 = c3.q;
                    if (c3.o) {
                        return;
                    }
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                this.b = c2.getMapZoomer();
                this.c = c2.getCameraHeaderAngle();
                this.d = c2.getMapAngle();
                c2.getGeoCenter(this.a);
                if (!(mapZoomer == this.b && this.c == cameraHeaderAngle && this.d == mapAngle && this.a.x == this.e.x && this.a.y == this.e.y)) {
                    z = true;
                }
                if (z) {
                    try {
                        this.f.e(false);
                        if (this.f.w() != null) {
                            DPoint dPoint = new DPoint();
                            MapProjection.geo2LonLat(this.a.x, this.a.y, dPoint);
                            this.f.a(new CameraPosition(new LatLng(dPoint.y, dPoint.x), this.b, this.c, this.d));
                        }
                        this.f.A();
                        this.f.E();
                    } catch (RemoteException e3) {
                        e3.printStackTrace();
                        return;
                    }
                } else {
                    this.f.e(true);
                }
                if (z2) {
                    Message message = new Message();
                    message.what = 17;
                    this.f.e.sendMessage(message);
                }
                if (!(this.c == cameraHeaderAngle && this.d == mapAngle) && this.f.u().c()) {
                    this.f.f();
                }
                if (mapZoomer != this.b && this.f.u().a()) {
                    this.f.g();
                    return;
                }
                return;
            }
        }
    }

    public Context getContext() {
        return this.f.getContext();
    }

    public String getMapSvrAddress() {
        return "http://m.amap.com";
    }

    public boolean isMapEngineValid() {
        return this.f.a() != null;
    }

    public void onSetParameter(MapCore mapCore) {
        this.f.e(false);
    }
}
