package com.amap.api.a;

import android.graphics.Point;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.autonavi.amap.mapcore.IPoint;

public class j {
    a a = a.none;
    float b;
    float c;
    float d;
    float e;
    float f;
    float g;
    CameraPosition h;
    LatLngBounds i;
    int j;
    int k;
    int l;
    Point m = null;
    boolean n = false;
    boolean o = false;
    IPoint p;
    boolean q = false;

    enum a {
        none,
        zoomIn,
        changeCenter,
        changeTilt,
        changeBearing,
        changeGeoCenterZoom,
        STISET,
        TRAFFICSET,
        zoomOut,
        zoomTo,
        zoomBy,
        scrollBy,
        newCameraPosition,
        newLatLngBounds,
        newLatLngBoundsWithSize,
        changeGeoCenterZoomTiltBearing
    }

    private j() {
    }

    public static j a() {
        return new j();
    }

    public static j a(float f2) {
        j a2 = a();
        a2.a = a.zoomTo;
        a2.d = f2;
        return a2;
    }

    public static j a(float f2, float f3) {
        j a2 = a();
        a2.a = a.scrollBy;
        a2.b = f2;
        a2.c = f3;
        return a2;
    }

    public static j a(float f2, Point point) {
        j a2 = a();
        a2.a = a.zoomBy;
        a2.e = f2;
        a2.m = point;
        return a2;
    }

    public static j a(CameraPosition cameraPosition) {
        j a2 = a();
        a2.a = a.newCameraPosition;
        a2.h = cameraPosition;
        return a2;
    }

    public static j a(LatLng latLng) {
        return a(CameraPosition.builder().target(latLng).build());
    }

    public static j a(LatLng latLng, float f2) {
        return a(CameraPosition.builder().target(latLng).zoom(f2).build());
    }

    public static j a(LatLng latLng, float f2, float f3, float f4) {
        return a(CameraPosition.builder().target(latLng).zoom(f2).bearing(f3).tilt(f4).build());
    }

    public static j a(LatLngBounds latLngBounds, int i2) {
        j a2 = a();
        a2.a = a.newLatLngBounds;
        a2.i = latLngBounds;
        a2.j = i2;
        return a2;
    }

    public static j a(LatLngBounds latLngBounds, int i2, int i3, int i4) {
        j a2 = a();
        a2.a = a.newLatLngBoundsWithSize;
        a2.i = latLngBounds;
        a2.j = i4;
        a2.k = i2;
        a2.l = i3;
        return a2;
    }

    public static j a(IPoint iPoint) {
        j a2 = a();
        a2.a = a.changeCenter;
        a2.p = iPoint;
        return a2;
    }

    static j a(IPoint iPoint, float f2, float f3, float f4) {
        j a2 = a();
        a2.a = a.changeGeoCenterZoomTiltBearing;
        a2.p = iPoint;
        a2.d = f2;
        a2.g = f3;
        a2.f = f4;
        return a2;
    }

    public static j b() {
        j a2 = a();
        a2.a = a.zoomIn;
        return a2;
    }

    public static j b(float f2) {
        return a(f2, (Point) null);
    }

    public static j c() {
        j a2 = a();
        a2.a = a.zoomOut;
        return a2;
    }

    public static j c(float f2) {
        j a2 = a();
        a2.a = a.changeTilt;
        a2.f = f2;
        return a2;
    }

    public static j d(float f2) {
        j a2 = a();
        a2.a = a.changeBearing;
        a2.g = f2;
        return a2;
    }
}
