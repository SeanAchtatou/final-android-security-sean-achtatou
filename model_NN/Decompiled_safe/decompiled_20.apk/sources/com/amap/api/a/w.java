package com.amap.api.a;

import android.graphics.Point;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.VisibleRegion;

public interface w {
    Point a(LatLng latLng);

    LatLng a(Point point);

    VisibleRegion a();
}
