package com.amap.api.a;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.amap.api.maps.AMapOptions;

public interface r {
    View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    p a();

    void a(Activity activity);

    void a(Activity activity, AMapOptions aMapOptions, Bundle bundle);

    void a(Bundle bundle);

    void a(AMapOptions aMapOptions);

    void b();

    void b(Bundle bundle);

    void c();

    void d();

    void e();

    void f();
}
