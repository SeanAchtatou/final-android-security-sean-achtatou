package com.amap.api.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.amap.api.a.b.g;

class aq extends LinearLayout {
    /* access modifiers changed from: private */
    public Bitmap a;
    private Bitmap b;
    /* access modifiers changed from: private */
    public Bitmap c;
    private Bitmap d;
    /* access modifiers changed from: private */
    public Bitmap e;
    /* access modifiers changed from: private */
    public Bitmap f;
    /* access modifiers changed from: private */
    public ImageView g;
    /* access modifiers changed from: private */
    public ImageView h;
    /* access modifiers changed from: private */
    public p i;

    public aq(Context context, ad adVar, p pVar) {
        super(context);
        this.i = pVar;
        try {
            this.a = g.a("zoomin_selected.png");
            this.a = g.a(this.a, m.a);
            this.b = g.a("zoomin_unselected.png");
            this.b = g.a(this.b, m.a);
            this.c = g.a("zoomout_selected.png");
            this.c = g.a(this.c, m.a);
            this.d = g.a("zoomout_unselected.png");
            this.d = g.a(this.d, m.a);
            this.e = g.a("zoomin_pressed.png");
            this.f = g.a("zoomout_pressed.png");
            this.e = g.a(this.e, m.a);
            this.f = g.a(this.f, m.a);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.g = new ImageView(context);
        this.g.setImageBitmap(this.a);
        this.g.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
        this.h = new ImageView(context);
        this.h.setImageBitmap(this.c);
        this.h.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
        this.g.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (aq.this.i.z() < aq.this.i.m()) {
                    if (motionEvent.getAction() == 0) {
                        aq.this.g.setImageBitmap(aq.this.e);
                    } else if (motionEvent.getAction() == 1) {
                        aq.this.g.setImageBitmap(aq.this.a);
                        try {
                            aq.this.i.b(j.b());
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return false;
            }
        });
        this.h.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (aq.this.i.z() > aq.this.i.n()) {
                    if (motionEvent.getAction() == 0) {
                        aq.this.h.setImageBitmap(aq.this.f);
                    } else if (motionEvent.getAction() == 1) {
                        aq.this.h.setImageBitmap(aq.this.c);
                        try {
                            aq.this.i.b(j.c());
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return false;
            }
        });
        this.g.setPadding(0, 0, 20, -2);
        this.h.setPadding(0, 0, 20, 20);
        setOrientation(1);
        addView(this.g);
        addView(this.h);
    }

    public void a() {
        try {
            this.a.recycle();
            this.b.recycle();
            this.c.recycle();
            this.d.recycle();
            this.e.recycle();
            this.f.recycle();
            this.a = null;
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = null;
            this.f = null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(float f2) {
        if (f2 < this.i.m() && f2 > this.i.n()) {
            this.g.setImageBitmap(this.a);
            this.h.setImageBitmap(this.c);
        } else if (f2 == this.i.n()) {
            this.h.setImageBitmap(this.d);
            this.g.setImageBitmap(this.a);
        } else if (f2 == this.i.m()) {
            this.g.setImageBitmap(this.b);
            this.h.setImageBitmap(this.c);
        }
    }
}
