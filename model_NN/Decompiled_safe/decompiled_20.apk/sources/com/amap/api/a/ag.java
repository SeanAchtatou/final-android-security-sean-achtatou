package com.amap.api.a;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;
import com.amap.api.a.b.g;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.autonavi.amap.mapcore.FPoint;
import com.autonavi.amap.mapcore.IPoint;
import com.autonavi.amap.mapcore.MapProjection;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

class ag implements s {
    private static int a = 0;
    private FloatBuffer b = null;
    private String c;
    private int d;
    private LatLng e;
    private String f;
    private String g;
    private BitmapDescriptor h;
    private float i = 0.5f;
    private float j = 1.0f;
    private boolean k = false;
    private boolean l = true;
    private ae m;
    private FloatBuffer n;
    private Object o;
    private boolean p = false;

    public ag(MarkerOptions markerOptions, ae aeVar) {
        this.m = aeVar;
        if (markerOptions.getPosition() != null) {
            this.e = markerOptions.getPosition();
        }
        this.i = markerOptions.getAnchorU();
        this.j = markerOptions.getAnchorV();
        this.h = markerOptions.getIcon();
        this.l = markerOptions.isVisible();
        this.g = markerOptions.getSnippet();
        this.f = markerOptions.getTitle();
        this.k = markerOptions.isDraggable();
        this.c = d();
    }

    private void a(GL10 gl10, int i2, FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        if (floatBuffer != null && floatBuffer2 != null) {
            gl10.glEnable(3042);
            gl10.glBlendFunc(770, 771);
            gl10.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            gl10.glEnable(3553);
            gl10.glEnableClientState(32884);
            gl10.glEnableClientState(32888);
            gl10.glBindTexture(3553, i2);
            gl10.glVertexPointer(3, 5126, 0, floatBuffer);
            gl10.glTexCoordPointer(2, 5126, 0, floatBuffer2);
            gl10.glDrawArrays(6, 0, 4);
            gl10.glDisableClientState(32884);
            gl10.glDisableClientState(32888);
            gl10.glDisable(3553);
            gl10.glDisable(3042);
        }
    }

    private static String c(String str) {
        a++;
        return str + a;
    }

    private void w() {
        if (this.m.a != null) {
            this.m.a.e(false);
        }
    }

    public void a(float f2, float f3) {
        this.i = f2;
        this.j = f3;
        if (m()) {
            this.m.e(this);
            this.m.d(this);
        }
        w();
    }

    public void a(BitmapDescriptor bitmapDescriptor) {
        this.h = bitmapDescriptor;
        this.p = false;
        this.n.clear();
        this.n = null;
        if (m()) {
            this.m.e(this);
            this.m.d(this);
        }
        w();
    }

    public void a(LatLng latLng) {
        this.e = latLng;
        w();
    }

    public void a(Object obj) {
        this.o = obj;
    }

    public void a(String str) {
        this.f = str;
        w();
    }

    public void a(GL10 gl10) {
        g.a(gl10, this.d);
        this.d = 0;
        this.p = false;
    }

    public void a(GL10 gl10, p pVar) {
        if (this.l && c() != null && i() != null) {
            if (!this.p || this.d == 0) {
                if (this.d != 0) {
                    a(gl10);
                }
                this.d = g.a(gl10, i().getBitmap());
                this.p = true;
                v();
            } else if (v()) {
                a(gl10, this.d, this.b, this.n);
            }
        }
    }

    public void a(boolean z) {
        this.k = z;
        w();
    }

    public boolean a() {
        w();
        return this.m.b(this);
    }

    public boolean a(s sVar) {
        return equals(sVar) || sVar.d().equals(d());
    }

    public Rect b() {
        IPoint s = s();
        return s == null ? new Rect(0, 0, 0, 0) : new Rect(s.x, s.y, s.x + this.h.getWidth(), s.y + this.h.getHeight());
    }

    public void b(String str) {
        this.g = str;
        w();
    }

    public void b(boolean z) {
        this.l = z;
        w();
    }

    public LatLng c() {
        return this.e;
    }

    public String d() {
        if (this.c == null) {
            this.c = c("Marker");
        }
        return this.c;
    }

    public int e() {
        return this.d;
    }

    public FPoint f() {
        FPoint fPoint = new FPoint();
        if (this.h != null) {
            fPoint.x = ((float) this.h.getWidth()) * this.i;
            fPoint.y = ((float) this.h.getHeight()) * this.j;
        }
        return fPoint;
    }

    public String g() {
        return this.f;
    }

    public String h() {
        return this.g;
    }

    public BitmapDescriptor i() {
        if (this.h == null) {
            this.h = BitmapDescriptorFactory.defaultMarker();
        }
        return this.h;
    }

    public boolean j() {
        return this.k;
    }

    public void k() {
        if (n()) {
            this.m.d(this);
            w();
        }
    }

    public void l() {
        if (m()) {
            this.m.e(this);
            w();
        }
    }

    public boolean m() {
        return this.m.f(this);
    }

    public boolean n() {
        return this.l;
    }

    public void o() {
        Bitmap bitmap;
        try {
            a();
            if (!(this.h == null || (bitmap = this.h.getBitmap()) == null)) {
                bitmap.recycle();
                this.h = null;
            }
            if (this.n != null) {
                this.n.clear();
                this.n = null;
            }
            if (this.b != null) {
                this.b.clear();
                this.b = null;
            }
            this.e = null;
            this.o = null;
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.d("destroy erro", "MarkerDelegateImp destroy");
        }
    }

    public int p() {
        return super.hashCode();
    }

    public Object q() {
        return this.o;
    }

    public IPoint r() {
        if (c() == null) {
            return null;
        }
        IPoint iPoint = new IPoint();
        this.m.a().b(c().latitude, c().longitude, iPoint);
        return iPoint;
    }

    public IPoint s() {
        IPoint r = r();
        if (r == null) {
            return null;
        }
        FPoint f2 = f();
        r.x = (int) (((float) r.x) - f2.x);
        r.y = (int) (((float) r.y) - f2.y);
        return r;
    }

    public float t() {
        return this.i;
    }

    public float u() {
        return this.j;
    }

    public boolean v() {
        if (this.m == null) {
            return false;
        }
        p a2 = this.m.a();
        LatLng c2 = c();
        if (c2 == null) {
            return false;
        }
        MapProjection c3 = a2.c();
        IPoint iPoint = new IPoint();
        a2.b(c2.latitude, c2.longitude, iPoint);
        int width = i().getWidth();
        int height = i().getHeight();
        int t = (int) (((float) iPoint.x) - (((float) width) * t()));
        int u = (int) (((float) iPoint.y) + (((float) height) * (1.0f - u())));
        if (t - width > a2.i() || t < (-width) * 2 || u < (-height) * 2 || u - height > a2.j()) {
            return false;
        }
        int width2 = i().getWidth();
        int height2 = i().getHeight();
        int height3 = i().getBitmap().getHeight();
        float width3 = ((float) width2) / ((float) i().getBitmap().getWidth());
        float f2 = ((float) height2) / ((float) height3);
        if (this.n == null) {
            this.n = g.a(new float[]{0.0f, f2, width3, f2, width3, 0.0f, 0.0f, 0.0f});
        }
        FPoint fPoint = new FPoint();
        c3.win2Map(t, u, fPoint);
        FPoint fPoint2 = new FPoint();
        c3.win2Map(t + width, u, fPoint2);
        FPoint fPoint3 = new FPoint();
        c3.win2Map(width + t, u - height, fPoint3);
        FPoint fPoint4 = new FPoint();
        c3.win2Map(t, u - height, fPoint4);
        float[] fArr = {fPoint.x, fPoint.y, 0.0f, fPoint2.x, fPoint2.y, 0.0f, fPoint3.x, fPoint3.y, 0.0f, fPoint4.x, fPoint4.y, 0.0f};
        if (this.b == null) {
            this.b = g.a(fArr);
        } else {
            this.b = g.a(fArr, this.b);
        }
        return true;
    }
}
