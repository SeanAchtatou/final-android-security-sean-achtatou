package com.amap.api.a;

import android.graphics.Color;
import android.os.RemoteException;
import android.util.Log;
import com.amap.api.a.b.g;
import com.amap.api.maps.AMapNativeRenderer;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.autonavi.amap.mapcore.DPoint;
import com.autonavi.amap.mapcore.FPoint;
import com.autonavi.amap.mapcore.IPoint;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.microedition.khronos.opengles.GL10;

class ak implements v {
    float a;
    float b;
    float c;
    float d;
    float[] e;
    float[] f = new float[480];
    float[] g = new float[60];
    private p h;
    private float i = 10.0f;
    private int j = -16777216;
    private float k = BitmapDescriptorFactory.HUE_RED;
    private boolean l = true;
    private String m;
    private List<IPoint> n = new ArrayList();
    private FloatBuffer o;
    private int p = 0;
    private boolean q = true;
    private LatLngBounds r = null;

    public ak(p pVar) {
        this.h = pVar;
        try {
            this.m = c();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    private List<LatLng> k() {
        if (this.n == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (IPoint next : this.n) {
            if (next != null) {
                DPoint dPoint = new DPoint();
                this.h.b(next.x, next.y, dPoint);
                arrayList.add(new LatLng(dPoint.y, dPoint.x));
            }
        }
        return arrayList;
    }

    public void a(float f2) {
        this.k = f2;
        this.h.e(false);
    }

    public void a(int i2) {
        this.j = i2;
        this.a = ((float) Color.alpha(i2)) / 255.0f;
        this.b = ((float) Color.red(i2)) / 255.0f;
        this.c = ((float) Color.green(i2)) / 255.0f;
        this.d = ((float) Color.blue(i2)) / 255.0f;
        this.h.e(false);
    }

    public void a(List<LatLng> list) {
        b(list);
    }

    public void a(GL10 gl10) {
        if (this.n != null && this.n.size() != 0 && this.i > BitmapDescriptorFactory.HUE_RED) {
            if (this.p == 0) {
                g();
            }
            if (this.e != null && this.p > 0) {
                if (this.q) {
                    float mapLenWithWin = this.h.c().getMapLenWithWin((int) h());
                    AMapNativeRenderer.nativeDrawLineByTextureID(this.e, this.e.length, mapLenWithWin, this.h.b(), this.b, this.c, this.d, this.a, mapLenWithWin / 2.0f);
                    return;
                }
                if (this.o == null && this.e != null) {
                    this.o = g.a(this.e);
                }
                n.a(gl10, 3, i(), this.o, h(), this.p);
            }
        }
    }

    public void a(boolean z) {
        this.l = z;
        this.h.e(false);
    }

    public boolean a() {
        if (this.r == null) {
            return false;
        }
        LatLngBounds B = this.h.B();
        if (B == null) {
            return true;
        }
        return B.contains(this.r) || this.r.intersects(B);
    }

    public boolean a(t tVar) {
        return equals(tVar) || tVar.c().equals(c());
    }

    public void b() {
        this.h.a(c());
        this.h.e(false);
    }

    public void b(float f2) {
        this.i = f2;
        this.h.e(false);
    }

    /* access modifiers changed from: package-private */
    public void b(List<LatLng> list) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        this.n.clear();
        if (list != null) {
            for (LatLng next : list) {
                IPoint iPoint = new IPoint();
                this.h.a(next.latitude, next.longitude, iPoint);
                this.n.add(iPoint);
                builder.include(next);
            }
        }
        this.r = builder.build();
        this.p = 0;
        this.e = new float[(this.n.size() * 3)];
        this.h.e(false);
    }

    public void b(boolean z) {
        this.q = z;
        this.h.e(false);
    }

    public String c() {
        if (this.m == null) {
            this.m = o.a("Polyline");
        }
        return this.m;
    }

    public float d() {
        return this.k;
    }

    public boolean e() {
        return this.l;
    }

    public int f() {
        return super.hashCode();
    }

    public void g() {
        int i2 = 0;
        FPoint fPoint = new FPoint();
        Iterator<IPoint> it = this.n.iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                break;
            }
            IPoint next = it.next();
            this.h.a(next.y, next.x, fPoint);
            this.e[i3 * 3] = fPoint.x;
            this.e[(i3 * 3) + 1] = fPoint.y;
            this.e[(i3 * 3) + 2] = 0.0f;
            i2 = i3 + 1;
        }
        if (!this.q) {
            this.o = g.a(this.e);
        }
        this.p = this.n.size();
    }

    public float h() {
        return this.i;
    }

    public int i() {
        return this.j;
    }

    public List<LatLng> j() {
        return k();
    }

    public void n() {
        try {
            if (this.e != null) {
                this.e = null;
            }
            if (this.o != null) {
                this.o.clear();
                this.o = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.d("destroy erro", "PolylineDelegateImp destroy");
        }
    }
}
