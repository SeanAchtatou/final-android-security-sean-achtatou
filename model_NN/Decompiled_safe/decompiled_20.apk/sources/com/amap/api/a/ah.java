package com.amap.api.a;

import android.graphics.Color;
import android.os.RemoteException;
import android.support.v4.view.MotionEventCompat;
import com.amap.api.a.x;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CircleOptions;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;

class ah {
    private p a;
    private Marker b;
    private q c;

    ah(p pVar) {
        this.a = pVar;
        try {
            this.b = this.a.a(new MarkerOptions().anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromAsset(x.a.marker_gps_no_sharing.name() + ".png")).position(new LatLng(0.0d, 0.0d)));
            this.c = this.a.a(new CircleOptions().strokeWidth(1.0f).fillColor(Color.argb(100, 0, 0, 180)).strokeColor(Color.argb((int) MotionEventCompat.ACTION_MASK, 0, 0, 220)).center(new LatLng(0.0d, 0.0d)));
            this.c.a(200.0d);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void a() {
        if (this.c != null) {
            this.a.a(this.c.c());
        }
        if (this.b != null) {
            this.a.b(this.b.getId());
        }
    }

    public void a(LatLng latLng, double d) {
        this.b.setPosition(latLng);
        try {
            this.c.a(latLng);
            if (d != -1.0d) {
                this.c.a(d);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void a(MyLocationStyle myLocationStyle) {
        if (this.b != null && this.c != null) {
            try {
                this.b.remove();
                this.c.b();
                this.b = this.a.a(new MarkerOptions().anchor(myLocationStyle.getAnchorU(), myLocationStyle.getAnchorV()).icon(myLocationStyle.getMyLocationIcon()).position(new LatLng(0.0d, 0.0d)));
                this.c = this.a.a(new CircleOptions().strokeWidth(myLocationStyle.getStrokeWidth()).fillColor(myLocationStyle.getRadiusFillColor()).strokeColor(myLocationStyle.getStrokeColor()).center(new LatLng(0.0d, 0.0d)));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
