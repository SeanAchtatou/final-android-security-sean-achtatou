package com.amap.api.a;

import android.content.Context;
import android.view.View;
import com.amap.api.a.b.g;
import java.util.Comparator;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.microedition.khronos.opengles.GL10;

public class an extends View {
    CopyOnWriteArrayList<y> a = new CopyOnWriteArrayList<>();
    a b = new a();
    CopyOnWriteArrayList<Integer> c = new CopyOnWriteArrayList<>();
    private p d;

    class a implements Comparator<Object> {
        a() {
        }

        public int compare(Object obj, Object obj2) {
            t tVar = (t) obj;
            t tVar2 = (t) obj2;
            if (!(tVar == null || tVar2 == null)) {
                try {
                    if (tVar.d() > tVar2.d()) {
                        return 1;
                    }
                    if (tVar.d() < tVar2.d()) {
                        return -1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return 0;
        }
    }

    public an(Context context, p pVar) {
        super(context);
        this.d = pVar;
    }

    public void a() {
        Iterator<y> it = this.a.iterator();
        while (it.hasNext()) {
            y next = it.next();
            if (next != null) {
                next.a();
            }
        }
        this.a.clear();
    }

    public void a(GL10 gl10) {
        Iterator<Integer> it = this.c.iterator();
        while (it.hasNext()) {
            g.a(gl10, it.next().intValue());
        }
        this.c.clear();
        Iterator<y> it2 = this.a.iterator();
        while (it2.hasNext()) {
            y next = it2.next();
            if (next.e()) {
                next.a(gl10);
            }
        }
    }

    public void a(boolean z) {
        Iterator<y> it = this.a.iterator();
        while (it.hasNext()) {
            y next = it.next();
            if (next != null) {
                next.b(z);
            }
        }
    }

    public void b() {
        Iterator<y> it = this.a.iterator();
        while (it.hasNext()) {
            y next = it.next();
            if (next != null && next.e()) {
                next.g();
            }
        }
    }

    public void c() {
        Iterator<y> it = this.a.iterator();
        while (it.hasNext()) {
            y next = it.next();
            if (next != null) {
                next.h();
            }
        }
    }

    public void d() {
        Iterator<y> it = this.a.iterator();
        while (it.hasNext()) {
            y next = it.next();
            if (next != null) {
                next.i();
            }
        }
    }
}
