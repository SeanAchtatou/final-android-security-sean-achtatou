package com.amap.api.a;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.amap.api.a.b.e;
import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.model.CameraPosition;

public class ab implements r {
    public static Context a;
    private p b;
    private AMapOptions c;

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (this.b == null) {
            if (a == null && layoutInflater != null) {
                a = (Activity) layoutInflater.getContext();
            }
            if (a == null) {
                throw new NullPointerException("Context 为 null 请在地图调用之前 使用 MapsInitializer.initialize(Context paramContext) 来设置Context");
            }
            int i = a.getResources().getDisplayMetrics().densityDpi;
            if (i <= 120) {
                m.a = 0.5f;
            } else if (i <= 160) {
                m.a = 0.8f;
            } else if (i <= 240) {
                m.a = 0.87f;
            } else if (i <= 320) {
                m.a = 1.0f;
            } else if (i <= 480) {
                m.a = 1.5f;
            } else {
                m.a = 0.9f;
            }
            this.b = new b(a);
        }
        if (this.c == null && bundle != null) {
            this.c = (AMapOptions) bundle.getParcelable("MapOptions");
        }
        b(this.c);
        e.a("MapFragmentDelegateImp", "onCreateView", 113);
        return this.b.x();
    }

    public p a() {
        return this.b;
    }

    public void a(Activity activity) {
        a = activity;
    }

    public void a(Activity activity, AMapOptions aMapOptions, Bundle bundle) {
        a = activity;
        this.c = aMapOptions;
    }

    public void a(Bundle bundle) {
        e.a("MapFragmentDelegateImp", "onCreate", 113);
    }

    public void a(AMapOptions aMapOptions) {
        this.c = aMapOptions;
    }

    public void b() {
        if (this.b != null) {
            this.b.onResume();
        }
    }

    public void b(Bundle bundle) {
        if (this.b != null) {
            if (this.c == null) {
                this.c = new AMapOptions();
            }
            this.c = this.c.camera(a().l());
            bundle.putParcelable("MapOptions", this.c);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(AMapOptions aMapOptions) {
        if (aMapOptions != null && this.b != null) {
            CameraPosition camera = aMapOptions.getCamera();
            if (camera != null) {
                this.b.a(j.a(camera.target, camera.zoom, camera.bearing, camera.tilt));
            }
            z u = this.b.u();
            u.h(aMapOptions.getRotateGesturesEnabled().booleanValue());
            u.e(aMapOptions.getScrollGesturesEnabled().booleanValue());
            u.g(aMapOptions.getTiltGesturesEnabled().booleanValue());
            u.b(aMapOptions.getZoomControlsEnabled().booleanValue());
            u.f(aMapOptions.getZoomGesturesEnabled().booleanValue());
            u.c(aMapOptions.getCompassEnabled().booleanValue());
            u.a(aMapOptions.getScaleControlsEnabled().booleanValue());
            this.b.setZOrderOnTop(aMapOptions.getZOrderOnTop().booleanValue());
        }
    }

    public void c() {
        if (this.b != null) {
            this.b.onPause();
        }
    }

    public void d() {
    }

    public void e() {
        if (a() != null) {
            a().e();
        }
        a = null;
        this.b = null;
        this.c = null;
        try {
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void f() {
    }
}
