package com.amap.api.a;

import android.os.RemoteException;
import android.util.Log;
import com.amap.api.a.b.f;
import com.amap.api.a.b.g;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.search.poisearch.PoiSearch;
import com.autonavi.amap.mapcore.DPoint;
import com.autonavi.amap.mapcore.FPoint;
import com.autonavi.amap.mapcore.IPoint;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import javax.microedition.khronos.opengles.GL10;

class aj implements u {
    private static float n = 1.0E10f;
    private p a;
    private float b = BitmapDescriptorFactory.HUE_RED;
    private boolean c = true;
    private String d;
    private float e;
    private int f;
    private int g;
    private List<IPoint> h = new ArrayList();
    private FloatBuffer i;
    private FloatBuffer j;
    private int k = 0;
    private int l = 0;
    private LatLngBounds m = null;

    public aj(p pVar) {
        this.a = pVar;
        try {
            this.d = c();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    static FPoint[] a(FPoint[] fPointArr) {
        int length = fPointArr.length;
        FPoint[] fPointArr2 = new FPoint[length];
        for (int i2 = 0; i2 < length; i2++) {
            fPointArr2[i2] = new FPoint(fPointArr[i2].x * n, fPointArr[i2].y * n);
        }
        ArrayList arrayList = new ArrayList();
        f.a(fPointArr2, arrayList);
        int size = arrayList.size();
        FPoint[] fPointArr3 = new FPoint[size];
        for (int i3 = 0; i3 < size; i3++) {
            fPointArr3[i3] = new FPoint();
            fPointArr3[i3].x = ((FPoint) arrayList.get(i3)).x / n;
            fPointArr3[i3].y = ((FPoint) arrayList.get(i3)).y / n;
        }
        return fPointArr3;
    }

    public void a(float f2) {
        this.b = f2;
        this.a.e(false);
    }

    public void a(int i2) {
        this.f = i2;
        this.a.e(false);
    }

    public void a(List<LatLng> list) {
        b(list);
        this.a.e(false);
    }

    public void a(GL10 gl10) {
        if (this.h != null && this.h.size() != 0) {
            if (this.i == null || this.j == null || this.k == 0 || this.l == 0) {
                g();
            }
            if (this.i != null && this.j != null && this.k > 0 && this.l > 0) {
                n.a(gl10, i(), k(), this.i, h(), this.j, this.k, this.l);
            }
        }
    }

    public void a(boolean z) {
        this.c = z;
        this.a.e(false);
    }

    public boolean a() {
        if (this.m == null) {
            return false;
        }
        LatLngBounds B = this.a.B();
        if (B == null) {
            return true;
        }
        return this.m.contains(B) || this.m.intersects(B);
    }

    public boolean a(t tVar) {
        return equals(tVar) || tVar.c().equals(c());
    }

    public void b() {
        this.a.a(c());
        this.a.e(false);
    }

    public void b(float f2) {
        this.e = f2;
        this.a.e(false);
    }

    public void b(int i2) {
        this.g = i2;
        this.a.e(false);
    }

    /* access modifiers changed from: package-private */
    public void b(List<LatLng> list) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        this.h.clear();
        if (list != null) {
            LatLng latLng = null;
            for (LatLng next : list) {
                if (!next.equals(latLng)) {
                    IPoint iPoint = new IPoint();
                    this.a.a(next.latitude, next.longitude, iPoint);
                    this.h.add(iPoint);
                    builder.include(next);
                    latLng = next;
                }
            }
            int size = this.h.size();
            if (size > 1) {
                IPoint iPoint2 = this.h.get(0);
                IPoint iPoint3 = this.h.get(size - 1);
                if (iPoint2.x == iPoint3.x && iPoint2.y == iPoint3.y) {
                    this.h.remove(size - 1);
                }
            }
        }
        this.m = builder.build();
        if (this.i != null) {
            this.i.clear();
        }
        if (this.j != null) {
            this.j.clear();
        }
        this.k = 0;
        this.l = 0;
        this.a.e(false);
    }

    public String c() {
        if (this.d == null) {
            this.d = o.a(PoiSearch.SearchBound.POLYGON_SHAPE);
        }
        return this.d;
    }

    public float d() {
        return this.b;
    }

    public boolean e() {
        return this.c;
    }

    public int f() {
        return super.hashCode();
    }

    public void g() {
        FPoint[] fPointArr = new FPoint[this.h.size()];
        float[] fArr = new float[(this.h.size() * 3)];
        int i2 = 0;
        for (IPoint next : this.h) {
            fPointArr[i2] = new FPoint();
            this.a.a(next.y, next.x, fPointArr[i2]);
            fArr[i2 * 3] = fPointArr[i2].x;
            fArr[(i2 * 3) + 1] = fPointArr[i2].y;
            fArr[(i2 * 3) + 2] = 0.0f;
            i2++;
        }
        FPoint[] a2 = a(fPointArr);
        if (a2.length == 0) {
            if (n == 1.0E10f) {
                n = 1.0E8f;
            } else {
                n = 1.0E10f;
            }
            a2 = a(fPointArr);
        }
        float[] fArr2 = new float[(a2.length * 3)];
        int i3 = 0;
        for (FPoint fPoint : a2) {
            fArr2[i3 * 3] = fPoint.x;
            fArr2[(i3 * 3) + 1] = fPoint.y;
            fArr2[(i3 * 3) + 2] = 0.0f;
            i3++;
        }
        this.k = fPointArr.length;
        this.l = a2.length;
        this.i = g.a(fArr);
        this.j = g.a(fArr2);
    }

    public float h() {
        return this.e;
    }

    public int i() {
        return this.f;
    }

    public List<LatLng> j() {
        return l();
    }

    public int k() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public List<LatLng> l() {
        if (this.h == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (IPoint next : this.h) {
            if (next != null) {
                DPoint dPoint = new DPoint();
                this.a.b(next.x, next.y, dPoint);
                arrayList.add(new LatLng(dPoint.y, dPoint.x));
            }
        }
        return arrayList;
    }

    public void n() {
        try {
            if (this.i != null) {
                this.i.clear();
                this.i = null;
            }
            if (this.j != null) {
                this.j = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.d("destroy erro", "PolygonDelegateImp destroy");
        }
    }
}
