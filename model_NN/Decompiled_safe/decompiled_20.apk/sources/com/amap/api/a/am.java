package com.amap.api.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;
import com.amap.api.search.poisearch.PoiTypeDef;

class am extends View {
    private String a = PoiTypeDef.All;
    private int b = 0;
    private b c;
    private Paint d;
    private Paint e;
    private Rect f;

    public am(Context context, b bVar) {
        super(context);
        this.c = bVar;
        this.d = new Paint();
        this.f = new Rect();
        this.d.setAntiAlias(true);
        this.d.setColor(-16777216);
        this.d.setStrokeWidth(2.0f);
        this.d.setStyle(Paint.Style.STROKE);
        this.e = new Paint();
        this.e.setAntiAlias(true);
        this.e.setColor(-16777216);
        this.e.setTextSize(20.0f);
    }

    public void a() {
        this.d = null;
        this.e = null;
        this.f = null;
        this.a = null;
    }

    public void a(int i) {
        this.b = i;
    }

    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (!this.a.equals(PoiTypeDef.All) && this.b != 0) {
            Point C = this.c.C();
            this.e.getTextBounds(this.a, 0, this.a.length(), this.f);
            int width = C.x + this.b > this.c.getWidth() + -10 ? (this.c.getWidth() - 10) - ((this.b + this.f.width()) / 2) : C.x + ((this.b - this.f.width()) / 2);
            int height = (C.y - this.f.height()) - 10;
            canvas.drawText(this.a, (float) width, (float) height, this.e);
            int width2 = width - ((this.b - this.f.width()) / 2);
            int height2 = height + this.f.height();
            canvas.drawLine((float) width2, (float) (height2 - 2), (float) width2, (float) (height2 + 2), this.d);
            canvas.drawLine((float) width2, (float) height2, (float) (this.b + width2), (float) height2, this.d);
            canvas.drawLine((float) (this.b + width2), (float) (height2 - 2), (float) (this.b + width2), (float) (height2 + 2), this.d);
        }
    }
}
