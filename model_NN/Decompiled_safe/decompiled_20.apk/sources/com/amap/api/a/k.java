package com.amap.api.a;

import android.os.RemoteException;
import android.util.Log;
import com.amap.api.a.b.g;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.autonavi.amap.mapcore.FPoint;
import com.autonavi.amap.mapcore.IPoint;
import com.autonavi.amap.mapcore.MapProjection;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

class k implements q {
    private static float n = 4.0075016E7f;
    private static int o = 256;
    private static int p = 20;
    private LatLng a = null;
    private double b = 0.0d;
    private float c = 10.0f;
    private int d = -16777216;
    private int e = 0;
    private float f = BitmapDescriptorFactory.HUE_RED;
    private boolean g = true;
    private String h;
    private p i;
    private FloatBuffer j;
    private FloatBuffer k;
    private int l = 0;
    private int m = 0;

    public k(p pVar) {
        this.i = pVar;
        try {
            this.h = c();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    private float b(double d2) {
        return (float) ((Math.cos((3.141592653589793d * d2) / 180.0d) * ((double) n)) / ((double) (o << p)));
    }

    private double c(double d2) {
        return 1.0d / ((double) b(d2));
    }

    public void a(double d2) {
        this.b = d2;
        h();
    }

    public void a(float f2) {
        this.f = f2;
        this.i.e(false);
    }

    public void a(int i2) {
        this.d = i2;
    }

    public void a(LatLng latLng) {
        this.a = latLng;
        h();
    }

    public void a(GL10 gl10) {
        if (i() != null && this.b > 0.0d && e()) {
            if (this.j == null || this.k == null || this.l == 0 || this.m == 0) {
                g();
            }
            if (this.j != null && this.k != null && this.l > 0 && this.m > 0) {
                n.b(gl10, m(), l(), this.j, k(), this.k, this.l, this.m);
            }
        }
    }

    public void a(boolean z) {
        this.g = z;
        this.i.e(false);
    }

    public boolean a() {
        return true;
    }

    public boolean a(t tVar) {
        return equals(tVar) || tVar.c().equals(c());
    }

    public void b() {
        this.i.a(c());
        this.i.e(false);
    }

    public void b(float f2) {
        this.c = f2;
        this.i.e(false);
    }

    public void b(int i2) {
        this.e = i2;
        this.i.e(false);
    }

    public String c() {
        if (this.h == null) {
            this.h = o.a("Circle");
        }
        return this.h;
    }

    public float d() {
        return this.f;
    }

    public boolean e() {
        return this.g;
    }

    public int f() {
        return 0;
    }

    public void g() {
        LatLng i2 = i();
        if (i2 != null) {
            FPoint[] fPointArr = new FPoint[360];
            float[] fArr = new float[(fPointArr.length * 3)];
            double c2 = c(i().latitude) * j();
            IPoint iPoint = new IPoint();
            MapProjection c3 = this.i.c();
            MapProjection.lonlat2Geo(i2.longitude, i2.latitude, iPoint);
            for (int i3 = 0; i3 < 360; i3++) {
                double d2 = (((double) i3) * 3.141592653589793d) / 180.0d;
                FPoint fPoint = new FPoint();
                c3.geo2Map((int) ((Math.sin(d2) * c2) + ((double) iPoint.x)), (int) ((Math.cos(d2) * c2) + ((double) iPoint.y)), fPoint);
                fPointArr[i3] = fPoint;
                fArr[i3 * 3] = fPointArr[i3].x;
                fArr[(i3 * 3) + 1] = fPointArr[i3].y;
                fArr[(i3 * 3) + 2] = 0.0f;
            }
            FPoint[] a2 = aj.a(fPointArr);
            int i4 = 0;
            float[] fArr2 = new float[(a2.length * 3)];
            for (FPoint fPoint2 : a2) {
                fArr2[i4 * 3] = fPoint2.x;
                fArr2[(i4 * 3) + 1] = fPoint2.y;
                fArr2[(i4 * 3) + 2] = 0.0f;
                i4++;
            }
            this.l = fPointArr.length;
            this.m = a2.length;
            this.j = g.a(fArr);
            this.k = g.a(fArr2);
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.l = 0;
        this.m = 0;
        if (this.j != null) {
            this.j.clear();
        }
        if (this.k != null) {
            this.k.clear();
        }
        this.i.e(false);
    }

    public LatLng i() {
        return this.a;
    }

    public double j() {
        return this.b;
    }

    public float k() {
        return this.c;
    }

    public int l() {
        return this.d;
    }

    public int m() {
        return this.e;
    }

    public void n() {
        try {
            this.a = null;
            if (this.j != null) {
                this.j.clear();
                this.j = null;
            }
            if (this.k != null) {
                this.k.clear();
                this.j = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.d("destroy erro", "CircleDelegateImp destroy");
        }
    }
}
