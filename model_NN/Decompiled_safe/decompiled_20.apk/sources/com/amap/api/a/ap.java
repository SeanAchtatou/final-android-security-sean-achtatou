package com.amap.api.a;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;
import com.amap.api.a.b.g;
import java.io.IOException;
import java.io.InputStream;

class ap extends View {
    Rect a = new Rect();
    int b = 10;
    private Bitmap c;
    private Bitmap d;
    private Paint e = new Paint();
    private boolean f = false;
    private int g = 0;
    private b h;
    private int i = 0;

    public ap(Context context, b bVar) {
        super(context);
        this.h = bVar;
        AssetManager assets = context.getResources().getAssets();
        try {
            InputStream open = assets.open("ap.data");
            this.c = BitmapFactory.decodeStream(open);
            this.c = g.a(this.c, m.a);
            open.close();
            InputStream open2 = assets.open("ap1.data");
            this.d = BitmapFactory.decodeStream(open2);
            this.d = g.a(this.d, m.a);
            open2.close();
            this.g = this.d.getHeight();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.e.setAntiAlias(true);
        this.e.setColor(-16777216);
        this.e.setStyle(Paint.Style.STROKE);
    }

    public void a() {
        try {
            this.c.recycle();
            this.d.recycle();
            this.c = null;
            this.d = null;
            this.e = null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void a(boolean z) {
        this.f = z;
        invalidate();
    }

    public Bitmap b() {
        return this.f ? this.d : this.c;
    }

    public Point c() {
        return new Point(this.b, (getHeight() - this.g) - 10);
    }

    public void onDraw(Canvas canvas) {
        this.e.getTextBounds("V2.0.4", 0, 6, this.a);
        int width = this.d.getWidth() + 3 + this.a.width();
        if (this.i == 1) {
            this.b = (this.h.getWidth() - width) / 2;
        } else if (this.i == 2) {
            this.b = (this.h.getWidth() - width) - 10;
        } else {
            this.b = 10;
        }
        canvas.drawBitmap(b(), (float) this.b, (float) ((getHeight() - this.g) - 10), this.e);
        canvas.drawText("V2.0.4", (float) (this.d.getWidth() + this.b + 3), (float) (getHeight() - 10), this.e);
    }
}
