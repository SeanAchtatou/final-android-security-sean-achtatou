package com.amap.api.a;

import android.content.Context;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import org.apache.commons.httpclient.HttpStatus;

public class i {
    private static float J = ((float) (Math.log(0.78d) / Math.log(0.9d)));
    private static final float[] K = new float[HttpStatus.SC_SWITCHING_PROTOCOLS];
    private static final float[] L = new float[HttpStatus.SC_SWITCHING_PROTOCOLS];
    private static float P = 8.0f;
    private static float Q;
    private float A;
    private float B;
    private boolean C;
    private Interpolator D;
    private boolean E;
    private float F;
    private float G;
    private int H;
    private float I;
    private float M;
    private final float N;
    private float O;
    private int a;
    private int b;
    private int c;
    private float d;
    private float e;
    private float f;
    private int g;
    private int h;
    private float i;
    private float j;
    private float k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private float r;
    private float s;
    private float t;
    private long u;
    private long v;
    private float w;
    private float x;
    private float y;
    private float z;

    static {
        float f2;
        float f3;
        float f4;
        float f5;
        float f6 = BitmapDescriptorFactory.HUE_RED;
        int i2 = 0;
        float f7 = 0.0f;
        while (i2 < 100) {
            float f8 = ((float) i2) / 100.0f;
            float f9 = 1.0f;
            float f10 = f7;
            while (true) {
                f2 = ((f9 - f10) / 2.0f) + f10;
                f3 = 3.0f * f2 * (1.0f - f2);
                float f11 = ((((1.0f - f2) * 0.175f) + (0.35000002f * f2)) * f3) + (f2 * f2 * f2);
                if (((double) Math.abs(f11 - f8)) < 1.0E-5d) {
                    break;
                } else if (f11 > f8) {
                    f9 = f2;
                } else {
                    f10 = f2;
                }
            }
            K[i2] = (f2 * f2 * f2) + (f3 * (((1.0f - f2) * 0.5f) + f2));
            float f12 = 1.0f;
            while (true) {
                f4 = ((f12 - f6) / 2.0f) + f6;
                f5 = 3.0f * f4 * (1.0f - f4);
                float f13 = ((((1.0f - f4) * 0.5f) + f4) * f5) + (f4 * f4 * f4);
                if (((double) Math.abs(f13 - f8)) < 1.0E-5d) {
                    break;
                } else if (f13 > f8) {
                    f12 = f4;
                } else {
                    f6 = f4;
                }
            }
            L[i2] = (f4 * f4 * f4) + ((((1.0f - f4) * 0.175f) + (0.35000002f * f4)) * f5);
            i2++;
            f7 = f10;
        }
        float[] fArr = K;
        L[100] = 1.0f;
        fArr[100] = 1.0f;
        Q = 1.0f;
        Q = 1.0f / a(1.0f);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public i(Context context, Interpolator interpolator) {
        this(context, interpolator, context.getApplicationInfo().targetSdkVersion >= 11);
    }

    public i(Context context, Interpolator interpolator, boolean z2) {
        this.I = ViewConfiguration.getScrollFriction();
        this.C = true;
        this.D = interpolator;
        this.N = context.getResources().getDisplayMetrics().density * 160.0f;
        this.M = b(ViewConfiguration.getScrollFriction());
        this.E = z2;
        this.O = b(0.84f);
    }

    static float a(float f2) {
        float f3 = P * f2;
        return (f3 < 1.0f ? f3 - (1.0f - ((float) Math.exp((double) (-f3)))) : ((1.0f - ((float) Math.exp((double) (1.0f - f3)))) * 0.63212055f) + 0.36787945f) * Q;
    }

    private float b(float f2) {
        return 386.0878f * this.N * f2;
    }

    private double c(float f2) {
        return Math.log((double) ((0.35f * Math.abs(f2)) / (this.I * this.O)));
    }

    private int d(float f2) {
        return (int) (Math.exp(c(f2) / (((double) J) - 1.0d)) * 1000.0d);
    }

    private double e(float f2) {
        return Math.exp(c(f2) * (((double) J) / (((double) J) - 1.0d))) * ((double) (this.I * this.O));
    }

    public void a(int i2, int i3, float f2, float f3, float f4, int i4, int i5, float f5, float f6, float f7, long j2) {
        this.a = 3;
        this.C = false;
        this.v = j2;
        this.u = AnimationUtils.currentAnimationTimeMillis();
        this.b = i2;
        this.c = i3;
        this.d = f2;
        this.e = f3;
        this.f = f4;
        this.g = i2 + i4;
        this.h = i3 + i5;
        this.i = f2 + f5;
        this.j = f3 + f6;
        this.k = f4 + f7;
        this.x = (float) i4;
        this.y = (float) i5;
        this.z = f5;
        this.A = f6;
        this.B = f7;
        this.w = 1.0f / ((float) this.v);
    }

    public void a(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        float f2 = 1.0f;
        if (this.E && !this.C) {
            float g2 = g();
            float f3 = (float) (this.g - this.b);
            float f4 = (float) (this.h - this.c);
            float sqrt = (float) Math.sqrt((double) ((f3 * f3) + (f4 * f4)));
            float f5 = (f3 / sqrt) * g2;
            float f6 = g2 * (f4 / sqrt);
            if (Math.signum((float) i4) == Math.signum(f5) && Math.signum((float) i5) == Math.signum(f6)) {
                i4 = (int) (f5 + ((float) i4));
                i5 = (int) (f6 + ((float) i5));
            }
        }
        this.a = 1;
        this.C = false;
        float sqrt2 = (float) Math.sqrt((double) ((i4 * i4) + (i5 * i5)));
        this.F = sqrt2;
        this.v = (long) d(sqrt2);
        this.u = AnimationUtils.currentAnimationTimeMillis();
        this.b = i2;
        this.c = i3;
        float f7 = sqrt2 == BitmapDescriptorFactory.HUE_RED ? 1.0f : ((float) i4) / sqrt2;
        if (sqrt2 != BitmapDescriptorFactory.HUE_RED) {
            f2 = ((float) i5) / sqrt2;
        }
        double e2 = e(sqrt2);
        this.H = (int) (((double) Math.signum(sqrt2)) * e2);
        this.l = i6;
        this.m = i7;
        this.n = i8;
        this.o = i9;
        this.g = ((int) Math.round(((double) f7) * e2)) + i2;
        this.g = Math.min(this.g, this.m);
        this.g = Math.max(this.g, this.l);
        this.h = ((int) Math.round(((double) f2) * e2)) + i3;
        this.h = Math.min(this.h, this.o);
        this.h = Math.max(this.h, this.n);
    }

    public final void a(boolean z2) {
        this.C = z2;
    }

    public final boolean a() {
        return this.C;
    }

    public final int b() {
        return this.p;
    }

    public final int c() {
        return this.q;
    }

    public final float d() {
        return this.r;
    }

    public final float e() {
        return this.s;
    }

    public final float f() {
        return this.t;
    }

    public float g() {
        return this.a == 1 ? this.G : this.F - ((this.M * ((float) i())) / 2000.0f);
    }

    public boolean h() {
        if (this.C) {
            return false;
        }
        int currentAnimationTimeMillis = (int) (AnimationUtils.currentAnimationTimeMillis() - this.u);
        if (((long) currentAnimationTimeMillis) < this.v) {
            switch (this.a) {
                case 0:
                    float f2 = ((float) currentAnimationTimeMillis) * this.w;
                    float a2 = this.D == null ? a(f2) : this.D.getInterpolation(f2);
                    this.p = this.b + Math.round(this.x * a2);
                    this.q = Math.round(a2 * this.y) + this.c;
                    break;
                case 1:
                    float f3 = ((float) currentAnimationTimeMillis) / ((float) this.v);
                    int i2 = (int) (100.0f * f3);
                    float f4 = 1.0f;
                    float f5 = BitmapDescriptorFactory.HUE_RED;
                    if (i2 < 100) {
                        float f6 = ((float) i2) / 100.0f;
                        float f7 = K[i2];
                        f5 = (K[i2 + 1] - f7) / ((((float) (i2 + 1)) / 100.0f) - f6);
                        f4 = ((f3 - f6) * f5) + f7;
                    }
                    this.G = ((f5 * ((float) this.H)) / ((float) this.v)) * 1000.0f;
                    this.p = this.b + Math.round(((float) (this.g - this.b)) * f4);
                    this.p = Math.min(this.p, this.m);
                    this.p = Math.max(this.p, this.l);
                    this.q = this.c + Math.round(f4 * ((float) (this.h - this.c)));
                    this.q = Math.min(this.q, this.o);
                    this.q = Math.max(this.q, this.n);
                    if (this.p == this.g && this.q == this.h) {
                        this.C = true;
                        break;
                    }
                case 2:
                    float f8 = ((float) currentAnimationTimeMillis) * this.w;
                    this.r = ((this.D == null ? a(f8) : this.D.getInterpolation(f8)) * this.z) + this.d;
                    break;
                case 3:
                    float f9 = ((float) currentAnimationTimeMillis) * this.w;
                    float a3 = this.D == null ? a(f9) : this.D.getInterpolation(f9);
                    this.p = this.b + Math.round(this.x * a3);
                    this.q = this.c + Math.round(this.y * a3);
                    this.r = this.d + (this.z * a3);
                    this.s = this.e + (this.A * a3);
                    this.t = (a3 * this.B) + this.f;
                    break;
            }
        } else {
            this.p = this.g;
            this.q = this.h;
            this.r = this.i;
            this.s = this.j;
            this.t = this.k;
            this.C = true;
        }
        return true;
    }

    public int i() {
        return (int) (AnimationUtils.currentAnimationTimeMillis() - this.u);
    }

    public final int j() {
        return this.a;
    }
}
