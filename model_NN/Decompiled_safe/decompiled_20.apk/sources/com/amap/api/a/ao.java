package com.amap.api.a;

class ao implements z {
    private p a;
    private boolean b = true;
    private boolean c = true;
    private boolean d = true;
    private boolean e = false;
    private boolean f = true;
    private boolean g = true;
    private boolean h = true;
    private boolean i = false;
    private int j = 0;

    ao(p pVar) {
        this.a = pVar;
    }

    public void a(int i2) {
        this.j = i2;
        this.a.b(i2);
    }

    public void a(boolean z) {
        this.i = z;
        this.a.d(this.i);
    }

    public boolean a() {
        return this.i;
    }

    public void b(boolean z) {
        this.g = z;
        this.a.a(this.g);
    }

    public boolean b() {
        return this.g;
    }

    public void c(boolean z) {
        this.h = z;
        this.a.c(this.h);
    }

    public boolean c() {
        return this.h;
    }

    public void d(boolean z) {
        this.e = z;
        this.a.b(this.e);
    }

    public boolean d() {
        return this.e;
    }

    public void e(boolean z) {
        this.c = z;
    }

    public boolean e() {
        return this.c;
    }

    public void f(boolean z) {
        this.f = z;
    }

    public boolean f() {
        return this.f;
    }

    public void g(boolean z) {
        this.d = z;
    }

    public boolean g() {
        return this.d;
    }

    public void h(boolean z) {
        this.b = z;
    }

    public boolean h() {
        return this.b;
    }

    public int i() {
        return this.j;
    }

    public void i(boolean z) {
        h(z);
        g(z);
        f(z);
        e(z);
    }
}
