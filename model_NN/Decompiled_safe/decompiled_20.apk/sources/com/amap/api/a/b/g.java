package com.amap.api.a.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.opengl.GLUtils;
import android.os.Environment;
import com.amap.api.maps.MapsInitializer;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Arrays;
import javax.microedition.khronos.opengles.GL10;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class g {
    public static double a(LatLng latLng, LatLng latLng2) {
        double d = latLng.longitude;
        double d2 = latLng.latitude;
        double d3 = d * 0.01745329251994329d;
        double d4 = d2 * 0.01745329251994329d;
        double d5 = latLng2.longitude * 0.01745329251994329d;
        double d6 = latLng2.latitude * 0.01745329251994329d;
        double sin = Math.sin(d3);
        double sin2 = Math.sin(d4);
        double cos = Math.cos(d3);
        double cos2 = Math.cos(d4);
        double sin3 = Math.sin(d5);
        double sin4 = Math.sin(d6);
        double cos3 = Math.cos(d5);
        double cos4 = Math.cos(d6);
        double[] dArr = {cos * cos2, cos2 * sin, sin2};
        double[] dArr2 = {cos4 * cos3, cos4 * sin3, sin4};
        return Math.asin(Math.sqrt((((dArr[0] - dArr2[0]) * (dArr[0] - dArr2[0])) + ((dArr[1] - dArr2[1]) * (dArr[1] - dArr2[1]))) + ((dArr[2] - dArr2[2]) * (dArr[2] - dArr2[2]))) / 2.0d) * 1.27420015798544E7d;
    }

    public static float a(float f) {
        if (f < BitmapDescriptorFactory.HUE_RED) {
            return BitmapDescriptorFactory.HUE_RED;
        }
        if (f > 45.0f) {
            return 45.0f;
        }
        return f;
    }

    public static int a(int i) {
        int log = (int) (Math.log((double) i) / Math.log(2.0d));
        return (1 << log) >= i ? 1 << log : 1 << (log + 1);
    }

    public static int a(GL10 gl10, Bitmap bitmap) {
        int[] iArr = new int[1];
        gl10.glEnable(3553);
        gl10.glGenTextures(1, iArr, 0);
        gl10.glBindTexture(3553, iArr[0]);
        gl10.glTexParameterf(3553, 10241, 9729.0f);
        gl10.glTexParameterf(3553, 10240, 9729.0f);
        gl10.glTexParameterf(3553, 10242, 33071.0f);
        gl10.glTexParameterf(3553, 10243, 33071.0f);
        GLUtils.texImage2D(3553, 0, bitmap, 0);
        gl10.glDisable(3553);
        return iArr[0];
    }

    public static int a(Object[] objArr) {
        return Arrays.hashCode(objArr);
    }

    public static Bitmap a(Bitmap bitmap, float f) {
        if (bitmap == null) {
            return null;
        }
        return Bitmap.createScaledBitmap(bitmap, (int) (((float) bitmap.getWidth()) * f), (int) (((float) bitmap.getHeight()) * f), false);
    }

    public static Bitmap a(String str) {
        try {
            InputStream resourceAsStream = BitmapDescriptorFactory.class.getResourceAsStream("/assets/" + str);
            Bitmap decodeStream = BitmapFactory.decodeStream(resourceAsStream);
            resourceAsStream.close();
            return decodeStream;
        } catch (Exception e) {
            return null;
        }
    }

    public static String a(Context context) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return context.getCacheDir().toString() + CookieSpec.PATH_DELIM;
        }
        if (MapsInitializer.sdcardDir == null || MapsInitializer.sdcardDir.equals(PoiTypeDef.All)) {
            File file = new File(Environment.getExternalStorageDirectory(), "amap");
            if (!file.exists()) {
                file.mkdir();
            }
            File file2 = new File(file, "mini_mapv3");
            if (!file2.exists()) {
                file2.mkdir();
            }
            return file2.toString() + CookieSpec.PATH_DELIM;
        }
        File file3 = new File(MapsInitializer.sdcardDir);
        if (!file3.exists()) {
            file3.mkdirs();
        }
        return MapsInitializer.sdcardDir;
    }

    public static String a(String str, Object obj) {
        return str + "=" + String.valueOf(obj);
    }

    public static String a(String... strArr) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String append : strArr) {
            sb.append(append);
            if (i != strArr.length - 1) {
                sb.append(",");
            }
            i++;
        }
        return sb.toString();
    }

    public static FloatBuffer a(float[] fArr) {
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(fArr.length * 4);
        allocateDirect.order(ByteOrder.nativeOrder());
        FloatBuffer asFloatBuffer = allocateDirect.asFloatBuffer();
        asFloatBuffer.put(fArr);
        asFloatBuffer.position(0);
        return asFloatBuffer;
    }

    public static FloatBuffer a(float[] fArr, FloatBuffer floatBuffer) {
        floatBuffer.clear();
        floatBuffer.put(fArr);
        floatBuffer.position(0);
        return floatBuffer;
    }

    public static void a(GL10 gl10, int i) {
        gl10.glDeleteTextures(1, new int[]{i}, 0);
    }

    public static byte[] a(Context context, String str) {
        try {
            InputStream open = context.getAssets().open(str);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = open.read(bArr);
                if (read >= 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.close();
                    open.close();
                    return byteArray;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0039 A[SYNTHETIC, Splitter:B:23:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte[] r5) {
        /*
            r2 = 0
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0043, all -> 0x0035 }
            r1 = 0
            int r3 = r5.length     // Catch:{ Exception -> 0x0043, all -> 0x0035 }
            r0.<init>(r5, r1, r3)     // Catch:{ Exception -> 0x0043, all -> 0x0035 }
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0043, all -> 0x0035 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0043, all -> 0x0035 }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0021 }
            r0.<init>()     // Catch:{ Exception -> 0x0021 }
            r2 = 128(0x80, float:1.794E-43)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0021 }
        L_0x0016:
            int r3 = r1.read(r2)     // Catch:{ Exception -> 0x0021 }
            if (r3 < 0) goto L_0x002b
            r4 = 0
            r0.write(r2, r4, r3)     // Catch:{ Exception -> 0x0021 }
            goto L_0x0016
        L_0x0021:
            r0 = move-exception
        L_0x0022:
            r0.printStackTrace()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ Exception -> 0x003d }
        L_0x002a:
            return r5
        L_0x002b:
            byte[] r5 = r0.toByteArray()     // Catch:{ Exception -> 0x0021 }
            r1.close()     // Catch:{ Exception -> 0x0033 }
            goto L_0x002a
        L_0x0033:
            r0 = move-exception
            goto L_0x002a
        L_0x0035:
            r0 = move-exception
            r1 = r2
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ Exception -> 0x003f }
        L_0x003c:
            throw r0
        L_0x003d:
            r0 = move-exception
            goto L_0x002a
        L_0x003f:
            r1 = move-exception
            goto L_0x003c
        L_0x0041:
            r0 = move-exception
            goto L_0x0037
        L_0x0043:
            r0 = move-exception
            r1 = r2
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.a.b.g.a(byte[]):byte[]");
    }

    public static float b(float f) {
        if (f > 20.0f) {
            return 20.0f;
        }
        if (f < 4.0f) {
            return 4.0f;
        }
        return f;
    }

    public static String b(int i) {
        return i < 1000 ? i + "m" : (i / 1000) + "km";
    }

    public static String b(Context context) {
        if (MapsInitializer.sdcardDir == null || MapsInitializer.sdcardDir.equals(PoiTypeDef.All)) {
            File file = new File(Environment.getExternalStorageDirectory(), "amap");
            if (!file.exists()) {
                file.mkdir();
            }
            File file2 = new File(file, "VMAP2");
            if (!file2.exists()) {
                file2.mkdir();
            }
            return file2.toString() + CookieSpec.PATH_DELIM;
        }
        String str = MapsInitializer.sdcardDir + "VMAP2/";
        File file3 = new File(str);
        if (file3.exists()) {
            return str;
        }
        file3.mkdirs();
        return str;
    }

    public static int c(float f) {
        int i = 1;
        int i2 = 0;
        int i3 = 1;
        while (((float) i3) <= f) {
            i3 *= 2;
            i2 = i;
            i++;
        }
        return i2;
    }

    public static boolean c(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        NetworkInfo.State state = activeNetworkInfo.getState();
        return (state == null || state == NetworkInfo.State.DISCONNECTED || state == NetworkInfo.State.DISCONNECTING) ? false : true;
    }
}
