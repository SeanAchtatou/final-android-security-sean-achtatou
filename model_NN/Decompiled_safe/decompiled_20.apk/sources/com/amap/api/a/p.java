package com.amap.api.a;

import android.location.Location;
import android.view.View;
import com.amap.api.maps.AMap;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.CircleOptions;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.maps.model.PolygonOptions;
import com.amap.api.maps.model.PolylineOptions;
import com.autonavi.amap.mapcore.DPoint;
import com.autonavi.amap.mapcore.FPoint;
import com.autonavi.amap.mapcore.IPoint;
import com.autonavi.amap.mapcore.MapProjection;

public interface p {
    LatLngBounds B();

    float D();

    q a(CircleOptions circleOptions);

    u a(PolygonOptions polygonOptions);

    v a(PolylineOptions polylineOptions);

    Marker a(MarkerOptions markerOptions);

    void a(double d, double d2, IPoint iPoint);

    void a(int i);

    void a(int i, int i2, DPoint dPoint);

    void a(int i, int i2, FPoint fPoint);

    void a(Location location);

    void a(j jVar);

    void a(j jVar, long j, AMap.CancelableCallback cancelableCallback);

    void a(j jVar, AMap.CancelableCallback cancelableCallback);

    void a(s sVar);

    void a(AMap.InfoWindowAdapter infoWindowAdapter);

    void a(AMap.OnCameraChangeListener onCameraChangeListener);

    void a(AMap.OnInfoWindowClickListener onInfoWindowClickListener);

    void a(AMap.OnMapClickListener onMapClickListener);

    void a(AMap.OnMapLoadedListener onMapLoadedListener);

    void a(AMap.OnMapLongClickListener onMapLongClickListener);

    void a(AMap.OnMarkerClickListener onMarkerClickListener);

    void a(AMap.OnMarkerDragListener onMarkerDragListener);

    void a(AMap.OnMyLocationChangeListener onMyLocationChangeListener);

    void a(AMap.onMapPrintScreenListener onmapprintscreenlistener);

    void a(LocationSource locationSource);

    void a(MyLocationStyle myLocationStyle);

    void a(boolean z);

    boolean a(String str);

    int b();

    void b(double d, double d2, IPoint iPoint);

    void b(int i);

    void b(int i, int i2, DPoint dPoint);

    void b(j jVar);

    void b(boolean z);

    boolean b(s sVar);

    boolean b(String str);

    MapProjection c();

    void c(boolean z);

    void d(boolean z);

    void e();

    void e(boolean z);

    void f(boolean z);

    void g(boolean z);

    int i();

    int j();

    void k();

    CameraPosition l();

    float m();

    float n();

    void o();

    void onPause();

    void onResume();

    void p();

    int q();

    boolean r();

    boolean s();

    void setZOrderOnTop(boolean z);

    Location t();

    z u();

    w v();

    View x();

    void y();

    float z();
}
