package com.amap.api.a;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.amap.api.a.b.g;
import com.autonavi.amap.mapcore.IPoint;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.microedition.khronos.opengles.GL10;

class ae extends View {
    p a;
    private CopyOnWriteArrayList<s> b = new CopyOnWriteArrayList<>();
    private IPoint c;
    private s d;
    private final Handler e = new Handler();
    private CopyOnWriteArrayList<Integer> f = new CopyOnWriteArrayList<>();

    public ae(Context context, AttributeSet attributeSet, p pVar) {
        super(context, attributeSet);
        this.a = pVar;
    }

    /* access modifiers changed from: private */
    public void f() {
        Iterator<s> it = this.b.iterator();
        while (it.hasNext()) {
            s next = it.next();
            if (this.d != null && this.d.d().equals(next.d())) {
                Rect b2 = next.b();
                this.c = new IPoint((next.i().getWidth() / 2) + b2.left, b2.top);
                this.a.k();
            }
        }
    }

    public p a() {
        return this.a;
    }

    public s a(MotionEvent motionEvent) {
        for (int size = this.b.size() - 1; size >= 0; size--) {
            s sVar = this.b.get(size);
            if (a(sVar.b(), (int) motionEvent.getX(), (int) motionEvent.getY())) {
                return sVar;
            }
        }
        return null;
    }

    public s a(String str) {
        Iterator<s> it = this.b.iterator();
        while (it.hasNext()) {
            s next = it.next();
            if (next != null && next.d().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public void a(s sVar) {
        e(sVar);
        this.b.remove(sVar);
        this.b.add(sVar);
    }

    /* access modifiers changed from: package-private */
    public void a(Integer num) {
        if (num.intValue() != 0) {
            this.f.add(num);
        }
    }

    public void a(GL10 gl10) {
        Iterator<Integer> it = this.f.iterator();
        while (it.hasNext()) {
            g.a(gl10, it.next().intValue());
        }
        this.f.clear();
        this.e.postDelayed(new Runnable() {
            public void run() {
                ae.this.f();
            }
        }, 0);
        Iterator<s> it2 = this.b.iterator();
        while (it2.hasNext()) {
            it2.next().a(gl10, this.a);
        }
    }

    public boolean a(Rect rect, int i, int i2) {
        return rect.contains(i, i2);
    }

    /* access modifiers changed from: protected */
    public int b() {
        return this.b.size();
    }

    public boolean b(MotionEvent motionEvent) {
        for (int size = this.b.size() - 1; size >= 0; size--) {
            s sVar = this.b.get(size);
            Rect b2 = sVar.b();
            boolean a2 = a(b2, (int) motionEvent.getX(), (int) motionEvent.getY());
            if (a2) {
                this.c = new IPoint(b2.left + (sVar.i().getWidth() / 2), b2.top);
                this.d = sVar;
                return a2;
            }
        }
        return false;
    }

    public boolean b(s sVar) {
        if (sVar.e() != 0) {
            a(Integer.valueOf(sVar.e()));
        }
        e(sVar);
        return this.b.remove(sVar);
    }

    public void c() {
        Iterator<s> it = this.b.iterator();
        while (it.hasNext()) {
            a(Integer.valueOf(it.next().e()));
        }
        this.b.clear();
    }

    public void c(s sVar) {
        int indexOf = this.b.indexOf(sVar);
        int size = this.b.size() - 1;
        this.b.set(indexOf, this.b.get(size));
        this.b.set(size, sVar);
    }

    public s d() {
        return this.d;
    }

    public void d(s sVar) {
        if (this.c == null) {
            this.c = new IPoint();
        }
        Rect b2 = sVar.b();
        this.c = new IPoint(b2.left + (sVar.i().getWidth() / 2), b2.top);
        this.d = sVar;
        try {
            this.a.a(d());
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void e() {
        try {
            Iterator<s> it = this.b.iterator();
            while (it.hasNext()) {
                s next = it.next();
                if (next != null) {
                    next.o();
                }
            }
            c();
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.d("amapApi", "MapOverlayImageView clear erro" + e2.getMessage());
        }
    }

    public void e(s sVar) {
        if (f(sVar)) {
            this.a.y();
            this.d = null;
        }
    }

    public boolean f(s sVar) {
        return this.a.b(sVar);
    }
}
