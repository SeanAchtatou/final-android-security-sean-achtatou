package com.amap.api.a;

import android.location.Location;
import android.os.RemoteException;
import com.amap.api.maps.LocationSource;

class g implements LocationSource.OnLocationChangedListener {
    Location a;
    private p b;

    g(p pVar) {
        this.b = pVar;
    }

    public void onLocationChanged(Location location) {
        this.a = location;
        try {
            if (this.b.s()) {
                this.b.a(location);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
