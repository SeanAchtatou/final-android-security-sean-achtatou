package com.amap.api.a.a;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import com.amap.api.maps.model.BitmapDescriptorFactory;

public abstract class d extends a {
    protected float h;
    protected float i;
    protected float j;
    protected float k;
    private final float l;
    private float m;
    private float n;
    private float o;
    private float p;

    public d(Context context) {
        super(context);
        this.l = (float) ViewConfiguration.get(context).getScaledEdgeSlop();
    }

    protected static float a(MotionEvent motionEvent, int i2) {
        return i2 < motionEvent.getPointerCount() ? (motionEvent.getX() - motionEvent.getRawX()) + motionEvent.getX(i2) : BitmapDescriptorFactory.HUE_RED;
    }

    protected static float b(MotionEvent motionEvent, int i2) {
        return i2 < motionEvent.getPointerCount() ? (motionEvent.getY() - motionEvent.getRawY()) + motionEvent.getY(i2) : BitmapDescriptorFactory.HUE_RED;
    }

    /* access modifiers changed from: protected */
    public void b(MotionEvent motionEvent) {
        super.b(motionEvent);
        MotionEvent motionEvent2 = this.c;
        this.o = -1.0f;
        this.p = -1.0f;
        float x = motionEvent2.getX(0);
        float y = motionEvent2.getY(0);
        this.h = motionEvent2.getX(1) - x;
        this.i = motionEvent2.getY(1) - y;
        float x2 = motionEvent.getX(0);
        float y2 = motionEvent.getY(0);
        this.j = motionEvent.getX(1) - x2;
        this.k = motionEvent.getY(1) - y2;
    }

    public float c() {
        if (this.o == -1.0f) {
            float f = this.j;
            float f2 = this.k;
            this.o = FloatMath.sqrt((f * f) + (f2 * f2));
        }
        return this.o;
    }

    /* access modifiers changed from: protected */
    public boolean d(MotionEvent motionEvent) {
        DisplayMetrics displayMetrics = this.a.getResources().getDisplayMetrics();
        this.m = ((float) displayMetrics.widthPixels) - this.l;
        this.n = ((float) displayMetrics.heightPixels) - this.l;
        float f = this.l;
        float f2 = this.m;
        float f3 = this.n;
        float rawX = motionEvent.getRawX();
        float rawY = motionEvent.getRawY();
        float a = a(motionEvent, 1);
        float b = b(motionEvent, 1);
        boolean z = rawX < f || rawY < f || rawX > f2 || rawY > f3;
        boolean z2 = a < f || b < f || a > f2 || b > f3;
        return (z && z2) || z || z2;
    }
}
