package com.amap.api.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.Location;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.amap.api.a.b.g;
import com.amap.api.maps.model.LatLng;

class aa extends LinearLayout {
    Bitmap a;
    Bitmap b;
    Bitmap c;
    ImageView d;
    p e;
    boolean f = false;

    public aa(Context context, ad adVar, p pVar) {
        super(context);
        this.e = pVar;
        try {
            this.a = g.a("location_selected.png");
            this.b = g.a("location_pressed.png");
            this.a = g.a(this.a, m.a);
            this.b = g.a(this.b, m.a);
            this.c = g.a("location_unselected.png");
            this.c = g.a(this.c, m.a);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.d = new ImageView(context);
        this.d.setImageBitmap(this.a);
        this.d.setPadding(0, 20, 20, 0);
        this.d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
        this.d.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (aa.this.f) {
                    if (motionEvent.getAction() == 0) {
                        aa.this.d.setImageBitmap(aa.this.b);
                    } else if (motionEvent.getAction() == 1) {
                        try {
                            aa.this.d.setImageBitmap(aa.this.a);
                            aa.this.e.g(true);
                            Location t = aa.this.e.t();
                            if (t != null) {
                                LatLng latLng = new LatLng(t.getLatitude(), t.getLongitude());
                                aa.this.e.a(t);
                                aa.this.e.a(j.a(latLng, aa.this.e.z()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                return false;
            }
        });
        addView(this.d);
    }

    public void a() {
        try {
            this.a.recycle();
            this.b.recycle();
            this.c.recycle();
            this.a = null;
            this.b = null;
            this.c = null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(boolean z) {
        this.f = z;
        if (z) {
            this.d.setImageBitmap(this.a);
        } else {
            this.d.setImageBitmap(this.c);
        }
        this.d.invalidate();
    }
}
