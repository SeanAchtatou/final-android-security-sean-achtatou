package com.amap.api.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.api.a.a.b;
import com.amap.api.a.a.c;
import com.amap.api.a.af;
import com.amap.api.a.b.g;
import com.amap.api.a.j;
import com.amap.api.maps.AMap;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.CircleOptions;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.maps.model.PolygonOptions;
import com.amap.api.maps.model.PolylineOptions;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.autonavi.amap.mapcore.DPoint;
import com.autonavi.amap.mapcore.FPoint;
import com.autonavi.amap.mapcore.IPoint;
import com.autonavi.amap.mapcore.MapCore;
import com.autonavi.amap.mapcore.MapProjection;
import java.nio.IntBuffer;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import org.apache.commons.httpclient.HttpStatus;

class b extends GLSurfaceView implements GLSurfaceView.Renderer, p {
    /* access modifiers changed from: private */
    public AMap.OnCameraChangeListener A;
    /* access modifiers changed from: private */
    public AMap.OnMapClickListener B;
    /* access modifiers changed from: private */
    public AMap.OnMapLongClickListener C;
    /* access modifiers changed from: private */
    public AMap.OnInfoWindowClickListener D;
    private AMap.InfoWindowAdapter E;
    /* access modifiers changed from: private */
    public View F;
    /* access modifiers changed from: private */
    public Marker G;
    private w H;
    /* access modifiers changed from: private */
    public z I;
    private LocationSource J;
    private Rect K;
    private boolean L;
    private boolean M;
    private g N;
    /* access modifiers changed from: private */
    public com.amap.api.a.a.b O;
    private ah P;
    /* access modifiers changed from: private */
    public h Q;
    /* access modifiers changed from: private */
    public i R;
    /* access modifiers changed from: private */
    public int S;
    /* access modifiers changed from: private */
    public int T;
    /* access modifiers changed from: private */
    public AMap.CancelableCallback U;
    /* access modifiers changed from: private */
    public boolean V;
    /* access modifiers changed from: private */
    public boolean W;
    private Drawable X;
    private Location Y;
    private final int[] Z;
    ad a;
    private boolean aa;
    /* access modifiers changed from: private */
    public AMap.onMapPrintScreenListener ab;
    private SensorManager ac;
    private Sensor ad;
    private Sensor ae;
    private Sensor af;
    private boolean ag;
    /* access modifiers changed from: private */
    public float[] ah;
    /* access modifiers changed from: private */
    public float[] ai;
    private SensorEventListener aj;
    /* access modifiers changed from: private */
    public boolean ak;
    /* access modifiers changed from: private */
    public boolean al;
    private Handler am;
    private Runnable an;
    private boolean ao;
    /* access modifiers changed from: private */
    public boolean ap;
    /* access modifiers changed from: private */
    public boolean aq;
    /* access modifiers changed from: private */
    public boolean ar;
    /* access modifiers changed from: private */
    public Marker as;
    /* access modifiers changed from: private */
    public boolean at;
    /* access modifiers changed from: private */
    public boolean au;
    /* access modifiers changed from: private */
    public int av;
    /* access modifiers changed from: private */
    public boolean aw;
    private Thread ax;
    /* access modifiers changed from: private */
    public LatLngBounds ay;
    aq b;
    boolean c;
    o d;
    final Handler e;
    private int f;
    private int g;
    private MapCore h;
    /* access modifiers changed from: private */
    public Context i;
    private a j;
    /* access modifiers changed from: private */
    public MapProjection k;
    private GestureDetector l;
    private ScaleGestureDetector m;
    private com.amap.api.a.a.c n;
    private SurfaceHolder o;
    private af p;
    /* access modifiers changed from: private */
    public ae q;
    /* access modifiers changed from: private */
    public ap r;
    private aa s;
    /* access modifiers changed from: private */
    public l t;
    private am u;
    /* access modifiers changed from: private */
    public an v;
    private AMap.OnMyLocationChangeListener w;
    /* access modifiers changed from: private */
    public AMap.OnMarkerClickListener x;
    /* access modifiers changed from: private */
    public AMap.OnMarkerDragListener y;
    /* access modifiers changed from: private */
    public AMap.OnMapLoadedListener z;

    class a implements b.a {
        Float a;
        Float b;
        float c;
        j d;
        private float f;
        private float g;
        private float h;
        private float i;
        private float j;

        private a() {
            this.a = null;
            this.b = null;
            this.d = j.c(this.c);
        }

        /* synthetic */ a(b bVar, c cVar) {
            this();
        }

        public void a(float f2, float f3, float f4, float f5, float f6) {
            this.f = f3;
            this.h = f4;
            this.g = f5;
            this.i = f6;
            this.j = (this.i - this.h) / (this.g - this.f);
            this.a = null;
            this.b = null;
        }

        public boolean a(MotionEvent motionEvent, float f2, float f3, float f4, float f5) {
            try {
                if (!b.this.I.g()) {
                    return true;
                }
                if (this.b == null) {
                    this.b = Float.valueOf(f5);
                }
                if (this.a == null) {
                    this.a = Float.valueOf(f3);
                }
                float f6 = this.h - f3;
                float f7 = this.i - f5;
                float f8 = this.f - f2;
                float f9 = this.g - f4;
                if (((double) Math.abs(this.j - ((f5 - f3) / (f4 - f2)))) >= 0.2d || (((f6 <= BitmapDescriptorFactory.HUE_RED || f7 <= BitmapDescriptorFactory.HUE_RED) && (f6 >= BitmapDescriptorFactory.HUE_RED || f7 >= BitmapDescriptorFactory.HUE_RED)) || ((f8 < BitmapDescriptorFactory.HUE_RED || f9 < BitmapDescriptorFactory.HUE_RED) && (f8 > BitmapDescriptorFactory.HUE_RED || f9 > BitmapDescriptorFactory.HUE_RED)))) {
                    return false;
                }
                boolean unused = b.this.ap = true;
                this.c = b.this.k.getCameraHeaderAngle() - ((this.a.floatValue() - f3) / 4.0f);
                this.d.f = this.c;
                this.d.q = true;
                b.this.a.a(this.d);
                this.a = Float.valueOf(f3);
                this.b = Float.valueOf(f5);
                return true;
            } catch (RemoteException e2) {
                e2.printStackTrace();
                return true;
            }
        }
    }

    /* renamed from: com.amap.api.a.b$b  reason: collision with other inner class name */
    class C0000b implements GestureDetector.OnDoubleTapListener {
        private C0000b() {
        }

        /* synthetic */ C0000b(b bVar, c cVar) {
            this();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
            if (com.amap.api.a.b.f(r5.a).f() == false) goto L_0x000d;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onDoubleTap(android.view.MotionEvent r6) {
            /*
                r5 = this;
                r4 = 1
                com.amap.api.a.b r0 = com.amap.api.a.b.this     // Catch:{ RemoteException -> 0x000e }
                com.amap.api.a.z r0 = r0.I     // Catch:{ RemoteException -> 0x000e }
                boolean r0 = r0.f()     // Catch:{ RemoteException -> 0x000e }
                if (r0 != 0) goto L_0x0012
            L_0x000d:
                return r4
            L_0x000e:
                r0 = move-exception
                r0.printStackTrace()
            L_0x0012:
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                int r0 = r0.av
                if (r0 > r4) goto L_0x000d
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                com.autonavi.amap.mapcore.MapProjection r0 = r0.k
                float r0 = r0.getMapZoomer()
                com.amap.api.a.b r1 = com.amap.api.a.b.this
                float r1 = r1.m()
                int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                if (r0 == 0) goto L_0x000d
                float r0 = r6.getX()
                float r1 = r6.getY()
                int r0 = (int) r0
                int r1 = (int) r1
                r2 = 1065353216(0x3f800000, float:1.0)
                android.graphics.Point r3 = new android.graphics.Point
                r3.<init>(r0, r1)
                com.amap.api.a.j r0 = com.amap.api.a.j.a(r2, r3)
                r0.q = r4
                com.amap.api.a.b r1 = com.amap.api.a.b.this
                com.amap.api.a.ad r1 = r1.a
                r1.a(r0)
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.amap.api.a.b.C0000b.onDoubleTap(android.view.MotionEvent):boolean");
        }

        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            return false;
        }
    }

    class c implements GestureDetector.OnGestureListener {
        FPoint a;
        IPoint b;
        j c;

        private c() {
            this.a = new FPoint();
            this.b = new IPoint();
            this.c = j.a(this.b);
        }

        /* synthetic */ c(b bVar, c cVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.api.a.b.a(boolean, com.amap.api.maps.model.CameraPosition):void
         arg types: [int, ?[OBJECT, ARRAY]]
         candidates:
          com.amap.api.a.b.a(com.amap.api.a.b, int):int
          com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.AMap$CancelableCallback):com.amap.api.maps.AMap$CancelableCallback
          com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.AMap$onMapPrintScreenListener):com.amap.api.maps.AMap$onMapPrintScreenListener
          com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.model.LatLngBounds):com.amap.api.maps.model.LatLngBounds
          com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.model.Marker):com.amap.api.maps.model.Marker
          com.amap.api.a.b.a(com.amap.api.a.b, boolean):boolean
          com.amap.api.a.b.a(com.amap.api.a.b, float[]):float[]
          com.amap.api.a.b.a(com.amap.api.a.j, com.amap.api.maps.AMap$CancelableCallback):void
          com.amap.api.a.p.a(com.amap.api.a.j, com.amap.api.maps.AMap$CancelableCallback):void
          com.amap.api.a.b.a(boolean, com.amap.api.maps.model.CameraPosition):void */
        public boolean onDown(MotionEvent motionEvent) {
            boolean unused = b.this.at = false;
            if (!b.this.R.a()) {
                b.this.R.a(true);
                b.this.a(true, (CameraPosition) null);
                if (b.this.U != null) {
                    b.this.U.onCancel();
                }
                AMap.CancelableCallback unused2 = b.this.U = (AMap.CancelableCallback) null;
            }
            int unused3 = b.this.av = 0;
            this.a.x = motionEvent.getX();
            this.a.y = motionEvent.getY();
            return true;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0011, code lost:
            if (com.amap.api.a.b.f(r10.d).e() == false) goto L_0x0013;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onFling(android.view.MotionEvent r11, android.view.MotionEvent r12, float r13, float r14) {
            /*
                r10 = this;
                r9 = 1
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                r1 = 0
                boolean unused = r0.at = r1
                com.amap.api.a.b r0 = com.amap.api.a.b.this     // Catch:{ RemoteException -> 0x0014 }
                com.amap.api.a.z r0 = r0.I     // Catch:{ RemoteException -> 0x0014 }
                boolean r0 = r0.e()     // Catch:{ RemoteException -> 0x0014 }
                if (r0 != 0) goto L_0x0018
            L_0x0013:
                return r9
            L_0x0014:
                r0 = move-exception
                r0.printStackTrace()
            L_0x0018:
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                com.amap.api.a.a.b r0 = r0.O
                boolean r0 = r0.b()
                if (r0 != 0) goto L_0x0013
                long r0 = r11.getEventTime()
                com.amap.api.a.b r2 = com.amap.api.a.b.this
                com.amap.api.a.a.b r2 = r2.O
                long r2 = r2.c()
                long r0 = r0 - r2
                r2 = 30
                int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r0 < 0) goto L_0x0013
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                int r0 = r0.i()
                com.amap.api.a.b r1 = com.amap.api.a.b.this
                int r1 = r1.j()
                int r6 = r0 * 2
                int r8 = r1 * 2
                com.amap.api.a.b r2 = com.amap.api.a.b.this
                int r0 = r0 / 2
                int unused = r2.S = r0
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                int r1 = r1 / 2
                int unused = r0.T = r1
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                r1 = 0
                com.amap.api.maps.AMap.CancelableCallback unused = r0.U = r1
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                com.amap.api.a.i r0 = r0.R
                com.amap.api.a.b r1 = com.amap.api.a.b.this
                int r1 = r1.S
                com.amap.api.a.b r2 = com.amap.api.a.b.this
                int r2 = r2.T
                float r3 = -r13
                int r3 = (int) r3
                int r3 = r3 * 3
                int r3 = r3 / 5
                float r4 = -r14
                int r4 = (int) r4
                int r4 = r4 * 3
                int r4 = r4 / 5
                int r5 = -r6
                int r7 = -r8
                r0.a(r1, r2, r3, r4, r5, r6, r7, r8)
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                com.amap.api.a.an r0 = r0.v
                if (r0 == 0) goto L_0x0013
                com.amap.api.a.b r0 = com.amap.api.a.b.this
                com.amap.api.a.an r0 = r0.v
                r0.a(r9)
                goto L_0x0013
            */
            throw new UnsupportedOperationException("Method not decompiled: com.amap.api.a.b.c.onFling(android.view.MotionEvent, android.view.MotionEvent, float, float):boolean");
        }

        public void onLongPress(MotionEvent motionEvent) {
            boolean unused = b.this.at = false;
            if (b.this.C != null) {
                DPoint dPoint = new DPoint();
                b.this.a((int) motionEvent.getX(), (int) motionEvent.getY(), dPoint);
                b.this.C.onMapLongClick(new LatLng(dPoint.y, dPoint.x));
                boolean unused2 = b.this.aw = true;
            }
            s a2 = b.this.q.a(motionEvent);
            if (b.this.y != null && a2 != null && a2.j()) {
                Marker unused3 = b.this.as = new Marker(a2);
                LatLng position = b.this.as.getPosition();
                IPoint iPoint = new IPoint();
                b.this.b(position.latitude, position.longitude, iPoint);
                iPoint.y -= 60;
                DPoint dPoint2 = new DPoint();
                b.this.a(iPoint.x, iPoint.y, dPoint2);
                b.this.as.setPosition(new LatLng(dPoint2.y, dPoint2.x));
                b.this.y.onMarkerDragStart(b.this.as);
                boolean unused4 = b.this.ar = true;
            }
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            boolean unused = b.this.at = true;
            if ((!b.this.R.a() && b.this.R.j() == 1) || b.this.O.b() || motionEvent2.getEventTime() - b.this.O.c() < 30) {
                boolean unused2 = b.this.at = false;
            } else if (motionEvent2.getPointerCount() >= 2) {
                boolean unused3 = b.this.at = false;
            } else {
                try {
                    if (!b.this.I.e()) {
                        boolean unused4 = b.this.at = false;
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                if (b.this.av > 1) {
                    boolean unused5 = b.this.at = false;
                } else {
                    float f3 = this.a.x;
                    float f4 = this.a.y;
                    FPoint fPoint = new FPoint();
                    FPoint fPoint2 = new FPoint();
                    FPoint fPoint3 = new FPoint();
                    b.this.k.win2Map((int) f3, (int) f4, fPoint);
                    b.this.k.win2Map((int) (f3 - f), (int) (f4 - f2), fPoint2);
                    float f5 = fPoint2.x - fPoint.x;
                    float f6 = fPoint2.y - fPoint.y;
                    b.this.k.getMapCenter(fPoint3);
                    b.this.k.map2Geo(fPoint3.x - f5, fPoint3.y - f6, this.b);
                    this.c.p = this.b;
                    b.this.a.a(this.c);
                }
            }
            return true;
        }

        public void onShowPress(MotionEvent motionEvent) {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            boolean unused = b.this.at = false;
            if (b.this.aw) {
                boolean unused2 = b.this.aw = false;
            } else {
                if (b.this.F != null) {
                    if (b.this.q.a(new Rect(b.this.F.getLeft(), b.this.F.getTop(), b.this.F.getRight(), b.this.F.getBottom()), (int) motionEvent.getX(), (int) motionEvent.getY()) && b.this.D != null) {
                        s d2 = b.this.q.d();
                        if (d2.n()) {
                            b.this.D.onInfoWindowClick(new Marker(d2));
                        }
                    }
                }
                try {
                    if (b.this.q.b(motionEvent)) {
                        s d3 = b.this.q.d();
                        if (d3.n() && d3 != null) {
                            Marker marker = new Marker(d3);
                            if (b.this.x != null) {
                                if (!b.this.x.onMarkerClick(marker) && b.this.q.b() > 0) {
                                    try {
                                        b.this.a(d3);
                                        LatLng c2 = d3.c();
                                        if (c2 != null) {
                                            IPoint iPoint = new IPoint();
                                            b.this.a(c2.latitude, c2.longitude, iPoint);
                                            b.this.a(j.a(iPoint));
                                        }
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            b.this.q.c(d3);
                        }
                    } else if (b.this.B != null) {
                        DPoint dPoint = new DPoint();
                        b.this.a((int) motionEvent.getX(), (int) motionEvent.getY(), dPoint);
                        b.this.B.onMapClick(new LatLng(dPoint.y, dPoint.x));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            return true;
        }
    }

    class d implements c.a {
        float a;
        float b;
        j c;

        private d() {
            this.c = j.d(this.b);
        }

        /* synthetic */ d(b bVar, c cVar) {
            this();
        }

        public boolean a(com.amap.api.a.a.c cVar) {
            if (b.this.ap) {
                return false;
            }
            float b2 = cVar.b();
            this.a += b2;
            if (!b.this.au && Math.abs(this.a) <= 30.0f && Math.abs(this.a) <= 350.0f) {
                return true;
            }
            boolean unused = b.this.au = true;
            this.b = b2 + b.this.k.getMapAngle();
            this.c.g = this.b;
            this.c.q = true;
            b.this.a.a(this.c);
            this.a = BitmapDescriptorFactory.HUE_RED;
            return true;
        }

        public boolean b(com.amap.api.a.a.c cVar) {
            try {
                if (!b.this.I.h()) {
                    return false;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            boolean unused = b.this.au = false;
            this.a = BitmapDescriptorFactory.HUE_RED;
            int unused2 = b.this.av = 2;
            return !b.this.ap && ((float) (b.this.getWidth() / 4)) < cVar.c();
        }

        public void c(com.amap.api.a.a.c cVar) {
            boolean unused = b.this.au = false;
            this.a = BitmapDescriptorFactory.HUE_RED;
            b.this.I();
        }
    }

    class e implements ScaleGestureDetector.OnScaleGestureListener {
        private float b;

        private e() {
            this.b = BitmapDescriptorFactory.HUE_RED;
        }

        /* synthetic */ e(b bVar, c cVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.api.a.b.c(com.amap.api.a.b, boolean):boolean
         arg types: [com.amap.api.a.b, int]
         candidates:
          com.amap.api.a.b.c(com.amap.api.a.b, int):int
          com.amap.api.a.b.c(com.amap.api.a.b, boolean):boolean */
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            if (!b.this.ap) {
                float scaleFactor = scaleGestureDetector.getScaleFactor();
                if (b.this.aq || ((double) scaleFactor) > 1.08d || ((double) scaleFactor) < 0.92d) {
                    boolean unused = b.this.aq = true;
                    j a2 = j.a((scaleFactor - 1.0f) + this.b);
                    a2.q = true;
                    b.this.a.a(a2);
                }
            }
            return false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.amap.api.a.b.c(com.amap.api.a.b, boolean):boolean
         arg types: [com.amap.api.a.b, int]
         candidates:
          com.amap.api.a.b.c(com.amap.api.a.b, int):int
          com.amap.api.a.b.c(com.amap.api.a.b, boolean):boolean */
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            try {
                if (!b.this.I.f()) {
                    return false;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            int unused = b.this.av = 2;
            if (b.this.ap) {
                return false;
            }
            boolean unused2 = b.this.aq = false;
            this.b = b.this.k.getMapZoomer();
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
            this.b = BitmapDescriptorFactory.HUE_RED;
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            if (((double) scaleFactor) < 1.03d && ((double) scaleFactor) > 0.97d && !b.this.aq && !b.this.ap) {
                j c = j.c();
                c.q = true;
                b.this.a.a(c);
            }
            b.this.I();
        }
    }

    public b(Context context) {
        this(context, null);
        ab.a = context;
        this.i = context;
        this.o = getHolder();
        this.o.addCallback(this);
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = -1;
        this.g = 1;
        this.j = null;
        this.a = new ad(this);
        this.o = null;
        this.K = new Rect();
        this.L = true;
        this.M = false;
        this.c = false;
        this.S = 0;
        this.T = 0;
        this.U = null;
        this.V = false;
        this.W = false;
        this.X = null;
        this.Z = new int[]{10000000, 5000000, 2000000, 1000000, 500000, 200000, 100000, 50000, 30000, 20000, 10000, 5000, 2000, 1000, 500, HttpStatus.SC_OK, 100, 50, 25, 10, 5};
        this.aa = false;
        this.ab = null;
        this.ag = false;
        this.d = new o();
        this.ah = new float[3];
        this.ai = new float[3];
        this.aj = new c(this);
        this.ak = false;
        this.al = false;
        this.am = new Handler();
        this.an = new d(this);
        this.ao = false;
        this.ap = false;
        this.aq = false;
        this.ar = false;
        this.as = null;
        this.at = false;
        this.au = false;
        this.av = 0;
        this.aw = false;
        this.ax = new e(this);
        this.ay = null;
        this.e = new f(this);
        setBackgroundColor(Color.argb((int) MotionEventCompat.ACTION_MASK, 235, 235, 235));
        this.Q = new h();
        this.i = context;
        this.h = new MapCore(context);
        this.j = new a(this);
        this.h.setMapCallback(this.j);
        this.k = this.h.getMapstate();
        this.a.a(j.a(new LatLng(39.924216d, 116.3978653d), 10.0f, (float) BitmapDescriptorFactory.HUE_RED, (float) BitmapDescriptorFactory.HUE_RED));
        this.H = new al(this);
        this.N = new g(this);
        this.I = new ao(this);
        this.l = new GestureDetector(context, new c(this, null));
        this.l.setOnDoubleTapListener(new C0000b(this, null));
        this.l.setIsLongpressEnabled(true);
        this.m = new ScaleGestureDetector(context, new e(this, null));
        this.n = new com.amap.api.a.a.c(context, new d(this, null));
        this.O = new com.amap.api.a.a.b(context, new a(this, null));
        this.p = new af(context, this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        this.p.addView(this, 0, layoutParams);
        this.q = new ae(this.i, attributeSet, this);
        this.p.addView(this.q, new af.a(layoutParams));
        this.r = new ap(this.i, this);
        this.p.addView(this.r, layoutParams);
        this.u = new am(this.i, this);
        this.p.addView(this.u, layoutParams);
        this.v = new an(this.i, this);
        this.p.addView(this.v, layoutParams);
        this.b = new aq(this.i, this.a, this);
        this.p.addView(this.b, new af.a(-2, -2, new LatLng(0.0d, 0.0d), 0, 0, 83));
        af.a aVar = new af.a(-2, -2, new LatLng(0.0d, 0.0d), 0, 0, 83);
        this.s = new aa(this.i, this.a, this);
        this.p.addView(this.s, aVar);
        try {
            if (!u().d()) {
                this.s.setVisibility(8);
            }
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
        af.a aVar2 = new af.a(-2, -2, new LatLng(0.0d, 0.0d), 0, 0, 51);
        this.t = new l(this.i, this.a, this);
        this.p.addView(this.t, aVar2);
        this.t.setVisibility(8);
        this.R = new i(context, new AccelerateDecelerateInterpolator());
        this.P = new ah(this);
        setRenderer(this);
        this.ac = (SensorManager) context.getSystemService("sensor");
        this.ad = this.ac.getDefaultSensor(3);
        this.ae = this.ac.getDefaultSensor(1);
        this.af = this.ac.getDefaultSensor(2);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void F() {
        /*
            r8 = this;
            r7 = 3
            r6 = 1135869952(0x43b40000, float:360.0)
            float[] r0 = new float[r7]
            r1 = 9
            float[] r1 = new float[r1]
            r2 = 0
            float[] r3 = r8.ah
            float[] r4 = r8.ai
            android.hardware.SensorManager.getRotationMatrix(r1, r2, r3, r4)
            android.hardware.SensorManager.getOrientation(r1, r0)
            r1 = 0
            r0 = r0[r1]
            double r0 = (double) r0
            double r0 = java.lang.Math.toDegrees(r0)
            float r1 = (float) r0
            com.autonavi.amap.mapcore.MapProjection r0 = r8.k
            float r2 = r0.getMapAngle()
            android.content.Context r0 = r8.getContext()
            java.lang.String r3 = "window"
            java.lang.Object r0 = r0.getSystemService(r3)
            android.view.WindowManager r0 = (android.view.WindowManager) r0
            android.view.Display r0 = r0.getDefaultDisplay()
            int r0 = r0.getRotation()
            java.lang.String r3 = "AMapDelegateImpGLSurfaceView"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "rotation: "
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
            if (r0 == 0) goto L_0x007a
            r3 = 1
            if (r0 != r3) goto L_0x006b
            r0 = 1119092736(0x42b40000, float:90.0)
            float r0 = r0 + r1
            float r0 = r0 % r6
        L_0x0054:
            float r1 = r2 - r0
            float r1 = java.lang.Math.abs(r1)
            double r1 = (double) r1
            r3 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x006a
            com.amap.api.a.ad r1 = r8.a
            com.amap.api.a.j r0 = com.amap.api.a.j.d(r0)
            r1.a(r0)
        L_0x006a:
            return
        L_0x006b:
            r3 = 2
            if (r0 != r3) goto L_0x0073
            r0 = 1127481344(0x43340000, float:180.0)
            float r0 = r0 + r1
            float r0 = r0 % r6
            goto L_0x0054
        L_0x0073:
            if (r0 != r7) goto L_0x007a
            r0 = 1132920832(0x43870000, float:270.0)
            float r0 = r0 + r1
            float r0 = r0 % r6
            goto L_0x0054
        L_0x007a:
            r0 = r1
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.a.b.F():void");
    }

    private LatLng G() {
        DPoint dPoint = new DPoint();
        IPoint iPoint = new IPoint();
        this.k.getGeoCenter(iPoint);
        MapProjection.geo2LonLat(iPoint.x, iPoint.y, dPoint);
        return new LatLng(dPoint.y, dPoint.x);
    }

    private CameraPosition H() {
        return CameraPosition.builder().target(G()).bearing(this.k.getMapAngle()).tilt(this.k.getCameraHeaderAngle()).zoom(this.k.getMapZoomer()).build();
    }

    /* access modifiers changed from: private */
    public void I() {
        this.ap = false;
        this.aq = false;
        this.ar = false;
        if (this.y != null && this.as != null) {
            this.y.onMarkerDragEnd(this.as);
            this.as = null;
        }
    }

    public static Bitmap a(int i2, int i3, int i4, int i5, GL10 gl10) {
        int[] iArr = new int[(i4 * i5)];
        int[] iArr2 = new int[(i4 * i5)];
        IntBuffer wrap = IntBuffer.wrap(iArr);
        wrap.position(0);
        gl10.glReadPixels(i2, i3, i4, i5, 6408, 5121, wrap);
        for (int i6 = 0; i6 < i5; i6++) {
            for (int i7 = 0; i7 < i4; i7++) {
                int i8 = iArr[(i6 * i4) + i7];
                iArr2[(((i5 - i6) - 1) * i4) + i7] = (i8 & -16711936) | ((i8 << 16) & 16711680) | ((i8 >> 16) & MotionEventCompat.ACTION_MASK);
            }
        }
        try {
            Bitmap createBitmap = Bitmap.createBitmap(i4, i5, Bitmap.Config.ARGB_8888);
            createBitmap.setPixels(iArr2, 0, i4, 0, 0, i4, i5);
            return createBitmap;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private void a(MotionEvent motionEvent) {
        if (this.ar && this.as != null) {
            DPoint dPoint = new DPoint();
            a((int) motionEvent.getX(), (int) (motionEvent.getY() - 60.0f), dPoint);
            this.as.setPosition(new LatLng(dPoint.y, dPoint.x));
            this.y.onMarkerDrag(this.as);
        }
    }

    /* access modifiers changed from: package-private */
    public void A() {
        this.e.obtainMessage(18).sendToTarget();
    }

    public LatLngBounds B() {
        return this.ay;
    }

    /* access modifiers changed from: package-private */
    public Point C() {
        return this.r.c();
    }

    public float D() {
        int width = getWidth();
        DPoint dPoint = new DPoint();
        DPoint dPoint2 = new DPoint();
        a(0, 0, dPoint);
        a(width, 0, dPoint2);
        return (float) (g.a(new LatLng(dPoint.y, dPoint.x), new LatLng(dPoint2.y, dPoint2.x)) / ((double) width));
    }

    /* access modifiers changed from: package-private */
    public void E() {
        this.e.sendEmptyMessage(20);
    }

    public float a(float f2) {
        return g.b(f2);
    }

    public q a(CircleOptions circleOptions) {
        k kVar = new k(this);
        kVar.b(circleOptions.getFillColor());
        kVar.a(circleOptions.getCenter());
        kVar.a(circleOptions.isVisible());
        kVar.b(circleOptions.getStrokeWidth());
        kVar.a(circleOptions.getZIndex());
        kVar.a(circleOptions.getStrokeColor());
        kVar.a(circleOptions.getRadius());
        this.d.a(kVar);
        e(false);
        return kVar;
    }

    public u a(PolygonOptions polygonOptions) {
        aj ajVar = new aj(this);
        ajVar.a(polygonOptions.getFillColor());
        ajVar.a(polygonOptions.getPoints());
        ajVar.a(polygonOptions.isVisible());
        ajVar.b(polygonOptions.getStrokeWidth());
        ajVar.a(polygonOptions.getZIndex());
        ajVar.b(polygonOptions.getStrokeColor());
        this.d.a(ajVar);
        e(false);
        return ajVar;
    }

    public v a(PolylineOptions polylineOptions) {
        ak akVar = new ak(this);
        akVar.a(polylineOptions.getColor());
        akVar.a(polylineOptions.getPoints());
        akVar.a(polylineOptions.isVisible());
        akVar.b(polylineOptions.getWidth());
        akVar.a(polylineOptions.getZIndex());
        akVar.b(polylineOptions.isUseTexture());
        this.d.a(akVar);
        e(false);
        return akVar;
    }

    public Marker a(MarkerOptions markerOptions) {
        ag agVar = new ag(markerOptions, this.q);
        this.q.a(agVar);
        e(false);
        return new Marker(agVar);
    }

    public MapCore a() {
        return this.h;
    }

    public void a(double d2, double d3, IPoint iPoint) {
        MapProjection.lonlat2Geo(d3, d2, iPoint);
    }

    public void a(int i2) {
        if (i2 == 2) {
            this.g = 2;
            this.a.a(new ac(2011).a(true));
            this.r.a(true);
            return;
        }
        this.g = 1;
        this.a.a(new ac(2011).a(false));
        this.r.a(false);
    }

    public void a(int i2, int i3, DPoint dPoint) {
        FPoint fPoint = new FPoint();
        this.k.win2Map(i2, i3, fPoint);
        IPoint iPoint = new IPoint();
        this.k.map2Geo(fPoint.x, fPoint.y, iPoint);
        MapProjection.geo2LonLat(iPoint.x, iPoint.y, dPoint);
    }

    public void a(int i2, int i3, FPoint fPoint) {
        this.k.geo2Map(i3, i2, fPoint);
    }

    public void a(int i2, int i3, IPoint iPoint) {
        FPoint fPoint = new FPoint();
        this.k.win2Map(i2, i3, fPoint);
        this.k.map2Geo(fPoint.x, fPoint.y, iPoint);
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.e.postDelayed(new Runnable() {
            public void run() {
                b.this.t.setVisibility(8);
            }
        }, j2);
    }

    public void a(Location location) {
        if (location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            try {
                if (!s() || this.J == null) {
                    this.P.a();
                    this.P = null;
                    return;
                }
                if (this.P == null) {
                    this.P = new ah(this);
                    a(j.a(latLng, this.k.getMapZoomer()));
                }
                this.P.a(latLng, (double) location.getAccuracy());
                if (!(this.w == null || (this.Y != null && this.Y.getBearing() == location.getBearing() && this.Y.getAccuracy() == location.getAccuracy() && this.Y.getLatitude() == location.getLatitude() && this.Y.getLongitude() == location.getLongitude()))) {
                    this.w.onMyLocationChange(location);
                }
                this.Y = new Location(location);
                e(false);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(j jVar) {
        if (jVar.a == j.a.newLatLngBounds) {
            com.amap.api.a.b.b.a(getWidth() > 0 && getHeight() > 0, "the map must have a size");
        }
        this.U = null;
        jVar.q = true;
        this.a.a(jVar);
    }

    public void a(j jVar, long j2, AMap.CancelableCallback cancelableCallback) {
        if (jVar.a == j.a.newLatLngBounds) {
            com.amap.api.a.b.b.a(getWidth() > 0 && getHeight() > 0, "the map must have a size");
        }
        if (!this.R.a()) {
            this.R.a(true);
            if (this.U != null) {
                this.U.onCancel();
            }
        }
        this.U = cancelableCallback;
        if (this.V) {
            this.W = true;
        }
        if (jVar.a == j.a.scrollBy) {
            if (jVar.b == BitmapDescriptorFactory.HUE_RED && jVar.c == BitmapDescriptorFactory.HUE_RED) {
                this.e.obtainMessage(17).sendToTarget();
                return;
            }
            IPoint iPoint = new IPoint();
            this.k.getGeoCenter(iPoint);
            IPoint iPoint2 = new IPoint();
            a((getWidth() / 2) + ((int) jVar.b), (getHeight() / 2) + ((int) jVar.c), iPoint2);
            this.R.a(iPoint.x, iPoint.y, this.k.getMapZoomer(), this.k.getMapAngle(), this.k.getCameraHeaderAngle(), iPoint2.x - iPoint.x, iPoint2.y - iPoint.y, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, j2);
        } else if (jVar.a == j.a.zoomIn) {
            float mapZoomer = this.k.getMapZoomer();
            float b2 = g.b(1.0f + mapZoomer) - mapZoomer;
            if (b2 == BitmapDescriptorFactory.HUE_RED) {
                this.e.obtainMessage(17).sendToTarget();
                return;
            }
            IPoint iPoint3 = new IPoint();
            this.k.getGeoCenter(iPoint3);
            this.R.a(iPoint3.x, iPoint3.y, mapZoomer, this.k.getMapAngle(), this.k.getCameraHeaderAngle(), 0, 0, b2, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, j2);
        } else if (jVar.a == j.a.zoomOut) {
            float mapZoomer2 = this.k.getMapZoomer();
            float b3 = g.b(mapZoomer2 - 1.0f) - mapZoomer2;
            if (b3 == BitmapDescriptorFactory.HUE_RED) {
                this.e.obtainMessage(17).sendToTarget();
                return;
            }
            IPoint iPoint4 = new IPoint();
            this.k.getGeoCenter(iPoint4);
            this.R.a(iPoint4.x, iPoint4.y, mapZoomer2, this.k.getMapAngle(), this.k.getCameraHeaderAngle(), 0, 0, b3, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, j2);
        } else if (jVar.a == j.a.zoomTo) {
            float mapZoomer3 = this.k.getMapZoomer();
            float b4 = g.b(jVar.d) - mapZoomer3;
            if (b4 == BitmapDescriptorFactory.HUE_RED) {
                this.e.obtainMessage(17).sendToTarget();
                return;
            }
            IPoint iPoint5 = new IPoint();
            this.k.getGeoCenter(iPoint5);
            this.R.a(iPoint5.x, iPoint5.y, mapZoomer3, this.k.getMapAngle(), this.k.getCameraHeaderAngle(), 0, 0, b4, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, j2);
        } else if (jVar.a == j.a.zoomBy) {
            float f2 = jVar.e;
            float mapZoomer4 = this.k.getMapZoomer();
            float b5 = g.b(f2 + mapZoomer4) - mapZoomer4;
            if (b5 == BitmapDescriptorFactory.HUE_RED) {
                this.e.obtainMessage(17).sendToTarget();
                return;
            }
            Point point = jVar.m;
            IPoint iPoint6 = new IPoint();
            this.k.getGeoCenter(iPoint6);
            int i2 = 0;
            int i3 = 0;
            if (point != null) {
                IPoint iPoint7 = new IPoint();
                a(point.x, point.y, iPoint7);
                i2 = (int) (((double) iPoint7.x) + (((double) (iPoint6.x - iPoint7.x)) / Math.pow(2.0d, (double) b5)));
                i3 = (int) ((((double) (iPoint6.y - iPoint7.y)) / Math.pow(2.0d, (double) b5)) + ((double) iPoint7.y));
            }
            this.R.a(iPoint6.x, iPoint6.y, mapZoomer4, this.k.getMapAngle(), this.k.getCameraHeaderAngle(), i2, i3, b5, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, j2);
        } else if (jVar.a == j.a.newCameraPosition) {
            IPoint iPoint8 = new IPoint();
            this.k.getGeoCenter(iPoint8);
            IPoint iPoint9 = new IPoint();
            CameraPosition cameraPosition = jVar.h;
            MapProjection.lonlat2Geo(cameraPosition.target.longitude, cameraPosition.target.latitude, iPoint9);
            float mapZoomer5 = this.k.getMapZoomer();
            int i4 = iPoint9.x - iPoint8.x;
            int i5 = iPoint9.y - iPoint8.y;
            float b6 = g.b(cameraPosition.zoom) - mapZoomer5;
            float mapAngle = this.k.getMapAngle();
            float f3 = cameraPosition.bearing - mapAngle;
            float cameraHeaderAngle = this.k.getCameraHeaderAngle();
            float a2 = g.a(cameraPosition.tilt) - cameraHeaderAngle;
            if (i4 == 0 && i5 == 0 && b6 == BitmapDescriptorFactory.HUE_RED && f3 == BitmapDescriptorFactory.HUE_RED && a2 == BitmapDescriptorFactory.HUE_RED) {
                this.e.obtainMessage(17).sendToTarget();
                return;
            }
            this.R.a(iPoint8.x, iPoint8.y, mapZoomer5, mapAngle, cameraHeaderAngle, i4, i5, b6, f3, a2, j2);
        } else {
            jVar.q = true;
            this.a.a(jVar);
        }
        e(false);
    }

    public void a(j jVar, AMap.CancelableCallback cancelableCallback) {
        a(jVar, 250, cancelableCallback);
    }

    public void a(s sVar) {
        int i2;
        int i3 = -2;
        ViewGroup.LayoutParams layoutParams = null;
        if (sVar != null) {
            if ((sVar.g() != null || sVar.h() != null) && this.E != null) {
                y();
                Marker marker = new Marker(sVar);
                this.F = this.E.getInfoWindow(marker);
                try {
                    if (this.X == null) {
                        this.X = ai.a(this.i, "infowindow_bg.9.png");
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                if (this.F == null) {
                    this.F = this.E.getInfoContents(marker);
                }
                if (this.F != null) {
                    layoutParams = this.F.getLayoutParams();
                    if (this.F.getBackground() == null) {
                        this.F.setBackgroundDrawable(this.X);
                    }
                } else {
                    LinearLayout linearLayout = new LinearLayout(this.i);
                    linearLayout.setBackgroundDrawable(this.X);
                    TextView textView = new TextView(this.i);
                    textView.setText(sVar.g());
                    textView.setTextColor(-16777216);
                    TextView textView2 = new TextView(this.i);
                    textView2.setTextColor(-16777216);
                    textView2.setText(sVar.h());
                    linearLayout.setOrientation(1);
                    linearLayout.addView(textView);
                    linearLayout.addView(textView2);
                    this.F = linearLayout;
                }
                this.F.setDrawingCacheEnabled(true);
                this.F.setDrawingCacheQuality(0);
                FPoint f2 = sVar.f();
                if (layoutParams != null) {
                    i2 = layoutParams.width;
                    i3 = layoutParams.height;
                } else {
                    i2 = -2;
                }
                af.a aVar = new af.a(i2, i3, sVar.c(), (-((int) f2.x)) + (sVar.i().getWidth() / 2), (-((int) f2.y)) + 2, 81);
                this.G = marker;
                this.p.addView(this.F, aVar);
            }
        }
    }

    public void a(AMap.InfoWindowAdapter infoWindowAdapter) {
        this.E = infoWindowAdapter;
    }

    public void a(AMap.OnCameraChangeListener onCameraChangeListener) {
        this.A = onCameraChangeListener;
    }

    public void a(AMap.OnInfoWindowClickListener onInfoWindowClickListener) {
        this.D = onInfoWindowClickListener;
    }

    public void a(AMap.OnMapClickListener onMapClickListener) {
        this.B = onMapClickListener;
    }

    public void a(AMap.OnMapLoadedListener onMapLoadedListener) {
        this.z = onMapLoadedListener;
    }

    public void a(AMap.OnMapLongClickListener onMapLongClickListener) {
        this.C = onMapLongClickListener;
    }

    public void a(AMap.OnMarkerClickListener onMarkerClickListener) {
        this.x = onMarkerClickListener;
    }

    public void a(AMap.OnMarkerDragListener onMarkerDragListener) {
        this.y = onMarkerDragListener;
    }

    public void a(AMap.OnMyLocationChangeListener onMyLocationChangeListener) {
        this.w = onMyLocationChangeListener;
    }

    public void a(AMap.onMapPrintScreenListener onmapprintscreenlistener) {
        this.ab = onmapprintscreenlistener;
        this.aa = true;
    }

    public void a(LocationSource locationSource) {
        this.J = locationSource;
        if (locationSource != null) {
            this.s.a(true);
        } else {
            this.s.a(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(CameraPosition cameraPosition) {
        Message message = new Message();
        message.what = 10;
        message.obj = cameraPosition;
        this.e.sendMessage(message);
    }

    /* access modifiers changed from: package-private */
    public void a(LatLngBounds latLngBounds, int i2, int i3, int i4) {
        IPoint iPoint = new IPoint();
        MapProjection.lonlat2Geo((latLngBounds.northeast.longitude + latLngBounds.southwest.longitude) / 2.0d, (latLngBounds.northeast.latitude + latLngBounds.southwest.latitude) / 2.0d, iPoint);
        j a2 = j.a(iPoint, this.k.getMapZoomer(), (float) BitmapDescriptorFactory.HUE_RED, (float) BitmapDescriptorFactory.HUE_RED);
        a2.o = true;
        this.a.a(a2);
        try {
            Thread.sleep(41);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        float mapZoomer = this.k.getMapZoomer();
        IPoint iPoint2 = new IPoint();
        IPoint iPoint3 = new IPoint();
        b(latLngBounds.northeast.latitude, latLngBounds.northeast.longitude, iPoint2);
        b(latLngBounds.southwest.latitude, latLngBounds.southwest.longitude, iPoint3);
        float max = Math.max(((float) (iPoint2.x - iPoint3.x)) / ((float) (i2 - (i4 * 2))), ((float) (iPoint3.y - iPoint2.y)) / ((float) (i3 - (i4 * 2))));
        j a3 = j.a(max > 1.0f ? g.b(mapZoomer - ((float) g.c(max))) : ((double) max) < 0.5d ? g.b((((float) g.c(1.0f / max)) + mapZoomer) - 1.0f) : mapZoomer);
        a3.q = true;
        this.a.a(a3);
    }

    public void a(MyLocationStyle myLocationStyle) {
        if (d() != null) {
            d().a(myLocationStyle);
        }
    }

    public void a(boolean z2) {
        if (z2) {
            this.b.setVisibility(0);
        } else {
            this.b.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2, CameraPosition cameraPosition) {
        if (this.A != null && this.R.a() && isEnabled()) {
            if (cameraPosition == null) {
                try {
                    cameraPosition = l();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
            this.A.onCameraChangeFinish(cameraPosition);
        }
    }

    public boolean a(String str) {
        e(false);
        return this.d.b(str);
    }

    public int b() {
        return this.f;
    }

    public void b(double d2, double d3, IPoint iPoint) {
        IPoint iPoint2 = new IPoint();
        FPoint fPoint = new FPoint();
        MapProjection.lonlat2Geo(d3, d2, iPoint2);
        this.k.geo2Map(iPoint2.x, iPoint2.y, fPoint);
        this.k.map2Win(fPoint.x, fPoint.y, iPoint);
        iPoint.y = getHeight() - iPoint.y;
    }

    public void b(int i2) {
        if (this.r != null) {
            this.r.a(i2);
            this.r.invalidate();
            if (this.u.getVisibility() == 0) {
                this.u.invalidate();
            }
        }
    }

    public void b(int i2, int i3, DPoint dPoint) {
        MapProjection.geo2LonLat(i2, i3, dPoint);
    }

    public void b(j jVar) {
        a(jVar, (AMap.CancelableCallback) null);
    }

    public void b(boolean z2) {
        if (z2) {
            this.s.setVisibility(0);
        } else {
            this.s.setVisibility(8);
        }
    }

    public boolean b(s sVar) {
        if (this.G == null || this.F == null) {
            return false;
        }
        return this.G.getId().equals(sVar.d());
    }

    public boolean b(String str) {
        s sVar;
        try {
            sVar = this.q.a(str);
        } catch (RemoteException e2) {
            e2.printStackTrace();
            sVar = null;
        }
        if (sVar == null) {
            return false;
        }
        e(false);
        return this.q.b(sVar);
    }

    public MapProjection c() {
        if (this.k == null) {
            this.k = this.h.getMapstate();
        }
        return this.k;
    }

    public void c(boolean z2) {
        if (z2) {
            float mapAngle = this.k.getMapAngle();
            float cameraHeaderAngle = this.k.getCameraHeaderAngle();
            if (mapAngle != BitmapDescriptorFactory.HUE_RED || cameraHeaderAngle != BitmapDescriptorFactory.HUE_RED) {
                this.t.setVisibility(0);
                this.t.b();
                return;
            }
            return;
        }
        this.t.setVisibility(8);
    }

    public ah d() {
        return this.P;
    }

    public void d(boolean z2) {
        if (z2) {
            this.u.setVisibility(0);
            g();
            return;
        }
        this.u.a(PoiTypeDef.All);
        this.u.a(0);
        this.u.setVisibility(8);
    }

    public void e() {
        try {
            this.b.a();
            this.u.a();
            this.r.a();
            this.s.a();
            this.t.a();
            this.v.a();
            this.d.b();
            this.q.e();
            if (h.d != null) {
                h.d.disconnect();
            }
            if (this.ax != null) {
                this.ax.interrupt();
            }
            if (this.j != null) {
                this.j.OnMapDestory(this.h);
            }
            if (this.X != null) {
                this.X.setCallback(null);
            }
            this.p.removeAllViews();
            y();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public synchronized void e(boolean z2) {
        if (!z2) {
            this.al = false;
            this.am.removeCallbacks(this.an);
            this.ak = false;
        } else if (!this.ak && !this.al) {
            this.al = true;
            this.am.postDelayed(this.an, 6000);
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.e.obtainMessage(14).sendToTarget();
    }

    public void f(boolean z2) {
        this.ao = z2;
        this.a.a(new ac(2).a(z2));
    }

    /* access modifiers changed from: package-private */
    public void g() {
        this.e.sendEmptyMessage(15);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g(boolean r4) {
        /*
            r3 = this;
            r2 = 0
            com.amap.api.maps.LocationSource r0 = r3.J
            if (r0 == 0) goto L_0x0026
            if (r4 == 0) goto L_0x0021
            com.amap.api.maps.LocationSource r0 = r3.J
            com.amap.api.a.g r1 = r3.N
            r0.activate(r1)
            com.amap.api.a.aa r0 = r3.s
            r1 = 1
            r0.a(r1)
        L_0x0014:
            if (r4 == 0) goto L_0x002c
            com.amap.api.a.aa r0 = r3.s
            r0.setVisibility(r2)
        L_0x001b:
            r3.L = r4
            r3.e(r2)
            return
        L_0x0021:
            com.amap.api.maps.LocationSource r0 = r3.J
            r0.deactivate()
        L_0x0026:
            com.amap.api.a.aa r0 = r3.s
            r0.a(r2)
            goto L_0x0014
        L_0x002c:
            com.amap.api.a.aa r0 = r3.s
            r1 = 8
            r0.setVisibility(r1)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.a.b.g(boolean):void");
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.u != null) {
            int width = getWidth();
            DPoint dPoint = new DPoint();
            DPoint dPoint2 = new DPoint();
            a(0, 0, dPoint);
            a(width, 0, dPoint2);
            double a2 = g.a(new LatLng(dPoint.y, dPoint.x), new LatLng(dPoint2.y, dPoint2.x));
            int mapZoomer = (int) this.k.getMapZoomer();
            String b2 = g.b(this.Z[mapZoomer]);
            this.u.a((int) (((double) (width * this.Z[mapZoomer])) / a2));
            this.u.a(b2);
            this.u.invalidate();
        }
    }

    public int i() {
        return this.K.width();
    }

    public int j() {
        return this.K.height();
    }

    public void k() {
        if (this.F != null && this.G != null) {
            af.a aVar = (af.a) this.F.getLayoutParams();
            if (aVar != null) {
                aVar.b = this.G.getPosition();
            }
            this.p.a();
            e(false);
        }
    }

    public CameraPosition l() {
        return H();
    }

    public float m() {
        return 20.0f;
    }

    public float n() {
        return 4.0f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.api.a.b.a(boolean, com.amap.api.maps.model.CameraPosition):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.amap.api.a.b.a(com.amap.api.a.b, int):int
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.AMap$CancelableCallback):com.amap.api.maps.AMap$CancelableCallback
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.AMap$onMapPrintScreenListener):com.amap.api.maps.AMap$onMapPrintScreenListener
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.model.LatLngBounds):com.amap.api.maps.model.LatLngBounds
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.model.Marker):com.amap.api.maps.model.Marker
      com.amap.api.a.b.a(com.amap.api.a.b, boolean):boolean
      com.amap.api.a.b.a(com.amap.api.a.b, float[]):float[]
      com.amap.api.a.b.a(com.amap.api.a.j, com.amap.api.maps.AMap$CancelableCallback):void
      com.amap.api.a.p.a(com.amap.api.a.j, com.amap.api.maps.AMap$CancelableCallback):void
      com.amap.api.a.b.a(boolean, com.amap.api.maps.model.CameraPosition):void */
    public void o() {
        if (!this.R.a()) {
            this.R.a(true);
            a(true, (CameraPosition) null);
            if (this.U != null) {
                this.U.onCancel();
            }
            this.U = null;
        }
        e(false);
    }

    public void onDrawFrame(GL10 gl10) {
        if (this.aa) {
            this.e.obtainMessage(16, a(0, 0, getWidth(), getHeight(), gl10)).sendToTarget();
            this.aa = false;
        }
        if (!this.ak || this.al) {
            this.h.setGL(gl10);
            this.h.drawFrame(gl10);
            this.v.a(gl10);
            this.d.a(gl10);
            this.q.a(gl10);
            if (!this.R.a()) {
                this.e.sendEmptyMessage(13);
            }
            if (!this.M) {
                this.e.sendEmptyMessage(11);
                this.M = true;
            }
        }
    }

    public void onPause() {
        if (this.ag) {
            this.ac.unregisterListener(this.aj);
        }
        if (this.j != null) {
            this.j.onPause();
        }
        if (this.v != null) {
            this.v.c();
        }
    }

    public void onResume() {
        if (this.j != null) {
            this.j.onResume();
            e(false);
        }
        if (this.v != null) {
            this.v.d();
        }
        if (this.ag) {
            this.ac.registerListener(this.aj, this.ae, 3);
            this.ac.registerListener(this.aj, this.af, 3);
        }
    }

    public void onSurfaceChanged(GL10 gl10, int i2, int i3) {
        this.K = new Rect(0, 0, i2, i3);
        this.h.setGL(gl10);
        this.h.surfaceChange(gl10, i2, i3);
        int i4 = this.i.getResources().getDisplayMetrics().densityDpi;
        this.h.setInternaltexture(g.a(this.i, "bk.pvr"), 1);
        if (i4 >= 240) {
            this.h.setInternaltexture(g.a(g.a(this.i, "icn_h.data")), 0);
        } else {
            this.h.setInternaltexture(g.a(g.a(this.i, "icn.data")), 0);
        }
        this.h.setInternaltexture(g.a(this.i, "roadarrow.pvr"), 2);
        this.h.setInternaltexture(g.a(this.i, "LineRound.pvr"), 3);
        if (i4 <= 120) {
            this.h.setParameter(2051, 100, 50, 1, 0);
        } else if (i4 <= 160) {
            if (Math.max(i2, i3) <= 480) {
                this.h.setParameter(2051, 120, 60, 1, 0);
            } else {
                this.h.setParameter(2051, 100, 80, 1, 0);
            }
        } else if (i4 <= 240) {
            this.h.setParameter(2051, 100, 90, 2, 0);
        } else if (i4 <= 320) {
            this.h.setParameter(2051, 90, 100, 2, 0);
        } else if (i4 <= 480) {
            this.h.setParameter(2051, 70, 150, 2, 0);
        } else {
            this.h.setParameter(2051, 60, 180, 2, 0);
        }
        this.h.setParameter(1011, 0, 0, 0, 0);
        this.h.setParameter(1021, 1, 0, 0, 0);
        this.h.setParameter(1022, 1, 0, 0, 0);
        this.h.setParameter(1023, 1, 0, 0, 0);
        e(false);
        this.e.obtainMessage(19).sendToTarget();
    }

    public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        this.h.setGL(gl10);
        this.h.setStyleData(g.a(this.i, "style_v3.data"), 0);
        this.h.setStyleData(g.a(this.i, "style_sv3.data"), 1);
        this.h.surfaceCreate(gl10);
        Bitmap a2 = g.a("lineTexture.png");
        this.f = g.a(gl10, a2);
        a2.recycle();
        if (!this.c) {
            try {
                this.ax.setName("AuthThread");
                this.ax.start();
                this.c = true;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        e(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.api.a.b.a(boolean, com.amap.api.maps.model.CameraPosition):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.amap.api.a.b.a(com.amap.api.a.b, int):int
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.AMap$CancelableCallback):com.amap.api.maps.AMap$CancelableCallback
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.AMap$onMapPrintScreenListener):com.amap.api.maps.AMap$onMapPrintScreenListener
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.model.LatLngBounds):com.amap.api.maps.model.LatLngBounds
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.model.Marker):com.amap.api.maps.model.Marker
      com.amap.api.a.b.a(com.amap.api.a.b, boolean):boolean
      com.amap.api.a.b.a(com.amap.api.a.b, float[]):float[]
      com.amap.api.a.b.a(com.amap.api.a.j, com.amap.api.maps.AMap$CancelableCallback):void
      com.amap.api.a.p.a(com.amap.api.a.j, com.amap.api.maps.AMap$CancelableCallback):void
      com.amap.api.a.b.a(boolean, com.amap.api.maps.model.CameraPosition):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        e(false);
        this.l.onTouchEvent(motionEvent);
        this.m.onTouchEvent(motionEvent);
        this.n.a(motionEvent);
        this.O.a(motionEvent);
        super.onTouchEvent(motionEvent);
        if (motionEvent.getAction() == 2) {
            a(motionEvent);
        }
        if (motionEvent.getAction() == 1) {
            I();
            if (this.at) {
                Log.d("onTouchEvent", "mIsScrolling");
                a(false, (CameraPosition) null);
            }
            this.at = false;
        }
        e(false);
        return true;
    }

    public void p() {
        try {
            y();
            this.d.a();
            this.v.a();
            this.q.c();
            e(false);
        } catch (Exception e2) {
            Log.d("amapApi", "AMapDelegateImpGLSurfaceView clear erro" + e2.getMessage());
            e2.printStackTrace();
        }
    }

    public int q() {
        return this.g;
    }

    public boolean r() {
        return this.ao;
    }

    public boolean s() {
        return this.L;
    }

    public void setZOrderOnTop(boolean z2) {
        super.setZOrderOnTop(z2);
    }

    public Location t() {
        if (this.J != null) {
            return this.N.a;
        }
        return null;
    }

    public z u() {
        return this.I;
    }

    public w v() {
        return this.H;
    }

    public AMap.OnCameraChangeListener w() {
        return this.A;
    }

    public View x() {
        return this.p;
    }

    public void y() {
        if (this.F != null) {
            this.F.clearFocus();
            this.p.removeView(this.F);
            Drawable background = this.F.getBackground();
            if (background != null) {
                background.setCallback(null);
            }
            this.F = null;
        }
        this.G = null;
    }

    public float z() {
        return this.k.getMapZoomer();
    }
}
