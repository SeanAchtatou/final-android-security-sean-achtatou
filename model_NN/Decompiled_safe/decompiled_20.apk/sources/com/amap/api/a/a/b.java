package com.amap.api.a.a;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.util.FloatMath;
import android.view.MotionEvent;
import java.lang.reflect.Method;

public class b {
    protected final Context a;
    protected Method b;
    protected Method c;
    protected Method d;
    protected Method e;
    private boolean f = false;
    private final a g;
    private long h = 0;
    private boolean i = false;

    public interface a {
        void a(float f, float f2, float f3, float f4, float f5);

        boolean a(MotionEvent motionEvent, float f, float f2, float f3, float f4);
    }

    public b(Context context, a aVar) {
        this.a = context;
        this.g = aVar;
        d();
    }

    private void d() {
        try {
            this.b = MotionEvent.class.getMethod("getPointerCount", new Class[0]);
            this.e = MotionEvent.class.getMethod("getPointerId", Integer.TYPE);
            this.c = MotionEvent.class.getMethod("getX", Integer.TYPE);
            this.d = MotionEvent.class.getMethod("getY", Integer.TYPE);
            this.f = true;
        } catch (Exception e2) {
            this.f = false;
            e2.printStackTrace();
        }
    }

    public boolean a() {
        return this.f;
    }

    public boolean a(MotionEvent motionEvent) {
        if (!a()) {
            return false;
        }
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        try {
            if (((Integer) this.b.invoke(motionEvent, new Object[0])).intValue() < 2) {
                return false;
            }
            Float f2 = (Float) this.c.invoke(motionEvent, 0);
            Float f3 = (Float) this.c.invoke(motionEvent, 1);
            Float f4 = (Float) this.d.invoke(motionEvent, 0);
            Float f5 = (Float) this.d.invoke(motionEvent, 1);
            float sqrt = FloatMath.sqrt(((f3.floatValue() - f2.floatValue()) * (f3.floatValue() - f2.floatValue())) + ((f5.floatValue() - f4.floatValue()) * (f5.floatValue() - f4.floatValue())));
            if (action == 5) {
                this.g.a(sqrt, f2.floatValue(), f4.floatValue(), f3.floatValue(), f5.floatValue());
                this.i = true;
                return true;
            } else if (action == 6) {
                this.h = motionEvent.getEventTime();
                if (this.i) {
                    this.i = false;
                }
                return false;
            } else {
                if (this.i && action == 2) {
                    return this.g.a(motionEvent, f2.floatValue(), f4.floatValue(), f3.floatValue(), f5.floatValue());
                }
                return false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean b() {
        return this.i;
    }

    public long c() {
        return this.h;
    }
}
