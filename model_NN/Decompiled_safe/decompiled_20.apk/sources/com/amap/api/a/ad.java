package com.amap.api.a;

import java.util.concurrent.CopyOnWriteArrayList;

class ad {
    b a;
    private CopyOnWriteArrayList<j> b = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ac> c = new CopyOnWriteArrayList<>();

    public ad(b bVar) {
        this.a = bVar;
    }

    public ac a() {
        if (b() == 0) {
            return null;
        }
        ac acVar = this.c.get(0);
        this.c.remove(acVar);
        return acVar;
    }

    public synchronized void a(ac acVar) {
        this.a.e(false);
        this.c.add(acVar);
        this.a.e(false);
    }

    public void a(j jVar) {
        this.a.e(false);
        this.b.add(jVar);
        this.a.e(false);
    }

    public int b() {
        return this.c.size();
    }

    public j c() {
        if (d() == 0) {
            return null;
        }
        j jVar = this.b.get(0);
        this.b.remove(jVar);
        this.a.e(false);
        return jVar;
    }

    public int d() {
        return this.b.size();
    }
}
