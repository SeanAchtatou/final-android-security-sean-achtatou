package com.amap.api.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.amap.api.a.b.g;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.autonavi.amap.mapcore.MapProjection;

class l extends LinearLayout {
    Bitmap a;
    Bitmap b;
    ImageView c;
    ad d;
    p e;

    public l(Context context, ad adVar, p pVar) {
        super(context);
        this.d = adVar;
        this.e = pVar;
        try {
            Bitmap a2 = g.a("maps_dav_compass_needle_large.png");
            this.b = g.a(a2, m.a * 0.8f);
            Bitmap a3 = g.a(a2, m.a * 0.7f);
            this.a = Bitmap.createBitmap(this.b.getWidth(), this.b.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(this.a);
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setFilterBitmap(true);
            canvas.drawBitmap(a3, (float) ((this.b.getWidth() - a3.getWidth()) / 2), (float) ((this.b.getHeight() - a3.getHeight()) / 2), paint);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.c = new ImageView(context);
        this.c.setScaleType(ImageView.ScaleType.MATRIX);
        this.c.setImageBitmap(this.a);
        b();
        this.c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
        this.c.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    l.this.c.setImageBitmap(l.this.b);
                    return false;
                } else if (motionEvent.getAction() != 1) {
                    return false;
                } else {
                    try {
                        l.this.c.setImageBitmap(l.this.a);
                        CameraPosition l = l.this.e.l();
                        l.this.e.b(j.a(new CameraPosition(l.target, l.zoom, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED)));
                        return false;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            }
        });
        addView(this.c);
    }

    public void a() {
        try {
            this.a.recycle();
            this.b.recycle();
            this.a = null;
            this.b = null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void b() {
        MapProjection c2 = this.e.c();
        float mapAngle = c2.getMapAngle();
        float cameraHeaderAngle = c2.getCameraHeaderAngle();
        Matrix matrix = new Matrix();
        matrix.postRotate(-mapAngle, (float) (this.c.getDrawable().getBounds().width() / 2), (float) (this.c.getDrawable().getBounds().height() / 2));
        matrix.postScale(1.0f, (float) Math.cos((((double) cameraHeaderAngle) * 3.141592653589793d) / 180.0d), (float) (this.c.getDrawable().getBounds().width() / 2), (float) (this.c.getDrawable().getBounds().height() / 2));
        this.c.setImageMatrix(matrix);
    }
}
