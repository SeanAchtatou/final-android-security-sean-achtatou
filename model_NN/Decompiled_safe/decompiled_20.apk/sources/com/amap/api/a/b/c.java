package com.amap.api.a.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;

public class c {
    public static String a = PoiTypeDef.All;
    public static int b = 2048;
    private static c c = null;
    private static String d = null;
    private static Context e = null;
    private static TelephonyManager f;
    private static ConnectivityManager g;
    private static String h;
    private static String i;

    private c() {
    }

    public static c a(Context context) {
        if (c == null) {
            c = new c();
            e = context;
            f = (TelephonyManager) context.getSystemService("phone");
            g = (ConnectivityManager) e.getSystemService("connectivity");
            h = e.getApplicationContext().getPackageName();
            i = e();
            a = b(e);
        }
        return c;
    }

    public static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[b];
        while (true) {
            int read = inputStream.read(bArr, 0, b);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static String b(Context context) {
        if (a == null || a.equals(PoiTypeDef.All)) {
            try {
                a = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("com.amap.api.v2.apikey");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return a;
    }

    public static Proxy c(Context context) {
        Proxy proxy;
        int defaultPort;
        String str;
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.getType() == 1) {
                    String host = android.net.Proxy.getHost(context);
                    defaultPort = android.net.Proxy.getPort(context);
                    str = host;
                } else {
                    String defaultHost = android.net.Proxy.getDefaultHost();
                    defaultPort = android.net.Proxy.getDefaultPort();
                    str = defaultHost;
                }
                if (str != null) {
                    proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, defaultPort));
                    return proxy;
                }
            }
            proxy = null;
            return proxy;
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            return null;
        } catch (Exception e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private static String e() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) e.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int i2 = displayMetrics.widthPixels;
        int i3 = displayMetrics.heightPixels;
        String str = i3 > i2 ? i2 + "*" + i3 : i3 + "*" + i2;
        i = str;
        return str;
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append("ia=1&");
        if (a != null && a.length() > 0) {
            sb.append("key=");
            sb.append(a);
            sb.append("&");
        }
        sb.append("ct=android");
        String deviceId = f.getDeviceId();
        String subscriberId = f.getSubscriberId();
        sb.append("&ime=" + deviceId);
        sb.append("&sim=" + subscriberId);
        sb.append("&pkg=" + h);
        sb.append("&mod=");
        sb.append(c());
        sb.append("&sv=");
        sb.append(b());
        sb.append("&nt=");
        sb.append(d());
        String networkOperatorName = f.getNetworkOperatorName();
        sb.append("&np=");
        sb.append(networkOperatorName);
        sb.append("&ctm=" + System.currentTimeMillis());
        sb.append("&re=" + i);
        sb.append("&av=V2.0.4");
        sb.append("&pro=map");
        return sb.toString();
    }

    public String b() {
        return Build.VERSION.RELEASE;
    }

    public String c() {
        return Build.MODEL;
    }

    public String d() {
        NetworkInfo activeNetworkInfo;
        return (e.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0 || g == null || (activeNetworkInfo = g.getActiveNetworkInfo()) == null) ? PoiTypeDef.All : activeNetworkInfo.getTypeName();
    }
}
