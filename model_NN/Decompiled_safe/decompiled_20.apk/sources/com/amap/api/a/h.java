package com.amap.api.a;

import android.util.Log;
import com.amap.api.a.b.a;
import com.amap.api.a.b.c;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.search.core.AMapException;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.util.zip.GZIPInputStream;
import org.json.JSONObject;

public class h {
    public static int a = -1;
    static String b = PoiTypeDef.All;
    public static HttpURLConnection d = null;
    int c = 0;

    protected static InputStream a(HttpURLConnection httpURLConnection) {
        try {
            PushbackInputStream pushbackInputStream = new PushbackInputStream(httpURLConnection.getInputStream(), 2);
            byte[] bArr = new byte[2];
            pushbackInputStream.read(bArr);
            pushbackInputStream.unread(bArr);
            return (bArr[0] == 31 && bArr[1] == -117) ? new GZIPInputStream(pushbackInputStream) : pushbackInputStream;
        } catch (ProtocolException e) {
            throw new a(AMapException.ERROR_PROTOCOL);
        } catch (UnknownHostException e2) {
            throw new a(AMapException.ERROR_UNKNOW_HOST);
        } catch (UnknownServiceException e3) {
            throw new a(AMapException.ERROR_UNKNOW_SERVICE);
        } catch (IOException e4) {
            throw new a(AMapException.ERROR_IO);
        }
    }

    private static String a() {
        return "http://restapi.amap.com/log/init";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
        throw new com.amap.api.a.b.a(com.amap.api.search.core.AMapException.ERROR_IO);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x006f, code lost:
        throw new com.amap.api.a.b.a(com.amap.api.search.core.AMapException.ERROR_IO);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:7:0x0020, B:29:0x0057] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:9:0x0023=Splitter:B:9:0x0023, B:31:0x005a=Splitter:B:31:0x005a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean a(android.content.Context r5) {
        /*
            r1 = 0
            java.lang.Class<com.amap.api.a.h> r2 = com.amap.api.a.h.class
            monitor-enter(r2)
            byte[] r0 = b()     // Catch:{ all -> 0x003a }
            java.lang.String r3 = a()     // Catch:{ a -> 0x003d }
            java.net.Proxy r4 = com.amap.api.a.b.c.c(r5)     // Catch:{ a -> 0x003d }
            java.net.HttpURLConnection r0 = com.amap.api.a.b.d.a(r3, r0, r4)     // Catch:{ a -> 0x003d }
            com.amap.api.a.h.d = r0     // Catch:{ a -> 0x003d }
            java.io.InputStream r1 = a(r0)     // Catch:{ a -> 0x003d }
            boolean r0 = a(r1)     // Catch:{ a -> 0x003d }
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x0031 }
        L_0x0023:
            java.net.HttpURLConnection r1 = com.amap.api.a.h.d     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x002f
            java.net.HttpURLConnection r1 = com.amap.api.a.h.d     // Catch:{ all -> 0x003a }
            r1.disconnect()     // Catch:{ all -> 0x003a }
            r1 = 0
            com.amap.api.a.h.d = r1     // Catch:{ all -> 0x003a }
        L_0x002f:
            monitor-exit(r2)
            return r0
        L_0x0031:
            r0 = move-exception
            com.amap.api.a.b.a r0 = new com.amap.api.a.b.a     // Catch:{ all -> 0x003a }
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)     // Catch:{ all -> 0x003a }
            throw r0     // Catch:{ all -> 0x003a }
        L_0x003a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x003d:
            r0 = move-exception
            java.lang.String r3 = "AuthFailure"
            java.lang.String r4 = r0.a()     // Catch:{ all -> 0x0054 }
            android.util.Log.i(r3, r4)     // Catch:{ all -> 0x0054 }
            r0.printStackTrace()     // Catch:{ all -> 0x0054 }
            com.amap.api.a.b.a r3 = new com.amap.api.a.b.a     // Catch:{ all -> 0x0054 }
            java.lang.String r0 = r0.a()     // Catch:{ all -> 0x0054 }
            r3.<init>(r0)     // Catch:{ all -> 0x0054 }
            throw r3     // Catch:{ all -> 0x0054 }
        L_0x0054:
            r0 = move-exception
            if (r1 == 0) goto L_0x005a
            r1.close()     // Catch:{ IOException -> 0x0067 }
        L_0x005a:
            java.net.HttpURLConnection r1 = com.amap.api.a.h.d     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0066
            java.net.HttpURLConnection r1 = com.amap.api.a.h.d     // Catch:{ all -> 0x003a }
            r1.disconnect()     // Catch:{ all -> 0x003a }
            r1 = 0
            com.amap.api.a.h.d = r1     // Catch:{ all -> 0x003a }
        L_0x0066:
            throw r0     // Catch:{ all -> 0x003a }
        L_0x0067:
            r0 = move-exception
            com.amap.api.a.b.a r0 = new com.amap.api.a.b.a     // Catch:{ all -> 0x003a }
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)     // Catch:{ all -> 0x003a }
            throw r0     // Catch:{ all -> 0x003a }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.a.h.a(android.content.Context):boolean");
    }

    private static boolean a(InputStream inputStream) {
        try {
            JSONObject jSONObject = new JSONObject(new String(c.a(inputStream)));
            if (jSONObject.has(LocationManagerProxy.KEY_STATUS_CHANGED)) {
                int i = jSONObject.getInt(LocationManagerProxy.KEY_STATUS_CHANGED);
                if (i == 1) {
                    a = 1;
                } else if (i == 0) {
                    a = 0;
                }
            }
            if (jSONObject.has("info")) {
                b = jSONObject.getString("info");
            }
            if (a != 0) {
                return a == 1;
            }
            Log.i("AuthFailure", b);
            throw new a(AMapException.ERROR_FAILURE_AUTH);
        } catch (Exception e) {
            e.printStackTrace();
            return a == 1;
        } catch (Throwable th) {
            return a == 1;
        }
    }

    private static byte[] b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("resType=json&encode=UTF-8");
        return stringBuffer.toString().getBytes();
    }
}
