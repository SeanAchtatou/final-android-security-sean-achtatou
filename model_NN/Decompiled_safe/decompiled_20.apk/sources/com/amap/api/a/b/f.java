package com.amap.api.a.b;

import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.autonavi.amap.mapcore.FPoint;
import java.util.List;

public class f {
    static float a(FPoint[] fPointArr) {
        int length = fPointArr.length;
        float f = BitmapDescriptorFactory.HUE_RED;
        int i = length - 1;
        for (int i2 = 0; i2 < length; i2++) {
            f += (fPointArr[i].x * fPointArr[i2].y) - (fPointArr[i].y * fPointArr[i2].x);
            i = i2;
        }
        return 0.5f * f;
    }

    static boolean a(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        return ((f5 - f3) * (f8 - f4)) - ((f6 - f4) * (f7 - f3)) >= BitmapDescriptorFactory.HUE_RED && ((f - f5) * (f8 - f6)) - ((f2 - f6) * (f7 - f5)) >= BitmapDescriptorFactory.HUE_RED && ((f3 - f) * (f8 - f2)) - ((f4 - f2) * (f7 - f)) >= BitmapDescriptorFactory.HUE_RED;
    }

    static boolean a(FPoint[] fPointArr, int i, int i2, int i3, int i4, int[] iArr) {
        float f = fPointArr[iArr[i]].x;
        float f2 = fPointArr[iArr[i]].y;
        float f3 = fPointArr[iArr[i2]].x;
        float f4 = fPointArr[iArr[i2]].y;
        float f5 = fPointArr[iArr[i3]].x;
        float f6 = fPointArr[iArr[i3]].y;
        if (1.0E-10f > ((f3 - f) * (f6 - f2)) - ((f4 - f2) * (f5 - f))) {
            return false;
        }
        for (int i5 = 0; i5 < i4; i5++) {
            if (i5 != i && i5 != i2 && i5 != i3 && a(f, f2, f3, f4, f5, f6, fPointArr[iArr[i5]].x, fPointArr[iArr[i5]].y)) {
                return false;
            }
        }
        return true;
    }

    public static boolean a(FPoint[] fPointArr, List<FPoint> list) {
        int length = fPointArr.length;
        if (length < 3) {
            return false;
        }
        int[] iArr = new int[length];
        if (BitmapDescriptorFactory.HUE_RED < a(fPointArr)) {
            for (int i = 0; i < length; i++) {
                iArr[i] = i;
            }
        } else {
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = (length - 1) - i2;
            }
        }
        int i3 = length * 2;
        int i4 = length - 1;
        while (length > 2) {
            int i5 = i3 - 1;
            if (i3 <= 0) {
                return false;
            }
            if (length <= i4) {
                i4 = 0;
            }
            int i6 = i4 + 1;
            if (length <= i6) {
                i6 = 0;
            }
            int i7 = i6 + 1;
            if (length <= i7) {
                i7 = 0;
            }
            if (a(fPointArr, i4, i6, i7, length, iArr)) {
                int i8 = iArr[i4];
                int i9 = iArr[i6];
                int i10 = iArr[i7];
                list.add(fPointArr[i8]);
                list.add(fPointArr[i9]);
                list.add(fPointArr[i10]);
                int i11 = i6;
                for (int i12 = i6 + 1; i12 < length; i12++) {
                    iArr[i11] = iArr[i12];
                    i11++;
                }
                length--;
                i3 = length * 2;
            } else {
                i3 = i5;
            }
            i4 = i6;
        }
        return true;
    }
}
