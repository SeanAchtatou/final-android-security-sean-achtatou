package com.amap.api.a;

import android.graphics.Point;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.VisibleRegion;
import com.autonavi.amap.mapcore.DPoint;
import com.autonavi.amap.mapcore.IPoint;

class al implements w {
    private p a;

    public al(p pVar) {
        this.a = pVar;
    }

    public Point a(LatLng latLng) {
        IPoint iPoint = new IPoint();
        this.a.b(latLng.latitude, latLng.longitude, iPoint);
        return new Point(iPoint.x, iPoint.y);
    }

    public LatLng a(Point point) {
        DPoint dPoint = new DPoint();
        this.a.a(point.x, point.y, dPoint);
        return new LatLng(dPoint.y, dPoint.x);
    }

    public VisibleRegion a() {
        int i = this.a.i();
        int j = this.a.j();
        LatLng a2 = a(new Point(0, 0));
        LatLng a3 = a(new Point(i, 0));
        LatLng a4 = a(new Point(0, j));
        LatLng a5 = a(new Point(i, j));
        return new VisibleRegion(a4, a5, a2, a3, LatLngBounds.builder().include(a4).include(a5).include(a2).include(a3).build());
    }
}
