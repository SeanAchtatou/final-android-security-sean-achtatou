package com.amap.api.a;

import android.graphics.Rect;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.LatLng;
import com.autonavi.amap.mapcore.FPoint;
import javax.microedition.khronos.opengles.GL10;

public interface s {
    void a(float f, float f2);

    void a(BitmapDescriptor bitmapDescriptor);

    void a(LatLng latLng);

    void a(Object obj);

    void a(String str);

    void a(GL10 gl10, p pVar);

    void a(boolean z);

    boolean a();

    boolean a(s sVar);

    Rect b();

    void b(String str);

    void b(boolean z);

    LatLng c();

    String d();

    int e();

    FPoint f();

    String g();

    String h();

    BitmapDescriptor i();

    boolean j();

    void k();

    void l();

    boolean m();

    boolean n();

    void o();

    int p();

    Object q();
}
