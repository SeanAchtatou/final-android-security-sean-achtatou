package com.amap.api.a;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.TimeUtils;
import android.widget.Toast;
import com.amap.api.maps.AMap;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.search.route.Route;
import com.autonavi.amap.mapcore.DPoint;
import com.autonavi.amap.mapcore.FPoint;
import com.autonavi.amap.mapcore.IPoint;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.util.LangUtils;

class f extends Handler {
    final /* synthetic */ b a;

    f(b bVar) {
        this.a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.amap.api.a.b.a(boolean, com.amap.api.maps.model.CameraPosition):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.amap.api.a.b.a(com.amap.api.a.b, int):int
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.AMap$CancelableCallback):com.amap.api.maps.AMap$CancelableCallback
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.AMap$onMapPrintScreenListener):com.amap.api.maps.AMap$onMapPrintScreenListener
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.model.LatLngBounds):com.amap.api.maps.model.LatLngBounds
      com.amap.api.a.b.a(com.amap.api.a.b, com.amap.api.maps.model.Marker):com.amap.api.maps.model.Marker
      com.amap.api.a.b.a(com.amap.api.a.b, boolean):boolean
      com.amap.api.a.b.a(com.amap.api.a.b, float[]):float[]
      com.amap.api.a.b.a(com.amap.api.a.j, com.amap.api.maps.AMap$CancelableCallback):void
      com.amap.api.a.p.a(com.amap.api.a.j, com.amap.api.maps.AMap$CancelableCallback):void
      com.amap.api.a.b.a(boolean, com.amap.api.maps.model.CameraPosition):void */
    public void handleMessage(Message message) {
        Bitmap drawingCache;
        if (message != null) {
            this.a.e(false);
            switch (message.what) {
                case 2:
                    if (this.a.Q != null) {
                        Toast.makeText(this.a.i, h.b, 0).show();
                        break;
                    } else {
                        return;
                    }
                case 10:
                    CameraPosition cameraPosition = (CameraPosition) message.obj;
                    if (!(cameraPosition == null || this.a.A == null)) {
                        this.a.A.onCameraChange(cameraPosition);
                        break;
                    }
                case Route.DrivingSaveMoney /*11*/:
                    if (this.a.z != null) {
                        this.a.z.onMapLoaded();
                        break;
                    }
                    break;
                case Route.DrivingLeastDistance /*12*/:
                    j jVar = (j) message.obj;
                    if (jVar != null) {
                        this.a.a(jVar.i, jVar.k, jVar.l, jVar.j);
                        break;
                    }
                    break;
                case Route.DrivingNoFastRoad /*13*/:
                    if (this.a.R != null && this.a.R.h()) {
                        switch (this.a.R.j()) {
                            case 2:
                                j a2 = j.a(this.a.R.d());
                                if (this.a.R.a()) {
                                    a2.q = true;
                                }
                                this.a.a.a(a2);
                                break;
                            case 3:
                                j a3 = j.a(new IPoint(this.a.R.b(), this.a.R.c()), this.a.R.d(), this.a.R.e(), this.a.R.f());
                                if (this.a.R.a()) {
                                    a3.q = true;
                                }
                                this.a.a.a(a3);
                                break;
                            default:
                                int b = this.a.R.b() - this.a.S;
                                int c = this.a.R.c() - this.a.T;
                                int unused = this.a.S = this.a.R.b();
                                int unused2 = this.a.T = this.a.R.c();
                                FPoint fPoint = new FPoint((float) (b + (this.a.getWidth() / 2)), (float) (c + (this.a.getHeight() / 2)));
                                FPoint fPoint2 = new FPoint();
                                this.a.k.win2Map((int) fPoint.x, (int) fPoint.y, fPoint2);
                                IPoint iPoint = new IPoint();
                                this.a.k.map2Geo(fPoint2.x, fPoint2.y, iPoint);
                                j a4 = j.a(iPoint);
                                if (this.a.R.a()) {
                                    a4.q = true;
                                }
                                this.a.a.a(a4);
                                break;
                        }
                    }
                case 14:
                    if (this.a.t != null) {
                        float mapAngle = this.a.k.getMapAngle();
                        float cameraHeaderAngle = this.a.k.getCameraHeaderAngle();
                        if (!(this.a.t.getVisibility() == 0 || (mapAngle == BitmapDescriptorFactory.HUE_RED && cameraHeaderAngle == BitmapDescriptorFactory.HUE_RED))) {
                            this.a.t.setVisibility(0);
                        }
                        this.a.t.b();
                        if (mapAngle == BitmapDescriptorFactory.HUE_RED && cameraHeaderAngle == BitmapDescriptorFactory.HUE_RED) {
                            this.a.a(500L);
                            break;
                        }
                    } else {
                        return;
                    }
                case 15:
                    this.a.h();
                    break;
                case 16:
                    if (this.a.ab != null) {
                        Bitmap bitmap = (Bitmap) message.obj;
                        if (bitmap != null) {
                            Canvas canvas = new Canvas(bitmap);
                            if (this.a.r != null) {
                                this.a.r.onDraw(canvas);
                            }
                            if (!(this.a.F == null || this.a.G == null || (drawingCache = this.a.F.getDrawingCache(true)) == null)) {
                                canvas.drawBitmap(drawingCache, (float) this.a.F.getLeft(), (float) this.a.F.getTop(), new Paint());
                            }
                            this.a.ab.onMapPrint(new BitmapDrawable(this.a.getResources(), bitmap));
                        } else {
                            this.a.ab.onMapPrint(null);
                        }
                        AMap.onMapPrintScreenListener unused3 = this.a.ab = (AMap.onMapPrintScreenListener) null;
                        break;
                    }
                    break;
                case LangUtils.HASH_SEED /*17*/:
                    if (!this.a.aq && !this.a.au && !this.a.ap) {
                        this.a.a(true, (CameraPosition) null);
                    }
                    if (this.a.U != null) {
                        boolean unused4 = this.a.V = true;
                        this.a.U.onFinish();
                        boolean unused5 = this.a.V = false;
                    }
                    if (this.a.W) {
                        boolean unused6 = this.a.W = false;
                        break;
                    } else {
                        AMap.CancelableCallback unused7 = this.a.U = (AMap.CancelableCallback) null;
                        break;
                    }
                case 18:
                    int width = this.a.getWidth();
                    int height = this.a.getHeight();
                    if (width > 0 && height > 0) {
                        DPoint dPoint = new DPoint();
                        this.a.a(0, 0, dPoint);
                        LatLng latLng = new LatLng(dPoint.y, dPoint.x);
                        this.a.a(width, 0, dPoint);
                        LatLng latLng2 = new LatLng(dPoint.y, dPoint.x);
                        this.a.a(0, height, dPoint);
                        LatLng latLng3 = new LatLng(dPoint.y, dPoint.x);
                        this.a.a(width, height, dPoint);
                        LatLngBounds unused8 = this.a.ay = LatLngBounds.builder().include(latLng3).include(new LatLng(dPoint.y, dPoint.x)).include(latLng).include(latLng2).build();
                        break;
                    } else {
                        LatLngBounds unused9 = this.a.ay = (LatLngBounds) null;
                        break;
                    }
                case TimeUtils.HUNDRED_DAY_FIELD_LEN:
                    this.a.setBackgroundColor(0);
                    this.a.E();
                    break;
                case MultiThreadedHttpConnectionManager.DEFAULT_MAX_TOTAL_CONNECTIONS /*20*/:
                    if (this.a.R.a() || !(this.a.R.j() == 1 || this.a.v == null)) {
                        this.a.v.a(false);
                    }
                    this.a.v.b();
                    break;
            }
            this.a.e(false);
        }
    }
}
