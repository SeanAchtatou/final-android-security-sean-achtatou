package com.amap.api.a;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.amap.api.maps.model.LatLng;
import com.autonavi.amap.mapcore.IPoint;

class af extends ViewGroup {
    private p a;

    public class a extends ViewGroup.LayoutParams {
        public int a;
        public LatLng b;
        public int c;
        public int d;
        public int e;

        public a(int i, int i2, LatLng latLng, int i3, int i4, int i5) {
            super(i, i2);
            this.a = 1;
            this.b = null;
            this.c = 0;
            this.d = 0;
            this.e = 51;
            this.a = 0;
            this.b = latLng;
            this.c = i3;
            this.d = i4;
            this.e = i5;
        }

        public a(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.a = 1;
            this.b = null;
            this.c = 0;
            this.d = 0;
            this.e = 51;
        }

        public a(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.a = 1;
            this.b = null;
            this.c = 0;
            this.d = 0;
            this.e = 51;
        }
    }

    public af(Context context, p pVar) {
        super(context);
        this.a = pVar;
        setBackgroundColor(-1);
    }

    private void a(View view, int i, int i2, int i3, int i4, int i5) {
        View view2;
        if ((view instanceof ListView) && (view2 = (View) view.getParent()) != null) {
            i = view2.getWidth();
            i2 = view2.getHeight();
        }
        view.measure(i, i2);
        if (i == -2) {
            i = view.getMeasuredWidth();
        } else if (i == -1) {
            i = getMeasuredWidth();
        }
        if (i2 == -2) {
            i2 = view.getMeasuredHeight();
        } else if (i2 == -1) {
            i2 = getMeasuredHeight();
        }
        int i6 = i5 & 7;
        int i7 = i5 & 112;
        if (i6 == 5) {
            i3 -= i;
        } else if (i6 == 1) {
            i3 -= i / 2;
        }
        if (i7 == 80) {
            i4 -= i2;
        } else if (i7 == 16) {
            i4 -= i2 / 2;
        }
        view.layout(i3, i4, i3 + i, i4 + i2);
    }

    private void a(View view, a aVar) {
        a(view, aVar.width, aVar.height, aVar.c, aVar.d, aVar.e);
    }

    private void b(View view, a aVar) {
        if (view instanceof aq) {
            a(view, aVar.width, aVar.height, getWidth() - view.getWidth(), getHeight(), aVar.e);
        } else if (view instanceof aa) {
            a(view, aVar.width, aVar.height, getWidth() - view.getWidth(), view.getHeight(), aVar.e);
        } else if (view instanceof l) {
            a(view, aVar.width, aVar.height, 0, 0, aVar.e);
        } else if (aVar.b != null) {
            IPoint iPoint = new IPoint();
            this.a.b(aVar.b.latitude, aVar.b.longitude, iPoint);
            iPoint.x += aVar.c;
            iPoint.y += aVar.d;
            a(view, aVar.width, aVar.height, iPoint.x, iPoint.y, aVar.e);
        }
    }

    public void a() {
        onLayout(false, 0, 0, 0, 0);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt != null) {
                if (childAt.getLayoutParams() instanceof a) {
                    b(childAt, (a) childAt.getLayoutParams());
                } else {
                    a(childAt, new a(childAt.getLayoutParams()));
                }
            }
        }
    }
}
