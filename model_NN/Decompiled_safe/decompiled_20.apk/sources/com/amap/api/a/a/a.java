package com.amap.api.a.a;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

public abstract class a {
    protected final Context a;
    protected boolean b;
    protected MotionEvent c;
    protected MotionEvent d;
    protected float e;
    protected float f;
    protected long g;

    public a(Context context) {
        this.a = context;
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.c != null) {
            this.c.recycle();
            this.c = null;
        }
        if (this.d != null) {
            this.d.recycle();
            this.d = null;
        }
        this.b = false;
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i, MotionEvent motionEvent);

    public boolean a(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        if (!this.b) {
            a(action, motionEvent);
            return true;
        }
        b(action, motionEvent);
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract void b(int i, MotionEvent motionEvent);

    /* access modifiers changed from: protected */
    public void b(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = this.c;
        if (this.d != null) {
            this.d.recycle();
            this.d = null;
        }
        this.d = MotionEvent.obtain(motionEvent);
        this.g = motionEvent.getEventTime() - motionEvent2.getEventTime();
        this.e = motionEvent.getPressure(c(motionEvent));
        this.f = motionEvent2.getPressure(c(motionEvent2));
    }

    public final int c(MotionEvent motionEvent) {
        return (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
    }
}
