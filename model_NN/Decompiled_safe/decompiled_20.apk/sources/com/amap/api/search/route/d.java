package com.amap.api.search.route;

import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.core.l;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;

public abstract class d extends l<e, ArrayList<Route>> {
    protected String j = "起点";
    protected String k = "目的地";
    protected LatLonPoint l = ((e) this.b).a.mFrom;
    protected LatLonPoint m = ((e) this.b).a.mTo;

    public d(e eVar, Proxy proxy, String str, String str2) {
        super(eVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<Route> b(InputStream inputStream) {
        return null;
    }

    public abstract void a(List<LatLonPoint> list);

    /* access modifiers changed from: protected */
    public LatLonPoint[] a(String[] strArr) {
        int i = 0;
        LatLonPoint[] latLonPointArr = new LatLonPoint[(strArr.length / 2)];
        int length = strArr.length;
        int i2 = 0;
        while (i2 < length - 1) {
            latLonPointArr[i] = new LatLonPoint(Double.parseDouble(strArr[i2 + 1]), Double.parseDouble(strArr[i2]));
            i2 += 2;
            i++;
        }
        return latLonPointArr;
    }
}
