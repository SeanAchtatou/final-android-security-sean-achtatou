package com.amap.api.search.route;

import com.amap.api.search.core.AMapException;
import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.core.a;
import com.amap.api.search.core.b;
import com.amap.api.search.core.d;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class f extends c {
    public List<LatLonPoint> i;

    public f(e eVar, Proxy proxy, String str, String str2) {
        super(eVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<Route> b(InputStream inputStream) {
        String str;
        ArrayList<Route> arrayList = new ArrayList<>();
        try {
            str = new String(a.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        d.b(str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.getInt("count") > 0) {
                JSONArray jSONArray = jSONObject.getJSONArray("list");
                Route route = new Route(((e) this.b).b);
                LinkedList linkedList = new LinkedList();
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    WalkSegment walkSegment = new WalkSegment();
                    String string = jSONObject2.getString("roadLength");
                    if (string.contains("千米")) {
                        string = new StringBuilder().append((int) (Double.parseDouble(string.substring(0, string.length() - 2)) * 1000.0d)).toString();
                    } else if (string.contains("米")) {
                        string = string.substring(0, string.length() - 1);
                    } else if (string.contains("公里")) {
                        string = new StringBuilder().append((int) (Double.parseDouble(string.substring(0, string.length() - 2)) * 1000.0d)).toString();
                    }
                    walkSegment.setLength(Integer.parseInt(string));
                    walkSegment.setRoadName(jSONObject2.getString("roadName"));
                    walkSegment.setActionDescription(jSONObject2.getString("action"));
                    try {
                        walkSegment.setShapes(a(jSONObject2.getString("coor").split(",|;")));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    walkSegment.setConsumeTime(jSONObject2.getString("driveTime"));
                    walkSegment.setAccessorialInfo(jSONObject2.getString("accessorialInfo"));
                    walkSegment.setDirection(jSONObject2.getString("direction"));
                    walkSegment.setTextInfo(jSONObject2.getString("textInfo"));
                    if (walkSegment.getShapes().length != 0) {
                        linkedList.add(walkSegment);
                    }
                }
                if (linkedList.size() == 0) {
                    return null;
                }
                route.a(linkedList);
                a(route);
                for (Segment route2 : route.a()) {
                    route2.setRoute(route);
                }
                route.setStartPlace(this.j);
                route.setTargetPlace(this.k);
                arrayList.add(route);
            }
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
        if (arrayList.size() == 0) {
            throw new AMapException(AMapException.ERROR_IO);
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e4) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(Route route) {
        for (int stepCount = route.getStepCount() - 1; stepCount > 0; stepCount--) {
            WalkSegment walkSegment = (WalkSegment) route.getStep(stepCount);
            WalkSegment walkSegment2 = (WalkSegment) route.getStep(stepCount - 1);
            walkSegment.setActionCode(walkSegment2.getActionCode());
            walkSegment.setActionDescription(walkSegment2.getActionDescription());
        }
        WalkSegment walkSegment3 = (WalkSegment) route.getStep(0);
        walkSegment3.setActionCode(-1);
        walkSegment3.setActionDescription(PoiTypeDef.All);
    }

    public void a(List<LatLonPoint> list) {
        this.i = list;
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        sb.append("sid=8003");
        sb.append("&encode=utf-8");
        sb.append("&ia=1");
        sb.append("&xys=" + ((e) this.b).a() + "," + ((e) this.b).c() + ";");
        sb.append(((e) this.b).b() + "," + ((e) this.b).d());
        sb.append("&resType=json");
        sb.append("&RouteType=" + ((e) this.b).b);
        sb.append("&per=");
        sb.append(50);
        sb.append("&key=");
        sb.append(b.a);
        return sb.toString().getBytes();
    }
}
