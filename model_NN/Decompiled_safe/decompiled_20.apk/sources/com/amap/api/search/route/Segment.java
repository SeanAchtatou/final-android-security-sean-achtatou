package com.amap.api.search.route;

import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.poisearch.PoiTypeDef;

public class Segment {
    private LatLonPoint a = null;
    private LatLonPoint b = null;
    protected int mLength;
    protected Route mRoute;
    protected LatLonPoint[] mShapes;
    protected String strTimeConsume = PoiTypeDef.All;

    private void a() {
        double d = Double.MAX_VALUE;
        double d2 = Double.MIN_VALUE;
        double d3 = Double.MAX_VALUE;
        double d4 = Double.MIN_VALUE;
        LatLonPoint[] latLonPointArr = this.mShapes;
        int length = latLonPointArr.length;
        int i = 0;
        while (i < length) {
            LatLonPoint latLonPoint = latLonPointArr[i];
            double longitude = latLonPoint.getLongitude();
            double latitude = latLonPoint.getLatitude();
            if (longitude > d2) {
                d2 = longitude;
            }
            if (longitude >= d) {
                longitude = d;
            }
            if (latitude > d4) {
                d4 = latitude;
            }
            if (latitude >= d3) {
                latitude = d3;
            }
            i++;
            d = longitude;
            d3 = latitude;
        }
        this.a = new LatLonPoint(d3, d);
        this.b = new LatLonPoint(d4, d2);
    }

    private int b() {
        int segmentIndex = this.mRoute.getSegmentIndex(this);
        if (segmentIndex >= 0) {
            return segmentIndex;
        }
        throw new IllegalArgumentException("this segment is not in the route !");
    }

    public String getConsumeTime() {
        return this.strTimeConsume;
    }

    public LatLonPoint getFirstPoint() {
        return this.mShapes[0];
    }

    public LatLonPoint getLastPoint() {
        return this.mShapes[this.mShapes.length - 1];
    }

    public int getLength() {
        return this.mLength;
    }

    public LatLonPoint getLowerLeftPoint() {
        if (this.a == null) {
            a();
        }
        return this.a;
    }

    public Segment getNext() {
        int b2 = b();
        if (b2 == this.mRoute.getStepCount() - 1) {
            return null;
        }
        return this.mRoute.getStep(b2 + 1);
    }

    public Segment getPrev() {
        int b2 = b();
        if (b2 == 0) {
            return null;
        }
        return this.mRoute.getStep(b2 - 1);
    }

    public LatLonPoint[] getShapes() {
        return this.mShapes;
    }

    public LatLonPoint getUpperRightPoint() {
        if (this.b == null) {
            a();
        }
        return this.b;
    }

    public void setConsumeTime(String str) {
        this.strTimeConsume = str;
    }

    public void setLength(int i) {
        this.mLength = i;
    }

    public void setRoute(Route route) {
        this.mRoute = route;
    }

    public void setShapes(LatLonPoint[] latLonPointArr) {
        this.mShapes = latLonPointArr;
    }
}
