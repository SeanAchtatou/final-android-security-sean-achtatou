package com.amap.api.search.core;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import java.util.List;

public final class Inputtips {
    Handler a = new f(this);
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public InputtipsListener c;

    public interface InputtipsListener {
        void onGetInputtips(List<String> list);
    }

    public Inputtips(Context context, InputtipsListener inputtipsListener) {
        b.a(context);
        this.b = context;
        this.c = inputtipsListener;
    }

    public final void requestInputtips(final String str, final String str2) {
        new Thread() {
            public void run() {
                try {
                    Message message = new Message();
                    message.what = 100;
                    message.obj = (List) new g(new h(str, str2), d.b(Inputtips.this.b), null, null).g();
                    Inputtips.this.a.sendMessage(message);
                } catch (AMapException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
