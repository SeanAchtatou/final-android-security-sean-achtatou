package com.amap.api.search.geocoder;

import android.location.Address;
import com.amap.api.search.core.k;
import com.amap.api.search.core.l;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;

class a extends l<b, ArrayList<Address>> {
    public int i = 0;

    public a(b bVar, Proxy proxy, String str, String str2) {
        super(bVar, proxy, str, str2);
        this.i = bVar.b;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0114, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0115, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x011b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x011c, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0114 A[ExcHandler: JSONException (r0v5 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:4:0x0013] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<android.location.Address> b(java.io.InputStream r14) {
        /*
            r13 = this;
            r3 = 0
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r1 = 0
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0022 }
            byte[] r4 = com.amap.api.search.core.a.a(r14)     // Catch:{ Exception -> 0x0022 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0022 }
        L_0x0010:
            com.amap.api.search.core.d.b(r0)
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.String r0 = "count"
            boolean r0 = r1.has(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r0 != 0) goto L_0x0028
            r0 = r2
        L_0x0021:
            return r0
        L_0x0022:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0010
        L_0x0028:
            java.lang.String r0 = "count"
            int r0 = r1.getInt(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r0 <= 0) goto L_0x0118
            java.lang.String r0 = "list"
            org.json.JSONArray r4 = r1.getJSONArray(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
        L_0x0036:
            int r0 = r4.length()     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r3 >= r0) goto L_0x0118
            org.json.JSONObject r5 = r4.getJSONObject(r3)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            android.location.Address r6 = com.amap.api.search.core.d.a()     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.String r0 = "name"
            java.lang.String r0 = r5.getString(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r1 != 0) goto L_0x0053
            r6.setFeatureName(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
        L_0x0053:
            java.lang.String r0 = "address"
            java.lang.String r0 = r5.getString(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r1 != 0) goto L_0x0063
            r1 = 2
            r6.setAddressLine(r1, r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
        L_0x0063:
            java.lang.String r0 = "province"
            java.lang.String r1 = r5.getString(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r0 != 0) goto L_0x0072
            r6.setAdminArea(r1)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
        L_0x0072:
            java.lang.String r0 = "city"
            java.lang.String r0 = r5.getString(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            boolean r7 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r7 == 0) goto L_0x007f
            r0 = r1
        L_0x007f:
            r6.setLocality(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.String r7 = "district"
            java.lang.String r7 = r5.getString(r7)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            boolean r8 = android.text.TextUtils.isEmpty(r7)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r8 != 0) goto L_0x00ab
            java.lang.Class r8 = r6.getClass()     // Catch:{ Exception -> 0x0120, JSONException -> 0x0114 }
            java.lang.String r9 = "setSubLocality"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x0120, JSONException -> 0x0114 }
            r11 = 0
            java.lang.Class<java.lang.String> r12 = java.lang.String.class
            r10[r11] = r12     // Catch:{ Exception -> 0x0120, JSONException -> 0x0114 }
            java.lang.reflect.Method r8 = r8.getMethod(r9, r10)     // Catch:{ Exception -> 0x0120, JSONException -> 0x0114 }
            if (r8 == 0) goto L_0x00ab
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x0120, JSONException -> 0x0114 }
            r10 = 0
            r9[r10] = r7     // Catch:{ Exception -> 0x0120, JSONException -> 0x0114 }
            r8.invoke(r6, r9)     // Catch:{ Exception -> 0x0120, JSONException -> 0x0114 }
        L_0x00ab:
            r8 = 0
            java.lang.String r9 = "中国"
            r6.setAddressLine(r8, r9)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            boolean r8 = r1.equals(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r8 != 0) goto L_0x00fe
            r8 = 1
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            r9.<init>()     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.StringBuilder r1 = r9.append(r1)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            r6.setAddressLine(r8, r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
        L_0x00d0:
            java.lang.String r0 = "x"
            java.lang.String r0 = r5.getString(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r1 != 0) goto L_0x00e3
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            r6.setLongitude(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
        L_0x00e3:
            java.lang.String r0 = "y"
            java.lang.String r0 = r5.getString(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            if (r1 != 0) goto L_0x00f6
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            r6.setLatitude(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
        L_0x00f6:
            r2.add(r6)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0036
        L_0x00fe:
            r1 = 1
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            r8.<init>()     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.StringBuilder r0 = r8.append(r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            r6.setAddressLine(r1, r0)     // Catch:{ JSONException -> 0x0114, Exception -> 0x011b }
            goto L_0x00d0
        L_0x0114:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0118:
            r0 = r2
            goto L_0x0021
        L_0x011b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0118
        L_0x0120:
            r8 = move-exception
            goto L_0x00ab
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.search.geocoder.a.b(java.io.InputStream):java.util.ArrayList");
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        sb.append("?sid=7000&resType=json&encode=utf-8&address=");
        String str = ((b) this.b).a;
        try {
            str = URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sb.append(str);
        String str2 = ((b) this.b).c;
        if (str2 != null && !str2.equals(PoiTypeDef.All)) {
            try {
                str2 = URLEncoder.encode(str2, "utf-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            sb.append("&city=" + str2);
        }
        sb.append("&count=");
        sb.append(((b) this.b).b);
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String e() {
        return k.a().b() + "/geocode/simple?";
    }
}
