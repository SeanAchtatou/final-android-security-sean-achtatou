package com.amap.api.search.route;

import com.amap.api.search.core.AMapException;
import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.core.a;
import com.amap.api.search.core.d;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b extends c {
    List<LatLonPoint> i;

    public b(e eVar, Proxy proxy, String str, String str2) {
        super(eVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<Route> b(InputStream inputStream) {
        String str;
        ArrayList<Route> arrayList = new ArrayList<>();
        try {
            str = new String(a.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        d.b(str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.getInt("count") > 0) {
                JSONArray jSONArray = jSONObject.getJSONArray("list");
                Route route = new Route(((e) this.b).b);
                LinkedList linkedList = new LinkedList();
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    DriveSegment driveSegment = new DriveSegment();
                    String string = jSONObject2.getString("roadLength");
                    if (string.contains("千米")) {
                        string = new StringBuilder().append((int) (Double.parseDouble(string.substring(0, string.length() - 2)) * 1000.0d)).toString();
                    } else if (string.contains("米")) {
                        string = string.substring(0, string.length() - 1);
                    } else if (string.contains("公里")) {
                        string = new StringBuilder().append((int) (Double.parseDouble(string.substring(0, string.length() - 2)) * 1000.0d)).toString();
                    }
                    driveSegment.setLength(Integer.parseInt(string));
                    driveSegment.setRoadName(jSONObject2.getString("roadName"));
                    driveSegment.setActionDescription(jSONObject2.getString("action"));
                    try {
                        driveSegment.setShapes(a(jSONObject2.getString("coor").split(",|;")));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    driveSegment.setConsumeTime(jSONObject2.getString("driveTime"));
                    if (driveSegment.getShapes().length != 0) {
                        linkedList.add(driveSegment);
                    }
                }
                if (linkedList.size() == 0) {
                    return null;
                }
                route.a(linkedList);
                a(route);
                for (Segment route2 : route.a()) {
                    route2.setRoute(route);
                }
                route.setStartPlace(this.j);
                route.setTargetPlace(this.k);
                arrayList.add(route);
            }
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
        if (arrayList.size() == 0) {
            throw new AMapException(AMapException.ERROR_IO);
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e4) {
                throw new AMapException(AMapException.ERROR_IO);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(Route route) {
        for (int stepCount = route.getStepCount() - 1; stepCount > 0; stepCount--) {
            DriveSegment driveSegment = (DriveSegment) route.getStep(stepCount);
            DriveSegment driveSegment2 = (DriveSegment) route.getStep(stepCount - 1);
            driveSegment.setActionCode(driveSegment2.getActionCode());
            driveSegment.setActionDescription(driveSegment2.getActionDescription());
        }
        DriveSegment driveSegment3 = (DriveSegment) route.getStep(0);
        driveSegment3.setActionCode(-1);
        driveSegment3.setActionDescription(PoiTypeDef.All);
    }

    public void a(List<LatLonPoint> list) {
        this.i = list;
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        sb.append("sid=8000");
        sb.append("&encode=utf-8");
        sb.append("&xys=" + ((e) this.b).a() + "," + ((e) this.b).c() + ";");
        if (this.i != null && this.i.size() > 0) {
            int size = this.i.size();
            String str = PoiTypeDef.All;
            for (int i2 = 0; i2 < size; i2++) {
                LatLonPoint latLonPoint = this.i.get(i2);
                str = str + latLonPoint.getLongitude() + "," + latLonPoint.getLatitude() + ";";
            }
            sb.append(str);
        }
        sb.append(((e) this.b).b() + "," + ((e) this.b).d());
        sb.append("&resType=json");
        sb.append("&RouteType=" + ((e) this.b).b);
        sb.append("&per=");
        sb.append(50);
        return sb.toString().getBytes();
    }
}
