package com.amap.api.search.route;

public class WalkSegment extends DriveWalkSegment {
    protected String mAccessorialInfo;
    protected String mTextInfo;

    public String getAccessorialInfo() {
        return this.mAccessorialInfo;
    }

    public String getTextInfo() {
        return this.mTextInfo;
    }

    public void setAccessorialInfo(String str) {
        this.mAccessorialInfo = str;
    }

    public void setTextInfo(String str) {
        this.mTextInfo = str;
    }
}
