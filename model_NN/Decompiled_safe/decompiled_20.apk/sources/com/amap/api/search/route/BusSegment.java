package com.amap.api.search.route;

import com.amap.api.search.core.LatLonPoint;

public class BusSegment extends Segment {
    protected String mFirstStation;
    protected String mLastStation;
    protected String mLine;
    protected String[] mPassStopName;
    protected LatLonPoint[] mPassStopPos;

    public String getFirstStationName() {
        return this.mFirstStation;
    }

    public String getLastStationName() {
        return this.mLastStation;
    }

    public String getLineName() {
        return this.mLine;
    }

    public String getOffStationName() {
        return this.mPassStopName[this.mPassStopName.length - 1];
    }

    public String getOnStationName() {
        return this.mPassStopName[0];
    }

    public String[] getPassStopName() {
        return this.mPassStopName;
    }

    public int getStopNumber() {
        return this.mPassStopPos.length;
    }

    public void setFirstStationName(String str) {
        this.mFirstStation = str;
    }

    public void setLastStationName(String str) {
        this.mLastStation = str;
    }

    public void setLineName(String str) {
        this.mLine = str;
    }

    public void setPassStopName(String[] strArr) {
        this.mPassStopName = strArr;
    }

    public void setPassStopPos(LatLonPoint[] latLonPointArr) {
        this.mPassStopPos = latLonPointArr;
    }
}
