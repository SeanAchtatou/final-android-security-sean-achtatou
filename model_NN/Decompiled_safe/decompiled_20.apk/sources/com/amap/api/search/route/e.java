package com.amap.api.search.route;

import com.amap.api.search.route.Route;

public class e {
    public Route.FromAndTo a;
    public int b;

    public e(Route.FromAndTo fromAndTo, int i) {
        this.a = fromAndTo;
        this.b = i;
    }

    public double a() {
        return this.a.mFrom.getLongitude();
    }

    public double b() {
        return this.a.mTo.getLongitude();
    }

    public double c() {
        return this.a.mFrom.getLatitude();
    }

    public double d() {
        return this.a.mTo.getLatitude();
    }
}
