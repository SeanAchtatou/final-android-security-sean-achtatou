package com.amap.api.search.poisearch;

import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.core.a;
import com.amap.api.search.core.d;
import com.amap.api.search.core.k;
import com.amap.api.search.core.l;
import com.amap.api.search.poisearch.PoiSearch;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b extends l<c, ArrayList<PoiItem>> {
    private int i = 1;
    private int j = 20;
    private int k = 0;
    private ArrayList<String> l = new ArrayList<>();

    public b(c cVar, Proxy proxy, String str, String str2) {
        super(cVar, proxy, str, str2);
    }

    private boolean a(String str) {
        return str == null || str.equals(PoiTypeDef.All);
    }

    public int a() {
        return this.j;
    }

    /* renamed from: a */
    public ArrayList<PoiItem> b(InputStream inputStream) {
        String str;
        ArrayList<PoiItem> arrayList = new ArrayList<>();
        try {
            str = new String(a.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        d.b(str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has("list")) {
                return arrayList;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("list");
            this.k = jSONObject.getInt("count");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                String string = jSONObject2.getString("pguid");
                double d = jSONObject2.getDouble("x");
                PoiItem poiItem = new PoiItem(string, new LatLonPoint(jSONObject2.getDouble("y"), d), jSONObject2.getString("name"), jSONObject2.getString("address"));
                if (jSONObject2.has("adcode")) {
                    poiItem.setAdCode(jSONObject2.getString("adcode"));
                }
                if (jSONObject2.has("citycode")) {
                    poiItem.setCityCode(jSONObject2.getString("citycode"));
                }
                if (jSONObject2.has("distance")) {
                    poiItem.setDistance(Integer.parseInt(jSONObject2.getString("distance")));
                    if (poiItem.getDistance() == 0) {
                        poiItem.setDistance(-1);
                    }
                }
                poiItem.setTel(jSONObject2.getString("tel"));
                String str2 = PoiTypeDef.All;
                if (jSONObject2.has("typecode")) {
                    str2 = jSONObject2.getString("typecode");
                }
                if (str2.length() > 4) {
                    poiItem.setTypeCode(str2.substring(0, 4));
                } else {
                    poiItem.setTypeCode(PoiTypeDef.All);
                }
                String[] split = jSONObject2.getString("type").split(";");
                String str3 = split[0];
                for (int i3 = 1; i3 < split.length; i3++) {
                    str3 = str3 + PoiItem.DesSplit + split[i3];
                }
                poiItem.setTypeDes(str3);
                poiItem.setXmlNode(jSONObject2.getString("xml"));
                arrayList.add(poiItem);
            }
            return arrayList;
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public void a(int i2) {
        this.i = i2;
    }

    public int b() {
        return this.k;
    }

    public void b(int i2) {
        int i3 = 20;
        int i4 = i2 > 20 ? 20 : i2;
        if (i4 > 0) {
            i3 = i4;
        }
        this.j = i3;
    }

    public PoiSearch.Query c() {
        return ((c) this.b).a;
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        if (((c) this.b).b == null) {
            sb.append("sid=1000&city=");
            String city = ((c) this.b).a.getCity();
            if (a(city)) {
                sb.append("total");
            } else {
                try {
                    city = URLEncoder.encode(city, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                sb.append(city);
            }
        } else if (((c) this.b).b.getShape().equals(PoiSearch.SearchBound.BOUND_SHAPE)) {
            sb.append("sid=1002&city=total");
            sb.append("&cenX=");
            sb.append(((c) this.b).b.getCenter().getLongitude());
            sb.append("&cenY=");
            sb.append(((c) this.b).b.getCenter().getLatitude());
            sb.append("&range=");
            sb.append(((c) this.b).b.getRange());
        } else if (((c) this.b).b.getShape().equals(PoiSearch.SearchBound.RECTANGLE_SHAPE)) {
            sb.append("sid=1005");
            LatLonPoint lowerLeft = ((c) this.b).b.getLowerLeft();
            LatLonPoint upperRight = ((c) this.b).b.getUpperRight();
            double latitude = lowerLeft.getLatitude();
            double longitude = lowerLeft.getLongitude();
            double latitude2 = upperRight.getLatitude();
            double longitude2 = upperRight.getLongitude();
            sb.append("&regionType=rectangle");
            sb.append("&region=" + longitude + "," + latitude + ";" + longitude2 + "," + latitude2);
        }
        String queryString = ((c) this.b).a.getQueryString();
        try {
            queryString = URLEncoder.encode(queryString, "utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        sb.append("&keyword=" + queryString);
        sb.append("&number=" + this.j);
        sb.append("&batch=" + this.i);
        String category = ((c) this.b).a.getCategory();
        try {
            category = URLEncoder.encode(category, "utf-8");
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        }
        sb.append("&type=" + category);
        sb.append("&resType=json&encode=utf-8");
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String e() {
        return k.a().b() + "/gss/simple?";
    }

    public PoiSearch.SearchBound i() {
        return ((c) this.b).b;
    }

    public List<String> j() {
        return this.l;
    }
}
