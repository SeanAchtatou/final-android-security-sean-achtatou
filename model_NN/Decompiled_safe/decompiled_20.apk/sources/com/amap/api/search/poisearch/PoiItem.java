package com.amap.api.search.poisearch;

import android.os.Parcel;
import android.os.Parcelable;
import com.amap.api.search.core.LatLonPoint;

public class PoiItem {
    public static final Parcelable.Creator<PoiItem> CREATOR = new a();
    public static final String DesSplit = " - ";
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private int h;
    protected final LatLonPoint mPoint;
    protected final String mSnippet;
    protected final String mTitle;

    private PoiItem(Parcel parcel) {
        this.f = PoiTypeDef.All;
        this.h = -1;
        this.a = parcel.readString();
        this.d = parcel.readString();
        this.c = parcel.readString();
        this.b = parcel.readString();
        this.f = parcel.readString();
        this.h = parcel.readInt();
        this.mPoint = (LatLonPoint) parcel.readValue(LatLonPoint.class.getClassLoader());
        this.mTitle = parcel.readString();
        this.mSnippet = parcel.readString();
        this.e = parcel.readString();
    }

    /* synthetic */ PoiItem(Parcel parcel, a aVar) {
        this(parcel);
    }

    public PoiItem(String str, LatLonPoint latLonPoint, String str2, String str3) {
        this.f = PoiTypeDef.All;
        this.h = -1;
        this.a = str;
        this.mPoint = latLonPoint;
        this.mTitle = str2;
        this.mSnippet = str3;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == getClass() && this.a == ((PoiItem) obj).a;
    }

    public String getAdCode() {
        return this.d;
    }

    public String getCityCode() {
        return this.e;
    }

    public int getDistance() {
        return this.h;
    }

    public String getPoiId() {
        return this.a;
    }

    public LatLonPoint getPoint() {
        return this.mPoint;
    }

    public String getSnippet() {
        return this.mSnippet;
    }

    public String getTel() {
        return this.c;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getTypeCode() {
        return this.b;
    }

    public String getTypeDes() {
        return this.f;
    }

    public String getXmlNode() {
        return this.g;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public void setAdCode(String str) {
        this.d = str;
    }

    public void setCityCode(String str) {
        this.e = str;
    }

    public void setDistance(int i) {
        this.h = i;
    }

    public void setTel(String str) {
        this.c = str;
    }

    public void setTypeCode(String str) {
        this.b = str;
    }

    public void setTypeDes(String str) {
        this.f = str;
    }

    public void setXmlNode(String str) {
        this.g = str;
    }

    public String toString() {
        return this.mTitle;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.d);
        parcel.writeString(this.c);
        parcel.writeString(this.b);
        parcel.writeString(this.f);
        parcel.writeInt(this.h);
        parcel.writeValue(this.mPoint);
        parcel.writeString(this.mTitle);
        parcel.writeString(this.mSnippet);
        parcel.writeString(this.e);
    }
}
