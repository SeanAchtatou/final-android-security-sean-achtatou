package com.amap.api.search.busline;

import com.amap.api.search.busline.BusQuery;
import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.core.k;
import com.amap.api.search.core.l;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;

public class a extends l<BusQuery, ArrayList<BusLineItem>> {
    private int i = 1;
    private int j = 10;
    private int k = 0;

    public a(BusQuery busQuery, Proxy proxy, String str, String str2) {
        super(busQuery, proxy, str, str2);
    }

    private ArrayList<LatLonPoint> a(String str) {
        String[] split = str.split(",|;");
        ArrayList<LatLonPoint> arrayList = new ArrayList<>();
        for (int i2 = 0; i2 < split.length - 1; i2 += 2) {
            arrayList.add(new LatLonPoint(Double.parseDouble(split[i2 + 1]), Double.parseDouble(split[i2])));
        }
        return arrayList;
    }

    private boolean b(String str) {
        return str == null || str.equals(PoiTypeDef.All);
    }

    public int a() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0032 A[Catch:{ JSONException -> 0x01d2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x01d6 A[EDGE_INSN: B:27:0x01d6->B:25:0x01d6 ?: BREAK  , SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.amap.api.search.busline.BusLineItem> b(java.io.InputStream r19) {
        /*
            r18 = this;
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r2 = 0
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x01bc }
            byte[] r4 = com.amap.api.search.core.a.a(r19)     // Catch:{ Exception -> 0x01bc }
            r1.<init>(r4)     // Catch:{ Exception -> 0x01bc }
            com.amap.api.search.core.d.b(r1)     // Catch:{ Exception -> 0x01d7 }
        L_0x0012:
            com.amap.api.search.core.d.b(r1)
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01d2 }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r1 = "count"
            int r1 = r2.getInt(r1)     // Catch:{ JSONException -> 0x01d2 }
            r0 = r18
            r0.k = r1     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r1 = "list"
            org.json.JSONArray r4 = r2.getJSONArray(r1)     // Catch:{ JSONException -> 0x01d2 }
            r1 = 0
            r2 = r1
        L_0x002c:
            int r1 = r4.length()     // Catch:{ JSONException -> 0x01d2 }
            if (r2 >= r1) goto L_0x01d6
            org.json.JSONObject r1 = r4.getJSONObject(r2)     // Catch:{ JSONException -> 0x01d2 }
            com.amap.api.search.busline.BusLineItem r5 = new com.amap.api.search.busline.BusLineItem     // Catch:{ JSONException -> 0x01d2 }
            r5.<init>()     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "length"
            double r6 = r1.getDouble(r6)     // Catch:{ JSONException -> 0x01d2 }
            float r6 = (float) r6     // Catch:{ JSONException -> 0x01d2 }
            r5.setmLength(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "name"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmName(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "description"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmDescription(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "status"
            int r6 = r1.getInt(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmStatus(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "speed"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r7 = ""
            boolean r6 = r6.equals(r7)     // Catch:{ JSONException -> 0x01d2 }
            if (r6 != 0) goto L_0x0078
            java.lang.String r6 = "speed"
            double r6 = r1.getDouble(r6)     // Catch:{ JSONException -> 0x01d2 }
            float r6 = (float) r6     // Catch:{ JSONException -> 0x01d2 }
            r5.setmSpeed(r6)     // Catch:{ JSONException -> 0x01d2 }
        L_0x0078:
            java.lang.String r6 = "xys"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r0 = r18
            java.util.ArrayList r6 = r0.a(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmXys(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "line_id"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmLineId(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "key_name"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmKeyName(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "front_name"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmFrontName(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "terminal_name"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmTerminalName(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "start_time"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmStartTime(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "end_time"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmEndTime(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "company"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmCompany(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "basic_price"
            double r6 = r1.getDouble(r6)     // Catch:{ JSONException -> 0x01d2 }
            float r6 = (float) r6     // Catch:{ JSONException -> 0x01d2 }
            r5.setmBasicPrice(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "total_price"
            double r6 = r1.getDouble(r6)     // Catch:{ JSONException -> 0x01d2 }
            float r6 = (float) r6     // Catch:{ JSONException -> 0x01d2 }
            r5.setmTotalPrice(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "commutation_ticket"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r7 = "1"
            boolean r6 = r6.equals(r7)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmCommunicationTicket(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "auto"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r7 = "1"
            boolean r6 = r6.equals(r7)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmAuto(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "ic_card"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r7 = "1"
            boolean r6 = r6.equals(r7)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmIcCard(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "loop"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r7 = "1"
            boolean r6 = r6.equals(r7)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmLoop(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "double_deck"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r7 = "1"
            boolean r6 = r6.equals(r7)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmDoubleDeck(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "data_source"
            int r6 = r1.getInt(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmDataSource(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "air"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r7 = "1"
            boolean r6 = r6.equals(r7)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmAir(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "front_spell"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmFrontSpell(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "terminal_spell"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmTerminalSpell(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "express_way"
            java.lang.String r6 = r1.getString(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r7 = "1"
            boolean r6 = r6.equals(r7)     // Catch:{ JSONException -> 0x01d2 }
            r5.setmExpressWay(r6)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r6 = "stationdes"
            org.json.JSONArray r6 = r1.getJSONArray(r6)     // Catch:{ JSONException -> 0x01d2 }
            int r7 = r6.length()     // Catch:{ JSONException -> 0x01d2 }
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ JSONException -> 0x01d2 }
            r8.<init>()     // Catch:{ JSONException -> 0x01d2 }
            r1 = 0
        L_0x016e:
            if (r1 >= r7) goto L_0x01c7
            com.amap.api.search.busline.BusStationItem r9 = new com.amap.api.search.busline.BusStationItem     // Catch:{ JSONException -> 0x01d2 }
            r9.<init>()     // Catch:{ JSONException -> 0x01d2 }
            org.json.JSONObject r10 = r6.getJSONObject(r1)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r11 = "code"
            java.lang.String r11 = r10.getString(r11)     // Catch:{ JSONException -> 0x01d2 }
            r9.setmCode(r11)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r11 = "xy"
            java.lang.String r11 = r10.getString(r11)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r12 = ","
            java.lang.String[] r11 = r11.split(r12)     // Catch:{ JSONException -> 0x01d2 }
            com.amap.api.search.core.LatLonPoint r12 = new com.amap.api.search.core.LatLonPoint     // Catch:{ JSONException -> 0x01d2 }
            r13 = 1
            r13 = r11[r13]     // Catch:{ JSONException -> 0x01d2 }
            double r13 = java.lang.Double.parseDouble(r13)     // Catch:{ JSONException -> 0x01d2 }
            r15 = 0
            r11 = r11[r15]     // Catch:{ JSONException -> 0x01d2 }
            double r15 = java.lang.Double.parseDouble(r11)     // Catch:{ JSONException -> 0x01d2 }
            r12.<init>(r13, r15)     // Catch:{ JSONException -> 0x01d2 }
            r9.setmCoord(r12)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r11 = "name"
            java.lang.String r11 = r10.getString(r11)     // Catch:{ JSONException -> 0x01d2 }
            r9.setmName(r11)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r11 = "stationNum"
            int r10 = r10.getInt(r11)     // Catch:{ JSONException -> 0x01d2 }
            r9.setmStationNum(r10)     // Catch:{ JSONException -> 0x01d2 }
            r8.add(r9)     // Catch:{ JSONException -> 0x01d2 }
            int r1 = r1 + 1
            goto L_0x016e
        L_0x01bc:
            r1 = move-exception
            r17 = r1
            r1 = r2
            r2 = r17
        L_0x01c2:
            r2.printStackTrace()
            goto L_0x0012
        L_0x01c7:
            r5.setmStations(r8)     // Catch:{ JSONException -> 0x01d2 }
            r3.add(r5)     // Catch:{ JSONException -> 0x01d2 }
            int r1 = r2 + 1
            r2 = r1
            goto L_0x002c
        L_0x01d2:
            r1 = move-exception
            r1.printStackTrace()
        L_0x01d6:
            return r3
        L_0x01d7:
            r2 = move-exception
            goto L_0x01c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.search.busline.a.b(java.io.InputStream):java.util.ArrayList");
    }

    public void a(int i2) {
        this.i = i2;
    }

    public BusQuery b() {
        return (BusQuery) this.b;
    }

    public void b(int i2) {
        int i3 = 20;
        if (i2 <= 20) {
            i3 = i2;
        }
        if (i3 <= 0) {
            i3 = 10;
        }
        this.j = i3;
    }

    public int c() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        sb.append("encode=utf-8");
        sb.append("&resType=json&city=");
        String city = ((BusQuery) this.b).getCity();
        if (b(city)) {
            sb.append("total");
        } else {
            try {
                sb.append(URLEncoder.encode(city, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_LINE_NAME) {
            sb.append("&sid=8004");
            sb.append("&busName=");
        } else if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_ID) {
            sb.append("&sid=8085");
            sb.append("&ids=");
        } else if (((BusQuery) this.b).getCategory() == BusQuery.SearchType.BY_STATION_NAME) {
            sb.append("&sid=8086");
            sb.append("&stationName=");
        }
        String queryString = ((BusQuery) this.b).getQueryString();
        try {
            queryString = URLEncoder.encode(queryString, "utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        sb.append(queryString);
        sb.append("&number=");
        sb.append(this.j);
        sb.append("&batch=");
        sb.append(this.i);
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String e() {
        return k.a().b() + "/bus/simple";
    }
}
