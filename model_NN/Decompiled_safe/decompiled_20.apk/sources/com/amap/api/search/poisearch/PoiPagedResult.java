package com.amap.api.search.poisearch;

import com.amap.api.search.poisearch.PoiSearch;
import java.util.ArrayList;
import java.util.List;

public final class PoiPagedResult {
    private int a;
    private ArrayList<ArrayList<PoiItem>> b;
    private b c;

    private PoiPagedResult(b bVar, ArrayList<PoiItem> arrayList) {
        this.c = bVar;
        this.a = a(bVar.b());
        a(arrayList);
    }

    private int a(int i) {
        int a2 = this.c.a();
        int i2 = ((i + a2) - 1) / a2;
        if (i2 > 30) {
            return 30;
        }
        return i2;
    }

    static PoiPagedResult a(b bVar, ArrayList<PoiItem> arrayList) {
        return new PoiPagedResult(bVar, arrayList);
    }

    private void a(ArrayList<PoiItem> arrayList) {
        this.b = new ArrayList<>();
        for (int i = 0; i <= this.a; i++) {
            this.b.add(null);
        }
        if (this.a > 0) {
            this.b.set(1, arrayList);
        }
    }

    private boolean b(int i) {
        return i <= this.a && i > 0;
    }

    public final PoiSearch.SearchBound getBound() {
        return this.c.i();
    }

    public final List<PoiItem> getPage(int i) {
        if (this.a == 0) {
            return null;
        }
        if (!b(i)) {
            throw new IllegalArgumentException("page out of range");
        }
        ArrayList arrayList = (ArrayList) getPageLocal(i);
        if (arrayList != null) {
            return arrayList;
        }
        this.c.a(i);
        ArrayList arrayList2 = (ArrayList) this.c.g();
        this.b.set(i, arrayList2);
        return arrayList2;
    }

    public final int getPageCount() {
        return this.a;
    }

    public final List<PoiItem> getPageLocal(int i) {
        if (b(i)) {
            return this.b.get(i);
        }
        throw new IllegalArgumentException("page out of range");
    }

    public final PoiSearch.Query getQuery() {
        return this.c.c();
    }

    public final List<String> getSearchSuggestions() {
        return this.c.j();
    }
}
