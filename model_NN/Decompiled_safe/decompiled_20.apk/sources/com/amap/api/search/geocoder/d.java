package com.amap.api.search.geocoder;

import android.location.Address;
import com.amap.api.search.core.a;
import com.amap.api.search.core.k;
import com.amap.api.search.core.l;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class d extends l<e, ArrayList<List<Address>>> {
    private String i = null;
    private String j = null;
    private String k = null;
    private ArrayList<Address> l = new ArrayList<>();
    private ArrayList<Address> m = new ArrayList<>();
    private ArrayList<Address> n = new ArrayList<>();
    private boolean o;
    private boolean p;
    private boolean q;

    public d(e eVar, Proxy proxy, String str, String str2) {
        super(eVar, proxy, str, str2);
    }

    private String a(JSONObject jSONObject) {
        return jSONObject.getString("name");
    }

    private void a(JSONObject jSONObject, String str) {
        String substring;
        String str2;
        int i2 = 0;
        if (!jSONObject.isNull(str) && !jSONObject.get(str).equals(PoiTypeDef.All)) {
            JSONArray jSONArray = jSONObject.getJSONArray(str);
            if (str.equals("poilist")) {
                for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                    Address a = com.amap.api.search.core.d.a();
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
                    a.setFeatureName(jSONObject2.getString("name"));
                    a.setLatitude(jSONObject2.getDouble("y"));
                    a.setLongitude(jSONObject2.getDouble("x"));
                    String string = jSONObject2.getString("tel");
                    if (string != null && !string.equals(PoiTypeDef.All)) {
                        a.setPhone(string);
                    }
                    String string2 = jSONObject2.getString("address");
                    if (string2 != null && !string2.equals(PoiTypeDef.All)) {
                        if (Character.isDigit(string2.charAt(string2.length() - 1))) {
                            string2 = string2 + "号";
                        }
                        a.setAddressLine(2, string2);
                        int indexOf = string2.indexOf(36335);
                        if (indexOf != -1) {
                            str2 = string2.substring(0, indexOf + 1);
                            substring = string2.substring(indexOf + 1);
                        } else {
                            int indexOf2 = string2.indexOf(34903);
                            if (indexOf2 != -1) {
                                str2 = string2.substring(0, indexOf2 + 1);
                                substring = string2.substring(indexOf2 + 1);
                            } else {
                                int length = string2.length();
                                int i4 = 0;
                                while (true) {
                                    if (i4 >= string2.length()) {
                                        i4 = length;
                                        break;
                                    } else if (Character.isDigit(string2.charAt(i4))) {
                                        break;
                                    } else {
                                        i4++;
                                    }
                                }
                                String substring2 = string2.substring(0, i4);
                                substring = string2.substring(i4);
                                str2 = substring2;
                            }
                        }
                        a.setThoroughfare(str2);
                        try {
                            Method method = a.getClass().getMethod("setSubThoroughfare", String.class);
                            if (!(method == null || substring == null || substring.equals(PoiTypeDef.All))) {
                                method.invoke(a, substring);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        Method method2 = a.getClass().getMethod("setPremises", String.class);
                        if (method2 != null) {
                            method2.invoke(a, Geocoder.POI);
                        }
                    } catch (Exception e2) {
                    }
                    if (a != null) {
                        this.m.add(a);
                        this.p = true;
                    }
                }
            } else if (str.equals("crosslist")) {
                while (i2 < jSONArray.length()) {
                    Address a2 = com.amap.api.search.core.d.a();
                    JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                    a2.setFeatureName(jSONObject3.getJSONObject("road1").getString("name") + "-" + jSONObject3.getJSONObject("road2").getString("name"));
                    a2.setLatitude(jSONObject3.getDouble("y"));
                    a2.setLongitude(jSONObject3.getDouble("x"));
                    try {
                        Method method3 = a2.getClass().getMethod("setPremises", String.class);
                        if (method3 != null) {
                            method3.invoke(a2, Geocoder.Cross);
                        }
                    } catch (Exception e3) {
                    }
                    if (a2 != null) {
                        this.n.add(a2);
                        this.q = true;
                    }
                    i2++;
                }
            } else if (str.equals("roadlist")) {
                while (i2 < jSONArray.length()) {
                    Address a3 = com.amap.api.search.core.d.a();
                    JSONObject jSONObject4 = jSONArray.getJSONObject(i2);
                    String string3 = jSONObject4.getString("name");
                    a3.setFeatureName(string3);
                    a3.setLatitude(jSONObject4.getDouble("y"));
                    a3.setLongitude(jSONObject4.getDouble("x"));
                    a3.setAddressLine(2, string3);
                    a3.setThoroughfare(string3);
                    try {
                        Method method4 = a3.getClass().getMethod("setPremises", String.class);
                        if (method4 != null) {
                            method4.invoke(a3, Geocoder.Street_Road);
                        }
                    } catch (Exception e4) {
                    }
                    if (a3 != null) {
                        this.l.add(a3);
                        this.o = true;
                    }
                    i2++;
                }
            }
        }
    }

    private ArrayList<List<Address>> b(ArrayList<List<Address>> arrayList) {
        if (this.o) {
            arrayList.add(this.l);
        }
        if (this.p) {
            arrayList.add(this.m);
        }
        if (this.q) {
            arrayList.add(this.n);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<List<Address>> b(InputStream inputStream) {
        String str;
        ArrayList<List<Address>> arrayList = new ArrayList<>();
        try {
            str = new String(a.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        com.amap.api.search.core.d.b(str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("list")) {
                JSONArray jSONArray = jSONObject.getJSONArray("list");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    if (jSONObject2.has("province")) {
                        this.j = a(jSONObject2.getJSONObject("province"));
                    }
                    if (jSONObject2.has("district")) {
                        this.k = a(jSONObject2.getJSONObject("district"));
                    }
                    if (jSONObject2.has("city")) {
                        this.i = a(jSONObject2.getJSONObject("city"));
                        if (this.i == null || this.i.equals(PoiTypeDef.All)) {
                            this.i = this.j;
                        }
                    }
                    if (jSONObject2.has("roadlist")) {
                        a(jSONObject2, "roadlist");
                        if (this.l.size() > 0) {
                            a(this.l);
                        }
                    }
                    if (jSONObject2.has("crosslist")) {
                        a(jSONObject2, "crosslist");
                        if (this.n.size() > 0) {
                            a(this.n);
                        }
                    }
                    if (jSONObject2.has("poilist")) {
                        a(jSONObject2, "poilist");
                        if (this.m.size() > 0) {
                            a(this.m);
                        }
                    }
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        if (this.q || this.p || this.o) {
            return b(arrayList);
        }
        if (this.j != null) {
            Address a = com.amap.api.search.core.d.a();
            a.setAdminArea(this.j);
            a.setLocality(this.i);
            a.setFeatureName(this.k);
            a.setLatitude(((e) this.b).b);
            a.setLongitude(((e) this.b).a);
            try {
                Method method = a.getClass().getMethod("setSubLocality", String.class);
                if (method != null) {
                    method.invoke(a, this.k);
                }
            } catch (Exception e5) {
            }
            a.setAddressLine(0, "中国");
            if (!this.j.equals(this.i)) {
                a.setAddressLine(1, this.j + this.i + this.k);
            } else {
                a.setAddressLine(1, this.i + this.k);
            }
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(a);
            arrayList.add(arrayList2);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList<Address> arrayList) {
        if (this.j != null && arrayList.size() == 0) {
            arrayList.add(com.amap.api.search.core.d.a());
        }
        Iterator<Address> it = arrayList.iterator();
        while (it.hasNext()) {
            Address next = it.next();
            next.setAdminArea(this.j);
            next.setLocality(this.i);
            try {
                Method method = next.getClass().getMethod("setSubLocality", String.class);
                if (method != null) {
                    method.invoke(next, this.k);
                }
            } catch (Exception e) {
            }
            next.setAddressLine(0, "中国");
            if (!this.j.equals(this.i)) {
                next.setAddressLine(1, this.j + this.i + this.k);
            } else {
                next.setAddressLine(1, this.i + this.k);
            }
        }
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        sb.append("sid=7001&resType=json&encode=utf-8");
        sb.append("&region=" + ((e) this.b).a + "," + ((e) this.b).b);
        sb.append("&range=" + ((e) this.b).g);
        sb.append("&roadnum=" + ((e) this.b).d);
        sb.append("&crossnum=" + ((e) this.b).e);
        sb.append("&poinum=" + ((e) this.b).c);
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String e() {
        return k.a().b() + "/rgeocode/simple?";
    }
}
