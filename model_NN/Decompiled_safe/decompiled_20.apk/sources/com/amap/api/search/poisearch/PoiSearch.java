package com.amap.api.search.poisearch;

import android.content.Context;
import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.core.b;
import com.amap.api.search.core.d;
import java.util.ArrayList;

public class PoiSearch {
    private SearchBound a;
    private Query b;
    private Context c;
    private int d = 20;

    public class Query {
        private String a;
        private String b;
        private String c;

        public Query(String str, String str2) {
            this(str, str2, null);
        }

        public Query(String str, String str2, String str3) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            if (!b()) {
                throw new IllegalArgumentException("Empty  query and catagory");
            }
        }

        private boolean b() {
            return !d.a(this.a) || !d.a(this.b);
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return PoiTypeDef.All;
        }

        public String getCategory() {
            return (this.b == null || this.b.equals("00") || this.b.equals("00|")) ? a() : this.b;
        }

        public String getCity() {
            return this.c;
        }

        public String getQueryString() {
            return this.a;
        }
    }

    public class SearchBound {
        public static final String BOUND_SHAPE = "bound";
        public static final String ELLIPSE_SHAPE = "Ellipse";
        public static final String POLYGON_SHAPE = "Polygon";
        public static final String RECTANGLE_SHAPE = "Rectangle";
        private LatLonPoint a;
        private LatLonPoint b;
        private int c;
        private LatLonPoint d;
        private String e;

        public SearchBound(LatLonPoint latLonPoint, int i) {
            this.e = BOUND_SHAPE;
            this.c = i;
            this.d = latLonPoint;
            a(latLonPoint, d.a(i), d.a(i));
        }

        public SearchBound(LatLonPoint latLonPoint, LatLonPoint latLonPoint2) {
            this.e = RECTANGLE_SHAPE;
            a(latLonPoint, latLonPoint2);
        }

        private void a(LatLonPoint latLonPoint, double d2, double d3) {
            double d4 = d2 / 2.0d;
            double d5 = d3 / 2.0d;
            double latitude = latLonPoint.getLatitude();
            double longitude = latLonPoint.getLongitude();
            a(new LatLonPoint(latitude - d4, longitude - d5), new LatLonPoint(d4 + latitude, d5 + longitude));
        }

        private void a(LatLonPoint latLonPoint, LatLonPoint latLonPoint2) {
            this.a = latLonPoint;
            this.b = latLonPoint2;
            if (this.a.getLatitude() >= this.b.getLatitude() || this.a.getLongitude() >= this.b.getLongitude()) {
                throw new IllegalArgumentException("invalid rect ");
            }
            this.d = new LatLonPoint((this.a.getLatitude() + this.b.getLatitude()) / 2.0d, (this.a.getLongitude() + this.b.getLongitude()) / 2.0d);
        }

        public LatLonPoint getCenter() {
            return this.d;
        }

        public double getLatSpanInMeter() {
            return this.b.getLatitude() - this.a.getLatitude();
        }

        public double getLonSpanInMeter() {
            return this.b.getLongitude() - this.a.getLongitude();
        }

        public LatLonPoint getLowerLeft() {
            return this.a;
        }

        public int getRange() {
            return this.c;
        }

        public String getShape() {
            return this.e;
        }

        public LatLonPoint getUpperRight() {
            return this.b;
        }
    }

    public PoiSearch(Context context, Query query) {
        b.a(context);
        this.c = context;
        setQuery(query);
    }

    public PoiSearch(Context context, String str, Query query) {
        b.a(context);
        this.c = context;
        setQuery(query);
    }

    public SearchBound getBound() {
        return this.a;
    }

    public Query getQuery() {
        return this.b;
    }

    public PoiPagedResult searchPOI() {
        b bVar = new b(new c(this.b, this.a), d.b(this.c), d.a(this.c), null);
        bVar.a(1);
        bVar.b(this.d);
        return PoiPagedResult.a(bVar, (ArrayList) bVar.g());
    }

    public void setBound(SearchBound searchBound) {
        this.a = searchBound;
    }

    public void setPageSize(int i) {
        this.d = i;
    }

    @Deprecated
    public void setPoiNumber(int i) {
        setPageSize(i);
    }

    public void setQuery(Query query) {
        this.b = query;
    }
}
