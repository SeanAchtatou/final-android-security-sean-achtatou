package com.amap.api.search.busline;

import com.amap.api.search.core.LatLonPoint;

public class BusStationItem {
    private String a;
    private LatLonPoint b;
    private String c;
    private String d;
    private int e;

    public String getmCode() {
        return this.d;
    }

    public LatLonPoint getmCoord() {
        return this.b;
    }

    public String getmName() {
        return this.a;
    }

    public String getmSpell() {
        return this.c;
    }

    public int getmStationNum() {
        return this.e;
    }

    public void setmCode(String str) {
        this.d = str;
    }

    public void setmCoord(LatLonPoint latLonPoint) {
        this.b = latLonPoint;
    }

    public void setmName(String str) {
        this.a = str;
    }

    public void setmSpell(String str) {
        this.c = str;
    }

    public void setmStationNum(int i) {
        this.e = i;
    }

    public String toString() {
        return "Name: " + this.a + " Coord: " + this.b.toString() + " Spell: " + this.c + " Code: " + this.d + " StationNum: " + this.e;
    }
}
