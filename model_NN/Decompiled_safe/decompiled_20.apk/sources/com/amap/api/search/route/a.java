package com.amap.api.search.route;

import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.core.d;
import com.amap.api.search.core.k;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a extends d {
    List<LatLonPoint> i;

    public a(e eVar, Proxy proxy, String str, String str2) {
        super(eVar, proxy, str, str2);
    }

    private WalkSegment a(LatLonPoint latLonPoint, LatLonPoint latLonPoint2) {
        WalkSegment walkSegment = new WalkSegment();
        LatLonPoint[] latLonPointArr = new LatLonPoint[2];
        walkSegment.setShapes(latLonPointArr);
        latLonPointArr[0] = latLonPoint;
        latLonPointArr[1] = latLonPoint2;
        double latitude = latLonPoint2.getLatitude() - latLonPoint.getLatitude();
        double longitude = latLonPoint2.getLongitude() - latLonPoint.getLongitude();
        walkSegment.setLength(d.a(Math.sqrt((latitude * latitude) + (longitude * longitude))));
        if (walkSegment.getLength() == 0) {
            walkSegment.setLength(10);
        }
        return walkSegment;
    }

    private List<Segment> a(LinkedList<Segment> linkedList) {
        int size = linkedList.size();
        Segment[] segmentArr = new Segment[(size - 1)];
        for (int i2 = 0; i2 <= size - 2; i2++) {
            segmentArr[i2] = a(linkedList.get(i2).getLastPoint(), linkedList.get(i2 + 1).getFirstPoint());
        }
        for (int i3 = 0; i3 <= size - 2; i3++) {
            linkedList.add((i3 * 2) + 1, segmentArr[i3]);
        }
        linkedList.addFirst(a(this.l, linkedList.getFirst().getFirstPoint()));
        linkedList.addLast(a(linkedList.getLast().getLastPoint(), this.m));
        return linkedList;
    }

    private void a(BusSegment busSegment, String str) {
        busSegment.setLineName(str);
        busSegment.setFirstStationName(PoiTypeDef.All);
        busSegment.setLastStationName(PoiTypeDef.All);
        int indexOf = str.indexOf("(");
        int lastIndexOf = str.lastIndexOf(")");
        if (indexOf > 0 && lastIndexOf > 0 && lastIndexOf > indexOf) {
            busSegment.setLineName(str.substring(0, indexOf));
            try {
                String[] split = str.substring(indexOf + 1, lastIndexOf).split("--");
                busSegment.setFirstStationName(split[0]);
                busSegment.setLastStationName(split[1]);
            } catch (Exception e) {
            }
        }
    }

    private void a(LatLonPoint[] latLonPointArr, String[] strArr) {
        int i2 = 0;
        int i3 = 1;
        while (i2 < strArr.length - 1) {
            latLonPointArr[i3] = new LatLonPoint((double) ((int) (Double.parseDouble(strArr[i2 + 1]) * 1000000.0d)), (double) ((int) (Double.parseDouble(strArr[i2]) * 1000000.0d)));
            i2 += 2;
            i3++;
        }
    }

    private void a(String[] strArr, String[] strArr2) {
        for (int i2 = 0; i2 < strArr2.length; i2++) {
            strArr[i2 + 1] = strArr2[i2];
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<Route> b(InputStream inputStream) {
        String str;
        ArrayList<Route> arrayList = new ArrayList<>();
        try {
            str = new String(com.amap.api.search.core.a.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        d.b(str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("list") && !jSONObject.isNull("list")) {
                JSONArray jSONArray = jSONObject.getJSONArray("list");
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= jSONArray.length()) {
                        break;
                    }
                    JSONArray jSONArray2 = jSONArray.getJSONObject(i3).getJSONArray("segmentList");
                    Route route = new Route(((e) this.b).b);
                    LinkedList linkedList = new LinkedList();
                    int i4 = 0;
                    while (true) {
                        int i5 = i4;
                        if (i5 >= jSONArray2.length()) {
                            break;
                        }
                        JSONObject jSONObject2 = jSONArray2.getJSONObject(i5);
                        BusSegment busSegment = new BusSegment();
                        String string = jSONObject2.getString("startName");
                        String string2 = jSONObject2.getString("endName");
                        a(busSegment, jSONObject2.getString("busName"));
                        busSegment.setLength(jSONObject2.getInt("driverLength"));
                        int i6 = jSONObject2.getInt("passDepotCount");
                        String[] strArr = new String[(i6 + 2)];
                        LatLonPoint[] latLonPointArr = new LatLonPoint[(i6 + 2)];
                        String[] split = jSONObject2.getString("passDepotName").split(" ");
                        String[] split2 = jSONObject2.getString("passDepotCoordinate").split(",|;");
                        String[] split3 = jSONObject2.getString("coordinateList").split(",|;");
                        a(strArr, split);
                        try {
                            a(latLonPointArr, split2);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        try {
                            busSegment.setShapes(a(split3));
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                        latLonPointArr[0] = busSegment.getFirstPoint().Copy();
                        latLonPointArr[i6 + 1] = busSegment.getLastPoint().Copy();
                        strArr[0] = string;
                        strArr[i6 + 1] = string2;
                        busSegment.setPassStopPos(latLonPointArr);
                        busSegment.setPassStopName(strArr);
                        linkedList.add(busSegment);
                        i4 = i5 + 1;
                    }
                    route.a(linkedList);
                    a(route);
                    for (Segment route2 : route.a()) {
                        route2.setRoute(route);
                    }
                    route.setStartPlace(this.j);
                    route.setTargetPlace(this.k);
                    arrayList.add(route);
                    i2 = i3 + 1;
                }
            }
        } catch (JSONException e4) {
            e4.printStackTrace();
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(Route route) {
        route.a(a((LinkedList<Segment>) ((LinkedList) route.a())));
    }

    public void a(List<LatLonPoint> list) {
        this.i = list;
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        sb.append("sid=8001");
        sb.append("&encode=utf-8");
        sb.append("&xys=" + ((e) this.b).a() + "," + ((e) this.b).c() + ";");
        if (this.i != null && this.i.size() > 0) {
            int size = this.i.size();
            String str = PoiTypeDef.All;
            for (int i2 = 0; i2 < size; i2++) {
                LatLonPoint latLonPoint = this.i.get(i2);
                str = str + latLonPoint.getLongitude() + "," + latLonPoint.getLatitude() + ";";
            }
            sb.append(str);
        }
        sb.append(((e) this.b).b() + "," + ((e) this.b).d());
        sb.append("&resType=json");
        sb.append("&RouteType=" + ((e) this.b).b);
        sb.append("&per=");
        sb.append(50);
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String e() {
        return k.a().b() + "/bus/simple";
    }
}
