package com.amap.api.search.geocoder;

import android.content.Context;
import android.location.Address;
import com.amap.api.search.core.AMapException;
import com.amap.api.search.core.b;
import com.amap.api.search.core.d;
import com.amap.api.search.core.m;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.util.ArrayList;
import java.util.List;

public final class Geocoder {
    public static final String Cross = "Cross";
    public static final String POI = "POI";
    public static final String Street_Road = "StreetAndRoad";
    private String a;
    private Context b;

    public Geocoder(Context context) {
        b.a(context);
        a(context, d.a(context));
    }

    public Geocoder(Context context, String str) {
        b.a(context);
        a(context, d.a(context));
    }

    private List<List<Address>> a(double d, double d2, int i, int i2, int i3, int i4, boolean z) {
        if (d.a) {
            if (d < d.a(1000000L) || d > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException latitude == " + d);
            } else if (d2 < d.a(50000000L) || d2 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException longitude == " + d2);
            }
        }
        return (i > 0 || i2 > 0 || i3 > 0) ? (List) new d(new e(d2, d, i, i2, i3, 0, i4, z), d.b(this.b), this.a, null).g() : new ArrayList();
    }

    private List<Address> a(double d, double d2, int i, int i2, boolean z) {
        if (d.a) {
            if (d < d.a(1000000L) || d > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException latitude == " + d);
            } else if (d2 < d.a(50000000L) || d2 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException longitude == " + d2);
            }
        }
        return i <= 0 ? new ArrayList() : (List) new c(new e(d2, d, 0, 0, 0, i, i2, z), d.b(this.b), this.a, null).g();
    }

    private List<Address> a(List<Address> list, double d, double d2, double d3, double d4, int i) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (Address next : list) {
            double longitude = next.getLongitude();
            double latitude = next.getLatitude();
            if (longitude <= d4 && longitude >= d2 && latitude <= d3 && latitude >= d && arrayList.size() < i) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private void a(Context context, String str) {
        this.b = context;
        this.a = str;
    }

    public final List<Address> getFromLocation(double d, double d2, int i) {
        return a(d, d2, i, 500, false);
    }

    public final List<Address> getFromLocation(double d, double d2, int i, int i2) {
        return a(d, d2, i, i2, false);
    }

    public final List<List<Address>> getFromLocation(double d, double d2, int i, int i2, int i3, int i4) {
        return a(d, d2, i, i2, i3, i4, false);
    }

    public final List<Address> getFromLocationName(String str, int i) {
        return getFromLocationName(str, i, d.a(1000000L), d.a(50000000L), d.a(65000000L), d.a(145000000L));
    }

    public final List<Address> getFromLocationName(String str, int i, double d, double d2, double d3, double d4) {
        return getFromLocationName(str, PoiTypeDef.All, i, d, d2, d3, d4);
    }

    public final List<Address> getFromLocationName(String str, String str2, int i) {
        return getFromLocationName(str, str2, i, d.a(1000000L), d.a(50000000L), d.a(65000000L), d.a(145000000L));
    }

    public final List<Address> getFromLocationName(String str, String str2, int i, double d, double d2, double d3, double d4) {
        if (str == null) {
            throw new IllegalArgumentException("locationName == null");
        }
        if (d.a) {
            if (d < d.a(1000000L) || d > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException lowerLeftLatitude == " + d);
            } else if (d2 < d.a(50000000L) || d2 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException lowerLeftLongitude == " + d2);
            } else if (d3 < d.a(1000000L) || d3 > d.a(65000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException upperRightLatitude == " + d3);
            } else if (d4 < d.a(50000000L) || d4 > d.a(145000000L)) {
                throw new AMapException("无效的参数 - IllegalArgumentException upperRightLongitude == " + d4);
            }
        }
        if (i <= 0) {
            return new ArrayList();
        }
        List<Address> list = (List) new a(new b(str, str2, 15), d.b(this.b), this.a, null).g();
        return d.a ? a(list, d, d2, d3, d4, i) : list;
    }

    public final List<Address> getFromRawGpsLocation(double d, double d2, int i) {
        return m.a(d, d2, this.b, this.a) == null ? new ArrayList() : getFromLocation(d, d2, i);
    }

    public final List<List<Address>> getFromRawGpsLocation(double d, double d2, int i, int i2, int i3, int i4) {
        return m.a(d, d2, this.b, this.a) == null ? new ArrayList() : a(d, d2, i, i2, i3, i4, false);
    }
}
