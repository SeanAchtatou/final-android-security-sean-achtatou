package com.amap.api.search.core;

import android.content.Context;
import android.location.Address;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.amap.api.location.LocationManagerProxy;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public class d {
    public static boolean a = true;
    static float[] b = new float[9];
    private static String c = null;

    public static double a(int i) {
        return ((double) i) / 111700.0d;
    }

    public static double a(long j) {
        return ((double) j) / 1000000.0d;
    }

    public static int a(double d) {
        return (int) (111700.0d * d);
    }

    public static Address a() {
        Address address = new Address(Locale.CHINA);
        address.setCountryCode("CN");
        address.setCountryName("中国");
        return address;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r6) {
        /*
            r0 = 0
            java.lang.String r1 = com.amap.api.search.core.d.c
            if (r1 != 0) goto L_0x008a
            r1 = 16
            char[] r3 = new char[r1]
            r3 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70} // fill-array
            java.lang.String r1 = ""
            android.content.pm.PackageManager r2 = r6.getPackageManager()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r5 = 64
            android.content.pm.PackageInfo r2 = r2.getPackageInfo(r4, r5)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            android.content.pm.Signature[] r2 = r2.signatures     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r4 = "MD5"
            java.security.MessageDigest r4 = java.security.MessageDigest.getInstance(r4)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r5 = 0
            r2 = r2[r5]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            byte[] r2 = r2.toByteArray()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r4.update(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            byte[] r4 = r4.digest()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r2 = r1
            r1 = r0
        L_0x0034:
            int r0 = r4.length     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            if (r1 >= r0) goto L_0x0088
            byte r0 = r4[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            if (r0 >= 0) goto L_0x0085
            byte r0 = r4[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r0 = r0 + 256
        L_0x003f:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r5.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r5 = r0 / 16
            char r5 = r3[r5]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r2 = r2.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r5.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r0 = r0 % 16
            char r0 = r3[r0]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r2 = r4.length     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            int r2 = r2 + -1
            if (r1 == r2) goto L_0x0081
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            r2.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r2 = ":"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
        L_0x0081:
            int r1 = r1 + 1
            r2 = r0
            goto L_0x0034
        L_0x0085:
            byte r0 = r4[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
            goto L_0x003f
        L_0x0088:
            com.amap.api.search.core.d.c = r2     // Catch:{ NoSuchAlgorithmException -> 0x008f, NameNotFoundException -> 0x008d }
        L_0x008a:
            java.lang.String r0 = com.amap.api.search.core.d.c
            return r0
        L_0x008d:
            r0 = move-exception
            goto L_0x008a
        L_0x008f:
            r0 = move-exception
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.search.core.d.a(android.content.Context):java.lang.String");
    }

    public static boolean a(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static Proxy b(Context context) {
        Proxy proxy;
        int defaultPort;
        String str;
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.getType() == 1) {
                    String host = android.net.Proxy.getHost(context);
                    defaultPort = android.net.Proxy.getPort(context);
                    str = host;
                } else {
                    String defaultHost = android.net.Proxy.getDefaultHost();
                    defaultPort = android.net.Proxy.getDefaultPort();
                    str = defaultHost;
                }
                if (str != null) {
                    proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, defaultPort));
                    return proxy;
                }
            }
            proxy = null;
            return proxy;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void b(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has(LocationManagerProxy.KEY_STATUS_CHANGED)) {
                String string = jSONObject.getString(LocationManagerProxy.KEY_STATUS_CHANGED);
                if (string.equals("E6008")) {
                    throw new AMapException("key为空");
                } else if (string.equals("E6012")) {
                    throw new AMapException("key不存在");
                } else if (string.equals("E6018")) {
                    throw new AMapException("key被锁定");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
