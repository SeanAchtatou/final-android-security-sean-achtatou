package com.amap.api.search.core;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.util.zip.GZIPInputStream;

public abstract class l<T, V> {
    protected Proxy a;
    protected T b;
    protected int c = 1;
    protected int d = 20;
    protected int e = 0;
    protected int f = 0;
    protected String g;
    protected String h = PoiTypeDef.All;

    public l(T t, Proxy proxy, String str, String str2) {
        this.a = proxy;
        this.b = t;
        this.c = 1;
        this.d = 5;
        this.e = 2;
        this.g = str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x0073 A[SYNTHETIC, Splitter:B:41:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private V a() {
        /*
            r9 = this;
            r4 = 0
            r0 = 0
            r1 = r4
            r2 = r4
            r3 = r4
        L_0x0005:
            int r5 = r9.c
            if (r0 >= r5) goto L_0x009b
            java.lang.String r5 = r9.e()     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            r9.h = r5     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            byte[] r5 = r9.f()     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            if (r5 != 0) goto L_0x0034
            java.lang.String r5 = r9.h     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            java.net.Proxy r6 = r9.a     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            java.net.HttpURLConnection r3 = com.amap.api.search.core.e.a(r5, r6)     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
        L_0x001d:
            java.io.InputStream r5 = r9.a(r3)     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            java.lang.Object r1 = r9.a(r5)     // Catch:{ AMapException -> 0x009f }
            int r0 = r9.c     // Catch:{ AMapException -> 0x009f }
            if (r5 == 0) goto L_0x00a3
            r5.close()     // Catch:{ IOException -> 0x003d }
            r2 = r4
        L_0x002d:
            if (r3 == 0) goto L_0x0005
            r3.disconnect()
            r3 = r4
            goto L_0x0005
        L_0x0034:
            java.lang.String r6 = r9.h     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            java.net.Proxy r7 = r9.a     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            java.net.HttpURLConnection r3 = com.amap.api.search.core.e.a(r6, r5, r7)     // Catch:{ AMapException -> 0x0046, all -> 0x009c }
            goto L_0x001d
        L_0x003d:
            r0 = move-exception
            com.amap.api.search.core.AMapException r0 = new com.amap.api.search.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            r5 = move-exception
            r8 = r5
            r5 = r2
            r2 = r8
        L_0x004a:
            int r0 = r0 + 1
            int r6 = r9.c     // Catch:{ all -> 0x0070 }
            if (r0 >= r6) goto L_0x007c
            int r2 = r9.e     // Catch:{ InterruptedException -> 0x0065 }
            int r2 = r2 * 1000
            long r6 = (long) r2     // Catch:{ InterruptedException -> 0x0065 }
            java.lang.Thread.sleep(r6)     // Catch:{ InterruptedException -> 0x0065 }
            if (r5 == 0) goto L_0x00a1
            r5.close()     // Catch:{ IOException -> 0x0089 }
            r2 = r4
        L_0x005e:
            if (r3 == 0) goto L_0x0005
            r3.disconnect()
            r3 = r4
            goto L_0x0005
        L_0x0065:
            r0 = move-exception
            com.amap.api.search.core.AMapException r1 = new com.amap.api.search.core.AMapException     // Catch:{ all -> 0x0070 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0070 }
            r1.<init>(r0)     // Catch:{ all -> 0x0070 }
            throw r1     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r0 = move-exception
        L_0x0071:
            if (r5 == 0) goto L_0x0076
            r5.close()     // Catch:{ IOException -> 0x0092 }
        L_0x0076:
            if (r3 == 0) goto L_0x007b
            r3.disconnect()
        L_0x007b:
            throw r0
        L_0x007c:
            r9.h()     // Catch:{ all -> 0x0070 }
            com.amap.api.search.core.AMapException r0 = new com.amap.api.search.core.AMapException     // Catch:{ all -> 0x0070 }
            java.lang.String r1 = r2.getErrorMessage()     // Catch:{ all -> 0x0070 }
            r0.<init>(r1)     // Catch:{ all -> 0x0070 }
            throw r0     // Catch:{ all -> 0x0070 }
        L_0x0089:
            r0 = move-exception
            com.amap.api.search.core.AMapException r0 = new com.amap.api.search.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x0092:
            r0 = move-exception
            com.amap.api.search.core.AMapException r0 = new com.amap.api.search.core.AMapException
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)
            throw r0
        L_0x009b:
            return r1
        L_0x009c:
            r0 = move-exception
            r5 = r2
            goto L_0x0071
        L_0x009f:
            r2 = move-exception
            goto L_0x004a
        L_0x00a1:
            r2 = r5
            goto L_0x005e
        L_0x00a3:
            r2 = r5
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.search.core.l.a():java.lang.Object");
    }

    private V a(InputStream inputStream) {
        return b(inputStream);
    }

    /* access modifiers changed from: protected */
    public InputStream a(HttpURLConnection httpURLConnection) {
        try {
            PushbackInputStream pushbackInputStream = new PushbackInputStream(httpURLConnection.getInputStream(), 2);
            byte[] bArr = new byte[2];
            pushbackInputStream.read(bArr);
            pushbackInputStream.unread(bArr);
            return (bArr[0] == 31 && bArr[1] == -117) ? new GZIPInputStream(pushbackInputStream) : pushbackInputStream;
        } catch (ProtocolException e2) {
            throw new AMapException(AMapException.ERROR_PROTOCOL);
        } catch (UnknownHostException e3) {
            throw new AMapException(AMapException.ERROR_UNKNOW_HOST);
        } catch (UnknownServiceException e4) {
            throw new AMapException(AMapException.ERROR_UNKNOW_SERVICE);
        } catch (IOException e5) {
            throw new AMapException(AMapException.ERROR_IO);
        }
    }

    /* access modifiers changed from: protected */
    public abstract V b(InputStream inputStream);

    /* access modifiers changed from: protected */
    public abstract byte[] d();

    /* access modifiers changed from: protected */
    public abstract String e();

    /* access modifiers changed from: protected */
    public byte[] f() {
        return d();
    }

    public V g() {
        if (this.b != null) {
            return a();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public V h() {
        return null;
    }
}
