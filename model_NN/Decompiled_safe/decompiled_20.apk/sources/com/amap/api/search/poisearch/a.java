package com.amap.api.search.poisearch;

import android.os.Parcel;
import android.os.Parcelable;

final class a implements Parcelable.Creator<PoiItem> {
    a() {
    }

    /* renamed from: a */
    public final PoiItem createFromParcel(Parcel parcel) {
        return new PoiItem(parcel, null);
    }

    /* renamed from: a */
    public final PoiItem[] newArray(int i) {
        return new PoiItem[i];
    }
}
