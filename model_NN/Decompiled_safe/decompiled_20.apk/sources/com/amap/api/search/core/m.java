package com.amap.api.search.core;

import android.content.Context;

public class m {
    public static LatLonPoint a(double d, double d2, Context context, String str) {
        try {
            LatLonPoint latLonPoint = (LatLonPoint) new j(new LatLonPoint(d, d2), d.b(context), str, null).g();
            if (Double.valueOf(0.0d).doubleValue() == latLonPoint.getLatitude() && Double.valueOf(0.0d).doubleValue() == latLonPoint.getLongitude()) {
                return null;
            }
            return latLonPoint != null ? new LatLonPoint(latLonPoint.getLatitude(), latLonPoint.getLongitude()) : null;
        } catch (Exception e) {
            return null;
        }
    }
}
