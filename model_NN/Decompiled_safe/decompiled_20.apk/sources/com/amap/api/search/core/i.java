package com.amap.api.search.core;

import android.os.Parcel;
import android.os.Parcelable;

final class i implements Parcelable.Creator<LatLonPoint> {
    i() {
    }

    /* renamed from: a */
    public final LatLonPoint createFromParcel(Parcel parcel) {
        return new LatLonPoint(parcel, (i) null);
    }

    /* renamed from: a */
    public final LatLonPoint[] newArray(int i) {
        return new LatLonPoint[i];
    }
}
