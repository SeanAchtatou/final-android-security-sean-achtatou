package com.amap.api.search.busline;

import android.content.Context;
import com.amap.api.search.core.b;
import com.amap.api.search.core.d;
import java.util.ArrayList;

public class BusSearch {
    private Context a;
    private BusQuery b;
    private int c = 10;

    public BusSearch(Context context, BusQuery busQuery) {
        b.a(context);
        this.a = context;
        this.b = busQuery;
    }

    public BusSearch(Context context, String str, BusQuery busQuery) {
        b.a(context);
        this.a = context;
        this.b = busQuery;
    }

    public BusQuery getQuery() {
        return this.b;
    }

    public BusPagedResult searchBusLine() {
        a aVar = new a(this.b, d.b(this.a), d.a(this.a), null);
        aVar.a(1);
        aVar.b(this.c);
        return BusPagedResult.a(aVar, (ArrayList) aVar.g());
    }

    public void setPageSize(int i) {
        this.c = i;
    }

    public void setQuery(BusQuery busQuery) {
        this.b = busQuery;
    }
}
