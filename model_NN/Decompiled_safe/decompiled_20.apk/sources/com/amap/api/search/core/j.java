package com.amap.api.search.core;

import java.io.InputStream;
import java.net.Proxy;
import org.json.JSONException;
import org.json.JSONObject;

public class j extends l<LatLonPoint, LatLonPoint> {
    private LatLonPoint i;

    public j(LatLonPoint latLonPoint, Proxy proxy, String str, String str2) {
        super(latLonPoint, proxy, str, str2);
        this.i = latLonPoint;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public LatLonPoint b(InputStream inputStream) {
        String str;
        LatLonPoint latLonPoint = this.i;
        try {
            str = new String(a.a(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
            str = null;
        }
        d.b(str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("xys")) {
                String[] split = jSONObject.getString("xys").split(",");
                for (int i2 = 0; i2 < split.length; i2++) {
                    latLonPoint.setLongitude(Double.parseDouble(split[0]));
                    latLonPoint.setLatitude(Double.parseDouble(split[1]));
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return latLonPoint;
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        sb.append("sid=15001&resType=json&ia=1&encode=utf-8&xys=");
        sb.append(((LatLonPoint) this.b).getLongitude());
        sb.append(",");
        sb.append(((LatLonPoint) this.b).getLatitude());
        sb.append("&key=" + b.a);
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String e() {
        return k.a().b();
    }
}
