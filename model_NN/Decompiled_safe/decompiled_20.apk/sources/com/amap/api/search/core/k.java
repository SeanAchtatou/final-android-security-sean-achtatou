package com.amap.api.search.core;

public class k {
    private static k b;
    private String a = "http://restapi.amap.com";

    private k() {
    }

    public static synchronized k a() {
        k kVar;
        synchronized (k.class) {
            if (b == null) {
                b = new k();
            }
            kVar = b;
        }
        return kVar;
    }

    public String b() {
        return this.a;
    }
}
