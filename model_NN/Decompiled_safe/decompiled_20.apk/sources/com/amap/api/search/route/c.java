package com.amap.api.search.route;

import com.amap.api.search.core.k;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;

abstract class c extends d {
    public c(e eVar, Proxy proxy, String str, String str2) {
        super(eVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ArrayList<Route> b(InputStream inputStream) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(Route route) {
        for (int stepCount = route.getStepCount() - 1; stepCount > 0; stepCount--) {
            DriveSegment driveSegment = (DriveSegment) route.getStep(stepCount);
            DriveSegment driveSegment2 = (DriveSegment) route.getStep(stepCount - 1);
            driveSegment.setActionCode(driveSegment2.getActionCode());
            driveSegment.setActionDescription(driveSegment2.getActionDescription());
        }
        DriveSegment driveSegment3 = (DriveSegment) route.getStep(0);
        driveSegment3.setActionCode(-1);
        driveSegment3.setActionDescription(PoiTypeDef.All);
    }

    /* access modifiers changed from: protected */
    public String e() {
        return k.a().b() + "/route/simple";
    }
}
