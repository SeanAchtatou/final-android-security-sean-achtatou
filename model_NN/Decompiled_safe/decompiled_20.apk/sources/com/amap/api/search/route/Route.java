package com.amap.api.search.route;

import android.content.Context;
import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.net.Proxy;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

public class Route {
    public static final int BusDefault = 0;
    public static final int BusLeaseChange = 2;
    public static final int BusLeaseWalk = 3;
    public static final int BusMostComfortable = 4;
    public static final int BusSaveMoney = 1;
    public static final int DrivingDefault = 10;
    public static final int DrivingLeastDistance = 12;
    public static final int DrivingNoFastRoad = 13;
    public static final int DrivingSaveMoney = 11;
    public static final int WalkDefault = 23;
    private LatLonPoint a = null;
    private LatLonPoint b = null;
    private int c;
    public d mHelper;
    protected List<Segment> mSegs;
    protected String mStartPlace;
    protected String mTargetPlace;

    public class FromAndTo {
        public LatLonPoint mFrom;
        public LatLonPoint mTo;

        public FromAndTo(LatLonPoint latLonPoint, LatLonPoint latLonPoint2) {
            this.mFrom = latLonPoint;
            this.mTo = latLonPoint2;
        }
    }

    public class a extends d {
        public a() {
            super();
        }

        private String b(int i) {
            StringBuilder sb = new StringBuilder();
            sb.append("步行去往");
            if (i == Route.this.getStepCount() - 1) {
                sb.append("目的地");
            } else {
                sb.append(((BusSegment) Route.this.getStep(i + 1)).getLineName() + "车站");
            }
            sb.append("\n大约" + Route.a(Route.this.getStep(i).getLength()));
            return sb.toString();
        }

        private String c(int i) {
            BusSegment busSegment = (BusSegment) Route.this.getStep(i);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(String.format("%s ( %s -- %s ) - %s%s\n", busSegment.getLineName(), busSegment.getFirstStationName(), busSegment.getLastStationName(), busSegment.getLastStationName(), "方向"));
            stringBuffer.append("上车 : " + busSegment.getOnStationName() + "\n");
            stringBuffer.append("下车 : " + busSegment.getOffStationName() + "\n");
            stringBuffer.append(String.format("%s%d%s (%s)", "公交", Integer.valueOf(busSegment.getStopNumber() - 1), "站", "大约" + Route.a(busSegment.getLength())));
            return stringBuffer.toString();
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            int stepCount = Route.this.getStepCount();
            int i = 0;
            for (int i2 = 1; i2 < stepCount; i2 += 2) {
                BusSegment busSegment = (BusSegment) Route.this.getStep(i2);
                if (i2 != 1) {
                    sb.append(" -> ");
                }
                sb.append(busSegment.getLineName());
                i += busSegment.getLength();
            }
            if (i != 0) {
                sb.append("\n");
            }
            int i3 = 0;
            for (int i4 = 0; i4 < stepCount; i4 += 2) {
                i3 += Route.this.getStep(i4).getLength();
            }
            sb.append(String.format("%s%s  %s%s", "乘车", Route.a(i), "步行", Route.a(i3)));
            return sb.toString();
        }

        public String a(int i) {
            String a2 = super.a(i);
            return a2 != null ? a2 : Route.this.getStep(i) instanceof BusSegment ? c(i) : b(i);
        }
    }

    class b extends e {
        b() {
            super();
        }
    }

    class c extends e {
        c() {
            super();
        }
    }

    public abstract class d {
        public d() {
        }

        private String b(int i) {
            if (i == 0) {
                return Route.this.mStartPlace;
            }
            if (i == Route.this.getStepCount()) {
                return Route.this.mTargetPlace;
            }
            return null;
        }

        public abstract String a();

        public String a(int i) {
            return b(i);
        }
    }

    abstract class e extends d {
        e() {
            super();
        }

        public String a() {
            String str;
            StringBuffer stringBuffer = new StringBuffer();
            String str2 = PoiTypeDef.All;
            int stepCount = Route.this.getStepCount();
            int i = 0;
            int i2 = 0;
            while (i < stepCount) {
                DriveWalkSegment driveWalkSegment = (DriveWalkSegment) Route.this.getStep(i);
                i2 += driveWalkSegment.getLength();
                if (com.amap.api.search.core.d.a(driveWalkSegment.getRoadName()) || driveWalkSegment.getRoadName().equals(str2)) {
                    str = str2;
                } else {
                    if (!com.amap.api.search.core.d.a(stringBuffer.toString())) {
                        stringBuffer.append(" -> ");
                    }
                    stringBuffer.append(driveWalkSegment.getRoadName());
                    str = driveWalkSegment.getRoadName();
                }
                i++;
                str2 = str;
            }
            if (!com.amap.api.search.core.d.a(stringBuffer.toString())) {
                stringBuffer.append("\n");
            }
            stringBuffer.append(String.format("%s", "大约" + Route.a(i2)));
            return stringBuffer.toString();
        }

        public String a(int i) {
            String a = super.a(i);
            if (a != null) {
                return a;
            }
            String str = PoiTypeDef.All;
            DriveWalkSegment driveWalkSegment = (DriveWalkSegment) Route.this.getStep(i);
            if (!com.amap.api.search.core.d.a(driveWalkSegment.getRoadName())) {
                str = driveWalkSegment.getRoadName() + " ";
            }
            return (str + driveWalkSegment.getActionDescription() + " ") + String.format("%s%s", "大约", Route.a(driveWalkSegment.getLength()));
        }
    }

    public Route(int i) {
        this.c = i;
        if (isBus(i)) {
            this.mHelper = new a();
        } else if (isDrive(i)) {
            this.mHelper = new b();
        } else if (isWalk(i)) {
            this.mHelper = new c();
        } else {
            throw new IllegalArgumentException("Unkown mode");
        }
    }

    static String a(int i) {
        if (i > 10000) {
            return (i / 1000) + "公里";
        } else if (i > 1000) {
            return new DecimalFormat("##0.0").format((double) (((float) i) / 1000.0f)) + "公里";
        } else if (i > 100) {
            return ((i / 50) * 50) + "米";
        } else {
            int i2 = (i / 10) * 10;
            if (i2 == 0) {
                i2 = 10;
            }
            return i2 + "米";
        }
    }

    private void b() {
        double d2 = Double.MAX_VALUE;
        double d3 = Double.MAX_VALUE;
        for (Segment lowerLeftPoint : this.mSegs) {
            LatLonPoint lowerLeftPoint2 = lowerLeftPoint.getLowerLeftPoint();
            double longitude = lowerLeftPoint2.getLongitude();
            double latitude = lowerLeftPoint2.getLatitude();
            if (longitude < d2) {
                d2 = longitude;
            }
            d3 = latitude < d3 ? latitude : d3;
        }
        double d4 = Double.MIN_VALUE;
        double d5 = Double.MIN_VALUE;
        for (Segment upperRightPoint : this.mSegs) {
            LatLonPoint upperRightPoint2 = upperRightPoint.getUpperRightPoint();
            double longitude2 = upperRightPoint2.getLongitude();
            double latitude2 = upperRightPoint2.getLatitude();
            if (longitude2 > d5) {
                d5 = longitude2;
            }
            if (latitude2 > d4) {
                d4 = latitude2;
            }
        }
        this.a = new LatLonPoint(d3, d2);
        this.b = new LatLonPoint(d4, d5);
    }

    public static List<Route> calculateRoute(Context context, FromAndTo fromAndTo, int i) {
        com.amap.api.search.core.b.a(context);
        e eVar = new e(fromAndTo, i);
        Proxy b2 = com.amap.api.search.core.d.b(context);
        String a2 = com.amap.api.search.core.d.a(context);
        return (List) (isBus(i) ? new a(eVar, b2, a2, null) : isWalk(i) ? new f(eVar, b2, a2, null) : new b(eVar, b2, a2, null)).g();
    }

    public static List<Route> calculateRoute(Context context, FromAndTo fromAndTo, int i, List<LatLonPoint> list) {
        com.amap.api.search.core.b.a(context);
        e eVar = new e(fromAndTo, i);
        Proxy b2 = com.amap.api.search.core.d.b(context);
        String a2 = com.amap.api.search.core.d.a(context);
        d aVar = isBus(i) ? new a(eVar, b2, a2, null) : isWalk(i) ? new f(eVar, b2, a2, null) : new b(eVar, b2, a2, null);
        aVar.a(list);
        return (List) aVar.g();
    }

    public static boolean isBus(int i) {
        return i >= 0 && i <= 4;
    }

    public static boolean isDrive(int i) {
        return i >= 10 && i <= 13;
    }

    public static boolean isWalk(int i) {
        return i == 23;
    }

    /* access modifiers changed from: package-private */
    public List<Segment> a() {
        return this.mSegs;
    }

    /* access modifiers changed from: package-private */
    public void a(List<Segment> list) {
        this.mSegs = list;
    }

    public int getLength() {
        int i = 0;
        Iterator<Segment> it = this.mSegs.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().getLength() + i2;
        }
    }

    public LatLonPoint getLowerLeftPoint() {
        if (this.a == null) {
            b();
        }
        return this.a;
    }

    public int getMode() {
        return this.c;
    }

    public String getOverview() {
        return this.mHelper.a();
    }

    public int getSegmentIndex(Segment segment) {
        return this.mSegs.indexOf(segment);
    }

    public String getStartPlace() {
        return this.mStartPlace;
    }

    public LatLonPoint getStartPos() {
        return this.mSegs.get(0).getFirstPoint();
    }

    public Segment getStep(int i) {
        return this.mSegs.get(i);
    }

    public int getStepCount() {
        return this.mSegs.size();
    }

    public String getStepedDescription(int i) {
        return this.mHelper.a(i);
    }

    public String getTargetPlace() {
        return this.mTargetPlace;
    }

    public LatLonPoint getTargetPos() {
        return this.mSegs.get(getStepCount() - 1).getLastPoint();
    }

    public LatLonPoint getUpperRightPoint() {
        if (this.b == null) {
            b();
        }
        return this.b;
    }

    public void setStartPlace(String str) {
        this.mStartPlace = str;
    }

    public void setTargetPlace(String str) {
        this.mTargetPlace = str;
    }
}
