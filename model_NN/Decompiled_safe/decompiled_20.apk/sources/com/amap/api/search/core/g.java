package com.amap.api.search.core;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;
import java.net.URLEncoder;
import java.util.ArrayList;

public class g extends l<h, ArrayList<String>> {
    public g(h hVar, Proxy proxy, String str, String str2) {
        super(hVar, proxy, str, str2);
    }

    private boolean a(String str) {
        return str == null || str.equals(PoiTypeDef.All);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022 A[Catch:{ JSONException -> 0x0041 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<java.lang.String> b(java.io.InputStream r6) {
        /*
            r5 = this;
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r1 = 0
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0039 }
            byte[] r3 = com.amap.api.search.core.a.a(r6)     // Catch:{ Exception -> 0x0039 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0039 }
            com.amap.api.search.core.d.b(r0)     // Catch:{ Exception -> 0x0046 }
        L_0x0012:
            com.amap.api.search.core.d.b(r0)
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0041 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0041 }
            java.lang.String r0 = "list"
            boolean r0 = r1.has(r0)     // Catch:{ JSONException -> 0x0041 }
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = "list"
            org.json.JSONArray r1 = r1.getJSONArray(r0)     // Catch:{ JSONException -> 0x0041 }
            r0 = 0
        L_0x0029:
            int r3 = r1.length()     // Catch:{ JSONException -> 0x0041 }
            if (r0 >= r3) goto L_0x0045
            java.lang.String r3 = r1.getString(r0)     // Catch:{ JSONException -> 0x0041 }
            r2.add(r3)     // Catch:{ JSONException -> 0x0041 }
            int r0 = r0 + 1
            goto L_0x0029
        L_0x0039:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x003d:
            r1.printStackTrace()
            goto L_0x0012
        L_0x0041:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0045:
            return r2
        L_0x0046:
            r1 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.search.core.g.b(java.io.InputStream):java.util.ArrayList");
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        StringBuilder sb = new StringBuilder();
        sb.append("sid=1014");
        sb.append("&ia=1");
        sb.append("&keyword=");
        try {
            sb.append(URLEncoder.encode(((h) this.b).a, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sb.append("&city=");
        String str = ((h) this.b).b;
        if (a(str)) {
            sb.append("total");
        } else {
            try {
                sb.append(URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
        sb.append("&encode=utf-8");
        sb.append("&resType=json");
        sb.append("&key=" + b.a);
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: protected */
    public String e() {
        return k.a().b() + "/gss/simple";
    }
}
