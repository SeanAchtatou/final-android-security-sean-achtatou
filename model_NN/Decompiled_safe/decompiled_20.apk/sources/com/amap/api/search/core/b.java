package com.amap.api.search.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.amap.api.search.poisearch.PoiTypeDef;

public class b {
    public static String a = PoiTypeDef.All;
    private static b b = null;
    private static String c = null;
    private static Context d = null;
    private static TelephonyManager e;
    private static ConnectivityManager f;
    private static String g;
    private static String h;

    private b() {
    }

    public static b a(Context context) {
        if (b == null) {
            b = new b();
            d = context;
            e = (TelephonyManager) context.getSystemService("phone");
            f = (ConnectivityManager) d.getSystemService("connectivity");
            g = d.getApplicationContext().getPackageName();
            h = e();
            a = b(d);
        }
        return b;
    }

    public static String b(Context context) {
        if (a == null || a.equals(PoiTypeDef.All)) {
            try {
                a = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("com.amap.api.v2.apikey");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return a;
    }

    private static String e() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) d.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.widthPixels;
        int i2 = displayMetrics.heightPixels;
        String str = i2 > i ? i + "*" + i2 : i2 + "*" + i;
        h = str;
        return str;
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append("ia=1&");
        if (a != null && a.length() > 0) {
            sb.append("key=");
            sb.append(a);
            sb.append("&");
        }
        sb.append("ct=android");
        String deviceId = e.getDeviceId();
        String subscriberId = e.getSubscriberId();
        sb.append("&ime=" + deviceId);
        sb.append("&sim=" + subscriberId);
        sb.append("&pkg=" + g);
        sb.append("&mod=");
        sb.append(c());
        sb.append("&sv=");
        sb.append(b());
        sb.append("&nt=");
        sb.append(d());
        String networkOperatorName = e.getNetworkOperatorName();
        sb.append("&np=");
        sb.append(networkOperatorName);
        sb.append("&ctm=" + System.currentTimeMillis());
        sb.append("&re=" + h);
        sb.append("&av=" + c.c);
        sb.append("&pro=sea");
        return sb.toString();
    }

    public String b() {
        return Build.VERSION.RELEASE;
    }

    public String c() {
        return Build.MODEL;
    }

    public String d() {
        NetworkInfo activeNetworkInfo;
        return (d.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0 || f == null || (activeNetworkInfo = f.getActiveNetworkInfo()) == null) ? PoiTypeDef.All : activeNetworkInfo.getTypeName();
    }
}
