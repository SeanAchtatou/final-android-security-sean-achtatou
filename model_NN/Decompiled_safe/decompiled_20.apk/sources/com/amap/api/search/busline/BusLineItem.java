package com.amap.api.search.busline;

import com.amap.api.search.core.LatLonPoint;
import java.util.ArrayList;

public class BusLineItem {
    private ArrayList<BusStationItem> A;
    private LatLonPoint B = null;
    private LatLonPoint C = null;
    private float a;
    private String b;
    private int c;
    private String d;
    private int e;
    private float f;
    private ArrayList<LatLonPoint> g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private float o;
    private float p;
    private boolean q;
    private boolean r;
    private boolean s;
    private boolean t;
    private boolean u;
    private int v;
    private boolean w;
    private String x;
    private String y;
    private boolean z;

    private void a() {
        if (this.g != null && this.g.size() != 0) {
            double d2 = Double.MIN_VALUE;
            double d3 = Double.MAX_VALUE;
            double d4 = Double.MAX_VALUE;
            double d5 = Double.MIN_VALUE;
            for (int i2 = 0; i2 < this.g.size(); i2++) {
                LatLonPoint latLonPoint = this.g.get(i2);
                double longitude = latLonPoint.getLongitude();
                double latitude = latLonPoint.getLatitude();
                if (longitude < d4) {
                    d4 = longitude;
                }
                if (latitude < d3) {
                    d3 = latitude;
                }
                if (longitude > d5) {
                    d5 = longitude;
                }
                if (latitude > d2) {
                    d2 = latitude;
                }
            }
            this.B = new LatLonPoint(d3, d4);
            this.C = new LatLonPoint(d2, d5);
        }
    }

    public LatLonPoint getLowerLeftPoint() {
        if (this.B == null) {
            a();
        }
        return this.B;
    }

    public LatLonPoint getUpperRightPoint() {
        if (this.C == null) {
            a();
        }
        return this.C;
    }

    public boolean getmAir() {
        return this.w;
    }

    public boolean getmAuto() {
        return this.r;
    }

    public float getmBasicPrice() {
        return this.o;
    }

    public boolean getmCommunicationTicket() {
        return this.q;
    }

    public String getmCompany() {
        return this.n;
    }

    public int getmDataSource() {
        return this.v;
    }

    public String getmDescription() {
        return this.d;
    }

    public String getmEndTime() {
        return this.m;
    }

    public String getmFrontName() {
        return this.j;
    }

    public String getmFrontSpell() {
        return this.x;
    }

    public String getmKeyName() {
        return this.i;
    }

    public float getmLength() {
        return this.a;
    }

    public String getmLineId() {
        return this.h;
    }

    public String getmName() {
        return this.b;
    }

    public float getmSpeed() {
        return this.f;
    }

    public String getmStartTime() {
        return this.l;
    }

    public ArrayList<BusStationItem> getmStations() {
        return this.A;
    }

    public int getmStatus() {
        return this.e;
    }

    public String getmTerminalName() {
        return this.k;
    }

    public String getmTerminalSpell() {
        return this.y;
    }

    public float getmTotalPrice() {
        return this.p;
    }

    public int getmType() {
        return this.c;
    }

    public ArrayList<LatLonPoint> getmXys() {
        return this.g;
    }

    public boolean ismDoubleDeck() {
        return this.u;
    }

    public boolean ismExpressWay() {
        return this.z;
    }

    public boolean ismIcCard() {
        return this.s;
    }

    public boolean ismLoop() {
        return this.t;
    }

    public void setmAir(boolean z2) {
        this.w = z2;
    }

    public void setmAuto(boolean z2) {
        this.r = z2;
    }

    public void setmBasicPrice(float f2) {
        this.o = f2;
    }

    public void setmCommunicationTicket(boolean z2) {
        this.q = z2;
    }

    public void setmCompany(String str) {
        this.n = str;
    }

    public void setmDataSource(int i2) {
        this.v = i2;
    }

    public void setmDescription(String str) {
        this.d = str;
    }

    public void setmDoubleDeck(boolean z2) {
        this.u = z2;
    }

    public void setmEndTime(String str) {
        this.m = str;
    }

    public void setmExpressWay(boolean z2) {
        this.z = z2;
    }

    public void setmFrontName(String str) {
        this.j = str;
    }

    public void setmFrontSpell(String str) {
        this.x = str;
    }

    public void setmIcCard(boolean z2) {
        this.s = z2;
    }

    public void setmKeyName(String str) {
        this.i = str;
    }

    public void setmLength(float f2) {
        this.a = f2;
    }

    public void setmLineId(String str) {
        this.h = str;
    }

    public void setmLoop(boolean z2) {
        this.t = z2;
    }

    public void setmName(String str) {
        this.b = str;
    }

    public void setmSpeed(float f2) {
        this.f = f2;
    }

    public void setmStartTime(String str) {
        this.l = str;
    }

    public void setmStations(ArrayList<BusStationItem> arrayList) {
        this.A = arrayList;
    }

    public void setmStatus(int i2) {
        this.e = i2;
    }

    public void setmTerminalName(String str) {
        this.k = str;
    }

    public void setmTerminalSpell(String str) {
        this.y = str;
    }

    public void setmTotalPrice(float f2) {
        this.p = f2;
    }

    public void setmType(int i2) {
        this.c = i2;
    }

    public void setmXys(ArrayList<LatLonPoint> arrayList) {
        this.g = arrayList;
    }

    public String toString() {
        return this.b + " " + this.l + "-" + this.m;
    }
}
