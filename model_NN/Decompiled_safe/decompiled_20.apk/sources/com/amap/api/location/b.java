package com.amap.api.location;

import android.content.Context;
import android.os.Bundle;
import com.amap.api.location.a;
import com.amap.api.location.core.d;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.autonavi.aps.amapapi.AmapLoc;
import com.autonavi.aps.amapapi.Factory;
import com.autonavi.aps.amapapi.IAPS;

public class b implements Runnable {
    private static volatile boolean b = true;
    private static b e;
    private IAPS a = null;
    private Thread c = null;
    private Context d;
    private long f = 2000;
    private long g = 60000;
    private int h;
    private a.C0001a i;

    protected b(Context context, a.C0001a aVar) {
        this.d = context;
        this.a = Factory.getInstance();
        d.a(context);
        this.a.init(context);
        this.a.setAuth("yun_droid_mapsdk##32M0145A3D7E1266UY6BC6E017AD2387##" + d.b(context) + "," + d.a());
        this.i = aVar;
    }

    private AMapLocation a(AmapLoc amapLoc) {
        AMapLocation aMapLocation = new AMapLocation(PoiTypeDef.All);
        aMapLocation.setProvider(LocationProviderProxy.AMapNetwork);
        aMapLocation.setLatitude(amapLoc.getLat());
        aMapLocation.setLongitude(amapLoc.getLon());
        aMapLocation.setAccuracy(amapLoc.getAccuracy());
        aMapLocation.setTime(amapLoc.getTime());
        Bundle bundle = new Bundle();
        bundle.putString("citycode", amapLoc.getCitycode());
        bundle.putString("desc", amapLoc.getDesc());
        bundle.putString("adcode", amapLoc.getAdcode());
        aMapLocation.setExtras(bundle);
        try {
            a(aMapLocation, amapLoc.getCitycode(), amapLoc.getAdcode(), amapLoc.getDesc());
        } catch (Exception e2) {
        }
        return aMapLocation;
    }

    public static b a(Context context, a.C0001a aVar) {
        if (e == null) {
            e = new b(context, aVar);
        }
        b = true;
        return e;
    }

    private void a(AMapLocation aMapLocation, String str, String str2, String str3) {
        String[] split = str3.split(" ");
        aMapLocation.d(str);
        aMapLocation.e(str2);
        if (str.equals(PoiTypeDef.All) || !a(str)) {
            if (split.length > 3) {
                aMapLocation.a(split[0]);
                aMapLocation.b(split[1]);
                aMapLocation.c(split[2]);
            }
        } else if (split.length > 2) {
            aMapLocation.b(split[0]);
            aMapLocation.c(split[1]);
        }
    }

    private boolean a(String str) {
        return str.endsWith("010") || str.endsWith("021") || str.endsWith("022") || str.endsWith("023");
    }

    private AmapLoc b() {
        if (com.amap.api.location.core.b.a == -1) {
            if (com.amap.api.location.core.b.a(this.d)) {
                return c();
            }
            return null;
        } else if (com.amap.api.location.core.b.a == 1) {
            return c();
        } else {
            return null;
        }
    }

    private AmapLoc c() {
        AmapLoc amapLoc = null;
        try {
            amapLoc = this.a.getLocation();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        d();
        return amapLoc;
    }

    private void d() {
        int i2 = com.amap.api.location.core.b.c + 1;
        com.amap.api.location.core.b.c = i2;
        if (i2 == 1000) {
            com.amap.api.location.core.b.a = -1;
            com.amap.api.location.core.b.c = 0;
        }
    }

    private boolean e() {
        if (System.currentTimeMillis() - a.c <= 5 * this.f) {
            return false;
        }
        a.b = false;
        return true;
    }

    public void a() {
        b = false;
        if (this.c != null) {
            this.c.interrupt();
        }
        this.a = null;
        e = null;
    }

    public void a(long j) {
        if (j > this.f) {
            this.f = j;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        java.lang.Thread.sleep(r3.f);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0051, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        java.lang.Thread.sleep(r3.f);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ad, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0049 A[ExcHandler: a (e com.amap.api.location.core.a), Splitter:B:7:0x0015] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00a5 A[ExcHandler: all (r0v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:7:0x0015] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r3 = this;
            int r0 = r3.h
            r1 = 1
            if (r0 != r1) goto L_0x000a
            long r0 = r3.g     // Catch:{ Exception -> 0x0037 }
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0037 }
        L_0x000a:
            boolean r0 = com.amap.api.location.b.b
            if (r0 == 0) goto L_0x00b5
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r3.c = r0
            r0 = 0
            boolean r1 = com.amap.api.location.a.b     // Catch:{ a -> 0x0049, Exception -> 0x0093, all -> 0x00a5 }
            if (r1 == 0) goto L_0x001f
            boolean r1 = r3.e()     // Catch:{ a -> 0x0049, Exception -> 0x0093, all -> 0x00a5 }
            if (r1 == 0) goto L_0x0023
        L_0x001f:
            boolean r1 = com.amap.api.location.a.d     // Catch:{ a -> 0x0049, Exception -> 0x0093, all -> 0x00a5 }
            if (r1 != 0) goto L_0x0059
        L_0x0023:
            long r0 = r3.f     // Catch:{ Exception -> 0x0040, a -> 0x0049, all -> 0x00a5 }
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0040, a -> 0x0049, all -> 0x00a5 }
        L_0x0028:
            long r0 = r3.f     // Catch:{ Exception -> 0x002e }
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x002e }
            goto L_0x000a
        L_0x002e:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            goto L_0x000a
        L_0x0037:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            goto L_0x000a
        L_0x0040:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ a -> 0x0049, Exception -> 0x0093, all -> 0x00a5 }
            r0.interrupt()     // Catch:{ a -> 0x0049, Exception -> 0x0093, all -> 0x00a5 }
            goto L_0x0028
        L_0x0049:
            r0 = move-exception
            long r0 = r3.f     // Catch:{ Exception -> 0x0050 }
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0050 }
            goto L_0x000a
        L_0x0050:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            goto L_0x000a
        L_0x0059:
            com.autonavi.aps.amapapi.AmapLoc r1 = r3.b()     // Catch:{ a -> 0x0049, Exception -> 0x0093, all -> 0x00a5 }
            if (r1 == 0) goto L_0x0063
            com.amap.api.location.AMapLocation r0 = r3.a(r1)     // Catch:{ a -> 0x0049, Exception -> 0x0093, all -> 0x00a5 }
        L_0x0063:
            if (r0 == 0) goto L_0x0083
            boolean r1 = com.amap.api.location.a.d
            if (r1 == 0) goto L_0x0083
            boolean r1 = com.amap.api.location.a.b
            if (r1 == 0) goto L_0x0073
            boolean r1 = r3.e()
            if (r1 == 0) goto L_0x0083
        L_0x0073:
            android.os.Message r1 = new android.os.Message
            r1.<init>()
            r1.obj = r0
            int r0 = com.amap.api.location.a.a
            r1.what = r0
            com.amap.api.location.a$a r0 = r3.i
            r0.sendMessage(r1)
        L_0x0083:
            long r0 = r3.f     // Catch:{ Exception -> 0x0089 }
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0089 }
            goto L_0x000a
        L_0x0089:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            goto L_0x000a
        L_0x0093:
            r0 = move-exception
            long r0 = r3.f     // Catch:{ Exception -> 0x009b }
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x009b }
            goto L_0x000a
        L_0x009b:
            r0 = move-exception
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
            goto L_0x000a
        L_0x00a5:
            r0 = move-exception
            long r1 = r3.f     // Catch:{ Exception -> 0x00ac }
            java.lang.Thread.sleep(r1)     // Catch:{ Exception -> 0x00ac }
        L_0x00ab:
            throw r0
        L_0x00ac:
            r1 = move-exception
            java.lang.Thread r1 = java.lang.Thread.currentThread()
            r1.interrupt()
            goto L_0x00ab
        L_0x00b5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.location.b.run():void");
    }
}
