package com.amap.api.location;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import com.amap.api.location.core.d;
import com.amap.api.location.core.f;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class LocationManagerProxy {
    public static final String GPS_PROVIDER = "gps";
    public static final String KEY_LOCATION_CHANGED = "location";
    public static final String KEY_PROVIDER_ENABLED = "providerEnabled";
    public static final String KEY_PROXIMITY_ENTERING = "entering";
    public static final String KEY_STATUS_CHANGED = "status";
    public static final String NETWORK_PROVIDER = "network";
    private static LocationManagerProxy b = null;
    private LocationManager a = null;
    private a c = null;
    /* access modifiers changed from: private */
    public Context d;
    private e e;
    private b f;
    /* access modifiers changed from: private */
    public ArrayList<PendingIntent> g = new ArrayList<>();
    private Hashtable<String, LocationProviderProxy> h = new Hashtable<>();
    private String i;
    /* access modifiers changed from: private */
    public double j;
    /* access modifiers changed from: private */
    public double k;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public long m = 0;
    /* access modifiers changed from: private */
    public double n = 0.0d;
    private e o;
    private a p;
    /* access modifiers changed from: private */
    public ArrayList<PendingIntent> q = new ArrayList<>();
    private d r;
    private String s;

    class a implements AMapLocationListener {
        a() {
        }

        public void onLocationChanged(Location location) {
            if (LocationManagerProxy.this.l && LocationManagerProxy.this.q.size() > 0) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                double abs = Math.abs(((latitude - LocationManagerProxy.this.j) * (latitude - LocationManagerProxy.this.j)) + ((longitude - LocationManagerProxy.this.k) * (longitude - LocationManagerProxy.this.k)));
                Iterator it = LocationManagerProxy.this.q.iterator();
                while (it.hasNext()) {
                    PendingIntent pendingIntent = (PendingIntent) it.next();
                    if (f.a() > LocationManagerProxy.this.m && LocationManagerProxy.this.m != 0) {
                        LocationManagerProxy.this.removeProximityAlert(pendingIntent);
                    } else if (Math.abs(abs - (LocationManagerProxy.this.n * LocationManagerProxy.this.n)) < 0.5d) {
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, location);
                        intent.putExtras(bundle);
                        try {
                            pendingIntent.send(LocationManagerProxy.this.d, 0, intent);
                        } catch (PendingIntent.CanceledException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        public void onLocationChanged(AMapLocation aMapLocation) {
            if (LocationManagerProxy.this.l && LocationManagerProxy.this.q.size() > 0) {
                double latitude = aMapLocation.getLatitude();
                double longitude = aMapLocation.getLongitude();
                double abs = Math.abs(((latitude - LocationManagerProxy.this.j) * (latitude - LocationManagerProxy.this.j)) + ((longitude - LocationManagerProxy.this.k) * (longitude - LocationManagerProxy.this.k)));
                Iterator it = LocationManagerProxy.this.q.iterator();
                while (it.hasNext()) {
                    PendingIntent pendingIntent = (PendingIntent) it.next();
                    if (f.a() > LocationManagerProxy.this.m && LocationManagerProxy.this.m != 0) {
                        LocationManagerProxy.this.removeProximityAlert(pendingIntent);
                    } else if (Math.abs(abs - (LocationManagerProxy.this.n * LocationManagerProxy.this.n)) < 0.5d) {
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, aMapLocation);
                        intent.putExtras(bundle);
                        try {
                            pendingIntent.send(LocationManagerProxy.this.d, 0, intent);
                        } catch (PendingIntent.CanceledException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    class b implements AMapLocationListener {
        b() {
        }

        public void onLocationChanged(Location location) {
            if (LocationManagerProxy.this.g != null && LocationManagerProxy.this.g.size() > 0) {
                Iterator it = LocationManagerProxy.this.g.iterator();
                while (it.hasNext()) {
                    PendingIntent pendingIntent = (PendingIntent) it.next();
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, location);
                    intent.putExtras(bundle);
                    try {
                        pendingIntent.send(LocationManagerProxy.this.d, 0, intent);
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public void onLocationChanged(AMapLocation aMapLocation) {
            if (LocationManagerProxy.this.g != null && LocationManagerProxy.this.g.size() > 0) {
                Iterator it = LocationManagerProxy.this.g.iterator();
                while (it.hasNext()) {
                    PendingIntent pendingIntent = (PendingIntent) it.next();
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, aMapLocation);
                    intent.putExtras(bundle);
                    try {
                        pendingIntent.send(LocationManagerProxy.this.d, 0, intent);
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    private LocationManagerProxy(Activity activity) {
        a(activity.getApplicationContext());
    }

    private LocationManagerProxy(Context context) {
        a(context);
    }

    private void a(Context context) {
        this.d = context;
        this.r = d.a(context);
        this.a = (LocationManager) context.getSystemService(KEY_LOCATION_CHANGED);
        this.c = a.a(context.getApplicationContext(), this.a);
        this.s = this.r.c(context);
    }

    public static synchronized LocationManagerProxy getInstance(Activity activity) {
        LocationManagerProxy locationManagerProxy;
        synchronized (LocationManagerProxy.class) {
            if (b == null) {
                b = new LocationManagerProxy(activity);
            }
            locationManagerProxy = b;
        }
        return locationManagerProxy;
    }

    public static synchronized LocationManagerProxy getInstance(Context context) {
        LocationManagerProxy locationManagerProxy;
        synchronized (LocationManagerProxy.class) {
            if (b == null) {
                b = new LocationManagerProxy(context);
            }
            locationManagerProxy = b;
        }
        return locationManagerProxy;
    }

    public boolean addGpsStatusListener(GpsStatus.Listener listener) {
        if (this.a != null) {
            return this.a.addGpsStatusListener(listener);
        }
        return false;
    }

    public void addProximityAlert(double d2, double d3, float f2, long j2, PendingIntent pendingIntent) {
        if (LocationProviderProxy.AMapNetwork.equals(this.i) && this.r.a(this.s)) {
            if (this.o == null) {
                this.o = new e(this);
            }
            if (this.p == null) {
                this.p = new a();
            }
            this.o.a(this.p, 10000, f2, this.i);
            this.l = true;
            this.j = d2;
            this.k = d3;
            this.n = (double) f2;
            if (j2 != -1) {
                this.m = f.a() + j2;
            }
            this.q.add(pendingIntent);
        } else if (this.a != null) {
            this.a.addProximityAlert(d2, d3, f2, j2, pendingIntent);
        }
    }

    public void addTestProvider(String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i2, int i3) {
        if (this.a != null) {
            this.a.addTestProvider(str, z, z2, z3, z4, z5, z6, z7, i2, i3);
        }
    }

    public void clearTestProviderEnabled(String str) {
        if (this.a != null) {
            this.a.clearTestProviderEnabled(str);
        }
    }

    public void clearTestProviderLocation(String str) {
        if (this.a != null) {
            this.a.clearTestProviderLocation(str);
        }
    }

    public void clearTestProviderStatus(String str) {
        if (this.a != null) {
            this.a.clearTestProviderStatus(str);
        }
    }

    public void destory() {
        if (this.c != null) {
            this.c.b();
        }
        if (this.h != null) {
            this.h.clear();
        }
        if (this.g != null) {
            this.g.clear();
        }
        if (this.q != null) {
            this.q.clear();
        }
        this.h = null;
        this.g = null;
        this.q = null;
        this.c = null;
        b = null;
    }

    public List<String> getAllProviders() {
        List<String> allProviders = this.a.getAllProviders();
        if (allProviders == null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(LocationProviderProxy.AMapNetwork);
            arrayList.addAll(this.a.getAllProviders());
            return arrayList;
        } else if (allProviders.contains(LocationProviderProxy.AMapNetwork)) {
            return allProviders;
        } else {
            allProviders.add(LocationProviderProxy.AMapNetwork);
            return allProviders;
        }
    }

    public String getBestProvider(Criteria criteria, boolean z) {
        String str = LocationProviderProxy.AMapNetwork;
        if (criteria == null) {
            return str;
        }
        if (!getProvider(LocationProviderProxy.AMapNetwork).meetsCriteria(criteria)) {
            str = this.a.getBestProvider(criteria, z);
        }
        return (!z || f.b(this.d)) ? str : this.a.getBestProvider(criteria, z);
    }

    public GpsStatus getGpsStatus(GpsStatus gpsStatus) {
        if (this.a != null) {
            return this.a.getGpsStatus(gpsStatus);
        }
        return null;
    }

    public AMapLocation getLastKnownLocation(String str) {
        if (!LocationProviderProxy.AMapNetwork.equals(str) || !this.r.a(this.s)) {
            Location lastKnownLocation = this.a.getLastKnownLocation(str);
            if (this.a == null || lastKnownLocation == null) {
                return null;
            }
            return new AMapLocation(lastKnownLocation);
        }
        this.i = str;
        return this.c.a();
    }

    public LocationProviderProxy getProvider(String str) {
        if (str == null) {
            throw new IllegalArgumentException("name不能为空！");
        } else if (this.h.containsKey(str)) {
            return this.h.get(str);
        } else {
            LocationProviderProxy a2 = LocationProviderProxy.a(this.a, str);
            this.h.put(str, a2);
            return a2;
        }
    }

    public List<String> getProviders(Criteria criteria, boolean z) {
        List<String> providers = this.a.getProviders(criteria, z);
        if (providers == null || providers.size() == 0) {
            providers = new ArrayList<>();
        }
        if (LocationProviderProxy.AMapNetwork.equals(getBestProvider(criteria, z))) {
            providers.add(LocationProviderProxy.AMapNetwork);
        }
        return providers;
    }

    public List<String> getProviders(boolean z) {
        List<String> providers = this.a.getProviders(z);
        if (isProviderEnabled(LocationProviderProxy.AMapNetwork)) {
            if (providers == null || providers.size() == 0) {
                providers = new ArrayList<>();
            }
            providers.add(LocationProviderProxy.AMapNetwork);
        }
        return providers;
    }

    public boolean isProviderEnabled(String str) {
        return LocationProviderProxy.AMapNetwork.equals(str) ? f.b(this.d) : this.a.isProviderEnabled(str);
    }

    public void removeGpsStatusListener(GpsStatus.Listener listener) {
        if (this.a != null) {
            this.a.removeGpsStatusListener(listener);
        }
    }

    public void removeProximityAlert(PendingIntent pendingIntent) {
        if (LocationProviderProxy.AMapNetwork.equals(this.i) && this.r.a(this.s)) {
            if (this.o != null) {
                this.o.a();
            }
            this.q.remove(pendingIntent);
            this.o = null;
            this.l = false;
            this.m = 0;
            this.n = 0.0d;
            this.j = 0.0d;
            this.k = 0.0d;
        } else if (this.a != null) {
            this.a.removeProximityAlert(pendingIntent);
        }
    }

    public void removeUpdates(PendingIntent pendingIntent) {
        if (this.e != null) {
            this.g.remove(pendingIntent);
            this.e.a();
        }
        this.e = null;
        this.a.removeUpdates(pendingIntent);
    }

    public void removeUpdates(AMapLocationListener aMapLocationListener) {
        if (aMapLocationListener != null) {
            if (this.c != null) {
                this.c.a(aMapLocationListener);
            }
            this.a.removeUpdates(aMapLocationListener);
        }
    }

    public void requestLocationUpdates(String str, long j2, float f2, PendingIntent pendingIntent) {
        if (!LocationProviderProxy.AMapNetwork.equals(str) || !this.r.a(this.s)) {
            this.a.requestLocationUpdates(str, j2, f2, pendingIntent);
            return;
        }
        if (this.e == null) {
            this.e = new e(this);
        }
        if (this.f == null) {
            this.f = new b();
        }
        this.e.a(this.f, j2, f2);
        this.g.add(pendingIntent);
    }

    public void requestLocationUpdates(String str, long j2, float f2, AMapLocationListener aMapLocationListener) {
        String str2 = (this.r.a(this.s) || !LocationProviderProxy.AMapNetwork.equals(str)) ? str : NETWORK_PROVIDER;
        this.i = str2;
        if (LocationProviderProxy.AMapNetwork.equals(str2) && this.r.a(this.s)) {
            this.c.a(j2, f2, aMapLocationListener, LocationProviderProxy.AMapNetwork);
        } else if (GPS_PROVIDER.equals(str2)) {
            this.c.a(j2, f2, aMapLocationListener, GPS_PROVIDER);
        } else {
            this.a.requestLocationUpdates(str2, j2, f2, aMapLocationListener);
        }
    }

    public void setGpsEnable(boolean z) {
        this.c.a(z);
    }
}
