package com.amap.api.location;

import android.location.Criteria;
import android.location.LocationManager;
import android.location.LocationProvider;

public class LocationProviderProxy {
    public static final String AMapNetwork = "lbs";
    public static final int AVAILABLE = 2;
    public static final int OUT_OF_SERVICE = 0;
    public static final int TEMPORARILY_UNAVAILABLE = 1;
    private LocationManager a;
    private String b;

    protected LocationProviderProxy(LocationManager locationManager, String str) {
        this.a = locationManager;
        this.b = str;
    }

    private LocationProvider a() {
        return this.a.getProvider(this.b);
    }

    static LocationProviderProxy a(LocationManager locationManager, String str) {
        return new LocationProviderProxy(locationManager, str);
    }

    public int getAccuracy() {
        if (AMapNetwork.equals(this.b)) {
            return 2;
        }
        return a().getAccuracy();
    }

    public String getName() {
        return AMapNetwork.equals(this.b) ? AMapNetwork : a().getName();
    }

    public int getPowerRequirement() {
        if (AMapNetwork.equals(this.b)) {
            return 2;
        }
        return a().getPowerRequirement();
    }

    public boolean hasMonetaryCost() {
        if (AMapNetwork.equals(this.b)) {
            return false;
        }
        return a().hasMonetaryCost();
    }

    public boolean meetsCriteria(Criteria criteria) {
        if (!AMapNetwork.equals(this.b)) {
            return a().meetsCriteria(criteria);
        }
        if (criteria == null) {
            return true;
        }
        return !criteria.isAltitudeRequired() && !criteria.isBearingRequired() && !criteria.isSpeedRequired() && criteria.getAccuracy() != 1;
    }

    public boolean requiresCell() {
        if (AMapNetwork.equals(this.b)) {
            return true;
        }
        return a().requiresCell();
    }

    public boolean requiresNetwork() {
        if (AMapNetwork.equals(this.b)) {
            return true;
        }
        return a().requiresNetwork();
    }

    public boolean requiresSatellite() {
        if (AMapNetwork.equals(this.b)) {
            return false;
        }
        return a().requiresNetwork();
    }

    public boolean supportsAltitude() {
        if (AMapNetwork.equals(this.b)) {
            return false;
        }
        return a().supportsAltitude();
    }

    public boolean supportsBearing() {
        if (AMapNetwork.equals(this.b)) {
            return false;
        }
        return a().supportsBearing();
    }

    public boolean supportsSpeed() {
        if (AMapNetwork.equals(this.b)) {
            return false;
        }
        return a().supportsSpeed();
    }
}
