package com.amap.api.location.core;

import android.util.Log;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.search.core.AMapException;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.util.zip.GZIPInputStream;
import org.json.JSONObject;

public class b {
    public static int a = -1;
    public static String b = PoiTypeDef.All;
    public static int c = 0;

    protected static InputStream a(HttpURLConnection httpURLConnection) {
        try {
            PushbackInputStream pushbackInputStream = new PushbackInputStream(httpURLConnection.getInputStream(), 2);
            byte[] bArr = new byte[2];
            pushbackInputStream.read(bArr);
            pushbackInputStream.unread(bArr);
            return (bArr[0] == 31 && bArr[1] == -117) ? new GZIPInputStream(pushbackInputStream) : pushbackInputStream;
        } catch (ProtocolException e) {
            throw new a(AMapException.ERROR_PROTOCOL);
        } catch (UnknownHostException e2) {
            throw new a(AMapException.ERROR_UNKNOW_HOST);
        } catch (UnknownServiceException e3) {
            throw new a(AMapException.ERROR_UNKNOW_SERVICE);
        } catch (IOException e4) {
            throw new a(AMapException.ERROR_IO);
        }
    }

    private static String a() {
        return i.a().b() + "/log/init";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0030, code lost:
        throw new com.amap.api.location.core.a(com.amap.api.search.core.AMapException.ERROR_IO);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0060, code lost:
        throw new com.amap.api.location.core.a(com.amap.api.search.core.AMapException.ERROR_IO);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:9:0x001e, B:31:0x004f] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x004f A[SYNTHETIC, Splitter:B:31:0x004f] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0054 A[SYNTHETIC, Splitter:B:34:0x0054] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean a(android.content.Context r6) {
        /*
            r1 = 0
            java.lang.Class<com.amap.api.location.core.b> r3 = com.amap.api.location.core.b.class
            monitor-enter(r3)
            byte[] r0 = b()     // Catch:{ all -> 0x0031 }
            java.lang.String r2 = a()     // Catch:{ a -> 0x0034, all -> 0x0061 }
            java.net.Proxy r4 = com.amap.api.location.core.f.a(r6)     // Catch:{ a -> 0x0034, all -> 0x0061 }
            java.net.HttpURLConnection r2 = com.amap.api.location.core.h.a(r2, r0, r4)     // Catch:{ a -> 0x0034, all -> 0x0061 }
            java.io.InputStream r1 = a(r2)     // Catch:{ a -> 0x0064 }
            boolean r0 = a(r1)     // Catch:{ a -> 0x0064 }
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x0028 }
        L_0x0021:
            if (r2 == 0) goto L_0x0026
            r2.disconnect()     // Catch:{ all -> 0x0031 }
        L_0x0026:
            monitor-exit(r3)
            return r0
        L_0x0028:
            r0 = move-exception
            com.amap.api.location.core.a r0 = new com.amap.api.location.core.a     // Catch:{ all -> 0x0031 }
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)     // Catch:{ all -> 0x0031 }
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0034:
            r0 = move-exception
            r2 = r1
        L_0x0036:
            java.lang.String r4 = "AuthFailure"
            java.lang.String r5 = r0.a()     // Catch:{ all -> 0x004c }
            android.util.Log.i(r4, r5)     // Catch:{ all -> 0x004c }
            r0.printStackTrace()     // Catch:{ all -> 0x004c }
            com.amap.api.location.core.a r4 = new com.amap.api.location.core.a     // Catch:{ all -> 0x004c }
            java.lang.String r0 = r0.a()     // Catch:{ all -> 0x004c }
            r4.<init>(r0)     // Catch:{ all -> 0x004c }
            throw r4     // Catch:{ all -> 0x004c }
        L_0x004c:
            r0 = move-exception
        L_0x004d:
            if (r1 == 0) goto L_0x0052
            r1.close()     // Catch:{ IOException -> 0x0058 }
        L_0x0052:
            if (r2 == 0) goto L_0x0057
            r2.disconnect()     // Catch:{ all -> 0x0031 }
        L_0x0057:
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0058:
            r0 = move-exception
            com.amap.api.location.core.a r0 = new com.amap.api.location.core.a     // Catch:{ all -> 0x0031 }
            java.lang.String r1 = "IO 操作异常 - IOException"
            r0.<init>(r1)     // Catch:{ all -> 0x0031 }
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0061:
            r0 = move-exception
            r2 = r1
            goto L_0x004d
        L_0x0064:
            r0 = move-exception
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.amap.api.location.core.b.a(android.content.Context):boolean");
    }

    private static boolean a(InputStream inputStream) {
        try {
            JSONObject jSONObject = new JSONObject(new String(c.a(inputStream)));
            if (jSONObject.has(LocationManagerProxy.KEY_STATUS_CHANGED)) {
                int i = jSONObject.getInt(LocationManagerProxy.KEY_STATUS_CHANGED);
                if (i == 1) {
                    a = 1;
                } else if (i == 0) {
                    a = 0;
                }
            }
            if (jSONObject.has("info")) {
                b = jSONObject.getString("info");
            }
            if (a != 0) {
                return a == 1;
            }
            Log.i("AuthFailure", b);
            throw new a(AMapException.ERROR_FAILURE_AUTH);
        } catch (Exception e) {
            e.printStackTrace();
            return a == 1;
        } catch (Throwable th) {
            return a == 1;
        }
    }

    private static byte[] b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("resType=json&encode=UTF-8");
        return stringBuffer.toString().getBytes();
    }
}
