package com.amap.api.location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Message;
import com.a.a.a;

class d implements LocationListener {
    final /* synthetic */ c a;

    d(c cVar) {
        this.a = cVar;
    }

    public void onLocationChanged(Location location) {
        AMapLocation aMapLocation;
        a.b = true;
        a.c = System.currentTimeMillis();
        AMapLocation aMapLocation2 = null;
        try {
            if (this.a.e.a(this.a.f)) {
                double[] a2 = a.a(location.getLongitude(), location.getLatitude());
                aMapLocation = new AMapLocation(location);
                try {
                    aMapLocation.setLatitude(a2[1]);
                    aMapLocation.setLongitude(a2[0]);
                } catch (Exception e) {
                    Message message = new Message();
                    message.obj = aMapLocation;
                    message.what = a.a;
                    this.a.c.sendMessage(message);
                    a.b = true;
                    a.c = System.currentTimeMillis();
                    return;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    aMapLocation2 = aMapLocation;
                    th = th2;
                    Message message2 = new Message();
                    message2.obj = aMapLocation2;
                    message2.what = a.a;
                    this.a.c.sendMessage(message2);
                    a.b = true;
                    a.c = System.currentTimeMillis();
                    throw th;
                }
            } else {
                aMapLocation = new AMapLocation(location);
            }
            Message message3 = new Message();
            message3.obj = aMapLocation;
            message3.what = a.a;
            this.a.c.sendMessage(message3);
            a.b = true;
            a.c = System.currentTimeMillis();
        } catch (Exception e2) {
            aMapLocation = null;
        } catch (Throwable th3) {
            th = th3;
            Message message22 = new Message();
            message22.obj = aMapLocation2;
            message22.what = a.a;
            this.a.c.sendMessage(message22);
            a.b = true;
            a.c = System.currentTimeMillis();
            throw th;
        }
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
