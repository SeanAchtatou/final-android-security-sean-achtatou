package com.amap.api.location;

import android.location.Location;

public class AMapLocation extends Location {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;

    public AMapLocation(Location location) {
        super(location);
    }

    public AMapLocation(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public void e(String str) {
        this.e = str;
    }

    public String getAdCode() {
        return this.e;
    }

    public String getCity() {
        return this.b;
    }

    public String getCityCode() {
        return this.d;
    }

    public String getDistrict() {
        return this.c;
    }

    public String getProvince() {
        return this.a;
    }
}
