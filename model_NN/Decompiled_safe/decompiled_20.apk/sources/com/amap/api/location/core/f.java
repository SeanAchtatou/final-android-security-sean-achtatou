package com.amap.api.location.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.net.InetSocketAddress;
import java.net.Proxy;

public class f {
    public static boolean a = true;
    static float[] b = new float[9];
    private static String c = null;

    public static long a() {
        return (System.nanoTime() * 1000) / 1000000000;
    }

    public static Proxy a(Context context) {
        Proxy proxy;
        int defaultPort;
        String str;
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.getType() == 1) {
                    String host = android.net.Proxy.getHost(context);
                    defaultPort = android.net.Proxy.getPort(context);
                    str = host;
                } else {
                    String defaultHost = android.net.Proxy.getDefaultHost();
                    defaultPort = android.net.Proxy.getDefaultPort();
                    str = defaultHost;
                }
                if (str != null) {
                    proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, defaultPort));
                    return proxy;
                }
            }
            proxy = null;
            return proxy;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void a(Context context, AMapLocation aMapLocation) {
        SharedPreferences.Editor edit = context.getSharedPreferences("last_know_location", 0).edit();
        edit.putString("last_know_lat", String.valueOf(aMapLocation.getLatitude()));
        edit.putString("last_know_lng", String.valueOf(aMapLocation.getLongitude()));
        edit.commit();
    }

    public static boolean b(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        NetworkInfo.State state = activeNetworkInfo.getState();
        return (state == null || state == NetworkInfo.State.DISCONNECTED || state == NetworkInfo.State.DISCONNECTING) ? false : true;
    }

    public static AMapLocation c(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("last_know_location", 0);
        AMapLocation aMapLocation = new AMapLocation(PoiTypeDef.All);
        aMapLocation.setProvider(LocationProviderProxy.AMapNetwork);
        double parseDouble = Double.parseDouble(sharedPreferences.getString("last_know_lat", "0.0"));
        double parseDouble2 = Double.parseDouble(sharedPreferences.getString("last_know_lng", "0.0"));
        aMapLocation.setLatitude(parseDouble);
        aMapLocation.setLongitude(parseDouble2);
        return aMapLocation;
    }
}
