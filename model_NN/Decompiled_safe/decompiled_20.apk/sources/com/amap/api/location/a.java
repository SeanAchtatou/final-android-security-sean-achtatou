package com.amap.api.location;

import android.content.Context;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import com.amap.api.location.core.f;
import java.util.Iterator;
import java.util.Vector;

public class a {
    public static int a = 100;
    public static boolean b = false;
    public static long c;
    public static boolean d = true;
    public static boolean e = true;
    /* access modifiers changed from: private */
    public static Vector<f> g = null;
    private static c i = null;
    private static b j = null;
    private static a k = null;
    /* access modifiers changed from: private */
    public Context f;
    private C0001a h = null;
    /* access modifiers changed from: private */
    public AMapLocation l;
    private Thread m;

    /* renamed from: com.amap.api.location.a$a  reason: collision with other inner class name */
    class C0001a extends Handler {
        C0001a() {
        }

        public void handleMessage(Message message) {
            if (message.what == a.a) {
                Iterator it = a.g.iterator();
                while (it.hasNext()) {
                    ((f) it.next()).c.onLocationChanged((AMapLocation) message.obj);
                }
                AMapLocation unused = a.this.l = (AMapLocation) message.obj;
                if (a.this.l != null) {
                    f.a(a.this.f, a.this.l);
                }
            }
        }
    }

    private a(Context context, LocationManager locationManager) {
        this.f = context;
        g = new Vector<>();
        this.h = new C0001a();
        i = c.a(context, locationManager, this.h);
        j = b.a(context, this.h);
    }

    public static a a(Context context, LocationManager locationManager) {
        if (k == null) {
            k = new a(context, locationManager);
        }
        return k;
    }

    public AMapLocation a() {
        return this.l != null ? this.l : f.c(this.f);
    }

    public void a(long j2, float f2, AMapLocationListener aMapLocationListener, String str) {
        g.add(new f(j2, f2, aMapLocationListener, str));
        if (str == LocationManagerProxy.GPS_PROVIDER) {
            i.a(j2, f2, aMapLocationListener, str);
        } else if (str == LocationProviderProxy.AMapNetwork) {
            if (e) {
                i.a(j2, f2, aMapLocationListener, str);
            }
            j.a(j2);
            d = true;
            if (this.m == null) {
                this.m = new Thread(j);
                this.m.start();
            }
        }
    }

    public void a(AMapLocationListener aMapLocationListener) {
        int i2;
        int size = g.size();
        int i3 = 0;
        while (i3 < size) {
            f fVar = g.get(i3);
            if (aMapLocationListener.equals(fVar.c)) {
                g.remove(fVar);
                i2 = size - 1;
            } else {
                i2 = size;
            }
            i3++;
            size = i2;
        }
        if (i != null && g.size() == 0) {
            i.a();
            b = false;
            d = false;
        }
    }

    public void a(boolean z) {
        e = z;
    }

    public void b() {
        if (j != null) {
            j.a();
            j = null;
        }
        if (i != null) {
            i.a();
            i = null;
        }
        g.clear();
        b = false;
        this.m = null;
        k = null;
    }
}
