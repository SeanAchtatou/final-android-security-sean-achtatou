package com.amap.api.location.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.amap.api.search.poisearch.PoiTypeDef;

public class d {
    public static String a = PoiTypeDef.All;
    private static d b = null;
    private static String c = null;
    private static Context d = null;
    private static TelephonyManager e;
    private static ConnectivityManager f;
    private static String g;
    private static String h;

    private d() {
    }

    public static d a(Context context) {
        if (b == null) {
            b = new d();
            d = context;
            e = (TelephonyManager) context.getSystemService("phone");
            f = (ConnectivityManager) d.getSystemService("connectivity");
            g = d.getApplicationContext().getPackageName();
            h = f();
            a = b(d);
        }
        return b;
    }

    public static String a() {
        return g;
    }

    public static String b(Context context) {
        if (a == null || a.equals(PoiTypeDef.All)) {
            try {
                a = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("com.amap.api.v2.apikey");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return a;
    }

    private static String f() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) d.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.widthPixels;
        int i2 = displayMetrics.heightPixels;
        String str = i2 > i ? i + "*" + i2 : i2 + "*" + i;
        h = str;
        return str;
    }

    public boolean a(String str) {
        return str == null || str.equals(PoiTypeDef.All) || str.equals("0") || str.equals("460") || str.equals("461");
    }

    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("ia=1&");
        if (a != null && a.length() > 0) {
            sb.append("key=");
            sb.append(a);
            sb.append("&");
        }
        sb.append("ct=android");
        String deviceId = e.getDeviceId();
        String subscriberId = e.getSubscriberId();
        sb.append("&ime=" + deviceId);
        sb.append("&sim=" + subscriberId);
        sb.append("&pkg=" + g);
        sb.append("&mod=");
        sb.append(d());
        sb.append("&sv=");
        sb.append(c());
        sb.append("&nt=");
        sb.append(e());
        String networkOperatorName = e.getNetworkOperatorName();
        sb.append("&np=");
        sb.append(networkOperatorName);
        sb.append("&ctm=" + System.currentTimeMillis());
        sb.append("&re=" + h);
        sb.append("&av=" + e.k);
        sb.append("&pro=loc");
        return sb.toString();
    }

    public String c() {
        return Build.VERSION.RELEASE;
    }

    public String c(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            try {
                return telephonyManager.getNetworkOperator().substring(0, 3);
            } catch (Exception e2) {
            }
        }
        return PoiTypeDef.All;
    }

    public String d() {
        return Build.MODEL;
    }

    public String e() {
        NetworkInfo activeNetworkInfo;
        return (d.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0 || f == null || (activeNetworkInfo = f.getActiveNetworkInfo()) == null) ? PoiTypeDef.All : activeNetworkInfo.getTypeName();
    }
}
