package com.amap.api.location;

public class f {
    public long a;
    public float b;
    public AMapLocationListener c;
    public boolean d = true;
    public AMapLocation e = null;
    public String f;

    public f(long j, float f2, AMapLocationListener aMapLocationListener, String str) {
        this.a = j;
        this.b = f2;
        this.c = aMapLocationListener;
        this.f = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        return this.c == null ? fVar.c == null : this.c.equals(fVar.c);
    }

    public int hashCode() {
        return (this.c == null ? 0 : this.c.hashCode()) + 31;
    }
}
