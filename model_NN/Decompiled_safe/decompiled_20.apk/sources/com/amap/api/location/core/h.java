package com.amap.api.location.core;

import android.content.Context;
import com.amap.api.search.core.AMapException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import org.apache.commons.httpclient.methods.PostMethod;

public class h {
    public static HttpURLConnection a(String str, byte[] bArr, Proxy proxy) {
        if (str == null) {
            throw new a(AMapException.ERROR_INVALID_PARAMETER);
        }
        try {
            URL url = new URL(str);
            HttpURLConnection httpURLConnection = (proxy == null || e.n) ? (HttpURLConnection) url.openConnection() : (HttpURLConnection) url.openConnection(proxy);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(50000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestProperty("Content-Type", PostMethod.FORM_URL_ENCODED_CONTENT_TYPE);
            httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bArr.length));
            httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("X-MapAPI", d.a((Context) null).b());
            httpURLConnection.setRequestProperty("ia", "1");
            httpURLConnection.setRequestProperty("key", d.a);
            httpURLConnection.connect();
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bArr);
            outputStream.flush();
            outputStream.close();
            if (httpURLConnection.getResponseCode() == 200) {
                return httpURLConnection;
            }
            throw new a(AMapException.ERROR_CONNECTION);
        } catch (UnknownHostException e) {
            throw new a(AMapException.ERROR_UNKNOW_HOST);
        } catch (MalformedURLException e2) {
            throw new a(AMapException.ERROR_URL);
        } catch (ProtocolException e3) {
            throw new a(AMapException.ERROR_PROTOCOL);
        } catch (SocketTimeoutException e4) {
            throw new a(AMapException.ERROR_SOCKE_TIME_OUT);
        } catch (IOException e5) {
            throw new a(AMapException.ERROR_IO);
        }
    }
}
