package com.amap.api.location;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import com.amap.api.location.a;
import com.amap.api.location.core.d;

public class c {
    private static c b = null;
    private LocationManager a = null;
    /* access modifiers changed from: private */
    public a.C0001a c;
    private Context d;
    /* access modifiers changed from: private */
    public d e;
    /* access modifiers changed from: private */
    public String f;
    private LocationListener g = new d(this);

    private c(Context context, LocationManager locationManager, a.C0001a aVar) {
        this.a = locationManager;
        this.c = aVar;
        this.d = context;
        this.e = d.a(context);
        this.f = this.e.c(context);
    }

    public static c a(Context context, LocationManager locationManager, a.C0001a aVar) {
        if (b == null) {
            b = new c(context, locationManager, aVar);
        }
        return b;
    }

    public void a() {
        this.a.removeUpdates(this.g);
    }

    public void a(long j, float f2, LocationListener locationListener, String str) {
        try {
            if (this.a.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER)) {
                this.a.requestLocationUpdates(LocationManagerProxy.GPS_PROVIDER, j, f2, this.g);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
