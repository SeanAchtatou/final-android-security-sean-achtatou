package com.amap.api.location.core;

import android.os.Parcel;
import android.os.Parcelable;

final class g implements Parcelable.Creator<GeoPoint> {
    g() {
    }

    /* renamed from: a */
    public final GeoPoint createFromParcel(Parcel parcel) {
        return new GeoPoint(parcel, (g) null);
    }

    /* renamed from: a */
    public final GeoPoint[] newArray(int i) {
        return new GeoPoint[i];
    }
}
