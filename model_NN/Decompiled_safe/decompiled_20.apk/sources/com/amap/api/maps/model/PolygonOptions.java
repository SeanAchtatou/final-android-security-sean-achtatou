package com.amap.api.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class PolygonOptions implements Parcelable {
    public static final PolygonOptionsCreator CREATOR = new PolygonOptionsCreator();
    String a;
    private final List<LatLng> b = new ArrayList();
    private float c = 10.0f;
    private int d = -16777216;
    private int e = -16777216;
    private float f = BitmapDescriptorFactory.HUE_RED;
    private boolean g = true;

    public final PolygonOptions add(LatLng latLng) {
        this.b.add(latLng);
        return this;
    }

    public final PolygonOptions add(LatLng... latLngArr) {
        this.b.addAll(Arrays.asList(latLngArr));
        return this;
    }

    public final PolygonOptions addAll(Iterable<LatLng> iterable) {
        for (LatLng add : iterable) {
            this.b.add(add);
        }
        return this;
    }

    public final int describeContents() {
        return 0;
    }

    public final PolygonOptions fillColor(int i) {
        this.e = i;
        return this;
    }

    public final int getFillColor() {
        return this.e;
    }

    public final List<LatLng> getPoints() {
        return this.b;
    }

    public final int getStrokeColor() {
        return this.d;
    }

    public final float getStrokeWidth() {
        return this.c;
    }

    public final float getZIndex() {
        return this.f;
    }

    public final boolean isVisible() {
        return this.g;
    }

    public final PolygonOptions strokeColor(int i) {
        this.d = i;
        return this;
    }

    public final PolygonOptions strokeWidth(float f2) {
        this.c = f2;
        return this;
    }

    public final PolygonOptions visible(boolean z) {
        this.g = z;
        return this;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.b);
        parcel.writeFloat(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeFloat(this.f);
        parcel.writeByte((byte) (this.g ? 0 : 1));
        parcel.writeString(this.a);
    }

    public final PolygonOptions zIndex(float f2) {
        this.f = f2;
        return this;
    }
}
