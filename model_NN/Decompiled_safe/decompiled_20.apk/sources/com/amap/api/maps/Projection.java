package com.amap.api.maps;

import android.graphics.Point;
import android.os.RemoteException;
import com.amap.api.a.w;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.RuntimeRemoteException;
import com.amap.api.maps.model.VisibleRegion;

public class Projection {
    private final w a;

    Projection(w wVar) {
        this.a = wVar;
    }

    public LatLng fromScreenLocation(Point point) {
        try {
            return this.a.a(point);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public VisibleRegion getVisibleRegion() {
        try {
            return this.a.a();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public Point toScreenLocation(LatLng latLng) {
        try {
            return this.a.a(latLng);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
