package com.amap.api.maps.model;

import com.amap.api.a.s;

public final class Marker {
    s a;

    public Marker(s sVar) {
        this.a = sVar;
    }

    public final void destroy() {
        try {
            if (this.a != null) {
                this.a.o();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Marker)) {
            return false;
        }
        return this.a.a(((Marker) obj).a);
    }

    public final String getId() {
        return this.a.d();
    }

    public final Object getObject() {
        return this.a.q();
    }

    public final LatLng getPosition() {
        return this.a.c();
    }

    public final String getSnippet() {
        return this.a.h();
    }

    public final String getTitle() {
        return this.a.g();
    }

    public final int hashCode() {
        return this.a.p();
    }

    public final void hideInfoWindow() {
        this.a.l();
    }

    public final boolean isDraggable() {
        return this.a.j();
    }

    public final boolean isInfoWindowShown() {
        return this.a.m();
    }

    public final boolean isVisible() {
        return this.a.n();
    }

    public final void remove() {
        try {
            this.a.a();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void setAnchor(float f, float f2) {
        this.a.a(f, f2);
    }

    public final void setDraggable(boolean z) {
        this.a.a(z);
    }

    public final void setIcon(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.a.a(bitmapDescriptor);
        }
    }

    public final void setObject(Object obj) {
        this.a.a(obj);
    }

    public final void setPosition(LatLng latLng) {
        this.a.a(latLng);
    }

    public final void setSnippet(String str) {
        this.a.b(str);
    }

    public final void setTitle(String str) {
        this.a.a(str);
    }

    public final void setVisible(boolean z) {
        this.a.b(z);
    }

    public final void showInfoWindow() {
        this.a.k();
    }
}
