package com.amap.api.maps.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.umeng.fb.f;

public final class CircleOptions implements Parcelable {
    public static final CircleOptionsCreator CREATOR = new CircleOptionsCreator();
    String a;
    private LatLng b = null;
    private double c = 0.0d;
    private float d = 10.0f;
    private int e = -16777216;
    private int f = 0;
    private float g = BitmapDescriptorFactory.HUE_RED;
    private boolean h = true;

    public final CircleOptions center(LatLng latLng) {
        this.b = latLng;
        return this;
    }

    public final int describeContents() {
        return 0;
    }

    public final CircleOptions fillColor(int i) {
        this.f = i;
        return this;
    }

    public final LatLng getCenter() {
        return this.b;
    }

    public final int getFillColor() {
        return this.f;
    }

    public final double getRadius() {
        return this.c;
    }

    public final int getStrokeColor() {
        return this.e;
    }

    public final float getStrokeWidth() {
        return this.d;
    }

    public final float getZIndex() {
        return this.g;
    }

    public final boolean isVisible() {
        return this.h;
    }

    public final CircleOptions radius(double d2) {
        this.c = d2;
        return this;
    }

    public final CircleOptions strokeColor(int i) {
        this.e = i;
        return this;
    }

    public final CircleOptions strokeWidth(float f2) {
        this.d = f2;
        return this;
    }

    public final CircleOptions visible(boolean z) {
        this.h = z;
        return this;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        Bundle bundle = new Bundle();
        if (this.b != null) {
            bundle.putDouble(f.ae, this.b.latitude);
            bundle.putDouble(f.af, this.b.longitude);
        }
        parcel.writeBundle(bundle);
        parcel.writeDouble(this.c);
        parcel.writeFloat(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        parcel.writeFloat(this.g);
        parcel.writeByte((byte) (this.h ? 1 : 0));
        parcel.writeString(this.a);
    }

    public final CircleOptions zIndex(float f2) {
        this.g = f2;
        return this;
    }
}
