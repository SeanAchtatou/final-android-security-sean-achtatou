package com.amap.api.maps.model;

import com.amap.api.a.y;

public final class TileOverlay {
    private y a;

    public TileOverlay(y yVar) {
        this.a = yVar;
    }

    public final void clearTileCache() {
        this.a.b();
    }

    public final boolean equals(Object obj) {
        return this.a.a(this.a);
    }

    public final String getId() {
        return this.a.c();
    }

    public final float getZIndex() {
        return this.a.d();
    }

    public final int hashCode() {
        return this.a.f();
    }

    public final boolean isVisible() {
        return this.a.e();
    }

    public final void remove() {
        this.a.a();
    }

    public final void setVisible(boolean z) {
        this.a.a(z);
    }

    public final void setZIndex(float f) {
        this.a.a(f);
    }
}
