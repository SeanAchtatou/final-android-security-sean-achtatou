package com.amap.api.maps.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MarkerOptionsCreator implements Parcelable.Creator<MarkerOptions> {
    public MarkerOptions createFromParcel(Parcel parcel) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position((LatLng) parcel.readParcelable(LatLng.class.getClassLoader()));
        markerOptions.icon((BitmapDescriptor) parcel.readParcelable(BitmapDescriptor.class.getClassLoader()));
        markerOptions.title(parcel.readString());
        markerOptions.snippet(parcel.readString());
        markerOptions.anchor(parcel.readFloat(), parcel.readFloat());
        boolean[] zArr = new boolean[1];
        parcel.readBooleanArray(zArr);
        markerOptions.visible(zArr[0]);
        markerOptions.a = parcel.readString();
        return markerOptions;
    }

    public MarkerOptions[] newArray(int i) {
        return new MarkerOptions[i];
    }
}
