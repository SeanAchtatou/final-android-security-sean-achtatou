package com.amap.api.maps;

public class AMapNativeRenderer {
    static {
        try {
            System.loadLibrary("mapv3ex");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static native void nativeDrawLineByTextureID(float[] fArr, int i, float f, int i2, float f2, float f3, float f4, float f5, float f6);
}
