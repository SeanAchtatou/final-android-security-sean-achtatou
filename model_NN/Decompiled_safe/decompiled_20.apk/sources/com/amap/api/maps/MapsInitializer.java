package com.amap.api.maps;

import android.content.Context;
import com.amap.api.a.ab;
import com.amap.api.search.poisearch.PoiTypeDef;

public final class MapsInitializer {
    public static String sdcardDir = PoiTypeDef.All;

    public static void initialize(Context context) {
        ab.a = context;
    }
}
