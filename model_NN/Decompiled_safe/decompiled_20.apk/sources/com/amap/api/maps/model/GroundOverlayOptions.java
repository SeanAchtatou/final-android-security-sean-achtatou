package com.amap.api.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.amap.api.a.b.b;

public final class GroundOverlayOptions implements Parcelable {
    public static final GroundOverlayOptionsCreator CREATOR = new GroundOverlayOptionsCreator();
    public static final float NO_DIMENSION = -1.0f;
    private final int a;
    private BitmapDescriptor b;
    private LatLng c;
    private float d;
    private float e;
    private LatLngBounds f;
    private float g;
    private float h;
    private boolean i;
    private float j;
    private float k;
    private float l;

    public GroundOverlayOptions() {
        this.i = true;
        this.j = BitmapDescriptorFactory.HUE_RED;
        this.k = 0.5f;
        this.l = 0.5f;
        this.a = 1;
    }

    GroundOverlayOptions(int i2, IBinder iBinder, LatLng latLng, float f2, float f3, LatLngBounds latLngBounds, float f4, float f5, boolean z, float f6, float f7, float f8) {
        this.i = true;
        this.j = BitmapDescriptorFactory.HUE_RED;
        this.k = 0.5f;
        this.l = 0.5f;
        this.a = i2;
        this.b = BitmapDescriptorFactory.fromBitmap(null);
        this.c = latLng;
        this.d = f2;
        this.e = f3;
        this.f = latLngBounds;
        this.g = f4;
        this.h = f5;
        this.i = z;
        this.j = f6;
        this.k = f7;
        this.l = f8;
    }

    private GroundOverlayOptions a(LatLng latLng, float f2, float f3) {
        this.c = latLng;
        this.d = f2;
        this.e = f3;
        return this;
    }

    public final GroundOverlayOptions anchor(float f2, float f3) {
        this.k = f2;
        this.l = f3;
        return this;
    }

    public final GroundOverlayOptions bearing(float f2) {
        this.g = ((f2 % 360.0f) + 360.0f) % 360.0f;
        return this;
    }

    public final int describeContents() {
        return 0;
    }

    public final float getAnchorU() {
        return this.k;
    }

    public final float getAnchorV() {
        return this.l;
    }

    public final float getBearing() {
        return this.g;
    }

    public final LatLngBounds getBounds() {
        return this.f;
    }

    public final float getHeight() {
        return this.e;
    }

    public final BitmapDescriptor getImage() {
        return this.b;
    }

    public final LatLng getLocation() {
        return this.c;
    }

    public final float getTransparency() {
        return this.j;
    }

    public final float getWidth() {
        return this.d;
    }

    public final float getZIndex() {
        return this.h;
    }

    public final GroundOverlayOptions image(BitmapDescriptor bitmapDescriptor) {
        this.b = bitmapDescriptor;
        return this;
    }

    public final boolean isVisible() {
        return this.i;
    }

    public final GroundOverlayOptions position(LatLng latLng, float f2) {
        boolean z = true;
        b.a(this.f == null, "Position has already been set using positionFromBounds");
        b.b(latLng != null, "Location must be specified");
        if (f2 < BitmapDescriptorFactory.HUE_RED) {
            z = false;
        }
        b.b(z, "Width must be non-negative");
        return a(latLng, f2, -1.0f);
    }

    public final GroundOverlayOptions position(LatLng latLng, float f2, float f3) {
        boolean z = true;
        b.a(this.f == null, "Position has already been set using positionFromBounds");
        b.b(latLng != null, "Location must be specified");
        b.b(f2 >= BitmapDescriptorFactory.HUE_RED, "Width must be non-negative");
        if (f3 < BitmapDescriptorFactory.HUE_RED) {
            z = false;
        }
        b.b(z, "Height must be non-negative");
        return a(latLng, f2, f3);
    }

    public final GroundOverlayOptions positionFromBounds(LatLngBounds latLngBounds) {
        b.a(this.c == null, "Position has already been set using position: " + this.c);
        return this;
    }

    public final GroundOverlayOptions transparency(float f2) {
        b.b(f2 >= BitmapDescriptorFactory.HUE_RED && f2 <= 1.0f, "Transparency must be in the range [0..1]");
        this.j = f2;
        return this;
    }

    public final GroundOverlayOptions visible(boolean z) {
        this.i = z;
        return this;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.a);
        parcel.writeParcelable(this.b, i2);
        parcel.writeParcelable(this.c, i2);
        parcel.writeFloat(this.d);
        parcel.writeFloat(this.e);
        parcel.writeParcelable(this.f, i2);
        parcel.writeFloat(this.g);
        parcel.writeFloat(this.h);
        parcel.writeByte((byte) (this.i ? 1 : 0));
        parcel.writeFloat(this.j);
        parcel.writeFloat(this.k);
        parcel.writeFloat(this.l);
    }

    public final GroundOverlayOptions zIndex(float f2) {
        this.h = f2;
        return this;
    }
}
