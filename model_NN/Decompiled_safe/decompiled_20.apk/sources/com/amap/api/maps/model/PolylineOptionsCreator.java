package com.amap.api.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class PolylineOptionsCreator implements Parcelable.Creator<PolylineOptions> {
    public PolylineOptions createFromParcel(Parcel parcel) {
        boolean z = true;
        PolylineOptions polylineOptions = new PolylineOptions();
        ArrayList arrayList = new ArrayList();
        parcel.readTypedList(arrayList, LatLng.CREATOR);
        float readFloat = parcel.readFloat();
        int readInt = parcel.readInt();
        float readFloat2 = parcel.readFloat();
        if (parcel.readByte() != 1) {
            z = false;
        }
        polylineOptions.addAll(arrayList);
        polylineOptions.width(readFloat);
        polylineOptions.color(readInt);
        polylineOptions.zIndex(readFloat2);
        polylineOptions.visible(z);
        polylineOptions.a = parcel.readString();
        return polylineOptions;
    }

    public PolylineOptions[] newArray(int i) {
        return new PolylineOptions[i];
    }
}
