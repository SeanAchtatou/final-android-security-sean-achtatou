package com.amap.api.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.text.DecimalFormat;

public final class LatLng implements Parcelable {
    public static final LatLngCreator CREATOR = new LatLngCreator();
    private static DecimalFormat a = new DecimalFormat("0.000000");
    public final double latitude;
    public final double longitude;

    public LatLng(double d, double d2) {
        if (-180.0d > d2 || d2 >= 180.0d) {
            this.longitude = a(((((d2 - 180.0d) % 360.0d) + 360.0d) % 360.0d) - 180.0d);
        } else {
            this.longitude = a(d2);
        }
        this.latitude = a(Math.max(-90.0d, Math.min(90.0d, d)));
    }

    private static double a(double d) {
        return Double.parseDouble(a.format(d));
    }

    public final LatLng clone() {
        return new LatLng(this.latitude, this.longitude);
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLng)) {
            return false;
        }
        LatLng latLng = (LatLng) obj;
        return Double.doubleToLongBits(this.latitude) == Double.doubleToLongBits(latLng.latitude) && Double.doubleToLongBits(this.longitude) == Double.doubleToLongBits(latLng.longitude);
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.latitude);
        long doubleToLongBits2 = Double.doubleToLongBits(this.longitude);
        return ((((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) + 31) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)));
    }

    public final String toString() {
        return "lat/lng: (" + this.latitude + "," + this.longitude + ")";
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(this.longitude);
        parcel.writeDouble(this.latitude);
    }
}
