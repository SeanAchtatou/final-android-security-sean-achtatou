package com.amap.api.maps.model;

import android.os.Parcel;
import android.os.Parcelable;

class a implements Parcelable.Creator<Tile> {
    a() {
    }

    /* renamed from: a */
    public Tile createFromParcel(Parcel parcel) {
        return new Tile(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.createByteArray());
    }

    /* renamed from: a */
    public Tile[] newArray(int i) {
        return new Tile[i];
    }
}
