package com.amap.api.maps.model;

import android.os.Parcel;
import android.os.Parcelable;

public final class MarkerOptions implements Parcelable {
    public static final MarkerOptionsCreator CREATOR = new MarkerOptionsCreator();
    String a;
    private LatLng b;
    private String c;
    private String d;
    private BitmapDescriptor e;
    private float f = 0.5f;
    private float g = 1.0f;
    private boolean h = false;
    private boolean i = true;

    public final MarkerOptions anchor(float f2, float f3) {
        this.f = f2;
        this.g = f3;
        return this;
    }

    public final int describeContents() {
        return 0;
    }

    public final MarkerOptions draggable(boolean z) {
        this.h = z;
        return this;
    }

    public final float getAnchorU() {
        return this.f;
    }

    public final float getAnchorV() {
        return this.g;
    }

    public final BitmapDescriptor getIcon() {
        if (this.e == null) {
            this.e = BitmapDescriptorFactory.defaultMarker();
        }
        return this.e;
    }

    public final LatLng getPosition() {
        return this.b;
    }

    public final String getSnippet() {
        return this.d;
    }

    public final String getTitle() {
        return this.c;
    }

    public final MarkerOptions icon(BitmapDescriptor bitmapDescriptor) {
        this.e = bitmapDescriptor;
        return this;
    }

    public final boolean isDraggable() {
        return this.h;
    }

    public final boolean isVisible() {
        return this.i;
    }

    public final MarkerOptions position(LatLng latLng) {
        this.b = latLng;
        return this;
    }

    public final MarkerOptions snippet(String str) {
        this.d = str;
        return this;
    }

    public final MarkerOptions title(String str) {
        this.c = str;
        return this;
    }

    public final MarkerOptions visible(boolean z) {
        this.i = z;
        return this;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        parcel.writeParcelable(this.b, i2);
        parcel.writeParcelable(this.e, i2);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeFloat(this.f);
        parcel.writeFloat(this.g);
        parcel.writeBooleanArray(new boolean[]{this.i, this.h});
        parcel.writeString(this.a);
    }
}
