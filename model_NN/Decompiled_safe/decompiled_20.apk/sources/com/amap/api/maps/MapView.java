package com.amap.api.maps;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.amap.api.a.ab;
import com.amap.api.a.p;
import com.amap.api.a.r;
import com.amap.api.maps.model.RuntimeRemoteException;

public class MapView extends FrameLayout {
    private r a;
    private AMap b;

    public MapView(Context context) {
        super(context);
        getMapFragmentDelegate().a((Activity) context);
    }

    public MapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        getMapFragmentDelegate().a((Activity) context);
    }

    public MapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        getMapFragmentDelegate().a((Activity) context);
    }

    public MapView(Context context, AMapOptions aMapOptions) {
        super(context);
        getMapFragmentDelegate().a((Activity) context);
        getMapFragmentDelegate().a(aMapOptions);
    }

    public AMap getMap() {
        r mapFragmentDelegate = getMapFragmentDelegate();
        if (mapFragmentDelegate == null) {
            return null;
        }
        try {
            p a2 = mapFragmentDelegate.a();
            if (a2 == null) {
                return null;
            }
            if (this.b == null) {
                this.b = new AMap(a2);
            }
            return this.b;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    /* access modifiers changed from: protected */
    public r getMapFragmentDelegate() {
        if (this.a == null) {
            this.a = new ab();
        }
        return this.a;
    }

    public final void onCreate(Bundle bundle) {
        try {
            addView(getMapFragmentDelegate().a((LayoutInflater) null, (ViewGroup) null, bundle));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public final void onDestroy() {
        try {
            getMapFragmentDelegate().e();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public final void onLowMemory() {
        try {
            getMapFragmentDelegate().f();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public final void onPause() {
        try {
            getMapFragmentDelegate().c();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public final void onResume() {
        try {
            getMapFragmentDelegate().b();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        try {
            getMapFragmentDelegate().b(bundle);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
