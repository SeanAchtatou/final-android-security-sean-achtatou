package com.amap.api.maps.model;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import com.amap.api.a.x;

public class MyLocationStyle implements Parcelable {
    private BitmapDescriptor a = BitmapDescriptorFactory.fromAsset(x.a.marker_gps_no_sharing.name() + ".png");
    private float b = 0.5f;
    private float c = 0.5f;
    private int d = Color.argb(100, 0, 0, 180);
    private int e = Color.argb((int) MotionEventCompat.ACTION_MASK, 0, 0, 220);
    private float f = 1.0f;

    public MyLocationStyle anchor(float f2, float f3) {
        this.b = f2;
        this.c = f3;
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public float getAnchorU() {
        return this.b;
    }

    public float getAnchorV() {
        return this.c;
    }

    public BitmapDescriptor getMyLocationIcon() {
        return this.a;
    }

    public int getRadiusFillColor() {
        return this.d;
    }

    public int getStrokeColor() {
        return this.e;
    }

    public float getStrokeWidth() {
        return this.f;
    }

    public MyLocationStyle myLocationIcon(BitmapDescriptor bitmapDescriptor) {
        this.a = bitmapDescriptor;
        return this;
    }

    public MyLocationStyle radiusFillColor(int i) {
        this.d = i;
        return this;
    }

    public MyLocationStyle strokeColor(int i) {
        this.e = i;
        return this;
    }

    public MyLocationStyle strokeWidth(float f2) {
        this.f = f2;
        return this;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, i);
        parcel.writeFloat(this.b);
        parcel.writeFloat(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeFloat(this.f);
    }
}
