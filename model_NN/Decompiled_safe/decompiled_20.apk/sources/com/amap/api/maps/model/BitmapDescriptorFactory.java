package com.amap.api.maps.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.amap.api.a.ab;
import com.amap.api.a.x;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.FileInputStream;
import java.io.InputStream;

public final class BitmapDescriptorFactory {
    public static final float HUE_AZURE = 210.0f;
    public static final float HUE_BLUE = 240.0f;
    public static final float HUE_CYAN = 180.0f;
    public static final float HUE_GREEN = 120.0f;
    public static final float HUE_MAGENTA = 300.0f;
    public static final float HUE_ORANGE = 30.0f;
    public static final float HUE_RED = 0.0f;
    public static final float HUE_ROSE = 330.0f;
    public static final float HUE_VIOLET = 270.0f;
    public static final float HUE_YELLOW = 60.0f;

    public static BitmapDescriptor defaultMarker() {
        try {
            return fromAsset(x.a.marker_default.name() + ".png");
        } catch (Exception e) {
            return null;
        }
    }

    public static BitmapDescriptor defaultMarker(float f) {
        try {
            float f2 = (float) ((((int) (15.0f + f)) / 30) * 30);
            float f3 = f2 > 330.0f ? 330.0f : f2 < HUE_RED ? 0.0f : f2;
            String str = PoiTypeDef.All;
            if (f3 == HUE_RED) {
                str = "RED";
            } else if (f3 == 30.0f) {
                str = "ORANGE";
            } else if (f3 == 60.0f) {
                str = "YELLOW";
            } else if (f3 == 120.0f) {
                str = "GREEN";
            } else if (f3 == 180.0f) {
                str = "CYAN";
            } else if (f3 == 210.0f) {
                str = "AZURE";
            } else if (f3 == 240.0f) {
                str = "BLUE";
            } else if (f3 == 270.0f) {
                str = "VIOLET";
            } else if (f3 == 300.0f) {
                str = "MAGENTA";
            } else if (f3 == 330.0f) {
                str = "ROSE";
            }
            return fromAsset(str + ".png");
        } catch (Exception e) {
            return null;
        }
    }

    public static BitmapDescriptor fromAsset(String str) {
        try {
            InputStream resourceAsStream = BitmapDescriptorFactory.class.getResourceAsStream("/assets/" + str);
            Bitmap decodeStream = BitmapFactory.decodeStream(resourceAsStream);
            resourceAsStream.close();
            return fromBitmap(decodeStream);
        } catch (Exception e) {
            return null;
        }
    }

    public static BitmapDescriptor fromBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        return new BitmapDescriptor(bitmap);
    }

    public static BitmapDescriptor fromFile(String str) {
        try {
            Context context = ab.a;
            if (context == null) {
                return null;
            }
            FileInputStream openFileInput = context.openFileInput(str);
            Bitmap decodeStream = BitmapFactory.decodeStream(openFileInput);
            openFileInput.close();
            return fromBitmap(decodeStream);
        } catch (Exception e) {
            return null;
        }
    }

    public static BitmapDescriptor fromPath(String str) {
        try {
            return fromBitmap(BitmapFactory.decodeFile(str));
        } catch (Exception e) {
            return null;
        }
    }

    public static BitmapDescriptor fromResource(int i) {
        try {
            Context context = ab.a;
            if (context != null) {
                return fromBitmap(BitmapFactory.decodeStream(context.getResources().openRawResource(i)));
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
