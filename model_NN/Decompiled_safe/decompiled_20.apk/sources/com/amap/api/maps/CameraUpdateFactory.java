package com.amap.api.maps;

import android.graphics.Point;
import com.amap.api.a.j;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;

public final class CameraUpdateFactory {
    public static CameraUpdate newCameraPosition(CameraPosition cameraPosition) {
        return new CameraUpdate(j.a(cameraPosition));
    }

    public static CameraUpdate newLatLng(LatLng latLng) {
        return new CameraUpdate(j.a(latLng));
    }

    public static CameraUpdate newLatLngBounds(LatLngBounds latLngBounds, int i) {
        return new CameraUpdate(j.a(latLngBounds, i));
    }

    public static CameraUpdate newLatLngBounds(LatLngBounds latLngBounds, int i, int i2, int i3) {
        return new CameraUpdate(j.a(latLngBounds, i, i2, i3));
    }

    public static CameraUpdate newLatLngZoom(LatLng latLng, float f) {
        return new CameraUpdate(j.a(latLng, f));
    }

    public static CameraUpdate scrollBy(float f, float f2) {
        return new CameraUpdate(j.a(f, f2));
    }

    public static CameraUpdate zoomBy(float f) {
        return new CameraUpdate(j.b(f));
    }

    public static CameraUpdate zoomBy(float f, Point point) {
        return new CameraUpdate(j.a(f, point));
    }

    public static CameraUpdate zoomIn() {
        return new CameraUpdate(j.b());
    }

    public static CameraUpdate zoomOut() {
        return new CameraUpdate(j.c());
    }

    public static CameraUpdate zoomTo(float f) {
        return new CameraUpdate(j.a(f));
    }
}
