package com.amap.api.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class PolylineOptions implements Parcelable {
    public static final PolylineOptionsCreator CREATOR = new PolylineOptionsCreator();
    String a;
    private final List<LatLng> b = new ArrayList();
    private float c = 10.0f;
    private int d = -16777216;
    private float e = BitmapDescriptorFactory.HUE_RED;
    private boolean f = true;
    private boolean g = true;

    public final PolylineOptions add(LatLng latLng) {
        this.b.add(latLng);
        return this;
    }

    public final PolylineOptions add(LatLng... latLngArr) {
        this.b.addAll(Arrays.asList(latLngArr));
        return this;
    }

    public final PolylineOptions addAll(Iterable<LatLng> iterable) {
        for (LatLng add : iterable) {
            this.b.add(add);
        }
        return this;
    }

    public final PolylineOptions color(int i) {
        this.d = i;
        return this;
    }

    public final int describeContents() {
        return 0;
    }

    public final int getColor() {
        return this.d;
    }

    public final List<LatLng> getPoints() {
        return this.b;
    }

    public final float getWidth() {
        return this.c;
    }

    public final float getZIndex() {
        return this.e;
    }

    public final boolean isUseTexture() {
        return this.g;
    }

    public final boolean isVisible() {
        return this.f;
    }

    public final PolylineOptions setUseTexture(boolean z) {
        this.g = z;
        return this;
    }

    public final PolylineOptions visible(boolean z) {
        this.f = z;
        return this;
    }

    public final PolylineOptions width(float f2) {
        this.c = f2;
        return this;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(getPoints());
        parcel.writeFloat(getWidth());
        parcel.writeInt(getColor());
        parcel.writeFloat(getZIndex());
        parcel.writeByte((byte) (isVisible() ? 1 : 0));
        parcel.writeString(this.a);
    }

    public final PolylineOptions zIndex(float f2) {
        this.e = f2;
        return this;
    }
}
