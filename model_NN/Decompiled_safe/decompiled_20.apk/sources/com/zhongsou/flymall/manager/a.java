package com.zhongsou.flymall.manager;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import java.util.Stack;

public final class a {
    private static Stack<Activity> a;
    private static a b;

    private a() {
    }

    public static a a() {
        if (b == null) {
            b = new a();
        }
        return b;
    }

    public static void a(Activity activity) {
        if (a == null) {
            a = new Stack<>();
        }
        a.add(activity);
    }

    public static void a(Context context) {
        try {
            c();
            ((ActivityManager) context.getSystemService("activity")).restartPackage(context.getPackageName());
            System.exit(0);
        } catch (Exception e) {
        }
    }

    public static Activity b() {
        return a.lastElement();
    }

    public static void b(Activity activity) {
        if (activity != null) {
            a.remove(activity);
            activity.finish();
        }
    }

    public static void c() {
        int size = a.size();
        for (int i = 0; i < size; i++) {
            if (a.get(i) != null) {
                a.get(i).finish();
            }
        }
        a.clear();
    }
}
