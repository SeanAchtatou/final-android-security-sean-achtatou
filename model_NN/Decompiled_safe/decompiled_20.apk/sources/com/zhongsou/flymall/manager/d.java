package com.zhongsou.flymall.manager;

import android.os.Handler;
import android.os.Message;

final class d implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ int b = 0;
    final /* synthetic */ int c = 0;
    final /* synthetic */ Handler d;
    final /* synthetic */ b e;

    d(b bVar, String str, Handler handler) {
        this.e = bVar;
        this.a = str;
        this.d = handler;
    }

    public final void run() {
        Message obtain = Message.obtain();
        b bVar = this.e;
        obtain.obj = b.a(this.a, this.b, this.c);
        this.d.sendMessage(obtain);
    }
}
