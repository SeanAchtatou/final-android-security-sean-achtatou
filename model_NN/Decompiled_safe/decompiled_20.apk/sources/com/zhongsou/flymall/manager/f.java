package com.zhongsou.flymall.manager;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import org.apache.commons.httpclient.cookie.CookieSpec;

final class f extends Handler {
    final /* synthetic */ e a;

    f(e eVar) {
        this.a = eVar;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.a.d.dismiss();
                Toast.makeText(this.a.b, "无法下载安装文件，请检查SD卡是否挂载", 3000).show();
                return;
            case 1:
                this.a.f.setProgress(this.a.i);
                this.a.g.setText(this.a.r + CookieSpec.PATH_DELIM + this.a.q);
                return;
            case 2:
                this.a.d.dismiss();
                e.g(this.a);
                return;
            default:
                return;
        }
    }
}
