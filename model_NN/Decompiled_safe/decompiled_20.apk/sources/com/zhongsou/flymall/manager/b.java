package com.zhongsou.flymall.manager;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a;
import com.zhongsou.flymall.d;
import com.zhongsou.flymall.g.e;
import com.zhongsou.flymall.g.f;
import java.io.File;
import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class b {
    public static int a = 3;
    public static int b = 4;
    private static HashMap<String, SoftReference<Bitmap>> c = new HashMap<>();
    private static ExecutorService d = Executors.newFixedThreadPool(20);
    /* access modifiers changed from: private */
    public static Map<ImageView, String> e = Collections.synchronizedMap(new WeakHashMap());
    private Bitmap f;

    public b() {
    }

    public b(Resources resources) {
        this.f = BitmapFactory.decodeResource(resources, R.drawable.image_default);
    }

    public static Bitmap a(Resources resources) {
        return BitmapFactory.decodeResource(resources, R.drawable.image_default);
    }

    public static Bitmap a(String str) {
        if (c.containsKey(str)) {
            return (Bitmap) c.get(str).get();
        }
        return null;
    }

    public static Bitmap a(String str, int i, int i2) {
        d e2;
        Bitmap bitmap;
        Bitmap bitmap2;
        try {
            Bitmap a2 = a.a(str);
            if (i <= 0 || i2 <= 0) {
                bitmap = a2;
            } else {
                try {
                    bitmap = Bitmap.createScaledBitmap(a2, i, i2, true);
                } catch (d e3) {
                    d dVar = e3;
                    bitmap = bitmap2;
                    e2 = dVar;
                    e2.printStackTrace();
                    return bitmap;
                }
            }
            try {
                c.put(str, new SoftReference(bitmap));
            } catch (d e4) {
                e2 = e4;
                e2.printStackTrace();
                return bitmap;
            }
        } catch (d e5) {
            d dVar2 = e5;
            bitmap = null;
            e2 = dVar2;
        }
        return bitmap;
    }

    public static void a() {
        c.clear();
        e.clear();
    }

    private void a(String str, ImageView imageView, Bitmap bitmap, int i) {
        Bitmap bitmap2;
        if (str == null || str.trim().length() <= 0) {
            bitmap2 = bitmap;
        } else {
            if (!str.startsWith("http://")) {
                str = AppContext.a().getString(R.string.IMAGE_DOMAIN) + str;
                if (i > 0 && !str.contains("_")) {
                    str = str.replaceAll("(\\.[a-zA-Z]+$)", "_" + i + "$1");
                }
            }
            e.put(imageView, str);
            bitmap2 = a(str);
        }
        if (bitmap2 != null) {
            imageView.setImageBitmap(bitmap2);
            return;
        }
        String a2 = e.a(str);
        if (new File(imageView.getContext().getFilesDir() + File.separator + a2).exists()) {
            imageView.setImageBitmap(f.a(imageView.getContext(), a2));
            return;
        }
        imageView.setImageBitmap(bitmap);
        d.execute(new d(this, str, new c(this, imageView, str)));
    }

    public final void a(Bitmap bitmap) {
        this.f = bitmap;
    }

    public final void a(String str, ImageView imageView) {
        a(str, imageView, this.f, 0);
    }

    public final void a(String str, ImageView imageView, int i) {
        a(str, imageView, this.f, i);
    }
}
