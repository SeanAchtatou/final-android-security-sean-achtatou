package com.zhongsou.flymall.manager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.ab;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import java.io.File;

public final class e implements o {
    private static e a;
    /* access modifiers changed from: private */
    public Context b;
    private Dialog c;
    /* access modifiers changed from: private */
    public Dialog d;
    private Dialog e;
    /* access modifiers changed from: private */
    public ProgressBar f;
    /* access modifiers changed from: private */
    public TextView g;
    private ProgressDialog h;
    /* access modifiers changed from: private */
    public int i;
    private Thread j;
    /* access modifiers changed from: private */
    public boolean k;
    private String l = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String m = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String n = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String o = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String p = PoiTypeDef.All;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public String r;
    private f s;
    private boolean t;
    private boolean u = false;
    private String v = PoiTypeDef.All;
    private int w;
    /* access modifiers changed from: private */
    public ab x;
    /* access modifiers changed from: private */
    public Handler y = new f(this);
    private Runnable z = new k(this);

    public static e a() {
        if (a == null) {
            a = new e();
        }
        a.k = false;
        return a;
    }

    private void a(int i2) {
        if (this.t && this.h != null) {
            this.h.dismiss();
            this.h = null;
        }
        if (this.e != null) {
            this.e.dismiss();
            this.e = null;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.b);
        builder.setTitle("系统提示");
        if (i2 == 0) {
            builder.setMessage("您当前已经是最新版本");
        } else if (i2 == 1) {
            builder.setMessage("无法获取版本更新信息");
        }
        builder.setPositiveButton("确定", (DialogInterface.OnClickListener) null);
        this.e = builder.create();
        this.e.show();
    }

    static /* synthetic */ void g(e eVar) {
        Log.i("UpdateManager", "openApk");
        File file = new File(eVar.o);
        if (file.exists()) {
            Intent intent = new Intent();
            intent.addFlags(268435456);
            intent.setAction("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse("file://" + file.toString()), "application/vnd.android.package-archive");
            eVar.b.startActivity(intent);
        }
    }

    static /* synthetic */ void i(e eVar) {
        Log.i("UpdateManager", "showDownloadDialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(eVar.b);
        builder.setTitle("正在下载新版本");
        View inflate = LayoutInflater.from(eVar.b).inflate((int) R.layout.update_progress, (ViewGroup) null);
        eVar.f = (ProgressBar) inflate.findViewById(R.id.update_progress);
        eVar.g = (TextView) inflate.findViewById(R.id.update_progress_text);
        builder.setView(inflate);
        builder.setNegativeButton("取消", new i(eVar));
        builder.setOnCancelListener(new j(eVar));
        eVar.d = builder.create();
        eVar.d.setCanceledOnTouchOutside(false);
        eVar.d.show();
        Log.i("UpdateManager", "downloadApk");
        eVar.j = new Thread(eVar.z);
        eVar.j.start();
    }

    public final void a(Context context, boolean z2) {
        if (!this.u) {
            this.u = true;
            Log.i("UpdateManager", "checkAppUpdate");
            this.b = context;
            this.t = z2;
            Log.i("UpdateManager", "getCurrentVersion");
            try {
                PackageInfo packageInfo = this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 0);
                this.v = packageInfo.versionName;
                this.w = packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace(System.err);
            }
            if (z2) {
                if (this.h == null) {
                    this.h = ProgressDialog.show(this.b, null, "正在检测，请稍后...", true, false);
                } else if (this.h.isShowing()) {
                    return;
                } else {
                    if (this.e != null && this.e.isShowing()) {
                        return;
                    }
                }
            }
            Log.i("UpdateManager", "checkVersion");
            this.s = new f(this);
            this.s.a();
            this.s.d();
        }
    }

    public final void a(String str) {
        if (this.t) {
            a(1);
        }
        this.u = false;
    }

    public final void checkVersionSuccess(ab abVar) {
        Log.i("UpdateManager", "checkVersionSuccess");
        if (this.h == null || this.h.isShowing()) {
            if (this.t && this.h != null) {
                this.h.dismiss();
                this.h = null;
            }
            this.x = abVar;
            System.out.println(this.x);
            if (this.x != null) {
                if (this.w < this.x.getVersionCode()) {
                    this.m = this.x.getDownloadUrl();
                    this.l = this.x.getUpdateLog();
                    Log.i("UpdateManager", "showNoticeDialog");
                    AlertDialog.Builder builder = new AlertDialog.Builder(this.b);
                    builder.setTitle("软件版本更新");
                    builder.setMessage(this.l);
                    builder.setPositiveButton("立即更新", new g(this));
                    builder.setNegativeButton("以后再说", new h(this));
                    this.c = builder.create();
                    this.c.show();
                } else if (this.t) {
                    a(0);
                }
            }
            this.u = false;
        }
    }
}
