package com.zhongsou.flymall.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import com.zhongsou.flymall.g.e;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

final class c extends Handler {
    final /* synthetic */ ImageView a;
    final /* synthetic */ String b;
    final /* synthetic */ b c;

    c(b bVar, ImageView imageView, String str) {
        this.c = bVar;
        this.a = imageView;
        this.b = str;
    }

    public final void handleMessage(Message message) {
        String str = (String) b.e.get(this.a);
        if (str != null && str.equals(this.b) && message.obj != null) {
            this.a.setImageBitmap((Bitmap) message.obj);
            try {
                Context context = this.a.getContext();
                String a2 = e.a(this.b);
                Bitmap bitmap = (Bitmap) message.obj;
                if (bitmap != null && a2 != null && context != null) {
                    FileOutputStream openFileOutput = context.openFileOutput(a2, 0);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    openFileOutput.write(byteArrayOutputStream.toByteArray());
                    openFileOutput.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
