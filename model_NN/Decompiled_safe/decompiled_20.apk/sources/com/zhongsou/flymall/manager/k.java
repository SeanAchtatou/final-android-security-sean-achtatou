package com.zhongsou.flymall.manager;

import android.os.Environment;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;

final class k implements Runnable {
    final /* synthetic */ e a;

    k(e eVar) {
        this.a = eVar;
    }

    public final void run() {
        try {
            String str = "FlyMallApp_" + this.a.x.getVersionName() + ".apk";
            String str2 = "FlyMallApp_" + this.a.x.getVersionName() + ".tmp";
            if (Environment.getExternalStorageState().equals("mounted")) {
                String unused = this.a.n = Environment.getExternalStorageDirectory().getAbsolutePath() + "/FlyMall/Update/";
                File file = new File(this.a.n);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String unused2 = this.a.o = this.a.n + str;
                String unused3 = this.a.p = this.a.n + str2;
            }
            if (this.a.o == null || this.a.o == PoiTypeDef.All) {
                this.a.y.sendEmptyMessage(0);
                return;
            }
            File file2 = new File(this.a.o);
            File file3 = new File(this.a.p);
            FileOutputStream fileOutputStream = new FileOutputStream(file3);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.a.m).openConnection();
            httpURLConnection.connect();
            int contentLength = httpURLConnection.getContentLength();
            InputStream inputStream = httpURLConnection.getInputStream();
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            String unused4 = this.a.q = decimalFormat.format((double) ((((float) contentLength) / 1024.0f) / 1024.0f)) + "MB";
            byte[] bArr = new byte[1024];
            int i = 0;
            while (true) {
                int read = inputStream.read(bArr);
                i += read;
                String unused5 = this.a.r = decimalFormat.format((double) ((((float) i) / 1024.0f) / 1024.0f)) + "MB";
                int unused6 = this.a.i = (int) ((((float) i) / ((float) contentLength)) * 100.0f);
                this.a.y.sendEmptyMessage(1);
                if (read > 0) {
                    fileOutputStream.write(bArr, 0, read);
                    if (this.a.k) {
                        break;
                    }
                } else if (file3.renameTo(file2)) {
                    this.a.y.sendEmptyMessage(2);
                }
            }
            fileOutputStream.close();
            inputStream.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
