package com.zhongsou.flymall;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.activity.MainActivity;
import com.zhongsou.flymall.g.g;

public class AppStart extends Activity {
    static /* synthetic */ void a(AppStart appStart) {
        appStart.startActivity(new Intent(appStart, MainActivity.class));
        appStart.finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        View inflate = View.inflate(this, R.layout.start, null);
        setContentView(inflate);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.3f, 1.0f);
        alphaAnimation.setDuration(2000);
        inflate.startAnimation(alphaAnimation);
        alphaAnimation.setAnimationListener(new f(this));
        AppContext appContext = (AppContext) getApplication();
        if (g.a(appContext.a("cookie"))) {
            String a = appContext.a("cookie_name");
            String a2 = appContext.a("cookie_value");
            if (!g.a(a) && !g.a(a2)) {
                appContext.a("cookie", a + "=" + a2);
                appContext.a("cookie_domain", "cookie_name", "cookie_value", "cookie_version", "cookie_path");
            }
        }
        new g(this).start();
    }
}
