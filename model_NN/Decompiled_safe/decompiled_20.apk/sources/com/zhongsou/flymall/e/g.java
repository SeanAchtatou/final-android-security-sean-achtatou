package com.zhongsou.flymall.e;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import scala.collection.mutable.StringBuilder;

public final class g {
    public static final g a = null;

    static {
        new g();
    }

    private g() {
        a = this;
    }

    public static boolean a(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected();
        } catch (Exception e) {
            System.out.println(new StringBuilder().append((Object) "CheckConnectivity Exception: ").append((Object) e.getMessage()).toString());
            Log.v("connectivity", e.toString());
            return false;
        }
    }
}
