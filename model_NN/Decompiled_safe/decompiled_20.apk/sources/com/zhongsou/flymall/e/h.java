package com.zhongsou.flymall.e;

import android.util.Log;
import android.widget.Toast;
import com.zhongsou.flymall.AppContext;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ScalaSignature;

@ScalaSignature(bytes = "\u0006\u000193A!\u0001\u0002\u0001\u0017\ty\u0001\n\u001e;q\u0007>tG/\u001a=u\u00136\u0004HN\u0003\u0002\u0004\t\u0005\u0019a.\u001a;\u000b\u0005\u00151\u0011a\u00024ms6\fG\u000e\u001c\u0006\u0003\u000f!\t\u0001B\u001f5p]\u001e\u001cx.\u001e\u0006\u0002\u0013\u0005\u00191m\\7\u0004\u0001M\u0019\u0001\u0001\u0004\n\u0011\u00055\u0001R\"\u0001\b\u000b\u0003=\tQa]2bY\u0006L!!\u0005\b\u0003\r\u0005s\u0017PU3g!\t\u0019B#D\u0001\u0003\u0013\t)\"A\u0001\u0007J\u0011R$\boQ8oi\u0016DH\u000f\u0003\u0005\u0018\u0001\t\u0005\t\u0015!\u0003\u0019\u0003\u0019iW\r\u001e5pIB\u0011\u0011\u0004\b\b\u0003\u001biI!a\u0007\b\u0002\rA\u0013X\rZ3g\u0013\tibD\u0001\u0004TiJLgn\u001a\u0006\u000379AQ\u0001\t\u0001\u0005\u0002\u0005\na\u0001P5oSRtDC\u0001\u0012$!\t\u0019\u0002\u0001C\u0003\u0018?\u0001\u0007\u0001\u0004C\u0004&\u0001\t\u0007I\u0011\u0002\u0014\u0002\u0019M+%KV#S?\u0016\u0013&k\u0014*\u0016\u0003aAa\u0001\u000b\u0001!\u0002\u0013A\u0012!D*F%Z+%kX#S%>\u0013\u0006\u0005C\u0003+\u0001\u0011\u00051&A\bp]\"#H\u000f]\"pI\u0016,%O]8s)\tas\u0006\u0005\u0002\u000e[%\u0011aF\u0004\u0002\u0005+:LG\u000fC\u00031S\u0001\u0007\u0011'\u0001\u0004ti\u0006$Xo\u001d\t\u0003e]j\u0011a\r\u0006\u0003iU\n\u0001bY1mY\n\f7m\u001b\u0006\u0003m!\tA\"\u00198ee>LG-];fefL!\u0001O\u001a\u0003\u0015\u0005S\u0017\r_*uCR,8\u000fC\u0003;\u0001\u0011\u00051(A\bp]*\u001bxN\\\"pI\u0016,%O]8s)\taC\bC\u0003>s\u0001\u0007a(\u0001\u0003kg>t\u0007CA\n@\u0013\t\u0001%A\u0001\tIiR\u0004(j]8o%\u0016\u001c\bo\u001c8tK\")!\t\u0001C\u0001\u0007\u0006\u0001rN\u001c&t_:\u0004\u0016M]:f\u000bJ\u0014xN]\u000b\u0002Y!)Q\t\u0001C\u0005\r\u0006iqN\\*feZ,'/\u0012:s_J$\u0012\u0001\f\u0005\u0006\u0011\u0002!\t!S\u0001\u0010_:\u0014Uo]5oKN\u001cXI\u001d:peR\u0011AF\u0013\u0005\u0006\u0017\u001e\u0003\r\u0001G\u0001\u0004gR\u0014\b\"B'\u0001\t\u0003\u0019\u0015AD8o)>\\WM\\#ya&\u0014X\r\u001a")
public final class h implements n {
    private final String a;
    private final String b = "您的网速不给力，稍后再试下吧";

    public h(String str) {
        this.a = str;
    }

    public static void a(String str) {
        Toast.makeText(AppContext.a(), str, 0).show();
    }

    private void d() {
        String str = this.a;
        if (str == null || !str.equals("config")) {
            Log.d("Http", new StringBuilder().append((Object) "error on call:").append((Object) this.a).toString());
            Toast.makeText(AppContext.a(), this.b, 0).show();
        }
    }

    public final void a() {
        d();
    }

    public final void b() {
        d();
    }

    public final void c() {
        d();
    }
}
