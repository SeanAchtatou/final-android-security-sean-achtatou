package com.zhongsou.flymall.e;

import com.b.a.e;
import scala.Function1;
import scala.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractPartialFunction;
import scala.runtime.ObjectRef;

public final class k extends AbstractPartialFunction<Tuple2<String, Object>, Object> implements Serializable {
    private final ObjectRef a;

    public k(ObjectRef objectRef) {
        this.a = objectRef;
    }

    public final /* synthetic */ Object applyOrElse(Object obj, Function1 function1) {
        Tuple2 tuple2 = (Tuple2) obj;
        if (tuple2 == null) {
            return function1.apply(tuple2);
        }
        Object _2 = tuple2._2();
        return ((e) this.a.elem).put((String) tuple2._1(), _2);
    }

    public final /* bridge */ /* synthetic */ boolean isDefinedAt(Object obj) {
        return ((Tuple2) obj) != null;
    }
}
