package com.zhongsou.flymall.e;

import com.b.a.a;
import com.c.b.f;
import com.umeng.analytics.MobclickAgent;
import com.zhongsou.flymall.AppContext;

public final class p implements f {
    private static p a;

    private p() {
    }

    public static p a() {
        if (a == null) {
            a = new p();
        }
        return a;
    }

    public final <T> T a(String str, byte[] bArr) {
        String str2;
        try {
            str2 = new String(bArr);
            try {
                return new m(a.a(str2));
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            str2 = null;
            MobclickAgent.onError(AppContext.a(), str + ",parse json error:" + e.getMessage() + ",response:" + str2);
            e.printStackTrace();
            return null;
        }
    }
}
