package com.zhongsou.flymall.e;

import com.amap.api.location.LocationManagerProxy;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.b.a.b;
import com.b.a.e;
import scala.Predef$;
import scala.reflect.ScalaSignature;

@ScalaSignature(bytes = "\u0006\u0001q3A!\u0001\u0002\u0001\u0017\t\u0001\u0002\n\u001e;q\u0015N|gNU3ta>t7/\u001a\u0006\u0003\u0007\u0011\t1A\\3u\u0015\t)a!A\u0004gYfl\u0017\r\u001c7\u000b\u0005\u001dA\u0011\u0001\u0003>i_:<7o\\;\u000b\u0003%\t1aY8n\u0007\u0001\u0019\"\u0001\u0001\u0007\u0011\u00055\u0001R\"\u0001\b\u000b\u0003=\tQa]2bY\u0006L!!\u0005\b\u0003\r\u0005s\u0017PU3g\u0011!\u0019\u0002A!A!\u0002\u0013!\u0012\u0001\u00026t_:\u0004\"!\u0006\u000e\u000e\u0003YQ!a\u0006\r\u0002\u0011\u0019\f7\u000f\u001e6t_:T!!\u0007\u0005\u0002\u000f\u0005d\u0017NY1cC&\u00111D\u0006\u0002\u000b\u0015N{ej\u00142kK\u000e$\b\"B\u000f\u0001\t\u0003q\u0012A\u0002\u001fj]&$h\b\u0006\u0002 CA\u0011\u0001\u0005A\u0007\u0002\u0005!)1\u0003\ba\u0001)!)1\u0005\u0001C\u0001I\u0005Qq-\u001a;IK\u0006$\u0017J\u001c;\u0015\u0007\u0015B\u0013\u0007\u0005\u0002\u000eM%\u0011qE\u0004\u0002\u0004\u0013:$\b\"B\u0015#\u0001\u0004Q\u0013!C2iS2$g*Y7f!\tYcF\u0004\u0002\u000eY%\u0011QFD\u0001\u0007!J,G-\u001a4\n\u0005=\u0002$AB*ue&twM\u0003\u0002.\u001d!)!G\ta\u0001K\u00059A-\u001a4bk2$\b\"\u0002\u001b\u0001\t\u0003)\u0014AD4fi\"+\u0017\r\u001a\"p_2,\u0017M\u001c\u000b\u0003me\u0002\"!D\u001c\n\u0005ar!a\u0002\"p_2,\u0017M\u001c\u0005\u0006SM\u0002\rA\u000b\u0005\u0006w\u0001!\t\u0001P\u0001\u000eO\u0016$\b*Z1e'R\u0014\u0018N\\4\u0015\u0005)j\u0004\"B\u0015;\u0001\u0004Q\u0003\"B \u0001\t\u0003\u0001\u0015aB4fi\"+\u0017\rZ\u000b\u0002)!)!\t\u0001C\u0001\u0007\u0006aq-\u001a;C_\u0012L\u0018I\u001d:bsV\tA\t\u0005\u0002\u0016\u000b&\u0011aI\u0006\u0002\n\u0015N{e*\u0011:sCfDQ\u0001\u0013\u0001\u0005\u0002%\u000b1bZ3u\u0005>$\u0017\u0010T8oOV\t!\n\u0005\u0002\u000e\u0017&\u0011AJ\u0004\u0002\u0005\u0019>tw\rC\u0003O\u0001\u0011\u0005q*\u0001\bhKR\u0014u\u000eZ=C_>dW-\u00198\u0016\u0003YBQ!\u0015\u0001\u0005\u0002I\u000bQbZ3u\u0005>$\u0017p\u0015;sS:<W#\u0001\u0016\t\u000bQ\u0003A\u0011\u0001!\u0002\u000f\u001d,GOQ8es\")a\u000b\u0001C\u0001/\u00069q-\u001a;D_\u0012,W#A\u0013\t\u000be\u0003A\u0011\u0001*\u0002\u0015\u001d,G/T3tg\u0006<W\rC\u0003\\\u0001\u0011\u0005q*\u0001\u0003jg>[\u0007")
public class m {
    private final e a;

    public m(e eVar) {
        this.a = eVar;
        Predef$.MODULE$.require(eVar != null);
    }

    private e f() {
        return this.a.b("header");
    }

    public final b a() {
        return this.a.c("body");
    }

    public final String a(String str) {
        String f = f().f(str);
        return f == null ? PoiTypeDef.All : f;
    }

    public final String b() {
        String f = this.a.f("body");
        return f == null ? PoiTypeDef.All : f;
    }

    public final e c() {
        return this.a.b("body");
    }

    public final int d() {
        try {
            return f().d(LocationManagerProxy.KEY_STATUS_CHANGED);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public final boolean e() {
        return d() == 200;
    }
}
