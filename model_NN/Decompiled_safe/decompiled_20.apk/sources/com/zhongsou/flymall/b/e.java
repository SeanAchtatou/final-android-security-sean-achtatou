package com.zhongsou.flymall.b;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.umeng.fb.f;
import com.zhongsou.flymall.AppContext;

public class e extends SQLiteOpenHelper {
    public static final String[] b = {"KEYWORD"};
    public static final String[] c = {"KEYWORD", "LASTUPDATE"};
    public static final String[] d = {"cart_id", "gd_id", "article_id", f.V, "name", "img_local", "img_remote", "stock_attr", "num", "price", "invalidation", "stock", "act_stock", "is_checked", "is_act_changed", "act_id", "act_type"};
    public static final String[] e = {"area_id", "province", "city", "area", "province_id", "city_id"};
    protected SQLiteDatabase a;

    public e() {
        super(AppContext.a(), "flymall.db", (SQLiteDatabase.CursorFactory) null, 8);
    }

    public final void a() {
        this.a = getWritableDatabase();
    }

    public final void b() {
        this.a = getReadableDatabase();
    }

    public void close() {
        this.a.close();
        this.a = null;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE SEARCH_HISTORY (KEYWORD TEXT(50) NOT NULL ,LASTUPDATE TEXT(13) NOT NULL)");
        sQLiteDatabase.execSQL("CREATE TABLE SEARCH_HOT (KEYWORD TEXT(50) NOT NULL ,LASTUPDATE TEXT(13) NOT NULL)");
        sQLiteDatabase.execSQL("CREATE TABLE CART (cart_id INTEGER PRIMARY KEY AUTOINCREMENT ,gd_id INTEGER NOT NULL ,article_id INTEGER NOT NULL ,user_id INTEGER NOT NULL ,name TEXT(50) NOT NULL ,img_local TEXT(50) NULL, img_remote TEXT(50) NOT NULL, stock_attr TEXT(150) NULL,num INTEGER NOT NULL,price REAL NOT NULL,invalidation INTEGER NOT NULL,stock INTEGER NOT NULL,act_stock INTEGER NOT NULL,is_checked REAL NOT NULL,is_act_changed REAL NOT NULL,act_id INTEGER NOT NULL,act_type INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("CREATE TABLE AREAS (area_id INTEGER PRIMARY KEY AUTOINCREMENT ,province TEXT(60),city TEXT(60),area TEXT(60),province_id INTEGER,city_id INTEGER)");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i2 > i) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS SEARCH_HISTORY");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS SEARCH_HOT");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS CART");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS AREAS");
            onCreate(sQLiteDatabase);
        }
    }
}
