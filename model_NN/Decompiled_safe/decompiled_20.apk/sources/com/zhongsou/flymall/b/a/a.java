package com.zhongsou.flymall.b.a;

import android.content.ContentValues;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.fb.f;

public class a {
    private long a = 0;
    private long b = 0;
    private long c = 0;
    private long d = 0;
    private String e = PoiTypeDef.All;
    private String f = PoiTypeDef.All;
    private String g = PoiTypeDef.All;
    private String h = PoiTypeDef.All;
    private int i = 1;
    private double j = 0.0d;
    private int k = 1;
    private int l = 0;
    private int m = 0;
    private int n = 0;
    private long o = 0;
    private int p = 0;
    private int q = 0;

    public final ContentValues a() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("cart_id", Long.valueOf(this.a));
        contentValues.put("gd_id", Long.valueOf(this.b));
        contentValues.put("article_id", Long.valueOf(this.c));
        contentValues.put(f.V, Long.valueOf(this.d));
        contentValues.put("name", this.e);
        contentValues.put("img_local", this.f);
        contentValues.put("img_remote", this.g);
        contentValues.put("stock_attr", this.h);
        contentValues.put("num", Integer.valueOf(this.i));
        contentValues.put("price", Double.valueOf(this.j));
        contentValues.put("is_checked", Integer.valueOf(this.k));
        contentValues.put("invalidation", Integer.valueOf(this.l));
        contentValues.put("stock", Integer.valueOf(this.m));
        contentValues.put("act_stock", Integer.valueOf(this.n));
        contentValues.put("act_id", Long.valueOf(this.o));
        contentValues.put("act_type", Integer.valueOf(this.p));
        contentValues.put("is_act_changed", Integer.valueOf(this.q));
        return contentValues;
    }

    public long getAct_id() {
        return this.o;
    }

    public int getAct_stock() {
        return this.n;
    }

    public int getAct_type() {
        return this.p;
    }

    public long getArticle_id() {
        return this.c;
    }

    public long getCart_id() {
        return this.a;
    }

    public long getGd_id() {
        return this.b;
    }

    public String getImg_local() {
        return this.f;
    }

    public String getImg_remote() {
        return this.g;
    }

    public int getInvalidation() {
        return this.l;
    }

    public int getIs_act_changed() {
        return this.q;
    }

    public int getIs_checked() {
        return this.k;
    }

    public String getName() {
        return this.e;
    }

    public int getNum() {
        return this.i;
    }

    public double getPrice() {
        return this.j;
    }

    public int getStock() {
        return this.m;
    }

    public String getStock_attr() {
        return this.h;
    }

    public long getUser_id() {
        return this.d;
    }

    public void setAct_id(long j2) {
        this.o = j2;
    }

    public void setAct_stock(int i2) {
        this.n = i2;
    }

    public void setAct_type(int i2) {
        this.p = i2;
    }

    public void setArticle_id(long j2) {
        this.c = j2;
    }

    public void setCart_id(long j2) {
        this.a = j2;
    }

    public void setGd_id(long j2) {
        this.b = j2;
    }

    public void setImg_local(String str) {
        this.f = str;
    }

    public void setImg_remote(String str) {
        this.g = str;
    }

    public void setInvalidation(int i2) {
        this.l = i2;
    }

    public void setIs_act_changed(int i2) {
        this.q = i2;
    }

    public void setIs_checked(int i2) {
        this.k = i2;
    }

    public void setName(String str) {
        this.e = str;
    }

    public void setNum(int i2) {
        this.i = i2;
    }

    public void setPrice(double d2) {
        this.j = d2;
    }

    public void setStock(int i2) {
        this.m = i2;
    }

    public void setStock_attr(String str) {
        this.h = str;
    }

    public void setUser_id(long j2) {
        this.d = j2;
    }
}
