package com.zhongsou.flymall.b;

import com.umeng.fb.f;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.activity.dp;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.g.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class d {
    private static final String[] a = {"gd_id", "article_id", f.V};

    public static int a(int i, a aVar) {
        int act_stock = (i > aVar.getStock() || (f(aVar) && i > aVar.getAct_stock())) ? f(aVar) ? aVar.getAct_stock() <= aVar.getStock() ? aVar.getAct_stock() : aVar.getStock() : aVar.getStock() : i;
        if (act_stock < 0) {
            return 0;
        }
        if (act_stock <= 999) {
            return act_stock;
        }
        return 999;
    }

    public static int a(boolean z) {
        return z ? 1 : 0;
    }

    private static long a(c cVar, a aVar, List<String> list) {
        return a(cVar, aVar, list, a);
    }

    private static long a(c cVar, a aVar, List<String> list, String... strArr) {
        List<a> a2 = cVar.a(aVar, "1", strArr);
        if (a2 == null || a2.size() <= 0) {
            return cVar.a(aVar);
        }
        a aVar2 = a2.get(0);
        for (String next : list) {
            if (next.equals("num_add")) {
                aVar2.setNum(aVar2.getNum() + aVar.getNum());
            } else if (next.equals("num")) {
                aVar2.setNum(aVar.getNum());
            } else if (next.equals("is_checked")) {
                aVar2.setIs_checked(aVar.getIs_checked());
            } else if (next.equals("price")) {
                aVar2.setPrice(aVar.getPrice());
            } else if (next.equals(f.V)) {
                aVar2.setUser_id(aVar.getUser_id());
            } else if (next.equals("invalidation")) {
                aVar2.setInvalidation(aVar.getInvalidation());
            } else if (next.equals("stock")) {
                aVar2.setStock(aVar.getStock());
            } else if (next.equals("act_id")) {
                aVar2.setAct_id(aVar.getAct_id());
            } else if (next.equals("act_type")) {
                aVar2.setAct_type(aVar.getAct_type());
            } else if (next.equals("act_stock")) {
                aVar2.setAct_stock(aVar.getAct_stock());
            } else if (next.equals("is_act_changed")) {
                aVar2.setIs_act_changed(aVar.getIs_act_changed());
            }
        }
        cVar.a(aVar2, strArr);
        return aVar2.getCart_id();
    }

    public static List<a> a() {
        return b(AppContext.a().e());
    }

    public static List<Long> a(List<a> list) {
        List asList = Arrays.asList("num_add");
        c cVar = new c();
        cVar.a();
        ArrayList arrayList = new ArrayList();
        for (a next : list) {
            long a2 = a(cVar, next, asList);
            if (a(String.valueOf(next.getCart_id()))) {
                arrayList.add(Long.valueOf(a2));
            }
        }
        cVar.close();
        return arrayList;
    }

    public static List<a> a(List<String> list, long j) {
        c cVar = new c();
        cVar.a();
        List<a> a2 = cVar.a("cart_id in(" + c.a(list, ",") + ") and user_id = ?", String.valueOf(j));
        cVar.close();
        return a2;
    }

    public static void a(long j) {
        d(Arrays.asList(Long.valueOf(j)));
    }

    public static void a(a aVar) {
        List asList = Arrays.asList("num_add");
        c cVar = new c();
        cVar.a();
        a(cVar, aVar, asList);
        cVar.close();
    }

    public static void a(a aVar, List<String> list) {
        c cVar = new c();
        cVar.a();
        b(cVar, aVar, list);
        cVar.close();
    }

    public static void a(List<a> list, List<String> list2) {
        c cVar = new c();
        cVar.a();
        for (a b : list) {
            b(cVar, b, list2);
        }
        cVar.close();
    }

    private static boolean a(String str) {
        if (dp.b == null || dp.b.size() == 0) {
            return false;
        }
        for (String equals : dp.b) {
            if (str.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    public static int b(boolean z) {
        return z ? 1 : 0;
    }

    public static List<a> b(long j) {
        a aVar = new a();
        aVar.setUser_id(j);
        c cVar = new c();
        cVar.a();
        List<a> a2 = cVar.a(aVar, null, f.V);
        cVar.close();
        return a2;
    }

    public static void b(a aVar) {
        a(aVar, Arrays.asList("is_checked"));
    }

    private static void b(c cVar, a aVar, List<String> list) {
        a(cVar, aVar, list, "cart_id");
    }

    public static void b(List<a> list) {
        a(list, Arrays.asList("is_checked"));
    }

    public static int c(a aVar) {
        return a(aVar.getNum(), aVar);
    }

    public static void c(List<a> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (a cart_id : list) {
            arrayList.add(Long.valueOf(cart_id.getCart_id()));
        }
        d(arrayList);
    }

    public static void d(List<Long> list) {
        if (list != null && list.size() != 0) {
            c cVar = new c();
            cVar.a();
            for (Long longValue : list) {
                cVar.a(longValue.longValue());
            }
            cVar.close();
        }
    }

    public static boolean d(a aVar) {
        return aVar.getIs_checked() == 1;
    }

    public static boolean e(a aVar) {
        return aVar.getInvalidation() == 0;
    }

    private static boolean f(a aVar) {
        return aVar.getAct_id() > 0 && aVar.getAct_type() == 14;
    }
}
