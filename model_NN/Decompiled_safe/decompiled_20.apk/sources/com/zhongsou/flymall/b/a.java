package com.zhongsou.flymall.b;

import android.database.Cursor;
import com.b.a.e;
import com.zhongsou.flymall.d.b;
import java.util.ArrayList;
import java.util.List;

public final class a extends e {
    public final List<b> a(String[] strArr, String str, String... strArr2) {
        if (str == null || str.length() == 0) {
            return new ArrayList(0);
        }
        Cursor query = this.a.query(true, "AREAS", strArr, str, strArr2, null, null, null, null);
        if (query == null || query.isAfterLast()) {
            query.close();
            return new ArrayList(0);
        }
        com.b.a.b bVar = new com.b.a.b();
        while (query.moveToNext()) {
            e eVar = new e();
            for (String str2 : strArr) {
                eVar.put(str2, query.getString(query.getColumnIndex(str2)));
            }
            bVar.add(eVar);
        }
        query.close();
        return com.b.a.a.b(bVar.toString(), b.class);
    }
}
