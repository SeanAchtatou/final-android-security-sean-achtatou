package com.zhongsou.flymall.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.b.a.b;
import com.b.a.e;
import com.umeng.fb.f;
import com.zhongsou.flymall.b.a.a;
import java.util.ArrayList;
import java.util.List;

public final class c extends e {
    private static String a(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strArr.length; i++) {
            sb.append(strArr[i]).append("=?");
            if (i + 1 < strArr.length) {
                sb.append(" and ");
            }
        }
        return sb.toString();
    }

    private static String[] a(ContentValues contentValues, String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String asString : strArr) {
            arrayList.add(contentValues.getAsString(asString));
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public final int a(long j) {
        return this.a.delete("CART", "cart_id = ?", new String[]{String.valueOf(j)});
    }

    public final long a(a aVar) {
        if (aVar == null) {
            return 0;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("gd_id", Long.valueOf(aVar.getGd_id()));
        contentValues.put("article_id", Long.valueOf(aVar.getArticle_id()));
        contentValues.put(f.V, Long.valueOf(aVar.getUser_id()));
        contentValues.put("name", aVar.getName());
        contentValues.put("img_local", aVar.getImg_local());
        contentValues.put("img_remote", aVar.getImg_remote());
        contentValues.put("stock_attr", aVar.getStock_attr());
        contentValues.put("num", Integer.valueOf(aVar.getNum()));
        contentValues.put("price", Double.valueOf(aVar.getPrice()));
        contentValues.put("invalidation", Integer.valueOf(aVar.getInvalidation()));
        contentValues.put("stock", Integer.valueOf(aVar.getStock()));
        contentValues.put("act_stock", Integer.valueOf(aVar.getAct_stock()));
        contentValues.put("is_checked", Integer.valueOf(aVar.getIs_checked()));
        contentValues.put("is_act_changed", Integer.valueOf(aVar.getIs_act_changed()));
        contentValues.put("act_id", Long.valueOf(aVar.getAct_id()));
        contentValues.put("act_type", Integer.valueOf(aVar.getAct_type()));
        return this.a.insert("CART", null, contentValues);
    }

    public final long a(a aVar, String... strArr) {
        if (aVar == null) {
            return 0;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("cart_id", Long.valueOf(aVar.getCart_id()));
        contentValues.put("gd_id", Long.valueOf(aVar.getGd_id()));
        contentValues.put("article_id", Long.valueOf(aVar.getArticle_id()));
        contentValues.put(f.V, Long.valueOf(aVar.getUser_id()));
        contentValues.put("name", aVar.getName());
        contentValues.put("img_local", aVar.getImg_local());
        contentValues.put("img_remote", aVar.getImg_remote());
        contentValues.put("stock_attr", aVar.getStock_attr());
        contentValues.put("num", Integer.valueOf(aVar.getNum()));
        contentValues.put("price", Double.valueOf(aVar.getPrice()));
        contentValues.put("invalidation", Integer.valueOf(aVar.getInvalidation()));
        contentValues.put("stock", Integer.valueOf(aVar.getStock()));
        contentValues.put("act_stock", Integer.valueOf(aVar.getAct_stock()));
        contentValues.put("is_checked", Integer.valueOf(aVar.getIs_checked()));
        contentValues.put("is_act_changed", Integer.valueOf(aVar.getIs_act_changed()));
        contentValues.put("act_id", Long.valueOf(aVar.getAct_id()));
        contentValues.put("act_type", Integer.valueOf(aVar.getAct_type()));
        return (long) this.a.update("CART", contentValues, a(strArr), a(contentValues, strArr));
    }

    public final List<a> a(a aVar, String str, String... strArr) {
        String[] strArr2;
        if (aVar == null) {
            return new ArrayList(0);
        }
        ContentValues a = aVar.a();
        if (strArr == null || strArr.length <= 0) {
            strArr2 = null;
        } else {
            strArr2 = new String[strArr.length];
            for (int i = 0; i < strArr.length; i++) {
                strArr2[i] = a.getAsString(strArr[i]);
            }
        }
        Cursor query = this.a.query("CART", d, a(strArr), strArr2, null, null, str);
        if (query == null || query.isAfterLast()) {
            query.close();
            return new ArrayList(0);
        }
        b bVar = new b();
        while (query.moveToNext()) {
            e eVar = new e();
            for (String str2 : d) {
                if ("price".equals(str2)) {
                    eVar.put(str2, Double.valueOf(query.getDouble(query.getColumnIndex(str2))));
                } else {
                    eVar.put(str2, query.getString(query.getColumnIndex(str2)));
                }
            }
            bVar.add(eVar);
        }
        query.close();
        return com.b.a.a.b(bVar.toString(), a.class);
    }

    public final List<a> a(String str, String... strArr) {
        if (str == null || str.length() == 0) {
            return new ArrayList(0);
        }
        Cursor query = this.a.query("CART", d, str, strArr, null, null, null);
        if (query == null || query.isAfterLast()) {
            query.close();
            return new ArrayList(0);
        }
        b bVar = new b();
        while (query.moveToNext()) {
            e eVar = new e();
            for (String str2 : d) {
                if ("price".equals(str2)) {
                    eVar.put(str2, Double.valueOf(query.getDouble(query.getColumnIndex(str2))));
                } else {
                    eVar.put(str2, query.getString(query.getColumnIndex(str2)));
                }
            }
            bVar.add(eVar);
        }
        query.close();
        return com.b.a.a.b(bVar.toString(), a.class);
    }
}
