package com.zhongsou.flymall.b;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public final class j extends e {
    private static String f = "KEYWORD=?";

    public final long a(String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("KEYWORD", str);
        contentValues.put("LASTUPDATE", Long.valueOf(System.currentTimeMillis()));
        return this.a.insert("SEARCH_HOT", null, contentValues);
    }

    public final boolean c() {
        this.a.delete("SEARCH_HOT", null, null);
        return true;
    }

    public final List<String> d() {
        ArrayList arrayList = null;
        Cursor query = this.a.query("SEARCH_HOT", c, null, null, null, null, "LASTUPDATE desc");
        if (query.getCount() > 0) {
            arrayList = new ArrayList();
            query.moveToFirst();
            while (!query.isAfterLast()) {
                arrayList.add(query.getString(0));
                query.moveToNext();
            }
        }
        if (query != null) {
            query.close();
        }
        return arrayList;
    }
}
