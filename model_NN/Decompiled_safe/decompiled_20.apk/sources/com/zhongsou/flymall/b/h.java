package com.zhongsou.flymall.b;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public final class h extends e {
    private static String f = "KEYWORD=?";

    public final long a(String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("KEYWORD", str);
        contentValues.put("LASTUPDATE", Long.valueOf(System.currentTimeMillis()));
        return this.a.insert("SEARCH_HISTORY", null, contentValues);
    }

    public final long b(String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("LASTUPDATE", Long.valueOf(System.currentTimeMillis()));
        return (long) this.a.update("SEARCH_HISTORY", contentValues, f, new String[]{str});
    }

    public final long c(String str) {
        return (long) this.a.delete("SEARCH_HISTORY", f, new String[]{str});
    }

    public final List<String> c() {
        ArrayList arrayList = null;
        Cursor query = this.a.query("SEARCH_HISTORY", b, null, null, null, null, "LASTUPDATE desc");
        if (query.getCount() > 0) {
            arrayList = new ArrayList();
            query.moveToFirst();
            while (!query.isAfterLast()) {
                arrayList.add(query.getString(0));
                query.moveToNext();
            }
        }
        if (query != null) {
            query.close();
        }
        return arrayList;
    }
}
