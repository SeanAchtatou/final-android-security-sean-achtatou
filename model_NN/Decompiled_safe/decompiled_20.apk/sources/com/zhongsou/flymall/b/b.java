package com.zhongsou.flymall.b;

import java.util.List;

public final class b {
    public static List<com.zhongsou.flymall.d.b> a() {
        a aVar = new a();
        aVar.a();
        List<com.zhongsou.flymall.d.b> a = aVar.a(new String[]{"area_id", "province"}, "city_id = ?", "0");
        aVar.close();
        return a;
    }

    public static List<com.zhongsou.flymall.d.b> a(long j) {
        a aVar = new a();
        aVar.a();
        List<com.zhongsou.flymall.d.b> a = aVar.a(new String[]{"area_id", "city", "city_id"}, "province_id = ? and area_id = city_id", String.valueOf(j));
        aVar.close();
        return a;
    }

    public static List<com.zhongsou.flymall.d.b> b(long j) {
        a aVar = new a();
        aVar.a();
        List<com.zhongsou.flymall.d.b> a = aVar.a(new String[]{"area_id", "area"}, "city_id = ? and area_id != city_id", String.valueOf(j));
        aVar.close();
        return a;
    }
}
