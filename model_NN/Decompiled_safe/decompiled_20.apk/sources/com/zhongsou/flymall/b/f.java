package com.zhongsou.flymall.b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.IOException;

public final class f extends e {
    public final void a(BufferedReader bufferedReader) {
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            writableDatabase.beginTransaction();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    writableDatabase.setTransactionSuccessful();
                    writableDatabase.endTransaction();
                    writableDatabase.close();
                    return;
                } else if (!TextUtils.isEmpty(readLine)) {
                    writableDatabase.execSQL(readLine);
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public final boolean c() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor query = readableDatabase.query("AREAS", new String[]{"area_id"}, null, null, null, null, null, "1");
        if (query == null || query.isAfterLast()) {
            query.close();
            readableDatabase.close();
            return false;
        }
        query.close();
        readableDatabase.close();
        return true;
    }
}
