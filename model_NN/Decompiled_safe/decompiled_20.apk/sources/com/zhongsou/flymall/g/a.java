package com.zhongsou.flymall.g;

import android.text.Html;
import android.text.Spanned;
import com.amap.api.maps.model.LatLng;
import com.amap.api.search.core.LatLonPoint;
import java.text.DecimalFormat;

public final class a {
    public static Spanned a(String str) {
        if (str == null) {
            return null;
        }
        return Html.fromHtml(str.replace("\n", "<br />"));
    }

    public static LatLonPoint a(LatLng latLng) {
        return new LatLonPoint(latLng.latitude, latLng.longitude);
    }

    public static String a() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            sb.append("&nbsp;");
        }
        return sb.toString();
    }

    public static String a(int i) {
        if (i > 10000) {
            return (i / 1000) + "公里";
        } else if (i > 1000) {
            return new DecimalFormat("##0.0").format((double) (((float) i) / 1000.0f)) + "公里";
        } else if (i > 100) {
            return ((i / 50) * 50) + "米";
        } else {
            int i2 = (i / 10) * 10;
            if (i2 == 0) {
                i2 = 10;
            }
            return i2 + "米";
        }
    }

    public static String a(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<font color=").append(str2).append(">").append(str).append("</font>");
        return stringBuffer.toString();
    }

    public static boolean b(String str) {
        return str == null || str.trim().length() == 0;
    }
}
