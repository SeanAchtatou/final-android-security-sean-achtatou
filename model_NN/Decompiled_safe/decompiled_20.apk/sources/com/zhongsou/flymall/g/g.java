package com.zhongsou.flymall.g;

import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.common.b.e;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public final class g {
    private static final NumberFormat a;
    private static final Pattern b = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
    private static final ThreadLocal<SimpleDateFormat> c = new h();
    private static final ThreadLocal<SimpleDateFormat> d = new i();

    static {
        NumberFormat numberInstance = NumberFormat.getNumberInstance();
        a = numberInstance;
        numberInstance.setMaximumIntegerDigits(9);
        a.setMaximumFractionDigits(2);
        a.setMinimumFractionDigits(2);
    }

    public static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        inputStream.close();
        return sb.toString();
    }

    public static String a(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public static boolean a(Object obj) {
        return obj == null || obj.toString().length() == 0;
    }

    public static boolean a(String str) {
        if (str == null || PoiTypeDef.All.equals(str)) {
            return true;
        }
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != ' ' && charAt != 9 && charAt != 13 && charAt != 10) {
                return false;
            }
        }
        return true;
    }

    public static boolean b(Object obj) {
        return !a(obj);
    }

    public static boolean b(String str) {
        if (str == null || str.trim().length() == 0) {
            return false;
        }
        return b.matcher(str).matches();
    }

    public static int c(Object obj) {
        if (obj == null) {
            return 0;
        }
        return g(obj.toString());
    }

    public static String c(String str) {
        try {
            return URLEncoder.encode(str, e.f);
        } catch (Exception e) {
            return str;
        }
    }

    public static String d(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        return str.lastIndexOf("@") != -1 ? str.substring(0, str.lastIndexOf("@")) : str;
    }

    public static long e(String str) {
        try {
            return Long.parseLong(str);
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean f(String str) {
        try {
            return Boolean.parseBoolean(str);
        } catch (Exception e) {
            return false;
        }
    }

    private static int g(String str) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return 0;
        }
    }
}
