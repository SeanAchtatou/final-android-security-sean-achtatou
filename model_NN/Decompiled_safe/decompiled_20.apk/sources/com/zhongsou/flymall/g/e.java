package com.zhongsou.flymall.g;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import org.apache.commons.httpclient.cookie.CookieSpec;

public final class e {
    public static final BigInteger a;
    public static final BigInteger b;
    public static final BigInteger c = a.multiply(b);
    public static final BigInteger d = a.multiply(c);
    public static final BigInteger e = a.multiply(d);
    public static final BigInteger f = a.multiply(e);
    public static final BigInteger g = a.multiply(f);
    public static final BigInteger h = a.multiply(g);

    static {
        BigInteger valueOf = BigInteger.valueOf(1024);
        a = valueOf;
        b = valueOf.multiply(valueOf);
    }

    public static long a(File file) {
        try {
            if (!file.exists()) {
                throw new IllegalArgumentException(file + " does not exist");
            } else if (!file.isDirectory()) {
                throw new IllegalArgumentException(file + " is not a directory");
            } else {
                File[] listFiles = file.listFiles();
                if (listFiles == null) {
                    return 0;
                }
                long j = 0;
                for (File file2 : listFiles) {
                    if (!file2.exists()) {
                        throw new IllegalArgumentException(file2 + " does not exist");
                    }
                    j += file2.isDirectory() ? a(file2) : file2.length();
                    if (j < 0) {
                        return j;
                    }
                }
                return j;
            }
        } catch (IllegalArgumentException e2) {
            return 0;
        }
    }

    public static String a(long j) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        return j < 1024 ? decimalFormat.format((double) j) + "B" : j < 1048576 ? decimalFormat.format(((double) j) / 1024.0d) + "KB" : j < 1073741824 ? decimalFormat.format(((double) j) / 1048576.0d) + "MB" : decimalFormat.format(((double) j) / 1.073741824E9d) + "G";
    }

    public static String a(String str) {
        return g.a(str) ? PoiTypeDef.All : str.substring(str.lastIndexOf(File.separator) + 1);
    }

    public static void a(String str, String str2) {
        try {
            Runtime.getRuntime().exec("chmod " + str + " " + str2);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public static boolean b(File file) {
        boolean z = false;
        String path = file.getPath();
        if (file.exists() || !file.isDirectory()) {
        }
        String[] list = file.list();
        int i = 0;
        while (i < list.length) {
            File file2 = path.endsWith(File.separator) ? new File(path + list[i]) : new File(path + File.separator + list[i]);
            if (file2.isFile()) {
                file2.delete();
            }
            if (file2.isDirectory()) {
                b(new File(path + CookieSpec.PATH_DELIM + list[i]));
                new File(path + CookieSpec.PATH_DELIM + list[i]).delete();
            }
            i++;
            z = true;
        }
        return z;
    }
}
