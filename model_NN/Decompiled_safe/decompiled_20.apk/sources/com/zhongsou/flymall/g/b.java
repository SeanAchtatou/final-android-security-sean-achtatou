package com.zhongsou.flymall.g;

import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import java.math.BigDecimal;

public final class b {
    public static String a(double d) {
        return AppContext.a().getString(R.string.RMB) + a(c(d));
    }

    public static String a(BigDecimal bigDecimal) {
        return String.valueOf(bigDecimal.setScale(2, 4));
    }

    public static BigDecimal a(BigDecimal bigDecimal, double d) {
        return bigDecimal.add(c(d));
    }

    public static String b(double d) {
        return a(c(d));
    }

    private static BigDecimal c(double d) {
        return new BigDecimal(Double.toString(d));
    }
}
