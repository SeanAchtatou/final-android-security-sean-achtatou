package com.zhongsou.flymall.g;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public final class f {
    /* JADX WARNING: Unknown top exception splitter block from list: {B:17:0x001a=Splitter:B:17:0x001a, B:10:0x000f=Splitter:B:10:0x000f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(android.content.Context r3, java.lang.String r4) {
        /*
            r0 = 0
            java.io.FileInputStream r2 = r3.openFileInput(r4)     // Catch:{ FileNotFoundException -> 0x000d, OutOfMemoryError -> 0x0018, all -> 0x0023 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ FileNotFoundException -> 0x0032, OutOfMemoryError -> 0x0030 }
            r2.close()     // Catch:{ Exception -> 0x002a }
        L_0x000c:
            return r0
        L_0x000d:
            r1 = move-exception
            r2 = r0
        L_0x000f:
            r1.printStackTrace()     // Catch:{ all -> 0x002e }
            r2.close()     // Catch:{ Exception -> 0x0016 }
            goto L_0x000c
        L_0x0016:
            r1 = move-exception
            goto L_0x000c
        L_0x0018:
            r1 = move-exception
            r2 = r0
        L_0x001a:
            r1.printStackTrace()     // Catch:{ all -> 0x002e }
            r2.close()     // Catch:{ Exception -> 0x0021 }
            goto L_0x000c
        L_0x0021:
            r1 = move-exception
            goto L_0x000c
        L_0x0023:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0026:
            r2.close()     // Catch:{ Exception -> 0x002c }
        L_0x0029:
            throw r0
        L_0x002a:
            r1 = move-exception
            goto L_0x000c
        L_0x002c:
            r1 = move-exception
            goto L_0x0029
        L_0x002e:
            r0 = move-exception
            goto L_0x0026
        L_0x0030:
            r1 = move-exception
            goto L_0x001a
        L_0x0032:
            r1 = move-exception
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.zhongsou.flymall.g.f.a(android.content.Context, java.lang.String):android.graphics.Bitmap");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:10:0x0010=Splitter:B:10:0x0010, B:17:0x001b=Splitter:B:17:0x001b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.io.File r3) {
        /*
            r0 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x000e, OutOfMemoryError -> 0x0019, all -> 0x0024 }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x000e, OutOfMemoryError -> 0x0019, all -> 0x0024 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ FileNotFoundException -> 0x0033, OutOfMemoryError -> 0x0031 }
            r2.close()     // Catch:{ Exception -> 0x002b }
        L_0x000d:
            return r0
        L_0x000e:
            r1 = move-exception
            r2 = r0
        L_0x0010:
            r1.printStackTrace()     // Catch:{ all -> 0x002f }
            r2.close()     // Catch:{ Exception -> 0x0017 }
            goto L_0x000d
        L_0x0017:
            r1 = move-exception
            goto L_0x000d
        L_0x0019:
            r1 = move-exception
            r2 = r0
        L_0x001b:
            r1.printStackTrace()     // Catch:{ all -> 0x002f }
            r2.close()     // Catch:{ Exception -> 0x0022 }
            goto L_0x000d
        L_0x0022:
            r1 = move-exception
            goto L_0x000d
        L_0x0024:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0027:
            r2.close()     // Catch:{ Exception -> 0x002d }
        L_0x002a:
            throw r0
        L_0x002b:
            r1 = move-exception
            goto L_0x000d
        L_0x002d:
            r1 = move-exception
            goto L_0x002a
        L_0x002f:
            r0 = move-exception
            goto L_0x0027
        L_0x0031:
            r1 = move-exception
            goto L_0x001b
        L_0x0033:
            r1 = move-exception
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.zhongsou.flymall.g.f.a(java.io.File):android.graphics.Bitmap");
    }

    public static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.close();
                inputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }
}
