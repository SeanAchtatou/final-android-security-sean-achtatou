package com.zhongsou.flymall.g;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class d {
    public static j a(long j) {
        j jVar = new j();
        if (j != 0) {
            long j2 = j / 1000;
            jVar.a(j2 / 3600);
            jVar.b((j2 % 3600) / 60);
            jVar.c((j2 % 3600) % 60);
        }
        return jVar;
    }

    public static String a(String str) {
        Date b = b(str);
        return b == null ? PoiTypeDef.All : new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(b);
    }

    private static Date b(String str) {
        int i = 0;
        String[] strArr = {"yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy:MM:dd HH:mm:ss", "yyyy-MM-dd", "dd.MM.yy HH:mm", "yyyyMMdd HHmmss", "yyyyMMdd HHmm", "MM/dd/yy hh:mm a", "HH:mm:ss dd.MM.yyyy", "yyyy:MM:dd", "yyyy:MM:dd HH:mm", "dd.MM.yy", "yyyyMMdd", "EEE, dd MMM yyyy HH:mm:ss", "MM/dd/yy", "yyyy:MM:dd HH:mm:sss", "yyyy/MM/dd"};
        while (i < strArr.length) {
            try {
                return new SimpleDateFormat(strArr[i], Locale.ENGLISH).parse(str);
            } catch (ParseException e) {
                e.printStackTrace();
                i++;
            }
        }
        return null;
    }
}
