package com.zhongsou.flymall.g;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.util.Collection;

public final class c {
    public static String a(Collection<?> collection, String str) {
        if (collection == null) {
            return null;
        }
        if (str == null) {
            str = ",";
        }
        StringBuffer stringBuffer = new StringBuffer(1024);
        boolean z = true;
        for (Object next : collection) {
            if (next == null) {
                next = PoiTypeDef.All;
            }
            if (z) {
                z = false;
            } else {
                stringBuffer.append(str);
            }
            stringBuffer.append(next.toString());
        }
        return stringBuffer.toString();
    }
}
