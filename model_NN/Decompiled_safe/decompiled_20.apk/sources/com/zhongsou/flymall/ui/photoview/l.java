package com.zhongsou.flymall.ui.photoview;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.manager.b;

public final class l extends AsyncTask<String, Integer, Bitmap> {
    final /* synthetic */ PhotoViewWraper a;

    public l(PhotoViewWraper photoViewWraper) {
        this.a = photoViewWraper;
    }

    private static Bitmap a(String... strArr) {
        Exception e;
        String str = strArr[0];
        try {
            new b();
            Bitmap a2 = b.a(str);
            if (a2 != null) {
                return a2;
            }
            try {
                return b.a(str, 0, 0);
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            e = e3;
        }
        e.printStackTrace();
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        return a((String[]) objArr);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap == null) {
            bitmap = BitmapFactory.decodeResource(this.a.getResources(), R.drawable.no_image_default);
        }
        this.a.b.setImageBitmap(bitmap);
        this.a.b.setVisibility(0);
        this.a.a.setVisibility(8);
    }
}
