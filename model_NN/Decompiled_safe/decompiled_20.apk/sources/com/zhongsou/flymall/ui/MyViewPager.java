package com.zhongsou.flymall.ui;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

public class MyViewPager extends ViewPager {
    private int a;
    private int b;

    public MyViewPager(Context context) {
        super(context);
        a();
    }

    public MyViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        this.a = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEventCompat.ACTION_MASK) {
            case 0:
                this.b = (int) motionEvent.getX();
                break;
            case 2:
                int abs = Math.abs(((int) motionEvent.getX()) - this.b);
                Log.v("FAN", "mTouchSlop=" + this.a);
                if (abs > this.a / 2 && getParent() != null) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                this.b = (int) motionEvent.getX();
                break;
        }
        return super.dispatchTouchEvent(motionEvent);
    }
}
