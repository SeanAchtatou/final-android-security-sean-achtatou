package com.zhongsou.flymall.ui.photoview;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;

@TargetApi(5)
class q extends p {
    private int f = -1;
    private int g = 0;

    public q(Context context) {
        super(context);
    }

    public boolean a(MotionEvent motionEvent) {
        int i = 0;
        switch (motionEvent.getAction() & MotionEventCompat.ACTION_MASK) {
            case 0:
                this.f = motionEvent.getPointerId(0);
                break;
            case 1:
            case 3:
                this.f = -1;
                break;
            case 6:
                int action = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                if (motionEvent.getPointerId(action) == this.f) {
                    int i2 = action == 0 ? 1 : 0;
                    this.f = motionEvent.getPointerId(i2);
                    this.b = motionEvent.getX(i2);
                    this.c = motionEvent.getY(i2);
                    break;
                }
                break;
        }
        if (this.f != -1) {
            i = this.f;
        }
        this.g = motionEvent.findPointerIndex(i);
        return super.a(motionEvent);
    }

    /* access modifiers changed from: package-private */
    public final float b(MotionEvent motionEvent) {
        try {
            return motionEvent.getX(this.g);
        } catch (Exception e) {
            return motionEvent.getX();
        }
    }

    /* access modifiers changed from: package-private */
    public final float c(MotionEvent motionEvent) {
        try {
            return motionEvent.getY(this.g);
        } catch (Exception e) {
            return motionEvent.getY();
        }
    }
}
