package com.zhongsou.flymall.ui.photoview;

import android.view.ScaleGestureDetector;

final class s implements ScaleGestureDetector.OnScaleGestureListener {
    final /* synthetic */ r a;

    s(r rVar) {
        this.a = rVar;
    }

    public final boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        this.a.a.a(scaleGestureDetector.getScaleFactor(), scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
        return true;
    }

    public final boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return true;
    }

    public final void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
    }
}
