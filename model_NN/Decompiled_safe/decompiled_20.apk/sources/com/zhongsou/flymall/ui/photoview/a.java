package com.zhongsou.flymall.ui.photoview;

import android.os.Build;
import android.support.v4.view.ViewCompatJB;
import android.view.View;

public final class a {
    public static void a(View view, Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            ViewCompatJB.postOnAnimation(view, runnable);
        } else {
            view.postDelayed(runnable, 16);
        }
    }
}
