package com.zhongsou.flymall.ui.photoview;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

public final class b extends PagerAdapter {
    protected final Context a;
    protected c b;
    private List<String> c;

    public b(Context context, List<String> list) {
        this.a = context;
        this.c = list;
    }

    public final void a(c cVar) {
        this.b = cVar;
    }

    public final void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    public final int getCount() {
        return this.c.size();
    }

    public final /* synthetic */ Object instantiateItem(ViewGroup viewGroup, int i) {
        PhotoViewWraper photoViewWraper = new PhotoViewWraper(this.a);
        photoViewWraper.setUrl(this.c.get(i));
        photoViewWraper.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        viewGroup.addView(photoViewWraper, 0);
        return photoViewWraper;
    }

    public final boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public final void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        super.setPrimaryItem(viewGroup, i, obj);
        if (this.b != null) {
            this.b.a(i);
        }
    }
}
