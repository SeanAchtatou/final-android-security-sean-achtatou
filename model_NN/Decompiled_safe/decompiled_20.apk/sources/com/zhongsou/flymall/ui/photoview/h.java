package com.zhongsou.flymall.ui.photoview;

import android.content.Context;
import android.graphics.RectF;
import android.util.Log;
import android.widget.ImageView;

final class h implements Runnable {
    final /* synthetic */ d a;
    private final m b;
    private int c;
    private int d;

    public h(d dVar, Context context) {
        this.a = dVar;
        this.b = new n(context);
    }

    public final void a() {
        if (d.a) {
            Log.d("PhotoViewAttacher", "Cancel Fling");
        }
        this.b.b();
    }

    public final void a(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        RectF b2 = this.a.b();
        if (b2 != null) {
            int round = Math.round(-b2.left);
            if (((float) i) < b2.width()) {
                i6 = Math.round(b2.width() - ((float) i));
                i5 = 0;
            } else {
                i5 = round;
                i6 = round;
            }
            int round2 = Math.round(-b2.top);
            if (((float) i2) < b2.height()) {
                i8 = Math.round(b2.height() - ((float) i2));
                i7 = 0;
            } else {
                i7 = round2;
                i8 = round2;
            }
            this.c = round;
            this.d = round2;
            if (d.a) {
                Log.d("PhotoViewAttacher", "fling. StartX:" + round + " StartY:" + round2 + " MaxX:" + i6 + " MaxY:" + i8);
            }
            if (round != i6 || round2 != i8) {
                this.b.a(round, round2, i3, i4, i5, i6, i7, i8);
            }
        }
    }

    public final void run() {
        ImageView c2 = this.a.c();
        if (c2 != null && this.b.a()) {
            int c3 = this.b.c();
            int d2 = this.b.d();
            if (d.a) {
                Log.d("PhotoViewAttacher", "fling run(). CurrentX:" + this.c + " CurrentY:" + this.d + " NewX:" + c3 + " NewY:" + d2);
            }
            this.a.l.postTranslate((float) (this.c - c3), (float) (this.d - d2));
            this.a.b(this.a.j());
            this.c = c3;
            this.d = d2;
            a.a(c2, this);
        }
    }
}
