package com.zhongsou.flymall.ui.photoview;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import java.lang.ref.WeakReference;

public final class d implements GestureDetector.OnDoubleTapListener, View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener, t {
    static final boolean a = Log.isLoggable("PhotoViewAttacher", 3);
    private float b = 1.0f;
    private float c = 1.75f;
    private float d = 3.0f;
    private boolean e = true;
    /* access modifiers changed from: private */
    public WeakReference<ImageView> f;
    private ViewTreeObserver g;
    private GestureDetector h;
    private o i;
    private final Matrix j = new Matrix();
    private final Matrix k = new Matrix();
    /* access modifiers changed from: private */
    public final Matrix l = new Matrix();
    private final RectF m = new RectF();
    private final float[] n = new float[9];
    private i o;
    private j p;
    private k q;
    /* access modifiers changed from: private */
    public View.OnLongClickListener r;
    private int s;
    private int t;
    private int u;
    private int v;
    private h w;
    private int x = 2;
    private boolean y;
    private ImageView.ScaleType z = ImageView.ScaleType.FIT_CENTER;

    public d(ImageView imageView) {
        this.f = new WeakReference<>(imageView);
        imageView.setOnTouchListener(this);
        this.g = imageView.getViewTreeObserver();
        this.g.addOnGlobalLayoutListener(this);
        b(imageView);
        if (!imageView.isInEditMode()) {
            Context context = imageView.getContext();
            int i2 = Build.VERSION.SDK_INT;
            o pVar = i2 < 5 ? new p(context) : i2 < 8 ? new q(context) : new r(context);
            pVar.a = this;
            this.i = pVar;
            this.h = new GestureDetector(imageView.getContext(), new e(this));
            this.h.setOnDoubleTapListener(this);
            b(true);
        }
    }

    private RectF a(Matrix matrix) {
        Drawable drawable;
        ImageView c2 = c();
        if (c2 == null || (drawable = c2.getDrawable()) == null) {
            return null;
        }
        this.m.set(BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
        matrix.mapRect(this.m);
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void a(Drawable drawable) {
        ImageView c2 = c();
        if (c2 != null && drawable != null) {
            float width = (float) c2.getWidth();
            float height = (float) c2.getHeight();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            this.j.reset();
            float f2 = width / ((float) intrinsicWidth);
            float f3 = height / ((float) intrinsicHeight);
            if (this.z != ImageView.ScaleType.CENTER) {
                if (this.z != ImageView.ScaleType.CENTER_CROP) {
                    if (this.z != ImageView.ScaleType.CENTER_INSIDE) {
                        RectF rectF = new RectF(BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, (float) intrinsicWidth, (float) intrinsicHeight);
                        RectF rectF2 = new RectF(BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, width, height);
                        switch (f.a[this.z.ordinal()]) {
                            case 2:
                                this.j.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.START);
                                break;
                            case 3:
                                this.j.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.END);
                                break;
                            case 4:
                                this.j.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
                                break;
                            case 5:
                                this.j.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.FILL);
                                break;
                        }
                    } else {
                        float min = Math.min(1.0f, Math.min(f2, f3));
                        this.j.postScale(min, min);
                        this.j.postTranslate((width - (((float) intrinsicWidth) * min)) / 2.0f, (height - (((float) intrinsicHeight) * min)) / 2.0f);
                    }
                } else {
                    float max = Math.max(f2, f3);
                    this.j.postScale(max, max);
                    this.j.postTranslate((width - (((float) intrinsicWidth) * max)) / 2.0f, (height - (((float) intrinsicHeight) * max)) / 2.0f);
                }
            } else {
                this.j.postTranslate((width - ((float) intrinsicWidth)) / 2.0f, (height - ((float) intrinsicHeight)) / 2.0f);
            }
            m();
        }
    }

    private static boolean a(ImageView imageView) {
        return (imageView == null || imageView.getDrawable() == null) ? false : true;
    }

    private static void b(float f2, float f3, float f4) {
        if (f2 >= f3) {
            throw new IllegalArgumentException("MinZoom should be less than MidZoom");
        } else if (f3 >= f4) {
            throw new IllegalArgumentException("MidZoom should be less than MaxZoom");
        }
    }

    /* access modifiers changed from: private */
    public void b(Matrix matrix) {
        ImageView c2 = c();
        if (c2 != null) {
            ImageView c3 = c();
            if (c3 == null || (c3 instanceof PhotoView) || c3.getScaleType() == ImageView.ScaleType.MATRIX) {
                c2.setImageMatrix(matrix);
                if (this.o != null && a(matrix) != null) {
                    i iVar = this.o;
                    return;
                }
                return;
            }
            throw new IllegalStateException("The ImageView's ScaleType has been changed since attaching a PhotoViewAttacher");
        }
    }

    private static void b(ImageView imageView) {
        if (imageView != null && !(imageView instanceof PhotoView)) {
            imageView.setScaleType(ImageView.ScaleType.MATRIX);
        }
    }

    private void c(float f2, float f3, float f4) {
        ImageView c2 = c();
        if (c2 != null) {
            c2.post(new g(this, g(), f2, f3, f4));
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        l();
        b(j());
    }

    private void l() {
        RectF a2;
        float f2;
        float f3 = BitmapDescriptorFactory.HUE_RED;
        ImageView c2 = c();
        if (c2 != null && (a2 = a(j())) != null) {
            float height = a2.height();
            float width = a2.width();
            int height2 = c2.getHeight();
            if (height <= ((float) height2)) {
                switch (f.a[this.z.ordinal()]) {
                    case 2:
                        f2 = -a2.top;
                        break;
                    case 3:
                        f2 = (((float) height2) - height) - a2.top;
                        break;
                    default:
                        f2 = ((((float) height2) - height) / 2.0f) - a2.top;
                        break;
                }
            } else {
                f2 = a2.top > BitmapDescriptorFactory.HUE_RED ? -a2.top : a2.bottom < ((float) height2) ? ((float) height2) - a2.bottom : 0.0f;
            }
            int width2 = c2.getWidth();
            if (width <= ((float) width2)) {
                switch (f.a[this.z.ordinal()]) {
                    case 2:
                        f3 = -a2.left;
                        break;
                    case 3:
                        f3 = (((float) width2) - width) - a2.left;
                        break;
                    default:
                        f3 = ((((float) width2) - width) / 2.0f) - a2.left;
                        break;
                }
                this.x = 2;
            } else if (a2.left > BitmapDescriptorFactory.HUE_RED) {
                this.x = 0;
                f3 = -a2.left;
            } else if (a2.right < ((float) width2)) {
                f3 = ((float) width2) - a2.right;
                this.x = 1;
            } else {
                this.x = -1;
            }
            this.l.postTranslate(f3, f2);
        }
    }

    private void m() {
        this.l.reset();
        b(j());
        l();
    }

    public final void a() {
        if (this.f != null) {
            this.f.get().getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
        this.g = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.f = null;
    }

    public final void a(float f2) {
        b(f2, this.c, this.d);
        this.b = f2;
    }

    public final void a(float f2, float f3) {
        if (a) {
            Log.d("PhotoViewAttacher", String.format("onDrag: dx: %.2f. dy: %.2f", Float.valueOf(f2), Float.valueOf(f3)));
        }
        ImageView c2 = c();
        if (c2 != null && a(c2)) {
            this.l.postTranslate(f2, f3);
            k();
            if (this.e && !this.i.a()) {
                if (this.x == 2 || ((this.x == 0 && f2 >= 1.0f) || (this.x == 1 && f2 <= -1.0f))) {
                    c2.getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
        }
    }

    public final void a(float f2, float f3, float f4) {
        if (a) {
            Log.d("PhotoViewAttacher", String.format("onScale: scale: %.2f. fX: %.2f. fY: %.2f", Float.valueOf(f2), Float.valueOf(f3), Float.valueOf(f4)));
        }
        if (!a(c())) {
            return;
        }
        if (g() < this.d || f2 < 1.0f) {
            this.l.postScale(f2, f2, f3, f4);
            k();
        }
    }

    public final void a(float f2, float f3, float f4, float f5) {
        if (a) {
            Log.d("PhotoViewAttacher", "onFling. sX: " + f2 + " sY: " + f3 + " Vx: " + f4 + " Vy: " + f5);
        }
        ImageView c2 = c();
        if (a(c2)) {
            this.w = new h(this, c2.getContext());
            this.w.a(c2.getWidth(), c2.getHeight(), (int) f4, (int) f5);
            c2.post(this.w);
        }
    }

    public final void a(View.OnLongClickListener onLongClickListener) {
        this.r = onLongClickListener;
    }

    public final void a(ImageView.ScaleType scaleType) {
        boolean z2;
        if (scaleType != null) {
            switch (f.a[scaleType.ordinal()]) {
                case 1:
                    throw new IllegalArgumentException(scaleType.name() + " is not supported in PhotoView");
                default:
                    z2 = true;
                    break;
            }
        } else {
            z2 = false;
        }
        if (z2 && scaleType != this.z) {
            this.z = scaleType;
            i();
        }
    }

    public final void a(i iVar) {
        this.o = iVar;
    }

    public final void a(j jVar) {
        this.p = jVar;
    }

    public final void a(k kVar) {
        this.q = kVar;
    }

    public final void a(boolean z2) {
        this.e = z2;
    }

    public final RectF b() {
        l();
        return a(j());
    }

    public final void b(float f2) {
        b(this.b, f2, this.d);
        this.c = f2;
    }

    public final void b(boolean z2) {
        this.y = z2;
        i();
    }

    public final ImageView c() {
        ImageView imageView = null;
        if (this.f != null) {
            imageView = this.f.get();
        }
        if (imageView == null) {
            a();
        }
        return imageView;
    }

    public final void c(float f2) {
        b(this.b, this.c, f2);
        this.d = f2;
    }

    public final float d() {
        return this.b;
    }

    public final float e() {
        return this.c;
    }

    public final float f() {
        return this.d;
    }

    public final float g() {
        this.l.getValues(this.n);
        return this.n[0];
    }

    public final ImageView.ScaleType h() {
        return this.z;
    }

    public final void i() {
        ImageView c2 = c();
        if (c2 == null) {
            return;
        }
        if (this.y) {
            b(c2);
            a(c2.getDrawable());
            return;
        }
        m();
    }

    /* access modifiers changed from: protected */
    public final Matrix j() {
        this.k.set(this.j);
        this.k.postConcat(this.l);
        return this.k;
    }

    public final boolean onDoubleTap(MotionEvent motionEvent) {
        try {
            float g2 = g();
            float x2 = motionEvent.getX();
            float y2 = motionEvent.getY();
            if (g2 < this.c) {
                c(this.c, x2, y2);
                return true;
            } else if (g2 < this.c || g2 >= this.d) {
                c(this.b, x2, y2);
                return true;
            } else {
                c(this.d, x2, y2);
                return true;
            }
        } catch (ArrayIndexOutOfBoundsException e2) {
            return true;
        }
    }

    public final boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    public final void onGlobalLayout() {
        ImageView c2 = c();
        if (c2 != null && this.y) {
            int top = c2.getTop();
            int right = c2.getRight();
            int bottom = c2.getBottom();
            int left = c2.getLeft();
            if (top != this.s || bottom != this.u || left != this.v || right != this.t) {
                a(c2.getDrawable());
                this.s = top;
                this.t = right;
                this.u = bottom;
                this.v = left;
            }
        }
    }

    public final boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        RectF b2;
        if (c() != null) {
            if (this.p != null && (b2 = b()) != null && b2.contains(motionEvent.getX(), motionEvent.getY())) {
                float f2 = b2.left;
                b2.width();
                float f3 = b2.top;
                b2.height();
                j jVar = this.p;
                return true;
            } else if (this.q != null) {
                k kVar = this.q;
                motionEvent.getX();
                motionEvent.getY();
            }
        }
        return false;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        RectF b2;
        boolean z2 = false;
        if (!this.y) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                view.getParent().requestDisallowInterceptTouchEvent(true);
                if (this.w != null) {
                    this.w.a();
                    this.w = null;
                    break;
                }
                break;
            case 1:
            case 3:
                if (g() < this.b && (b2 = b()) != null) {
                    view.post(new g(this, g(), this.b, b2.centerX(), b2.centerY()));
                    z2 = true;
                    break;
                }
        }
        if (this.h != null && this.h.onTouchEvent(motionEvent)) {
            z2 = true;
        }
        if (this.i == null || !this.i.a(motionEvent)) {
            return z2;
        }
        return true;
    }
}
