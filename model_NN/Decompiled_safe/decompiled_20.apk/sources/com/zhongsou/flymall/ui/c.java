package com.zhongsou.flymall.ui;

import android.content.Context;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.c.b;

final class c implements View.OnClickListener {
    final /* synthetic */ b a;

    c(b bVar) {
        this.a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [android.app.Activity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public final void onClick(View view) {
        if (!AppContext.a().b()) {
            b.a((Context) this.a.a.f, (int) R.string.network_not_connected);
        } else if (this.a.a.e != null && !this.a.a.d.isShown() && this.a.a.d.getVisibility() == 8) {
            this.a.a.e.a();
        }
    }
}
