package com.zhongsou.flymall.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;

public class ScrollLayout extends ViewGroup {
    private Scroller a;
    private VelocityTracker b;
    private int c;
    private int d;
    private int e;
    private int f;
    private float g;
    private float h;
    private i i;
    private boolean j;

    public ScrollLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ScrollLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = 0;
        this.e = 0;
        this.j = true;
        this.a = new Scroller(context);
        this.c = this.d;
        this.f = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    public final void a(int i2) {
        if (!this.j) {
            setToScreen(i2);
            return;
        }
        int max = Math.max(0, Math.min(i2, getChildCount() - 1));
        if (getScrollX() != getWidth() * max) {
            int width = (getWidth() * max) - getScrollX();
            this.a.startScroll(getScrollX(), 0, width, 0, Math.abs(width) * 1);
            this.c = max;
            invalidate();
            if (this.i != null) {
                this.i.a(this.c);
            }
        }
    }

    public final void a(i iVar) {
        this.i = iVar;
    }

    public void computeScroll() {
        if (this.a.computeScrollOffset()) {
            scrollTo(this.a.getCurrX(), this.a.getCurrY());
            postInvalidate();
        }
    }

    public int getCurScreen() {
        return this.c;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int i2 = 1;
        int action = motionEvent.getAction();
        if (action == 2 && this.e != 0) {
            return true;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 0:
                this.g = x;
                this.h = y;
                if (this.a.isFinished()) {
                    i2 = 0;
                }
                this.e = i2;
                break;
            case 1:
            case 3:
                this.e = 0;
                break;
            case 2:
                if (((int) Math.abs(this.g - x)) > this.f) {
                    this.e = 1;
                    break;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = 0;
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                childAt.layout(i6, 0, i6 + measuredWidth, childAt.getMeasuredHeight());
                i6 += measuredWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int size = View.MeasureSpec.getSize(i2);
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException("ScrollLayout only canmCurScreen run at EXACTLY mode!");
        } else if (View.MeasureSpec.getMode(i3) != 1073741824) {
            throw new IllegalStateException("ScrollLayout only can run at EXACTLY mode!");
        } else {
            int childCount = getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                getChildAt(i4).measure(i2, i3);
            }
            scrollTo(this.c * size, 0);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.j) {
            return false;
        }
        if (this.b == null) {
            this.b = VelocityTracker.obtain();
        }
        this.b.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (action) {
            case 0:
                if (!this.a.isFinished()) {
                    this.a.abortAnimation();
                }
                this.g = x;
                this.h = y;
                break;
            case 1:
                VelocityTracker velocityTracker = this.b;
                velocityTracker.computeCurrentVelocity(1000);
                int xVelocity = (int) velocityTracker.getXVelocity();
                if (xVelocity > 600 && this.c > 0) {
                    a(this.c - 1);
                } else if (xVelocity >= -600 || this.c >= getChildCount() - 1) {
                    int width = getWidth();
                    a((getScrollX() + (width / 2)) / width);
                } else {
                    a(this.c + 1);
                }
                if (this.b != null) {
                    this.b.recycle();
                    this.b = null;
                }
                this.e = 0;
                break;
            case 2:
                int i2 = (int) (this.g - x);
                int i3 = (int) (this.h - y);
                if (Math.abs(i2) >= 200 || Math.abs(i3) <= 10) {
                    this.h = y;
                    this.g = x;
                    scrollBy(i2, 0);
                    break;
                }
            case 3:
                this.e = 0;
                break;
        }
        return true;
    }

    public void setIsScroll(boolean z) {
        this.j = z;
    }

    public void setToScreen(int i2) {
        int max = Math.max(0, Math.min(i2, getChildCount() - 1));
        this.c = max;
        scrollTo(max * getWidth(), 0);
        if (this.i != null) {
            this.i.a(this.c);
        }
    }
}
