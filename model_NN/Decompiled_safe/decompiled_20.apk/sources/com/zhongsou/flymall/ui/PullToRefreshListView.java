package com.zhongsou.flymall.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.unionpay.upomp.lthj.plugin.ui.R;

public class PullToRefreshListView extends ListView implements AbsListView.OnScrollListener {
    public h a;
    private LayoutInflater b;
    private LinearLayout c;
    private TextView d;
    private TextView e;
    private ImageView f;
    private ProgressBar g;
    private RotateAnimation h;
    private RotateAnimation i;
    private boolean j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private boolean r;
    private boolean s = true;

    public PullToRefreshListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public PullToRefreshListView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    public PullToRefreshListView(Context context, AttributeSet attributeSet, int i2, boolean z) {
        super(context, attributeSet, i2);
        a(context);
        setHasPullToRefresh(z);
    }

    private void a(Context context) {
        this.h = new RotateAnimation(BitmapDescriptorFactory.HUE_RED, -180.0f, 1, 0.5f, 1, 0.5f);
        this.h.setInterpolator(new LinearInterpolator());
        this.h.setDuration(100);
        this.h.setFillAfter(true);
        this.i = new RotateAnimation(-180.0f, BitmapDescriptorFactory.HUE_RED, 1, 0.5f, 1, 0.5f);
        this.i.setInterpolator(new LinearInterpolator());
        this.i.setDuration(100);
        this.i.setFillAfter(true);
        this.b = LayoutInflater.from(context);
        this.c = (LinearLayout) this.b.inflate((int) R.layout.pull_to_refresh_head, (ViewGroup) null);
        this.f = (ImageView) this.c.findViewById(R.id.head_arrowImageView);
        this.f.setMinimumWidth(50);
        this.f.setMinimumHeight(50);
        this.g = (ProgressBar) this.c.findViewById(R.id.head_progressBar);
        this.d = (TextView) this.c.findViewById(R.id.head_tipsTextView);
        this.e = (TextView) this.c.findViewById(R.id.head_lastUpdatedTextView);
        this.m = this.c.getPaddingTop();
        LinearLayout linearLayout = this.c;
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-1, -2);
        }
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(0, 0, layoutParams.width);
        int i2 = layoutParams.height;
        linearLayout.measure(childMeasureSpec, i2 > 0 ? View.MeasureSpec.makeMeasureSpec(i2, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0));
        this.l = this.c.getMeasuredHeight();
        this.k = this.c.getMeasuredWidth();
        this.c.setPadding(this.c.getPaddingLeft(), this.l * -1, this.c.getPaddingRight(), this.c.getPaddingBottom());
        this.c.invalidate();
        addHeaderView(this.c);
        setOnScrollListener(this);
    }

    private void b() {
        switch (this.q) {
            case 0:
                this.g.setVisibility(8);
                this.d.setVisibility(0);
                this.e.setVisibility(0);
                this.f.clearAnimation();
                this.f.setVisibility(0);
                if (this.r) {
                    this.r = false;
                    this.f.clearAnimation();
                    this.f.startAnimation(this.i);
                }
                this.d.setText((int) R.string.pull_down_refresh);
                return;
            case 1:
                this.f.setVisibility(0);
                this.g.setVisibility(8);
                this.d.setVisibility(0);
                this.e.setVisibility(0);
                this.f.clearAnimation();
                this.f.startAnimation(this.h);
                this.d.setText((int) R.string.release_update);
                return;
            case 2:
                this.c.setPadding(this.c.getPaddingLeft(), this.m, this.c.getPaddingRight(), this.c.getPaddingBottom());
                this.c.invalidate();
                this.g.setVisibility(0);
                this.f.clearAnimation();
                this.f.setVisibility(8);
                this.d.setText((int) R.string.loading);
                this.e.setVisibility(8);
                return;
            case 3:
                this.c.setPadding(this.c.getPaddingLeft(), this.l * -1, this.c.getPaddingRight(), this.c.getPaddingBottom());
                this.c.invalidate();
                this.g.setVisibility(8);
                this.f.clearAnimation();
                this.f.setImageResource(R.drawable.ic_pulltorefresh_arrow);
                this.d.setText((int) R.string.pull_down_refresh);
                this.e.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public final void a() {
        this.q = 3;
        b();
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        this.o = i2;
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
        this.p = i2;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.s) {
            return super.onTouchEvent(motionEvent);
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (this.o == 0 && !this.j) {
                    this.n = (int) motionEvent.getY();
                    this.j = true;
                    break;
                }
            case 1:
            case 3:
                if (!(this.q == 2 || this.q == 3)) {
                    if (this.q == 0) {
                        this.q = 3;
                        b();
                    } else if (this.q == 1) {
                        this.q = 2;
                        b();
                        if (this.a != null) {
                            h hVar = this.a;
                        }
                    }
                }
                this.j = false;
                this.r = false;
                break;
            case 2:
                int y = (int) motionEvent.getY();
                if (!this.j && this.o == 0) {
                    this.j = true;
                    this.n = y;
                }
                if (this.q != 2 && this.j) {
                    if (this.q == 1) {
                        if (y - this.n < this.l + 20 && y - this.n > 0) {
                            this.q = 0;
                            b();
                        } else if (y - this.n <= 0) {
                            this.q = 3;
                            b();
                        }
                    } else if (this.q == 0) {
                        if (y - this.n >= this.l + 20 && this.p == 1) {
                            this.q = 1;
                            this.r = true;
                            b();
                        } else if (y - this.n <= 0) {
                            this.q = 3;
                            b();
                        }
                    } else if (this.q == 3 && y - this.n > 0) {
                        this.q = 0;
                        b();
                    }
                    if (this.q == 0) {
                        this.c.setPadding(this.c.getPaddingLeft(), (this.l * -1) + (y - this.n), this.c.getPaddingRight(), this.c.getPaddingBottom());
                        this.c.invalidate();
                    }
                    if (this.q == 1) {
                        this.c.setPadding(this.c.getPaddingLeft(), (y - this.n) - this.l, this.c.getPaddingRight(), this.c.getPaddingBottom());
                        this.c.invalidate();
                        break;
                    }
                }
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setHasPullToRefresh(boolean z) {
        this.s = z;
    }

    public void setOnRefreshListener(h hVar) {
        this.a = hVar;
    }
}
