package com.zhongsou.flymall.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.unionpay.upomp.lthj.plugin.ui.R;
import java.util.Date;

public class HomeListView extends ListView implements AbsListView.OnScrollListener {
    private LayoutInflater a;
    private LinearLayout b;
    private TextView c;
    private TextView d;
    private ImageView e;
    private ProgressBar f;
    private RotateAnimation g;
    private RotateAnimation h;
    private boolean i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private g p;
    private boolean q;

    public HomeListView(Context context) {
        super(context);
        a(context);
    }

    public HomeListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private String a(int i2) {
        return getResources().getString(i2);
    }

    private void a(Context context) {
        setCacheColorHint(context.getResources().getColor(R.color.transparent));
        this.a = LayoutInflater.from(context);
        this.b = (LinearLayout) this.a.inflate((int) R.layout.home_head, (ViewGroup) null);
        this.e = (ImageView) this.b.findViewById(R.id.head_arrowImageView);
        this.e.setMinimumWidth(70);
        this.e.setMinimumHeight(50);
        this.f = (ProgressBar) this.b.findViewById(R.id.head_progressBar);
        this.c = (TextView) this.b.findViewById(R.id.head_tipsTextView);
        this.d = (TextView) this.b.findViewById(R.id.head_lastUpdatedTextView);
        LinearLayout linearLayout = this.b;
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-1, -2);
        }
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(0, 0, layoutParams.width);
        int i2 = layoutParams.height;
        linearLayout.measure(childMeasureSpec, i2 > 0 ? View.MeasureSpec.makeMeasureSpec(i2, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0));
        this.k = this.b.getMeasuredHeight();
        this.j = this.b.getMeasuredWidth();
        this.b.setPadding(0, this.k * -1, 0, 0);
        this.b.invalidate();
        Log.v("size", "width:" + this.j + " height:" + this.k);
        addHeaderView(this.b, null, false);
        setOnScrollListener(this);
        this.g = new RotateAnimation(BitmapDescriptorFactory.HUE_RED, -180.0f, 1, 0.5f, 1, 0.5f);
        this.g.setInterpolator(new LinearInterpolator());
        this.g.setDuration(250);
        this.g.setFillAfter(true);
        this.h = new RotateAnimation(-180.0f, BitmapDescriptorFactory.HUE_RED, 1, 0.5f, 1, 0.5f);
        this.h.setInterpolator(new LinearInterpolator());
        this.h.setDuration(200);
        this.h.setFillAfter(true);
        this.n = 3;
        this.q = false;
    }

    private void b() {
        switch (this.n) {
            case 0:
                this.e.setVisibility(0);
                this.f.setVisibility(8);
                this.c.setVisibility(0);
                this.d.setVisibility(0);
                this.e.clearAnimation();
                this.e.startAnimation(this.g);
                this.c.setText(a((int) R.string.release_update));
                Log.v("listview", "当前状态，松开刷新");
                return;
            case 1:
                this.f.setVisibility(8);
                this.c.setVisibility(0);
                this.d.setVisibility(0);
                this.e.clearAnimation();
                this.e.setVisibility(0);
                if (this.o) {
                    this.o = false;
                    this.e.clearAnimation();
                    this.e.startAnimation(this.h);
                }
                this.c.setText(a((int) R.string.pull_down_refresh));
                Log.v("listview", "当前状态，下拉刷新");
                return;
            case 2:
                this.b.setPadding(0, 0, 0, 0);
                this.f.setVisibility(0);
                this.e.clearAnimation();
                this.e.setVisibility(8);
                this.c.setText(a((int) R.string.bein_refresh));
                this.d.setVisibility(0);
                Log.v("listview", "当前状态,正在刷新...");
                return;
            case 3:
                this.b.setPadding(0, this.k * -1, 0, 0);
                this.f.setVisibility(8);
                this.e.clearAnimation();
                this.e.setImageResource(R.drawable.arrow);
                this.c.setText(a((int) R.string.pull_down_refresh));
                this.d.setVisibility(0);
                Log.v("listview", "当前状态，done");
                return;
            default:
                return;
        }
    }

    public final void a() {
        this.n = 3;
        this.d.setText(a((int) R.string.last_refresh) + new Date().toLocaleString());
        b();
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.q) {
            this.m = getFirstVisiblePosition();
            switch (motionEvent.getAction()) {
                case 0:
                    if (this.m == 0 && !this.i) {
                        this.i = true;
                        this.l = (int) motionEvent.getY();
                        Log.v("listview", "在down时候记录当前位置‘");
                        break;
                    }
                case 1:
                    if (!(this.n == 2 || this.n == 4)) {
                        int i2 = this.n;
                        if (this.n == 1) {
                            this.n = 3;
                            b();
                            Log.v("listview", "由下拉刷新状态，到done状态");
                        }
                        if (this.n == 0) {
                            this.n = 2;
                            b();
                            if (this.p != null) {
                                this.p.a();
                            }
                            Log.v("listview", "由松开刷新状态，到done状态");
                        }
                    }
                    this.i = false;
                    this.o = false;
                    break;
                case 2:
                    int y = (int) motionEvent.getY();
                    if (!this.i && this.m == 0) {
                        Log.v("listview", "在move时候记录下位置");
                        this.i = true;
                        this.l = y;
                    }
                    if (this.n != 2 && this.i && this.n != 4 && getFirstVisiblePosition() == 0) {
                        if (this.n == 0) {
                            setSelection(0);
                            if ((y - this.l) / 3 < this.k && y - this.l > 0) {
                                this.n = 1;
                                b();
                                Log.v("listview", "由松开刷新状态转变到下拉刷新状态");
                            } else if (y - this.l <= 0) {
                                this.n = 3;
                                b();
                                Log.v("listview", "由松开刷新状态转变到done状态");
                            }
                        }
                        if (this.n == 1) {
                            if ((y - this.l) / 3 >= this.k) {
                                this.n = 0;
                                this.o = true;
                                b();
                                Log.v("listview", "由done或者下拉刷新状态转变到松开刷新");
                            } else if (y - this.l <= 0) {
                                this.n = 3;
                                b();
                                Log.v("listview", "由DOne或者下拉刷新状态转变到done状态");
                            }
                        }
                        if (this.n == 3 && y - this.l > 0) {
                            this.n = 1;
                            b();
                        }
                        if (this.n == 1) {
                            this.b.setPadding(0, (this.k * -1) + ((y - this.l) / 3), 0, 0);
                        }
                        if (this.n == 0) {
                            this.b.setPadding(0, ((y - this.l) / 3) - this.k, 0, 0);
                            break;
                        }
                    }
                    break;
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setAdapter(BaseAdapter baseAdapter) {
        this.d.setText(a((int) R.string.last_refresh) + new Date().toLocaleString());
        super.setAdapter((ListAdapter) baseAdapter);
    }

    public void setonRefreshListener(g gVar) {
        this.p = gVar;
        this.q = true;
    }
}
