package com.zhongsou.flymall.ui.photoview;

import android.widget.ImageView;

final class g implements Runnable {
    final /* synthetic */ d a;
    private final float b;
    private final float c;
    private final float d;
    private final float e;

    public g(d dVar, float f, float f2, float f3, float f4) {
        this.a = dVar;
        this.d = f2;
        this.b = f3;
        this.c = f4;
        if (f < f2) {
            this.e = 1.07f;
        } else {
            this.e = 0.93f;
        }
    }

    public final void run() {
        ImageView c2 = this.a.c();
        if (c2 != null) {
            this.a.l.postScale(this.e, this.e, this.b, this.c);
            this.a.k();
            float g = this.a.g();
            if ((this.e <= 1.0f || g >= this.d) && (this.e >= 1.0f || this.d >= g)) {
                float f = this.d / g;
                this.a.l.postScale(f, f, this.b, this.c);
                this.a.k();
                return;
            }
            a.a(c2, this);
        }
    }
}
