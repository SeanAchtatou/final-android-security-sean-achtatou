package com.zhongsou.flymall.ui.photoview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.unionpay.upomp.lthj.plugin.ui.R;

public class PhotoViewWraper extends RelativeLayout {
    protected View a;
    protected PhotoView b;
    protected Context c;

    public PhotoViewWraper(Context context) {
        super(context);
        this.c = context;
        a();
    }

    public PhotoViewWraper(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
        a();
    }

    private void a() {
        this.b = new PhotoView(this.c);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        this.b.setLayoutParams(layoutParams);
        addView(this.b);
        this.b.setVisibility(8);
        this.a = LayoutInflater.from(this.c).inflate((int) R.layout.image_zoom_dialog, (ViewGroup) null);
        this.a.setLayoutParams(layoutParams);
        addView(this.a);
    }

    public PhotoView getImageView() {
        return this.b;
    }

    public void setUrl(String str) {
        new l(this).execute(str);
    }
}
