package com.zhongsou.flymall.ui.photoview;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

final class e extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ d a;

    e(d dVar) {
        this.a = dVar;
    }

    public final void onLongPress(MotionEvent motionEvent) {
        if (this.a.r != null) {
            this.a.r.onLongClick((View) this.a.f.get());
        }
    }
}
