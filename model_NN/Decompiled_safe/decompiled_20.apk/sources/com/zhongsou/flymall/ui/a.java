package com.zhongsou.flymall.ui;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;

public final class a {
    View a;
    TextView b;
    Button c;
    ProgressBar d;
    f e;
    /* access modifiers changed from: private */
    public final Activity f;

    public a(Activity activity) {
        this.f = activity;
        if (activity != null) {
            this.a = activity.findViewById(R.id.ll_data_loading);
            if (this.a != null) {
                this.b = (TextView) this.a.findViewById(R.id.loading_tip_txt);
                this.d = (ProgressBar) this.a.findViewById(R.id.loading_progress_bar);
                this.c = (Button) this.a.findViewById(R.id.again_load);
            }
        }
    }

    public final void a() {
        this.f.runOnUiThread(new b(this));
    }

    public final void a(f fVar) {
        this.e = fVar;
    }

    public final void b() {
        this.f.runOnUiThread(new d(this));
    }

    public final void c() {
        this.f.runOnUiThread(new e(this));
    }
}
