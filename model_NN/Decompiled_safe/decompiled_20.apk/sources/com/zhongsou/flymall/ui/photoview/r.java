package com.zhongsou.flymall.ui.photoview;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

@TargetApi(8)
final class r extends q {
    private final ScaleGestureDetector f;
    private final ScaleGestureDetector.OnScaleGestureListener g = new s(this);

    public r(Context context) {
        super(context);
        this.f = new ScaleGestureDetector(context, this.g);
    }

    public final boolean a() {
        return this.f.isInProgress();
    }

    public final boolean a(MotionEvent motionEvent) {
        this.f.onTouchEvent(motionEvent);
        return super.a(motionEvent);
    }
}
