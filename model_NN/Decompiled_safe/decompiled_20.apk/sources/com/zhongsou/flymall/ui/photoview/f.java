package com.zhongsou.flymall.ui.photoview;

import android.widget.ImageView;

final /* synthetic */ class f {
    static final /* synthetic */ int[] a = new int[ImageView.ScaleType.values().length];

    static {
        try {
            a[ImageView.ScaleType.MATRIX.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            a[ImageView.ScaleType.FIT_START.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            a[ImageView.ScaleType.FIT_END.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            a[ImageView.ScaleType.FIT_XY.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
