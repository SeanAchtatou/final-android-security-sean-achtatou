package com.zhongsou.flymall.ui.photoview;

import android.content.Context;
import android.widget.Scroller;

final class n extends m {
    private Scroller a;

    public n(Context context) {
        this.a = new Scroller(context);
    }

    public final void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.a.fling(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public final boolean a() {
        return this.a.computeScrollOffset();
    }

    public final void b() {
        this.a.forceFinished(true);
    }

    public final int c() {
        return this.a.getCurrX();
    }

    public final int d() {
        return this.a.getCurrY();
    }
}
