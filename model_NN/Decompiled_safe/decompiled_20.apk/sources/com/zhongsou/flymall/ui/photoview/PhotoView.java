package com.zhongsou.flymall.ui.photoview;

import android.content.Context;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class PhotoView extends ImageView {
    private final d a;
    private ImageView.ScaleType b;

    public PhotoView(Context context) {
        this(context, null);
    }

    public PhotoView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PhotoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        super.setScaleType(ImageView.ScaleType.MATRIX);
        this.a = new d(this);
        if (this.b != null) {
            setScaleType(this.b);
            this.b = null;
        }
    }

    public RectF getDisplayRect() {
        return this.a.b();
    }

    public float getMaxScale() {
        return this.a.f();
    }

    public float getMidScale() {
        return this.a.e();
    }

    public float getMinScale() {
        return this.a.d();
    }

    public float getScale() {
        return this.a.g();
    }

    public ImageView.ScaleType getScaleType() {
        return this.a.h();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.a.a();
        super.onDetachedFromWindow();
    }

    public void setAllowParentInterceptOnEdge(boolean z) {
        this.a.a(z);
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (this.a != null) {
            this.a.i();
        }
    }

    public void setImageResource(int i) {
        super.setImageResource(i);
        if (this.a != null) {
            this.a.i();
        }
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        if (this.a != null) {
            this.a.i();
        }
    }

    public void setMaxScale(float f) {
        this.a.c(f);
    }

    public void setMidScale(float f) {
        this.a.b(f);
    }

    public void setMinScale(float f) {
        this.a.a(f);
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.a.a(onLongClickListener);
    }

    public void setOnMatrixChangeListener(i iVar) {
        this.a.a(iVar);
    }

    public void setOnPhotoTapListener(j jVar) {
        this.a.a(jVar);
    }

    public void setOnViewTapListener(k kVar) {
        this.a.a(kVar);
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        if (this.a != null) {
            this.a.a(scaleType);
        } else {
            this.b = scaleType;
        }
    }

    public void setZoomable(boolean z) {
        this.a.b(z);
    }
}
