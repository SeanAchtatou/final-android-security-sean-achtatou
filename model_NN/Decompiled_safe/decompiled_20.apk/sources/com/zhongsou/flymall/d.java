package com.zhongsou.flymall;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.widget.Toast;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.manager.a;
import java.lang.Thread;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.UnknownHostException;
import org.apache.commons.httpclient.HttpException;

public final class d extends Exception implements Thread.UncaughtExceptionHandler {
    private byte a;
    private int b;
    private Thread.UncaughtExceptionHandler c;

    private d() {
        this.c = Thread.getDefaultUncaughtExceptionHandler();
    }

    private d(byte b2, int i, Exception exc) {
        super(exc);
        this.a = b2;
        this.b = i;
    }

    public static d a() {
        return new d();
    }

    public static d a(int i) {
        return new d((byte) 3, i, null);
    }

    public static d a(Exception exc) {
        return new d((byte) 4, 0, exc);
    }

    public static d b(Exception exc) {
        return ((exc instanceof UnknownHostException) || (exc instanceof ConnectException)) ? new d((byte) 1, 0, exc) : exc instanceof HttpException ? a(exc) : exc instanceof SocketException ? new d((byte) 2, 0, exc) : a(exc);
    }

    public final void a(Context context) {
        switch (this.a) {
            case 1:
                Toast.makeText(context, (int) R.string.network_not_connected, 0).show();
                return;
            case 2:
                Toast.makeText(context, (int) R.string.socket_exception_error, 0).show();
                return;
            case 3:
                Toast.makeText(context, context.getString(R.string.http_status_code_error, Integer.valueOf(this.b)), 0).show();
                return;
            case 4:
                Toast.makeText(context, (int) R.string.http_exception_error, 0).show();
                return;
            case 5:
                Toast.makeText(context, (int) R.string.xml_parser_failed, 0).show();
                return;
            case 6:
                Toast.makeText(context, (int) R.string.io_exception_error, 0).show();
                return;
            case MotionEventCompat.ACTION_HOVER_MOVE:
                Toast.makeText(context, (int) R.string.app_run_code_error, 0).show();
                return;
            default:
                return;
        }
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        boolean z = false;
        if (th != null) {
            a.a();
            Activity b2 = a.b();
            if (b2 != null) {
                PackageInfo c2 = ((AppContext) b2.getApplicationContext()).c();
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Version: " + c2.versionName + "(" + c2.versionCode + ")\n");
                stringBuffer.append("Android: " + Build.VERSION.RELEASE + "(" + Build.MODEL + ")\n");
                stringBuffer.append("Exception: " + th.getMessage() + "\n");
                StackTraceElement[] stackTrace = th.getStackTrace();
                for (int i = 0; i < stackTrace.length; i++) {
                    stringBuffer.append(stackTrace[i].toString() + "\n");
                }
                new e(this, b2, stringBuffer.toString()).start();
                z = true;
            }
        }
        if (!z && this.c != null) {
            this.c.uncaughtException(thread, th);
        }
    }
}
