package com.zhongsou.flymall;

import android.content.Context;
import android.os.Looper;
import com.zhongsou.flymall.c.b;

final class e extends Thread {
    final /* synthetic */ Context a;
    final /* synthetic */ String b;
    final /* synthetic */ d c;

    e(d dVar, Context context, String str) {
        this.c = dVar;
        this.a = context;
        this.b = str;
    }

    public final void run() {
        Looper.prepare();
        b.c(this.a, this.b);
        Looper.loop();
    }
}
