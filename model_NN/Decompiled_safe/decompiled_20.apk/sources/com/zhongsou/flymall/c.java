package com.zhongsou.flymall;

import com.zhongsou.flymall.d.ac;
import java.util.Properties;

final class c extends Properties {
    final /* synthetic */ ac a;
    final /* synthetic */ AppContext b;

    c(AppContext appContext, ac acVar) {
        this.b = appContext;
        this.a = acVar;
        setProperty("user.uid", String.valueOf(this.a.getUid()));
        setProperty("user.username", this.a.getUsername());
        setProperty("user.name", this.a.getUname());
        setProperty("user.session", this.a.getSession());
        setProperty("user.isRememberMe", String.valueOf(this.a.a()));
        setProperty("user.level", this.a.getLevel());
        setProperty("user.point", this.a.getPoint());
    }
}
