package com.zhongsou.flymall.activity;

import android.content.DialogInterface;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.aa;
import com.zhongsou.flymall.d.s;
import com.zhongsou.flymall.g.g;
import java.util.List;

final class ci implements DialogInterface.OnClickListener {
    final /* synthetic */ List a;
    final /* synthetic */ CheckActivity b;

    ci(CheckActivity checkActivity, List list) {
        this.b = checkActivity;
        this.a = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        s sVar = (s) this.a.get(i);
        ((TextView) this.b.g.findViewById(R.id.check_order_pay_content)).setText(this.b.b(sVar.getPlatform()));
        this.b.o.setPayid(String.valueOf(sVar.getPay_id()));
        this.b.o.setPayt(String.valueOf(sVar.getPay_type()));
        String shipt = this.b.o.getShipt();
        long longValue = g.a(shipt) ? 0 : Long.valueOf(shipt).longValue();
        if (sVar.getPay_type() == 1 && longValue != 4) {
            this.b.a(this.b.u);
        } else if (sVar.getPay_type() != 1 && longValue == 4) {
            this.b.a((aa) this.b.t.get(0));
        }
        dialogInterface.dismiss();
    }
}
