package com.zhongsou.flymall.activity;

import android.content.DialogInterface;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.zhongsou.flymall.d.b;

final class i implements DialogInterface.OnClickListener {
    final /* synthetic */ h a;

    i(h hVar) {
        this.a = hVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        b bVar = (b) this.a.a.k.get(i);
        long area_id = bVar.getArea_id();
        if (area_id != this.a.a.n) {
            long unused = this.a.a.o = 0;
            this.a.a.i.setText(PoiTypeDef.All);
            this.a.a.j.setText(PoiTypeDef.All);
        }
        long unused2 = this.a.a.n = area_id;
        this.a.a.h.setText(bVar.getProvince());
        dialogInterface.dismiss();
    }
}
