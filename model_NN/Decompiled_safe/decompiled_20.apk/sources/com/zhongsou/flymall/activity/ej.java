package com.zhongsou.flymall.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import java.util.ArrayList;

final class ej implements AdapterView.OnItemClickListener {
    final /* synthetic */ ProductActivity a;

    ej(ProductActivity productActivity) {
        this.a = productActivity;
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.a, ProductImgsActivity.class);
        intent.putStringArrayListExtra("imgs", (ArrayList) this.a.E.getImgs());
        intent.putExtra("position", i);
        this.a.startActivity(intent);
    }
}
