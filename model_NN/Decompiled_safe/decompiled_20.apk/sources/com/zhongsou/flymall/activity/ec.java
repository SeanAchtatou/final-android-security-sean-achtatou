package com.zhongsou.flymall.activity;

import android.os.Handler;
import android.os.Message;
import com.amap.api.search.route.Route;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.a;

final class ec extends Handler {
    final /* synthetic */ MenDianMapActivity a;

    ec(MenDianMapActivity menDianMapActivity) {
        this.a = menDianMapActivity;
    }

    public final void handleMessage(Message message) {
        if (message.what == 2002) {
            if (this.a.r != null && this.a.r.size() > 0) {
                Route unused = this.a.p = (Route) this.a.r.get(0);
                if (this.a.p != null) {
                    a unused2 = this.a.o = new a(this.a, this.a.j, this.a.p);
                    this.a.o.b();
                    this.a.o.a();
                    this.a.d.setVisibility(0);
                    this.a.e.setBackgroundResource(R.drawable.map_prev_disable);
                    this.a.f.setBackgroundResource(R.drawable.btn_route_next);
                }
            }
        } else if (message.what == 2004) {
            this.a.a((String) message.obj);
        }
    }
}
