package com.zhongsou.flymall.activity;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.g.g;

final class f implements View.OnClickListener {
    final /* synthetic */ AddressAddActivity a;

    f(AddressAddActivity addressAddActivity) {
        this.a = addressAddActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.AddressAddActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void onClick(View view) {
        EditText editText = (EditText) this.a.findViewById(R.id.address_add_name);
        EditText editText2 = (EditText) this.a.findViewById(R.id.address_add_mobile);
        EditText editText3 = (EditText) this.a.findViewById(R.id.address_add_code);
        EditText editText4 = (EditText) this.a.findViewById(R.id.address_add_province);
        EditText editText5 = (EditText) this.a.findViewById(R.id.address_add_city);
        EditText editText6 = (EditText) this.a.findViewById(R.id.address_add_area);
        EditText editText7 = (EditText) this.a.findViewById(R.id.address_add_address);
        this.a.g.setName(editText.getText().toString());
        this.a.g.setMobile(editText2.getText().toString());
        this.a.g.setCode(editText3.getText().toString());
        this.a.g.setProvince(editText4.getText().toString());
        this.a.g.setCity(editText5.getText().toString());
        this.a.g.setArea(editText6.getText().toString());
        this.a.g.setAddress(editText7.getText().toString());
        if (g.a(editText.getText().toString())) {
            b.a((Context) this.a, "请填写收货人姓名！");
            AddressAddActivity.b(this.a);
        } else if (!b.a("mobile", editText2.getText().toString())) {
            b.a((Context) this.a, "请填写手机号码！");
            AddressAddActivity.b(this.a);
        } else if (!b.a("zipcode", editText3.getText().toString())) {
            b.a((Context) this.a, "请填写邮政编码！");
            AddressAddActivity.b(this.a);
        } else if (g.a(editText4.getText().toString())) {
            b.a((Context) this.a, "请选择所在省份！");
            AddressAddActivity.b(this.a);
        } else if (g.a(editText5.getText().toString())) {
            b.a((Context) this.a, "请选择所在城市！");
        } else if (g.a(editText6.getText().toString())) {
            b.a((Context) this.a, "请选择所在地区！");
            AddressAddActivity.b(this.a);
        } else if (g.a(editText7.getText().toString())) {
            b.a((Context) this.a, "请填写详细地址！");
            AddressAddActivity.b(this.a);
        } else {
            AddressAddActivity.a(this.a, this.a.g);
        }
    }
}
