package com.zhongsou.flymall.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.a.j;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.d;
import com.zhongsou.flymall.d.e;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SearchAttrsActivity extends BaseActivity implements o {
    protected String a = "SearchAttrsActivity";
    private f b;
    /* access modifiers changed from: private */
    public Long c = 0L;
    /* access modifiers changed from: private */
    public List<d> d = new ArrayList();
    /* access modifiers changed from: private */
    public j e;
    /* access modifiers changed from: private */
    public ExpandableListView f;
    /* access modifiers changed from: private */
    public int g = -1;
    /* access modifiers changed from: private */
    public int h = -1;
    private Map<String, List<String>> i = null;
    private List<d> j;

    static /* synthetic */ void a(SearchAttrsActivity searchAttrsActivity, int i2, int i3) {
        RadioButton radioButton;
        int firstVisiblePosition = searchAttrsActivity.f.getFirstVisiblePosition();
        int lastVisiblePosition = searchAttrsActivity.f.getLastVisiblePosition();
        int i4 = i2 + i3 + 1;
        List<Map<String, e>> a2 = searchAttrsActivity.e.a();
        for (int i5 = 0; i5 <= lastVisiblePosition - firstVisiblePosition; i5++) {
            View childAt = searchAttrsActivity.f.getChildAt(i5);
            if (!(childAt == null || childAt.getTag() == null || ((radioButton = (RadioButton) childAt.findViewById(R.id.itemattrs_id)) == null && !(radioButton instanceof RadioButton)))) {
                Map map = (Map) radioButton.getTag();
                if (i5 == i4) {
                    radioButton.setChecked(true);
                    List<Map<String, e>> arrayList = a2 == null ? new ArrayList<>() : a2;
                    if (!arrayList.contains(map)) {
                        arrayList.add(map);
                        a2 = arrayList;
                    } else {
                        a2 = arrayList;
                    }
                } else {
                    radioButton.setChecked(false);
                    if (a2 != null && a2.contains(map)) {
                        a2.remove(map);
                    }
                }
            }
        }
        searchAttrsActivity.e.a(a2);
    }

    private void a(List<d> list) {
        if (this.i != null && this.i.size() != 0) {
            List<Map<String, e>> a2 = this.e.a();
            for (d next : list) {
                for (e next2 : next.getCont()) {
                    String type = next.getType();
                    if (this.i.containsKey(type)) {
                        Iterator it = this.i.get(type).iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (((String) it.next()).equals(next2.getId())) {
                                    HashMap hashMap = new HashMap();
                                    hashMap.put(type, next2);
                                    a2.add(hashMap);
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            this.e.a(a2);
        }
    }

    public final void a(String str) {
        Log.i("ProductAttrsActivity", "onHttpError:::::methodName:::::" + str);
    }

    public void loadLvAttrsDataSuccess(List<d> list) {
        Log.i("ProductAttrsActivity", "loadLvAttrsDataSuccess");
        if (list == null || list.size() == 0) {
            b.b(this, getResources().getString(R.string.more_search_attrs_screen) + getResources().getString(R.string.load_empty));
            b.a(this, this.c, this.j);
            return;
        }
        a(list);
        this.d.addAll(list);
        this.e.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.product_attrs);
        Intent intent = getIntent();
        this.c = Long.valueOf(intent.getLongExtra("cid", -1));
        this.i = (Map) intent.getSerializableExtra("attrsMap");
        this.j = (List) intent.getSerializableExtra("attrsList");
        this.c.longValue();
        Log.i("ProductAttrsActivity", "initHeadView");
        Button button = (Button) findViewById(R.id.head_back_btn);
        Button button2 = (Button) findViewById(R.id.head_right_btn);
        button.setVisibility(0);
        button.setText((int) R.string.more_search_attrs_clear);
        button2.setVisibility(0);
        button2.setText((int) R.string.more_search_attrs_screen);
        button.setOnClickListener(new fz(this));
        button2.setOnClickListener(new ga(this));
        Log.i("ProductAttrsActivity", "initAttrsListView");
        this.f = (ExpandableListView) findViewById(R.id.frame_expandable_listview_attrs);
        this.e = new j(this, this.d);
        this.f.setAdapter(this.e);
        this.f.setOnGroupExpandListener(new gb(this));
        this.f.setOnChildClickListener(new gc(this));
        Log.i("ProductAttrsActivity", "initAttrsListViewData");
        Log.i("ProductAttrsActivity", "loadLvAttrsData.cid:::::" + this.c);
        this.b = new f(this);
        this.b.a();
        this.b.a(this.c);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return false;
        }
        b.a(this, this.c, this.j);
        return false;
    }
}
