package com.zhongsou.flymall.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.a.aq;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.g.g;
import java.util.LinkedList;

public class BetaSearchActivity extends BaseActivity implements View.OnClickListener, View.OnKeyListener, AdapterView.OnItemClickListener {
    private ListView a;
    private LinkedList<String> b = new LinkedList<>();
    private aq c;
    private EditText d;
    private SharedPreferences e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public ImageView g;

    private boolean a() {
        String trim = this.d.getText().toString().trim();
        if (g.b((Object) trim)) {
            com.zhongsou.flymall.b.g.a(trim);
            this.b = new LinkedList<>(com.zhongsou.flymall.b.g.a());
            this.c.a(this.b);
            this.c.notifyDataSetChanged();
            b.a(this, 0, trim);
            return true;
        }
        this.d.requestFocus();
        this.d.setText(PoiTypeDef.All);
        return true;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_clearwords:
                if (g.b(this.d.getText())) {
                    this.d.setText(PoiTypeDef.All);
                    this.e = getSharedPreferences("flymall", 0);
                    SharedPreferences.Editor edit = this.e.edit();
                    edit.putString("last_search_keyword", g.d(PoiTypeDef.All));
                    edit.commit();
                    return;
                }
                return;
            case R.id.search_cancel:
                if (R.string.search_search == ((Integer) view.getTag()).intValue()) {
                    a();
                    return;
                } else if (R.string.search_cancel == ((Integer) view.getTag()).intValue()) {
                    finish();
                    return;
                } else {
                    return;
                }
            case R.id.search_ll:
            default:
                return;
            case R.id.search_history:
                this.c.a(this.b);
                this.c.notifyDataSetChanged();
                this.a.setVisibility(0);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.beta_main_search);
        this.d = (EditText) findViewById(R.id.search_editer);
        this.a = (ListView) findViewById(R.id.history_search_list);
        this.c = new aq(this, this.b);
        this.a.setAdapter((ListAdapter) this.c);
        this.a.setOnItemClickListener(this);
        this.g = (ImageView) findViewById(R.id.search_clearwords);
        this.g.setOnClickListener(this);
        this.f = (Button) findViewById(R.id.search_cancel);
        this.f.setOnClickListener(this);
        this.b = new LinkedList<>(com.zhongsou.flymall.b.g.a());
        this.c.a(this.b);
        this.c.notifyDataSetChanged();
        this.e = getSharedPreferences("flymall", 0);
        findViewById(R.id.search_history).setOnClickListener(this);
        findViewById(R.id.search_editer).setOnKeyListener(this);
        this.e = getSharedPreferences("flymall", 0);
        String string = this.e.getString("last_search_keyword", PoiTypeDef.All);
        if (!string.equals(PoiTypeDef.All)) {
            this.d.setText(string);
            this.d.setSelection(string.length());
        }
        this.f.setText((int) R.string.search_cancel);
        this.f.setTag(Integer.valueOf((int) R.string.search_cancel));
        this.g.setVisibility(8);
        this.d.addTextChangedListener(new bc(this));
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        String str = PoiTypeDef.All;
        if (this.b.size() > 0) {
            str = this.b.get(i);
        }
        if (g.b((Object) str)) {
            this.d.setText(str);
            com.zhongsou.flymall.b.g.a(str);
            this.b = new LinkedList<>(com.zhongsou.flymall.b.g.a());
            this.c.a(this.b);
            this.c.notifyDataSetChanged();
            b.a(this, 0, str);
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if ((i == 66 || i == 84) && keyEvent.getAction() == 0) {
            return a();
        }
        return false;
    }
}
