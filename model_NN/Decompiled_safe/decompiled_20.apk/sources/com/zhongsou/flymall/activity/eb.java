package com.zhongsou.flymall.activity;

import android.os.Message;
import com.amap.api.search.core.AMapException;
import com.amap.api.search.route.Route;
import java.util.List;

final class eb implements Runnable {
    final /* synthetic */ Route.FromAndTo a;
    final /* synthetic */ MenDianMapActivity b;

    eb(MenDianMapActivity menDianMapActivity, Route.FromAndTo fromAndTo) {
        this.b = menDianMapActivity;
        this.a = fromAndTo;
    }

    public final void run() {
        try {
            List unused = this.b.r = Route.calculateRoute(this.b, this.a, this.b.q);
            if (this.b.r != null || this.b.r.size() > 0) {
                this.b.t.sendMessage(Message.obtain(this.b.t, 2002));
            }
        } catch (AMapException e) {
            Message message = new Message();
            message.what = 2004;
            message.obj = e.getErrorMessage();
            this.b.t.sendMessage(message);
        }
    }
}
