package com.zhongsou.flymall.activity;

import android.widget.AbsListView;

final class ei implements AbsListView.OnScrollListener {
    final /* synthetic */ ProductActivity a;

    ei(ProductActivity productActivity) {
        this.a = productActivity;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        int unused = this.a.R = i2;
        int unused2 = this.a.Q = (i + i2) - 1;
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        int count = (this.a.O.getCount() - 1) + 1;
        if (i == 0 && this.a.Q == count) {
            this.a.a();
        }
    }
}
