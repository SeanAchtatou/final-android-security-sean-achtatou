package com.zhongsou.flymall.activity;

import android.widget.ExpandableListView;

final class gb implements ExpandableListView.OnGroupExpandListener {
    final /* synthetic */ SearchAttrsActivity a;

    gb(SearchAttrsActivity searchAttrsActivity) {
        this.a = searchAttrsActivity;
    }

    public final void onGroupExpand(int i) {
        for (int i2 = 0; i2 < this.a.e.getGroupCount(); i2++) {
            if (i != i2) {
                this.a.f.collapseGroup(i2);
            }
        }
    }
}
