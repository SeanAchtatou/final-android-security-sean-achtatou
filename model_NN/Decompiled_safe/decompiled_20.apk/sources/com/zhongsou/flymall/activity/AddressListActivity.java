package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.h;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.a;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.g;
import java.util.ArrayList;
import java.util.List;

public class AddressListActivity extends BaseActivity implements o {
    private AppContext a;
    private f b;
    private ProgressDialog c;
    private h d;
    private ListView e;
    private LinearLayout f;
    /* access modifiers changed from: private */
    public List<a> g;
    /* access modifiers changed from: private */
    public long h;
    private long i;
    /* access modifiers changed from: private */
    public a j;
    /* access modifiers changed from: private */
    public int k;

    private void a(long j2) {
        this.c = ProgressDialog.show(this, null, "正在加载数据，请稍后...");
        this.g.clear();
        this.b = b();
        this.b.c(Long.valueOf(j2));
    }

    static /* synthetic */ void b(AddressListActivity addressListActivity, long j2) {
        addressListActivity.c = ProgressDialog.show(addressListActivity, null, "正在删除数据，请稍后...");
        addressListActivity.b = addressListActivity.b();
        addressListActivity.b.d(Long.valueOf(j2));
    }

    static /* synthetic */ void b(AddressListActivity addressListActivity, a aVar) {
        Intent intent = new Intent();
        intent.putExtra("address", aVar);
        intent.setClass(addressListActivity, AddressEditActivity.class);
        addressListActivity.startActivity(intent);
    }

    static /* synthetic */ void c(AddressListActivity addressListActivity, long j2) {
        addressListActivity.c = ProgressDialog.show(addressListActivity, null, "正在设置地址，请稍后...");
        addressListActivity.b = addressListActivity.b();
        addressListActivity.b.e(Long.valueOf(j2));
    }

    static /* synthetic */ void e(AddressListActivity addressListActivity) {
        Intent intent = new Intent();
        intent.setClass(addressListActivity, AddressAddActivity.class);
        addressListActivity.startActivity(intent);
    }

    public final void a(String str) {
        if (this.c != null && this.c.isShowing()) {
            this.c.dismiss();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.AddressListActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public void delAddressSuccess(String str) {
        String str2;
        if (this.c != null && this.c.isShowing()) {
            this.c.dismiss();
        }
        if (!g.b((Object) str) || Integer.valueOf(str).intValue() <= 0) {
            str2 = "操作失败！";
        } else {
            str2 = "操作成功！";
            a(this.i);
        }
        b.a((Context) this, str2);
    }

    public void getAddressDataSuccess(List<a> list) {
        if (this.c != null && this.c.isShowing()) {
            this.c.dismiss();
        }
        if (this.d.getCount() != 0) {
            this.d.a();
        }
        if (list.size() > 0) {
            this.e.setVisibility(0);
            this.f.setVisibility(8);
            this.g = list;
            for (a a2 : list) {
                this.d.a(a2);
            }
            this.d.notifyDataSetChanged();
            return;
        }
        this.e.setVisibility(8);
        this.f.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.AddressListActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Log.i("AddressList", "onCreate");
        super.onCreate(bundle);
        setContentView((int) R.layout.address_list);
        this.a = (AppContext) getApplication();
        if (!this.a.b()) {
            b.a((Context) this, (int) R.string.network_not_connected);
        }
        this.k = getIntent().getIntExtra("type", 0);
        this.i = this.a.e();
        Button button = (Button) findViewById(R.id.address_add_btn);
        Button button2 = (Button) findViewById(R.id.head_back_btn);
        button.setVisibility(0);
        button2.setVisibility(0);
        ((TextView) findViewById(R.id.main_head_title)).setText((int) R.string.address_list_title);
        button.setOnClickListener(new aa(this));
        button2.setOnClickListener(new ab(this));
        this.e = (ListView) findViewById(R.id.address_listView);
        this.f = (LinearLayout) findViewById(R.id.address_empty_list);
        this.g = new ArrayList();
        this.d = new h(this, this.g);
        this.e.setAdapter((ListAdapter) this.d);
        this.e.setOnItemClickListener(new x(this));
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return false;
        }
        setResult(-1, getIntent());
        finish();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        a(this.i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.AddressListActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public void setDefaultAddressSuccess(String str) {
        String str2;
        if (this.c != null && this.c.isShowing()) {
            this.c.dismiss();
        }
        if (!g.b((Object) str) || Integer.valueOf(str).intValue() <= 0) {
            str2 = "操作失败！";
        } else {
            str2 = "操作成功！";
            a(this.i);
        }
        b.a((Context) this, str2);
    }
}
