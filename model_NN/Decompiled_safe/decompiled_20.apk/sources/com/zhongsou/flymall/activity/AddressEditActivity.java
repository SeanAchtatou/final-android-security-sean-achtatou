package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.b.e;
import com.zhongsou.flymall.d.a;
import com.zhongsou.flymall.d.b;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.g;
import java.util.ArrayList;
import java.util.List;

public class AddressEditActivity extends BaseActivity implements o {
    private AppContext a;
    /* access modifiers changed from: private */
    public ProgressDialog b;
    private f c;
    /* access modifiers changed from: private */
    public a d;
    /* access modifiers changed from: private */
    public EditText e;
    /* access modifiers changed from: private */
    public EditText f;
    /* access modifiers changed from: private */
    public EditText g;
    /* access modifiers changed from: private */
    public EditText h;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public EditText j;
    /* access modifiers changed from: private */
    public EditText k;
    /* access modifiers changed from: private */
    public List<b> l = new ArrayList();
    /* access modifiers changed from: private */
    public List<b> m = new ArrayList();
    /* access modifiers changed from: private */
    public List<b> n = new ArrayList();
    /* access modifiers changed from: private */
    public long o = 0;
    /* access modifiers changed from: private */
    public long p = 0;
    private String[] q = {"北京", "天津", "上海", "重庆"};
    private Handler r = new w(this);

    private void a() {
        this.r.sendEmptyMessage(0);
    }

    static /* synthetic */ void a(AddressEditActivity addressEditActivity, a aVar) {
        if (addressEditActivity.b == null) {
            addressEditActivity.b = new ProgressDialog(addressEditActivity);
            addressEditActivity.b.setIndeterminate(true);
            addressEditActivity.b.setMessage("正在保存数据，请稍后...");
            addressEditActivity.b.setCancelable(true);
            addressEditActivity.b.setCanceledOnTouchOutside(false);
            addressEditActivity.b.show();
        } else {
            addressEditActivity.b.setMessage("正在保存数据，请稍后...");
            addressEditActivity.b.show();
        }
        addressEditActivity.c = addressEditActivity.b();
        addressEditActivity.c.e(com.b.a.a.a(com.b.a.a.a(aVar)));
    }

    /* access modifiers changed from: private */
    public boolean c(String str) {
        for (String equals : this.q) {
            if (str.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static String d(String str) {
        if ("北京".equals(str)) {
            return "北京市";
        }
        if ("天津".equals(str)) {
            return "天津市";
        }
        if ("上海".equals(str)) {
            return "上海市";
        }
        if ("重庆".equals(str)) {
            return "重庆市";
        }
        return null;
    }

    public final void a(String str) {
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.AddressEditActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.address_add);
        this.a = (AppContext) getApplication();
        if (!this.a.b()) {
            com.zhongsou.flymall.c.b.a((Context) this, (int) R.string.network_not_connected);
        }
        this.d = (a) getIntent().getSerializableExtra("address");
        if (c(this.d.getProvince())) {
            this.d.setArea(this.d.getCity());
            this.d.setCity(d(this.d.getProvince()));
        }
        Button button = (Button) findViewById(R.id.address_save_btn);
        Button button2 = (Button) findViewById(R.id.head_back_btn);
        button.setVisibility(0);
        button2.setVisibility(0);
        ((TextView) findViewById(R.id.main_head_title)).setText((int) R.string.address_edit_title);
        button.setOnClickListener(new o(this));
        button2.setOnClickListener(new p(this));
        this.e = (EditText) findViewById(R.id.address_add_name);
        this.f = (EditText) findViewById(R.id.address_add_mobile);
        this.g = (EditText) findViewById(R.id.address_add_code);
        this.h = (EditText) findViewById(R.id.address_add_province);
        this.i = (EditText) findViewById(R.id.address_add_city);
        this.j = (EditText) findViewById(R.id.address_add_area);
        this.k = (EditText) findViewById(R.id.address_add_address);
        this.e.setText(this.d.getName());
        this.f.setText(this.d.getMobile());
        this.g.setText(this.d.getCode());
        this.h.setText(this.d.getProvince());
        this.i.setText(this.d.getCity());
        this.j.setText(this.d.getArea());
        this.k.setText(this.d.getAddress());
        this.h = (EditText) findViewById(R.id.address_add_province);
        this.i = (EditText) findViewById(R.id.address_add_city);
        this.j = (EditText) findViewById(R.id.address_add_area);
        this.h.setOnClickListener(new q(this));
        this.i.setOnClickListener(new s(this));
        this.j.setOnClickListener(new u(this));
        String area = this.d.getArea();
        com.zhongsou.flymall.b.a aVar = new com.zhongsou.flymall.b.a();
        aVar.a();
        List<b> a2 = aVar.a(e.e, "area = ?", area);
        aVar.close();
        b bVar = (a2 == null || a2.size() <= 0) ? null : a2.get(0);
        if (bVar != null) {
            this.o = bVar.getProvince_id();
            this.p = bVar.getCity_id();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.AddressEditActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public void updateAddressSuccess(String str) {
        a();
        com.zhongsou.flymall.c.b.a((Context) this, (!g.b(str) || Integer.valueOf(str).intValue() <= 0) ? "操作失败！" : "操作成功！");
        finish();
    }
}
