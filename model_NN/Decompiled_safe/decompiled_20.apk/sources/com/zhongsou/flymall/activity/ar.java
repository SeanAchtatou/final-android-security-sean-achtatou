package com.zhongsou.flymall.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import java.util.ArrayList;

final class ar implements AdapterView.OnItemClickListener {
    final /* synthetic */ BetaProductActivity a;

    ar(BetaProductActivity betaProductActivity) {
        this.a = betaProductActivity;
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.a, ProductImgsActivity.class);
        intent.putStringArrayListExtra("imgs", (ArrayList) this.a.C.getImgs());
        intent.putExtra("position", i);
        this.a.startActivity(intent);
    }
}
