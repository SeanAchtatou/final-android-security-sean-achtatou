package com.zhongsou.flymall.activity;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.g.g;

final class du implements View.OnClickListener {
    final /* synthetic */ dp a;

    du(dp dpVar) {
        this.a = dpVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void onClick(View view) {
        this.a.q.setUsername(((EditText) this.a.d.findViewById(R.id.et_login_username)).getText().toString());
        this.a.q.setPassword(((EditText) this.a.d.findViewById(R.id.et_login_pwd)).getText().toString());
        this.a.q.setAutoLogin(((CheckBox) this.a.d.findViewById(R.id.et_login_auto)).isChecked());
        if (g.a(this.a.q.getUsername()) && g.a(this.a.q.getPassword())) {
            this.a.d();
            b.a((Context) this.a.d, this.a.d.getResources().getString(R.string.loginActivity_input_name_pwd));
        } else if (g.a(this.a.q.getUsername())) {
            this.a.d();
            b.a((Context) this.a.d, this.a.d.getResources().getString(R.string.loginActivity_input_name));
        } else if (g.a(this.a.q.getPassword())) {
            this.a.d();
            b.a((Context) this.a.d, this.a.d.getResources().getString(R.string.loginActivity_input_pwd));
        } else {
            this.a.e();
        }
    }
}
