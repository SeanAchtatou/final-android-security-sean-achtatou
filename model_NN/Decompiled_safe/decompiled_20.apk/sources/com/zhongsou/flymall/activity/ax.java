package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class ax implements View.OnClickListener {
    final /* synthetic */ BetaProductListActivity a;

    ax(BetaProductListActivity betaProductListActivity) {
        this.a = betaProductListActivity;
    }

    public final void onClick(View view) {
        ProgressDialog unused = this.a.u = ProgressDialog.show(this.a, null, this.a.getString(R.string.product_loading));
        String unused2 = this.a.f = "sellNum desc";
        this.a.a();
        this.a.n.setChecked(false);
        BetaProductListActivity.b(this.a, this.a.f);
        this.a.c();
    }
}
