package com.zhongsou.flymall.activity;

import android.app.AlertDialog;
import android.view.View;
import com.zhongsou.flymall.b.b;
import java.util.List;

final class q implements View.OnClickListener {
    final /* synthetic */ AddressEditActivity a;

    q(AddressEditActivity addressEditActivity) {
        this.a = addressEditActivity;
    }

    public final void onClick(View view) {
        List unused = this.a.l = b.a();
        int size = this.a.l.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            strArr[i] = ((com.zhongsou.flymall.d.b) this.a.l.get(i)).getProvince();
        }
        new AlertDialog.Builder(this.a).setTitle("请选择").setItems(strArr, new r(this)).show();
    }
}
