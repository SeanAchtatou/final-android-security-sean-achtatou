package com.zhongsou.flymall.activity;

import android.view.View;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.d;
import com.zhongsou.flymall.d.e;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class ga implements View.OnClickListener {
    final /* synthetic */ SearchAttrsActivity a;

    ga(SearchAttrsActivity searchAttrsActivity) {
        this.a = searchAttrsActivity;
    }

    public final void onClick(View view) {
        boolean z;
        List<Map<String, e>> a2 = this.a.e.a();
        ArrayList arrayList = new ArrayList();
        if (a2.size() != 0) {
            for (Map next : a2) {
                for (String str : next.keySet()) {
                    e eVar = (e) next.get(str);
                    Iterator it = arrayList.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            z = false;
                            break;
                        }
                        d dVar = (d) it.next();
                        if (dVar.getType() == str) {
                            dVar.getCont().add(eVar);
                            z = true;
                            break;
                        }
                    }
                    if (!z) {
                        d dVar2 = new d();
                        dVar2.setType(str);
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.add(eVar);
                        dVar2.setCont(arrayList2);
                        arrayList.add(dVar2);
                    }
                }
            }
        }
        b.a(this.a, this.a.c, arrayList);
    }
}
