package com.zhongsou.flymall.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class dk extends BaseAdapter {
    final /* synthetic */ InvoiceActivity a;
    private Context b;
    private String[] c;

    public dk(InvoiceActivity invoiceActivity, Context context, String[] strArr) {
        this.a = invoiceActivity;
        this.b = context;
        this.c = strArr;
    }

    public final int getCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.length;
    }

    public final Object getItem(int i) {
        return this.c[i];
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        dl dlVar;
        if (view == null) {
            dlVar = new dl();
            view = LayoutInflater.from(this.b).inflate((int) R.layout.invoice_item, (ViewGroup) null);
            dlVar.a = view.findViewById(R.id.invoice_item_list);
            view.setTag(dlVar);
        } else {
            dlVar = (dl) view.getTag();
        }
        CheckBox checkBox = (CheckBox) dlVar.a.findViewById(R.id.invoice_item_checkbox);
        checkBox.setText(this.c[i]);
        checkBox.setChecked(this.a.d == i);
        return view;
    }
}
