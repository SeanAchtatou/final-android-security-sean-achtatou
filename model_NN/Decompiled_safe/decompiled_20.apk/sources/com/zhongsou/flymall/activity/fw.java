package com.zhongsou.flymall.activity;

import android.view.MotionEvent;
import android.view.View;

final class fw implements View.OnTouchListener {
    final /* synthetic */ ProductSecKillActivity a;

    fw(ProductSecKillActivity productSecKillActivity) {
        this.a = productSecKillActivity;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a.b = (int) motionEvent.getX();
                return false;
            case 1:
                this.a.c = (int) motionEvent.getX();
                if (this.a.b >= this.a.c || this.a.e.getLastVisiblePosition() != this.a.e.getCount() - 1) {
                    return false;
                }
                this.a.a++;
                this.a.d.a();
                return false;
            default:
                return false;
        }
    }
}
