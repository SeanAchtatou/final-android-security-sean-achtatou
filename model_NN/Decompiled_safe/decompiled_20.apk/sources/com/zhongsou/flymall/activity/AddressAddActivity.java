package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.d.a;
import com.zhongsou.flymall.d.b;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.g;
import java.util.ArrayList;
import java.util.List;

public class AddressAddActivity extends BaseActivity implements o {
    private AppContext a;
    /* access modifiers changed from: private */
    public ProgressDialog b;
    private f c;
    private Button d;
    private Button e;
    private long f;
    /* access modifiers changed from: private */
    public a g;
    /* access modifiers changed from: private */
    public EditText h;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public EditText j;
    /* access modifiers changed from: private */
    public List<b> k = new ArrayList();
    /* access modifiers changed from: private */
    public List<b> l = new ArrayList();
    /* access modifiers changed from: private */
    public List<b> m = new ArrayList();
    /* access modifiers changed from: private */
    public long n = 0;
    /* access modifiers changed from: private */
    public long o = 0;
    private String[] p = {"北京", "天津", "上海", "重庆"};
    private Handler q = new n(this);

    private void a() {
        this.q.sendEmptyMessage(0);
    }

    static /* synthetic */ void a(AddressAddActivity addressAddActivity, a aVar) {
        if (addressAddActivity.b == null) {
            addressAddActivity.b = new ProgressDialog(addressAddActivity);
            addressAddActivity.b.setIndeterminate(true);
            addressAddActivity.b.setMessage("正在保存数据，请稍后...");
            addressAddActivity.b.setCancelable(true);
            addressAddActivity.b.setCanceledOnTouchOutside(false);
            addressAddActivity.b.show();
        } else {
            addressAddActivity.b.setMessage("正在保存数据，请稍后...");
            addressAddActivity.b.show();
        }
        addressAddActivity.c = addressAddActivity.b();
        f fVar = addressAddActivity.c;
        Long.valueOf(addressAddActivity.f);
        fVar.d(com.b.a.a.a(com.b.a.a.a(aVar)));
    }

    static /* synthetic */ boolean a(AddressAddActivity addressAddActivity, String str) {
        for (String equals : addressAddActivity.p) {
            if (str.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    static /* synthetic */ String b(String str) {
        if ("北京".equals(str)) {
            return "北京市";
        }
        if ("天津".equals(str)) {
            return "天津市";
        }
        if ("上海".equals(str)) {
            return "上海市";
        }
        if ("重庆".equals(str)) {
            return "重庆市";
        }
        return null;
    }

    static /* synthetic */ void b(AddressAddActivity addressAddActivity) {
        InputMethodManager inputMethodManager = (InputMethodManager) addressAddActivity.getSystemService("input_method");
        View currentFocus = addressAddActivity.getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public final void a(String str) {
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.AddressAddActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public void addAddressSuccess(String str) {
        a();
        com.zhongsou.flymall.c.b.a((Context) this, (!g.b(str) || Integer.valueOf(str).intValue() <= 0) ? "操作失败！" : "操作成功！");
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.AddressAddActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.address_add);
        this.a = (AppContext) getApplication();
        if (!this.a.b()) {
            com.zhongsou.flymall.c.b.a((Context) this, (int) R.string.network_not_connected);
        }
        this.f = AppContext.a().e();
        this.g = new a();
        this.d = (Button) findViewById(R.id.address_save_btn);
        this.e = (Button) findViewById(R.id.head_back_btn);
        this.d.setVisibility(0);
        this.e.setVisibility(0);
        ((TextView) findViewById(R.id.main_head_title)).setText((int) R.string.address_add_title);
        this.d.setOnClickListener(new f(this));
        this.e.setOnClickListener(new g(this));
        this.h = (EditText) findViewById(R.id.address_add_province);
        this.i = (EditText) findViewById(R.id.address_add_city);
        this.j = (EditText) findViewById(R.id.address_add_area);
        this.h.setOnClickListener(new h(this));
        this.i.setOnClickListener(new j(this));
        this.j.setOnClickListener(new l(this));
    }
}
