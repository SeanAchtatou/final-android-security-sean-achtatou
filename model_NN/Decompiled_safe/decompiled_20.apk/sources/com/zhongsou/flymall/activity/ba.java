package com.zhongsou.flymall.activity;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.z;

final class ba implements AdapterView.OnItemClickListener {
    final /* synthetic */ BetaProductListActivity a;

    ba(BetaProductListActivity betaProductListActivity) {
        this.a = betaProductListActivity;
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        Log.i("ProductList", "onItemClick");
        if (i != 0 && view != this.a.r) {
            z zVar = view instanceof TextView ? (z) view.getTag() : (z) ((TextView) view.findViewById(R.id.product_title)).getTag();
            if (zVar != null) {
                b.a(view.getContext(), zVar);
            }
        }
    }
}
