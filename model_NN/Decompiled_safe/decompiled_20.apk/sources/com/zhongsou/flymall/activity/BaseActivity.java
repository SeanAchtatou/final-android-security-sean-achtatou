package com.zhongsou.flymall.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.manager.a;

public class BaseActivity extends Activity {
    private f a;
    private ProgressDialog b = null;

    public final void a(int i, int... iArr) {
        String string = getString(i);
        if (findViewById(R.id.main_head_title) != null) {
            ((TextView) findViewById(R.id.main_head_title)).setText(string);
        }
        if (iArr != null && iArr.length > 0) {
            for (int i2 : iArr) {
                if (findViewById(i2) != null) {
                    findViewById(i2).setVisibility(0);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final f b() {
        if (this.a == null) {
            this.a = new f(this);
            this.a.a();
        }
        return this.a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.BaseActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!AppContext.a().b()) {
            b.a((Context) this, (int) R.string.network_not_connected);
        }
        a.a();
        a.a((Activity) this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a.a();
        a.b(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
        }
    }
}
