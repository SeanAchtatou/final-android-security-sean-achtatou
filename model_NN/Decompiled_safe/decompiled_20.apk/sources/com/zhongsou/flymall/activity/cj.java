package com.zhongsou.flymall.activity;

import android.content.DialogInterface;
import com.zhongsou.flymall.d.aa;
import com.zhongsou.flymall.g.g;

final class cj implements DialogInterface.OnClickListener {
    final /* synthetic */ CheckActivity a;

    cj(CheckActivity checkActivity) {
        this.a = checkActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String payt = this.a.o.getPayt();
        long longValue = g.a(payt) ? 0 : Long.valueOf(payt).longValue();
        new aa();
        this.a.a(1 == longValue ? this.a.u : (aa) this.a.t.get(i));
        dialogInterface.dismiss();
    }
}
