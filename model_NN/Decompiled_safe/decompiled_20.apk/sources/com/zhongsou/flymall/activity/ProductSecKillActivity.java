package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.al;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.x;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import java.util.List;

public class ProductSecKillActivity extends BaseActivity implements o {
    public int a = 1;
    int b = 0;
    int c = 8;
    fx d = new fu(this);
    /* access modifiers changed from: private */
    public ListView e;
    private al f;
    private f g;
    private TextView h;
    private Button i;
    private ProgressDialog j;
    private LinearLayout k;
    private boolean l = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.ProductSecKillActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public final void a(int i2) {
        if (this.l) {
            b.a((Context) this, (int) R.string.has_load_all);
        } else if (!AppContext.a().b()) {
            b.a((Context) this, (int) R.string.network_not_connected);
        } else {
            if (i2 != 0) {
                this.k.setVisibility(0);
            }
            this.g = new f(this);
            this.g.a();
            this.g.a(Integer.valueOf(i2));
        }
    }

    public final void a(String str) {
        if (this.j != null) {
            this.j.dismiss();
        }
        if (this.k != null) {
            this.k.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.ProductSecKillActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public void getTimeBuyActGoodsSuccess(List<x> list) {
        this.j.dismiss();
        this.k.setVisibility(8);
        if (list != null) {
            if (list.size() > 0) {
                this.f.a(list);
            } else {
                this.l = true;
                b.a((Context) this, (int) R.string.has_load_all);
            }
        }
        this.f.notifyDataSetChanged();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.product_seckill_list);
        this.j = ProgressDialog.show(this, null, getString(R.string.click_loading));
        this.h = (TextView) findViewById(R.id.main_head_title);
        this.i = (Button) findViewById(R.id.head_back_btn);
        this.h.setText((int) R.string.seckill_product_text);
        this.i.setVisibility(0);
        this.i.setOnClickListener(new fv(this));
        this.k = (LinearLayout) findViewById(R.id.ll_loading_more);
        this.e = (ListView) findViewById(R.id.lv_seckill_list);
        this.e.setOnTouchListener(new fw(this));
        this.e.addHeaderView(LayoutInflater.from(this).inflate((int) R.layout.listview_header, (ViewGroup) this.e, false));
        this.f = new al(this);
        this.e.setAdapter((ListAdapter) this.f);
        a(0);
    }
}
