package com.zhongsou.flymall.activity;

import android.view.View;
import android.widget.CheckBox;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import java.util.List;

final class bw extends Thread {
    final /* synthetic */ View a;
    final /* synthetic */ bv b;

    bw(bv bvVar, View view) {
        this.b = bvVar;
        this.a = view;
    }

    public final void run() {
        bo boVar = this.b.a;
        CheckBox checkBox = (CheckBox) this.a;
        List<a> a2 = d.a();
        for (a is_checked : a2) {
            is_checked.setIs_checked(d.a(checkBox.isChecked()));
        }
        d.b(a2);
        this.b.a.a.e();
    }
}
