package com.zhongsou.flymall.activity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.g.e;
import com.zhongsou.flymall.manager.b;
import java.io.File;

public final class ed implements View.OnClickListener {
    private MainActivity a;
    private TextView b;
    private TextView c;
    private TextView d;

    public ed(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    private void c() {
        this.c.setText(e.a(e.a(this.a.getFilesDir())));
        try {
            PackageInfo packageInfo = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 0);
            this.d = (TextView) this.a.findViewById(R.id.about_version);
            this.d.setText("当前版本：v" + packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace(System.err);
        }
    }

    public final ed a() {
        this.b = (TextView) this.a.findViewById(R.id.main_head_title);
        this.c = (TextView) this.a.findViewById(R.id.tv_cache_size);
        this.a.findViewById(R.id.more_clear_cache).setOnClickListener(this);
        this.a.findViewById(R.id.more_check_version).setOnClickListener(this);
        this.a.findViewById(R.id.more_about).setOnClickListener(this);
        c();
        return this;
    }

    public final ed b() {
        this.b.setText((int) R.string.main_navigation_more);
        c();
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.manager.e.a(android.content.Context, boolean):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, int]
     candidates:
      com.zhongsou.flymall.manager.e.a(com.zhongsou.flymall.manager.e, int):int
      com.zhongsou.flymall.manager.e.a(com.zhongsou.flymall.manager.e, java.lang.String):java.lang.String
      com.zhongsou.flymall.manager.e.a(android.content.Context, boolean):void */
    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.more_clear_cache:
                File filesDir = this.a.getFilesDir();
                if (e.a(filesDir) != 0) {
                    b.a();
                    com.zhongsou.flymall.c.b.b(this.a, "清除图片：" + e.a(e.a(filesDir)));
                    e.b(filesDir);
                    this.c.setText(e.a(e.a(filesDir)));
                    return;
                }
                return;
            case R.id.tv_cache_size:
            case R.id.about_version:
            default:
                return;
            case R.id.more_check_version:
                com.zhongsou.flymall.manager.e.a().a((Context) this.a, true);
                return;
            case R.id.more_about:
                com.zhongsou.flymall.c.b.a((Activity) this.a);
                return;
        }
    }
}
