package com.zhongsou.flymall.activity;

import android.content.Context;
import android.content.DialogInterface;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.b;
import java.util.List;

final class bs implements DialogInterface.OnClickListener {
    final /* synthetic */ br a;

    bs(br brVar) {
        this.a = brVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.b(android.content.Context, int):android.app.AlertDialog
     arg types: [com.zhongsou.flymall.activity.MainActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.b(android.content.Context, long):void
      com.zhongsou.flymall.c.b.b(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.b(android.content.Context, int):android.app.AlertDialog */
    public final void onClick(DialogInterface dialogInterface, int i) {
        bo boVar = this.a.a;
        List c = bo.c();
        if (c.size() == 0) {
            dialogInterface.dismiss();
            b.b((Context) this.a.a.a, (int) R.string.cart_del_btn_promotion_message);
            return;
        }
        dialogInterface.dismiss();
        this.a.a.a.c();
        new bt(this, c).start();
    }
}
