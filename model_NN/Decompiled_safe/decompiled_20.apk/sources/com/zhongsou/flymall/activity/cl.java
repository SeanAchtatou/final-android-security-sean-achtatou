package com.zhongsou.flymall.activity;

import android.content.DialogInterface;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import java.util.List;

final class cl implements DialogInterface.OnClickListener {
    final /* synthetic */ List a;
    final /* synthetic */ CheckActivity b;

    cl(CheckActivity checkActivity, List list) {
        this.b = checkActivity;
        this.a = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            ((TextView) this.b.k.findViewById(R.id.check_order_coupon_content)).setText("无");
            this.b.o.setCouid(PoiTypeDef.All);
            this.b.o.setCoupon_amount("0");
            this.b.h();
        } else {
            this.b.a(this.a);
        }
        dialogInterface.dismiss();
    }
}
