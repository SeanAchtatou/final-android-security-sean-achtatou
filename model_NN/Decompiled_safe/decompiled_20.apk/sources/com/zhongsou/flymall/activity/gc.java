package com.zhongsou.flymall.activity;

import android.view.View;
import android.widget.ExpandableListView;

final class gc implements ExpandableListView.OnChildClickListener {
    final /* synthetic */ SearchAttrsActivity a;

    gc(SearchAttrsActivity searchAttrsActivity) {
        this.a = searchAttrsActivity;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        if (!(this.a.g == i && this.a.h == i2)) {
            int unused = this.a.g = i;
            int unused2 = this.a.h = i2;
            SearchAttrsActivity.a(this.a, i, i2);
        }
        return false;
    }
}
