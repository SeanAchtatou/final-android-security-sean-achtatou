package com.zhongsou.flymall.activity;

import android.text.Editable;
import android.text.TextWatcher;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class fy implements TextWatcher {
    final /* synthetic */ SearchActivity a;

    fy(SearchActivity searchActivity) {
        this.a = searchActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (charSequence.length() > 0) {
            this.a.i.setText((int) R.string.search_search);
            this.a.i.setTag(Integer.valueOf((int) R.string.search_search));
            this.a.j.setVisibility(0);
            return;
        }
        this.a.i.setText((int) R.string.search_cancel);
        this.a.i.setTag(Integer.valueOf((int) R.string.search_cancel));
        this.a.j.setVisibility(8);
    }
}
