package com.zhongsou.flymall.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.b.a.a;
import com.b.a.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.t;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.m;
import com.zhongsou.flymall.d.n;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.g;
import com.zhongsou.flymall.h;
import java.util.ArrayList;
import java.util.List;

public class OrderDetailActivity extends BaseActivity implements View.OnClickListener, o, h {
    long a = 0;
    String b = PoiTypeDef.All;
    TextView c;
    Button d;
    Button e;
    ListView f;
    List<n> g = new ArrayList();
    private ProgressDialog h;
    private f i;
    private AppContext j;
    private t k;

    private void a() {
        this.j = AppContext.a();
        if (this.j.d()) {
            this.i = new f(this);
            this.i.a();
            this.i.f(Long.valueOf(this.a));
            return;
        }
        finish();
    }

    public final void a(String str) {
        if (this.h != null) {
            this.h.dismiss();
        }
    }

    public void getAliPayInfoSuccess(e eVar) {
        this.h.dismiss();
        b.getPayInfoSuccess(this, this, eVar);
        a();
    }

    @SuppressLint({"SimpleDateFormat"})
    public void getOrderDetailSuccess(e eVar) {
        this.h.dismiss();
        if (eVar != null) {
            try {
                m mVar = (m) a.a(eVar.a(), m.class);
                this.b = mVar.getSn();
                if (mVar.a()) {
                    this.e.setVisibility(0);
                }
                ((TextView) findViewById(R.id.order_detail_amount_value)).setText(com.zhongsou.flymall.g.b.a(mVar.getAmount()));
                ((TextView) findViewById(R.id.order_detail_ship)).setText(" (" + mVar.getShipt() + "：" + com.zhongsou.flymall.g.b.a(mVar.getShipa()) + ")");
                ((TextView) findViewById(R.id.order_detail_order_sn_value)).setText(mVar.getSn());
                ((TextView) findViewById(R.id.order_detail_create_time_value)).setText(g.a(mVar.getTime()));
                com.zhongsou.flymall.d.a address = mVar.getAddress();
                if (address != null) {
                    ((TextView) findViewById(R.id.order_detail_receiver_value)).setText(address.getName());
                    ((TextView) findViewById(R.id.order_detail_address_value)).setText(address.getAddress());
                }
                if (mVar.getHas_invoice() == 1) {
                    ((TextView) findViewById(R.id.order_detail_invoice_title_value)).setText(mVar.getInvoice_title());
                    ((TextView) findViewById(R.id.order_detail_invoice_content_value)).setText(mVar.getInvoice_content());
                    findViewById(R.id.order_detail_invoice_type).setVisibility(0);
                    findViewById(R.id.order_detail_invoice_title).setVisibility(0);
                    findViewById(R.id.order_detail_invoice_content).setVisibility(0);
                }
                if (mVar.getUse_counpon() == 1) {
                    ((TextView) findViewById(R.id.order_detail_coupon_value)).setText(com.zhongsou.flymall.g.b.a(mVar.getCoupon_amount()) + "(满" + com.zhongsou.flymall.g.b.a(mVar.getLower_amount()) + "使用)");
                    findViewById(R.id.order_detail_coupon).setVisibility(0);
                }
                this.g.clear();
                if (mVar.getItems() != null && mVar.getItems().size() > 0) {
                    this.g.addAll(mVar.getItems());
                }
                this.k.notifyDataSetChanged();
                b.a(this.f);
            } catch (Exception e2) {
                Log.e("OrderDetailActivity", "getOrderDetailSuccess");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        b.a(intent, this.e, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.OrderDetailActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public void onClick(View view) {
        if (view.getId() == R.id.head_back_btn) {
            finish();
        } else if (view.getId() == R.id.head_right_btn) {
            if (!this.j.b()) {
                b.a((Context) this, (int) R.string.network_not_connected);
                return;
            }
            this.i = b();
            this.i.b(this.b);
        } else if (view.getId() == R.id.main_footbar_home) {
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Log.i("Order", "onCreate");
        this.h = ProgressDialog.show(this, null, getString(R.string.click_loading));
        this.a = getIntent().getLongExtra("order_id", 0);
        super.onCreate(bundle);
        setContentView((int) R.layout.order_detail);
        this.c = (TextView) findViewById(R.id.main_head_title);
        this.c.setText((int) R.string.order_detail_title);
        this.d = (Button) findViewById(R.id.head_back_btn);
        this.d.setVisibility(0);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(R.id.head_right_btn);
        this.e.setText((int) R.string.order_detail_pay);
        this.e.setOnClickListener(this);
        this.f = (ListView) findViewById(R.id.order_detail_list);
        this.k = new t(this, this.g);
        this.f.setAdapter((ListAdapter) this.k);
        a();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void paySuccess() {
        this.h.dismiss();
        this.e.setVisibility(8);
    }
}
