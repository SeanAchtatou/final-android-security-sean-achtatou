package com.zhongsou.flymall.activity;

import android.os.Handler;
import android.os.Message;

/* renamed from: com.zhongsou.flymall.activity.do  reason: invalid class name */
final class Cdo extends Handler {
    final /* synthetic */ MainActivity a;

    Cdo(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void handleMessage(Message message) {
        if (message.what == 0) {
            if (this.a.p != null) {
                this.a.p.dismiss();
            }
        } else if (message.what == 1) {
            this.a.j.b();
        }
    }
}
