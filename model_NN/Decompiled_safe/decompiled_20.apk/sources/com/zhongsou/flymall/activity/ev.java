package com.zhongsou.flymall.activity;

import android.widget.AbsListView;

final class ev implements AbsListView.OnScrollListener {
    final /* synthetic */ ProductActivity a;

    ev(ProductActivity productActivity) {
        this.a = productActivity;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        int unused = this.a.ad = i2;
        int unused2 = this.a.ac = (i + i2) - 1;
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        int count = (this.a.Y.getCount() - 1) + 1;
        if (i == 0 && this.a.ac == count) {
            this.a.c();
        }
    }
}
