package com.zhongsou.flymall.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.zhongsou.flymall.d.a;

final class x implements AdapterView.OnItemClickListener {
    final /* synthetic */ AddressListActivity a;

    x(AddressListActivity addressListActivity) {
        this.a = addressListActivity;
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        a unused = this.a.j = (a) this.a.g.get(i);
        long unused2 = this.a.h = this.a.j.getId();
        if (this.a.k == 1) {
            Intent intent = this.a.getIntent();
            intent.putExtra("address", this.a.j);
            this.a.setResult(11, intent);
            this.a.finish();
        } else if (this.a.j.a()) {
            new AlertDialog.Builder(this.a).setTitle("请选择").setItems(new String[]{"编辑", "删除"}, new y(this)).show();
        } else {
            new AlertDialog.Builder(this.a).setTitle("请选择").setItems(new String[]{"设为默认", "编辑", "删除"}, new z(this)).show();
        }
    }
}
