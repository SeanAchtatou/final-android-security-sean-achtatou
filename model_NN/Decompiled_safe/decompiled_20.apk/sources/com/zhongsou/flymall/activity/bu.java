package com.zhongsou.flymall.activity;

import android.content.Context;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.c.b;
import java.io.Serializable;
import java.util.List;

final class bu implements View.OnClickListener {
    final /* synthetic */ bo a;

    bu(bo boVar) {
        this.a = boVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.b(android.content.Context, int):android.app.AlertDialog
     arg types: [com.zhongsou.flymall.activity.MainActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.b(android.content.Context, long):void
      com.zhongsou.flymall.c.b.b(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.b(android.content.Context, int):android.app.AlertDialog */
    public final void onClick(View view) {
        bo boVar = this.a;
        List<String> d = bo.d();
        if (d == null || d.size() == 0) {
            b.b((Context) this.a.a, (int) R.string.prompt_cart_empty_message);
        } else if (!AppContext.a().d()) {
            dp.a = true;
            dp.b = d;
            this.a.a.a(3);
        } else if (bo.b(this.a)) {
            b.a(this.a.a, "cartIds", (Serializable) d);
        }
    }
}
