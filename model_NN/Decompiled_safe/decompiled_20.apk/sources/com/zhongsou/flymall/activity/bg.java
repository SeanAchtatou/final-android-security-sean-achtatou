package com.zhongsou.flymall.activity;

import android.content.Context;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.g.g;

final class bg implements View.OnClickListener {
    final /* synthetic */ MainActivity a;
    final /* synthetic */ a b;
    final /* synthetic */ bd c;

    bg(bd bdVar, MainActivity mainActivity, a aVar) {
        this.c = bdVar;
        this.a = mainActivity;
        this.b = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void onClick(View view) {
        String obj = this.c.d.getText().toString();
        if (g.a(obj) || Integer.valueOf(obj).intValue() == 0) {
            b.a(this.a, R.string.cart_dialog_del_title, R.string.cart_dialog_del_message, new bh(this), new bj(this));
            return;
        }
        int intValue = ((Integer) this.c.d.getTag()).intValue();
        int a2 = d.a(intValue, this.b);
        if (intValue != a2) {
            b.a((Context) this.a, this.b.getName() + this.a.getString(R.string.prompt_cart_is_full_stock) + a2);
            return;
        }
        this.c.b();
        this.a.c();
        new bk(this, intValue).start();
    }
}
