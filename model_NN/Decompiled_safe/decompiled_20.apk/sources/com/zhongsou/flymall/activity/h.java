package com.zhongsou.flymall.activity;

import android.app.AlertDialog;
import android.view.View;
import com.zhongsou.flymall.b.b;
import java.util.List;

final class h implements View.OnClickListener {
    final /* synthetic */ AddressAddActivity a;

    h(AddressAddActivity addressAddActivity) {
        this.a = addressAddActivity;
    }

    public final void onClick(View view) {
        List unused = this.a.k = b.a();
        int size = this.a.k.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            strArr[i] = ((com.zhongsou.flymall.d.b) this.a.k.get(i)).getProvince();
        }
        new AlertDialog.Builder(this.a).setTitle("请选择").setItems(strArr, new i(this)).show();
    }
}
