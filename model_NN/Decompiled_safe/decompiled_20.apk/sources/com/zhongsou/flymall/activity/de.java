package com.zhongsou.flymall.activity;

import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.c.a;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.k;
import java.util.ArrayList;
import java.util.List;

public final class de extends PagerAdapter {
    List<k> a = new ArrayList();
    List<ImageView> b = new ArrayList();
    final /* synthetic */ cn c;

    public de(cn cnVar) {
        this.c = cnVar;
    }

    public final void a(List<k> list) {
        this.a.clear();
        this.b.clear();
        if (list != null && list.size() > 0) {
            k kVar = list.get(0);
            this.a.add(list.get(list.size() - 1));
            this.b.add(null);
            for (int i = 0; i < list.size(); i++) {
                this.a.add(list.get(i));
                this.b.add(null);
            }
            this.a.add(kVar);
            this.b.add(null);
        }
        notifyDataSetChanged();
    }

    public final void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) obj);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object instantiateItem(ViewGroup viewGroup, int i) {
        ImageView imageView = this.b.get(i);
        if (imageView == null) {
            k kVar = this.a.get(i);
            ImageView imageView2 = new ImageView(this.c.c);
            imageView2.setBackgroundResource(R.drawable.loop_default);
            imageView2.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
            String image = kVar.getImage();
            if (!TextUtils.isEmpty(image)) {
                ((a) this.c.m.a(imageView2)).a(this.c.n + image, viewGroup.getWidth());
            }
            imageView2.setOnClickListener(new df(this, kVar));
            this.b.set(i, imageView2);
            imageView = imageView2;
        }
        viewGroup.addView(imageView);
        return imageView;
    }

    public final boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }
}
