package com.zhongsou.flymall.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.ZoomButtonsController;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.umeng.common.b.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.af;
import com.zhongsou.flymall.a.ah;
import com.zhongsou.flymall.a.aj;
import com.zhongsou.flymall.a.as;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.d.t;
import com.zhongsou.flymall.d.u;
import com.zhongsou.flymall.d.v;
import com.zhongsou.flymall.d.w;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.b;
import com.zhongsou.flymall.g.g;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends BaseActivity implements o {
    private ImageButton[] A = new ImageButton[0];
    private View B;
    private View C;
    private View D;
    /* access modifiers changed from: private */
    public v E;
    private ListView F;
    /* access modifiers changed from: private */
    public ImageButton G;
    private GridView H;
    /* access modifiers changed from: private */
    public aj I;
    private View J;
    private View K;
    private LinearLayout L;
    private View M;
    private ListView N;
    /* access modifiers changed from: private */
    public ah O;
    private List<u> P;
    /* access modifiers changed from: private */
    public int Q = 0;
    /* access modifiers changed from: private */
    public int R;
    private View S;
    private TextView T;
    private LinearLayout U;
    /* access modifiers changed from: private */
    public int V = 1;
    private View W;
    private ListView X;
    /* access modifiers changed from: private */
    public af Y;
    /* access modifiers changed from: private */
    public int Z = 1;
    private f a;
    private LinearLayout aa;
    private List<t> ab;
    /* access modifiers changed from: private */
    public int ac = 0;
    /* access modifiers changed from: private */
    public int ad;
    /* access modifiers changed from: private */
    public ImageButton ae;
    /* access modifiers changed from: private */
    public ImageButton af;
    /* access modifiers changed from: private */
    public ImageButton ag;
    private Handler ah = new eu(this);
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private TextView i;
    private TextView j;
    private TextView k;
    private Gallery l;
    /* access modifiers changed from: private */
    public ProgressDialog m;
    private FrameLayout n = null;
    private WebView o;
    private LinearLayout p;
    /* access modifiers changed from: private */
    public long q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private boolean v = false;
    private boolean w = false;
    private boolean x = false;
    private View y;
    private View[] z = new View[0];

    /* access modifiers changed from: private */
    public void a(int i2) {
        Log.v("Huang", "Pno:" + i2);
        this.a.a(String.valueOf(this.q), String.valueOf(i2));
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        this.x = false;
        if (!this.r || this.q != j2) {
            this.q = j2;
            b(getString(R.string.product_loading));
            this.a = new f(this);
            this.a.a();
            this.a.a(j2);
        } else {
            a((int) R.string.product_base_info, R.id.head_back_btn);
            a(this.y);
        }
        for (ImageButton selected : this.A) {
            selected.setSelected(false);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j2, String str, double d2) {
        a aVar = new a();
        aVar.setGd_id(this.q);
        aVar.setArticle_id(j2);
        aVar.setUser_id(AppContext.a().e());
        if (this.E.getImgs() != null && this.E.getImgs().size() > 0) {
            aVar.setImg_remote(this.E.getImgs().get(0));
        }
        aVar.setPrice(d2);
        aVar.setName(this.E.getName());
        if (this.E.a() && str != null && str.length() > 0) {
            aVar.setStock_attr(this.E.getStock_name() + ":" + str);
            e();
        }
        aVar.setAct_id(this.E.getAct_id());
        d.a(aVar);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.add_to_cart_success);
        builder.setPositiveButton((int) R.string.go_to_cart, new en(this));
        builder.setNegativeButton((int) R.string.continue_shopping, new eo(this));
        builder.create().show();
    }

    private void a(View view) {
        if (view != null) {
            view.setVisibility(0);
        }
        for (View view2 : this.z) {
            if (!(view2 == null || view == view2)) {
                view2.setVisibility(8);
            }
        }
    }

    static /* synthetic */ void a(ProductActivity productActivity, ImageButton imageButton) {
        if (imageButton != null) {
            imageButton.setSelected(true);
        }
        for (ImageButton imageButton2 : productActivity.A) {
            if (!(imageButton2 == null || imageButton == imageButton2)) {
                imageButton2.setSelected(false);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        Log.v("Huang", "Pno:" + i2);
        this.a.b(String.valueOf(this.q), String.valueOf(i2));
    }

    static /* synthetic */ void b(ProductActivity productActivity, long j2) {
        productActivity.x = true;
        if (!productActivity.s) {
            productActivity.a = new f(productActivity);
            productActivity.a.a();
            productActivity.a.b(j2);
            return;
        }
        productActivity.a((int) R.string.product_desc, R.id.head_back_btn);
        productActivity.a(productActivity.n);
    }

    private void b(String str) {
        if (this.m == null) {
            this.m = new ProgressDialog(this);
            this.m.setIndeterminate(true);
            this.m.setMessage(str);
            this.m.setCancelable(true);
            this.m.setCanceledOnTouchOutside(false);
            this.m.show();
            return;
        }
        this.m.setMessage(str);
        this.m.show();
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.x) {
            a(this.q);
        } else {
            finish();
        }
    }

    static /* synthetic */ void g(ProductActivity productActivity) {
        if (productActivity.E == null) {
            return;
        }
        if (productActivity.E.a()) {
            List<w> stock_vals = productActivity.E.getStock_vals();
            productActivity.x = true;
            if (!productActivity.v) {
                productActivity.a((int) R.string.select_product_base_info, R.id.head_back_btn);
                productActivity.a(productActivity.K);
                ((TextView) productActivity.findViewById(R.id.txt_tip_attr)).setText(productActivity.getString(R.string.select) + productActivity.E.getStock_name() + "：");
                productActivity.F = (ListView) productActivity.findViewById(R.id.list_stock_attr);
                productActivity.F.setOnItemClickListener(new ek(productActivity));
                productActivity.F.setAdapter((ListAdapter) new as(productActivity, stock_vals));
                productActivity.v = true;
                return;
            }
            productActivity.a(productActivity.K);
            return;
        }
        productActivity.a(productActivity.E.getArticle_id(), null, productActivity.E.getPrice());
    }

    static /* synthetic */ void m(ProductActivity productActivity) {
        productActivity.x = true;
        if (!productActivity.t) {
            productActivity.a((int) R.string.product_discuss, R.id.head_back_btn);
            productActivity.a(productActivity.M);
            productActivity.M.setVisibility(0);
            productActivity.a = new f(productActivity);
            productActivity.a.a();
            productActivity.N = (ListView) productActivity.findViewById(R.id.lv_product_discuss_list);
            productActivity.U = (LinearLayout) productActivity.findViewById(R.id.ll_product_discuss_empty);
            productActivity.N.setOnScrollListener(new ei(productActivity));
            productActivity.N.setOnItemClickListener(new et(productActivity));
            productActivity.N.addFooterView(productActivity.S);
            productActivity.P = new ArrayList();
            productActivity.O = new ah(productActivity, productActivity.P);
            productActivity.N.setVisibility(8);
            productActivity.N.setAdapter((ListAdapter) productActivity.O);
            productActivity.b(productActivity.getString(R.string.product_discuss_loading));
            productActivity.a(productActivity.V);
            return;
        }
        productActivity.a((int) R.string.product_discuss, R.id.head_back_btn);
        productActivity.a(productActivity.M);
    }

    static /* synthetic */ void o(ProductActivity productActivity) {
        productActivity.x = true;
        if (!productActivity.u) {
            productActivity.a((int) R.string.product_consult, R.id.head_back_btn);
            productActivity.a(productActivity.W);
            productActivity.W.setVisibility(0);
            productActivity.a = new f(productActivity);
            productActivity.a.a();
            productActivity.X = (ListView) productActivity.findViewById(R.id.lv_product_consult_list);
            productActivity.aa = (LinearLayout) productActivity.findViewById(R.id.ll_product_consult_empty);
            productActivity.X.setOnScrollListener(new ev(productActivity));
            productActivity.X.setOnItemClickListener(new ew(productActivity));
            productActivity.X.setOnItemClickListener(new ex(productActivity));
            productActivity.X.addFooterView(productActivity.S);
            productActivity.ab = new ArrayList();
            productActivity.Y = new af(productActivity, productActivity.ab);
            productActivity.X.setVisibility(8);
            productActivity.X.setAdapter((ListAdapter) productActivity.Y);
            productActivity.b(productActivity.getString(R.string.product_consult_loading));
            productActivity.b(productActivity.Z);
            return;
        }
        productActivity.a((int) R.string.product_consult, R.id.head_back_btn);
        productActivity.a(productActivity.W);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.e.f.a(long, java.lang.Integer):com.c.a
     arg types: [long, int]
     candidates:
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Object):void
      com.zhongsou.flymall.e.f.a(java.lang.Integer, java.lang.Integer):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.Long, java.lang.Double):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.String):com.c.a
      com.zhongsou.flymall.e.f.a(long, java.lang.Integer):com.c.a */
    static /* synthetic */ void q(ProductActivity productActivity) {
        productActivity.x = true;
        if (!productActivity.w) {
            productActivity.a(productActivity.J);
            productActivity.a((int) R.string.product_recommend, R.id.head_back_btn);
            productActivity.H = (GridView) productActivity.findViewById(R.id.gv_product_list);
            productActivity.L = (LinearLayout) productActivity.findViewById(R.id.ll_product_recommend_empty);
            productActivity.I = new aj(productActivity, true);
            productActivity.H.setAdapter((ListAdapter) productActivity.I);
            productActivity.H.setOnItemClickListener(new ey(productActivity));
            productActivity.b(productActivity.getString(R.string.product_loading));
            productActivity.a = new f(productActivity);
            productActivity.a.a();
            productActivity.a.a(productActivity.q, (Integer) 10);
            return;
        }
        productActivity.a((int) R.string.product_recommend, R.id.head_back_btn);
        productActivity.a(productActivity.J);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.T.setText((int) R.string.loading);
        this.ah.postDelayed(new el(this), 1000);
    }

    public final void a(String str) {
        d();
        e();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.T.setText((int) R.string.loading);
        this.ah.postDelayed(new em(this), 1000);
    }

    public final void d() {
        this.ah.sendEmptyMessage(0);
    }

    public void getGoodsDescSuccess(String str) {
        a((int) R.string.product_desc, R.id.head_back_btn);
        a(this.n);
        this.n.setVisibility(0);
        this.o = (WebView) findViewById(R.id.web_goods_desc);
        this.p = (LinearLayout) findViewById(R.id.ll_product_desc_empty);
        this.o.getSettings().setSupportZoom(true);
        this.o.getSettings().setBuiltInZoomControls(true);
        this.o.getSettings().setDefaultTextEncodingName(e.f);
        setZoomControlGone(this.o);
        if (TextUtils.isEmpty(str)) {
            this.o.setVisibility(8);
            this.p.setVisibility(0);
        } else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            this.o.getSettings().setJavaScriptEnabled(true);
            this.o.loadUrl(getString(R.string.WEB_DOMAIN) + "/kp/module/product/showDesc?gd_id=" + this.q + "&width=" + ((new BigDecimal(displayMetrics.widthPixels).divide(new BigDecimal((double) displayMetrics.density)).intValue() - new BigDecimal(12).multiply(new BigDecimal((double) displayMetrics.density)).intValue()) - 5));
            this.o.setVisibility(0);
            this.p.setVisibility(8);
        }
        this.s = true;
    }

    public void getProductConsultSuccess(List<t> list) {
        Log.v("Huang", "consultList.size():" + list.size());
        d();
        if (list == null || list.size() == 0) {
            this.X.removeFooterView(this.S);
            if (this.Y.getCount() == 0) {
                this.X.setVisibility(8);
                this.aa.setVisibility(0);
            }
            Log.v("Huang", "proConsultList.size():" + list.size());
        } else {
            this.X.setVisibility(0);
            this.aa.setVisibility(8);
            if (list.size() < 10) {
                this.X.removeFooterView(this.S);
            } else {
                this.Z++;
            }
            this.ab = list;
            for (t a2 : list) {
                this.Y.a(a2);
            }
            this.X.setSelection((this.ac - this.ad) + 1);
            this.Y.notifyDataSetChanged();
        }
        this.T.setText((int) R.string.main_navigation_more);
        this.u = true;
    }

    public void getProductDiscussSuccess(List<u> list) {
        d();
        if (list == null || list.size() == 0) {
            Log.v("Huang", "proDisList.size():" + this.P.size());
            this.N.removeFooterView(this.S);
            if (this.O.getCount() == 0) {
                this.N.setVisibility(8);
                this.U.setVisibility(0);
            }
        } else {
            this.N.setVisibility(0);
            this.U.setVisibility(8);
            if (list.size() < 10) {
                this.N.removeFooterView(this.S);
            } else {
                this.V++;
            }
            this.P = list;
            for (u a2 : list) {
                this.O.a(a2);
            }
            this.N.setSelection((this.Q - this.R) + 1);
            this.O.notifyDataSetChanged();
        }
        this.T.setText((int) R.string.main_navigation_more);
        this.t = true;
    }

    public void getProductSuccess(v vVar) {
        this.r = false;
        this.s = false;
        this.t = false;
        this.u = false;
        this.v = false;
        this.w = false;
        this.x = false;
        a((int) R.string.product_base_info, R.id.head_back_btn);
        a(this.y);
        this.y.setVisibility(0);
        this.B = findViewById(R.id.goods_split_attr);
        this.C = findViewById(R.id.stock_split_attr);
        this.D = findViewById(R.id.order_act);
        this.b = (TextView) findViewById(R.id.txt_product_name);
        this.c = (TextView) findViewById(R.id.txt_product_price);
        this.d = (TextView) findViewById(R.id.txt_product_market_price);
        this.e = (TextView) findViewById(R.id.txt_split_attr);
        this.f = (TextView) findViewById(R.id.txt_split_val);
        this.g = (TextView) findViewById(R.id.txt_stock_attr);
        this.h = (TextView) findViewById(R.id.txt_stock_val);
        this.i = (TextView) findViewById(R.id.txt_product_order_act);
        this.j = (TextView) findViewById(R.id.txt_freight);
        this.k = (TextView) findViewById(R.id.txt_product_price_discount);
        this.l = (Gallery) findViewById(R.id.img_products);
        ((Button) findViewById(R.id.head_back_btn)).setOnClickListener(new ez(this));
        ((Button) findViewById(R.id.head_right_btn_ask_question)).setOnClickListener(new fa(this));
        ((Button) findViewById(R.id.btn_add_cart)).setOnClickListener(new fb(this));
        this.l.setOnItemClickListener(new ej(this));
        this.b.setText(Html.fromHtml(vVar.getTitle()));
        this.c.setText(vVar.getPriceText());
        this.d.setText(b.a(vVar.getMarket_price()));
        this.d.getPaint().setFlags(16);
        if (g.a(vVar.getSplit_name())) {
            this.B.setVisibility(8);
        } else {
            this.e.setText(vVar.getSplit_name() + "：");
            this.f.setText(vVar.getSplit_value());
        }
        if (g.a(vVar.getStock_name())) {
            this.C.setVisibility(8);
        } else {
            this.g.setText(vVar.getStock_name() + "：");
            this.h.setText(vVar.getStockValString());
        }
        if (g.a(vVar.getOrder_act())) {
            this.D.setVisibility(8);
        } else {
            this.i.setText(vVar.getOrder_act());
        }
        if (vVar.getPrice() == 0.0d || (vVar.getDiscount() > BitmapDescriptorFactory.HUE_RED && vVar.getDiscount() < 10.0f)) {
            this.k.setText(vVar.getDiscount() + "折");
            this.k.setVisibility(0);
        } else {
            this.k.setVisibility(8);
        }
        this.j.setText(vVar.getFreightStr());
        this.l.setAdapter((SpinnerAdapter) new com.zhongsou.flymall.a.e(this, vVar.getImgs()));
        this.E = vVar;
        d();
        this.r = true;
    }

    public void getSameCateTopSaleSuccess(List<z> list) {
        d();
        if (list == null || list.size() <= 0) {
            this.L.setVisibility(0);
            this.H.setVisibility(8);
        } else {
            this.L.setVisibility(8);
            this.H.setVisibility(0);
            this.I.a(list);
        }
        this.I.notifyDataSetChanged();
        this.w = true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.product);
        this.q = getIntent().getLongExtra("gd_id", 0);
        a((int) R.string.product_base_info, R.id.head_back_btn);
        this.S = getLayoutInflater().inflate((int) R.layout.load_more, (ViewGroup) null);
        this.T = (TextView) this.S.findViewById(R.id.btn_load_more);
        this.y = findViewById(R.id.product_base_info);
        this.n = (FrameLayout) findViewById(R.id.pro_goods_desc);
        this.J = findViewById(R.id.ll_product_recommend);
        this.K = findViewById(R.id.product_attr);
        this.M = findViewById(R.id.product_discuss);
        this.W = findViewById(R.id.product_consult);
        this.z = new View[]{this.y, this.K, this.n, this.J, this.M, this.W};
        this.ae = (ImageButton) findViewById(R.id.image_button_desc);
        this.af = (ImageButton) findViewById(R.id.image_button_discuss);
        this.ag = (ImageButton) findViewById(R.id.image_button_consult);
        this.G = (ImageButton) findViewById(R.id.image_button_recommend);
        this.A = new ImageButton[]{this.ae, this.af, this.ag, this.G};
        this.ae.setOnClickListener(new ep(this));
        this.af.setOnClickListener(new eq(this));
        this.ag.setOnClickListener(new er(this));
        this.G.setOnClickListener(new es(this));
        a(this.q);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.x) {
            return super.onKeyDown(i2, keyEvent);
        }
        a(this.q);
        return true;
    }

    public void setZoomControlGone(View view) {
        try {
            Field declaredField = WebView.class.getDeclaredField("mZoomButtonsController");
            declaredField.setAccessible(true);
            ZoomButtonsController zoomButtonsController = new ZoomButtonsController(view);
            zoomButtonsController.getZoomControls().setVisibility(8);
            try {
                declaredField.set(view, zoomButtonsController);
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            }
        } catch (SecurityException e4) {
            e4.printStackTrace();
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
        }
    }
}
