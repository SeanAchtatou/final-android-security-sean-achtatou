package com.zhongsou.flymall.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.a.aq;
import com.zhongsou.flymall.b.i;
import com.zhongsou.flymall.b.j;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.e.e;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.g.g;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class SearchActivity extends BaseActivity implements View.OnClickListener, View.OnKeyListener, AdapterView.OnItemClickListener {
    private ListView a;
    private ListView b;
    private List<String> c = new ArrayList();
    private LinkedList<String> d = new LinkedList<>();
    private aq e;
    private aq f;
    private EditText g;
    private SharedPreferences h;
    /* access modifiers changed from: private */
    public Button i;
    /* access modifiers changed from: private */
    public ImageView j;
    private f k;

    private void a(String str, String str2) {
        this.h = getSharedPreferences("flymall", 0);
        SharedPreferences.Editor edit = this.h.edit();
        edit.putString(str, g.d(str2));
        edit.commit();
    }

    private boolean a() {
        String trim = this.g.getText().toString().trim();
        if (g.b((Object) trim)) {
            com.zhongsou.flymall.b.g.a(trim);
            this.d = new LinkedList<>(com.zhongsou.flymall.b.g.a());
            this.e.a(this.d);
            this.e.notifyDataSetChanged();
            b.a(this, 0, trim);
            return true;
        }
        this.g.requestFocus();
        this.g.setText(PoiTypeDef.All);
        return true;
    }

    public void hotKeywordsSuccess(e eVar) {
        this.c = eVar.a();
        List<String> list = this.c;
        j jVar = new j();
        jVar.b();
        jVar.c();
        if (list != null && list.size() > 0) {
            for (String a2 : list) {
                jVar.a(a2);
            }
        }
        jVar.close();
        this.c = i.a();
        this.f.a(this.c);
        this.f.notifyDataSetChanged();
        Calendar instance = Calendar.getInstance();
        instance.add(5, 1);
        a("hot_update_time", new StringBuilder().append(instance.getTimeInMillis()).toString());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_clearwords:
                if (g.b(this.g.getText())) {
                    this.g.setText(PoiTypeDef.All);
                    a("last_search_keyword", PoiTypeDef.All);
                    return;
                }
                return;
            case R.id.search_cancel:
                if (R.string.search_search == ((Integer) view.getTag()).intValue()) {
                    a();
                    return;
                } else if (R.string.search_cancel == ((Integer) view.getTag()).intValue()) {
                    finish();
                    return;
                } else {
                    return;
                }
            case R.id.search_history:
                this.e.a(this.d);
                this.e.notifyDataSetChanged();
                this.a.setVisibility(0);
                this.b.setVisibility(8);
                return;
            case R.id.search_hot:
                if (g.a(this.c)) {
                    this.c = Collections.EMPTY_LIST;
                }
                this.f.a(this.c);
                this.f.notifyDataSetChanged();
                this.b.setVisibility(0);
                this.a.setVisibility(8);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main_search);
        this.g = (EditText) findViewById(R.id.search_editer);
        this.a = (ListView) findViewById(R.id.history_search_list);
        this.e = new aq(this, this.d);
        this.a.setAdapter((ListAdapter) this.e);
        this.a.setOnItemClickListener(this);
        this.b = (ListView) findViewById(R.id.hot_keyword_search_list);
        this.f = new aq(this, this.c);
        this.b.setAdapter((ListAdapter) this.f);
        this.b.setOnItemClickListener(this);
        this.j = (ImageView) findViewById(R.id.search_clearwords);
        this.j.setOnClickListener(this);
        this.i = (Button) findViewById(R.id.search_cancel);
        this.i.setOnClickListener(this);
        this.d = new LinkedList<>(com.zhongsou.flymall.b.g.a());
        this.e.a(this.d);
        this.e.notifyDataSetChanged();
        this.h = getSharedPreferences("flymall", 0);
        String string = this.h.getString("hot_update_time", "0");
        Date date = new Date();
        if (!string.equals("0")) {
            date = new Date(Long.parseLong(string));
        }
        if (date.after(date)) {
            this.c = i.a();
        } else {
            this.k = new f(this);
            this.k.a();
            this.k.c();
        }
        findViewById(R.id.search_hot).setOnClickListener(this);
        findViewById(R.id.search_history).setOnClickListener(this);
        findViewById(R.id.search_editer).setOnKeyListener(this);
        this.h = getSharedPreferences("flymall", 0);
        String string2 = this.h.getString("last_search_keyword", PoiTypeDef.All);
        if (!string2.equals(PoiTypeDef.All)) {
            this.g.setText(string2);
            this.g.setSelection(string2.length());
        }
        this.i.setText((int) R.string.search_cancel);
        this.i.setTag(Integer.valueOf((int) R.string.search_cancel));
        this.j.setVisibility(8);
        this.g.addTextChangedListener(new fy(this));
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        String str = PoiTypeDef.All;
        if (this.b.getVisibility() == 0 && this.c.size() > 0) {
            str = this.c.get(i2);
            this.f.a(this.c);
            this.f.notifyDataSetChanged();
        } else if (this.d.size() > 0) {
            str = this.d.get(i2);
        }
        if (g.b((Object) str)) {
            this.g.setText(str);
            com.zhongsou.flymall.b.g.a(str);
            this.d = new LinkedList<>(com.zhongsou.flymall.b.g.a());
            this.e.a(this.d);
            this.e.notifyDataSetChanged();
            b.a(this, 0, str);
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if ((i2 == 66 || i2 == 84) && keyEvent.getAction() == 0) {
            return a();
        }
        return false;
    }
}
