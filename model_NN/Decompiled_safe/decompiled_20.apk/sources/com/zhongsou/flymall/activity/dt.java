package com.zhongsou.flymall.activity;

import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class dt implements View.OnClickListener {
    final /* synthetic */ dp a;

    dt(dp dpVar) {
        this.a = dpVar;
    }

    public final void onClick(View view) {
        this.a.c = true;
        this.a.s.setVisibility(0);
        this.a.h.setVisibility(8);
        this.a.g.setText((int) R.string.register);
        this.a.d.findViewById(R.id.login).setVisibility(8);
        this.a.d.findViewById(R.id.register).setVisibility(0);
        this.a.d.findViewById(R.id.member).setVisibility(8);
    }
}
