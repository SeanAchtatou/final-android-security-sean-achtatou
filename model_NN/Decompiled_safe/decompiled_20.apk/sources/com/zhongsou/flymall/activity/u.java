package com.zhongsou.flymall.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import com.zhongsou.flymall.c.b;
import java.util.List;

final class u implements View.OnClickListener {
    final /* synthetic */ AddressEditActivity a;

    u(AddressEditActivity addressEditActivity) {
        this.a = addressEditActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.AddressEditActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void onClick(View view) {
        if (this.a.p == 0) {
            b.a((Context) this.a, "请先选择城市");
            return;
        }
        List unused = this.a.n = com.zhongsou.flymall.b.b.b(this.a.p);
        int size = this.a.n.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            strArr[i] = ((com.zhongsou.flymall.d.b) this.a.n.get(i)).getArea();
        }
        new AlertDialog.Builder(this.a).setTitle("请选择").setItems(strArr, new v(this)).show();
    }
}
