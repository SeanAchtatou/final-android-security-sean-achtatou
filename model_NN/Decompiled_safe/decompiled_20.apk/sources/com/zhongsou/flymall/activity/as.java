package com.zhongsou.flymall.activity;

import android.view.View;
import android.widget.AdapterView;
import com.zhongsou.flymall.a.at;

final class as implements AdapterView.OnItemClickListener {
    final /* synthetic */ BetaProductActivity a;

    as(BetaProductActivity betaProductActivity) {
        this.a = betaProductActivity;
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        at atVar = (at) view.getTag();
        this.a.a(atVar.c.getArticle_id(), atVar.c.getAttr_val(), atVar.c.getPrice());
    }
}
