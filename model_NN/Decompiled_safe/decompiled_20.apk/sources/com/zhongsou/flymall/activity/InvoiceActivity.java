package com.zhongsou.flymall.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.g.g;

public class InvoiceActivity extends BaseActivity {
    private ListView a;
    private String[] b;
    /* access modifiers changed from: private */
    public dk c;
    /* access modifiers changed from: private */
    public int d = 0;
    private String e;
    private Button f;
    /* access modifiers changed from: private */
    public CheckBox g;
    /* access modifiers changed from: private */
    public CheckBox h;
    private View i;
    private View j;
    private TextView k;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.InvoiceActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    static /* synthetic */ void d(InvoiceActivity invoiceActivity) {
        String str = PoiTypeDef.All;
        String str2 = invoiceActivity.b[invoiceActivity.d];
        if (invoiceActivity.g.isChecked()) {
            str = "个人";
        } else if (invoiceActivity.h.isChecked()) {
            str = invoiceActivity.k.getText().toString();
            if (g.a(str)) {
                b.a((Context) invoiceActivity, "请填写单位名称！");
                return;
            }
        }
        Intent intent = invoiceActivity.getIntent();
        intent.putExtra("invoiceType", "普通发票");
        intent.putExtra("invoiceTitle", str);
        intent.putExtra("invoiceContent", str2);
        invoiceActivity.setResult(-1, intent);
        invoiceActivity.finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.invoice);
        Intent intent = getIntent();
        this.d = intent.getIntExtra("curPos", 0);
        this.e = intent.getStringExtra("invoiceTitle");
        ((TextView) findViewById(R.id.main_head_title)).setText("选择发票信息");
        this.f = (Button) findViewById(R.id.invoice_select_btn);
        this.f.setVisibility(0);
        this.i = findViewById(R.id.invoice_person_wraper);
        this.j = findViewById(R.id.invoice_company_wraper);
        this.g = (CheckBox) findViewById(R.id.invoice_person);
        this.h = (CheckBox) findViewById(R.id.invoice_company);
        this.k = (TextView) findViewById(R.id.invoice_company_name);
        if (g.a(this.e) || "个人".equals(this.e)) {
            this.g.setChecked(true);
            this.h.setChecked(false);
        } else {
            this.h.setChecked(true);
            this.g.setChecked(false);
            this.k.setText(this.e);
        }
        this.a = (ListView) findViewById(R.id.invoice_list);
        this.b = getIntent().getStringArrayExtra("invoices");
        this.c = new dk(this, this, this.b);
        this.a.setAdapter((ListAdapter) this.c);
        b.a(this.a);
        this.a.setOnItemClickListener(new dg(this));
        this.i.setOnClickListener(new dh(this));
        this.j.setOnClickListener(new di(this));
        this.f.setOnClickListener(new dj(this));
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return false;
        }
        setResult(-1, getIntent());
        finish();
        return false;
    }
}
