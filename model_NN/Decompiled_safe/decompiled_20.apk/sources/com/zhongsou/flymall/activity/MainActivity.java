package com.zhongsou.flymall.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.h;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.manager.e;
import com.zhongsou.flymall.ui.ScrollLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SuppressLint({"HandlerLeak"})
public class MainActivity extends BaseActivity implements o {
    /* access modifiers changed from: private */
    public ScrollLayout a;
    private RadioButton[] b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    private RadioButton e;
    private RadioButton f;
    private RadioButton g;
    private RadioButton h;
    private RadioButton i;
    /* access modifiers changed from: private */
    public bo j;
    private cn k;
    private by l;
    private dp m;
    private ed n;
    private AppContext o;
    /* access modifiers changed from: private */
    public ProgressDialog p;
    private Handler q = new Cdo(this);

    private static String a(long j2, long j3) {
        return j2 + "_" + j3;
    }

    static /* synthetic */ void a(MainActivity mainActivity, int i2) {
        AppContext.a = i2;
        switch (i2) {
            case 0:
                if (mainActivity.k == null) {
                    mainActivity.k = new cn(mainActivity).a();
                }
                mainActivity.k.b();
                return;
            case 1:
                if (mainActivity.l == null) {
                    mainActivity.l = new by(mainActivity).a();
                }
                mainActivity.l.b();
                return;
            case 2:
                if (mainActivity.j == null) {
                    mainActivity.j = new bo(mainActivity);
                }
                mainActivity.j.a();
                return;
            case 3:
                if (mainActivity.m == null) {
                    mainActivity.m = new dp(mainActivity).a();
                }
                mainActivity.m.b();
                return;
            case 4:
                if (mainActivity.n == null) {
                    mainActivity.n = new ed(mainActivity).a();
                }
                mainActivity.n.b();
                return;
            default:
                return;
        }
    }

    static /* synthetic */ void b(MainActivity mainActivity, int i2) {
        if (mainActivity.d != i2) {
            mainActivity.b[mainActivity.d].setChecked(false);
            mainActivity.b[i2].setChecked(true);
            mainActivity.d = i2;
        }
    }

    static /* synthetic */ void f() {
    }

    /* access modifiers changed from: private */
    public void g() {
        boolean z = true;
        this.e.setChecked(this.d == 0);
        this.f.setChecked(this.d == 1);
        this.g.setChecked(this.d == 2);
        this.h.setChecked(this.d == 3);
        RadioButton radioButton = this.i;
        if (this.d != 4) {
            z = false;
        }
        radioButton.setChecked(z);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_relativelayout_header);
        int childCount = relativeLayout.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if ((relativeLayout.getChildAt(i2) instanceof Button) || (relativeLayout.getChildAt(i2) instanceof ImageButton)) {
                relativeLayout.getChildAt(i2).setVisibility(8);
            }
        }
    }

    public final void a(int i2) {
        this.a.a(i2);
    }

    public final void a(String str) {
        Log.i("methodName", str);
        this.j.a(str);
    }

    public final void c() {
        if (this.p == null) {
            this.p = new ProgressDialog(this);
            this.p.setIndeterminate(true);
            this.p.setMessage(getString(R.string.loading));
            this.p.setCancelable(true);
            this.p.setCanceledOnTouchOutside(false);
        }
        this.p.show();
    }

    public final void d() {
        this.q.sendEmptyMessage(0);
    }

    public final void e() {
        this.q.sendEmptyMessage(1);
    }

    public void getCartSuccess(List<h> list) {
        bo boVar = this.j;
        List<a> a2 = d.a();
        HashMap hashMap = new HashMap(a2.size());
        for (a next : a2) {
            hashMap.put(a(next.getGd_id(), next.getArticle_id()), next);
        }
        ArrayList arrayList = new ArrayList(a2.size());
        for (h next2 : list) {
            a aVar = (a) hashMap.get(a(next2.getGd_id(), next2.getArticle_id()));
            if ((aVar != null && (next2.getPrice() != aVar.getPrice() || next2.getInvalidation() != aVar.getInvalidation() || next2.getStock() != aVar.getStock() || next2.getAct_id() != aVar.getAct_id() || next2.getAct_type() != aVar.getAct_type() || next2.getAct_stock() != aVar.getAct_stock())) || aVar.getNum() > 999) {
                aVar.setPrice(next2.getPrice());
                aVar.setInvalidation(next2.getInvalidation());
                aVar.setStock(next2.getStock());
                aVar.setIs_act_changed(d.b(next2.getAct_id() != aVar.getAct_id()));
                aVar.setAct_id(next2.getAct_id());
                aVar.setAct_type(next2.getAct_type());
                aVar.setAct_stock(next2.getAct_stock());
                if (aVar.getNum() > 999) {
                    aVar.setNum(999);
                }
                arrayList.add(aVar);
            }
        }
        this.j.b(a2);
        this.j.a(a2);
        d();
        if (arrayList.size() > 0) {
            d.a(arrayList, Arrays.asList("price", "num", "invalidation", "stock", "is_act_changed", "act_id", "act_type", "act_stock"));
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (intent != null) {
            try {
                Log.d(com.upomp.pay.c.a.n, "这是支付成功后，回调返回的报文，需自行解析" + new String(intent.getExtras().getByteArray("xml"), "utf-8"));
            } catch (Exception e2) {
                Log.d(com.upomp.pay.c.a.n, "Exception is " + e2);
            }
        } else {
            Log.d(com.upomp.pay.c.a.n, "data is null");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.manager.e.a(android.content.Context, boolean):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, int]
     candidates:
      com.zhongsou.flymall.manager.e.a(com.zhongsou.flymall.manager.e, int):int
      com.zhongsou.flymall.manager.e.a(com.zhongsou.flymall.manager.e, java.lang.String):java.lang.String
      com.zhongsou.flymall.manager.e.a(android.content.Context, boolean):void */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main);
        this.o = AppContext.a();
        boolean z = AppContext.a == 0;
        if (z) {
            this.o.h();
        }
        this.a = (ScrollLayout) findViewById(R.id.main_scrolllayout);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.main_linearlayout_footer);
        this.c = this.a.getChildCount();
        this.b = new RadioButton[this.c];
        for (int i2 = 0; i2 < this.c; i2++) {
            this.b[i2] = (RadioButton) linearLayout.getChildAt(i2);
            this.b[i2].setTag(Integer.valueOf(i2));
            this.b[i2].setChecked(false);
            this.b[i2].setOnClickListener(new dm(this));
        }
        this.a.a(new dn(this));
        this.e = (RadioButton) findViewById(R.id.main_footbar_home);
        this.f = (RadioButton) findViewById(R.id.main_footbar_category);
        this.g = (RadioButton) findViewById(R.id.main_footbar_cart);
        this.h = (RadioButton) findViewById(R.id.main_footbar_member);
        this.i = (RadioButton) findViewById(R.id.main_footbar_setting);
        this.k = new cn(this).a().b();
        this.l = new by(this).a();
        this.m = new dp(this).a();
        this.n = new ed(this).a();
        if (this.o.k() && z) {
            e.a().a((Context) this, false);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (this.d == 1 && this.l != null && this.l.d() > 0) {
                this.l.c();
                return true;
            } else if (this.d != 3 || this.m == null || !this.m.c) {
                d();
                b.b((Context) this);
                return true;
            } else {
                this.m.c();
                this.m.c = false;
                return true;
            }
        } else if (i2 == 82) {
            return true;
        } else {
            if (i2 != 84) {
                return super.onKeyDown(i2, keyEvent);
            }
            b.a((Context) this);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        dp.a = false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.c == 0) {
            this.c = 5;
        }
        this.d = AppContext.a;
        g();
        this.a.setIsScroll(this.o.l());
        if (this.d != 0) {
            this.a.setToScreen(this.d);
        }
    }
}
