package com.zhongsou.flymall.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.z;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.d.aa;
import com.zhongsou.flymall.d.q;
import com.zhongsou.flymall.d.r;
import com.zhongsou.flymall.d.s;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.b;
import com.zhongsou.flymall.g.g;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BetaCheckActivity extends BaseActivity implements View.OnClickListener, o {
    public static Map<Integer, String> a;
    /* access modifiers changed from: private */
    public f b;
    private Handler c;
    /* access modifiers changed from: private */
    public ScrollView d;
    private ListView e;
    private View f;
    /* access modifiers changed from: private */
    public View g;
    private View h;
    /* access modifiers changed from: private */
    public View i;
    /* access modifiers changed from: private */
    public View j;
    private Button k;
    private z l;
    private com.zhongsou.flymall.d.o m = new com.zhongsou.flymall.d.o();
    /* access modifiers changed from: private */
    public r n = new r();
    /* access modifiers changed from: private */
    public List<a> o = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> p = new ArrayList();
    private Button q;
    /* access modifiers changed from: private */
    public ProgressDialog r;
    /* access modifiers changed from: private */
    public List<aa> s = new ArrayList();
    /* access modifiers changed from: private */
    public aa t;
    private com.zhongsou.flymall.d.a u = null;
    private Runnable v = new ak(this);

    static {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        a = linkedHashMap;
        linkedHashMap.put(1, "提交成功");
        a.put(100, "请填写收货地址");
        a.put(2, "收货地址不存在，请重新选择");
        a.put(3, "优惠券已失效");
        a.put(4, "请选择支付方式");
        a.put(5, "订单项错误 ");
        a.put(6, "发票信息错误");
        a.put(7, "配送信息错误");
        a.put(8, "库存不足 ");
        a.put(9, "价格错误");
        a.put(10, "总价错误");
        a.put(11, "数据库插入错误 ");
        a.put(12, "重复提交");
        a.put(13, "商品已下架");
        a.put(14, "商品已删除 ");
        a.put(15, "活动不存在");
        a.put(16, "活动已经结束");
        a.put(17, "活动未开始");
        a.put(18, "超过可以购买的数量");
        a.put(19, "订单数据错误");
        a.put(20, "您选择的优惠券不存在");
        a.put(21, "您选择的优惠券活动已结束");
    }

    static /* synthetic */ int a(r rVar) {
        if (g.a(rVar.getAdrid())) {
            return 100;
        }
        if (g.a(rVar.getPayt())) {
            return 4;
        }
        return g.a(rVar.getShipt()) ? 7 : 1;
    }

    public static String a(int i2) {
        return a.get(Integer.valueOf(i2));
    }

    private String a(long j2, long j3) {
        for (a next : this.o) {
            if (j2 == next.getGd_id() && j3 == next.getArticle_id()) {
                return next.getName() + (j3 > 0 ? " " + next.getStock_attr() : PoiTypeDef.All) + "，";
            }
        }
        return PoiTypeDef.All;
    }

    private void a() {
        if (this.r != null) {
            this.r.dismiss();
            this.r = null;
        }
    }

    /* access modifiers changed from: private */
    public void a(aa aaVar) {
        TextView textView = (TextView) this.h.findViewById(R.id.check_order_ship_content);
        if (aaVar != null) {
            textView.setText(aaVar.getName() + " ￥" + b.b(aaVar.getPrice()));
            this.n.setShipt(aaVar.getValue());
            this.n.setShipa(String.valueOf(aaVar.getPrice()));
        } else {
            textView.setText(PoiTypeDef.All);
            this.n.setShipt(null);
            this.n.setShipa(null);
        }
        h();
    }

    /* access modifiers changed from: private */
    public String b(int i2) {
        return i2 == 0 ? getResources().getString(R.string.pay_platform_hdfk) : i2 == 11 ? "网上支付 (" + getResources().getString(R.string.pay_platform_zfb) + ")" : i2 == 12 ? "网上支付 (" + getResources().getString(R.string.pay_platform_yl) + ")" : PoiTypeDef.All;
    }

    private void b(String str) {
        new AlertDialog.Builder(this).setTitle("提示").setMessage(str).setPositiveButton("确定", new af(this)).show();
    }

    private void c() {
        List<s> payType = this.m.getPayType();
        if (payType != null && payType.size() != 0) {
            String[] strArr = new String[payType.size()];
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < payType.size()) {
                    strArr[i3] = b(payType.get(i3).getPlatform());
                    i2 = i3 + 1;
                } else {
                    new AlertDialog.Builder(this).setTitle("请选择").setItems(strArr, new ah(this, payType)).show();
                    return;
                }
            }
        }
    }

    private void d() {
        String[] strArr;
        int i2 = 0;
        String payt = this.n.getPayt();
        if (1 == (g.a(payt) ? 0 : Long.valueOf(payt).longValue())) {
            strArr = new String[]{this.t.getName()};
        } else {
            String[] strArr2 = new String[this.s.size()];
            while (true) {
                int i3 = i2;
                if (i3 >= this.s.size()) {
                    break;
                }
                strArr2[i3] = this.s.get(i3).getName();
                i2 = i3 + 1;
            }
            strArr = strArr2;
        }
        new AlertDialog.Builder(this).setTitle("请选择").setItems(strArr, new ai(this)).show();
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.m.getInvoices() != null) {
            String[] invoices = this.m.getInvoices();
            Intent intent = new Intent(this, InvoiceActivity.class);
            intent.putExtra("invoices", invoices);
            TextView textView = (TextView) this.j.findViewById(R.id.check_order_invoice_belongs_content);
            String obj = ((TextView) this.j.findViewById(R.id.check_order_invoice_content_content)).getText().toString();
            int length = invoices.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    i2 = 0;
                    break;
                } else if (obj.equals(invoices[i2])) {
                    break;
                } else {
                    i2++;
                }
            }
            intent.putExtra("curPos", i2);
            intent.putExtra("invoiceTitle", textView.getText().toString());
            startActivityForResult(intent, 1);
        }
    }

    private void f() {
        double d2;
        this.r = ProgressDialog.show(this, null, "正在加载");
        this.b = new f(this);
        this.b.a();
        double d3 = 0.0d;
        Iterator<a> it = this.o.iterator();
        while (true) {
            d2 = d3;
            if (!it.hasNext()) {
                break;
            }
            a next = it.next();
            d3 = d2 + (next.getPrice() * ((double) next.getNum()));
        }
        this.b.a(Long.valueOf(this.u == null ? 0 : this.u.getId()), Double.valueOf(d2));
    }

    private void g() {
        List<aa> freight = this.m.getFreight();
        if (freight != null && freight.size() != 0) {
            this.s = new ArrayList(freight.size());
            for (aa next : freight) {
                if ("4".equals(next.getValue())) {
                    this.t = next;
                } else {
                    this.s.add(next);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        double d2 = 0.0d;
        double d3 = 0.0d;
        for (a next : this.o) {
            d3 += next.getPrice() * ((double) next.getNum());
        }
        double doubleValue = g.b(this.n.getShipa()) ? d3 + Double.valueOf(this.n.getShipa()).doubleValue() : d3;
        if (doubleValue > 0.0d) {
            d2 = doubleValue;
        }
        ((TextView) findViewById(R.id.check_order_anount)).setText("￥" + b.b(d2));
        this.n.setTotal(String.valueOf(Double.valueOf(new DecimalFormat("0.00").format(d2)).doubleValue()));
    }

    private s i() {
        List<s> payType = this.m.getPayType();
        if (payType == null || payType.size() == 0) {
            return null;
        }
        for (s next : payType) {
            if (next.getPlatform() == 11) {
                return next;
            }
        }
        return payType.get(0);
    }

    public final void a(String str) {
        Log.i("CheckActivity", " onHttpError::::: comment onHttpError: " + str);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.BetaCheckActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public void loadCheckOrderDataSuccess(com.zhongsou.flymall.d.o oVar) {
        com.zhongsou.flymall.d.a aVar;
        com.zhongsou.flymall.d.a aVar2;
        String str;
        this.m = oVar;
        this.n.setToken(this.m.getToken());
        List<s> payType = this.m.getPayType();
        List<aa> freight = this.m.getFreight();
        String str2 = (payType == null || payType.size() == 0) ? "支付方式为空" : (freight == null || freight.size() == 0) ? "配送方式为空" : PoiTypeDef.All;
        if (g.b((Object) str2)) {
            com.zhongsou.flymall.c.b.a((Context) this, str2);
        }
        if (this.u != null) {
            aVar = this.u;
        } else {
            List<com.zhongsou.flymall.d.a> address = this.m.getAddress();
            if (address == null || address.size() == 0) {
                aVar = null;
            } else if (address.size() == 1) {
                aVar = address.get(0);
            } else {
                Iterator<com.zhongsou.flymall.d.a> it = address.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        aVar2 = null;
                        break;
                    }
                    aVar2 = it.next();
                    if (aVar2.a()) {
                        break;
                    }
                }
                aVar = aVar2 == null ? address.get(0) : aVar2;
            }
        }
        if (aVar != null) {
            TextView textView = (TextView) this.f.findViewById(R.id.check_order_address_address);
            ((TextView) this.f.findViewById(R.id.check_order_address_username)).setText(aVar.getName());
            ((TextView) this.f.findViewById(R.id.check_order_address_tel)).setText(aVar.getMobile());
            if (aVar == null) {
                str = PoiTypeDef.All;
            } else {
                String str3 = PoiTypeDef.All;
                if (g.b((Object) aVar.getProvince())) {
                    str3 = str3 + aVar.getProvince();
                }
                if (g.b((Object) aVar.getCity())) {
                    str3 = str3 + aVar.getCity();
                }
                if (g.b((Object) aVar.getArea())) {
                    str3 = str3 + aVar.getArea();
                }
                str = str3 + aVar.getAddress();
            }
            textView.setText(str);
            this.n.setAdrid(String.valueOf(aVar.getId()));
        }
        s i2 = i();
        TextView textView2 = (TextView) this.g.findViewById(R.id.check_order_pay_content);
        if (i2 != null) {
            textView2.setText(b(i2.getPlatform()));
            this.n.setPayid(String.valueOf(i2.getPay_id()));
            this.n.setPayt(String.valueOf(i2.getPay_type()));
        } else {
            textView2.setText(PoiTypeDef.All);
            this.n.setPayid(null);
            this.n.setPayt(null);
        }
        List<aa> freight2 = this.m.getFreight();
        aa aaVar = (freight2 == null || freight2.size() == 0) ? null : freight2.get(0);
        if (aaVar != null) {
            a(aaVar);
        }
        this.n.setHasinv("0");
        g();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.BetaCheckActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public void loadOrderDataSuccess(q qVar) {
        boolean z = false;
        a();
        int status = qVar.getStatus();
        if (status == 1) {
            new ag(this).start();
            Intent intent = new Intent();
            intent.putExtra("orderResult", qVar);
            intent.setClass(this, SuccessActivity.class);
            startActivity(intent);
            finish();
        } else if (status == 18) {
            b(a(qVar.getGd_id(), qVar.getArticle_id()) + a(status));
        } else {
            if (status == 8 || status == 13 || status == 14) {
                b(a(qVar.getGd_id(), qVar.getArticle_id()) + a(status));
                return;
            }
            if (status == 15 || status == 16 || status == 17) {
                z = true;
            }
            if (z) {
                b(a(status));
            } else {
                com.zhongsou.flymall.c.b.a((Context) this, a(status));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == 11) {
            this.u = (com.zhongsou.flymall.d.a) intent.getSerializableExtra("address");
            if (g.a(this.n.getAdrid()) || this.u.getId() != ((long) Integer.valueOf(this.n.getAdrid()).intValue())) {
                f();
                return;
            }
            return;
        }
        String stringExtra = intent.getStringExtra("invoiceTitle");
        String stringExtra2 = intent.getStringExtra("invoiceContent");
        if (g.b((Object) stringExtra) && g.b((Object) stringExtra2)) {
            this.i.setVisibility(8);
            this.j.setVisibility(0);
            ((TextView) this.j.findViewById(R.id.check_order_invoice_type_content)).setText(getResources().getString(R.string.check_order_invoice_type_cont));
            ((TextView) this.j.findViewById(R.id.check_order_invoice_belongs_content)).setText(stringExtra);
            ((TextView) this.j.findViewById(R.id.check_order_invoice_content_content)).setText(stringExtra2);
            this.n.setHasinv("1");
            this.n.setInvt(stringExtra);
            this.n.setInvc(stringExtra2);
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.check_order_address_item) {
            Intent intent = new Intent(this, AddressListActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, 1);
        } else if (id == R.id.check_order_pay_item) {
            c();
        } else if (id == R.id.check_order_ship_item) {
            d();
        } else if (id != R.id.check_order_invoice_item && id != R.id.check_order_invoice_item_default) {
        } else {
            if ("1".equals(this.n.getHasinv())) {
                new AlertDialog.Builder(this).setTitle("请选择").setItems(new String[]{"取消发票", "编辑发票"}, new aj(this)).show();
                return;
            }
            e();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.beta_check);
        this.d = (ScrollView) findViewById(R.id.order_info_scrollview);
        this.e = (ListView) findViewById(R.id.check_order_product_list);
        this.f = findViewById(R.id.check_order_address_item);
        this.g = findViewById(R.id.check_order_pay_item);
        this.h = findViewById(R.id.check_order_ship_item);
        this.i = findViewById(R.id.check_order_invoice_item_default);
        this.j = findViewById(R.id.check_order_invoice_item);
        this.q = (Button) findViewById(R.id.check_order_submit);
        this.k = (Button) findViewById(R.id.head_back_btn);
        this.k.setVisibility(0);
        ((TextView) findViewById(R.id.main_head_title)).setText(getResources().getString(R.string.check_order_head_title));
        this.f.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
        this.j.setOnClickListener(this);
        this.i.setOnClickListener(this);
        this.k.setOnClickListener(new ad(this));
        this.q.setOnClickListener(new ae(this));
        this.p = getIntent().getStringArrayListExtra("cartIds");
        this.n.setUid(String.valueOf(AppContext.a().e()));
        this.n.setSession(AppContext.a().f());
        this.o = d.a(this.p, AppContext.a().e());
        this.l = new z(this, this.o);
        this.e.setAdapter((ListAdapter) this.l);
        com.zhongsou.flymall.c.b.a(this.e);
        this.c = new Handler();
        this.c.post(this.v);
        f();
        this.d.post(new ac(this));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        a();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
