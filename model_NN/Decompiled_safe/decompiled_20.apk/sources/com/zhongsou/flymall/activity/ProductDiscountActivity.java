package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.aj;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.f;
import com.zhongsou.flymall.d.g;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.e.o;
import java.util.List;

public class ProductDiscountActivity extends BaseActivity implements o {
    public int a = 1;
    g b = new fc(this);
    private GridView c;
    /* access modifiers changed from: private */
    public aj d;
    private ProgressDialog e;
    private TextView f;
    private Button g;
    private LinearLayout h;
    private boolean i = false;
    /* access modifiers changed from: private */
    public f j;
    private com.zhongsou.flymall.e.f k;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.ProductDiscountActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.e.f.a(java.lang.Integer, java.lang.Integer):com.c.a
     arg types: [int, java.lang.Integer]
     candidates:
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Object):void
      com.zhongsou.flymall.e.f.a(long, java.lang.Integer):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.Long, java.lang.Double):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.String):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.Integer, java.lang.Integer):com.c.a */
    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.i) {
            b.a((Context) this, (int) R.string.has_load_all);
        } else if (!AppContext.a().b()) {
            b.a((Context) this, (int) R.string.network_not_connected);
        } else {
            if (i2 != 0 && !this.i) {
                this.h.setVisibility(0);
            }
            this.k = new com.zhongsou.flymall.e.f(this);
            this.k.a();
            this.k.a((Integer) 10, Integer.valueOf(i2));
        }
    }

    public final void a(String str) {
        if (this.e != null) {
            this.e.dismiss();
        }
        if (this.h != null) {
            this.h.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.ProductDiscountActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public void getRebateActItemSuccess(List<z> list) {
        this.e.dismiss();
        this.h.setVisibility(8);
        if (list != null) {
            if (list.size() > 0) {
                this.d.a(list);
            } else {
                this.i = true;
                b.a((Context) this, (int) R.string.has_load_all);
            }
        }
        this.d.notifyDataSetChanged();
    }

    public void onBackPressed() {
        if (this.e != null) {
            this.e.dismiss();
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.product_common_list);
        this.e = ProgressDialog.show(this, null, getString(R.string.click_loading));
        this.f = (TextView) findViewById(R.id.main_head_title);
        this.g = (Button) findViewById(R.id.head_back_btn);
        this.f.setText((int) R.string.discount_product_text);
        this.g.setVisibility(0);
        this.g.setOnClickListener(new fd(this));
        this.h = (LinearLayout) findViewById(R.id.ll_loading_more);
        this.c = (GridView) findViewById(R.id.gv_promotion_product_list);
        this.j = new f(this.b, this, this.c);
        this.c.setOnTouchListener(this.j);
        this.d = new aj(this, false);
        this.c.setAdapter((ListAdapter) this.d);
        this.c.setOnItemClickListener(new fe(this));
        a(0);
    }
}
