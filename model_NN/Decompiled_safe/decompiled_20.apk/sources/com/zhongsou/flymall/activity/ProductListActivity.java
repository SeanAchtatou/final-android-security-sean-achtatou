package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.ab;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.d;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.ui.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public class ProductListActivity extends BaseActivity implements o {
    private AppContext a;
    private f b;
    private String c;
    /* access modifiers changed from: private */
    public Long d;
    /* access modifiers changed from: private */
    public List<d> e = null;
    /* access modifiers changed from: private */
    public String f = "sellNum desc";
    private int g = 0;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public RadioButton i;
    private RelativeLayout j;
    private TextView k;
    private ImageView l;
    private ImageView m;
    /* access modifiers changed from: private */
    public RadioButton n;
    /* access modifiers changed from: private */
    public PullToRefreshListView o;
    /* access modifiers changed from: private */
    public List<z> p = new ArrayList();
    private ab q;
    /* access modifiers changed from: private */
    public View r;
    /* access modifiers changed from: private */
    public TextView s;
    /* access modifiers changed from: private */
    public ProgressBar t;
    /* access modifiers changed from: private */
    public ProgressDialog u;

    /* access modifiers changed from: private */
    public void a() {
        this.h = 0;
        this.p.clear();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String):com.c.a
     arg types: [java.lang.String, int, java.lang.Integer, java.lang.String]
     candidates:
      com.zhongsou.flymall.e.f.a(java.lang.Object, com.zhongsou.flymall.e.m, com.c.b.d, java.lang.String):boolean
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String):com.c.a */
    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.g == 0) {
            Log.i("ProductList", "loadLvProductDataByKeyWord");
            this.b = new f(this);
            this.b.a();
            String str = this.f;
            Log.e("ProductList", "kkk" + this.c);
            this.b.a(this.c, (Integer) 10, Integer.valueOf(i2), str);
        } else if (1 == this.g) {
            Log.i("ProductList", "loadLvProductDataByCategory");
            this.b = new f(this);
            this.b.a();
            List list = this.e;
            if (this.e == null) {
                list = new ArrayList();
            }
            this.b.a(this.d, list, 10, Integer.valueOf(i2), this.f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void a(ImageView imageView, int i2) {
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), i2);
        Matrix matrix = new Matrix();
        matrix.postRotate(180.0f);
        imageView.setImageBitmap(null);
        imageView.setImageBitmap(Bitmap.createBitmap(decodeResource, 0, 0, decodeResource.getWidth(), decodeResource.getHeight(), matrix, true));
    }

    static /* synthetic */ void b(ProductListActivity productListActivity, String str) {
        if ("mobilePrice asc".equals(str)) {
            productListActivity.j.setBackgroundResource(R.drawable.tabbar_middle_click_bg);
            productListActivity.k.setTextColor(productListActivity.getResources().getColor(R.color.frame_button_text_select));
            productListActivity.a(productListActivity.l, (int) R.drawable.tab_arrow_selected);
            productListActivity.a(productListActivity.m, (int) R.drawable.tab_arrow);
            productListActivity.l.setVisibility(0);
            productListActivity.m.setVisibility(0);
        } else if ("mobilePrice desc".equals(str)) {
            productListActivity.j.setBackgroundResource(R.drawable.tabbar_middle_click_bg);
            productListActivity.k.setTextColor(productListActivity.getResources().getColor(R.color.frame_button_text_select));
            productListActivity.l.setImageDrawable(productListActivity.getResources().getDrawable(R.drawable.tab_arrow));
            productListActivity.m.setImageDrawable(productListActivity.getResources().getDrawable(R.drawable.tab_arrow_selected));
            productListActivity.l.setVisibility(0);
            productListActivity.m.setVisibility(0);
        } else {
            productListActivity.j.setBackgroundResource(R.drawable.tabbar_bg);
            productListActivity.k.setTextColor(productListActivity.getResources().getColor(R.color.text_color_ff4a4a4a));
            productListActivity.l.setVisibility(8);
            productListActivity.m.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        Log.i("ProductList", "initProductListViewData");
        a(0);
    }

    public final void a(String str) {
        if (this.u != null) {
            this.u.dismiss();
        }
        Log.i("ProductList", " onHttpError::::: comment onHttpError: " + str);
        this.s.setText((int) R.string.server_exception_error);
        this.t.setVisibility(8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.ProductListActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public void loadLvProductDataSuccess(List<z> list) {
        if (this.u != null) {
            this.u.dismiss();
        }
        Log.i("ProductList", "loadLvProductDataSuccess");
        if ((list == null || list.isEmpty()) && this.h == 0) {
            this.p.clear();
            this.q.notifyDataSetChanged();
            this.s.setText((int) R.string.load_empty);
            this.t.setVisibility(8);
            b.a((Context) this, (int) R.string.load_empty);
        }
        int size = list.size();
        if (size >= 0) {
            this.h += list.size();
            if (size > 0) {
                this.p.addAll(list);
            }
            if (size < 10) {
                this.o.setTag(3);
                this.q.notifyDataSetChanged();
                this.s.setText((int) R.string.load_full);
            } else if (size == 10) {
                this.o.setTag(1);
                this.q.notifyDataSetChanged();
                this.s.setText((int) R.string.load_more);
            }
        } else if (size == -1) {
            this.o.setTag(1);
            this.q.notifyDataSetChanged();
            this.s.setText((int) R.string.load_error);
            ((com.zhongsou.flymall.d) list).a(this);
        }
        if (this.q.getCount() == 0) {
            this.o.setTag(4);
            this.s.setText((int) R.string.load_empty);
        }
        this.t.setVisibility(8);
        this.o.a();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        Log.i("ProductList", "onActivityResult");
        if (intent.hasExtra("attrsList")) {
            this.e = (List) intent.getSerializableExtra("attrsList");
            a();
            c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.ProductListActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public void onCreate(Bundle bundle) {
        Log.i("ProductList", "onCreate");
        super.onCreate(bundle);
        setContentView((int) R.layout.product_list);
        this.a = (AppContext) getApplication();
        if (!this.a.b()) {
            b.a((Context) this, (int) R.string.network_not_connected);
        }
        Intent intent = getIntent();
        this.c = intent.getStringExtra("k");
        this.d = Long.valueOf(intent.getLongExtra("cate_id", -1));
        Log.i("ProductList", "intent[ keyword::::" + this.c + "cid::::" + this.d + " ]");
        if (this.d.longValue() == -1) {
            this.g = 0;
        } else {
            this.g = 1;
        }
        Log.i("ProductList", "initHeadView");
        Button button = (Button) findViewById(R.id.head_back_btn);
        ((TextView) findViewById(R.id.main_head_title)).setText((int) R.string.frame_title_search_result);
        button.setVisibility(0);
        button.setOnClickListener(new fh(this));
        if (this.g == 1) {
            Button button2 = (Button) findViewById(R.id.head_right_btn);
            button2.setVisibility(0);
            button2.setOnClickListener(new fi(this));
        }
        Log.i("ProductList", "initTabView");
        this.i = (RadioButton) findViewById(R.id.search_product_sales);
        this.j = (RelativeLayout) findViewById(R.id.search_product_price);
        this.k = (TextView) findViewById(R.id.search_product_price_tv);
        this.l = (ImageView) findViewById(R.id.search_product_price_image_up);
        this.m = (ImageView) findViewById(R.id.search_product_price_image_down);
        this.n = (RadioButton) findViewById(R.id.search_product_time);
        this.i.setOnClickListener(new fj(this));
        this.j.setOnClickListener(new fk(this));
        this.n.setOnClickListener(new fl(this));
        Log.i("ProductList", "initProductListView");
        this.q = new ab(this, this.p);
        this.r = getLayoutInflater().inflate((int) R.layout.listview_footer, (ViewGroup) null);
        this.s = (TextView) this.r.findViewById(R.id.listview_foot_more);
        this.t = (ProgressBar) this.r.findViewById(R.id.listview_foot_progress);
        this.o = (PullToRefreshListView) findViewById(R.id.frame_listview_product);
        this.o.setHasPullToRefresh(false);
        this.o.addFooterView(this.r);
        this.o.setAdapter((ListAdapter) this.q);
        this.o.setOnItemClickListener(new fm(this));
        this.o.setOnScrollListener(new fn(this));
        c();
    }
}
