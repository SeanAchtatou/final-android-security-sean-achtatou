package com.zhongsou.flymall.activity;

import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class dz implements View.OnClickListener {
    final /* synthetic */ MenDianMapActivity a;

    dz(MenDianMapActivity menDianMapActivity) {
        this.a = menDianMapActivity;
    }

    public final void onClick(View view) {
        if (this.a.o == null) {
            return;
        }
        if (!this.a.o.c()) {
            this.a.e.setBackgroundResource(R.drawable.map_prev_disable);
            this.a.f.setBackgroundResource(R.drawable.btn_route_next);
            return;
        }
        this.a.e.setBackgroundResource(R.drawable.btn_route_pre);
        this.a.f.setBackgroundResource(R.drawable.btn_route_next);
    }
}
