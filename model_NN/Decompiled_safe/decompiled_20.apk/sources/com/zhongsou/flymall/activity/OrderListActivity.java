package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.b.a.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.w;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.p;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.h;
import java.util.ArrayList;
import java.util.List;

public class OrderListActivity extends BaseActivity implements o, h {
    private AppContext a;
    private int b;
    /* access modifiers changed from: private */
    public String c;
    private ListView d;
    private w e;
    private List<p> f = new ArrayList();
    private f g;
    private ProgressDialog h;
    /* access modifiers changed from: private */
    public RadioButton i;
    /* access modifiers changed from: private */
    public RadioButton j;
    /* access modifiers changed from: private */
    public boolean k;

    private void c() {
        e eVar = new e();
        eVar.put("pno", "1");
        eVar.put("ps", "10");
        eVar.put("hasp", PoiTypeDef.All);
        eVar.put("uid", Long.valueOf(this.a.e()));
        eVar.put("session", this.a.f());
        System.out.println(eVar);
        this.g = b();
        this.g.b(eVar);
        this.b = 0;
    }

    public final void a() {
        e eVar = new e();
        eVar.put("pno", "1");
        eVar.put("ps", "10");
        eVar.put("hasp", PoiTypeDef.All);
        eVar.put("isbefore", this.c);
        eVar.put("uid", Long.valueOf(this.a.e()));
        eVar.put("session", this.a.f());
        System.out.println(eVar);
        this.g = b();
        this.g.c(eVar);
        this.b = 1;
    }

    public final void a(String str) {
        if (this.h != null) {
            this.h.dismiss();
        }
        Log.i("ProductList", " onHttpError::::: comment onHttpError: " + str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.OrderListActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public final void b(String str) {
        if (!this.a.b()) {
            ((Button) findViewById(R.id.order_pay_btn)).setBackgroundResource(R.drawable.btn_gray_title_bar);
            b.a((Context) this, (int) R.string.network_not_connected);
            return;
        }
        this.g = b();
        this.g.b(str);
    }

    public void getAliPayInfoSuccess(e eVar) {
        b.getPayInfoSuccess(this, this, eVar);
    }

    public void loadUserOrderDataSuccess(List<p> list) {
        if (this.h.isShowing()) {
            this.h.dismiss();
        }
        this.e.a().clear();
        this.e.a(list);
        this.e.notifyDataSetChanged();
        if (list == null || list.size() == 0) {
            this.d.setVisibility(8);
            findViewById(R.id.order_empty_list).setVisibility(0);
            return;
        }
        this.d.setVisibility(0);
        findViewById(R.id.order_empty_list).setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        b.a(intent, (Button) null, this);
        c();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.h = ProgressDialog.show(this, null, getString(R.string.product_loading));
        this.a = AppContext.a();
        Intent intent = getIntent();
        this.b = intent.getIntExtra("order_type", 0);
        if (this.b == 0) {
            setContentView((int) R.layout.user_order_unpay);
        } else if (this.b == 1) {
            setContentView((int) R.layout.user_order_all);
        }
        this.k = intent.getBooleanExtra("isNotMember", false);
        TextView textView = (TextView) findViewById(R.id.main_head_title);
        Button button = (Button) findViewById(R.id.head_back_btn);
        if (this.b == 0) {
            textView.setText((int) R.string.order_unpay_head_title);
        } else if (this.b == 1) {
            textView.setText((int) R.string.order_all_head_title);
        }
        button.setVisibility(0);
        button.setOnClickListener(new ee(this));
        if (this.b == 0) {
            this.d = (ListView) findViewById(R.id.user_unpay_order_listview);
            this.e = new w(this, this.f, 0);
        } else if (this.b == 1) {
            this.d = (ListView) findViewById(R.id.user_all_order_listview);
            this.e = new w(this, this.f, 1);
        }
        this.d.setAdapter((ListAdapter) this.e);
        if (this.b == 1) {
            this.i = (RadioButton) findViewById(R.id.order_all_after);
            this.i.setOnClickListener(new ef(this));
            this.j = (RadioButton) findViewById(R.id.order_all_before);
            this.j.setOnClickListener(new eg(this));
        }
        this.d.setOnItemClickListener(new eh(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.b == 0) {
            c();
        } else if (this.b == 1) {
            a();
        }
    }

    public void paySuccess() {
        c();
    }
}
