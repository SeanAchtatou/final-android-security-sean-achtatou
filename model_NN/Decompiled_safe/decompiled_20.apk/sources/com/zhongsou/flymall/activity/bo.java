package com.zhongsou.flymall.activity;

import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.a.m;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.h;
import com.zhongsou.flymall.e.f;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class bo {
    /* access modifiers changed from: private */
    public MainActivity a;
    private Button b = ((Button) this.a.findViewById(R.id.go_to_the_promotions_btn));
    private Button c;
    private Button d;
    private CheckBox e;
    private ListView f;
    private TextView g;
    private m h;

    public bo(MainActivity mainActivity) {
        this.a = mainActivity;
        this.b.setOnClickListener(new bq(this));
        this.c = (Button) this.a.findViewById(R.id.cart_del_button);
        this.c.setOnClickListener(new br(this));
        this.d = (Button) this.a.findViewById(R.id.go_to_the_checkout);
        this.d.setOnClickListener(new bu(this));
        this.e = (CheckBox) this.a.findViewById(R.id.cart_goods_item_checkbox_all);
        this.e.setChecked(!e());
        this.e.setOnClickListener(new bv(this));
        List<a> a2 = d.a();
        this.f = (ListView) this.a.findViewById(R.id.cart_goods_list);
        this.h = new m(this.a, this, a2);
        this.f.setAdapter((ListAdapter) this.h);
        this.f.setOnItemClickListener(new bx(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    static /* synthetic */ boolean b(bo boVar) {
        for (a next : c(d.a())) {
            int c2 = d.c(next);
            if (next.getNum() != c2) {
                b.a((Context) boVar.a, next.getName() + boVar.a.getString(R.string.prompt_cart_is_full_stock) + c2);
                return false;
            }
        }
        return true;
    }

    static /* synthetic */ List c() {
        List<a> c2 = c(d.a());
        ArrayList arrayList = new ArrayList(c2.size());
        for (a cart_id : c2) {
            arrayList.add(Long.valueOf(cart_id.getCart_id()));
        }
        return arrayList;
    }

    private static List<a> c(List<a> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (a next : list) {
            if (d.d(next)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    static /* synthetic */ List d() {
        List<a> c2 = c(d.a());
        ArrayList arrayList = new ArrayList(c2.size());
        for (a next : c2) {
            if (d.e(next)) {
                arrayList.add(String.valueOf(next.getCart_id()));
            }
        }
        return arrayList;
    }

    private static boolean e() {
        for (a d2 : d.a()) {
            if (!d.d(d2)) {
                return true;
            }
        }
        return false;
    }

    public final void a() {
        this.g = (TextView) this.a.findViewById(R.id.main_head_title);
        this.g.setText((int) R.string.cart_title);
        this.a.c();
        new bp(this).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public final void a(String str) {
        Log.i("methodName", str);
        if ("getCart".equals(str)) {
            b(d.a());
            this.a.d();
            b.a((Context) this.a, (int) R.string.server_exception_error);
        }
    }

    public final void a(List<a> list) {
        this.h.a(list);
        this.h.notifyDataSetChanged();
    }

    public final void b() {
        int i = 8;
        boolean z = false;
        boolean z2 = !(d.a().size() == 0);
        int i2 = z2 ? 0 : 8;
        if (!z2) {
            i = 0;
        }
        this.a.findViewById(R.id.cart_cart_bar).setVisibility(i2);
        this.a.findViewById(R.id.cart_del_button).setVisibility(i2);
        this.a.findViewById(R.id.cart_goods_item_checkbox_all).setVisibility(i2);
        this.a.findViewById(R.id.cart_goods_count).setVisibility(i2);
        this.a.findViewById(R.id.cart_goods_count_value).setVisibility(i2);
        this.a.findViewById(R.id.cart_amount).setVisibility(i2);
        this.a.findViewById(R.id.cart_amount_value).setVisibility(i2);
        this.a.findViewById(R.id.go_to_the_checkout).setVisibility(i2);
        this.a.findViewById(R.id.cart_goods_list).setVisibility(i2);
        this.a.findViewById(R.id.ll_product_cart_empty).setVisibility(i);
        CheckBox checkBox = this.e;
        if (!e()) {
            z = true;
        }
        checkBox.setChecked(z);
        f b2 = this.a.b();
        List<a> a2 = d.a();
        ArrayList arrayList = new ArrayList(a2.size());
        for (a next : a2) {
            h hVar = new h();
            hVar.setGd_id(next.getGd_id());
            hVar.setArticle_id(next.getArticle_id());
            hVar.setPrice(next.getPrice());
            hVar.setInvalidation(next.getInvalidation());
            hVar.setStock(next.getStock());
            hVar.setAct_id(next.getAct_id());
            hVar.setAct_type(next.getAct_type());
            hVar.setAct_stock(next.getAct_stock());
            arrayList.add(hVar);
        }
        b2.a(arrayList);
    }

    public final void b(List<a> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (a next : list) {
            if (d.d(next) && d.e(next)) {
                arrayList.add(next);
            }
        }
        BigDecimal bigDecimal = new BigDecimal(0);
        Iterator it = arrayList.iterator();
        int i = 0;
        while (true) {
            BigDecimal bigDecimal2 = bigDecimal;
            if (it.hasNext()) {
                a aVar = (a) it.next();
                i += aVar.getNum();
                bigDecimal = com.zhongsou.flymall.g.b.a(bigDecimal2, aVar.getPrice() * ((double) aVar.getNum()));
            } else {
                ((TextView) this.a.findViewById(R.id.cart_goods_count_value)).setText(String.valueOf(i));
                ((TextView) this.a.findViewById(R.id.cart_amount_value)).setText(this.a.getString(R.string.RMB) + com.zhongsou.flymall.g.b.a(bigDecimal2));
                return;
            }
        }
    }
}
