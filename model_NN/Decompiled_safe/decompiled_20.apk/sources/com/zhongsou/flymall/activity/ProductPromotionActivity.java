package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.aj;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.f;
import com.zhongsou.flymall.d.g;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.e.o;
import java.util.List;

public class ProductPromotionActivity extends BaseActivity implements o {
    g a = new fo(this);
    private GridView b;
    /* access modifiers changed from: private */
    public aj c;
    private ProgressDialog d;
    private TextView e;
    private Button f;
    private boolean g = false;
    private LinearLayout h;
    /* access modifiers changed from: private */
    public f i;
    private com.zhongsou.flymall.e.f j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.ProductPromotionActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String):com.c.a
     arg types: [java.lang.String, int, java.lang.Integer, java.lang.String]
     candidates:
      com.zhongsou.flymall.e.f.a(java.lang.Object, com.zhongsou.flymall.e.m, com.c.b.d, java.lang.String):boolean
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String):com.c.a */
    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.g) {
            b.a((Context) this, (int) R.string.has_load_all);
        } else if (!AppContext.a().b()) {
            b.a((Context) this, (int) R.string.network_not_connected);
        } else {
            if (i2 != 0 && !this.g) {
                this.h.setVisibility(0);
            }
            this.j = new com.zhongsou.flymall.e.f(this);
            this.j.a();
            this.j.a(PoiTypeDef.All, (Integer) 10, Integer.valueOf(i2), "sellNum desc");
        }
    }

    public final void a(String str) {
        if (this.d != null) {
            this.d.dismiss();
        }
        if (this.h != null) {
            this.h.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.ProductPromotionActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public void loadLvProductDataSuccess(List<z> list) {
        this.d.dismiss();
        this.h.setVisibility(8);
        if (list != null) {
            if (list.size() > 0) {
                this.c.a(list);
            } else {
                this.g = true;
                b.a((Context) this, (int) R.string.has_load_all);
            }
        }
        this.c.notifyDataSetChanged();
    }

    public void onBackPressed() {
        if (this.d != null) {
            this.d.dismiss();
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.product_common_list);
        this.d = ProgressDialog.show(this, null, getString(R.string.click_loading));
        this.e = (TextView) findViewById(R.id.main_head_title);
        this.f = (Button) findViewById(R.id.head_back_btn);
        this.e.setText((int) R.string.promotion_product_text);
        this.f.setVisibility(0);
        this.f.setOnClickListener(new fp(this));
        this.b = (GridView) findViewById(R.id.gv_promotion_product_list);
        this.h = (LinearLayout) findViewById(R.id.ll_loading_more);
        this.i = new f(this.a, this, this.b);
        this.b.setOnTouchListener(this.i);
        this.c = new aj(this, true);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setOnItemClickListener(new fq(this));
        a(0);
    }
}
