package com.zhongsou.flymall.activity;

import android.widget.AbsListView;

final class da implements AbsListView.OnScrollListener {
    final /* synthetic */ cn a;

    da(cn cnVar) {
        this.a = cnVar;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        int unused = this.a.t = i2;
        int unused2 = this.a.s = (i + i2) - 1;
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        int count = (this.a.p.getCount() - 1) + 3;
        if (i == 0 && this.a.s == count) {
            this.a.c();
        }
    }
}
