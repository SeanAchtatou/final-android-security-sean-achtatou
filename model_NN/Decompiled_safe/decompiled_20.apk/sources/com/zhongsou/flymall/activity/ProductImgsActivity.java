package com.zhongsou.flymall.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.g.g;
import com.zhongsou.flymall.ui.photoview.PhotoViewPager;
import com.zhongsou.flymall.ui.photoview.b;
import java.util.List;

public class ProductImgsActivity extends BaseActivity {
    private int a;
    /* access modifiers changed from: private */
    public List<String> b;
    /* access modifiers changed from: private */
    public TextView c;
    private Button d;
    private PhotoViewPager e;

    private static String a(String str) {
        if (g.a(str)) {
            return PoiTypeDef.All;
        }
        try {
            String[] split = str.split("\\.");
            str = split[0] + "_2." + split[1];
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return !str.startsWith("http://") ? AppContext.a().getString(R.string.IMAGE_DOMAIN) + str : str;
    }

    private void a() {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            this.b.set(i, a(this.b.get(i)));
        }
        b bVar = new b(this, this.b);
        bVar.a(new fg(this));
        this.e = (PhotoViewPager) findViewById(R.id.viewer);
        this.e.setOffscreenPageLimit(3);
        this.e.setAdapter(bVar);
        this.e.setCurrentItem(this.a);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.product_imgs_gallery);
        Intent intent = getIntent();
        this.a = intent.getIntExtra("position", 0);
        this.b = intent.getStringArrayListExtra("imgs");
        if (this.b != null && this.b.size() != 0) {
            this.c = (TextView) findViewById(R.id.main_head_title);
            this.c.setText("1/" + this.b.size());
            this.d = (Button) findViewById(R.id.head_back_btn);
            this.d.setVisibility(0);
            this.d.setOnClickListener(new ff(this));
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
