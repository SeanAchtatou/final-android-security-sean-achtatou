package com.zhongsou.flymall.activity;

import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.r;
import com.zhongsou.flymall.d.i;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public final class by implements o {
    /* access modifiers changed from: private */
    public MainActivity a;
    private f b;
    private AppContext c;
    private TextView d;
    private Button e;
    /* access modifiers changed from: private */
    public long f = -1;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public List<i> h = new ArrayList();
    /* access modifiers changed from: private */
    public LinkedList<i> i = new LinkedList<>();
    private ListView j;
    private r k;

    public by(MainActivity mainActivity) {
        this.a = mainActivity;
        this.b = new f(this);
        this.c = AppContext.a();
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        this.a.c();
        this.h.clear();
        this.k = new r(this.a, this.h);
        this.j.setAdapter((ListAdapter) this.k);
        this.b.a();
        this.b.c(j2);
    }

    private void e() {
        this.e.setVisibility(this.f > 0 ? 0 : 8);
        this.d.setText(this.g);
    }

    public final by a() {
        this.d = (TextView) this.a.findViewById(R.id.main_head_title);
        this.e = (Button) this.a.findViewById(R.id.head_cate_back_btn);
        this.e.setOnClickListener(new bz(this));
        this.g = this.a.getResources().getString(R.string.main_navigation_catagory);
        this.j = (ListView) this.a.findViewById(R.id.category_listView);
        this.k = new r(this.a, this.h);
        this.j.setAdapter((ListAdapter) this.k);
        this.j.setOnItemClickListener(new ca(this));
        return this;
    }

    public final void a(String str) {
    }

    public final by b() {
        if (this.h.isEmpty()) {
            a(this.f);
        }
        e();
        return this;
    }

    public final void c() {
        if (AppContext.a == 1 && this.f > 0) {
            i removeFirst = this.i.removeFirst();
            this.f = removeFirst.getId();
            this.g = removeFirst.getName();
            a(this.f);
        }
    }

    public final long d() {
        return this.f;
    }

    public final void getCategorySuccess(List<i> list) {
        this.h = list;
        this.k = new r(this.a, this.h);
        this.j.setAdapter((ListAdapter) this.k);
        e();
        this.a.d();
    }
}
