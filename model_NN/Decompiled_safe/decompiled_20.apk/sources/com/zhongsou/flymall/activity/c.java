package com.zhongsou.flymall.activity;

import android.content.Intent;
import android.view.View;
import com.zhongsou.flymall.d.l;

final class c implements View.OnClickListener {
    final /* synthetic */ AboutActivity a;

    c(AboutActivity aboutActivity) {
        this.a = aboutActivity;
    }

    public final void onClick(View view) {
        l lVar = new l();
        lVar.setName(this.a.c);
        lVar.setAddress(this.a.e);
        Intent intent = new Intent();
        intent.setClass(this.a, MenDianMapActivity.class);
        intent.putExtra("mendian", lVar);
        this.a.startActivity(intent);
    }
}
