package com.zhongsou.flymall.activity;

import android.content.DialogInterface;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.zhongsou.flymall.d.b;

final class r implements DialogInterface.OnClickListener {
    final /* synthetic */ q a;

    r(q qVar) {
        this.a = qVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        b bVar = (b) this.a.a.l.get(i);
        long area_id = bVar.getArea_id();
        if (area_id != this.a.a.o) {
            long unused = this.a.a.p = 0;
            this.a.a.i.setText(PoiTypeDef.All);
            this.a.a.j.setText(PoiTypeDef.All);
        }
        long unused2 = this.a.a.o = area_id;
        this.a.a.h.setText(bVar.getProvince());
        dialogInterface.dismiss();
    }
}
