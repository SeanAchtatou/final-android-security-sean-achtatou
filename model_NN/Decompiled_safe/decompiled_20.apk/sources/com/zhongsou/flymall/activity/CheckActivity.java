package com.zhongsou.flymall.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.z;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.d.aa;
import com.zhongsou.flymall.d.j;
import com.zhongsou.flymall.d.q;
import com.zhongsou.flymall.d.r;
import com.zhongsou.flymall.d.s;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.b;
import com.zhongsou.flymall.g.g;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CheckActivity extends BaseActivity implements View.OnClickListener, o {
    public static Map<Integer, String> a;
    /* access modifiers changed from: private */
    public f b;
    private Handler c;
    /* access modifiers changed from: private */
    public ScrollView d;
    private ListView e;
    private View f;
    /* access modifiers changed from: private */
    public View g;
    private View h;
    /* access modifiers changed from: private */
    public View i;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public View k;
    private Button l;
    private z m;
    private com.zhongsou.flymall.d.o n = new com.zhongsou.flymall.d.o();
    /* access modifiers changed from: private */
    public r o = new r();
    /* access modifiers changed from: private */
    public List<a> p = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> q = new ArrayList();
    private Button r;
    /* access modifiers changed from: private */
    public ProgressDialog s;
    /* access modifiers changed from: private */
    public List<aa> t = new ArrayList();
    /* access modifiers changed from: private */
    public aa u;
    private com.zhongsou.flymall.d.a v = null;
    /* access modifiers changed from: private */
    public j w = null;
    private Runnable x = new cd(this);

    static {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        a = linkedHashMap;
        linkedHashMap.put(1, "提交成功");
        a.put(100, "请填写收货地址");
        a.put(2, "收货地址不存在，请重新选择");
        a.put(3, "优惠券已失效");
        a.put(4, "请选择支付方式");
        a.put(5, "订单项错误 ");
        a.put(6, "发票信息错误");
        a.put(7, "配送信息错误");
        a.put(8, "库存不足 ");
        a.put(9, "价格错误");
        a.put(10, "总价错误");
        a.put(11, "数据库插入错误 ");
        a.put(12, "重复提交");
        a.put(13, "商品已下架");
        a.put(14, "商品已删除 ");
        a.put(15, "活动不存在");
        a.put(16, "活动已经结束");
        a.put(17, "活动未开始");
        a.put(18, "超过可以购买的数量");
        a.put(19, "订单数据错误");
        a.put(20, "您选择的优惠券不存在");
        a.put(21, "您选择的优惠券活动已结束");
    }

    static /* synthetic */ int a(r rVar) {
        if (g.a(rVar.getAdrid())) {
            return 100;
        }
        if (g.a(rVar.getPayt())) {
            return 4;
        }
        return g.a(rVar.getShipt()) ? 7 : 1;
    }

    public static String a(int i2) {
        return a.get(Integer.valueOf(i2));
    }

    private String a(long j2, long j3) {
        for (a next : this.p) {
            if (j2 == next.getGd_id() && j3 == next.getArticle_id()) {
                return next.getName() + (j3 > 0 ? " " + next.getStock_attr() : PoiTypeDef.All) + "，";
            }
        }
        return PoiTypeDef.All;
    }

    private void a() {
        if (this.s != null) {
            this.s.dismiss();
            this.s = null;
        }
    }

    /* access modifiers changed from: private */
    public void a(aa aaVar) {
        TextView textView = (TextView) this.h.findViewById(R.id.check_order_ship_content);
        if (aaVar != null) {
            textView.setText(aaVar.getName() + " ￥" + b.b(aaVar.getPrice()));
            this.o.setShipt(aaVar.getValue());
            this.o.setShipa(String.valueOf(aaVar.getPrice()));
        } else {
            textView.setText(PoiTypeDef.All);
            this.o.setShipt(null);
            this.o.setShipa(null);
        }
        h();
    }

    /* access modifiers changed from: private */
    public void a(List<j> list) {
        new AlertDialog.Builder(this).setTitle("请选择").setSingleChoiceItems(new com.zhongsou.flymall.a.a(this, list), 0, new cc(this, list)).show();
    }

    /* access modifiers changed from: private */
    public String b(int i2) {
        return i2 == 0 ? getResources().getString(R.string.pay_platform_hdfk) : i2 == 11 ? "网上支付 (" + getResources().getString(R.string.pay_platform_zfb) + ")" : i2 == 12 ? "网上支付 (" + getResources().getString(R.string.pay_platform_yl) + ")" : PoiTypeDef.All;
    }

    /* access modifiers changed from: private */
    public static String b(j jVar) {
        return jVar != null ? "￥" + b.b(jVar.getPrice()) + "(满" + b.b(jVar.getMinAmount()) + "使用)" : PoiTypeDef.All;
    }

    private void b(String str) {
        new AlertDialog.Builder(this).setTitle("提示").setMessage(str).setPositiveButton("确定", new cg(this)).show();
    }

    private void c() {
        List<s> payType = this.n.getPayType();
        if (payType != null && payType.size() != 0) {
            String[] strArr = new String[payType.size()];
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < payType.size()) {
                    strArr[i3] = b(payType.get(i3).getPlatform());
                    i2 = i3 + 1;
                } else {
                    new AlertDialog.Builder(this).setTitle("请选择").setItems(strArr, new ci(this, payType)).show();
                    return;
                }
            }
        }
    }

    private void d() {
        String[] strArr;
        int i2 = 0;
        String payt = this.o.getPayt();
        if (1 == (g.a(payt) ? 0 : Long.valueOf(payt).longValue())) {
            strArr = new String[]{this.u.getName()};
        } else {
            String[] strArr2 = new String[this.t.size()];
            while (true) {
                int i3 = i2;
                if (i3 >= this.t.size()) {
                    break;
                }
                strArr2[i3] = this.t.get(i3).getName();
                i2 = i3 + 1;
            }
            strArr = strArr2;
        }
        new AlertDialog.Builder(this).setTitle("请选择").setItems(strArr, new cj(this)).show();
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.n.getInvoices() != null) {
            String[] invoices = this.n.getInvoices();
            Intent intent = new Intent(this, InvoiceActivity.class);
            intent.putExtra("invoices", invoices);
            TextView textView = (TextView) this.j.findViewById(R.id.check_order_invoice_belongs_content);
            String obj = ((TextView) this.j.findViewById(R.id.check_order_invoice_content_content)).getText().toString();
            int length = invoices.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    i2 = 0;
                    break;
                } else if (obj.equals(invoices[i2])) {
                    break;
                } else {
                    i2++;
                }
            }
            intent.putExtra("curPos", i2);
            intent.putExtra("invoiceTitle", textView.getText().toString());
            startActivityForResult(intent, 1);
        }
    }

    private void f() {
        double d2;
        this.s = ProgressDialog.show(this, null, "正在加载");
        this.b = new f(this);
        this.b.a();
        double d3 = 0.0d;
        Iterator<a> it = this.p.iterator();
        while (true) {
            d2 = d3;
            if (!it.hasNext()) {
                break;
            }
            a next = it.next();
            d3 = d2 + (next.getPrice() * ((double) next.getNum()));
        }
        this.b.a(Long.valueOf(this.v == null ? 0 : this.v.getId()), Double.valueOf(d2));
    }

    private void g() {
        List<aa> freight = this.n.getFreight();
        if (freight != null && freight.size() != 0) {
            this.t = new ArrayList(freight.size());
            for (aa next : freight) {
                if ("4".equals(next.getValue())) {
                    this.u = next;
                } else {
                    this.t.add(next);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        double d2 = 0.0d;
        double d3 = 0.0d;
        for (a next : this.p) {
            d3 += next.getPrice() * ((double) next.getNum());
        }
        if (g.b((Object) this.o.getShipa())) {
            d3 += Double.valueOf(this.o.getShipa()).doubleValue();
        }
        double doubleValue = g.b(this.o.getCoupon_amount()) ? d3 - Double.valueOf(this.o.getCoupon_amount()).doubleValue() : d3;
        if (doubleValue > 0.0d) {
            d2 = doubleValue;
        }
        ((TextView) findViewById(R.id.check_order_anount)).setText("￥" + b.b(d2));
        this.o.setTotal(String.valueOf(Double.valueOf(new DecimalFormat("0.00").format(d2)).doubleValue()));
    }

    private s i() {
        List<s> payType = this.n.getPayType();
        if (payType == null || payType.size() == 0) {
            return null;
        }
        for (s next : payType) {
            if (next.getPlatform() == 11) {
                return next;
            }
        }
        return payType.get(0);
    }

    public final void a(String str) {
        Log.i("CheckActivity", " onHttpError::::: comment onHttpError: " + str);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.CheckActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public void loadCheckOrderDataSuccess(com.zhongsou.flymall.d.o oVar) {
        com.zhongsou.flymall.d.a aVar;
        com.zhongsou.flymall.d.a aVar2;
        j jVar;
        String str;
        this.n = oVar;
        this.o.setToken(this.n.getToken());
        List<s> payType = this.n.getPayType();
        List<aa> freight = this.n.getFreight();
        String str2 = (payType == null || payType.size() == 0) ? "支付方式为空" : (freight == null || freight.size() == 0) ? "配送方式为空" : PoiTypeDef.All;
        if (g.b((Object) str2)) {
            com.zhongsou.flymall.c.b.a((Context) this, str2);
        }
        if (this.v != null) {
            aVar = this.v;
        } else {
            List<com.zhongsou.flymall.d.a> address = this.n.getAddress();
            if (address == null || address.size() == 0) {
                aVar = null;
            } else if (address.size() == 1) {
                aVar = address.get(0);
            } else {
                Iterator<com.zhongsou.flymall.d.a> it = address.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        aVar2 = null;
                        break;
                    }
                    aVar2 = it.next();
                    if (aVar2.a()) {
                        break;
                    }
                }
                aVar = aVar2 == null ? address.get(0) : aVar2;
            }
        }
        if (aVar != null) {
            TextView textView = (TextView) this.f.findViewById(R.id.check_order_address_address);
            ((TextView) this.f.findViewById(R.id.check_order_address_username)).setText(aVar.getName());
            ((TextView) this.f.findViewById(R.id.check_order_address_tel)).setText(aVar.getMobile());
            if (aVar == null) {
                str = PoiTypeDef.All;
            } else {
                String str3 = PoiTypeDef.All;
                if (g.b((Object) aVar.getProvince())) {
                    str3 = str3 + aVar.getProvince();
                }
                if (g.b((Object) aVar.getCity())) {
                    str3 = str3 + aVar.getCity();
                }
                if (g.b((Object) aVar.getArea())) {
                    str3 = str3 + aVar.getArea();
                }
                str = str3 + aVar.getAddress();
            }
            textView.setText(str);
            this.o.setAdrid(String.valueOf(aVar.getId()));
        }
        s i2 = i();
        TextView textView2 = (TextView) this.g.findViewById(R.id.check_order_pay_content);
        if (i2 != null) {
            textView2.setText(b(i2.getPlatform()));
            this.o.setPayid(String.valueOf(i2.getPay_id()));
            this.o.setPayt(String.valueOf(i2.getPay_type()));
        } else {
            textView2.setText(PoiTypeDef.All);
            this.o.setPayid(null);
            this.o.setPayt(null);
        }
        if (this.w != null) {
            jVar = this.w;
        } else {
            List<j> coupons = this.n.getCoupons();
            jVar = (coupons == null || coupons.size() == 0) ? null : coupons.get(0);
        }
        if (jVar != null) {
            ((TextView) this.k.findViewById(R.id.check_order_coupon_content)).setText(b(jVar));
            this.o.setCouid(String.valueOf(jVar.getId()));
            this.o.setCoupon_amount(String.valueOf(jVar.getPrice()));
        }
        List<aa> freight2 = this.n.getFreight();
        a((freight2 == null || freight2.size() == 0) ? null : freight2.get(0));
        this.o.setHasinv("0");
        g();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.CheckActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public void loadOrderDataSuccess(q qVar) {
        boolean z = false;
        a();
        int status = qVar.getStatus();
        if (status == 1) {
            new ch(this).start();
            Intent intent = new Intent();
            intent.putExtra("orderResult", qVar);
            intent.setClass(this, SuccessActivity.class);
            startActivity(intent);
            finish();
            return;
        }
        if (status == 20 || status == 21) {
            List<j> coupons = this.n.getCoupons();
            int size = coupons.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    break;
                } else if (coupons.get(i2).getId() == Long.valueOf(this.o.getCouid()).longValue()) {
                    coupons.remove(i2);
                    break;
                } else {
                    i2++;
                }
            }
            com.zhongsou.flymall.c.b.a((Context) this, a(status));
        } else if (status == 18) {
            b(a(qVar.getGd_id(), qVar.getArticle_id()) + a(status));
        } else {
            if (status == 8 || status == 13 || status == 14) {
                b(a(qVar.getGd_id(), qVar.getArticle_id()) + a(status));
                return;
            }
            if (status == 15 || status == 16 || status == 17) {
                z = true;
            }
            if (z) {
                b(a(status));
            } else {
                com.zhongsou.flymall.c.b.a((Context) this, a(status));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i3 == 11) {
            this.v = (com.zhongsou.flymall.d.a) intent.getSerializableExtra("address");
            if (g.a(this.o.getAdrid()) || this.v.getId() != ((long) Integer.valueOf(this.o.getAdrid()).intValue())) {
                f();
                return;
            }
            return;
        }
        String stringExtra = intent.getStringExtra("invoiceTitle");
        String stringExtra2 = intent.getStringExtra("invoiceContent");
        if (g.b((Object) stringExtra) && g.b((Object) stringExtra2)) {
            this.i.setVisibility(8);
            this.j.setVisibility(0);
            ((TextView) this.j.findViewById(R.id.check_order_invoice_type_content)).setText(getResources().getString(R.string.check_order_invoice_type_cont));
            ((TextView) this.j.findViewById(R.id.check_order_invoice_belongs_content)).setText(stringExtra);
            ((TextView) this.j.findViewById(R.id.check_order_invoice_content_content)).setText(stringExtra2);
            this.o.setHasinv("1");
            this.o.setInvt(stringExtra);
            this.o.setInvc(stringExtra2);
        }
    }

    public void onClick(View view) {
        List<j> coupons;
        int id = view.getId();
        if (id == R.id.check_order_address_item) {
            Intent intent = new Intent(this, AddressListActivity.class);
            intent.putExtra("type", 1);
            startActivityForResult(intent, 1);
        } else if (id == R.id.check_order_pay_item) {
            c();
        } else if (id == R.id.check_order_ship_item) {
            d();
        } else if (id == R.id.check_order_invoice_item || id == R.id.check_order_invoice_item_default) {
            if ("1".equals(this.o.getHasinv())) {
                new AlertDialog.Builder(this).setTitle("请选择").setItems(new String[]{"取消发票", "编辑发票"}, new ck(this)).show();
            } else {
                e();
            }
        } else if (id == R.id.check_order_coupon_item && (coupons = this.n.getCoupons()) != null && coupons.size() != 0) {
            if (g.b((Object) this.o.getCouid())) {
                new AlertDialog.Builder(this).setTitle("请选择").setItems(new String[]{"取消选择", "修改选择"}, new cl(this, coupons)).show();
            } else {
                a(coupons);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.check);
        this.d = (ScrollView) findViewById(R.id.order_info_scrollview);
        this.e = (ListView) findViewById(R.id.check_order_product_list);
        this.f = findViewById(R.id.check_order_address_item);
        this.g = findViewById(R.id.check_order_pay_item);
        this.h = findViewById(R.id.check_order_ship_item);
        this.i = findViewById(R.id.check_order_invoice_item_default);
        this.j = findViewById(R.id.check_order_invoice_item);
        this.k = findViewById(R.id.check_order_coupon_item);
        this.r = (Button) findViewById(R.id.check_order_submit);
        this.l = (Button) findViewById(R.id.head_back_btn);
        this.l.setVisibility(0);
        ((TextView) findViewById(R.id.main_head_title)).setText(getResources().getString(R.string.check_order_head_title));
        this.f.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
        this.j.setOnClickListener(this);
        this.i.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.l.setOnClickListener(new ce(this));
        this.r.setOnClickListener(new cf(this));
        this.q = getIntent().getStringArrayListExtra("cartIds");
        this.o.setUid(String.valueOf(AppContext.a().e()));
        this.o.setSession(AppContext.a().f());
        this.p = d.a(this.q, AppContext.a().e());
        this.m = new z(this, this.p);
        this.e.setAdapter((ListAdapter) this.m);
        com.zhongsou.flymall.c.b.a(this.e);
        this.c = new Handler();
        this.c.post(this.x);
        f();
        this.d.post(new cb(this));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        a();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
