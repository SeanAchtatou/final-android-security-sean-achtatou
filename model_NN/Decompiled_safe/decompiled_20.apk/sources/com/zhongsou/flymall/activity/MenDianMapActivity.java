package com.zhongsou.flymall.activity;

import android.app.Activity;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps.AMap;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.SupportMapFragment;
import com.amap.api.maps.UiSettings;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.search.core.AMapException;
import com.amap.api.search.geocoder.Geocoder;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.amap.api.search.route.Route;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.a;
import com.zhongsou.flymall.d.l;
import java.util.List;

public class MenDianMapActivity extends FragmentActivity implements AMapLocationListener, AMap.InfoWindowAdapter, AMap.OnInfoWindowClickListener, AMap.OnMapLoadedListener, AMap.OnMarkerClickListener, LocationSource {
    /* access modifiers changed from: private */
    public l a;
    private Button b;
    private TextView c;
    /* access modifiers changed from: private */
    public LinearLayout d;
    /* access modifiers changed from: private */
    public ImageButton e;
    /* access modifiers changed from: private */
    public ImageButton f;
    /* access modifiers changed from: private */
    public Marker g;
    private LatLng h;
    /* access modifiers changed from: private */
    public LatLng i;
    /* access modifiers changed from: private */
    public AMap j;
    private UiSettings k;
    private LocationSource.OnLocationChangedListener l;
    private LocationManagerProxy m;
    /* access modifiers changed from: private */
    public Geocoder n;
    /* access modifiers changed from: private */
    public a o;
    /* access modifiers changed from: private */
    public Route p;
    /* access modifiers changed from: private */
    public int q = 10;
    /* access modifiers changed from: private */
    public List<Route> r;
    /* access modifiers changed from: private */
    public Handler s = new dx(this);
    /* access modifiers changed from: private */
    public Handler t = new ec(this);

    public final void a(String str) {
        Toast.makeText(getApplicationContext(), str, 0).show();
    }

    public void activate(LocationSource.OnLocationChangedListener onLocationChangedListener) {
        this.l = onLocationChangedListener;
        if (this.m == null) {
            this.m = LocationManagerProxy.getInstance((Activity) this);
        }
        this.m.requestLocationUpdates(LocationProviderProxy.AMapNetwork, 5000, 10.0f, this);
    }

    public void deactivate() {
        this.l = null;
        if (this.m != null) {
            this.m.removeUpdates(this);
            this.m.destory();
        }
        this.m = null;
    }

    public View getInfoContents(Marker marker) {
        return null;
    }

    public View getInfoWindow(Marker marker) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView((int) R.layout.mendian_map);
        this.a = (l) getIntent().getSerializableExtra("mendian");
        this.c = (TextView) findViewById(R.id.main_head_title);
        TextView textView = this.c;
        String name = this.a.getName();
        if (name == null || PoiTypeDef.All.equals(name)) {
            str = PoiTypeDef.All;
        } else {
            char[] charArray = name.toCharArray();
            if (charArray.length < 0) {
                str = PoiTypeDef.All;
            } else {
                int i2 = 0;
                int i3 = 0;
                for (int i4 = 0; i4 < charArray.length; i4++) {
                    int i5 = charArray[i4] / 128 == 0 ? 1 : 2;
                    if (i3 > 16 - i5) {
                        break;
                    }
                    i3 += i5;
                    i2++;
                }
                str = new String(charArray, 0, i2);
            }
        }
        textView.setText(str);
        this.b = (Button) findViewById(R.id.head_back_btn);
        this.b.setVisibility(0);
        this.d = (LinearLayout) findViewById(R.id.LinearLayoutLayout_index_bottom);
        this.e = (ImageButton) findViewById(R.id.pre_index);
        this.f = (ImageButton) findViewById(R.id.next_index);
        this.b.setOnClickListener(new dy(this));
        this.e.setOnClickListener(new dz(this));
        this.f.setOnClickListener(new ea(this));
    }

    public void onInfoWindowClick(Marker marker) {
        if (!marker.equals(this.g)) {
            return;
        }
        if (this.h == null) {
            a("正在进行定位，请稍后再尝试");
            return;
        }
        if (this.i == null) {
            try {
                List<Address> fromLocationName = this.n.getFromLocationName(this.a.getAddress(), 3);
                if (fromLocationName != null && fromLocationName.size() > 0) {
                    Address address = fromLocationName.get(0);
                    this.i = new LatLng(address.getLatitude(), address.getLongitude());
                }
            } catch (AMapException e2) {
                e2.printStackTrace();
            }
        }
        if (this.h != null && this.i != null) {
            new Thread(new eb(this, new Route.FromAndTo(com.zhongsou.flymall.g.a.a(this.h), com.zhongsou.flymall.g.a.a(this.i)))).start();
        }
    }

    public void onLocationChanged(Location location) {
    }

    public void onLocationChanged(AMapLocation aMapLocation) {
        if (this.l != null) {
            this.l.onLocationChanged(aMapLocation);
            this.h = new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude());
        }
    }

    public void onMapLoaded() {
    }

    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        deactivate();
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.j == null) {
            this.j = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if (this.j != null) {
                this.n = new Geocoder(this);
                this.k = this.j.getUiSettings();
                this.k.setScaleControlsEnabled(true);
                this.k.setCompassEnabled(true);
                this.k.setRotateGesturesEnabled(false);
                MyLocationStyle myLocationStyle = new MyLocationStyle();
                myLocationStyle.myLocationIcon(BitmapDescriptorFactory.defaultMarker(120.0f));
                myLocationStyle.strokeWidth(5.0f);
                this.j.setMyLocationStyle(myLocationStyle);
                this.j.setOnMarkerClickListener(this);
                this.j.setOnInfoWindowClickListener(this);
                this.j.setInfoWindowAdapter(this);
                this.j.setOnMapLoadedListener(this);
                this.m = LocationManagerProxy.getInstance((Activity) this);
                this.j.setLocationSource(this);
                this.j.setMyLocationEnabled(true);
                new Thread(new dw(this, this.a.getAddress())).start();
            }
        }
    }

    public void onStatusChanged(String str, int i2, Bundle bundle) {
    }
}
