package com.zhongsou.flymall.activity;

import android.view.View;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.d;
import com.zhongsou.flymall.d.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

final class fi implements View.OnClickListener {
    final /* synthetic */ ProductListActivity a;

    fi(ProductListActivity productListActivity) {
        this.a = productListActivity;
    }

    public final void onClick(View view) {
        Object obj;
        HashMap hashMap = new HashMap();
        if (!(this.a.e == null || this.a.e.size() == 0)) {
            for (d dVar : this.a.e) {
                String type = dVar.getType();
                if (hashMap.containsKey(type)) {
                    obj = (List) hashMap.get(type);
                    for (e id : dVar.getCont()) {
                        obj.add(id.getId());
                    }
                } else {
                    obj = new ArrayList();
                    for (e id2 : dVar.getCont()) {
                        obj.add(id2.getId());
                    }
                }
                hashMap.put(type, obj);
            }
        }
        b.a(this.a, this.a.d, hashMap, this.a.e);
    }
}
