package com.zhongsou.flymall.activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import com.umeng.common.b.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import java.math.BigDecimal;

public class CompanyDescActivity extends BaseActivity {
    private Button a;
    private String b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.company_desc);
        this.b = getIntent().getExtras().getString("url");
        this.a = (Button) findViewById(R.id.head_back_btn);
        this.a.setVisibility(0);
        ((TextView) findViewById(R.id.main_head_title)).setText((int) R.string.company_desc);
        this.a.setOnClickListener(new cm(this));
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        WebView webView = (WebView) findViewById(R.id.web_company_desc);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDefaultTextEncodingName(e.f);
        webView.getSettings().setJavaScriptEnabled(true);
        int intValue = (new BigDecimal(displayMetrics.widthPixels).divide(new BigDecimal((double) displayMetrics.density)).intValue() - new BigDecimal(12).multiply(new BigDecimal((double) displayMetrics.density)).intValue()) - 5;
        Log.i("desc", this.b + intValue);
        webView.loadUrl(this.b + intValue);
    }
}
