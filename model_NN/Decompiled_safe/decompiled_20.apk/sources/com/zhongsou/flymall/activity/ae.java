package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.c;
import com.zhongsou.flymall.e.f;
import java.util.ArrayList;

final class ae implements View.OnClickListener {
    final /* synthetic */ BetaCheckActivity a;

    ae(BetaCheckActivity betaCheckActivity) {
        this.a = betaCheckActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.BetaCheckActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void onClick(View view) {
        ArrayList arrayList = new ArrayList();
        for (a aVar : this.a.o) {
            c cVar = new c();
            cVar.setArtid(String.valueOf(aVar.getArticle_id()));
            cVar.setCount(String.valueOf(aVar.getNum()));
            cVar.setPrice(String.valueOf(aVar.getPrice()));
            cVar.setGdid(String.valueOf(aVar.getGd_id()));
            cVar.setActid(String.valueOf(aVar.getAct_id()));
            arrayList.add(cVar);
        }
        this.a.n.setArtis(arrayList);
        this.a.h();
        BetaCheckActivity betaCheckActivity = this.a;
        int a2 = BetaCheckActivity.a(this.a.n);
        if (a2 != 1) {
            b.a((Context) this.a, BetaCheckActivity.a(a2));
            return;
        }
        ProgressDialog unused = this.a.r = ProgressDialog.show(this.a, null, "正在提交...");
        System.out.println(com.b.a.a.a(this.a.n));
        f unused2 = this.a.b = new f(this.a);
        this.a.b.a();
        this.a.b.a(com.b.a.a.a(com.b.a.a.a(this.a.n)));
    }
}
