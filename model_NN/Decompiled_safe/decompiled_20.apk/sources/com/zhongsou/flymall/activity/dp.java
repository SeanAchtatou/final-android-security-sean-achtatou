package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.b.a.a;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.ac;
import com.zhongsou.flymall.d.y;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.g;
import java.io.Serializable;
import java.util.List;

public final class dp implements View.OnClickListener, o {
    public static boolean a = false;
    public static List<String> b = null;
    public boolean c = false;
    /* access modifiers changed from: private */
    public MainActivity d;
    private f e;
    /* access modifiers changed from: private */
    public AppContext f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public Button h;
    private Button i;
    private TableRow j;
    private TableRow k;
    private TableRow l;
    private EditText m;
    private EditText n;
    private EditText o;
    private ProgressDialog p;
    /* access modifiers changed from: private */
    public ac q;
    private RelativeLayout r;
    /* access modifiers changed from: private */
    public Button s;
    private int t;

    public dp(MainActivity mainActivity) {
        this.d = mainActivity;
        this.e = new f(this);
        this.f = AppContext.a();
        this.p = new ProgressDialog(this.d);
    }

    /* access modifiers changed from: private */
    public void d() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.d.getSystemService("input_method");
        View currentFocus = this.d.getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        Log.i("login param", a.a(this.q));
        this.e.a();
        f fVar = this.e;
        String username = this.q.getUsername();
        String password = this.q.getPassword();
        Long.valueOf(this.q.getUid());
        if (this.q.getSession() != null) {
            this.q.getSession();
        }
        fVar.d(username, password);
    }

    /* access modifiers changed from: private */
    public void f() {
        this.q = this.f.j();
        if (this.f.d()) {
            this.g.setText((int) R.string.user_center);
            LinearLayout linearLayout = (LinearLayout) this.d.findViewById(R.id.member);
            ((TextView) linearLayout.findViewById(R.id.user_name)).setText(this.q.getUname());
            ((TextView) linearLayout.findViewById(R.id.user_level)).setText(this.q.getLevel());
            this.d.findViewById(R.id.member).setVisibility(0);
            this.d.findViewById(R.id.login).setVisibility(8);
            this.d.findViewById(R.id.register).setVisibility(8);
            this.i.setVisibility(0);
            this.h.setVisibility(8);
            this.s.setVisibility(8);
            if (this.f.d()) {
                this.e.a();
                this.e.b(Long.valueOf(this.q.getUid()));
            }
            h();
            return;
        }
        g();
    }

    private void g() {
        this.d.findViewById(R.id.login).setVisibility(0);
        this.d.findViewById(R.id.register).setVisibility(8);
        this.d.findViewById(R.id.member).setVisibility(8);
        this.h.setVisibility(0);
        this.i.setVisibility(8);
        this.g.setText((int) R.string.loginActivity_login);
        this.s.setVisibility(8);
        h();
        if (g.b((Object) this.q.getUsername())) {
            ((EditText) this.d.findViewById(R.id.et_login_username)).setText(this.q.getUsername());
            ((EditText) this.d.findViewById(R.id.et_login_pwd)).setText(PoiTypeDef.All);
            ((CheckBox) this.d.findViewById(R.id.et_login_auto)).setChecked(true);
        }
    }

    private void h() {
        this.m.setText(PoiTypeDef.All);
        this.n.setText(PoiTypeDef.All);
        this.o.setText(PoiTypeDef.All);
    }

    public final dp a() {
        this.g = (TextView) this.d.findViewById(R.id.main_head_title);
        this.h = (Button) this.d.findViewById(R.id.head_login_btn);
        this.i = (Button) this.d.findViewById(R.id.head_logout_btn);
        this.s = (Button) this.d.findViewById(R.id.head_back_btn);
        this.r = (RelativeLayout) this.d.findViewById(R.id.rl_user_register);
        this.s.setOnClickListener(this);
        this.r.setOnClickListener(new dt(this));
        this.h.setOnClickListener(new du(this));
        this.i.setOnClickListener(new dv(this));
        this.m = (EditText) this.d.findViewById(R.id.et_reg_username);
        this.n = (EditText) this.d.findViewById(R.id.et_reg_pwd1);
        this.o = (EditText) this.d.findViewById(R.id.et_reg_pwd2);
        this.d.findViewById(R.id.btn_register).setOnClickListener(this);
        this.j = (TableRow) this.d.findViewById(R.id.user_unpay_order_tablerow);
        this.k = (TableRow) this.d.findViewById(R.id.user_all_order_tablerow);
        this.l = (TableRow) this.d.findViewById(R.id.user_address_tablerow);
        this.j.setOnClickListener(new dq(this));
        this.k.setOnClickListener(new dr(this));
        this.l.setOnClickListener(new ds(this));
        return this;
    }

    public final void a(String str) {
        if (this.p != null) {
            this.p.dismiss();
        }
    }

    public final dp b() {
        f();
        return this;
    }

    public final void c() {
        if (AppContext.a == 3) {
            g();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void checkLoginSuccess(ac acVar) {
        Log.i("login back", a.a(acVar));
        boolean d2 = this.f.d();
        switch (acVar.getStatus()) {
            case 0:
                acVar.setUsername(this.q.getUsername());
                acVar.setAutoLogin(this.q.a());
                this.f.a(acVar);
                d();
                if (a) {
                    b.a(this.d, "cartIds", (Serializable) b);
                    return;
                }
                f();
                if (!d2) {
                    b.a((Context) this.d, this.d.getResources().getString(R.string.loginActivity_login_success));
                    return;
                }
                return;
            case 1:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.loginActivity_account_not_exist));
                this.f.i();
                return;
            case 2:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.loginActivity_account_pwd_error));
                this.f.i();
                return;
            case 3:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.loginActivity_account_state_error));
                this.f.i();
                return;
            case 4:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.loginActivity_session_error));
                this.f.i();
                return;
            case 5:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.loginActivity_account_not_exist));
                this.f.i();
                return;
            case 6:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.loginActivity_account_not_exist));
                this.f.i();
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void checkRegisterSuccess(y yVar) {
        this.p.dismiss();
        switch (yVar.getStatus()) {
            case 1:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.register_success));
                this.q.setUsername(this.m.getText().toString().trim());
                this.q.setPassword(this.n.getText().toString().trim());
                e();
                return;
            case 2:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.username_or_password_empty));
                return;
            case 3:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.email_already_exist));
                return;
            case 4:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.email_length_error));
                return;
            case 5:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.email_format_error));
                return;
            case 6:
                d();
                b.a((Context) this.d, this.d.getResources().getString(R.string.pwd_length_error));
                return;
            default:
                return;
        }
    }

    public final void loadUnPayOrderCountSuccess(String str) {
        this.t = Integer.valueOf(str).intValue();
        TextView textView = (TextView) ((LinearLayout) this.d.findViewById(R.id.member)).findViewById(R.id.user_unpay_order_num_textview);
        if (this.t > 0) {
            textView.setText(String.valueOf(this.t));
            textView.setVisibility(0);
            return;
        }
        textView.setVisibility(8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void onClick(View view) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        switch (view.getId()) {
            case R.id.head_back_btn:
                g();
                return;
            case R.id.btn_register:
                String trim = this.m.getText().toString().trim();
                if (TextUtils.isEmpty(trim)) {
                    d();
                    b.a((Context) this.d, this.d.getResources().getString(R.string.email_no_empty));
                    z = false;
                } else if (!g.b(this.m.getText().toString().trim())) {
                    d();
                    b.a((Context) this.d, this.d.getResources().getString(R.string.email_format_error));
                    z = false;
                } else if (trim.length() > 64) {
                    d();
                    b.a((Context) this.d, this.d.getResources().getString(R.string.email_length_error));
                    z = false;
                } else {
                    z = true;
                }
                if (z) {
                    String trim2 = this.n.getText().toString().trim();
                    if (TextUtils.isEmpty(trim2)) {
                        d();
                        b.a((Context) this.d, this.d.getResources().getString(R.string.pwd_no_empty));
                        z2 = false;
                    } else if (trim2.length() <= 0 || trim2.length() > 32) {
                        d();
                        b.a((Context) this.d, this.d.getResources().getString(R.string.pwd_length_error));
                        z2 = false;
                    } else {
                        z2 = true;
                    }
                    if (z2) {
                        String trim3 = this.o.getText().toString().trim();
                        if (TextUtils.isEmpty(trim3)) {
                            d();
                            b.a((Context) this.d, this.d.getResources().getString(R.string.twice_pwd_error));
                            z3 = false;
                        } else if (!trim3.equals(this.n.getText().toString().trim())) {
                            d();
                            b.a((Context) this.d, this.d.getResources().getString(R.string.twice_pwd_error));
                            z3 = false;
                        }
                        if (z3) {
                            this.p.setMessage(this.d.getResources().getString(R.string.user_registering));
                            this.p.setCanceledOnTouchOutside(false);
                            this.p.show();
                            String trim4 = this.m.getText().toString().trim();
                            String trim5 = this.n.getText().toString().trim();
                            f fVar = new f(this);
                            fVar.a();
                            fVar.c(trim4, trim5);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
