package com.zhongsou.flymall.activity;

import android.widget.AbsListView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.g.g;

final class bb implements AbsListView.OnScrollListener {
    final /* synthetic */ BetaProductListActivity a;

    bb(BetaProductListActivity betaProductListActivity) {
        this.a = betaProductListActivity;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.a.o.onScroll(absListView, i, i2, i3);
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        boolean z;
        this.a.o.onScrollStateChanged(absListView, i);
        if (!this.a.p.isEmpty()) {
            try {
                z = absListView.getPositionForView(this.a.r) == absListView.getLastVisiblePosition();
            } catch (Exception e) {
                z = false;
            }
            if (!z || (this.a.t.getVisibility() != 0 && !this.a.getResources().getString(R.string.load_ing).equals(this.a.s.getText()))) {
                int c = g.c(this.a.o.getTag());
                if (z && c == 1) {
                    this.a.o.setTag(2);
                    this.a.s.setText((int) R.string.load_ing);
                    this.a.t.setVisibility(0);
                    this.a.a((this.a.h / 10) + 1);
                }
            }
        }
    }
}
