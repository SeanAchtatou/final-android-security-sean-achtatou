package com.zhongsou.flymall.activity;

import android.app.Dialog;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.b.a.a;

public final class bd {
    private Dialog a;
    private Button b;
    private Button c;
    /* access modifiers changed from: private */
    public EditText d = ((EditText) this.a.findViewById(R.id.cart_dialog_goods_count));
    /* access modifiers changed from: private */
    public Button e;
    private Button f;

    public bd(MainActivity mainActivity, a aVar) {
        this.a = new Dialog(mainActivity, R.style.Dialog);
        Window window = this.a.getWindow();
        window.setFlags(1024, 2048);
        window.setGravity(17);
        window.setLayout(-2, -2);
        this.a.setContentView((int) R.layout.cart_dialog);
        this.a.setTitle((int) R.string.cart_dialog_update_count_title);
        this.a.setFeatureDrawableAlpha(0, 0);
        this.d.setTag(Integer.valueOf(aVar.getNum()));
        this.d.setText(String.valueOf(aVar.getNum()));
        this.d.setSelection(this.d.length());
        this.d.addTextChangedListener(new be(this));
        this.d.setOnEditorActionListener(new bf(this));
        this.e = (Button) this.a.findViewById(R.id.cart_dialog_update_count_positive);
        this.e.setOnClickListener(new bg(this, mainActivity, aVar));
        this.f = (Button) this.a.findViewById(R.id.cart_dialog_cancle);
        this.f.setOnClickListener(new bl(this));
        this.b = (Button) this.a.findViewById(R.id.cart_dialog_sub);
        this.b.setOnClickListener(new bm(this));
        this.c = (Button) this.a.findViewById(R.id.cart_dialog_add);
        this.c.setOnClickListener(new bn(this));
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        int intValue = ((Integer) this.d.getTag()).intValue() + i;
        if (intValue <= 0) {
            intValue = 1;
        } else if (intValue > 999) {
            intValue = 999;
        }
        this.d.setText(String.valueOf(intValue));
        this.d.setSelection(this.d.length());
    }

    public final void a() {
        this.a.show();
    }

    public final void b() {
        this.a.dismiss();
    }
}
