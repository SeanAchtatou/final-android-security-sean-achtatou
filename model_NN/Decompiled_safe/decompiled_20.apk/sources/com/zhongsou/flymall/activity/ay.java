package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class ay implements View.OnClickListener {
    final /* synthetic */ BetaProductListActivity a;

    ay(BetaProductListActivity betaProductListActivity) {
        this.a = betaProductListActivity;
    }

    public final void onClick(View view) {
        ProgressDialog unused = this.a.u = ProgressDialog.show(this.a, null, this.a.getString(R.string.product_loading));
        if ("mobilePrice asc".equals(this.a.f)) {
            String unused2 = this.a.f = "mobilePrice desc";
        } else {
            String unused3 = this.a.f = "mobilePrice asc";
        }
        this.a.a();
        this.a.i.setChecked(false);
        this.a.n.setChecked(false);
        BetaProductListActivity.b(this.a, this.a.f);
        this.a.c();
    }
}
