package com.zhongsou.flymall.activity;

import android.graphics.Bitmap;
import android.util.Log;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

final class a extends Thread {
    final /* synthetic */ AboutActivity a;

    a(AboutActivity aboutActivity) {
        this.a = aboutActivity;
    }

    public final void run() {
        try {
            Bitmap a2 = this.a.a;
            String a3 = AboutActivity.h;
            String string = AppContext.a().getString(R.string.PIC_TEMP_PATH);
            File file = new File(string);
            if (!file.exists()) {
                file.mkdir();
            }
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(string + a3)));
            a2.compress(Bitmap.CompressFormat.JPEG, 80, bufferedOutputStream);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            Log.i("AboutActivity", "图片保存成功！");
        } catch (IOException e) {
            Log.i("AboutActivity", "图片保存失败！");
            e.printStackTrace();
        }
    }
}
