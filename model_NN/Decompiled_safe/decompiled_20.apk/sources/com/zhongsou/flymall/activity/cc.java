package com.zhongsou.flymall.activity;

import android.content.DialogInterface;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.j;
import java.util.List;

final class cc implements DialogInterface.OnClickListener {
    final /* synthetic */ List a;
    final /* synthetic */ CheckActivity b;

    cc(CheckActivity checkActivity, List list) {
        this.b = checkActivity;
        this.a = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        j unused = this.b.w = (j) this.a.get(i);
        CheckActivity checkActivity = this.b;
        ((TextView) this.b.k.findViewById(R.id.check_order_coupon_content)).setText(CheckActivity.b(this.b.w));
        this.b.o.setCouid(String.valueOf(this.b.w.getId()));
        this.b.o.setCoupon_amount(String.valueOf(this.b.w.getPrice()));
        this.b.h();
        dialogInterface.dismiss();
    }
}
