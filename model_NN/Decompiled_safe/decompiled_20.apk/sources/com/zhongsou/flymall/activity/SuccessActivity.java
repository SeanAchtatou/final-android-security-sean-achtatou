package com.zhongsou.flymall.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.b.a.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.q;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.h;

public class SuccessActivity extends BaseActivity implements View.OnClickListener, o, h {
    private q a;
    /* access modifiers changed from: private */
    public Button b;

    public final void a(String str) {
        Log.i("SuccessActivity", "onHttpError:::::methodName:::::" + str);
    }

    public void getAliPayInfoSuccess(e eVar) {
        b.getPayInfoSuccess(this, this, eVar);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        b.a(intent, this.b, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, int):void
     arg types: [com.zhongsou.flymall.activity.SuccessActivity, ?]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.online_pay_btn:
                if (!AppContext.a().b()) {
                    b.a((Context) this, (int) R.string.network_not_connected);
                    return;
                } else {
                    b().b(this.a.getSn());
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.success);
        Log.i("SuccessActivity", "initHeadView");
        Button button = (Button) findViewById(R.id.head_right_btn);
        ((TextView) findViewById(R.id.main_head_title)).setText((int) R.string.success_order_head_title);
        button.setText((int) R.string.btn_success);
        button.setVisibility(0);
        button.setOnClickListener(new gd(this));
        this.a = (q) getIntent().getSerializableExtra("orderResult");
        this.b = (Button) findViewById(R.id.online_pay_btn);
        ((TextView) findViewById(R.id.order_sn)).setText(this.a.getSn());
        ((TextView) findViewById(R.id.order_amount)).setText(com.zhongsou.flymall.g.b.a(Double.valueOf(this.a.getAmount()).doubleValue()));
        ((TextView) findViewById(R.id.order_pay)).setText(this.a.getPayt());
        if ("货到付款".equals(this.a.getPayt()) || this.a.getAmount() == 0.0d) {
            this.b.setVisibility(8);
        } else {
            this.b.setVisibility(0);
        }
        this.b.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void paySuccess() {
        this.b.setVisibility(8);
    }
}
