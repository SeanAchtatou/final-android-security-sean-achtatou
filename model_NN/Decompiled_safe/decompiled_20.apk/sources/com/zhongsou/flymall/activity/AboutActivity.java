package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.b.a.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.f;
import com.zhongsou.flymall.g.g;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

public class AboutActivity extends BaseActivity implements o {
    /* access modifiers changed from: private */
    public static String h = "logo.jpg";
    private static String i = (AppContext.a().getString(R.string.PIC_TEMP_PATH) + h);
    /* access modifiers changed from: private */
    public Bitmap a;
    private ProgressDialog b = null;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    private String f;
    /* access modifiers changed from: private */
    public String g;

    private void c() {
        ((TextView) findViewById(R.id.about_company_name)).setText(this.c);
        ((TextView) findViewById(R.id.about_company_address)).setText(this.e);
        ((TextView) findViewById(R.id.company_phone)).setText(this.d);
        File file = new File(i);
        if (file.exists()) {
            ((ImageView) findViewById(R.id.app_icon)).setImageBitmap(f.a(file));
        }
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
        }
    }

    public final void a(String str) {
        c();
    }

    public void getAboutUsSuccess(String str) {
        System.out.println(str);
        if (g.a(str)) {
            c();
            return;
        }
        new e();
        e a2 = e.a(str);
        this.c = a2.f("company_name");
        this.d = a2.f("company_phone");
        this.e = a2.f("company_address");
        this.f = getString(R.string.WEB_DOMAIN) + a2.f("company_logo");
        this.g = a2.f("company_desc");
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.f).openConnection();
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setRequestMethod("GET");
            byte[] a3 = httpURLConnection.getResponseCode() == 200 ? f.a(httpURLConnection.getInputStream()) : null;
            if (a3 != null) {
                this.a = BitmapFactory.decodeByteArray(a3, 0, a3.length);
                ((ImageView) findViewById(R.id.app_icon)).setImageBitmap(this.a);
            }
            new a(this).start();
        } catch (Exception e2) {
        }
        ((TextView) findViewById(R.id.about_company_name)).setText(this.c);
        ((TextView) findViewById(R.id.about_company_address)).setText(this.e);
        ((TextView) findViewById(R.id.company_phone)).setText(this.d);
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.about);
        this.b = ProgressDialog.show(this, getString(R.string.more_about), getString(R.string.click_loading), true);
        ((TextView) findViewById(R.id.main_head_title)).setText((int) R.string.more_about);
        this.c = getString(R.string.company_name);
        this.d = getString(R.string.company_phone);
        this.e = getString(R.string.company_address);
        this.g = getString(R.string.WEB_DOMAIN);
        b().e();
        Button button = (Button) findViewById(R.id.head_back_btn);
        button.setOnClickListener(new b(this));
        button.setVisibility(0);
        ((RelativeLayout) findViewById(R.id.about_company_address_layout)).setOnClickListener(new c(this));
        ((RelativeLayout) findViewById(R.id.company_phone_layout)).setOnClickListener(new d(this));
        ((RelativeLayout) findViewById(R.id.company_desc_layout)).setOnClickListener(new e(this));
    }
}
