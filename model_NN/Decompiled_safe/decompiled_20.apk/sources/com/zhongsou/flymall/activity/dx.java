package com.zhongsou.flymall.activity;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;

final class dx extends Handler {
    final /* synthetic */ MenDianMapActivity a;

    dx(MenDianMapActivity menDianMapActivity) {
        this.a = menDianMapActivity;
    }

    public final void handleMessage(Message message) {
        if (message.what == 3000) {
            Marker unused = this.a.g = this.a.j.addMarker(new MarkerOptions().position(this.a.i).title(this.a.a.getName()).snippet("点击获取路线").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            this.a.g.showInfoWindow();
            this.a.j.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(this.a.i).zoom(18.0f).bearing(BitmapDescriptorFactory.HUE_RED).tilt(45.0f).build()), 1000, null);
        } else if (message.what == 1001) {
            Toast.makeText(this.a.getApplicationContext(), "请检查网络连接是否正确？", 0).show();
        }
    }
}
