package com.zhongsou.flymall.activity;

import android.text.Editable;
import android.text.TextWatcher;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class bc implements TextWatcher {
    final /* synthetic */ BetaSearchActivity a;

    bc(BetaSearchActivity betaSearchActivity) {
        this.a = betaSearchActivity;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (charSequence.length() > 0) {
            this.a.f.setText((int) R.string.search_search);
            this.a.f.setTag(Integer.valueOf((int) R.string.search_search));
            this.a.g.setVisibility(0);
            return;
        }
        this.a.f.setText((int) R.string.search_cancel);
        this.a.f.setTag(Integer.valueOf((int) R.string.search_cancel));
        this.a.g.setVisibility(8);
    }
}
