package com.zhongsou.flymall.activity;

import android.os.Handler;
import android.os.Message;

final class dd extends Handler {
    final /* synthetic */ cn a;

    dd(cn cnVar) {
        this.a = cnVar;
    }

    public final void handleMessage(Message message) {
        int currentItem = this.a.g.getCurrentItem() + 1;
        if (currentItem >= this.a.g.getAdapter().getCount()) {
            currentItem = 0;
        }
        this.a.g.setCurrentItem(currentItem);
    }
}
