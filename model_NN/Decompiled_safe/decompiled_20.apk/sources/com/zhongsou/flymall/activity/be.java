package com.zhongsou.flymall.activity;

import android.text.Editable;
import android.text.TextWatcher;
import com.zhongsou.flymall.g.g;

final class be implements TextWatcher {
    final /* synthetic */ bd a;

    be(bd bdVar) {
        this.a = bdVar;
    }

    public final void afterTextChanged(Editable editable) {
        String obj = editable.toString();
        if (g.b((Object) obj)) {
            this.a.d.setTag(Integer.valueOf(Integer.valueOf(obj).intValue()));
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
