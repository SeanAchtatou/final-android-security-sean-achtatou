package com.zhongsou.flymall.activity;

import android.content.DialogInterface;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.zhongsou.flymall.d.b;

final class t implements DialogInterface.OnClickListener {
    final /* synthetic */ s a;

    t(s sVar) {
        this.a = sVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        b bVar = (b) this.a.a.m.get(i);
        long city_id = bVar.getCity_id();
        if (city_id != this.a.a.p) {
            this.a.a.j.setText(PoiTypeDef.All);
        }
        long unused = this.a.a.p = city_id;
        this.a.a.i.setText(bVar.getCity());
        dialogInterface.dismiss();
    }
}
