package com.zhongsou.flymall.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.b;

final class bx implements AdapterView.OnItemClickListener {
    final /* synthetic */ bo a;

    bx(bo boVar) {
        this.a = boVar;
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        b.a(this.a.a, ((Long) ((CheckBox) view.findViewById(R.id.cart_goods_item_checkbox)).getTag()).longValue());
    }
}
