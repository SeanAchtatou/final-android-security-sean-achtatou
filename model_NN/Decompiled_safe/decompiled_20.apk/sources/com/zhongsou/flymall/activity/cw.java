package com.zhongsou.flymall.activity;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import com.zhongsou.flymall.a.g;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.x;

final class cw implements AdapterView.OnItemClickListener {
    final /* synthetic */ cn a;

    cw(cn cnVar) {
        this.a = cnVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.MainActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        x xVar = ((g) view.getTag()).c;
        if (xVar.getNum() != 0) {
            b.a(this.a.c, xVar.getGd_id());
        } else {
            b.a((Context) this.a.c, "该商品已经被抢光。亲，下次要速度哦！");
        }
    }
}
