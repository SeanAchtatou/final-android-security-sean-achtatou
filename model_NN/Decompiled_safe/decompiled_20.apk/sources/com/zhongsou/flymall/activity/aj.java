package com.zhongsou.flymall.activity;

import android.content.DialogInterface;
import com.amap.api.search.poisearch.PoiTypeDef;

final class aj implements DialogInterface.OnClickListener {
    final /* synthetic */ BetaCheckActivity a;

    aj(BetaCheckActivity betaCheckActivity) {
        this.a = betaCheckActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.a.n.setHasinv("0");
            this.a.n.setInvt(PoiTypeDef.All);
            this.a.n.setInvc(PoiTypeDef.All);
            this.a.i.setVisibility(0);
            this.a.j.setVisibility(8);
        } else {
            this.a.e();
        }
        dialogInterface.dismiss();
    }
}
