package com.zhongsou.flymall.activity;

import android.view.View;
import com.zhongsou.flymall.d.d;
import com.zhongsou.flymall.d.e;

final class fz implements View.OnClickListener {
    final /* synthetic */ SearchAttrsActivity a;

    fz(SearchAttrsActivity searchAttrsActivity) {
        this.a = searchAttrsActivity;
    }

    public final void onClick(View view) {
        this.a.e.a().clear();
        for (d cont : this.a.d) {
            for (e checked : cont.getCont()) {
                checked.setChecked(false);
            }
        }
        for (int i = 0; i < this.a.e.getGroupCount(); i++) {
            this.a.f.collapseGroup(i);
        }
    }
}
