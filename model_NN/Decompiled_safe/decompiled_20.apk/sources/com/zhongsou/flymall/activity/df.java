package com.zhongsou.flymall.activity;

import android.app.Activity;
import android.view.View;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.k;

final class df implements View.OnClickListener {
    final /* synthetic */ k a;
    final /* synthetic */ de b;

    df(de deVar, k kVar) {
        this.b = deVar;
        this.a = kVar;
    }

    public final void onClick(View view) {
        switch (this.a.getTarget()) {
            case 1:
                b.a(this.b.c.c, this.a.getCateId(), (String) null);
                return;
            case 2:
                b.e(this.b.c.c);
                return;
            case 3:
                b.d((Activity) this.b.c.c);
                return;
            case 4:
                b.c((Activity) this.b.c.c);
                return;
            case 5:
                b.b((Activity) this.b.c.c);
                return;
            default:
                return;
        }
    }
}
