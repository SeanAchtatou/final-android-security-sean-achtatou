package com.zhongsou.flymall.activity;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.b.a.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.a.ad;
import com.zhongsou.flymall.a.ao;
import com.zhongsou.flymall.a.c;
import com.zhongsou.flymall.a.f;
import com.zhongsou.flymall.d.k;
import com.zhongsou.flymall.d.x;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.d;
import com.zhongsou.flymall.g.g;
import com.zhongsou.flymall.g.j;
import com.zhongsou.flymall.manager.b;
import com.zhongsou.flymall.ui.HomeListView;
import com.zhongsou.flymall.ui.a;
import java.util.ArrayList;
import java.util.List;

public final class cn implements o {
    private List<x> A;
    private int B = 1;
    private Gallery C;
    private f D;
    private final String E = "sellNum desc";
    private List<z> F;
    private int G = 1;
    private Gallery H;
    private c I;
    private List<z> J;
    private int K = 1;
    private Gallery L;
    private ao M;
    b a;
    Handler b = new dd(this);
    /* access modifiers changed from: private */
    public MainActivity c;
    private com.zhongsou.flymall.e.f d;
    private TextView e;
    private ImageButton f;
    /* access modifiers changed from: private */
    public ViewPager g;
    /* access modifiers changed from: private */
    public de h;
    private LinearLayout i;
    private int j = 0;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public a l;
    /* access modifiers changed from: private */
    public com.c.a m;
    /* access modifiers changed from: private */
    public String n;
    private HomeListView o;
    /* access modifiers changed from: private */
    public ad p;
    private final String q = "startTime desc";
    private List<z> r;
    /* access modifiers changed from: private */
    public int s = 0;
    /* access modifiers changed from: private */
    public int t;
    private View u;
    private View v;
    private LinearLayout w;
    private TextView x;
    /* access modifiers changed from: private */
    public int y = 1;
    private final String z = "sellNum desc";

    public cn(MainActivity mainActivity) {
        this.c = mainActivity;
        this.d = new com.zhongsou.flymall.e.f(this);
        this.d.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String):com.c.a
     arg types: [java.lang.String, int, java.lang.Integer, java.lang.String]
     candidates:
      com.zhongsou.flymall.e.f.a(java.lang.Object, com.zhongsou.flymall.e.m, com.c.b.d, java.lang.String):boolean
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String):com.c.a */
    /* access modifiers changed from: private */
    public void b(int i2) {
        this.d.a(PoiTypeDef.All, (Integer) 10, Integer.valueOf(i2), "startTime desc");
    }

    static /* synthetic */ void b(cn cnVar) {
        cnVar.p.a();
        cnVar.D.a();
        cnVar.I.a();
        cnVar.M.a();
        cnVar.y = 1;
        cnVar.b(1);
        cnVar.d.a(".");
        cnVar.c(1);
        cnVar.d(1);
        cnVar.e(1);
    }

    private void c(int i2) {
        this.d.b(PoiTypeDef.All, 9, Integer.valueOf(i2), "sellNum desc");
    }

    private void d(int i2) {
        this.d.c(PoiTypeDef.All, 9, Integer.valueOf(i2), "sellNum desc");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.e.f.a(java.lang.Integer, java.lang.Integer):com.c.a
     arg types: [int, java.lang.Integer]
     candidates:
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Object):void
      com.zhongsou.flymall.e.f.a(long, java.lang.Integer):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.Long, java.lang.Double):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.String):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.Integer, java.lang.Integer):com.c.a */
    private void e(int i2) {
        this.d.a((Integer) 9, Integer.valueOf(i2));
    }

    public final cn a() {
        this.l = new a(this.c);
        this.l.a(new co(this));
        this.e = (TextView) this.c.findViewById(R.id.main_head_title);
        this.f = (ImageButton) this.c.findViewById(R.id.main_head_search);
        this.f.setOnClickListener(new cp(this));
        this.n = this.c.getResources().getString(R.string.IMAGE_DOMAIN);
        this.m = new com.c.a((Activity) this.c);
        this.a = new b();
        this.o = (HomeListView) this.c.findViewById(R.id.lv_new_products_list);
        this.v = this.c.getLayoutInflater().inflate((int) R.layout.load_more, (ViewGroup) null);
        this.u = this.c.getLayoutInflater().inflate((int) R.layout.frame_home_head, (ViewGroup) null);
        this.w = (LinearLayout) this.v.findViewById(R.id.load_more_parent);
        this.x = (TextView) this.v.findViewById(R.id.btn_load_more);
        b(1);
        this.o.setOnItemClickListener(new cy(this));
        this.o.addHeaderView(this.u);
        this.o.addFooterView(this.v);
        this.r = new ArrayList();
        this.p = new ad(this.c, this.r);
        this.o.setVisibility(8);
        this.o.setAdapter((BaseAdapter) this.p);
        this.o.setonRefreshListener(new cz(this));
        this.o.setOnScrollListener(new da(this));
        this.g = (ViewPager) this.c.findViewById(R.id.ad_imgs);
        this.i = (LinearLayout) this.c.findViewById(R.id.point_linear);
        this.h = new de(this);
        this.g.setAdapter(this.h);
        this.g.setOnPageChangeListener(new db(this));
        this.d.a(".");
        ((LinearLayout) this.c.findViewById(R.id.tv_kill_pro_more)).setOnClickListener(new cq(this));
        ((LinearLayout) this.c.findViewById(R.id.tv_hot_pro_more)).setOnClickListener(new cr(this));
        ((LinearLayout) this.c.findViewById(R.id.tv_rebate_act_more)).setOnClickListener(new cs(this));
        ((LinearLayout) this.c.findViewById(R.id.tv_new_pro_more)).setOnClickListener(new ct(this));
        this.C = (Gallery) this.c.findViewById(R.id.gallery_kill_pro);
        c(this.B);
        this.C.setOnItemClickListener(new cw(this));
        this.A = new ArrayList();
        this.D = new f(this.c, this.A);
        this.C.setAdapter((SpinnerAdapter) this.D);
        this.H = (Gallery) this.c.findViewById(R.id.gallery_hot_pro);
        d(this.G);
        this.H.setOnItemClickListener(new cx(this));
        this.F = new ArrayList();
        this.I = new c(this.c, this.F);
        this.H.setAdapter((SpinnerAdapter) this.I);
        this.L = (Gallery) this.c.findViewById(R.id.gallery_rebate_act);
        e(this.K);
        this.L.setOnItemClickListener(new cv(this));
        this.J = new ArrayList();
        this.M = new ao(this.c, this.J);
        this.L.setAdapter((SpinnerAdapter) this.M);
        return this;
    }

    public final void a(int i2) {
        View childAt = this.i.getChildAt(this.j);
        View childAt2 = this.i.getChildAt(i2);
        if (childAt != null && childAt2 != null) {
            ((ImageView) childAt).setBackgroundResource(R.drawable.feature_point);
            ((ImageView) childAt2).setBackgroundResource(R.drawable.feature_point_cur);
            this.j = i2;
        }
    }

    public final void a(String str) {
        this.l.a();
    }

    public final cn b() {
        this.e.setText((int) R.string.app_name);
        this.f.setVisibility(0);
        return this;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.x.setText((int) R.string.loading);
        this.b.postDelayed(new cu(this), 1000);
    }

    public final void getAliPayInfoSuccess(e eVar) {
        com.zhongsou.flymall.c.b.getPayInfoSuccess(this.c, this.c, eVar);
    }

    public final void getHotProItemSuccess(List<z> list) {
        if (list != null && list.size() != 0) {
            this.F = list;
            for (z a2 : list) {
                this.I.a(a2);
            }
            this.H.setSelection(this.I.getCount() / 2);
            this.I.notifyDataSetChanged();
            this.c.findViewById(R.id.ll_hot_pro).setVisibility(0);
        }
    }

    public final void getKillProItemSuccess(String str) {
        j a2;
        if (!g.a(str)) {
            Log.v("HomeScrollView", "掌上秒杀resultJson:" + str);
            e a3 = com.b.a.a.a(str);
            long longValue = a3.e("start_time").longValue();
            long longValue2 = a3.e("end_time").longValue();
            long longValue3 = a3.e("now_time").longValue();
            if (longValue > longValue3) {
                a2 = d.a(longValue - longValue3);
                ((TextView) this.c.findViewById(R.id.tv_kill_pro_time_suffix)).setText("分后开始");
            } else {
                a2 = d.a(longValue2 - longValue3);
                ((TextView) this.c.findViewById(R.id.tv_kill_pro_time_suffix)).setText("分后结束");
            }
            ((TextView) this.c.findViewById(R.id.tv_kill_pro_hh)).setText(a2.a());
            ((TextView) this.c.findViewById(R.id.tv_kill_pro_mm)).setText(a2.b());
            List<x> b2 = com.b.a.a.b(a3.f("actgoods"), x.class);
            Log.v("HomeScrollView", "掌上秒杀List<ActGoods>Size:" + b2.size());
            if (!(b2 == null || b2.size() == 0)) {
                this.A = b2;
                for (x a4 : b2) {
                    this.D.a(a4);
                }
                this.C.setSelection(this.D.getCount() / 2);
                this.D.notifyDataSetChanged();
            }
            this.c.findViewById(R.id.ll_kill_pro).setVisibility(0);
        }
    }

    public final void getLoopImagesSuccess(List<k> list) {
        this.l.b();
        this.o.setVisibility(0);
        if (list != null && list.size() > 0) {
            this.k = list.size();
            this.i.removeAllViews();
            for (int i2 = 0; i2 < list.size(); i2++) {
                ImageView imageView = new ImageView(this.c);
                if (i2 == 0) {
                    imageView.setBackgroundResource(R.drawable.feature_point_cur);
                } else {
                    imageView.setBackgroundResource(R.drawable.feature_point);
                }
                this.i.addView(imageView);
            }
            this.h.a(list);
            this.g.setCurrentItem(1, false);
            this.b.sendEmptyMessageDelayed(0, 2000);
        }
    }

    public final void getRebateActItemSuccess(List<z> list) {
        if (list != null && list.size() != 0) {
            this.J = list;
            for (z a2 : list) {
                this.M.a(a2);
            }
            this.L.setSelection(this.M.getCount() / 2);
            this.M.notifyDataSetChanged();
            this.c.findViewById(R.id.ll_rebate_act).setVisibility(0);
        }
    }

    public final void loadLvProductDataSuccess(List<z> list) {
        Log.v("Huang", "List<SearchProduct>Size:" + list.size());
        Log.v("Huang", "pNo_newPro:" + this.y);
        if (list == null || list.size() == 0) {
            this.o.removeFooterView(this.v);
        } else {
            if (list.size() < 10) {
                Toast.makeText(this.c, this.c.getResources().getString(R.string.has_load_all), 0).show();
                this.y = 1;
                this.o.removeFooterView(this.v);
                this.v.setVisibility(8);
            } else {
                if (this.o.getFooterViewsCount() == 0) {
                    this.o.addFooterView(this.v);
                    this.v.setVisibility(0);
                }
                this.y++;
                Log.v("Huang", "pNo_newPro++:" + this.y);
            }
            this.r = list;
            for (z a2 : list) {
                this.p.a(a2);
            }
            this.o.setSelection((this.s - this.t) + 1);
            this.p.notifyDataSetChanged();
            this.o.a();
        }
        Log.v("Huang", "newProAdapter.getCount():" + this.p.getCount());
        this.x.setText((int) R.string.main_navigation_more);
    }
}
