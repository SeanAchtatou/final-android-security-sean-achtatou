package com.zhongsou.flymall.activity;

import android.location.Address;
import android.os.Message;
import com.amap.api.maps.model.LatLng;
import com.amap.api.search.core.AMapException;
import com.autonavi.amap.mapcore.ERROR_CODE;
import java.util.List;

final class dw implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ MenDianMapActivity b;

    dw(MenDianMapActivity menDianMapActivity, String str) {
        this.b = menDianMapActivity;
        this.a = str;
    }

    public final void run() {
        try {
            List<Address> fromLocationName = this.b.n.getFromLocationName(this.a, 3);
            if (fromLocationName != null && fromLocationName.size() > 0) {
                Address address = fromLocationName.get(0);
                LatLng unused = this.b.i = new LatLng(address.getLatitude(), address.getLongitude());
                this.b.s.sendMessage(Message.obtain(this.b.s, 3000));
            }
        } catch (AMapException e) {
            this.b.s.sendMessage(Message.obtain(this.b.s, (int) ERROR_CODE.CONN_CREATE_FALSE));
        }
    }
}
