package com.zhongsou.flymall.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.ZoomButtonsController;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.umeng.common.b.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.a.aj;
import com.zhongsou.flymall.a.as;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.d.v;
import com.zhongsou.flymall.d.w;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.e.o;
import com.zhongsou.flymall.g.b;
import com.zhongsou.flymall.g.g;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

public class BetaProductActivity extends BaseActivity implements o {
    private View A;
    private View B;
    /* access modifiers changed from: private */
    public v C;
    private ListView D;
    /* access modifiers changed from: private */
    public ImageButton E;
    private GridView F;
    /* access modifiers changed from: private */
    public aj G;
    private View H;
    private View I;
    private LinearLayout J;
    /* access modifiers changed from: private */
    public ImageButton K;
    private Handler L = new an(this);
    private f a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private TextView i;
    private TextView j;
    private TextView k;
    private Gallery l;
    /* access modifiers changed from: private */
    public ProgressDialog m;
    private FrameLayout n = null;
    private WebView o;
    private LinearLayout p;
    /* access modifiers changed from: private */
    public long q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private boolean v = false;
    private View w;
    private View[] x = new View[0];
    private ImageButton[] y = new ImageButton[0];
    private View z;

    /* access modifiers changed from: private */
    public void a(long j2) {
        this.v = false;
        if (!this.r || this.q != j2) {
            this.q = j2;
            b(getString(R.string.product_loading));
            this.a = new f(this);
            this.a.a();
            this.a.a(j2);
        } else {
            a((int) R.string.product_base_info, R.id.head_back_btn);
            a(this.w);
        }
        for (ImageButton selected : this.y) {
            selected.setSelected(false);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j2, String str, double d2) {
        a aVar = new a();
        aVar.setGd_id(this.q);
        aVar.setArticle_id(j2);
        aVar.setUser_id(AppContext.a().e());
        if (this.C.getImgs() != null && this.C.getImgs().size() > 0) {
            aVar.setImg_remote(this.C.getImgs().get(0));
        }
        aVar.setPrice(d2);
        aVar.setName(this.C.getName());
        if (this.C.a() && str != null && str.length() > 0) {
            aVar.setStock_attr(this.C.getStock_name() + ":" + str);
            c();
        }
        aVar.setAct_id(this.C.getAct_id());
        d.a(aVar);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.add_to_cart_success);
        builder.setPositiveButton((int) R.string.go_to_cart, new at(this));
        builder.setNegativeButton((int) R.string.continue_shopping, new au(this));
        builder.create().show();
    }

    private void a(View view) {
        if (view != null) {
            view.setVisibility(0);
        }
        for (View view2 : this.x) {
            if (!(view2 == null || view == view2)) {
                view2.setVisibility(8);
            }
        }
    }

    static /* synthetic */ void a(BetaProductActivity betaProductActivity, ImageButton imageButton) {
        if (imageButton != null) {
            imageButton.setSelected(true);
        }
        for (ImageButton imageButton2 : betaProductActivity.y) {
            if (!(imageButton2 == null || imageButton == imageButton2)) {
                imageButton2.setSelected(false);
            }
        }
    }

    static /* synthetic */ void b(BetaProductActivity betaProductActivity, long j2) {
        betaProductActivity.v = true;
        if (!betaProductActivity.s) {
            betaProductActivity.a = new f(betaProductActivity);
            betaProductActivity.a.a();
            betaProductActivity.a.b(j2);
            return;
        }
        betaProductActivity.a((int) R.string.product_desc, R.id.head_back_btn);
        betaProductActivity.a(betaProductActivity.n);
    }

    private void b(String str) {
        if (this.m == null) {
            this.m = new ProgressDialog(this);
            this.m.setIndeterminate(true);
            this.m.setMessage(str);
            this.m.setCancelable(true);
            this.m.setCanceledOnTouchOutside(false);
            this.m.show();
            return;
        }
        this.m.setMessage(str);
        this.m.show();
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.v) {
            a(this.q);
        } else {
            finish();
        }
    }

    static /* synthetic */ void c(BetaProductActivity betaProductActivity) {
        if (betaProductActivity.C == null) {
            return;
        }
        if (betaProductActivity.C.a()) {
            List<w> stock_vals = betaProductActivity.C.getStock_vals();
            betaProductActivity.v = true;
            if (!betaProductActivity.t) {
                betaProductActivity.a((int) R.string.select_product_base_info, R.id.head_back_btn);
                betaProductActivity.a(betaProductActivity.I);
                ((TextView) betaProductActivity.findViewById(R.id.txt_tip_attr)).setText(betaProductActivity.getString(R.string.select) + betaProductActivity.C.getStock_name() + "：");
                betaProductActivity.D = (ListView) betaProductActivity.findViewById(R.id.list_stock_attr);
                betaProductActivity.D.setOnItemClickListener(new as(betaProductActivity));
                betaProductActivity.D.setAdapter((ListAdapter) new as(betaProductActivity, stock_vals));
                betaProductActivity.t = true;
                return;
            }
            betaProductActivity.a(betaProductActivity.I);
            return;
        }
        betaProductActivity.a(betaProductActivity.C.getArticle_id(), null, betaProductActivity.C.getPrice());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.e.f.a(long, java.lang.Integer):com.c.a
     arg types: [long, int]
     candidates:
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.Object):void
      com.zhongsou.flymall.e.f.a(java.lang.Integer, java.lang.Integer):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.Long, java.lang.Double):com.c.a
      com.zhongsou.flymall.e.f.a(java.lang.String, java.lang.String):com.c.a
      com.zhongsou.flymall.e.f.a(long, java.lang.Integer):com.c.a */
    static /* synthetic */ void g(BetaProductActivity betaProductActivity) {
        betaProductActivity.v = true;
        if (!betaProductActivity.u) {
            betaProductActivity.a(betaProductActivity.H);
            betaProductActivity.a((int) R.string.product_recommend, R.id.head_back_btn);
            betaProductActivity.F = (GridView) betaProductActivity.findViewById(R.id.gv_product_list);
            betaProductActivity.J = (LinearLayout) betaProductActivity.findViewById(R.id.ll_product_recommend_empty);
            betaProductActivity.G = new aj(betaProductActivity, true);
            betaProductActivity.F.setAdapter((ListAdapter) betaProductActivity.G);
            betaProductActivity.F.setOnItemClickListener(new al(betaProductActivity));
            betaProductActivity.b(betaProductActivity.getString(R.string.product_loading));
            betaProductActivity.a = new f(betaProductActivity);
            betaProductActivity.a.a();
            betaProductActivity.a.a(betaProductActivity.q, (Integer) 10);
            return;
        }
        betaProductActivity.a((int) R.string.product_recommend, R.id.head_back_btn);
        betaProductActivity.a(betaProductActivity.H);
    }

    public final void a() {
        this.L.sendEmptyMessage(0);
    }

    public final void a(String str) {
        a();
        c();
    }

    public void getGoodsDescSuccess(String str) {
        a((int) R.string.product_desc, R.id.head_back_btn);
        a(this.n);
        this.n.setVisibility(0);
        this.o = (WebView) findViewById(R.id.web_goods_desc);
        this.p = (LinearLayout) findViewById(R.id.ll_product_desc_empty);
        this.o.getSettings().setSupportZoom(true);
        this.o.getSettings().setBuiltInZoomControls(true);
        this.o.getSettings().setDefaultTextEncodingName(e.f);
        setZoomControlGone(this.o);
        if (TextUtils.isEmpty(str)) {
            this.o.setVisibility(8);
            this.p.setVisibility(0);
        } else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            this.o.getSettings().setJavaScriptEnabled(true);
            this.o.loadUrl(getString(R.string.WEB_DOMAIN) + "/kp/module/product/showDesc?gd_id=" + this.q + "&width=" + ((new BigDecimal(displayMetrics.widthPixels).divide(new BigDecimal((double) displayMetrics.density)).intValue() - new BigDecimal(12).multiply(new BigDecimal((double) displayMetrics.density)).intValue()) - 5));
            this.o.setVisibility(0);
            this.p.setVisibility(8);
        }
        this.s = true;
    }

    public void getProductSuccess(v vVar) {
        this.r = false;
        this.s = false;
        this.t = false;
        this.u = false;
        this.v = false;
        a((int) R.string.product_base_info, R.id.head_back_btn);
        a(this.w);
        this.w.setVisibility(0);
        this.z = findViewById(R.id.goods_split_attr);
        this.A = findViewById(R.id.stock_split_attr);
        this.B = findViewById(R.id.order_act);
        this.b = (TextView) findViewById(R.id.txt_product_name);
        this.c = (TextView) findViewById(R.id.txt_product_price);
        this.d = (TextView) findViewById(R.id.txt_product_market_price);
        this.e = (TextView) findViewById(R.id.txt_split_attr);
        this.f = (TextView) findViewById(R.id.txt_split_val);
        this.g = (TextView) findViewById(R.id.txt_stock_attr);
        this.h = (TextView) findViewById(R.id.txt_stock_val);
        this.i = (TextView) findViewById(R.id.txt_product_order_act);
        this.j = (TextView) findViewById(R.id.txt_freight);
        this.k = (TextView) findViewById(R.id.txt_product_price_discount);
        this.l = (Gallery) findViewById(R.id.img_products);
        ((Button) findViewById(R.id.head_back_btn)).setOnClickListener(new ao(this));
        ((Button) findViewById(R.id.head_right_btn_ask_question)).setOnClickListener(new ap(this));
        ((Button) findViewById(R.id.btn_add_cart)).setOnClickListener(new aq(this));
        this.l.setOnItemClickListener(new ar(this));
        this.b.setText(Html.fromHtml(vVar.getTitle()));
        this.c.setText(vVar.getPriceText());
        this.d.setText(b.a(vVar.getMarket_price()));
        this.d.getPaint().setFlags(16);
        if (g.a(vVar.getSplit_name())) {
            this.z.setVisibility(8);
        } else {
            this.e.setText(vVar.getSplit_name() + "：");
            this.f.setText(vVar.getSplit_value());
        }
        if (g.a(vVar.getStock_name())) {
            this.A.setVisibility(8);
        } else {
            this.g.setText(vVar.getStock_name() + "：");
            this.h.setText(vVar.getStockValString());
        }
        if (g.a(vVar.getOrder_act())) {
            this.B.setVisibility(8);
        } else {
            this.i.setText(vVar.getOrder_act());
        }
        if (vVar.getPrice() == 0.0d || (vVar.getDiscount() > BitmapDescriptorFactory.HUE_RED && vVar.getDiscount() < 10.0f)) {
            this.k.setText(vVar.getDiscount() + "折");
            this.k.setVisibility(0);
        } else {
            this.k.setVisibility(8);
        }
        this.j.setText(vVar.getFreightStr());
        this.l.setAdapter((SpinnerAdapter) new com.zhongsou.flymall.a.e(this, vVar.getImgs()));
        this.C = vVar;
        a();
        this.r = true;
    }

    public void getSameCateTopSaleSuccess(List<z> list) {
        a();
        if (list == null || list.size() <= 0) {
            this.J.setVisibility(0);
            this.F.setVisibility(8);
        } else {
            this.J.setVisibility(8);
            this.F.setVisibility(0);
            this.G.a(list);
        }
        this.G.notifyDataSetChanged();
        this.u = true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.beta_product);
        this.q = getIntent().getLongExtra("gd_id", 0);
        a((int) R.string.product_base_info, R.id.head_back_btn);
        this.w = findViewById(R.id.product_base_info);
        this.n = (FrameLayout) findViewById(R.id.pro_goods_desc);
        this.H = findViewById(R.id.ll_product_recommend);
        this.I = findViewById(R.id.product_attr);
        this.x = new View[]{this.w, this.I, this.n, this.H};
        this.K = (ImageButton) findViewById(R.id.image_button_desc);
        this.E = (ImageButton) findViewById(R.id.image_button_recommend);
        this.y = new ImageButton[]{this.K, this.E};
        this.K.setOnClickListener(new av(this));
        this.E.setOnClickListener(new am(this));
        a(this.q);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.v) {
            return super.onKeyDown(i2, keyEvent);
        }
        a(this.q);
        return true;
    }

    public void setZoomControlGone(View view) {
        try {
            Field declaredField = WebView.class.getDeclaredField("mZoomButtonsController");
            declaredField.setAccessible(true);
            ZoomButtonsController zoomButtonsController = new ZoomButtonsController(view);
            zoomButtonsController.getZoomControls().setVisibility(8);
            try {
                declaredField.set(view, zoomButtonsController);
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            }
        } catch (SecurityException e4) {
            e4.printStackTrace();
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
        }
    }
}
