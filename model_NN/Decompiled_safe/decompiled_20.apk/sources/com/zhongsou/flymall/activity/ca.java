package com.zhongsou.flymall.activity;

import android.view.View;
import android.widget.AdapterView;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.d.i;

final class ca implements AdapterView.OnItemClickListener {
    final /* synthetic */ by a;

    ca(by byVar) {
        this.a = byVar;
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        i iVar = (i) this.a.h.get(i);
        if (iVar.a()) {
            this.a.i.addFirst(new i(this.a.f, this.a.g));
            long unused = this.a.f = iVar.getId();
            String unused2 = this.a.g = iVar.getName();
            this.a.a(this.a.f);
            return;
        }
        b.a(this.a.a, iVar.getId(), (String) null);
    }
}
