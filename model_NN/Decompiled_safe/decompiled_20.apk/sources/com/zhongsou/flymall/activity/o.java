package com.zhongsou.flymall.activity;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.c.b;
import com.zhongsou.flymall.g.g;

final class o implements View.OnClickListener {
    final /* synthetic */ AddressEditActivity a;

    o(AddressEditActivity addressEditActivity) {
        this.a = addressEditActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
     arg types: [com.zhongsou.flymall.activity.AddressEditActivity, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void */
    public final void onClick(View view) {
        EditText unused = this.a.e = (EditText) this.a.findViewById(R.id.address_add_name);
        EditText unused2 = this.a.f = (EditText) this.a.findViewById(R.id.address_add_mobile);
        EditText unused3 = this.a.g = (EditText) this.a.findViewById(R.id.address_add_code);
        EditText unused4 = this.a.h = (EditText) this.a.findViewById(R.id.address_add_province);
        EditText unused5 = this.a.i = (EditText) this.a.findViewById(R.id.address_add_city);
        EditText unused6 = this.a.j = (EditText) this.a.findViewById(R.id.address_add_area);
        EditText unused7 = this.a.k = (EditText) this.a.findViewById(R.id.address_add_address);
        this.a.d.setName(this.a.e.getText().toString());
        this.a.d.setMobile(this.a.f.getText().toString());
        this.a.d.setCode(this.a.g.getText().toString());
        this.a.d.setProvince(this.a.h.getText().toString());
        this.a.d.setCity(this.a.i.getText().toString());
        this.a.d.setArea(this.a.j.getText().toString());
        this.a.d.setAddress(this.a.k.getText().toString());
        if (g.a(this.a.e.getText().toString())) {
            b.a((Context) this.a, "请填写收货人姓名！");
        } else if (!b.a("mobile", this.a.f.getText().toString())) {
            b.a((Context) this.a, "请填写手机号码！");
        } else if (!b.a("zipcode", this.a.g.getText().toString())) {
            b.a((Context) this.a, "请填写邮政编码！");
        } else if (g.a(this.a.h.getText().toString())) {
            b.a((Context) this.a, "请选择所在省份！");
        } else if (g.a(this.a.i.getText().toString())) {
            b.a((Context) this.a, "请选择所在城市！");
        } else if (g.a(this.a.j.getText().toString())) {
            b.a((Context) this.a, "请选择所在地区！");
        } else if (g.a(this.a.k.getText().toString())) {
            b.a((Context) this.a, "请填写详细地址！");
        } else {
            AddressEditActivity.a(this.a, this.a.d);
        }
    }
}
