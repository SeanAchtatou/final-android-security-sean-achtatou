package com.zhongsou.flymall.activity;

import android.support.v4.view.ViewPager;

final class db implements ViewPager.OnPageChangeListener {
    final /* synthetic */ cn a;

    db(cn cnVar) {
        this.a = cnVar;
    }

    public final void onPageScrollStateChanged(int i) {
        if (i == 0) {
            this.a.b.sendEmptyMessageDelayed(0, 2000);
        }
    }

    public final void onPageScrolled(int i, float f, int i2) {
        this.a.b.removeMessages(0);
    }

    public final void onPageSelected(int i) {
        if (i == 0 || i == this.a.k) {
            this.a.a(this.a.k - 1);
        } else if (i == this.a.k + 1) {
            this.a.a(0);
        } else {
            this.a.a(i - 1);
        }
        if (i == 0 || i == this.a.h.getCount() - 1) {
            this.a.g.postDelayed(new dc(this, i), 100);
        }
    }
}
