package com.zhongsou.flymall.activity;

import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class ea implements View.OnClickListener {
    final /* synthetic */ MenDianMapActivity a;

    ea(MenDianMapActivity menDianMapActivity) {
        this.a = menDianMapActivity;
    }

    public final void onClick(View view) {
        if (this.a.o == null) {
            return;
        }
        if (!this.a.o.d()) {
            this.a.e.setBackgroundResource(R.drawable.btn_route_pre);
            this.a.f.setBackgroundResource(R.drawable.map_next_disable);
            return;
        }
        this.a.e.setBackgroundResource(R.drawable.btn_route_pre);
        this.a.f.setBackgroundResource(R.drawable.btn_route_next);
    }
}
