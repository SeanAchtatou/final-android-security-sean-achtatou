package com.zhongsou.flymall.activity;

import android.app.ProgressDialog;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.R;

final class fl implements View.OnClickListener {
    final /* synthetic */ ProductListActivity a;

    fl(ProductListActivity productListActivity) {
        this.a = productListActivity;
    }

    public final void onClick(View view) {
        ProgressDialog unused = this.a.u = ProgressDialog.show(this.a, null, this.a.getString(R.string.product_loading));
        String unused2 = this.a.f = "startTime desc";
        this.a.a();
        this.a.i.setChecked(false);
        ProductListActivity.b(this.a, this.a.f);
        this.a.c();
    }
}
