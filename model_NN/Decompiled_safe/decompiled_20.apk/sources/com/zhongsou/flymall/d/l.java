package com.zhongsou.flymall.d;

import java.io.Serializable;
import java.util.Date;

public final class l implements Serializable {
    private long a;
    private String b;
    private String c;
    private String d;
    private Date e;
    private String f;
    private String g;

    public final String getAddress() {
        return this.c;
    }

    public final Date getCreate_time() {
        return this.e;
    }

    public final String getDesc() {
        return this.g;
    }

    public final String getImg() {
        return this.f;
    }

    public final long getMendian_id() {
        return this.a;
    }

    public final String getName() {
        return this.b;
    }

    public final String getPhone() {
        return this.d;
    }

    public final void setAddress(String str) {
        this.c = str;
    }

    public final void setCreate_time(Date date) {
        this.e = date;
    }

    public final void setDesc(String str) {
        this.g = str;
    }

    public final void setImg(String str) {
        this.f = str;
    }

    public final void setMendian_id(long j) {
        this.a = j;
    }

    public final void setName(String str) {
        this.b = str;
    }

    public final void setPhone(String str) {
        this.d = str;
    }
}
