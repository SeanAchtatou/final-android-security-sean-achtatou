package com.zhongsou.flymall.d;

import java.io.Serializable;
import java.util.Date;

public class p implements Serializable {
    private long a;
    private long b;
    private Date c;
    private int d;
    private double e;
    private String f;
    private double g;
    private String h;

    public double getAmount() {
        return this.e;
    }

    public long getId() {
        return this.a;
    }

    public String getImg() {
        return this.h;
    }

    public int getNum() {
        return this.d;
    }

    public double getShipa() {
        return this.g;
    }

    public String getShipt() {
        return this.f;
    }

    public long getSn() {
        return this.b;
    }

    public Date getTime() {
        return this.c;
    }

    public void setAmount(double d2) {
        this.e = d2;
    }

    public void setId(long j) {
        this.a = j;
    }

    public void setImg(String str) {
        this.h = str;
    }

    public void setNum(int i) {
        this.d = i;
    }

    public void setShipa(double d2) {
        this.g = d2;
    }

    public void setShipt(String str) {
        this.f = str;
    }

    public void setSn(long j) {
        this.b = j;
    }

    public void setTime(Date date) {
        this.c = date;
    }

    public String toString() {
        return "{id:" + this.a + ";sn:" + this.b + ";time:" + this.c + ";num:" + this.d + ";amount:" + this.e + ";shipt:" + this.f + ";shipa:" + this.g + ";img:" + this.h + "}";
    }
}
