package com.zhongsou.flymall.d;

import java.io.Serializable;

public class z implements Serializable {
    private Long a;
    private String b;
    private String c;
    private double d;
    private double e;
    private String f;
    private int g;
    private String h;

    public String getActInfo() {
        return this.h;
    }

    public int getActType() {
        return this.g;
    }

    public Long getId() {
        return this.a;
    }

    public String getImg() {
        return this.c;
    }

    public double getMprice() {
        return this.e;
    }

    public String getName() {
        return this.b;
    }

    public double getPrice() {
        return this.d;
    }

    public String getPromote() {
        return this.f;
    }

    public void setActInfo(String str) {
        this.h = str;
    }

    public void setActType(int i) {
        this.g = i;
    }

    public void setId(Long l) {
        this.a = l;
    }

    public void setImg(String str) {
        this.c = str;
    }

    public void setMprice(double d2) {
        this.e = d2;
    }

    public void setName(String str) {
        this.b = str;
    }

    public void setPrice(double d2) {
        this.d = d2;
    }

    public void setPromote(String str) {
        this.f = str;
    }

    public String toString() {
        return "{id:" + this.a + ";name" + this.b + ";img" + this.c + ";price" + this.d + ";mprice" + this.e + ";promote" + this.f + ";actType" + this.g + ";actInfo" + this.h + ";}";
    }
}
