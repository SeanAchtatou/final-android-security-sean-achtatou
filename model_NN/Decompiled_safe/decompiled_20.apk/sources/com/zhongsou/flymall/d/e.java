package com.zhongsou.flymall.d;

import java.io.Serializable;

public final class e implements Serializable {
    private String a;
    private String b;
    private boolean c;

    public final String getId() {
        return this.a;
    }

    public final String getName() {
        return this.b;
    }

    public final void setChecked(boolean z) {
        this.c = z;
    }

    public final void setId(String str) {
        this.a = str;
    }

    public final void setName(String str) {
        this.b = str;
    }
}
