package com.zhongsou.flymall.d;

public class h {
    private long a = 0;
    private long b = 0;
    private double c = 0.0d;
    private int d = 0;
    private int e = 0;
    private int f = 0;
    private long g = 0;
    private int h = 0;

    public long getAct_id() {
        return this.g;
    }

    public int getAct_stock() {
        return this.f;
    }

    public int getAct_type() {
        return this.h;
    }

    public long getArticle_id() {
        return this.b;
    }

    public long getGd_id() {
        return this.a;
    }

    public int getInvalidation() {
        return this.d;
    }

    public double getPrice() {
        return this.c;
    }

    public int getStock() {
        return this.e;
    }

    public void setAct_id(long j) {
        this.g = j;
    }

    public void setAct_stock(int i) {
        this.f = i;
    }

    public void setAct_type(int i) {
        this.h = i;
    }

    public void setArticle_id(long j) {
        this.b = j;
    }

    public void setGd_id(long j) {
        this.a = j;
    }

    public void setInvalidation(int i) {
        this.d = i;
    }

    public void setPrice(double d2) {
        this.c = d2;
    }

    public void setStock(int i) {
        this.e = i;
    }
}
