package com.zhongsou.flymall.d;

import java.io.Serializable;

public class q implements Serializable {
    private String a;
    private long b;
    private String c;
    private double d;
    private int e;
    private long f;
    private long g;
    private long h;

    public static long getSerialversionuid() {
        return -5821887554030596952L;
    }

    public long getAct_id() {
        return this.h;
    }

    public double getAmount() {
        return this.d;
    }

    public long getArticle_id() {
        return this.g;
    }

    public long getGd_id() {
        return this.f;
    }

    public long getOid() {
        return this.b;
    }

    public String getPayt() {
        return this.c;
    }

    public String getSn() {
        return this.a;
    }

    public int getStatus() {
        return this.e;
    }

    public void setAct_id(long j) {
        this.h = j;
    }

    public void setAmount(double d2) {
        this.d = d2;
    }

    public void setArticle_id(long j) {
        this.g = j;
    }

    public void setGd_id(long j) {
        this.f = j;
    }

    public void setOid(long j) {
        this.b = j;
    }

    public void setPayt(String str) {
        this.c = str;
    }

    public void setSn(String str) {
        this.a = str;
    }

    public void setStatus(int i) {
        this.e = i;
    }

    public String toString() {
        return "{sn:" + this.a + ";oid:" + this.b + ";payt:" + this.c + ";amount:" + this.d + ";status:" + this.e + "}";
    }
}
