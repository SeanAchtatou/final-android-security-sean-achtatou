package com.zhongsou.flymall.d;

import com.amap.api.search.poisearch.PoiTypeDef;
import com.zhongsou.flymall.e.q;
import com.zhongsou.flymall.g.b;
import java.util.ArrayList;
import java.util.List;

public class v extends q {
    private String a = PoiTypeDef.All;
    private String b = PoiTypeDef.All;
    private long c = 0;
    private double d = 0.0d;
    private double e = 0.0d;
    private double f = 0.0d;
    private List<String> g = new ArrayList(6);
    private String h = PoiTypeDef.All;
    private String i = PoiTypeDef.All;
    private String j = PoiTypeDef.All;
    private boolean k = false;
    private long l = 0;
    private List<w> m = new ArrayList();
    private String n = PoiTypeDef.All;
    private List<aa> o;
    private float p;
    private long q;
    private int r;

    public final boolean a() {
        return this.k;
    }

    public long getAct_id() {
        return this.q;
    }

    public int getAct_type() {
        return this.r;
    }

    public long getArticle_id() {
        return this.l;
    }

    public float getDiscount() {
        return this.p;
    }

    public List<aa> getFreight() {
        return this.o;
    }

    public String getFreightStr() {
        StringBuilder sb = new StringBuilder();
        if (this.o != null && this.o.size() > 0) {
            for (aa next : this.o) {
                sb.append(next.getName()).append(b.a(next.getPrice())).append(" ");
            }
        }
        return sb.toString();
    }

    public long getGd_id() {
        return this.c;
    }

    public List<String> getImgs() {
        return this.g;
    }

    public String getMarketPriceText() {
        return b.a(this.f);
    }

    public double getMarket_price() {
        return this.f;
    }

    public double getMax_price() {
        return this.e;
    }

    public String getName() {
        return this.a;
    }

    public String getOrder_act() {
        return this.n;
    }

    public double getPrice() {
        return this.d;
    }

    public String getPriceText() {
        return this.d == this.e ? b.a(this.d) : b.a(this.d) + "~" + b.a(this.e);
    }

    public String getPword() {
        return this.b;
    }

    public String getSplit_name() {
        return this.h;
    }

    public String getSplit_value() {
        return this.i;
    }

    public String getStockValString() {
        if (this.m == null || this.m.size() == 0) {
            return PoiTypeDef.All;
        }
        StringBuilder sb = new StringBuilder();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.m.size()) {
                return sb.toString();
            }
            sb.append(this.m.get(i3).getAttr_val());
            if (i3 + 1 < this.m.size()) {
                sb.append("、");
            }
            i2 = i3 + 1;
        }
    }

    public String getStock_name() {
        return this.j;
    }

    public List<w> getStock_vals() {
        return this.m;
    }

    public String getTitle() {
        return "<span>" + this.a + "</span><span><font color=\"red\">" + this.b + "</font></span>";
    }

    public void setAct_id(long j2) {
        this.q = j2;
    }

    public void setAct_type(int i2) {
        this.r = i2;
    }

    public void setArticle_id(long j2) {
        this.l = j2;
    }

    public void setDiscount(float f2) {
        this.p = f2;
    }

    public void setFreight(List<aa> list) {
        this.o = list;
    }

    public void setGd_id(long j2) {
        this.c = j2;
    }

    public void setHasStock(boolean z) {
        this.k = z;
    }

    public void setImgs(List<String> list) {
        this.g = list;
    }

    public void setMarket_price(double d2) {
        this.f = d2;
    }

    public void setMax_price(double d2) {
        this.e = d2;
    }

    public void setName(String str) {
        this.a = str;
    }

    public void setOrder_act(String str) {
        this.n = str;
    }

    public void setPrice(double d2) {
        this.d = d2;
    }

    public void setPword(String str) {
        this.b = str;
    }

    public void setSplit_name(String str) {
        this.h = str;
    }

    public void setSplit_value(String str) {
        this.i = str;
    }

    public void setStock_name(String str) {
        this.j = str;
    }

    public void setStock_vals(List<w> list) {
        this.m = list;
    }
}
