package com.zhongsou.flymall.d;

import java.io.Serializable;

public final class aa implements Serializable {
    private String a;
    private String b;
    private double c;

    public final String getName() {
        return this.a;
    }

    public final double getPrice() {
        return this.c;
    }

    public final String getValue() {
        return this.b;
    }

    public final void setName(String str) {
        this.a = str;
    }

    public final void setPrice(double d) {
        this.c = d;
    }

    public final void setValue(String str) {
        this.b = str;
    }
}
