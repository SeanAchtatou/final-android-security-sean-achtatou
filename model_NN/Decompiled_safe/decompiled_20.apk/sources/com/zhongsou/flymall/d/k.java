package com.zhongsou.flymall.d;

import java.io.Serializable;

public class k implements Serializable {
    private String a;
    private int b;
    private long c;

    public long getCateId() {
        return this.c;
    }

    public String getImage() {
        return this.a;
    }

    public int getTarget() {
        return this.b;
    }

    public void setCateId(long j) {
        this.c = j;
    }

    public void setImage(String str) {
        this.a = str;
    }

    public void setTarget(int i) {
        this.b = i;
    }
}
