package com.zhongsou.flymall.d;

import java.io.Serializable;

public class b implements Serializable {
    private long a;
    private String b;
    private String c;
    private String d;
    private long e;
    private long f;

    public String getArea() {
        return this.d;
    }

    public long getArea_id() {
        return this.a;
    }

    public String getCity() {
        return this.c;
    }

    public long getCity_id() {
        return this.f;
    }

    public String getProvince() {
        return this.b;
    }

    public long getProvince_id() {
        return this.e;
    }

    public void setArea(String str) {
        this.d = str;
    }

    public void setArea_id(long j) {
        this.a = j;
    }

    public void setCity(String str) {
        this.c = str;
    }

    public void setCity_id(long j) {
        this.f = j;
    }

    public void setProvince(String str) {
        this.b = str;
    }

    public void setProvince_id(long j) {
        this.e = j;
    }
}
