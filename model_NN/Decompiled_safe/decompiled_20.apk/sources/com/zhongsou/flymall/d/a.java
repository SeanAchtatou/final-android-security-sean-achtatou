package com.zhongsou.flymall.d;

import java.io.Serializable;

public class a implements Serializable {
    private long a;
    private String b;
    private String c;
    private String d;
    private boolean e;
    private String f;
    private String g;
    private String h;
    private String i;

    public final boolean a() {
        return this.e;
    }

    public String getAddress() {
        return this.d;
    }

    public String getArea() {
        return this.i;
    }

    public String getCity() {
        return this.h;
    }

    public String getCode() {
        return this.f;
    }

    public long getId() {
        return this.a;
    }

    public String getMobile() {
        return this.c;
    }

    public String getName() {
        return this.b;
    }

    public String getProvince() {
        return this.g;
    }

    public void setAddress(String str) {
        this.d = str;
    }

    public void setArea(String str) {
        this.i = str;
    }

    public void setCity(String str) {
        this.h = str;
    }

    public void setCode(String str) {
        this.f = str;
    }

    public void setId(long j) {
        this.a = j;
    }

    public void setIsDefault(boolean z) {
        this.e = z;
    }

    public void setMobile(String str) {
        this.c = str;
    }

    public void setName(String str) {
        this.b = str;
    }

    public void setProvince(String str) {
        this.g = str;
    }
}
