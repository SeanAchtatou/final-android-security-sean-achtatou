package com.zhongsou.flymall.d;

import java.io.Serializable;

public final class n implements Serializable {
    private long a;
    private String b;
    private double c;
    private String d;
    private int e;
    private double f;

    public final double getAmount() {
        return this.c;
    }

    public final long getGdid() {
        return this.a;
    }

    public final String getImg() {
        return this.d;
    }

    public final double getItem_price() {
        return this.f;
    }

    public final String getName() {
        return this.b;
    }

    public final int getNum() {
        return this.e;
    }

    public final void setAmount(double d2) {
        this.c = d2;
    }

    public final void setGdid(long j) {
        this.a = j;
    }

    public final void setImg(String str) {
        this.d = str;
    }

    public final void setItem_price(double d2) {
        this.f = d2;
    }

    public final void setName(String str) {
        this.b = str;
    }

    public final void setNum(int i) {
        this.e = i;
    }
}
