package com.zhongsou.flymall.d;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.Serializable;

public class x implements Serializable {
    private long a;
    private long b;
    private String c;
    private String d = PoiTypeDef.All;
    private double e;
    private double f;
    private double g;
    private Long h;
    private Long i;
    private long j;
    private long k;

    public long getAct_gd_id() {
        return this.b;
    }

    public double getDiscount() {
        return this.g;
    }

    public Long getEnd_time() {
        return this.i;
    }

    public long getGd_id() {
        return this.a;
    }

    public String getGd_name() {
        return this.c;
    }

    public String getImage() {
        return this.d;
    }

    public double getMarketPrice() {
        return this.f;
    }

    public long getNow_time() {
        return this.k;
    }

    public long getNum() {
        return this.j;
    }

    public double getPrice() {
        return this.e;
    }

    public Long getStart_time() {
        return this.h;
    }

    public void setAct_gd_id(long j2) {
        this.b = j2;
    }

    public void setDiscount(double d2) {
        this.g = d2;
    }

    public void setEnd_time(Long l) {
        this.i = l;
    }

    public void setGd_id(long j2) {
        this.a = j2;
    }

    public void setGd_name(String str) {
        this.c = str;
    }

    public void setImage(String str) {
        this.d = str;
    }

    public void setMarketPrice(double d2) {
        this.f = d2;
    }

    public void setNow_time(long j2) {
        this.k = j2;
    }

    public void setNum(long j2) {
        this.j = j2;
    }

    public void setPrice(double d2) {
        this.e = d2;
    }

    public void setStart_time(Long l) {
        this.h = l;
    }
}
