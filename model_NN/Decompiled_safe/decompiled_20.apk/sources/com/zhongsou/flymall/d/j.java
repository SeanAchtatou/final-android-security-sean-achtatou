package com.zhongsou.flymall.d;

import java.io.Serializable;

public final class j implements Serializable {
    private long a;
    private double b;
    private double c;
    private String d;
    private String e;

    public final String getBtime() {
        return this.d;
    }

    public final String getEtime() {
        return this.e;
    }

    public final long getId() {
        return this.a;
    }

    public final double getMinAmount() {
        return this.c;
    }

    public final double getPrice() {
        return this.b;
    }

    public final void setBtime(String str) {
        this.d = str;
    }

    public final void setEtime(String str) {
        this.e = str;
    }

    public final void setId(long j) {
        this.a = j;
    }

    public final void setMinAmount(double d2) {
        this.c = d2;
    }

    public final void setPrice(double d2) {
        this.b = d2;
    }
}
