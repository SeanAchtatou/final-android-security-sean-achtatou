package com.zhongsou.flymall.d;

import java.io.Serializable;
import java.util.List;

public class d implements Serializable {
    private String a;
    private String b;
    private List<e> c;

    public List<e> getCont() {
        return this.c;
    }

    public String getName() {
        return this.a;
    }

    public String getType() {
        return this.b;
    }

    public void setCont(List<e> list) {
        this.c = list;
    }

    public void setName(String str) {
        this.a = str;
    }

    public void setType(String str) {
        this.b = str;
    }
}
