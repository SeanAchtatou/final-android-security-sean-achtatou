package com.zhongsou.flymall.d;

import java.io.Serializable;

public class t implements Serializable {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;

    public String getAnswer() {
        return this.e;
    }

    public String getLevel() {
        return this.c;
    }

    public String getName() {
        return this.b;
    }

    public String getQuestion() {
        return this.a;
    }

    public String getTime() {
        return this.d;
    }

    public void setAnswer(String str) {
        this.e = str;
    }

    public void setLevel(String str) {
        this.c = str;
    }

    public void setName(String str) {
        this.b = str;
    }

    public void setQuestion(String str) {
        this.a = str;
    }

    public void setTime(String str) {
        this.d = str;
    }
}
