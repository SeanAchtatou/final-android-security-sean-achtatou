package com.zhongsou.flymall.d;

import java.io.Serializable;

public final class c implements Serializable {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;

    public final String getActid() {
        return this.e;
    }

    public final String getArtid() {
        return this.b;
    }

    public final String getCount() {
        return this.c;
    }

    public final String getGdid() {
        return this.a;
    }

    public final String getPrice() {
        return this.d;
    }

    public final void setActid(String str) {
        this.e = str;
    }

    public final void setArtid(String str) {
        this.b = str;
    }

    public final void setCount(String str) {
        this.c = str;
    }

    public final void setGdid(String str) {
        this.a = str;
    }

    public final void setPrice(String str) {
        this.d = str;
    }
}
