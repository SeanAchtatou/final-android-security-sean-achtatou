package com.zhongsou.flymall.d;

import java.io.Serializable;

public class ac implements Serializable {
    private long a;
    private String b;
    private String c;
    private String d;
    private int e;
    private boolean f;
    private String g;
    private String h;
    private String i;

    public final boolean a() {
        return this.f;
    }

    public String getLevel() {
        return this.h;
    }

    public String getPassword() {
        return this.c;
    }

    public String getPoint() {
        return this.g;
    }

    public String getSession() {
        return this.d;
    }

    public int getStatus() {
        return this.e;
    }

    public long getUid() {
        return this.a;
    }

    public String getUname() {
        return this.i;
    }

    public String getUsername() {
        return this.b;
    }

    public void setAutoLogin(boolean z) {
        this.f = z;
    }

    public void setLevel(String str) {
        this.h = str;
    }

    public void setPassword(String str) {
        this.c = str;
    }

    public void setPoint(String str) {
        this.g = str;
    }

    public void setSession(String str) {
        this.d = str;
    }

    public void setStatus(int i2) {
        this.e = i2;
    }

    public void setUid(long j) {
        this.a = j;
    }

    public void setUname(String str) {
        this.i = str;
    }

    public void setUsername(String str) {
        this.b = str;
    }
}
