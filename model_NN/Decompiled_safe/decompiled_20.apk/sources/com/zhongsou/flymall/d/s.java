package com.zhongsou.flymall.d;

import java.io.Serializable;

public final class s implements Serializable {
    private int a;
    private long b;
    private int c;

    public final long getPay_id() {
        return this.b;
    }

    public final int getPay_type() {
        return this.a;
    }

    public final int getPlatform() {
        return this.c;
    }

    public final void setPay_id(long j) {
        this.b = j;
    }

    public final void setPay_type(int i) {
        this.a = i;
    }

    public final void setPlatform(int i) {
        this.c = i;
    }
}
