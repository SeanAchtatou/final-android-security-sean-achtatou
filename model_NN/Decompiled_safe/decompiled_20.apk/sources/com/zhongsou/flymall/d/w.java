package com.zhongsou.flymall.d;

public class w {
    private long a;
    private long b;
    private String c;
    private String d;
    private long e;
    private double f;

    public long getArticle_id() {
        return this.b;
    }

    public String getAttr_name() {
        return this.c;
    }

    public String getAttr_val() {
        return this.d;
    }

    public long getCount() {
        return this.e;
    }

    public long getGd_id() {
        return this.a;
    }

    public double getPrice() {
        return this.f;
    }

    public void setArticle_id(long j) {
        this.b = j;
    }

    public void setAttr_name(String str) {
        this.c = str;
    }

    public void setAttr_val(String str) {
        this.d = str;
    }

    public void setCount(long j) {
        this.e = j;
    }

    public void setGd_id(long j) {
        this.a = j;
    }

    public void setPrice(double d2) {
        this.f = d2;
    }
}
