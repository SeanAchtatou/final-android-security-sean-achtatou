package com.zhongsou.flymall.d;

import java.io.Serializable;

public class u implements Serializable {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;

    public String getContent() {
        return this.f;
    }

    public String getGrades() {
        return this.c;
    }

    public String getLevel() {
        return this.b;
    }

    public String getName() {
        return this.a;
    }

    public String getTime() {
        return this.d;
    }

    public String getTitle() {
        return this.e;
    }

    public void setContent(String str) {
        this.f = str;
    }

    public void setGrades(String str) {
        this.c = str;
    }

    public void setLevel(String str) {
        this.b = str;
    }

    public void setName(String str) {
        this.a = str;
    }

    public void setTime(String str) {
        this.d = str;
    }

    public void setTitle(String str) {
        this.e = str;
    }
}
