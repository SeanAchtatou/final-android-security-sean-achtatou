package com.zhongsou.flymall.d;

import java.io.Serializable;
import java.util.List;

public final class r implements Serializable {
    private List<c> a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;

    public final String getAdrid() {
        return this.b;
    }

    public final List<c> getArtis() {
        return this.a;
    }

    public final String getCouid() {
        return this.f;
    }

    public final String getCoupon_amount() {
        return this.g;
    }

    public final String getHasinv() {
        return this.i;
    }

    public final String getInvc() {
        return this.k;
    }

    public final String getInvt() {
        return this.j;
    }

    public final String getPayid() {
        return this.e;
    }

    public final String getPayt() {
        return this.c;
    }

    public final String getRemark() {
        return this.m;
    }

    public final String getSession() {
        return this.p;
    }

    public final String getShipa() {
        return this.h;
    }

    public final String getShipt() {
        return this.d;
    }

    public final String getToken() {
        return this.n;
    }

    public final String getTotal() {
        return this.l;
    }

    public final String getUid() {
        return this.o;
    }

    public final void setAdrid(String str) {
        this.b = str;
    }

    public final void setArtis(List<c> list) {
        this.a = list;
    }

    public final void setCouid(String str) {
        this.f = str;
    }

    public final void setCoupon_amount(String str) {
        this.g = str;
    }

    public final void setHasinv(String str) {
        this.i = str;
    }

    public final void setInvc(String str) {
        this.k = str;
    }

    public final void setInvt(String str) {
        this.j = str;
    }

    public final void setPayid(String str) {
        this.e = str;
    }

    public final void setPayt(String str) {
        this.c = str;
    }

    public final void setRemark(String str) {
        this.m = str;
    }

    public final void setSession(String str) {
        this.p = str;
    }

    public final void setShipa(String str) {
        this.h = str;
    }

    public final void setShipt(String str) {
        this.d = str;
    }

    public final void setToken(String str) {
        this.n = str;
    }

    public final void setTotal(String str) {
        this.l = str;
    }

    public final void setUid(String str) {
        this.o = str;
    }
}
