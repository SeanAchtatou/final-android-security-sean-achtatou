package com.zhongsou.flymall.d;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;

public final class f implements View.OnTouchListener {
    Context a;
    public int b = 1;
    private g c;
    private int d = 0;
    private int e = 0;
    private GridView f;

    public f(g gVar, Context context, GridView gridView) {
        this.c = gVar;
        this.a = context;
        this.f = gridView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.d = (int) motionEvent.getX();
                return false;
            case 1:
                this.e = (int) motionEvent.getX();
                if (this.d >= this.e) {
                    return false;
                }
                Log.e("zhongsou", "gridView====" + this.f);
                if (this.f.getLastVisiblePosition() != this.f.getCount() - 1) {
                    return false;
                }
                this.b++;
                this.c.a();
                return false;
            default:
                return false;
        }
    }
}
