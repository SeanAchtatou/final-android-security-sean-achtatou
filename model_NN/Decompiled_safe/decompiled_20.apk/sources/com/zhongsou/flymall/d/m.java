package com.zhongsou.flymall.d;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class m implements Serializable {
    private long a;
    private String b;
    private Date c;
    private int d;
    private double e;
    private String f;
    private double g;
    private int h;
    private String i;
    private boolean j;
    private String k;
    private String l;
    private int m;
    private double n;
    private double o;
    private int p;
    private List<n> q = new ArrayList();
    private a r;

    public static long getSerialversionuid() {
        return -7086049760270712439L;
    }

    public final boolean a() {
        return this.j;
    }

    public a getAddress() {
        return this.r;
    }

    public double getAmount() {
        return this.e;
    }

    public double getCoupon_amount() {
        return this.o;
    }

    public int getHas_invoice() {
        return this.m;
    }

    public long getId() {
        return this.a;
    }

    public String getImg() {
        return this.i;
    }

    public String getInvoice_content() {
        return this.l;
    }

    public String getInvoice_title() {
        return this.k;
    }

    public List<n> getItems() {
        return this.q;
    }

    public double getLower_amount() {
        return this.n;
    }

    public int getNum() {
        return this.d;
    }

    public int getPaystat() {
        return this.h;
    }

    public double getShipa() {
        return this.g;
    }

    public String getShipt() {
        return this.f;
    }

    public String getSn() {
        return this.b;
    }

    public Date getTime() {
        return this.c;
    }

    public int getUse_counpon() {
        return this.p;
    }

    public void setAddress(a aVar) {
        this.r = aVar;
    }

    public void setAmount(double d2) {
        this.e = d2;
    }

    public void setCanpay(boolean z) {
        this.j = z;
    }

    public void setCoupon_amount(double d2) {
        this.o = d2;
    }

    public void setHas_invoice(int i2) {
        this.m = i2;
    }

    public void setId(long j2) {
        this.a = j2;
    }

    public void setImg(String str) {
        this.i = str;
    }

    public void setInvoice_content(String str) {
        this.l = str;
    }

    public void setInvoice_title(String str) {
        this.k = str;
    }

    public void setItems(List<n> list) {
        this.q = list;
    }

    public void setLower_amount(double d2) {
        this.n = d2;
    }

    public void setNum(int i2) {
        this.d = i2;
    }

    public void setPaystat(int i2) {
        this.h = i2;
    }

    public void setShipa(double d2) {
        this.g = d2;
    }

    public void setShipt(String str) {
        this.f = str;
    }

    public void setSn(String str) {
        this.b = str;
    }

    public void setTime(Date date) {
        this.c = date;
    }

    public void setUse_counpon(int i2) {
        this.p = i2;
    }
}
