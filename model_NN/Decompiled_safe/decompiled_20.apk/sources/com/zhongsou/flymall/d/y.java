package com.zhongsou.flymall.d;

import java.io.Serializable;

public class y implements Serializable {
    private long a;
    private String b;
    private int c;

    public String getSession() {
        return this.b;
    }

    public int getStatus() {
        return this.c;
    }

    public long getUid() {
        return this.a;
    }

    public void setSession(String str) {
        this.b = str;
    }

    public void setStatus(int i) {
        this.c = i;
    }

    public void setUid(long j) {
        this.a = j;
    }
}
