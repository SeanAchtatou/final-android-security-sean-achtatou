package com.zhongsou.flymall.d;

import java.io.Serializable;

public class ab implements Serializable {
    private int a;
    private String b;
    private String c;
    private String d;

    public String getDownloadUrl() {
        return this.c;
    }

    public String getUpdateLog() {
        return this.d;
    }

    public int getVersionCode() {
        return this.a;
    }

    public String getVersionName() {
        return this.b;
    }

    public void setDownloadUrl(String str) {
        this.c = str;
    }

    public void setUpdateLog(String str) {
        this.d = str;
    }

    public void setVersionCode(int i) {
        this.a = i;
    }

    public void setVersionName(String str) {
        this.b = str;
    }

    public String toString() {
        return "{versionCode:" + this.a + ";versionName:" + this.b + ";downloadUrl:" + this.c + ";updateLog:" + this.d + ";}";
    }
}
