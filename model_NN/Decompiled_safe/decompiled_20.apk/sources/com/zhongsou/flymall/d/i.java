package com.zhongsou.flymall.d;

import java.io.Serializable;

public class i implements Serializable {
    private long a;
    private String b;
    private boolean c;

    public i() {
    }

    public i(long j, String str) {
        this.a = j;
        this.b = str;
    }

    public static long getSerialversionuid() {
        return 7326380210286216324L;
    }

    public final boolean a() {
        return this.c;
    }

    public long getId() {
        return this.a;
    }

    public String getName() {
        return this.b;
    }

    public void setHasChild(boolean z) {
        this.c = z;
    }

    public void setId(long j) {
        this.a = j;
    }

    public void setName(String str) {
        this.b = str;
    }
}
