package com.zhongsou.flymall.d;

import java.io.Serializable;
import java.util.List;

public class o implements Serializable {
    private List<a> a;
    private List<s> b;
    private List<aa> c;
    private String[] d;
    private List<j> e;
    private String f;

    public List<a> getAddress() {
        return this.a;
    }

    public List<j> getCoupons() {
        return this.e;
    }

    public List<aa> getFreight() {
        return this.c;
    }

    public String[] getInvoices() {
        return this.d;
    }

    public List<s> getPayType() {
        return this.b;
    }

    public String getToken() {
        return this.f;
    }

    public void setAddress(List<a> list) {
        this.a = list;
    }

    public void setCoupons(List<j> list) {
        this.e = list;
    }

    public void setFreight(List<aa> list) {
        this.c = list;
    }

    public void setInvoices(String[] strArr) {
        this.d = strArr;
    }

    public void setPayType(List<s> list) {
        this.b = list;
    }

    public void setToken(String str) {
        this.f = str;
    }
}
