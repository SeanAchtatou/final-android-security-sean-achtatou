package com.zhongsou.flymall.f;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.zhongsou.flymall.g.e;

final class g implements DialogInterface.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ Context b;
    final /* synthetic */ e c;

    g(e eVar, String str, Context context) {
        this.c = eVar;
        this.a = str;
        this.b = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        e.a("777", this.a);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.parse("file://" + this.a), "application/vnd.android.package-archive");
        this.b.startActivity(intent);
    }
}
