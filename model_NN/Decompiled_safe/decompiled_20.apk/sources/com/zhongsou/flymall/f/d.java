package com.zhongsou.flymall.f;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.alipay.android.app.IRemoteServiceCallback;

final class d extends IRemoteServiceCallback.Stub {
    final /* synthetic */ a a;

    d(a aVar) {
        this.a = aVar;
    }

    public final boolean isHideLoadingScreen() {
        return false;
    }

    public final void payEnd(boolean z, String str) {
        System.out.println(z);
    }

    public final void startActivity(String str, String str2, int i, Bundle bundle) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        if (bundle == null) {
            bundle = new Bundle();
        }
        try {
            bundle.putInt("CallingPid", i);
            intent.putExtras(bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
        intent.setClassName(str, str2);
        this.a.d.startActivity(intent);
    }
}
