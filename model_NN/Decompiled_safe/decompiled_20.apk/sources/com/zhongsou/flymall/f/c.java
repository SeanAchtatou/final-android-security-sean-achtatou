package com.zhongsou.flymall.f;

import android.os.Handler;

final class c implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ Handler b;
    final /* synthetic */ a c;

    c(a aVar, String str, Handler handler) {
        this.c = aVar;
        this.a = str;
        this.b = handler;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005e, code lost:
        r0.printStackTrace();
        r3.b.sendMessage(new android.os.Message());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r3 = this;
            com.zhongsou.flymall.f.a r0 = r3.c     // Catch:{ Exception -> 0x005d }
            java.lang.Integer r1 = r0.a     // Catch:{ Exception -> 0x005d }
            monitor-enter(r1)     // Catch:{ Exception -> 0x005d }
            com.zhongsou.flymall.f.a r0 = r3.c     // Catch:{ all -> 0x005a }
            com.alipay.android.app.IAlixPay r0 = r0.b     // Catch:{ all -> 0x005a }
            if (r0 != 0) goto L_0x0012
            com.zhongsou.flymall.f.a r0 = r3.c     // Catch:{ all -> 0x005a }
            java.lang.Integer r0 = r0.a     // Catch:{ all -> 0x005a }
            r0.wait()     // Catch:{ all -> 0x005a }
        L_0x0012:
            monitor-exit(r1)     // Catch:{ all -> 0x005a }
            com.zhongsou.flymall.f.a r0 = r3.c     // Catch:{ Exception -> 0x005d }
            com.alipay.android.app.IAlixPay r0 = r0.b     // Catch:{ Exception -> 0x005d }
            com.zhongsou.flymall.f.a r1 = r3.c     // Catch:{ Exception -> 0x005d }
            com.alipay.android.app.IRemoteServiceCallback r1 = r1.f     // Catch:{ Exception -> 0x005d }
            r0.registerCallback(r1)     // Catch:{ Exception -> 0x005d }
            com.zhongsou.flymall.f.a r0 = r3.c     // Catch:{ Exception -> 0x005d }
            com.alipay.android.app.IAlixPay r0 = r0.b     // Catch:{ Exception -> 0x005d }
            java.lang.String r1 = r3.a     // Catch:{ Exception -> 0x005d }
            java.lang.String r0 = r0.Pay(r1)     // Catch:{ Exception -> 0x005d }
            com.zhongsou.flymall.f.a r1 = r3.c     // Catch:{ Exception -> 0x005d }
            r2 = 0
            r1.c = r2     // Catch:{ Exception -> 0x005d }
            com.zhongsou.flymall.f.a r1 = r3.c     // Catch:{ Exception -> 0x005d }
            com.alipay.android.app.IAlixPay r1 = r1.b     // Catch:{ Exception -> 0x005d }
            com.zhongsou.flymall.f.a r2 = r3.c     // Catch:{ Exception -> 0x005d }
            com.alipay.android.app.IRemoteServiceCallback r2 = r2.f     // Catch:{ Exception -> 0x005d }
            r1.unregisterCallback(r2)     // Catch:{ Exception -> 0x005d }
            com.zhongsou.flymall.f.a r1 = r3.c     // Catch:{ Exception -> 0x005d }
            android.app.Activity r1 = r1.d     // Catch:{ Exception -> 0x005d }
            android.content.Context r1 = r1.getApplicationContext()     // Catch:{ Exception -> 0x005d }
            com.zhongsou.flymall.f.a r2 = r3.c     // Catch:{ Exception -> 0x005d }
            android.content.ServiceConnection r2 = r2.e     // Catch:{ Exception -> 0x005d }
            r1.unbindService(r2)     // Catch:{ Exception -> 0x005d }
            android.os.Message r1 = new android.os.Message     // Catch:{ Exception -> 0x005d }
            r1.<init>()     // Catch:{ Exception -> 0x005d }
            r1.obj = r0     // Catch:{ Exception -> 0x005d }
            android.os.Handler r0 = r3.b     // Catch:{ Exception -> 0x005d }
            r0.sendMessage(r1)     // Catch:{ Exception -> 0x005d }
        L_0x0059:
            return
        L_0x005a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ Exception -> 0x005d }
            throw r0     // Catch:{ Exception -> 0x005d }
        L_0x005d:
            r0 = move-exception
            r0.printStackTrace()
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            android.os.Handler r1 = r3.b
            r1.sendMessage(r0)
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.zhongsou.flymall.f.c.run():void");
    }
}
