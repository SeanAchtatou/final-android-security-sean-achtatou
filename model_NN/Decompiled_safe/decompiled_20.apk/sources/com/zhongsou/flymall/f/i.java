package com.zhongsou.flymall.f;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

final class i extends Handler {
    final /* synthetic */ e a;

    i(e eVar) {
        this.a = eVar;
    }

    public final void handleMessage(Message message) {
        try {
            this.a.b();
            e eVar = this.a;
            Context context = this.a.a;
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("安装提示");
            builder.setMessage("为保证您的交易安全，需要您安装支付宝安全支付服务，才能进行付款。\n\n点击确定，立即安装。");
            builder.setPositiveButton("确定", new g(eVar, (String) message.obj, context));
            builder.setNegativeButton("取消", new h(eVar));
            builder.show();
            super.handleMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
