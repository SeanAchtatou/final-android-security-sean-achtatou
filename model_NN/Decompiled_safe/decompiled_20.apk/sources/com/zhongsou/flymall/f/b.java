package com.zhongsou.flymall.f;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.alipay.android.app.IAlixPay;

final class b implements ServiceConnection {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.a.a) {
            this.a.b = IAlixPay.Stub.asInterface(iBinder);
            this.a.a.notify();
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.a.b = null;
    }
}
