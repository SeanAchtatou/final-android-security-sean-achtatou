package com.zhongsou.flymall.f;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.util.Log;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.zhongsou.flymall.c.b;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.apache.commons.httpclient.cookie.Cookie2;
import org.json.JSONException;
import org.json.JSONObject;

public final class e {
    Context a = null;
    private ProgressDialog b = null;
    /* access modifiers changed from: private */
    public Handler c = new i(this);

    public e(Context context) {
        this.a = context;
    }

    private JSONObject a(String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("action", "update");
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("platform", "android");
            jSONObject2.put(Cookie2.VERSION, str);
            jSONObject2.put("partner", PoiTypeDef.All);
            jSONObject.put("data", jSONObject2);
            return b(jSONObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean a(Context context, String str, String str2) {
        try {
            InputStream open = context.getAssets().open(str);
            File file = new File(str2);
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = open.read(bArr);
                if (read > 0) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.close();
                    open.close();
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private JSONObject b(String str) {
        JSONObject jSONObject;
        String a2;
        j jVar = new j(this.a);
        try {
            synchronized (jVar) {
                a2 = jVar.a(str, "https://msp.alipay.com/x.htm");
            }
            jSONObject = new JSONObject(a2);
        } catch (Exception e) {
            e.printStackTrace();
            jSONObject = null;
        }
        if (jSONObject != null) {
            Log.d("AliPay", jSONObject.toString());
        }
        return jSONObject;
    }

    public final String a(PackageInfo packageInfo) {
        try {
            JSONObject a2 = a(packageInfo.versionName);
            if (a2.getString("needUpdate").equalsIgnoreCase("true")) {
                return a2.getString("updateUrl");
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog
     arg types: [android.content.Context, java.lang.String]
     candidates:
      com.zhongsou.flymall.c.b.a(android.content.Context, android.content.DialogInterface$OnClickListener):android.app.AlertDialog
      com.zhongsou.flymall.c.b.a(android.content.Context, int):void
      com.zhongsou.flymall.c.b.a(android.content.Context, long):void
      com.zhongsou.flymall.c.b.a(android.content.Context, com.zhongsou.flymall.d.z):void
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.String):void
      com.zhongsou.flymall.c.b.a(java.lang.String, java.lang.String):boolean
      com.zhongsou.flymall.c.b.a(android.content.Context, java.lang.CharSequence):android.app.ProgressDialog */
    public final boolean a() {
        boolean z = false;
        List<PackageInfo> installedPackages = this.a.getPackageManager().getInstalledPackages(0);
        int i = 0;
        while (true) {
            if (i >= installedPackages.size()) {
                break;
            } else if (installedPackages.get(i).packageName.equalsIgnoreCase("com.alipay.android.app")) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            String str = this.a.getCacheDir().getAbsolutePath() + "/temp.apk";
            a(this.a, "alipay_plugin.apk", str);
            this.b = b.a(this.a, (CharSequence) "正在检测安全支付服务版本");
            new Thread(new f(this, str)).start();
        }
        return z;
    }

    public final boolean a(String str, String str2) {
        try {
            return new j(this.a).b(str, str2);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        try {
            if (this.b != null) {
                this.b.dismiss();
                this.b = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
