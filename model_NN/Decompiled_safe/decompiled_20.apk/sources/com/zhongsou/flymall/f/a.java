package com.zhongsou.flymall.f;

import android.app.Activity;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import com.alipay.android.app.IAlixPay;
import com.alipay.android.app.IRemoteServiceCallback;

public final class a {
    Integer a = 0;
    IAlixPay b = null;
    boolean c = false;
    Activity d = null;
    /* access modifiers changed from: private */
    public ServiceConnection e = new b(this);
    /* access modifiers changed from: private */
    public IRemoteServiceCallback f = new d(this);

    public final boolean a(String str, Handler handler, Activity activity) {
        if (this.c) {
            return false;
        }
        this.c = true;
        this.d = activity;
        if (!new e(this.d).a()) {
            return false;
        }
        if (this.b == null) {
            activity.getApplicationContext().bindService(new Intent(IAlixPay.class.getName()), this.e, 1);
        }
        new Thread(new c(this, str, handler)).start();
        return true;
    }
}
