package com.zhongsou.flymall.f;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.zhongsou.flymall.g.g;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

public final class j {
    Proxy a = null;
    Context b;
    private int c = 30000;
    private int d = 30000;

    public j(Context context) {
        this.b = context;
        HttpsURLConnection.setDefaultHostnameVerifier(new k(this));
    }

    private void a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.getType() == 0) {
            String defaultHost = android.net.Proxy.getDefaultHost();
            int defaultPort = android.net.Proxy.getDefaultPort();
            if (defaultHost != null) {
                this.a = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(defaultHost, defaultPort));
            }
        }
    }

    public final String a(String str, String str2) {
        HttpURLConnection httpURLConnection;
        IOException e;
        String str3;
        a();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("requestData", str));
        try {
            UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(arrayList, "utf-8");
            URL url = new URL(str2);
            httpURLConnection = this.a != null ? (HttpURLConnection) url.openConnection(this.a) : (HttpURLConnection) url.openConnection();
            try {
                httpURLConnection.setConnectTimeout(this.c);
                httpURLConnection.setReadTimeout(this.d);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.addRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
                httpURLConnection.connect();
                OutputStream outputStream = httpURLConnection.getOutputStream();
                urlEncodedFormEntity.writeTo(outputStream);
                outputStream.flush();
                str3 = g.a(httpURLConnection.getInputStream());
            } catch (IOException e2) {
                IOException iOException = e2;
                str3 = null;
                e = iOException;
                try {
                    e.printStackTrace();
                    httpURLConnection.disconnect();
                    return str3;
                } catch (Throwable th) {
                    th = th;
                    httpURLConnection.disconnect();
                    throw th;
                }
            }
            try {
                Log.d("AliPayNetworkManager", "response " + str3);
                httpURLConnection.disconnect();
            } catch (IOException e3) {
                e = e3;
                e.printStackTrace();
                httpURLConnection.disconnect();
                return str3;
            }
        } catch (IOException e4) {
            httpURLConnection = null;
            e = e4;
            str3 = null;
            e.printStackTrace();
            httpURLConnection.disconnect();
            return str3;
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            httpURLConnection.disconnect();
            throw th;
        }
        return str3;
    }

    public final boolean b(String str, String str2) {
        a();
        try {
            URL url = new URL(str);
            HttpURLConnection httpURLConnection = this.a != null ? (HttpURLConnection) url.openConnection(this.a) : (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(this.c);
            httpURLConnection.setReadTimeout(this.d);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            File file = new File(str2);
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read > 0) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.close();
                    inputStream.close();
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
