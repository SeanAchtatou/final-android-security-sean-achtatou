package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.n;
import com.zhongsou.flymall.manager.b;
import java.util.List;

public final class t extends BaseAdapter {
    List<n> a;
    /* access modifiers changed from: private */
    public Context b;
    private LayoutInflater c;
    private int d = R.layout.order_detail_item;
    private b e;

    public t(Context context, List<n> list) {
        this.b = context;
        this.c = LayoutInflater.from(context);
        this.a = list;
        this.e = new b(context.getResources());
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        v vVar = new v();
        if (view == null) {
            view = this.c.inflate(this.d, (ViewGroup) null);
            vVar.a = (ImageView) view.findViewById(R.id.order_detail_item_img);
            vVar.b = (TextView) view.findViewById(R.id.order_detail_item_name);
            vVar.c = (TextView) view.findViewById(R.id.order_detail_item_price);
            vVar.d = (TextView) view.findViewById(R.id.order_detail_item_num);
            view.setTag(vVar);
        } else {
            vVar = (v) view.getTag();
        }
        n nVar = this.a.get(i);
        vVar.b.setText(nVar.getName());
        vVar.c.setText("￥" + nVar.getItem_price());
        vVar.d.setText(new StringBuilder().append(nVar.getNum()).toString());
        b bVar = this.e;
        b bVar2 = this.e;
        bVar.a(b.a(this.b.getResources()));
        this.e.a(nVar.getImg(), vVar.a);
        view.setOnClickListener(new u(this, nVar));
        if (getCount() == 1) {
            view.setBackgroundResource(R.drawable.list_item_one_bg);
        } else if (i == 0) {
            view.setBackgroundResource(R.drawable.list_item_top_bg);
        } else if (i == getCount() - 1) {
            view.setBackgroundResource(R.drawable.list_item_bottom_bg);
        } else {
            view.setBackgroundResource(R.drawable.list_item_center_bg);
        }
        return view;
    }
}
