package com.zhongsou.flymall.a;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.manager.b;
import java.util.List;

public final class ad extends BaseAdapter {
    private List<z> a;
    private Context b;
    private b c;

    public ad(Context context, List<z> list) {
        this.b = context;
        this.a = list;
        this.c = new b(context.getResources());
    }

    public final void a() {
        if (this.a != null) {
            this.a.clear();
        }
    }

    public final void a(z zVar) {
        this.a.add(zVar);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ae aeVar;
        if (view == null) {
            view = View.inflate(this.b, R.layout.product_listitem, null);
            aeVar = new ae();
            aeVar.a = (ImageView) view.findViewById(R.id.product_img);
            aeVar.b = (TextView) view.findViewById(R.id.product_title);
            aeVar.c = (TextView) view.findViewById(R.id.product_price);
            aeVar.d = (TextView) view.findViewById(R.id.product_promote);
            aeVar.e = (TextView) view.findViewById(R.id.product_mprice);
            view.setTag(aeVar);
        } else {
            aeVar = (ae) view.getTag();
        }
        z zVar = this.a.get(i);
        if (!TextUtils.isEmpty(zVar.getImg())) {
            this.c.a(zVar.getImg(), aeVar.a);
        } else {
            aeVar.a.setBackgroundResource(R.drawable.icon);
        }
        aeVar.b.setText(zVar.getName());
        aeVar.c.setText(com.zhongsou.flymall.g.b.a(zVar.getPrice()));
        aeVar.d.setText(zVar.getPromote());
        aeVar.e.setText(com.zhongsou.flymall.g.b.a(zVar.getMprice()));
        aeVar.e.getPaint().setFlags(16);
        aeVar.f = zVar;
        return view;
    }
}
