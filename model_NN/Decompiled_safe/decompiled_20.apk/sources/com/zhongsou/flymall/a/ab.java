package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.manager.b;
import java.util.List;

public final class ab extends BaseAdapter {
    private Context a;
    private List<z> b;
    private LayoutInflater c;
    private int d;
    private b e = null;

    public ab(Context context, List<z> list) {
        this.a = context;
        this.c = LayoutInflater.from(context);
        this.d = R.layout.product_listitem;
        this.b = list;
        this.e = new b(context.getResources());
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return 0;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ac acVar;
        if (view == null) {
            view = this.c.inflate(this.d, (ViewGroup) null);
            acVar = new ac();
            acVar.a = (TextView) view.findViewById(R.id.product_title);
            acVar.b = (TextView) view.findViewById(R.id.product_price);
            acVar.c = (TextView) view.findViewById(R.id.product_mprice);
            acVar.d = (TextView) view.findViewById(R.id.product_promote);
            acVar.e = (ImageView) view.findViewById(R.id.product_img);
            view.setTag(acVar);
        } else {
            acVar = (ac) view.getTag();
        }
        z zVar = this.b.get(i);
        acVar.a.setText(zVar.getName());
        acVar.a.setTag(zVar);
        acVar.b.setText(com.zhongsou.flymall.g.b.a(zVar.getPrice()));
        acVar.c.setText(com.zhongsou.flymall.g.b.a(zVar.getMprice()));
        acVar.c.getPaint().setFlags(16);
        acVar.d.setText(zVar.getPromote());
        b bVar = this.e;
        b bVar2 = this.e;
        bVar.a(b.a(this.a.getResources()));
        this.e.a(zVar.getImg(), acVar.e);
        return view;
    }
}
