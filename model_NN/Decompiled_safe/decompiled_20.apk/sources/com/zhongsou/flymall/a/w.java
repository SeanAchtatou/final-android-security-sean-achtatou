package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.activity.OrderListActivity;
import com.zhongsou.flymall.d.p;
import com.zhongsou.flymall.g.g;
import com.zhongsou.flymall.manager.b;
import java.util.List;

public final class w extends BaseAdapter {
    private Context a;
    private List<p> b;
    private LayoutInflater c;
    private int d;
    private b e;
    private int f = 0;

    public w(Context context, List<p> list, int i) {
        this.a = context;
        this.c = LayoutInflater.from(context);
        this.d = R.layout.user_order_item;
        this.b = list;
        this.e = new b(context.getResources());
        this.f = i;
    }

    public final List<p> a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        ((OrderListActivity) this.a).b(str);
    }

    public final void a(List<p> list) {
        this.b = list;
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        y yVar;
        p pVar = this.b.get(i);
        if (view == null) {
            view = this.c.inflate(this.d, (ViewGroup) null);
            y yVar2 = new y();
            yVar2.b = (ImageView) view.findViewById(R.id.product_imageview);
            yVar2.c = (TextView) view.findViewById(R.id.order_sn);
            yVar2.d = (TextView) view.findViewById(R.id.order_time);
            yVar2.e = (TextView) view.findViewById(R.id.order_amount);
            yVar2.f = (TextView) view.findViewById(R.id.order_ship_amount);
            yVar2.g = (TextView) view.findViewById(R.id.order_count);
            yVar2.h = (Button) view.findViewById(R.id.order_ship_btn);
            yVar2.i = (Button) view.findViewById(R.id.order_pay_btn);
            yVar2.j = (Button) view.findViewById(R.id.order_receiving_btn);
            yVar2.a = pVar.getId();
            view.setTag(yVar2);
            yVar = yVar2;
        } else {
            yVar = (y) view.getTag();
        }
        yVar.c.setText(String.valueOf(pVar.getSn()));
        yVar.d.setText(g.a(pVar.getTime()));
        yVar.e.setText(com.zhongsou.flymall.g.b.a(pVar.getAmount()));
        yVar.f.setText(pVar.getShipt() + com.zhongsou.flymall.g.b.a(pVar.getShipa()));
        yVar.g.setText("共" + String.valueOf(pVar.getNum()) + "件商品");
        yVar.k = pVar;
        b bVar = this.e;
        b bVar2 = this.e;
        bVar.a(b.a(this.a.getResources()));
        this.e.a(pVar.getImg(), yVar.b);
        if (this.f == 0) {
            yVar.i.setVisibility(0);
            yVar.i.setBackgroundResource(R.drawable.btn_gray_title_bar);
            yVar.i.setOnClickListener(new x(this, pVar));
        } else if (this.f == 1) {
            yVar.i.setVisibility(8);
        }
        return view;
    }
}
