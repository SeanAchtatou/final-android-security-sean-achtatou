package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.a;
import java.util.List;

public final class h extends BaseAdapter {
    private Context a;
    private List<a> b;

    public h(Context context, List<a> list) {
        this.a = context;
        this.b = list;
    }

    public final void a() {
        if (this.b != null) {
            this.b.clear();
        }
    }

    public final void a(a aVar) {
        this.b.add(aVar);
    }

    public final int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            i iVar = new i();
            view = LayoutInflater.from(this.a).inflate((int) R.layout.address_item, (ViewGroup) null);
            view.setTag(iVar);
        } else {
            view.getTag();
        }
        a aVar = this.b.get(i);
        TextView textView = (TextView) view.findViewById(R.id.address_item_index);
        TextView textView2 = (TextView) view.findViewById(R.id.address_item_name);
        TextView textView3 = (TextView) view.findViewById(R.id.address_item_mobile);
        TextView textView4 = (TextView) view.findViewById(R.id.address_item_address);
        TextView textView5 = (TextView) view.findViewById(R.id.address_item_default_flag);
        if (aVar.a()) {
            textView5.setVisibility(0);
        } else {
            textView5.setVisibility(8);
        }
        textView.setText("地址" + String.valueOf(i + 1));
        textView2.setText(aVar.getName());
        textView3.setText(aVar.getMobile());
        textView4.setText(aVar.getProvince() + aVar.getCity() + aVar.getArea() + aVar.getAddress());
        return view;
    }
}
