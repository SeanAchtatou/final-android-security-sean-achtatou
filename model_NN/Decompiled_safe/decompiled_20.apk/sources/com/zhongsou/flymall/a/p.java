package com.zhongsou.flymall.a;

import android.view.View;
import android.widget.EditText;
import com.zhongsou.flymall.activity.MainActivity;
import com.zhongsou.flymall.activity.bd;
import com.zhongsou.flymall.activity.bo;
import com.zhongsou.flymall.b.a.a;

final class p implements View.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ a b;
    final /* synthetic */ m c;

    p(m mVar, EditText editText, a aVar) {
        this.c = mVar;
        this.a = editText;
        this.b = aVar;
    }

    public final void onClick(View view) {
        this.a.setSelection(this.a.length());
        MainActivity a2 = this.c.a;
        bo unused = this.c.b;
        new bd(a2, this.b).a();
    }
}
