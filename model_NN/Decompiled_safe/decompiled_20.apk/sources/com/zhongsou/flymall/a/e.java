package com.zhongsou.flymall.a;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import com.zhongsou.flymall.manager.b;
import java.util.ArrayList;
import java.util.List;

public final class e extends BaseAdapter {
    private Activity a;
    private List<String> b = new ArrayList(0);
    private b c;

    public e(Activity activity, List<String> list) {
        this.a = activity;
        this.b = list;
        this.c = new b(activity.getResources());
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = new ImageView(this.a);
        this.c.a(this.b.get(i), imageView, b.b);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.a.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        imageView.setLayoutParams(new Gallery.LayoutParams(displayMetrics.widthPixels / 2, -1));
        return imageView;
    }
}
