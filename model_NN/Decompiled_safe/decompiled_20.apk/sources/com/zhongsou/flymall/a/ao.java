package com.zhongsou.flymall.a;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.manager.b;
import java.util.List;

public final class ao extends BaseAdapter {
    private List<z> a;
    private Context b;
    private b c;

    public ao(Context context, List<z> list) {
        this.b = context;
        this.a = list;
        this.c = new b(context.getResources());
    }

    public final void a() {
        if (this.a != null) {
            this.a.clear();
        }
    }

    public final void a(z zVar) {
        this.a.add(zVar);
    }

    public final int getCount() {
        if (this.a.size() <= 3) {
            return this.a.size();
        }
        return Integer.MAX_VALUE;
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ap apVar;
        if (view == null) {
            view = View.inflate(this.b, R.layout.rebate_act_grid_item, null);
            apVar = new ap();
            apVar.a = (ImageView) view.findViewById(R.id.iv_rebate_act_grid);
            apVar.b = (TextView) view.findViewById(R.id.tv_rebate_act_price);
            view.setTag(apVar);
        } else {
            apVar = (ap) view.getTag();
        }
        if (this.a != null && this.a.size() > 0) {
            z zVar = this.a.get(i % this.a.size());
            if (!TextUtils.isEmpty(zVar.getImg())) {
                this.c.a(zVar.getImg(), apVar.a);
            } else {
                apVar.a.setBackgroundResource(R.drawable.image_default);
            }
            apVar.b.setText(com.zhongsou.flymall.g.b.a(zVar.getPrice()));
            apVar.c = zVar;
        }
        return view;
    }
}
