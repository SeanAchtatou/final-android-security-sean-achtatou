package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import java.util.List;

public final class aq extends BaseAdapter {
    private List<String> a = null;
    private Context b;

    public aq(Context context, List<String> list) {
        this.b = context;
        this.a = list;
    }

    public final void a(List<String> list) {
        this.a = list;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ar arVar;
        if (view == null) {
            ar arVar2 = new ar();
            view = LayoutInflater.from(this.b).inflate((int) R.layout.item, (ViewGroup) null);
            arVar2.a = (TextView) view.findViewById(R.id.title);
            view.setTag(arVar2);
            arVar = arVar2;
        } else {
            arVar = (ar) view.getTag();
        }
        arVar.a.setText(this.a.get(i));
        return view;
    }
}
