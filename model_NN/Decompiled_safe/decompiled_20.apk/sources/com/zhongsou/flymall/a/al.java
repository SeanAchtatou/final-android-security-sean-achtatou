package com.zhongsou.flymall.a;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.x;
import com.zhongsou.flymall.g.d;
import com.zhongsou.flymall.g.j;
import com.zhongsou.flymall.manager.b;
import java.util.ArrayList;
import java.util.List;

public final class al extends BaseAdapter {
    public List<x> a = new ArrayList();
    /* access modifiers changed from: private */
    public Context b;
    private b c;

    public al(Context context) {
        this.b = context;
        this.c = new b(context.getResources());
    }

    public final void a(List<x> list) {
        this.a.addAll(list);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        an anVar;
        String str;
        if (view == null) {
            anVar = new an(this);
            view = View.inflate(this.b, R.layout.product_seckill_list_item, null);
            anVar.a = (ImageView) view.findViewById(R.id.iv_seckill_product);
            anVar.d = (TextView) view.findViewById(R.id.tv_seckill_product_name);
            anVar.b = (TextView) view.findViewById(R.id.tv_seckill_price);
            anVar.e = (TextView) view.findViewById(R.id.tv_discount);
            anVar.c = (TextView) view.findViewById(R.id.tv_seckill_time);
            anVar.f = (Button) view.findViewById(R.id.btn_seckill_instance);
            view.setTag(anVar);
        } else {
            anVar = (an) view.getTag();
        }
        this.c.a(this.a.get(i).getImage(), anVar.a);
        anVar.d.setText(this.a.get(i).getGd_name());
        anVar.b.setText(com.zhongsou.flymall.g.b.a(this.a.get(i).getPrice()));
        anVar.e.setText(this.a.get(i).getDiscount() + "折");
        TextView textView = anVar.c;
        Long start_time = this.a.get(i).getStart_time();
        Long end_time = this.a.get(i).getEnd_time();
        long now_time = this.a.get(i).getNow_time();
        if (now_time - start_time.longValue() > 0) {
            j a2 = d.a(end_time.longValue() - now_time);
            str = "秒杀将于<font color='red'>" + a2.a() + "</font>时<font color='red'>" + a2.b() + "</font>分后结束";
            anVar.f.setText((int) R.string.immediate_seckill);
        } else if (start_time.longValue() - now_time > 0) {
            j a3 = d.a(start_time.longValue() - now_time);
            str = "秒杀将于<font color='red'>" + a3.a() + "</font>时<font color='red'>" + a3.b() + "</font>分后开始";
            anVar.f.setText((int) R.string.coming);
        } else {
            str = "秒杀已结束";
        }
        textView.setText(Html.fromHtml(str));
        if (this.a.get(i).getNow_time() < this.a.get(i).getStart_time().longValue() || this.a.get(i).getNow_time() >= this.a.get(i).getEnd_time().longValue()) {
            anVar.f.setBackgroundResource(R.drawable.button_bg);
            anVar.f.setClickable(false);
            anVar.f.setTextColor(-7829368);
        } else {
            anVar.f.setBackgroundResource(R.drawable.selector_red_button);
            anVar.f.setTextColor(-1);
            anVar.f.setOnClickListener(new am(this, i));
        }
        anVar.f.setPadding(5, 5, 5, 5);
        anVar.f.setTextSize(15.0f);
        return view;
    }
}
