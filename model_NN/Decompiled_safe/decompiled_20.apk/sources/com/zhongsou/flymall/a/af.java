package com.zhongsou.flymall.a;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.t;
import java.util.List;

public final class af extends BaseAdapter {
    private List<t> a;
    private Context b;

    public af(Context context, List<t> list) {
        this.b = context;
        this.a = list;
    }

    public final void a(t tVar) {
        this.a.add(tVar);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ag agVar;
        if (view == null) {
            view = View.inflate(this.b, R.layout.product_consult_item, null);
            agVar = new ag();
            agVar.a = (TextView) view.findViewById(R.id.tv_product_consult_item_question);
            agVar.b = (TextView) view.findViewById(R.id.tv_product_consult_item_username);
            agVar.c = (TextView) view.findViewById(R.id.tv_product_consult_item_time);
            agVar.d = (TextView) view.findViewById(R.id.tv_product_consult_item_answer);
            view.setTag(agVar);
        } else {
            agVar = (ag) view.getTag();
        }
        t tVar = this.a.get(i);
        agVar.a.setText(tVar.getQuestion());
        if (TextUtils.isEmpty(tVar.getName())) {
            agVar.b.setText("HuangLiBo(不是会员)");
        } else {
            agVar.b.setText(tVar.getName() + "(" + tVar.getLevel() + ")");
        }
        agVar.c.setText(tVar.getTime());
        if (TextUtils.isEmpty(tVar.getAnswer())) {
            agVar.d.setText("正在积极的回答...");
        } else {
            agVar.d.setText(tVar.getAnswer());
        }
        agVar.e = tVar;
        return view;
    }
}
