package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.manager.b;
import java.util.List;

public final class z extends BaseAdapter {
    private List<a> a = null;
    private Context b;
    private b c;

    public z(Context context, List<a> list) {
        this.b = context;
        this.a = list;
        this.c = new b(context.getResources());
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        aa aaVar;
        if (view == null) {
            aa aaVar2 = new aa();
            view = LayoutInflater.from(this.b).inflate((int) R.layout.order_product_item, (ViewGroup) null);
            aaVar2.a = view.findViewById(R.id.check_order_product_item);
            view.setTag(aaVar2);
            aaVar = aaVar2;
        } else {
            aaVar = (aa) view.getTag();
        }
        a aVar = this.a.get(i);
        ((TextView) aaVar.a.findViewById(R.id.order_item_name)).setText(aVar.getName() + " " + aVar.getStock_attr());
        ((TextView) aaVar.a.findViewById(R.id.order_item_price)).setText("￥" + com.zhongsou.flymall.g.b.b(aVar.getPrice()));
        ((TextView) aaVar.a.findViewById(R.id.order_item_count)).setText(String.valueOf(aVar.getNum()));
        this.c.a(aVar.getImg_remote(), (ImageView) aaVar.a.findViewById(R.id.order_item_img));
        return view;
    }
}
