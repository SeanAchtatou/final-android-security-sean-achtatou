package com.zhongsou.flymall.a;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.manager.b;
import java.util.ArrayList;
import java.util.List;

public final class aj extends BaseAdapter {
    public List<z> a = new ArrayList();
    private Context b;
    private boolean c = true;
    private b d;

    public aj(Context context, boolean z) {
        this.b = context;
        this.c = z;
        this.d = new b(context.getResources());
    }

    public final void a(List<z> list) {
        this.a.addAll(list);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ak akVar;
        if (view == null) {
            akVar = new ak(this);
            view = View.inflate(this.b, R.layout.product_common_list_item, null);
            akVar.a = (ImageView) view.findViewById(R.id.iv_promot_product);
            akVar.b = (TextView) view.findViewById(R.id.tv_promot_info);
            akVar.c = (TextView) view.findViewById(R.id.tv_price);
            akVar.d = (TextView) view.findViewById(R.id.tv_original_price);
            akVar.e = (TextView) view.findViewById(R.id.tv_discount_product);
            view.setTag(akVar);
        } else {
            akVar = (ak) view.getTag();
        }
        if (this.c) {
            akVar.d.setVisibility(0);
            akVar.e.setVisibility(8);
            akVar.d.setText(com.zhongsou.flymall.g.b.a(this.a.get(i).getMprice()));
        } else {
            akVar.d.setVisibility(8);
            akVar.e.setVisibility(0);
            akVar.e.setText(this.a.get(i).getActInfo() + "折");
        }
        if (!TextUtils.isEmpty(this.a.get(i).getImg())) {
            this.d.a(this.a.get(i).getImg(), akVar.a);
        } else {
            b bVar = this.d;
            b bVar2 = this.d;
            bVar.a(b.a(this.b.getResources()));
        }
        akVar.b.setText(this.a.get(i).getName().trim());
        akVar.c.setText(com.zhongsou.flymall.g.b.a(this.a.get(i).getPrice()));
        return view;
    }
}
