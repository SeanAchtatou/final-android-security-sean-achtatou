package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.j;
import com.zhongsou.flymall.g.b;
import com.zhongsou.flymall.g.d;
import java.util.List;

public final class a extends BaseAdapter {
    private Context a;
    private List<j> b;

    public a(Context context, List<j> list) {
        this.a = context;
        this.b = list;
    }

    public final int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        b bVar;
        if (view == null) {
            b bVar2 = new b();
            view = LayoutInflater.from(this.a).inflate((int) R.layout.coupon_item, (ViewGroup) null);
            bVar2.a = view.findViewById(R.id.coupon_item_list);
            view.setTag(bVar2);
            bVar = bVar2;
        } else {
            bVar = (b) view.getTag();
        }
        TextView textView = (TextView) bVar.a.findViewById(R.id.copuon_item_name);
        TextView textView2 = (TextView) bVar.a.findViewById(R.id.copuon_item_time);
        j jVar = this.b.get(i);
        textView.setText(jVar != null ? b.b(jVar.getPrice()) + "(满" + b.b(jVar.getMinAmount()) + "使用)" : PoiTypeDef.All);
        textView2.setText("有效期：" + d.a(jVar.getBtime()) + "~" + d.a(jVar.getEtime()));
        return view;
    }
}
