package com.zhongsou.flymall.a;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.x;
import com.zhongsou.flymall.manager.b;
import java.util.List;

public final class f extends BaseAdapter {
    private List<x> a;
    private Context b;
    private b c;

    public f(Context context, List<x> list) {
        this.b = context;
        this.a = list;
        this.c = new b(context.getResources());
    }

    public final void a() {
        if (this.a != null) {
            this.a.clear();
        }
    }

    public final void a(x xVar) {
        this.a.add(xVar);
    }

    public final int getCount() {
        if (this.a.size() <= 3) {
            return this.a.size();
        }
        return Integer.MAX_VALUE;
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        g gVar;
        if (view == null) {
            view = View.inflate(this.b, R.layout.kill_pro_grid_item, null);
            gVar = new g();
            gVar.a = (ImageView) view.findViewById(R.id.iv_kill_pro_grid);
            gVar.b = (TextView) view.findViewById(R.id.tv_kill_pro_price);
            view.setTag(gVar);
        } else {
            gVar = (g) view.getTag();
        }
        if (this.a != null && this.a.size() > 0) {
            x xVar = this.a.get(i % this.a.size());
            if (!TextUtils.isEmpty(xVar.getImage())) {
                this.c.a(xVar.getImage(), gVar.a);
            } else {
                gVar.a.setBackgroundResource(R.drawable.image_default);
            }
            if (xVar.getNum() != 0) {
                gVar.b.setText(com.zhongsou.flymall.g.b.a(xVar.getPrice()));
                gVar.b.setTextColor(this.b.getResources().getColor(R.color.text_color_ff2400));
            } else {
                gVar.b.setText("抢光");
                gVar.b.setTextColor(this.b.getResources().getColor(R.color.text_color_989898));
            }
            gVar.c = xVar;
        }
        return view;
    }
}
