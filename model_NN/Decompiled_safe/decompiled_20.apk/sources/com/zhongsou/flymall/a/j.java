package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.d;
import com.zhongsou.flymall.d.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class j extends BaseExpandableListAdapter {
    private Context a;
    private List<d> b;
    private LayoutInflater c;
    private int d;
    private int e;
    private List<Map<String, e>> f = null;

    public j(Context context, List<d> list) {
        this.a = context;
        this.c = LayoutInflater.from(context);
        this.d = R.layout.attrs_listitem;
        this.e = R.layout.attrsitem_listitem;
        this.b = list;
        this.f = new ArrayList();
    }

    public final List<Map<String, e>> a() {
        return this.f;
    }

    public final void a(List<Map<String, e>> list) {
        this.f = list;
    }

    public final Object getChild(int i, int i2) {
        return this.b.get(i).getCont().get(i2);
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        View inflate = this.c.inflate(this.e, (ViewGroup) null);
        k kVar = new k();
        kVar.a = (TextView) inflate.findViewById(R.id.itemattrs_name);
        kVar.b = (RadioButton) inflate.findViewById(R.id.itemattrs_id);
        inflate.setTag(kVar);
        e eVar = this.b.get(i).getCont().get(i2);
        kVar.a.setText(eVar.getName());
        kVar.a.setTag(eVar);
        HashMap hashMap = new HashMap();
        hashMap.put(this.b.get(i).getType(), eVar);
        kVar.b.setTag(hashMap);
        if (this.f.contains(hashMap)) {
            kVar.b.setChecked(true);
        } else {
            kVar.b.setChecked(false);
        }
        return inflate;
    }

    public final int getChildrenCount(int i) {
        return this.b.get(i).getCont().size();
    }

    public final Object getGroup(int i) {
        return this.b.get(i);
    }

    public final int getGroupCount() {
        return this.b.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        View inflate = this.c.inflate(this.d, (ViewGroup) null);
        l lVar = new l();
        lVar.a = (TextView) inflate.findViewById(R.id.attrs_name);
        lVar.b = (ImageView) inflate.findViewById(R.id.attrs_image);
        inflate.setTag(lVar);
        d dVar = this.b.get(i);
        lVar.a.setText(dVar.getName());
        lVar.a.setTag(dVar);
        lVar.b.setBackgroundResource(R.drawable.pref_icon_more);
        return inflate;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
