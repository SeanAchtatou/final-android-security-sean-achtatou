package com.zhongsou.flymall.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.activity.MainActivity;
import com.zhongsou.flymall.activity.bo;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.manager.b;
import java.util.List;

public final class m extends BaseAdapter {
    /* access modifiers changed from: private */
    public MainActivity a;
    /* access modifiers changed from: private */
    public bo b;
    private List<a> c;
    private LayoutInflater d;

    public m(MainActivity mainActivity, bo boVar, List<a> list) {
        this.a = mainActivity;
        this.b = boVar;
        this.c = list;
        this.d = LayoutInflater.from(mainActivity);
    }

    public final void a(List<a> list) {
        this.c = list;
    }

    public final int getCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public final Object getItem(int i) {
        if (this.c == null || this.c.size() < i) {
            return null;
        }
        return this.c.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        q qVar;
        if (view == null) {
            view = this.d.inflate((int) R.layout.cart_goods_item, (ViewGroup) null);
            qVar = new q();
            qVar.a = (CheckBox) view.findViewById(R.id.cart_goods_item_checkbox);
            qVar.b = (ImageView) view.findViewById(R.id.cart_goods_item_img);
            qVar.c = (TextView) view.findViewById(R.id.cart_goods_item_name);
            qVar.d = (RelativeLayout) view.findViewById(R.id.cart_goods_item_invalidtion);
            qVar.e = (RelativeLayout) view.findViewById(R.id.cart_goods_item_validation);
            qVar.f = (EditText) view.findViewById(R.id.cart_goods_item_count_value);
            qVar.g = (TextView) view.findViewById(R.id.cart_goods_item_amount_value);
            qVar.h = new b(this.a.getResources());
            view.setTag(qVar);
        } else {
            qVar = (q) view.getTag();
        }
        a aVar = this.c.get(i);
        qVar.a.setChecked(d.d(aVar));
        qVar.a.setOnClickListener(new n(this, aVar));
        qVar.a.setTag(Long.valueOf(aVar.getGd_id()));
        if (d.e(aVar)) {
            qVar.e.setVisibility(0);
            qVar.d.setVisibility(8);
            EditText editText = qVar.f;
            editText.setText(String.valueOf(aVar.getNum()));
            editText.setOnClickListener(new p(this, editText, aVar));
            qVar.g.setText(com.zhongsou.flymall.g.b.a(aVar.getPrice()));
        } else {
            qVar.e.setVisibility(8);
            qVar.d.setVisibility(0);
        }
        qVar.h.a(aVar.getImg_remote(), qVar.b);
        qVar.c.setText(aVar.getName() + " " + aVar.getStock_attr());
        return view;
    }
}
