package com.zhongsou.flymall.a;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.u;
import java.util.List;

public final class ah extends BaseAdapter {
    private List<u> a;
    private Context b;

    public ah(Context context, List<u> list) {
        this.b = context;
        this.a = list;
    }

    public final void a(u uVar) {
        this.a.add(uVar);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ai aiVar;
        if (view == null) {
            view = View.inflate(this.b, R.layout.product_discuss_item, null);
            aiVar = new ai();
            aiVar.a = (TextView) view.findViewById(R.id.tv_product_discuss_item_username);
            aiVar.b = (RatingBar) view.findViewById(R.id.rb_product_discuss_item_ratingBar);
            aiVar.d = (TextView) view.findViewById(R.id.tv_product_discuss_item_content);
            aiVar.c = (TextView) view.findViewById(R.id.tv_product_discuss_item_title);
            aiVar.e = (TextView) view.findViewById(R.id.tv_product_discuss_item_time);
            view.setTag(aiVar);
        } else {
            aiVar = (ai) view.getTag();
        }
        u uVar = this.a.get(i);
        if (TextUtils.isEmpty(uVar.getName())) {
            aiVar.a.setText("HuangLiBo(不是会员)");
        } else {
            aiVar.a.setText(uVar.getName() + "(" + uVar.getLevel() + ")");
        }
        aiVar.b.setRating(Float.parseFloat(uVar.getGrades()));
        aiVar.e.setText(uVar.getTime());
        aiVar.c.setText(uVar.getTitle());
        aiVar.d.setText(uVar.getContent());
        aiVar.f = uVar;
        return view;
    }
}
