package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.w;
import com.zhongsou.flymall.g.b;
import java.util.ArrayList;
import java.util.List;

public final class as extends BaseAdapter {
    private List<w> a = new ArrayList(0);
    private Context b;
    private LayoutInflater c;

    public as(Context context, List<w> list) {
        this.b = context;
        this.a = list;
        this.c = LayoutInflater.from(this.b);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        if (i >= this.a.size()) {
            return null;
        }
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        if (i >= this.a.size()) {
            return 0;
        }
        return this.a.get(i).getArticle_id();
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        at atVar;
        if (view == null) {
            view = this.c.inflate((int) R.layout.product_attr_item, (ViewGroup) null);
            atVar = new at(this);
            atVar.a = (TextView) view.findViewById(R.id.txt_attr);
            atVar.b = (TextView) view.findViewById(R.id.txt_attr_price);
            view.setTag(atVar);
        } else {
            atVar = (at) view.getTag();
        }
        atVar.c = this.a.get(i);
        atVar.a.setText(atVar.c.getAttr_val());
        atVar.b.setText(b.a(atVar.c.getPrice()));
        return view;
    }
}
