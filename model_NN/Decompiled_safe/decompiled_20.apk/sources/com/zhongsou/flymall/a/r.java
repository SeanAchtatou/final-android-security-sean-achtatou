package com.zhongsou.flymall.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.zhongsou.flymall.d.i;
import java.util.List;

public final class r extends BaseAdapter {
    private Context a;
    private List<i> b;

    public r(Context context, List<i> list) {
        this.a = context;
        this.b = list;
    }

    public final int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    public final Object getItem(int i) {
        return null;
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        s sVar;
        if (view == null) {
            sVar = new s();
            view = LayoutInflater.from(this.a).inflate((int) R.layout.category_item, (ViewGroup) null);
            sVar.a = view.findViewById(R.id.cate_item_list);
            view.setTag(sVar);
        } else {
            sVar = (s) view.getTag();
        }
        ((TextView) sVar.a.findViewById(R.id.cate_item_name)).setText(this.b.get(i).getName());
        return view;
    }
}
