package com.zhongsou.flymall;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.zhongsou.flymall.activity.dp;
import com.zhongsou.flymall.b.a.a;
import com.zhongsou.flymall.b.d;
import com.zhongsou.flymall.d.ac;
import com.zhongsou.flymall.e.f;
import com.zhongsou.flymall.g.g;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

@SuppressLint({"DefaultLocale"})
public class AppContext extends Application {
    public static int a = 0;
    private static AppContext g;
    private Hashtable<String, Object> b = new Hashtable<>();
    private boolean c = false;
    private long d = 0;
    private String e = PoiTypeDef.All;
    private f f = new f(this);

    public AppContext() {
        g = this;
    }

    public static synchronized AppContext a() {
        AppContext appContext;
        synchronized (AppContext.class) {
            if (g == null) {
                g = new AppContext();
            }
            appContext = g;
        }
        return appContext;
    }

    public final String a(String str) {
        Properties a2 = b.a(this).a();
        if (a2 != null) {
            return a2.getProperty(str);
        }
        return null;
    }

    public final void a(ac acVar) {
        List<a> b2;
        this.d = acVar.getUid();
        this.e = acVar.getSession();
        this.c = true;
        b.a(this).a(new c(this, acVar));
        if (this.d != 0 && (b2 = d.b(0)) != null && b2.size() != 0) {
            for (a user_id : b2) {
                user_id.setUser_id(this.d);
            }
            d.c(b2);
            List<Long> a2 = d.a(b2);
            ArrayList arrayList = new ArrayList(a2.size());
            for (Long longValue : a2) {
                arrayList.add(String.valueOf(longValue.longValue()));
            }
            dp.b = arrayList;
        }
    }

    public final void a(String str, String str2) {
        b.a(this).a(str, str2);
    }

    public final void a(String... strArr) {
        b.a(this).a(strArr);
    }

    public final boolean b() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public final PackageInfo c() {
        PackageInfo packageInfo;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace(System.err);
            packageInfo = null;
        }
        return packageInfo == null ? new PackageInfo() : packageInfo;
    }

    public void checkLoginSuccess(ac acVar) {
        if (acVar != null) {
            Log.i("login back AppContext", com.b.a.a.a(acVar));
        }
        if (acVar == null || acVar.getStatus() != 0) {
            g();
        }
    }

    public final boolean d() {
        return this.c;
    }

    public final long e() {
        return this.d;
    }

    public final String f() {
        return this.e;
    }

    public final void g() {
        a("cookie");
        this.c = false;
        this.d = 0;
        this.e = PoiTypeDef.All;
        a("user.uid", "user.session");
    }

    public final void h() {
        ac j = j();
        if (j == null || j.getUid() <= 0 || !j.a()) {
            g();
            return;
        }
        this.d = j.getUid();
        this.e = j.getSession();
        this.c = true;
        Log.i("login param AppContext", com.b.a.a.a(j));
        this.f.a();
        f fVar = this.f;
        String username = j.getUsername();
        String password = j.getPassword();
        Long.valueOf(j.getUid());
        if (j.getSession() != null) {
            j.getSession();
        }
        fVar.d(username, password);
    }

    public final void i() {
        this.d = 0;
        this.c = false;
        this.e = PoiTypeDef.All;
        a("user.uid", "user.name", "user.session", "user.isRememberMe", "user.level", "user.point");
    }

    public final ac j() {
        ac acVar = new ac();
        acVar.setUid(g.e(a("user.uid")));
        acVar.setUsername(a("user.username"));
        acVar.setSession(a("user.session"));
        acVar.setAutoLogin(g.f(a("user.isRememberMe")));
        acVar.setLevel(a("user.level"));
        acVar.setPoint(a("user.point"));
        acVar.setUname(a("user.name"));
        return acVar;
    }

    public final boolean k() {
        String a2 = a("perf_checkup");
        if (g.a(a2)) {
            return true;
        }
        return g.f(a2);
    }

    public final boolean l() {
        String a2 = a("perf_scroll");
        if (g.a(a2)) {
            return false;
        }
        return g.f(a2);
    }

    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(d.a());
    }
}
