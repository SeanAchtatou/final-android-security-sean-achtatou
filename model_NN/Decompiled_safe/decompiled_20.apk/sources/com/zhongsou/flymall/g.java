package com.zhongsou.flymall;

import android.util.Log;
import com.zhongsou.flymall.b.f;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

final class g extends Thread {
    final /* synthetic */ AppStart a;

    g(AppStart appStart) {
        this.a = appStart;
    }

    public final void run() {
        f fVar = new f();
        if (!fVar.c()) {
            try {
                InputStream open = this.a.getAssets().open("init_areas.sql");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(open));
                fVar.a();
                fVar.a(bufferedReader);
                fVar.close();
                bufferedReader.close();
                open.close();
                Log.i("app start", "init db");
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }
}
