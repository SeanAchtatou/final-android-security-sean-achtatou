package com.zhongsou.flymall;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public final class b {
    private static b b;
    private Context a;

    public static b a(Context context) {
        if (b == null) {
            b bVar = new b();
            b = bVar;
            bVar.a = context;
        }
        return b;
    }

    private void b(Properties properties) {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2 = null;
        try {
            fileOutputStream = new FileOutputStream(new File(this.a.getDir("config", 0), "config"));
            try {
                properties.store(fileOutputStream, (String) null);
                fileOutputStream.flush();
                try {
                    fileOutputStream.close();
                } catch (Exception e) {
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    e.printStackTrace();
                    try {
                        fileOutputStream.close();
                    } catch (Exception e3) {
                    }
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream2 = fileOutputStream;
                    try {
                        fileOutputStream2.close();
                    } catch (Exception e4) {
                    }
                    throw th;
                }
            }
        } catch (Exception e5) {
            e = e5;
            fileOutputStream = null;
            e.printStackTrace();
            fileOutputStream.close();
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream2.close();
            throw th;
        }
    }

    public final Properties a() {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2 = null;
        Properties properties = new Properties();
        try {
            fileInputStream = new FileInputStream(this.a.getDir("config", 0).getPath() + File.separator + "config");
            try {
                properties.load(fileInputStream);
                try {
                    fileInputStream.close();
                } catch (Exception e) {
                }
            } catch (Exception e2) {
                try {
                    fileInputStream.close();
                } catch (Exception e3) {
                }
                return properties;
            } catch (Throwable th) {
                Throwable th2 = th;
                fileInputStream2 = fileInputStream;
                th = th2;
                try {
                    fileInputStream2.close();
                } catch (Exception e4) {
                }
                throw th;
            }
        } catch (Exception e5) {
            fileInputStream = null;
            fileInputStream.close();
            return properties;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream2.close();
            throw th;
        }
        return properties;
    }

    public final void a(String str, String str2) {
        Properties a2 = a();
        a2.setProperty(str, str2);
        b(a2);
    }

    public final void a(Properties properties) {
        Properties a2 = a();
        a2.putAll(properties);
        b(a2);
    }

    public final void a(String... strArr) {
        Properties a2 = a();
        for (String remove : strArr) {
            a2.remove(remove);
        }
        b(a2);
    }
}
