package com.zhongsou.flymall.c;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.b.a.e;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.unionpay.upomp.lthj.util.PluginHelper;
import com.upomp.pay.c.a;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.activity.AboutActivity;
import com.zhongsou.flymall.activity.BetaCheckActivity;
import com.zhongsou.flymall.activity.BetaProductActivity;
import com.zhongsou.flymall.activity.BetaProductListActivity;
import com.zhongsou.flymall.activity.BetaSearchActivity;
import com.zhongsou.flymall.activity.CompanyDescActivity;
import com.zhongsou.flymall.activity.MainActivity;
import com.zhongsou.flymall.activity.OrderDetailActivity;
import com.zhongsou.flymall.activity.OrderListActivity;
import com.zhongsou.flymall.activity.ProductDiscountActivity;
import com.zhongsou.flymall.activity.ProductPromotionActivity;
import com.zhongsou.flymall.activity.ProductRecommendActivity;
import com.zhongsou.flymall.activity.ProductSecKillActivity;
import com.zhongsou.flymall.activity.SearchAttrsActivity;
import com.zhongsou.flymall.d.d;
import com.zhongsou.flymall.d.z;
import com.zhongsou.flymall.g.g;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParser;

public final class b {
    private static Pattern a = Pattern.compile("\\[{1}([0-9]\\d*)\\]{1}");

    public static AlertDialog a(Context context, int i, int i2, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        if (onClickListener2 == null) {
            onClickListener2 = new e();
        }
        return new AlertDialog.Builder(context).setTitle(i).setMessage(i2).setPositiveButton((int) R.string.cart_dialog_positive_button, onClickListener).setNegativeButton((int) R.string.cart_dialog_negative_button, onClickListener2).show();
    }

    public static AlertDialog a(Context context, DialogInterface.OnClickListener onClickListener) {
        return a(context, R.string.cart_del_btn_title, R.string.cart_del_btn_message, onClickListener, null);
    }

    public static ProgressDialog a(Context context, CharSequence charSequence) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle((CharSequence) null);
        progressDialog.setMessage(charSequence);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(true);
        progressDialog.show();
        return progressDialog;
    }

    public static void a(Activity activity) {
        Intent intent = new Intent();
        intent.setClass(activity, AboutActivity.class);
        activity.startActivity(intent);
    }

    public static void a(Activity activity, long j, String str) {
        Intent intent = new Intent();
        if (j > 0) {
            intent.putExtra("cate_id", j);
        }
        if (str != null) {
            intent.putExtra("k", str);
        }
        intent.setClass(activity, BetaProductListActivity.class);
        activity.startActivity(intent);
    }

    public static void a(Activity activity, Long l, List<d> list) {
        Log.i("UIHelper", "showScreenRedirect.cate_id:" + l);
        Intent intent = new Intent(activity, BetaProductListActivity.class);
        intent.putExtra("cate_id", l);
        intent.putExtra("attrsList", (Serializable) list);
        activity.setResult(0, intent);
        activity.finish();
    }

    public static void a(Activity activity, Long l, Map<String, List<String>> map, List<d> list) {
        Log.i("UIHelper", "showScreenRedirect.cate_id:" + l);
        Intent intent = new Intent(activity, SearchAttrsActivity.class);
        intent.putExtra("cid", l);
        intent.putExtra("attrsMap", (Serializable) map);
        intent.putExtra("attrsList", (Serializable) list);
        activity.startActivityForResult(intent, 1);
    }

    public static void a(Context context) {
        context.startActivity(new Intent(context, BetaSearchActivity.class));
    }

    public static void a(Context context, int i) {
        Toast.makeText(context, i, 0).show();
    }

    public static void a(Context context, long j) {
        Log.i("UIHelper", "showProductRedirect.gd_id:" + j);
        Intent intent = new Intent(context, BetaProductActivity.class);
        intent.putExtra("gd_id", j);
        context.startActivity(intent);
    }

    public static void a(Context context, z zVar) {
        a(context, zVar.getId().longValue());
    }

    public static void a(Context context, String str) {
        Toast.makeText(context, str, 0).show();
    }

    public static void a(Context context, String str, Serializable serializable) {
        Intent intent = new Intent(context, BetaCheckActivity.class);
        intent.putExtra(str, serializable);
        context.startActivity(intent);
    }

    public static void a(ListView listView) {
        ListAdapter adapter = listView.getAdapter();
        if (adapter != null) {
            int count = adapter.getCount();
            int i = 0;
            for (int i2 = 0; i2 < count; i2++) {
                View view = adapter.getView(i2, null, listView);
                view.measure(0, 0);
                i += view.getMeasuredHeight();
            }
            ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
            layoutParams.height = (listView.getDividerHeight() * (adapter.getCount() - 1)) + i;
            listView.setLayoutParams(layoutParams);
        }
    }

    public static boolean a(Intent intent, Button button, Context context) {
        boolean z = false;
        if (intent != null) {
            try {
                String str = new String(intent.getExtras().getByteArray("xml"), "utf-8");
                Log.d(a.n, "这是支付成功后，回调返回的报文，需自行解析" + str);
                if (str.indexOf("<respCode>0000</respCode>") > 0) {
                    z = true;
                    a(context, "支付成功，谢谢您的消费，亲！");
                    if (button != null) {
                        button.setVisibility(8);
                    }
                } else {
                    a(context, "亲，很遗憾，您还没有支付成功哦！");
                }
            } catch (Exception e) {
                Log.d(a.n, "Exception is " + e);
            }
        } else {
            Log.d(a.n, "data is null");
        }
        return z;
    }

    public static boolean a(String str, String str2) {
        return Pattern.compile(str.equals("email") ? "(?:\\w[-._\\w]*\\w@\\w[-._\\w]*\\w\\.\\w{2,3}$)" : str.equals("zipcode") ? "^[0-9]{6}$" : str.equals("mobile") ? "^1\\d{10}$" : "^\\S+$").matcher(str2).find();
    }

    public static AlertDialog b(Context context, int i) {
        return new AlertDialog.Builder(context).setTitle((int) R.string.cart_dialog_title).setMessage(i).setPositiveButton((int) R.string.cart_dialog_positive_button, new d()).show();
    }

    public static void b(Activity activity) {
        Intent intent = new Intent();
        intent.putExtra("k", PoiTypeDef.All);
        intent.setClass(activity, ProductSecKillActivity.class);
        activity.startActivity(intent);
    }

    public static void b(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(17301659);
        builder.setTitle((int) R.string.app_menu_surelogout);
        builder.setPositiveButton((int) R.string.sure, new i(context));
        builder.setNegativeButton((int) R.string.cancle, new c());
        builder.show();
    }

    public static void b(Context context, long j) {
        Log.i("UIHelper", "showOrderDetailRedirect.order_id:" + j);
        Intent intent = new Intent(context, OrderDetailActivity.class);
        intent.putExtra("order_id", j);
        context.startActivity(intent);
    }

    public static void b(Context context, String str) {
        Toast.makeText(context, str, 500).show();
    }

    public static void c(Activity activity) {
        activity.startActivity(new Intent(activity, ProductPromotionActivity.class));
    }

    public static void c(Context context) {
        com.zhongsou.flymall.manager.a.a();
        com.zhongsou.flymall.manager.a.c();
        Intent intent = new Intent(context, MainActivity.class);
        AppContext.a = 2;
        context.startActivity(intent);
    }

    public static void c(Context context, int i) {
        Log.i("UIHelper", "showOrderListRedirect.order_type:" + i);
        Intent intent = new Intent(context, OrderListActivity.class);
        intent.putExtra("order_type", i);
        context.startActivity(intent);
    }

    public static void c(Context context, String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(17301659);
        builder.setTitle((int) R.string.app_error);
        builder.setMessage((int) R.string.app_error_message);
        builder.setPositiveButton((int) R.string.submit_report, new g(context, str));
        builder.setNegativeButton((int) R.string.sure, new h(context));
        builder.show();
    }

    public static void d(Activity activity) {
        activity.startActivity(new Intent(activity, ProductRecommendActivity.class));
    }

    public static void d(Context context) {
        com.zhongsou.flymall.manager.a.a();
        com.zhongsou.flymall.manager.a.c();
        Intent intent = new Intent(context, MainActivity.class);
        AppContext.a = 3;
        context.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void d(Context context, int i) {
        Log.i("UIHelper", "showOrderListRedirect.order_type:" + i);
        Intent intent = new Intent(context, OrderListActivity.class);
        intent.putExtra("order_type", i);
        intent.putExtra("isNotMember", true);
        context.startActivity(intent);
    }

    public static void d(Context context, String str) {
        try {
            context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str.replace("-", PoiTypeDef.All).replace("电话:", PoiTypeDef.All))));
        } catch (Exception e) {
            e.printStackTrace();
            b(context, "无法拨通电话");
        }
    }

    public static void e(Activity activity) {
        activity.startActivity(new Intent(activity, ProductDiscountActivity.class));
    }

    public static void e(Context context, String str) {
        Intent intent = new Intent(context, CompanyDescActivity.class);
        intent.putExtra("url", str);
        context.startActivity(intent);
    }

    public static void getPayInfoSuccess(Context context, Activity activity, e eVar) {
        if (eVar.containsKey(LocationManagerProxy.KEY_STATUS_CHANGED)) {
            switch (eVar.d(LocationManagerProxy.KEY_STATUS_CHANGED)) {
                case 1:
                    new com.zhongsou.flymall.f.a().a(eVar.f("payinfo"), new f(activity), activity);
                    return;
                case 6:
                    String f = eVar.f("reMeg");
                    String f2 = eVar.f("sign");
                    if (g.a(f) || g.a(f2)) {
                        b(context, "支付有问题！");
                        return;
                    }
                    InputStream a2 = com.upomp.pay.b.a.a(f);
                    new com.upomp.pay.a.a();
                    new com.upomp.pay.a.b();
                    try {
                        com.upomp.pay.a.a aVar = new com.upomp.pay.a.a();
                        XmlPullParser newPullParser = Xml.newPullParser();
                        newPullParser.setInput(a2, com.umeng.common.b.e.f);
                        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                            switch (eventType) {
                                case 2:
                                    if ("merchantId".equals(newPullParser.getName())) {
                                        com.upomp.pay.a.a.a(newPullParser.nextText());
                                        Log.d("Xmlpar", "______-___________" + com.upomp.pay.a.a.a());
                                    }
                                    if (!"merchantOrderId".equals(newPullParser.getName())) {
                                        if (!"merchantOrderTime".equals(newPullParser.getName())) {
                                            break;
                                        } else {
                                            com.upomp.pay.a.a.c(newPullParser.nextText());
                                            Log.d("Xmlpar", "______-___________" + com.upomp.pay.a.a.a());
                                            break;
                                        }
                                    } else {
                                        com.upomp.pay.a.a.b(newPullParser.nextText());
                                        Log.d("Xmlpar", "______-___________" + com.upomp.pay.a.a.a());
                                        break;
                                    }
                            }
                        }
                        System.out.println(aVar);
                        a.b = com.upomp.pay.a.a.a();
                        a.c = com.upomp.pay.a.a.b();
                        a.d = com.upomp.pay.a.a.c();
                        StringBuilder sb = new StringBuilder();
                        sb.append("merchantId=").append(a.b).append("&merchantOrderId=").append(a.c).append("&merchantOrderTime=").append(a.d);
                        a.i = sb.toString();
                        Log.d(a.n, "这是订单验证的3位原串===\n" + a.i);
                        a.j = f2;
                        Log.d(a.n, "这是订单验证的3位签名===\n" + a.j);
                        String str = "<?xml version='1.0' encoding='UTF-8' ?><upomp  application='LanchPay.Req' version='1.0.0' ><merchantId>" + a.b + "</merchantId><merchantOrderId>" + a.c + "</merchantOrderId><merchantOrderTime>" + a.d + "</merchantOrderTime><sign>" + a.j + "</sign></upomp>";
                        Log.d(a.n, "这是订单验证报文===\n" + str);
                        new com.upomp.pay.activity.a();
                        byte[] bytes = str.getBytes();
                        Bundle bundle = new Bundle();
                        bundle.putByteArray("xml", bytes);
                        bundle.putString("action_cmd", a.o);
                        bundle.putBoolean("test", false);
                        PluginHelper.LaunchPlugin(activity, bundle);
                        return;
                    } catch (Exception e) {
                        Log.d("UIHelper", "Exception is " + e);
                        return;
                    }
                default:
                    b(context, "支付有问题！");
                    return;
            }
        }
    }
}
