package com.zhongsou.flymall.c;

import android.content.Context;
import android.graphics.Color;
import android.text.Spanned;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.PolylineOptions;
import com.amap.api.search.core.LatLonPoint;
import com.amap.api.search.route.BusSegment;
import com.amap.api.search.route.DriveSegment;
import com.amap.api.search.route.Route;
import com.amap.api.search.route.Segment;
import com.amap.api.search.route.WalkSegment;
import com.unionpay.upomp.lthj.plugin.ui.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class a implements AMap.InfoWindowAdapter, AMap.OnMarkerClickListener {
    private static int h = 15;
    private AMap a;
    private Route b;
    private LatLng c;
    private LatLng d;
    private Context e;
    private Map<Integer, Marker> f;
    private int g = 0;

    public a(Context context, AMap aMap, Route route) {
        this.e = context;
        this.a = aMap;
        this.b = route;
        this.a.setOnMarkerClickListener(this);
        this.a.setInfoWindowAdapter(this);
        this.c = a(route.getStartPos());
        this.d = a(route.getTargetPos());
        this.f = new HashMap();
    }

    private static LatLng a(LatLonPoint latLonPoint) {
        return new LatLng(latLonPoint.getLatitude(), latLonPoint.getLongitude());
    }

    private static ArrayList<LatLng> a(LatLonPoint[] latLonPointArr) {
        ArrayList<LatLng> arrayList = new ArrayList<>();
        for (LatLonPoint a2 : latLonPointArr) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }

    public final void a() {
        Marker addMarker = this.a.addMarker(new MarkerOptions().position(this.c).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_start)).title("0"));
        addMarker.showInfoWindow();
        this.a.moveCamera(CameraUpdateFactory.newLatLngZoom(this.c, (float) h));
        this.f.put(0, addMarker);
        for (int i = 0; i < this.b.getStepCount(); i++) {
            if (i != 0) {
                Segment step = this.b.getStep(i);
                BitmapDescriptor bitmapDescriptor = null;
                if (step instanceof BusSegment) {
                    bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_bus);
                } else if (step instanceof WalkSegment) {
                    bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_man);
                } else if (step instanceof DriveSegment) {
                    bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_car);
                }
                this.f.put(Integer.valueOf(i), this.a.addMarker(new MarkerOptions().position(a(this.b.getStep(i).getFirstPoint())).icon(bitmapDescriptor).anchor(0.5f, 0.5f).title(new StringBuilder().append(i).toString())));
            }
            this.a.addPolyline(new PolylineOptions().addAll(a(this.b.getStep(i).getShapes())).color(Color.argb(180, 54, 114, 227)).width(10.9f));
        }
        this.f.put(Integer.valueOf(this.b.getStepCount()), this.a.addMarker(new MarkerOptions().position(this.d).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_end)).title(new StringBuilder().append(this.b.getStepCount()).toString())));
    }

    public final void b() {
        this.g = 0;
        this.a.clear();
    }

    public final boolean c() {
        if (this.g > 0) {
            this.g--;
            Marker marker = this.f.get(Integer.valueOf(this.g));
            this.a.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), (float) h));
            marker.showInfoWindow();
        }
        return this.g != 0;
    }

    public final boolean d() {
        if (this.g < this.b.getStepCount()) {
            this.g++;
            Marker marker = this.f.get(Integer.valueOf(this.g));
            this.a.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), (float) h));
            marker.showInfoWindow();
        }
        return this.g != this.b.getStepCount();
    }

    public final View getInfoContents(Marker marker) {
        Spanned a2;
        int parseInt = Integer.parseInt(marker.getTitle());
        Context context = this.e;
        if (parseInt < 0 || parseInt > this.b.getStepCount()) {
            return null;
        }
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundResource(R.drawable.custom_info_bubble);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(2);
        TextView textView = new TextView(context);
        if (parseInt == this.b.getStepCount()) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("到达");
            stringBuffer.append(this.b.getTargetPlace());
            a2 = com.zhongsou.flymall.g.a.a(stringBuffer.toString());
        } else if (this.b.getStep(parseInt) instanceof BusSegment) {
            BusSegment busSegment = (BusSegment) this.b.getStep(parseInt);
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(com.zhongsou.flymall.g.a.a(busSegment.getLineName(), "#000000"));
            stringBuffer2.append(com.zhongsou.flymall.g.a.a());
            stringBuffer2.append(com.zhongsou.flymall.g.a.a(busSegment.getLastStationName(), "#000000"));
            stringBuffer2.append("方向");
            stringBuffer2.append("<br />");
            stringBuffer2.append("上车 : ");
            stringBuffer2.append(com.zhongsou.flymall.g.a.a(busSegment.getOnStationName(), "#000000"));
            stringBuffer2.append(com.zhongsou.flymall.g.a.a());
            stringBuffer2.append("<br />");
            stringBuffer2.append("下车 : ");
            stringBuffer2.append(com.zhongsou.flymall.g.a.a(busSegment.getOffStationName(), "#000000"));
            stringBuffer2.append("<br />");
            stringBuffer2.append(String.format("%s%d%s , ", "公交", Integer.valueOf(busSegment.getStopNumber() - 1), "站"));
            stringBuffer2.append("大约" + com.zhongsou.flymall.g.a.a(busSegment.getLength()));
            a2 = com.zhongsou.flymall.g.a.a(stringBuffer2.toString());
        } else if (this.b.getStep(parseInt) instanceof DriveSegment) {
            DriveSegment driveSegment = (DriveSegment) this.b.getStep(parseInt);
            a2 = com.zhongsou.flymall.g.a.a((com.zhongsou.flymall.g.a.a((com.zhongsou.flymall.g.a.b(driveSegment.getRoadName()) || com.zhongsou.flymall.g.a.b(driveSegment.getActionDescription())) ? driveSegment.getActionDescription() + driveSegment.getRoadName() : driveSegment.getActionDescription() + " --> " + driveSegment.getRoadName(), "#808080") + "<br />") + String.format("%s%s", "大约", com.zhongsou.flymall.g.a.a(driveSegment.getLength())));
        } else if (this.b.getMode() == 23) {
            WalkSegment walkSegment = (WalkSegment) this.b.getStep(parseInt);
            a2 = com.zhongsou.flymall.g.a.a((com.zhongsou.flymall.g.a.a((com.zhongsou.flymall.g.a.b(walkSegment.getRoadName()) || com.zhongsou.flymall.g.a.b(walkSegment.getActionDescription())) ? walkSegment.getActionDescription() + walkSegment.getRoadName() : walkSegment.getActionDescription() + " --> " + walkSegment.getRoadName(), "#808080") + "<br />") + String.format("%s%s", "大约", com.zhongsou.flymall.g.a.a(walkSegment.getLength())));
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("步行去往");
            if (parseInt == this.b.getStepCount() - 1) {
                sb.append(com.zhongsou.flymall.g.a.a("目的地", "#808080"));
            } else {
                sb.append(com.zhongsou.flymall.g.a.a(((BusSegment) this.b.getStep(parseInt + 1)).getLineName() + "车站", "#808080"));
            }
            sb.append("<br />");
            sb.append("大约" + com.zhongsou.flymall.g.a.a(this.b.getStep(parseInt).getLength()));
            a2 = com.zhongsou.flymall.g.a.a(sb.toString());
        }
        String[] split = a2.toString().split("\\n", 2);
        textView.setTextColor(-16777216);
        textView.setText(com.zhongsou.flymall.g.a.a(split[0]));
        textView.setPadding(3, 0, 0, 3);
        linearLayout2.addView(textView, new LinearLayout.LayoutParams(-1, -2));
        TextView textView2 = new TextView(context);
        textView2.setBackgroundColor(Color.rgb(165, 166, 165));
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setOrientation(1);
        TextView textView3 = new TextView(context);
        if (split.length == 2) {
            linearLayout3.addView(textView2, new LinearLayout.LayoutParams(-1, 1));
            textView3.setText(com.zhongsou.flymall.g.a.a(split[1]));
            textView3.setTextColor(Color.rgb(82, 85, 82));
            linearLayout3.addView(textView3, new LinearLayout.LayoutParams(-1, -2));
        }
        LinearLayout linearLayout4 = new LinearLayout(context);
        linearLayout4.setOrientation(0);
        linearLayout4.setGravity(1);
        linearLayout.addView(linearLayout2, new LinearLayout.LayoutParams(-1, -2));
        linearLayout.addView(linearLayout3, new LinearLayout.LayoutParams(-1, 1));
        linearLayout.addView(linearLayout4, new LinearLayout.LayoutParams(-1, -2));
        return linearLayout;
    }

    public final View getInfoWindow(Marker marker) {
        return null;
    }

    public final boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        this.g = Integer.parseInt(marker.getTitle());
        return false;
    }
}
