package com.zhongsou.flymall.c;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import com.zhongsou.flymall.manager.a;

final class g implements DialogInterface.OnClickListener {
    final /* synthetic */ Context a;
    final /* synthetic */ String b;

    g(Context context, String str) {
        this.a = context;
        this.b = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        Log.e(this.a.getClass().getName(), this.b);
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("message/rfc822");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{"houxs@zhongsou.com", "ravegenius1985@126.com", "tiansj@zhongsou.com", "huanglb@zhongsou.com", "min.feng@iftek.cn", "yangzzh@zhongsou.com", "liyc@zhongsou.com", "shisy@zhongsou.com"});
        intent.putExtra("android.intent.extra.SUBJECT", "Android客户端 - 错误报告");
        intent.putExtra("android.intent.extra.TEXT", this.b);
        this.a.startActivity(Intent.createChooser(intent, "发送错误报告"));
        a.a();
        a.a(this.a);
    }
}
