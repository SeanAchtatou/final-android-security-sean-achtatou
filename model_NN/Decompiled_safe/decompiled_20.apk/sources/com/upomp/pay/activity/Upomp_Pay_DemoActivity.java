package com.upomp.pay.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.unionpay.upomp.lthj.plugin.ui.R;
import com.upomp.pay.c.a;

public class Upomp_Pay_DemoActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (intent != null) {
            try {
                Log.d(a.n, "这是支付成功后，回调返回的报文，需自行解析" + new String(intent.getExtras().getByteArray("xml"), "utf-8"));
            } catch (Exception e) {
                Log.d(a.n, "Exception is " + e);
            }
        } else {
            Log.d(a.n, "data is null");
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.test_upomp_pay);
    }
}
