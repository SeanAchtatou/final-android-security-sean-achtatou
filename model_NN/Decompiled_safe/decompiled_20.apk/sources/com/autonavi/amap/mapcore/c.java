package com.autonavi.amap.mapcore;

import java.util.Vector;

class c extends Vector {
    protected int a = -1;

    c() {
    }

    public synchronized Object a() {
        Object elementAt;
        if (b()) {
            elementAt = null;
        } else {
            elementAt = super.elementAt(0);
            super.removeElementAt(0);
        }
        return elementAt;
    }

    public synchronized void a(Object obj) {
        if (this.a > 0 && size() > this.a) {
            a();
        }
        super.addElement(obj);
    }

    public boolean b() {
        return super.isEmpty();
    }

    public synchronized void clear() {
        super.removeAllElements();
    }
}
