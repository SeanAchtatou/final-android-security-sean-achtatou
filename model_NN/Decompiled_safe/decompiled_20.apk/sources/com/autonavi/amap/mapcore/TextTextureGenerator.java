package com.autonavi.amap.mapcore;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Environment;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class TextTextureGenerator {
    private static final int ALIGNCENTER = 51;
    private static final int ALIGNLEFT = 49;
    private static final int ALIGNRIGHT = 50;
    static final int AN_LABEL_MAXCHARINLINE = 7;
    static final int AN_LABEL_MULITYLINE_SPAN = 2;
    static final int TEXT_FONTSIZE = 24;
    static Paint paint = null;
    int nLabelLinesize;
    int nMaxSizePerline;
    int textureHeight;
    int textureWidth;

    public TextTextureGenerator() {
        paint = newPaint(null, TEXT_FONTSIZE, ALIGNLEFT);
    }

    public static int GetNearstSize2N(int i) {
        int i2 = 1;
        while (i > i2) {
            i2 *= 2;
        }
        return i2;
    }

    public static void generaAsccIITexturePng() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), "asccii.png"));
            Paint newPaint = newPaint(null, TEXT_FONTSIZE, ALIGNLEFT);
            Bitmap createBitmap = Bitmap.createBitmap(384, 384, Bitmap.Config.ARGB_8888);
            Paint.FontMetricsInt fontMetricsInt = newPaint.getFontMetricsInt();
            Canvas canvas = new Canvas(createBitmap);
            Paint newPaint2 = newPaint(null, TEXT_FONTSIZE, ALIGNLEFT);
            float[] fArr = new float[1];
            for (int i = 0; i < 16; i++) {
                for (int i2 = 0; i2 < 16; i2++) {
                    char c = (char) ((i * 16) + i2);
                    canvas.drawText(new StringBuilder().append(c).toString(), (float) (i2 * 16), (float) (((i * 16) - fontMetricsInt.ascent) - 2), newPaint2);
                    newPaint2.getTextWidths(new StringBuilder().append(c).toString(), fArr);
                }
            }
            createBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            createBitmap.recycle();
        }
    }

    public static byte[] getCharWidths() {
        Paint newPaint = newPaint(null, TEXT_FONTSIZE, ALIGNLEFT);
        byte[] bArr = new byte[256];
        float[] fArr = new float[1];
        for (int i = 0; i < 256; i++) {
            newPaint.getTextWidths(new StringBuilder().append((char) i).toString(), fArr);
            bArr[i] = (byte) ((int) fArr[0]);
        }
        return bArr;
    }

    private float getFullWidth(float[] fArr) {
        float f = BitmapDescriptorFactory.HUE_RED;
        for (float f2 : fArr) {
            f += f2;
        }
        return f;
    }

    private byte[] getPixels(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        byte[] bArr = new byte[(bitmap.getWidth() * bitmap.getHeight() * 4)];
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        wrap.order(ByteOrder.nativeOrder());
        bitmap.copyPixelsToBuffer(wrap);
        return bArr;
    }

    private float getStringWidth(String str) {
        float f;
        float[] fArr = new float[1];
        float f2 = 0.0f;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt <= 0 || charAt >= 256) {
                f = 24.0f;
            } else {
                paint.getTextWidths(new StringBuilder().append(charAt).toString(), fArr);
                f = fArr[0];
            }
            f2 += f;
        }
        return f2;
    }

    private static Paint newPaint(String str, int i, int i2) {
        Paint paint2 = new Paint();
        paint2.setColor(-1);
        paint2.setTextSize((float) i);
        paint2.setAntiAlias(true);
        paint2.setTypeface(Typeface.DEFAULT);
        switch (i2) {
            case ALIGNLEFT /*49*/:
                paint2.setTextAlign(Paint.Align.LEFT);
                break;
            case ALIGNRIGHT /*50*/:
                paint2.setTextAlign(Paint.Align.RIGHT);
                break;
            case ALIGNCENTER /*51*/:
                paint2.setTextAlign(Paint.Align.CENTER);
                break;
            default:
                paint2.setTextAlign(Paint.Align.LEFT);
                break;
        }
        return paint2;
    }

    public byte[] getTextPixelBuffer(int i) {
        Bitmap createBitmap = Bitmap.createBitmap(TEXT_FONTSIZE, TEXT_FONTSIZE, Bitmap.Config.ALPHA_8);
        Canvas canvas = new Canvas(createBitmap);
        ByteBuffer allocate = ByteBuffer.allocate(576);
        int i2 = (-paint.getFontMetricsInt().ascent) - 1;
        createBitmap.copyPixelsFromBuffer(allocate);
        canvas.drawText(new char[]{(char) i}, 0, 1, (float) BitmapDescriptorFactory.HUE_RED, (float) i2, paint);
        byte[] pixels = getPixels(createBitmap);
        createBitmap.recycle();
        return pixels;
    }
}
