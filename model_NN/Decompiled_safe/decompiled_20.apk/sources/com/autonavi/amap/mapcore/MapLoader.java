package com.autonavi.amap.mapcore;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.util.ArrayList;

public class MapLoader {
    long createtime;
    int datasource = 0;
    boolean inRequest = false;
    boolean mCanceled = false;
    BaseMapCallImplement mMapCallback;
    MapCore mMapCore;
    int mapLevel;
    public ArrayList<MapSourceGridData> mapTiles = new ArrayList<>();
    int nextImgDataLength = 0;
    byte[] recievedDataBuffer;
    int recievedDataSize = 0;
    boolean recievedHeader = false;

    public MapLoader(BaseMapCallImplement baseMapCallImplement, MapCore mapCore, int i) {
        this.mMapCallback = baseMapCallImplement;
        this.datasource = i;
        this.mMapCore = mapCore;
        this.createtime = System.currentTimeMillis();
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0099 A[SYNTHETIC, Splitter:B:33:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b1 A[SYNTHETIC, Splitter:B:44:0x00b1] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processRecivedData() {
        /*
            r9 = this;
            r8 = 4
            r7 = 8
            r6 = 0
        L_0x0004:
            int r0 = r9.nextImgDataLength
            if (r0 != 0) goto L_0x0017
            int r0 = r9.recievedDataSize
            if (r0 < r7) goto L_0x00b7
            byte[] r0 = r9.recievedDataBuffer
            int r0 = com.autonavi.amap.mapcore.Convert.getInt(r0, r6)
            int r0 = r0 + 8
            r9.nextImgDataLength = r0
            goto L_0x0004
        L_0x0017:
            int r0 = r9.recievedDataSize
            int r1 = r9.nextImgDataLength
            if (r0 < r1) goto L_0x00b7
            byte[] r0 = r9.recievedDataBuffer
            int r0 = com.autonavi.amap.mapcore.Convert.getInt(r0, r6)
            byte[] r1 = r9.recievedDataBuffer
            int r3 = com.autonavi.amap.mapcore.Convert.getInt(r1, r8)
            if (r3 != 0) goto L_0x0070
            int r1 = r9.datasource
            r2 = 2
            if (r1 != r2) goto L_0x004f
            byte[] r1 = r9.recievedDataBuffer
            int r0 = r0 + 8
            r9.processRecivedTileDataBmp(r1, r7, r0)
        L_0x0037:
            byte[] r0 = r9.recievedDataBuffer
            int r1 = r9.nextImgDataLength
            byte[] r2 = r9.recievedDataBuffer
            int r3 = r9.recievedDataSize
            int r4 = r9.nextImgDataLength
            int r3 = r3 - r4
            com.autonavi.amap.mapcore.Convert.moveArray(r0, r1, r2, r6, r3)
            int r0 = r9.recievedDataSize
            int r1 = r9.nextImgDataLength
            int r0 = r0 - r1
            r9.recievedDataSize = r0
            r9.nextImgDataLength = r6
            goto L_0x0004
        L_0x004f:
            int r1 = r9.datasource
            if (r1 != r8) goto L_0x005b
            byte[] r1 = r9.recievedDataBuffer
            int r0 = r0 + 8
            r9.processRecivedTileDataVTmc(r1, r7, r0)
            goto L_0x0037
        L_0x005b:
            int r1 = r9.datasource
            r2 = 6
            if (r1 != r2) goto L_0x0068
            byte[] r1 = r9.recievedDataBuffer
            int r0 = r0 + 8
            r9.processRecivedModels(r1, r7, r0)
            goto L_0x0037
        L_0x0068:
            byte[] r1 = r9.recievedDataBuffer
            int r0 = r0 + 8
            r9.processRecivedTileData(r1, r7, r0)
            goto L_0x0037
        L_0x0070:
            r2 = 0
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x00ba, all -> 0x00ad }
            byte[] r1 = r9.recievedDataBuffer     // Catch:{ Exception -> 0x00ba, all -> 0x00ad }
            r5 = 8
            r4.<init>(r1, r5, r0)     // Catch:{ Exception -> 0x00ba, all -> 0x00ad }
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x00ba, all -> 0x00ad }
            r1.<init>(r4)     // Catch:{ Exception -> 0x00ba, all -> 0x00ad }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0093 }
            r0.<init>()     // Catch:{ Exception -> 0x0093 }
            r2 = 128(0x80, float:1.794E-43)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0093 }
        L_0x0088:
            int r4 = r1.read(r2)     // Catch:{ Exception -> 0x0093 }
            if (r4 < 0) goto L_0x009f
            r5 = 0
            r0.write(r2, r5, r4)     // Catch:{ Exception -> 0x0093 }
            goto L_0x0088
        L_0x0093:
            r0 = move-exception
        L_0x0094:
            r0.printStackTrace()     // Catch:{ all -> 0x00b8 }
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ Exception -> 0x009d }
            goto L_0x0037
        L_0x009d:
            r0 = move-exception
            goto L_0x0037
        L_0x009f:
            byte[] r0 = r0.toByteArray()     // Catch:{ Exception -> 0x0093 }
            r2 = 0
            r9.processRecivedTileData(r0, r2, r3)     // Catch:{ Exception -> 0x0093 }
            r1.close()     // Catch:{ Exception -> 0x00ab }
            goto L_0x0037
        L_0x00ab:
            r0 = move-exception
            goto L_0x0037
        L_0x00ad:
            r0 = move-exception
            r1 = r2
        L_0x00af:
            if (r1 == 0) goto L_0x00b4
            r1.close()     // Catch:{ Exception -> 0x00b5 }
        L_0x00b4:
            throw r0
        L_0x00b5:
            r1 = move-exception
            goto L_0x00b4
        L_0x00b7:
            return
        L_0x00b8:
            r0 = move-exception
            goto L_0x00af
        L_0x00ba:
            r0 = move-exception
            r1 = r2
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.amap.mapcore.MapLoader.processRecivedData():void");
    }

    public void OnException(int i) {
        if (this.datasource != 4 && this.datasource != 1) {
            this.mMapCallback.OnMapLoaderError(i);
        }
    }

    public void addReuqestTiles(MapSourceGridData mapSourceGridData) {
        this.mapTiles.add(mapSourceGridData);
    }

    public void destory() {
        this.mapTiles.clear();
        this.mapTiles = null;
        this.recievedDataBuffer = null;
        VTMCDataCache.vtmcHs.clear();
        VTMCDataCache.vtmcList.clear();
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v13, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v14, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v15, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doRequest() {
        /*
            r7 = this;
            r4 = 1
            r2 = 0
            r1 = 0
            r6 = 1002(0x3ea, float:1.404E-42)
            r7.inRequest = r4
            boolean r0 = r7.isRequestValid()
            if (r0 != 0) goto L_0x0011
            r7.onConnectionOver(r7)
        L_0x0010:
            return
        L_0x0011:
            com.autonavi.amap.mapcore.BaseMapCallImplement r0 = r7.mMapCallback
            android.content.Context r0 = r0.getContext()
            java.lang.String r3 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r3)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()
            if (r0 == 0) goto L_0x011f
            int r0 = r0.getType()
            if (r0 != r4) goto L_0x00e0
            com.autonavi.amap.mapcore.BaseMapCallImplement r0 = r7.mMapCallback
            android.content.Context r0 = r0.getContext()
            android.net.Proxy.getHost(r0)
            com.autonavi.amap.mapcore.BaseMapCallImplement r0 = r7.mMapCallback
            android.content.Context r0 = r0.getContext()
            android.net.Proxy.getPort(r0)
            r0 = r1
            r3 = r1
        L_0x003f:
            org.apache.http.params.BasicHttpParams r4 = new org.apache.http.params.BasicHttpParams     // Catch:{ IOException -> 0x00ca }
            r4.<init>()     // Catch:{ IOException -> 0x00ca }
            r5 = 20000(0x4e20, float:2.8026E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r4, r5)     // Catch:{ IOException -> 0x00ca }
            r5 = 20000(0x4e20, float:2.8026E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r4, r5)     // Catch:{ IOException -> 0x00ca }
            org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IOException -> 0x00ca }
            r5.<init>(r4)     // Catch:{ IOException -> 0x00ca }
            if (r0 == 0) goto L_0x0063
            org.apache.http.HttpHost r0 = new org.apache.http.HttpHost     // Catch:{ IOException -> 0x00ca }
            r0.<init>(r3, r2)     // Catch:{ IOException -> 0x00ca }
            org.apache.http.params.HttpParams r2 = r5.getParams()     // Catch:{ IOException -> 0x00ca }
            java.lang.String r3 = "http.route.default-proxy"
            r2.setParameter(r3, r0)     // Catch:{ IOException -> 0x00ca }
        L_0x0063:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ca }
            r0.<init>()     // Catch:{ IOException -> 0x00ca }
            java.lang.String r2 = r7.getGridParma()     // Catch:{ IOException -> 0x00ca }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x00ca }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00ca }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ca }
            r2.<init>()     // Catch:{ IOException -> 0x00ca }
            com.autonavi.amap.mapcore.BaseMapCallImplement r3 = r7.mMapCallback     // Catch:{ IOException -> 0x00ca }
            java.lang.String r3 = r3.getMapSvrAddress()     // Catch:{ IOException -> 0x00ca }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x00ca }
            java.lang.String r3 = "/amapsrv/MPS?"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x00ca }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ IOException -> 0x00ca }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00ca }
            org.apache.http.client.methods.HttpGet r2 = new org.apache.http.client.methods.HttpGet     // Catch:{ IOException -> 0x00ca }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00ca }
            org.apache.http.HttpResponse r0 = r5.execute(r2)     // Catch:{ IOException -> 0x00ca }
            r2 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r3 = r0.getStatusLine()     // Catch:{ IOException -> 0x00ca }
            int r3 = r3.getStatusCode()     // Catch:{ IOException -> 0x00ca }
            if (r2 != r3) goto L_0x00f8
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ IOException -> 0x00ca }
            java.io.InputStream r1 = r0.getContent()     // Catch:{ IOException -> 0x00ca }
            r7.onConnectionOpened(r7)     // Catch:{ IOException -> 0x00ca }
            r0 = 2048(0x800, float:2.87E-42)
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x00ca }
        L_0x00b5:
            int r2 = r1.read(r0)     // Catch:{ IOException -> 0x00ca }
            if (r2 < 0) goto L_0x00fd
            boolean r3 = r7.isRequestValid()     // Catch:{ IOException -> 0x00ca }
            if (r3 == 0) goto L_0x00fd
            boolean r3 = r7.mCanceled     // Catch:{ IOException -> 0x00ca }
            if (r3 != 0) goto L_0x00fd
            r3 = 0
            r7.onConnectionRecieveData(r7, r3, r0, r2)     // Catch:{ IOException -> 0x00ca }
            goto L_0x00b5
        L_0x00ca:
            r0 = move-exception
            r0 = 1002(0x3ea, float:1.404E-42)
            r7.OnException(r0)     // Catch:{ all -> 0x010d }
            r7.onConnectionOver(r7)
            if (r1 == 0) goto L_0x0010
            r1.close()     // Catch:{ IOException -> 0x00da }
            goto L_0x0010
        L_0x00da:
            r0 = move-exception
            r7.OnException(r6)
            goto L_0x0010
        L_0x00e0:
            java.lang.String r3 = android.net.Proxy.getDefaultHost()
            int r2 = android.net.Proxy.getDefaultPort()
            if (r3 == 0) goto L_0x011c
            java.net.Proxy r0 = new java.net.Proxy
            java.net.Proxy$Type r4 = java.net.Proxy.Type.HTTP
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress
            r5.<init>(r3, r2)
            r0.<init>(r4, r5)
            goto L_0x003f
        L_0x00f8:
            r0 = 1002(0x3ea, float:1.404E-42)
            r7.OnException(r0)     // Catch:{ IOException -> 0x00ca }
        L_0x00fd:
            r7.onConnectionOver(r7)
            if (r1 == 0) goto L_0x0010
            r1.close()     // Catch:{ IOException -> 0x0107 }
            goto L_0x0010
        L_0x0107:
            r0 = move-exception
            r7.OnException(r6)
            goto L_0x0010
        L_0x010d:
            r0 = move-exception
            r7.onConnectionOver(r7)
            if (r1 == 0) goto L_0x0116
            r1.close()     // Catch:{ IOException -> 0x0117 }
        L_0x0116:
            throw r0
        L_0x0117:
            r1 = move-exception
            r7.OnException(r6)
            goto L_0x0116
        L_0x011c:
            r0 = r1
            goto L_0x003f
        L_0x011f:
            r0 = r1
            r3 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.amap.mapcore.MapLoader.doRequest():void");
    }

    public String getGridParma() {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.mapTiles.size()) {
                break;
            }
            stringBuffer.append(this.mapTiles.get(i2).getGridName() + ";");
            i = i2 + 1;
        }
        if (stringBuffer.length() <= 0) {
            return null;
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        if (this.datasource == 0) {
            return "t=VMMV3&cp=1&mesh=" + stringBuffer.toString();
        }
        if (this.datasource == 1) {
            return "t=VMMBLDV3&cp=1&mesh=" + stringBuffer.toString();
        }
        if (this.datasource == 2) {
            return "t=BMPBM&mesh=" + stringBuffer.toString();
        }
        if (this.datasource == 3) {
            return "t=BMTI&mesh=" + stringBuffer.toString();
        }
        if (this.datasource == 4) {
            return "t=TMCV&mesh=" + stringBuffer.toString();
        }
        if (this.datasource == 6) {
            return "t=VMMV3&type=mod&cp=0&mid=" + stringBuffer.toString();
        }
        return null;
    }

    public boolean isRequestValid() {
        return this.mMapCallback.isGridsInScreen(this.mapTiles, this.datasource);
    }

    public void onConnectionError(MapLoader mapLoader, int i, String str) {
    }

    public void onConnectionOpened(MapLoader mapLoader) {
        this.recievedDataBuffer = new byte[AccessibilityEventCompat.TYPE_GESTURE_DETECTION_START];
        this.nextImgDataLength = 0;
        this.recievedDataSize = 0;
        this.recievedHeader = false;
    }

    public void onConnectionOver(MapLoader mapLoader) {
        int i = 0;
        this.recievedDataBuffer = null;
        this.nextImgDataLength = 0;
        this.recievedDataSize = 0;
        if (this.mMapCallback != null && this.mMapCallback.tileProcessCtrl != null && mapLoader != null && mapLoader.mapTiles != null) {
            while (true) {
                int i2 = i;
                if (i2 < mapLoader.mapTiles.size()) {
                    if (mapLoader.mapTiles.get(i2) != null) {
                        this.mMapCallback.tileProcessCtrl.a(mapLoader.mapTiles.get(i2).getKeyGridName());
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void onConnectionRecieveData(MapLoader mapLoader, int i, byte[] bArr, int i2) {
        if (mapLoader != null && bArr != null) {
            System.arraycopy(bArr, 0, this.recievedDataBuffer, this.recievedDataSize, i2);
            this.recievedDataSize += i2;
            if (!this.recievedHeader) {
                if (this.recievedDataSize <= 7) {
                    return;
                }
                if (Convert.getInt(this.recievedDataBuffer, 0) != 0) {
                    mapLoader.mCanceled = true;
                    return;
                }
                Convert.getInt(this.recievedDataBuffer, 4);
                Convert.moveArray(this.recievedDataBuffer, 8, this.recievedDataBuffer, 0, i2 - 8);
                this.recievedDataSize -= 8;
                this.nextImgDataLength = 0;
                this.recievedHeader = true;
                processRecivedData();
            }
            processRecivedData();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* access modifiers changed from: package-private */
    public void processRecivedModels(byte[] bArr, int i, int i2) {
        new String(bArr, i + 1, (int) bArr[i]);
        if (this.mMapCallback.isMapEngineValid()) {
            System.arraycopy(bArr, i, new byte[(i2 - i)], 0, i2 - i);
            this.mMapCore.putMapData(bArr, i, i2 - i, this.datasource);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* access modifiers changed from: package-private */
    public void processRecivedTileData(byte[] bArr, int i, int i2) {
        Convert.getShort(this.recievedDataBuffer, i);
        int i3 = i + 2;
        Convert.getShort(this.recievedDataBuffer, i3);
        int i4 = i3 + 2;
        Convert.getInt(this.recievedDataBuffer, i4);
        int i5 = i4 + 4;
        new String(bArr, i5 + 1, (int) bArr[i5]);
        if (this.mMapCallback.isMapEngineValid()) {
            this.mMapCore.putMapData(bArr, i, i2 - i, this.datasource);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* access modifiers changed from: package-private */
    public void processRecivedTileDataBmp(byte[] bArr, int i, int i2) {
        Convert.getInt(this.recievedDataBuffer, i);
        int i3 = i + 4;
        new String(bArr, i3 + 1, (int) bArr[i3]);
        if (this.mMapCallback.isMapEngineValid()) {
            this.mMapCore.putMapData(bArr, i, i2 - i, this.datasource);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* access modifiers changed from: package-private */
    public void processRecivedTileDataVTmc(byte[] bArr, int i, int i2) {
        Convert.getInt(this.recievedDataBuffer, i);
        int i3 = i + 4;
        String str = new String(bArr, i3 + 1, (int) bArr[i3]);
        if (this.mMapCallback.isMapEngineValid()) {
            VTMCDataCache instance = VTMCDataCache.getInstance();
            byte[] bArr2 = new byte[(i2 - i)];
            System.arraycopy(bArr, i, bArr2, 0, i2 - i);
            instance.putData(str, bArr2);
            this.mMapCore.putMapData(bArr, i, i2 - i, this.datasource);
        }
    }
}
