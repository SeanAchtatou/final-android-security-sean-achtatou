package com.autonavi.amap.mapcore;

public class MapProjection {
    private boolean m_bNewInstance = false;
    int native_instance = 0;

    static {
        try {
            System.loadLibrary("amapv3");
        } catch (Exception e) {
        }
    }

    public MapProjection(int i) {
        this.native_instance = i;
        this.m_bNewInstance = false;
    }

    public MapProjection(MapCore mapCore) {
        this.native_instance = nativeCreate(mapCore.getInstanceHandle());
        this.m_bNewInstance = true;
    }

    public static void geo2LonLat(int i, int i2, DPoint dPoint) {
        nativeGeo2LonLat(i, i2, dPoint);
    }

    public static void lonlat2Geo(double d, double d2, IPoint iPoint) {
        nativeLonLat2Geo(d, d2, iPoint);
    }

    private static native int nativeCreate(int i);

    private static native void nativeDestroy(int i);

    private static native void nativeGeo2LonLat(int i, int i2, DPoint dPoint);

    private static native void nativeGeo2Map(int i, int i2, int i3, FPoint fPoint);

    private static native void nativeGetBound(int i, IPoint iPoint);

    private static native float nativeGetCameraHeaderAngle(int i);

    private static native void nativeGetCenterMap(int i, FPoint fPoint);

    private static native void nativeGetGeoCenter(int i, IPoint iPoint);

    private static native float nativeGetMapAngle(int i);

    private static native void nativeGetMapCenter(int i, FPoint fPoint);

    private static native float nativeGetMapLenWithWin(int i, int i2);

    private static native float nativeGetMapLenWithWinbyY(int i, int i2, int i3);

    private static native float nativeGetMapZoomer(int i);

    private static native void nativeLonLat2Geo(double d, double d2, IPoint iPoint);

    private static native void nativeMap2Geo(int i, float f, float f2, IPoint iPoint);

    private static native void nativeMap2Win(int i, float f, float f2, IPoint iPoint);

    private static native void nativeSetCameraHeaderAngle(int i, float f);

    private static native void nativeSetCenterWithMap(int i, float f, float f2);

    private static native void nativeSetGeoCenter(int i, int i2, int i3);

    private static native void nativeSetMapAngle(int i, float f);

    private static native void nativeSetMapCenter(int i, float f, float f2);

    private static native void nativeSetMapZoomer(int i, float f);

    private static native void nativeWin2Map(int i, int i2, int i3, FPoint fPoint);

    public void finalize() {
        if (this.m_bNewInstance) {
            nativeDestroy(this.native_instance);
        }
    }

    public void geo2Map(int i, int i2, FPoint fPoint) {
        nativeGeo2Map(this.native_instance, i, i2, fPoint);
    }

    public void getBound(IPoint iPoint) {
        nativeGetBound(this.native_instance, iPoint);
    }

    public float getCameraHeaderAngle() {
        return nativeGetCameraHeaderAngle(this.native_instance);
    }

    public void getCenterMap(FPoint fPoint) {
        nativeGetCenterMap(this.native_instance, fPoint);
    }

    public void getGeoCenter(IPoint iPoint) {
        nativeGetGeoCenter(this.native_instance, iPoint);
    }

    /* access modifiers changed from: package-private */
    public int getInstanceHandle() {
        return this.native_instance;
    }

    public float getMapAngle() {
        return nativeGetMapAngle(this.native_instance);
    }

    public void getMapCenter(FPoint fPoint) {
        nativeGetMapCenter(this.native_instance, fPoint);
    }

    public float getMapLenWithWin(int i) {
        return nativeGetMapLenWithWin(this.native_instance, i);
    }

    public float getMapLenWithWinbyY(int i, int i2) {
        return nativeGetMapLenWithWinbyY(this.native_instance, i, i2);
    }

    public float getMapZoomer() {
        return nativeGetMapZoomer(this.native_instance);
    }

    public void map2Geo(float f, float f2, IPoint iPoint) {
        nativeMap2Geo(this.native_instance, f, f2, iPoint);
    }

    public void map2Win(float f, float f2, IPoint iPoint) {
        nativeMap2Win(this.native_instance, f, f2, iPoint);
    }

    public void setCameraHeaderAngle(float f) {
        nativeSetCameraHeaderAngle(this.native_instance, f);
    }

    public void setCenterWithMap(float f, float f2) {
        nativeSetCenterWithMap(this.native_instance, f, f2);
    }

    public void setGeoCenter(int i, int i2) {
        nativeSetGeoCenter(this.native_instance, i, i2);
    }

    public void setMapAngle(float f) {
        nativeSetMapAngle(this.native_instance, f);
    }

    public void setMapCenter(float f, float f2) {
        nativeSetMapCenter(this.native_instance, f, f2);
    }

    public void setMapZoomer(float f) {
        nativeSetMapZoomer(this.native_instance, f);
    }

    public void win2Map(int i, int i2, FPoint fPoint) {
        nativeWin2Map(this.native_instance, i, i2, fPoint);
    }
}
