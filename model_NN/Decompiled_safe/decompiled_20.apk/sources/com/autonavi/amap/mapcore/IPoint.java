package com.autonavi.amap.mapcore;

public class IPoint {
    public int x;
    public int y;

    public IPoint() {
    }

    public IPoint(int i, int i2) {
        this.x = i;
        this.y = i2;
    }
}
