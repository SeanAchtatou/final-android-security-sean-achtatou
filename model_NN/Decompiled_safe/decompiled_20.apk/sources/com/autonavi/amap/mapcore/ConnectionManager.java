package com.autonavi.amap.mapcore;

public class ConnectionManager extends Thread {
    c connectionPool = new c();
    int sleepTime = 30;
    boolean threadFlag = true;

    public ConnectionManager() {
        super("ConnectionManager");
    }

    public synchronized void addConntionTask(MapLoader mapLoader) {
        this.connectionPool.a(mapLoader);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.threadFlag = false;
        if (this.connectionPool != null) {
            this.connectionPool.clear();
        }
        interrupt();
    }

    public synchronized int getTaskCount() {
        return this.connectionPool.size();
    }

    public synchronized void insertConntionTask(MapLoader mapLoader) {
        this.connectionPool.insertElementAt(mapLoader, 0);
    }

    public synchronized boolean isEmptyTask() {
        return this.connectionPool.size() == 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        sleep((long) r5.sleepTime);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
        L_0x0000:
            boolean r0 = r5.threadFlag
            if (r0 == 0) goto L_0x004c
        L_0x0004:
            boolean r0 = r5.threadFlag
            if (r0 == 0) goto L_0x0012
            boolean r0 = r5.isInterrupted()
            if (r0 != 0) goto L_0x0012
            com.autonavi.amap.mapcore.c r0 = r5.connectionPool
            if (r0 != 0) goto L_0x0022
        L_0x0012:
            r5.interrupt()     // Catch:{ Exception -> 0x001e }
        L_0x0015:
            int r0 = r5.sleepTime     // Catch:{ Exception -> 0x001c }
            long r0 = (long) r0     // Catch:{ Exception -> 0x001c }
            sleep(r0)     // Catch:{ Exception -> 0x001c }
            goto L_0x0000
        L_0x001c:
            r0 = move-exception
            goto L_0x0000
        L_0x001e:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0022:
            com.autonavi.amap.mapcore.c r0 = r5.connectionPool
            java.lang.Object r0 = r0.a()
            com.autonavi.amap.mapcore.MapLoader r0 = (com.autonavi.amap.mapcore.MapLoader) r0
            if (r0 == 0) goto L_0x0015
        L_0x002c:
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = r0.createtime
            long r1 = r1 - r3
            r3 = 50
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x003d
            r0.doRequest()
            goto L_0x0004
        L_0x003d:
            r1 = 10
            sleep(r1)     // Catch:{ Exception -> 0x0047 }
        L_0x0042:
            boolean r1 = r5.threadFlag
            if (r1 != 0) goto L_0x002c
            goto L_0x0004
        L_0x0047:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0042
        L_0x004c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.amap.mapcore.ConnectionManager.run():void");
    }
}
