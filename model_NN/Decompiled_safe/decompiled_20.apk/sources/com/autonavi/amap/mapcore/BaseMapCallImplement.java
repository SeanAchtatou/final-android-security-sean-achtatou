package com.autonavi.amap.mapcore;

import com.amap.api.a.b.e;
import java.util.ArrayList;

public abstract class BaseMapCallImplement implements IBaseMapCallback, IMapCallback {
    private ArrayList<MapSourceGridData> baseMapGrids = new ArrayList<>();
    private ArrayList<MapSourceGridData> bldMapGrids = new ArrayList<>();
    private ArrayList<MapSourceGridData> bmpbmMapGirds = new ArrayList<>();
    private ConnectionManager connectionManager = null;
    c connectionPool;
    private ArrayList<MapSourceGridData> mapModels = new ArrayList<>();
    private ArrayList<MapSourceGridData> screenGirds = new ArrayList<>();
    private ArrayList<MapSourceGridData> stiMapGirds = new ArrayList<>();
    d tileProcessCtrl = null;
    private ArrayList<MapSourceGridData> vectmcMapGirds = new ArrayList<>();

    private void destory() {
        this.baseMapGrids.clear();
        this.bldMapGrids.clear();
        this.bmpbmMapGirds.clear();
        this.vectmcMapGirds.clear();
        this.stiMapGirds.clear();
        this.mapModels.clear();
        this.screenGirds.clear();
    }

    private void proccessRequiredData(MapCore mapCore, ArrayList<MapSourceGridData> arrayList, int i) {
        ArrayList arrayList2 = new ArrayList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < arrayList.size()) {
                MapSourceGridData mapSourceGridData = arrayList.get(i3);
                if (this.tileProcessCtrl != null && mapSourceGridData != null) {
                    if (!this.tileProcessCtrl.b(mapSourceGridData.getKeyGridName())) {
                        arrayList2.add(mapSourceGridData);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            } else if (arrayList2.size() > 0) {
                sendMapDataRequest(mapCore, arrayList2, i);
                return;
            } else {
                return;
            }
        }
    }

    private void sendMapDataRequest(MapCore mapCore, ArrayList<MapSourceGridData> arrayList, int i) {
        if (arrayList.size() != 0) {
            MapLoader mapLoader = new MapLoader(this, mapCore, i);
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < arrayList.size()) {
                    MapSourceGridData mapSourceGridData = arrayList.get(i3);
                    if (this.tileProcessCtrl != null) {
                        if (!this.tileProcessCtrl.b(mapSourceGridData.getKeyGridName())) {
                            this.tileProcessCtrl.c(mapSourceGridData.getKeyGridName());
                            mapLoader.addReuqestTiles(mapSourceGridData);
                        }
                        i2 = i3 + 1;
                    } else {
                        return;
                    }
                } else if (this.connectionManager != null) {
                    this.connectionManager.insertConntionTask(mapLoader);
                    return;
                } else {
                    return;
                }
            }
        }
    }

    public void OnMapDataRequired(MapCore mapCore, int i, String[] strArr) {
        ArrayList<MapSourceGridData> arrayList;
        switch (i) {
            case 0:
                arrayList = this.baseMapGrids;
                break;
            case 1:
                arrayList = this.bldMapGrids;
                break;
            case 2:
                arrayList = this.bmpbmMapGirds;
                break;
            case 3:
                arrayList = this.stiMapGirds;
                break;
            case 4:
                arrayList = this.vectmcMapGirds;
                break;
            case 5:
                arrayList = this.screenGirds;
                break;
            case 6:
                arrayList = this.mapModels;
                break;
            default:
                arrayList = null;
                break;
        }
        if (arrayList != null) {
            arrayList.clear();
            for (String mapSourceGridData : strArr) {
                arrayList.add(new MapSourceGridData(mapSourceGridData, i));
            }
            if (5 != i) {
                proccessRequiredData(mapCore, arrayList, i);
            }
        }
    }

    public void OnMapDestory(MapCore mapCore) {
        destory();
        onPause();
    }

    public void OnMapLabelsRequired(MapCore mapCore, int[] iArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = iArr[i2];
            byte[] textPixelBuffer = new TextTextureGenerator().getTextPixelBuffer(i3);
            if (textPixelBuffer != null) {
                mapCore.putCharbitmap(i3, textPixelBuffer);
            }
        }
    }

    public void OnMapSurfaceCreate(MapCore mapCore) {
        onResume();
    }

    public ArrayList<MapSourceGridData> getScreenGridList(int i) {
        switch (i) {
            case 0:
                return this.baseMapGrids;
            case 1:
                return this.bldMapGrids;
            case 2:
                return this.bmpbmMapGirds;
            case 3:
                return this.stiMapGirds;
            case 4:
                return this.vectmcMapGirds;
            case 5:
                return this.screenGirds;
            case 6:
                return this.mapModels;
            default:
                return null;
        }
    }

    public boolean isGridInList(MapSourceGridData mapSourceGridData, ArrayList<MapSourceGridData> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getGridName().equals(mapSourceGridData.getGridName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isGridsInScreen(ArrayList<MapSourceGridData> arrayList, int i) {
        try {
            if (arrayList.size() == 0) {
                return false;
            }
            if (!isMapEngineValid()) {
                return false;
            }
            ArrayList<MapSourceGridData> screenGridList = getScreenGridList(i);
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                if (isGridInList(arrayList.get(i2), screenGridList)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public void onPause() {
        try {
            if (this.connectionManager != null) {
                this.connectionManager.threadFlag = false;
                try {
                    this.connectionPool = (c) this.connectionManager.connectionPool.clone();
                    e.a("onPause", "connectionPool=" + this.connectionPool.size(), 111);
                    this.connectionManager.connectionPool.clear();
                    this.connectionManager.interrupt();
                    this.connectionManager = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (this.tileProcessCtrl != null) {
                this.tileProcessCtrl.c = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onResume() {
        try {
            if (this.tileProcessCtrl == null) {
                this.tileProcessCtrl = new d();
            } else {
                this.tileProcessCtrl.c = true;
            }
            if (this.connectionManager == null) {
                this.connectionManager = new ConnectionManager();
                if (this.connectionPool != null) {
                    this.connectionManager.connectionPool = this.connectionPool;
                }
                this.connectionManager.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
