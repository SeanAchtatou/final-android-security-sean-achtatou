package com.autonavi.aps.amapapi;

import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.search.poisearch.PoiTypeDef;

public class AmapLoc {
    private float accuracy = BitmapDescriptorFactory.HUE_RED;
    private String adcode = PoiTypeDef.All;
    private float bearing = BitmapDescriptorFactory.HUE_RED;
    private String city = PoiTypeDef.All;
    private String citycode = PoiTypeDef.All;
    private String country = PoiTypeDef.All;
    private String desc = PoiTypeDef.All;
    private double lat = 0.0d;
    private double lon = 0.0d;
    private String poiname = PoiTypeDef.All;
    private String provider = PoiTypeDef.All;
    private String province = PoiTypeDef.All;
    private String retype = PoiTypeDef.All;
    private String road = PoiTypeDef.All;
    private float speed = BitmapDescriptorFactory.HUE_RED;
    private String street = PoiTypeDef.All;
    private long time = 0;
    private String type = "new";

    public float getAccuracy() {
        return this.accuracy;
    }

    public String getAdcode() {
        return this.adcode;
    }

    public float getBearing() {
        return this.bearing;
    }

    public String getCity() {
        return this.city;
    }

    public String getCitycode() {
        return this.citycode;
    }

    public String getCountry() {
        return this.country;
    }

    public String getDesc() {
        return this.desc;
    }

    public double getLat() {
        return this.lat;
    }

    public double getLon() {
        return this.lon;
    }

    public String getPoiname() {
        return this.poiname;
    }

    public String getProvider() {
        return this.provider;
    }

    public String getProvince() {
        return this.province;
    }

    public String getRetype() {
        return this.retype;
    }

    public String getRoad() {
        return this.road;
    }

    public float getSpeed() {
        return this.speed;
    }

    public String getStreet() {
        return this.street;
    }

    public long getTime() {
        return this.time;
    }

    public String getType() {
        return this.type;
    }

    public void setAccuracy(float f) {
        this.accuracy = f;
    }

    public void setAdcode(String str) {
        this.adcode = str;
    }

    public void setBearing(float f) {
        this.bearing = f;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public void setCitycode(String str) {
        this.citycode = str;
    }

    public void setCountry(String str) {
        this.country = str;
    }

    public void setDesc(String str) {
        this.desc = str;
    }

    public void setLat(double d) {
        this.lat = d;
    }

    public void setLon(double d) {
        this.lon = d;
    }

    public void setPoiname(String str) {
        this.poiname = str;
    }

    public void setProvider(String str) {
        this.provider = str;
    }

    public void setProvince(String str) {
        this.province = str;
    }

    public void setRetype(String str) {
        this.retype = str;
    }

    public void setRoad(String str) {
        this.road = str;
    }

    public void setSpeed(float f) {
        this.speed = f;
    }

    public void setStreet(String str) {
        this.street = str;
    }

    public void setTime(long j) {
        this.time = j;
    }

    public void setType(String str) {
        this.type = str;
    }
}
