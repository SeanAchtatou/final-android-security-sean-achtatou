package com.autonavi.aps.amapapi;

import android.location.Location;
import android.net.wifi.ScanResult;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.fb.f;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public final class g {
    private static DecimalFormat r = null;
    private static SimpleDateFormat s = null;
    public int a = 9;
    public String b = "888888888888888";
    public String c = "888888888888888";
    public String d = "00:00:00:00:00:00";
    public String e = "0000-00-00-00-00-00";
    public String f = "0";
    public String g = "0";
    public String h = "0";
    public String i = "0";
    public String j = "0";
    public String k = "0";
    public String l = "0";
    public String m = "0";
    public String n = "0000-00-00-00-00-00";
    public List<f> o = new ArrayList();
    public List<c> p = new ArrayList();
    public List<ScanResult> q = new ArrayList();

    protected g() {
    }

    private static String a(Location location, String str) {
        return str.equals("lon") ? a(Double.valueOf(location.getLongitude()), "#.000000") : str.equals(f.ae) ? a(Double.valueOf(location.getLatitude()), "#.000000") : str.equals("speed") ? a(Float.valueOf(location.getSpeed()), "#") : str.equals("altitude") ? a(Double.valueOf(location.getAltitude()), "#") : str.equals("accuracy") ? a(Float.valueOf(location.getAccuracy()), "#") : str.equals("bearing") ? a(Float.valueOf(location.getBearing()), "#") : str.equals("time") ? a(Long.valueOf(location.getTime()), "#") : PoiTypeDef.All;
    }

    public static String a(Object obj, String str) {
        if (r == null) {
            r = new DecimalFormat("#");
        }
        r.applyPattern(str);
        return r.format(obj);
    }

    public final void a() {
        this.o.clear();
        this.o = null;
        this.p.clear();
        this.p = null;
        this.q.clear();
        this.q = null;
        r = null;
    }

    public final void a(Location location, float f2, int i2) {
        this.f = a(location, "lon");
        this.g = a(location, f.ae);
        this.h = a(location, "speed");
        this.i = a(location, "altitude");
        this.j = a(location, "accuracy");
        this.k = a(location, "bearing");
        this.l = String.valueOf(i2);
        r.applyPattern("0");
        this.m = r.format((double) f2);
        long parseLong = Long.parseLong(a(location, "time"));
        if (s == null) {
            s = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        } else {
            s.applyPattern("yyyy-MM-dd-HH-mm-ss");
        }
        this.n = parseLong > 0 ? s.format(Long.valueOf(parseLong)) : "0000-00-00-00-00-00";
    }

    public final void a(List<f> list) {
        this.o.addAll(list);
    }

    public final void b(List<c> list) {
        this.p.addAll(list);
    }

    public final void c(List<ScanResult> list) {
        this.q.addAll(list);
    }
}
