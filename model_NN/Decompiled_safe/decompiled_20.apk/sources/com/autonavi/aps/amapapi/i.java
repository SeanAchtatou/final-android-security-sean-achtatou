package com.autonavi.aps.amapapi;

import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public final class i {

    final class a extends DefaultHandler {
        public AmapLoc a;
        private String c;

        private a() {
            this.a = new AmapLoc();
            this.c = PoiTypeDef.All;
        }

        /* synthetic */ a(i iVar, byte b2) {
            this();
        }

        public final void characters(char[] cArr, int i, int i2) {
            this.c = String.valueOf(cArr, i, i2);
        }

        public final void endElement(String str, String str2, String str3) {
            if (str2.equals("retype")) {
                this.a.setRetype(this.c);
            } else if (str2.equals("adcode")) {
                this.a.setAdcode(this.c);
            } else if (str2.equals("citycode")) {
                this.a.setCitycode(this.c);
            } else if (str2.equals("radius")) {
                try {
                    this.a.setAccuracy(Float.valueOf(this.c).floatValue());
                } catch (Exception e) {
                    Utils.printE(e);
                    this.a.setAccuracy(BitmapDescriptorFactory.HUE_RED);
                }
            } else if (str2.equals("cenx")) {
                try {
                    this.c = g.a(Double.valueOf(this.c), "#.000000");
                    this.a.setLon(Double.valueOf(this.c).doubleValue());
                } catch (Exception e2) {
                    Utils.printE(e2);
                    this.a.setLon(0.0d);
                }
            } else if (str2.equals("ceny")) {
                try {
                    this.c = g.a(Double.valueOf(this.c), "#.000000");
                    this.a.setLat(Double.valueOf(this.c).doubleValue());
                } catch (Exception e3) {
                    Utils.printE(e3);
                    this.a.setLat(0.0d);
                }
            } else if (str2.equals("desc")) {
                this.a.setDesc(this.c);
            } else if (str2.equals("country")) {
                this.a.setCountry(this.c);
            } else if (str2.equals("province")) {
                this.a.setProvince(this.c);
            } else if (str2.equals("city")) {
                this.a.setCity(this.c);
            } else if (str2.equals("road")) {
                this.a.setRoad(this.c);
            } else if (str2.equals("street")) {
                this.a.setStreet(this.c);
            } else if (str2.equals("poiname")) {
                this.a.setPoiname(this.c);
            }
        }

        public final void startElement(String str, String str2, String str3, Attributes attributes) {
            this.c = PoiTypeDef.All;
        }
    }

    final class b extends DefaultHandler {
        public String a;
        private boolean c;

        private b() {
            this.a = PoiTypeDef.All;
            this.c = false;
        }

        /* synthetic */ b(i iVar, byte b2) {
            this();
        }

        public final void characters(char[] cArr, int i, int i2) {
            if (this.c) {
                this.a = String.valueOf(cArr, i, i2);
            }
        }

        public final void endElement(String str, String str2, String str3) {
            if (str2.equals("sres")) {
                this.c = false;
            }
        }

        public final void startElement(String str, String str2, String str3, Attributes attributes) {
            if (str2.equals("sres")) {
                this.c = true;
            }
        }
    }

    protected i() {
    }

    public final String a(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes());
        b bVar = new b(this, (byte) 0);
        try {
            SAXParserFactory.newInstance().newSAXParser().parse(byteArrayInputStream, bVar);
            byteArrayInputStream.close();
        } catch (Exception e) {
            Utils.printE(e);
        }
        return bVar.a;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.autonavi.aps.amapapi.AmapLoc b(java.lang.String r6) {
        /*
            r5 = this;
            if (r6 == 0) goto L_0x0008
            int r0 = r6.length()
            if (r0 != 0) goto L_0x000a
        L_0x0008:
            r0 = 0
        L_0x0009:
            return r0
        L_0x000a:
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            byte[] r1 = r6.getBytes()
            r0.<init>(r1)
            javax.xml.parsers.SAXParserFactory r1 = javax.xml.parsers.SAXParserFactory.newInstance()
            com.autonavi.aps.amapapi.i$a r2 = new com.autonavi.aps.amapapi.i$a
            r3 = 0
            r2.<init>(r5, r3)
            javax.xml.parsers.SAXParser r1 = r1.newSAXParser()     // Catch:{ Exception -> 0x003c, all -> 0x003a }
            r1.parse(r0, r2)     // Catch:{ Exception -> 0x003c, all -> 0x003a }
            r0.close()     // Catch:{ Exception -> 0x003c, all -> 0x003a }
        L_0x0027:
            com.autonavi.aps.amapapi.AmapLoc r0 = r2.a
            java.lang.String r1 = "network"
            r0.setProvider(r1)
            com.autonavi.aps.amapapi.AmapLoc r0 = r2.a
            long r3 = com.autonavi.aps.amapapi.Utils.getTime()
            r0.setTime(r3)
            com.autonavi.aps.amapapi.AmapLoc r0 = r2.a
            goto L_0x0009
        L_0x003a:
            r0 = move-exception
            throw r0
        L_0x003c:
            r0 = move-exception
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.aps.amapapi.i.b(java.lang.String):com.autonavi.aps.amapapi.AmapLoc");
    }
}
