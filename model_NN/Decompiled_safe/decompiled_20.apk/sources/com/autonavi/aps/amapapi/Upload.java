package com.autonavi.aps.amapapi;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.IBinder;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Upload extends Service {
    /* access modifiers changed from: private */
    public List<File> lstFile = new ArrayList();
    private a upBinder = new a();

    public final class a extends Binder {
        public a() {
        }

        public final Upload a() {
            return Upload.this;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x002a A[SYNTHETIC, Splitter:B:24:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0014  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean doUpload(java.lang.String r8, java.io.File r9) {
        /*
            r7 = this;
            r0 = 0
            r1 = 1
            monitor-enter(r7)
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0021 }
            r2.<init>(r9)     // Catch:{ Exception -> 0x0021 }
            int r3 = r2.available()     // Catch:{ Exception -> 0x0021 }
            if (r3 > 0) goto L_0x0017
            r3 = r1
        L_0x000f:
            r2.close()     // Catch:{ Exception -> 0x00a5 }
        L_0x0012:
            if (r3 == 0) goto L_0x002a
            r0 = r1
        L_0x0015:
            monitor-exit(r7)
            return r0
        L_0x0017:
            int r3 = r2.available()     // Catch:{ Exception -> 0x0021 }
            r4 = 10000(0x2710, float:1.4013E-41)
            if (r3 <= r4) goto L_0x00a8
            r3 = r1
            goto L_0x000f
        L_0x0021:
            r2 = move-exception
            r3 = r0
        L_0x0023:
            com.autonavi.aps.amapapi.Utils.printE(r2)     // Catch:{ all -> 0x0027 }
            goto L_0x0012
        L_0x0027:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x002a:
            org.apache.http.impl.client.DefaultHttpClient r2 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ all -> 0x0027 }
            r2.<init>()     // Catch:{ all -> 0x0027 }
            org.apache.http.params.HttpParams r3 = r2.getParams()     // Catch:{ all -> 0x0027 }
            r4 = 60000(0xea60, float:8.4078E-41)
            com.autonavi.aps.amapapi.Utils.setTimeOut(r3, r4)     // Catch:{ all -> 0x0027 }
            org.apache.http.client.methods.HttpPost r3 = new org.apache.http.client.methods.HttpPost     // Catch:{ all -> 0x0027 }
            r3.<init>(r8)     // Catch:{ all -> 0x0027 }
            org.apache.http.entity.FileEntity r4 = new org.apache.http.entity.FileEntity     // Catch:{ all -> 0x0027 }
            java.lang.String r5 = "binary/octet-stream"
            r4.<init>(r9, r5)     // Catch:{ all -> 0x0027 }
            java.lang.String r5 = "UTF-8"
            r4.setContentEncoding(r5)     // Catch:{ all -> 0x0027 }
            r3.setEntity(r4)     // Catch:{ all -> 0x0027 }
            org.apache.http.HttpResponse r4 = r2.execute(r3)     // Catch:{ UnknownHostException -> 0x007d, Exception -> 0x0089 }
            org.apache.http.StatusLine r5 = r4.getStatusLine()     // Catch:{ UnknownHostException -> 0x007d, Exception -> 0x0089 }
            int r5 = r5.getStatusCode()     // Catch:{ UnknownHostException -> 0x007d, Exception -> 0x0089 }
            org.apache.http.HttpEntity r4 = r4.getEntity()     // Catch:{ UnknownHostException -> 0x007d, Exception -> 0x0089 }
            java.io.InputStream r4 = r4.getContent()     // Catch:{ UnknownHostException -> 0x007d, Exception -> 0x0089 }
            java.lang.String r4 = com.autonavi.aps.amapapi.Utils.isToStr(r4)     // Catch:{ UnknownHostException -> 0x007d, Exception -> 0x0089 }
            r6 = 200(0xc8, float:2.8E-43)
            if (r5 != r6) goto L_0x0072
            java.lang.String r5 = "true"
            boolean r4 = r4.equals(r5)     // Catch:{ UnknownHostException -> 0x007d, Exception -> 0x0089 }
            if (r4 == 0) goto L_0x0072
            r0 = r1
        L_0x0072:
            r3.abort()     // Catch:{ all -> 0x0027 }
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()     // Catch:{ all -> 0x0027 }
            r1.shutdown()     // Catch:{ all -> 0x0027 }
            goto L_0x0015
        L_0x007d:
            r1 = move-exception
            r3.abort()     // Catch:{ all -> 0x0027 }
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()     // Catch:{ all -> 0x0027 }
            r1.shutdown()     // Catch:{ all -> 0x0027 }
            goto L_0x0015
        L_0x0089:
            r1 = move-exception
            com.autonavi.aps.amapapi.Utils.printE(r1)     // Catch:{ all -> 0x0099 }
            r3.abort()     // Catch:{ all -> 0x0027 }
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()     // Catch:{ all -> 0x0027 }
            r1.shutdown()     // Catch:{ all -> 0x0027 }
            goto L_0x0015
        L_0x0099:
            r0 = move-exception
            r3.abort()     // Catch:{ all -> 0x0027 }
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()     // Catch:{ all -> 0x0027 }
            r1.shutdown()     // Catch:{ all -> 0x0027 }
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x00a5:
            r2 = move-exception
            goto L_0x0023
        L_0x00a8:
            r3 = r0
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.aps.amapapi.Upload.doUpload(java.lang.String, java.io.File):boolean");
    }

    private synchronized void upload() {
        if (this.lstFile.size() != 0) {
            if (h.a(((ConnectivityManager) Utils.getServ(this, "connectivity")).getActiveNetworkInfo()) != -1) {
                new Thread() {
                    public final void run() {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("http://cgicol.amap.com/collection/writedata?ver=v1.1_amap");
                        stringBuffer.append("&zei=").append(d.a);
                        stringBuffer.append("&zsi=").append(d.b);
                        while (Upload.this.lstFile.size() > 0) {
                            File file = (File) Upload.this.lstFile.get(0);
                            if (!file.exists()) {
                                Upload.this.lstFile.remove(file);
                            } else {
                                if (Upload.this.doUpload(stringBuffer.toString(), file)) {
                                    file.delete();
                                    Upload.this.lstFile.remove(0);
                                }
                                try {
                                    Thread.sleep(2000);
                                } catch (Exception e) {
                                    Utils.printE(e);
                                }
                            }
                        }
                        stringBuffer.delete(0, stringBuffer.length());
                    }
                }.start();
            }
        }
    }

    public int getFileNumInQueue() {
        return this.lstFile.size();
    }

    public IBinder onBind(Intent intent) {
        return this.upBinder;
    }

    public void onCreate() {
    }

    public void onDestroy() {
        this.lstFile.clear();
        this.upBinder = null;
        super.onDestroy();
    }

    public void onStart(Intent intent, int i) {
        updateQueue();
    }

    public synchronized void updateQueue() {
        if (this.lstFile.size() <= 0) {
            File[] listFiles = new File(j.a((Context) this)).listFiles();
            if (listFiles.length != 0) {
                this.lstFile.clear();
                for (File file : listFiles) {
                    if (file.getName().indexOf("new") != -1 && file.exists()) {
                        this.lstFile.add(file);
                    }
                }
                upload();
            }
        }
    }
}
