package com.autonavi.aps.amapapi;

import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

public final class h {
    private static h a = null;

    private h() {
    }

    public static int a(NetworkInfo networkInfo) {
        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            return networkInfo.getType();
        }
        return -1;
    }

    public static synchronized h a() {
        h hVar;
        synchronized (h.class) {
            if (a == null) {
                a = new h();
            }
            hVar = a;
        }
        return hVar;
    }

    public static String a(TelephonyManager telephonyManager) {
        int i = 0;
        if (telephonyManager != null) {
            i = telephonyManager.getNetworkType();
        }
        return d.k.get(i, "UNKNOWN");
    }

    /* JADX WARNING: Removed duplicated region for block: B:102:0x01a4 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01a9 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x01b2 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x01b7 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01bc A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x01cc A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x01d1 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x01da A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x01df A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x01e4 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0134 A[SYNTHETIC, Splitter:B:65:0x0134] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0139 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0142 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0147 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x014c A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0159 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x015e A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0167 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x016c A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0171 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x017e A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0183 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x018c A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0191 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0196 A[Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String a(java.lang.String r12, android.content.Context r13, android.net.ConnectivityManager r14) {
        /*
            r11 = this;
            r2 = 1
            r5 = -1
            r3 = 0
            r0 = 0
            monitor-enter(r11)
            java.lang.String r6 = ""
            android.net.NetworkInfo r1 = r14.getActiveNetworkInfo()     // Catch:{ all -> 0x01e8 }
            int r4 = a(r1)     // Catch:{ all -> 0x01e8 }
            if (r4 != r5) goto L_0x0013
        L_0x0011:
            monitor-exit(r11)
            return r0
        L_0x0013:
            org.apache.http.params.BasicHttpParams r8 = new org.apache.http.params.BasicHttpParams     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            r8.<init>()     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            int r4 = r1.getType()     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            if (r4 != r2) goto L_0x0116
            java.lang.String r4 = android.net.Proxy.getHost(r13)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            int r1 = android.net.Proxy.getPort(r13)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            r7 = r4
            r4 = r1
        L_0x0028:
            if (r7 == 0) goto L_0x0128
            int r1 = r7.length()     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            if (r1 <= 0) goto L_0x0128
            if (r4 == r5) goto L_0x0128
            r1 = r2
        L_0x0033:
            if (r1 == 0) goto L_0x003f
            org.apache.http.HttpHost r1 = new org.apache.http.HttpHost     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            r1.<init>(r7, r4)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            java.lang.String r4 = "http.route.default-proxy"
            r8.setParameter(r4, r1)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
        L_0x003f:
            r1 = 10000(0x2710, float:1.4013E-41)
            com.autonavi.aps.amapapi.Utils.setTimeOut(r8, r1)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            org.apache.http.conn.scheme.SchemeRegistry r1 = new org.apache.http.conn.scheme.SchemeRegistry     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            r1.<init>()     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            org.apache.http.conn.scheme.PlainSocketFactory r4 = org.apache.http.conn.scheme.PlainSocketFactory.getSocketFactory()     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            org.apache.http.conn.scheme.Scheme r5 = new org.apache.http.conn.scheme.Scheme     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            java.lang.String r7 = "http"
            r9 = 80
            r5.<init>(r7, r4, r9)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            r1.register(r5)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            org.apache.http.impl.conn.SingleClientConnManager r4 = new org.apache.http.impl.conn.SingleClientConnManager     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            r4.<init>(r8, r1)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            r5.<init>(r4, r8)     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost     // Catch:{ UnknownHostException -> 0x026f, SocketException -> 0x0248, SocketTimeoutException -> 0x0229, ConnectTimeoutException -> 0x020e, all -> 0x01eb }
            java.lang.String r1 = "http://aps.amap.com/APS/r"
            r4.<init>(r1)     // Catch:{ UnknownHostException -> 0x026f, SocketException -> 0x0248, SocketTimeoutException -> 0x0229, ConnectTimeoutException -> 0x020e, all -> 0x01eb }
            org.apache.http.entity.StringEntity r1 = new org.apache.http.entity.StringEntity     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.lang.String r7 = "GBK"
            r1.<init>(r12, r7)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.lang.String r7 = "text/xml"
            r1.setContentType(r7)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.lang.StringBuffer r7 = new java.lang.StringBuffer     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            r7.<init>()     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.lang.String r8 = "application/soap+xml;charset="
            r7.append(r8)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.lang.String r8 = "GBK"
            r7.append(r8)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.lang.String r8 = "Content-Type"
            java.lang.String r9 = r7.toString()     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            r4.addHeader(r8, r9)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.lang.String r8 = "Accept-Encoding"
            java.lang.String r9 = "gzip"
            r4.addHeader(r8, r9)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            r8 = 0
            int r9 = r7.length()     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            r7.delete(r8, r9)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            r4.setEntity(r1)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            org.apache.http.HttpResponse r8 = r5.execute(r4)     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            org.apache.http.StatusLine r1 = r8.getStatusLine()     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            int r1 = r1.getStatusCode()     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r1 != r9) goto L_0x028e
            org.apache.http.HttpEntity r1 = r8.getEntity()     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.io.InputStream r1 = r1.getContent()     // Catch:{ UnknownHostException -> 0x0276, SocketException -> 0x024f, SocketTimeoutException -> 0x0230, ConnectTimeoutException -> 0x0214, all -> 0x01f3 }
            java.lang.String r9 = "Content-Encoding"
            org.apache.http.Header r8 = r8.getFirstHeader(r9)     // Catch:{ UnknownHostException -> 0x027c, SocketException -> 0x0255, SocketTimeoutException -> 0x0236, ConnectTimeoutException -> 0x0219, all -> 0x01fa }
            if (r8 == 0) goto L_0x012b
            java.lang.String r8 = r8.getValue()     // Catch:{ UnknownHostException -> 0x027c, SocketException -> 0x0255, SocketTimeoutException -> 0x0236, ConnectTimeoutException -> 0x0219, all -> 0x01fa }
            java.lang.String r9 = "gzip"
            boolean r8 = r8.equalsIgnoreCase(r9)     // Catch:{ UnknownHostException -> 0x027c, SocketException -> 0x0255, SocketTimeoutException -> 0x0236, ConnectTimeoutException -> 0x0219, all -> 0x01fa }
            if (r8 == 0) goto L_0x012b
        L_0x00cc:
            if (r2 == 0) goto L_0x028b
            java.util.zip.GZIPInputStream r3 = new java.util.zip.GZIPInputStream     // Catch:{ UnknownHostException -> 0x027c, SocketException -> 0x0255, SocketTimeoutException -> 0x0236, ConnectTimeoutException -> 0x0219, all -> 0x01fa }
            r3.<init>(r1)     // Catch:{ UnknownHostException -> 0x027c, SocketException -> 0x0255, SocketTimeoutException -> 0x0236, ConnectTimeoutException -> 0x0219, all -> 0x01fa }
        L_0x00d3:
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ UnknownHostException -> 0x0282, SocketException -> 0x025b, SocketTimeoutException -> 0x023c, ConnectTimeoutException -> 0x021e, all -> 0x0201 }
            java.lang.String r1 = "GBK"
            r2.<init>(r3, r1)     // Catch:{ UnknownHostException -> 0x0282, SocketException -> 0x025b, SocketTimeoutException -> 0x023c, ConnectTimeoutException -> 0x021e, all -> 0x0201 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ UnknownHostException -> 0x0287, SocketException -> 0x0260, SocketTimeoutException -> 0x0241, ConnectTimeoutException -> 0x0222, all -> 0x0207 }
            r8 = 2048(0x800, float:2.87E-42)
            r1.<init>(r2, r8)     // Catch:{ UnknownHostException -> 0x0287, SocketException -> 0x0260, SocketTimeoutException -> 0x0241, ConnectTimeoutException -> 0x0222, all -> 0x0207 }
        L_0x00e1:
            java.lang.String r8 = r1.readLine()     // Catch:{ UnknownHostException -> 0x0131, SocketException -> 0x0264, SocketTimeoutException -> 0x0245, ConnectTimeoutException -> 0x0226, all -> 0x020c }
            if (r8 != 0) goto L_0x012d
            java.lang.String r6 = r7.toString()     // Catch:{ UnknownHostException -> 0x0131, SocketException -> 0x0264, SocketTimeoutException -> 0x0245, ConnectTimeoutException -> 0x0226, all -> 0x020c }
            r8 = 0
            int r9 = r7.length()     // Catch:{ UnknownHostException -> 0x0131, SocketException -> 0x0264, SocketTimeoutException -> 0x0245, ConnectTimeoutException -> 0x0226, all -> 0x020c }
            r7.delete(r8, r9)     // Catch:{ UnknownHostException -> 0x0131, SocketException -> 0x0264, SocketTimeoutException -> 0x0245, ConnectTimeoutException -> 0x0226, all -> 0x020c }
        L_0x00f3:
            r4.abort()     // Catch:{ all -> 0x01e8 }
            org.apache.http.conn.ClientConnectionManager r4 = r5.getConnectionManager()     // Catch:{ all -> 0x01e8 }
            r4.shutdown()     // Catch:{ all -> 0x01e8 }
            if (r3 == 0) goto L_0x0102
            r3.close()     // Catch:{ all -> 0x01e8 }
        L_0x0102:
            if (r2 == 0) goto L_0x0107
            r2.close()     // Catch:{ all -> 0x01e8 }
        L_0x0107:
            if (r1 == 0) goto L_0x010c
            r1.close()     // Catch:{ all -> 0x01e8 }
        L_0x010c:
            r1 = r6
        L_0x010d:
            int r2 = r1.length()     // Catch:{ all -> 0x01e8 }
            if (r2 <= 0) goto L_0x0011
            r0 = r1
            goto L_0x0011
        L_0x0116:
            int r1 = r1.getType()     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            if (r1 != 0) goto L_0x0293
            java.lang.String r4 = android.net.Proxy.getDefaultHost()     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            int r1 = android.net.Proxy.getDefaultPort()     // Catch:{ UnknownHostException -> 0x0267, SocketException -> 0x0151, SocketTimeoutException -> 0x0176, ConnectTimeoutException -> 0x019c, all -> 0x01c2 }
            r7 = r4
            r4 = r1
            goto L_0x0028
        L_0x0128:
            r1 = r3
            goto L_0x0033
        L_0x012b:
            r2 = r3
            goto L_0x00cc
        L_0x012d:
            r7.append(r8)     // Catch:{ UnknownHostException -> 0x0131, SocketException -> 0x0264, SocketTimeoutException -> 0x0245, ConnectTimeoutException -> 0x0226, all -> 0x020c }
            goto L_0x00e1
        L_0x0131:
            r7 = move-exception
        L_0x0132:
            if (r4 == 0) goto L_0x0137
            r4.abort()     // Catch:{ all -> 0x01e8 }
        L_0x0137:
            if (r5 == 0) goto L_0x0140
            org.apache.http.conn.ClientConnectionManager r4 = r5.getConnectionManager()     // Catch:{ all -> 0x01e8 }
            r4.shutdown()     // Catch:{ all -> 0x01e8 }
        L_0x0140:
            if (r3 == 0) goto L_0x0145
            r3.close()     // Catch:{ all -> 0x01e8 }
        L_0x0145:
            if (r2 == 0) goto L_0x014a
            r2.close()     // Catch:{ all -> 0x01e8 }
        L_0x014a:
            if (r1 == 0) goto L_0x010c
            r1.close()     // Catch:{ all -> 0x01e8 }
            r1 = r6
            goto L_0x010d
        L_0x0151:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            r4 = r0
            r5 = r0
        L_0x0157:
            if (r4 == 0) goto L_0x015c
            r4.abort()     // Catch:{ all -> 0x01e8 }
        L_0x015c:
            if (r5 == 0) goto L_0x0165
            org.apache.http.conn.ClientConnectionManager r4 = r5.getConnectionManager()     // Catch:{ all -> 0x01e8 }
            r4.shutdown()     // Catch:{ all -> 0x01e8 }
        L_0x0165:
            if (r3 == 0) goto L_0x016a
            r3.close()     // Catch:{ all -> 0x01e8 }
        L_0x016a:
            if (r2 == 0) goto L_0x016f
            r2.close()     // Catch:{ all -> 0x01e8 }
        L_0x016f:
            if (r1 == 0) goto L_0x010c
            r1.close()     // Catch:{ all -> 0x01e8 }
            r1 = r6
            goto L_0x010d
        L_0x0176:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            r4 = r0
            r5 = r0
        L_0x017c:
            if (r4 == 0) goto L_0x0181
            r4.abort()     // Catch:{ all -> 0x01e8 }
        L_0x0181:
            if (r5 == 0) goto L_0x018a
            org.apache.http.conn.ClientConnectionManager r4 = r5.getConnectionManager()     // Catch:{ all -> 0x01e8 }
            r4.shutdown()     // Catch:{ all -> 0x01e8 }
        L_0x018a:
            if (r3 == 0) goto L_0x018f
            r3.close()     // Catch:{ all -> 0x01e8 }
        L_0x018f:
            if (r2 == 0) goto L_0x0194
            r2.close()     // Catch:{ all -> 0x01e8 }
        L_0x0194:
            if (r1 == 0) goto L_0x010c
            r1.close()     // Catch:{ all -> 0x01e8 }
            r1 = r6
            goto L_0x010d
        L_0x019c:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            r4 = r0
            r5 = r0
        L_0x01a2:
            if (r4 == 0) goto L_0x01a7
            r4.abort()     // Catch:{ all -> 0x01e8 }
        L_0x01a7:
            if (r5 == 0) goto L_0x01b0
            org.apache.http.conn.ClientConnectionManager r4 = r5.getConnectionManager()     // Catch:{ all -> 0x01e8 }
            r4.shutdown()     // Catch:{ all -> 0x01e8 }
        L_0x01b0:
            if (r3 == 0) goto L_0x01b5
            r3.close()     // Catch:{ all -> 0x01e8 }
        L_0x01b5:
            if (r2 == 0) goto L_0x01ba
            r2.close()     // Catch:{ all -> 0x01e8 }
        L_0x01ba:
            if (r1 == 0) goto L_0x010c
            r1.close()     // Catch:{ all -> 0x01e8 }
            r1 = r6
            goto L_0x010d
        L_0x01c2:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r4 = r0
            r5 = r0
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x01ca:
            if (r4 == 0) goto L_0x01cf
            r4.abort()     // Catch:{ all -> 0x01e8 }
        L_0x01cf:
            if (r5 == 0) goto L_0x01d8
            org.apache.http.conn.ClientConnectionManager r4 = r5.getConnectionManager()     // Catch:{ all -> 0x01e8 }
            r4.shutdown()     // Catch:{ all -> 0x01e8 }
        L_0x01d8:
            if (r3 == 0) goto L_0x01dd
            r3.close()     // Catch:{ all -> 0x01e8 }
        L_0x01dd:
            if (r2 == 0) goto L_0x01e2
            r2.close()     // Catch:{ all -> 0x01e8 }
        L_0x01e2:
            if (r1 == 0) goto L_0x01e7
            r1.close()     // Catch:{ all -> 0x01e8 }
        L_0x01e7:
            throw r0     // Catch:{ all -> 0x01e8 }
        L_0x01e8:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x01eb:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r4 = r0
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x01ca
        L_0x01f3:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x01ca
        L_0x01fa:
            r2 = move-exception
            r3 = r1
            r1 = r0
            r10 = r0
            r0 = r2
            r2 = r10
            goto L_0x01ca
        L_0x0201:
            r1 = move-exception
            r2 = r0
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x01ca
        L_0x0207:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x01ca
        L_0x020c:
            r0 = move-exception
            goto L_0x01ca
        L_0x020e:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            r4 = r0
            goto L_0x01a2
        L_0x0214:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            goto L_0x01a2
        L_0x0219:
            r2 = move-exception
            r2 = r0
            r3 = r1
            r1 = r0
            goto L_0x01a2
        L_0x021e:
            r1 = move-exception
            r1 = r0
            r2 = r0
            goto L_0x01a2
        L_0x0222:
            r1 = move-exception
            r1 = r0
            goto L_0x01a2
        L_0x0226:
            r7 = move-exception
            goto L_0x01a2
        L_0x0229:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            r4 = r0
            goto L_0x017c
        L_0x0230:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            goto L_0x017c
        L_0x0236:
            r2 = move-exception
            r2 = r0
            r3 = r1
            r1 = r0
            goto L_0x017c
        L_0x023c:
            r1 = move-exception
            r1 = r0
            r2 = r0
            goto L_0x017c
        L_0x0241:
            r1 = move-exception
            r1 = r0
            goto L_0x017c
        L_0x0245:
            r7 = move-exception
            goto L_0x017c
        L_0x0248:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            r4 = r0
            goto L_0x0157
        L_0x024f:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            goto L_0x0157
        L_0x0255:
            r2 = move-exception
            r2 = r0
            r3 = r1
            r1 = r0
            goto L_0x0157
        L_0x025b:
            r1 = move-exception
            r1 = r0
            r2 = r0
            goto L_0x0157
        L_0x0260:
            r1 = move-exception
            r1 = r0
            goto L_0x0157
        L_0x0264:
            r7 = move-exception
            goto L_0x0157
        L_0x0267:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            r4 = r0
            r5 = r0
            goto L_0x0132
        L_0x026f:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            r4 = r0
            goto L_0x0132
        L_0x0276:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            goto L_0x0132
        L_0x027c:
            r2 = move-exception
            r2 = r0
            r3 = r1
            r1 = r0
            goto L_0x0132
        L_0x0282:
            r1 = move-exception
            r1 = r0
            r2 = r0
            goto L_0x0132
        L_0x0287:
            r1 = move-exception
            r1 = r0
            goto L_0x0132
        L_0x028b:
            r3 = r1
            goto L_0x00d3
        L_0x028e:
            r1 = r0
            r2 = r0
            r3 = r0
            goto L_0x00f3
        L_0x0293:
            r4 = r5
            r7 = r0
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.aps.amapapi.h.a(java.lang.String, android.content.Context, android.net.ConnectivityManager):java.lang.String");
    }
}
