package com.autonavi.aps.amapapi;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.widget.Toast;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

public class Utils {
    private Utils() {
    }

    @SuppressLint({"InlinedApi"})
    public static boolean airPlaneModeOn(Context context) {
        if (context == null) {
            return false;
        }
        int i = Build.VERSION.SDK_INT;
        ContentResolver contentResolver = context.getContentResolver();
        return i < 17 ? Settings.System.getInt(contentResolver, "airplane_mode_on", 0) == 1 : Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) == 1;
    }

    public static int asu2Dbm(int i) {
        return (i * 2) - 113;
    }

    public static boolean coordInCN(Location location) {
        if (location == null) {
            return false;
        }
        boolean z = true;
        if (location.getLongitude() < 73.0d || location.getLongitude() > 136.0d) {
            z = false;
        } else if (location.getLatitude() < 3.0d || location.getLatitude() > 54.0d) {
            z = false;
        }
        return z;
    }

    public static boolean coordInCN(String str, String str2) {
        Double valueOf = Double.valueOf(str);
        Double valueOf2 = Double.valueOf(str2);
        if (valueOf.doubleValue() < 73.0d || valueOf.doubleValue() > 136.0d) {
            return false;
        }
        return valueOf2.doubleValue() >= 3.0d && valueOf2.doubleValue() <= 54.0d;
    }

    public static float distance(double[] dArr) {
        if (dArr.length != 4) {
            return BitmapDescriptorFactory.HUE_RED;
        }
        float[] fArr = new float[1];
        Location.distanceBetween(dArr[0], dArr[1], dArr[2], dArr[3], fArr);
        return fArr[0];
    }

    public static int getCellType(CellLocation cellLocation, Context context) {
        if (airPlaneModeOn(context)) {
            new Object[1][0] = "air plane mode is on";
            return 9;
        } else if (cellLocation instanceof GsmCellLocation) {
            return 1;
        } else {
            try {
                Class.forName("android.telephony.cdma.CdmaCellLocation");
                return 2;
            } catch (Exception e) {
                return 9;
            }
        }
    }

    public static String getJSONValue(JSONObject jSONObject, String str) {
        try {
            return jSONObject.getString(str);
        } catch (Exception e) {
            Object[] objArr = {"no value for ", str};
            return PoiTypeDef.All;
        }
    }

    public static String[] getMccMnc(TelephonyManager telephonyManager) {
        String networkOperator = telephonyManager.getNetworkOperator();
        String[] strArr = {"0", "0"};
        if (networkOperator.length() > 4) {
            strArr[0] = networkOperator.substring(0, 3);
            strArr[1] = networkOperator.substring(3, 5);
        } else {
            new Object[1][0] = "get mcc and mnc error";
        }
        return strArr;
    }

    public static Object getServ(Context context, String str) {
        if (context == null) {
            return null;
        }
        return context.getApplicationContext().getSystemService(str);
    }

    public static long getTime() {
        return System.currentTimeMillis();
    }

    public static boolean hasGpsProvider(LocationManager locationManager) {
        List<String> allProviders;
        if (locationManager == null || (allProviders = locationManager.getAllProviders()) == null) {
            return false;
        }
        return allProviders.contains(LocationManagerProxy.GPS_PROVIDER);
    }

    public static boolean hasNetworkProvider(LocationManager locationManager) {
        List<String> allProviders;
        if (locationManager == null || (allProviders = locationManager.getAllProviders()) == null) {
            return false;
        }
        return allProviders.contains(LocationManagerProxy.NETWORK_PROVIDER);
    }

    public static String isToStr(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                return byteArrayOutputStream.toString();
            }
            byteArrayOutputStream.write(read);
        }
    }

    public static void printE(Throwable th) {
    }

    public static void setTimeOut(HttpParams httpParams, int i) {
        httpParams.setIntParameter(HttpConnectionParams.CONNECTION_TIMEOUT, i);
        httpParams.setIntParameter("http.socket.timeout", i);
        httpParams.setLongParameter("http.conn-manager.timeout", (long) i);
    }

    public static void toast(Context context, String str) {
        if ("http://aps.amap.com/APS/r".indexOf("test") != -1) {
            Toast.makeText(context, str, 0).show();
            new Object[1][0] = str;
        } else if (d.d.indexOf("test") != -1) {
            Toast.makeText(context, str, 0).show();
            new Object[1][0] = str;
        } else if ("http://cgicol.amap.com/collection/writedata?ver=v1.1_amap".indexOf("test") != -1) {
            Toast.makeText(context, str, 0).show();
            new Object[1][0] = str;
        }
    }

    public static void writeCat(String str, boolean z) {
    }

    public static void writeCat(Object... objArr) {
    }
}
