package com.autonavi.aps.amapapi;

import android.util.SparseArray;
import com.amap.api.search.poisearch.PoiTypeDef;

public final class d {
    public static String a = null;
    public static String b = null;
    public static String c = null;
    public static String d = PoiTypeDef.All;
    public static String e = PoiTypeDef.All;
    public static String f = PoiTypeDef.All;
    public static boolean g = false;
    public static boolean h = true;
    public static boolean i = true;
    public static boolean j = true;
    public static final SparseArray<String> k;

    static {
        SparseArray<String> sparseArray = new SparseArray<>();
        k = sparseArray;
        sparseArray.append(0, "UNKNOWN");
        k.append(1, "GPRS");
        k.append(2, "EDGE");
        k.append(3, "UMTS");
        k.append(4, "CDMA");
        k.append(5, "EVDO_0");
        k.append(6, "EVDO_A");
        k.append(7, "1xRTT");
        k.append(8, "HSDPA");
        k.append(9, "HSUPA");
        k.append(10, "HSPA");
        k.append(11, "IDEN");
        k.append(12, "EVDO_B");
        k.append(13, "LTE");
        k.append(14, "EHRPD");
        k.append(15, "HSPAP");
    }
}
