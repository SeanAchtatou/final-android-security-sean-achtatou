package com.autonavi.aps.amapapi;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Environment;
import android.os.IBinder;
import com.autonavi.aps.amapapi.Upload;
import java.io.File;
import org.apache.commons.httpclient.cookie.CookieSpec;

public final class j {
    private static j a = null;
    private static Intent b = null;
    /* access modifiers changed from: private */
    public static Upload c = null;
    private static ServiceConnection d = new ServiceConnection() {
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            j.c = ((Upload.a) iBinder).a();
        }

        public final void onServiceDisconnected(ComponentName componentName) {
        }
    };

    private j() {
    }

    public static synchronized j a() {
        j jVar;
        synchronized (j.class) {
            if (a == null) {
                a = new j();
            }
            jVar = a;
        }
        return jVar;
    }

    public static String a(Context context) {
        File externalStorageDirectory = Environment.getExternalStorageState().equals("mounted") ? Environment.getExternalStorageDirectory() : context.getCacheDir();
        StringBuilder sb = new StringBuilder();
        sb.append(externalStorageDirectory.getAbsolutePath()).append(CookieSpec.PATH_DELIM);
        sb.append("apsamapapi/");
        return sb.toString();
    }

    public static void b(Context context) {
        if (d.j) {
            if (b == null) {
                Intent intent = new Intent();
                b = intent;
                intent.setClass(context.getApplicationContext(), Upload.class);
            }
            if (context.getApplicationContext().startService(b) != null) {
                context.getApplicationContext().bindService(b, d, 1);
            }
        }
    }

    public static void c(Context context) {
        if (b != null) {
            try {
                context.unbindService(d);
            } catch (Exception e) {
            }
            context.stopService(b);
        }
    }

    public final synchronized void b() {
    }
}
