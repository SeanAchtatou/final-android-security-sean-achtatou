package com.autonavi.aps.amapapi;

import com.amap.api.search.poisearch.PoiTypeDef;

public final class c {
    private String a = PoiTypeDef.All;
    private String b = PoiTypeDef.All;
    private int c = 0;
    private int d = 0;
    private int e = 0;
    private int f = 0;
    private int g = 0;
    private int h = -113;

    protected c() {
    }

    public final String a() {
        return this.a;
    }

    public final void a(int i) {
        if (i < Integer.MAX_VALUE) {
            this.c = i;
        }
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(int i) {
        if (this.c < Integer.MAX_VALUE) {
            this.d = i;
        }
    }

    public final void b(String str) {
        this.b = str;
    }

    public final int c() {
        return this.c;
    }

    public final void c(int i) {
        this.e = i;
    }

    public final int d() {
        return this.d;
    }

    public final void d(int i) {
        this.f = i;
    }

    public final int e() {
        return this.e;
    }

    public final void e(int i) {
        this.g = i;
    }

    public final int f() {
        return this.f;
    }

    public final void f(int i) {
        this.h = i;
    }

    public final int g() {
        return this.g;
    }

    public final int h() {
        return this.h;
    }
}
