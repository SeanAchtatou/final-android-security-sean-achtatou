package com.autonavi.aps.amapapi;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class b {
    private static b a = null;
    private Hashtable<String, a> b = new Hashtable<>();
    private long c = 0;

    final class a {
        private AmapLoc b;
        private String c;

        private a() {
            this.b = null;
            this.c = null;
        }

        /* synthetic */ a(b bVar, byte b2) {
            this();
        }

        public final AmapLoc a() {
            return this.b;
        }

        public final void a(AmapLoc amapLoc) {
            this.b = amapLoc;
        }

        public final void a(String str) {
            this.c = str.replace("##", "#");
        }

        public final String b() {
            return this.c;
        }
    }

    private b() {
    }

    private static double a(double[] dArr, double[] dArr2) {
        double d = 0.0d;
        double d2 = 0.0d;
        double d3 = 0.0d;
        for (int i = 0; i < dArr.length; i++) {
            d2 += dArr[i] * dArr[i];
            d += dArr2[i] * dArr2[i];
            d3 += dArr[i] * dArr2[i];
        }
        return d3 / (Math.sqrt(d) * Math.sqrt(d2));
    }

    private a a(StringBuilder sb, String str) {
        a aVar;
        boolean z;
        Hashtable hashtable = new Hashtable();
        Hashtable hashtable2 = new Hashtable();
        Hashtable hashtable3 = new Hashtable();
        Iterator<Map.Entry<String, a>> it = this.b.entrySet().iterator();
        while (true) {
            if (it.hasNext()) {
                Map.Entry next = it.next();
                String str2 = (String) next.getKey();
                aVar = (a) next.getValue();
                if (aVar.b().length() > 0 && sb.length() > 0 && str2.indexOf(str) != -1) {
                    String b2 = aVar.b();
                    int indexOf = b2.indexOf(",access");
                    if (indexOf == -1 || indexOf < 17) {
                        z = false;
                    } else {
                        if (sb.indexOf(",access") != -1) {
                            if (sb.toString().indexOf(String.valueOf(b2.substring(indexOf - 17, indexOf)) + ",access") != -1) {
                                z = true;
                            }
                        }
                        z = false;
                    }
                    if (z) {
                        break;
                    }
                    a(aVar.b(), hashtable);
                    a(sb.toString(), hashtable2);
                    hashtable3.clear();
                    for (String put : hashtable.keySet()) {
                        hashtable3.put(put, PoiTypeDef.All);
                    }
                    for (String put2 : hashtable2.keySet()) {
                        hashtable3.put(put2, PoiTypeDef.All);
                    }
                    Set keySet = hashtable3.keySet();
                    double[] dArr = new double[keySet.size()];
                    double[] dArr2 = new double[keySet.size()];
                    int i = 0;
                    Iterator it2 = keySet.iterator();
                    while (true) {
                        int i2 = i;
                        if (!it2.hasNext()) {
                            break;
                        }
                        String str3 = (String) it2.next();
                        dArr[i2] = hashtable.containsKey(str3) ? 1.0d : 0.0d;
                        dArr2[i2] = hashtable2.containsKey(str3) ? 1.0d : 0.0d;
                        i = i2 + 1;
                    }
                    keySet.clear();
                    if (a(dArr, dArr2) > 0.8500000238418579d) {
                        break;
                    }
                }
            } else {
                aVar = null;
                break;
            }
        }
        hashtable.clear();
        hashtable2.clear();
        hashtable3.clear();
        return aVar;
    }

    public static b a() {
        if (a == null) {
            a = new b();
        }
        return a;
    }

    private static void a(String str, Hashtable<String, String> hashtable) {
        hashtable.clear();
        for (String str2 : str.split("#")) {
            if (str2.length() > 0) {
                hashtable.put(str2, PoiTypeDef.All);
            }
        }
    }

    private boolean c() {
        return this.c != 0 && (this.b.size() > 180 || Utils.getTime() - this.c > 600000);
    }

    public final AmapLoc a(String str, StringBuilder sb) {
        a aVar;
        if (!d.i) {
            b();
            return null;
        } else if (str.indexOf("#gps") != -1) {
            return null;
        } else {
            if (c()) {
                b();
                return null;
            }
            if (str.indexOf("#cellwifi") != -1) {
                aVar = a(sb, "#cellwifi");
                if (aVar == null) {
                }
            } else if (str.indexOf("#wifi") != -1) {
                aVar = a(sb, "#wifi");
                if (aVar == null) {
                }
            } else {
                aVar = str.indexOf("#cell") != -1 ? this.b.get(str) : null;
            }
            if (aVar == null) {
                return null;
            }
            return aVar.a();
        }
    }

    public final void a(String str, AmapLoc amapLoc, StringBuilder sb) {
        boolean z;
        if (!d.i) {
            b();
            return;
        }
        if (str == null || amapLoc == null) {
            z = false;
        } else {
            z = true;
            if (str.indexOf("#network") == -1) {
                z = false;
            } else if (amapLoc.getLon() == 0.0d) {
                z = false;
            }
        }
        if (z && !amapLoc.getType().equals("cache")) {
            if (c()) {
                b();
            }
            this.c = Utils.getTime();
            a aVar = new a(this, (byte) 0);
            amapLoc.setType("cache");
            aVar.a(amapLoc);
            aVar.a(sb.toString());
            this.b.put(str, aVar);
        }
    }

    public final void b() {
        this.c = 0;
        this.b.clear();
    }
}
