package com.autonavi.aps.amapapi;

import com.amap.api.search.poisearch.PoiTypeDef;

public final class f {
    private String a = PoiTypeDef.All;
    private String b = PoiTypeDef.All;
    private int c = 0;
    private int d = 0;
    private int e = -113;

    protected f() {
    }

    public final String a() {
        return this.a;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(int i) {
        this.d = i;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final int c() {
        return this.c;
    }

    public final void c(int i) {
        this.e = i;
    }

    public final int d() {
        return this.d;
    }

    public final int e() {
        return this.e;
    }
}
