package com.autonavi.aps.amapapi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Looper;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

public final class a implements IAPS {
    private static a a = null;
    private long A = 0;
    private long B = 0;
    private boolean C = false;
    /* access modifiers changed from: private */
    public boolean D = false;
    /* access modifiers changed from: private */
    public long E = 0;
    private long F = 0;
    private long G = 0;
    private long H = 0;
    private boolean I = true;
    private h J = h.a();
    private int K = 0;
    private String L = "00:00:00:00:00:00";
    private GpsStatus M = null;
    private Context b = null;
    private int c = 9;
    private ConnectivityManager d = null;
    /* access modifiers changed from: private */
    public WifiManager e = null;
    private TelephonyManager f = null;
    private LocationManager g = null;
    private LocationListener h = null;
    private LocationListener i = null;
    private GpsStatus.Listener j = null;
    private float k = BitmapDescriptorFactory.HUE_RED;
    private int l = 0;
    /* access modifiers changed from: private */
    public Location m = null;
    /* access modifiers changed from: private */
    public Location n = null;
    /* access modifiers changed from: private */
    public long o = 0;
    /* access modifiers changed from: private */
    public List<f> p = new ArrayList();
    /* access modifiers changed from: private */
    public List<c> q = new ArrayList();
    /* access modifiers changed from: private */
    public List<ScanResult> r = new ArrayList();
    private e s = new e();
    private PhoneStateListener t = null;
    /* access modifiers changed from: private */
    public int u = -113;
    private C0002a v = new C0002a(this, (byte) 0);
    private WifiInfo w = null;
    private String x = null;
    private AmapLoc y = null;
    private long z = 0;

    /* renamed from: com.autonavi.aps.amapapi.a$a  reason: collision with other inner class name */
    final class C0002a extends BroadcastReceiver {
        private C0002a() {
        }

        /* synthetic */ C0002a(a aVar, byte b) {
            this();
        }

        public final void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("android.net.wifi.SCAN_RESULTS")) {
                a.this.r = a.this.e.getScanResults();
                if (a.this.r == null) {
                    a.this.r = new ArrayList();
                }
                a aVar = a.this;
                a.a(a.this.r);
            } else if (action.equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                switch (a.this.e.getWifiState()) {
                    case 0:
                        a.this.f();
                        return;
                    case 1:
                        a.this.f();
                        return;
                    case 2:
                    case 3:
                    default:
                        return;
                    case 4:
                        a.this.f();
                        return;
                }
            } else if (action.equals("android.intent.action.SCREEN_ON")) {
                CellLocation.requestLocationUpdate();
                a.this.h();
            }
        }
    }

    private a() {
    }

    private synchronized AmapLoc a(Location location) {
        AmapLoc amapLoc;
        amapLoc = new AmapLoc();
        amapLoc.setProvider(LocationManagerProxy.GPS_PROVIDER);
        amapLoc.setLon(Double.valueOf(g.a(Double.valueOf(location.getLongitude()), "#.000000")).doubleValue());
        amapLoc.setLat(Double.valueOf(g.a(Double.valueOf(location.getLatitude()), "#.000000")).doubleValue());
        amapLoc.setAccuracy(Float.valueOf(g.a(Float.valueOf(location.getAccuracy()), "#.0")).floatValue());
        amapLoc.setSpeed(Float.valueOf(g.a(Float.valueOf(location.getSpeed()), "#.0")).floatValue());
        amapLoc.setBearing(Float.valueOf(g.a(Float.valueOf(location.getBearing()), "#.0")).floatValue());
        amapLoc.setTime(Utils.getTime());
        if (!a(amapLoc)) {
            amapLoc = null;
        }
        return amapLoc;
    }

    private AmapLoc a(String str) {
        if (this.b == null) {
            return null;
        }
        this.A = Utils.getTime();
        new AmapLoc();
        i iVar = new i();
        AmapLoc b2 = iVar.b(this.s.a(iVar.a(this.J.a(str, this.b, this.d)), "GBK"));
        if (a(b2)) {
            return b2;
        }
        return null;
    }

    public static synchronized IAPS a() {
        a aVar;
        synchronized (a.class) {
            if (a == null) {
                a = new a();
            }
            aVar = a;
        }
        return aVar;
    }

    private synchronized StringBuilder a(g gVar) {
        StringBuilder sb;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String sb2;
        sb = new StringBuilder();
        d.a = "888888888888888";
        d.b = "888888888888888";
        d.c = PoiTypeDef.All;
        StringBuilder sb3 = new StringBuilder();
        StringBuilder sb4 = new StringBuilder();
        StringBuilder sb5 = new StringBuilder();
        String str8 = this.c == 2 ? "1" : "0";
        boolean z2 = false;
        long time = Utils.getTime() - this.o;
        if (this.o != 0) {
            if (this.g == null) {
                z2 = true;
            } else if (!this.g.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER)) {
                z2 = true;
            } else if (time > 20000) {
                z2 = true;
            }
        }
        if (!z2) {
            if (gVar != null) {
                gVar.a(this.m, this.k, this.l);
            }
            if (this.m.getLongitude() > 0.0d) {
                String a2 = g.a(Double.valueOf(this.m.getLongitude()), "#.000000");
                String a3 = g.a(Double.valueOf(this.m.getLatitude()), "#.000000");
                str = g.a(Float.valueOf(this.m.getAccuracy()), "#.0");
                str2 = a3;
                str3 = a2;
                str4 = "1";
            }
            str = "0";
            str2 = "0";
            str3 = "0";
            str4 = "0";
        } else {
            g();
            if (this.n.getLongitude() > 0.0d) {
                String a4 = g.a(Double.valueOf(this.n.getLongitude()), "#.000000");
                String a5 = g.a(Double.valueOf(this.n.getLatitude()), "#.000000");
                str = g.a(Float.valueOf(this.n.getAccuracy()), "#.0");
                str2 = a5;
                str3 = a4;
                str4 = "2";
            }
            str = "0";
            str2 = "0";
            str3 = "0";
            str4 = "0";
        }
        if (this.n == null) {
            this.n = new Location(LocationManagerProxy.NETWORK_PROVIDER);
        } else {
            this.n.reset();
        }
        if (this.I) {
            if (this.g == null) {
                this.g = (LocationManager) Utils.getServ(this.b, LocationManagerProxy.KEY_LOCATION_CHANGED);
            }
            if (Utils.hasNetworkProvider(this.g) && this.g.isProviderEnabled(LocationManagerProxy.NETWORK_PROVIDER) && this.e.isWifiEnabled()) {
                if (this.i == null) {
                    this.i = new LocationListener() {
                        public final void onLocationChanged(Location location) {
                            a.this.n = location;
                        }

                        public final void onProviderDisabled(String str) {
                            a.this.n.reset();
                        }

                        public final void onProviderEnabled(String str) {
                        }

                        public final void onStatusChanged(String str, int i, Bundle bundle) {
                            switch (i) {
                                case 0:
                                    a.this.n.reset();
                                    return;
                                case 1:
                                    a.this.n.reset();
                                    return;
                                case 2:
                                default:
                                    return;
                            }
                        }
                    };
                }
                this.g.requestLocationUpdates(LocationManagerProxy.NETWORK_PROVIDER, 1000, 10.0f, this.i, Looper.getMainLooper());
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e2) {
                    Utils.printE(e2);
                }
                if (this.g != null) {
                    this.g.removeUpdates(this.i);
                }
            }
        }
        if (this.f != null) {
            if (d.a == null || d.a.equals("888888888888888")) {
                d.a = this.f.getDeviceId();
            }
            if (d.a == null) {
                d.a = "888888888888888";
            }
            if (d.b == null || d.b.equals("888888888888888")) {
                d.b = this.f.getSubscriberId();
            }
            if (d.b == null) {
                d.b = "888888888888888";
            }
            if (d.c == null || d.c.equals(PoiTypeDef.All)) {
                d.c = this.f.getLine1Number();
            }
            if (d.c == null) {
                d.c = PoiTypeDef.All;
            }
        }
        if (h.a(this.d.getActiveNetworkInfo()) != -1) {
            str6 = h.a(this.f);
            if (this.e.isWifiEnabled()) {
                str5 = a(this.w) ? "2" : "1";
            } else {
                f();
                str5 = PoiTypeDef.All;
            }
        } else {
            this.w = null;
            str5 = PoiTypeDef.All;
            str6 = PoiTypeDef.All;
        }
        sb.append("<?xml version=\"1.0\" encoding=\"");
        sb.append("GBK\"?>");
        sb.append("<Cell_Req ver=\"2.0\"><HDR version=\"2.0\" cdma=\"");
        sb.append(str8);
        sb.append("\" gtype=\"").append(str4);
        sb.append("\" glong=\"").append(str3);
        sb.append("\" glat=\"").append(str2);
        sb.append("\" precision=\"").append(str);
        sb.append("\"><src>").append(d.d);
        sb.append("</src><license>").append(d.e);
        sb.append("</license><clientid>").append(d.f);
        sb.append("</clientid><imei>").append(d.a);
        sb.append("</imei><imsi>").append(d.b);
        sb.append("</imsi><smac>").append(this.L);
        sb.append("</smac></HDR><DRR phnum=\"").append(d.c);
        sb.append("\" nettype=\"").append(str6);
        sb.append("\" inftype=\"").append(str5).append("\">");
        if (gVar != null) {
            gVar.b = this.s.a(d.a);
            gVar.c = this.s.a(d.b);
            gVar.d = this.s.a(this.L);
        }
        if (this.p.size() > 0 || this.q.size() > 0) {
            StringBuilder sb6 = new StringBuilder();
            switch (this.c) {
                case 1:
                    if (gVar != null) {
                        gVar.a(this.p);
                    }
                    f fVar = this.p.get(0);
                    sb6.delete(0, sb6.length());
                    sb6.append("<mcc>").append(fVar.a()).append("</mcc>");
                    sb6.append("<mnc>").append(fVar.b()).append("</mnc>");
                    sb6.append("<lac>").append(fVar.c()).append("</lac>");
                    sb6.append("<cellid>").append(fVar.d());
                    sb6.append("</cellid>");
                    sb6.append("<signal>").append(fVar.e());
                    sb6.append("</signal>");
                    String sb7 = sb6.toString();
                    for (int i2 = 0; i2 < this.p.size(); i2++) {
                        if (i2 != 0) {
                            f fVar2 = this.p.get(i2);
                            sb3.append(fVar2.c()).append(",");
                            sb3.append(fVar2.d()).append(",");
                            sb3.append(fVar2.e()).append("*");
                        }
                    }
                    sb2 = sb7;
                    break;
                case 2:
                    if (gVar != null) {
                        gVar.b(this.q);
                    }
                    c cVar = this.q.get(0);
                    sb6.delete(0, sb6.length());
                    sb6.append("<mcc>").append(cVar.a()).append("</mcc>");
                    sb6.append("<sid>").append(cVar.e()).append("</sid>");
                    sb6.append("<nid>").append(cVar.f()).append("</nid>");
                    sb6.append("<bid>").append(cVar.g()).append("</bid>");
                    sb6.append("<lon>").append(cVar.d()).append("</lon>");
                    sb6.append("<lat>").append(cVar.c()).append("</lat>");
                    sb6.append("<signal>").append(cVar.h()).append("</signal>");
                    sb2 = sb6.toString();
                    break;
                default:
                    sb2 = PoiTypeDef.All;
                    break;
            }
            sb6.delete(0, sb6.length());
            str7 = sb2;
        } else {
            str7 = PoiTypeDef.All;
        }
        if (this.e.isWifiEnabled()) {
            if (gVar != null) {
                gVar.c(this.r);
            }
            if (a(this.w)) {
                sb5.append(this.w.getBSSID()).append(",");
                sb5.append(this.w.getRssi()).append(",");
                sb5.append(this.w.getSSID().replace("*", "."));
            }
            for (int i3 = 0; i3 < this.r.size(); i3++) {
                ScanResult scanResult = this.r.get(i3);
                sb4.append(scanResult.BSSID).append(",");
                sb4.append(scanResult.level).append(",");
                sb4.append(scanResult.SSID).append("*");
            }
        } else {
            f();
        }
        sb.append(str7);
        sb.append(String.format("<nb>%s</nb>", sb3));
        if (sb4.length() == 0) {
            sb.append(String.format("<macs><![CDATA[%s]]></macs>", sb5));
        } else {
            sb4.deleteCharAt(sb4.length() - 1);
            sb.append(String.format("<macs><![CDATA[%s]]></macs>", sb4));
        }
        sb.append(String.format("<mmac><![CDATA[%s]]></mmac>", sb5));
        sb.append("</DRR></Cell_Req>");
        a(sb);
        sb3.delete(0, sb3.length());
        sb4.delete(0, sb4.length());
        sb5.delete(0, sb5.length());
        return sb;
    }

    /* access modifiers changed from: private */
    public synchronized void a(CellLocation cellLocation) {
        switch (Utils.getCellType(cellLocation, this.b)) {
            case 1:
                if (cellLocation != null) {
                    if (!(this.f == null || ((GsmCellLocation) cellLocation).getLac() == 0)) {
                        List<NeighboringCellInfo> neighboringCellInfo = this.f.getNeighboringCellInfo();
                        this.c = 1;
                        this.p.clear();
                        List<f> list = this.p;
                        GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                        String[] mccMnc = Utils.getMccMnc(this.f);
                        f fVar = new f();
                        fVar.a(mccMnc[0]);
                        fVar.b(mccMnc[1]);
                        fVar.a(gsmCellLocation.getLac());
                        fVar.b(gsmCellLocation.getCid());
                        fVar.c(this.u);
                        list.add(fVar);
                        for (NeighboringCellInfo neighboringCellInfo2 : neighboringCellInfo) {
                            if (neighboringCellInfo2.getCid() <= 65534) {
                                List<f> list2 = this.p;
                                String[] mccMnc2 = Utils.getMccMnc(this.f);
                                f fVar2 = new f();
                                fVar2.a(mccMnc2[0]);
                                fVar2.b(mccMnc2[1]);
                                fVar2.a(neighboringCellInfo2.getLac());
                                fVar2.b(neighboringCellInfo2.getCid());
                                fVar2.c(Utils.asu2Dbm(neighboringCellInfo2.getRssi()));
                                list2.add(fVar2);
                            }
                        }
                        break;
                    }
                }
                break;
            case 2:
                if (cellLocation != null) {
                    CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                    this.c = 2;
                    this.q.clear();
                    String[] mccMnc3 = Utils.getMccMnc(this.f);
                    c cVar = new c();
                    cVar.a(mccMnc3[0]);
                    cVar.b(mccMnc3[1]);
                    cVar.c(cdmaCellLocation.getSystemId());
                    cVar.d(cdmaCellLocation.getNetworkId());
                    cVar.e(cdmaCellLocation.getBaseStationId());
                    cVar.f(this.u);
                    cVar.a(cdmaCellLocation.getBaseStationLatitude());
                    cVar.b(cdmaCellLocation.getBaseStationLongitude());
                    this.q.add(cVar);
                    break;
                }
                break;
        }
    }

    static /* synthetic */ void a(a aVar, int i2) {
        if (aVar.g != null && aVar.g.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER)) {
            switch (i2) {
                case 2:
                    aVar.g();
                    return;
                case 3:
                default:
                    return;
                case 4:
                    if (!(aVar.G != 0 && Utils.getTime() - aVar.G < 3000)) {
                        aVar.G = Utils.getTime();
                        aVar.k = BitmapDescriptorFactory.HUE_RED;
                        aVar.M = aVar.g.getGpsStatus(null);
                        int maxSatellites = aVar.M.getMaxSatellites();
                        Iterator<GpsSatellite> it = aVar.M.getSatellites().iterator();
                        aVar.l = 0;
                        while (it.hasNext() && aVar.l <= maxSatellites) {
                            aVar.k = it.next().getSnr() + aVar.k;
                            aVar.l++;
                        }
                        return;
                    }
                    return;
            }
        }
    }

    static /* synthetic */ void a(a aVar, SignalStrength signalStrength) {
        aVar.u = Utils.asu2Dbm(signalStrength.getGsmSignalStrength());
        switch (aVar.c) {
            case 1:
                if (aVar.p.size() > 0) {
                    aVar.p.get(0).c(aVar.u);
                    return;
                }
                return;
            case 2:
                if (aVar.q.size() > 0) {
                    aVar.q.get(0).f(aVar.u);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private static void a(StringBuilder sb) {
        for (String str : new String[]{" phnum=\"\"", " nettype=\"\"", " inftype=\"\"", "<macs><![CDATA[]]></macs>", "<nb></nb>", "<mmac><![CDATA[]]></mmac>", " gtype=\"0\"", " glong=\"0.0\"", " glat=\"0.0\"", " precision=\"0.0\"", " glong=\"0\"", " glat=\"0\"", " precision=\"0\"", "<smac>null</smac>"}) {
            while (sb.indexOf(str) != -1) {
                int indexOf = sb.indexOf(str);
                sb.delete(indexOf, str.length() + indexOf);
            }
        }
        while (sb.indexOf("*<") != -1) {
            sb.deleteCharAt(sb.indexOf("*<"));
        }
    }

    static /* synthetic */ void a(List list) {
        if (list != null && list.size() > 0) {
            HashMap hashMap = new HashMap();
            for (int i2 = 0; i2 < list.size(); i2++) {
                ScanResult scanResult = (ScanResult) list.get(i2);
                if (WifiManager.calculateSignalLevel(scanResult.level, 20) > 0) {
                    if (scanResult.SSID != null) {
                        scanResult.SSID = scanResult.SSID.replace("*", ".");
                    } else {
                        scanResult.SSID = "null";
                    }
                    hashMap.put(Integer.valueOf(scanResult.level), scanResult);
                    if (i2 > 29) {
                        break;
                    }
                }
            }
            TreeMap treeMap = new TreeMap(Collections.reverseOrder());
            treeMap.putAll(hashMap);
            list.clear();
            for (Object obj : treeMap.keySet()) {
                list.add((ScanResult) treeMap.get(obj));
            }
            hashMap.clear();
            treeMap.clear();
        }
    }

    private static boolean a(WifiInfo wifiInfo) {
        return (wifiInfo == null || wifiInfo.getBSSID() == null || wifiInfo.getBSSID().equals("00:00:00:00:00:00")) ? false : true;
    }

    private boolean a(AmapLoc amapLoc) {
        if (amapLoc != null && !amapLoc.getRetype().equals("5") && !amapLoc.getRetype().equals("6") && amapLoc.getLon() >= 1.0d) {
            return !amapLoc.getProvider().equals(LocationManagerProxy.GPS_PROVIDER) || amapLoc.getAccuracy() <= 500.0f || !this.e.isWifiEnabled() || this.r.size() <= 0;
        }
        return false;
    }

    private StringBuilder b() {
        if (Utils.airPlaneModeOn(this.b)) {
            this.c = 9;
            this.p.clear();
            this.q.clear();
        }
        StringBuilder sb = new StringBuilder(700);
        switch (this.c) {
            case 1:
                for (int i2 = 0; i2 < this.p.size(); i2++) {
                    if (i2 != 0) {
                        f fVar = this.p.get(i2);
                        sb.append("#").append(fVar.b());
                        sb.append("|").append(fVar.c());
                        sb.append("|").append(fVar.d());
                    }
                }
                break;
        }
        if ((this.L == null || this.L.equals("00:00:00:00:00:00")) && this.w != null) {
            this.L = this.w.getMacAddress();
            if (this.L == null) {
                this.L = "00:00:00:00:00:00";
            }
        }
        if (this.e.isWifiEnabled()) {
            String bssid = a(this.w) ? this.w.getBSSID() : PoiTypeDef.All;
            boolean z2 = false;
            for (int i3 = 0; i3 < this.r.size(); i3++) {
                String str = this.r.get(i3).BSSID;
                String str2 = "nb";
                if (bssid.equals(str)) {
                    str2 = "access";
                    z2 = true;
                }
                sb.append(String.format("#%s,%s", str, str2));
            }
            if (!z2 && bssid.length() > 0) {
                sb.append("#").append(bssid);
                sb.append(",access");
            }
        } else {
            f();
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
        }
        return sb;
    }

    private synchronized String c() {
        StringBuilder a2;
        String str = null;
        boolean z2 = true;
        boolean z3 = false;
        synchronized (this) {
            if (Utils.airPlaneModeOn(this.b) ? false : this.F == 0 ? false : Utils.getTime() - this.F >= 60000) {
                CellLocation.requestLocationUpdate();
                this.F = Utils.getTime();
            }
            if (!this.e.isWifiEnabled() ? false : this.H == 0 ? false : Utils.getTime() - this.H >= 20000) {
                h();
            }
            long time = Utils.getTime() - this.B;
            if (!this.D) {
                z2 = false;
            }
            if (time >= 5000) {
                z3 = z2;
            }
            this.D = false;
            if (z3) {
                g gVar = new g();
                StringBuilder a3 = a(gVar);
                j a4 = j.a();
                Context context = this.b;
                a4.b();
                gVar.a();
                this.B = Utils.getTime();
                a2 = a3;
            } else {
                a2 = a((g) null);
            }
            try {
                this.s.a(a2);
                a2.insert(0, "</src><sreq>");
                a2.insert(0, d.d);
                a2.insert(0, "\"?><saps><src>");
                a2.insert(0, "GBK");
                a2.insert(0, "<?xml version=\"1.0\" encoding=\"");
                a2.append("</sreq></saps>");
                str = a2.toString();
            } catch (Exception e2) {
                Utils.printE(e2);
                a2.delete(0, a2.length());
            }
        }
        return str;
    }

    private void d() {
        g();
        try {
            if (this.h != null) {
                this.g.removeUpdates(this.h);
            }
        } catch (Exception e2) {
            Utils.printE(e2);
        } finally {
            this.h = null;
        }
    }

    private void e() {
        this.k = BitmapDescriptorFactory.HUE_RED;
        try {
            if (this.j != null) {
                this.g.removeGpsStatusListener(this.j);
            }
        } catch (Exception e2) {
            Utils.printE(e2);
        } finally {
            this.j = null;
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        this.r.clear();
        this.w = null;
    }

    /* access modifiers changed from: private */
    public void g() {
        this.m.reset();
        this.k = BitmapDescriptorFactory.HUE_RED;
        this.l = 0;
        this.o = 0;
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.e.isWifiEnabled()) {
            this.e.startScan();
            this.H = Utils.getTime();
        }
    }

    public final void destroy() {
        setUpload(false);
        try {
            this.b.unregisterReceiver(this.v);
        } catch (Exception e2) {
        } finally {
            this.v = null;
        }
        d();
        this.n = new Location(LocationManagerProxy.NETWORK_PROVIDER);
        try {
            if (this.i != null) {
                this.g.removeUpdates(this.i);
            }
        } catch (Exception e3) {
            Utils.printE(e3);
        } finally {
            this.i = null;
        }
        e();
        try {
            this.f.listen(this.t, 0);
        } catch (Exception e4) {
            Utils.printE(e4);
        }
        b.a().b();
        this.C = false;
        this.z = 0;
        this.p.clear();
        this.q.clear();
        this.u = -113;
        f();
        this.x = null;
        this.y = null;
        g();
        this.M = null;
        this.b = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.j = null;
        a = null;
        System.gc();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x020d, code lost:
        if (r1 != false) goto L_0x020f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.autonavi.aps.amapapi.AmapLoc getLocation() {
        /*
            r11 = this;
            r7 = 3
            r10 = 1101004800(0x41a00000, float:20.0)
            r1 = 1
            r2 = 0
            monitor-enter(r11)
            int r0 = r11.K     // Catch:{ all -> 0x0041 }
            int r0 = r0 + 1
            r11.K = r0     // Catch:{ all -> 0x0041 }
            int r0 = r11.K     // Catch:{ all -> 0x0041 }
            if (r0 != r1) goto L_0x001d
            android.net.wifi.WifiManager r0 = r11.e     // Catch:{ all -> 0x0041 }
            boolean r0 = r0.isWifiEnabled()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x001d
            r3 = 2000(0x7d0, double:9.88E-321)
            java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x003c }
        L_0x001d:
            long r3 = r11.z     // Catch:{ all -> 0x0041 }
            long r5 = com.autonavi.aps.amapapi.Utils.getTime()     // Catch:{ all -> 0x0041 }
            long r3 = r5 - r3
            r5 = 100
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x028a
            r0 = r1
        L_0x002c:
            if (r0 == 0) goto L_0x0044
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.y     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0044
            long r0 = com.autonavi.aps.amapapi.Utils.getTime()     // Catch:{ all -> 0x0041 }
            r11.z = r0     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.y     // Catch:{ all -> 0x0041 }
        L_0x003a:
            monitor-exit(r11)
            return r0
        L_0x003c:
            r0 = move-exception
            com.autonavi.aps.amapapi.Utils.printE(r0)     // Catch:{ all -> 0x0041 }
            goto L_0x001d
        L_0x0041:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x0044:
            android.content.Context r0 = r11.b     // Catch:{ all -> 0x0041 }
            boolean r0 = com.autonavi.aps.amapapi.Utils.airPlaneModeOn(r0)     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x005a
            r0 = 9
            r11.c = r0     // Catch:{ all -> 0x0041 }
            java.util.List<com.autonavi.aps.amapapi.f> r0 = r11.p     // Catch:{ all -> 0x0041 }
            r0.clear()     // Catch:{ all -> 0x0041 }
            java.util.List<com.autonavi.aps.amapapi.c> r0 = r11.q     // Catch:{ all -> 0x0041 }
            r0.clear()     // Catch:{ all -> 0x0041 }
        L_0x005a:
            java.lang.String r0 = ""
            android.location.Location r3 = r11.m     // Catch:{ all -> 0x0041 }
            boolean r3 = com.autonavi.aps.amapapi.Utils.coordInCN(r3)     // Catch:{ all -> 0x0041 }
            if (r3 == 0) goto L_0x0086
            java.lang.String r3 = "gps"
        L_0x0066:
            android.net.wifi.WifiManager r4 = r11.e     // Catch:{ all -> 0x0041 }
            boolean r4 = r4.isWifiEnabled()     // Catch:{ all -> 0x0041 }
            if (r4 == 0) goto L_0x0089
            android.net.wifi.WifiManager r4 = r11.e     // Catch:{ all -> 0x0041 }
            android.net.wifi.WifiInfo r4 = r4.getConnectionInfo()     // Catch:{ all -> 0x0041 }
            r11.w = r4     // Catch:{ all -> 0x0041 }
        L_0x0076:
            int r4 = r11.c     // Catch:{ all -> 0x0041 }
            switch(r4) {
                case 1: goto L_0x008d;
                case 2: goto L_0x00fe;
                case 9: goto L_0x017d;
                default: goto L_0x007b;
            }     // Catch:{ all -> 0x0041 }
        L_0x007b:
            r3 = r0
        L_0x007c:
            java.lang.String r0 = ""
            boolean r0 = r3.equals(r0)     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0192
            r0 = 0
            goto L_0x003a
        L_0x0086:
            java.lang.String r3 = "network"
            goto L_0x0066
        L_0x0089:
            r11.f()     // Catch:{ all -> 0x0041 }
            goto L_0x0076
        L_0x008d:
            java.util.List<com.autonavi.aps.amapapi.f> r4 = r11.p     // Catch:{ all -> 0x0041 }
            int r4 = r4.size()     // Catch:{ all -> 0x0041 }
            if (r4 <= 0) goto L_0x007b
            java.util.List<com.autonavi.aps.amapapi.f> r0 = r11.p     // Catch:{ all -> 0x0041 }
            r4 = 0
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.f r0 = (com.autonavi.aps.amapapi.f) r0     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0041 }
            r4.<init>()     // Catch:{ all -> 0x0041 }
            java.lang.String r5 = r0.a()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.String r6 = "#"
            r5.append(r6)     // Catch:{ all -> 0x0041 }
            java.lang.String r5 = r0.b()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.String r6 = "#"
            r5.append(r6)     // Catch:{ all -> 0x0041 }
            int r5 = r0.c()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.String r6 = "#"
            r5.append(r6)     // Catch:{ all -> 0x0041 }
            int r0 = r0.d()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0041 }
            java.lang.String r5 = "#"
            r0.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r0 = r4.append(r3)     // Catch:{ all -> 0x0041 }
            java.lang.String r3 = "#"
            r0.append(r3)     // Catch:{ all -> 0x0041 }
            java.util.List<android.net.wifi.ScanResult> r0 = r11.r     // Catch:{ all -> 0x0041 }
            int r0 = r0.size()     // Catch:{ all -> 0x0041 }
            if (r0 > 0) goto L_0x00f0
            android.net.wifi.WifiInfo r0 = r11.w     // Catch:{ all -> 0x0041 }
            boolean r0 = a(r0)     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x00fb
        L_0x00f0:
            java.lang.String r0 = "cellwifi"
        L_0x00f2:
            r4.append(r0)     // Catch:{ all -> 0x0041 }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x0041 }
            r3 = r0
            goto L_0x007c
        L_0x00fb:
            java.lang.String r0 = "cell"
            goto L_0x00f2
        L_0x00fe:
            java.util.List<com.autonavi.aps.amapapi.c> r4 = r11.q     // Catch:{ all -> 0x0041 }
            int r4 = r4.size()     // Catch:{ all -> 0x0041 }
            if (r4 <= 0) goto L_0x007b
            java.util.List<com.autonavi.aps.amapapi.c> r0 = r11.q     // Catch:{ all -> 0x0041 }
            r4 = 0
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.c r0 = (com.autonavi.aps.amapapi.c) r0     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0041 }
            r4.<init>()     // Catch:{ all -> 0x0041 }
            java.lang.String r5 = r0.a()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.String r6 = "#"
            r5.append(r6)     // Catch:{ all -> 0x0041 }
            java.lang.String r5 = r0.b()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.String r6 = "#"
            r5.append(r6)     // Catch:{ all -> 0x0041 }
            int r5 = r0.e()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.String r6 = "#"
            r5.append(r6)     // Catch:{ all -> 0x0041 }
            int r5 = r0.f()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r5 = r4.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.String r6 = "#"
            r5.append(r6)     // Catch:{ all -> 0x0041 }
            int r0 = r0.g()     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0041 }
            java.lang.String r5 = "#"
            r0.append(r5)     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r0 = r4.append(r3)     // Catch:{ all -> 0x0041 }
            java.lang.String r3 = "#"
            r0.append(r3)     // Catch:{ all -> 0x0041 }
            java.util.List<android.net.wifi.ScanResult> r0 = r11.r     // Catch:{ all -> 0x0041 }
            int r0 = r0.size()     // Catch:{ all -> 0x0041 }
            if (r0 > 0) goto L_0x016e
            android.net.wifi.WifiInfo r0 = r11.w     // Catch:{ all -> 0x0041 }
            boolean r0 = a(r0)     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x017a
        L_0x016e:
            java.lang.String r0 = "cellwifi"
        L_0x0170:
            r4.append(r0)     // Catch:{ all -> 0x0041 }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x0041 }
            r3 = r0
            goto L_0x007c
        L_0x017a:
            java.lang.String r0 = "cell"
            goto L_0x0170
        L_0x017d:
            java.util.List<android.net.wifi.ScanResult> r3 = r11.r     // Catch:{ all -> 0x0041 }
            int r3 = r3.size()     // Catch:{ all -> 0x0041 }
            if (r3 > 0) goto L_0x018d
            android.net.wifi.WifiInfo r3 = r11.w     // Catch:{ all -> 0x0041 }
            boolean r3 = a(r3)     // Catch:{ all -> 0x0041 }
            if (r3 == 0) goto L_0x007b
        L_0x018d:
            java.lang.String r0 = "#wifi"
            r3 = r0
            goto L_0x007c
        L_0x0192:
            java.lang.StringBuilder r4 = r11.b()     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.b r0 = com.autonavi.aps.amapapi.b.a()     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r0 = r0.a(r3, r4)     // Catch:{ all -> 0x0041 }
            if (r0 != 0) goto L_0x0249
            java.lang.String r0 = "#gps"
            int r0 = r3.indexOf(r0)     // Catch:{ all -> 0x0041 }
            r5 = -1
            if (r0 == r5) goto L_0x01de
            r0 = 0
            r11.I = r0     // Catch:{ all -> 0x0041 }
            java.lang.String r0 = r11.c()     // Catch:{ all -> 0x0041 }
            boolean r1 = com.autonavi.aps.amapapi.d.h     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x01d5
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.a(r0)     // Catch:{ all -> 0x0041 }
            r11.y = r0     // Catch:{ all -> 0x0041 }
        L_0x01ba:
            com.autonavi.aps.amapapi.b r0 = com.autonavi.aps.amapapi.b.a()     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r1 = r11.y     // Catch:{ all -> 0x0041 }
            r0.a(r3, r1, r4)     // Catch:{ all -> 0x0041 }
            r0 = 0
            int r1 = r4.length()     // Catch:{ all -> 0x0041 }
            r4.delete(r0, r1)     // Catch:{ all -> 0x0041 }
            long r0 = com.autonavi.aps.amapapi.Utils.getTime()     // Catch:{ all -> 0x0041 }
            r11.z = r0     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.y     // Catch:{ all -> 0x0041 }
            goto L_0x003a
        L_0x01d5:
            android.location.Location r0 = r11.m     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.a(r0)     // Catch:{ all -> 0x0041 }
            r11.y = r0     // Catch:{ all -> 0x0041 }
            goto L_0x01ba
        L_0x01de:
            int r0 = r11.K     // Catch:{ all -> 0x0041 }
            if (r0 <= r7) goto L_0x01ff
            r0 = r1
        L_0x01e3:
            r11.I = r0     // Catch:{ all -> 0x0041 }
            java.lang.String r0 = r11.c()     // Catch:{ all -> 0x0041 }
            java.lang.String r5 = r11.x     // Catch:{ all -> 0x0041 }
            boolean r5 = r0.equals(r5)     // Catch:{ all -> 0x0041 }
            if (r5 == 0) goto L_0x0201
            com.autonavi.aps.amapapi.AmapLoc r5 = r11.y     // Catch:{ all -> 0x0041 }
            if (r5 == 0) goto L_0x0201
            long r0 = com.autonavi.aps.amapapi.Utils.getTime()     // Catch:{ all -> 0x0041 }
            r11.z = r0     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.y     // Catch:{ all -> 0x0041 }
            goto L_0x003a
        L_0x01ff:
            r0 = r2
            goto L_0x01e3
        L_0x0201:
            r11.x = r0     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.a(r0)     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r5 = r11.y     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x020d
            if (r5 != 0) goto L_0x0212
        L_0x020d:
            if (r1 == 0) goto L_0x01ba
        L_0x020f:
            r11.y = r0     // Catch:{ all -> 0x0041 }
            goto L_0x01ba
        L_0x0212:
            r6 = 4
            double[] r6 = new double[r6]     // Catch:{ all -> 0x0041 }
            r7 = 0
            double r8 = r0.getLat()     // Catch:{ all -> 0x0041 }
            r6[r7] = r8     // Catch:{ all -> 0x0041 }
            r7 = 1
            double r8 = r0.getLon()     // Catch:{ all -> 0x0041 }
            r6[r7] = r8     // Catch:{ all -> 0x0041 }
            r7 = 2
            double r8 = r5.getLat()     // Catch:{ all -> 0x0041 }
            r6[r7] = r8     // Catch:{ all -> 0x0041 }
            r7 = 3
            double r8 = r5.getLon()     // Catch:{ all -> 0x0041 }
            r6[r7] = r8     // Catch:{ all -> 0x0041 }
            float r6 = com.autonavi.aps.amapapi.Utils.distance(r6)     // Catch:{ all -> 0x0041 }
            int r6 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r6 > 0) goto L_0x020d
            float r6 = r0.getAccuracy()     // Catch:{ all -> 0x0041 }
            float r5 = r5.getAccuracy()     // Catch:{ all -> 0x0041 }
            float r5 = r6 - r5
            int r5 = (r5 > r10 ? 1 : (r5 == r10 ? 0 : -1))
            if (r5 > 0) goto L_0x020d
            r1 = r2
            goto L_0x020d
        L_0x0249:
            long r5 = r11.z     // Catch:{ all -> 0x0041 }
            r7 = 0
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 == 0) goto L_0x027e
            long r5 = com.autonavi.aps.amapapi.Utils.getTime()     // Catch:{ all -> 0x0041 }
            long r7 = r11.A     // Catch:{ all -> 0x0041 }
            long r5 = r5 - r7
            r7 = 300000(0x493e0, double:1.482197E-318)
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 <= 0) goto L_0x027e
        L_0x025f:
            if (r1 == 0) goto L_0x020f
            r0 = 1
            r11.I = r0     // Catch:{ all -> 0x0041 }
            java.lang.String r0 = r11.c()     // Catch:{ all -> 0x0041 }
            java.lang.String r1 = r11.x     // Catch:{ all -> 0x0041 }
            boolean r1 = r0.equals(r1)     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x0280
            com.autonavi.aps.amapapi.AmapLoc r1 = r11.y     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x0280
            long r0 = com.autonavi.aps.amapapi.Utils.getTime()     // Catch:{ all -> 0x0041 }
            r11.z = r0     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.y     // Catch:{ all -> 0x0041 }
            goto L_0x003a
        L_0x027e:
            r1 = r2
            goto L_0x025f
        L_0x0280:
            r11.x = r0     // Catch:{ all -> 0x0041 }
            com.autonavi.aps.amapapi.AmapLoc r0 = r11.a(r0)     // Catch:{ all -> 0x0041 }
            r11.y = r0     // Catch:{ all -> 0x0041 }
            goto L_0x01ba
        L_0x028a:
            r0 = r2
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.autonavi.aps.amapapi.a.getLocation():com.autonavi.aps.amapapi.AmapLoc");
    }

    public final String getVersion() {
        return "2.0.201305091240";
    }

    public final void init(Context context) {
        this.b = context.getApplicationContext();
        Utils.toast(this.b, "in debug mode, only for test");
        this.m = new Location(LocationManagerProxy.GPS_PROVIDER);
        this.n = new Location(LocationManagerProxy.NETWORK_PROVIDER);
        this.e = (WifiManager) Utils.getServ(this.b, "wifi");
        h();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        this.b.registerReceiver(this.v, intentFilter);
        this.d = (ConnectivityManager) Utils.getServ(this.b, "connectivity");
        CellLocation.requestLocationUpdate();
        this.f = (TelephonyManager) Utils.getServ(this.b, "phone");
        this.t = new PhoneStateListener() {
            public final void onCellLocationChanged(CellLocation cellLocation) {
                a.this.D = true;
                if (!(a.this.E != 0 && Utils.getTime() - a.this.E < 8000)) {
                    a.this.a(cellLocation);
                    a.this.E = Utils.getTime();
                }
            }

            public final void onServiceStateChanged(ServiceState serviceState) {
                switch (serviceState.getState()) {
                    case 1:
                        a.this.p.clear();
                        a.this.q.clear();
                        a.this.u = -113;
                        return;
                    default:
                        return;
                }
            }

            public final void onSignalStrengthsChanged(SignalStrength signalStrength) {
                a.a(a.this, signalStrength);
            }
        };
        this.f.listen(this.t, 272);
    }

    public final void setAuth(String str) {
        if (str != null && str.indexOf("##") != -1) {
            String[] split = str.split("##");
            if (split.length == 3) {
                d.d = split[0];
                d.e = split[1];
                d.f = split[2];
            }
        }
    }

    public final void setGpsOffset(boolean z2) {
        d.h = z2;
    }

    public final void setUpload(boolean z2) {
        if (z2) {
            j.b(this.b.getApplicationContext());
        } else {
            j.c(this.b.getApplicationContext());
        }
    }

    public final void setUseCache(boolean z2) {
        d.i = z2;
    }

    public final void setUseGps(boolean z2) {
        if (z2 != d.g && !this.C) {
            this.C = true;
            if (this.g == null) {
                this.g = (LocationManager) Utils.getServ(this.b, LocationManagerProxy.KEY_LOCATION_CHANGED);
            }
            d.g = z2;
            if (z2) {
                if (Utils.hasGpsProvider(this.g)) {
                    d();
                    this.h = new LocationListener() {
                        public final void onLocationChanged(Location location) {
                            a.this.D = true;
                            a.this.m = location;
                            a.this.o = Utils.getTime();
                        }

                        public final void onProviderDisabled(String str) {
                            a.this.g();
                        }

                        public final void onProviderEnabled(String str) {
                        }

                        public final void onStatusChanged(String str, int i, Bundle bundle) {
                            switch (i) {
                                case 0:
                                    a.this.g();
                                    return;
                                case 1:
                                    a.this.g();
                                    return;
                                case 2:
                                default:
                                    return;
                            }
                        }
                    };
                    this.g.sendExtraCommand(LocationManagerProxy.GPS_PROVIDER, "force_xtra_injection", null);
                    this.g.requestLocationUpdates(LocationManagerProxy.GPS_PROVIDER, 400, (float) BitmapDescriptorFactory.HUE_RED, this.h, Looper.getMainLooper());
                }
                if (Utils.hasGpsProvider(this.g)) {
                    e();
                    this.j = new GpsStatus.Listener() {
                        public final void onGpsStatusChanged(int i) {
                            a.a(a.this, i);
                        }
                    };
                    this.g.addGpsStatusListener(this.j);
                }
            } else {
                d();
                e();
            }
            this.C = false;
        }
    }
}
