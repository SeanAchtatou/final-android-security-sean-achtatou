package com.autonavi.aps.amapapi;

import android.content.Context;

public interface IAPS {
    void destroy();

    AmapLoc getLocation();

    String getVersion();

    void init(Context context);

    void setAuth(String str);

    void setGpsOffset(boolean z);

    void setUpload(boolean z);

    void setUseCache(boolean z);

    void setUseGps(boolean z);
}
