package com.autonavi.aps.amapapi;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

public final class e {
    private String a = "DESede";
    private Cipher b = null;
    private Cipher c = null;

    public e() {
        try {
            SecureRandom secureRandom = new SecureRandom();
            SecretKey generateSecret = SecretKeyFactory.getInstance(this.a).generateSecret(new DESedeKeySpec("autonavi00spas$#@!666666".getBytes()));
            this.b = Cipher.getInstance(this.a);
            this.b.init(1, generateSecret, secureRandom);
            this.c = Cipher.getInstance(this.a);
            this.c.init(2, generateSecret, secureRandom);
        } catch (Exception e) {
            Utils.printE(e);
        }
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                sb.append(String.format("0%s", hexString));
            } else {
                sb.append(hexString);
            }
        }
        return sb.toString();
    }

    private static byte[] b(String str) {
        byte[] bArr = null;
        if (!(str == null || str.length() == 0 || str.length() % 2 == 1)) {
            try {
                bArr = new byte[(str.length() / 2)];
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < str.length(); i += 2) {
                    sb.delete(0, sb.length());
                    sb.append("0X");
                    sb.append(str.substring(i, i + 2));
                    bArr[i / 2] = (byte) Integer.decode(sb.toString()).intValue();
                }
            } catch (Exception e) {
                Utils.printE(e);
            }
        }
        return bArr;
    }

    public final String a(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        try {
            return a(this.b.doFinal(str.getBytes()));
        } catch (Exception e) {
            Utils.printE(e);
            return null;
        }
    }

    public final String a(String str, String str2) {
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            return new String(this.c.doFinal(b(str)), str2);
        } catch (Exception e) {
            Utils.printE(e);
            return null;
        }
    }

    public final void a(StringBuilder sb) {
        if (sb != null && sb.length() != 0) {
            try {
                byte[] bytes = sb.toString().getBytes();
                sb.delete(0, sb.length());
                sb.append(a(this.b.doFinal(bytes)));
            } catch (Exception e) {
                Utils.printE(e);
            }
        }
    }
}
