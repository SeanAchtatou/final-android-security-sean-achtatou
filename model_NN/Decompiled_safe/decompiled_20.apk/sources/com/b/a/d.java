package com.b.a;

public final class d extends RuntimeException {
    public d() {
    }

    public d(String str) {
        super(str);
    }

    public d(String str, Throwable th) {
        super(str, th);
    }
}
