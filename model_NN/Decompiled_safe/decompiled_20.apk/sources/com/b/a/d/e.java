package com.b.a.d;

public class e<K, V> {
    private final f<K, V>[] a;
    private final int b;

    public e() {
        this((byte) 0);
    }

    public e(byte b2) {
        this.b = 1023;
        this.a = new f[1024];
    }

    public final V a(Object obj) {
        for (f<K, V> fVar = this.a[System.identityHashCode(obj) & this.b]; fVar != null; fVar = fVar.d) {
            if (obj == fVar.b) {
                return fVar.c;
            }
        }
        return null;
    }

    public final boolean a(K k, V v) {
        int identityHashCode = System.identityHashCode(k);
        int i = identityHashCode & this.b;
        for (f<K, V> fVar = this.a[i]; fVar != null; fVar = fVar.d) {
            if (k == fVar.b) {
                fVar.c = v;
                return true;
            }
        }
        this.a[i] = new f<>(k, v, identityHashCode, this.a[i]);
        return false;
    }
}
