package com.b.a.d;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public final class c implements Comparable<c> {
    private final String a;
    private final Method b;
    private final Field c;
    private final Class<?> d;
    private final Type e;
    private final Class<?> f;

    public c(String str, Class<?> cls, Class<?> cls2, Type type, Field field) {
        this.a = str;
        this.f = cls;
        this.d = cls2;
        this.e = type;
        this.b = null;
        this.c = field;
        if (field != null) {
            field.setAccessible(true);
        }
    }

    public c(String str, Method method, Field field) {
        this(str, method, field, (Class<?>) null, (Type) null);
    }

    public c(String str, Method method, Field field, Class<?> cls, Type type) {
        Class<?> type2;
        Type genericType;
        Class<?> cls2;
        Type a2;
        Class<?> returnType;
        this.a = str;
        this.b = method;
        this.c = field;
        if (method != null) {
            method.setAccessible(true);
        }
        if (field != null) {
            field.setAccessible(true);
        }
        if (method != null) {
            if (method.getParameterTypes().length == 1) {
                returnType = method.getParameterTypes()[0];
                genericType = method.getGenericParameterTypes()[0];
            } else {
                returnType = method.getReturnType();
                genericType = method.getGenericReturnType();
            }
            this.f = method.getDeclaringClass();
            type2 = returnType;
        } else {
            type2 = field.getType();
            genericType = field.getGenericType();
            this.f = field.getDeclaringClass();
        }
        if (cls == null || type2 != Object.class || !(genericType instanceof TypeVariable) || (a2 = a(cls, (TypeVariable<?>) ((TypeVariable) genericType))) == null) {
            Type a3 = a(cls, type, genericType);
            if (a3 != genericType) {
                if (a3 instanceof ParameterizedType) {
                    cls2 = g.a(a3);
                } else if (a3 instanceof Class) {
                    cls2 = g.a(a3);
                }
                this.e = a3;
                this.d = cls2;
                return;
            }
            cls2 = type2;
            this.e = a3;
            this.d = cls2;
            return;
        }
        this.d = g.a(a2);
        this.e = a2;
    }

    private static Type a(Class<?> cls, Type type, Type type2) {
        if (cls == null || type == null || !(type instanceof ParameterizedType) || !(type2 instanceof TypeVariable)) {
            return type2;
        }
        ParameterizedType parameterizedType = (ParameterizedType) type;
        TypeVariable typeVariable = (TypeVariable) type2;
        for (int i = 0; i < cls.getTypeParameters().length; i++) {
            if (cls.getTypeParameters()[i].getName().equals(typeVariable.getName())) {
                return parameterizedType.getActualTypeArguments()[i];
            }
        }
        return type2;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.reflect.GenericDeclaration] */
    private static Type a(Class<?> cls, TypeVariable<?> typeVariable) {
        Type genericSuperclass;
        ? genericDeclaration = typeVariable.getGenericDeclaration();
        do {
            genericSuperclass = cls.getGenericSuperclass();
            if (genericSuperclass == null) {
                return null;
            }
            if (genericSuperclass instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
                if (parameterizedType.getRawType() == genericDeclaration) {
                    TypeVariable<?>[] typeParameters = genericDeclaration.getTypeParameters();
                    Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                    for (int i = 0; i < typeParameters.length; i++) {
                        if (typeParameters[i] == typeVariable) {
                            return actualTypeArguments[i];
                        }
                    }
                    return null;
                }
            }
            cls = g.a(genericSuperclass);
        } while (genericSuperclass != null);
        return null;
    }

    public final Class<?> a() {
        return this.d;
    }

    public final Object a(Object obj) {
        return this.b != null ? this.b.invoke(obj, new Object[0]) : this.c.get(obj);
    }

    public final <T extends Annotation> T a(Class cls) {
        T t = null;
        if (this.b != null) {
            t = this.b.getAnnotation(cls);
        }
        return (t != null || this.c == null) ? t : this.c.getAnnotation(cls);
    }

    public final void a(Object obj, Object obj2) {
        if (this.b != null) {
            this.b.invoke(obj, obj2);
            return;
        }
        this.c.set(obj, obj2);
    }

    public final Type b() {
        return this.e;
    }

    public final String c() {
        return this.a;
    }

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return this.a.compareTo(((c) obj).a);
    }

    public final Method d() {
        return this.b;
    }

    public final Field e() {
        return this.c;
    }

    public final void f() {
        if (this.b != null) {
            this.b.setAccessible(true);
        } else {
            this.c.setAccessible(true);
        }
    }

    public final String toString() {
        return this.a;
    }
}
