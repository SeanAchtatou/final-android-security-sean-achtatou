package com.b.a.d;

import com.b.a.a.a;
import com.b.a.d;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public final class b {
    private final Class<?> a;
    private final Type b;
    private Constructor<?> c;
    private Constructor<?> d;
    private Method e;
    private final List<c> f = new ArrayList();

    private b(Class<?> cls) {
        this.a = cls;
        this.b = cls;
    }

    public static b a(Class<?> cls, Type type) {
        Constructor<?> constructor;
        Constructor<?> constructor2;
        Constructor<?> constructor3;
        Method method;
        com.b.a.a.b bVar;
        b bVar2 = new b(cls);
        if (Modifier.isAbstract(cls.getModifiers())) {
            constructor2 = null;
        } else {
            Constructor<?>[] declaredConstructors = cls.getDeclaredConstructors();
            int length = declaredConstructors.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    constructor = null;
                    break;
                }
                constructor = declaredConstructors[i];
                if (constructor.getParameterTypes().length == 0) {
                    break;
                }
                i++;
            }
            if (constructor == null && cls.isMemberClass() && !Modifier.isStatic(cls.getModifiers())) {
                Constructor<?>[] declaredConstructors2 = cls.getDeclaredConstructors();
                int length2 = declaredConstructors2.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length2) {
                        break;
                    }
                    constructor2 = declaredConstructors2[i2];
                    if (constructor2.getParameterTypes().length == 1 && constructor2.getParameterTypes()[0].equals(cls.getDeclaringClass())) {
                        break;
                    }
                    i2++;
                }
            }
            constructor2 = constructor;
        }
        if (constructor2 != null) {
            constructor2.setAccessible(true);
            bVar2.c = constructor2;
        } else if (constructor2 == null && !cls.isInterface() && !Modifier.isAbstract(cls.getModifiers())) {
            Constructor<?>[] declaredConstructors3 = cls.getDeclaredConstructors();
            int length3 = declaredConstructors3.length;
            int i3 = 0;
            while (true) {
                if (i3 >= length3) {
                    constructor3 = null;
                    break;
                }
                Constructor<?> constructor4 = declaredConstructors3[i3];
                if (((a) constructor4.getAnnotation(a.class)) != null) {
                    constructor3 = constructor4;
                    break;
                }
                i3++;
            }
            if (constructor3 != null) {
                constructor3.setAccessible(true);
                bVar2.d = constructor3;
                int i4 = 0;
                while (true) {
                    int i5 = i4;
                    if (i5 >= constructor3.getParameterTypes().length) {
                        return bVar2;
                    }
                    Annotation[] annotationArr = constructor3.getParameterAnnotations()[i5];
                    com.b.a.a.b bVar3 = null;
                    int length4 = annotationArr.length;
                    int i6 = 0;
                    while (true) {
                        if (i6 >= length4) {
                            break;
                        }
                        Annotation annotation = annotationArr[i6];
                        if (annotation instanceof com.b.a.a.b) {
                            bVar3 = (com.b.a.a.b) annotation;
                            break;
                        }
                        i6++;
                    }
                    if (bVar3 == null) {
                        throw new d("illegal json creator");
                    }
                    Class<?> cls2 = constructor3.getParameterTypes()[i5];
                    Type type2 = constructor3.getGenericParameterTypes()[i5];
                    Field a2 = a(cls, bVar3.a());
                    if (a2 != null) {
                        a2.setAccessible(true);
                    }
                    bVar2.a(new c(bVar3.a(), cls, cls2, type2, a2));
                    i4 = i5 + 1;
                }
            } else {
                Method[] declaredMethods = cls.getDeclaredMethods();
                int length5 = declaredMethods.length;
                int i7 = 0;
                while (true) {
                    if (i7 >= length5) {
                        method = null;
                        break;
                    }
                    Method method2 = declaredMethods[i7];
                    if (Modifier.isStatic(method2.getModifiers()) && cls.isAssignableFrom(method2.getReturnType()) && ((a) method2.getAnnotation(a.class)) != null) {
                        method = method2;
                        break;
                    }
                    i7++;
                }
                if (method != null) {
                    method.setAccessible(true);
                    bVar2.e = method;
                    int i8 = 0;
                    while (true) {
                        int i9 = i8;
                        if (i9 >= method.getParameterTypes().length) {
                            return bVar2;
                        }
                        Annotation[] annotationArr2 = method.getParameterAnnotations()[i9];
                        com.b.a.a.b bVar4 = null;
                        int length6 = annotationArr2.length;
                        int i10 = 0;
                        while (true) {
                            if (i10 >= length6) {
                                break;
                            }
                            Annotation annotation2 = annotationArr2[i10];
                            if (annotation2 instanceof com.b.a.a.b) {
                                bVar4 = (com.b.a.a.b) annotation2;
                                break;
                            }
                            i10++;
                        }
                        if (bVar4 == null) {
                            throw new d("illegal json creator");
                        }
                        Class<?> cls3 = method.getParameterTypes()[i9];
                        Type type3 = method.getGenericParameterTypes()[i9];
                        Field a3 = a(cls, bVar4.a());
                        if (a3 != null) {
                            a3.setAccessible(true);
                        }
                        bVar2.a(new c(bVar4.a(), cls, cls3, type3, a3));
                        i8 = i9 + 1;
                    }
                } else {
                    throw new d("default constructor not found. " + cls);
                }
            }
        }
        for (Method method3 : cls.getMethods()) {
            String name = method3.getName();
            if (name.length() >= 4 && !Modifier.isStatic(method3.getModifiers()) && ((method3.getReturnType().equals(Void.TYPE) || method3.getReturnType().equals(cls)) && method3.getParameterTypes().length == 1)) {
                com.b.a.a.b bVar5 = (com.b.a.a.b) method3.getAnnotation(com.b.a.a.b.class);
                if (bVar5 != null) {
                    if (bVar5.d()) {
                        if (bVar5.a().length() != 0) {
                            bVar2.a(new c(bVar5.a(), method3, (Field) null, cls, type));
                            method3.setAccessible(true);
                        }
                    }
                }
                if (name.startsWith("set") && Character.isUpperCase(name.charAt(3))) {
                    String str = Character.toLowerCase(name.charAt(3)) + name.substring(4);
                    Field a4 = a(cls, str);
                    if (a4 == null || (bVar = (com.b.a.a.b) a4.getAnnotation(com.b.a.a.b.class)) == null || bVar.a().length() == 0) {
                        bVar2.a(new c(str, method3, (Field) null, cls, type));
                        method3.setAccessible(true);
                    } else {
                        bVar2.a(new c(bVar.a(), method3, a4, cls, type));
                    }
                }
            }
        }
        for (Field field : cls.getFields()) {
            if (!Modifier.isStatic(field.getModifiers()) && Modifier.isPublic(field.getModifiers())) {
                boolean z = false;
                for (c c2 : bVar2.f) {
                    if (c2.c().equals(field.getName())) {
                        z = true;
                    }
                }
                if (!z) {
                    bVar2.a(new c(field.getName(), null, field));
                }
            }
        }
        return bVar2;
    }

    private static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Exception e2) {
            return null;
        }
    }

    private boolean a(c cVar) {
        for (c c2 : this.f) {
            if (c2.c().equals(cVar.c())) {
                return false;
            }
        }
        this.f.add(cVar);
        return true;
    }

    public final Constructor<?> a() {
        return this.c;
    }

    public final Constructor<?> b() {
        return this.d;
    }

    public final Method c() {
        return this.e;
    }

    public final List<c> d() {
        return this.f;
    }
}
