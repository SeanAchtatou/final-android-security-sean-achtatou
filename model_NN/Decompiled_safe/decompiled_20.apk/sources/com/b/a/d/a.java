package com.b.a.d;

import java.util.Arrays;

public final class a {
    public static final char[] a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
    public static final int[] b;

    static {
        int[] iArr = new int[256];
        b = iArr;
        Arrays.fill(iArr, -1);
        int length = a.length;
        for (int i = 0; i < length; i++) {
            b[a[i]] = i;
        }
        b[61] = 0;
    }

    public static final byte[] a(char[] cArr, int i, int i2) {
        int i3;
        int i4;
        int i5 = 0;
        if (i2 == 0) {
            return new byte[0];
        }
        int i6 = (i + i2) - 1;
        int i7 = i;
        while (i3 < i6 && b[cArr[i3]] < 0) {
            i7 = i3 + 1;
        }
        int i8 = i6;
        while (i8 > 0 && b[cArr[i8]] < 0) {
            i8--;
        }
        int i9 = cArr[i8] == '=' ? cArr[i8 + -1] == '=' ? 2 : 1 : 0;
        int i10 = (i8 - i3) + 1;
        if (i2 > 76) {
            i4 = (cArr[76] == 13 ? i10 / 78 : 0) << 1;
        } else {
            i4 = 0;
        }
        int i11 = (((i10 - i4) * 6) >> 3) - i9;
        byte[] bArr = new byte[i11];
        int i12 = (i11 / 3) * 3;
        int i13 = 0;
        int i14 = 0;
        while (i14 < i12) {
            int i15 = i3 + 1;
            int i16 = i15 + 1;
            int i17 = (b[cArr[i3]] << 18) | (b[cArr[i15]] << 12);
            int i18 = i16 + 1;
            int i19 = (b[cArr[i16]] << 6) | i17;
            i3 = i18 + 1;
            int i20 = i19 | b[cArr[i18]];
            int i21 = i14 + 1;
            bArr[i14] = (byte) (i20 >> 16);
            int i22 = i21 + 1;
            bArr[i21] = (byte) (i20 >> 8);
            i14 = i22 + 1;
            bArr[i22] = (byte) i20;
            if (i4 > 0 && (i13 = i13 + 1) == 19) {
                i3 += 2;
                i13 = 0;
            }
        }
        if (i14 < i11) {
            int i23 = 0;
            for (int i24 = i3; i24 <= i8 - i9; i24++) {
                i5++;
                i23 = (b[cArr[i24]] << (18 - (i5 * 6))) | i23;
            }
            int i25 = 16;
            for (int i26 = i14; i26 < i11; i26++) {
                bArr[i26] = (byte) (i23 >> i25);
                i25 -= 8;
            }
        }
        return bArr;
    }
}
