package com.b.a.d;

import com.b.a.a;
import com.b.a.a.b;
import com.b.a.a.c;
import com.b.a.b.a.u;
import com.b.a.b.j;
import com.b.a.d;
import com.b.a.e;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.HttpState;

public final class g {
    private static Map<String, Class<?>> a;

    static {
        HashMap hashMap = new HashMap();
        a = hashMap;
        hashMap.put("byte", Byte.TYPE);
        a.put("short", Short.TYPE);
        a.put("int", Integer.TYPE);
        a.put("long", Long.TYPE);
        a.put("float", Float.TYPE);
        a.put("double", Double.TYPE);
        a.put("boolean", Boolean.TYPE);
        a.put("char", Character.TYPE);
        a.put("[byte", byte[].class);
        a.put("[short", short[].class);
        a.put("[int", int[].class);
        a.put("[long", long[].class);
        a.put("[float", float[].class);
        a.put("[double", double[].class);
        a.put("[boolean", boolean[].class);
        a.put("[char", char[].class);
    }

    public static Class<?> a(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        Class<?> cls = a.get(str);
        if (cls != null) {
            return cls;
        }
        if (str.charAt(0) == '[') {
            return Array.newInstance(a(str.substring(1)), 0).getClass();
        }
        try {
            return Thread.currentThread().getContextClassLoader().loadClass(str);
        } catch (Throwable th) {
            return cls;
        }
    }

    public static Class<?> a(Type type) {
        Type type2 = type;
        while (type2.getClass() != Class.class) {
            if (!(type2 instanceof ParameterizedType)) {
                return Object.class;
            }
            type2 = ((ParameterizedType) type2).getRawType();
        }
        return (Class) type2;
    }

    private static <T> T a(Object obj, Class<T> cls) {
        try {
            if (obj instanceof String) {
                String str = (String) obj;
                if (str.length() == 0) {
                    return null;
                }
                return Enum.valueOf(cls, str);
            }
            if (obj instanceof Number) {
                int intValue = ((Number) obj).intValue();
                for (T t : (Object[]) cls.getMethod("values", new Class[0]).invoke(null, new Object[0])) {
                    T t2 = (Enum) t;
                    if (t2.ordinal() == intValue) {
                        return t2;
                    }
                }
            }
            throw new d("can not cast to : " + cls.getName());
        } catch (Exception e) {
            throw new d("can not cast to : " + cls.getName(), e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.d.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.Class, com.b.a.b.j):T
     arg types: [java.util.Map, java.lang.Class<T>, com.b.a.b.j]
     candidates:
      com.b.a.d.g.a(java.lang.Object, java.lang.Class, com.b.a.b.j):T
      com.b.a.d.g.a(java.lang.Object, java.lang.reflect.Type, com.b.a.b.j):T
      com.b.a.d.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.Class, com.b.a.b.j):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.d.g.a(java.lang.Object, java.lang.Class, com.b.a.b.j):T
     arg types: [java.lang.Object, java.lang.Class<?>, com.b.a.b.j]
     candidates:
      com.b.a.d.g.a(java.lang.Object, java.lang.reflect.Type, com.b.a.b.j):T
      com.b.a.d.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.Class, com.b.a.b.j):T
      com.b.a.d.g.a(java.lang.Object, java.lang.Class, com.b.a.b.j):T */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T> T a(java.lang.Object r6, java.lang.Class<T> r7, com.b.a.b.j r8) {
        /*
            r2 = 0
            r4 = 0
            if (r6 != 0) goto L_0x0007
            r6 = r4
        L_0x0006:
            return r6
        L_0x0007:
            java.lang.Class r0 = r6.getClass()
            if (r7 == r0) goto L_0x0006
            boolean r0 = r6 instanceof java.util.Map
            if (r0 == 0) goto L_0x001c
            java.lang.Class<java.util.Map> r0 = java.util.Map.class
            if (r7 == r0) goto L_0x0006
            java.util.Map r6 = (java.util.Map) r6
            java.lang.Object r6 = a(r6, r7, r8)
            goto L_0x0006
        L_0x001c:
            boolean r0 = r7.isArray()
            if (r0 == 0) goto L_0x0053
            boolean r0 = r6 instanceof java.util.Collection
            if (r0 == 0) goto L_0x0053
            java.util.Collection r6 = (java.util.Collection) r6
            r0 = 0
            java.lang.Class r1 = r7.getComponentType()
            int r2 = r6.size()
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r1, r2)
            java.util.Iterator r2 = r6.iterator()
        L_0x0039:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0051
            java.lang.Object r3 = r2.next()
            java.lang.Class r4 = r7.getComponentType()
            java.lang.Object r3 = a(r3, r4, r8)
            java.lang.reflect.Array.set(r1, r0, r3)
            int r0 = r0 + 1
            goto L_0x0039
        L_0x0051:
            r6 = r1
            goto L_0x0006
        L_0x0053:
            java.lang.Class r0 = r6.getClass()
            boolean r0 = r7.isAssignableFrom(r0)
            if (r0 != 0) goto L_0x0006
            java.lang.Class r0 = java.lang.Boolean.TYPE
            if (r7 == r0) goto L_0x0065
            java.lang.Class<java.lang.Boolean> r0 = java.lang.Boolean.class
            if (r7 != r0) goto L_0x006a
        L_0x0065:
            java.lang.Boolean r6 = k(r6)
            goto L_0x0006
        L_0x006a:
            java.lang.Class r0 = java.lang.Byte.TYPE
            if (r7 == r0) goto L_0x0072
            java.lang.Class<java.lang.Byte> r0 = java.lang.Byte.class
            if (r7 != r0) goto L_0x0077
        L_0x0072:
            java.lang.Byte r6 = b(r6)
            goto L_0x0006
        L_0x0077:
            java.lang.Class r0 = java.lang.Short.TYPE
            if (r7 == r0) goto L_0x007f
            java.lang.Class<java.lang.Short> r0 = java.lang.Short.class
            if (r7 != r0) goto L_0x0084
        L_0x007f:
            java.lang.Short r6 = d(r6)
            goto L_0x0006
        L_0x0084:
            java.lang.Class r0 = java.lang.Integer.TYPE
            if (r7 == r0) goto L_0x008c
            java.lang.Class<java.lang.Integer> r0 = java.lang.Integer.class
            if (r7 != r0) goto L_0x0092
        L_0x008c:
            java.lang.Integer r6 = j(r6)
            goto L_0x0006
        L_0x0092:
            java.lang.Class r0 = java.lang.Long.TYPE
            if (r7 == r0) goto L_0x009a
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            if (r7 != r0) goto L_0x00a0
        L_0x009a:
            java.lang.Long r6 = i(r6)
            goto L_0x0006
        L_0x00a0:
            java.lang.Class r0 = java.lang.Float.TYPE
            if (r7 == r0) goto L_0x00a8
            java.lang.Class<java.lang.Float> r0 = java.lang.Float.class
            if (r7 != r0) goto L_0x00ae
        L_0x00a8:
            java.lang.Float r6 = g(r6)
            goto L_0x0006
        L_0x00ae:
            java.lang.Class r0 = java.lang.Double.TYPE
            if (r7 == r0) goto L_0x00b6
            java.lang.Class<java.lang.Double> r0 = java.lang.Double.class
            if (r7 != r0) goto L_0x00bc
        L_0x00b6:
            java.lang.Double r6 = h(r6)
            goto L_0x0006
        L_0x00bc:
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r7 != r0) goto L_0x00c6
            java.lang.String r6 = a(r6)
            goto L_0x0006
        L_0x00c6:
            java.lang.Class<java.math.BigDecimal> r0 = java.math.BigDecimal.class
            if (r7 != r0) goto L_0x00d0
            java.math.BigDecimal r6 = e(r6)
            goto L_0x0006
        L_0x00d0:
            java.lang.Class<java.math.BigInteger> r0 = java.math.BigInteger.class
            if (r7 != r0) goto L_0x00da
            java.math.BigInteger r6 = f(r6)
            goto L_0x0006
        L_0x00da:
            java.lang.Class<java.util.Date> r0 = java.util.Date.class
            if (r7 != r0) goto L_0x00e4
            java.util.Date r6 = l(r6)
            goto L_0x0006
        L_0x00e4:
            java.lang.Class<java.sql.Date> r0 = java.sql.Date.class
            if (r7 != r0) goto L_0x0158
            if (r6 != 0) goto L_0x00ed
            r6 = r4
            goto L_0x0006
        L_0x00ed:
            boolean r0 = r6 instanceof java.util.Calendar
            if (r0 == 0) goto L_0x00ff
            java.sql.Date r0 = new java.sql.Date
            java.util.Calendar r6 = (java.util.Calendar) r6
            long r1 = r6.getTimeInMillis()
            r0.<init>(r1)
            r6 = r0
            goto L_0x0006
        L_0x00ff:
            boolean r0 = r6 instanceof java.sql.Date
            if (r0 == 0) goto L_0x0107
            java.sql.Date r6 = (java.sql.Date) r6
            goto L_0x0006
        L_0x0107:
            boolean r0 = r6 instanceof java.util.Date
            if (r0 == 0) goto L_0x0119
            java.sql.Date r0 = new java.sql.Date
            java.util.Date r6 = (java.util.Date) r6
            long r1 = r6.getTime()
            r0.<init>(r1)
            r6 = r0
            goto L_0x0006
        L_0x0119:
            boolean r0 = r6 instanceof java.lang.Number
            if (r0 == 0) goto L_0x023e
            r0 = r6
            java.lang.Number r0 = (java.lang.Number) r0
            long r0 = r0.longValue()
        L_0x0124:
            boolean r5 = r6 instanceof java.lang.String
            if (r5 == 0) goto L_0x0138
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            int r1 = r0.length()
            if (r1 != 0) goto L_0x0134
            r6 = r4
            goto L_0x0006
        L_0x0134:
            long r0 = java.lang.Long.parseLong(r0)
        L_0x0138:
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x0151
            com.b.a.d r0 = new com.b.a.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "can not cast to Date, value : "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0151:
            java.sql.Date r6 = new java.sql.Date
            r6.<init>(r0)
            goto L_0x0006
        L_0x0158:
            java.lang.Class<java.sql.Timestamp> r0 = java.sql.Timestamp.class
            if (r7 != r0) goto L_0x01cc
            if (r6 != 0) goto L_0x0161
            r6 = r4
            goto L_0x0006
        L_0x0161:
            boolean r0 = r6 instanceof java.util.Calendar
            if (r0 == 0) goto L_0x0173
            java.sql.Timestamp r0 = new java.sql.Timestamp
            java.util.Calendar r6 = (java.util.Calendar) r6
            long r1 = r6.getTimeInMillis()
            r0.<init>(r1)
            r6 = r0
            goto L_0x0006
        L_0x0173:
            boolean r0 = r6 instanceof java.sql.Timestamp
            if (r0 == 0) goto L_0x017b
            java.sql.Timestamp r6 = (java.sql.Timestamp) r6
            goto L_0x0006
        L_0x017b:
            boolean r0 = r6 instanceof java.util.Date
            if (r0 == 0) goto L_0x018d
            java.sql.Timestamp r0 = new java.sql.Timestamp
            java.util.Date r6 = (java.util.Date) r6
            long r1 = r6.getTime()
            r0.<init>(r1)
            r6 = r0
            goto L_0x0006
        L_0x018d:
            boolean r0 = r6 instanceof java.lang.Number
            if (r0 == 0) goto L_0x023b
            r0 = r6
            java.lang.Number r0 = (java.lang.Number) r0
            long r0 = r0.longValue()
        L_0x0198:
            boolean r5 = r6 instanceof java.lang.String
            if (r5 == 0) goto L_0x01ac
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            int r1 = r0.length()
            if (r1 != 0) goto L_0x01a8
            r6 = r4
            goto L_0x0006
        L_0x01a8:
            long r0 = java.lang.Long.parseLong(r0)
        L_0x01ac:
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x01c5
            com.b.a.d r0 = new com.b.a.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "can not cast to Date, value : "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x01c5:
            java.sql.Timestamp r6 = new java.sql.Timestamp
            r6.<init>(r0)
            goto L_0x0006
        L_0x01cc:
            boolean r0 = r7.isEnum()
            if (r0 == 0) goto L_0x01d8
            java.lang.Object r6 = a(r6, r7)
            goto L_0x0006
        L_0x01d8:
            java.lang.Class<java.util.Calendar> r0 = java.util.Calendar.class
            boolean r0 = r0.isAssignableFrom(r7)
            if (r0 == 0) goto L_0x0213
            java.util.Date r1 = l(r6)
            java.lang.Class<java.util.Calendar> r0 = java.util.Calendar.class
            if (r7 != r0) goto L_0x01f2
            java.util.Calendar r0 = java.util.Calendar.getInstance()
        L_0x01ec:
            r0.setTime(r1)
            r6 = r0
            goto L_0x0006
        L_0x01f2:
            java.lang.Object r0 = r7.newInstance()     // Catch:{ Exception -> 0x01f9 }
            java.util.Calendar r0 = (java.util.Calendar) r0     // Catch:{ Exception -> 0x01f9 }
            goto L_0x01ec
        L_0x01f9:
            r0 = move-exception
            com.b.a.d r1 = new com.b.a.d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "can not cast to : "
            r2.<init>(r3)
            java.lang.String r3 = r7.getName()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x0213:
            boolean r0 = r6 instanceof java.lang.String
            if (r0 == 0) goto L_0x0222
            java.lang.String r6 = (java.lang.String) r6
            int r0 = r6.length()
            if (r0 != 0) goto L_0x0222
            r6 = r4
            goto L_0x0006
        L_0x0222:
            com.b.a.d r0 = new com.b.a.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "can not cast to : "
            r1.<init>(r2)
            java.lang.String r2 = r7.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x023b:
            r0 = r2
            goto L_0x0198
        L_0x023e:
            r0 = r2
            goto L_0x0124
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.d.g.a(java.lang.Object, java.lang.Class, com.b.a.b.j):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T> T a(java.lang.Object r8, java.lang.reflect.Type r9, com.b.a.b.j r10) {
        /*
            r7 = 1
            r3 = 0
            r6 = 0
            r0 = r9
            r1 = r8
        L_0x0005:
            if (r1 != 0) goto L_0x0009
            r1 = r3
        L_0x0008:
            return r1
        L_0x0009:
            boolean r2 = r0 instanceof java.lang.Class
            if (r2 == 0) goto L_0x0014
            java.lang.Class r0 = (java.lang.Class) r0
            java.lang.Object r1 = a(r1, r0, r10)
            goto L_0x0008
        L_0x0014:
            boolean r2 = r0 instanceof java.lang.reflect.ParameterizedType
            if (r2 == 0) goto L_0x00d3
            java.lang.reflect.ParameterizedType r0 = (java.lang.reflect.ParameterizedType) r0
            java.lang.reflect.Type r9 = r0.getRawType()
            java.lang.Class<java.util.List> r2 = java.util.List.class
            if (r9 == r2) goto L_0x0026
            java.lang.Class<java.util.ArrayList> r2 = java.util.ArrayList.class
            if (r9 != r2) goto L_0x0050
        L_0x0026:
            java.lang.reflect.Type[] r2 = r0.getActualTypeArguments()
            r4 = r2[r6]
            boolean r2 = r1 instanceof java.lang.Iterable
            if (r2 == 0) goto L_0x0050
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r0 = r1
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r0 = r0.iterator()
        L_0x003c:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x004e
            java.lang.Object r1 = r0.next()
            java.lang.Object r1 = a(r1, r4, r10)
            r2.add(r1)
            goto L_0x003c
        L_0x004e:
            r1 = r2
            goto L_0x0008
        L_0x0050:
            java.lang.Class<java.util.Map> r2 = java.util.Map.class
            if (r9 == r2) goto L_0x0058
            java.lang.Class<java.util.HashMap> r2 = java.util.HashMap.class
            if (r9 != r2) goto L_0x009a
        L_0x0058:
            java.lang.reflect.Type[] r2 = r0.getActualTypeArguments()
            r4 = r2[r6]
            java.lang.reflect.Type[] r2 = r0.getActualTypeArguments()
            r5 = r2[r7]
            boolean r2 = r1 instanceof java.util.Map
            if (r2 == 0) goto L_0x009a
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.util.Map r1 = (java.util.Map) r1
            java.util.Set r0 = r1.entrySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x0077:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0097
            java.lang.Object r0 = r1.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r3 = r0.getKey()
            java.lang.Object r3 = a(r3, r4, r10)
            java.lang.Object r0 = r0.getValue()
            java.lang.Object r0 = a(r0, r5, r10)
            r2.put(r3, r0)
            goto L_0x0077
        L_0x0097:
            r1 = r2
            goto L_0x0008
        L_0x009a:
            boolean r2 = r1 instanceof java.lang.String
            if (r2 == 0) goto L_0x00aa
            r2 = r1
            java.lang.String r2 = (java.lang.String) r2
            int r2 = r2.length()
            if (r2 != 0) goto L_0x00aa
            r1 = r3
            goto L_0x0008
        L_0x00aa:
            java.lang.reflect.Type[] r2 = r0.getActualTypeArguments()
            int r2 = r2.length
            if (r2 != r7) goto L_0x00be
            java.lang.reflect.Type[] r2 = r0.getActualTypeArguments()
            r2 = r2[r6]
            boolean r2 = r2 instanceof java.lang.reflect.WildcardType
            if (r2 == 0) goto L_0x00be
            r0 = r9
            goto L_0x0005
        L_0x00be:
            com.b.a.d r1 = new com.b.a.d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "can not cast to : "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x00d3:
            boolean r2 = r1 instanceof java.lang.String
            if (r2 == 0) goto L_0x00e3
            r2 = r1
            java.lang.String r2 = (java.lang.String) r2
            int r2 = r2.length()
            if (r2 != 0) goto L_0x00e3
            r1 = r3
            goto L_0x0008
        L_0x00e3:
            boolean r2 = r0 instanceof java.lang.reflect.TypeVariable
            if (r2 != 0) goto L_0x0008
            com.b.a.d r1 = new com.b.a.d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "can not cast to : "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.d.g.a(java.lang.Object, java.lang.reflect.Type, com.b.a.b.j):java.lang.Object");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private static <T> T a(Map<String, Object> map, Class<T> cls, j jVar) {
        int i = 0;
        if (cls == StackTraceElement.class) {
            try {
                String str = (String) map.get("className");
                String str2 = (String) map.get("methodName");
                String str3 = (String) map.get("fileName");
                Number number = (Number) map.get("lineNumber");
                if (number != null) {
                    i = number.intValue();
                }
                return new StackTraceElement(str, str2, str3, i);
            } catch (Exception e) {
                throw new d(e.getMessage(), e);
            }
        } else {
            Object obj = map.get("@type");
            Class cls2 = cls;
            if (obj instanceof String) {
                cls2 = a((String) obj);
            }
            if (cls2.isInterface()) {
                return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{cls2}, map instanceof e ? (e) map : new e(map));
            }
            Map<String, u> b = jVar.b(cls2);
            T newInstance = cls2.newInstance();
            for (Map.Entry next : b.entrySet()) {
                String str4 = (String) next.getKey();
                Method b2 = ((u) next.getValue()).b();
                if (map.containsKey(str4)) {
                    b2.invoke(newInstance, a(map.get(str4), b2.getGenericParameterTypes()[0], jVar));
                }
            }
            return newInstance;
        }
    }

    public static final String a(Object obj) {
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.d.g.a(java.lang.Class<?>, boolean):java.util.List<com.b.a.d.c>
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.b.a.d.g.a(java.lang.Object, java.lang.Class):T
      com.b.a.d.g.a(java.lang.Class<?>, java.lang.String):boolean
      com.b.a.d.g.a(java.lang.Class<?>, boolean):java.util.List<com.b.a.d.c> */
    public static List<c> a(Class<?> cls) {
        return a(cls, true);
    }

    public static List<c> a(Class<?> cls, boolean z) {
        boolean z2;
        String[] strArr;
        b bVar;
        b bVar2;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Method method : cls.getMethods()) {
            String name = method.getName();
            if (!Modifier.isStatic(method.getModifiers()) && !method.getReturnType().equals(Void.TYPE) && method.getParameterTypes().length == 0) {
                b bVar3 = (b) method.getAnnotation(b.class);
                if (bVar3 != null) {
                    if (bVar3.c()) {
                        if (bVar3.a().length() != 0) {
                            String a2 = bVar3.a();
                            linkedHashMap.put(a2, new c(a2, method, null));
                        }
                    }
                }
                if (name.startsWith("get")) {
                    if (name.length() >= 4 && !name.equals("getClass") && Character.isUpperCase(name.charAt(3))) {
                        String str = Character.toLowerCase(name.charAt(3)) + name.substring(4);
                        if (!a(cls, str)) {
                            Field a3 = j.a(cls, str);
                            String a4 = (a3 == null || (bVar2 = (b) a3.getAnnotation(b.class)) == null || bVar2.a().length() == 0) ? str : bVar2.a();
                            linkedHashMap.put(a4, new c(a4, method, a3));
                        }
                    }
                }
                if (name.startsWith("is") && name.length() >= 3 && Character.isUpperCase(name.charAt(2))) {
                    String str2 = Character.toLowerCase(name.charAt(2)) + name.substring(3);
                    Field a5 = j.a(cls, str2);
                    String a6 = (a5 == null || (bVar = (b) a5.getAnnotation(b.class)) == null || bVar.a().length() == 0) ? str2 : bVar.a();
                    linkedHashMap.put(a6, new c(a6, method, a5));
                }
            }
        }
        for (Field field : cls.getFields()) {
            if (!Modifier.isStatic(field.getModifiers()) && Modifier.isPublic(field.getModifiers()) && !linkedHashMap.containsKey(field.getName())) {
                linkedHashMap.put(field.getName(), new c(field.getName(), null, field));
            }
        }
        ArrayList arrayList = new ArrayList();
        c cVar = (c) cls.getAnnotation(c.class);
        if (cVar != null) {
            String[] a7 = cVar.a();
            if (a7 != null && a7.length == linkedHashMap.size()) {
                int length = a7.length;
                int i = 0;
                while (true) {
                    if (i < length) {
                        if (!linkedHashMap.containsKey(a7[i])) {
                            break;
                        }
                        i++;
                    } else {
                        strArr = a7;
                        z2 = true;
                        break;
                    }
                }
            }
            strArr = a7;
            z2 = false;
        } else {
            z2 = false;
            strArr = null;
        }
        if (z2) {
            for (String str3 : strArr) {
                arrayList.add((c) linkedHashMap.get(str3));
            }
        } else {
            for (c add : linkedHashMap.values()) {
                arrayList.add(add);
            }
            if (z) {
                Collections.sort(arrayList);
            }
        }
        return arrayList;
    }

    private static boolean a(Class<?> cls, String str) {
        c cVar = (c) cls.getAnnotation(c.class);
        if (!(cVar == null || cVar.b() == null)) {
            for (String equalsIgnoreCase : cVar.b()) {
                if (str.equalsIgnoreCase(equalsIgnoreCase)) {
                    return true;
                }
            }
        }
        return cls.getSuperclass() != Object.class && a(cls.getSuperclass(), str);
    }

    public static final Byte b(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Number) {
            return Byte.valueOf(((Number) obj).byteValue());
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() != 0) {
                return Byte.valueOf(Byte.parseByte(str));
            }
            return null;
        }
        throw new d("can not cast to byte, value : " + obj);
    }

    public static final Character c(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Character) {
            return (Character) obj;
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() == 0) {
                return null;
            }
            if (str.length() == 1) {
                return Character.valueOf(str.charAt(0));
            }
            throw new d("can not cast to byte, value : " + obj);
        }
        throw new d("can not cast to byte, value : " + obj);
    }

    public static final Short d(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Number) {
            return Short.valueOf(((Number) obj).shortValue());
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() != 0) {
                return Short.valueOf(Short.parseShort(str));
            }
            return null;
        }
        throw new d("can not cast to short, value : " + obj);
    }

    public static final BigDecimal e(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof BigDecimal) {
            return (BigDecimal) obj;
        }
        if (obj instanceof BigInteger) {
            return new BigDecimal((BigInteger) obj);
        }
        String obj2 = obj.toString();
        if (obj2.length() == 0) {
            return null;
        }
        return new BigDecimal(obj2);
    }

    public static final BigInteger f(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof BigInteger) {
            return (BigInteger) obj;
        }
        if ((obj instanceof Float) || (obj instanceof Double)) {
            return BigInteger.valueOf(((Number) obj).longValue());
        }
        String obj2 = obj.toString();
        if (obj2.length() == 0) {
            return null;
        }
        return new BigInteger(obj2);
    }

    public static final Float g(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Number) {
            return Float.valueOf(((Number) obj).floatValue());
        }
        if (obj instanceof String) {
            String obj2 = obj.toString();
            if (obj2.length() != 0) {
                return Float.valueOf(Float.parseFloat(obj2));
            }
            return null;
        }
        throw new d("can not cast to float, value : " + obj);
    }

    public static final Double h(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Number) {
            return Double.valueOf(((Number) obj).doubleValue());
        }
        if (obj instanceof String) {
            String obj2 = obj.toString();
            if (obj2.length() != 0) {
                return Double.valueOf(Double.parseDouble(obj2));
            }
            return null;
        }
        throw new d("can not cast to double, value : " + obj);
    }

    public static final Long i(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Number) {
            return Long.valueOf(((Number) obj).longValue());
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() != 0) {
                return Long.valueOf(Long.parseLong(str));
            }
            return null;
        }
        throw new d("can not cast to long, value : " + obj);
    }

    public static final Integer j(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        if (obj instanceof Number) {
            return Integer.valueOf(((Number) obj).intValue());
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() == 0) {
                return null;
            }
            return Integer.valueOf(Integer.parseInt(str));
        }
        throw new d("can not cast to int, value : " + obj);
    }

    public static final Boolean k(Object obj) {
        boolean z = true;
        if (obj == null) {
            return null;
        }
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }
        if (obj instanceof Number) {
            if (((Number) obj).intValue() != 1) {
                z = false;
            }
            return Boolean.valueOf(z);
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() == 0) {
                return null;
            }
            if ("true".equals(str)) {
                return Boolean.TRUE;
            }
            if (HttpState.PREEMPTIVE_DEFAULT.equals(str)) {
                return Boolean.FALSE;
            }
            if ("1".equals(str)) {
                return Boolean.TRUE;
            }
        }
        throw new d("can not cast to int, value : " + obj);
    }

    private static Date l(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Calendar) {
            return ((Calendar) obj).getTime();
        }
        if (obj instanceof Date) {
            return (Date) obj;
        }
        long longValue = obj instanceof Number ? ((Number) obj).longValue() : 0;
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.indexOf(45) != -1) {
                try {
                    return new SimpleDateFormat(str.length() == a.b.length() ? a.b : str.length() == 10 ? "yyyy-MM-dd" : str.length() == 19 ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd HH:mm:ss.SSS").parse(str);
                } catch (ParseException e) {
                    throw new d("can not cast to Date, value : " + str);
                }
            } else if (str.length() == 0) {
                return null;
            } else {
                longValue = Long.parseLong(str);
            }
        }
        if (longValue > 0) {
            return new Date(longValue);
        }
        throw new d("can not cast to Date, value : " + obj);
    }
}
