package com.b.a.d;

public final class f<K, V> {
    public final int a;
    public final K b;
    public V c;
    public final f<K, V> d;

    public f(K k, V v, int i, f<K, V> fVar) {
        this.b = k;
        this.c = v;
        this.d = fVar;
        this.a = i;
    }
}
