package com.b.a.b.a;

import com.b.a.b.f;
import com.b.a.b.j;
import com.b.a.d.c;
import java.lang.reflect.Type;
import java.util.Map;

public final class ar extends u {
    private final am c;

    public ar(j jVar, Class<?> cls, c cVar) {
        super(cls, cVar);
        this.c = jVar.a(cVar);
    }

    public final int a() {
        return this.c.a();
    }

    public final void a(com.b.a.b.c cVar, Object obj, Type type, Map<String, Object> map) {
        String obj2;
        f k = cVar.k();
        if (k.e() == 4) {
            obj2 = k.q();
            k.b(16);
        } else {
            Object j = cVar.j();
            obj2 = j == null ? null : j.toString();
        }
        if (obj == null) {
            map.put(this.a.c(), obj2);
        } else {
            a(obj, obj2);
        }
    }
}
