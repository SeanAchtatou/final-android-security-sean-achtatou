package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.e;
import com.b.a.b.f;
import com.b.a.d;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class s implements am {
    public static final s a = new s();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.a.s.a(com.b.a.b.c, java.lang.Object):void
     arg types: [com.b.a.b.c, T]
     candidates:
      com.b.a.b.a.s.a(com.b.a.b.c, java.lang.Class):T
      com.b.a.b.a.s.a(com.b.a.b.c, java.lang.Object):void */
    private static <T> T a(c cVar, Class<T> cls) {
        if (cVar.k().e() == 8) {
            cVar.k().b(16);
            return null;
        }
        T hashMap = cls.isAssignableFrom(HashMap.class) ? new HashMap() : cls.isAssignableFrom(TreeMap.class) ? new TreeMap() : cls.isAssignableFrom(ConcurrentHashMap.class) ? new ConcurrentHashMap() : cls.isAssignableFrom(Properties.class) ? new Properties() : cls.isAssignableFrom(IdentityHashMap.class) ? new IdentityHashMap() : null;
        if (cls == Class.class) {
            Object j = cVar.j();
            if (j == null) {
                return null;
            }
            if (j instanceof String) {
                try {
                    return Thread.currentThread().getContextClassLoader().loadClass((String) j);
                } catch (ClassNotFoundException e) {
                    throw new d(e.getMessage(), e);
                }
            }
        } else if (cls == Serializable.class) {
            return cVar.j();
        }
        if (hashMap == null) {
            throw new d("not support type : " + cls);
        }
        try {
            a(cVar, (Object) hashMap);
            return hashMap;
        } catch (d e2) {
            throw e2;
        } catch (Throwable th) {
            throw new d(th.getMessage(), th);
        }
    }

    private static <T> T a(c cVar, ParameterizedType parameterizedType, Object obj) {
        Map hashMap;
        try {
            f k = cVar.k();
            if (k.e() == 8) {
                k.l();
                return null;
            }
            Type rawType = parameterizedType.getRawType();
            if (rawType instanceof Class) {
                Class<ConcurrentMap> cls = (Class) rawType;
                if (Map.class.isAssignableFrom(cls)) {
                    if (!Modifier.isAbstract(cls.getModifiers())) {
                        hashMap = cls == HashMap.class ? new HashMap() : cls.newInstance();
                    } else if (cls == Map.class) {
                        hashMap = new HashMap();
                    } else if (cls == SortedMap.class) {
                        hashMap = new TreeMap();
                    } else if (cls == ConcurrentMap.class) {
                        hashMap = new ConcurrentHashMap();
                    } else {
                        throw new d("can not create instance : " + cls);
                    }
                    Type type = parameterizedType.getActualTypeArguments()[0];
                    Type type2 = parameterizedType.getActualTypeArguments()[1];
                    return type == String.class ? a(cVar, hashMap, type2, obj) : a(cVar, hashMap, type, type2, obj);
                }
            }
            throw new d("not support type : " + parameterizedType);
        } catch (d e) {
            throw e;
        } catch (Throwable th) {
            throw new d(th.getMessage(), th);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0067, code lost:
        r3.a(4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006f, code lost:
        if (r3.e() != 4) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0071, code lost:
        r4 = r3.q();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007b, code lost:
        if ("@".equals(r4) == false) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007d, code lost:
        r12 = r2.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0081, code lost:
        r3.b(13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008a, code lost:
        if (r3.e() == 13) goto L_0x0116;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0093, code lost:
        throw new com.b.a.d("illegal ref");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009f, code lost:
        if ("..".equals(r4) == false) goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a1, code lost:
        r1 = r2.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a9, code lost:
        if (r1.a() == null) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ab, code lost:
        r12 = r1.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b0, code lost:
        r11.a(new com.b.a.b.d(r1, r4));
        r11.a(1);
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c4, code lost:
        if ("$".equals(r4) == false) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00c6, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00cb, code lost:
        if (r1.b() == null) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00cd, code lost:
        r1 = r1.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d6, code lost:
        if (r1.a() == null) goto L_0x00dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00d8, code lost:
        r12 = r1.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00dd, code lost:
        r11.a(new com.b.a.b.d(r1, r4));
        r11.a(1);
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00eb, code lost:
        r11.a(new com.b.a.b.d(r2, r4));
        r11.a(1);
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0115, code lost:
        throw new com.b.a.d("illegal ref, " + com.b.a.b.g.a(r3.e()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0116, code lost:
        r3.b(16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x011b, code lost:
        r11.a(r2);
        r12 = r12;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object a(com.b.a.b.c r11, java.util.Map<java.lang.Object, java.lang.Object> r12, java.lang.reflect.Type r13, java.lang.reflect.Type r14, java.lang.Object r15) {
        /*
            r0 = 0
            r10 = 13
            r9 = 16
            r8 = 4
            com.b.a.b.f r3 = r11.k()
            int r1 = r3.e()
            r2 = 12
            if (r1 == r2) goto L_0x0031
            int r1 = r3.e()
            if (r1 == r9) goto L_0x0031
            com.b.a.d r0 = new com.b.a.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "syntax error, expect {, actual "
            r1.<init>(r2)
            java.lang.String r2 = r3.f()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0031:
            com.b.a.b.j r1 = r11.c()
            com.b.a.b.a.am r1 = r1.a(r13)
            com.b.a.b.j r2 = r11.c()
            com.b.a.b.a.am r4 = r2.a(r14)
            int r2 = r1.a()
            r3.b(r2)
            com.b.a.b.i r2 = r11.f()
        L_0x004c:
            int r5 = r3.e()     // Catch:{ all -> 0x0094 }
            if (r5 != r10) goto L_0x005b
            r0 = 16
            r3.b(r0)     // Catch:{ all -> 0x0094 }
            r11.a(r2)
        L_0x005a:
            return r12
        L_0x005b:
            int r5 = r3.e()     // Catch:{ all -> 0x0094 }
            if (r5 != r8) goto L_0x0120
            boolean r5 = r3.r()     // Catch:{ all -> 0x0094 }
            if (r5 == 0) goto L_0x0120
            r1 = 4
            r3.a(r1)     // Catch:{ all -> 0x0094 }
            int r1 = r3.e()     // Catch:{ all -> 0x0094 }
            if (r1 != r8) goto L_0x00f9
            java.lang.String r4 = r3.q()     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = "@"
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x0094 }
            if (r1 == 0) goto L_0x0099
            java.lang.Object r12 = r2.a()     // Catch:{ all -> 0x0094 }
        L_0x0081:
            r0 = 13
            r3.b(r0)     // Catch:{ all -> 0x0094 }
            int r0 = r3.e()     // Catch:{ all -> 0x0094 }
            if (r0 == r10) goto L_0x0116
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = "illegal ref"
            r0.<init>(r1)     // Catch:{ all -> 0x0094 }
            throw r0     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r0 = move-exception
            r11.a(r2)
            throw r0
        L_0x0099:
            java.lang.String r1 = ".."
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x0094 }
            if (r1 == 0) goto L_0x00be
            com.b.a.b.i r1 = r2.b()     // Catch:{ all -> 0x0094 }
            java.lang.Object r5 = r1.a()     // Catch:{ all -> 0x0094 }
            if (r5 == 0) goto L_0x00b0
            java.lang.Object r12 = r1.a()     // Catch:{ all -> 0x0094 }
            goto L_0x0081
        L_0x00b0:
            com.b.a.b.d r5 = new com.b.a.b.d     // Catch:{ all -> 0x0094 }
            r5.<init>(r1, r4)     // Catch:{ all -> 0x0094 }
            r11.a(r5)     // Catch:{ all -> 0x0094 }
            r1 = 1
            r11.a(r1)     // Catch:{ all -> 0x0094 }
            r12 = r0
            goto L_0x0081
        L_0x00be:
            java.lang.String r1 = "$"
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x0094 }
            if (r1 == 0) goto L_0x00eb
            r1 = r2
        L_0x00c7:
            com.b.a.b.i r5 = r1.b()     // Catch:{ all -> 0x0094 }
            if (r5 == 0) goto L_0x00d2
            com.b.a.b.i r1 = r1.b()     // Catch:{ all -> 0x0094 }
            goto L_0x00c7
        L_0x00d2:
            java.lang.Object r5 = r1.a()     // Catch:{ all -> 0x0094 }
            if (r5 == 0) goto L_0x00dd
            java.lang.Object r12 = r1.a()     // Catch:{ all -> 0x0094 }
            goto L_0x0081
        L_0x00dd:
            com.b.a.b.d r5 = new com.b.a.b.d     // Catch:{ all -> 0x0094 }
            r5.<init>(r1, r4)     // Catch:{ all -> 0x0094 }
            r11.a(r5)     // Catch:{ all -> 0x0094 }
            r1 = 1
            r11.a(r1)     // Catch:{ all -> 0x0094 }
            r12 = r0
            goto L_0x0081
        L_0x00eb:
            com.b.a.b.d r1 = new com.b.a.b.d     // Catch:{ all -> 0x0094 }
            r1.<init>(r2, r4)     // Catch:{ all -> 0x0094 }
            r11.a(r1)     // Catch:{ all -> 0x0094 }
            r1 = 1
            r11.a(r1)     // Catch:{ all -> 0x0094 }
            r12 = r0
            goto L_0x0081
        L_0x00f9:
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            java.lang.String r4 = "illegal ref, "
            r1.<init>(r4)     // Catch:{ all -> 0x0094 }
            int r3 = r3.e()     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = com.b.a.b.g.a(r3)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0094 }
            r0.<init>(r1)     // Catch:{ all -> 0x0094 }
            throw r0     // Catch:{ all -> 0x0094 }
        L_0x0116:
            r0 = 16
            r3.b(r0)     // Catch:{ all -> 0x0094 }
            r11.a(r2)
            goto L_0x005a
        L_0x0120:
            int r5 = r12.size()     // Catch:{ all -> 0x0094 }
            if (r5 != 0) goto L_0x0148
            int r5 = r3.e()     // Catch:{ all -> 0x0094 }
            if (r5 != r8) goto L_0x0148
            java.lang.String r5 = "@type"
            java.lang.String r6 = r3.q()     // Catch:{ all -> 0x0094 }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x0094 }
            if (r5 == 0) goto L_0x0148
            r5 = 4
            r3.a(r5)     // Catch:{ all -> 0x0094 }
            r5 = 16
            r3.b(r5)     // Catch:{ all -> 0x0094 }
            int r5 = r1.a()     // Catch:{ all -> 0x0094 }
            r3.b(r5)     // Catch:{ all -> 0x0094 }
        L_0x0148:
            r5 = 0
            java.lang.Object r5 = r1.a(r11, r13, r5)     // Catch:{ all -> 0x0094 }
            int r6 = r3.e()     // Catch:{ all -> 0x0094 }
            r7 = 17
            if (r6 == r7) goto L_0x016e
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            java.lang.String r4 = "syntax error, expect :, actual "
            r1.<init>(r4)     // Catch:{ all -> 0x0094 }
            int r3 = r3.e()     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0094 }
            r0.<init>(r1)     // Catch:{ all -> 0x0094 }
            throw r0     // Catch:{ all -> 0x0094 }
        L_0x016e:
            int r6 = r4.a()     // Catch:{ all -> 0x0094 }
            r3.b(r6)     // Catch:{ all -> 0x0094 }
            java.lang.Object r6 = r4.a(r11, r14, r5)     // Catch:{ all -> 0x0094 }
            int r7 = r12.size()     // Catch:{ all -> 0x0094 }
            if (r7 != 0) goto L_0x018a
            if (r2 == 0) goto L_0x018a
            java.lang.Object r7 = r2.a()     // Catch:{ all -> 0x0094 }
            if (r7 == r12) goto L_0x018a
            r11.a(r2, r12, r15)     // Catch:{ all -> 0x0094 }
        L_0x018a:
            r12.put(r5, r6)     // Catch:{ all -> 0x0094 }
            int r5 = r3.e()     // Catch:{ all -> 0x0094 }
            if (r5 != r9) goto L_0x004c
            int r5 = r1.a()     // Catch:{ all -> 0x0094 }
            r3.b(r5)     // Catch:{ all -> 0x0094 }
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.a.s.a(com.b.a.b.c, java.util.Map, java.lang.reflect.Type, java.lang.reflect.Type, java.lang.Object):java.lang.Object");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Map, java.lang.String):void
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r1 = r9.c().a((java.lang.reflect.Type) r0);
        r2.b(16);
        r9.a(2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0162, code lost:
        if (r3 == null) goto L_0x016b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0166, code lost:
        if ((r12 instanceof java.lang.Integer) != false) goto L_0x016b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0168, code lost:
        r9.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x016b, code lost:
        r0 = (java.util.Map) r1.a(r9, r0, r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0171, code lost:
        r9.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map a(com.b.a.b.c r9, java.util.Map<java.lang.String, java.lang.Object> r10, java.lang.reflect.Type r11, java.lang.Object r12) {
        /*
            r8 = 39
            r7 = 13
            r6 = 58
            r5 = 34
            com.b.a.b.f r2 = r9.k()
            int r0 = r2.e()
            r1 = 12
            if (r0 == r1) goto L_0x002d
            com.b.a.d r0 = new com.b.a.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "syntax error, expect {, actual "
            r1.<init>(r3)
            int r2 = r2.e()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002d:
            com.b.a.b.i r3 = r9.f()
        L_0x0031:
            r2.g()     // Catch:{ all -> 0x007d }
            char r0 = r2.h()     // Catch:{ all -> 0x007d }
            com.b.a.b.e r1 = com.b.a.b.e.AllowArbitraryCommas     // Catch:{ all -> 0x007d }
            boolean r1 = r9.a(r1)     // Catch:{ all -> 0x007d }
            if (r1 == 0) goto L_0x004f
        L_0x0040:
            r1 = 44
            if (r0 != r1) goto L_0x004f
            r2.j()     // Catch:{ all -> 0x007d }
            r2.g()     // Catch:{ all -> 0x007d }
            char r0 = r2.h()     // Catch:{ all -> 0x007d }
            goto L_0x0040
        L_0x004f:
            if (r0 != r5) goto L_0x0082
            com.b.a.b.k r0 = r9.b()     // Catch:{ all -> 0x007d }
            r1 = 34
            java.lang.String r0 = r2.a(r0, r1)     // Catch:{ all -> 0x007d }
            r2.g()     // Catch:{ all -> 0x007d }
            char r1 = r2.h()     // Catch:{ all -> 0x007d }
            if (r1 == r6) goto L_0x0117
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x007d }
            java.lang.String r4 = "expect ':' at "
            r1.<init>(r4)     // Catch:{ all -> 0x007d }
            int r2 = r2.p()     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x007d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x007d }
            r0.<init>(r1)     // Catch:{ all -> 0x007d }
            throw r0     // Catch:{ all -> 0x007d }
        L_0x007d:
            r0 = move-exception
            r9.a(r3)
            throw r0
        L_0x0082:
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x0095
            r2.j()     // Catch:{ all -> 0x007d }
            r2.k()     // Catch:{ all -> 0x007d }
            r0 = 16
            r2.b(r0)     // Catch:{ all -> 0x007d }
            r9.a(r3)
        L_0x0094:
            return r10
        L_0x0095:
            if (r0 != r8) goto L_0x00d3
            com.b.a.b.e r0 = com.b.a.b.e.AllowSingleQuotes     // Catch:{ all -> 0x007d }
            boolean r0 = r9.a(r0)     // Catch:{ all -> 0x007d }
            if (r0 != 0) goto L_0x00a7
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x007d }
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)     // Catch:{ all -> 0x007d }
            throw r0     // Catch:{ all -> 0x007d }
        L_0x00a7:
            com.b.a.b.k r0 = r9.b()     // Catch:{ all -> 0x007d }
            r1 = 39
            java.lang.String r0 = r2.a(r0, r1)     // Catch:{ all -> 0x007d }
            r2.g()     // Catch:{ all -> 0x007d }
            char r1 = r2.h()     // Catch:{ all -> 0x007d }
            if (r1 == r6) goto L_0x0117
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x007d }
            java.lang.String r4 = "expect ':' at "
            r1.<init>(r4)     // Catch:{ all -> 0x007d }
            int r2 = r2.p()     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x007d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x007d }
            r0.<init>(r1)     // Catch:{ all -> 0x007d }
            throw r0     // Catch:{ all -> 0x007d }
        L_0x00d3:
            com.b.a.b.e r0 = com.b.a.b.e.AllowUnQuotedFieldNames     // Catch:{ all -> 0x007d }
            boolean r0 = r9.a(r0)     // Catch:{ all -> 0x007d }
            if (r0 != 0) goto L_0x00e3
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x007d }
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)     // Catch:{ all -> 0x007d }
            throw r0     // Catch:{ all -> 0x007d }
        L_0x00e3:
            com.b.a.b.k r0 = r9.b()     // Catch:{ all -> 0x007d }
            java.lang.String r0 = r2.a(r0)     // Catch:{ all -> 0x007d }
            r2.g()     // Catch:{ all -> 0x007d }
            char r1 = r2.h()     // Catch:{ all -> 0x007d }
            if (r1 == r6) goto L_0x0117
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x007d }
            java.lang.String r5 = "expect ':' at "
            r4.<init>(r5)     // Catch:{ all -> 0x007d }
            int r2 = r2.p()     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ all -> 0x007d }
            java.lang.String r4 = ", actual "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x007d }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x007d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x007d }
            r0.<init>(r1)     // Catch:{ all -> 0x007d }
            throw r0     // Catch:{ all -> 0x007d }
        L_0x0117:
            r1 = r0
            r2.j()     // Catch:{ all -> 0x007d }
            r2.g()     // Catch:{ all -> 0x007d }
            r2.h()     // Catch:{ all -> 0x007d }
            r2.k()     // Catch:{ all -> 0x007d }
            java.lang.String r0 = "@type"
            if (r1 != r0) goto L_0x0177
            com.b.a.b.k r0 = r9.b()     // Catch:{ all -> 0x007d }
            r1 = 34
            java.lang.String r0 = r2.a(r0, r1)     // Catch:{ all -> 0x007d }
            java.lang.Class r0 = com.b.a.d.g.a(r0)     // Catch:{ all -> 0x007d }
            java.lang.Class r1 = r10.getClass()     // Catch:{ all -> 0x007d }
            if (r0 != r1) goto L_0x0151
            r0 = 16
            r2.b(r0)     // Catch:{ all -> 0x007d }
            int r0 = r2.e()     // Catch:{ all -> 0x007d }
            if (r0 != r7) goto L_0x0031
            r0 = 16
            r2.b(r0)     // Catch:{ all -> 0x007d }
            r9.a(r3)
            goto L_0x0094
        L_0x0151:
            com.b.a.b.j r1 = r9.c()     // Catch:{ all -> 0x007d }
            com.b.a.b.a.am r1 = r1.a(r0)     // Catch:{ all -> 0x007d }
            r4 = 16
            r2.b(r4)     // Catch:{ all -> 0x007d }
            r2 = 2
            r9.a(r2)     // Catch:{ all -> 0x007d }
            if (r3 == 0) goto L_0x016b
            boolean r2 = r12 instanceof java.lang.Integer     // Catch:{ all -> 0x007d }
            if (r2 != 0) goto L_0x016b
            r9.i()     // Catch:{ all -> 0x007d }
        L_0x016b:
            java.lang.Object r0 = r1.a(r9, r0, r12)     // Catch:{ all -> 0x007d }
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ all -> 0x007d }
            r9.a(r3)
            r10 = r0
            goto L_0x0094
        L_0x0177:
            r2.l()     // Catch:{ all -> 0x007d }
            int r0 = r2.e()     // Catch:{ all -> 0x007d }
            r4 = 8
            if (r0 != r4) goto L_0x019d
            r0 = 0
            r2.l()     // Catch:{ all -> 0x007d }
        L_0x0186:
            r10.put(r1, r0)     // Catch:{ all -> 0x007d }
            r9.a(r10, r1)     // Catch:{ all -> 0x007d }
            r9.a(r3, r0, r1)     // Catch:{ all -> 0x007d }
            int r0 = r2.e()     // Catch:{ all -> 0x007d }
            if (r0 != r7) goto L_0x0031
            r2.l()     // Catch:{ all -> 0x007d }
            r9.a(r3)
            goto L_0x0094
        L_0x019d:
            java.lang.Object r0 = r9.a(r11)     // Catch:{ all -> 0x007d }
            goto L_0x0186
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.a.s.a(com.b.a.b.c, java.util.Map, java.lang.reflect.Type, java.lang.Object):java.util.Map");
    }

    private static void a(c cVar, Object obj) {
        Class<?> cls = obj.getClass();
        Map<String, u> b = cVar.c().b(cls);
        f k = cVar.k();
        if (k.e() == 13) {
            k.b(16);
        } else if (k.e() == 12 || k.e() == 16) {
            Object[] objArr = new Object[1];
            while (true) {
                String b2 = k.b(cVar.b());
                if (b2 == null) {
                    if (k.e() == 13) {
                        k.b(16);
                        return;
                    } else if (k.e() == 16 && cVar.a(e.AllowArbitraryCommas)) {
                    }
                }
                u uVar = b.get(b2);
                if (uVar != null) {
                    Method b3 = uVar.b();
                    Class<?> cls2 = b3.getParameterTypes()[0];
                    Type type = b3.getGenericParameterTypes()[0];
                    if (cls2 == Integer.TYPE) {
                        k.a(2);
                        objArr[0] = z.a(cVar);
                    } else if (cls2 == String.class) {
                        k.a(4);
                        objArr[0] = aq.a(cVar);
                    } else if (cls2 == Long.TYPE) {
                        k.a(2);
                        objArr[0] = ah.a(cVar);
                    } else if (cls2 == List.class) {
                        k.a(12);
                        objArr[0] = o.a.a(cVar, type, null);
                    } else {
                        am a2 = cVar.c().a(cls2, type);
                        k.a(a2.a());
                        objArr[0] = a2.a(cVar, type, null);
                    }
                    try {
                        b3.invoke(obj, objArr);
                        if (k.e() != 16 && k.e() == 13) {
                            k.b(16);
                            return;
                        }
                    } catch (Exception e) {
                        throw new d("set proprety error, " + b3.getName(), e);
                    }
                } else if (!cVar.a(e.IgnoreNotMatch)) {
                    throw new d("setter not found, class " + cls.getName() + ", property " + b2);
                } else {
                    k.i();
                    cVar.j();
                    if (k.e() == 13) {
                        k.l();
                        return;
                    }
                }
            }
        } else {
            throw new d("syntax error, expect {, actual " + k.f());
        }
    }

    public final int a() {
        return 12;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
     arg types: [java.lang.reflect.Type, java.util.ArrayList]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void */
    public final <T> T a(c cVar, Type type, Object obj) {
        if (type instanceof Class) {
            return a(cVar, (Class) type);
        }
        if (type instanceof ParameterizedType) {
            return a(cVar, (ParameterizedType) type, obj);
        }
        if (type instanceof TypeVariable) {
            return cVar.a(obj);
        }
        if (type instanceof WildcardType) {
            return cVar.a(obj);
        }
        if (type instanceof GenericArrayType) {
            Type genericComponentType = ((GenericArrayType) type).getGenericComponentType();
            ArrayList arrayList = new ArrayList();
            cVar.a(genericComponentType, (Collection) arrayList);
            if (genericComponentType instanceof Class) {
                Object obj2 = (Object[]) Array.newInstance((Class) genericComponentType, arrayList.size());
                arrayList.toArray(obj2);
                return obj2;
            }
        }
        throw new d("not support type : " + type);
    }
}
