package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import java.lang.reflect.Type;
import java.math.BigDecimal;

public final class g implements am {
    public static final g a = new g();

    public final int a() {
        return 2;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 2) {
            long t = k.t();
            k.b(16);
            return new BigDecimal(t);
        } else if (k.e() == 3) {
            T x = k.x();
            k.b(16);
            return x;
        } else {
            Object j = cVar.j();
            if (j == null) {
                return null;
            }
            return com.b.a.d.g.e(j);
        }
    }
}
