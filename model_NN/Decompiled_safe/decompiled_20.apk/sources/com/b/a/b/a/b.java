package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d.g;
import java.lang.reflect.Array;
import java.lang.reflect.Type;

public final class b implements am {
    public static final b a = new b();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.a.b.a(com.b.a.b.c, java.lang.Class, com.b.a.b):T
     arg types: [com.b.a.b.c, java.lang.Class<?>, com.b.a.b]
     candidates:
      com.b.a.b.a.b.a(com.b.a.b.c, java.lang.reflect.Type, java.lang.Object):T
      com.b.a.b.a.am.a(com.b.a.b.c, java.lang.reflect.Type, java.lang.Object):T
      com.b.a.b.a.b.a(com.b.a.b.c, java.lang.Class, com.b.a.b):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.d.g.a(java.lang.Object, java.lang.Class, com.b.a.b.j):T
     arg types: [java.lang.Object, java.lang.Class<?>, com.b.a.b.j]
     candidates:
      com.b.a.d.g.a(java.lang.Object, java.lang.reflect.Type, com.b.a.b.j):T
      com.b.a.d.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.Class, com.b.a.b.j):T
      com.b.a.d.g.a(java.lang.Object, java.lang.Class, com.b.a.b.j):T */
    private <T> T a(c cVar, Class<T> cls, com.b.a.b bVar) {
        if (bVar == null) {
            return null;
        }
        int size = bVar.size();
        Class<?> componentType = cls.getComponentType();
        T newInstance = Array.newInstance(componentType, size);
        for (int i = 0; i < size; i++) {
            Object obj = bVar.get(i);
            if (componentType.isArray()) {
                if (!componentType.isInstance(obj)) {
                    obj = a(cVar, (Class) componentType, (com.b.a.b) obj);
                }
                Array.set(newInstance, i, obj);
            } else {
                Array.set(newInstance, i, g.a(obj, (Class) componentType, cVar.c()));
            }
        }
        bVar.c(newInstance);
        bVar.a((Type) componentType);
        return newInstance;
    }

    public final int a() {
        return 14;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 8) {
            k.b(16);
            return null;
        } else if (k.e() == 4) {
            Object n = k.n();
            k.b(16);
            return n;
        } else {
            Class cls = (Class) type;
            Class<?> componentType = cls.getComponentType();
            com.b.a.b bVar = new com.b.a.b();
            cVar.a(componentType, bVar, obj);
            return a(cVar, cls, bVar);
        }
    }
}
