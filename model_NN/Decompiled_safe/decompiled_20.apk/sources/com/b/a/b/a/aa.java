package com.b.a.b.a;

import com.b.a.b.f;
import com.b.a.d.c;
import com.b.a.d.g;
import java.lang.reflect.Type;
import java.util.Map;

public final class aa extends u {
    public aa(Class<?> cls, c cVar) {
        super(cls, cVar);
    }

    public final int a() {
        return 2;
    }

    public final void a(com.b.a.b.c cVar, Object obj, Type type, Map<String, Object> map) {
        Integer j;
        f k = cVar.k();
        if (k.e() == 2) {
            int u = k.u();
            k.b(16);
            if (obj == null) {
                map.put(this.a.c(), Integer.valueOf(u));
            } else {
                a(obj, Integer.valueOf(u));
            }
        } else {
            if (k.e() == 8) {
                j = null;
                k.b(16);
            } else {
                j = g.j(cVar.j());
            }
            if (j != null || c() != Integer.TYPE) {
                if (obj == null) {
                    map.put(this.a.c(), j);
                } else {
                    a(obj, j);
                }
            }
        }
    }
}
