package com.b.a.b.a;

import com.b.a.b.j;
import java.lang.reflect.Constructor;

public final class as extends ad {
    public as(j jVar, Class<?> cls) {
        super(jVar, cls);
    }

    private static Throwable a(String str, Throwable th, Class<?> cls) {
        Constructor<?> constructor;
        Constructor<?> constructor2 = null;
        Constructor<?> constructor3 = null;
        Constructor<?>[] constructors = cls.getConstructors();
        int length = constructors.length;
        int i = 0;
        Constructor<?> constructor4 = null;
        while (i < length) {
            Constructor<?> constructor5 = constructors[i];
            if (constructor5.getParameterTypes().length == 0) {
                Constructor<?> constructor6 = constructor3;
                constructor = constructor5;
                constructor5 = constructor6;
            } else if (constructor5.getParameterTypes().length == 1 && constructor5.getParameterTypes()[0] == String.class) {
                constructor = constructor2;
            } else if (constructor5.getParameterTypes().length == 2 && constructor5.getParameterTypes()[0] == String.class && constructor5.getParameterTypes()[1] == Throwable.class) {
                constructor4 = constructor5;
                constructor5 = constructor3;
                constructor = constructor2;
            } else {
                constructor5 = constructor3;
                constructor = constructor2;
            }
            i++;
            constructor2 = constructor;
            constructor3 = constructor5;
        }
        if (constructor4 != null) {
            return (Throwable) constructor4.newInstance(str, th);
        } else if (constructor3 != null) {
            return (Throwable) constructor3.newInstance(str);
        } else if (constructor2 != null) {
            return (Throwable) constructor2.newInstance(new Object[0]);
        } else {
            return null;
        }
    }

    public final int a() {
        return 12;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final <T> T a(com.b.a.b.c r11, java.lang.reflect.Type r12, java.lang.Object r13) {
        /*
            r10 = this;
            com.b.a.b.f r5 = r11.k()
            int r0 = r11.d()
            r1 = 2
            if (r0 != r1) goto L_0x004d
            r0 = 0
            r11.a(r0)
        L_0x000f:
            r3 = 0
            r0 = 0
            if (r12 == 0) goto L_0x013c
            boolean r1 = r12 instanceof java.lang.Class
            if (r1 == 0) goto L_0x013c
            java.lang.Class r12 = (java.lang.Class) r12
            java.lang.Class<java.lang.Throwable> r1 = java.lang.Throwable.class
            boolean r1 = r1.isAssignableFrom(r12)
            if (r1 == 0) goto L_0x013c
        L_0x0021:
            r4 = 0
            r1 = 0
            java.util.HashMap r6 = new java.util.HashMap
            r6.<init>()
            r2 = r12
        L_0x0029:
            com.b.a.b.k r0 = r11.b()
            java.lang.String r0 = r5.b(r0)
            if (r0 != 0) goto L_0x006d
            int r7 = r5.e()
            r8 = 13
            if (r7 != r8) goto L_0x005d
            r0 = 16
            r5.b(r0)
        L_0x0040:
            if (r2 != 0) goto L_0x0120
            java.lang.Exception r0 = new java.lang.Exception
            r0.<init>(r4, r3)
        L_0x0047:
            if (r1 == 0) goto L_0x004c
            r0.setStackTrace(r1)
        L_0x004c:
            return r0
        L_0x004d:
            int r0 = r5.e()
            r1 = 12
            if (r0 == r1) goto L_0x000f
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)
            throw r0
        L_0x005d:
            int r7 = r5.e()
            r8 = 16
            if (r7 != r8) goto L_0x006d
            com.b.a.b.e r7 = com.b.a.b.e.AllowArbitraryCommas
            boolean r7 = r5.a(r7)
            if (r7 != 0) goto L_0x0029
        L_0x006d:
            r7 = 4
            r5.a(r7)
            java.lang.String r7 = "@type"
            boolean r7 = r7.equals(r0)
            if (r7 == 0) goto L_0x00b4
            int r0 = r5.e()
            r2 = 4
            if (r0 != r2) goto L_0x00ac
            java.lang.String r0 = r5.q()
            java.lang.Class r0 = com.b.a.d.g.a(r0)
            r2 = 16
            r5.b(r2)
            r2 = r3
            r3 = r4
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0092:
            int r4 = r5.e()
            r7 = 16
            if (r4 == r7) goto L_0x0136
            int r4 = r5.e()
            r7 = 13
            if (r4 != r7) goto L_0x011a
            r4 = 16
            r5.b(r4)
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0040
        L_0x00ac:
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)
            throw r0
        L_0x00b4:
            java.lang.String r7 = "message"
            boolean r7 = r7.equals(r0)
            if (r7 == 0) goto L_0x00e2
            int r0 = r5.e()
            r4 = 8
            if (r0 != r4) goto L_0x00ce
            r0 = 0
        L_0x00c5:
            r5.l()
            r9 = r1
            r1 = r2
            r2 = r3
            r3 = r0
            r0 = r9
            goto L_0x0092
        L_0x00ce:
            int r0 = r5.e()
            r4 = 4
            if (r0 != r4) goto L_0x00da
            java.lang.String r0 = r5.q()
            goto L_0x00c5
        L_0x00da:
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)
            throw r0
        L_0x00e2:
            java.lang.String r7 = "cause"
            boolean r7 = r7.equals(r0)
            if (r7 == 0) goto L_0x00f9
            r0 = 0
            java.lang.String r3 = "cause"
            java.lang.Object r0 = r10.a(r11, r0, r3)
            java.lang.Throwable r0 = (java.lang.Throwable) r0
            r3 = r4
            r9 = r2
            r2 = r0
            r0 = r1
            r1 = r9
            goto L_0x0092
        L_0x00f9:
            java.lang.String r7 = "stackTrace"
            boolean r7 = r7.equals(r0)
            if (r7 == 0) goto L_0x010d
            java.lang.Class<java.lang.StackTraceElement[]> r0 = java.lang.StackTraceElement[].class
            java.lang.Object r0 = r11.a(r0)
            java.lang.StackTraceElement[] r0 = (java.lang.StackTraceElement[]) r0
            r1 = r2
            r2 = r3
            r3 = r4
            goto L_0x0092
        L_0x010d:
            java.lang.Object r7 = r11.j()
            r6.put(r0, r7)
            r0 = r1
            r1 = r2
            r2 = r3
            r3 = r4
            goto L_0x0092
        L_0x011a:
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0029
        L_0x0120:
            java.lang.Throwable r0 = a(r4, r3, r2)     // Catch:{ Exception -> 0x012d }
            if (r0 != 0) goto L_0x0047
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x012d }
            r0.<init>(r4, r3)     // Catch:{ Exception -> 0x012d }
            goto L_0x0047
        L_0x012d:
            r0 = move-exception
            com.b.a.d r1 = new com.b.a.d
            java.lang.String r2 = "create instance error"
            r1.<init>(r2, r0)
            throw r1
        L_0x0136:
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0029
        L_0x013c:
            r12 = r0
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.a.as.a(com.b.a.b.c, java.lang.reflect.Type, java.lang.Object):java.lang.Object");
    }
}
