package com.b.a.b.a;

import com.b.a.b.d;
import com.b.a.d.c;
import java.lang.reflect.Type;
import java.util.Map;

public final class r extends u {
    private am c;

    public r(Class<?> cls, c cVar) {
        super(cls, cVar);
    }

    public final int a() {
        if (this.c != null) {
            return this.c.a();
        }
        return 2;
    }

    public final void a(com.b.a.b.c cVar, Object obj, Type type, Map<String, Object> map) {
        if (this.c == null) {
            this.c = cVar.c().a(this.a);
        }
        Object a = this.c.a(cVar, d(), this.a.c());
        if (cVar.d() == 1) {
            d h = cVar.h();
            h.a(this);
            h.a(cVar.f());
            cVar.a(0);
        } else if (obj == null) {
            map.put(this.a.c(), a);
        } else {
            a(obj, a);
        }
    }
}
