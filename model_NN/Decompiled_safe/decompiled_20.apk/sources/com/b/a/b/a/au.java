package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.d;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

public final class au extends a implements am {
    public static final au a = new au();

    public final int a() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public final <T> T a(c cVar, Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Date) {
            return new Timestamp(((Date) obj).getTime());
        }
        if (obj instanceof Number) {
            return new Timestamp(((Number) obj).longValue());
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() == 0) {
                return null;
            }
            try {
                return new Timestamp(cVar.a().parse(str).getTime());
            } catch (ParseException e) {
                return new Timestamp(Long.parseLong(str));
            }
        } else {
            throw new d("parse error");
        }
    }
}
