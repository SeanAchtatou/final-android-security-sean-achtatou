package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d;
import java.lang.reflect.Type;
import java.sql.Time;

public final class at implements am {
    public static final at a = new at();

    public final int a() {
        return 2;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 16) {
            k.b(4);
            if (k.e() != 4) {
                throw new d("syntax error");
            }
            k.a(2);
            if (k.e() != 2) {
                throw new d("syntax error");
            }
            long t = k.t();
            k.b(13);
            if (k.e() != 13) {
                throw new d("syntax error");
            }
            k.b(16);
            return new Time(t);
        }
        T j = cVar.j();
        if (j == null) {
            return null;
        }
        if (j instanceof Time) {
            return j;
        }
        if (j instanceof Number) {
            return new Time(((Number) j).longValue());
        }
        if (j instanceof String) {
            String str = (String) j;
            if (str.length() == 0) {
                return null;
            }
            f fVar = new f(str);
            return new Time(fVar.y() ? fVar.z().getTimeInMillis() : Long.parseLong(str));
        }
        throw new d("parse error");
    }
}
