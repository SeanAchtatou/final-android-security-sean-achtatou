package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d;
import java.text.ParseException;
import java.util.Date;

public final class p extends a implements am {
    public static final p a = new p();

    public final int a() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public final <T> T a(c cVar, Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Date) {
            return obj;
        }
        if (obj instanceof Number) {
            return new Date(((Number) obj).longValue());
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() == 0) {
                return null;
            }
            f fVar = new f(str);
            if (fVar.y()) {
                return fVar.z().getTime();
            }
            try {
                return cVar.a().parse(str);
            } catch (ParseException e) {
                return new Date(Long.parseLong(str));
            }
        } else {
            throw new d("parse error");
        }
    }
}
