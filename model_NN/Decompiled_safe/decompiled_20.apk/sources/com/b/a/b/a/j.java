package com.b.a.b.a;

import com.b.a.b.f;
import com.b.a.d.c;
import com.b.a.d.g;
import java.lang.reflect.Type;
import java.util.Map;

public final class j extends u {
    public j(Class<?> cls, c cVar) {
        super(cls, cVar);
    }

    public final int a() {
        return 6;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.a.u.a(java.lang.Object, boolean):void
     arg types: [java.lang.Object, int]
     candidates:
      com.b.a.b.a.u.a(java.lang.Object, java.lang.Object):void
      com.b.a.b.a.u.a(java.lang.Object, boolean):void */
    public final void a(com.b.a.b.c cVar, Object obj, Type type, Map<String, Object> map) {
        boolean z = true;
        f k = cVar.k();
        if (k.e() == 6) {
            k.b(16);
            if (obj == null) {
                map.put(this.a.c(), Boolean.TRUE);
            } else {
                a(obj, true);
            }
        } else if (k.e() == 2) {
            int u = k.u();
            k.b(16);
            if (u != 1) {
                z = false;
            }
            if (obj == null) {
                map.put(this.a.c(), Boolean.valueOf(z));
            } else {
                a(obj, z);
            }
        } else if (k.e() == 8) {
            k.b(16);
            if (c() != Boolean.TYPE) {
                a(obj, (Object) null);
            }
        } else if (k.e() == 7) {
            k.b(16);
            if (obj == null) {
                map.put(this.a.c(), Boolean.FALSE);
            } else {
                a(obj, false);
            }
        } else {
            Boolean k2 = g.k(cVar.j());
            if (k2 != null || c() != Boolean.TYPE) {
                if (obj == null) {
                    map.put(this.a.c(), k2);
                } else {
                    a(obj, k2);
                }
            }
        }
    }
}
