package com.b.a.b;

import android.support.v4.util.TimeUtils;
import android.support.v4.view.MotionEventCompat;
import com.amap.api.search.route.Route;
import com.umeng.fb.f;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.util.LangUtils;

public final class g {
    public static String a(int i) {
        switch (i) {
            case 1:
                return f.an;
            case 2:
                return "int";
            case 3:
                return "float";
            case 4:
                return "string";
            case 5:
                return "iso8601";
            case 6:
                return "true";
            case MotionEventCompat.ACTION_HOVER_MOVE:
                return HttpState.PREEMPTIVE_DEFAULT;
            case 8:
                return "null";
            case MotionEventCompat.ACTION_HOVER_ENTER:
                return "new";
            case 10:
                return "(";
            case Route.DrivingSaveMoney:
                return ")";
            case Route.DrivingLeastDistance:
                return "{";
            case Route.DrivingNoFastRoad:
                return "}";
            case 14:
                return "[";
            case 15:
                return "]";
            case 16:
                return ",";
            case LangUtils.HASH_SEED /*17*/:
                return ":";
            case 18:
                return "ident";
            case TimeUtils.HUNDRED_DAY_FIELD_LEN:
                return "fieldName";
            case MultiThreadedHttpConnectionManager.DEFAULT_MAX_TOTAL_CONNECTIONS /*20*/:
                return "EOF";
            case 21:
                return "Set";
            case 22:
                return "TreeSet";
            default:
                return "Unkown";
        }
    }
}
