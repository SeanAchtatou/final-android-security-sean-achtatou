package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d.g;
import java.lang.reflect.Type;

public final class ah implements am {
    public static final ah a = new ah();

    public static <T> T a(c cVar) {
        f k = cVar.k();
        if (k.e() == 2) {
            long t = k.t();
            k.b(16);
            return Long.valueOf(t);
        }
        Object j = cVar.j();
        if (j == null) {
            return null;
        }
        return g.i(j);
    }

    public final int a() {
        return 2;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        return a(cVar);
    }
}
