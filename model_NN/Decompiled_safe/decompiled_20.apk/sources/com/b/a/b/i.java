package com.b.a.b;

public final class i {
    private Object a;
    private final i b;
    private final Object c;

    public i(i iVar, Object obj, Object obj2) {
        this.b = iVar;
        this.a = obj;
        this.c = obj2;
    }

    public final Object a() {
        return this.a;
    }

    public final void a(Object obj) {
        this.a = obj;
    }

    public final i b() {
        return this.b;
    }

    public final Object c() {
        return this.c;
    }

    public final String d() {
        return this.b == null ? "$" : this.c instanceof Integer ? this.b.d() + "[" + this.c + "]" : this.b.d() + "." + this.c;
    }

    public final String toString() {
        return d();
    }
}
