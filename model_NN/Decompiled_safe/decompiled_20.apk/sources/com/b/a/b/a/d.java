package com.b.a.b.a;

import com.b.a.b.f;
import com.b.a.d.c;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

public final class d extends u {
    public d(Class<?> cls, c cVar) {
        super(cls, cVar);
    }

    public final int a() {
        return 14;
    }

    public final void a(com.b.a.b.c cVar, Object obj, Type type, Map<String, Object> map) {
        ArrayList arrayList;
        f k = cVar.k();
        if (k.e() == 8) {
            k.b(16);
            arrayList = null;
        } else {
            arrayList = new ArrayList();
            c.a(cVar, arrayList);
        }
        if (obj == null) {
            map.put(this.a.c(), arrayList);
        } else {
            a(obj, arrayList);
        }
    }
}
