package com.b.a.b;

import com.b.a.b;
import com.b.a.b.a.aa;
import com.b.a.b.a.ab;
import com.b.a.b.a.ac;
import com.b.a.b.a.ad;
import com.b.a.b.a.ae;
import com.b.a.b.a.ag;
import com.b.a.b.a.ah;
import com.b.a.b.a.ai;
import com.b.a.b.a.aj;
import com.b.a.b.a.al;
import com.b.a.b.a.am;
import com.b.a.b.a.an;
import com.b.a.b.a.ao;
import com.b.a.b.a.ap;
import com.b.a.b.a.aq;
import com.b.a.b.a.ar;
import com.b.a.b.a.at;
import com.b.a.b.a.au;
import com.b.a.b.a.d;
import com.b.a.b.a.f;
import com.b.a.b.a.g;
import com.b.a.b.a.h;
import com.b.a.b.a.i;
import com.b.a.b.a.k;
import com.b.a.b.a.l;
import com.b.a.b.a.m;
import com.b.a.b.a.n;
import com.b.a.b.a.o;
import com.b.a.b.a.p;
import com.b.a.b.a.q;
import com.b.a.b.a.r;
import com.b.a.b.a.s;
import com.b.a.b.a.u;
import com.b.a.b.a.v;
import com.b.a.b.a.w;
import com.b.a.b.a.x;
import com.b.a.b.a.y;
import com.b.a.b.a.z;
import com.b.a.d.c;
import com.b.a.d.e;
import java.io.Closeable;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;

public final class j {
    private static j c = new j();
    protected final k a = new k();
    private final Set<Class<?>> b = new HashSet();
    private final e<Type, am> d = new e<>();
    private s e = new s();
    private boolean f = false;

    public j() {
        this.b.add(Boolean.TYPE);
        this.b.add(Boolean.class);
        this.b.add(Character.TYPE);
        this.b.add(Character.class);
        this.b.add(Byte.TYPE);
        this.b.add(Byte.class);
        this.b.add(Short.TYPE);
        this.b.add(Short.class);
        this.b.add(Integer.TYPE);
        this.b.add(Integer.class);
        this.b.add(Long.TYPE);
        this.b.add(Long.class);
        this.b.add(Float.TYPE);
        this.b.add(Float.class);
        this.b.add(Double.TYPE);
        this.b.add(Double.class);
        this.b.add(BigInteger.class);
        this.b.add(BigDecimal.class);
        this.b.add(String.class);
        this.b.add(Date.class);
        this.b.add(java.sql.Date.class);
        this.b.add(Time.class);
        this.b.add(Timestamp.class);
        this.d.a(SimpleDateFormat.class, q.a);
        this.d.a(Timestamp.class, au.a);
        this.d.a(java.sql.Date.class, ao.a);
        this.d.a(Time.class, at.a);
        this.d.a(Date.class, p.a);
        this.d.a(Calendar.class, k.a);
        this.d.a(com.b.a.e.class, ac.a);
        this.d.a(b.class, ab.a);
        this.d.a(Map.class, aj.a);
        this.d.a(HashMap.class, aj.a);
        this.d.a(LinkedHashMap.class, aj.a);
        this.d.a(TreeMap.class, aj.a);
        this.d.a(ConcurrentMap.class, aj.a);
        this.d.a(ConcurrentHashMap.class, aj.a);
        this.d.a(Collection.class, o.a);
        this.d.a(List.class, o.a);
        this.d.a(ArrayList.class, o.a);
        this.d.a(Object.class, ae.a);
        this.d.a(String.class, aq.a);
        this.d.a(Character.TYPE, m.a);
        this.d.a(Character.class, m.a);
        this.d.a(Byte.TYPE, al.a);
        this.d.a(Byte.class, al.a);
        this.d.a(Short.TYPE, al.a);
        this.d.a(Short.class, al.a);
        this.d.a(Integer.TYPE, z.a);
        this.d.a(Integer.class, z.a);
        this.d.a(Long.TYPE, ah.a);
        this.d.a(Long.class, ah.a);
        this.d.a(BigInteger.class, h.a);
        this.d.a(BigDecimal.class, g.a);
        this.d.a(Float.TYPE, w.a);
        this.d.a(Float.class, w.a);
        this.d.a(Double.TYPE, al.a);
        this.d.a(Double.class, al.a);
        this.d.a(Boolean.TYPE, i.a);
        this.d.a(Boolean.class, i.a);
        this.d.a(Class.class, n.a);
        this.d.a(char[].class, l.a);
        this.d.a(UUID.class, an.a);
        this.d.a(TimeZone.class, an.a);
        this.d.a(Locale.class, ag.a);
        this.d.a(InetAddress.class, x.a);
        this.d.a(Inet4Address.class, x.a);
        this.d.a(Inet6Address.class, x.a);
        this.d.a(InetSocketAddress.class, y.a);
        this.d.a(File.class, v.a);
        this.d.a(URI.class, an.a);
        this.d.a(URL.class, an.a);
        this.d.a(Pattern.class, an.a);
        this.d.a(Charset.class, an.a);
        this.d.a(Number.class, al.a);
        this.d.a(StackTraceElement.class, ap.a);
        this.d.a(Serializable.class, this.e);
        this.d.a(Cloneable.class, this.e);
        this.d.a(Comparable.class, this.e);
        this.d.a(Closeable.class, this.e);
    }

    public static u a(j jVar, Class<?> cls, c cVar) {
        Class<?> a2 = cVar.a();
        if (a2 == Boolean.TYPE || a2 == Boolean.class) {
            return new com.b.a.b.a.j(cls, cVar);
        }
        if (a2 == Integer.TYPE || a2 == Integer.class) {
            return new aa(cls, cVar);
        }
        if (a2 == Long.TYPE || a2 == Long.class) {
            return new ai(jVar, cls, cVar);
        }
        if (a2 == String.class) {
            return new ar(jVar, cls, cVar);
        }
        if (a2 != List.class && a2 != ArrayList.class) {
            return new r(cls, cVar);
        }
        Type b2 = cVar.b();
        return (!(b2 instanceof ParameterizedType) || ((ParameterizedType) b2).getActualTypeArguments()[0] != String.class) ? new f(cls, cVar) : new d(cls, cVar);
    }

    public static j a() {
        return c;
    }

    public static Field a(Class<?> cls, String str) {
        while (true) {
            for (Field field : cls.getDeclaredFields()) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            if (cls.getSuperclass() == null || cls.getSuperclass() == Object.class) {
                return null;
            }
            cls = cls.getSuperclass();
        }
        return null;
    }

    public final am a(c cVar) {
        return a(cVar.a(), cVar.b());
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [java.lang.reflect.Type, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.b.a.b.a.am a(java.lang.Class<?> r4, java.lang.reflect.Type r5) {
        /*
            r3 = this;
            com.b.a.d.e<java.lang.reflect.Type, com.b.a.b.a.am> r0 = r3.d
            java.lang.Object r0 = r0.a(r5)
            com.b.a.b.a.am r0 = (com.b.a.b.a.am) r0
            if (r0 == 0) goto L_0x000b
        L_0x000a:
            return r0
        L_0x000b:
            if (r5 != 0) goto L_0x00ab
            r1 = r4
        L_0x000e:
            com.b.a.d.e<java.lang.reflect.Type, com.b.a.b.a.am> r0 = r3.d
            java.lang.Object r0 = r0.a(r1)
            com.b.a.b.a.am r0 = (com.b.a.b.a.am) r0
            if (r0 != 0) goto L_0x000a
            boolean r2 = r1 instanceof java.lang.reflect.WildcardType
            if (r2 != 0) goto L_0x0024
            boolean r2 = r1 instanceof java.lang.reflect.TypeVariable
            if (r2 != 0) goto L_0x0024
            boolean r2 = r1 instanceof java.lang.reflect.ParameterizedType
            if (r2 == 0) goto L_0x002c
        L_0x0024:
            com.b.a.d.e<java.lang.reflect.Type, com.b.a.b.a.am> r0 = r3.d
            java.lang.Object r0 = r0.a(r4)
            com.b.a.b.a.am r0 = (com.b.a.b.a.am) r0
        L_0x002c:
            if (r0 != 0) goto L_0x000a
            boolean r0 = r4.isEnum()
            if (r0 == 0) goto L_0x003f
            com.b.a.b.a.t r0 = new com.b.a.b.a.t
            r0.<init>(r4)
        L_0x0039:
            com.b.a.d.e<java.lang.reflect.Type, com.b.a.b.a.am> r2 = r3.d
            r2.a(r1, r0)
            goto L_0x000a
        L_0x003f:
            boolean r0 = r4.isArray()
            if (r0 == 0) goto L_0x0048
            com.b.a.b.a.b r0 = com.b.a.b.a.b.a
            goto L_0x000a
        L_0x0048:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            if (r4 == r0) goto L_0x005c
            java.lang.Class<java.util.HashSet> r0 = java.util.HashSet.class
            if (r4 == r0) goto L_0x005c
            java.lang.Class<java.util.Collection> r0 = java.util.Collection.class
            if (r4 == r0) goto L_0x005c
            java.lang.Class<java.util.List> r0 = java.util.List.class
            if (r4 == r0) goto L_0x005c
            java.lang.Class<java.util.ArrayList> r0 = java.util.ArrayList.class
            if (r4 != r0) goto L_0x007a
        L_0x005c:
            boolean r0 = r1 instanceof java.lang.reflect.ParameterizedType
            if (r0 == 0) goto L_0x0077
            r0 = r1
            java.lang.reflect.ParameterizedType r0 = (java.lang.reflect.ParameterizedType) r0
            java.lang.reflect.Type[] r0 = r0.getActualTypeArguments()
            r2 = 0
            r2 = r0[r2]
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r2 != r0) goto L_0x0071
            com.b.a.b.a.c r0 = com.b.a.b.a.c.a
            goto L_0x0039
        L_0x0071:
            com.b.a.b.a.e r0 = new com.b.a.b.a.e
            r0.<init>(r4, r2)
            goto L_0x0039
        L_0x0077:
            com.b.a.b.a.o r0 = com.b.a.b.a.o.a
            goto L_0x0039
        L_0x007a:
            java.lang.Class<java.util.Collection> r0 = java.util.Collection.class
            boolean r0 = r0.isAssignableFrom(r4)
            if (r0 == 0) goto L_0x0085
            com.b.a.b.a.o r0 = com.b.a.b.a.o.a
            goto L_0x0039
        L_0x0085:
            java.lang.Class<java.util.Map> r0 = java.util.Map.class
            boolean r0 = r0.isAssignableFrom(r4)
            if (r0 == 0) goto L_0x0090
            com.b.a.b.a.aj r0 = com.b.a.b.a.aj.a
            goto L_0x0039
        L_0x0090:
            java.lang.Class<java.lang.Throwable> r0 = java.lang.Throwable.class
            boolean r0 = r0.isAssignableFrom(r4)
            if (r0 == 0) goto L_0x009e
            com.b.a.b.a.as r0 = new com.b.a.b.a.as
            r0.<init>(r3, r4)
            goto L_0x0039
        L_0x009e:
            java.lang.Class<java.lang.Class> r0 = java.lang.Class.class
            if (r4 != r0) goto L_0x00a5
            com.b.a.b.a.s r0 = r3.e
            goto L_0x0039
        L_0x00a5:
            com.b.a.b.a.ad r0 = new com.b.a.b.a.ad
            r0.<init>(r3, r4, r1)
            goto L_0x0039
        L_0x00ab:
            r1 = r5
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.j.a(java.lang.Class, java.lang.reflect.Type):com.b.a.b.a.am");
    }

    public final am a(Type type) {
        Type type2 = type;
        while (true) {
            am a2 = this.d.a(type2);
            if (a2 != null) {
                return a2;
            }
            if (type2 instanceof Class) {
                return a((Class) type2, type2);
            }
            if (!(type2 instanceof ParameterizedType)) {
                return this.e;
            }
            Type rawType = ((ParameterizedType) type2).getRawType();
            if (rawType instanceof Class) {
                return a((Class) rawType, type2);
            }
            type2 = rawType;
        }
    }

    public final boolean a(Class<?> cls) {
        return this.b.contains(cls);
    }

    public final k b() {
        return this.a;
    }

    public final Map<String, u> b(Class<?> cls) {
        am a2 = a((Type) cls);
        return a2 instanceof ad ? ((ad) a2).b() : Collections.emptyMap();
    }
}
