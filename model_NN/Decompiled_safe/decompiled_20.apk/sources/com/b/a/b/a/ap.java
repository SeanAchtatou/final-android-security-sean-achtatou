package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.e;
import com.b.a.b.f;
import com.b.a.b.g;
import com.b.a.d;
import java.lang.reflect.Type;

public final class ap implements am {
    public static final ap a = new ap();

    public final int a() {
        return 12;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 8) {
            k.l();
            return null;
        } else if (k.e() == 12 || k.e() == 16) {
            String str = null;
            String str2 = null;
            String str3 = null;
            int i = 0;
            while (true) {
                String b = k.b(cVar.b());
                if (b == null) {
                    if (k.e() == 13) {
                        k.b(16);
                        break;
                    } else if (k.e() == 16 && k.a(e.AllowArbitraryCommas)) {
                    }
                }
                k.a(4);
                if (b == "className") {
                    if (k.e() == 8) {
                        str = null;
                    } else if (k.e() == 4) {
                        str = k.q();
                    } else {
                        throw new d("syntax error");
                    }
                } else if (b == "methodName") {
                    if (k.e() == 8) {
                        str2 = null;
                    } else if (k.e() == 4) {
                        str2 = k.q();
                    } else {
                        throw new d("syntax error");
                    }
                } else if (b == "fileName") {
                    if (k.e() == 8) {
                        str3 = null;
                    } else if (k.e() == 4) {
                        str3 = k.q();
                    } else {
                        throw new d("syntax error");
                    }
                } else if (b == "lineNumber") {
                    if (k.e() == 8) {
                        i = 0;
                    } else if (k.e() == 2) {
                        i = k.u();
                    } else {
                        throw new d("syntax error");
                    }
                } else if (b == "nativeMethod") {
                    if (k.e() == 8) {
                        k.b(16);
                    } else if (k.e() == 6) {
                        k.b(16);
                    } else if (k.e() == 7) {
                        k.b(16);
                    } else {
                        throw new d("syntax error");
                    }
                } else if (b != "@type") {
                    throw new d("syntax error : " + b);
                } else if (k.e() != 8) {
                    if (k.e() == 4) {
                        String q = k.q();
                        if (!q.equals("java.lang.StackTraceElement")) {
                            throw new d("syntax error : " + q);
                        }
                    } else {
                        throw new d("syntax error");
                    }
                }
                if (k.e() != 16 && k.e() == 13) {
                    k.b(16);
                    break;
                }
            }
            return new StackTraceElement(str, str2, str3, i);
        } else {
            throw new d("syntax error: " + g.a(k.e()));
        }
    }
}
