package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d.g;
import java.lang.reflect.Type;
import java.math.BigDecimal;

public final class z implements am {
    public static final z a = new z();

    public static <T> T a(c cVar) {
        f k = cVar.k();
        if (k.e() == 8) {
            k.b(16);
            return null;
        } else if (k.e() == 2) {
            int u = k.u();
            k.b(16);
            return Integer.valueOf(u);
        } else if (k.e() != 3) {
            return g.j(cVar.j());
        } else {
            BigDecimal x = k.x();
            k.b(16);
            return Integer.valueOf(x.intValue());
        }
    }

    public final int a() {
        return 2;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        return a(cVar);
    }
}
