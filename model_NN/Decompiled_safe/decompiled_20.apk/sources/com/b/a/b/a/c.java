package com.b.a.b.a;

import com.b.a.b.e;
import com.b.a.b.f;
import com.b.a.d;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class c implements am {
    public static final c a = new c();

    public static void a(com.b.a.b.c cVar, Collection collection) {
        String obj;
        f k = cVar.k();
        if (k.e() == 8) {
            k.b(16);
            return;
        }
        if (k.e() == 21) {
            k.l();
        }
        if (k.e() != 14) {
            throw new d("exepct '[', but " + k.e());
        }
        while (true) {
            k.b(4);
            while (true) {
                if (k.a(e.AllowArbitraryCommas)) {
                    while (k.e() == 16) {
                        k.l();
                    }
                }
                if (k.e() != 15) {
                    if (k.e() == 4) {
                        obj = k.q();
                        k.b(16);
                    } else {
                        Object j = cVar.j();
                        obj = j == null ? null : j.toString();
                    }
                    collection.add(obj);
                    if (k.e() == 16) {
                    }
                } else {
                    k.b(16);
                    return;
                }
            }
        }
    }

    public final int a() {
        return 14;
    }

    public final <T> T a(com.b.a.b.c cVar, Type type, Object obj) {
        Type rawType;
        T t = null;
        f k = cVar.k();
        if (k.e() == 8) {
            k.b(16);
        } else {
            if (type == Set.class || type == HashSet.class) {
                t = new HashSet();
            } else if ((type instanceof ParameterizedType) && ((rawType = ((ParameterizedType) type).getRawType()) == Set.class || rawType == HashSet.class)) {
                t = new HashSet();
            }
            if (t == null) {
                t = new ArrayList();
            }
            a(cVar, t);
        }
        return t;
    }
}
