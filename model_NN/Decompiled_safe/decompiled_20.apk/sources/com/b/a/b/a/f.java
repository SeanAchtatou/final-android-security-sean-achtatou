package com.b.a.b.a;

import com.b.a.b.i;
import com.b.a.d.c;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

public final class f extends u {
    private final Type c;
    private int d;
    private am e;

    public f(Class<?> cls, c cVar) {
        super(cls, cVar);
        if (d() instanceof ParameterizedType) {
            this.c = ((ParameterizedType) d()).getActualTypeArguments()[0];
        } else {
            this.c = Object.class;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.b.a.b.c r11, java.lang.reflect.Type r12, java.util.Collection r13) {
        /*
            r10 = this;
            r3 = 0
            r4 = -1
            r9 = 16
            java.lang.reflect.Type r1 = r10.c
            boolean r0 = r1 instanceof java.lang.reflect.TypeVariable
            if (r0 == 0) goto L_0x004a
            boolean r0 = r12 instanceof java.lang.reflect.ParameterizedType
            if (r0 == 0) goto L_0x004a
            r0 = r1
            java.lang.reflect.TypeVariable r0 = (java.lang.reflect.TypeVariable) r0
            java.lang.reflect.ParameterizedType r12 = (java.lang.reflect.ParameterizedType) r12
            r2 = 0
            java.lang.reflect.Type r5 = r12.getRawType()
            boolean r5 = r5 instanceof java.lang.Class
            if (r5 == 0) goto L_0x00d1
            java.lang.reflect.Type r2 = r12.getRawType()
            java.lang.Class r2 = (java.lang.Class) r2
            r5 = r2
        L_0x0023:
            if (r5 == 0) goto L_0x00ce
            java.lang.reflect.TypeVariable[] r2 = r5.getTypeParameters()
            int r6 = r2.length
            r2 = r3
        L_0x002b:
            if (r2 >= r6) goto L_0x00ce
            java.lang.reflect.TypeVariable[] r7 = r5.getTypeParameters()
            r7 = r7[r2]
            java.lang.String r7 = r7.getName()
            java.lang.String r8 = r0.getName()
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x0073
            r0 = r2
        L_0x0042:
            if (r0 == r4) goto L_0x004a
            java.lang.reflect.Type[] r1 = r12.getActualTypeArguments()
            r1 = r1[r0]
        L_0x004a:
            com.b.a.b.f r2 = r11.k()
            int r0 = r2.e()
            r4 = 14
            if (r0 == r4) goto L_0x0076
            com.b.a.d r0 = new com.b.a.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "exepct '[', but "
            r1.<init>(r3)
            int r2 = r2.e()
            java.lang.String r2 = com.b.a.b.g.a(r2)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0073:
            int r2 = r2 + 1
            goto L_0x002b
        L_0x0076:
            com.b.a.b.a.am r0 = r10.e
            if (r0 != 0) goto L_0x008c
            com.b.a.b.j r0 = r11.c()
            com.b.a.b.a.am r0 = r0.a(r1)
            r10.e = r0
            com.b.a.b.a.am r0 = r10.e
            int r0 = r0.a()
            r10.d = r0
        L_0x008c:
            int r0 = r10.d
            r2.b(r0)
            r0 = r3
        L_0x0092:
            com.b.a.b.e r3 = com.b.a.b.e.AllowArbitraryCommas
            boolean r3 = r2.a(r3)
            if (r3 == 0) goto L_0x00a4
        L_0x009a:
            int r3 = r2.e()
            if (r3 != r9) goto L_0x00a4
            r2.l()
            goto L_0x009a
        L_0x00a4:
            int r3 = r2.e()
            r4 = 15
            if (r3 == r4) goto L_0x00ca
            com.b.a.b.a.am r3 = r10.e
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            java.lang.Object r3 = r3.a(r11, r1, r4)
            r13.add(r3)
            r11.a(r13)
            int r3 = r2.e()
            if (r3 != r9) goto L_0x00c7
            int r3 = r10.d
            r2.b(r3)
        L_0x00c7:
            int r0 = r0 + 1
            goto L_0x0092
        L_0x00ca:
            r2.b(r9)
            return
        L_0x00ce:
            r0 = r4
            goto L_0x0042
        L_0x00d1:
            r5 = r2
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.a.f.a(com.b.a.b.c, java.lang.reflect.Type, java.util.Collection):void");
    }

    public final int a() {
        return 14;
    }

    public final void a(com.b.a.b.c cVar, Object obj, Type type, Map<String, Object> map) {
        if (cVar.k().e() == 8) {
            a(obj, (Object) null);
            return;
        }
        ArrayList arrayList = new ArrayList();
        i f = cVar.f();
        cVar.a(f, obj, this.a.c());
        a(cVar, type, arrayList);
        cVar.a(f);
        if (obj == null) {
            map.put(this.a.c(), arrayList);
        } else {
            a(obj, arrayList);
        }
    }
}
