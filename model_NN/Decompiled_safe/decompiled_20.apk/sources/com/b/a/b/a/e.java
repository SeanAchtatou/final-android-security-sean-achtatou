package com.b.a.b.a;

import com.b.a.b.c;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;

public final class e implements am {
    private Type a;
    private Class<?> b;

    public e(Class<?> cls, Type type) {
        this.b = cls;
        this.a = type;
    }

    public final int a() {
        return 14;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        T t = null;
        if (cVar.k().e() == 8) {
            cVar.k().l();
        } else {
            t = this.b.isAssignableFrom(LinkedHashSet.class) ? new LinkedHashSet() : this.b.isAssignableFrom(HashSet.class) ? new HashSet() : new ArrayList();
            cVar.a(this.a, t, obj);
        }
        return t;
    }
}
