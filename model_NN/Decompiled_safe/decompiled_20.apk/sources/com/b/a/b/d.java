package com.b.a.b;

import com.b.a.b.a.u;

public final class d {
    private final i a;
    private final String b;
    private u c;
    private i d;

    public d(i iVar, String str) {
        this.a = iVar;
        this.b = str;
    }

    public final i a() {
        return this.a;
    }

    public final void a(u uVar) {
        this.c = uVar;
    }

    public final void a(i iVar) {
        this.d = iVar;
    }

    public final String b() {
        return this.b;
    }

    public final u c() {
        return this.c;
    }

    public final i d() {
        return this.d;
    }
}
