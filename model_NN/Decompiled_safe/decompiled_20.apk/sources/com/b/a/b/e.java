package com.b.a.b;

public enum e {
    AutoCloseSource,
    AllowComment,
    AllowUnQuotedFieldNames,
    AllowSingleQuotes,
    InternFieldNames,
    AllowISO8601DateFormat,
    AllowArbitraryCommas,
    UseBigDecimal,
    IgnoreNotMatch,
    SortFeidFastMatch,
    DisableASM,
    DisableCircularReferenceDetect,
    InitStringFieldAsEmpty;
    
    private final int n = (1 << ordinal());

    public static boolean a(int i, e eVar) {
        return (eVar.n & i) != 0;
    }

    public static int b(int i, e eVar) {
        return eVar.n | i;
    }

    public final int a() {
        return this.n;
    }
}
