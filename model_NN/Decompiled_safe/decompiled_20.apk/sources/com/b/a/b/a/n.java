package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d;
import com.b.a.d.g;
import java.lang.reflect.Type;

public final class n implements am {
    public static final n a = new n();

    public final int a() {
        return 4;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 8) {
            k.l();
            return null;
        } else if (k.e() != 4) {
            throw new d("expect className");
        } else {
            String q = k.q();
            k.b(16);
            return g.a(q);
        }
    }
}
