package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.b.i;
import com.b.a.d;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class aj implements am {
    public static final aj a = new aj();

    private static Map<Object, Object> a(Type type) {
        Type type2 = type;
        while (type2 != Properties.class) {
            if (type2 == Hashtable.class) {
                return new Hashtable();
            }
            if (type2 == IdentityHashMap.class) {
                return new IdentityHashMap();
            }
            if (type2 == SortedMap.class || type2 == TreeMap.class) {
                return new TreeMap();
            }
            if (type2 == ConcurrentMap.class || type2 == ConcurrentHashMap.class) {
                return new ConcurrentHashMap();
            }
            if (type2 == Map.class || type2 == HashMap.class) {
                return new HashMap();
            }
            if (type2 == LinkedHashMap.class) {
                return new LinkedHashMap();
            }
            if (type2 instanceof ParameterizedType) {
                type2 = ((ParameterizedType) type2).getRawType();
            } else if (type2 instanceof Class) {
                Class cls = (Class) type2;
                if (cls.isInterface()) {
                    throw new d("unsupport type " + type2);
                }
                try {
                    return (Map) cls.newInstance();
                } catch (Exception e) {
                    throw new d("unsupport type " + type2, e);
                }
            } else {
                throw new d("unsupport type " + type2);
            }
        }
        return new Properties();
    }

    public final int a() {
        return 12;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
     arg types: [T, java.lang.Object]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object */
    public final <T> T a(c cVar, Type type, Object obj) {
        T a2;
        f k = cVar.k();
        if (k.e() == 8) {
            k.b(16);
            return null;
        }
        T a3 = a(type);
        i f = cVar.f();
        try {
            cVar.a(f, a3, obj);
            if (k.e() == 13) {
                k.b(16);
                return a3;
            }
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                Type type2 = parameterizedType.getActualTypeArguments()[0];
                Type type3 = parameterizedType.getActualTypeArguments()[1];
                if (String.class == type2) {
                    s sVar = s.a;
                    a2 = s.a(cVar, a3, type3, obj);
                } else {
                    s sVar2 = s.a;
                    a2 = s.a(cVar, a3, type2, type3, obj);
                }
            } else {
                a2 = cVar.a((Map) a3, obj);
            }
            cVar.a(f);
            return a2;
        } finally {
            cVar.a(f);
        }
    }
}
