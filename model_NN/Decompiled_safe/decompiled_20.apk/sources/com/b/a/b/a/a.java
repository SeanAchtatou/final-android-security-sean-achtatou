package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.e;
import com.b.a.b.f;
import com.b.a.d;
import java.lang.reflect.Type;

public abstract class a implements am {
    /* access modifiers changed from: protected */
    public abstract <T> T a(c cVar, Object obj);

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    public final <T> T a(c cVar, Type type, Object obj) {
        Long l;
        f k = cVar.k();
        if (k.e() == 2) {
            Long valueOf = Long.valueOf(k.t());
            k.b(16);
            l = valueOf;
        } else if (k.e() == 4) {
            String q = k.q();
            k.b(16);
            l = q;
            if (k.a(e.AllowISO8601DateFormat)) {
                f fVar = new f(q);
                l = q;
                if (fVar.y()) {
                    l = fVar.z().getTime();
                }
            }
        } else if (k.e() == 8) {
            k.l();
            l = null;
        } else if (cVar.d() == 2) {
            cVar.b(16);
            if (k.e() != 4) {
                throw new d("syntax error");
            } else if (!"val".equals(k.q())) {
                throw new d("syntax error");
            } else {
                k.l();
                cVar.b(17);
                Object j = cVar.j();
                cVar.b(13);
                cVar.a(0);
                l = j;
            }
        } else {
            l = cVar.j();
        }
        return a(cVar, l);
    }
}
