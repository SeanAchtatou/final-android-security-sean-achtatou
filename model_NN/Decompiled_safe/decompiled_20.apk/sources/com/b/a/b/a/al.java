package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d.g;
import java.lang.reflect.Type;

public final class al implements am {
    public static final al a = new al();

    public final int a() {
        return 2;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 2) {
            if (type == Double.TYPE || type == Double.class) {
                String v = k.v();
                k.b(16);
                return Double.valueOf(Double.parseDouble(v));
            }
            long t = k.t();
            k.b(16);
            return (type == Double.TYPE || type == Double.class) ? Double.valueOf((double) t) : (type == Short.TYPE || type == Short.class) ? Short.valueOf((short) ((int) t)) : (type == Byte.TYPE || type == Byte.class) ? Byte.valueOf((byte) ((int) t)) : (t < -2147483648L || t > 2147483647L) ? Long.valueOf(t) : Integer.valueOf((int) t);
        } else if (k.e() != 3) {
            Object j = cVar.j();
            if (j == null) {
                return null;
            }
            return (type == Double.TYPE || type == Double.class) ? g.h(j) : (type == Short.TYPE || type == Short.class) ? g.d(j) : (type == Byte.TYPE || type == Byte.class) ? g.b(j) : g.e(j);
        } else if (type == Double.TYPE || type == Double.class) {
            String v2 = k.v();
            k.b(16);
            return Double.valueOf(Double.parseDouble(v2));
        } else {
            T x = k.x();
            k.b(16);
            return (type == Short.TYPE || type == Short.class) ? Short.valueOf(x.shortValue()) : (type == Byte.TYPE || type == Byte.class) ? Byte.valueOf(x.byteValue()) : x;
        }
    }
}
