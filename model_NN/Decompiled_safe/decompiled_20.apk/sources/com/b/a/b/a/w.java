package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d.g;
import java.lang.reflect.Type;

public final class w implements am {
    public static final w a = new w();

    public final int a() {
        return 2;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 2) {
            String v = k.v();
            k.b(16);
            return Float.valueOf(Float.parseFloat(v));
        } else if (k.e() == 3) {
            float w = k.w();
            k.b(16);
            return Float.valueOf(w);
        } else {
            Object j = cVar.j();
            if (j == null) {
                return null;
            }
            return g.g(j);
        }
    }
}
