package com.b.a.b.a;

import com.b.a.b.f;
import com.b.a.b.j;
import com.b.a.d.c;
import com.b.a.d.g;
import java.lang.reflect.Type;
import java.util.Map;

public final class ai extends u {
    private final am c;

    public ai(j jVar, Class<?> cls, c cVar) {
        super(cls, cVar);
        this.c = jVar.a(cVar);
    }

    public final int a() {
        return this.c.a();
    }

    public final void a(com.b.a.b.c cVar, Object obj, Type type, Map<String, Object> map) {
        Long i;
        f k = cVar.k();
        if (k.e() == 2) {
            long t = k.t();
            k.b(16);
            if (obj == null) {
                map.put(this.a.c(), Long.valueOf(t));
            } else {
                a(obj, Long.valueOf(t));
            }
        } else {
            if (k.e() == 8) {
                i = null;
                k.b(16);
            } else {
                i = g.i(cVar.j());
            }
            if (i != null || c() != Long.TYPE) {
                if (obj == null) {
                    map.put(this.a.c(), i);
                } else {
                    a(obj, i);
                }
            }
        }
    }
}
