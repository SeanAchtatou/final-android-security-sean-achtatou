package com.b.a.b.a;

import com.b.a.b;
import com.b.a.b.c;
import com.b.a.d.g;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public final class af extends u {
    private final int c;
    private final List d;
    private final c e;

    public af(c cVar, List list, int i) {
        super(null, null);
        this.e = cVar;
        this.c = i;
        this.d = list;
    }

    public final int a() {
        return 0;
    }

    public final void a(c cVar, Object obj, Type type, Map<String, Object> map) {
    }

    public final void a(Object obj, Object obj2) {
        b bVar;
        Object b;
        this.d.set(this.c, obj2);
        if ((this.d instanceof b) && (b = (bVar = (b) this.d).b()) != null && Array.getLength(b) > this.c) {
            if (bVar.c() != null) {
                obj2 = g.a(obj2, bVar.c(), this.e.c());
            }
            Array.set(b, this.c, obj2);
        }
    }
}
