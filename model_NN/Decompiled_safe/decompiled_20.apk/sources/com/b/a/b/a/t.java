package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public final class t implements am {
    private final Class<?> a;
    private final Map<Integer, Enum> b = new HashMap();
    private final Map<String, Enum> c = new HashMap();

    public t(Class<?> cls) {
        this.a = cls;
        try {
            for (Object obj : (Object[]) cls.getMethod("values", new Class[0]).invoke(null, new Object[0])) {
                Enum enumR = (Enum) obj;
                this.b.put(Integer.valueOf(enumR.ordinal()), enumR);
                this.c.put(enumR.name(), enumR);
            }
        } catch (Exception e) {
            throw new d("init enum values error, " + cls.getName());
        }
    }

    public final int a() {
        return 2;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        try {
            f k = cVar.k();
            if (k.e() == 2) {
                Integer valueOf = Integer.valueOf(k.u());
                k.b(16);
                T t = this.b.get(valueOf);
                if (t != null) {
                    return t;
                }
                throw new d("parse enum " + this.a.getName() + " error, value : " + valueOf);
            } else if (k.e() == 4) {
                String q = k.q();
                k.b(16);
                if (q.length() == 0) {
                    return null;
                }
                this.c.get(q);
                return Enum.valueOf(this.a, q);
            } else if (k.e() == 8) {
                k.b(16);
                return null;
            } else {
                throw new d("parse enum " + this.a.getName() + " error, value : " + cVar.j());
            }
        } catch (d e) {
            throw e;
        } catch (Throwable th) {
            throw new d(th.getMessage(), th);
        }
    }
}
