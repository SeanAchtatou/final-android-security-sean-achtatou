package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.d;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;

public final class o implements am {
    public static final o a = new o();

    public final int a() {
        return 14;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        T t;
        if (cVar.k().e() == 8) {
            cVar.k().b(16);
            return null;
        }
        Type type2 = type;
        while (!(type2 instanceof Class)) {
            if (type2 instanceof ParameterizedType) {
                type2 = ((ParameterizedType) type2).getRawType();
            } else {
                throw new d("TODO");
            }
        }
        Class<AbstractCollection> cls = (Class) type2;
        if (cls == AbstractCollection.class) {
            t = new ArrayList();
        } else if (cls.isAssignableFrom(HashSet.class)) {
            t = new HashSet();
        } else if (cls.isAssignableFrom(LinkedHashSet.class)) {
            t = new LinkedHashSet();
        } else if (cls.isAssignableFrom(ArrayList.class)) {
            t = new ArrayList();
        } else {
            try {
                t = (Collection) cls.newInstance();
            } catch (Exception e) {
                throw new d("create instane error, class " + cls.getName());
            }
        }
        cVar.a(type instanceof ParameterizedType ? ((ParameterizedType) type).getActualTypeArguments()[0] : Object.class, t, obj);
        return t;
    }
}
