package com.b.a.b;

import com.amap.api.search.route.Route;
import com.b.a.a;
import com.b.a.d;
import java.lang.ref.SoftReference;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;

public final class f {
    private static final ThreadLocal<SoftReference<char[]>> q = new ThreadLocal<>();
    private static boolean[] u;
    private static final char[] v = "\"@type\":\"".toCharArray();
    private static final int[] w = new int[103];
    boolean a;
    public int b;
    public final int c;
    public final int d;
    public final int e;
    private final char[] f;
    private int g;
    private final int h;
    private int i;
    private char j;
    private int k;
    private char[] l;
    private int m;
    private int n;
    private int o;
    private h p;
    private int r;
    private Calendar s;
    private boolean t;

    static {
        boolean[] zArr = new boolean[256];
        u = zArr;
        zArr[32] = true;
        u[10] = true;
        u[13] = true;
        u[9] = true;
        u[12] = true;
        u[8] = true;
        for (int i2 = 48; i2 <= 57; i2++) {
            w[i2] = i2 - 48;
        }
        for (int i3 = 97; i3 <= 102; i3++) {
            w[i3] = (i3 - 97) + 10;
        }
        for (int i4 = 65; i4 <= 70; i4++) {
            w[i4] = (i4 - 65) + 10;
        }
    }

    public f(String str) {
        this(str, a.a);
    }

    public f(String str, int i2) {
        this(str.toCharArray(), str.length(), i2);
    }

    private f(char[] cArr, int i2, int i3) {
        this.p = h.a;
        this.r = a.a;
        this.s = null;
        this.t = false;
        this.b = 0;
        this.c = 10;
        this.d = 19;
        this.e = 23;
        this.r = i3;
        SoftReference softReference = q.get();
        if (softReference != null) {
            this.l = (char[]) softReference.get();
            q.set(null);
        }
        if (this.l == null) {
            this.l = new char[64];
        }
        this.i = i2;
        if (i2 == cArr.length) {
            if (cArr.length <= 0 || !a(cArr[cArr.length - 1])) {
                char[] cArr2 = new char[(i2 + 1)];
                System.arraycopy(cArr, 0, cArr2, 0, cArr.length);
                cArr = cArr2;
            } else {
                i2--;
            }
        }
        this.f = cArr;
        this.h = i2;
        this.f[this.h] = 26;
        this.g = -1;
        char[] cArr3 = this.f;
        int i4 = this.g + 1;
        this.g = i4;
        this.j = cArr3[i4];
        if (this.j == 65279) {
            char[] cArr4 = this.f;
            int i5 = this.g + 1;
            this.g = i5;
            this.j = cArr4[i5];
        }
    }

    private void C() {
        this.n = this.g - 1;
        this.a = false;
        do {
            this.m++;
            char[] cArr = this.f;
            int i2 = this.g + 1;
            this.g = i2;
            this.j = cArr[i2];
        } while (Character.isLetterOrDigit(this.j));
        Integer a2 = this.p.a(q());
        if (a2 != null) {
            this.o = a2.intValue();
        } else {
            this.o = 18;
        }
    }

    private static boolean a(char c2) {
        return c2 == ' ' || c2 == 10 || c2 == 13 || c2 == 9 || c2 == 12 || c2 == 8;
    }

    private final void b(char c2) {
        if (this.m == this.l.length) {
            char[] cArr = new char[(this.l.length * 2)];
            System.arraycopy(this.l, 0, cArr, 0, this.l.length);
            this.l = cArr;
        }
        char[] cArr2 = this.l;
        int i2 = this.m;
        this.m = i2 + 1;
        cArr2[i2] = c2;
    }

    public final boolean A() {
        switch (this.o) {
            case 1:
            case Route.DrivingNoFastRoad:
            default:
                return false;
            case MultiThreadedHttpConnectionManager.DEFAULT_MAX_TOTAL_CONNECTIONS /*20*/:
                return true;
        }
    }

    public final void B() {
        if (this.l.length <= 8192) {
            q.set(new SoftReference(this.l));
        }
        this.l = null;
    }

    public final Number a(boolean z) {
        char c2 = this.f[(this.n + this.m) - 1];
        return c2 == 'F' ? Float.valueOf(Float.parseFloat(new String(this.f, this.n, this.m - 1))) : c2 == 'D' ? Double.valueOf(Double.parseDouble(new String(this.f, this.n, this.m - 1))) : z ? x() : Double.valueOf(Double.parseDouble(v()));
    }

    public final String a(k kVar) {
        boolean[] zArr = b.b;
        int i2 = this.j;
        if (!(this.j >= zArr.length || zArr[i2])) {
            throw new d("illegal identifier : " + this.j);
        }
        boolean[] zArr2 = b.c;
        this.n = this.g;
        this.m = 1;
        while (true) {
            char[] cArr = this.f;
            int i3 = this.g + 1;
            this.g = i3;
            char c2 = cArr[i3];
            if (c2 < zArr2.length && !zArr2[c2]) {
                break;
            }
            i2 = (i2 * 31) + c2;
            this.m++;
        }
        this.j = this.f[this.g];
        this.o = 18;
        if (this.m == 4 && i2 == 3392903 && this.f[this.n] == 'n' && this.f[this.n + 1] == 'u' && this.f[this.n + 2] == 'l' && this.f[this.n + 3] == 'l') {
            return null;
        }
        return kVar.a(this.f, this.n, this.m, i2);
    }

    public final String a(k kVar, char c2) {
        this.n = this.g;
        this.m = 0;
        boolean z = false;
        int i2 = 0;
        while (true) {
            char[] cArr = this.f;
            int i3 = this.g + 1;
            this.g = i3;
            char c3 = cArr[i3];
            if (c3 == c2) {
                this.o = 4;
                char[] cArr2 = this.f;
                int i4 = this.g + 1;
                this.g = i4;
                this.j = cArr2[i4];
                return !z ? kVar.a(this.f, this.n + 1, this.m, i2) : kVar.a(this.l, 0, this.m, i2);
            } else if (c3 == 26) {
                throw new d("unclosed.str");
            } else if (c3 == '\\') {
                if (!z) {
                    if (this.m >= this.l.length) {
                        int length = this.l.length * 2;
                        if (this.m > length) {
                            length = this.m;
                        }
                        char[] cArr3 = new char[length];
                        System.arraycopy(this.l, 0, cArr3, 0, this.l.length);
                        this.l = cArr3;
                    }
                    System.arraycopy(this.f, this.n + 1, this.l, 0, this.m);
                    z = true;
                }
                char[] cArr4 = this.f;
                int i5 = this.g + 1;
                this.g = i5;
                char c4 = cArr4[i5];
                switch (c4) {
                    case '\"':
                        i2 = (i2 * 31) + 34;
                        b('\"');
                        continue;
                    case '/':
                        i2 = (i2 * 31) + 47;
                        b('/');
                        continue;
                    case 'F':
                    case HttpStatus.SC_PROCESSING /*102*/:
                        i2 = (i2 * 31) + 12;
                        b(12);
                        continue;
                    case '\\':
                        i2 = (i2 * 31) + 92;
                        b('\\');
                        continue;
                    case 'b':
                        i2 = (i2 * 31) + 8;
                        b(8);
                        continue;
                    case 'n':
                        i2 = (i2 * 31) + 10;
                        b(10);
                        continue;
                    case 'r':
                        i2 = (i2 * 31) + 13;
                        b(13);
                        continue;
                    case 't':
                        i2 = (i2 * 31) + 9;
                        b(9);
                        continue;
                    case 'u':
                        char[] cArr5 = this.f;
                        int i6 = this.g + 1;
                        this.g = i6;
                        char c5 = cArr5[i6];
                        char[] cArr6 = this.f;
                        int i7 = this.g + 1;
                        this.g = i7;
                        char c6 = cArr6[i7];
                        char[] cArr7 = this.f;
                        int i8 = this.g + 1;
                        this.g = i8;
                        char c7 = cArr7[i8];
                        char[] cArr8 = this.f;
                        int i9 = this.g + 1;
                        this.g = i9;
                        int parseInt = Integer.parseInt(new String(new char[]{c5, c6, c7, cArr8[i9]}), 16);
                        i2 = (i2 * 31) + parseInt;
                        b((char) parseInt);
                        continue;
                    default:
                        this.j = c4;
                        throw new d("unclosed.str.lit");
                }
            } else {
                i2 = (i2 * 31) + c3;
                if (!z) {
                    this.m++;
                } else if (this.m == this.l.length) {
                    b(c3);
                } else {
                    char[] cArr9 = this.l;
                    int i10 = this.m;
                    this.m = i10 + 1;
                    cArr9[i10] = c3;
                }
            }
        }
    }

    public final void a(int i2) {
        while (this.j != ':') {
            if (a(this.j)) {
                char[] cArr = this.f;
                int i3 = this.g + 1;
                this.g = i3;
                this.j = cArr[i3];
            } else {
                throw new d("not match ':', actual " + this.j);
            }
        }
        char[] cArr2 = this.f;
        int i4 = this.g + 1;
        this.g = i4;
        this.j = cArr2[i4];
        while (true) {
            if (i2 == 2) {
                if (this.j >= '0' && this.j <= '9') {
                    this.m = 0;
                    this.k = this.g;
                    o();
                    return;
                } else if (this.j == '\"') {
                    this.m = 0;
                    this.k = this.g;
                    m();
                    return;
                }
            } else if (i2 == 4) {
                if (this.j == '\"') {
                    this.m = 0;
                    this.k = this.g;
                    m();
                    return;
                } else if (this.j >= '0' && this.j <= '9') {
                    this.m = 0;
                    this.k = this.g;
                    o();
                    return;
                }
            } else if (i2 == 12) {
                if (this.j == '{') {
                    this.o = 12;
                    char[] cArr3 = this.f;
                    int i5 = this.g + 1;
                    this.g = i5;
                    this.j = cArr3[i5];
                    return;
                } else if (this.j == '[') {
                    this.o = 14;
                    char[] cArr4 = this.f;
                    int i6 = this.g + 1;
                    this.g = i6;
                    this.j = cArr4[i6];
                    return;
                }
            } else if (i2 == 14) {
                if (this.j == '[') {
                    this.o = 14;
                    char[] cArr5 = this.f;
                    int i7 = this.g + 1;
                    this.g = i7;
                    this.j = cArr5[i7];
                    return;
                } else if (this.j == '{') {
                    this.o = 12;
                    char[] cArr6 = this.f;
                    int i8 = this.g + 1;
                    this.g = i8;
                    this.j = cArr6[i8];
                    return;
                }
            }
            if (a(this.j)) {
                char[] cArr7 = this.f;
                int i9 = this.g + 1;
                this.g = i9;
                this.j = cArr7[i9];
            } else {
                l();
                return;
            }
        }
    }

    public final boolean a() {
        return this.t;
    }

    public final boolean a(e eVar) {
        return e.a(this.r, eVar);
    }

    public final String b(k kVar) {
        g();
        if (this.j == '\"') {
            return a(kVar, '\"');
        }
        if (this.j == '\'') {
            if (a(e.AllowSingleQuotes)) {
                return a(kVar, '\'');
            }
            throw new d("syntax error");
        } else if (this.j == '}') {
            char[] cArr = this.f;
            int i2 = this.g + 1;
            this.g = i2;
            this.j = cArr[i2];
            this.o = 13;
            return null;
        } else if (this.j == ',') {
            char[] cArr2 = this.f;
            int i3 = this.g + 1;
            this.g = i3;
            this.j = cArr2[i3];
            this.o = 16;
            return null;
        } else if (this.j == 26) {
            this.o = 20;
            return null;
        } else if (a(e.AllowUnQuotedFieldNames)) {
            return a(kVar);
        } else {
            throw new d("syntax error");
        }
    }

    public final void b() {
        this.t = false;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0196 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(int r8) {
        /*
            r7 = this;
            r6 = 123(0x7b, float:1.72E-43)
            r5 = 91
            r4 = 14
            r3 = 0
            r2 = 12
        L_0x0009:
            switch(r8) {
                case 2: goto L_0x00b1;
                case 4: goto L_0x0100;
                case 12: goto L_0x003b;
                case 14: goto L_0x0150;
                case 15: goto L_0x0178;
                case 16: goto L_0x0061;
                case 20: goto L_0x0190;
                default: goto L_0x000c;
            }
        L_0x000c:
            char r0 = r7.j
            r1 = 32
            if (r0 == r1) goto L_0x002e
            char r0 = r7.j
            r1 = 10
            if (r0 == r1) goto L_0x002e
            char r0 = r7.j
            r1 = 13
            if (r0 == r1) goto L_0x002e
            char r0 = r7.j
            r1 = 9
            if (r0 == r1) goto L_0x002e
            char r0 = r7.j
            if (r0 == r2) goto L_0x002e
            char r0 = r7.j
            r1 = 8
            if (r0 != r1) goto L_0x019c
        L_0x002e:
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x0009
        L_0x003b:
            char r0 = r7.j
            if (r0 != r6) goto L_0x004e
            r7.o = r2
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
        L_0x004d:
            return
        L_0x004e:
            char r0 = r7.j
            if (r0 != r5) goto L_0x000c
            r7.o = r4
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x0061:
            char r0 = r7.j
            r1 = 44
            if (r0 != r1) goto L_0x0078
            r0 = 16
            r7.o = r0
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x0078:
            char r0 = r7.j
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x008f
            r0 = 13
            r7.o = r0
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x008f:
            char r0 = r7.j
            r1 = 93
            if (r0 != r1) goto L_0x00a6
            r0 = 15
            r7.o = r0
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x00a6:
            char r0 = r7.j
            r1 = 26
            if (r0 != r1) goto L_0x000c
            r0 = 20
            r7.o = r0
            goto L_0x004d
        L_0x00b1:
            char r0 = r7.j
            r1 = 48
            if (r0 < r1) goto L_0x00c7
            char r0 = r7.j
            r1 = 57
            if (r0 > r1) goto L_0x00c7
            r7.m = r3
            int r0 = r7.g
            r7.k = r0
            r7.o()
            goto L_0x004d
        L_0x00c7:
            char r0 = r7.j
            r1 = 34
            if (r0 != r1) goto L_0x00d8
            r7.m = r3
            int r0 = r7.g
            r7.k = r0
            r7.m()
            goto L_0x004d
        L_0x00d8:
            char r0 = r7.j
            if (r0 != r5) goto L_0x00ec
            r7.o = r4
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x00ec:
            char r0 = r7.j
            if (r0 != r6) goto L_0x000c
            r7.o = r2
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x0100:
            char r0 = r7.j
            r1 = 34
            if (r0 != r1) goto L_0x0111
            r7.m = r3
            int r0 = r7.g
            r7.k = r0
            r7.m()
            goto L_0x004d
        L_0x0111:
            char r0 = r7.j
            r1 = 48
            if (r0 < r1) goto L_0x0128
            char r0 = r7.j
            r1 = 57
            if (r0 > r1) goto L_0x0128
            r7.m = r3
            int r0 = r7.g
            r7.k = r0
            r7.o()
            goto L_0x004d
        L_0x0128:
            char r0 = r7.j
            if (r0 != r5) goto L_0x013c
            r7.o = r4
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x013c:
            char r0 = r7.j
            if (r0 != r6) goto L_0x000c
            r7.o = r2
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x0150:
            char r0 = r7.j
            if (r0 != r5) goto L_0x0164
            r7.o = r4
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x0164:
            char r0 = r7.j
            if (r0 != r6) goto L_0x000c
            r7.o = r2
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x0178:
            char r0 = r7.j
            r1 = 93
            if (r0 != r1) goto L_0x0190
            r0 = 15
            r7.o = r0
            char[] r0 = r7.f
            int r1 = r7.g
            int r1 = r1 + 1
            r7.g = r1
            char r0 = r0[r1]
            r7.j = r0
            goto L_0x004d
        L_0x0190:
            char r0 = r7.j
            r1 = 26
            if (r0 != r1) goto L_0x000c
            r0 = 20
            r7.o = r0
            goto L_0x004d
        L_0x019c:
            r7.l()
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.f.b(int):void");
    }

    public final int c() {
        return this.g;
    }

    public final String c(k kVar) {
        return kVar == null ? !this.a ? new String(this.f, this.n + 1, this.m) : new String(this.l, 0, this.m) : !this.a ? kVar.a(this.f, this.n + 1, this.m) : kVar.a(this.l, 0, this.m);
    }

    public final boolean d() {
        for (int i2 = 0; i2 < this.h; i2++) {
            if (!a(this.f[i2])) {
                return false;
            }
        }
        return true;
    }

    public final int e() {
        return this.o;
    }

    public final String f() {
        return g.a(this.o);
    }

    public final void g() {
        while (u[this.j]) {
            char[] cArr = this.f;
            int i2 = this.g + 1;
            this.g = i2;
            this.j = cArr[i2];
        }
    }

    public final char h() {
        return this.j;
    }

    public final void i() {
        while (this.j != ':') {
            if (this.j == ' ' || this.j == 10 || this.j == 13 || this.j == 9 || this.j == 12 || this.j == 8) {
                char[] cArr = this.f;
                int i2 = this.g + 1;
                this.g = i2;
                this.j = cArr[i2];
            } else {
                throw new d("not match ':' - " + this.j);
            }
        }
        char[] cArr2 = this.f;
        int i3 = this.g + 1;
        this.g = i3;
        this.j = cArr2[i3];
        l();
    }

    public final void j() {
        char[] cArr = this.f;
        int i2 = this.g + 1;
        this.g = i2;
        this.j = cArr[i2];
    }

    public final void k() {
        this.m = 0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x0017 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void l() {
        /*
            r12 = this;
            r11 = 13
            r10 = 12
            r9 = 10
            r8 = 9
            r7 = 8
            r0 = 0
            r12.m = r0
        L_0x000d:
            int r0 = r12.g
            r12.k = r0
            char r0 = r12.j
            r1 = 34
            if (r0 != r1) goto L_0x001b
            r12.m()
        L_0x001a:
            return
        L_0x001b:
            char r0 = r12.j
            r1 = 44
            if (r0 != r1) goto L_0x0032
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            r0 = 16
            r12.o = r0
            goto L_0x001a
        L_0x0032:
            char r0 = r12.j
            r1 = 48
            if (r0 < r1) goto L_0x0042
            char r0 = r12.j
            r1 = 57
            if (r0 > r1) goto L_0x0042
            r12.o()
            goto L_0x001a
        L_0x0042:
            char r0 = r12.j
            r1 = 45
            if (r0 != r1) goto L_0x004c
            r12.o()
            goto L_0x001a
        L_0x004c:
            char r0 = r12.j
            switch(r0) {
                case 8: goto L_0x019a;
                case 9: goto L_0x019a;
                case 10: goto L_0x019a;
                case 12: goto L_0x019a;
                case 13: goto L_0x019a;
                case 32: goto L_0x019a;
                case 39: goto L_0x0073;
                case 40: goto L_0x0561;
                case 41: goto L_0x0571;
                case 58: goto L_0x05c7;
                case 68: goto L_0x055c;
                case 83: goto L_0x031d;
                case 84: goto L_0x0247;
                case 91: goto L_0x0583;
                case 93: goto L_0x0595;
                case 102: goto L_0x039b;
                case 110: goto L_0x0450;
                case 116: goto L_0x01a8;
                case 123: goto L_0x05a7;
                case 125: goto L_0x05b7;
                default: goto L_0x0051;
            }
        L_0x0051:
            int r0 = r12.g
            int r1 = r12.h
            if (r0 == r1) goto L_0x0065
            char r0 = r12.j
            r1 = 26
            if (r0 != r1) goto L_0x05e5
            int r0 = r12.g
            int r0 = r0 + 1
            int r1 = r12.h
            if (r0 != r1) goto L_0x05e5
        L_0x0065:
            int r0 = r12.o
            r1 = 20
            if (r0 != r1) goto L_0x05d9
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "EOF error"
            r0.<init>(r1)
            throw r0
        L_0x0073:
            com.b.a.b.e r0 = com.b.a.b.e.AllowSingleQuotes
            boolean r0 = r12.a(r0)
            if (r0 != 0) goto L_0x0083
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "Feature.AllowSingleQuotes is false"
            r0.<init>(r1)
            throw r0
        L_0x0083:
            int r0 = r12.g
            r12.n = r0
            r0 = 0
            r12.a = r0
        L_0x008a:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r1 = 39
            if (r0 == r1) goto L_0x0189
            r1 = 26
            if (r0 != r1) goto L_0x00a4
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "unclosed single-quote string"
            r0.<init>(r1)
            throw r0
        L_0x00a4:
            r1 = 92
            if (r0 != r1) goto L_0x0165
            boolean r0 = r12.a
            if (r0 != 0) goto L_0x00d6
            r0 = 1
            r12.a = r0
            int r0 = r12.m
            char[] r1 = r12.l
            int r1 = r1.length
            if (r0 <= r1) goto L_0x00c8
            int r0 = r12.m
            int r0 = r0 * 2
            char[] r0 = new char[r0]
            char[] r1 = r12.l
            r2 = 0
            r3 = 0
            char[] r4 = r12.l
            int r4 = r4.length
            java.lang.System.arraycopy(r1, r2, r0, r3, r4)
            r12.l = r0
        L_0x00c8:
            char[] r0 = r12.f
            int r1 = r12.n
            int r1 = r1 + 1
            char[] r2 = r12.l
            r3 = 0
            int r4 = r12.m
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
        L_0x00d6:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            switch(r0) {
                case 34: goto L_0x00ed;
                case 39: goto L_0x00ff;
                case 47: goto L_0x00f9;
                case 70: goto L_0x0109;
                case 92: goto L_0x00f3;
                case 98: goto L_0x0105;
                case 102: goto L_0x0109;
                case 110: goto L_0x010e;
                case 114: goto L_0x0113;
                case 116: goto L_0x0118;
                case 117: goto L_0x011d;
                default: goto L_0x00e3;
            }
        L_0x00e3:
            r12.j = r0
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "unclosed single-quote string"
            r0.<init>(r1)
            throw r0
        L_0x00ed:
            r0 = 34
            r12.b(r0)
            goto L_0x008a
        L_0x00f3:
            r0 = 92
            r12.b(r0)
            goto L_0x008a
        L_0x00f9:
            r0 = 47
            r12.b(r0)
            goto L_0x008a
        L_0x00ff:
            r0 = 39
            r12.b(r0)
            goto L_0x008a
        L_0x0105:
            r12.b(r7)
            goto L_0x008a
        L_0x0109:
            r12.b(r10)
            goto L_0x008a
        L_0x010e:
            r12.b(r9)
            goto L_0x008a
        L_0x0113:
            r12.b(r11)
            goto L_0x008a
        L_0x0118:
            r12.b(r8)
            goto L_0x008a
        L_0x011d:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            char[] r1 = r12.f
            int r2 = r12.g
            int r2 = r2 + 1
            r12.g = r2
            char r1 = r1[r2]
            char[] r2 = r12.f
            int r3 = r12.g
            int r3 = r3 + 1
            r12.g = r3
            char r2 = r2[r3]
            char[] r3 = r12.f
            int r4 = r12.g
            int r4 = r4 + 1
            r12.g = r4
            char r3 = r3[r4]
            java.lang.String r4 = new java.lang.String
            r5 = 4
            char[] r5 = new char[r5]
            r6 = 0
            r5[r6] = r0
            r0 = 1
            r5[r0] = r1
            r0 = 2
            r5[r0] = r2
            r0 = 3
            r5[r0] = r3
            r4.<init>(r5)
            r0 = 16
            int r0 = java.lang.Integer.parseInt(r4, r0)
            char r0 = (char) r0
            r12.b(r0)
            goto L_0x008a
        L_0x0165:
            boolean r1 = r12.a
            if (r1 != 0) goto L_0x0171
            int r0 = r12.m
            int r0 = r0 + 1
            r12.m = r0
            goto L_0x008a
        L_0x0171:
            int r1 = r12.m
            char[] r2 = r12.l
            int r2 = r2.length
            if (r1 != r2) goto L_0x017d
            r12.b(r0)
            goto L_0x008a
        L_0x017d:
            char[] r1 = r12.l
            int r2 = r12.m
            int r3 = r2 + 1
            r12.m = r3
            r1[r2] = r0
            goto L_0x008a
        L_0x0189:
            r0 = 4
            r12.o = r0
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            goto L_0x001a
        L_0x019a:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            goto L_0x000d
        L_0x01a8:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 116(0x74, float:1.63E-43)
            if (r0 == r1) goto L_0x01be
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x01be:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 114(0x72, float:1.6E-43)
            if (r0 == r1) goto L_0x01d4
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x01d4:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 117(0x75, float:1.64E-43)
            if (r0 == r1) goto L_0x01ea
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x01ea:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 101(0x65, float:1.42E-43)
            if (r0 == r1) goto L_0x0200
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x0200:
            char[] r0 = r12.f
            int r1 = r12.g
            char r0 = r0[r1]
            r12.j = r0
            char r0 = r12.j
            r1 = 32
            if (r0 == r1) goto L_0x023a
            char r0 = r12.j
            r1 = 44
            if (r0 == r1) goto L_0x023a
            char r0 = r12.j
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 == r1) goto L_0x023a
            char r0 = r12.j
            r1 = 93
            if (r0 == r1) goto L_0x023a
            char r0 = r12.j
            if (r0 == r9) goto L_0x023a
            char r0 = r12.j
            if (r0 == r11) goto L_0x023a
            char r0 = r12.j
            if (r0 == r8) goto L_0x023a
            char r0 = r12.j
            r1 = 26
            if (r0 == r1) goto L_0x023a
            char r0 = r12.j
            if (r0 == r10) goto L_0x023a
            char r0 = r12.j
            if (r0 != r7) goto L_0x023f
        L_0x023a:
            r0 = 6
            r12.o = r0
            goto L_0x001a
        L_0x023f:
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "scan true error"
            r0.<init>(r1)
            throw r0
        L_0x0247:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 84
            if (r0 == r1) goto L_0x025d
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x025d:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 114(0x72, float:1.6E-43)
            if (r0 == r1) goto L_0x0273
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x0273:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 101(0x65, float:1.42E-43)
            if (r0 == r1) goto L_0x0289
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x0289:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 101(0x65, float:1.42E-43)
            if (r0 == r1) goto L_0x029f
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x029f:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 83
            if (r0 == r1) goto L_0x02b5
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x02b5:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 101(0x65, float:1.42E-43)
            if (r0 == r1) goto L_0x02cb
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x02cb:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 116(0x74, float:1.63E-43)
            if (r0 == r1) goto L_0x02e1
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x02e1:
            char[] r0 = r12.f
            int r1 = r12.g
            char r0 = r0[r1]
            r12.j = r0
            char r0 = r12.j
            r1 = 32
            if (r0 == r1) goto L_0x030f
            char r0 = r12.j
            if (r0 == r9) goto L_0x030f
            char r0 = r12.j
            if (r0 == r11) goto L_0x030f
            char r0 = r12.j
            if (r0 == r8) goto L_0x030f
            char r0 = r12.j
            if (r0 == r10) goto L_0x030f
            char r0 = r12.j
            if (r0 == r7) goto L_0x030f
            char r0 = r12.j
            r1 = 91
            if (r0 == r1) goto L_0x030f
            char r0 = r12.j
            r1 = 40
            if (r0 != r1) goto L_0x0315
        L_0x030f:
            r0 = 22
            r12.o = r0
            goto L_0x001a
        L_0x0315:
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "scan set error"
            r0.<init>(r1)
            throw r0
        L_0x031d:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 83
            if (r0 == r1) goto L_0x0333
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x0333:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 101(0x65, float:1.42E-43)
            if (r0 == r1) goto L_0x0349
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x0349:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 116(0x74, float:1.63E-43)
            if (r0 == r1) goto L_0x035f
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x035f:
            char[] r0 = r12.f
            int r1 = r12.g
            char r0 = r0[r1]
            r12.j = r0
            char r0 = r12.j
            r1 = 32
            if (r0 == r1) goto L_0x038d
            char r0 = r12.j
            if (r0 == r9) goto L_0x038d
            char r0 = r12.j
            if (r0 == r11) goto L_0x038d
            char r0 = r12.j
            if (r0 == r8) goto L_0x038d
            char r0 = r12.j
            if (r0 == r10) goto L_0x038d
            char r0 = r12.j
            if (r0 == r7) goto L_0x038d
            char r0 = r12.j
            r1 = 91
            if (r0 == r1) goto L_0x038d
            char r0 = r12.j
            r1 = 40
            if (r0 != r1) goto L_0x0393
        L_0x038d:
            r0 = 21
            r12.o = r0
            goto L_0x001a
        L_0x0393:
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "scan set error"
            r0.<init>(r1)
            throw r0
        L_0x039b:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 102(0x66, float:1.43E-43)
            if (r0 == r1) goto L_0x03b1
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse false"
            r0.<init>(r1)
            throw r0
        L_0x03b1:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 97
            if (r0 == r1) goto L_0x03c7
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse false"
            r0.<init>(r1)
            throw r0
        L_0x03c7:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 108(0x6c, float:1.51E-43)
            if (r0 == r1) goto L_0x03dd
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse false"
            r0.<init>(r1)
            throw r0
        L_0x03dd:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 115(0x73, float:1.61E-43)
            if (r0 == r1) goto L_0x03f3
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse false"
            r0.<init>(r1)
            throw r0
        L_0x03f3:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 101(0x65, float:1.42E-43)
            if (r0 == r1) goto L_0x0409
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse false"
            r0.<init>(r1)
            throw r0
        L_0x0409:
            char[] r0 = r12.f
            int r1 = r12.g
            char r0 = r0[r1]
            r12.j = r0
            char r0 = r12.j
            r1 = 32
            if (r0 == r1) goto L_0x0443
            char r0 = r12.j
            r1 = 44
            if (r0 == r1) goto L_0x0443
            char r0 = r12.j
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 == r1) goto L_0x0443
            char r0 = r12.j
            r1 = 93
            if (r0 == r1) goto L_0x0443
            char r0 = r12.j
            if (r0 == r9) goto L_0x0443
            char r0 = r12.j
            if (r0 == r11) goto L_0x0443
            char r0 = r12.j
            if (r0 == r8) goto L_0x0443
            char r0 = r12.j
            r1 = 26
            if (r0 == r1) goto L_0x0443
            char r0 = r12.j
            if (r0 == r10) goto L_0x0443
            char r0 = r12.j
            if (r0 != r7) goto L_0x0448
        L_0x0443:
            r0 = 7
            r12.o = r0
            goto L_0x001a
        L_0x0448:
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "scan false error"
            r0.<init>(r1)
            throw r0
        L_0x0450:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 110(0x6e, float:1.54E-43)
            if (r0 == r1) goto L_0x0466
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse null or new"
            r0.<init>(r1)
            throw r0
        L_0x0466:
            char[] r0 = r12.f
            int r1 = r12.g
            char r0 = r0[r1]
            r1 = 117(0x75, float:1.64E-43)
            if (r0 != r1) goto L_0x04e8
            int r0 = r12.g
            int r0 = r0 + 1
            r12.g = r0
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 108(0x6c, float:1.51E-43)
            if (r0 == r1) goto L_0x048c
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x048c:
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 108(0x6c, float:1.51E-43)
            if (r0 == r1) goto L_0x04a2
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse true"
            r0.<init>(r1)
            throw r0
        L_0x04a2:
            char[] r0 = r12.f
            int r1 = r12.g
            char r0 = r0[r1]
            r12.j = r0
            char r0 = r12.j
            r1 = 32
            if (r0 == r1) goto L_0x04dc
            char r0 = r12.j
            r1 = 44
            if (r0 == r1) goto L_0x04dc
            char r0 = r12.j
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 == r1) goto L_0x04dc
            char r0 = r12.j
            r1 = 93
            if (r0 == r1) goto L_0x04dc
            char r0 = r12.j
            if (r0 == r9) goto L_0x04dc
            char r0 = r12.j
            if (r0 == r11) goto L_0x04dc
            char r0 = r12.j
            if (r0 == r8) goto L_0x04dc
            char r0 = r12.j
            r1 = 26
            if (r0 == r1) goto L_0x04dc
            char r0 = r12.j
            if (r0 == r10) goto L_0x04dc
            char r0 = r12.j
            if (r0 != r7) goto L_0x04e0
        L_0x04dc:
            r12.o = r7
            goto L_0x001a
        L_0x04e0:
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "scan true error"
            r0.<init>(r1)
            throw r0
        L_0x04e8:
            char[] r0 = r12.f
            int r1 = r12.g
            char r0 = r0[r1]
            r1 = 101(0x65, float:1.42E-43)
            if (r0 == r1) goto L_0x04fa
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse e"
            r0.<init>(r1)
            throw r0
        L_0x04fa:
            int r0 = r12.g
            int r0 = r0 + 1
            r12.g = r0
            char[] r0 = r12.f
            int r1 = r12.g
            int r2 = r1 + 1
            r12.g = r2
            char r0 = r0[r1]
            r1 = 119(0x77, float:1.67E-43)
            if (r0 == r1) goto L_0x0516
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "error parse w"
            r0.<init>(r1)
            throw r0
        L_0x0516:
            char[] r0 = r12.f
            int r1 = r12.g
            char r0 = r0[r1]
            r12.j = r0
            char r0 = r12.j
            r1 = 32
            if (r0 == r1) goto L_0x0550
            char r0 = r12.j
            r1 = 44
            if (r0 == r1) goto L_0x0550
            char r0 = r12.j
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 == r1) goto L_0x0550
            char r0 = r12.j
            r1 = 93
            if (r0 == r1) goto L_0x0550
            char r0 = r12.j
            if (r0 == r9) goto L_0x0550
            char r0 = r12.j
            if (r0 == r11) goto L_0x0550
            char r0 = r12.j
            if (r0 == r8) goto L_0x0550
            char r0 = r12.j
            r1 = 26
            if (r0 == r1) goto L_0x0550
            char r0 = r12.j
            if (r0 == r10) goto L_0x0550
            char r0 = r12.j
            if (r0 != r7) goto L_0x0554
        L_0x0550:
            r12.o = r8
            goto L_0x001a
        L_0x0554:
            com.b.a.d r0 = new com.b.a.d
            java.lang.String r1 = "scan true error"
            r0.<init>(r1)
            throw r0
        L_0x055c:
            r12.C()
            goto L_0x001a
        L_0x0561:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            r12.o = r9
            goto L_0x001a
        L_0x0571:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            r0 = 11
            r12.o = r0
            goto L_0x001a
        L_0x0583:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            r0 = 14
            r12.o = r0
            goto L_0x001a
        L_0x0595:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            r0 = 15
            r12.o = r0
            goto L_0x001a
        L_0x05a7:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            r12.o = r10
            goto L_0x001a
        L_0x05b7:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            r12.o = r11
            goto L_0x001a
        L_0x05c7:
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            r0 = 17
            r12.o = r0
            goto L_0x001a
        L_0x05d9:
            r0 = 20
            r12.o = r0
            int r0 = r12.i
            r12.g = r0
            r12.k = r0
            goto L_0x001a
        L_0x05e5:
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r1 = 0
            char r2 = r12.j
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r0[r1] = r2
            r0 = 1
            r12.o = r0
            char[] r0 = r12.f
            int r1 = r12.g
            int r1 = r1 + 1
            r12.g = r1
            char r0 = r0[r1]
            r12.j = r0
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.f.l():void");
    }

    public final void m() {
        this.n = this.g;
        this.a = false;
        while (true) {
            char[] cArr = this.f;
            int i2 = this.g + 1;
            this.g = i2;
            char c2 = cArr[i2];
            if (c2 == '\"') {
                this.o = 4;
                char[] cArr2 = this.f;
                int i3 = this.g + 1;
                this.g = i3;
                this.j = cArr2[i3];
                return;
            } else if (c2 == '\\') {
                if (!this.a) {
                    this.a = true;
                    if (this.m >= this.l.length) {
                        int length = this.l.length * 2;
                        if (this.m > length) {
                            length = this.m;
                        }
                        char[] cArr3 = new char[length];
                        System.arraycopy(this.l, 0, cArr3, 0, this.l.length);
                        this.l = cArr3;
                    }
                    System.arraycopy(this.f, this.n + 1, this.l, 0, this.m);
                }
                char[] cArr4 = this.f;
                int i4 = this.g + 1;
                this.g = i4;
                char c3 = cArr4[i4];
                switch (c3) {
                    case '\"':
                        b('\"');
                        continue;
                    case '/':
                        b('/');
                        continue;
                    case 'F':
                    case HttpStatus.SC_PROCESSING /*102*/:
                        b(12);
                        continue;
                    case '\\':
                        b('\\');
                        continue;
                    case 'b':
                        b(8);
                        continue;
                    case 'n':
                        b(10);
                        continue;
                    case 'r':
                        b(13);
                        continue;
                    case 't':
                        b(9);
                        continue;
                    case 'u':
                        char[] cArr5 = this.f;
                        int i5 = this.g + 1;
                        this.g = i5;
                        char c4 = cArr5[i5];
                        char[] cArr6 = this.f;
                        int i6 = this.g + 1;
                        this.g = i6;
                        char c5 = cArr6[i6];
                        char[] cArr7 = this.f;
                        int i7 = this.g + 1;
                        this.g = i7;
                        char c6 = cArr7[i7];
                        char[] cArr8 = this.f;
                        int i8 = this.g + 1;
                        this.g = i8;
                        b((char) Integer.parseInt(new String(new char[]{c4, c5, c6, cArr8[i8]}), 16));
                        continue;
                    case 'x':
                        char[] cArr9 = this.f;
                        int i9 = this.g + 1;
                        this.g = i9;
                        char c7 = cArr9[i9];
                        char[] cArr10 = this.f;
                        int i10 = this.g + 1;
                        this.g = i10;
                        b((char) ((w[c7] * 16) + w[cArr10[i10]]));
                        continue;
                    default:
                        this.j = c3;
                        throw new d("unclosed string : " + c3);
                }
            } else if (!this.a) {
                this.m++;
            } else if (this.m == this.l.length) {
                b(c2);
            } else {
                char[] cArr11 = this.l;
                int i11 = this.m;
                this.m = i11 + 1;
                cArr11[i11] = c2;
            }
        }
    }

    public final byte[] n() {
        return com.b.a.d.a.a(this.f, this.n + 1, this.m);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x010e, code lost:
        if (r8.j != '-') goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0132, code lost:
        if (r8.j != 'F') goto L_0x00db;
     */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x012e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void o() {
        /*
            r8 = this;
            r7 = 68
            r6 = 45
            r1 = 1
            r5 = 57
            r4 = 48
            int r0 = r8.g
            r8.n = r0
            char r0 = r8.j
            if (r0 != r6) goto L_0x0023
            int r0 = r8.m
            int r0 = r0 + 1
            r8.m = r0
            char[] r0 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r0 = r0[r2]
            r8.j = r0
        L_0x0023:
            char r0 = r8.j
            if (r0 < r4) goto L_0x003e
            char r0 = r8.j
            if (r0 > r5) goto L_0x003e
            int r0 = r8.m
            int r0 = r0 + 1
            r8.m = r0
            char[] r0 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r0 = r0[r2]
            r8.j = r0
            goto L_0x0023
        L_0x003e:
            r0 = 0
            char r2 = r8.j
            r3 = 46
            if (r2 != r3) goto L_0x0073
            int r0 = r8.m
            int r0 = r0 + 1
            r8.m = r0
            char[] r0 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r0 = r0[r2]
            r8.j = r0
        L_0x0057:
            char r0 = r8.j
            if (r0 < r4) goto L_0x0072
            char r0 = r8.j
            if (r0 > r5) goto L_0x0072
            int r0 = r8.m
            int r0 = r0 + 1
            r8.m = r0
            char[] r0 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r0 = r0[r2]
            r8.j = r0
            goto L_0x0057
        L_0x0072:
            r0 = r1
        L_0x0073:
            char r2 = r8.j
            r3 = 76
            if (r2 != r3) goto L_0x0091
            int r1 = r8.m
            int r1 = r1 + 1
            r8.m = r1
            char[] r1 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r1 = r1[r2]
            r8.j = r1
        L_0x008b:
            if (r0 == 0) goto L_0x0135
            r0 = 3
            r8.o = r0
        L_0x0090:
            return
        L_0x0091:
            char r2 = r8.j
            r3 = 83
            if (r2 != r3) goto L_0x00aa
            int r1 = r8.m
            int r1 = r1 + 1
            r8.m = r1
            char[] r1 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r1 = r1[r2]
            r8.j = r1
            goto L_0x008b
        L_0x00aa:
            char r2 = r8.j
            r3 = 66
            if (r2 != r3) goto L_0x00c3
            int r1 = r8.m
            int r1 = r1 + 1
            r8.m = r1
            char[] r1 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r1 = r1[r2]
            r8.j = r1
            goto L_0x008b
        L_0x00c3:
            char r2 = r8.j
            r3 = 70
            if (r2 != r3) goto L_0x00dd
            int r0 = r8.m
            int r0 = r0 + 1
            r8.m = r0
        L_0x00cf:
            char[] r0 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r0 = r0[r2]
            r8.j = r0
        L_0x00db:
            r0 = r1
            goto L_0x008b
        L_0x00dd:
            char r2 = r8.j
            if (r2 != r7) goto L_0x00e8
            int r0 = r8.m
            int r0 = r0 + 1
            r8.m = r0
            goto L_0x00cf
        L_0x00e8:
            char r2 = r8.j
            r3 = 101(0x65, float:1.42E-43)
            if (r2 == r3) goto L_0x00f4
            char r2 = r8.j
            r3 = 69
            if (r2 != r3) goto L_0x008b
        L_0x00f4:
            int r0 = r8.m
            int r0 = r0 + 1
            r8.m = r0
            char[] r0 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r0 = r0[r2]
            r8.j = r0
            char r0 = r8.j
            r2 = 43
            if (r0 == r2) goto L_0x0110
            char r0 = r8.j
            if (r0 != r6) goto L_0x0122
        L_0x0110:
            int r0 = r8.m
            int r0 = r0 + 1
            r8.m = r0
            char[] r0 = r8.f
            int r2 = r8.g
            int r2 = r2 + 1
            r8.g = r2
            char r0 = r0[r2]
            r8.j = r0
        L_0x0122:
            char r0 = r8.j
            if (r0 < r4) goto L_0x012a
            char r0 = r8.j
            if (r0 <= r5) goto L_0x0110
        L_0x012a:
            char r0 = r8.j
            if (r0 == r7) goto L_0x00cf
            char r0 = r8.j
            r2 = 70
            if (r0 != r2) goto L_0x00db
            goto L_0x00cf
        L_0x0135:
            r0 = 2
            r8.o = r0
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.f.o():void");
    }

    public final int p() {
        return this.k;
    }

    public final String q() {
        return !this.a ? new String(this.f, this.n + 1, this.m) : new String(this.l, 0, this.m);
    }

    public final boolean r() {
        return !this.a && this.m == 4 && this.f[this.n + 1] == '$' && this.f[this.n + 2] == 'r' && this.f[this.n + 3] == 'e' && this.f[this.n + 4] == 'f';
    }

    public final Number s() {
        boolean z;
        long j2;
        int i2;
        int i3;
        long j3 = 0;
        int i4 = this.n;
        int i5 = this.m + this.n;
        char c2 = ' ';
        if (i5 > 0) {
            switch (this.f[i5 - 1]) {
                case 'B':
                    i5--;
                    c2 = 'B';
                    break;
                case com.umeng.common.b.d.b /*76*/:
                    i5--;
                    c2 = 'L';
                    break;
                case 'S':
                    i5--;
                    c2 = 'S';
                    break;
            }
        }
        if (this.f[this.n] == '-') {
            z = true;
            int i6 = i4 + 1;
            j2 = Long.MIN_VALUE;
            i2 = i6;
        } else {
            z = false;
            int i7 = i4;
            j2 = -9223372036854775807L;
            i2 = i7;
        }
        if (i2 < i5) {
            i3 = i2 + 1;
            j3 = (long) (-w[this.f[i2]]);
        } else {
            i3 = i2;
        }
        while (i3 < i5) {
            int i8 = i3 + 1;
            int i9 = w[this.f[i3]];
            if (j3 < -922337203685477580L) {
                return new BigInteger(v());
            }
            long j4 = j3 * 10;
            if (j4 < ((long) i9) + j2) {
                return new BigInteger(v());
            }
            j3 = j4 - ((long) i9);
            i3 = i8;
        }
        if (!z) {
            long j5 = -j3;
            return (j5 > 2147483647L || c2 == 'L') ? Long.valueOf(j5) : c2 == 'S' ? Short.valueOf((short) ((int) j5)) : c2 == 'B' ? Byte.valueOf((byte) ((int) j5)) : Integer.valueOf((int) j5);
        } else if (i3 > this.n + 1) {
            return (j3 < -2147483648L || c2 == 'L') ? Long.valueOf(j3) : c2 == 'S' ? Short.valueOf((short) ((int) j3)) : c2 == 'B' ? Byte.valueOf((byte) ((int) j3)) : Integer.valueOf((int) j3);
        } else {
            throw new NumberFormatException(v());
        }
    }

    public final long t() {
        boolean z;
        long j2;
        int i2;
        int i3;
        int i4;
        long j3 = 0;
        int i5 = this.n;
        int i6 = this.n + this.m;
        if (this.f[this.n] == '-') {
            z = true;
            int i7 = i5 + 1;
            j2 = Long.MIN_VALUE;
            i2 = i7;
        } else {
            z = false;
            int i8 = i5;
            j2 = -9223372036854775807L;
            i2 = i8;
        }
        if (i2 < i6) {
            i3 = i2 + 1;
            j3 = (long) (-w[this.f[i2]]);
        } else {
            i3 = i2;
        }
        while (true) {
            if (i3 >= i6) {
                i4 = i3;
                break;
            }
            i4 = i3 + 1;
            char c2 = this.f[i3];
            if (c2 == 'L' || c2 == 'S' || c2 == 'B') {
                break;
            }
            int i9 = w[c2];
            if (j3 < -922337203685477580L) {
                throw new NumberFormatException(v());
            }
            long j4 = j3 * 10;
            if (j4 < ((long) i9) + j2) {
                throw new NumberFormatException(v());
            }
            j3 = j4 - ((long) i9);
            i3 = i4;
        }
        if (!z) {
            return -j3;
        }
        if (i4 > this.n + 1) {
            return j3;
        }
        throw new NumberFormatException(v());
    }

    public final int u() {
        int i2;
        boolean z;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int i7 = this.n;
        int i8 = this.n + this.m;
        if (this.f[this.n] == '-') {
            z = true;
            i2 = Integer.MIN_VALUE;
            i3 = i7 + 1;
        } else {
            i2 = -2147483647;
            z = false;
            i3 = i7;
        }
        if (i3 < i8) {
            i4 = i3 + 1;
            i6 = -w[this.f[i3]];
        } else {
            i4 = i3;
        }
        while (true) {
            if (i4 >= i8) {
                i5 = i4;
                break;
            }
            i5 = i4 + 1;
            char c2 = this.f[i4];
            if (c2 == 'L' || c2 == 'S' || c2 == 'B') {
                break;
            }
            int i9 = w[c2];
            if (i6 < -214748364) {
                throw new NumberFormatException(v());
            }
            int i10 = i6 * 10;
            if (i10 < i2 + i9) {
                throw new NumberFormatException(v());
            }
            i6 = i10 - i9;
            i4 = i5;
        }
        if (!z) {
            return -i6;
        }
        if (i5 > this.n + 1) {
            return i6;
        }
        throw new NumberFormatException(v());
    }

    public final String v() {
        char c2 = this.f[(this.n + this.m) - 1];
        int i2 = this.m;
        if (c2 == 'L' || c2 == 'S' || c2 == 'B' || c2 == 'F' || c2 == 'D') {
            i2--;
        }
        return new String(this.f, this.n, i2);
    }

    public final float w() {
        return Float.parseFloat(v());
    }

    public final BigDecimal x() {
        char c2 = this.f[(this.n + this.m) - 1];
        int i2 = this.m;
        if (c2 == 'L' || c2 == 'S' || c2 == 'B' || c2 == 'F' || c2 == 'D') {
            i2--;
        }
        return new BigDecimal(this.f, this.n, i2);
    }

    public final boolean y() {
        int i2 = this.h - this.g;
        if (i2 < this.c) {
            return false;
        }
        char c2 = this.f[this.g];
        char c3 = this.f[this.g + 1];
        char c4 = this.f[this.g + 2];
        char c5 = this.f[this.g + 3];
        if ((c2 != '1' && c2 != '2') || c3 < '0' || c3 > '9' || c4 < '0' || c4 > '9' || c5 < '0' || c5 > '9' || this.f[this.g + 4] != '-') {
            return false;
        }
        char c6 = this.f[this.g + 5];
        char c7 = this.f[this.g + 6];
        if (c6 == '0') {
            if (c7 < '1' || c7 > '9') {
                return false;
            }
        } else if (c6 != '1') {
            return false;
        } else {
            if (!(c7 == '0' || c7 == '1' || c7 == '2')) {
                return false;
            }
        }
        if (this.f[this.g + 7] != '-') {
            return false;
        }
        char c8 = this.f[this.g + 8];
        char c9 = this.f[this.g + 9];
        if (c8 == '0') {
            if (c9 < '1' || c9 > '9') {
                return false;
            }
        } else if (c8 == '1' || c8 == '2') {
            if (c9 < '0' || c9 > '9') {
                return false;
            }
        } else if (c8 != '3') {
            return false;
        } else {
            if (!(c9 == '0' || c9 == '1')) {
                return false;
            }
        }
        this.s = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
        int i3 = (w[c2] * 1000) + (w[c3] * 100) + (w[c4] * 10) + w[c5];
        int i4 = (w[c8] * 10) + w[c9];
        this.s.set(1, i3);
        this.s.set(2, ((w[c6] * 10) + w[c7]) - 1);
        this.s.set(5, i4);
        char c10 = this.f[this.g + 10];
        if (c10 == 'T') {
            if (i2 < this.d) {
                return false;
            }
            char c11 = this.f[this.g + 11];
            char c12 = this.f[this.g + 12];
            if (c11 == '0') {
                if (c12 < '0' || c12 > '9') {
                    return false;
                }
            } else if (c11 == '1') {
                if (c12 < '0' || c12 > '9') {
                    return false;
                }
            } else if (c11 != '2' || c12 < '0' || c12 > '4') {
                return false;
            }
            if (this.f[this.g + 13] != ':') {
                return false;
            }
            char c13 = this.f[this.g + 14];
            char c14 = this.f[this.g + 15];
            if (c13 < '0' || c13 > '5') {
                if (!(c13 == '6' && c14 == '0')) {
                    return false;
                }
            } else if (c14 < '0' || c14 > '9') {
                return false;
            }
            if (this.f[this.g + 16] != ':') {
                return false;
            }
            char c15 = this.f[this.g + 17];
            char c16 = this.f[this.g + 18];
            if (c15 < '0' || c15 > '5') {
                if (!(c15 == '6' && c16 == '0')) {
                    return false;
                }
            } else if (c16 < '0' || c16 > '9') {
                return false;
            }
            int i5 = (w[c11] * 10) + w[c12];
            int i6 = (w[c13] * 10) + w[c14];
            int i7 = (w[c15] * 10) + w[c16];
            this.s.set(11, i5);
            this.s.set(12, i6);
            this.s.set(13, i7);
            if (this.f[this.g + 19] != '.') {
                this.s.set(14, 0);
                char[] cArr = this.f;
                int i8 = this.g + 19;
                this.g = i8;
                this.j = cArr[i8];
                this.o = 5;
                return true;
            } else if (i2 < this.e) {
                return false;
            } else {
                char c17 = this.f[this.g + 20];
                char c18 = this.f[this.g + 21];
                char c19 = this.f[this.g + 22];
                if (c17 < '0' || c17 > '9' || c18 < '0' || c18 > '9' || c19 < '0' || c19 > '9') {
                    return false;
                }
                this.s.set(14, (w[c17] * 100) + (w[c18] * 10) + w[c19]);
                char[] cArr2 = this.f;
                int i9 = this.g + 23;
                this.g = i9;
                this.j = cArr2[i9];
                this.o = 5;
                return true;
            }
        } else if (c10 != '\"' && c10 != 26) {
            return false;
        } else {
            this.s.set(11, 0);
            this.s.set(12, 0);
            this.s.set(13, 0);
            this.s.set(14, 0);
            char[] cArr3 = this.f;
            int i10 = this.g + 10;
            this.g = i10;
            this.j = cArr3[i10];
            this.o = 5;
            return true;
        }
    }

    public final Calendar z() {
        return this.s;
    }
}
