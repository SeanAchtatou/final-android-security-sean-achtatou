package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import org.apache.commons.httpclient.cookie.Cookie2;

public final class y implements am {
    public static final y a = new y();

    public final int a() {
        return 12;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        int i;
        InetAddress inetAddress;
        InetAddress inetAddress2 = null;
        f k = cVar.k();
        if (k.e() == 8) {
            k.l();
            return null;
        }
        cVar.b(12);
        int i2 = 0;
        while (true) {
            String c = k.c(cVar.b());
            k.l();
            if (c.equals("address")) {
                cVar.b(17);
                int i3 = i2;
                inetAddress = (InetAddress) cVar.a(InetAddress.class);
                i = i3;
            } else if (c.equals(Cookie2.PORT)) {
                cVar.b(17);
                if (k.e() != 2) {
                    throw new d("port is not int");
                }
                i = k.u();
                k.l();
                inetAddress = inetAddress2;
            } else {
                cVar.b(17);
                cVar.j();
                i = i2;
                inetAddress = inetAddress2;
            }
            if (k.e() == 16) {
                k.l();
                inetAddress2 = inetAddress;
                i2 = i;
            } else {
                cVar.b(13);
                return new InetSocketAddress(inetAddress, i);
            }
        }
    }
}
