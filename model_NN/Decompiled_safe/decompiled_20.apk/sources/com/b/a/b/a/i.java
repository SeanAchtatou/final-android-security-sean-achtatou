package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d.g;
import java.lang.reflect.Type;

public final class i implements am {
    public static final i a = new i();

    public final int a() {
        return 6;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 6) {
            k.b(16);
            return Boolean.TRUE;
        } else if (k.e() == 7) {
            k.b(16);
            return Boolean.FALSE;
        } else if (k.e() == 2) {
            int u = k.u();
            k.b(16);
            return u == 1 ? Boolean.TRUE : Boolean.FALSE;
        } else {
            Object j = cVar.j();
            if (j == null) {
                return null;
            }
            return g.k(j);
        }
    }
}
