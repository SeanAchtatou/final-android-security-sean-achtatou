package com.b.a.b;

import android.support.v4.util.TimeUtils;
import android.support.v4.view.MotionEventCompat;
import com.amap.api.search.route.Route;
import com.b.a.a;
import com.b.a.b;
import com.b.a.b.a.af;
import com.b.a.b.a.ak;
import com.b.a.b.a.am;
import com.b.a.b.a.aq;
import com.b.a.b.a.s;
import com.b.a.b.a.z;
import com.b.a.d;
import com.b.a.e;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.util.LangUtils;

public final class c extends a {
    private static final Set<Class<?>> g;
    protected final Object a;
    protected final k b;
    protected j c;
    protected final f d;
    protected i e;
    private s f;
    private String h;
    private DateFormat i;
    private i[] j;
    private int k;
    private final List<d> l;
    private int m;

    static {
        HashSet hashSet = new HashSet();
        g = hashSet;
        hashSet.add(Boolean.TYPE);
        g.add(Byte.TYPE);
        g.add(Short.TYPE);
        g.add(Integer.TYPE);
        g.add(Long.TYPE);
        g.add(Float.TYPE);
        g.add(Double.TYPE);
        g.add(Boolean.class);
        g.add(Byte.class);
        g.add(Short.class);
        g.add(Integer.class);
        g.add(Long.class);
        g.add(Float.class);
        g.add(Double.class);
        g.add(BigInteger.class);
        g.add(BigDecimal.class);
        g.add(String.class);
    }

    private c(Object obj, f fVar, j jVar) {
        this.f = new s();
        this.h = a.b;
        this.j = new i[8];
        this.k = 0;
        this.l = new ArrayList();
        this.m = 0;
        this.d = fVar;
        this.a = obj;
        this.c = jVar;
        this.b = jVar.b();
        fVar.b(12);
    }

    public c(String str, j jVar) {
        this(str, new f(str, a.a), jVar);
    }

    public c(String str, j jVar, int i2) {
        this(str, new f(str, i2), jVar);
    }

    private i a(Object obj, Object obj2) {
        if (a(e.DisableCircularReferenceDetect)) {
            return null;
        }
        return a(this.e, obj, obj2);
    }

    private void a(i iVar, int i2) {
        while (i2 < this.k) {
            i iVar2 = this.j[i2];
            if (iVar2.b() == iVar) {
                int i3 = this.k - 1;
                if (i2 != i3) {
                    System.arraycopy(this.j, i2 + 1, this.j, i2, i3 - i2);
                }
                this.j[i3] = null;
                this.k--;
                a(iVar2, i2 + 1);
            }
            i2++;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
     arg types: [java.util.Collection, java.lang.Object]
     candidates:
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
     arg types: [com.b.a.e, java.lang.Integer]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
     arg types: [com.b.a.b, java.lang.Integer]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void */
    private void a(Collection collection, Object obj) {
        String str;
        int i2 = 0;
        f fVar = this.d;
        if (fVar.e() == 21 || fVar.e() == 22) {
            fVar.l();
        }
        if (fVar.e() != 14) {
            throw new d("syntax error, expect [, actual " + g.a(fVar.e()) + ", pos " + fVar.p());
        }
        fVar.b(4);
        i iVar = this.e;
        a((Object) collection, obj);
        while (true) {
            try {
                if (a(e.AllowArbitraryCommas)) {
                    while (fVar.e() == 16) {
                        fVar.l();
                    }
                }
                switch (fVar.e()) {
                    case 2:
                        Object s = fVar.s();
                        fVar.b(16);
                        str = s;
                        break;
                    case 3:
                        Object a2 = fVar.a(e.UseBigDecimal) ? fVar.a(true) : fVar.a(false);
                        fVar.b(16);
                        str = a2;
                        break;
                    case 4:
                        String q = fVar.q();
                        fVar.b(16);
                        str = q;
                        if (fVar.a(e.AllowISO8601DateFormat)) {
                            f fVar2 = new f(q);
                            str = q;
                            if (fVar2.y()) {
                                str = fVar2.z().getTime();
                                break;
                            }
                        }
                        break;
                    case 5:
                    case MotionEventCompat.ACTION_HOVER_ENTER:
                    case 10:
                    case Route.DrivingSaveMoney:
                    case Route.DrivingNoFastRoad:
                    default:
                        str = a((Object) null);
                        break;
                    case 6:
                        Object obj2 = Boolean.TRUE;
                        fVar.b(16);
                        str = obj2;
                        break;
                    case MotionEventCompat.ACTION_HOVER_MOVE:
                        Object obj3 = Boolean.FALSE;
                        fVar.b(16);
                        str = obj3;
                        break;
                    case 8:
                        fVar.b(4);
                        str = null;
                        break;
                    case Route.DrivingLeastDistance:
                        str = a((Map) new e(), (Object) Integer.valueOf(i2));
                        break;
                    case 14:
                        b bVar = new b();
                        a((Collection) bVar, (Object) Integer.valueOf(i2));
                        str = bVar;
                        break;
                    case 15:
                        fVar.b(16);
                        return;
                }
                collection.add(str);
                a(collection);
                if (fVar.e() == 16) {
                    fVar.b(4);
                }
                i2++;
            } finally {
                a(iVar);
            }
        }
    }

    public final i a(i iVar, Object obj, Object obj2) {
        int i2 = 0;
        if (a(e.DisableCircularReferenceDetect)) {
            return null;
        }
        if (this.d.a()) {
            while (true) {
                if (i2 >= this.k) {
                    break;
                }
                i iVar2 = this.j[i2];
                if (iVar2.b() == iVar && iVar2.c() == obj2) {
                    this.e = iVar2;
                    this.e.a(obj);
                    a(this.e, i2 + 1);
                    break;
                }
                i2++;
            }
            this.d.b();
        } else {
            this.e = new i(iVar, obj, obj2);
            i iVar3 = this.e;
            int i3 = this.k;
            this.k = i3 + 1;
            if (i3 >= this.j.length) {
                i[] iVarArr = new i[((this.j.length * 3) / 2)];
                System.arraycopy(this.j, 0, iVarArr, 0, this.j.length);
                this.j = iVarArr;
            }
            this.j[i3] = iVar3;
        }
        return this.e;
    }

    public final <T> T a(Class cls) {
        return a((Type) cls);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
     arg types: [java.util.HashSet, java.lang.Object]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
     arg types: [java.util.TreeSet, java.lang.Object]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
     arg types: [com.b.a.b, java.lang.Object]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
     arg types: [com.b.a.e, java.lang.Object]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object */
    public final Object a(Object obj) {
        f fVar = this.d;
        switch (fVar.e()) {
            case 2:
                Number s = fVar.s();
                fVar.l();
                return s;
            case 3:
                Number a2 = fVar.a(a(e.UseBigDecimal));
                fVar.l();
                return a2;
            case 4:
                String q = fVar.q();
                fVar.b(16);
                if (!fVar.a(e.AllowISO8601DateFormat)) {
                    return q;
                }
                f fVar2 = new f(q);
                return fVar2.y() ? fVar2.z().getTime() : q;
            case 5:
            case 10:
            case Route.DrivingSaveMoney:
            case Route.DrivingNoFastRoad:
            case 15:
            case 16:
            case LangUtils.HASH_SEED /*17*/:
            case 18:
            case TimeUtils.HUNDRED_DAY_FIELD_LEN:
            default:
                throw new d("syntax error, pos " + fVar.c());
            case 6:
                fVar.l();
                return Boolean.TRUE;
            case MotionEventCompat.ACTION_HOVER_MOVE:
                fVar.l();
                return Boolean.FALSE;
            case 8:
                fVar.l();
                return null;
            case MotionEventCompat.ACTION_HOVER_ENTER:
                fVar.b(18);
                if (fVar.e() != 18) {
                    throw new d("syntax error");
                }
                fVar.b(10);
                b(10);
                long longValue = fVar.s().longValue();
                b(2);
                b(11);
                return new Date(longValue);
            case Route.DrivingLeastDistance:
                return a((Map) new e(), obj);
            case 14:
                b bVar = new b();
                a((Collection) bVar, obj);
                return bVar;
            case MultiThreadedHttpConnectionManager.DEFAULT_MAX_TOTAL_CONNECTIONS /*20*/:
                if (fVar.d()) {
                    return null;
                }
                throw new d("unterminated json string, pos " + fVar.c());
            case 21:
                fVar.l();
                HashSet hashSet = new HashSet();
                a((Collection) hashSet, obj);
                return hashSet;
            case 22:
                fVar.l();
                TreeSet treeSet = new TreeSet();
                a((Collection) treeSet, obj);
                return treeSet;
        }
    }

    public final Object a(String str) {
        for (int i2 = 0; i2 < this.k; i2++) {
            if (str.equals(this.j[i2].d())) {
                return this.j[i2].a();
            }
        }
        return null;
    }

    public final <T> T a(Type type) {
        if (this.d.e() == 8) {
            this.d.l();
            return null;
        }
        try {
            return this.c.a(type).a(this, type, null);
        } catch (d e2) {
            throw e2;
        } catch (Throwable th) {
            throw new d(th.getMessage(), th);
        }
    }

    /* JADX INFO: additional move instructions added (5) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
     arg types: [java.util.Map, java.lang.Object]
     candidates:
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
     arg types: [com.b.a.b, java.lang.Object]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
     arg types: [com.b.a.e, java.lang.Object]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object */
    /* JADX WARNING: Code restructure failed: missing block: B:102:?, code lost:
        r5.b(4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x020b, code lost:
        if (r5.e() != 4) goto L_0x029d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x020d, code lost:
        r1 = r5.q();
        r5.b(13);
        r14 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x021d, code lost:
        if ("@".equals(r1) == false) goto L_0x0237;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0221, code lost:
        if (r13.e == null) goto L_0x0229;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0223, code lost:
        r14 = r13.e.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x022d, code lost:
        if (r5.e() == 13) goto L_0x0293;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0236, code lost:
        throw new com.b.a.d("syntax error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x023d, code lost:
        if ("..".equals(r1) == false) goto L_0x025c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x023f, code lost:
        r0 = r3.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0247, code lost:
        if (r0.a() == null) goto L_0x0250;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0249, code lost:
        r14 = r13.e.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0250, code lost:
        a(new com.b.a.b.d(r0, r1));
        r13.m = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0262, code lost:
        if ("$".equals(r1) == false) goto L_0x0287;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0264, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0269, code lost:
        if (r0.b() == null) goto L_0x0270;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x026b, code lost:
        r0 = r0.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0274, code lost:
        if (r0.a() == null) goto L_0x027b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0276, code lost:
        r14 = r0.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x027b, code lost:
        a(new com.b.a.b.d(r0, r1));
        r13.m = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0287, code lost:
        a(new com.b.a.b.d(r3, r1));
        r13.m = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0293, code lost:
        r5.b(16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0298, code lost:
        a(r3);
        r14 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x02b9, code lost:
        throw new com.b.a.d("illegal ref, " + com.b.a.b.g.a(r5.e()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x03da, code lost:
        if (r0 != '}') goto L_0x03ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x03dc, code lost:
        r5.j();
        r5.k();
        r5.l();
        a((java.lang.Object) r14, r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x03e8, code lost:
        a(r3);
        r14 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x040f, code lost:
        throw new com.b.a.d("syntax error, position at " + r5.p() + ", name " + r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r5.j();
        r5.k();
        r5.l();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009d, code lost:
        a(r3);
        r14 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01bf, code lost:
        r5.b(16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01c8, code lost:
        if (r5.e() != 13) goto L_0x01e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01ca, code lost:
        r5.b(16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        r14 = r4.newInstance();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01d2, code lost:
        a(r3);
        r14 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01e1, code lost:
        r13.m = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01e6, code lost:
        if (r13.e == null) goto L_0x01ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01ea, code lost:
        if ((r15 instanceof java.lang.Integer) != false) goto L_0x01ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01ec, code lost:
        i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01ef, code lost:
        r14 = r13.c.a((java.lang.reflect.Type) r4).a(r13, r4, r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01f8, code lost:
        a(r3);
        r14 = r14;
     */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0157 A[Catch:{ Exception -> 0x01d8, all -> 0x008b }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0168 A[Catch:{ Exception -> 0x01d8, all -> 0x008b }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x01fe  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.util.Map r14, java.lang.Object r15) {
        /*
            r13 = this;
            r11 = 58
            r10 = 34
            r9 = 13
            r2 = 1
            r8 = 16
            com.b.a.b.f r5 = r13.d
            int r0 = r5.e()
            r1 = 12
            if (r0 == r1) goto L_0x0032
            int r0 = r5.e()
            if (r0 == r8) goto L_0x0032
            com.b.a.d r0 = new com.b.a.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "syntax error, expect {, actual "
            r1.<init>(r2)
            java.lang.String r2 = r5.f()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0032:
            com.b.a.b.i r3 = r13.e
            r1 = 0
            r0 = r1
        L_0x0036:
            r5.g()     // Catch:{ all -> 0x008b }
            char r1 = r5.h()     // Catch:{ all -> 0x008b }
            com.b.a.b.e r4 = com.b.a.b.e.AllowArbitraryCommas     // Catch:{ all -> 0x008b }
            boolean r4 = r13.a(r4)     // Catch:{ all -> 0x008b }
            if (r4 == 0) goto L_0x0054
        L_0x0045:
            r4 = 44
            if (r1 != r4) goto L_0x0054
            r5.j()     // Catch:{ all -> 0x008b }
            r5.g()     // Catch:{ all -> 0x008b }
            char r1 = r5.h()     // Catch:{ all -> 0x008b }
            goto L_0x0045
        L_0x0054:
            r4 = 0
            if (r1 != r10) goto L_0x0090
            com.b.a.b.k r1 = r13.b     // Catch:{ all -> 0x008b }
            r6 = 34
            java.lang.String r1 = r5.a(r1, r6)     // Catch:{ all -> 0x008b }
            r5.g()     // Catch:{ all -> 0x008b }
            char r6 = r5.h()     // Catch:{ all -> 0x008b }
            if (r6 == r11) goto L_0x0416
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r4 = "expect ':' at "
            r2.<init>(r4)     // Catch:{ all -> 0x008b }
            int r4 = r5.p()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x008b }
            java.lang.String r4 = ", name "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008b }
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x008b:
            r0 = move-exception
            r13.a(r3)
            throw r0
        L_0x0090:
            r6 = 125(0x7d, float:1.75E-43)
            if (r1 != r6) goto L_0x00a1
            r5.j()     // Catch:{ all -> 0x008b }
            r5.k()     // Catch:{ all -> 0x008b }
            r5.l()     // Catch:{ all -> 0x008b }
            r13.a(r3)
        L_0x00a0:
            return r14
        L_0x00a1:
            r6 = 39
            if (r1 != r6) goto L_0x00df
            com.b.a.b.e r1 = com.b.a.b.e.AllowSingleQuotes     // Catch:{ all -> 0x008b }
            boolean r1 = r13.a(r1)     // Catch:{ all -> 0x008b }
            if (r1 != 0) goto L_0x00b5
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x00b5:
            com.b.a.b.k r1 = r13.b     // Catch:{ all -> 0x008b }
            r6 = 39
            java.lang.String r1 = r5.a(r1, r6)     // Catch:{ all -> 0x008b }
            r5.g()     // Catch:{ all -> 0x008b }
            char r6 = r5.h()     // Catch:{ all -> 0x008b }
            if (r6 == r11) goto L_0x0416
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r2 = "expect ':' at "
            r1.<init>(r2)     // Catch:{ all -> 0x008b }
            int r2 = r5.p()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008b }
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x00df:
            r6 = 26
            if (r1 != r6) goto L_0x00eb
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x00eb:
            r6 = 44
            if (r1 != r6) goto L_0x00f7
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x00f7:
            r6 = 48
            if (r1 < r6) goto L_0x00ff
            r6 = 57
            if (r1 <= r6) goto L_0x0103
        L_0x00ff:
            r6 = 45
            if (r1 != r6) goto L_0x0143
        L_0x0103:
            r5.k()     // Catch:{ all -> 0x008b }
            r5.o()     // Catch:{ all -> 0x008b }
            int r1 = r5.e()     // Catch:{ all -> 0x008b }
            r6 = 2
            if (r1 != r6) goto L_0x013d
            java.lang.Number r1 = r5.s()     // Catch:{ all -> 0x008b }
        L_0x0114:
            char r6 = r5.h()     // Catch:{ all -> 0x008b }
            if (r6 == r11) goto L_0x041b
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r4 = "expect ':' at "
            r2.<init>(r4)     // Catch:{ all -> 0x008b }
            int r4 = r5.p()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x008b }
            java.lang.String r4 = ", name "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008b }
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x013d:
            r1 = 1
            java.lang.Number r1 = r5.a(r1)     // Catch:{ all -> 0x008b }
            goto L_0x0114
        L_0x0143:
            r6 = 123(0x7b, float:1.72E-43)
            if (r1 == r6) goto L_0x014b
            r6 = 91
            if (r1 != r6) goto L_0x017d
        L_0x014b:
            r5.l()     // Catch:{ all -> 0x008b }
            r1 = 0
            java.lang.Object r1 = r13.a(r1)     // Catch:{ all -> 0x008b }
            r4 = r1
            r1 = r2
        L_0x0155:
            if (r1 != 0) goto L_0x015d
            r5.j()     // Catch:{ all -> 0x008b }
            r5.g()     // Catch:{ all -> 0x008b }
        L_0x015d:
            char r6 = r5.h()     // Catch:{ all -> 0x008b }
            r5.k()     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "@type"
            if (r4 != r1) goto L_0x01fe
            com.b.a.b.k r1 = r13.b     // Catch:{ all -> 0x008b }
            r4 = 34
            java.lang.String r1 = r5.a(r1, r4)     // Catch:{ all -> 0x008b }
            java.lang.Class r4 = com.b.a.d.g.a(r1)     // Catch:{ all -> 0x008b }
            if (r4 != 0) goto L_0x01bf
            java.lang.String r4 = "@type"
            r14.put(r4, r1)     // Catch:{ all -> 0x008b }
            goto L_0x0036
        L_0x017d:
            com.b.a.b.e r1 = com.b.a.b.e.AllowUnQuotedFieldNames     // Catch:{ all -> 0x008b }
            boolean r1 = r13.a(r1)     // Catch:{ all -> 0x008b }
            if (r1 != 0) goto L_0x018d
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x018d:
            com.b.a.b.k r1 = r13.b     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r5.a(r1)     // Catch:{ all -> 0x008b }
            r5.g()     // Catch:{ all -> 0x008b }
            char r6 = r5.h()     // Catch:{ all -> 0x008b }
            if (r6 == r11) goto L_0x0416
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r2 = "expect ':' at "
            r1.<init>(r2)     // Catch:{ all -> 0x008b }
            int r2 = r5.p()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.String r2 = ", actual "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008b }
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x01bf:
            r0 = 16
            r5.b(r0)     // Catch:{ all -> 0x008b }
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            if (r0 != r9) goto L_0x01e1
            r0 = 16
            r5.b(r0)     // Catch:{ all -> 0x008b }
            java.lang.Object r14 = r4.newInstance()     // Catch:{ Exception -> 0x01d8 }
            r13.a(r3)
            goto L_0x00a0
        L_0x01d8:
            r0 = move-exception
            com.b.a.d r1 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.String r2 = "create instance error"
            r1.<init>(r2, r0)     // Catch:{ all -> 0x008b }
            throw r1     // Catch:{ all -> 0x008b }
        L_0x01e1:
            r0 = 2
            r13.m = r0     // Catch:{ all -> 0x008b }
            com.b.a.b.i r0 = r13.e     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x01ef
            boolean r0 = r15 instanceof java.lang.Integer     // Catch:{ all -> 0x008b }
            if (r0 != 0) goto L_0x01ef
            r13.i()     // Catch:{ all -> 0x008b }
        L_0x01ef:
            com.b.a.b.j r0 = r13.c     // Catch:{ all -> 0x008b }
            com.b.a.b.a.am r0 = r0.a(r4)     // Catch:{ all -> 0x008b }
            java.lang.Object r14 = r0.a(r13, r4, r15)     // Catch:{ all -> 0x008b }
            r13.a(r3)
            goto L_0x00a0
        L_0x01fe:
            java.lang.String r1 = "$ref"
            if (r4 != r1) goto L_0x02ba
            r0 = 4
            r5.b(r0)     // Catch:{ all -> 0x008b }
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            r1 = 4
            if (r0 != r1) goto L_0x029d
            java.lang.String r1 = r5.q()     // Catch:{ all -> 0x008b }
            r0 = 13
            r5.b(r0)     // Catch:{ all -> 0x008b }
            r14 = 0
            java.lang.String r0 = "@"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x0237
            com.b.a.b.i r0 = r13.e     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x0229
            com.b.a.b.i r0 = r13.e     // Catch:{ all -> 0x008b }
            java.lang.Object r14 = r0.a()     // Catch:{ all -> 0x008b }
        L_0x0229:
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            if (r0 == r9) goto L_0x0293
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x0237:
            java.lang.String r0 = ".."
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x025c
            com.b.a.b.i r0 = r3.b()     // Catch:{ all -> 0x008b }
            java.lang.Object r2 = r0.a()     // Catch:{ all -> 0x008b }
            if (r2 == 0) goto L_0x0250
            com.b.a.b.i r0 = r13.e     // Catch:{ all -> 0x008b }
            java.lang.Object r14 = r0.a()     // Catch:{ all -> 0x008b }
            goto L_0x0229
        L_0x0250:
            com.b.a.b.d r2 = new com.b.a.b.d     // Catch:{ all -> 0x008b }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x008b }
            r13.a(r2)     // Catch:{ all -> 0x008b }
            r0 = 1
            r13.m = r0     // Catch:{ all -> 0x008b }
            goto L_0x0229
        L_0x025c:
            java.lang.String r0 = "$"
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008b }
            if (r0 == 0) goto L_0x0287
            r0 = r3
        L_0x0265:
            com.b.a.b.i r2 = r0.b()     // Catch:{ all -> 0x008b }
            if (r2 == 0) goto L_0x0270
            com.b.a.b.i r0 = r0.b()     // Catch:{ all -> 0x008b }
            goto L_0x0265
        L_0x0270:
            java.lang.Object r2 = r0.a()     // Catch:{ all -> 0x008b }
            if (r2 == 0) goto L_0x027b
            java.lang.Object r14 = r0.a()     // Catch:{ all -> 0x008b }
            goto L_0x0229
        L_0x027b:
            com.b.a.b.d r2 = new com.b.a.b.d     // Catch:{ all -> 0x008b }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x008b }
            r13.a(r2)     // Catch:{ all -> 0x008b }
            r0 = 1
            r13.m = r0     // Catch:{ all -> 0x008b }
            goto L_0x0229
        L_0x0287:
            com.b.a.b.d r0 = new com.b.a.b.d     // Catch:{ all -> 0x008b }
            r0.<init>(r3, r1)     // Catch:{ all -> 0x008b }
            r13.a(r0)     // Catch:{ all -> 0x008b }
            r0 = 1
            r13.m = r0     // Catch:{ all -> 0x008b }
            goto L_0x0229
        L_0x0293:
            r0 = 16
            r5.b(r0)     // Catch:{ all -> 0x008b }
            r13.a(r3)
            goto L_0x00a0
        L_0x029d:
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r2 = "illegal ref, "
            r1.<init>(r2)     // Catch:{ all -> 0x008b }
            int r2 = r5.e()     // Catch:{ all -> 0x008b }
            java.lang.String r2 = com.b.a.b.g.a(r2)     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008b }
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x02ba:
            if (r0 != 0) goto L_0x0413
            r13.a(r14, r15)     // Catch:{ all -> 0x008b }
            r1 = r2
        L_0x02c0:
            if (r6 != r10) goto L_0x02f8
            r5.m()     // Catch:{ all -> 0x008b }
            java.lang.String r0 = r5.q()     // Catch:{ all -> 0x008b }
            com.b.a.b.e r6 = com.b.a.b.e.AllowISO8601DateFormat     // Catch:{ all -> 0x008b }
            boolean r6 = r5.a(r6)     // Catch:{ all -> 0x008b }
            if (r6 == 0) goto L_0x02e4
            com.b.a.b.f r6 = new com.b.a.b.f     // Catch:{ all -> 0x008b }
            r6.<init>(r0)     // Catch:{ all -> 0x008b }
            boolean r7 = r6.y()     // Catch:{ all -> 0x008b }
            if (r7 == 0) goto L_0x02e4
            java.util.Calendar r0 = r6.z()     // Catch:{ all -> 0x008b }
            java.util.Date r0 = r0.getTime()     // Catch:{ all -> 0x008b }
        L_0x02e4:
            r14.put(r4, r0)     // Catch:{ all -> 0x008b }
        L_0x02e7:
            r5.g()     // Catch:{ all -> 0x008b }
            char r0 = r5.h()     // Catch:{ all -> 0x008b }
            r6 = 44
            if (r0 != r6) goto L_0x03d8
            r5.j()     // Catch:{ all -> 0x008b }
            r0 = r1
            goto L_0x0036
        L_0x02f8:
            r0 = 48
            if (r6 < r0) goto L_0x0300
            r0 = 57
            if (r6 <= r0) goto L_0x0304
        L_0x0300:
            r0 = 45
            if (r6 != r0) goto L_0x031b
        L_0x0304:
            r5.o()     // Catch:{ all -> 0x008b }
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            r6 = 2
            if (r0 != r6) goto L_0x0316
            java.lang.Number r0 = r5.s()     // Catch:{ all -> 0x008b }
        L_0x0312:
            r14.put(r4, r0)     // Catch:{ all -> 0x008b }
            goto L_0x02e7
        L_0x0316:
            java.math.BigDecimal r0 = r5.x()     // Catch:{ all -> 0x008b }
            goto L_0x0312
        L_0x031b:
            r0 = 91
            if (r6 != r0) goto L_0x0349
            r5.l()     // Catch:{ all -> 0x008b }
            com.b.a.b r0 = new com.b.a.b     // Catch:{ all -> 0x008b }
            r0.<init>()     // Catch:{ all -> 0x008b }
            r13.a(r0, r4)     // Catch:{ all -> 0x008b }
            r14.put(r4, r0)     // Catch:{ all -> 0x008b }
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            if (r0 != r9) goto L_0x033b
            r5.l()     // Catch:{ all -> 0x008b }
            r13.a(r3)
            goto L_0x00a0
        L_0x033b:
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            if (r0 == r8) goto L_0x0410
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.String r1 = "syntax error"
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x0349:
            r0 = 123(0x7b, float:1.72E-43)
            if (r6 != r0) goto L_0x0396
            r5.l()     // Catch:{ all -> 0x008b }
            com.b.a.e r0 = new com.b.a.e     // Catch:{ all -> 0x008b }
            r0.<init>()     // Catch:{ all -> 0x008b }
            java.lang.Object r0 = r13.a(r0, r4)     // Catch:{ all -> 0x008b }
            java.lang.String r6 = r4.toString()     // Catch:{ all -> 0x008b }
            r13.a(r14, r6)     // Catch:{ all -> 0x008b }
            r14.put(r4, r0)     // Catch:{ all -> 0x008b }
            r13.a(r3, r0, r4)     // Catch:{ all -> 0x008b }
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            if (r0 != r9) goto L_0x0377
            r5.l()     // Catch:{ all -> 0x008b }
            r13.a(r3)     // Catch:{ all -> 0x008b }
            r13.a(r3)
            goto L_0x00a0
        L_0x0377:
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            if (r0 == r8) goto L_0x0410
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r2 = "syntax error, "
            r1.<init>(r2)     // Catch:{ all -> 0x008b }
            java.lang.String r2 = r5.f()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008b }
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x0396:
            r5.l()     // Catch:{ all -> 0x008b }
            r0 = 0
            java.lang.Object r0 = r13.a(r0)     // Catch:{ all -> 0x008b }
            r14.put(r4, r0)     // Catch:{ all -> 0x008b }
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            if (r0 != r9) goto L_0x03af
            r5.l()     // Catch:{ all -> 0x008b }
            r13.a(r3)
            goto L_0x00a0
        L_0x03af:
            int r0 = r5.e()     // Catch:{ all -> 0x008b }
            if (r0 == r8) goto L_0x0410
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r2 = "syntax error, position at "
            r1.<init>(r2)     // Catch:{ all -> 0x008b }
            int r2 = r5.p()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.String r2 = ", name "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008b }
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x03d8:
            r1 = 125(0x7d, float:1.75E-43)
            if (r0 != r1) goto L_0x03ed
            r5.j()     // Catch:{ all -> 0x008b }
            r5.k()     // Catch:{ all -> 0x008b }
            r5.l()     // Catch:{ all -> 0x008b }
            r13.a(r14, r15)     // Catch:{ all -> 0x008b }
            r13.a(r3)
            goto L_0x00a0
        L_0x03ed:
            com.b.a.d r0 = new com.b.a.d     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r2 = "syntax error, position at "
            r1.<init>(r2)     // Catch:{ all -> 0x008b }
            int r2 = r5.p()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.String r2 = ", name "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x008b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008b }
            r0.<init>(r1)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ all -> 0x008b }
        L_0x0410:
            r0 = r1
            goto L_0x0036
        L_0x0413:
            r1 = r0
            goto L_0x02c0
        L_0x0416:
            r12 = r1
            r1 = r4
            r4 = r12
            goto L_0x0155
        L_0x041b:
            r12 = r1
            r1 = r4
            r4 = r12
            goto L_0x0155
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object");
    }

    public final DateFormat a() {
        if (this.i == null) {
            this.i = new SimpleDateFormat(this.h);
        }
        return this.i;
    }

    public final void a(int i2) {
        this.m = i2;
    }

    public final void a(d dVar) {
        this.l.add(dVar);
    }

    public final void a(i iVar) {
        if (!a(e.DisableCircularReferenceDetect)) {
            this.e = iVar;
        }
    }

    public final void a(Class<?> cls, Collection collection) {
        a(cls, collection, (Object) null);
    }

    public final void a(Type type, Collection collection) {
        a(type, collection, (Object) null);
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
     arg types: [java.util.Collection, java.lang.Object]
     candidates:
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i */
    public final void a(Type type, Collection collection, Object obj) {
        am a2;
        Object a3;
        String obj2;
        if (this.d.e() == 21 || this.d.e() == 22) {
            this.d.l();
        }
        if (this.d.e() != 14) {
            throw new d("exepct '[', but " + g.a(this.d.e()));
        }
        if (Integer.TYPE == type) {
            a2 = z.a;
            this.d.b(2);
        } else if (String.class == type) {
            a2 = aq.a;
            this.d.b(4);
        } else {
            a2 = this.c.a(type);
            this.d.b(a2.a());
        }
        i iVar = this.e;
        a((Object) collection, obj);
        int i2 = 0;
        while (true) {
            try {
                if (a(e.AllowArbitraryCommas)) {
                    while (this.d.e() == 16) {
                        this.d.l();
                    }
                }
                if (this.d.e() != 15) {
                    if (Integer.TYPE == type) {
                        collection.add(z.a(this));
                    } else if (String.class == type) {
                        if (this.d.e() == 4) {
                            obj2 = this.d.q();
                            this.d.b(16);
                        } else {
                            Object a4 = a((Object) null);
                            obj2 = a4 == null ? null : a4.toString();
                        }
                        collection.add(obj2);
                    } else {
                        if (this.d.e() == 8) {
                            this.d.l();
                            a3 = null;
                        } else {
                            a3 = a2.a(this, type, Integer.valueOf(i2));
                        }
                        collection.add(a3);
                        a(collection);
                    }
                    if (this.d.e() == 16) {
                        this.d.b(a2.a());
                    }
                    i2++;
                } else {
                    a(iVar);
                    this.d.b(16);
                    return;
                }
            } catch (Throwable th) {
                a(iVar);
                throw th;
            }
        }
    }

    public final void a(Collection collection) {
        if (this.m == 1) {
            d h2 = h();
            h2.a(new af(this, (List) collection, collection.size() - 1));
            h2.a(this.e);
            this.m = 0;
        }
    }

    public final void a(Map map, String str) {
        if (this.m == 1) {
            ak akVar = new ak(map, str);
            d h2 = h();
            h2.a(akVar);
            h2.a(this.e);
            this.m = 0;
        }
    }

    public final boolean a(e eVar) {
        return this.d.a(eVar);
    }

    public final k b() {
        return this.b;
    }

    public final void b(int i2) {
        f fVar = this.d;
        if (fVar.e() == i2) {
            fVar.l();
            return;
        }
        throw new d("syntax error, expect " + g.a(i2) + ", actual " + g.a(fVar.e()));
    }

    public final void b(Collection collection) {
        a(collection, (Object) null);
    }

    public final j c() {
        return this.c;
    }

    public final int d() {
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
     arg types: [com.b.a.e, ?[OBJECT, ARRAY]]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object */
    public final e e() {
        e eVar = new e();
        a((Map) eVar, (Object) null);
        return eVar;
    }

    public final i f() {
        return this.e;
    }

    public final List<d> g() {
        return this.l;
    }

    public final d h() {
        return this.l.get(this.l.size() - 1);
    }

    public final void i() {
        if (!a(e.DisableCircularReferenceDetect)) {
            this.e = this.e.b();
            this.j[this.k - 1] = null;
            this.k--;
        }
    }

    public final Object j() {
        return a((Object) null);
    }

    public final f k() {
        return this.d;
    }

    public final void l() {
        f fVar = this.d;
        try {
            if (a(e.AutoCloseSource) && !fVar.A()) {
                throw new d("not close json text, token : " + g.a(fVar.e()));
            }
        } finally {
            fVar.B();
        }
    }
}
