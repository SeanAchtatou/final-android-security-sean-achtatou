package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d;
import java.text.ParseException;
import java.util.Date;

public final class ao extends a implements am {
    public static final ao a = new ao();

    public final int a() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public final <T> T a(c cVar, Object obj) {
        long parseLong;
        if (obj == null) {
            return null;
        }
        if (obj instanceof Date) {
            return new java.sql.Date(((Date) obj).getTime());
        }
        if (obj instanceof Number) {
            return new java.sql.Date(((Number) obj).longValue());
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() == 0) {
                return null;
            }
            f fVar = new f(str);
            if (fVar.y()) {
                parseLong = fVar.z().getTimeInMillis();
            } else {
                try {
                    return new java.sql.Date(cVar.a().parse(str).getTime());
                } catch (ParseException e) {
                    parseLong = Long.parseLong(str);
                }
            }
            return new java.sql.Date(parseLong);
        }
        throw new d("parse error : " + obj);
    }
}
