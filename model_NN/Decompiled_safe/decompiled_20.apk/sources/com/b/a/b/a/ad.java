package com.b.a.b.a;

import com.amap.api.search.poisearch.PoiTypeDef;
import com.b.a.b.f;
import com.b.a.b.j;
import com.b.a.d;
import com.b.a.d.b;
import com.b.a.d.c;
import com.b.a.e;
import java.lang.reflect.Constructor;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ad implements am {
    private final Map<String, u> a;
    private final List<u> b;
    private final Class<?> c;
    private final Type d;
    private b e;

    public ad(j jVar, Class<?> cls) {
        this(jVar, cls, cls);
    }

    public ad(j jVar, Class<?> cls, Type type) {
        this.a = new IdentityHashMap();
        this.b = new ArrayList();
        this.c = cls;
        this.d = type;
        this.e = b.a(cls, type);
        for (c next : this.e.d()) {
            u a2 = j.a(jVar, cls, next);
            this.a.put(next.c().intern(), a2);
            this.b.add(a2);
        }
    }

    private Object a(com.b.a.b.c cVar, Type type) {
        Object newInstance;
        if ((type instanceof Class) && this.c.isInterface()) {
            return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{(Class) type}, new e());
        } else if (this.e.a() == null) {
            return null;
        } else {
            try {
                Constructor<?> a2 = this.e.a();
                if (a2.getParameterTypes().length == 0) {
                    newInstance = a2.newInstance(new Object[0]);
                } else {
                    newInstance = a2.newInstance(cVar.f().a());
                }
                if (!cVar.a(com.b.a.b.e.InitStringFieldAsEmpty)) {
                    return newInstance;
                }
                for (c next : this.e.d()) {
                    if (next.a() == String.class) {
                        try {
                            next.a(newInstance, PoiTypeDef.All);
                        } catch (Exception e2) {
                            throw new d("create instance error, class " + this.c.getName(), e2);
                        }
                    }
                }
                return newInstance;
            } catch (Exception e3) {
                throw new d("create instance error, class " + this.c.getName(), e3);
            }
        }
    }

    private boolean a(com.b.a.b.c cVar, String str, Object obj, Type type, Map<String, Object> map) {
        f k = cVar.k();
        u uVar = this.a.get(str);
        if (uVar == null) {
            Iterator<Map.Entry<String, u>> it = this.a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                if (((String) next.getKey()).equalsIgnoreCase(str)) {
                    uVar = (u) next.getValue();
                    break;
                }
            }
        }
        if (uVar != null) {
            k.a(uVar.a());
            uVar.a(cVar, obj, type, map);
            return true;
        } else if (!cVar.a(com.b.a.b.e.IgnoreNotMatch)) {
            throw new d("setter not found, class " + this.c.getName() + ", property " + str);
        } else {
            k.i();
            cVar.j();
            return false;
        }
    }

    public int a() {
        return 12;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01dd, code lost:
        if (r4 == null) goto L_0x01e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01df, code lost:
        r4.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x01e2, code lost:
        r12.a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:?, code lost:
        r5 = r11.e.d();
        r7 = r5.size();
        r9 = new java.lang.Object[r7];
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x027e, code lost:
        if (r4 >= r7) goto L_0x0294;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0280, code lost:
        r9[r4] = r6.get(r5.get(r4).c());
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x029a, code lost:
        if (r11.e.b() == null) goto L_0x02d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:?, code lost:
        r1 = r11.e.b().newInstance(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x02a6, code lost:
        if (r3 == null) goto L_0x02ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x02a8, code lost:
        r3.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x02ab, code lost:
        r12.a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x02d6, code lost:
        if (r11.e.c() == null) goto L_0x0304;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:?, code lost:
        r1 = r11.e.c().invoke(null, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0304, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a1, code lost:
        if (r2 != null) goto L_0x0304;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a3, code lost:
        if (r6 != null) goto L_0x0270;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r1 = a(r12, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a9, code lost:
        if (r3 == null) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ab, code lost:
        r3.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ae, code lost:
        r12.a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c7, code lost:
        r9.a(4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d0, code lost:
        if (r9.e() != 4) goto L_0x0161;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00d2, code lost:
        r3 = r9.q();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00dc, code lost:
        if ("@".equals(r3) == false) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00e1, code lost:
        r1 = r8.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        r9.b(13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00ee, code lost:
        if (r9.e() == 13) goto L_0x0182;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00f7, code lost:
        throw new com.b.a.d("illegal ref");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00f8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00f9, code lost:
        r3 = r4;
        r10 = r1;
        r1 = r2;
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0105, code lost:
        if ("..".equals(r3) == false) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0107, code lost:
        r1 = r8.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x010f, code lost:
        if (r1.a() == null) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0111, code lost:
        r1 = r1.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0117, code lost:
        r12.a(new com.b.a.b.d(r1, r3));
        r12.a(1);
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x012b, code lost:
        if ("$".equals(r3) == false) goto L_0x0153;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x012d, code lost:
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0132, code lost:
        if (r1.b() == null) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0134, code lost:
        r1 = r1.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x013d, code lost:
        if (r1.a() == null) goto L_0x0145;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x013f, code lost:
        r1 = r1.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0145, code lost:
        r12.a(new com.b.a.b.d(r1, r3));
        r12.a(1);
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0153, code lost:
        r12.a(new com.b.a.b.d(r8, r3));
        r12.a(1);
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x017d, code lost:
        throw new com.b.a.d("illegal ref, " + com.b.a.b.g.a(r9.e()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        r9.b(16);
        r2 = r12.a(r8, r1, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x018b, code lost:
        if (r2 == null) goto L_0x0190;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x018d, code lost:
        r2.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0190, code lost:
        r12.a(r8);
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T a(com.b.a.b.c r12, java.lang.reflect.Type r13, java.lang.Object r14) {
        /*
            r11 = this;
            com.b.a.b.f r9 = r12.k()
            int r1 = r9.e()
            r2 = 8
            if (r1 != r2) goto L_0x0013
            r1 = 16
            r9.b(r1)
            r1 = 0
        L_0x0012:
            return r1
        L_0x0013:
            com.b.a.b.i r8 = r12.f()
            r3 = 0
            r2 = 0
            r6 = 0
            int r1 = r9.e()     // Catch:{ all -> 0x0073 }
            r4 = 13
            if (r1 != r4) goto L_0x002f
            r1 = 16
            r9.b(r1)     // Catch:{ all -> 0x0073 }
            java.lang.Object r1 = r11.a(r12, r13)     // Catch:{ all -> 0x0073 }
            r12.a(r8)
            goto L_0x0012
        L_0x002f:
            int r1 = r9.e()     // Catch:{ all -> 0x0073 }
            r4 = 12
            if (r1 == r4) goto L_0x007d
            int r1 = r9.e()     // Catch:{ all -> 0x0073 }
            r4 = 16
            if (r1 == r4) goto L_0x007d
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ all -> 0x0073 }
            java.lang.String r4 = "syntax error, expect {, actual "
            r1.<init>(r4)     // Catch:{ all -> 0x0073 }
            java.lang.String r4 = r9.f()     // Catch:{ all -> 0x0073 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ all -> 0x0073 }
            java.lang.String r4 = ", pos "
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ all -> 0x0073 }
            int r4 = r9.p()     // Catch:{ all -> 0x0073 }
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ all -> 0x0073 }
            boolean r4 = r14 instanceof java.lang.String     // Catch:{ all -> 0x0073 }
            if (r4 == 0) goto L_0x0069
            java.lang.String r4 = ", fieldName "
            java.lang.StringBuffer r4 = r1.append(r4)     // Catch:{ all -> 0x0073 }
            r4.append(r14)     // Catch:{ all -> 0x0073 }
        L_0x0069:
            com.b.a.d r4 = new com.b.a.d     // Catch:{ all -> 0x0073 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0073 }
            r4.<init>(r1)     // Catch:{ all -> 0x0073 }
            throw r4     // Catch:{ all -> 0x0073 }
        L_0x0073:
            r1 = move-exception
        L_0x0074:
            if (r3 == 0) goto L_0x0079
            r3.a(r2)
        L_0x0079:
            r12.a(r8)
            throw r1
        L_0x007d:
            int r1 = r12.d()     // Catch:{ all -> 0x0073 }
            r4 = 2
            if (r1 != r4) goto L_0x0088
            r1 = 0
            r12.a(r1)     // Catch:{ all -> 0x0073 }
        L_0x0088:
            r4 = r3
        L_0x0089:
            com.b.a.b.k r1 = r12.b()     // Catch:{ all -> 0x017e }
            java.lang.String r3 = r9.b(r1)     // Catch:{ all -> 0x017e }
            if (r3 != 0) goto L_0x00c3
            int r1 = r9.e()     // Catch:{ all -> 0x017e }
            r5 = 13
            if (r1 != r5) goto L_0x00b3
            r1 = 16
            r9.b(r1)     // Catch:{ all -> 0x017e }
            r3 = r4
        L_0x00a1:
            if (r2 != 0) goto L_0x0304
            if (r6 != 0) goto L_0x0270
            java.lang.Object r1 = r11.a(r12, r13)     // Catch:{ all -> 0x0073 }
            if (r3 == 0) goto L_0x00ae
            r3.a(r1)
        L_0x00ae:
            r12.a(r8)
            goto L_0x0012
        L_0x00b3:
            int r1 = r9.e()     // Catch:{ all -> 0x017e }
            r5 = 16
            if (r1 != r5) goto L_0x00c3
            com.b.a.b.e r1 = com.b.a.b.e.AllowArbitraryCommas     // Catch:{ all -> 0x017e }
            boolean r1 = r12.a(r1)     // Catch:{ all -> 0x017e }
            if (r1 != 0) goto L_0x0089
        L_0x00c3:
            java.lang.String r1 = "$ref"
            if (r1 != r3) goto L_0x0195
            r1 = 4
            r9.a(r1)     // Catch:{ all -> 0x017e }
            int r1 = r9.e()     // Catch:{ all -> 0x017e }
            r3 = 4
            if (r1 != r3) goto L_0x0161
            java.lang.String r3 = r9.q()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = "@"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x017e }
            if (r1 == 0) goto L_0x00ff
            java.lang.Object r2 = r8.a()     // Catch:{ all -> 0x017e }
            r1 = r2
        L_0x00e3:
            r2 = 13
            r9.b(r2)     // Catch:{ all -> 0x00f8 }
            int r2 = r9.e()     // Catch:{ all -> 0x00f8 }
            r3 = 13
            if (r2 == r3) goto L_0x0182
            com.b.a.d r2 = new com.b.a.d     // Catch:{ all -> 0x00f8 }
            java.lang.String r3 = "illegal ref"
            r2.<init>(r3)     // Catch:{ all -> 0x00f8 }
            throw r2     // Catch:{ all -> 0x00f8 }
        L_0x00f8:
            r2 = move-exception
            r3 = r4
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x0074
        L_0x00ff:
            java.lang.String r1 = ".."
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x017e }
            if (r1 == 0) goto L_0x0125
            com.b.a.b.i r1 = r8.b()     // Catch:{ all -> 0x017e }
            java.lang.Object r5 = r1.a()     // Catch:{ all -> 0x017e }
            if (r5 == 0) goto L_0x0117
            java.lang.Object r2 = r1.a()     // Catch:{ all -> 0x017e }
            r1 = r2
            goto L_0x00e3
        L_0x0117:
            com.b.a.b.d r5 = new com.b.a.b.d     // Catch:{ all -> 0x017e }
            r5.<init>(r1, r3)     // Catch:{ all -> 0x017e }
            r12.a(r5)     // Catch:{ all -> 0x017e }
            r1 = 1
            r12.a(r1)     // Catch:{ all -> 0x017e }
            r1 = r2
            goto L_0x00e3
        L_0x0125:
            java.lang.String r1 = "$"
            boolean r1 = r1.equals(r3)     // Catch:{ all -> 0x017e }
            if (r1 == 0) goto L_0x0153
            r1 = r8
        L_0x012e:
            com.b.a.b.i r5 = r1.b()     // Catch:{ all -> 0x017e }
            if (r5 == 0) goto L_0x0139
            com.b.a.b.i r1 = r1.b()     // Catch:{ all -> 0x017e }
            goto L_0x012e
        L_0x0139:
            java.lang.Object r5 = r1.a()     // Catch:{ all -> 0x017e }
            if (r5 == 0) goto L_0x0145
            java.lang.Object r2 = r1.a()     // Catch:{ all -> 0x017e }
            r1 = r2
            goto L_0x00e3
        L_0x0145:
            com.b.a.b.d r5 = new com.b.a.b.d     // Catch:{ all -> 0x017e }
            r5.<init>(r1, r3)     // Catch:{ all -> 0x017e }
            r12.a(r5)     // Catch:{ all -> 0x017e }
            r1 = 1
            r12.a(r1)     // Catch:{ all -> 0x017e }
            r1 = r2
            goto L_0x00e3
        L_0x0153:
            com.b.a.b.d r1 = new com.b.a.b.d     // Catch:{ all -> 0x017e }
            r1.<init>(r8, r3)     // Catch:{ all -> 0x017e }
            r12.a(r1)     // Catch:{ all -> 0x017e }
            r1 = 1
            r12.a(r1)     // Catch:{ all -> 0x017e }
            r1 = r2
            goto L_0x00e3
        L_0x0161:
            com.b.a.d r1 = new com.b.a.d     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            java.lang.String r5 = "illegal ref, "
            r3.<init>(r5)     // Catch:{ all -> 0x017e }
            int r5 = r9.e()     // Catch:{ all -> 0x017e }
            java.lang.String r5 = com.b.a.b.g.a(r5)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ all -> 0x017e }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x017e }
            r1.<init>(r3)     // Catch:{ all -> 0x017e }
            throw r1     // Catch:{ all -> 0x017e }
        L_0x017e:
            r1 = move-exception
            r3 = r4
            goto L_0x0074
        L_0x0182:
            r2 = 16
            r9.b(r2)     // Catch:{ all -> 0x00f8 }
            com.b.a.b.i r2 = r12.a(r8, r1, r14)     // Catch:{ all -> 0x00f8 }
            if (r2 == 0) goto L_0x0190
            r2.a(r1)
        L_0x0190:
            r12.a(r8)
            goto L_0x0012
        L_0x0195:
            java.lang.String r1 = "@type"
            if (r1 != r3) goto L_0x01ef
            r1 = 4
            r9.a(r1)     // Catch:{ all -> 0x017e }
            int r1 = r9.e()     // Catch:{ all -> 0x017e }
            r3 = 4
            if (r1 != r3) goto L_0x01e7
            java.lang.String r3 = r9.q()     // Catch:{ all -> 0x017e }
            r1 = 16
            r9.b(r1)     // Catch:{ all -> 0x017e }
            boolean r1 = r13 instanceof java.lang.Class     // Catch:{ all -> 0x017e }
            if (r1 == 0) goto L_0x01cd
            r0 = r13
            java.lang.Class r0 = (java.lang.Class) r0     // Catch:{ all -> 0x017e }
            r1 = r0
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x017e }
            boolean r1 = r3.equals(r1)     // Catch:{ all -> 0x017e }
            if (r1 == 0) goto L_0x01cd
            int r1 = r9.e()     // Catch:{ all -> 0x017e }
            r3 = 13
            if (r1 != r3) goto L_0x0089
            r9.l()     // Catch:{ all -> 0x017e }
            r3 = r4
            goto L_0x00a1
        L_0x01cd:
            java.lang.Class r1 = com.b.a.d.g.a(r3)     // Catch:{ all -> 0x017e }
            com.b.a.b.j r3 = r12.c()     // Catch:{ all -> 0x017e }
            com.b.a.b.a.am r3 = r3.a(r1)     // Catch:{ all -> 0x017e }
            java.lang.Object r1 = r3.a(r12, r1, r14)     // Catch:{ all -> 0x017e }
            if (r4 == 0) goto L_0x01e2
            r4.a(r2)
        L_0x01e2:
            r12.a(r8)
            goto L_0x0012
        L_0x01e7:
            com.b.a.d r1 = new com.b.a.d     // Catch:{ all -> 0x017e }
            java.lang.String r3 = "syntax error"
            r1.<init>(r3)     // Catch:{ all -> 0x017e }
            throw r1     // Catch:{ all -> 0x017e }
        L_0x01ef:
            if (r2 != 0) goto L_0x030a
            if (r6 != 0) goto L_0x030a
            java.lang.Object r2 = r11.a(r12, r13)     // Catch:{ all -> 0x017e }
            if (r2 != 0) goto L_0x0204
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ all -> 0x017e }
            java.util.List<com.b.a.b.a.u> r1 = r11.b     // Catch:{ all -> 0x017e }
            int r1 = r1.size()     // Catch:{ all -> 0x017e }
            r6.<init>(r1)     // Catch:{ all -> 0x017e }
        L_0x0204:
            com.b.a.b.i r4 = r12.a(r8, r2, r14)     // Catch:{ all -> 0x017e }
            r7 = r4
            r4 = r2
        L_0x020a:
            r1 = r11
            r2 = r12
            r5 = r13
            boolean r1 = r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0267 }
            if (r1 != 0) goto L_0x0222
            int r1 = r9.e()     // Catch:{ all -> 0x0267 }
            r2 = 13
            if (r1 != r2) goto L_0x0306
            r9.l()     // Catch:{ all -> 0x0267 }
            r2 = r4
            r3 = r7
            goto L_0x00a1
        L_0x0222:
            int r1 = r9.e()     // Catch:{ all -> 0x0267 }
            r2 = 16
            if (r1 == r2) goto L_0x0306
            int r1 = r9.e()     // Catch:{ all -> 0x0267 }
            r2 = 13
            if (r1 != r2) goto L_0x023b
            r1 = 16
            r9.b(r1)     // Catch:{ all -> 0x0267 }
            r2 = r4
            r3 = r7
            goto L_0x00a1
        L_0x023b:
            int r1 = r9.e()     // Catch:{ all -> 0x0267 }
            r2 = 18
            if (r1 == r2) goto L_0x024a
            int r1 = r9.e()     // Catch:{ all -> 0x0267 }
            r2 = 1
            if (r1 != r2) goto L_0x026c
        L_0x024a:
            com.b.a.d r1 = new com.b.a.d     // Catch:{ all -> 0x0267 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0267 }
            java.lang.String r3 = "syntax error, unexpect token "
            r2.<init>(r3)     // Catch:{ all -> 0x0267 }
            int r3 = r9.e()     // Catch:{ all -> 0x0267 }
            java.lang.String r3 = com.b.a.b.g.a(r3)     // Catch:{ all -> 0x0267 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0267 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0267 }
            r1.<init>(r2)     // Catch:{ all -> 0x0267 }
            throw r1     // Catch:{ all -> 0x0267 }
        L_0x0267:
            r1 = move-exception
            r2 = r4
            r3 = r7
            goto L_0x0074
        L_0x026c:
            r2 = r4
            r4 = r7
            goto L_0x0089
        L_0x0270:
            com.b.a.d.b r1 = r11.e     // Catch:{ all -> 0x0073 }
            java.util.List r5 = r1.d()     // Catch:{ all -> 0x0073 }
            int r7 = r5.size()     // Catch:{ all -> 0x0073 }
            java.lang.Object[] r9 = new java.lang.Object[r7]     // Catch:{ all -> 0x0073 }
            r1 = 0
            r4 = r1
        L_0x027e:
            if (r4 >= r7) goto L_0x0294
            java.lang.Object r1 = r5.get(r4)     // Catch:{ all -> 0x0073 }
            com.b.a.d.c r1 = (com.b.a.d.c) r1     // Catch:{ all -> 0x0073 }
            java.lang.String r1 = r1.c()     // Catch:{ all -> 0x0073 }
            java.lang.Object r1 = r6.get(r1)     // Catch:{ all -> 0x0073 }
            r9[r4] = r1     // Catch:{ all -> 0x0073 }
            int r1 = r4 + 1
            r4 = r1
            goto L_0x027e
        L_0x0294:
            com.b.a.d.b r1 = r11.e     // Catch:{ all -> 0x0073 }
            java.lang.reflect.Constructor r1 = r1.b()     // Catch:{ all -> 0x0073 }
            if (r1 == 0) goto L_0x02d0
            com.b.a.d.b r1 = r11.e     // Catch:{ Exception -> 0x02b0 }
            java.lang.reflect.Constructor r1 = r1.b()     // Catch:{ Exception -> 0x02b0 }
            java.lang.Object r1 = r1.newInstance(r9)     // Catch:{ Exception -> 0x02b0 }
        L_0x02a6:
            if (r3 == 0) goto L_0x02ab
            r3.a(r1)
        L_0x02ab:
            r12.a(r8)
            goto L_0x0012
        L_0x02b0:
            r1 = move-exception
            com.b.a.d r4 = new com.b.a.d     // Catch:{ all -> 0x0073 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0073 }
            java.lang.String r6 = "create instance error, "
            r5.<init>(r6)     // Catch:{ all -> 0x0073 }
            com.b.a.d.b r6 = r11.e     // Catch:{ all -> 0x0073 }
            java.lang.reflect.Constructor r6 = r6.b()     // Catch:{ all -> 0x0073 }
            java.lang.String r6 = r6.toGenericString()     // Catch:{ all -> 0x0073 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0073 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0073 }
            r4.<init>(r5, r1)     // Catch:{ all -> 0x0073 }
            throw r4     // Catch:{ all -> 0x0073 }
        L_0x02d0:
            com.b.a.d.b r1 = r11.e     // Catch:{ all -> 0x0073 }
            java.lang.reflect.Method r1 = r1.c()     // Catch:{ all -> 0x0073 }
            if (r1 == 0) goto L_0x0304
            com.b.a.d.b r1 = r11.e     // Catch:{ Exception -> 0x02e4 }
            java.lang.reflect.Method r1 = r1.c()     // Catch:{ Exception -> 0x02e4 }
            r4 = 0
            java.lang.Object r1 = r1.invoke(r4, r9)     // Catch:{ Exception -> 0x02e4 }
            goto L_0x02a6
        L_0x02e4:
            r1 = move-exception
            com.b.a.d r4 = new com.b.a.d     // Catch:{ all -> 0x0073 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0073 }
            java.lang.String r6 = "create factory method error, "
            r5.<init>(r6)     // Catch:{ all -> 0x0073 }
            com.b.a.d.b r6 = r11.e     // Catch:{ all -> 0x0073 }
            java.lang.reflect.Method r6 = r6.c()     // Catch:{ all -> 0x0073 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0073 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0073 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0073 }
            r4.<init>(r5, r1)     // Catch:{ all -> 0x0073 }
            throw r4     // Catch:{ all -> 0x0073 }
        L_0x0304:
            r1 = r2
            goto L_0x02a6
        L_0x0306:
            r2 = r4
            r4 = r7
            goto L_0x0089
        L_0x030a:
            r7 = r4
            r4 = r2
            goto L_0x020a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.b.a.b.a.ad.a(com.b.a.b.c, java.lang.reflect.Type, java.lang.Object):java.lang.Object");
    }

    public final Map<String, u> b() {
        return this.a;
    }
}
