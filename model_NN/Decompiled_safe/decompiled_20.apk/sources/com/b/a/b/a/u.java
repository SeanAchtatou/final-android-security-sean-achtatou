package com.b.a.b.a;

import com.b.a.d;
import com.b.a.d.c;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Map;

public abstract class u {
    protected final c a;
    protected final Class<?> b;

    public u(Class<?> cls, c cVar) {
        this.b = cls;
        this.a = cVar;
    }

    public abstract int a();

    public abstract void a(com.b.a.b.c cVar, Object obj, Type type, Map<String, Object> map);

    public void a(Object obj, Object obj2) {
        Method d = this.a.d();
        if (d != null) {
            try {
                d.invoke(obj, obj2);
            } catch (Exception e) {
                throw new d("set property error, " + this.a.c(), e);
            }
        } else if (this.a.e() != null) {
            try {
                this.a.e().set(obj, obj2);
            } catch (Exception e2) {
                throw new d("set property error, " + this.a.c(), e2);
            }
        }
    }

    public final void a(Object obj, boolean z) {
        a(obj, Boolean.valueOf(z));
    }

    public final Method b() {
        return this.a.d();
    }

    public final Class<?> c() {
        return this.a.a();
    }

    public final Type d() {
        return this.a.b();
    }
}
