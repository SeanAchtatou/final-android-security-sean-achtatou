package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.d;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.UnknownHostException;

public final class x implements am {
    public static final x a = new x();

    public final int a() {
        return 4;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        String str = (String) cVar.j();
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return null;
        }
        try {
            return InetAddress.getByName(str);
        } catch (UnknownHostException e) {
            throw new d("deserialize error", e);
        }
    }
}
