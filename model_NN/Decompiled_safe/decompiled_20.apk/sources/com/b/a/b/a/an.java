package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.d;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

public final class an implements am {
    public static final an a = new an();

    public final int a() {
        return 4;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        String str = (String) cVar.j();
        if (str == null) {
            return null;
        }
        if (type == TimeZone.class) {
            return TimeZone.getTimeZone(str);
        }
        if (type == UUID.class) {
            return UUID.fromString(str);
        }
        if (type == URI.class) {
            return URI.create(str);
        }
        if (type == URL.class) {
            try {
                return new URL(str);
            } catch (MalformedURLException e) {
                throw new d("create url error", e);
            }
        } else if (type == Pattern.class) {
            return Pattern.compile(str);
        } else {
            if (type == Charset.class) {
                return Charset.forName(str);
            }
            throw new d("not support type : " + type);
        }
    }
}
