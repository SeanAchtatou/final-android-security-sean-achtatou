package com.b.a.b;

public final class l {
    public final String a = new String(this.c).intern();
    public final int b;
    public final char[] c;
    public final byte[] d;
    public l e;

    public l(char[] cArr, int i, int i2, int i3, l lVar) {
        this.c = new char[i2];
        System.arraycopy(cArr, i, this.c, 0, i2);
        this.e = lVar;
        this.b = i3;
        this.d = null;
    }
}
