package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import java.lang.reflect.Type;

public final class aq implements am {
    public static final aq a = new aq();

    public static <T> T a(c cVar) {
        f k = cVar.k();
        if (k.e() == 4) {
            T q = k.q();
            k.b(16);
            return q;
        } else if (k.e() == 2) {
            T v = k.v();
            k.b(16);
            return v;
        } else {
            Object j = cVar.j();
            if (j == null) {
                return null;
            }
            return j.toString();
        }
    }

    public final int a() {
        return 4;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        return a(cVar);
    }
}
