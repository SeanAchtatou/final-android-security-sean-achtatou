package com.b.a.b.a;

import com.b.a.a;
import com.b.a.b.c;
import com.b.a.b.f;
import java.lang.reflect.Type;

public final class l implements am {
    public static final l a = new l();

    public final int a() {
        return 4;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 4) {
            String q = k.q();
            k.b(16);
            return q.toCharArray();
        } else if (k.e() == 2) {
            Number s = k.s();
            k.b(16);
            return s.toString().toCharArray();
        } else {
            Object j = cVar.j();
            if (j == null) {
                return null;
            }
            return a.a(j).toCharArray();
        }
    }
}
