package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.d.g;
import java.lang.reflect.Type;

public final class m implements am {
    public static final m a = new m();

    public final int a() {
        return 4;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        Object j = cVar.j();
        if (j == null) {
            return null;
        }
        return g.c(j);
    }
}
