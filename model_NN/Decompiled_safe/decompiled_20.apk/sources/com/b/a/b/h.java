package com.b.a.b;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.HttpState;

public final class h {
    public static h a;
    private final Map<String, Integer> b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("null", 8);
        hashMap.put("new", 9);
        hashMap.put("true", 6);
        hashMap.put(HttpState.PREEMPTIVE_DEFAULT, 7);
        a = new h(hashMap);
    }

    private h(Map<String, Integer> map) {
        this.b = map;
    }

    public final Integer a(String str) {
        return this.b.get(str);
    }
}
