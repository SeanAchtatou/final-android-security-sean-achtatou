package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.d;
import java.text.SimpleDateFormat;

public final class q extends a implements am {
    public static final q a = new q();

    public final int a() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public final <T> T a(c cVar, Object obj) {
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.length() == 0) {
                return null;
            }
            return new SimpleDateFormat(str);
        }
        throw new d("parse error");
    }
}
