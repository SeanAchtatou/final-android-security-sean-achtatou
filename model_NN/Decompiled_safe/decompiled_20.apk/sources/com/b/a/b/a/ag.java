package com.b.a.b.a;

import com.b.a.b.c;
import java.lang.reflect.Type;
import java.util.Locale;

public final class ag implements am {
    public static final ag a = new ag();

    public final int a() {
        return 4;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        String str = (String) cVar.j();
        if (str == null) {
            return null;
        }
        String[] split = str.split("_");
        return split.length == 1 ? new Locale(split[0]) : split.length == 2 ? new Locale(split[0], split[1]) : new Locale(split[0], split[1], split[2]);
    }
}
