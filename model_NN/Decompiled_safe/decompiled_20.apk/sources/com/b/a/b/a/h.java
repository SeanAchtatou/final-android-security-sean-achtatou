package com.b.a.b.a;

import com.b.a.b.c;
import com.b.a.b.f;
import com.b.a.d.g;
import java.lang.reflect.Type;
import java.math.BigInteger;

public final class h implements am {
    public static final h a = new h();

    public final int a() {
        return 2;
    }

    public final <T> T a(c cVar, Type type, Object obj) {
        f k = cVar.k();
        if (k.e() == 2) {
            String v = k.v();
            k.b(16);
            return new BigInteger(v);
        }
        Object j = cVar.j();
        if (j == null) {
            return null;
        }
        return g.f(j);
    }
}
