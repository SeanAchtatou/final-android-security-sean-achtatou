package com.b.a.b;

public final class k {
    private final l[] a;
    private final String[] b;
    private final char[][] c;
    private final int d;

    public k() {
        this((byte) 0);
    }

    private k(byte b2) {
        this.d = 127;
        this.a = new l[128];
        this.b = new String[128];
        this.c = new char[128][];
    }

    public final String a(char[] cArr, int i, int i2) {
        int i3 = 0;
        int i4 = 0;
        int i5 = i;
        while (i3 < i2) {
            i4 = (i4 * 31) + cArr[i5];
            i3++;
            i5++;
        }
        return a(cArr, i, i2, i4);
    }

    public final String a(char[] cArr, int i, int i2, int i3) {
        boolean z;
        boolean z2;
        boolean z3;
        int i4 = i3 & this.d;
        String str = this.b[i4];
        if (str == null) {
            z = true;
        } else if (str.length() == i2) {
            char[] cArr2 = this.c[i4];
            int i5 = 0;
            while (true) {
                if (i5 >= i2) {
                    z3 = true;
                    break;
                } else if (cArr[i + i5] != cArr2[i5]) {
                    z3 = false;
                    break;
                } else {
                    i5++;
                }
            }
            if (z3) {
                return str;
            }
            z = z3;
        } else {
            z = false;
        }
        int i6 = 0;
        for (l lVar = this.a[i4]; lVar != null; lVar = lVar.e) {
            char[] cArr3 = lVar.c;
            if (i2 == cArr3.length && i3 == lVar.b) {
                int i7 = 0;
                while (true) {
                    if (i7 >= i2) {
                        z2 = true;
                        break;
                    } else if (cArr[i + i7] != cArr3[i7]) {
                        z2 = false;
                        break;
                    } else {
                        i7++;
                    }
                }
                if (z2) {
                    return lVar.a;
                }
                i6++;
            }
        }
        if (i6 >= 8) {
            return new String(cArr, i, i2);
        }
        l lVar2 = new l(cArr, i, i2, i3, this.a[i4]);
        this.a[i4] = lVar2;
        if (z) {
            this.b[i4] = lVar2.a;
            this.c[i4] = lVar2.c;
        }
        return lVar2.a;
    }
}
