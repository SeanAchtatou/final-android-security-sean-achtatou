package com.b.a.c;

import java.lang.reflect.Array;
import java.lang.reflect.Type;

public final class ag implements ai {
    public static final ag a = new ag();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        Class<?> cls = null;
        am i = yVar.i();
        if (obj != null) {
            int length = Array.getLength(obj);
            if (length == 0) {
                i.append((CharSequence) "[]");
                return;
            }
            ak b = yVar.b();
            yVar.a(b, obj, obj2);
            try {
                i.b('[');
                if (i.b(an.PrettyFormat)) {
                    yVar.d();
                    yVar.f();
                    for (int i2 = 0; i2 < length; i2++) {
                        if (i2 != 0) {
                            i.a(',');
                            yVar.f();
                        }
                        yVar.a(Array.get(obj, i2), Integer.valueOf(i2));
                    }
                    yVar.e();
                    yVar.f();
                    i.a(']');
                    return;
                }
                ai aiVar = null;
                for (int i3 = 0; i3 < length; i3++) {
                    Object obj3 = Array.get(obj, i3);
                    if (i3 != 0) {
                        i.b(',');
                    }
                    if (obj3 == null) {
                        i.append((CharSequence) "null");
                    } else {
                        Class<?> cls2 = obj3.getClass();
                        if (cls2 == cls) {
                            aiVar.a(yVar, obj3, Integer.valueOf(i3), null);
                        } else {
                            aiVar = yVar.a(cls2);
                            aiVar.a(yVar, obj3, Integer.valueOf(i3), null);
                            cls = cls2;
                        }
                    }
                }
                i.b(']');
                yVar.a(b);
            } finally {
                yVar.a(b);
            }
        } else if (i.b(an.WriteNullListAsEmpty)) {
            i.write("[]");
        } else {
            i.a();
        }
    }
}
