package com.b.a.c;

import java.lang.reflect.Type;

public final class o implements ai {
    public static final o a = new o();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        if (obj == null) {
            yVar.i().a();
        } else if (yVar.b(an.WriteEnumUsingToString)) {
            yVar.a(((Enum) obj).name());
        } else {
            i.a(((Enum) obj).ordinal());
        }
    }
}
