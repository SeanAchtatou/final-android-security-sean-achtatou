package com.b.a.c;

import java.lang.reflect.Type;

public final class ac implements ai {
    public static ac a = new ac();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        if (obj != null) {
            long longValue = ((Long) obj).longValue();
            i.a(longValue);
            if (yVar.b(an.WriteClassName) && longValue <= 2147483647L && longValue >= -2147483648L && type != Long.class) {
                i.a('L');
            }
        } else if (i.b(an.WriteNullNumberAsZero)) {
            i.a('0');
        } else {
            i.a();
        }
    }
}
