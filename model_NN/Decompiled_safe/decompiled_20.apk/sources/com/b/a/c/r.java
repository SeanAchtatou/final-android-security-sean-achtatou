package com.b.a.c;

import com.b.a.a.b;
import com.b.a.d.c;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public abstract class r implements Comparable<r> {
    protected final c a;
    private final String b;
    private final String c;
    private final String d;
    private boolean e = false;

    public r(c cVar) {
        this.a = cVar;
        cVar.f();
        this.b = "\"" + cVar.c() + "\":";
        this.c = "'" + cVar.c() + "':";
        this.d = cVar.c() + ":";
        b bVar = (b) cVar.a(b.class);
        if (bVar != null) {
            for (an anVar : bVar.e()) {
                if (anVar == an.WriteMapNullValue) {
                    this.e = true;
                }
            }
        }
    }

    public final Object a(Object obj) {
        return this.a.a(obj);
    }

    public final void a(y yVar) {
        am i = yVar.i();
        if (!yVar.b(an.QuoteFieldNames)) {
            i.write(this.d);
        } else if (yVar.b(an.UseSingleQuotes)) {
            i.write(this.c);
        } else {
            i.write(this.b);
        }
    }

    public abstract void a(y yVar, Object obj);

    public final boolean a() {
        return this.e;
    }

    public final Field b() {
        return this.a.e();
    }

    public final String c() {
        return this.a.c();
    }

    public /* synthetic */ int compareTo(Object obj) {
        return this.a.c().compareTo(((r) obj).a.c());
    }

    public final Method d() {
        return this.a.d();
    }
}
