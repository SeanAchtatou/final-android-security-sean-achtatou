package com.b.a.c;

public final class ak {
    private final ak a;
    private final Object b;
    private final Object c;
    private int d = 0;

    public ak(ak akVar, Object obj, Object obj2) {
        this.a = akVar;
        this.b = obj;
        this.c = obj2;
    }

    public final ak a() {
        return this.a;
    }

    public final Object b() {
        return this.b;
    }

    public final String c() {
        return this.a == null ? "$" : this.c instanceof Integer ? this.a.c() + "[" + this.c + "]" : this.a.c() + "." + this.c;
    }

    public final String toString() {
        return c();
    }
}
