package com.b.a.c;

import com.b.a.d.e;
import java.io.File;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

public final class al extends e<Type, ai> {
    private static final al a = new al();
    private boolean b;

    public al() {
        this((byte) 0);
    }

    private al(byte b2) {
        super((byte) 0);
        this.b = false;
        a(Boolean.class, e.a);
        a(Boolean.TYPE, e.a);
        a(Character.class, i.a);
        a(Byte.TYPE, w.a);
        a(Byte.class, w.a);
        a(Short.class, w.a);
        a(Short.TYPE, w.a);
        a(Integer.class, w.a);
        a(Integer.TYPE, w.a);
        a(Long.class, ac.a);
        a(Long.TYPE, ac.a);
        a(Float.class, t.a);
        a(Float.TYPE, t.a);
        a(Double.class, n.a);
        a(Double.TYPE, n.a);
        a(BigDecimal.class, c.a);
        a(BigInteger.class, d.a);
        a(String.class, ao.a);
        a(byte[].class, f.a);
        a(char[].class, h.a);
        a(Object[].class, ag.a);
        a(Class.class, j.a);
        a(SimpleDateFormat.class, l.a);
        a(Locale.class, aq.a);
        a(TimeZone.class, ap.a);
        a(UUID.class, aq.a);
        a(InetAddress.class, u.a);
        a(Inet4Address.class, u.a);
        a(Inet6Address.class, u.a);
        a(InetSocketAddress.class, v.a);
        a(File.class, s.a);
        a(URI.class, aq.a);
        a(URL.class, aq.a);
        a(Appendable.class, a.a);
        a(StringBuffer.class, a.a);
        a(StringBuilder.class, a.a);
        a(Pattern.class, aq.a);
        a(Charset.class, aq.a);
    }

    public static ai a(Class<?> cls) {
        return new aa(cls);
    }

    public static final al a() {
        return a;
    }
}
