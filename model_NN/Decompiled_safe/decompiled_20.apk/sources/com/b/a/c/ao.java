package com.b.a.c;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.lang.reflect.Type;

public final class ao implements ai {
    public static ao a = new ao();

    public static void a(y yVar, String str) {
        am i = yVar.i();
        if (str != null) {
            i.a(str);
        } else if (i.b(an.WriteNullStringAsEmpty)) {
            i.a(PoiTypeDef.All);
        } else {
            i.a();
        }
    }

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        a(yVar, (String) obj);
    }
}
