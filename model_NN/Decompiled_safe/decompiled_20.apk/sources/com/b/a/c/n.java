package com.b.a.c;

import java.lang.reflect.Type;

public final class n implements ai {
    public static final n a = new n();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        if (obj != null) {
            double doubleValue = ((Double) obj).doubleValue();
            if (Double.isNaN(doubleValue)) {
                i.a();
            } else if (Double.isInfinite(doubleValue)) {
                i.a();
            } else {
                String d = Double.toString(doubleValue);
                if (d.endsWith(".0")) {
                    d = d.substring(0, d.length() - 2);
                }
                i.append((CharSequence) d);
                if (yVar.b(an.WriteClassName)) {
                    i.a('D');
                }
            }
        } else if (yVar.b(an.WriteNullNumberAsZero)) {
            i.a('0');
        } else {
            i.a();
        }
    }
}
