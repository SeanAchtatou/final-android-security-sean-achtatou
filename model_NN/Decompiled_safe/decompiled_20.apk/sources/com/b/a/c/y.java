package com.b.a.c;

import com.b.a.a;
import com.b.a.c;
import com.b.a.d;
import com.b.a.f;
import java.io.IOException;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public final class y {
    private final al a;
    private final am b;
    private List<aj> c;
    private List<ar> d;
    private List<ae> e;
    private int f;
    private String g;
    private String h;
    private DateFormat i;
    private IdentityHashMap<Object, ak> j;
    private ak k;

    public y() {
        this(new am(), al.a());
    }

    public y(am amVar) {
        this(amVar, al.a());
    }

    private y(am amVar, al alVar) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = 0;
        this.g = "\t";
        this.h = a.b;
        this.j = null;
        this.b = amVar;
        this.a = alVar;
    }

    public final ai a(Class<?> cls) {
        boolean z;
        boolean z2 = false;
        ai aiVar = (ai) this.a.a((Object) cls);
        if (aiVar != null) {
            return aiVar;
        }
        if (Map.class.isAssignableFrom(cls)) {
            this.a.a(cls, ad.a);
        } else if (List.class.isAssignableFrom(cls)) {
            this.a.a(cls, ab.a);
        } else if (Collection.class.isAssignableFrom(cls)) {
            this.a.a(cls, k.a);
        } else if (Date.class.isAssignableFrom(cls)) {
            this.a.a(cls, m.a);
        } else if (c.class.isAssignableFrom(cls)) {
            this.a.a(cls, x.a);
        } else if (f.class.isAssignableFrom(cls)) {
            this.a.a(cls, z.a);
        } else if (cls.isEnum() || (cls.getSuperclass() != null && cls.getSuperclass().isEnum())) {
            this.a.a(cls, o.a);
        } else if (cls.isArray()) {
            Class<?> componentType = cls.getComponentType();
            this.a.a(cls, new b(componentType, a(componentType)));
        } else if (Throwable.class.isAssignableFrom(cls)) {
            this.a.a(cls, new q(cls));
        } else if (TimeZone.class.isAssignableFrom(cls)) {
            this.a.a(cls, ap.a);
        } else if (Appendable.class.isAssignableFrom(cls)) {
            this.a.a(cls, a.a);
        } else if (Charset.class.isAssignableFrom(cls)) {
            this.a.a(cls, aq.a);
        } else if (Enumeration.class.isAssignableFrom(cls)) {
            this.a.a(cls, p.a);
        } else if (Calendar.class.isAssignableFrom(cls)) {
            this.a.a(cls, g.a);
        } else {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z = false;
                    break;
                }
                Class<?> cls2 = interfaces[i2];
                if (cls2.getName().equals("net.sf.cglib.proxy.Factory")) {
                    z = false;
                    z2 = true;
                    break;
                } else if (cls2.getName().equals("javassist.util.proxy.ProxyObject")) {
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2 || z) {
                ai a2 = a((Class<?>) cls.getSuperclass());
                this.a.a(cls, a2);
                return a2;
            } else if (Proxy.isProxyClass(cls)) {
                al alVar = this.a;
                al alVar2 = this.a;
                alVar.a(cls, al.a(cls));
            } else {
                al alVar3 = this.a;
                al alVar4 = this.a;
                alVar3.a(cls, al.a(cls));
            }
        }
        return (ai) this.a.a((Object) cls);
    }

    public final DateFormat a() {
        if (this.i == null) {
            this.i = new SimpleDateFormat(this.h);
        }
        return this.i;
    }

    public final void a(ak akVar) {
        this.k = akVar;
    }

    public final void a(ak akVar, Object obj, Object obj2) {
        if (!b(an.DisableCircularReferenceDetect)) {
            this.k = new ak(akVar, obj, obj2);
            if (this.j == null) {
                this.j = new IdentityHashMap<>();
            }
            this.j.put(obj, this.k);
        }
    }

    public final void a(an anVar) {
        this.b.a(anVar);
    }

    public final void a(Object obj, Object obj2) {
        if (obj == null) {
            try {
                this.b.a();
            } catch (IOException e2) {
                throw new d(e2.getMessage(), e2);
            }
        } else {
            a(obj.getClass()).a(this, obj, obj2, null);
        }
    }

    public final void a(Object obj, String str) {
        if (obj instanceof Date) {
            this.b.a(new SimpleDateFormat(str).format((Date) obj));
            return;
        }
        c(obj);
    }

    public final void a(String str) {
        ao aoVar = ao.a;
        ao.a(this, str);
    }

    public final boolean a(Object obj) {
        if (!b(an.DisableCircularReferenceDetect) && this.j != null) {
            return this.j.containsKey(obj);
        }
        return false;
    }

    public final boolean a(Type type) {
        if (!this.b.b(an.WriteClassName)) {
            return false;
        }
        if (type == null && b(an.NotWriteRootClassName)) {
            if (this.k.a() == null) {
                return false;
            }
        }
        return true;
    }

    public final ak b() {
        return this.k;
    }

    public final void b(Object obj) {
        if (!b(an.DisableCircularReferenceDetect)) {
            ak akVar = this.k;
            if (obj == akVar.b()) {
                this.b.write("{\"$ref\":\"@\"}");
                return;
            }
            ak a2 = akVar.a();
            if (a2 == null || obj != a2.b()) {
                while (akVar.a() != null) {
                    akVar = akVar.a();
                }
                if (obj == akVar.b()) {
                    this.b.write("{\"$ref\":\"$\"}");
                    return;
                }
                String c2 = (this.j == null ? null : this.j.get(obj)).c();
                this.b.write("{\"$ref\":\"");
                this.b.write(c2);
                this.b.write("\"}");
                return;
            }
            this.b.write("{\"$ref\":\"..\"}");
        }
    }

    public final boolean b(an anVar) {
        return this.b.b(anVar);
    }

    public final List<ar> c() {
        return this.d;
    }

    public final void c(Object obj) {
        if (obj == null) {
            this.b.a();
            return;
        }
        try {
            a(obj.getClass()).a(this, obj, null, null);
        } catch (IOException e2) {
            throw new d(e2.getMessage(), e2);
        }
    }

    public final void d() {
        this.f++;
    }

    public final void e() {
        this.f--;
    }

    public final void f() {
        this.b.a(10);
        for (int i2 = 0; i2 < this.f; i2++) {
            this.b.write(this.g);
        }
    }

    public final List<ae> g() {
        return this.e;
    }

    public final List<aj> h() {
        return this.c;
    }

    public final am i() {
        return this.b;
    }

    public final void j() {
        this.b.a();
    }

    public final String toString() {
        return this.b.toString();
    }
}
