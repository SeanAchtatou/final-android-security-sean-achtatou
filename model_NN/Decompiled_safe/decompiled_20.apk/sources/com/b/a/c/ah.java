package com.b.a.c;

import com.b.a.a.b;
import com.b.a.d.c;
import java.util.Collection;
import org.apache.commons.httpclient.HttpState;

public final class ah extends r {
    boolean b = false;
    boolean c = false;
    boolean d = false;
    boolean e = false;
    private ai f;
    private Class<?> g;
    private String h;
    private boolean i = false;

    public ah(c cVar) {
        super(cVar);
        b bVar = (b) cVar.a(b.class);
        if (bVar != null) {
            this.h = bVar.b();
            if (this.h.trim().length() == 0) {
                this.h = null;
            }
            for (an anVar : bVar.e()) {
                if (anVar == an.WriteNullNumberAsZero) {
                    this.i = true;
                } else if (anVar == an.WriteNullStringAsEmpty) {
                    this.b = true;
                } else if (anVar == an.WriteNullBooleanAsFalse) {
                    this.c = true;
                } else if (anVar == an.WriteNullListAsEmpty) {
                    this.d = true;
                } else if (anVar == an.WriteEnumUsingToString) {
                    this.e = true;
                }
            }
        }
    }

    public final void a(y yVar, Object obj) {
        a(yVar);
        if (this.h != null) {
            yVar.a(obj, this.h);
            return;
        }
        if (this.f == null) {
            if (obj == null) {
                this.g = d().getReturnType();
            } else {
                this.g = obj.getClass();
            }
            this.f = yVar.a(this.g);
        }
        if (obj == null) {
            if (this.i && Number.class.isAssignableFrom(this.g)) {
                yVar.i().a('0');
            } else if (this.b && String.class == this.g) {
                yVar.i().write("\"\"");
            } else if (this.c && Boolean.class == this.g) {
                yVar.i().write(HttpState.PREEMPTIVE_DEFAULT);
            } else if (!this.d || !Collection.class.isAssignableFrom(this.g)) {
                this.f.a(yVar, null, this.a.c(), null);
            } else {
                yVar.i().write("[]");
            }
        } else if (!this.e || !this.g.isEnum()) {
            Class<?> cls = obj.getClass();
            if (cls == this.g) {
                this.f.a(yVar, obj, this.a.c(), this.a.b());
            } else {
                yVar.a(cls).a(yVar, obj, this.a.c(), null);
            }
        } else {
            yVar.i().a(((Enum) obj).name());
        }
    }
}
