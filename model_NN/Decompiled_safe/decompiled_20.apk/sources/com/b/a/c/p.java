package com.b.a.c;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Enumeration;

public final class p implements ai {
    public static p a = new p();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        if (obj != null) {
            Type type2 = (!yVar.b(an.WriteClassName) || !(type instanceof ParameterizedType)) ? null : ((ParameterizedType) type).getActualTypeArguments()[0];
            Enumeration enumeration = (Enumeration) obj;
            ak b = yVar.b();
            yVar.a(b, obj, obj2);
            try {
                i.b('[');
                int i2 = 0;
                while (enumeration.hasMoreElements()) {
                    Object nextElement = enumeration.nextElement();
                    int i3 = i2 + 1;
                    if (i2 != 0) {
                        i.b(',');
                    }
                    if (nextElement == null) {
                        i.a();
                        i2 = i3;
                    } else {
                        Class<?> cls = nextElement.getClass();
                        if (cls == Integer.class) {
                            i.a(((Integer) nextElement).intValue());
                            i2 = i3;
                        } else if (cls == Long.class) {
                            i.a(((Long) nextElement).longValue());
                            if (i.b(an.WriteClassName)) {
                                i.a('L');
                                i2 = i3;
                            } else {
                                i2 = i3;
                            }
                        } else {
                            yVar.a(cls).a(yVar, nextElement, Integer.valueOf(i3 - 1), type2);
                            i2 = i3;
                        }
                    }
                }
                i.b(']');
            } finally {
                yVar.a(b);
            }
        } else if (i.b(an.WriteNullListAsEmpty)) {
            i.write("[]");
        } else {
            i.a();
        }
    }
}
