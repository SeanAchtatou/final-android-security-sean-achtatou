package com.b.a.c;

import java.lang.reflect.Type;

public final class t implements ai {
    public static t a = new t();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        if (obj != null) {
            float floatValue = ((Float) obj).floatValue();
            if (Float.isNaN(floatValue)) {
                i.a();
            } else if (Float.isInfinite(floatValue)) {
                i.a();
            } else {
                String f = Float.toString(floatValue);
                if (f.endsWith(".0")) {
                    f = f.substring(0, f.length() - 2);
                }
                i.write(f);
                if (yVar.b(an.WriteClassName)) {
                    i.a('F');
                }
            }
        } else if (yVar.b(an.WriteNullNumberAsZero)) {
            i.a('0');
        } else {
            i.a();
        }
    }
}
