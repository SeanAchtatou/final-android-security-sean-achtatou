package com.b.a.c;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.lang.reflect.Type;

public final class i implements ai {
    public static final i a = new i();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        Character ch = (Character) obj;
        if (ch == null) {
            i.a(PoiTypeDef.All);
        } else if (ch.charValue() == 0) {
            i.a("\u0000");
        } else {
            i.a(ch.toString());
        }
    }
}
