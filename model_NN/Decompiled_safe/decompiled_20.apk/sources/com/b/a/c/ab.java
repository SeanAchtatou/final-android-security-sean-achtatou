package com.b.a.c;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public final class ab implements ai {
    public static final ab a = new ab();

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.c.am.a(int, char):void
     arg types: [int, int]
     candidates:
      com.b.a.c.am.a(java.lang.String, char):void
      com.b.a.c.am.a(long, char):void
      com.b.a.c.am.a(java.lang.String, long):void
      com.b.a.c.am.a(java.lang.String, java.lang.String):void
      com.b.a.c.am.a(java.lang.String, boolean):void
      com.b.a.c.am.a(int, char):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.c.am.a(long, char):void
     arg types: [long, int]
     candidates:
      com.b.a.c.am.a(java.lang.String, char):void
      com.b.a.c.am.a(int, char):void
      com.b.a.c.am.a(java.lang.String, long):void
      com.b.a.c.am.a(java.lang.String, java.lang.String):void
      com.b.a.c.am.a(java.lang.String, boolean):void
      com.b.a.c.am.a(long, char):void */
    public final void a(y yVar, Object obj, Object obj2, Type type) {
        boolean b = yVar.b(an.WriteClassName);
        am i = yVar.i();
        Type type2 = (!b || !(type instanceof ParameterizedType)) ? null : ((ParameterizedType) type).getActualTypeArguments()[0];
        if (obj != null) {
            List list = (List) obj;
            int size = list.size();
            int i2 = size - 1;
            if (i2 == -1) {
                i.append((CharSequence) "[]");
                return;
            }
            ak b2 = yVar.b();
            yVar.a(b2, obj, obj2);
            if (size > 1) {
                try {
                    if (i.b(an.PrettyFormat)) {
                        i.b('[');
                        yVar.d();
                        for (int i3 = 0; i3 < size; i3++) {
                            if (i3 != 0) {
                                i.b(',');
                            }
                            yVar.f();
                            Object obj3 = list.get(i3);
                            if (obj3 == null) {
                                yVar.i().a();
                            } else if (yVar.a(obj3)) {
                                yVar.b(obj3);
                            } else {
                                ai a2 = yVar.a(obj3.getClass());
                                yVar.a(new ak(b2, obj, obj2));
                                a2.a(yVar, obj3, Integer.valueOf(i3), type2);
                            }
                        }
                        yVar.e();
                        yVar.f();
                        i.b(']');
                        yVar.a(b2);
                        return;
                    }
                } catch (Throwable th) {
                    yVar.a(b2);
                    throw th;
                }
            }
            i.b('[');
            for (int i4 = 0; i4 < i2; i4++) {
                Object obj4 = list.get(i4);
                if (obj4 == null) {
                    i.append((CharSequence) "null,");
                } else {
                    Class<?> cls = obj4.getClass();
                    if (cls == Integer.class) {
                        i.a(((Integer) obj4).intValue(), ',');
                    } else if (cls == Long.class) {
                        long longValue = ((Long) obj4).longValue();
                        if (b) {
                            i.a(longValue, 'L');
                            i.a(',');
                        } else {
                            i.a(longValue, ',');
                        }
                    } else {
                        yVar.a(new ak(b2, obj, obj2));
                        if (yVar.a(obj4)) {
                            yVar.b(obj4);
                        } else {
                            yVar.a(obj4.getClass()).a(yVar, obj4, Integer.valueOf(i4), type2);
                        }
                        i.b(',');
                    }
                }
            }
            Object obj5 = list.get(i2);
            if (obj5 == null) {
                i.append((CharSequence) "null]");
            } else {
                Class<?> cls2 = obj5.getClass();
                if (cls2 == Integer.class) {
                    i.a(((Integer) obj5).intValue(), ']');
                } else if (cls2 != Long.class) {
                    yVar.a(new ak(b2, obj, obj2));
                    if (yVar.a(obj5)) {
                        yVar.b(obj5);
                    } else {
                        yVar.a(obj5.getClass()).a(yVar, obj5, Integer.valueOf(i2), type2);
                    }
                    i.b(']');
                } else if (b) {
                    i.a(((Long) obj5).longValue(), 'L');
                    i.a(']');
                } else {
                    i.a(((Long) obj5).longValue(), ']');
                }
            }
            yVar.a(b2);
        } else if (i.b(an.WriteNullListAsEmpty)) {
            i.write("[]");
        } else {
            i.a();
        }
    }
}
