package com.b.a.c;

import com.b.a.d;
import com.b.a.d.c;
import com.b.a.d.g;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class aa implements ai {
    private final r[] a;
    private final r[] b;

    public aa(Class<?> cls) {
        this(cls, (byte) 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.d.g.a(java.lang.Class<?>, boolean):java.util.List<com.b.a.d.c>
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.b.a.d.g.a(java.lang.Object, java.lang.Class):T
      com.b.a.d.g.a(java.lang.Class<?>, java.lang.String):boolean
      com.b.a.d.g.a(java.lang.Class<?>, boolean):java.util.List<com.b.a.d.c> */
    private aa(Class<?> cls, byte b2) {
        ArrayList arrayList = new ArrayList();
        for (c a2 : g.a(cls, false)) {
            arrayList.add(a(a2));
        }
        this.a = (r[]) arrayList.toArray(new r[arrayList.size()]);
        ArrayList arrayList2 = new ArrayList();
        for (c a3 : g.a(cls, true)) {
            arrayList2.add(a(a3));
        }
        this.b = (r[]) arrayList2.toArray(new r[arrayList2.size()]);
    }

    private static r a(c cVar) {
        return cVar.a() == Number.class ? new af(cVar) : new ah(cVar);
    }

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        boolean z;
        boolean z2;
        Object obj3;
        Field b2;
        am i = yVar.i();
        if (obj == null) {
            i.a();
        } else if (yVar.a(obj)) {
            yVar.b(obj);
        } else {
            r[] rVarArr = i.b(an.SortField) ? this.b : this.a;
            ak b3 = yVar.b();
            yVar.a(b3, obj, obj2);
            try {
                i.b('{');
                if (rVarArr.length > 0 && i.b(an.PrettyFormat)) {
                    yVar.d();
                    yVar.f();
                }
                boolean z3 = false;
                if (a(yVar, type) && obj.getClass() != type) {
                    i.b("@type");
                    yVar.c(obj.getClass());
                    z3 = true;
                }
                int i2 = 0;
                boolean z4 = z3;
                while (i2 < rVarArr.length) {
                    r rVar = rVarArr[i2];
                    if (!yVar.b(an.SkipTransientField) || (b2 = rVar.b()) == null || !Modifier.isTransient(b2.getModifiers())) {
                        Object a2 = rVar.a(obj);
                        rVar.c();
                        List<aj> h = yVar.h();
                        if (h != null) {
                            Iterator<aj> it = h.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (!it.next().a()) {
                                        z2 = false;
                                        break;
                                    }
                                } else {
                                    z2 = true;
                                    break;
                                }
                            }
                        } else {
                            z2 = true;
                        }
                        if (z2) {
                            String c = rVar.c();
                            List<ae> g = yVar.g();
                            if (g != null) {
                                for (ae a3 : g) {
                                    c = a3.a();
                                }
                            }
                            String str = c;
                            rVar.c();
                            List<ar> c2 = yVar.c();
                            if (c2 != null) {
                                obj3 = a2;
                                for (ar a4 : c2) {
                                    obj3 = a4.a();
                                }
                            } else {
                                obj3 = a2;
                            }
                            if (obj3 != null || rVar.a() || yVar.b(an.WriteMapNullValue)) {
                                if (z4) {
                                    i.b(',');
                                    if (i.b(an.PrettyFormat)) {
                                        yVar.f();
                                    }
                                }
                                if (str != rVar.c()) {
                                    i.b(str);
                                    yVar.c(obj3);
                                } else if (a2 != obj3) {
                                    rVar.a(yVar);
                                    yVar.c(obj3);
                                } else {
                                    rVar.a(yVar, obj3);
                                }
                                z = true;
                                i2++;
                                z4 = z;
                            }
                        }
                    }
                    z = z4;
                    i2++;
                    z4 = z;
                }
                if (rVarArr.length > 0 && i.b(an.PrettyFormat)) {
                    yVar.e();
                    yVar.f();
                }
                i.b('}');
                yVar.a(b3);
            } catch (Exception e) {
                throw new d("write javaBean error", e);
            } catch (Throwable th) {
                yVar.a(b3);
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(y yVar, Type type) {
        return yVar.a(type);
    }
}
