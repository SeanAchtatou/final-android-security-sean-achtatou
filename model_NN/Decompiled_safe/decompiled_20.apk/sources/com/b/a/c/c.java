package com.b.a.c;

import java.lang.reflect.Type;
import java.math.BigDecimal;

public final class c implements ai {
    public static final c a = new c();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        if (obj != null) {
            BigDecimal bigDecimal = (BigDecimal) obj;
            i.write(bigDecimal.toString());
            if (i.b(an.WriteClassName) && type != BigDecimal.class && bigDecimal.scale() == 0) {
                i.a('.');
            }
        } else if (i.b(an.WriteNullNumberAsZero)) {
            i.a('0');
        } else {
            i.a();
        }
    }
}
