package com.b.a.c;

import java.lang.reflect.Array;
import java.lang.reflect.Type;

public final class b implements ai {
    private final Class<?> a;
    private final ai b;

    public b(Class<?> cls, ai aiVar) {
        this.a = cls;
        this.b = aiVar;
    }

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        int i = 0;
        am i2 = yVar.i();
        if (obj != null) {
            ak b2 = yVar.b();
            yVar.a(b2, obj, obj2);
            try {
                i2.b('[');
                if (!(obj instanceof Object[])) {
                    int length = Array.getLength(obj);
                    while (i < length) {
                        if (i != 0) {
                            i2.b(',');
                        }
                        Object obj3 = Array.get(obj, i);
                        if (obj3 == null) {
                            i2.append((CharSequence) "null");
                        } else if (obj3.getClass() == this.a) {
                            this.b.a(yVar, obj3, Integer.valueOf(i), null);
                        } else {
                            yVar.a(obj3.getClass()).a(yVar, obj3, Integer.valueOf(i), null);
                        }
                        i++;
                    }
                } else {
                    Object[] objArr = (Object[]) obj;
                    int length2 = objArr.length;
                    while (i < length2) {
                        if (i != 0) {
                            i2.b(',');
                        }
                        Object obj4 = objArr[i];
                        if (obj4 == null) {
                            i2.append((CharSequence) "null");
                        } else if (obj4.getClass() == this.a) {
                            this.b.a(yVar, obj4, Integer.valueOf(i), null);
                        } else {
                            yVar.a(obj4.getClass()).a(yVar, obj4, Integer.valueOf(i), null);
                        }
                        i++;
                    }
                }
                i2.b(']');
            } finally {
                yVar.a(b2);
            }
        } else if (i2.b(an.WriteNullListAsEmpty)) {
            i2.write("[]");
        } else {
            i2.a();
        }
    }
}
