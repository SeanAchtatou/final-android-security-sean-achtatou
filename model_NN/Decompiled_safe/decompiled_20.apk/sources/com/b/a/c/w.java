package com.b.a.c;

import java.lang.reflect.Type;

public final class w implements ai {
    public static w a = new w();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        Number number = (Number) obj;
        if (number != null) {
            i.a(number.intValue());
            if (yVar.b(an.WriteClassName)) {
                Class<?> cls = obj.getClass();
                if (cls == Byte.TYPE || cls == Byte.class) {
                    i.a('B');
                } else if (cls == Short.TYPE || cls == Short.class) {
                    i.a('S');
                }
            }
        } else if (i.b(an.WriteNullNumberAsZero)) {
            i.a('0');
        } else {
            i.a();
        }
    }
}
