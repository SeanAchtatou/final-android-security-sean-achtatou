package com.b.a.c;

import java.lang.reflect.Type;
import java.math.BigInteger;

public final class d implements ai {
    public static final d a = new d();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        if (obj != null) {
            i.write(((BigInteger) obj).toString());
        } else if (i.b(an.WriteNullNumberAsZero)) {
            i.a('0');
        } else {
            i.a();
        }
    }
}
