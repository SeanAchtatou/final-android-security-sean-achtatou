package com.b.a.c;

import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import org.apache.commons.httpclient.cookie.Cookie2;

public final class v implements ai {
    public static v a = new v();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        if (obj == null) {
            yVar.j();
            return;
        }
        am i = yVar.i();
        InetSocketAddress inetSocketAddress = (InetSocketAddress) obj;
        InetAddress address = inetSocketAddress.getAddress();
        i.a('{');
        if (address != null) {
            i.b("address");
            yVar.c(address);
            i.a(',');
        }
        i.b(Cookie2.PORT);
        i.a(inetSocketAddress.getPort());
        i.a('}');
    }
}
