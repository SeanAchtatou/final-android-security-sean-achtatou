package com.b.a.c;

import java.lang.reflect.Type;
import org.apache.commons.httpclient.HttpState;

public final class e implements ai {
    public static final e a = new e();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        Boolean bool = (Boolean) obj;
        if (bool == null) {
            if (i.b(an.WriteNullBooleanAsFalse)) {
                i.write(HttpState.PREEMPTIVE_DEFAULT);
            } else {
                i.a();
            }
        } else if (bool.booleanValue()) {
            i.write("true");
        } else {
            i.write(HttpState.PREEMPTIVE_DEFAULT);
        }
    }
}
