package com.b.a.c;

import com.b.a.d.d;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;

public final class m implements ai {
    public static final m a = new m();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.c.am.a(long, char):void
     arg types: [long, int]
     candidates:
      com.b.a.c.am.a(java.lang.String, char):void
      com.b.a.c.am.a(int, char):void
      com.b.a.c.am.a(java.lang.String, long):void
      com.b.a.c.am.a(java.lang.String, java.lang.String):void
      com.b.a.c.am.a(java.lang.String, boolean):void
      com.b.a.c.am.a(long, char):void */
    public final void a(y yVar, Object obj, Object obj2, Type type) {
        char[] charArray;
        am i = yVar.i();
        if (obj == null) {
            i.a();
        } else if (!i.b(an.WriteClassName) || obj.getClass() == type) {
            Date date = (Date) obj;
            if (i.b(an.WriteDateUseDateFormat)) {
                i.a(yVar.a().format(date));
                return;
            }
            long time = date.getTime();
            if (yVar.b(an.UseISO8601DateFormat)) {
                if (yVar.b(an.UseSingleQuotes)) {
                    i.b('\'');
                } else {
                    i.b('\"');
                }
                Calendar instance = Calendar.getInstance();
                instance.setTimeInMillis(time);
                int i2 = instance.get(1);
                int i3 = instance.get(2) + 1;
                int i4 = instance.get(5);
                int i5 = instance.get(11);
                int i6 = instance.get(12);
                int i7 = instance.get(13);
                int i8 = instance.get(14);
                if (i8 != 0) {
                    charArray = "0000-00-00T00:00:00.000".toCharArray();
                    d.a(i8, 23, charArray);
                    d.a(i7, 19, charArray);
                    d.a(i6, 16, charArray);
                    d.a(i5, 13, charArray);
                    d.a(i4, 10, charArray);
                    d.a(i3, 7, charArray);
                    d.a(i2, 4, charArray);
                } else if (i7 == 0 && i6 == 0 && i5 == 0) {
                    charArray = "0000-00-00".toCharArray();
                    d.a(i4, 10, charArray);
                    d.a(i3, 7, charArray);
                    d.a(i2, 4, charArray);
                } else {
                    charArray = "0000-00-00T00:00:00".toCharArray();
                    d.a(i7, 19, charArray);
                    d.a(i6, 16, charArray);
                    d.a(i5, 13, charArray);
                    d.a(i4, 10, charArray);
                    d.a(i3, 7, charArray);
                    d.a(i2, 4, charArray);
                }
                i.write(charArray);
                if (yVar.b(an.UseSingleQuotes)) {
                    i.b('\'');
                } else {
                    i.b('\"');
                }
            } else {
                i.a(time);
            }
        } else if (obj.getClass() == Date.class) {
            i.write("new Date(");
            i.a(((Date) obj).getTime(), ')');
        } else {
            i.a('{');
            i.b("@type");
            yVar.a(obj.getClass().getName());
            i.a("val", ((Date) obj).getTime());
            i.a('}');
        }
    }
}
