package com.b.a.c;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

public final class l implements ai {
    public static final l a = new l();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        am i = yVar.i();
        if (obj == null) {
            i.a();
            return;
        }
        String pattern = ((SimpleDateFormat) obj).toPattern();
        if (!i.b(an.WriteClassName) || obj.getClass() == type) {
            i.a(pattern);
            return;
        }
        i.a('{');
        i.b("@type");
        yVar.a(obj.getClass().getName());
        i.a("val", pattern);
        i.a('}');
    }
}
