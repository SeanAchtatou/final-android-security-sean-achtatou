package com.b.a.c;

import com.b.a.a;
import com.b.a.b.b;
import com.b.a.d.d;
import java.io.Writer;
import java.lang.ref.SoftReference;

public final class am extends Writer {
    private static final ThreadLocal<SoftReference<char[]>> c = new ThreadLocal<>();
    protected char[] a;
    protected int b;
    private int d = a.c;

    public am() {
        SoftReference softReference = c.get();
        if (softReference != null) {
            this.a = (char[]) softReference.get();
            c.set(null);
        }
        if (this.a == null) {
            this.a = new char[1024];
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public am append(CharSequence charSequence, int i, int i2) {
        if (charSequence == null) {
            charSequence = "null";
        }
        String obj = charSequence.subSequence(i, i2).toString();
        write(obj, 0, obj.length());
        return this;
    }

    private void a(String str, char c2) {
        int i;
        int i2;
        if (str == null) {
            a();
            return;
        }
        int length = str.length();
        int i3 = this.b + length + 2;
        if (c2 != 0) {
            i3++;
        }
        if (i3 > this.a.length) {
            b(i3);
        }
        int i4 = this.b + 1;
        int i5 = i4 + length;
        this.a[this.b] = '\"';
        str.getChars(0, length, this.a, i4);
        this.b = i3;
        if (b(an.BrowserCompatible)) {
            int i6 = -1;
            for (int i7 = i4; i7 < i5; i7++) {
                char c3 = this.a[i7];
                if (c3 == '\"' || c3 == '/' || c3 == '\\') {
                    i3++;
                    i6 = i7;
                } else if (c3 == 8 || c3 == 12 || c3 == 10 || c3 == 13 || c3 == 9) {
                    i3++;
                    i6 = i7;
                } else if (c3 < ' ') {
                    i3 += 5;
                    i6 = i7;
                } else if (c3 >= 127) {
                    i3 += 5;
                    i6 = i7;
                }
            }
            if (i3 > this.a.length) {
                b(i3);
            }
            this.b = i3;
            int i8 = i5;
            while (i6 >= i4) {
                char c4 = this.a[i6];
                if (c4 == 8 || c4 == 12 || c4 == 10 || c4 == 13 || c4 == 9) {
                    System.arraycopy(this.a, i6 + 1, this.a, i6 + 2, (i8 - i6) - 1);
                    this.a[i6] = '\\';
                    this.a[i6 + 1] = b.f[c4];
                    i8++;
                } else if (c4 == '\"' || c4 == '/' || c4 == '\\') {
                    System.arraycopy(this.a, i6 + 1, this.a, i6 + 2, (i8 - i6) - 1);
                    this.a[i6] = '\\';
                    this.a[i6 + 1] = c4;
                    i8++;
                } else if (c4 < ' ') {
                    System.arraycopy(this.a, i6 + 1, this.a, i6 + 6, (i8 - i6) - 1);
                    this.a[i6] = '\\';
                    this.a[i6 + 1] = 'u';
                    this.a[i6 + 2] = '0';
                    this.a[i6 + 3] = '0';
                    this.a[i6 + 4] = b.g[c4 * 2];
                    this.a[i6 + 5] = b.g[(c4 * 2) + 1];
                    i8 += 5;
                } else if (c4 >= 127) {
                    System.arraycopy(this.a, i6 + 1, this.a, i6 + 6, (i8 - i6) - 1);
                    this.a[i6] = '\\';
                    this.a[i6 + 1] = 'u';
                    this.a[i6 + 2] = b.a[(c4 >>> 12) & 15];
                    this.a[i6 + 3] = b.a[(c4 >>> 8) & 15];
                    this.a[i6 + 4] = b.a[(c4 >>> 4) & 15];
                    this.a[i6 + 5] = b.a[c4 & 15];
                    i8 += 5;
                }
                i6--;
            }
            if (c2 != 0) {
                this.a[this.b - 2] = '\"';
                this.a[this.b - 1] = c2;
                return;
            }
            this.a[this.b - 1] = '\"';
            return;
        }
        int i9 = 0;
        int i10 = -1;
        char c5 = 0;
        int i11 = i4;
        while (i11 < i5) {
            char c6 = this.a[i11];
            if (c6 >= ']' || c6 == ' ' || ((c6 >= '0' && c6 != '\\') || ((c6 >= b.d.length || !b.d[c6]) && ((c6 != 9 || !b(an.WriteTabAsSpecial)) && (c6 != '/' || !b(an.WriteSlashAsSpecial)))))) {
                i = i10;
                i2 = i9;
            } else {
                i2 = i9 + 1;
                c5 = c6;
                i = i11;
            }
            i11++;
            i9 = i2;
            i10 = i;
        }
        int i12 = i3 + i9;
        if (i12 > this.a.length) {
            b(i12);
        }
        this.b = i12;
        if (i9 == 1) {
            System.arraycopy(this.a, i10 + 1, this.a, i10 + 2, (i5 - i10) - 1);
            this.a[i10] = '\\';
            this.a[i10 + 1] = b.f[c5];
        } else if (i9 > 1) {
            System.arraycopy(this.a, i10 + 1, this.a, i10 + 2, (i5 - i10) - 1);
            this.a[i10] = '\\';
            int i13 = i10 + 1;
            this.a[i13] = b.f[c5];
            int i14 = i5 + 1;
            for (int i15 = i13 - 2; i15 >= i4; i15--) {
                char c7 = this.a[i15];
                if ((c7 < b.d.length && b.d[c7]) || ((c7 == 9 && b(an.WriteTabAsSpecial)) || (c7 == '/' && b(an.WriteSlashAsSpecial)))) {
                    System.arraycopy(this.a, i15 + 1, this.a, i15 + 2, (i14 - i15) - 1);
                    this.a[i15] = '\\';
                    this.a[i15 + 1] = b.f[c7];
                    i14++;
                }
            }
        }
        if (c2 != 0) {
            this.a[this.b - 2] = '\"';
            this.a[this.b - 1] = c2;
            return;
        }
        this.a[this.b - 1] = '\"';
    }

    private void b(int i) {
        int length = ((this.a.length * 3) / 2) + 1;
        if (length >= i) {
            i = length;
        }
        char[] cArr = new char[i];
        System.arraycopy(this.a, 0, cArr, 0, this.b);
        this.a = cArr;
    }

    /* renamed from: a */
    public final am append(CharSequence charSequence) {
        String obj = charSequence == null ? "null" : charSequence.toString();
        write(obj, 0, obj.length());
        return this;
    }

    public final void a() {
        int i = this.b + 4;
        if (i > this.a.length) {
            b(i);
        }
        this.a[this.b] = 'n';
        this.a[this.b + 1] = 'u';
        this.a[this.b + 2] = 'l';
        this.a[this.b + 3] = 'l';
        this.b = i;
    }

    public final void a(char c2) {
        int i = this.b + 1;
        if (i > this.a.length) {
            b(i);
        }
        this.a[this.b] = c2;
        this.b = i;
    }

    public final void a(int i) {
        if (i == Integer.MIN_VALUE) {
            write("-2147483648");
            return;
        }
        int a2 = (i < 0 ? d.a((long) (-i)) + 1 : d.a((long) i)) + this.b;
        if (a2 > this.a.length) {
            b(a2);
        }
        d.a(i, a2, this.a);
        this.b = a2;
    }

    public final void a(int i, char c2) {
        if (i == Integer.MIN_VALUE) {
            write("-2147483648");
            a(c2);
            return;
        }
        int a2 = (i < 0 ? d.a((long) (-i)) + 1 : d.a((long) i)) + this.b;
        int i2 = a2 + 1;
        if (i2 > this.a.length) {
            b(i2);
        }
        d.a(i, a2, this.a);
        this.a[a2] = c2;
        this.b = i2;
    }

    public final void a(long j) {
        if (j == Long.MIN_VALUE) {
            write("-9223372036854775808");
            return;
        }
        int a2 = (j < 0 ? d.a(-j) + 1 : d.a(j)) + this.b;
        if (a2 > this.a.length) {
            b(a2);
        }
        d.a(j, a2, this.a);
        this.b = a2;
    }

    public final void a(long j, char c2) {
        if (j == Long.MIN_VALUE) {
            write("-9223372036854775808");
            a(c2);
            return;
        }
        int a2 = (j < 0 ? d.a(-j) + 1 : d.a(j)) + this.b;
        int i = a2 + 1;
        if (i > this.a.length) {
            b(i);
        }
        d.a(j, a2, this.a);
        this.a[a2] = c2;
        this.b = i;
    }

    public final void a(an anVar) {
        this.d |= anVar.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.c.am.a(java.lang.String, char):void
     arg types: [java.lang.String, int]
     candidates:
      com.b.a.c.am.a(int, char):void
      com.b.a.c.am.a(long, char):void
      com.b.a.c.am.a(java.lang.String, long):void
      com.b.a.c.am.a(java.lang.String, java.lang.String):void
      com.b.a.c.am.a(java.lang.String, boolean):void
      com.b.a.c.am.a(java.lang.String, char):void */
    public final void a(String str) {
        int i;
        int i2;
        char c2 = 0;
        if (!b(an.UseSingleQuotes)) {
            a(str, 0);
        } else if (str == null) {
            int i3 = this.b + 4;
            if (i3 > this.a.length) {
                b(i3);
            }
            "null".getChars(0, 4, this.a, this.b);
            this.b = i3;
        } else {
            int length = str.length();
            int i4 = this.b + length + 2;
            if (i4 > this.a.length) {
                b(i4);
            }
            int i5 = this.b + 1;
            int i6 = i5 + length;
            this.a[this.b] = '\'';
            str.getChars(0, length, this.a, i5);
            this.b = i4;
            int i7 = -1;
            int i8 = i5;
            int i9 = 0;
            while (i8 < i6) {
                char c3 = this.a[i8];
                if (c3 == 8 || c3 == 10 || c3 == 13 || c3 == 12 || c3 == '\\' || c3 == '\'' || ((c3 == 9 && b(an.WriteTabAsSpecial)) || (c3 == '/' && b(an.WriteSlashAsSpecial)))) {
                    i = i9 + 1;
                    c2 = c3;
                    i2 = i8;
                } else {
                    i2 = i7;
                    i = i9;
                }
                i8++;
                i9 = i;
                i7 = i2;
            }
            int i10 = i4 + i9;
            if (i10 > this.a.length) {
                b(i10);
            }
            this.b = i10;
            if (i9 == 1) {
                System.arraycopy(this.a, i7 + 1, this.a, i7 + 2, (i6 - i7) - 1);
                this.a[i7] = '\\';
                this.a[i7 + 1] = b.f[c2];
            } else if (i9 > 1) {
                System.arraycopy(this.a, i7 + 1, this.a, i7 + 2, (i6 - i7) - 1);
                this.a[i7] = '\\';
                int i11 = i7 + 1;
                this.a[i11] = b.f[c2];
                int i12 = i6 + 1;
                for (int i13 = i11 - 2; i13 >= i5; i13--) {
                    char c4 = this.a[i13];
                    if (c4 == 8 || c4 == 10 || c4 == 13 || c4 == 12 || c4 == '\\' || c4 == '\'' || ((c4 == 9 && b(an.WriteTabAsSpecial)) || (c4 == '/' && b(an.WriteSlashAsSpecial)))) {
                        System.arraycopy(this.a, i13 + 1, this.a, i13 + 2, (i12 - i13) - 1);
                        this.a[i13] = '\\';
                        this.a[i13 + 1] = b.f[c4];
                        i12++;
                    }
                }
            }
            this.a[this.b - 1] = '\'';
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.c.am.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.b.a.c.am.a(java.lang.String, char):void
      com.b.a.c.am.a(int, char):void
      com.b.a.c.am.a(long, char):void
      com.b.a.c.am.a(java.lang.String, long):void
      com.b.a.c.am.a(java.lang.String, java.lang.String):void
      com.b.a.c.am.a(java.lang.String, boolean):void */
    public final void a(String str, long j) {
        if (j == Long.MIN_VALUE || !b(an.QuoteFieldNames)) {
            a(',');
            a(str, false);
            a(j);
            return;
        }
        char c2 = b(an.UseSingleQuotes) ? '\'' : '\"';
        int a2 = j < 0 ? d.a(-j) + 1 : d.a(j);
        int length = str.length();
        int i = a2 + this.b + length + 4;
        if (i > this.a.length) {
            b(i);
        }
        int i2 = this.b;
        this.b = i;
        this.a[i2] = ',';
        int i3 = i2 + length + 1;
        this.a[i2 + 1] = c2;
        str.getChars(0, length, this.a, i2 + 2);
        this.a[i3 + 1] = c2;
        this.a[i3 + 2] = ':';
        d.a(j, this.b, this.a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.c.am.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.b.a.c.am.a(java.lang.String, char):void
      com.b.a.c.am.a(int, char):void
      com.b.a.c.am.a(long, char):void
      com.b.a.c.am.a(java.lang.String, long):void
      com.b.a.c.am.a(java.lang.String, java.lang.String):void
      com.b.a.c.am.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.c.am.a(java.lang.String, char):void
     arg types: [java.lang.String, int]
     candidates:
      com.b.a.c.am.a(int, char):void
      com.b.a.c.am.a(long, char):void
      com.b.a.c.am.a(java.lang.String, long):void
      com.b.a.c.am.a(java.lang.String, java.lang.String):void
      com.b.a.c.am.a(java.lang.String, boolean):void
      com.b.a.c.am.a(java.lang.String, char):void */
    public final void a(String str, String str2) {
        int length;
        int i;
        int i2;
        int i3;
        if (!b(an.QuoteFieldNames)) {
            a(',');
            a(str, false);
            if (str2 == null) {
                a();
            } else {
                a(str2);
            }
        } else if (b(an.UseSingleQuotes)) {
            a(',');
            a(str, false);
            if (str2 == null) {
                a();
            } else {
                a(str2);
            }
        } else if (b(an.BrowserCompatible)) {
            a(',');
            a(str, ':');
            a(str2, 0);
        } else {
            int length2 = str.length();
            int i4 = this.b;
            if (str2 == null) {
                length = 4;
                i = i4 + length2 + 8;
            } else {
                length = str2.length();
                i = i4 + length2 + length + 6;
            }
            if (i > this.a.length) {
                b(i);
            }
            this.a[this.b] = ',';
            int i5 = this.b + 2;
            int i6 = i5 + length2;
            this.a[this.b + 1] = '\"';
            str.getChars(0, length2, this.a, i5);
            this.b = i;
            this.a[i6] = '\"';
            int i7 = i6 + 1;
            int i8 = i7 + 1;
            this.a[i7] = ':';
            if (str2 == null) {
                int i9 = i8 + 1;
                this.a[i8] = 'n';
                int i10 = i9 + 1;
                this.a[i9] = 'u';
                this.a[i10] = 'l';
                this.a[i10 + 1] = 'l';
                return;
            }
            int i11 = i8 + 1;
            this.a[i8] = '\"';
            int i12 = i11 + length;
            str2.getChars(0, length, this.a, i11);
            if (!b(an.DisableCheckSpecialChar)) {
                int i13 = 0;
                int i14 = -1;
                char c2 = 0;
                int i15 = i11;
                while (i15 < i12) {
                    char c3 = this.a[i15];
                    if (c3 < ']') {
                        int i16 = this.d;
                        if (c3 != ' ' && (c3 <= '#' || c3 == '\\') && (c3 == 8 || c3 == 10 || c3 == 13 || c3 == 12 || c3 == '\\' || c3 == '\"' || ((c3 == 9 && an.a(i16, an.WriteTabAsSpecial)) || (c3 == '/' && an.a(i16, an.WriteTabAsSpecial))))) {
                            i3 = i13 + 1;
                            i2 = i15;
                            i15++;
                            i13 = i3;
                            i14 = i2;
                            c2 = c3;
                        }
                    }
                    c3 = c2;
                    i2 = i14;
                    i3 = i13;
                    i15++;
                    i13 = i3;
                    i14 = i2;
                    c2 = c3;
                }
                if (i13 > 0) {
                    int i17 = i + i13;
                    if (i17 > this.a.length) {
                        b(i17);
                    }
                    this.b = i17;
                }
                if (i13 == 1) {
                    System.arraycopy(this.a, i14 + 1, this.a, i14 + 2, (i12 - i14) - 1);
                    this.a[i14] = '\\';
                    this.a[i14 + 1] = b.f[c2];
                } else if (i13 > 1) {
                    System.arraycopy(this.a, i14 + 1, this.a, i14 + 2, (i12 - i14) - 1);
                    this.a[i14] = '\\';
                    int i18 = i14 + 1;
                    this.a[i18] = b.f[c2];
                    int i19 = i12 + 1;
                    for (int i20 = i18 - 2; i20 >= i11; i20--) {
                        char c4 = this.a[i20];
                        if (c4 == 8 || c4 == 10 || c4 == 13 || c4 == 12 || c4 == '\\' || c4 == '\"' || ((c4 == 9 && b(an.WriteTabAsSpecial)) || (c4 == '/' && b(an.WriteSlashAsSpecial)))) {
                            System.arraycopy(this.a, i20 + 1, this.a, i20 + 2, (i19 - i20) - 1);
                            this.a[i20] = '\\';
                            this.a[i20 + 1] = b.f[c4];
                            i19++;
                        }
                    }
                }
            }
            this.a[this.b - 1] = '\"';
        }
    }

    public final void a(String str, boolean z) {
        if (str == null) {
            write("null:");
            return;
        }
        if (b(an.UseSingleQuotes)) {
            if (b(an.QuoteFieldNames)) {
                boolean[] zArr = b.e;
                int length = str.length();
                int i = this.b + length + 3;
                if (i > this.a.length) {
                    b(i);
                }
                int i2 = this.b + 1;
                int i3 = i2 + length;
                this.a[this.b] = '\'';
                str.getChars(0, length, this.a, i2);
                this.b = i;
                while (i2 < i3) {
                    char c2 = this.a[i2];
                    if ((c2 < zArr.length && zArr[c2]) || ((c2 == 9 && b(an.WriteTabAsSpecial)) || (c2 == '/' && b(an.WriteSlashAsSpecial)))) {
                        i++;
                        if (i > this.a.length) {
                            b(i);
                        }
                        this.b = i;
                        System.arraycopy(this.a, i2 + 1, this.a, i2 + 2, (i3 - i2) - 1);
                        this.a[i2] = '\\';
                        i2++;
                        this.a[i2] = b.f[c2];
                        i3++;
                    }
                    i2++;
                }
                this.a[this.b - 2] = '\'';
            } else {
                boolean[] zArr2 = b.e;
                int length2 = str.length();
                int i4 = this.b + length2 + 1;
                if (i4 > this.a.length) {
                    b(i4);
                }
                int i5 = this.b;
                int i6 = i5 + length2;
                str.getChars(0, length2, this.a, i5);
                this.b = i4;
                boolean z2 = false;
                int i7 = i5;
                while (i7 < i6) {
                    char c3 = this.a[i7];
                    if (c3 < zArr2.length && zArr2[c3]) {
                        if (!z2) {
                            i4 += 3;
                            if (i4 > this.a.length) {
                                b(i4);
                            }
                            this.b = i4;
                            System.arraycopy(this.a, i7 + 1, this.a, i7 + 3, (i6 - i7) - 1);
                            System.arraycopy(this.a, 0, this.a, 1, i7);
                            this.a[i5] = '\'';
                            int i8 = i7 + 1;
                            this.a[i8] = '\\';
                            i7 = i8 + 1;
                            this.a[i7] = b.f[c3];
                            i6 += 2;
                            this.a[this.b - 2] = '\'';
                            z2 = true;
                        } else {
                            i4++;
                            if (i4 > this.a.length) {
                                b(i4);
                            }
                            this.b = i4;
                            System.arraycopy(this.a, i7 + 1, this.a, i7 + 2, i6 - i7);
                            this.a[i7] = '\\';
                            i7++;
                            this.a[i7] = b.f[c3];
                            i6++;
                        }
                    }
                    i7++;
                }
                this.a[i4 - 1] = ':';
                return;
            }
        } else if (b(an.QuoteFieldNames)) {
            boolean[] zArr3 = b.d;
            int length3 = str.length();
            int i9 = this.b + length3 + 3;
            if (i9 > this.a.length) {
                b(i9);
            }
            int i10 = this.b + 1;
            int i11 = i10 + length3;
            this.a[this.b] = '\"';
            str.getChars(0, length3, this.a, i10);
            this.b = i9;
            if (z) {
                while (i10 < i11) {
                    char c4 = this.a[i10];
                    if ((c4 < zArr3.length && zArr3[c4]) || ((c4 == 9 && b(an.WriteTabAsSpecial)) || (c4 == '/' && b(an.WriteSlashAsSpecial)))) {
                        i9++;
                        if (i9 > this.a.length) {
                            b(i9);
                        }
                        this.b = i9;
                        System.arraycopy(this.a, i10 + 1, this.a, i10 + 2, (i11 - i10) - 1);
                        this.a[i10] = '\\';
                        i10++;
                        this.a[i10] = b.f[c4];
                        i11++;
                    }
                    i10++;
                }
            }
            this.a[this.b - 2] = '\"';
        } else {
            boolean[] zArr4 = b.d;
            int length4 = str.length();
            int i12 = this.b + length4 + 1;
            if (i12 > this.a.length) {
                b(i12);
            }
            int i13 = this.b;
            int i14 = i13 + length4;
            str.getChars(0, length4, this.a, i13);
            this.b = i12;
            boolean z3 = false;
            int i15 = i13;
            while (i15 < i14) {
                char c5 = this.a[i15];
                if (c5 < zArr4.length && zArr4[c5]) {
                    if (!z3) {
                        i12 += 3;
                        if (i12 > this.a.length) {
                            b(i12);
                        }
                        this.b = i12;
                        System.arraycopy(this.a, i15 + 1, this.a, i15 + 3, (i14 - i15) - 1);
                        System.arraycopy(this.a, 0, this.a, 1, i15);
                        this.a[i13] = '\"';
                        int i16 = i15 + 1;
                        this.a[i16] = '\\';
                        i15 = i16 + 1;
                        this.a[i15] = b.f[c5];
                        i14 += 2;
                        this.a[this.b - 2] = '\"';
                        z3 = true;
                    } else {
                        i12++;
                        if (i12 > this.a.length) {
                            b(i12);
                        }
                        this.b = i12;
                        System.arraycopy(this.a, i15 + 1, this.a, i15 + 2, i14 - i15);
                        this.a[i15] = '\\';
                        i15++;
                        this.a[i15] = b.f[c5];
                        i14++;
                    }
                }
                i15++;
            }
        }
        this.a[this.b - 1] = ':';
    }

    public final void a(byte[] bArr) {
        int i = 0;
        int length = bArr.length;
        if (length == 0) {
            write("\"\"");
            return;
        }
        char[] cArr = com.b.a.d.a.a;
        int i2 = (length / 3) * 3;
        int i3 = this.b;
        int i4 = ((((length - 1) / 3) + 1) << 2) + this.b + 2;
        if (i4 > this.a.length) {
            b(i4);
        }
        this.b = i4;
        this.a[i3] = '\"';
        int i5 = i3 + 1;
        int i6 = 0;
        while (i6 < i2) {
            int i7 = i6 + 1;
            int i8 = i7 + 1;
            byte b2 = ((bArr[i7] & 255) << 8) | ((bArr[i6] & 255) << 16);
            i6 = i8 + 1;
            byte b3 = b2 | (bArr[i8] & 255);
            int i9 = i5 + 1;
            this.a[i5] = cArr[(b3 >>> 18) & 63];
            int i10 = i9 + 1;
            this.a[i9] = cArr[(b3 >>> 12) & 63];
            int i11 = i10 + 1;
            this.a[i10] = cArr[(b3 >>> 6) & 63];
            i5 = i11 + 1;
            this.a[i11] = cArr[b3 & 63];
        }
        int i12 = length - i2;
        if (i12 > 0) {
            int i13 = (bArr[i2] & 255) << 10;
            if (i12 == 2) {
                i = (bArr[length - 1] & 255) << 2;
            }
            int i14 = i | i13;
            this.a[i4 - 5] = cArr[i14 >> 12];
            this.a[i4 - 4] = cArr[(i14 >>> 6) & 63];
            this.a[i4 - 3] = i12 == 2 ? cArr[i14 & 63] : '=';
            this.a[i4 - 2] = '=';
        }
        this.a[i4 - 1] = '\"';
    }

    public final am b(char c2) {
        a(c2);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.c.am.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.b.a.c.am.a(java.lang.String, char):void
      com.b.a.c.am.a(int, char):void
      com.b.a.c.am.a(long, char):void
      com.b.a.c.am.a(java.lang.String, long):void
      com.b.a.c.am.a(java.lang.String, java.lang.String):void
      com.b.a.c.am.a(java.lang.String, boolean):void */
    public final void b(String str) {
        a(str, false);
    }

    public final boolean b(an anVar) {
        return an.a(this.d, anVar);
    }

    public final void close() {
        if (this.a.length <= 8192) {
            c.set(new SoftReference(this.a));
        }
        this.a = null;
    }

    public final void flush() {
    }

    public final String toString() {
        return new String(this.a, 0, this.b);
    }

    public final void write(int i) {
        int i2 = this.b + 1;
        if (i2 > this.a.length) {
            b(i2);
        }
        this.a[this.b] = (char) i;
        this.b = i2;
    }

    public final void write(String str) {
        if (str == null) {
            a();
            return;
        }
        int length = str.length();
        int i = this.b + length;
        if (i > this.a.length) {
            b(i);
        }
        str.getChars(0, length, this.a, this.b);
        this.b = i;
    }

    public final void write(String str, int i, int i2) {
        int i3 = this.b + i2;
        if (i3 > this.a.length) {
            b(i3);
        }
        str.getChars(i, i + i2, this.a, this.b);
        this.b = i3;
    }

    public final void write(char[] cArr, int i, int i2) {
        if (i < 0 || i > cArr.length || i2 < 0 || i + i2 > cArr.length || i + i2 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 != 0) {
            int i3 = this.b + i2;
            if (i3 > this.a.length) {
                b(i3);
            }
            System.arraycopy(cArr, i, this.a, this.b, i2);
            this.b = i3;
        }
    }
}
