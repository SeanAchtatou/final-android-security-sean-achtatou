package com.b.a.c;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;

public final class k implements ai {
    public static final k a = new k();

    public final void a(y yVar, Object obj, Object obj2, Type type) {
        int i = 0;
        am i2 = yVar.i();
        if (obj != null) {
            Type type2 = (!yVar.b(an.WriteClassName) || !(type instanceof ParameterizedType)) ? null : ((ParameterizedType) type).getActualTypeArguments()[0];
            Collection collection = (Collection) obj;
            ak b = yVar.b();
            yVar.a(b, obj, obj2);
            if (yVar.b(an.WriteClassName)) {
                if (HashSet.class == collection.getClass()) {
                    i2.append((CharSequence) "Set");
                } else if (TreeSet.class == collection.getClass()) {
                    i2.append((CharSequence) "TreeSet");
                }
            }
            try {
                i2.b('[');
                for (Object next : collection) {
                    int i3 = i + 1;
                    if (i != 0) {
                        i2.b(',');
                    }
                    if (next == null) {
                        i2.a();
                        i = i3;
                    } else {
                        Class<?> cls = next.getClass();
                        if (cls == Integer.class) {
                            i2.a(((Integer) next).intValue());
                            i = i3;
                        } else if (cls == Long.class) {
                            i2.a(((Long) next).longValue());
                            if (i2.b(an.WriteClassName)) {
                                i2.a('L');
                                i = i3;
                            } else {
                                i = i3;
                            }
                        } else {
                            yVar.a(cls).a(yVar, next, Integer.valueOf(i3 - 1), type2);
                            i = i3;
                        }
                    }
                }
                i2.b(']');
            } finally {
                yVar.a(b);
            }
        } else if (i2.b(an.WriteNullListAsEmpty)) {
            i2.write("[]");
        } else {
            i2.a();
        }
    }
}
