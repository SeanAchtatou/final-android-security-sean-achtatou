package com.b.a;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public class b extends a implements c, Serializable, Cloneable, List<Object>, RandomAccess {
    protected transient Object d;
    protected transient Type e;
    private final List<Object> f;

    public b() {
        this.f = new ArrayList(10);
    }

    public b(int i) {
        this.f = new ArrayList(i);
    }

    private b(List<Object> list) {
        this.f = list;
    }

    public final void a(Type type) {
        this.e = type;
    }

    public void add(int i, Object obj) {
        this.f.add(i, obj);
    }

    public boolean add(Object obj) {
        return this.f.add(obj);
    }

    public boolean addAll(int i, Collection<? extends Object> collection) {
        return this.f.addAll(i, collection);
    }

    public boolean addAll(Collection<? extends Object> collection) {
        return this.f.addAll(collection);
    }

    public final Object b() {
        return this.d;
    }

    public final Type c() {
        return this.e;
    }

    public final void c(Object obj) {
        this.d = obj;
    }

    public void clear() {
        this.f.clear();
    }

    public Object clone() {
        return new b(new ArrayList(this.f));
    }

    public boolean contains(Object obj) {
        return this.f.contains(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.f.containsAll(collection);
    }

    public boolean equals(Object obj) {
        return this.f.equals(obj);
    }

    public Object get(int i) {
        return this.f.get(i);
    }

    public int hashCode() {
        return this.f.hashCode();
    }

    public int indexOf(Object obj) {
        return this.f.indexOf(obj);
    }

    public boolean isEmpty() {
        return this.f.isEmpty();
    }

    public Iterator<Object> iterator() {
        return this.f.iterator();
    }

    public int lastIndexOf(Object obj) {
        return this.f.lastIndexOf(obj);
    }

    public ListIterator<Object> listIterator() {
        return this.f.listIterator();
    }

    public ListIterator<Object> listIterator(int i) {
        return this.f.listIterator(i);
    }

    public Object remove(int i) {
        return this.f.remove(i);
    }

    public boolean remove(Object obj) {
        return this.f.remove(obj);
    }

    public boolean removeAll(Collection<?> collection) {
        return this.f.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        return this.f.retainAll(collection);
    }

    public Object set(int i, Object obj) {
        return this.f.set(i, obj);
    }

    public int size() {
        return this.f.size();
    }

    public List<Object> subList(int i, int i2) {
        return this.f.subList(i, i2);
    }

    public Object[] toArray() {
        return this.f.toArray();
    }

    public <T> T[] toArray(T[] tArr) {
        return this.f.toArray(tArr);
    }
}
