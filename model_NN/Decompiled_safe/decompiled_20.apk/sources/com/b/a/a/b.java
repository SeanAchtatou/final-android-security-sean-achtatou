package com.b.a.a;

import com.b.a.c.an;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface b {
    String a() default "";

    String b() default "";

    boolean c() default true;

    boolean d() default true;

    an[] e() default {};
}
