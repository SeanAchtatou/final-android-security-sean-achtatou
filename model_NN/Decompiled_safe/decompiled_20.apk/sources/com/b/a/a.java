package com.b.a;

import com.b.a.b.a.u;
import com.b.a.b.c;
import com.b.a.b.d;
import com.b.a.b.e;
import com.b.a.b.f;
import com.b.a.b.j;
import com.b.a.c.am;
import com.b.a.c.an;
import com.b.a.c.y;
import com.b.a.d.g;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class a implements c, f {
    public static int a = ((((((((e.AutoCloseSource.a() | 0) | e.InternFieldNames.a()) | e.UseBigDecimal.a()) | e.AllowUnQuotedFieldNames.a()) | e.AllowSingleQuotes.a()) | e.AllowArbitraryCommas.a()) | e.SortFeidFastMatch.a()) | e.IgnoreNotMatch.a());
    public static String b = "yyyy-MM-dd HH:mm:ss";
    public static int c = ((((an.QuoteFieldNames.a() | 0) | an.SkipTransientField.a()) | an.WriteEnumUsingToString.a()) | an.SortField.a());

    private static <T> int a(c cVar) {
        if (cVar.a(e.DisableCircularReferenceDetect)) {
            return 0;
        }
        int size = cVar.g().size();
        for (int i = 0; i < size; i++) {
            d dVar = cVar.g().get(i);
            u c2 = dVar.c();
            Object obj = null;
            if (dVar.d() != null) {
                obj = dVar.d().a();
            }
            String b2 = dVar.b();
            c2.a(obj, b2.startsWith("$") ? cVar.a(b2) : dVar.a().a());
        }
        return size;
    }

    public static final e a(String str) {
        Object j;
        int i = a;
        if (str == null) {
            j = null;
        } else {
            c cVar = new c(str, j.a(), i);
            j = cVar.j();
            a(cVar);
            cVar.l();
        }
        return j instanceof e ? (e) j : (e) a(j, j.a());
    }

    public static final Object a(Object obj, j jVar) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof a) {
            return (a) obj;
        }
        if (obj instanceof Map) {
            Map map = (Map) obj;
            e eVar = new e(map.size());
            for (Map.Entry entry : map.entrySet()) {
                eVar.put(g.a(entry.getKey()), a(entry.getValue(), j.a()));
            }
            return eVar;
        } else if (obj instanceof Collection) {
            Collection<Object> collection = (Collection) obj;
            b bVar = new b(collection.size());
            for (Object a2 : collection) {
                bVar.add(a(a2, j.a()));
            }
            return bVar;
        } else {
            Class<?> cls = obj.getClass();
            if (cls.isEnum()) {
                return ((Enum) obj).name();
            }
            if (cls.isArray()) {
                int length = Array.getLength(obj);
                b bVar2 = new b(length);
                for (int i = 0; i < length; i++) {
                    bVar2.add(a(Array.get(obj, i), j.a()));
                }
                return bVar2;
            } else if (jVar.a(cls)) {
                return obj;
            } else {
                try {
                    List<com.b.a.d.c> a3 = g.a(cls);
                    e eVar2 = new e(a3.size());
                    for (com.b.a.d.c next : a3) {
                        eVar2.put(next.c(), a(next.a(obj), j.a()));
                    }
                    return eVar2;
                } catch (Exception e) {
                    throw new d("toJSON error", e);
                }
            }
        }
    }

    public static final <T> T a(String str, Class<T> cls) {
        e[] eVarArr = new e[0];
        j a2 = j.a();
        int i = a;
        if (str == null) {
            return null;
        }
        for (e b2 : eVarArr) {
            i = e.b(i, b2);
        }
        c cVar = new c(str, a2, i);
        T a3 = cVar.a((Type) cls);
        a(cVar);
        cVar.l();
        return a3;
    }

    public static final String a(Object obj) {
        return a(obj, new an[0]);
    }

    private static String a(Object obj, an... anVarArr) {
        am amVar = new am();
        try {
            y yVar = new y(amVar);
            for (an a2 : anVarArr) {
                yVar.a(a2);
            }
            yVar.c(obj);
            return amVar.toString();
        } finally {
            amVar.close();
        }
    }

    public static final Object b(Object obj) {
        return a(obj, j.a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void
     arg types: [java.lang.Class<T>, java.util.ArrayList]
     candidates:
      com.b.a.b.c.a(java.lang.Object, java.lang.Object):com.b.a.b.i
      com.b.a.b.c.a(com.b.a.b.i, int):void
      com.b.a.b.c.a(java.util.Collection, java.lang.Object):void
      com.b.a.b.c.a(java.util.Map, java.lang.Object):java.lang.Object
      com.b.a.b.c.a(java.lang.reflect.Type, java.util.Collection):void
      com.b.a.b.c.a(java.util.Map, java.lang.String):void
      com.b.a.b.c.a(java.lang.Class<?>, java.util.Collection):void */
    public static final <T> List<T> b(String str, Class<T> cls) {
        ArrayList arrayList = null;
        if (str != null) {
            c cVar = new c(str, j.a());
            f k = cVar.k();
            if (k.e() == 8) {
                k.l();
            } else {
                arrayList = new ArrayList();
                cVar.a((Class<?>) cls, (Collection) arrayList);
                a(cVar);
            }
            cVar.l();
        }
        return arrayList;
    }

    public final String a() {
        am amVar = new am();
        try {
            new y(amVar).c(this);
            return amVar.toString();
        } finally {
            amVar.close();
        }
    }

    public final void a(Appendable appendable) {
        am amVar = new am();
        try {
            new y(amVar).c(this);
            appendable.append(amVar.toString());
            amVar.close();
        } catch (IOException e) {
            throw new d(e.getMessage(), e);
        } catch (Throwable th) {
            amVar.close();
            throw th;
        }
    }

    public String toString() {
        return a();
    }
}
