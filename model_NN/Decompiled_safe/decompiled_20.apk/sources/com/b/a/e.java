package com.b.a;

import com.b.a.a.b;
import com.b.a.b.j;
import com.b.a.d.g;
import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class e extends a implements c, Serializable, Cloneable, InvocationHandler, Map<String, Object> {
    private final Map<String, Object> d;

    public e() {
        this(16, (byte) 0);
    }

    public e(int i) {
        this(i, (byte) 0);
    }

    private e(int i, byte b) {
        this.d = new HashMap(i);
    }

    public e(Map<String, Object> map) {
        this.d = map;
    }

    /* renamed from: a */
    public final Object put(String str, Object obj) {
        return this.d.put(str, obj);
    }

    public final e b(String str) {
        Object obj = this.d.get(str);
        return obj instanceof e ? (e) obj : (e) a.a(obj, j.a());
    }

    public final b c(String str) {
        Object obj = this.d.get(str);
        return obj instanceof b ? (b) obj : (b) a.a(obj, j.a());
    }

    public void clear() {
        this.d.clear();
    }

    public Object clone() {
        return new e(new HashMap(this.d));
    }

    public boolean containsKey(Object obj) {
        return this.d.containsKey(obj);
    }

    public boolean containsValue(Object obj) {
        return this.d.containsValue(obj);
    }

    public final int d(String str) {
        Object obj = get(str);
        if (obj == null) {
            return 0;
        }
        return g.j(obj).intValue();
    }

    public final Long e(String str) {
        return g.i(get(str));
    }

    public Set<Map.Entry<String, Object>> entrySet() {
        return this.d.entrySet();
    }

    public boolean equals(Object obj) {
        return this.d.equals(obj);
    }

    public final String f(String str) {
        Object obj = get(str);
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    public Object get(Object obj) {
        return this.d.get(obj);
    }

    public int hashCode() {
        return this.d.hashCode();
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        Class<?>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length == 1) {
            if (method.getReturnType() != Void.TYPE) {
                throw new d("illegal setter");
            }
            b bVar = (b) method.getAnnotation(b.class);
            String a = (bVar == null || bVar.a().length() == 0) ? null : bVar.a();
            if (a == null) {
                String name = method.getName();
                if (!name.startsWith("set")) {
                    throw new d("illegal setter");
                }
                String substring = name.substring(3);
                if (substring.length() == 0) {
                    throw new d("illegal setter");
                }
                a = Character.toLowerCase(substring.charAt(0)) + substring.substring(1);
            }
            this.d.put(a, objArr[0]);
            return null;
        } else if (parameterTypes.length != 0) {
            throw new UnsupportedOperationException(method.toGenericString());
        } else if (method.getReturnType() == Void.TYPE) {
            throw new d("illegal getter");
        } else {
            b bVar2 = (b) method.getAnnotation(b.class);
            String a2 = (bVar2 == null || bVar2.a().length() == 0) ? null : bVar2.a();
            if (a2 == null) {
                String name2 = method.getName();
                if (name2.startsWith("get")) {
                    String substring2 = name2.substring(3);
                    if (substring2.length() == 0) {
                        throw new d("illegal getter");
                    }
                    a2 = Character.toLowerCase(substring2.charAt(0)) + substring2.substring(1);
                } else if (name2.startsWith("is")) {
                    String substring3 = name2.substring(2);
                    if (substring3.length() == 0) {
                        throw new d("illegal getter");
                    }
                    a2 = Character.toLowerCase(substring3.charAt(0)) + substring3.substring(1);
                } else {
                    throw new d("illegal getter");
                }
            }
            return g.a(this.d.get(a2), method.getGenericReturnType(), j.a());
        }
    }

    public boolean isEmpty() {
        return this.d.isEmpty();
    }

    public Set<String> keySet() {
        return this.d.keySet();
    }

    public void putAll(Map<? extends String, ? extends Object> map) {
        this.d.putAll(map);
    }

    public Object remove(Object obj) {
        return this.d.remove(obj);
    }

    public int size() {
        return this.d.size();
    }

    public Collection<Object> values() {
        return this.d.values();
    }
}
