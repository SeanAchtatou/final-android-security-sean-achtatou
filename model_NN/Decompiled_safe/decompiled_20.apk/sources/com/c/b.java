package com.c;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.c.a.a;
import com.c.b;
import com.c.b.c;
import com.c.b.e;
import com.c.b.f;
import com.c.c.d;
import java.util.Map;
import java.util.WeakHashMap;

public abstract class b<T extends b<T>> implements d {
    private static final Class<?>[] k = {View.class};
    private static Class<?>[] l = {AdapterView.class, View.class, Integer.TYPE, Long.TYPE};
    private static Class<?>[] m = {AbsListView.class, Integer.TYPE};
    private static final Class<?>[] n = {CharSequence.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
    private static Class<?>[] o = {Integer.TYPE, Integer.TYPE};
    private static final Class<?>[] p = {Integer.TYPE};
    private static Class<?>[] q = {Integer.TYPE, Paint.class};
    private static WeakHashMap<Dialog, Void> r = new WeakHashMap<>();
    protected View a;
    protected Object b;
    protected a c;
    private View f;
    private Activity g;
    private Context h;
    private f i;
    private int j = 0;

    public b(Activity activity) {
        this.g = activity;
    }

    public b(Context context) {
        this.h = context;
    }

    public b(View view) {
        this.f = view;
        this.a = view;
    }

    private Context a() {
        return this.g != null ? this.g : this.f != null ? this.f.getContext() : this.h;
    }

    private <K> T a(c<K> cVar) {
        Log.d("AQuery.ajax", cVar.d());
        cVar.a(this.c);
        cVar.a(this.b);
        cVar.a(this.i);
        cVar.a(this.j);
        if (this.g != null) {
            cVar.a(this.g);
        } else {
            cVar.a(a());
        }
        b();
        return this;
    }

    private void b() {
        this.c = null;
        this.b = null;
        this.i = null;
        this.j = 0;
    }

    public final T a(Dialog dialog) {
        if (dialog != null) {
            try {
                dialog.show();
                r.put(dialog, null);
            } catch (Exception e) {
                com.c.c.a.a((Throwable) e);
            }
        }
        return this;
    }

    public final T a(View view) {
        this.a = view;
        b();
        return this;
    }

    public final T a(f fVar) {
        this.i = fVar;
        return this;
    }

    public final T a(String str, int i2) {
        Log.d("AQuery.image", str);
        if (this.a instanceof ImageView) {
            e.a(this.g, a(), (ImageView) this.a, str, i2, this.b, this.c, this.j);
            b();
        }
        return this;
    }

    public final <K> T a(String str, Class<K> cls, Object obj, String str2, boolean z) {
        c cVar = new c();
        ((c) ((c) ((c) cVar.a((Class) cls)).a(obj, str2)).a()).a(z);
        ((c) cVar.a((Class) cls)).a(str);
        return a(cVar);
    }

    public final <K> T a(String str, Map<String, ?> map, Class<K> cls, Object obj, String str2, boolean z) {
        c cVar = new c();
        ((c) ((c) cVar.a((Class) cls)).a(obj, str2)).a(z);
        ((c) ((c) cVar.a((Class) cls)).a(str)).a(map);
        return a(cVar);
    }

    public final T b(Dialog dialog) {
        if (dialog != null) {
            try {
                r.remove(dialog);
                dialog.dismiss();
            } catch (Exception e) {
                com.c.c.a.a((Throwable) e);
            }
        }
        return this;
    }
}
