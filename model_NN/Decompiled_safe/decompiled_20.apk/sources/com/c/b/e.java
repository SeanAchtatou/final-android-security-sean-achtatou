package com.c.b;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.c.c.a;
import com.c.c.b;
import com.c.c.c;
import com.c.c.g;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class e extends a<Bitmap, e> {
    private static Bitmap A = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
    private static Bitmap B = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
    private static int e = 20;
    private static int f = 20;
    private static int g = 2500;
    private static int h = 160000;
    private static int i = 1000000;
    private static boolean j = false;
    private static Map<String, Bitmap> k;
    private static Map<String, Bitmap> l;
    private static Map<String, Bitmap> m;
    private static HashMap<String, WeakHashMap<ImageView, e>> n = new HashMap<>();
    private WeakReference<ImageView> o;
    private int p;
    private int q;
    private File r;
    private Bitmap s;
    private int t;
    private Bitmap u;
    private float v;
    private int w;
    private boolean x = true;
    private float y = Float.MAX_VALUE;
    private boolean z;

    public e() {
        ((e) ((e) ((e) a(Bitmap.class)).b()).a()).a(PoiTypeDef.All);
    }

    private static int a(int i2, int i3) {
        int i4 = 1;
        for (int i5 = 0; i5 < 10 && i2 >= i3 * 2; i5++) {
            i2 /= 2;
            i4 *= 2;
        }
        return i4;
    }

    private static Bitmap a(String str, int i2, int i3) {
        String b = b(str, i2, i3);
        Bitmap bitmap = f().get(b);
        if (bitmap == null) {
            bitmap = g().get(b);
        }
        if (bitmap != null) {
            return bitmap;
        }
        Bitmap bitmap2 = h().get(b);
        if (bitmap2 == null || e() != 200) {
            return bitmap2;
        }
        m = null;
        return null;
    }

    private static Bitmap a(String str, BitmapFactory.Options options) {
        FileInputStream fileInputStream;
        Throwable th;
        Bitmap bitmap = null;
        if (options == null) {
            options = new BitmapFactory.Options();
        }
        options.inInputShareable = true;
        options.inPurgeable = true;
        try {
            fileInputStream = new FileInputStream(str);
            try {
                bitmap = BitmapFactory.decodeFileDescriptor(fileInputStream.getFD(), null, options);
                a.a((Closeable) fileInputStream);
            } catch (IOException e2) {
                e = e2;
                try {
                    a.b(e);
                    a.a((Closeable) fileInputStream);
                    return bitmap;
                } catch (Throwable th2) {
                    th = th2;
                    a.a((Closeable) fileInputStream);
                    throw th;
                }
            }
        } catch (IOException e3) {
            e = e3;
            fileInputStream = null;
            a.b(e);
            a.a((Closeable) fileInputStream);
            return bitmap;
        } catch (Throwable th3) {
            fileInputStream = null;
            th = th3;
            a.a((Closeable) fileInputStream);
            throw th;
        }
        return bitmap;
    }

    private Bitmap a(String str, byte[] bArr) {
        return a(str, bArr, this.p, this.x, this.w);
    }

    private static Bitmap a(String str, byte[] bArr, int i2, boolean z2, int i3) {
        BitmapFactory.Options options;
        Bitmap bitmap = null;
        if (i2 > 0) {
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inJustDecodeBounds = true;
            a(str, bArr, options2);
            int i4 = options2.outWidth;
            if (!z2) {
                i4 = Math.max(i4, options2.outHeight);
            }
            int a = a(i4, i2);
            options = new BitmapFactory.Options();
            options.inSampleSize = a;
        } else {
            options = null;
        }
        try {
            bitmap = a(str, bArr, options);
        } catch (OutOfMemoryError e2) {
            l = null;
            k = null;
            m = null;
            a.b(e2);
        }
        if (i3 <= 0) {
            return bitmap;
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        float f2 = (float) i3;
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, f2, f2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    private static Bitmap a(String str, byte[] bArr, BitmapFactory.Options options) {
        Bitmap bitmap = null;
        if (str != null) {
            bitmap = a(str, options);
        } else if (bArr != null) {
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        }
        if (bitmap == null && options != null && !options.inJustDecodeBounds) {
            a.b("decode image failed", str);
        }
        return bitmap;
    }

    private static Drawable a(ImageView imageView, Bitmap bitmap, float f2, float f3) {
        return f2 > BitmapDescriptorFactory.HUE_RED ? new g(imageView.getResources(), bitmap, imageView, f2, f3) : new BitmapDrawable(imageView.getResources(), bitmap);
    }

    public static void a(Activity activity, Context context, ImageView imageView, String str, int i2, Object obj, com.c.a.a aVar, int i3) {
        Bitmap a = a(str, i2, 0);
        if (a != null) {
            imageView.setTag(1090453505, str);
            c.a(obj, str, false);
            a(imageView, a, (Bitmap) null, 0, -1, (float) BitmapDescriptorFactory.HUE_RED, Float.MAX_VALUE, 4);
            return;
        }
        e eVar = new e();
        e eVar2 = (e) eVar.a(str);
        eVar2.o = new WeakReference<>(imageView);
        e eVar3 = (e) ((e) eVar2.b()).a();
        eVar3.p = i2;
        eVar3.q = 0;
        eVar3.u = null;
        eVar3.t = -1;
        eVar3.v = BitmapDescriptorFactory.HUE_RED;
        eVar3.y = Float.MAX_VALUE;
        ((e) ((e) ((e) eVar3.a(obj)).a(aVar)).a(i3)).w = 0;
        if (activity != null) {
            eVar.a(activity);
        } else {
            eVar.a(context);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.widget.ImageView r6, android.graphics.Bitmap r7, android.graphics.Bitmap r8, int r9, int r10, float r11, float r12, int r13) {
        /*
            r4 = 0
            r1 = 0
            r3 = 1
            if (r7 == 0) goto L_0x0016
            int r0 = r7.getWidth()
            if (r0 != r3) goto L_0x0016
            int r0 = r7.getHeight()
            if (r0 != r3) goto L_0x0016
            android.graphics.Bitmap r0 = com.c.b.e.A
            if (r7 == r0) goto L_0x0016
            r7 = r1
        L_0x0016:
            if (r7 == 0) goto L_0x0021
            r6.setVisibility(r4)
        L_0x001b:
            if (r7 != 0) goto L_0x0032
            r6.setImageBitmap(r1)
        L_0x0020:
            return
        L_0x0021:
            r0 = -2
            if (r9 != r0) goto L_0x002a
            r0 = 8
            r6.setVisibility(r0)
            goto L_0x001b
        L_0x002a:
            r0 = -1
            if (r9 != r0) goto L_0x001b
            r0 = 4
            r6.setVisibility(r0)
            goto L_0x001b
        L_0x0032:
            android.graphics.drawable.Drawable r2 = a(r6, r7, r11, r12)
            switch(r10) {
                case -3: goto L_0x0065;
                case -2: goto L_0x006a;
                case -1: goto L_0x0063;
                default: goto L_0x0039;
            }
        L_0x0039:
            r0 = r4
        L_0x003a:
            if (r0 == 0) goto L_0x0089
            if (r8 != 0) goto L_0x006e
            android.view.animation.AlphaAnimation r0 = new android.view.animation.AlphaAnimation
            r3 = 0
            r4 = 1065353216(0x3f800000, float:1.0)
            r0.<init>(r3, r4)
            android.view.animation.DecelerateInterpolator r3 = new android.view.animation.DecelerateInterpolator
            r3.<init>()
            r0.setInterpolator(r3)
            r3 = 300(0x12c, double:1.48E-321)
            r0.setDuration(r3)
        L_0x0053:
            r6.setImageDrawable(r2)
            if (r0 == 0) goto L_0x0094
            long r1 = android.view.animation.AnimationUtils.currentAnimationTimeMillis()
            r0.setStartTime(r1)
            r6.startAnimation(r0)
            goto L_0x0020
        L_0x0063:
            r0 = r3
            goto L_0x003a
        L_0x0065:
            r0 = 3
            if (r13 != r0) goto L_0x006a
            r0 = r3
            goto L_0x003a
        L_0x006a:
            if (r13 != r3) goto L_0x0039
            r0 = r3
            goto L_0x003a
        L_0x006e:
            android.graphics.drawable.Drawable r0 = a(r6, r8, r11, r12)
            r5 = 2
            android.graphics.drawable.Drawable[] r5 = new android.graphics.drawable.Drawable[r5]
            r5[r4] = r0
            r5[r3] = r2
            android.graphics.drawable.TransitionDrawable r0 = new android.graphics.drawable.TransitionDrawable
            r0.<init>(r5)
            r0.setCrossFadeEnabled(r3)
            r2 = 300(0x12c, float:4.2E-43)
            r0.startTransition(r2)
            r2 = r0
            r0 = r1
            goto L_0x0053
        L_0x0089:
            if (r10 <= 0) goto L_0x0098
            android.content.Context r0 = r6.getContext()
            android.view.animation.Animation r0 = android.view.animation.AnimationUtils.loadAnimation(r0, r10)
            goto L_0x0053
        L_0x0094:
            r6.setAnimation(r1)
            goto L_0x0020
        L_0x0098:
            r0 = r1
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.c.b.e.a(android.widget.ImageView, android.graphics.Bitmap, android.graphics.Bitmap, int, int, float, float, int):void");
    }

    private void a(ImageView imageView, Bitmap bitmap, boolean z2) {
        if (bitmap == null) {
            imageView.setImageDrawable(null);
        } else if (z2) {
            imageView.setImageDrawable(a(imageView, bitmap, this.v, this.y));
        } else if (this.b != null) {
            a(imageView, bitmap, this.u, this.q, this.t, this.v, this.y, this.b.l());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.b.e.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.c.b.e.a(java.lang.String, int, int):android.graphics.Bitmap
      com.c.b.e.a(java.lang.String, byte[], android.graphics.BitmapFactory$Options):android.graphics.Bitmap
      com.c.b.e.a(java.lang.String, android.graphics.Bitmap, com.c.b.d):void
      com.c.b.e.a(java.lang.String, java.io.File, com.c.b.d):java.lang.Object
      com.c.b.e.a(java.lang.String, byte[], com.c.b.d):java.lang.Object
      com.c.b.e.a(java.lang.String, java.lang.Object, com.c.b.d):void
      com.c.b.a.a(byte[], java.lang.String, com.c.b.d):java.lang.String
      com.c.b.a.a(java.lang.String, java.io.File, com.c.b.d):T
      com.c.b.a.a(java.lang.String, byte[], com.c.b.d):T
      com.c.b.a.a(java.lang.String, java.lang.Object, com.c.b.d):void
      com.c.b.e.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    private void a(e eVar, String str, ImageView imageView, Bitmap bitmap) {
        if (imageView != null && eVar != null) {
            if (str.equals(imageView.getTag(1090453505))) {
                if (imageView instanceof ImageView) {
                    eVar.a(imageView, bitmap, false);
                } else {
                    a(imageView, bitmap, false);
                }
            }
            b(false);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, Bitmap bitmap) {
        int i2 = this.p;
        int i3 = this.w;
        boolean z2 = this.z;
        if (bitmap != null) {
            Map<String, Bitmap> h2 = z2 ? h() : bitmap.getWidth() * bitmap.getHeight() <= g ? g() : f();
            if (i2 > 0 || i3 > 0) {
                h2.put(b(str, i2, i3), bitmap);
                if (!h2.containsKey(str)) {
                    h2.put(str, null);
                    return;
                }
                return;
            }
            h2.put(str, bitmap);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, Bitmap bitmap, d dVar) {
        ImageView imageView = this.o.get();
        WeakHashMap remove = n.remove(str);
        if (remove == null || !remove.containsKey(imageView)) {
            a(this, str, imageView, bitmap);
        }
        if (remove != null) {
            for (ImageView imageView2 : remove.keySet()) {
                e eVar = (e) remove.get(imageView2);
                eVar.b = dVar;
                a(eVar, str, imageView2, bitmap);
            }
        }
    }

    private void a(String str, ImageView imageView) {
        WeakHashMap weakHashMap = n.get(str);
        if (weakHashMap != null) {
            weakHashMap.put(imageView, this);
        } else if (n.containsKey(str)) {
            WeakHashMap weakHashMap2 = new WeakHashMap();
            weakHashMap2.put(imageView, this);
            n.put(str, weakHashMap2);
        } else {
            n.put(str, null);
        }
    }

    private static String b(String str, int i2, int i3) {
        String str2 = i2 > 0 ? str + "#" + i2 : str;
        return i3 > 0 ? str2 + "#" + i3 : str2;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public Bitmap b(String str) {
        if (this.s != null) {
            return this.s;
        }
        if (!this.d) {
            return null;
        }
        return a(str, this.p, this.w);
    }

    private static Map<String, Bitmap> f() {
        if (l == null) {
            l = Collections.synchronizedMap(new b(f, h, i));
        }
        return l;
    }

    private static Map<String, Bitmap> g() {
        if (k == null) {
            k = Collections.synchronizedMap(new b(e, g, 250000));
        }
        return k;
    }

    private static Map<String, Bitmap> h() {
        if (m == null) {
            m = Collections.synchronizedMap(new b(100, h, 250000));
        }
        return m;
    }

    /* access modifiers changed from: protected */
    public final File a(File file, String str) {
        return (this.r == null || !this.r.exists()) ? super.a(file, str) : this.r;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(String str, File file, d dVar) {
        return a(file.getAbsolutePath(), (byte[]) null);
    }

    public final /* synthetic */ Object a(String str, byte[] bArr, d dVar) {
        String num;
        Bitmap bitmap = null;
        File k2 = dVar.k();
        Bitmap a = a(k2 != null ? k2.getAbsolutePath() : null, bArr);
        if (a != null) {
            return a;
        }
        if (this.q > 0) {
            View view = this.o.get();
            if (!(view == null || (bitmap = b((num = Integer.toString(this.q)))) != null || (bitmap = BitmapFactory.decodeResource(view.getResources(), this.q)) == null)) {
                a(num, bitmap);
            }
        } else {
            bitmap = (this.q == -2 || this.q == -1) ? B : this.q == -3 ? this.u : a;
        }
        if (dVar.h() == 200) {
            return bitmap;
        }
        this.z = true;
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.b.e.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [android.widget.ImageView, ?[OBJECT, ARRAY], int]
     candidates:
      com.c.b.e.a(java.lang.String, int, int):android.graphics.Bitmap
      com.c.b.e.a(java.lang.String, byte[], android.graphics.BitmapFactory$Options):android.graphics.Bitmap
      com.c.b.e.a(java.lang.String, android.graphics.Bitmap, com.c.b.d):void
      com.c.b.e.a(java.lang.String, java.io.File, com.c.b.d):java.lang.Object
      com.c.b.e.a(java.lang.String, byte[], com.c.b.d):java.lang.Object
      com.c.b.e.a(java.lang.String, java.lang.Object, com.c.b.d):void
      com.c.b.a.a(byte[], java.lang.String, com.c.b.d):java.lang.String
      com.c.b.a.a(java.lang.String, java.io.File, com.c.b.d):T
      com.c.b.a.a(java.lang.String, byte[], com.c.b.d):T
      com.c.b.a.a(java.lang.String, java.lang.Object, com.c.b.d):void
      com.c.b.e.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.b.e.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.c.b.e.a(java.lang.String, int, int):android.graphics.Bitmap
      com.c.b.e.a(java.lang.String, byte[], android.graphics.BitmapFactory$Options):android.graphics.Bitmap
      com.c.b.e.a(java.lang.String, android.graphics.Bitmap, com.c.b.d):void
      com.c.b.e.a(java.lang.String, java.io.File, com.c.b.d):java.lang.Object
      com.c.b.e.a(java.lang.String, byte[], com.c.b.d):java.lang.Object
      com.c.b.e.a(java.lang.String, java.lang.Object, com.c.b.d):void
      com.c.b.a.a(byte[], java.lang.String, com.c.b.d):java.lang.String
      com.c.b.a.a(java.lang.String, java.io.File, com.c.b.d):T
      com.c.b.a.a(java.lang.String, byte[], com.c.b.d):T
      com.c.b.a.a(java.lang.String, java.lang.Object, com.c.b.d):void
      com.c.b.e.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    public final void a(Context context) {
        String d = d();
        ImageView imageView = this.o.get();
        if (d == null) {
            b(false);
            a(imageView, (Bitmap) null, false);
            return;
        }
        Bitmap c = b(d);
        if (c != null) {
            imageView.setTag(1090453505, d);
            this.b = new d().a(4).b();
            a(d, c, this.b);
            return;
        }
        if (!d.equals(imageView.getTag(1090453505)) || this.u != null) {
            imageView.setTag(1090453505, d);
            if (this.u == null || b(imageView.getContext())) {
                a(imageView, (Bitmap) null, true);
            } else {
                a(imageView, this.u, true);
            }
        }
        if (!n.containsKey(d)) {
            a(d, imageView);
            super.a(imageView.getContext());
            return;
        }
        b(true);
        a(d, imageView);
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        return !j;
    }
}
