package com.c.b;

import com.c.c.a;
import java.io.Closeable;
import java.io.File;
import java.util.Date;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.Header;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

public class d {
    private int a = HttpStatus.SC_OK;
    private String b = "OK";
    private String c;
    private byte[] d;
    private File e;
    private Date f = new Date();
    private boolean g;
    private DefaultHttpClient h;
    private long i;
    private int j = 1;
    private long k = System.currentTimeMillis();
    private boolean l;
    private boolean m;
    private boolean n;
    private String o;
    private HttpContext p;
    private Header[] q;
    private Closeable r;

    /* access modifiers changed from: protected */
    public final d a() {
        this.n = true;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(int i2) {
        this.j = i2;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(File file) {
        this.e = file;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(String str) {
        this.o = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(Date date) {
        this.f = date;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(DefaultHttpClient defaultHttpClient) {
        this.h = defaultHttpClient;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(HttpContext httpContext) {
        this.p = httpContext;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(boolean z) {
        this.g = z;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(byte[] bArr) {
        this.d = bArr;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d a(Header[] headerArr) {
        this.q = headerArr;
        return this;
    }

    /* access modifiers changed from: protected */
    public final void a(Closeable closeable) {
        this.r = closeable;
    }

    /* access modifiers changed from: protected */
    public final d b() {
        this.i = System.currentTimeMillis() - this.k;
        this.l = true;
        this.n = false;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d b(int i2) {
        this.a = i2;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d b(String str) {
        this.b = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public final d c() {
        this.i = System.currentTimeMillis() - this.k;
        this.l = false;
        d();
        return this;
    }

    /* access modifiers changed from: protected */
    public final d c(String str) {
        this.c = str;
        return this;
    }

    public final String d(String str) {
        if (this.q == null) {
            return null;
        }
        for (int i2 = 0; i2 < this.q.length; i2++) {
            if (str.equalsIgnoreCase(this.q[i2].getName())) {
                return this.q[i2].getValue();
            }
        }
        return null;
    }

    public final void d() {
        a.a(this.r);
        this.r = null;
    }

    /* access modifiers changed from: protected */
    public final boolean e() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final boolean f() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final boolean g() {
        return this.m;
    }

    public final int h() {
        return this.a;
    }

    public final String i() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final byte[] j() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final File k() {
        return this.e;
    }

    public final int l() {
        return this.j;
    }

    public final String m() {
        return this.o;
    }
}
