package com.c.b;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.c.c.c;
import com.c.c.d;
import com.c.c.h;
import com.umeng.common.b.e;
import com.umeng.fb.f;
import com.zhongsou.flymall.AppContext;
import com.zhongsou.flymall.e.n;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.xmlpull.v1.XmlPullParser;

public abstract class a<T, K> implements Runnable {
    private static f F;
    private static final Class<?>[] H = {String.class, Object.class, d.class};
    private static ExecutorService L;
    private static SocketFactory M;
    private static DefaultHttpClient N;
    private static int O = HttpStatus.SC_OK;
    private static int e = 20000;
    private static String f = null;
    private static int g = 1;
    private static boolean h = true;
    private static boolean i = true;
    private String A = e.f;
    private WeakReference<Activity> B;
    private int C = 4;
    private HttpUriRequest D;
    private boolean E = true;
    private HttpHost G;
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean P;
    protected T a;
    protected d b;
    protected boolean c;
    protected boolean d;
    private Class<T> j;
    private Reference<Object> k;
    private Object l;
    private String m;
    private WeakReference<Object> n;
    /* access modifiers changed from: private */
    public String o;
    private Map<String, Object> p;
    private Map<String, String> q;
    private Map<String, String> r;
    private f s;
    private int t = 0;
    private File u;
    private File v;
    private com.c.a.a w;
    private boolean x;
    private int y = 0;
    private long z;

    private static String a(HttpEntity httpEntity) {
        Header contentEncoding;
        if (httpEntity == null || (contentEncoding = httpEntity.getContentEncoding()) == null) {
            return null;
        }
        return contentEncoding.getValue();
    }

    private static String a(byte[] bArr, String str, d dVar) {
        Exception e2;
        String str2;
        String str3 = null;
        try {
            if (!"utf-8".equalsIgnoreCase(str)) {
                return new String(bArr, str);
            }
            String c2 = c(dVar.d("Content-Type"));
            com.c.c.a.b("parsing header", c2);
            if (c2 != null) {
                return new String(bArr, c2);
            }
            String str4 = new String(bArr, "utf-8");
            try {
                Matcher matcher = Pattern.compile("<meta [^>]*http-equiv[^>]*\"Content-Type\"[^>]*>", 2).matcher(str4);
                if (matcher.find()) {
                    str3 = c(matcher.group());
                }
                com.c.c.a.b("parsing needed", str3);
                if (str3 == null || "utf-8".equalsIgnoreCase(str3)) {
                    return str4;
                }
                com.c.c.a.b("correction needed", str3);
                str2 = new String(bArr, str3);
                try {
                    dVar.a(str2.getBytes("utf-8"));
                    return str2;
                } catch (Exception e3) {
                    e2 = e3;
                }
            } catch (Exception e4) {
                Exception exc = e4;
                str2 = str4;
                e2 = exc;
            }
        } catch (Exception e5) {
            e2 = e5;
            str2 = null;
            com.c.c.a.b(e2);
            return str2;
        }
    }

    private static Map<String, Object> a(Uri uri) {
        HashMap hashMap = new HashMap();
        for (String split : uri.getQuery().split("&")) {
            String[] split2 = split.split("=");
            if (split2.length >= 2) {
                hashMap.put(split2[0], split2[1]);
            } else if (split2.length == 1) {
                hashMap.put(split2[0], PoiTypeDef.All);
            }
        }
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.c.a.a(java.io.InputStream, java.io.OutputStream):void
     arg types: [java.io.InputStream, java.io.DataOutputStream]
     candidates:
      com.c.c.a.a(android.content.Context, int):java.io.File
      com.c.c.a.a(java.io.File, java.lang.String):java.io.File
      com.c.c.a.a(java.io.File, byte[]):void
      com.c.c.a.a(java.lang.Object, java.lang.Object):void
      com.c.c.a.a(java.io.File[], long):void
      com.c.c.a.a(java.io.InputStream, java.io.OutputStream):void */
    private static void a(DataOutputStream dataOutputStream, String str, String str2, InputStream inputStream) {
        dataOutputStream.writeBytes("--*****\r\n");
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + str2 + "\"\r\n");
        dataOutputStream.writeBytes("\r\n");
        com.c.c.a.a(inputStream, (OutputStream) dataOutputStream);
        dataOutputStream.writeBytes("\r\n");
    }

    private void a(String str, Map<String, String> map, Map<String, Object> map2, d dVar) {
        byte[] bArr;
        String str2;
        com.c.c.a.b("multipart", str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setConnectTimeout(e * 4);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data;charset=utf-8;boundary=*****");
        if (map != null) {
            for (String next : map.keySet()) {
                httpURLConnection.setRequestProperty(next, map.get(next));
            }
        }
        String l2 = l();
        if (l2 != null) {
            httpURLConnection.setRequestProperty("Cookie", l2);
        }
        if (this.w != null) {
            com.c.a.a aVar = this.w;
        }
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        for (Map.Entry next2 : map2.entrySet()) {
            String str3 = (String) next2.getKey();
            Object value = next2.getValue();
            if (value != null) {
                if (value instanceof File) {
                    File file = (File) value;
                    a(dataOutputStream, str3, file.getName(), new FileInputStream(file));
                } else if (value instanceof byte[]) {
                    a(dataOutputStream, str3, str3, new ByteArrayInputStream((byte[]) value));
                } else if (value instanceof InputStream) {
                    a(dataOutputStream, str3, str3, (InputStream) value);
                } else {
                    String obj = value.toString();
                    dataOutputStream.writeBytes("--*****\r\n");
                    dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + str3 + "\"");
                    dataOutputStream.writeBytes("\r\n");
                    dataOutputStream.writeBytes("\r\n");
                    dataOutputStream.write(obj.getBytes(e.f));
                    dataOutputStream.writeBytes("\r\n");
                }
            }
        }
        dataOutputStream.writeBytes("--*****--\r\n");
        dataOutputStream.flush();
        dataOutputStream.close();
        httpURLConnection.connect();
        int responseCode = httpURLConnection.getResponseCode();
        String responseMessage = httpURLConnection.getResponseMessage();
        String contentEncoding = httpURLConnection.getContentEncoding();
        if (responseCode < 200 || responseCode >= 300) {
            String str4 = new String(a(contentEncoding, httpURLConnection.getErrorStream()), e.f);
            com.c.c.a.b(f.an, str4);
            str2 = str4;
            bArr = null;
        } else {
            bArr = a(contentEncoding, httpURLConnection.getInputStream());
            str2 = null;
        }
        com.c.c.a.b("response", Integer.valueOf(responseCode));
        if (bArr != null) {
            com.c.c.a.b(Integer.valueOf(bArr.length), str);
        }
        dVar.b(responseCode).b(responseMessage).c(str).a(new Date()).a(bArr).a(str2).a((DefaultHttpClient) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d):void
     arg types: [org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d]
     candidates:
      com.c.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.c.b.a.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>, java.util.Map<java.lang.String, java.lang.Object>, com.c.b.d):void
      com.c.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d):void */
    private void a(String str, HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, Map<String, String> map, Map<String, Object> map2, d dVar) {
        HttpEntity urlEncodedFormEntity;
        httpEntityEnclosingRequestBase.getParams().setBooleanParameter(HttpMethodParams.USE_EXPECT_CONTINUE, false);
        Object obj = map2.get(d.d);
        if (obj instanceof HttpEntity) {
            urlEncodedFormEntity = (HttpEntity) obj;
        } else {
            ArrayList arrayList = new ArrayList();
            for (Map.Entry next : map2.entrySet()) {
                Object value = next.getValue();
                if (value != null) {
                    arrayList.add(new BasicNameValuePair((String) next.getKey(), value.toString()));
                }
            }
            urlEncodedFormEntity = new UrlEncodedFormEntity(arrayList, e.f);
        }
        if (map != null && !map.containsKey("Content-Type")) {
            map.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        }
        httpEntityEnclosingRequestBase.setEntity(urlEncodedFormEntity);
        a((HttpUriRequest) httpEntityEnclosingRequestBase, str, map, dVar);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0178  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(org.apache.http.client.methods.HttpUriRequest r21, java.lang.String r22, java.util.Map<java.lang.String, java.lang.String> r23, com.c.b.d r24) {
        /*
            r20 = this;
            java.lang.String r2 = "AQuery.httpDo"
            r0 = r22
            android.util.Log.d(r2, r0)
            java.lang.String r2 = com.c.b.a.f
            if (r2 == 0) goto L_0x0014
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = com.c.b.a.f
            r0 = r21
            r0.addHeader(r2, r3)
        L_0x0014:
            if (r23 == 0) goto L_0x0038
            java.util.Set r2 = r23.keySet()
            java.util.Iterator r4 = r2.iterator()
        L_0x001e:
            boolean r2 = r4.hasNext()
            if (r2 == 0) goto L_0x0038
            java.lang.Object r2 = r4.next()
            java.lang.String r2 = (java.lang.String) r2
            r0 = r23
            java.lang.Object r3 = r0.get(r2)
            java.lang.String r3 = (java.lang.String) r3
            r0 = r21
            r0.addHeader(r2, r3)
            goto L_0x001e
        L_0x0038:
            boolean r2 = com.c.b.a.h
            if (r2 == 0) goto L_0x0051
            if (r23 == 0) goto L_0x0048
            java.lang.String r2 = "Accept-Encoding"
            r0 = r23
            boolean r2 = r0.containsKey(r2)
            if (r2 != 0) goto L_0x0051
        L_0x0048:
            java.lang.String r2 = "Accept-Encoding"
            java.lang.String r3 = "gzip"
            r0 = r21
            r0.addHeader(r2, r3)
        L_0x0051:
            java.lang.String r2 = r20.l()
            if (r2 == 0) goto L_0x005e
            java.lang.String r3 = "Cookie"
            r0 = r21
            r0.addHeader(r3, r2)
        L_0x005e:
            r0 = r20
            com.c.a.a r2 = r0.w
            if (r2 == 0) goto L_0x0068
            r0 = r20
            com.c.a.a r2 = r0.w
        L_0x0068:
            org.apache.http.impl.client.DefaultHttpClient r2 = com.c.b.a.N
            if (r2 == 0) goto L_0x0070
            boolean r2 = com.c.b.a.i
            if (r2 != 0) goto L_0x00c8
        L_0x0070:
            java.lang.String r2 = "creating http client"
            com.c.c.a.a(r2)
            org.apache.http.params.BasicHttpParams r3 = new org.apache.http.params.BasicHttpParams
            r3.<init>()
            int r2 = com.c.b.a.e
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r3, r2)
            int r2 = com.c.b.a.e
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r3, r2)
            org.apache.http.conn.params.ConnPerRouteBean r2 = new org.apache.http.conn.params.ConnPerRouteBean
            r4 = 25
            r2.<init>(r4)
            org.apache.http.conn.params.ConnManagerParams.setMaxConnectionsPerRoute(r3, r2)
            r2 = 8192(0x2000, float:1.14794E-41)
            org.apache.http.params.HttpConnectionParams.setSocketBufferSize(r3, r2)
            org.apache.http.conn.scheme.SchemeRegistry r4 = new org.apache.http.conn.scheme.SchemeRegistry
            r4.<init>()
            org.apache.http.conn.scheme.Scheme r2 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r5 = "http"
            org.apache.http.conn.scheme.PlainSocketFactory r6 = org.apache.http.conn.scheme.PlainSocketFactory.getSocketFactory()
            r7 = 80
            r2.<init>(r5, r6, r7)
            r4.register(r2)
            org.apache.http.conn.scheme.Scheme r5 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r6 = "https"
            org.apache.http.conn.scheme.SocketFactory r2 = com.c.b.a.M
            if (r2 != 0) goto L_0x0127
            org.apache.http.conn.ssl.SSLSocketFactory r2 = org.apache.http.conn.ssl.SSLSocketFactory.getSocketFactory()
        L_0x00b4:
            r7 = 443(0x1bb, float:6.21E-43)
            r5.<init>(r6, r2, r7)
            r4.register(r5)
            org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager r2 = new org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
            r2.<init>(r3, r4)
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient
            r4.<init>(r2, r3)
            com.c.b.a.N = r4
        L_0x00c8:
            org.apache.http.impl.client.DefaultHttpClient r10 = com.c.b.a.N
            org.apache.http.params.HttpParams r2 = r21.getParams()
            r0 = r20
            org.apache.http.HttpHost r3 = r0.G
            if (r3 == 0) goto L_0x00dd
            java.lang.String r3 = "http.route.default-proxy"
            r0 = r20
            org.apache.http.HttpHost r4 = r0.G
            r2.setParameter(r3, r4)
        L_0x00dd:
            r0 = r20
            int r3 = r0.y
            if (r3 <= 0) goto L_0x0104
            java.lang.String r3 = "timeout param"
            java.lang.String r4 = "http.connection.timeout"
            com.c.c.a.b(r3, r4)
            java.lang.String r3 = "http.connection.timeout"
            r0 = r20
            int r4 = r0.y
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2.setParameter(r3, r4)
            java.lang.String r3 = "http.socket.timeout"
            r0 = r20
            int r4 = r0.y
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2.setParameter(r3, r4)
        L_0x0104:
            org.apache.http.protocol.BasicHttpContext r11 = new org.apache.http.protocol.BasicHttpContext
            r11.<init>()
            org.apache.http.impl.client.BasicCookieStore r2 = new org.apache.http.impl.client.BasicCookieStore
            r2.<init>()
            java.lang.String r3 = "http.cookie-store"
            r11.setAttribute(r3, r2)
            r0 = r21
            r1 = r20
            r1.D = r0
            r0 = r20
            boolean r2 = r0.P
            if (r2 == 0) goto L_0x012a
            java.io.IOException r2 = new java.io.IOException
            java.lang.String r3 = "Aborted"
            r2.<init>(r3)
            throw r2
        L_0x0127:
            org.apache.http.conn.scheme.SocketFactory r2 = com.c.b.a.M
            goto L_0x00b4
        L_0x012a:
            r0 = r21
            org.apache.http.HttpResponse r12 = r10.execute(r0, r11)
            r6 = 0
            org.apache.http.StatusLine r2 = r12.getStatusLine()
            int r13 = r2.getStatusCode()
            org.apache.http.StatusLine r2 = r12.getStatusLine()
            java.lang.String r14 = r2.getReasonPhrase()
            r4 = 0
            org.apache.http.HttpEntity r15 = r12.getEntity()
            r3 = 0
            r2 = 200(0xc8, float:2.8E-43)
            if (r13 < r2) goto L_0x014f
            r2 = 300(0x12c, float:4.2E-43)
            if (r13 < r2) goto L_0x01bd
        L_0x014f:
            if (r15 == 0) goto L_0x031e
            java.io.InputStream r2 = r15.getContent()     // Catch:{ Exception -> 0x01b5 }
            java.lang.String r5 = a(r15)     // Catch:{ Exception -> 0x01b5 }
            byte[] r5 = a(r5, r2)     // Catch:{ Exception -> 0x01b5 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x01b5 }
            java.lang.String r7 = "UTF-8"
            r2.<init>(r5, r7)     // Catch:{ Exception -> 0x01b5 }
            java.lang.String r4 = "error"
            com.c.c.a.b(r4, r2)     // Catch:{ Exception -> 0x030f }
        L_0x0169:
            r4 = r2
            r2 = r3
            r3 = r22
        L_0x016d:
            java.lang.String r5 = "response"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r13)
            com.c.c.a.b(r5, r7)
            if (r6 == 0) goto L_0x0182
            int r5 = r6.length
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r0 = r22
            com.c.c.a.b(r5, r0)
        L_0x0182:
            r0 = r24
            com.c.b.d r5 = r0.b(r13)
            com.c.b.d r5 = r5.b(r14)
            com.c.b.d r4 = r5.a(r4)
            com.c.b.d r3 = r4.c(r3)
            java.util.Date r4 = new java.util.Date
            r4.<init>()
            com.c.b.d r3 = r3.a(r4)
            com.c.b.d r3 = r3.a(r6)
            com.c.b.d r2 = r3.a(r2)
            com.c.b.d r2 = r2.a(r10)
            com.c.b.d r2 = r2.a(r11)
            org.apache.http.Header[] r3 = r12.getAllHeaders()
            r2.a(r3)
            return
        L_0x01b5:
            r2 = move-exception
        L_0x01b6:
            com.c.c.a.a(r2)
            r2 = r3
            r3 = r22
            goto L_0x016d
        L_0x01bd:
            java.lang.String r2 = "http.target_host"
            java.lang.Object r2 = r11.getAttribute(r2)
            org.apache.http.HttpHost r2 = (org.apache.http.HttpHost) r2
            java.lang.String r3 = "http.request"
            java.lang.Object r3 = r11.getAttribute(r3)
            org.apache.http.client.methods.HttpUriRequest r3 = (org.apache.http.client.methods.HttpUriRequest) r3
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r2 = r2.toURI()
            java.lang.StringBuilder r2 = r5.append(r2)
            java.net.URI r3 = r3.getURI()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r7 = r2.toString()
            r2 = 32
            r3 = 65536(0x10000, float:9.18355E-41)
            long r8 = r15.getContentLength()
            int r5 = (int) r8
            int r3 = java.lang.Math.min(r3, r5)
            int r16 = java.lang.Math.max(r2, r3)
            r0 = r20
            boolean r2 = r0 instanceof com.c.b.e
            if (r2 != 0) goto L_0x0282
            java.lang.String r2 = "Date"
            org.apache.http.Header[] r5 = r12.getHeaders(r2)
            java.lang.String r2 = "Expires"
            org.apache.http.Header[] r17 = r12.getHeaders(r2)
            r8 = 0
            r2 = 0
            if (r5 == 0) goto L_0x022c
            int r0 = r5.length
            r18 = r0
            if (r18 <= 0) goto L_0x022c
            java.util.Date r8 = new java.util.Date
            r9 = 0
            r5 = r5[r9]
            java.lang.String r5 = r5.getValue()
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = r5.trim()
            r8.<init>(r5)
            long r8 = r8.getTime()
        L_0x022c:
            if (r17 == 0) goto L_0x024b
            r0 = r17
            int r5 = r0.length
            if (r5 <= 0) goto L_0x024b
            java.util.Date r2 = new java.util.Date
            r3 = 0
            r3 = r17[r3]
            java.lang.String r3 = r3.getValue()
            java.lang.String r3 = r3.toString()
            java.lang.String r3 = r3.trim()
            r2.<init>(r3)
            long r2 = r2.getTime()
        L_0x024b:
            long r2 = r2 - r8
            long r8 = java.lang.System.currentTimeMillis()
            r17 = 0
            int r5 = (r2 > r17 ? 1 : (r2 == r17 ? 0 : -1))
            if (r5 > 0) goto L_0x0259
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
        L_0x0259:
            long r2 = r2 + r8
            r0 = r20
            r0.z = r2
            java.lang.String r2 = "CACHE"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "httpDo(),get expires from "
            r3.<init>(r5)
            r0 = r22
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = " header:"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r20
            long r8 = r0.z
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.String r3 = r3.toString()
            android.util.Log.d(r2, r3)
        L_0x0282:
            r3 = 0
            java.io.File r5 = r20.i()     // Catch:{ all -> 0x0306 }
            if (r5 != 0) goto L_0x02e4
            com.c.c.e r2 = new com.c.c.e     // Catch:{ all -> 0x0306 }
            r0 = r16
            r2.<init>(r0)     // Catch:{ all -> 0x0306 }
            r3 = r2
        L_0x0291:
            java.io.InputStream r8 = r15.getContent()     // Catch:{ all -> 0x0306 }
            java.lang.String r2 = a(r15)     // Catch:{ all -> 0x0306 }
            long r15 = r15.getContentLength()     // Catch:{ all -> 0x0306 }
            int r15 = (int) r15     // Catch:{ all -> 0x0306 }
            java.lang.String r9 = "gzip"
            boolean r2 = r9.equalsIgnoreCase(r2)     // Catch:{ all -> 0x0306 }
            if (r2 == 0) goto L_0x031c
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0306 }
            r2.<init>(r8)     // Catch:{ all -> 0x0306 }
            r9 = r2
        L_0x02ac:
            r2 = 0
            r0 = r20
            java.lang.ref.WeakReference<java.lang.Object> r8 = r0.n     // Catch:{ all -> 0x0306 }
            if (r8 == 0) goto L_0x031a
            r0 = r20
            java.lang.ref.WeakReference<java.lang.Object> r2 = r0.n     // Catch:{ all -> 0x0306 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0306 }
            r8 = r2
        L_0x02bc:
            r2 = 0
            if (r8 == 0) goto L_0x02c4
            com.c.c.f r2 = new com.c.c.f     // Catch:{ all -> 0x0306 }
            r2.<init>(r8)     // Catch:{ all -> 0x0306 }
        L_0x02c4:
            com.c.c.a.a(r9, r3, r15, r2)     // Catch:{ all -> 0x0306 }
            r3.flush()     // Catch:{ all -> 0x0306 }
            if (r5 != 0) goto L_0x02f3
            r0 = r3
            com.c.c.e r0 = (com.c.c.e) r0     // Catch:{ all -> 0x0306 }
            r2 = r0
            byte[] r2 = r2.toByteArray()     // Catch:{ all -> 0x0306 }
            r19 = r5
            r5 = r2
            r2 = r19
        L_0x02d9:
            r6 = 0
            com.c.c.a.a(r6)
            com.c.c.a.a(r3)
            r3 = r7
            r6 = r5
            goto L_0x016d
        L_0x02e4:
            r5.createNewFile()     // Catch:{ all -> 0x0306 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0306 }
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ all -> 0x0306 }
            r8.<init>(r5)     // Catch:{ all -> 0x0306 }
            r2.<init>(r8)     // Catch:{ all -> 0x0306 }
            r3 = r2
            goto L_0x0291
        L_0x02f3:
            boolean r2 = r5.exists()     // Catch:{ all -> 0x0306 }
            if (r2 == 0) goto L_0x0303
            long r8 = r5.length()     // Catch:{ all -> 0x0306 }
            r15 = 0
            int r2 = (r8 > r15 ? 1 : (r8 == r15 ? 0 : -1))
            if (r2 != 0) goto L_0x0317
        L_0x0303:
            r2 = 0
            r5 = r6
            goto L_0x02d9
        L_0x0306:
            r2 = move-exception
            r4 = 0
            com.c.c.a.a(r4)
            com.c.c.a.a(r3)
            throw r2
        L_0x030f:
            r4 = move-exception
            r19 = r4
            r4 = r2
            r2 = r19
            goto L_0x01b6
        L_0x0317:
            r2 = r5
            r5 = r6
            goto L_0x02d9
        L_0x031a:
            r8 = r2
            goto L_0x02bc
        L_0x031c:
            r9 = r8
            goto L_0x02ac
        L_0x031e:
            r2 = r4
            goto L_0x0169
        */
        throw new UnsupportedOperationException("Method not decompiled: com.c.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, java.util.Map, com.c.b.d):void");
    }

    private static byte[] a(String str, InputStream inputStream) {
        if ("gzip".equalsIgnoreCase(str)) {
            inputStream = new GZIPInputStream(inputStream);
        }
        return com.c.c.a.a(inputStream);
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b(java.util.Map<java.lang.String, java.lang.Object> r3) {
        /*
            java.util.Set r0 = r3.entrySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x0008:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x002d
            java.lang.Object r0 = r1.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getValue()
            java.lang.Object r0 = r0.getKey()
            com.c.c.a.b(r0, r2)
            boolean r0 = r2 instanceof java.io.File
            if (r0 != 0) goto L_0x002b
            boolean r0 = r2 instanceof byte[]
            if (r0 != 0) goto L_0x002b
            boolean r0 = r2 instanceof java.io.InputStream
            if (r0 == 0) goto L_0x0008
        L_0x002b:
            r0 = 1
        L_0x002c:
            return r0
        L_0x002d:
            r0 = 0
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.c.b.a.b(java.util.Map):boolean");
    }

    private static String c(String str) {
        int indexOf;
        if (str == null || (indexOf = str.indexOf("charset")) == -1) {
            return null;
        }
        return str.substring(indexOf + 7).replaceAll("[^\\w-]", PoiTypeDef.All);
    }

    private static String d(String str) {
        return str.replaceAll(" ", "%20").replaceAll("\\|", "%7C");
    }

    protected static int e() {
        return O;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.b.a.a(java.lang.String, java.lang.Object, com.c.b.d):void
     arg types: [java.lang.String, T, com.c.b.d]
     candidates:
      com.c.b.a.a(byte[], java.lang.String, com.c.b.d):java.lang.String
      com.c.b.a.a(java.lang.String, java.io.File, com.c.b.d):T
      com.c.b.a.a(java.lang.String, byte[], com.c.b.d):T
      com.c.b.a.a(java.lang.String, java.lang.Object, com.c.b.d):void */
    private void f() {
        Activity activity;
        b(false);
        this.I = true;
        if (this.B == null || ((activity = this.B.get()) != null && !activity.isFinishing())) {
            if (this.m != null) {
                Object obj = this.l != null ? this.l : this.k == null ? null : this.k.get();
                Class[] clsArr = {String.class, this.j, d.class};
                if (!(obj instanceof n) || this.b.h() == 200) {
                    com.c.c.a.a(obj, this.m, true, clsArr, H, this.o, this.a, this.b);
                } else {
                    d dVar = this.b;
                    ((n) obj).a();
                }
            } else {
                try {
                    a(this.o, (Object) this.a, this.b);
                } catch (Exception e2) {
                    com.c.c.a.b(e2);
                }
            }
        }
        if (this.a != null && this.c) {
            byte[] j2 = this.b.j();
            if (j2 != null) {
                try {
                    if (j2.length >= 50 && this.b.l() == 1) {
                        File h2 = h();
                        if (!this.b.g()) {
                            String str = this.o;
                            T t2 = this.a;
                            if (!(h2 == null || j2 == null)) {
                                com.c.c.a.a(h2, j2, this.z);
                            }
                        } else if (h2.exists()) {
                            h2.delete();
                        }
                    }
                } catch (Exception e3) {
                    com.c.c.a.a((Throwable) e3);
                }
            }
            this.b.a((byte[]) null);
        }
        if (!this.J) {
            this.b.d();
        }
        if (this.J) {
            synchronized (this) {
                try {
                    notifyAll();
                } catch (Exception e4) {
                    com.c.c.a.a((Throwable) e4);
                }
            }
        }
        com.c.c.a.a();
        return;
    }

    private String g() {
        if (this.w == null) {
            return this.o;
        }
        com.c.a.a aVar = this.w;
        return this.o;
    }

    private File h() {
        return com.c.c.a.a(this.u, g());
    }

    private File i() {
        File file;
        if (!c()) {
            file = null;
        } else if (this.v != null) {
            file = this.v;
        } else if (this.c) {
            file = h();
        } else {
            File c2 = com.c.c.a.c();
            if (c2 == null) {
                c2 = this.u;
            }
            file = com.c.c.a.a(c2, this.o);
        }
        if (file == null || file.exists()) {
            return file;
        }
        try {
            file.getParentFile().mkdirs();
            file.createNewFile();
            return file;
        } catch (Exception e2) {
            com.c.c.a.b(e2);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d):void
     arg types: [org.apache.http.client.methods.HttpDelete, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d]
     candidates:
      com.c.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.c.b.a.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>, java.util.Map<java.lang.String, java.lang.Object>, com.c.b.d):void
      com.c.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d):void
     arg types: [org.apache.http.client.methods.HttpGet, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d]
     candidates:
      com.c.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.c.b.a.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>, java.util.Map<java.lang.String, java.lang.Object>, com.c.b.d):void
      com.c.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, java.util.Map<java.lang.String, java.lang.String>, com.c.b.d):void */
    private void j() {
        String str = this.o;
        Map<String, Object> map = this.p;
        if (map == null && str.length() > 2000) {
            Uri parse = Uri.parse(str);
            String str2 = parse.getScheme() + "://" + parse.getAuthority() + parse.getPath();
            String fragment = parse.getFragment();
            if (fragment != null) {
                str2 = str2 + "#" + fragment;
            }
            map = a(parse);
            str = str2;
        }
        if (this.w != null) {
            com.c.a.a aVar = this.w;
        }
        if (2 == this.C) {
            Map<String, String> map2 = this.q;
            d dVar = this.b;
            com.c.c.a.b("get", str);
            String d2 = d(str);
            a((HttpUriRequest) new HttpDelete(d2), d2, map2, dVar);
        } else if (3 == this.C) {
            Map<String, String> map3 = this.q;
            d dVar2 = this.b;
            com.c.c.a.b("put", str);
            a(str, new HttpPut(str), map3, map, dVar2);
        } else {
            if (1 == this.C && map == null) {
                map = new HashMap<>();
            }
            if (map == null) {
                Map<String, String> map4 = this.q;
                d dVar3 = this.b;
                com.c.c.a.b("get", str);
                String d3 = d(str);
                a((HttpUriRequest) new HttpGet(d3), d3, map4, dVar3);
            } else if (b(map)) {
                a(str, this.q, map, this.b);
            } else {
                Map<String, String> map5 = this.q;
                d dVar4 = this.b;
                com.c.c.a.b("post", str);
                a(str, new HttpPost(str), map5, map, dVar4);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.b.a.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, T]
     candidates:
      com.c.b.a.a(java.lang.String, java.io.InputStream):byte[]
      com.c.b.a.a(java.io.File, java.lang.String):java.io.File
      com.c.b.a.a(java.lang.Object, java.lang.String):K
      com.c.b.a.a(java.lang.String, java.lang.Object):void */
    private void k() {
        if (this.o != null && this.d) {
            a(this.o, (Object) this.a);
        }
        f();
        this.k = null;
        this.l = null;
        this.n = null;
        this.D = null;
        this.s = null;
        this.w = null;
        this.B = null;
    }

    private String l() {
        if (this.r == null || this.r.size() == 0) {
            return null;
        }
        Iterator<String> it = this.r.keySet().iterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            String next = it.next();
            sb.append(next);
            sb.append("=");
            sb.append(this.r.get(next));
            if (it.hasNext()) {
                sb.append("; ");
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public File a(File file, String str) {
        if (this.z < 0) {
            return null;
        }
        File b2 = com.c.c.a.b(file, str);
        if (b2 == null) {
            return null;
        }
        Log.d("CACHE", "system  :" + System.currentTimeMillis());
        Log.d("CACHE", "modified:" + b2.lastModified());
        Log.d("CACHE", "diff    :" + (System.currentTimeMillis() > b2.lastModified()));
        if (this instanceof e) {
            return b2;
        }
        if (!com.zhongsou.flymall.e.f.a(AppContext.a()) || System.currentTimeMillis() <= b2.lastModified()) {
            return b2;
        }
        return null;
    }

    public final K a() {
        this.c = true;
        return this;
    }

    public final K a(int i2) {
        this.t = i2;
        return this;
    }

    public final K a(com.c.a.a aVar) {
        this.w = aVar;
        return this;
    }

    public final K a(f fVar) {
        this.s = fVar;
        return this;
    }

    public final K a(Class cls) {
        this.j = cls;
        return this;
    }

    public final K a(Object obj) {
        if (obj != null) {
            this.n = new WeakReference<>(obj);
        }
        return this;
    }

    public final K a(Object obj, String str) {
        this.k = new WeakReference(obj);
        this.m = str;
        this.l = null;
        return this;
    }

    public final K a(String str) {
        this.o = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public T a(String str, File file, d dVar) {
        byte[] a2;
        try {
            if (c()) {
                dVar.a(file);
                a2 = null;
            } else {
                a2 = com.c.c.a.a((InputStream) new FileInputStream(file));
            }
            return a(str, a2, dVar);
        } catch (Exception e2) {
            com.c.c.a.a((Throwable) e2);
            return null;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected T a(java.lang.String r5, byte[] r6, com.c.b.d r7) {
        /*
            r4 = this;
            r1 = 0
            java.lang.Class<T> r0 = r4.j
            if (r0 != 0) goto L_0x0006
        L_0x0005:
            return r1
        L_0x0006:
            java.io.File r0 = r7.k()
            if (r6 == 0) goto L_0x00c8
            java.lang.Class<T> r0 = r4.j
            java.lang.Class<android.graphics.Bitmap> r2 = android.graphics.Bitmap.class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x001d
            r0 = 0
            int r1 = r6.length
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeByteArray(r6, r0, r1)
            goto L_0x0005
        L_0x001d:
            java.lang.Class<T> r0 = r4.j
            java.lang.Class<org.json.JSONObject> r2 = org.json.JSONObject.class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0045
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x003b }
            java.lang.String r0 = r4.A     // Catch:{ Exception -> 0x003b }
            r2.<init>(r6, r0)     // Catch:{ Exception -> 0x003b }
            org.json.JSONTokener r0 = new org.json.JSONTokener     // Catch:{ Exception -> 0x0136 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0136 }
            java.lang.Object r0 = r0.nextValue()     // Catch:{ Exception -> 0x0136 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Exception -> 0x0136 }
        L_0x0039:
            r1 = r0
            goto L_0x0005
        L_0x003b:
            r0 = move-exception
            r2 = r1
        L_0x003d:
            com.c.c.a.a(r0)
            com.c.c.a.a(r2)
            r0 = r1
            goto L_0x0039
        L_0x0045:
            java.lang.Class<T> r0 = r4.j
            java.lang.Class<org.json.JSONArray> r2 = org.json.JSONArray.class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0069
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0063 }
            java.lang.String r2 = r4.A     // Catch:{ Exception -> 0x0063 }
            r0.<init>(r6, r2)     // Catch:{ Exception -> 0x0063 }
            org.json.JSONTokener r2 = new org.json.JSONTokener     // Catch:{ Exception -> 0x0063 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0063 }
            java.lang.Object r0 = r2.nextValue()     // Catch:{ Exception -> 0x0063 }
            org.json.JSONArray r0 = (org.json.JSONArray) r0     // Catch:{ Exception -> 0x0063 }
        L_0x0061:
            r1 = r0
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            com.c.c.a.a(r0)
            r0 = r1
            goto L_0x0061
        L_0x0069:
            java.lang.Class<T> r0 = r4.j
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x009b
            int r0 = r7.l()
            r2 = 1
            if (r0 != r2) goto L_0x0086
            java.lang.String r0 = "network"
            com.c.c.a.a(r0)
            java.lang.String r0 = r4.A
            java.lang.String r1 = a(r6, r0, r7)
            goto L_0x0005
        L_0x0086:
            java.lang.String r0 = "file"
            com.c.c.a.a(r0)
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0095 }
            java.lang.String r2 = r4.A     // Catch:{ Exception -> 0x0095 }
            r0.<init>(r6, r2)     // Catch:{ Exception -> 0x0095 }
            r1 = r0
            goto L_0x0005
        L_0x0095:
            r0 = move-exception
            com.c.c.a.a(r0)
            goto L_0x0005
        L_0x009b:
            java.lang.Class<T> r0 = r4.j
            java.lang.Class<byte[]> r2 = byte[].class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00a8
            r1 = r6
            goto L_0x0005
        L_0x00a8:
            com.c.b.f r0 = r4.s
            if (r0 == 0) goto L_0x00b8
            com.c.b.f r0 = r4.s
            java.lang.Class<T> r1 = r4.j
            java.lang.String r1 = r4.A
            java.lang.Object r1 = r0.a(r5, r6)
            goto L_0x0005
        L_0x00b8:
            com.c.b.f r0 = com.c.b.a.F
            if (r0 == 0) goto L_0x0005
            com.c.b.f r0 = com.c.b.a.F
            java.lang.Class<T> r1 = r4.j
            java.lang.String r1 = r4.A
            java.lang.Object r1 = r0.a(r5, r6)
            goto L_0x0005
        L_0x00c8:
            if (r0 == 0) goto L_0x0005
            java.lang.Class<T> r2 = r4.j
            java.lang.Class<java.io.File> r3 = java.io.File.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00d7
            r1 = r0
            goto L_0x0005
        L_0x00d7:
            java.lang.Class<T> r2 = r4.j
            java.lang.Class<com.c.c.h> r3 = com.c.c.h.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00f7
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00f1 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00f1 }
            com.c.c.h r0 = new com.c.c.h     // Catch:{ Exception -> 0x00f1 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00f1 }
            r7.a(r2)     // Catch:{ Exception -> 0x00f1 }
            r1 = r0
            goto L_0x0005
        L_0x00f1:
            r0 = move-exception
            com.c.c.a.b(r0)
            goto L_0x0005
        L_0x00f7:
            java.lang.Class<T> r2 = r4.j
            java.lang.Class<org.xmlpull.v1.XmlPullParser> r3 = org.xmlpull.v1.XmlPullParser.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x011b
            org.xmlpull.v1.XmlPullParser r6 = android.util.Xml.newPullParser()
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0115 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0115 }
            java.lang.String r0 = r4.A     // Catch:{ Exception -> 0x0115 }
            r6.setInput(r2, r0)     // Catch:{ Exception -> 0x0115 }
            r7.a(r2)     // Catch:{ Exception -> 0x0115 }
            r1 = r6
            goto L_0x0005
        L_0x0115:
            r0 = move-exception
            com.c.c.a.b(r0)
            goto L_0x0005
        L_0x011b:
            java.lang.Class<T> r2 = r4.j
            java.lang.Class<java.io.InputStream> r3 = java.io.InputStream.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0005
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0130 }
            r6.<init>(r0)     // Catch:{ Exception -> 0x0130 }
            r7.a(r6)     // Catch:{ Exception -> 0x0130 }
            r1 = r6
            goto L_0x0005
        L_0x0130:
            r0 = move-exception
            com.c.c.a.b(r0)
            goto L_0x0005
        L_0x0136:
            r0 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.c.b.a.a(java.lang.String, byte[], com.c.b.d):java.lang.Object");
    }

    public final K a(Map<String, ?> map) {
        this.p = map;
        return this;
    }

    public final K a(boolean z2) {
        this.x = z2;
        return this;
    }

    public final void a(Activity activity) {
        if (activity.isFinishing()) {
            com.c.c.a.a("Warning", "Possible memory leak. Calling ajax with a terminated activity.");
        }
        if (this.j == null) {
            com.c.c.a.a("Warning", "type() is not called with response type.");
            return;
        }
        this.B = new WeakReference<>(activity);
        a((Context) activity);
    }

    public void a(Context context) {
        if (this.b == null) {
            this.b = new d();
            this.b.c(this.o).a(this.x);
        } else if (this.b.e()) {
            this.b.c();
            this.a = null;
        }
        b(true);
        if (this.w == null || this.w.a()) {
            T b2 = b(this.o);
            if (b2 != null) {
                this.a = b2;
                this.b.a(4).b();
                f();
                return;
            }
            this.u = com.c.c.a.a(context, this.t);
            if (L == null) {
                L = Executors.newFixedThreadPool(g);
            }
            L.execute(this);
            return;
        }
        com.c.c.a.b("auth needed", this.o);
        this.w.a(this);
    }

    /* access modifiers changed from: protected */
    public void a(String str, Object obj) {
    }

    public void a(String str, T t2, d dVar) {
    }

    public final K b() {
        this.d = true;
        return this;
    }

    /* access modifiers changed from: protected */
    public T b(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final void b(boolean z2) {
        Object obj = this.n == null ? null : this.n.get();
        if (obj == null) {
            return;
        }
        if (com.c.c.a.b()) {
            c.a(obj, this.o, z2);
        } else {
            com.c.c.a.a((Runnable) new b(this, obj, z2));
        }
    }

    /* access modifiers changed from: protected */
    public final boolean b(Context context) {
        return this.c && com.c.c.a.b(com.c.c.a.a(context, this.t), this.o) != null;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return File.class.equals(this.j) || XmlPullParser.class.equals(this.j) || InputStream.class.equals(this.j) || h.class.equals(this.j);
    }

    public final String d() {
        return this.o;
    }

    public void run() {
        File a2;
        byte[] bArr = null;
        if (!this.b.e()) {
            if (!this.x && this.c && (a2 = a(this.u, g())) != null) {
                this.b.a(3);
                this.a = a(this.o, a2, this.b);
                if (this.a != null) {
                    this.b.a(new Date(a2.lastModified())).b();
                }
            }
            if (this.a == null) {
                String str = this.o;
                this.a = null;
                if (this.a != null) {
                    this.b.a(2).b();
                }
            }
            if (this.a == null) {
                if (this.o == null) {
                    this.b.b(-101).b();
                } else {
                    try {
                        j();
                        if (this.w != null) {
                            com.c.a.a aVar = this.w;
                            d dVar = this.b;
                            if (aVar.b() && !this.K) {
                                com.c.c.a.b("reauth needed", this.b.i());
                                this.K = true;
                                if (this.w.c()) {
                                    j();
                                } else {
                                    this.b.a();
                                }
                            }
                        }
                        bArr = this.b.j();
                    } catch (Exception e2) {
                        com.c.c.a.a((Throwable) e2);
                        this.b.b(-101).b("network error");
                    } catch (Throwable th) {
                        com.c.c.a.a(th);
                        this.b.b(-101).b();
                    }
                    try {
                        this.a = a(this.o, bArr, this.b);
                    } catch (Exception e3) {
                        com.c.c.a.a((Throwable) e3);
                    }
                    if (this.a == null && bArr != null) {
                        this.b.b(-103).b("transform error");
                    }
                    O = this.b.h();
                    this.b.b();
                }
            }
            if (this.b.f()) {
                return;
            }
            if (this.E) {
                com.c.c.a.a((Runnable) this);
            } else {
                k();
            }
        } else {
            k();
        }
    }
}
