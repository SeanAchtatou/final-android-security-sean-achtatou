package com.c.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

public final class g extends BitmapDrawable {
    private float a;
    private WeakReference<ImageView> b;
    private boolean c;
    private Matrix d;
    private int e;
    private float f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.c.g.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.c.c.g.a(int, int, int):int
      com.c.c.g.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    public g(Resources resources, Bitmap bitmap, ImageView imageView, float f2, float f3) {
        super(resources, bitmap);
        this.b = new WeakReference<>(imageView);
        this.a = f2;
        this.f = f3;
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        imageView.setImageMatrix(new Matrix());
        a(imageView, bitmap, false);
    }

    private int a(int i, int i2, int i3) {
        float f2 = this.a;
        if (this.a == Float.MAX_VALUE) {
            f2 = ((float) i2) / ((float) i);
        }
        return (int) (f2 * ((float) i3));
    }

    private static int a(ImageView imageView) {
        int i = 0;
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        if (layoutParams != null) {
            i = layoutParams.width;
        }
        if (i <= 0) {
            i = imageView.getWidth();
        }
        return i > 0 ? (i - imageView.getPaddingLeft()) - imageView.getPaddingRight() : i;
    }

    private void a(ImageView imageView, Bitmap bitmap, boolean z) {
        int a2 = a(imageView);
        if (a2 > 0) {
            int a3 = a(bitmap.getWidth(), bitmap.getHeight(), a2) + imageView.getPaddingTop() + imageView.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            if (layoutParams != null) {
                if (a3 != layoutParams.height) {
                    layoutParams.height = a3;
                    imageView.setLayoutParams(layoutParams);
                }
                if (z) {
                    this.c = true;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.c.g.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.c.c.g.a(int, int, int):int
      com.c.c.g.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void draw(android.graphics.Canvas r15) {
        /*
            r14 = this;
            r12 = 0
            r11 = 1073741824(0x40000000, float:2.0)
            r10 = 1069547520(0x3fc00000, float:1.5)
            r9 = 1065353216(0x3f800000, float:1.0)
            r2 = 0
            r0 = 0
            java.lang.ref.WeakReference<android.widget.ImageView> r1 = r14.b
            if (r1 == 0) goto L_0x0015
            java.lang.ref.WeakReference<android.widget.ImageView> r0 = r14.b
            java.lang.Object r0 = r0.get()
            android.widget.ImageView r0 = (android.widget.ImageView) r0
        L_0x0015:
            float r1 = r14.a
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x001d
            if (r0 != 0) goto L_0x0021
        L_0x001d:
            super.draw(r15)
        L_0x0020:
            return
        L_0x0021:
            android.graphics.Bitmap r4 = r14.getBitmap()
            int r5 = r4.getWidth()
            android.graphics.Matrix r1 = r14.d
            if (r1 == 0) goto L_0x0031
            int r1 = r14.e
            if (r5 == r1) goto L_0x00ab
        L_0x0031:
            int r6 = r4.getHeight()
            int r1 = a(r0)
            int r7 = r14.a(r5, r6, r1)
            if (r5 <= 0) goto L_0x0045
            if (r6 <= 0) goto L_0x0045
            if (r1 <= 0) goto L_0x0045
            if (r7 > 0) goto L_0x007d
        L_0x0045:
            r1 = 0
        L_0x0046:
            if (r1 == 0) goto L_0x0074
            int r2 = r0.getPaddingTop()
            int r3 = r0.getPaddingBottom()
            int r2 = r2 + r3
            int r3 = r0.getPaddingLeft()
            int r5 = r0.getPaddingRight()
            int r3 = r3 + r5
            if (r2 > 0) goto L_0x005e
            if (r3 <= 0) goto L_0x006d
        L_0x005e:
            int r5 = r0.getWidth()
            int r3 = r5 - r3
            int r5 = r0.getHeight()
            int r2 = r5 - r2
            r15.clipRect(r12, r12, r3, r2)
        L_0x006d:
            android.graphics.Paint r2 = r14.getPaint()
            r15.drawBitmap(r4, r1, r2)
        L_0x0074:
            boolean r1 = r14.c
            if (r1 != 0) goto L_0x0020
            r1 = 1
            r14.a(r0, r4, r1)
            goto L_0x0020
        L_0x007d:
            android.graphics.Matrix r3 = r14.d
            if (r3 == 0) goto L_0x0085
            int r3 = r14.e
            if (r5 == r3) goto L_0x00ab
        L_0x0085:
            android.graphics.Matrix r3 = new android.graphics.Matrix
            r3.<init>()
            r14.d = r3
            int r3 = r5 * r7
            int r8 = r1 * r6
            if (r3 < r8) goto L_0x00ae
            float r3 = (float) r7
            float r6 = (float) r6
            float r3 = r3 / r6
            float r1 = (float) r1
            float r6 = (float) r5
            float r6 = r6 * r3
            float r1 = r1 - r6
            r6 = 1056964608(0x3f000000, float:0.5)
            float r1 = r1 * r6
            r13 = r2
            r2 = r1
            r1 = r13
        L_0x009f:
            android.graphics.Matrix r6 = r14.d
            r6.setScale(r3, r3)
            android.graphics.Matrix r3 = r14.d
            r3.postTranslate(r2, r1)
            r14.e = r5
        L_0x00ab:
            android.graphics.Matrix r1 = r14.d
            goto L_0x0046
        L_0x00ae:
            float r1 = (float) r1
            float r3 = (float) r5
            float r3 = r1 / r3
            float r1 = r14.f
            r8 = 2139095039(0x7f7fffff, float:3.4028235E38)
            int r1 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r1 == 0) goto L_0x00c7
            float r1 = r14.f
            float r1 = r9 - r1
            float r1 = r1 / r11
        L_0x00c0:
            float r7 = (float) r7
            float r6 = (float) r6
            float r6 = r6 * r3
            float r6 = r7 - r6
            float r1 = r1 * r6
            goto L_0x009f
        L_0x00c7:
            float r1 = (float) r6
            float r8 = (float) r5
            float r1 = r1 / r8
            float r1 = java.lang.Math.min(r10, r1)
            float r1 = java.lang.Math.max(r9, r1)
            r8 = 1048576000(0x3e800000, float:0.25)
            float r1 = r10 - r1
            float r1 = r1 / r11
            float r1 = r1 + r8
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.c.c.g.draw(android.graphics.Canvas):void");
    }
}
