package com.c.c;

import android.graphics.Bitmap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public final class b extends LinkedHashMap<String, Bitmap> {
    private int a;
    private int b;
    private int c;
    private int d;

    public b(int i, int i2, int i3) {
        super(8, 0.75f, true);
        this.a = i;
        this.b = i2;
        this.c = i3;
    }

    private static int a(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return bitmap.getWidth() * bitmap.getHeight();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Bitmap remove(Object obj) {
        Bitmap bitmap = (Bitmap) super.remove(obj);
        if (bitmap != null) {
            this.d -= a(bitmap);
        }
        return bitmap;
    }

    public final void clear() {
        super.clear();
        this.d = 0;
    }

    public final /* synthetic */ Object put(Object obj, Object obj2) {
        String str = (String) obj;
        Bitmap bitmap = (Bitmap) obj2;
        Bitmap bitmap2 = null;
        int a2 = a(bitmap);
        if (a2 <= this.b) {
            this.d += a2;
            bitmap2 = (Bitmap) super.put(str, bitmap);
            if (bitmap2 != null) {
                this.d -= a(bitmap2);
            }
        }
        return bitmap2;
    }

    public final boolean removeEldestEntry(Map.Entry<String, Bitmap> entry) {
        if (this.d > this.c || size() > this.a) {
            remove(entry.getKey());
        }
        if (this.d <= this.c) {
            return false;
        }
        Iterator it = keySet().iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
            if (this.d <= this.c) {
                return false;
            }
        }
        return false;
    }
}
