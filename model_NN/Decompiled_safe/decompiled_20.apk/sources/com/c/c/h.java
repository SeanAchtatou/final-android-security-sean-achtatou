package com.c.c;

import android.util.Xml;
import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

public class h {
    private Element a;

    public h(InputStream inputStream) {
        try {
            this.a = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream).getDocumentElement();
        } catch (ParserConfigurationException e) {
            a.a((Throwable) e);
        } catch (IOException e2) {
            a.a((Throwable) e2);
            throw new SAXException(e2);
        }
    }

    private String a() {
        try {
            XmlSerializer newSerializer = Xml.newSerializer();
            StringWriter stringWriter = new StringWriter();
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument("utf-8", null);
            a(this.a, newSerializer, 0, null);
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String a(Node node) {
        String str = null;
        switch (node.getNodeType()) {
            case 3:
                str = node.getNodeValue();
                if (str != null) {
                    str = str.trim();
                    break;
                }
                break;
            case 4:
                str = node.getNodeValue();
                break;
        }
        return str == null ? PoiTypeDef.All : str;
    }

    private void a(Element element, XmlSerializer xmlSerializer, int i, String str) {
        int i2;
        int i3 = 0;
        String tagName = element.getTagName();
        a(xmlSerializer, i, str);
        xmlSerializer.startTag(PoiTypeDef.All, tagName);
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            for (int i4 = 0; i4 < attributes.getLength(); i4++) {
                Attr attr = (Attr) attributes.item(i4);
                xmlSerializer.attribute(PoiTypeDef.All, attr.getName(), attr.getValue());
            }
        }
        if (element.hasChildNodes()) {
            NodeList childNodes = element.getChildNodes();
            int i5 = 0;
            while (i3 < childNodes.getLength()) {
                Node item = childNodes.item(i3);
                switch (item.getNodeType()) {
                    case 1:
                        a((Element) item, xmlSerializer, i + 1, str);
                        i2 = i5 + 1;
                        continue;
                        i3++;
                        i5 = i2;
                    case 3:
                        xmlSerializer.text(a(item));
                        i2 = i5;
                        continue;
                        i3++;
                        i5 = i2;
                    case 4:
                        xmlSerializer.cdsect(a(item));
                        break;
                }
                i2 = i5;
                i3++;
                i5 = i2;
            }
            if (i5 > 0) {
                a(xmlSerializer, i, str);
            }
        }
        xmlSerializer.endTag(PoiTypeDef.All, tagName);
    }

    private static void a(XmlSerializer xmlSerializer, int i, String str) {
        if (str != null) {
            xmlSerializer.text("\n");
            for (int i2 = 0; i2 < i; i2++) {
                xmlSerializer.text(str);
            }
        }
    }

    public String toString() {
        Element element = this.a;
        return a();
    }
}
