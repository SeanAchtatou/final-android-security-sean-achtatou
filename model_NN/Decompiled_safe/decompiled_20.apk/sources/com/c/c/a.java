package com.c.c;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Thread;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class a {
    private static boolean a = true;
    private static Object b;
    private static Thread.UncaughtExceptionHandler c;
    private static Map<String, Long> d = new HashMap();
    private static Handler e;
    private static ScheduledExecutorService f;
    private static File g;
    private static File h;

    private static File a(Context context) {
        if (g == null) {
            File file = new File(context.getCacheDir(), "aquery");
            g = file;
            file.mkdirs();
        }
        return g;
    }

    public static File a(Context context, int i) {
        if (i != 1) {
            return a(context);
        }
        if (h != null) {
            return h;
        }
        File file = new File(a(context), "persistent");
        h = file;
        file.mkdirs();
        return h;
    }

    public static File a(File file, String str) {
        if (str == null) {
            return null;
        }
        return str.startsWith(File.separator) ? new File(str) : new File(file, new BigInteger(a(str.getBytes())).abs().toString(36));
    }

    public static Object a(Object obj, String str, boolean z, Class<?>[] clsArr, Class<?>[] clsArr2, Object... objArr) {
        try {
            return b(obj, str, z, clsArr, clsArr2, objArr);
        } catch (Exception e2) {
            a((Throwable) e2);
            b(e2);
            return null;
        }
    }

    public static Object a(Object obj, String str, boolean z, Class<?>[] clsArr, Object... objArr) {
        return a(obj, str, z, clsArr, null, objArr);
    }

    public static void a() {
        if (a && b != null) {
            synchronized (b) {
                b.notifyAll();
            }
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e2) {
                a((Throwable) e2);
            }
        }
    }

    public static void a(File file, long j, long j2) {
        long j3 = 0;
        boolean z = false;
        try {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                Arrays.sort(listFiles, new c());
                int length = listFiles.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    j3 += listFiles[i].length();
                    if (j3 > j) {
                        z = true;
                        break;
                    }
                    i++;
                }
                if (z) {
                    a(listFiles, j2);
                }
                File c2 = c();
                if (c2 != null && c2.exists()) {
                    a(c2.listFiles(), 0);
                }
            }
        } catch (Exception e2) {
            b(e2);
        }
    }

    public static void a(File file, byte[] bArr) {
        if (file != null) {
            try {
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (Exception e2) {
                        b("file create fail", file);
                        b(e2);
                    }
                }
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(bArr);
                fileOutputStream.close();
            } catch (Exception e3) {
                try {
                    b(e3);
                } catch (Exception e4) {
                    b(e4);
                }
            }
        }
    }

    public static void a(File file, byte[] bArr, long j) {
        if (f == null) {
            f = Executors.newSingleThreadScheduledExecutor();
        }
        ScheduledExecutorService scheduledExecutorService = f;
        c a2 = new c().a(file, bArr);
        a2.a(j);
        scheduledExecutorService.schedule(a2, 0, TimeUnit.MILLISECONDS);
    }

    public static void a(InputStream inputStream, OutputStream outputStream) {
        a(inputStream, outputStream, 0, null);
    }

    public static void a(InputStream inputStream, OutputStream outputStream, int i, f fVar) {
        b("content header", Integer.valueOf(i));
        if (fVar != null) {
            fVar.a();
            fVar.a(i);
        }
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                break;
            }
            outputStream.write(bArr, 0, read);
            if (fVar != null) {
                fVar.b(read);
            }
        }
        if (fVar != null) {
            fVar.b();
        }
    }

    public static void a(Object obj) {
        if (a) {
            Log.w("AQuery", new StringBuilder().append(obj).toString());
        }
    }

    public static void a(Object obj, Object obj2) {
        Log.w("AQuery", obj + ":" + obj2);
    }

    public static void a(Runnable runnable) {
        if (e == null) {
            e = new Handler(Looper.getMainLooper());
        }
        e.post(runnable);
    }

    public static void a(Throwable th) {
        if (a) {
            Log.w("AQuery", Log.getStackTraceString(th));
        }
    }

    private static void a(File[] fileArr, long j) {
        long j2 = 0;
        int i = 0;
        for (File file : fileArr) {
            if (file.isFile()) {
                j2 += file.length();
                if (j2 >= j) {
                    file.delete();
                    i++;
                }
            }
        }
        b("deleted", Integer.valueOf(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.c.a.a(java.io.InputStream, java.io.OutputStream):void
     arg types: [java.io.InputStream, java.io.ByteArrayOutputStream]
     candidates:
      com.c.c.a.a(android.content.Context, int):java.io.File
      com.c.c.a.a(java.io.File, java.lang.String):java.io.File
      com.c.c.a.a(java.io.File, byte[]):void
      com.c.c.a.a(java.lang.Object, java.lang.Object):void
      com.c.c.a.a(java.io.File[], long):void
      com.c.c.a.a(java.io.InputStream, java.io.OutputStream):void */
    public static byte[] a(InputStream inputStream) {
        byte[] bArr = null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(inputStream, (OutputStream) byteArrayOutputStream);
            bArr = byteArrayOutputStream.toByteArray();
        } catch (IOException e2) {
            b(e2);
        }
        a((Closeable) inputStream);
        return bArr;
    }

    private static byte[] a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e2) {
            b(e2);
            return null;
        }
    }

    public static File b(File file, String str) {
        File a2 = a(file, str);
        if (a2 == null || !a2.exists()) {
            return null;
        }
        return a2;
    }

    private static Object b(Object obj, String str, boolean z, Class<?>[] clsArr, Class<?>[] clsArr2, Object... objArr) {
        if (obj == null || str == null) {
            return null;
        }
        if (clsArr == null) {
            try {
                clsArr = new Class[0];
            } catch (NoSuchMethodException e2) {
                a((Throwable) e2);
                if (!z) {
                    return null;
                }
                if (clsArr2 != null) {
                    return obj.getClass().getMethod(str, clsArr2).invoke(obj, objArr);
                }
                try {
                    return obj.getClass().getMethod(str, new Class[0]).invoke(obj, new Object[0]);
                } catch (NoSuchMethodException e3) {
                    a((Throwable) e3);
                    return null;
                }
            }
        }
        return obj.getClass().getMethod(str, clsArr).invoke(obj, objArr);
    }

    public static void b(Object obj, Object obj2) {
        if (a) {
            Log.w("AQuery", obj + ":" + obj2);
        }
    }

    public static void b(Throwable th) {
        if (th != null) {
            try {
                a("reporting", Log.getStackTraceString(th));
                if (c != null) {
                    c.uncaughtException(Thread.currentThread(), th);
                }
            } catch (Exception e2) {
                a((Throwable) e2);
            }
        }
    }

    public static boolean b() {
        return Looper.getMainLooper().getThread().getId() == Thread.currentThread().getId();
    }

    public static File c() {
        File file = new File(Environment.getExternalStorageDirectory(), "aquery/temp");
        file.mkdirs();
        if (!file.exists()) {
            return null;
        }
        return file;
    }
}
