package com.c.c;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.ProgressBar;
import com.c.a;

public final class f implements Runnable {
    private ProgressBar a;
    private ProgressDialog b;
    private Activity c;
    private View d;
    private boolean e;
    private int f;
    private int g;
    private String h;

    public f(Object obj) {
        if (obj instanceof ProgressBar) {
            this.a = (ProgressBar) obj;
        } else if (obj instanceof ProgressDialog) {
            this.b = (ProgressDialog) obj;
        } else if (obj instanceof Activity) {
            this.c = (Activity) obj;
        } else if (obj instanceof View) {
            this.d = (View) obj;
        }
    }

    public final void a() {
        if (this.a != null) {
            this.a.setProgress(0);
            this.a.setMax(10000);
        }
        if (this.b != null) {
            this.b.setProgress(0);
            this.b.setMax(10000);
        }
        if (this.c != null) {
            this.c.setProgress(0);
        }
        this.e = false;
        this.g = 0;
        this.f = 10000;
    }

    public final void a(int i) {
        if (i <= 0) {
            this.e = true;
            i = 10000;
        }
        this.f = i;
        if (this.a != null) {
            this.a.setProgress(0);
            this.a.setMax(i);
        }
        if (this.b != null) {
            this.b.setProgress(0);
            this.b.setMax(i);
        }
    }

    public final void b() {
        if (this.a != null) {
            this.a.setProgress(this.a.getMax());
        }
        if (this.b != null) {
            this.b.setProgress(this.b.getMax());
        }
        if (this.c != null) {
            this.c.setProgress(9999);
        }
    }

    public final void b(int i) {
        int i2;
        int i3 = 1;
        if (this.a != null) {
            this.a.incrementProgressBy(this.e ? 1 : i);
        }
        if (this.b != null) {
            ProgressDialog progressDialog = this.b;
            if (!this.e) {
                i3 = i;
            }
            progressDialog.incrementProgressBy(i3);
        }
        if (this.c != null) {
            if (this.e) {
                i2 = this.g;
                this.g = i2 + 1;
            } else {
                this.g += i;
                i2 = (this.g * 10000) / this.f;
            }
            if (i2 > 9999) {
                i2 = 9999;
            }
            this.c.setProgress(i2);
        }
    }

    public final void run() {
        String str = this.h;
        if (this.b != null) {
            new a(this.b.getContext()).b(this.b);
        }
        if (this.c != null) {
            this.c.setProgressBarIndeterminateVisibility(false);
            this.c.setProgressBarVisibility(false);
        }
        if (this.a != null) {
            this.a.setTag(1090453505, str);
            this.a.setVisibility(0);
        }
        View view = this.a;
        if (view == null) {
            view = this.d;
        }
        if (view != null) {
            Object tag = view.getTag(1090453505);
            if (tag == null || tag.equals(str)) {
                view.setTag(1090453505, null);
                if (this.a != null && this.a.isIndeterminate()) {
                    view.setVisibility(8);
                }
            }
        }
    }
}
