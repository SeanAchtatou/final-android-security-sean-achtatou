package com.c.c;

import android.app.Activity;
import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import com.c.a;
import java.io.File;
import java.util.Comparator;

public final class c implements TextWatcher, View.OnClickListener, View.OnLongClickListener, AbsListView.OnScrollListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, Runnable, Comparator<File> {
    private Object a;
    private String b;
    private Object[] c;
    private boolean d;
    private Class<?>[] e;
    private int f;
    private long g;
    private int h = 0;
    private AbsListView.OnScrollListener i;
    private int j;
    private AdapterView.OnItemSelectedListener k;
    private boolean l = false;

    private void a(AbsListView absListView, int i2) {
        int count = absListView.getCount();
        int lastVisiblePosition = absListView.getLastVisiblePosition();
        if (i2 != 0 || count != lastVisiblePosition + 1) {
            this.j = -1;
        } else if (lastVisiblePosition != this.j) {
            this.j = lastVisiblePosition;
            b(absListView, Integer.valueOf(i2));
        }
    }

    public static void a(Object obj, String str, boolean z) {
        if (obj == null) {
            return;
        }
        if (obj instanceof View) {
            View view = (View) obj;
            ProgressBar progressBar = obj instanceof ProgressBar ? (ProgressBar) obj : null;
            if (z) {
                view.setTag(1090453505, str);
                view.setVisibility(0);
                if (progressBar != null) {
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    return;
                }
                return;
            }
            Object tag = view.getTag(1090453505);
            if (tag == null || tag.equals(str)) {
                view.setTag(1090453505, null);
                if (progressBar == null || progressBar.isIndeterminate()) {
                    view.setVisibility(8);
                }
            }
        } else if (obj instanceof Dialog) {
            Dialog dialog = (Dialog) obj;
            a aVar = new a(dialog.getContext());
            if (z) {
                aVar.a(dialog);
            } else {
                aVar.b(dialog);
            }
        } else if (obj instanceof Activity) {
            Activity activity = (Activity) obj;
            activity.setProgressBarIndeterminateVisibility(z);
            activity.setProgressBarVisibility(z);
            if (z) {
                activity.setProgress(0);
            }
        }
    }

    private Object b(Object... objArr) {
        if (this.b != null) {
            if (this.c != null) {
                objArr = this.c;
            }
            Object obj = this.a;
            if (obj == null) {
                obj = this;
            }
            return a.a(obj, this.b, this.d, this.e, objArr);
        }
        if (this.f != 0) {
            switch (this.f) {
                case 1:
                    File file = (File) this.c[0];
                    a.a(file, (byte[]) this.c[1]);
                    if (this.g > 0) {
                        file.setLastModified(this.g);
                        break;
                    }
                    break;
                case 2:
                    a.a((File) this.c[0], ((Long) this.c[1]).longValue(), ((Long) this.c[2]).longValue());
                    break;
            }
        }
        return null;
    }

    public final c a(Object... objArr) {
        this.f = 1;
        this.c = objArr;
        return this;
    }

    public final void a(long j2) {
        this.g = j2;
    }

    public final void afterTextChanged(Editable editable) {
    }

    public final void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        long lastModified = ((File) obj).lastModified();
        long lastModified2 = ((File) obj2).lastModified();
        if (lastModified2 > lastModified) {
            return 1;
        }
        return lastModified2 == lastModified ? 0 : -1;
    }

    public final void onClick(View view) {
        b(view);
    }

    public final void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        b(adapterView, view, Integer.valueOf(i2), Long.valueOf(j2));
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final void onItemSelected(android.widget.AdapterView<?> r9, android.view.View r10, int r11, long r12) {
        /*
            r8 = this;
            r6 = 0
            r7 = 1090453508(0x40ff0004, float:7.968752)
            r0 = 4
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r0[r6] = r9
            r1 = 1
            r0[r1] = r10
            r1 = 2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            r0[r1] = r2
            r1 = 3
            java.lang.Long r2 = java.lang.Long.valueOf(r12)
            r0[r1] = r2
            r8.b(r0)
            android.widget.AdapterView$OnItemSelectedListener r0 = r8.k
            if (r0 == 0) goto L_0x002a
            android.widget.AdapterView$OnItemSelectedListener r0 = r8.k
            r1 = r9
            r2 = r10
            r3 = r11
            r4 = r12
            r0.onItemSelected(r1, r2, r3, r4)
        L_0x002a:
            boolean r0 = r8.l
            if (r0 == 0) goto L_0x006b
            java.lang.Object r0 = r9.getTag(r7)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            if (r0 == r11) goto L_0x006b
            android.widget.Adapter r2 = r9.getAdapter()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
            r9.setTag(r7, r0)
            int r3 = r9.getChildCount()
            int r4 = r9.getFirstVisiblePosition()
            r1 = r6
        L_0x004e:
            if (r1 >= r3) goto L_0x006b
            android.view.View r5 = r9.getChildAt(r1)
            int r6 = r4 + r1
            java.lang.Object r0 = r5.getTag(r7)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x0064
            int r0 = r0.intValue()
            if (r0 == r6) goto L_0x0067
        L_0x0064:
            r2.getView(r6, r5, r9)
        L_0x0067:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x004e
        L_0x006b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.c.c.c.onItemSelected(android.widget.AdapterView, android.view.View, int, long):void");
    }

    public final boolean onLongClick(View view) {
        Object b2 = b(view);
        if (b2 instanceof Boolean) {
            return ((Boolean) b2).booleanValue();
        }
        return false;
    }

    public final void onNothingSelected(AdapterView<?> adapterView) {
        if (this.k != null) {
            this.k.onNothingSelected(adapterView);
        }
    }

    public final void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        a(absListView, this.h);
        if (this.i != null) {
            this.i.onScroll(absListView, i2, i3, i4);
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i2) {
        this.h = i2;
        a(absListView, i2);
        if (absListView instanceof ExpandableListView) {
            ExpandableListView expandableListView = (ExpandableListView) absListView;
            expandableListView.setTag(1090453508, Integer.valueOf(i2));
            if (i2 == 0) {
                int firstVisiblePosition = expandableListView.getFirstVisiblePosition();
                int lastVisiblePosition = expandableListView.getLastVisiblePosition() - firstVisiblePosition;
                ExpandableListAdapter expandableListAdapter = expandableListView.getExpandableListAdapter();
                for (int i3 = 0; i3 <= lastVisiblePosition; i3++) {
                    long expandableListPosition = expandableListView.getExpandableListPosition(i3 + firstVisiblePosition);
                    int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
                    int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
                    if (packedPositionGroup >= 0) {
                        View childAt = expandableListView.getChildAt(i3);
                        Long l2 = (Long) childAt.getTag(1090453508);
                        if (l2 != null && l2.longValue() == expandableListPosition) {
                            if (packedPositionChild == -1) {
                                expandableListAdapter.getGroupView(packedPositionGroup, expandableListView.isGroupExpanded(packedPositionGroup), childAt, expandableListView);
                            } else {
                                expandableListAdapter.getChildView(packedPositionGroup, packedPositionChild, packedPositionChild == expandableListAdapter.getChildrenCount(packedPositionGroup) + -1, childAt, expandableListView);
                            }
                            childAt.setTag(1090453508, null);
                        }
                    }
                }
            }
        } else {
            absListView.setTag(1090453508, Integer.valueOf(i2));
            if (i2 == 0) {
                int firstVisiblePosition2 = absListView.getFirstVisiblePosition();
                int lastVisiblePosition2 = absListView.getLastVisiblePosition() - firstVisiblePosition2;
                ListAdapter listAdapter = (ListAdapter) absListView.getAdapter();
                for (int i4 = 0; i4 <= lastVisiblePosition2; i4++) {
                    long j2 = (long) (i4 + firstVisiblePosition2);
                    View childAt2 = absListView.getChildAt(i4);
                    if (((Number) childAt2.getTag(1090453508)) != null) {
                        listAdapter.getView((int) j2, childAt2, absListView);
                        childAt2.setTag(1090453508, null);
                    }
                }
            }
        }
        if (this.i != null) {
            this.i.onScrollStateChanged(absListView, i2);
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        b(charSequence, Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
    }

    public final void run() {
        b(new Object[0]);
    }
}
