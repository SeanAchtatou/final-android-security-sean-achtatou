package com.umeng.fb.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.common.b.g;
import com.umeng.fb.b;
import com.umeng.fb.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    public static synchronized String a(Context context, JSONArray jSONArray) {
        String str;
        synchronized (c.class) {
            if (jSONArray != null) {
                if (jSONArray.length() != 0) {
                    str = PoiTypeDef.All;
                    for (int i = 0; i < jSONArray.length(); i++) {
                        try {
                            JSONArray jSONArray2 = jSONArray.getJSONArray(i);
                            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                                if (!jSONArray2.getString(i2).equals("end")) {
                                    JSONObject jSONObject = jSONArray2.getJSONObject(i2);
                                    if (f.as.equalsIgnoreCase(jSONObject.optString("type")) && b(context, jSONObject)) {
                                        str = d(context, jSONObject.optString(f.W));
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            str = PoiTypeDef.All;
        }
        return str;
    }

    public static synchronized List<b> a(Context context) {
        ArrayList arrayList;
        synchronized (c.class) {
            arrayList = new ArrayList();
            try {
                Iterator<?> it = context.getSharedPreferences(f.z, 0).getAll().values().iterator();
                while (it.hasNext()) {
                    arrayList.add(new b(new JSONArray((String) it.next())));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static synchronized void a(Context context, b bVar, int i) {
        synchronized (c.class) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(f.z, 0);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            String str = bVar.c;
            String string = sharedPreferences.getString(str, null);
            try {
                JSONArray jSONArray = new JSONArray();
                JSONArray jSONArray2 = new JSONArray(string);
                if (jSONArray2.length() == 1) {
                    edit.remove(bVar.c);
                } else {
                    for (int i2 = 0; i2 <= jSONArray2.length() - 1; i2++) {
                        if (i2 != i) {
                            jSONArray.put(jSONArray2.getJSONObject(i2));
                        }
                    }
                    edit.putString(str, jSONArray.toString());
                }
                edit.commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            bVar.b(i);
        }
        return;
    }

    public static synchronized void a(Context context, String str) {
        synchronized (c.class) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(f.A, 0);
            if (!g.c(str)) {
                String str2 = PoiTypeDef.All;
                for (String str3 : sharedPreferences.getString(f.B, PoiTypeDef.All).split(",")) {
                    if (!g.c(str3) && !str3.equals(str)) {
                        str2 = String.valueOf(str2) + "," + str3.trim();
                    }
                }
                a(sharedPreferences, f.B, str2);
            }
        }
    }

    public static synchronized void a(Context context, String str, String str2) {
        synchronized (c.class) {
            context.getSharedPreferences(str, 0).edit().remove(str2).commit();
        }
    }

    private static synchronized void a(SharedPreferences sharedPreferences, String str, String str2) {
        synchronized (c.class) {
            sharedPreferences.edit().putString(str, str2).commit();
        }
    }

    public static boolean a(Context context, b bVar) {
        return context.getSharedPreferences(f.A, 0).getString(f.B, PoiTypeDef.All).contains(bVar.c);
    }

    public static synchronized boolean a(Context context, JSONObject jSONObject) {
        boolean z = false;
        synchronized (c.class) {
            String optString = jSONObject.optString(f.W);
            SharedPreferences sharedPreferences = context.getSharedPreferences(f.z, 0);
            if (!g.c(optString)) {
                a(sharedPreferences, optString, "[" + jSONObject.toString() + "]");
                z = true;
            }
        }
        return z;
    }

    public static synchronized b b(Context context, String str) {
        b bVar;
        synchronized (c.class) {
            try {
                bVar = new b(new JSONArray(context.getSharedPreferences(f.z, 0).getString(str, null)));
            } catch (Exception e) {
                e.printStackTrace();
                bVar = null;
            }
        }
        return bVar;
    }

    public static synchronized boolean b(Context context, JSONObject jSONObject) {
        boolean z = true;
        synchronized (c.class) {
            String optString = jSONObject.optString(f.W);
            SharedPreferences sharedPreferences = context.getSharedPreferences(f.z, 0);
            try {
                JSONArray jSONArray = new JSONArray(sharedPreferences.getString(optString, null));
                if (f.aq.equals(jSONObject.opt("type"))) {
                    b bVar = new b(jSONArray);
                    int length = jSONArray.length() - 1;
                    while (true) {
                        if (length >= 0) {
                            String optString2 = jSONArray.getJSONObject(length).optString("reply_id", null);
                            String optString3 = jSONObject.optString("reply_id", null);
                            if (optString2 != null && optString2.equalsIgnoreCase(optString3)) {
                                a(context, bVar, length);
                                break;
                            }
                            length--;
                        } else {
                            break;
                        }
                    }
                    JSONArray jSONArray2 = new JSONArray(sharedPreferences.getString(optString, null));
                    jSONArray2.put(jSONObject);
                    a(sharedPreferences, optString, jSONArray2.toString());
                } else {
                    SharedPreferences sharedPreferences2 = context.getSharedPreferences(f.M, 0);
                    String string = sharedPreferences2.getString(optString, "RP0");
                    String optString4 = jSONObject.optString("reply_id");
                    if (!a.a(string, optString4)) {
                        jSONObject.put(f.U, g.a());
                        jSONArray.put(jSONObject);
                        a(sharedPreferences, optString, jSONArray.toString());
                        a(sharedPreferences2, optString, optString4);
                        a(context.getSharedPreferences(f.A, 0), f.C, optString4);
                    }
                    z = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z;
    }

    public static synchronized void c(Context context, String str) {
        synchronized (c.class) {
            context.getSharedPreferences(f.z, 0).edit().remove(str).commit();
        }
    }

    public static synchronized void c(Context context, JSONObject jSONObject) {
        synchronized (c.class) {
            b.d(jSONObject);
            e(context, jSONObject);
        }
    }

    private static synchronized String d(Context context, String str) {
        String string;
        synchronized (c.class) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(f.A, 0);
            string = sharedPreferences.getString(f.B, PoiTypeDef.All);
            if (!string.contains(str)) {
                string = String.valueOf(string) + "," + str;
                a(sharedPreferences, f.B, string);
            }
        }
        return string;
    }

    public static synchronized void d(Context context, JSONObject jSONObject) {
        synchronized (c.class) {
            b.c(jSONObject);
            e(context, jSONObject);
        }
    }

    public static synchronized boolean e(Context context, JSONObject jSONObject) {
        boolean z = false;
        synchronized (c.class) {
            String optString = jSONObject.optString(f.W);
            SharedPreferences sharedPreferences = context.getSharedPreferences(f.z, 0);
            try {
                JSONArray jSONArray = new JSONArray(sharedPreferences.getString(optString, "[]"));
                jSONArray.put(jSONObject);
                a(sharedPreferences, optString, jSONArray.toString());
                z = true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return z;
    }
}
