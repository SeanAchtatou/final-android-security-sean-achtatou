package com.umeng.fb.util;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.umeng.fb.b;
import com.umeng.fb.f;
import com.umeng.fb.ui.FeedbackConversation;
import com.umeng.fb.ui.FeedbackConversations;
import com.umeng.fb.ui.SendFeedback;
import java.util.Map;

public class ActivityStarter {
    public static Map<String, String> contactMap = null;
    public static Context lastContext = null;
    public static Map<String, String> remarkMap = null;
    public static boolean useGoBackButton = false;
    public static Context userContext = null;

    public static void openDetailActivity(Context context, b bVar) {
        if (userContext == null) {
            userContext = context;
        }
        FeedbackConversation.setUserContext(context);
        Intent intent = new Intent(context, FeedbackConversation.class);
        intent.setFlags(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY);
        intent.putExtra(f.W, bVar.c);
        context.startActivity(intent);
    }

    public static void openFeedbackListActivity(Context context) {
        if (userContext == null) {
            userContext = context;
        }
        Intent intent = new Intent(context, FeedbackConversations.class);
        intent.setFlags(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY);
        context.startActivity(intent);
    }

    public static void openSendFeedbackActivity(Context context) {
        if (userContext == null) {
            userContext = context;
        } else if (context instanceof FeedbackConversations) {
            lastContext = context;
        }
        openSendFeedbackActivity(context, null);
    }

    public static void openSendFeedbackActivity(Context context, b bVar) {
        Intent intent = new Intent(context, SendFeedback.class);
        intent.setFlags(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY);
        if (bVar != null && bVar.b == b.a.PureFail) {
            intent.putExtra(f.W, bVar.c);
        }
        context.startActivity(intent);
    }
}
