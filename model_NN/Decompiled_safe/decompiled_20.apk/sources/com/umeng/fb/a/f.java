package com.umeng.fb.a;

import android.content.Context;
import android.content.Intent;
import com.umeng.fb.util.a;
import com.umeng.fb.util.b;
import com.umeng.fb.util.c;
import java.util.concurrent.Callable;
import org.json.JSONObject;

public class f implements Callable<Boolean> {
    static String a = "MsgWorker";
    JSONObject b;
    Context c;

    public f(JSONObject jSONObject, Context context) {
        this.b = jSONObject;
        this.c = context;
    }

    /* renamed from: a */
    public Boolean call() {
        String str;
        String str2;
        String str3;
        JSONObject jSONObject;
        String optString = this.b.optString("type");
        String optString2 = this.b.optString(com.umeng.fb.f.W);
        if (com.umeng.fb.f.aq.equals(optString)) {
            str = com.umeng.fb.f.aq;
            str2 = "http://feedback.whalecloud.com/feedback/reply";
            str3 = "reply";
        } else {
            str = com.umeng.fb.f.ar;
            str2 = com.umeng.fb.f.y;
            str3 = com.umeng.fb.f.z;
        }
        try {
            jSONObject = a.a(str2, str3, this.b).a();
        } catch (Exception e) {
            e.printStackTrace();
            jSONObject = null;
        }
        Intent putExtra = new Intent().setAction(com.umeng.fb.f.aH).putExtra("type", str).putExtra(com.umeng.fb.f.W, optString2);
        if (b.a(jSONObject)) {
            b.e(this.b);
            putExtra.putExtra(com.umeng.fb.f.ax, com.umeng.fb.f.az);
        } else {
            b.c(this.b);
            putExtra.putExtra(com.umeng.fb.f.ax, "fail");
        }
        if (com.umeng.fb.f.aq.equals(optString)) {
            c.b(this.c, this.b);
        } else {
            c.a(this.c, com.umeng.fb.f.z, optString2);
            c.a(this.c, this.b);
        }
        this.c.sendBroadcast(putExtra);
        return null;
    }
}
