package com.umeng.fb.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.fb.a;
import com.umeng.fb.b.c;
import com.umeng.fb.b.d;
import com.umeng.fb.b.e;

public class b extends BaseAdapter {
    private static /* synthetic */ int[] f;
    Context a;
    LayoutInflater b;
    String c;
    String d = "FeedbackAdapter";
    com.umeng.fb.b e;

    class a {
        LinearLayout a;
        RelativeLayout b;
        TextView c;
        TextView d;
        View e;
        View f;

        a() {
        }
    }

    public b(Context context, com.umeng.fb.b bVar) {
        this.a = context;
        this.e = bVar;
        this.b = LayoutInflater.from(context);
    }

    private void a(com.umeng.fb.a aVar, TextView textView) {
        switch (a()[aVar.g.ordinal()]) {
            case 1:
                textView.setText(this.a.getString(e.t(this.a)));
                textView.setTextColor(-7829368);
                return;
            case 2:
                textView.setText(this.a.getString(e.u(this.a)));
                textView.setTextColor(-65536);
                return;
            case 3:
            default:
                String b2 = com.umeng.fb.util.a.b(aVar.e, this.a);
                if (PoiTypeDef.All.equals(b2)) {
                    textView.setText(PoiTypeDef.All);
                    return;
                }
                textView.setText(b2);
                textView.setTextColor(-7829368);
                return;
            case 4:
                textView.setText(this.a.getString(e.v(this.a)));
                textView.setTextColor(-65536);
                return;
        }
    }

    static /* synthetic */ int[] a() {
        int[] iArr = f;
        if (iArr == null) {
            iArr = new int[a.C0005a.values().length];
            try {
                iArr[a.C0005a.Fail.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[a.C0005a.OK.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[a.C0005a.Resending.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[a.C0005a.Sending.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            f = iArr;
        }
        return iArr;
    }

    public void a(com.umeng.fb.b bVar) {
        this.e = bVar;
    }

    public int getCount() {
        if (this.e == null) {
            return 0;
        }
        return this.e.f.size();
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        if (view == null) {
            view = this.b.inflate(d.e(this.a), (ViewGroup) null);
            a aVar2 = new a();
            aVar2.a = (LinearLayout) view.findViewById(c.p(this.a));
            aVar2.b = (RelativeLayout) aVar2.a.findViewById(c.q(this.a));
            aVar2.c = (TextView) aVar2.a.findViewById(c.r(this.a));
            aVar2.d = (TextView) aVar2.a.findViewById(c.s(this.a));
            aVar2.e = view.findViewById(c.t(this.a));
            aVar2.f = view.findViewById(c.u(this.a));
            view.setTag(aVar2);
            aVar = aVar2;
        } else {
            aVar = (a) view.getTag();
        }
        com.umeng.fb.a a2 = this.e.a(i);
        a(a2, aVar.d);
        aVar.c.setText(a2.a());
        if (a2.f == a.b.DevReply) {
            aVar.a.setGravity(5);
            aVar.b.setBackgroundResource(com.umeng.fb.b.b.b(this.a));
            aVar.f.setVisibility(8);
            aVar.e.setVisibility(0);
        } else {
            aVar.a.setGravity(3);
            aVar.b.setBackgroundResource(com.umeng.fb.b.b.c(this.a));
            aVar.f.setVisibility(0);
            aVar.e.setVisibility(8);
        }
        return view;
    }
}
