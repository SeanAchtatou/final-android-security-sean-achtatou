package com.umeng.fb.ui;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.umeng.fb.util.ActivityStarter;

class e implements View.OnClickListener {
    final /* synthetic */ SendFeedback a;

    e(SendFeedback sendFeedback) {
        this.a = sendFeedback;
    }

    public void onClick(View view) {
        ActivityStarter.openFeedbackListActivity(this.a);
        this.a.finish();
        ((InputMethodManager) this.a.getSystemService("input_method")).hideSoftInputFromWindow(this.a.e.getWindowToken(), 0);
    }
}
