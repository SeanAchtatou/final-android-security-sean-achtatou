package com.umeng.fb.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.common.b.g;
import com.umeng.fb.a;
import com.umeng.fb.b;
import com.umeng.fb.b.c;
import com.umeng.fb.b.e;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;

public class d extends BaseAdapter {
    LayoutInflater a;
    Context b;
    List<b> c;
    JSONArray d;
    String[] e;
    String f = PoiTypeDef.All;
    String g = "FeedbackListAdapter";

    class a {
        ImageView a;
        TextView b;
        TextView c;
        TextView d;

        a() {
        }
    }

    public d(Context context, List<b> list) {
        this.b = context;
        this.a = LayoutInflater.from(context);
        Collections.sort(list);
        this.c = list;
    }

    private String a(b bVar) {
        return bVar.d.a();
    }

    private String b(b bVar) {
        if (bVar.b == b.a.Normal) {
            for (int size = bVar.f.size() - 1; size >= 0; size--) {
                a.C0005a aVar = bVar.a(size).g;
                if (aVar == a.C0005a.Sending) {
                    return this.b.getString(e.g(this.b));
                }
                if (aVar == a.C0005a.Fail) {
                    return this.b.getString(e.h(this.b));
                }
                if (aVar == a.C0005a.Resending) {
                    return this.b.getString(e.i(this.b));
                }
            }
        } else if (bVar.b == b.a.PureFail) {
            return this.b.getString(e.j(this.b));
        } else {
            if (bVar.b == b.a.PureSending) {
                return this.b.getString(e.g(this.b));
            }
        }
        return PoiTypeDef.All;
    }

    private String c(b bVar) {
        if (bVar.f.size() == 1 || bVar.e.f != a.b.DevReply) {
            return null;
        }
        return bVar.e.a();
    }

    private String d(b bVar) {
        return com.umeng.fb.util.a.a(bVar.e.e, this.b);
    }

    public b.a a(int i) {
        return this.c.get(i).b;
    }

    public void a(List<b> list) {
        Collections.sort(list);
        this.c = list;
    }

    public b b(int i) {
        return this.c.get(i);
    }

    public int getCount() {
        return this.c.size();
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        if (view == null || view.getTag() == null) {
            view = this.a.inflate(com.umeng.fb.b.d.b(this.b), (ViewGroup) null);
            aVar = new a();
            aVar.a = (ImageView) view.findViewById(c.g(this.b));
            aVar.b = (TextView) view.findViewById(c.h(this.b));
            aVar.c = (TextView) view.findViewById(c.i(this.b));
            aVar.d = (TextView) view.findViewById(c.j(this.b));
            view.setTag(aVar);
        } else {
            aVar = (a) view.getTag();
        }
        b bVar = this.c.get(i);
        String a2 = a(bVar);
        String c2 = c(bVar);
        String b2 = b(bVar);
        String d2 = d(bVar);
        aVar.b.setText(a2);
        if (c2 == null) {
            aVar.c.setVisibility(8);
        } else {
            aVar.c.setVisibility(0);
            aVar.c.setText(c2);
        }
        if (g.c(b2)) {
            aVar.d.setText(d2);
        } else {
            aVar.d.setText(b2);
        }
        if (com.umeng.fb.util.c.a(this.b, bVar)) {
            aVar.a.setVisibility(0);
            aVar.a.setBackgroundResource(com.umeng.fb.b.b.a(this.b));
        } else {
            aVar.a.setVisibility(4);
        }
        return view;
    }
}
