package com.umeng.fb.ui;

import android.view.View;
import com.umeng.fb.util.ActivityStarter;

class c implements View.OnClickListener {
    final /* synthetic */ FeedbackConversations a;

    c(FeedbackConversations feedbackConversations) {
        this.a = feedbackConversations;
    }

    public void onClick(View view) {
        ActivityStarter.openSendFeedbackActivity(this.a);
    }
}
