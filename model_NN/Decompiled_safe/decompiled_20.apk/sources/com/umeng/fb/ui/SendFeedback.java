package com.umeng.fb.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.common.b.g;
import com.umeng.fb.UMFeedbackService;
import com.umeng.fb.b.d;
import com.umeng.fb.b.e;
import com.umeng.fb.f;
import com.umeng.fb.util.ActivityStarter;
import com.umeng.fb.util.FeedBackListener;
import com.umeng.fb.util.c;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SendFeedback extends Activity {
    static boolean a = true;
    private static final String b = SendFeedback.class.getName();
    public static ExecutorService executorService = Executors.newFixedThreadPool(3);
    /* access modifiers changed from: private */
    public Spinner c;
    /* access modifiers changed from: private */
    public Spinner d;
    /* access modifiers changed from: private */
    public EditText e;
    private TextView f;
    private TextView g;
    private ImageButton h;
    /* access modifiers changed from: private */
    public FeedBackListener i;
    /* access modifiers changed from: private */
    public Map<String, String> j;
    /* access modifiers changed from: private */
    public Map<String, String> k;
    /* access modifiers changed from: private */
    public JSONObject l;

    class a implements View.OnClickListener {
        private a() {
        }

        /* synthetic */ a(SendFeedback sendFeedback, a aVar) {
            this();
        }

        public void onClick(View view) {
            SendFeedback.this.finish();
            if (ActivityStarter.lastContext != null) {
                ((Activity) ActivityStarter.lastContext).finish();
            }
        }
    }

    class b implements View.OnClickListener {
        private b() {
        }

        /* synthetic */ b(SendFeedback sendFeedback, b bVar) {
            this();
        }

        public void onClick(View view) {
            int i = -1;
            String editable = SendFeedback.this.e != null ? SendFeedback.this.e.getText().toString() : null;
            if (g.c(editable)) {
                Toast.makeText(SendFeedback.this, SendFeedback.this.getString(e.a(SendFeedback.this)), 0).show();
            } else if (editable.length() > 140) {
                Toast.makeText(SendFeedback.this, SendFeedback.this.getString(e.b(SendFeedback.this)), 0).show();
            } else {
                int selectedItemPosition = SendFeedback.this.c != null ? SendFeedback.this.c.getSelectedItemPosition() : -1;
                if (SendFeedback.this.d != null) {
                    i = SendFeedback.this.d.getSelectedItemPosition();
                }
                if (SendFeedback.this.i != null) {
                    SendFeedback.this.i.onSubmitFB(SendFeedback.this);
                    SendFeedback.this.j = ActivityStarter.contactMap;
                    SendFeedback.this.k = ActivityStarter.remarkMap;
                    JSONObject jSONObject = new JSONObject();
                    JSONObject jSONObject2 = new JSONObject();
                    JSONObject json = SendFeedback.this.j != null ? SendFeedback.getJSON(SendFeedback.this.j) : null;
                    JSONObject json2 = SendFeedback.this.k != null ? SendFeedback.getJSON(SendFeedback.this.k) : null;
                    if (json != null) {
                        try {
                            jSONObject.put(f.I, json);
                        } catch (Exception e) {
                        }
                    }
                    if (json2 != null) {
                        jSONObject2.put(f.J, json2);
                    }
                    SharedPreferences.Editor edit = SendFeedback.this.getSharedPreferences(f.A, 0).edit();
                    edit.putInt(f.E, selectedItemPosition);
                    edit.putInt(f.F, i);
                    if (jSONObject.length() > 0) {
                        edit.putString(f.G, jSONObject.toString());
                    }
                    if (jSONObject2.length() > 0) {
                        edit.putString(f.H, jSONObject2.toString());
                    }
                    edit.commit();
                    try {
                        SendFeedback.this.l = com.umeng.fb.util.b.a(SendFeedback.this, editable, selectedItemPosition, i, json, json2);
                    } catch (Exception e2) {
                        if (f.h) {
                            e2.printStackTrace();
                        }
                        c.d(SendFeedback.this, SendFeedback.this.l);
                        return;
                    }
                } else {
                    SharedPreferences.Editor edit2 = SendFeedback.this.getSharedPreferences(f.A, 0).edit();
                    edit2.putInt(f.E, selectedItemPosition);
                    edit2.putInt(f.F, i);
                    edit2.commit();
                    try {
                        SendFeedback.this.l = com.umeng.fb.util.b.a(SendFeedback.this, editable, selectedItemPosition, i, null, null);
                    } catch (Exception e3) {
                        if (f.h) {
                            e3.printStackTrace();
                        }
                        c.d(SendFeedback.this, SendFeedback.this.l);
                        return;
                    }
                }
                c.c(SendFeedback.this, SendFeedback.this.l);
                SendFeedback.executorService.submit(new com.umeng.fb.a.f(SendFeedback.this.l, SendFeedback.this));
                SendFeedback.this.startActivity(new Intent(SendFeedback.this, FeedbackConversations.class).setFlags(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY));
                ((InputMethodManager) SendFeedback.this.getSystemService("input_method")).hideSoftInputFromWindow(SendFeedback.this.e.getWindowToken(), 0);
                SendFeedback.this.finish();
            }
        }
    }

    private void a() {
        this.c = (Spinner) findViewById(com.umeng.fb.b.c.a(this));
        this.d = (Spinner) findViewById(com.umeng.fb.b.c.b(this));
        this.f = (TextView) findViewById(com.umeng.fb.b.c.c(this));
        if (ActivityStarter.useGoBackButton) {
            this.g = (TextView) findViewById(com.umeng.fb.b.c.d(this));
            this.g.setVisibility(0);
        } else {
            this.g = (TextView) findViewById(com.umeng.fb.b.c.d(this));
            this.g.setVisibility(4);
        }
        this.e = (EditText) findViewById(com.umeng.fb.b.c.e(this));
        this.h = (ImageButton) findViewById(com.umeng.fb.b.c.f(this));
        if (this.c != null) {
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, getResources().getStringArray(com.umeng.fb.b.a.a(this)));
            arrayAdapter.setDropDownViewResource(17367049);
            this.c.setAdapter((SpinnerAdapter) arrayAdapter);
        }
        if (this.d != null) {
            ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, 17367048, getResources().getStringArray(com.umeng.fb.b.a.b(this)));
            arrayAdapter2.setDropDownViewResource(17367049);
            this.d.setAdapter((SpinnerAdapter) arrayAdapter2);
        }
        if (this.h != null) {
            this.h.setOnClickListener(new e(this));
        }
        b();
        c();
    }

    private void b() {
        if (this.e != null) {
            this.e.setHint(getString(e.d(this)));
        }
        if (this.f != null) {
            this.f.setText(getString(e.e(this)));
        }
        if (this.g != null) {
            this.g.setText(getString(e.f(this)));
        }
    }

    private void c() {
        int e2;
        int d2;
        String stringExtra = getIntent().getStringExtra(f.W);
        if (!(stringExtra == null || this.e == null)) {
            String string = getSharedPreferences(f.z, 0).getString(stringExtra, null);
            if (!g.c(string)) {
                try {
                    this.e.setText(new com.umeng.fb.a(new JSONArray(string).getJSONObject(0)).a());
                    c.a(this, f.z, stringExtra);
                } catch (Exception e3) {
                    if (f.h) {
                        e3.printStackTrace();
                    }
                }
            }
        }
        if (!(this.c == null || (d2 = d()) == -1)) {
            this.c.setSelection(d2);
        }
        if (!(this.d == null || (e2 = e()) == -1)) {
            this.d.setSelection(e2);
        }
        f();
    }

    private int d() {
        return getSharedPreferences(f.A, 0).getInt(f.E, -1);
    }

    private int e() {
        return getSharedPreferences(f.A, 0).getInt(f.F, -1);
    }

    private void f() {
        try {
            SharedPreferences sharedPreferences = getSharedPreferences(f.A, 0);
            String string = sharedPreferences.getString(f.G, PoiTypeDef.All);
            String string2 = sharedPreferences.getString(f.H, PoiTypeDef.All);
            if (string == null || string.length() <= 0) {
                this.j = null;
            } else {
                this.j = getMap(new JSONObject(string).getJSONObject(f.I));
            }
            if (string2 == null || string2.length() <= 0) {
                this.k = null;
            } else {
                this.k = getMap(new JSONObject(string2).getJSONObject(f.J));
            }
            if (this.i != null) {
                this.i.onResetFB(this, this.j, this.k);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public static JSONObject getJSON(Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry next : map.entrySet()) {
            try {
                jSONObject.put((String) next.getKey(), next.getValue().toString());
            } catch (Exception e2) {
                System.out.println(e2.getMessage());
            }
        }
        return jSONObject;
    }

    public static Map<String, String> getMap(JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            HashMap hashMap = new HashMap();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.get(next).toString());
            }
            return hashMap;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(d.a(this));
        setFBListener(UMFeedbackService.fbListener);
        a();
        if (this.f != null) {
            this.f.setOnClickListener(new b(this, null));
            if (this.e != null) {
                ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 0);
            }
        }
        if (this.g != null) {
            this.g.setOnClickListener(new a(this, null));
            ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.e.getWindowToken(), 0);
        } catch (Exception e2) {
            Log.e(b, e2.getMessage());
        }
        super.onPause();
    }

    public void setFBListener(FeedBackListener feedBackListener) {
        this.i = feedBackListener;
    }
}
