package com.umeng.fb.ui;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import com.umeng.fb.UMFeedbackService;
import com.umeng.fb.a.e;
import com.umeng.fb.b;
import com.umeng.fb.b.d;
import com.umeng.fb.f;
import com.umeng.fb.util.ActivityStarter;
import com.umeng.fb.util.c;

public class FeedbackConversations extends ListActivity {
    private static final int c = 0;
    private static final int d = 1;
    private static final int e = 2;
    private static final int f = 3;
    private static final int g = 4;
    private static /* synthetic */ int[] h;
    a a;
    ImageButton b;

    class a extends BroadcastReceiver {
        d a;

        public a(d dVar) {
            this.a = dVar;
        }

        public void onReceive(Context context, Intent intent) {
            this.a.a(c.a(FeedbackConversations.this));
            this.a.notifyDataSetChanged();
        }
    }

    static /* synthetic */ int[] a() {
        int[] iArr = h;
        if (iArr == null) {
            iArr = new int[b.a.values().length];
            try {
                iArr[b.a.HasFail.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[b.a.Normal.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[b.a.PureFail.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[b.a.PureSending.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            h = iArr;
        }
        return iArr;
    }

    private void b() {
        d dVar = (d) getListAdapter();
        dVar.a(c.a(this));
        dVar.notifyDataSetChanged();
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        b b2 = ((d) getListAdapter()).b(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
        switch (menuItem.getItemId()) {
            case 0:
            case 2:
                c.a(this, b2.c);
                ActivityStarter.openDetailActivity(this, b2);
                break;
            case 1:
                c.c(this, b2.c);
                b();
                break;
            case 3:
                ActivityStarter.openSendFeedbackActivity(this, b2);
                break;
            case 4:
                c.c(this, b2.c);
                b();
                break;
        }
        return super.onContextItemSelected(menuItem);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(d.c(this));
        this.b = (ImageButton) findViewById(com.umeng.fb.b.c.k(this));
        if (this.b != null) {
            this.b.setOnClickListener(new c(this));
        }
        if (!UMFeedbackService.getHasCheckedReply()) {
            new e(this).start();
            new com.umeng.fb.a.d(this).start();
        }
        registerForContextMenu(getListView());
        setListAdapter(new d(this, c.a(this)));
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        b.a aVar = ((d) getListAdapter()).b(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position).b;
        if (aVar == b.a.Normal) {
            contextMenu.add(0, 0, 0, getString(com.umeng.fb.b.e.l(this)));
            contextMenu.add(0, 1, 0, getString(com.umeng.fb.b.e.m(this)));
        } else if (aVar == b.a.PureSending) {
            contextMenu.add(0, 2, 0, getString(com.umeng.fb.b.e.n(this)));
            contextMenu.add(0, 4, 0, getString(com.umeng.fb.b.e.o(this)));
        } else if (aVar == b.a.PureFail) {
            contextMenu.add(0, 3, 0, getString(com.umeng.fb.b.e.p(this)));
            contextMenu.add(0, 4, 0, getString(com.umeng.fb.b.e.o(this)));
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        super.onListItemClick(listView, view, i, j);
        synchronized (((d) getListAdapter()).b(i)) {
            b b2 = ((d) getListAdapter()).b(i);
            b.a aVar = b2.b;
            c.a(this, b2.c);
            switch (a()[aVar.ordinal()]) {
                case 2:
                    ActivityStarter.openSendFeedbackActivity(this, b2);
                    break;
                default:
                    ActivityStarter.openDetailActivity(this, b2);
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        b();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.a = new a((d) getListAdapter());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(f.aH);
        intentFilter.addAction(f.aA);
        registerReceiver(this.a, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        unregisterReceiver(this.a);
    }
}
