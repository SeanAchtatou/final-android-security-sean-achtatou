package com.umeng.fb;

import com.umeng.common.b.g;
import java.util.Date;
import org.json.JSONObject;

public class a implements Comparable<a> {
    String a;
    String b;
    public String c;
    public String d;
    public Date e;
    public b f;
    public C0005a g = C0005a.OK;
    public JSONObject h;

    /* renamed from: com.umeng.fb.a$a  reason: collision with other inner class name */
    public enum C0005a {
        Sending,
        Fail,
        OK,
        Resending
    }

    public enum b {
        Starting,
        UserReply,
        DevReply
    }

    public a(JSONObject jSONObject) {
        if (jSONObject == null) {
            throw new Exception("invalid atom");
        }
        this.h = jSONObject;
        String optString = jSONObject.optString("type");
        if (f.ar.equals(optString)) {
            this.f = b.Starting;
        } else if (f.as.equals(optString)) {
            this.f = b.DevReply;
        } else if (f.aq.equals(optString)) {
            this.f = b.UserReply;
        }
        String optString2 = jSONObject.optString(f.am);
        if (f.av.equalsIgnoreCase(optString2)) {
            this.g = C0005a.Sending;
        } else if ("fail".equalsIgnoreCase(optString2)) {
            this.g = C0005a.Fail;
        } else if ("ok".equalsIgnoreCase(optString2)) {
            this.g = C0005a.OK;
        } else if ("ReSending".equalsIgnoreCase(optString2)) {
            this.g = C0005a.Resending;
        }
        if (this.f == b.Starting) {
            this.a = jSONObject.optString(f.T);
        }
        this.b = jSONObject.optString(f.T);
        if (g.c(this.b)) {
            this.b = jSONObject.optString(f.S);
        }
        this.c = jSONObject.optString(f.W);
        this.e = g.a(jSONObject.optString(f.U));
    }

    /* renamed from: a */
    public int compareTo(a aVar) {
        Date date = aVar.e;
        if (this.e == null || date == null || date.equals(this.e)) {
            return 0;
        }
        return date.after(this.e) ? -1 : 1;
    }

    public String a() {
        return this.f == b.Starting ? this.a : this.b;
    }
}
