package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.Vector;

public class i {
    private static final int b = 4;
    private Vector<Long> a;
    private String c;

    public i(String str) {
        this.a = new Vector<>(4);
        this.c = str;
    }

    public i(String str, int i) {
        this.c = str;
        if (i < 0) {
            this.a = new Vector<>(4);
        } else {
            this.a = new Vector<>(i);
        }
    }

    public static i a(Context context, String str) {
        return a(str, h.e(context).getString(str, null));
    }

    public static i a(String str, String str2) {
        i iVar = new i(str);
        if (!TextUtils.isEmpty(str2)) {
            for (String trim : str2.split(",")) {
                String trim2 = trim.trim();
                if (!TextUtils.isEmpty(trim2)) {
                    Long.valueOf(-1);
                    try {
                        iVar.a(Long.valueOf(Long.parseLong(trim2)));
                    } catch (Exception e) {
                    }
                }
            }
        }
        return iVar;
    }

    public Long a() {
        int size = this.a.size();
        if (size <= 0) {
            return -1L;
        }
        return this.a.remove(size - 1);
    }

    public void a(Context context) {
        String iVar = toString();
        SharedPreferences.Editor edit = h.e(context).edit();
        if (TextUtils.isEmpty(iVar)) {
            edit.remove(this.c).commit();
        } else {
            edit.putString(this.c, iVar).commit();
        }
    }

    public void a(Long l) {
        while (this.a.size() >= 4) {
            this.a.remove(0);
        }
        this.a.add(l);
    }

    public int b() {
        return this.a.size();
    }

    public String toString() {
        int size = this.a.size();
        if (size <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(4);
        for (int i = 0; i < size; i++) {
            stringBuffer.append(this.a.get(i));
            if (i != size - 1) {
                stringBuffer.append(",");
            }
        }
        this.a.clear();
        return stringBuffer.toString();
    }
}
