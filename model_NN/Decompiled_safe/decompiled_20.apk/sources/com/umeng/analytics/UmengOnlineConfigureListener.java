package com.umeng.analytics;

import org.json.JSONObject;

public interface UmengOnlineConfigureListener {
    void onDataReceived(JSONObject jSONObject);
}
