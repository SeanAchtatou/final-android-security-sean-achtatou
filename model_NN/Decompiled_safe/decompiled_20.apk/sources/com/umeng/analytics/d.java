package com.umeng.analytics;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.common.b.g;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class d {
    private ArrayList<JSONObject> a = new ArrayList<>();
    private ArrayList<JSONObject> b = new ArrayList<>();
    private HashMap<String, i> c = new HashMap<>();
    private HashMap<String, Map<String, String>> d = new HashMap<>();
    private int e = 10;

    private synchronized JSONObject a(JSONObject jSONObject, String str) {
        boolean z;
        JSONArray jSONArray;
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        if (this.a.size() > 0) {
            try {
                if (jSONObject.isNull("event")) {
                    JSONArray jSONArray2 = new JSONArray();
                    jSONObject.put("event", jSONArray2);
                    jSONArray = jSONArray2;
                } else {
                    jSONArray = jSONObject.getJSONArray("event");
                }
                Iterator<JSONObject> it = this.a.iterator();
                while (it.hasNext()) {
                    JSONObject next = it.next();
                    next.put("session_id", str);
                    jSONArray.put(next);
                }
                this.a.clear();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (this.b.size() > 0) {
            try {
                if (jSONObject.isNull("ekv")) {
                    JSONArray jSONArray3 = new JSONArray();
                    JSONObject jSONObject2 = new JSONObject();
                    b(jSONObject2, str);
                    jSONArray3.put(jSONObject2);
                    jSONObject.put("ekv", jSONArray3);
                } else {
                    JSONArray jSONArray4 = jSONObject.getJSONArray("ekv");
                    int length = jSONArray4.length() - 1;
                    while (true) {
                        if (length < 0) {
                            z = false;
                            break;
                        } else if (b((JSONObject) jSONArray4.get(length), str)) {
                            z = true;
                            break;
                        } else {
                            length--;
                        }
                    }
                    if (!z) {
                        JSONObject jSONObject3 = new JSONObject();
                        b(jSONObject3, str);
                        jSONArray4.put(jSONObject3);
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return jSONObject;
    }

    private boolean b(JSONObject jSONObject, String str) {
        JSONArray jSONArray;
        boolean z;
        if (jSONObject.has(str)) {
            jSONArray = jSONObject.getJSONArray(str);
            z = true;
        } else {
            jSONArray = new JSONArray();
            z = false;
        }
        Iterator<JSONObject> it = this.b.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next());
        }
        this.b.clear();
        jSONObject.put(str, jSONArray);
        return z;
    }

    public int a() {
        return this.a.size() + this.b.size();
    }

    public void a(int i) {
        this.e = i;
    }

    public void a(Context context) {
        String string;
        if (a() > 0 && (string = h.e(context).getString("session_id", null)) != null) {
            h.a(context, a(h.i(context), string));
        }
    }

    public void a(String str) {
        if (this.c.containsKey(str)) {
            this.c.get(str).a(Long.valueOf(System.currentTimeMillis()));
            return;
        }
        i iVar = new i(str);
        iVar.a(Long.valueOf(System.currentTimeMillis()));
        this.c.put(str, iVar);
    }

    public void a(String str, Map<String, String> map) {
        if (!this.d.containsKey(str)) {
            this.d.put(str, map);
        }
    }

    public synchronized boolean a(Context context, String str, String str2, long j, int i) {
        JSONObject jSONObject = new JSONObject();
        String a2 = g.a();
        String str3 = a2.split(" ")[0];
        String str4 = a2.split(" ")[1];
        try {
            jSONObject.put("tag", str);
            jSONObject.put("date", str3);
            jSONObject.put("time", str4);
            jSONObject.put("acc", i);
            if (!TextUtils.isEmpty(str2)) {
                jSONObject.put("label", str2);
            }
            if (j > 0) {
                jSONObject.put("du", j);
            }
            this.a.add(jSONObject);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return b();
    }

    public synchronized boolean a(Context context, String str, Map<String, String> map, long j) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("id", str);
            jSONObject.put("ts", System.currentTimeMillis());
            Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
            int i = 0;
            while (it.hasNext() && i < 10) {
                int i2 = i + 1;
                Map.Entry next = it.next();
                jSONObject.put((String) next.getKey(), (String) next.getValue());
                i = i2;
            }
            if (j > 0) {
                jSONObject.put("du", j);
            }
            this.b.add(jSONObject);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return b();
    }

    public long b(String str) {
        if (this.c.containsKey(str)) {
            return this.c.get(str).a().longValue();
        }
        return -1;
    }

    public boolean b() {
        return this.a.size() + this.b.size() > this.e;
    }

    public Map<String, String> c(String str) {
        return (!this.c.containsKey(str) || this.c.get(str).b() <= 0) ? this.d.remove(str) : this.d.get(str);
    }
}
