package com.umeng.update;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.common.Log;
import com.umeng.common.b.g;
import com.umeng.common.c;
import com.umeng.common.net.k;
import com.umeng.common.net.r;
import com.umeng.common.net.s;
import org.json.JSONObject;

public class UmengUpdateAgent {
    public static final String a = "1.3.0.20120822";
    private static boolean b = true;
    /* access modifiers changed from: private */
    public static boolean c = true;
    /* access modifiers changed from: private */
    public static String d = e;
    private static final String e = "update";
    /* access modifiers changed from: private */
    public static UmengUpdateListener f = null;
    private static final String g = "umeng_last_update_time";
    private static final String h = "umeng_update_internal";
    /* access modifiers changed from: private */
    public static final String[] i = {"http://au.umeng.com/api/check_app_update", "http://au.umeng.co/api/check_app_update"};
    /* access modifiers changed from: private */
    public static UmengUpdateAgent j = null;
    /* access modifiers changed from: private */
    public static UmengDownloadListener k;
    private static k l = new a();
    private static Handler m = new b();

    public class a extends r implements Runnable {
        Context a;

        public a(Context context) {
            this.a = context;
        }

        private JSONObject a(Context context) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("type", UmengUpdateAgent.e);
                jSONObject.put("appkey", com.umeng.common.b.p(context));
                jSONObject.put(com.umeng.common.a.f, com.umeng.common.b.d(context));
                jSONObject.put(com.umeng.common.a.c, com.umeng.common.b.u(context));
                jSONObject.put("sdk_version", UmengUpdateAgent.a);
                jSONObject.put(com.umeng.common.a.e, g.b(com.umeng.common.b.f(context)));
                jSONObject.put(com.umeng.common.a.d, com.umeng.common.b.t(context));
                return jSONObject;
            } catch (Exception e) {
                Log.b(UmengUpdateAgent.d, "exception in updateInternal", e);
                return null;
            }
        }

        private void b() {
            JSONObject a2 = a(this.a);
            UmengUpdateAgent d = UmengUpdateAgent.h();
            d.getClass();
            b bVar = new b(a2);
            UpdateResponse updateResponse = null;
            for (String a3 : UmengUpdateAgent.i) {
                bVar.a(a3);
                updateResponse = (UpdateResponse) a(bVar, UpdateResponse.class);
                if (updateResponse != null) {
                    break;
                }
            }
            UpdateResponse updateResponse2 = updateResponse;
            if (updateResponse2 == null) {
                UmengUpdateAgent.b(3, (UpdateResponse) null);
                return;
            }
            Log.a(UmengUpdateAgent.d, "response : " + updateResponse2.hasUpdate);
            if (updateResponse2.hasUpdate) {
                UmengUpdateAgent.b(0, updateResponse2);
                if (UmengUpdateAgent.c) {
                    ((Activity) this.a).runOnUiThread(new e(this, updateResponse2));
                    return;
                }
                return;
            }
            UmengUpdateAgent.b(1, (UpdateResponse) null);
        }

        public boolean a() {
            return false;
        }

        public void run() {
            try {
                b();
            } catch (Exception e) {
                UmengUpdateAgent.b(1, (UpdateResponse) null);
                Log.a(UmengUpdateAgent.d, "reques update error", e);
            }
        }
    }

    public class b extends s {
        private JSONObject e;

        public b(JSONObject jSONObject) {
            super(null);
            this.e = jSONObject;
        }

        public JSONObject a() {
            return this.e;
        }

        public String b() {
            return this.c;
        }
    }

    private static SharedPreferences a(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("umeng_analytic_online_setting_");
        stringBuffer.append(com.umeng.common.b.u(context));
        return context.getSharedPreferences(stringBuffer.toString(), 0);
    }

    /* access modifiers changed from: private */
    public static void b(int i2, UpdateResponse updateResponse) {
        if (f != null) {
            Message message = new Message();
            message.what = i2;
            message.obj = updateResponse;
            m.sendMessage(message);
        }
    }

    public static void destroy() {
        j = null;
    }

    /* access modifiers changed from: private */
    public static UmengUpdateAgent h() {
        if (j == null) {
            j = new UmengUpdateAgent();
        }
        return j;
    }

    public static void setOnDownloadListener(UmengDownloadListener umengDownloadListener) {
        k = umengDownloadListener;
    }

    public static void setUpdateAutoPopup(boolean z) {
        c = z;
    }

    public static void setUpdateListener(UmengUpdateListener umengUpdateListener) {
        f = umengUpdateListener;
    }

    public static void setUpdateOnlyWifi(boolean z) {
        b = z;
    }

    public static void showUpdateDialog(Context context, UpdateResponse updateResponse) {
        String str = PoiTypeDef.All;
        try {
            if (!com.umeng.common.b.k(context)) {
                str = String.valueOf(context.getString(c.a(context).f("UMGprsCondition"))) + "\n";
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(str);
            stringBuffer.append(context.getString(c.a(context).f("UMNewVersion")));
            stringBuffer.append(updateResponse.version);
            stringBuffer.append("\n");
            stringBuffer.append(updateResponse.updateLog);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(c.a(context).f("UMUpdateTitle"))).setMessage(stringBuffer.toString()).setPositiveButton(context.getString(c.a(context).f("UMUpdateNow")), new c(context, updateResponse)).setNegativeButton(context.getString(c.a(context).f("UMNotNow")), new d());
            builder.create().show();
        } catch (Exception e2) {
            Log.b(d, "Fail to create update dialog box.", e2);
        }
    }

    public static void update(Context context) {
        try {
            if (b && !com.umeng.common.b.k(context)) {
                b(2, null);
            } else if (context == null) {
                b(1, null);
                Log.b(d, "unexpected null context in update");
            } else {
                UmengUpdateAgent h2 = h();
                h2.getClass();
                new Thread(new a(context)).start();
            }
        } catch (Exception e2) {
            Log.b(d, "Exception occurred in Mobclick.update(). ", e2);
        }
    }

    public static void update(Context context, long j2) {
        if (context == null) {
            Log.a(d, "unexpected null Context");
            return;
        }
        SharedPreferences a2 = a(context);
        long j3 = a2.getLong("umeng_last_update_time", 0);
        long j4 = a2.getLong("umeng_update_internal", j2);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - j3 > j4) {
            update(context);
            a2.edit().putLong("umeng_last_update_time", currentTimeMillis).commit();
        }
    }

    public static void update(Context context, String str) {
        com.umeng.common.a.m = str;
        update(context);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        Log.a(e, "url: " + str);
        new com.umeng.common.net.a(context, e, com.umeng.common.b.v(context), str, l).a();
    }
}
