package com.umeng.common.b;

import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class f {
    public static int a;

    public static String a(byte[] bArr, String str) {
        if (bArr == null || bArr.length == 0) {
            return null;
        }
        Inflater inflater = new Inflater();
        byte[] bArr2 = new byte[100];
        inflater.setInput(bArr, 0, bArr.length);
        StringBuilder sb = new StringBuilder();
        while (!inflater.needsInput()) {
            sb.append(new String(bArr2, 0, inflater.inflate(bArr2), str));
        }
        inflater.end();
        return sb.toString();
    }

    public static byte[] a(String str, String str2) {
        ByteArrayOutputStream byteArrayOutputStream;
        Throwable th;
        if (g.c(str)) {
            return null;
        }
        Deflater deflater = new Deflater();
        deflater.setInput(str.getBytes(str2));
        deflater.finish();
        byte[] bArr = new byte[8192];
        a = 0;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            while (!deflater.finished()) {
                try {
                    int deflate = deflater.deflate(bArr);
                    a += deflate;
                    byteArrayOutputStream.write(bArr, 0, deflate);
                } catch (Throwable th2) {
                    th = th2;
                }
            }
            deflater.end();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Throwable th3) {
            Throwable th4 = th3;
            byteArrayOutputStream = null;
            th = th4;
            if (byteArrayOutputStream != null) {
                byteArrayOutputStream.close();
            }
            throw th;
        }
    }
}
