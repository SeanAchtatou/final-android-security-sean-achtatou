package com.umeng.common.b;

import java.io.UnsupportedEncodingException;

public class a {
    private static IllegalStateException a(String str, UnsupportedEncodingException unsupportedEncodingException) {
        return new IllegalStateException(String.valueOf(str) + ": " + unsupportedEncodingException);
    }

    public static String a(byte[] bArr) {
        return a(bArr, "ISO-8859-1");
    }

    public static String a(byte[] bArr, String str) {
        if (bArr == null) {
            return null;
        }
        try {
            return new String(bArr, str);
        } catch (UnsupportedEncodingException e) {
            throw a(str, e);
        }
    }

    public static byte[] a(String str) {
        return a(str, "ISO-8859-1");
    }

    public static byte[] a(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            return str.getBytes(str2);
        } catch (UnsupportedEncodingException e) {
            throw a(str2, e);
        }
    }

    public static String b(byte[] bArr) {
        return a(bArr, "US-ASCII");
    }

    public static byte[] b(String str) {
        return a(str, "US-ASCII");
    }

    public static String c(byte[] bArr) {
        return a(bArr, e.c);
    }

    public static byte[] c(String str) {
        return a(str, e.c);
    }

    public static String d(byte[] bArr) {
        return a(bArr, e.d);
    }

    public static byte[] d(String str) {
        return a(str, e.d);
    }

    public static String e(byte[] bArr) {
        return a(bArr, e.e);
    }

    public static byte[] e(String str) {
        return a(str, e.e);
    }

    public static String f(byte[] bArr) {
        return a(bArr, e.f);
    }

    public static byte[] f(String str) {
        return a(str, e.f);
    }
}
