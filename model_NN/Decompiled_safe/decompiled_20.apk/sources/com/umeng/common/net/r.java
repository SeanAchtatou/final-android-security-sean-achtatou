package com.umeng.common.net;

import com.umeng.common.Log;
import com.umeng.common.b.e;
import com.umeng.common.b.f;
import com.umeng.common.b.g;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;

public class r {
    private static final String a = r.class.getName();
    private Map<String, String> b;

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e) {
                        Log.b(a, "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(readLine) + "\n");
                }
            } catch (IOException e2) {
                Log.b(a, "Caught IOException in convertStreamToString()", e2);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e3) {
                    Log.b(a, "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e4) {
                    Log.b(a, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private JSONObject a(String str) {
        InputStream inputStream;
        int nextInt = new Random().nextInt(1000);
        try {
            String property = System.getProperty("line.separator");
            if (str.length() <= 1) {
                Log.b(a, String.valueOf(nextInt) + ":\tInvalid baseUrl.");
                return null;
            }
            Log.a(a, String.valueOf(nextInt) + ":\tget: " + str);
            HttpGet httpGet = new HttpGet(str);
            if (this.b != null && this.b.size() > 0) {
                for (String next : this.b.keySet()) {
                    httpGet.addHeader(next, this.b.get(next));
                }
            }
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
            HttpResponse execute = new DefaultHttpClient(basicHttpParams).execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    InputStream content = entity.getContent();
                    Header firstHeader = execute.getFirstHeader("Content-Encoding");
                    if (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("gzip")) {
                        if (firstHeader != null) {
                            if (firstHeader.getValue().equalsIgnoreCase("deflate")) {
                                Log.a(a, String.valueOf(nextInt) + "  Use InflaterInputStream get data....");
                                inputStream = new InflaterInputStream(content);
                            }
                        }
                        inputStream = content;
                    } else {
                        Log.a(a, String.valueOf(nextInt) + "  Use GZIPInputStream get data....");
                        inputStream = new GZIPInputStream(content);
                    }
                    String a2 = a(inputStream);
                    Log.a(a, String.valueOf(nextInt) + ":\tresponse: " + property + a2);
                    if (a2 == null) {
                        return null;
                    }
                    return new JSONObject(a2);
                }
            } else {
                Log.c(a, String.valueOf(nextInt) + ":\tFailed to send message. StatusCode = " + execute.getStatusLine().getStatusCode() + g.a + str);
            }
            return null;
        } catch (ClientProtocolException e) {
            Log.c(a, String.valueOf(nextInt) + ":\tClientProtocolException,Failed to send message." + str, e);
            return null;
        } catch (Exception e2) {
            Log.c(a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e2);
            return null;
        }
    }

    private JSONObject a(String str, JSONObject jSONObject) {
        String jSONObject2 = jSONObject.toString();
        int nextInt = new Random().nextInt(1000);
        Log.c(a, String.valueOf(nextInt) + ":\trequest: " + str + g.a + jSONObject2);
        HttpPost httpPost = new HttpPost(str);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        try {
            if (a()) {
                byte[] a2 = f.a("content=" + jSONObject2, Charset.defaultCharset().toString());
                httpPost.addHeader("Content-Encoding", "deflate");
                httpPost.setEntity(new InputStreamEntity(new ByteArrayInputStream(a2), (long) a2.length));
            } else {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair(com.umeng.fb.f.S, jSONObject2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, e.f));
            }
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity == null) {
                    return null;
                }
                InputStream content = entity.getContent();
                Header firstHeader = execute.getFirstHeader("Content-Encoding");
                String a3 = a((firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("deflate")) ? content : new InflaterInputStream(content));
                Log.a(a, String.valueOf(nextInt) + ":\tresponse: " + g.a + a3);
                if (a3 == null) {
                    return null;
                }
                return new JSONObject(a3);
            }
            Log.c(a, String.valueOf(nextInt) + ":\tFailed to send message. StatusCode = " + execute.getStatusLine().getStatusCode() + g.a + str);
            return null;
        } catch (ClientProtocolException e) {
            Log.c(a, String.valueOf(nextInt) + ":\tClientProtocolException,Failed to send message." + str, e);
            return null;
        } catch (IOException e2) {
            Log.c(a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e2);
            return null;
        } catch (JSONException e3) {
            Log.c(a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e3);
            return null;
        }
    }

    private void b(String str) {
        if (g.c(str) || !(s.b.equals(str.trim()) ^ s.a.equals(str.trim()))) {
            throw new RuntimeException("验证请求方式失败[" + str + "]");
        }
    }

    public r a(Map<String, String> map) {
        this.b = map;
        return this;
    }

    public <T extends t> T a(s sVar, Class cls) {
        String trim = sVar.c().trim();
        b(trim);
        JSONObject a2 = s.b.equals(trim) ? a(sVar.b()) : s.a.equals(trim) ? a(sVar.c, sVar.a()) : null;
        if (a2 == null) {
            return null;
        }
        try {
            return (t) cls.getConstructor(JSONObject.class).newInstance(a2);
        } catch (SecurityException e) {
            Log.b(a, "SecurityException", e);
        } catch (NoSuchMethodException e2) {
            Log.b(a, "NoSuchMethodException", e2);
        } catch (IllegalArgumentException e3) {
            Log.b(a, "IllegalArgumentException", e3);
        } catch (InstantiationException e4) {
            Log.b(a, "InstantiationException", e4);
        } catch (IllegalAccessException e5) {
            Log.b(a, "IllegalAccessException", e5);
        } catch (InvocationTargetException e6) {
            Log.b(a, "InvocationTargetException", e6);
        }
        return null;
    }

    public boolean a() {
        return false;
    }
}
