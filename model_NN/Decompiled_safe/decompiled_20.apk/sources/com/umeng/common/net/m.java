package com.umeng.common.net;

import android.os.AsyncTask;
import com.umeng.common.net.o;

public class m extends r {

    public interface a {
        void a();

        void a(o.a aVar);
    }

    class b extends AsyncTask<Integer, Integer, o.a> {
        private n b;
        private a c;

        public b(n nVar, a aVar) {
            this.b = nVar;
            this.c = aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public o.a doInBackground(Integer... numArr) {
            return m.this.a(this.b);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(o.a aVar) {
            if (this.c != null) {
                this.c.a(aVar);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (this.c != null) {
                this.c.a();
            }
        }
    }

    public o.a a(n nVar) {
        o oVar = (o) a(nVar, o.class);
        return oVar == null ? o.a.FAIL : oVar.a;
    }

    public void a(n nVar, a aVar) {
        try {
            new b(nVar, aVar).execute(new Integer[0]);
        } catch (Exception e) {
            if (aVar != null) {
                aVar.a(o.a.FAIL);
            }
        }
    }
}
