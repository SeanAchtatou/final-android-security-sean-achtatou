package com.umeng.common.net;

import com.amap.api.location.LocationManagerProxy;
import org.json.JSONObject;

public class o extends t {
    public a a;

    public enum a {
        SUCCESS,
        FAIL
    }

    public o(JSONObject jSONObject) {
        super(jSONObject);
        if ("ok".equalsIgnoreCase(jSONObject.optString(LocationManagerProxy.KEY_STATUS_CHANGED)) || "ok".equalsIgnoreCase(jSONObject.optString("success"))) {
            this.a = a.SUCCESS;
        } else {
            this.a = a.FAIL;
        }
    }
}
