package com.lthj.unipay.plugin;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.unionpay.upomp.lthj.link.PluginLink;

public class l {
    private static l c;
    private Dialog a;
    private View b;

    private l() {
    }

    public static l a() {
        if (c == null) {
            c = new l();
        }
        return c;
    }

    public void a(Context context, String str, View.OnClickListener onClickListener) {
        a(context, str, null, null, onClickListener, null);
    }

    public void a(Context context, String str, String str2) {
        a(context, str, str2, null);
    }

    public void a(Context context, String str, String str2, View.OnClickListener onClickListener) {
        if (context != null) {
            this.b = LayoutInflater.from(context).inflate(PluginLink.getLayoutupomp_lthj_onebtn_progress(), (ViewGroup) null);
            ((ProgressBar) this.b.findViewById(PluginLink.getIdupomp_lthj_dialog_progress())).setVisibility(8);
            Button button = (Button) this.b.findViewById(PluginLink.getIdupomp_lthj_button_cancel());
            if (!button.isShown()) {
                button.setVisibility(0);
            }
            if (!button.isEnabled()) {
                button.setEnabled(true);
            }
            button.setText(str);
            if (onClickListener == null) {
                button.setOnClickListener(new aj(this));
            } else {
                button.setOnClickListener(onClickListener);
            }
            ((TextView) this.b.findViewById(PluginLink.getIdupomp_lthj_dialog_message())).setText(str2);
            if (this.a == null) {
                this.a = new Dialog(context, PluginLink.getStyleupomp_lthj_common_dialog());
            }
            this.a.setContentView(this.b);
            this.a.setCancelable(false);
            this.a.show();
        }
    }

    public void a(Context context, String str, String str2, String str3, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        if (context != null) {
            this.b = LayoutInflater.from(context).inflate(PluginLink.getLayoutupomp_lthj_twobtn_progress(), (ViewGroup) null);
            Button button = (Button) this.b.findViewById(PluginLink.getIdupomp_lthj_button_ok());
            if (str2 != null) {
                button.setText(str2);
            }
            button.setOnClickListener(onClickListener);
            Button button2 = (Button) this.b.findViewById(PluginLink.getIdupomp_lthj_button_cancel());
            if (str3 != null) {
                button2.setText(str3);
            }
            if (onClickListener2 != null) {
                button2.setOnClickListener(onClickListener2);
            } else {
                button2.setOnClickListener(new am(this));
            }
            ((TextView) this.b.findViewById(PluginLink.getIdupomp_lthj_dialog_message())).setText(str);
            if (this.a == null) {
                this.a = new Dialog(context, PluginLink.getStyleupomp_lthj_common_dialog());
            }
            this.a.setContentView(this.b);
            this.a.setCancelable(false);
            this.a.show();
        }
    }

    public void a(Context context, boolean z, View.OnClickListener onClickListener) {
        if (context != null) {
            if (this.a == null) {
                this.a = new Dialog(context, PluginLink.getStyleupomp_lthj_common_dialog());
            }
            this.b = LayoutInflater.from(context).inflate(PluginLink.getLayoutupomp_lthj_onebtn_progress(), (ViewGroup) null);
            this.a.setContentView(this.b);
            Button button = (Button) this.b.findViewById(PluginLink.getIdupomp_lthj_button_cancel());
            if (!z) {
                button.setEnabled(false);
                button.setVisibility(4);
            }
            button.setTextColor(PluginLink.getDrawableupomp_lthj_dialog_ok_color());
            button.setOnClickListener(onClickListener);
            ((ProgressBar) this.b.findViewById(PluginLink.getIdupomp_lthj_dialog_progress())).setVisibility(0);
            this.a.setCancelable(false);
            this.a.show();
        }
    }

    public void b() {
        if (this.a != null && this.a.isShowing()) {
            this.a.dismiss();
            this.a = null;
        }
    }
}
