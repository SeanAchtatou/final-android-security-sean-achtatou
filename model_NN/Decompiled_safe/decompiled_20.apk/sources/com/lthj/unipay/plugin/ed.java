package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;

public class ed implements View.OnClickListener {
    final /* synthetic */ BankCardInfoActivity a;

    public ed(BankCardInfoActivity bankCardInfoActivity) {
        this.a = bankCardInfoActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, HomeActivity.class);
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
        l.a().b();
    }
}
