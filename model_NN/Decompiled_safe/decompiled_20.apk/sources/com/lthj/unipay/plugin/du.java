package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.Pay;

public class du extends w {
    private String A;
    private String B;
    private String C;
    private String a;
    private String b;
    private StringBuffer c = new StringBuffer();
    private StringBuffer d = new StringBuffer();
    private String e;
    private String f;
    private String g;
    private StringBuffer h = new StringBuffer();
    private StringBuffer i = new StringBuffer();
    private StringBuffer j = new StringBuffer();
    private StringBuffer k = new StringBuffer();
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y;
    private String z;

    public du(int i2) {
        super(i2);
    }

    public String a() {
        return this.A;
    }

    public void a(Data data) {
        Pay pay = (Pay) data;
        c(pay);
        this.a = pay.payType;
        this.b = pay.loginName;
        this.d.delete(0, this.d.length());
        this.d.append(pay.mobileNumber);
        this.m = pay.merchantId;
        this.n = pay.merchantOrderId;
        this.o = pay.merchantOrderTime;
        this.p = pay.merchantOrderAmt;
        this.v = pay.settleDate;
        this.w = pay.setlAmt;
        this.x = pay.setlCurrency;
        this.y = pay.converRate;
        this.z = pay.cupsQid;
        this.A = pay.cupsTraceNum;
        this.B = pay.cupsTraceTime;
        this.C = pay.cupsRespCode;
    }

    public void a(String str) {
        this.g = str;
    }

    public Data b() {
        Pay pay = new Pay();
        b(pay);
        pay.payType = this.a;
        pay.loginName = this.b;
        pay.password = this.c.toString();
        this.c.delete(0, this.c.length());
        pay.mobileNumber = this.d.toString();
        this.d.delete(0, this.d.length());
        pay.mobileMac = this.e;
        pay.validateCode = this.f;
        pay.activityId = this.g;
        pay.pan = this.h.toString();
        this.h.delete(0, this.h.length());
        pay.pin = this.i.toString();
        this.i.delete(0, this.i.length());
        pay.panDate = this.j.toString();
        this.j.delete(0, this.j.length());
        pay.cvn2 = this.k.toString();
        this.k.delete(0, this.k.length());
        pay.merchantName = this.l;
        pay.merchantId = this.m;
        pay.merchantOrderId = this.n;
        pay.merchantOrderTime = this.o;
        pay.merchantOrderAmt = this.p;
        pay.merchantOrderDesc = this.q;
        pay.transTimeout = this.r;
        pay.backEndUrl = this.s;
        pay.sign = this.t;
        pay.merchantPublicCert = this.u;
        return pay;
    }

    public void b(String str) {
        this.a = str;
    }

    public String c() {
        return this.z;
    }

    public void c(String str) {
        this.b = str;
    }

    public String d() {
        return this.B;
    }

    public void d(String str) {
        this.c.delete(0, this.c.length());
        this.c.append(str);
    }

    public void e(String str) {
        this.d.delete(0, this.d.length());
        this.d.append(str);
    }

    public void f(String str) {
        this.e = str;
    }

    public void g(String str) {
        this.f = str;
    }

    public void h(String str) {
        this.h.delete(0, this.h.length());
        this.h.append(str);
    }

    public void k(String str) {
        this.i.delete(0, this.i.length());
        this.i.append(str);
    }

    public void l(String str) {
        this.j.delete(0, this.j.length());
        this.j.append(str);
    }

    public void m(String str) {
        this.k.delete(0, this.k.length());
        this.k.append(str);
    }

    public void n(String str) {
        this.l = str;
    }

    public String o() {
        return this.C;
    }

    public void o(String str) {
        this.m = str;
    }

    public void p(String str) {
        this.n = str;
    }

    public void q(String str) {
        this.o = str;
    }

    public void r(String str) {
        this.p = str;
    }

    public void s(String str) {
        this.q = str;
    }

    public void t(String str) {
        this.t = str;
    }

    public void u(String str) {
        this.r = str;
    }

    public void v(String str) {
        this.u = str;
    }

    public void w(String str) {
        this.s = str;
    }
}
