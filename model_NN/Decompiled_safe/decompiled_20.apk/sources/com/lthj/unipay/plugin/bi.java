package com.lthj.unipay.plugin;

import android.os.Message;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;
import java.util.TimerTask;

public class bi extends TimerTask {
    final /* synthetic */ HomeActivity a;

    public bi(HomeActivity homeActivity) {
        this.a = homeActivity;
    }

    public void run() {
        Message obtain = Message.obtain();
        obtain.obj = new StringBuilder().append(this.a.a).toString();
        this.a.s.sendMessage(obtain);
        if (this.a.a < 0) {
            this.a.aaTimerTask.cancel();
            this.a.aaTimerTask = null;
            this.a.a = 60;
            return;
        }
        HomeActivity homeActivity = this.a;
        homeActivity.a--;
    }
}
