package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.ImageCheckCode;

public class q extends w {
    public String a;

    public q(int i) {
        super(i);
    }

    public String a() {
        return this.a;
    }

    public void a(Data data) {
        ImageCheckCode imageCheckCode = (ImageCheckCode) data;
        c(imageCheckCode);
        this.a = imageCheckCode.validateCodeUrl;
    }

    public Data b() {
        ImageCheckCode imageCheckCode = new ImageCheckCode();
        b(imageCheckCode);
        imageCheckCode.validateCodeUrl = this.a;
        return imageCheckCode;
    }
}
