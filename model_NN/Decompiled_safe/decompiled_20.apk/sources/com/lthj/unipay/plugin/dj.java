package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;

public class dj implements View.OnClickListener {
    final /* synthetic */ PayActivity a;

    public dj(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, HomeActivity.class);
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
        l.a().b();
    }
}
