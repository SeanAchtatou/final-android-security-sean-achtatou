package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.HeadData;

public abstract class w {
    private int a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private int m;

    public w() {
    }

    public w(int i2) {
        this.a = i2;
    }

    public abstract void a(Data data);

    public void a(HeadData headData) {
        if (headData != null) {
            this.b = headData.getApplication();
            this.c = headData.getVersion();
            this.d = headData.getPluginVersion();
            this.e = headData.getTerminalModel();
            this.f = headData.getTerminalOs();
            this.g = headData.getPluginSerialNo();
            this.h = headData.getTerminalPhysicalNo();
        }
    }

    public abstract Data b();

    public void b(Data data) {
        if (data != null) {
            data.type = e();
            data.application = f();
            data.version = g();
            data.pluginVersion = h();
            data.terminalModel = i();
            data.terminalOs = j();
            data.pluginSerialNo = k();
            data.terminalPhysicalNo = l();
            data.misc = this.j;
            data.msgExt = this.i;
        }
    }

    public void c(Data data) {
        if (data != null) {
            this.b = data.application;
            this.c = data.version;
            this.d = data.pluginVersion;
            this.e = data.terminalModel;
            this.f = data.terminalOs;
            this.g = data.pluginSerialNo;
            this.h = data.terminalPhysicalNo;
            this.i = data.msgExt;
            this.j = data.misc;
            this.k = data.respCode;
            this.l = data.respDesc;
            this.m = data.stateCode;
        }
    }

    public int e() {
        return this.a;
    }

    public String f() {
        return this.b;
    }

    public String g() {
        return this.c;
    }

    public String h() {
        return this.d;
    }

    public String i() {
        return this.e;
    }

    public void i(String str) {
        this.g = str;
    }

    public String j() {
        return this.f;
    }

    public void j(String str) {
        this.j = str;
    }

    public String k() {
        return this.g;
    }

    public String l() {
        return this.h;
    }

    public String m() {
        return this.k;
    }

    public String n() {
        return this.l;
    }
}
