package com.lthj.unipay.plugin;

import java.util.Vector;

public class dc {
    private String a;
    private String b;
    private byte[] c;
    private byte[] d;
    private String e;
    private String f;
    private byte[] g;
    private byte[] h;
    private String i;
    private byte j;
    private long k;
    private String l;
    private Vector m;
    private Vector n;

    public String a() {
        return this.a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:118:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003c A[SYNTHETIC, Splitter:B:16:0x003c] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0082 A[SYNTHETIC, Splitter:B:33:0x0082] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(byte[] r7) {
        /*
            r6 = this;
            r2 = 0
            java.lang.String r0 = ""
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x01a5, all -> 0x01a1 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x01a5, all -> 0x01a1 }
            org.xmlpull.v1.XmlPullParser r3 = android.util.Xml.newPullParser()     // Catch:{ Exception -> 0x0036 }
            java.lang.String r2 = "UTF-8"
            r3.setInput(r1, r2)     // Catch:{ Exception -> 0x0036 }
            int r2 = r3.getEventType()     // Catch:{ Exception -> 0x0036 }
        L_0x0015:
            r4 = 1
            if (r2 == r4) goto L_0x0193
            switch(r2) {
                case 0: goto L_0x001b;
                case 1: goto L_0x001b;
                case 2: goto L_0x0020;
                default: goto L_0x001b;
            }     // Catch:{ Exception -> 0x0036 }
        L_0x001b:
            int r2 = r3.next()     // Catch:{ Exception -> 0x0036 }
            goto L_0x0015
        L_0x0020:
            java.lang.String r2 = r3.getName()     // Catch:{ Exception -> 0x0036 }
            java.lang.String r4 = "config"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x0040
            r2 = 0
            java.lang.String r4 = "version"
            java.lang.String r2 = r3.getAttributeValue(r2, r4)     // Catch:{ Exception -> 0x0036 }
            r6.a = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            r0.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r1 == 0) goto L_0x003f
            r1.close()     // Catch:{ Exception -> 0x019b }
        L_0x003f:
            return
        L_0x0040:
            java.lang.String r4 = "frontPubKey"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x0054
            r0 = 0
            java.lang.String r2 = "version"
            java.lang.String r0 = r3.getAttributeValue(r0, r2)     // Catch:{ Exception -> 0x0036 }
            r6.e = r0     // Catch:{ Exception -> 0x0036 }
            java.lang.String r0 = "frontPubKey"
            goto L_0x001b
        L_0x0054:
            java.lang.String r4 = "upopPubkey"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x0068
            r0 = 0
            java.lang.String r2 = "version"
            java.lang.String r0 = r3.getAttributeValue(r0, r2)     // Catch:{ Exception -> 0x0036 }
            r6.i = r0     // Catch:{ Exception -> 0x0036 }
            java.lang.String r0 = "upopPubkey"
            goto L_0x001b
        L_0x0068:
            java.lang.String r4 = "oneline"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x0095
            java.lang.String r2 = "frontPubKey"
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x0086
            java.lang.String r2 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            r6.b = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x007f:
            r0 = move-exception
        L_0x0080:
            if (r1 == 0) goto L_0x0085
            r1.close()     // Catch:{ Exception -> 0x019e }
        L_0x0085:
            throw r0
        L_0x0086:
            java.lang.String r2 = "upopPubkey"
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x001b
            java.lang.String r2 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            r6.f = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x0095:
            java.lang.String r4 = "n"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x00c5
            java.lang.String r2 = "frontPubKey"
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x00b1
            java.lang.String r2 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            byte[] r2 = com.lthj.unipay.plugin.da.a(r2)     // Catch:{ Exception -> 0x0036 }
            r6.c = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x00b1:
            java.lang.String r2 = "upopPubkey"
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x001b
            java.lang.String r2 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            byte[] r2 = com.lthj.unipay.plugin.da.a(r2)     // Catch:{ Exception -> 0x0036 }
            r6.g = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x00c5:
            java.lang.String r4 = "e"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x00f5
            java.lang.String r2 = "frontPubKey"
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x00e1
            java.lang.String r2 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            byte[] r2 = com.lthj.unipay.plugin.da.a(r2)     // Catch:{ Exception -> 0x0036 }
            r6.d = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x00e1:
            java.lang.String r2 = "upopPubkey"
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x001b
            java.lang.String r2 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            byte[] r2 = com.lthj.unipay.plugin.da.a(r2)     // Catch:{ Exception -> 0x0036 }
            r6.h = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x00f5:
            java.lang.String r4 = "bindCardCount"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x010e
            java.lang.Byte r2 = new java.lang.Byte     // Catch:{ Exception -> 0x0036 }
            java.lang.String r4 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0036 }
            byte r2 = r2.byteValue()     // Catch:{ Exception -> 0x0036 }
            r6.j = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x010e:
            java.lang.String r4 = "allowBalance"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x011e
            java.lang.String r2 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            r6.l = r2     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x011e:
            java.lang.String r4 = "paymentMethod"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x0131
            java.util.Vector r0 = new java.util.Vector     // Catch:{ Exception -> 0x0036 }
            r0.<init>()     // Catch:{ Exception -> 0x0036 }
            r6.m = r0     // Catch:{ Exception -> 0x0036 }
            java.lang.String r0 = "paymentMethod"
            goto L_0x001b
        L_0x0131:
            java.lang.String r4 = "secureQuestion"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x0144
            java.util.Vector r0 = new java.util.Vector     // Catch:{ Exception -> 0x0036 }
            r0.<init>()     // Catch:{ Exception -> 0x0036 }
            r6.n = r0     // Catch:{ Exception -> 0x0036 }
            java.lang.String r0 = "secureQuestion"
            goto L_0x001b
        L_0x0144:
            java.lang.String r4 = "item"
            boolean r4 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r4 == 0) goto L_0x017a
            java.lang.String r2 = "paymentMethod"
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x0163
            java.util.Vector r2 = r6.m     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x001b
            java.util.Vector r2 = r6.m     // Catch:{ Exception -> 0x0036 }
            java.lang.String r4 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            r2.add(r4)     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x0163:
            java.lang.String r2 = "secureQuestion"
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x001b
            java.util.Vector r2 = r6.n     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x001b
            java.util.Vector r2 = r6.n     // Catch:{ Exception -> 0x0036 }
            java.lang.String r4 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            r2.add(r4)     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x017a:
            java.lang.String r4 = "serverTimeout"
            boolean r2 = r2.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0036 }
            if (r2 == 0) goto L_0x001b
            java.lang.Long r2 = new java.lang.Long     // Catch:{ Exception -> 0x0036 }
            java.lang.String r4 = r3.nextText()     // Catch:{ Exception -> 0x0036 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0036 }
            long r4 = r2.longValue()     // Catch:{ Exception -> 0x0036 }
            r6.k = r4     // Catch:{ Exception -> 0x0036 }
            goto L_0x001b
        L_0x0193:
            r1.close()     // Catch:{ Exception -> 0x0198 }
            goto L_0x003f
        L_0x0198:
            r0 = move-exception
            goto L_0x003f
        L_0x019b:
            r0 = move-exception
            goto L_0x003f
        L_0x019e:
            r1 = move-exception
            goto L_0x0085
        L_0x01a1:
            r0 = move-exception
            r1 = r2
            goto L_0x0080
        L_0x01a5:
            r0 = move-exception
            r1 = r2
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lthj.unipay.plugin.dc.a(byte[]):void");
    }

    public String b() {
        return this.e;
    }

    public byte c() {
        return this.j;
    }

    public byte[] d() {
        return this.c;
    }

    public byte[] e() {
        return this.d;
    }

    public byte[] f() {
        return this.g;
    }

    public byte[] g() {
        return this.h;
    }

    public Vector h() {
        return this.n;
    }
}
