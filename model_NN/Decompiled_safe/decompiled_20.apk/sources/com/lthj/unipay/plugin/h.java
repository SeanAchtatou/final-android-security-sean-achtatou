package com.lthj.unipay.plugin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class h {
    public String[] a = {"pluginSerialNoTest", "pluginSerialNoProduct", "commonPayPhone"};
    public String[] b = {"text", "text", "text"};
    private y c;
    private String d = "pluginfo";

    public h(Context context) {
        this.c = new y(context, this.d, this.a, this.b);
    }

    public cu a(int i) {
        cu cuVar = null;
        Cursor a2 = this.c.a(i);
        if (a2 != null) {
            if (a2.moveToNext()) {
                cu cuVar2 = new cu();
                cuVar2.a(i);
                String string = ap.a().d ? a2.getString(a2.getColumnIndex(this.a[0])) : a2.getString(a2.getColumnIndex(this.a[1]));
                String string2 = a2.getString(a2.getColumnIndex(this.a[2]));
                cuVar2.a(string);
                cuVar2.b(string2);
                cuVar = cuVar2;
            }
            if (a2 != null) {
                a2.close();
                this.c.a();
            }
        }
        return cuVar;
    }

    public boolean a(cu cuVar) {
        if (cuVar == null) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        if (ap.a().d) {
            contentValues.put(this.a[0], cuVar.b());
        } else {
            contentValues.put(this.a[1], cuVar.b());
        }
        contentValues.put(this.a[2], cuVar.c());
        return this.c.a(contentValues);
    }

    public boolean b(cu cuVar) {
        boolean z;
        boolean z2 = true;
        ContentValues contentValues = new ContentValues();
        if (cuVar.b() != null) {
            if (ap.a().d) {
                contentValues.put(this.a[0], cuVar.b());
            } else {
                contentValues.put(this.a[1], cuVar.b());
            }
            z = true;
        } else {
            z = false;
        }
        if (cuVar.c() != null) {
            contentValues.put(this.a[2], cuVar.c());
        } else {
            z2 = z;
        }
        if (z2) {
            return this.c.a(cuVar.a(), contentValues);
        }
        return false;
    }
}
