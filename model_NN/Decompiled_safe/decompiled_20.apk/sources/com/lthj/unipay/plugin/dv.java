package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity;
import com.unionpay.upomp.lthj.plugin.ui.SupportCardActivity;

public class dv implements View.OnClickListener {
    final /* synthetic */ BankCardInfoActivity a;

    public dv(BankCardInfoActivity bankCardInfoActivity) {
        this.a = bankCardInfoActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, SupportCardActivity.class);
        intent.putExtra("tranType", "2");
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
        l.a().b();
    }
}
