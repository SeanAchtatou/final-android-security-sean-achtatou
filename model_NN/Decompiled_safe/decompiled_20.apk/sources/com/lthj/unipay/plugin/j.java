package com.lthj.unipay.plugin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import com.unionpay.upomp.lthj.plugin.ui.KeyboardDialog;
import com.unionpay.upomp.lthj.plugin.ui.YearAndMonthDialog;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Vector;

public class j {
    public static GetBundleBankCardList a(Vector vector) {
        Iterator it = vector.iterator();
        while (it.hasNext()) {
            GetBundleBankCardList getBundleBankCardList = (GetBundleBankCardList) it.next();
            if ("1".equals(getBundleBankCardList.isDefault)) {
                return getBundleBankCardList;
            }
        }
        return null;
    }

    public static String a() {
        return "android" + Build.VERSION.RELEASE;
    }

    public static String a(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        return TextUtils.isEmpty(deviceId) ? "000000" : deviceId;
    }

    public static String a(String str) {
        if ("00".equals(str)) {
            return "信用卡";
        }
        if ("01".equals(str)) {
            return "借记卡";
        }
        if ("02".equals(str)) {
            return "储值卡";
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[]
     arg types: [java.lang.String, int]
     candidates:
      com.lthj.unipay.plugin.j.a(android.content.Context, java.lang.String):void
      com.lthj.unipay.plugin.j.a(android.content.Context, boolean):boolean
      com.lthj.unipay.plugin.j.a(java.lang.String, char):java.lang.String[] */
    public static Vector a(bw bwVar) {
        Vector vector = new Vector();
        String[] a = a(bwVar.a(), '|');
        String[] a2 = a(bwVar.c(), '|');
        String[] a3 = a(bwVar.o(), '|');
        String[] a4 = a(bwVar.d(), '|');
        String[] a5 = a(bwVar.p(), '|');
        String[] a6 = a(bwVar.q(), '|');
        String[] a7 = a(bwVar.r(), '|');
        if (a == null || a2 == null || a3 == null || a4 == null || a5 == null || a6 == null || a7 == null) {
            return null;
        }
        for (int i = 0; i < a.length; i++) {
            GetBundleBankCardList getBundleBankCardList = new GetBundleBankCardList();
            getBundleBankCardList.bindId = a[i];
            getBundleBankCardList.panType = a2[i];
            getBundleBankCardList.panBank = a3[i];
            getBundleBankCardList.panBankId = a4[i];
            getBundleBankCardList.pan = a5[i];
            getBundleBankCardList.mobileNumber = a6[i];
            getBundleBankCardList.isDefault = a7[i];
            vector.add(getBundleBankCardList);
        }
        return vector;
    }

    public static void a(Context context, EditText editText, int i) {
        KeyboardDialog keyboardDialog = new KeyboardDialog(context, PluginLink.getStyleupomp_lthj_keyboard_dialog());
        keyboardDialog.setInputText(editText);
        keyboardDialog.setType(i);
        keyboardDialog.show();
    }

    public static void a(Context context, EditText editText, int i, bt btVar) {
        YearAndMonthDialog yearAndMonthDialog = new YearAndMonthDialog(context, editText, i, btVar);
        yearAndMonthDialog.setType(i);
        yearAndMonthDialog.show();
    }

    public static void a(Context context, String str) {
        l.a().a(context, context.getString(PluginLink.getStringupomp_lthj_ok()), str);
    }

    public static void a(Context context, String str, int i) {
        l.a().a(context, context.getString(PluginLink.getStringupomp_lthj_ok()), b(context, str, i));
    }

    public static void a(ValidateCodeView validateCodeView) {
        if (validateCodeView.getVisibility() != 8) {
            if (validateCodeView != null) {
                validateCodeView.a().setVisibility(8);
            }
            validateCodeView.c().setText(PoiTypeDef.All);
            cl.a(validateCodeView, validateCodeView.b());
        }
    }

    public static boolean a(Context context, boolean z) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("shared_pre", 0);
        if (!z) {
            return sharedPreferences.getBoolean("isquickpay", false);
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("isquickpay", true);
        edit.commit();
        return z;
    }

    public static byte[] a(byte[] bArr) {
        return ec.a(bArr, 0);
    }

    public static String[] a(String str, char c) {
        if (str == null || PoiTypeDef.All.equals(str)) {
            return null;
        }
        Vector b = b(str, c);
        String[] strArr = new String[b.size()];
        b.copyInto(strArr);
        return strArr;
    }

    public static String b() {
        return Build.MODEL;
    }

    public static String b(Context context) {
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        return TextUtils.isEmpty(subscriberId) ? "000000" : subscriberId;
    }

    public static String b(Context context, String str, int i) {
        return str + "(" + i + ")";
    }

    public static String b(String str) {
        if ("01".equals(str)) {
            return "信用卡";
        }
        if ("00".equals(str)) {
            return "借记卡";
        }
        if ("02".equals(str)) {
            return "储值卡";
        }
        return null;
    }

    public static Vector b(String str, char c) {
        int i = 0;
        int length = str.length();
        if (str.charAt(length - 1) != c) {
            str = str + c;
            length++;
        }
        Vector vector = new Vector();
        for (int i2 = 0; i2 < length; i2++) {
            if (str.charAt(i2) == c) {
                if (i2 == 0) {
                    i = i2 + 1;
                } else {
                    vector.addElement(str.substring(i, i2));
                    i = i2 + 1;
                }
            }
        }
        return vector;
    }

    public static void b(Context context, String str) {
        new AlertDialog.Builder(context).setTitle("提示").setMessage(str).setPositiveButton("确定", new u()).show();
    }

    public static void b(ValidateCodeView validateCodeView) {
        if (ap.a().b.f()) {
            ap.a().b.f(false);
            if (validateCodeView != null) {
                validateCodeView.setVisibility(0);
                a(validateCodeView);
            }
        } else if (validateCodeView != null) {
            validateCodeView.setVisibility(8);
        }
    }

    public static byte[] b(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String c(String str) {
        if (str != null) {
            try {
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(str));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void c() {
        ap.a().b();
        JniMethod.getJniMethod().releaseResource();
        aq.a = PoiTypeDef.All;
        System.gc();
    }

    public static void c(Context context) {
        Dialog dialog = new Dialog(context, PluginLink.getStyleupomp_lthj_keyboard_dialog());
        View inflate = LayoutInflater.from(context).inflate(PluginLink.getLayoutupomp_lthj_cardinfo_tip(), (ViewGroup) null);
        dialog.setContentView(inflate);
        inflate.setOnClickListener(new s(dialog));
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void c(Context context, String str) {
        if (str != null && str.length() != 0) {
            SharedPreferences.Editor edit = context.getSharedPreferences("shared_pre", 0).edit();
            edit.putString("username", new String(JniMethod.getJniMethod().encryptConfig(str.getBytes(), str.length())));
            edit.commit();
        }
    }

    public static String d(Context context) {
        String string = context.getSharedPreferences("shared_pre", 0).getString("username", PoiTypeDef.All);
        if (PoiTypeDef.All.equals(string)) {
            return string;
        }
        try {
            return new String(JniMethod.getJniMethod().decryptConfig(string.getBytes(), string.length()));
        } catch (Exception e) {
            return PoiTypeDef.All;
        }
    }

    public static String d(String str) {
        if (TextUtils.isEmpty(str) || !TextUtils.isDigitsOnly(str)) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(str);
        while (stringBuffer.length() < 3) {
            stringBuffer.insert(0, "0");
        }
        stringBuffer.insert(stringBuffer.length() - 2, ".");
        return stringBuffer.append("  元").toString();
    }

    public static Bitmap e(String str) {
        try {
            return BitmapFactory.decodeStream(new URL(str).openConnection().getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String f(String str) {
        return (str == null || str.length() < 3) ? str : str.substring(0, 3) + "*****" + str.substring(str.length() - 3);
    }

    public static String g(String str) {
        if (str == null || str.length() < 4) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length() % 4 == 0 ? str.length() / 4 : (str.length() / 4) + 1;
        for (int i = 0; i < length - 1; i++) {
            stringBuffer.append(str.substring(i * 4, (i * 4) + 4) + " ");
        }
        for (int i2 = length - 1; i2 < length; i2++) {
            stringBuffer.append(str.substring(i2 * 4, str.length()));
        }
        return str.substring(0, 4) + " **** **** " + stringBuffer.substring(15);
    }

    public static String h(String str) {
        return str.substring(str.length() - 4);
    }
}
