package com.lthj.unipay.plugin;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.HeadData;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;

public class cl {
    public static void a(UIResponseListener uIResponseListener, Context context) {
        bw bwVar = new bw(8207);
        bwVar.a(HeadData.createHeadData("GetPanBankBindList.Req", context));
        bwVar.a(ap.a().z.toString());
        try {
            ck.a().a(bwVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str) {
        ai aiVar = new ai(8204);
        aiVar.a(HeadData.createHeadData("DefaultPanSet.Req", context));
        aiVar.a(ap.a().z.toString());
        aiVar.b(ap.a().A.toString());
        aiVar.c(str);
        try {
            ck.a().a(aiVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, bt btVar, String str2) {
        bz bzVar = new bz(8229);
        bzVar.a(HeadData.createHeadData("PanBankBindExpress.Req", context));
        bzVar.a(str);
        bzVar.b(ap.a().h);
        bzVar.c(ap.a().k);
        bzVar.d(ap.a().l);
        bzVar.e(ap.a().v.toString());
        bzVar.f(ap.a().y.toString());
        ap.a().y.setLength(0);
        if (!TextUtils.isEmpty(str2)) {
            bzVar.h(str2);
        }
        if (!(btVar == null || btVar.b() == null || btVar.a() == null)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(btVar.b(), btVar.b().length)));
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(btVar.a(), btVar.a().length)));
            bzVar.g(stringBuffer.toString());
            stringBuffer.delete(0, stringBuffer.length());
        }
        try {
            ck.a().a(bzVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        m mVar = new m(8195);
        mVar.a(HeadData.createHeadData("UserResetPwd.Req", context));
        mVar.a(str);
        mVar.c(str2);
        mVar.b(ap.a().c.d.toString());
        ap.a().c.e.setLength(0);
        try {
            ck.a().a(mVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, bt btVar, String str4, boolean z) {
        r rVar = new r(8203);
        rVar.a(HeadData.createHeadData("PanBankBind.Req", context));
        rVar.a(ap.a().z.toString());
        rVar.d(str3);
        rVar.e(ap.a().y.toString());
        ap.a().y.setLength(0);
        rVar.b(str);
        rVar.c(str2);
        if (str4 != null) {
            rVar.g(str4);
        }
        if (!(btVar == null || btVar.b() == null || btVar.a() == null)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(btVar.b(), btVar.b().length)));
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(btVar.a(), btVar.a().length)));
            rVar.f(stringBuffer.toString());
            stringBuffer.delete(0, stringBuffer.length());
        }
        if (z) {
            rVar.h("1");
        } else {
            rVar.h("0");
        }
        try {
            ck.a().a(rVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, String str4) {
        bb bbVar = new bb(8221);
        bbVar.a(HeadData.createHeadData("GetSecureQuestion.Req", context));
        bbVar.a(str);
        bbVar.c(str2);
        bbVar.b(str3);
        bbVar.d(str4);
        try {
            ck.a().a(bbVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, boolean z) {
        db dbVar = new db(8224);
        dbVar.a(HeadData.createHeadData("GetBankInfo.Req", context));
        if (z) {
            dbVar.c(ap.a().j);
        }
        dbVar.a(str);
        dbVar.b(str2);
        try {
            ck.a().a(dbVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, ProgressBar progressBar) {
        if (progressBar != null) {
            progressBar.setVisibility(0);
            q qVar = new q(8201);
            qVar.a(HeadData.createHeadData("GetVerifyCode.Req", progressBar.getContext()));
            try {
                ck.a().a(qVar, uIResponseListener, progressBar.getContext(), false, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(String str, UIResponseListener uIResponseListener, Context context) {
        o oVar = new o(8202);
        oVar.a(HeadData.createHeadData("GetBankList.Req", context));
        oVar.a(str);
        try {
            ck.a().a(oVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str, String str2, String str3, UIResponseListener uIResponseListener, Context context, Button button) {
        if (c.a(context, str)) {
            button.setText(PluginLink.getStringupomp_lthj_getting_mac());
            button.setEnabled(false);
            n nVar = new n(8200);
            nVar.b(str2);
            nVar.a(HeadData.createHeadData("GetMobileMac.Req", context));
            nVar.a(str);
            nVar.j(str3);
            try {
                ck.a().a(nVar, uIResponseListener, context, false, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str) {
        p pVar = new p(8196);
        pVar.a(HeadData.createHeadData("UserUpdatePwd.Req", context));
        pVar.a(ap.a().z.toString());
        pVar.d(ap.a().A.toString());
        pVar.e(str);
        pVar.b(ap.a().c.c.toString());
        pVar.c(ap.a().c.d.toString());
        ap.a().c.e.setLength(0);
        try {
            ck.a().a(pVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        ej ejVar = new ej(8206);
        ejVar.a(HeadData.createHeadData("PanDelete.Req", context));
        ejVar.a(ap.a().z.toString());
        ejVar.b(ap.a().A.toString());
        ejVar.d(str);
        ejVar.c(str2);
        try {
            ck.a().a(ejVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, String str4) {
        cp cpVar = new cp(8225);
        cpVar.a(HeadData.createHeadData("UserRegisterExpress.Req", context));
        cpVar.a(str);
        cpVar.e(str2);
        cpVar.b(ap.a().c.d.toString());
        cpVar.f(str3);
        cpVar.g(str4);
        cpVar.c(ap.a().h);
        cpVar.d(ap.a().k);
        cpVar.h(ap.a().l);
        try {
            ck.a().a(cpVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void c(UIResponseListener uIResponseListener, Context context, String str) {
        ao aoVar = new ao(8228);
        aoVar.a(HeadData.createHeadData("GetBanksService.Req", context));
        aoVar.a(str);
        try {
            ck.a().a(aoVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void c(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        ar arVar = new ar(8197);
        arVar.a(HeadData.createHeadData("UpdateMobileNumber.Req", context));
        arVar.a(ap.a().z.toString());
        arVar.b(ap.a().A.toString());
        arVar.c(str);
        arVar.d(str2);
        arVar.e(ap.a().B.toString());
        try {
            ck.a().a(arVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void d(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        cm cmVar = new cm(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        cmVar.a(HeadData.createHeadData("UserLogin.Req", context));
        cmVar.a(str);
        cmVar.b(ap.a().B.toString());
        cmVar.c(str2);
        try {
            ck.a().a(cmVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
