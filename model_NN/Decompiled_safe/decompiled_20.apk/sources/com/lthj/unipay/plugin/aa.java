package com.lthj.unipay.plugin;

import android.content.DialogInterface;
import com.unionpay.upomp.lthj.plugin.ui.SplashActivity;
import com.unionpay.upomp.lthj.util.PluginHelper;

public class aa implements DialogInterface.OnClickListener {
    final /* synthetic */ SplashActivity a;

    public aa(SplashActivity splashActivity) {
        this.a = splashActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        ap.a().e = false;
        synchronized (PluginHelper.a) {
            PluginHelper.a.notify();
        }
        j.c();
        this.a.finish();
    }
}
