package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import com.unionpay.upomp.lthj.plugin.ui.SplashActivity;

public class af extends Handler {
    final /* synthetic */ SplashActivity a;

    public af(SplashActivity splashActivity) {
        this.a = splashActivity;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.a.b >= 100) {
            this.a.b = 0;
            this.a.g.setProgress(this.a.b);
            return;
        }
        this.a.b += 5;
        this.a.g.incrementProgressBy(5);
    }
}
