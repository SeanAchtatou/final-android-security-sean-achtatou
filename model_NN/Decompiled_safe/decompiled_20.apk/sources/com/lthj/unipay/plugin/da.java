package com.lthj.unipay.plugin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class da {
    private static final cq a = new bo();

    public static byte[] a(String str) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a.a(str, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("exception decoding Hex string: " + e);
        }
    }
}
