package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.PreAuth;

public class ee extends w {
    private String A;
    private String B;
    private String a;
    private String b;
    private StringBuffer c;
    private StringBuffer d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y;
    private String z;

    public ee(int i2) {
        super(i2);
    }

    public void a(Data data) {
        PreAuth preAuth = (PreAuth) data;
        c(preAuth);
        this.a = preAuth.payType;
        this.b = preAuth.loginName;
        this.d.delete(0, this.d.length());
        this.d.append(preAuth.mobileNumber);
        this.l = preAuth.merchantId;
        this.m = preAuth.merchantOrderId;
        this.n = preAuth.merchantOrderTime;
        this.o = preAuth.merchantOrderAmt;
        this.u = preAuth.settleDate;
        this.v = preAuth.setlAmt;
        this.w = preAuth.setlCurrency;
        this.x = preAuth.converRate;
        this.y = preAuth.cupsQid;
        this.z = preAuth.cupsTraceNum;
        this.A = preAuth.cupsTraceTime;
        this.B = preAuth.cupsRespCode;
    }

    public void a(String str) {
        this.a = str;
    }

    public void a(StringBuffer stringBuffer) {
        this.c = stringBuffer;
    }

    public Data b() {
        PreAuth preAuth = new PreAuth();
        b(preAuth);
        preAuth.payType = this.a;
        preAuth.loginName = this.b;
        preAuth.password = this.c.toString();
        this.c.delete(0, this.c.length());
        preAuth.mobileNumber = this.d.toString();
        preAuth.mobileMac = this.e;
        preAuth.validateCode = this.f;
        preAuth.pan = this.g;
        preAuth.pin = this.h;
        preAuth.panDate = this.i;
        preAuth.cvn2 = this.j;
        preAuth.merchantId = this.l;
        preAuth.merchantName = this.k;
        preAuth.merchantOrderAmt = this.o;
        preAuth.merchantOrderDesc = this.p;
        preAuth.merchantOrderId = this.m;
        preAuth.merchantOrderTime = this.n;
        preAuth.transTimeout = this.q;
        preAuth.backEndUrl = this.r;
        return preAuth;
    }

    public void b(String str) {
        this.b = str;
    }

    public void c(String str) {
        this.d = new StringBuffer(str);
    }

    public void d(String str) {
        this.e = str;
    }

    public void e(String str) {
        this.f = str;
    }

    public void f(String str) {
        this.g = str;
    }

    public void g(String str) {
        this.h = str;
    }

    public void h(String str) {
        this.i = str;
    }

    public void k(String str) {
        this.j = str;
    }

    public void l(String str) {
        this.k = str;
    }

    public void m(String str) {
        this.l = str;
    }

    public void n(String str) {
        this.m = str;
    }

    public void o(String str) {
        this.n = str;
    }

    public void p(String str) {
        this.o = str;
    }

    public void q(String str) {
        this.p = str;
    }

    public void r(String str) {
        this.q = str;
    }

    public void s(String str) {
        this.r = str;
    }

    public void t(String str) {
        this.s = str;
    }

    public void u(String str) {
        this.t = str;
    }
}
