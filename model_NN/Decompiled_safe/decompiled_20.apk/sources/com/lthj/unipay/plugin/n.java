package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.SMSDynamicValCodeIssue;

public class n extends w {
    private String a;
    private String b;
    private String c;

    public n(int i) {
        super(i);
    }

    public String a() {
        return this.b;
    }

    public void a(Data data) {
        SMSDynamicValCodeIssue sMSDynamicValCodeIssue = (SMSDynamicValCodeIssue) data;
        c(sMSDynamicValCodeIssue);
        this.a = sMSDynamicValCodeIssue.mobileNumber;
        this.c = sMSDynamicValCodeIssue.mobileMac;
        this.b = sMSDynamicValCodeIssue.secureInfo;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        SMSDynamicValCodeIssue sMSDynamicValCodeIssue = new SMSDynamicValCodeIssue();
        b(sMSDynamicValCodeIssue);
        sMSDynamicValCodeIssue.mobileNumber = this.a;
        sMSDynamicValCodeIssue.secureInfo = this.b;
        return sMSDynamicValCodeIssue;
    }

    public void b(String str) {
        this.b = str;
    }

    public String c() {
        return this.c;
    }
}
