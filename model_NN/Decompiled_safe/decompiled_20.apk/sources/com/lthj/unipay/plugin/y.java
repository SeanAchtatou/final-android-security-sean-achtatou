package com.lthj.unipay.plugin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class y extends SQLiteOpenHelper {
    private static String a = "plug_info.db";
    private String b;
    private String[] c;
    private String[] d;
    private SQLiteDatabase e;

    public y(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, a, cursorFactory, di.c);
    }

    public y(Context context, String str, String[] strArr, String[] strArr2) {
        this(context, (String) null, (SQLiteDatabase.CursorFactory) null, di.c);
        this.b = str;
        this.c = strArr;
        this.d = strArr2;
    }

    public Cursor a(int i) {
        this.e = getReadableDatabase();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from ").append(this.b).append(" where _id = ").append(i);
        return this.e.rawQuery(stringBuffer.toString(), null);
    }

    public void a() {
        if (this.e != null) {
            this.e.close();
        }
    }

    public boolean a(int i, ContentValues contentValues) {
        this.e = getWritableDatabase();
        int update = this.e.update(this.b, contentValues, "_id = ?", new String[]{new StringBuilder().append(i).toString()});
        if (this.e != null) {
            this.e.close();
        }
        return update != 0;
    }

    public boolean a(ContentValues contentValues) {
        this.e = getWritableDatabase();
        long insert = this.e.insert(this.b, null, contentValues);
        if (this.e != null) {
            this.e.close();
        }
        return insert != -1;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Log.i("PlugDatabaseHelper", "PlugDatabaseHelper onCraete...");
        if (this.b == null || this.c == null || this.d == null) {
            Log.i("PlugDatabaseHelper", "PlugDatabaseHelper onCraete error..");
        } else if (this.c.length != this.d.length) {
            Log.i("PlugDatabaseHelper", "PlugDatabaseHelper onCraete error..");
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("CREATE TABLE ").append(this.b).append(" ( _id INTEGER PRIMARY KEY ");
            for (int i = 0; i < this.c.length; i++) {
                stringBuffer.append(",").append(this.c[i]).append(" ").append(this.d[i]);
            }
            stringBuffer.append(");");
            sQLiteDatabase.execSQL(stringBuffer.toString());
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.i("PlugDatabaseHelper", "PlugDatabaseHelper onUpgrade...");
        Log.w("PlugDatabaseHelper", "Upgrading database from version " + i + " to " + i2 + ", which will destroy all old data");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + this.b);
        onCreate(sQLiteDatabase);
    }
}
