package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.GetSecureQuestion;

public class bb extends w {
    private String a;
    private String b;
    private StringBuffer c = new StringBuffer();
    private String d;
    private String e;

    public bb(int i) {
        super(i);
    }

    public String a() {
        return this.a;
    }

    public void a(Data data) {
        GetSecureQuestion getSecureQuestion = (GetSecureQuestion) data;
        c(getSecureQuestion);
        this.a = getSecureQuestion.loginName;
        this.b = getSecureQuestion.mobileMac;
        this.c.delete(0, this.c.length());
        this.c.append(getSecureQuestion.mobileNumber);
        this.d = getSecureQuestion.secureQuestion;
        this.e = getSecureQuestion.validateCode;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        GetSecureQuestion getSecureQuestion = new GetSecureQuestion();
        b(getSecureQuestion);
        getSecureQuestion.loginName = this.a;
        getSecureQuestion.mobileMac = this.b;
        getSecureQuestion.mobileNumber = this.c.toString();
        getSecureQuestion.validateCode = this.e;
        return getSecureQuestion;
    }

    public void b(String str) {
        this.b = str;
    }

    public String c() {
        return this.c.toString();
    }

    public void c(String str) {
        this.c.delete(0, this.c.length());
        this.c.append(str);
    }

    public String d() {
        return this.d;
    }

    public void d(String str) {
        this.e = str;
    }
}
