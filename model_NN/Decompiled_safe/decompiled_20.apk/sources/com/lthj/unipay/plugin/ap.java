package com.lthj.unipay.plugin;

import android.util.Xml;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.common.b.e;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Vector;
import org.apache.commons.httpclient.cookie.Cookie2;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class ap {
    private static ap N = null;
    public StringBuffer A = new StringBuffer();
    public StringBuffer B = new StringBuffer();
    public Vector C;
    public GetBundleBankCardList D;
    public String E;
    public String F;
    public String G;
    public String H;
    public String I;
    public String J;
    public String K;
    public String L;
    public String M;
    public dc a;
    public i b;
    public z c;
    public boolean d;
    public boolean e = false;
    public boolean f = false;
    public boolean g = false;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public String p;
    public String q;
    public String r;
    public String s;
    public String t;
    public boolean u;
    public StringBuffer v = new StringBuffer();
    public StringBuffer w = new StringBuffer();
    public StringBuffer x = new StringBuffer();
    public StringBuffer y = new StringBuffer();
    public StringBuffer z = new StringBuffer();

    private ap() {
        e();
    }

    public static synchronized ap a() {
        ap apVar;
        synchronized (ap.class) {
            if (N == null) {
                N = new ap();
            }
            apVar = N;
        }
        return apVar;
    }

    private void e() {
        this.a = new dc();
        this.b = new i();
        this.c = new z();
    }

    public void a(boolean z2) {
        this.d = z2;
        if (z2) {
            this.L = "http://211.154.166.219/qzjy/GateWay/deal.action";
            this.M = "upomp_lthj_config_test.xml";
            return;
        }
        this.L = "http://mobilepay.unionpaysecure.com/qzjy/GateWay/deal.action";
        this.M = "upomp_lthj_config_fromal.xml";
    }

    public void a(byte[] bArr) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(byteArrayInputStream, e.f);
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        String name = newPullParser.getName();
                        if (!name.equalsIgnoreCase("merchantId")) {
                            if (!name.equalsIgnoreCase("merchantOrderId")) {
                                if (!name.equalsIgnoreCase("merchantOrderTime")) {
                                    if (!name.equalsIgnoreCase("sign")) {
                                        break;
                                    } else {
                                        this.r = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    this.l = newPullParser.nextText();
                                    break;
                                }
                            } else {
                                this.k = newPullParser.nextText();
                                break;
                            }
                        } else {
                            this.h = newPullParser.nextText();
                            break;
                        }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            throw e2;
        }
    }

    public void b() {
        this.c.a();
        N = null;
    }

    public void c() {
        this.z.delete(0, this.z.length());
        this.A.delete(0, this.A.length());
        this.B.delete(0, this.B.length());
    }

    public byte[] d() {
        XmlSerializer newSerializer = Xml.newSerializer();
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            newSerializer.setOutput(byteArrayOutputStream, "utf-8");
            newSerializer.startDocument("utf-8", true);
            newSerializer.startTag(null, "upomp");
            newSerializer.attribute(null, Cookie2.VERSION, "1.0.0");
            newSerializer.startTag(null, "application");
            newSerializer.text("LanchPay.Rsp");
            newSerializer.endTag(null, "application");
            newSerializer.startTag(null, "merchantId");
            newSerializer.text(this.h);
            newSerializer.endTag(null, "merchantId");
            newSerializer.startTag(null, "merchantOrderId");
            newSerializer.text(this.k);
            newSerializer.endTag(null, "merchantOrderId");
            newSerializer.startTag(null, "merchantOrderTime");
            newSerializer.text(this.l);
            newSerializer.endTag(null, "merchantOrderTime");
            newSerializer.startTag(null, "respCode");
            if (this.s != null) {
                newSerializer.text(this.s);
            } else {
                newSerializer.text(PoiTypeDef.All);
            }
            newSerializer.endTag(null, "respCode");
            newSerializer.startTag(null, "respDesc");
            if (this.t != null) {
                newSerializer.text(this.t);
            } else {
                newSerializer.text(PoiTypeDef.All);
            }
            newSerializer.endTag(null, "respDesc");
            newSerializer.endTag(null, "upomp");
            newSerializer.endDocument();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
