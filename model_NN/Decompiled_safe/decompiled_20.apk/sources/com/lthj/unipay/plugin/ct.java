package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import com.unionpay.upomp.lthj.link.PluginLink;

class ct extends Handler {
    final /* synthetic */ ck a;

    ct(ck ckVar) {
        this.a = ckVar;
    }

    private void a(w wVar) {
        if ("0000".equals(wVar.m())) {
            switch (wVar.e()) {
                case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE:
                case 8195:
                case 8196:
                case 8197:
                case 8200:
                case 8201:
                case 8203:
                case 8204:
                case 8206:
                    return;
                case 8198:
                case 8199:
                case 8202:
                case 8205:
                default:
                    l.a().b();
                    return;
            }
        } else {
            switch (wVar.e()) {
                case 8210:
                case 8222:
                case 8225:
                case 8227:
                case 8229:
                    l.a().b();
                    return;
                default:
                    return;
            }
        }
    }

    private void b(w wVar) {
        switch (wVar.e()) {
            case FragmentTransaction.TRANSIT_FRAGMENT_CLOSE:
            case 8221:
            case 8224:
                ap.a().b.f(true);
                return;
            default:
                return;
        }
    }

    public void handleMessage(Message message) {
        ca caVar;
        w wVar = (w) message.obj;
        if (!"3022".equals(this.a.d.m())) {
            a(wVar);
            this.a.a(wVar);
            if (!"0000".equals(wVar.m())) {
                b(wVar);
            }
            this.a.c.responseCallBack(wVar);
            if (this.a.e != null && (caVar = (ca) this.a.e.poll()) != null) {
                this.a.a(caVar);
            }
        } else if (this.a.a != null) {
            l.a().a(this.a.a, this.a.a.getString(PluginLink.getStringupomp_lthj_session_timeout_tip()), new as(this));
        }
    }
}
