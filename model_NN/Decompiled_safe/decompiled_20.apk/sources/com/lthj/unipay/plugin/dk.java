package com.lthj.unipay.plugin;

import android.os.Message;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;
import java.util.TimerTask;

public class dk extends TimerTask {
    final /* synthetic */ PayActivity a;

    public dk(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void run() {
        Message obtain = Message.obtain();
        obtain.obj = new StringBuilder().append(this.a.a).toString();
        this.a.F.sendMessage(obtain);
        if (this.a.a < 0) {
            this.a.aaTimerTask.cancel();
            this.a.aaTimerTask = null;
            this.a.a = 60;
            return;
        }
        PayActivity payActivity = this.a;
        payActivity.a--;
    }
}
