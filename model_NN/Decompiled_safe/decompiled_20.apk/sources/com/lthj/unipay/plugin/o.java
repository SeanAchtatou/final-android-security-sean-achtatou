package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.GetBankCardList;

public class o extends w {
    private String a;
    private String b;
    private String c;

    public o(int i) {
        super(i);
    }

    public String a() {
        return this.a;
    }

    public void a(Data data) {
        GetBankCardList getBankCardList = (GetBankCardList) data;
        c(getBankCardList);
        this.a = getBankCardList.panBankId;
        this.b = getBankCardList.panBank;
        this.c = getBankCardList.panType;
    }

    public void a(String str) {
        this.c = str;
    }

    public Data b() {
        GetBankCardList getBankCardList = new GetBankCardList();
        b(getBankCardList);
        getBankCardList.panType = this.c;
        return getBankCardList;
    }

    public String c() {
        return this.b;
    }

    public String d() {
        return this.c;
    }
}
