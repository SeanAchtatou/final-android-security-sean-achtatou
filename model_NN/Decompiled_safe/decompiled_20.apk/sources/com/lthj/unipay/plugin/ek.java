package com.lthj.unipay.plugin;

import android.content.DialogInterface;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;

public class ek implements DialogInterface.OnClickListener {
    final /* synthetic */ PayActivity a;

    public ek(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        ap.a().D = (GetBundleBankCardList) ap.a().C.get(i);
        this.a.l.a(ap.a().D.panBank + "-" + j.b(ap.a().D.panType) + "-" + ap.a().D.pan.substring(ap.a().D.pan.length() - 4, ap.a().D.pan.length()));
        this.a.m.a(j.f(ap.a().D.mobileNumber));
    }
}
