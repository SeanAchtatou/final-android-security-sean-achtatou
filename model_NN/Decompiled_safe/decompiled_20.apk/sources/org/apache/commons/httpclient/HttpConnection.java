package org.apache.commons.httpclient;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.httpclient.util.ExceptionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HttpConnection {
    private static final byte[] CRLF = {13, 10};
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$HttpConnection;
    private String hostName;
    private HttpConnectionManager httpConnectionManager;
    private InputStream inputStream;
    protected boolean isOpen;
    private InputStream lastResponseInputStream;
    private InetAddress localAddress;
    private boolean locked;
    private OutputStream outputStream;
    private HttpConnectionParams params;
    private int portNumber;
    private Protocol protocolInUse;
    private String proxyHostName;
    private int proxyPortNumber;
    private Socket socket;
    private boolean tunnelEstablished;
    private boolean usingSecureSocket;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HttpConnection == null) {
            cls = class$("org.apache.commons.httpclient.HttpConnection");
            class$org$apache$commons$httpclient$HttpConnection = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HttpConnection;
        }
        LOG = LogFactory.getLog(cls);
    }

    public HttpConnection(String str, int i) {
        this(null, -1, str, null, i, Protocol.getProtocol("http"));
    }

    public HttpConnection(String str, int i, String str2, int i2) {
        this(str, i, str2, null, i2, Protocol.getProtocol("http"));
    }

    public HttpConnection(String str, int i, String str2, int i2, Protocol protocol) {
        this.hostName = null;
        this.portNumber = -1;
        this.proxyHostName = null;
        this.proxyPortNumber = -1;
        this.socket = null;
        this.inputStream = null;
        this.outputStream = null;
        this.lastResponseInputStream = null;
        this.isOpen = false;
        this.params = new HttpConnectionParams();
        this.locked = false;
        this.usingSecureSocket = false;
        this.tunnelEstablished = false;
        if (str2 == null) {
            throw new IllegalArgumentException("host parameter is null");
        } else if (protocol == null) {
            throw new IllegalArgumentException("protocol is null");
        } else {
            this.proxyHostName = str;
            this.proxyPortNumber = i;
            this.hostName = str2;
            this.portNumber = protocol.resolvePort(i2);
            this.protocolInUse = protocol;
        }
    }

    public HttpConnection(String str, int i, String str2, String str3, int i2, Protocol protocol) {
        this(str, i, str2, i2, protocol);
    }

    public HttpConnection(String str, int i, Protocol protocol) {
        this(null, -1, str, null, i, protocol);
    }

    public HttpConnection(String str, String str2, int i, Protocol protocol) {
        this(null, -1, str, str2, i, protocol);
    }

    public HttpConnection(HostConfiguration hostConfiguration) {
        this(hostConfiguration.getProxyHost(), hostConfiguration.getProxyPort(), hostConfiguration.getHost(), hostConfiguration.getPort(), hostConfiguration.getProtocol());
        this.localAddress = hostConfiguration.getLocalAddress();
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void assertNotOpen() {
        if (this.isOpen) {
            throw new IllegalStateException("Connection is open");
        }
    }

    /* access modifiers changed from: protected */
    public void assertOpen() {
        if (!this.isOpen) {
            throw new IllegalStateException("Connection is not open");
        }
    }

    public void close() {
        LOG.trace("enter HttpConnection.close()");
        closeSocketAndStreams();
    }

    public boolean closeIfStale() {
        if (!this.isOpen || !isStale()) {
            return false;
        }
        LOG.debug("Connection is stale, closing...");
        close();
        return true;
    }

    /* access modifiers changed from: protected */
    public void closeSocketAndStreams() {
        LOG.trace("enter HttpConnection.closeSockedAndStreams()");
        this.isOpen = false;
        this.lastResponseInputStream = null;
        if (this.outputStream != null) {
            OutputStream outputStream2 = this.outputStream;
            this.outputStream = null;
            try {
                outputStream2.close();
            } catch (Exception e) {
                LOG.debug("Exception caught when closing output", e);
            }
        }
        if (this.inputStream != null) {
            InputStream inputStream2 = this.inputStream;
            this.inputStream = null;
            try {
                inputStream2.close();
            } catch (Exception e2) {
                LOG.debug("Exception caught when closing input", e2);
            }
        }
        if (this.socket != null) {
            Socket socket2 = this.socket;
            this.socket = null;
            try {
                socket2.close();
            } catch (Exception e3) {
                LOG.debug("Exception caught when closing socket", e3);
            }
        }
        this.tunnelEstablished = false;
        this.usingSecureSocket = false;
    }

    public void flushRequestOutputStream() {
        LOG.trace("enter HttpConnection.flushRequestOutputStream()");
        assertOpen();
        this.outputStream.flush();
    }

    public String getHost() {
        return this.hostName;
    }

    public HttpConnectionManager getHttpConnectionManager() {
        return this.httpConnectionManager;
    }

    public InputStream getLastResponseInputStream() {
        return this.lastResponseInputStream;
    }

    public InetAddress getLocalAddress() {
        return this.localAddress;
    }

    public HttpConnectionParams getParams() {
        return this.params;
    }

    public int getPort() {
        return this.portNumber < 0 ? isSecure() ? 443 : 80 : this.portNumber;
    }

    public Protocol getProtocol() {
        return this.protocolInUse;
    }

    public String getProxyHost() {
        return this.proxyHostName;
    }

    public int getProxyPort() {
        return this.proxyPortNumber;
    }

    public OutputStream getRequestOutputStream() {
        LOG.trace("enter HttpConnection.getRequestOutputStream()");
        assertOpen();
        OutputStream outputStream2 = this.outputStream;
        return Wire.CONTENT_WIRE.enabled() ? new WireLogOutputStream(outputStream2, Wire.CONTENT_WIRE) : outputStream2;
    }

    public InputStream getResponseInputStream() {
        LOG.trace("enter HttpConnection.getResponseInputStream()");
        assertOpen();
        return this.inputStream;
    }

    public int getSendBufferSize() {
        if (this.socket == null) {
            return -1;
        }
        return this.socket.getSendBufferSize();
    }

    public int getSoTimeout() {
        return this.params.getSoTimeout();
    }

    /* access modifiers changed from: protected */
    public Socket getSocket() {
        return this.socket;
    }

    public String getVirtualHost() {
        return this.hostName;
    }

    /* access modifiers changed from: protected */
    public boolean isLocked() {
        return this.locked;
    }

    public boolean isOpen() {
        return this.isOpen;
    }

    public boolean isProxied() {
        return this.proxyHostName != null && this.proxyPortNumber > 0;
    }

    public boolean isResponseAvailable() {
        LOG.trace("enter HttpConnection.isResponseAvailable()");
        return this.isOpen && this.inputStream.available() > 0;
    }

    public boolean isResponseAvailable(int i) {
        boolean z = true;
        LOG.trace("enter HttpConnection.isResponseAvailable(int)");
        assertOpen();
        if (this.inputStream.available() > 0) {
            return true;
        }
        try {
            this.socket.setSoTimeout(i);
            this.inputStream.mark(1);
            if (this.inputStream.read() != -1) {
                this.inputStream.reset();
                LOG.debug("Input data available");
            } else {
                LOG.debug("Input data not available");
                z = false;
            }
            try {
                this.socket.setSoTimeout(this.params.getSoTimeout());
                return z;
            } catch (IOException e) {
                LOG.debug("An error ocurred while resetting soTimeout, we will assume that no response is available.", e);
                return false;
            }
        } catch (InterruptedIOException e2) {
            if (!ExceptionUtil.isSocketTimeoutException(e2)) {
                throw e2;
            }
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Input data not available after ").append(i).append(" ms").toString());
            }
            try {
                this.socket.setSoTimeout(this.params.getSoTimeout());
                return false;
            } catch (IOException e3) {
                LOG.debug("An error ocurred while resetting soTimeout, we will assume that no response is available.", e3);
                return false;
            }
        } catch (Throwable th) {
            try {
                this.socket.setSoTimeout(this.params.getSoTimeout());
            } catch (IOException e4) {
                LOG.debug("An error ocurred while resetting soTimeout, we will assume that no response is available.", e4);
            }
            throw th;
        }
    }

    public boolean isSecure() {
        return this.protocolInUse.isSecure();
    }

    /* access modifiers changed from: protected */
    public boolean isStale() {
        if (!this.isOpen) {
            return true;
        }
        boolean z = false;
        try {
            if (this.inputStream.available() > 0) {
                return false;
            }
            this.socket.setSoTimeout(1);
            this.inputStream.mark(1);
            if (this.inputStream.read() == -1) {
                z = true;
            } else {
                this.inputStream.reset();
            }
            this.socket.setSoTimeout(this.params.getSoTimeout());
            return z;
        } catch (InterruptedIOException e) {
            if (ExceptionUtil.isSocketTimeoutException(e)) {
                return z;
            }
            throw e;
        } catch (IOException e2) {
            LOG.debug("An error occurred while reading from the socket, is appears to be stale", e2);
            return true;
        } catch (Throwable th) {
            this.socket.setSoTimeout(this.params.getSoTimeout());
            throw th;
        }
    }

    public boolean isStaleCheckingEnabled() {
        return this.params.isStaleCheckingEnabled();
    }

    public boolean isTransparent() {
        return !isProxied() || this.tunnelEstablished;
    }

    public void open() {
        LOG.trace("enter HttpConnection.open()");
        String str = this.proxyHostName == null ? this.hostName : this.proxyHostName;
        int i = this.proxyHostName == null ? this.portNumber : this.proxyPortNumber;
        assertNotOpen();
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("Open connection to ").append(str).append(":").append(i).toString());
        }
        try {
            if (this.socket == null) {
                this.usingSecureSocket = isSecure() && !isProxied();
                this.socket = ((!isSecure() || !isProxied()) ? this.protocolInUse.getSocketFactory() : Protocol.getProtocol("http").getSocketFactory()).createSocket(str, i, this.localAddress, 0, this.params);
            }
            this.socket.setTcpNoDelay(this.params.getTcpNoDelay());
            this.socket.setSoTimeout(this.params.getSoTimeout());
            int linger = this.params.getLinger();
            if (linger >= 0) {
                this.socket.setSoLinger(linger > 0, linger);
            }
            int sendBufferSize = this.params.getSendBufferSize();
            if (sendBufferSize >= 0) {
                this.socket.setSendBufferSize(sendBufferSize);
            }
            int receiveBufferSize = this.params.getReceiveBufferSize();
            if (receiveBufferSize >= 0) {
                this.socket.setReceiveBufferSize(receiveBufferSize);
            }
            int sendBufferSize2 = this.socket.getSendBufferSize();
            int i2 = (sendBufferSize2 > 2048 || sendBufferSize2 <= 0) ? 2048 : sendBufferSize2;
            int receiveBufferSize2 = this.socket.getReceiveBufferSize();
            if (receiveBufferSize2 > 2048 || receiveBufferSize2 <= 0) {
                receiveBufferSize2 = 2048;
            }
            this.inputStream = new BufferedInputStream(this.socket.getInputStream(), receiveBufferSize2);
            this.outputStream = new BufferedOutputStream(this.socket.getOutputStream(), i2);
            this.isOpen = true;
        } catch (IOException e) {
            closeSocketAndStreams();
            throw e;
        }
    }

    public void print(String str) {
        LOG.trace("enter HttpConnection.print(String)");
        write(EncodingUtil.getBytes(str, "ISO-8859-1"));
    }

    public void print(String str, String str2) {
        LOG.trace("enter HttpConnection.print(String)");
        write(EncodingUtil.getBytes(str, str2));
    }

    public void printLine() {
        LOG.trace("enter HttpConnection.printLine()");
        writeLine();
    }

    public void printLine(String str) {
        LOG.trace("enter HttpConnection.printLine(String)");
        writeLine(EncodingUtil.getBytes(str, "ISO-8859-1"));
    }

    public void printLine(String str, String str2) {
        LOG.trace("enter HttpConnection.printLine(String)");
        writeLine(EncodingUtil.getBytes(str, str2));
    }

    public String readLine() {
        LOG.trace("enter HttpConnection.readLine()");
        assertOpen();
        return HttpParser.readLine(this.inputStream);
    }

    public String readLine(String str) {
        LOG.trace("enter HttpConnection.readLine()");
        assertOpen();
        return HttpParser.readLine(this.inputStream, str);
    }

    public void releaseConnection() {
        LOG.trace("enter HttpConnection.releaseConnection()");
        if (this.locked) {
            LOG.debug("Connection is locked.  Call to releaseConnection() ignored.");
        } else if (this.httpConnectionManager != null) {
            LOG.debug("Releasing connection back to connection manager.");
            this.httpConnectionManager.releaseConnection(this);
        } else {
            LOG.warn("HttpConnectionManager is null.  Connection cannot be released.");
        }
    }

    public void setConnectionTimeout(int i) {
        this.params.setConnectionTimeout(i);
    }

    public void setHost(String str) {
        if (str == null) {
            throw new IllegalArgumentException("host parameter is null");
        }
        assertNotOpen();
        this.hostName = str;
    }

    public void setHttpConnectionManager(HttpConnectionManager httpConnectionManager2) {
        this.httpConnectionManager = httpConnectionManager2;
    }

    public void setLastResponseInputStream(InputStream inputStream2) {
        this.lastResponseInputStream = inputStream2;
    }

    public void setLocalAddress(InetAddress inetAddress) {
        assertNotOpen();
        this.localAddress = inetAddress;
    }

    /* access modifiers changed from: protected */
    public void setLocked(boolean z) {
        this.locked = z;
    }

    public void setParams(HttpConnectionParams httpConnectionParams) {
        if (httpConnectionParams == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = httpConnectionParams;
    }

    public void setPort(int i) {
        assertNotOpen();
        this.portNumber = i;
    }

    public void setProtocol(Protocol protocol) {
        assertNotOpen();
        if (protocol == null) {
            throw new IllegalArgumentException("protocol is null");
        }
        this.protocolInUse = protocol;
    }

    public void setProxyHost(String str) {
        assertNotOpen();
        this.proxyHostName = str;
    }

    public void setProxyPort(int i) {
        assertNotOpen();
        this.proxyPortNumber = i;
    }

    public void setSendBufferSize(int i) {
        this.params.setSendBufferSize(i);
    }

    public void setSoTimeout(int i) {
        this.params.setSoTimeout(i);
        if (this.socket != null) {
            this.socket.setSoTimeout(i);
        }
    }

    public void setSocketTimeout(int i) {
        assertOpen();
        if (this.socket != null) {
            this.socket.setSoTimeout(i);
        }
    }

    public void setStaleCheckingEnabled(boolean z) {
        this.params.setStaleCheckingEnabled(z);
    }

    public void setVirtualHost(String str) {
        assertNotOpen();
    }

    public void shutdownOutput() {
        LOG.trace("enter HttpConnection.shutdownOutput()");
        try {
            Class<?> cls = this.socket.getClass();
            cls.getMethod("shutdownOutput", new Class[0]).invoke(this.socket, new Object[0]);
        } catch (Exception e) {
            LOG.debug("Unexpected Exception caught", e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      org.apache.commons.httpclient.protocol.ProtocolSocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket
      org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket */
    public void tunnelCreated() {
        int i = 2048;
        LOG.trace("enter HttpConnection.tunnelCreated()");
        if (!isSecure() || !isProxied()) {
            throw new IllegalStateException("Connection must be secure and proxied to use this feature");
        } else if (this.usingSecureSocket) {
            throw new IllegalStateException("Already using a secure socket");
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Secure tunnel to ").append(this.hostName).append(":").append(this.portNumber).toString());
            }
            this.socket = ((SecureProtocolSocketFactory) this.protocolInUse.getSocketFactory()).createSocket(this.socket, this.hostName, this.portNumber, true);
            int sendBufferSize = this.params.getSendBufferSize();
            if (sendBufferSize >= 0) {
                this.socket.setSendBufferSize(sendBufferSize);
            }
            int receiveBufferSize = this.params.getReceiveBufferSize();
            if (receiveBufferSize >= 0) {
                this.socket.setReceiveBufferSize(receiveBufferSize);
            }
            int sendBufferSize2 = this.socket.getSendBufferSize();
            if (sendBufferSize2 > 2048) {
                sendBufferSize2 = 2048;
            }
            int receiveBufferSize2 = this.socket.getReceiveBufferSize();
            if (receiveBufferSize2 <= 2048) {
                i = receiveBufferSize2;
            }
            this.inputStream = new BufferedInputStream(this.socket.getInputStream(), i);
            this.outputStream = new BufferedOutputStream(this.socket.getOutputStream(), sendBufferSize2);
            this.usingSecureSocket = true;
            this.tunnelEstablished = true;
        }
    }

    public void write(byte[] bArr) {
        LOG.trace("enter HttpConnection.write(byte[])");
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        LOG.trace("enter HttpConnection.write(byte[], int, int)");
        if (i < 0) {
            throw new IllegalArgumentException("Array offset may not be negative");
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Array length may not be negative");
        } else if (i + i2 > bArr.length) {
            throw new IllegalArgumentException("Given offset and length exceed the array length");
        } else {
            assertOpen();
            this.outputStream.write(bArr, i, i2);
        }
    }

    public void writeLine() {
        LOG.trace("enter HttpConnection.writeLine()");
        write(CRLF);
    }

    public void writeLine(byte[] bArr) {
        LOG.trace("enter HttpConnection.writeLine(byte[])");
        write(bArr);
        writeLine();
    }
}
