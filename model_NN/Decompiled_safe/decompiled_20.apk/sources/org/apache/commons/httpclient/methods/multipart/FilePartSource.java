package org.apache.commons.httpclient.methods.multipart;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class FilePartSource implements PartSource {
    private File file;
    private String fileName;

    public FilePartSource(File file2) {
        this.file = null;
        this.fileName = null;
        this.file = file2;
        if (file2 == null) {
            return;
        }
        if (!file2.isFile()) {
            throw new FileNotFoundException("File is not a normal file.");
        } else if (!file2.canRead()) {
            throw new FileNotFoundException("File is not readable.");
        } else {
            this.fileName = file2.getName();
        }
    }

    public FilePartSource(String str, File file2) {
        this(file2);
        if (str != null) {
            this.fileName = str;
        }
    }

    public InputStream createInputStream() {
        return this.file != null ? new FileInputStream(this.file) : new ByteArrayInputStream(new byte[0]);
    }

    public String getFileName() {
        return this.fileName == null ? "noname" : this.fileName;
    }

    public long getLength() {
        if (this.file != null) {
            return this.file.length();
        }
        return 0;
    }
}
