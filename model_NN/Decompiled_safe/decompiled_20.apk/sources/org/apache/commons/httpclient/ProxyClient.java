package org.apache.commons.httpclient;

import java.net.Socket;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpParams;

public class ProxyClient {
    private HostConfiguration hostConfiguration;
    private HttpClientParams params;
    private HttpState state;

    /* renamed from: org.apache.commons.httpclient.ProxyClient$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    public class ConnectResponse {
        private ConnectMethod connectMethod;
        private Socket socket;

        private ConnectResponse() {
        }

        ConnectResponse(AnonymousClass1 r1) {
            this();
        }

        static void access$100(ConnectResponse connectResponse, ConnectMethod connectMethod2) {
            connectResponse.connectMethod = connectMethod2;
        }

        static void access$200(ConnectResponse connectResponse, Socket socket2) {
            connectResponse.socket = socket2;
        }

        private void setConnectMethod(ConnectMethod connectMethod2) {
            this.connectMethod = connectMethod2;
        }

        private void setSocket(Socket socket2) {
            this.socket = socket2;
        }

        public ConnectMethod getConnectMethod() {
            return this.connectMethod;
        }

        public Socket getSocket() {
            return this.socket;
        }
    }

    class DummyConnectionManager implements HttpConnectionManager {
        private HttpParams connectionParams;
        private HttpConnection httpConnection;

        DummyConnectionManager() {
        }

        public void closeIdleConnections(long j) {
        }

        public HttpConnection getConnection() {
            return this.httpConnection;
        }

        public HttpConnection getConnection(HostConfiguration hostConfiguration) {
            return getConnectionWithTimeout(hostConfiguration, -1);
        }

        public HttpConnection getConnection(HostConfiguration hostConfiguration, long j) {
            return getConnectionWithTimeout(hostConfiguration, j);
        }

        public HttpConnection getConnectionWithTimeout(HostConfiguration hostConfiguration, long j) {
            this.httpConnection = new HttpConnection(hostConfiguration);
            this.httpConnection.setHttpConnectionManager(this);
            this.httpConnection.getParams().setDefaults(this.connectionParams);
            return this.httpConnection;
        }

        public HttpConnectionManagerParams getParams() {
            return null;
        }

        public void releaseConnection(HttpConnection httpConnection2) {
        }

        public void setConnectionParams(HttpParams httpParams) {
            this.connectionParams = httpParams;
        }

        public void setParams(HttpConnectionManagerParams httpConnectionManagerParams) {
        }
    }

    public ProxyClient() {
        this(new HttpClientParams());
    }

    public ProxyClient(HttpClientParams httpClientParams) {
        this.state = new HttpState();
        this.params = null;
        this.hostConfiguration = new HostConfiguration();
        if (httpClientParams == null) {
            throw new IllegalArgumentException("Params may not be null");
        }
        this.params = httpClientParams;
    }

    public ConnectResponse connect() {
        HostConfiguration hostConfiguration2 = getHostConfiguration();
        if (hostConfiguration2.getProxyHost() == null) {
            throw new IllegalStateException("proxy host must be configured");
        } else if (hostConfiguration2.getHost() == null) {
            throw new IllegalStateException("destination host must be configured");
        } else if (hostConfiguration2.getProtocol().isSecure()) {
            throw new IllegalStateException("secure protocol socket factory may not be used");
        } else {
            ConnectMethod connectMethod = new ConnectMethod(getHostConfiguration());
            connectMethod.getParams().setDefaults(getParams());
            DummyConnectionManager dummyConnectionManager = new DummyConnectionManager();
            dummyConnectionManager.setConnectionParams(getParams());
            new HttpMethodDirector(dummyConnectionManager, hostConfiguration2, getParams(), getState()).executeMethod(connectMethod);
            ConnectResponse connectResponse = new ConnectResponse(null);
            ConnectResponse.access$100(connectResponse, connectMethod);
            if (connectMethod.getStatusCode() == 200) {
                ConnectResponse.access$200(connectResponse, dummyConnectionManager.getConnection().getSocket());
            } else {
                dummyConnectionManager.getConnection().close();
            }
            return connectResponse;
        }
    }

    public synchronized HostConfiguration getHostConfiguration() {
        return this.hostConfiguration;
    }

    public synchronized HttpClientParams getParams() {
        return this.params;
    }

    public synchronized HttpState getState() {
        return this.state;
    }

    public synchronized void setHostConfiguration(HostConfiguration hostConfiguration2) {
        this.hostConfiguration = hostConfiguration2;
    }

    public synchronized void setParams(HttpClientParams httpClientParams) {
        if (httpClientParams == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = httpClientParams;
    }

    public synchronized void setState(HttpState httpState) {
        this.state = httpState;
    }
}
