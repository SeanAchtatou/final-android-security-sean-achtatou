package org.apache.commons.httpclient;

import com.amap.api.search.poisearch.PoiTypeDef;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.util.URIUtil;

public class HttpURL extends URI {
    public static final int DEFAULT_PORT = 80;
    public static final char[] DEFAULT_SCHEME;
    public static final int _default_port = 80;
    public static final char[] _default_scheme;
    static final long serialVersionUID = -7158031098595039459L;

    static {
        char[] cArr = {'h', 't', 't', 'p'};
        DEFAULT_SCHEME = cArr;
        _default_scheme = cArr;
    }

    protected HttpURL() {
    }

    public HttpURL(String str) {
        parseUriReference(str, false);
        checkValid();
    }

    public HttpURL(String str, int i, String str2) {
        this(null, null, str, i, str2, null, null);
    }

    public HttpURL(String str, int i, String str2, String str3) {
        this(null, null, str, i, str2, str3, null);
    }

    public HttpURL(String str, String str2) {
        this.protocolCharset = str2;
        parseUriReference(str, false);
        checkValid();
    }

    public HttpURL(String str, String str2, int i, String str3) {
        this(str, str2, i, str3, (String) null, (String) null);
    }

    public HttpURL(String str, String str2, int i, String str3, String str4) {
        this(str, str2, i, str3, str4, (String) null);
    }

    public HttpURL(String str, String str2, int i, String str3, String str4, String str5) {
        StringBuffer stringBuffer = new StringBuffer();
        if (!(str == null && str2 == null && i == -1)) {
            this._scheme = DEFAULT_SCHEME;
            stringBuffer.append(_default_scheme);
            stringBuffer.append("://");
            if (str != null) {
                stringBuffer.append(str);
                stringBuffer.append('@');
            }
            if (str2 != null) {
                stringBuffer.append(URIUtil.encode(str2, URI.allowed_host));
                if (!(i == -1 && i == 80)) {
                    stringBuffer.append(':');
                    stringBuffer.append(i);
                }
            }
        }
        if (str3 != null) {
            if (scheme == null || str3.startsWith(CookieSpec.PATH_DELIM)) {
                stringBuffer.append(URIUtil.encode(str3, URI.allowed_abs_path));
            } else {
                throw new URIException(1, "abs_path requested");
            }
        }
        if (str4 != null) {
            stringBuffer.append('?');
            stringBuffer.append(URIUtil.encode(str4, URI.allowed_query));
        }
        if (str5 != null) {
            stringBuffer.append('#');
            stringBuffer.append(URIUtil.encode(str5, URI.allowed_fragment));
        }
        parseUriReference(stringBuffer.toString(), true);
        checkValid();
    }

    public HttpURL(String str, String str2, String str3) {
        this(str, str2, str3, -1, null, null, null);
    }

    public HttpURL(String str, String str2, String str3, int i) {
        this(str, str2, str3, i, null, null, null);
    }

    public HttpURL(String str, String str2, String str3, int i, String str4) {
        this(str, str2, str3, i, str4, null, null);
    }

    public HttpURL(String str, String str2, String str3, int i, String str4, String str5) {
        this(str, str2, str3, i, str4, str5, null);
    }

    public HttpURL(String str, String str2, String str3, int i, String str4, String str5, String str6) {
        this(toUserinfo(str, str2), str3, i, str4, str5, str6);
    }

    public HttpURL(String str, String str2, String str3, String str4) {
        this(null, null, str, -1, str2, str3, str4);
    }

    public HttpURL(String str, String str2, String str3, String str4, String str5) {
        this(str, str2, -1, str3, str4, str5);
    }

    public HttpURL(HttpURL httpURL, String str) {
        this(httpURL, new HttpURL(str));
    }

    public HttpURL(HttpURL httpURL, HttpURL httpURL2) {
        super(httpURL, httpURL2);
        checkValid();
    }

    public HttpURL(char[] cArr) {
        parseUriReference(new String(cArr), true);
        checkValid();
    }

    public HttpURL(char[] cArr, String str) {
        this.protocolCharset = str;
        parseUriReference(new String(cArr), true);
        checkValid();
    }

    protected static String toUserinfo(String str, String str2) {
        if (str == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(20);
        stringBuffer.append(URIUtil.encode(str, URI.allowed_within_userinfo));
        if (str2 == null) {
            return stringBuffer.toString();
        }
        stringBuffer.append(':');
        stringBuffer.append(URIUtil.encode(str2, URI.allowed_within_userinfo));
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public void checkValid() {
        if (!equals(this._scheme, DEFAULT_SCHEME) && this._scheme != null) {
            throw new URIException(1, "wrong class use");
        }
    }

    public String getEscapedPassword() {
        char[] rawPassword = getRawPassword();
        if (rawPassword == null) {
            return null;
        }
        return new String(rawPassword);
    }

    public String getEscapedUser() {
        char[] rawUser = getRawUser();
        if (rawUser == null) {
            return null;
        }
        return new String(rawUser);
    }

    public String getPassword() {
        char[] rawPassword = getRawPassword();
        if (rawPassword == null) {
            return null;
        }
        return decode(rawPassword, getProtocolCharset());
    }

    public int getPort() {
        if (this._port == -1) {
            return 80;
        }
        return this._port;
    }

    public char[] getRawAboveHierPath() {
        char[] rawCurrentHierPath = getRawCurrentHierPath();
        return (rawCurrentHierPath == null || rawCurrentHierPath.length == 0) ? rootPath : getRawCurrentHierPath(rawCurrentHierPath);
    }

    public char[] getRawCurrentHierPath() {
        return (this._path == null || this._path.length == 0) ? rootPath : super.getRawCurrentHierPath(this._path);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.indexFirstOf(char[], char):int
     arg types: [char[], int]
     candidates:
      org.apache.commons.httpclient.URI.indexFirstOf(java.lang.String, java.lang.String):int
      org.apache.commons.httpclient.URI.indexFirstOf(char[], char):int */
    public char[] getRawPassword() {
        int indexFirstOf = indexFirstOf(this._userinfo, ':');
        if (indexFirstOf == -1) {
            return null;
        }
        int length = (this._userinfo.length - indexFirstOf) - 1;
        char[] cArr = new char[length];
        System.arraycopy(this._userinfo, indexFirstOf + 1, cArr, 0, length);
        return cArr;
    }

    public char[] getRawPath() {
        char[] rawPath = super.getRawPath();
        return (rawPath == null || rawPath.length == 0) ? rootPath : rawPath;
    }

    public char[] getRawScheme() {
        if (this._scheme == null) {
            return null;
        }
        return DEFAULT_SCHEME;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.indexFirstOf(char[], char):int
     arg types: [char[], int]
     candidates:
      org.apache.commons.httpclient.URI.indexFirstOf(java.lang.String, java.lang.String):int
      org.apache.commons.httpclient.URI.indexFirstOf(char[], char):int */
    public char[] getRawUser() {
        if (this._userinfo == null || this._userinfo.length == 0) {
            return null;
        }
        int indexFirstOf = indexFirstOf(this._userinfo, ':');
        if (indexFirstOf == -1) {
            return this._userinfo;
        }
        char[] cArr = new char[indexFirstOf];
        System.arraycopy(this._userinfo, 0, cArr, 0, indexFirstOf);
        return cArr;
    }

    public String getScheme() {
        if (this._scheme == null) {
            return null;
        }
        return new String(DEFAULT_SCHEME);
    }

    public String getUser() {
        char[] rawUser = getRawUser();
        if (rawUser == null) {
            return null;
        }
        return decode(rawUser, getProtocolCharset());
    }

    public void setEscapedPassword(String str) {
        setRawPassword(str == null ? null : str.toCharArray());
    }

    public void setEscapedUser(String str) {
        setRawUser(str.toCharArray());
    }

    public void setEscapedUserinfo(String str, String str2) {
        setRawUserinfo(str.toCharArray(), str2 == null ? null : str2.toCharArray());
    }

    public void setPassword(String str) {
        setRawPassword(str == null ? null : encode(str, allowed_within_userinfo, getProtocolCharset()));
    }

    public void setQuery(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        String protocolCharset = getProtocolCharset();
        stringBuffer.append(encode(str, allowed_within_query, protocolCharset));
        stringBuffer.append('=');
        stringBuffer.append(encode(str2, allowed_within_query, protocolCharset));
        this._query = stringBuffer.toString().toCharArray();
        setURI();
    }

    public void setQuery(String[] strArr, String[] strArr2) {
        int length = strArr.length;
        if (length != strArr2.length) {
            throw new URIException("wrong array size of query");
        }
        StringBuffer stringBuffer = new StringBuffer();
        String protocolCharset = getProtocolCharset();
        for (int i = 0; i < length; i++) {
            stringBuffer.append(encode(strArr[i], allowed_within_query, protocolCharset));
            stringBuffer.append('=');
            stringBuffer.append(encode(strArr2[i], allowed_within_query, protocolCharset));
            if (i + 1 < length) {
                stringBuffer.append('&');
            }
        }
        this._query = stringBuffer.toString().toCharArray();
        setURI();
    }

    public void setRawPassword(char[] cArr) {
        if (cArr != null && !validate(cArr, within_userinfo)) {
            throw new URIException(3, "escaped password not valid");
        } else if (getRawUser() == null || getRawUser().length == 0) {
            throw new URIException(1, "username required");
        } else {
            String str = new String(getRawUser());
            String str2 = cArr == null ? null : new String(cArr);
            String stringBuffer = new StringBuffer().append(str).append(str2 == null ? PoiTypeDef.All : new StringBuffer(":").append(str2).toString()).toString();
            String str3 = new String(getRawHost());
            if (this._port != -1) {
                str3 = new StringBuffer().append(str3).append(":").append(this._port).toString();
            }
            String stringBuffer2 = new StringBuffer().append(stringBuffer).append("@").append(str3).toString();
            this._userinfo = stringBuffer.toCharArray();
            this._authority = stringBuffer2.toCharArray();
            setURI();
        }
    }

    public void setRawUser(char[] cArr) {
        if (cArr == null || cArr.length == 0) {
            throw new URIException(1, "user required");
        } else if (!validate(cArr, within_userinfo)) {
            throw new URIException(3, "escaped user not valid");
        } else {
            String str = new String(cArr);
            char[] rawPassword = getRawPassword();
            String str2 = rawPassword == null ? null : new String(rawPassword);
            String stringBuffer = new StringBuffer().append(str).append(str2 == null ? PoiTypeDef.All : new StringBuffer(":").append(str2).toString()).toString();
            String str3 = new String(getRawHost());
            if (this._port != -1) {
                str3 = new StringBuffer().append(str3).append(":").append(this._port).toString();
            }
            String stringBuffer2 = new StringBuffer().append(stringBuffer).append("@").append(str3).toString();
            this._userinfo = stringBuffer.toCharArray();
            this._authority = stringBuffer2.toCharArray();
            setURI();
        }
    }

    public void setRawUserinfo(char[] cArr, char[] cArr2) {
        if (cArr == null || cArr.length == 0) {
            throw new URIException(1, "user required");
        } else if (!validate(cArr, within_userinfo) || (cArr2 != null && !validate(cArr2, within_userinfo))) {
            throw new URIException(3, "escaped userinfo not valid");
        } else {
            String str = new String(cArr);
            String str2 = cArr2 == null ? null : new String(cArr2);
            String stringBuffer = new StringBuffer().append(str).append(str2 == null ? PoiTypeDef.All : new StringBuffer(":").append(str2).toString()).toString();
            String str3 = new String(getRawHost());
            if (this._port != -1) {
                str3 = new StringBuffer().append(str3).append(":").append(this._port).toString();
            }
            String stringBuffer2 = new StringBuffer().append(stringBuffer).append("@").append(str3).toString();
            this._userinfo = stringBuffer.toCharArray();
            this._authority = stringBuffer2.toCharArray();
            setURI();
        }
    }

    /* access modifiers changed from: protected */
    public void setURI() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this._scheme != null) {
            stringBuffer.append(this._scheme);
            stringBuffer.append(':');
        }
        if (this._is_net_path) {
            stringBuffer.append("//");
            if (this._authority != null) {
                if (this._userinfo == null) {
                    stringBuffer.append(this._authority);
                } else if (this._host != null) {
                    stringBuffer.append(this._host);
                    if (this._port != -1) {
                        stringBuffer.append(':');
                        stringBuffer.append(this._port);
                    }
                }
            }
        }
        if (this._opaque != null && this._is_opaque_part) {
            stringBuffer.append(this._opaque);
        } else if (!(this._path == null || this._path.length == 0)) {
            stringBuffer.append(this._path);
        }
        if (this._query != null) {
            stringBuffer.append('?');
            stringBuffer.append(this._query);
        }
        this._uri = stringBuffer.toString().toCharArray();
        this.hash = 0;
    }

    public void setUser(String str) {
        setRawUser(encode(str, allowed_within_userinfo, getProtocolCharset()));
    }

    public void setUserinfo(String str, String str2) {
        String protocolCharset = getProtocolCharset();
        setRawUserinfo(encode(str, within_userinfo, protocolCharset), str2 == null ? null : encode(str2, within_userinfo, protocolCharset));
    }
}
