package org.apache.commons.httpclient.auth;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.util.ParameterParser;

public final class AuthChallengeParser {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.util.ParameterParser.parse(java.lang.String, char):java.util.List
     arg types: [java.lang.String, int]
     candidates:
      org.apache.commons.httpclient.util.ParameterParser.parse(char[], char):java.util.List
      org.apache.commons.httpclient.util.ParameterParser.parse(java.lang.String, char):java.util.List */
    public static Map extractParams(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Challenge may not be null");
        }
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new MalformedChallengeException(new StringBuffer("Invalid challenge: ").append(str).toString());
        }
        HashMap hashMap = new HashMap();
        List parse = new ParameterParser().parse(str.substring(indexOf + 1, str.length()), ',');
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= parse.size()) {
                return hashMap;
            }
            NameValuePair nameValuePair = (NameValuePair) parse.get(i2);
            hashMap.put(nameValuePair.getName().toLowerCase(), nameValuePair.getValue());
            i = i2 + 1;
        }
    }

    public static String extractScheme(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Challenge may not be null");
        }
        int indexOf = str.indexOf(32);
        String substring = indexOf == -1 ? str : str.substring(0, indexOf);
        if (!substring.equals(PoiTypeDef.All)) {
            return substring.toLowerCase();
        }
        throw new MalformedChallengeException(new StringBuffer("Invalid challenge: ").append(str).toString());
    }

    public static Map parseChallenges(Header[] headerArr) {
        if (headerArr == null) {
            throw new IllegalArgumentException("Array of challenges may not be null");
        }
        HashMap hashMap = new HashMap(headerArr.length);
        for (Header value : headerArr) {
            String value2 = value.getValue();
            hashMap.put(extractScheme(value2), value2);
        }
        return hashMap;
    }
}
