package org.apache.commons.httpclient;

import java.io.OutputStream;
import org.apache.commons.httpclient.util.EncodingUtil;

public class ChunkedOutputStream extends OutputStream {
    private static final byte[] CRLF;
    private static final byte[] ENDCHUNK;
    private static final byte[] ZERO = {48};
    private byte[] cache;
    private int cachePosition;
    private OutputStream stream;
    private boolean wroteLastChunk;

    static {
        byte[] bArr = {13, 10};
        CRLF = bArr;
        ENDCHUNK = bArr;
    }

    public ChunkedOutputStream(OutputStream outputStream) {
        this(outputStream, 2048);
    }

    public ChunkedOutputStream(OutputStream outputStream, int i) {
        this.stream = null;
        this.cachePosition = 0;
        this.wroteLastChunk = false;
        this.cache = new byte[i];
        this.stream = outputStream;
    }

    public void close() {
        finish();
        super.close();
    }

    public void finish() {
        if (!this.wroteLastChunk) {
            flushCache();
            writeClosingChunk();
            this.wroteLastChunk = true;
        }
    }

    public void flush() {
        this.stream.flush();
    }

    /* access modifiers changed from: protected */
    public void flushCache() {
        if (this.cachePosition > 0) {
            byte[] asciiBytes = EncodingUtil.getAsciiBytes(new StringBuffer().append(Integer.toHexString(this.cachePosition)).append("\r\n").toString());
            this.stream.write(asciiBytes, 0, asciiBytes.length);
            this.stream.write(this.cache, 0, this.cachePosition);
            this.stream.write(ENDCHUNK, 0, ENDCHUNK.length);
            this.cachePosition = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void flushCacheWithAppend(byte[] bArr, int i, int i2) {
        byte[] asciiBytes = EncodingUtil.getAsciiBytes(new StringBuffer().append(Integer.toHexString(this.cachePosition + i2)).append("\r\n").toString());
        this.stream.write(asciiBytes, 0, asciiBytes.length);
        this.stream.write(this.cache, 0, this.cachePosition);
        this.stream.write(bArr, i, i2);
        this.stream.write(ENDCHUNK, 0, ENDCHUNK.length);
        this.cachePosition = 0;
    }

    public void write(int i) {
        this.cache[this.cachePosition] = (byte) i;
        this.cachePosition++;
        if (this.cachePosition == this.cache.length) {
            flushCache();
        }
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (i2 >= this.cache.length - this.cachePosition) {
            flushCacheWithAppend(bArr, i, i2);
            return;
        }
        System.arraycopy(bArr, i, this.cache, this.cachePosition, i2);
        this.cachePosition += i2;
    }

    /* access modifiers changed from: protected */
    public void writeClosingChunk() {
        this.stream.write(ZERO, 0, ZERO.length);
        this.stream.write(CRLF, 0, CRLF.length);
        this.stream.write(ENDCHUNK, 0, ENDCHUNK.length);
    }
}
