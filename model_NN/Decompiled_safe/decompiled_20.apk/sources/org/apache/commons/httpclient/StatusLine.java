package org.apache.commons.httpclient;

import com.amap.api.search.poisearch.PoiTypeDef;

public class StatusLine {
    private final String httpVersion;
    private final String reasonPhrase;
    private final int statusCode;
    private final String statusLine;

    public StatusLine(String str) {
        int i = 0;
        int length = str.length();
        int i2 = 0;
        while (Character.isWhitespace(str.charAt(i))) {
            try {
                i2++;
                i++;
            } catch (NumberFormatException e) {
                throw new ProtocolException(new StringBuffer("Unable to parse status code from status line: '").append(str).append("'").toString());
            } catch (StringIndexOutOfBoundsException e2) {
                throw new HttpException(new StringBuffer("Status-Line '").append(str).append("' is not valid").toString());
            }
        }
        int i3 = i + 4;
        if (!"HTTP".equals(str.substring(i, i3))) {
            throw new HttpException(new StringBuffer("Status-Line '").append(str).append("' does not start with HTTP").toString());
        }
        int indexOf = str.indexOf(" ", i3);
        if (indexOf <= 0) {
            throw new ProtocolException(new StringBuffer("Unable to parse HTTP-Version from the status line: '").append(str).append("'").toString());
        }
        this.httpVersion = str.substring(i2, indexOf).toUpperCase();
        int i4 = indexOf;
        while (str.charAt(i4) == ' ') {
            i4++;
        }
        int indexOf2 = str.indexOf(" ", i4);
        indexOf2 = indexOf2 < 0 ? length : indexOf2;
        this.statusCode = Integer.parseInt(str.substring(i4, indexOf2));
        int i5 = indexOf2 + 1;
        if (i5 < length) {
            this.reasonPhrase = str.substring(i5).trim();
        } else {
            this.reasonPhrase = PoiTypeDef.All;
        }
        this.statusLine = str;
    }

    public static boolean startsWithHTTP(String str) {
        int i = 0;
        while (Character.isWhitespace(str.charAt(i))) {
            try {
                i++;
            } catch (StringIndexOutOfBoundsException e) {
                return false;
            }
        }
        return "HTTP".equals(str.substring(i, i + 4));
    }

    public final String getHttpVersion() {
        return this.httpVersion;
    }

    public final String getReasonPhrase() {
        return this.reasonPhrase;
    }

    public final int getStatusCode() {
        return this.statusCode;
    }

    public final String toString() {
        return this.statusLine;
    }
}
