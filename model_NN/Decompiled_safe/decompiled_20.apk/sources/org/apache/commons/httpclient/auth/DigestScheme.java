package org.apache.commons.httpclient.auth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClientError;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.httpclient.util.ParameterFormatter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DigestScheme extends RFC2617Scheme {
    private static final char[] HEXADECIMAL = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final Log LOG;
    private static final String NC = "00000001";
    private static final int QOP_AUTH = 2;
    private static final int QOP_AUTH_INT = 1;
    private static final int QOP_MISSING = 0;
    static Class class$org$apache$commons$httpclient$auth$DigestScheme;
    private String cnonce;
    private boolean complete;
    private final ParameterFormatter formatter;
    private int qopVariant;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$auth$DigestScheme == null) {
            cls = class$("org.apache.commons.httpclient.auth.DigestScheme");
            class$org$apache$commons$httpclient$auth$DigestScheme = cls;
        } else {
            cls = class$org$apache$commons$httpclient$auth$DigestScheme;
        }
        LOG = LogFactory.getLog(cls);
    }

    public DigestScheme() {
        this.qopVariant = 0;
        this.complete = false;
        this.formatter = new ParameterFormatter();
    }

    public DigestScheme(String str) {
        this();
        processChallenge(str);
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public static String createCnonce() {
        LOG.trace("enter DigestScheme.createCnonce()");
        try {
            return encode(MessageDigest.getInstance("MD5").digest(EncodingUtil.getAsciiBytes(Long.toString(System.currentTimeMillis()))));
        } catch (NoSuchAlgorithmException e) {
            throw new HttpClientError("Unsupported algorithm in HTTP Digest authentication: MD5");
        }
    }

    private String createDigest(String str, String str2) {
        String str3;
        String stringBuffer;
        LOG.trace("enter DigestScheme.createDigest(String, String, Map)");
        String parameter = getParameter("uri");
        String parameter2 = getParameter("realm");
        String parameter3 = getParameter("nonce");
        String parameter4 = getParameter("qop");
        String parameter5 = getParameter("methodname");
        String parameter6 = getParameter("algorithm");
        if (parameter6 == null) {
            parameter6 = "MD5";
        }
        String parameter7 = getParameter("charset");
        if (parameter7 == null) {
            parameter7 = "ISO-8859-1";
        }
        if (this.qopVariant == 1) {
            LOG.warn("qop=auth-int is not supported");
            throw new AuthenticationException("Unsupported qop in HTTP Digest authentication");
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            StringBuffer stringBuffer2 = new StringBuffer(str.length() + parameter2.length() + str2.length() + 2);
            stringBuffer2.append(str);
            stringBuffer2.append(':');
            stringBuffer2.append(parameter2);
            stringBuffer2.append(':');
            stringBuffer2.append(str2);
            String stringBuffer3 = stringBuffer2.toString();
            if (parameter6.equals("MD5-sess")) {
                String encode = encode(instance.digest(EncodingUtil.getBytes(stringBuffer3, parameter7)));
                StringBuffer stringBuffer4 = new StringBuffer(encode.length() + parameter3.length() + this.cnonce.length() + 2);
                stringBuffer4.append(encode);
                stringBuffer4.append(':');
                stringBuffer4.append(parameter3);
                stringBuffer4.append(':');
                stringBuffer4.append(this.cnonce);
                str3 = stringBuffer4.toString();
            } else {
                if (!parameter6.equals("MD5")) {
                    LOG.warn(new StringBuffer("Unhandled algorithm ").append(parameter6).append(" requested").toString());
                }
                str3 = stringBuffer3;
            }
            String encode2 = encode(instance.digest(EncodingUtil.getBytes(str3, parameter7)));
            String str4 = null;
            if (this.qopVariant == 1) {
                LOG.error("Unhandled qop auth-int");
            } else {
                str4 = new StringBuffer().append(parameter5).append(":").append(parameter).toString();
            }
            String encode3 = encode(instance.digest(EncodingUtil.getAsciiBytes(str4)));
            if (this.qopVariant == 0) {
                LOG.debug("Using null qop method");
                StringBuffer stringBuffer5 = new StringBuffer(encode2.length() + parameter3.length() + encode3.length());
                stringBuffer5.append(encode2);
                stringBuffer5.append(':');
                stringBuffer5.append(parameter3);
                stringBuffer5.append(':');
                stringBuffer5.append(encode3);
                stringBuffer = stringBuffer5.toString();
            } else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(new StringBuffer("Using qop method ").append(parameter4).toString());
                }
                String qopVariantString = getQopVariantString();
                StringBuffer stringBuffer6 = new StringBuffer(encode2.length() + parameter3.length() + 8 + this.cnonce.length() + qopVariantString.length() + encode3.length() + 5);
                stringBuffer6.append(encode2);
                stringBuffer6.append(':');
                stringBuffer6.append(parameter3);
                stringBuffer6.append(':');
                stringBuffer6.append(NC);
                stringBuffer6.append(':');
                stringBuffer6.append(this.cnonce);
                stringBuffer6.append(':');
                stringBuffer6.append(qopVariantString);
                stringBuffer6.append(':');
                stringBuffer6.append(encode3);
                stringBuffer = stringBuffer6.toString();
            }
            return encode(instance.digest(EncodingUtil.getAsciiBytes(stringBuffer)));
        } catch (Exception e) {
            throw new AuthenticationException("Unsupported algorithm in HTTP Digest authentication: MD5");
        }
    }

    private String createDigestHeader(String str, String str2) {
        LOG.trace("enter DigestScheme.createDigestHeader(String, Map, String)");
        String parameter = getParameter("uri");
        String parameter2 = getParameter("realm");
        String parameter3 = getParameter("nonce");
        String parameter4 = getParameter("opaque");
        String parameter5 = getParameter("algorithm");
        ArrayList arrayList = new ArrayList(20);
        arrayList.add(new NameValuePair("username", str));
        arrayList.add(new NameValuePair("realm", parameter2));
        arrayList.add(new NameValuePair("nonce", parameter3));
        arrayList.add(new NameValuePair("uri", parameter));
        arrayList.add(new NameValuePair("response", str2));
        if (this.qopVariant != 0) {
            arrayList.add(new NameValuePair("qop", getQopVariantString()));
            arrayList.add(new NameValuePair("nc", NC));
            arrayList.add(new NameValuePair("cnonce", this.cnonce));
        }
        if (parameter5 != null) {
            arrayList.add(new NameValuePair("algorithm", parameter5));
        }
        if (parameter4 != null) {
            arrayList.add(new NameValuePair("opaque", parameter4));
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayList.size(); i++) {
            NameValuePair nameValuePair = (NameValuePair) arrayList.get(i);
            if (i > 0) {
                stringBuffer.append(", ");
            }
            this.formatter.setAlwaysUseQuotes(!("nc".equals(nameValuePair.getName()) || "qop".equals(nameValuePair.getName())));
            this.formatter.format(stringBuffer, nameValuePair);
        }
        return stringBuffer.toString();
    }

    private static String encode(byte[] bArr) {
        LOG.trace("enter DigestScheme.encode(byte[])");
        if (bArr.length != 16) {
            return null;
        }
        char[] cArr = new char[32];
        for (int i = 0; i < 16; i++) {
            cArr[i * 2] = HEXADECIMAL[(bArr[i] & 240) >> 4];
            cArr[(i * 2) + 1] = HEXADECIMAL[bArr[i] & 15];
        }
        return new String(cArr);
    }

    private String getQopVariantString() {
        return this.qopVariant == 1 ? "auth-int" : "auth";
    }

    public String authenticate(Credentials credentials, String str, String str2) {
        LOG.trace("enter DigestScheme.authenticate(Credentials, String, String)");
        try {
            UsernamePasswordCredentials usernamePasswordCredentials = (UsernamePasswordCredentials) credentials;
            getParameters().put("methodname", str);
            getParameters().put("uri", str2);
            return new StringBuffer("Digest ").append(createDigestHeader(usernamePasswordCredentials.getUserName(), createDigest(usernamePasswordCredentials.getUserName(), usernamePasswordCredentials.getPassword()))).toString();
        } catch (ClassCastException e) {
            throw new InvalidCredentialsException(new StringBuffer("Credentials cannot be used for digest authentication: ").append(credentials.getClass().getName()).toString());
        }
    }

    public String authenticate(Credentials credentials, HttpMethod httpMethod) {
        LOG.trace("enter DigestScheme.authenticate(Credentials, HttpMethod)");
        try {
            UsernamePasswordCredentials usernamePasswordCredentials = (UsernamePasswordCredentials) credentials;
            getParameters().put("methodname", httpMethod.getName());
            StringBuffer stringBuffer = new StringBuffer(httpMethod.getPath());
            String queryString = httpMethod.getQueryString();
            if (queryString != null) {
                if (queryString.indexOf("?") != 0) {
                    stringBuffer.append("?");
                }
                stringBuffer.append(httpMethod.getQueryString());
            }
            getParameters().put("uri", stringBuffer.toString());
            if (getParameter("charset") == null) {
                getParameters().put("charset", httpMethod.getParams().getCredentialCharset());
            }
            return new StringBuffer("Digest ").append(createDigestHeader(usernamePasswordCredentials.getUserName(), createDigest(usernamePasswordCredentials.getUserName(), usernamePasswordCredentials.getPassword()))).toString();
        } catch (ClassCastException e) {
            throw new InvalidCredentialsException(new StringBuffer("Credentials cannot be used for digest authentication: ").append(credentials.getClass().getName()).toString());
        }
    }

    public String getID() {
        String realm = getRealm();
        String parameter = getParameter("nonce");
        return parameter != null ? new StringBuffer().append(realm).append("-").append(parameter).toString() : realm;
    }

    public String getSchemeName() {
        return "digest";
    }

    public boolean isComplete() {
        if ("true".equalsIgnoreCase(getParameter("stale"))) {
            return false;
        }
        return this.complete;
    }

    public boolean isConnectionBased() {
        return false;
    }

    public void processChallenge(String str) {
        super.processChallenge(str);
        if (getParameter("realm") == null) {
            throw new MalformedChallengeException("missing realm in challange");
        } else if (getParameter("nonce") == null) {
            throw new MalformedChallengeException("missing nonce in challange");
        } else {
            boolean z = false;
            String parameter = getParameter("qop");
            if (parameter != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(parameter, ",");
                while (true) {
                    if (!stringTokenizer.hasMoreTokens()) {
                        break;
                    }
                    String trim = stringTokenizer.nextToken().trim();
                    if (trim.equals("auth")) {
                        this.qopVariant = 2;
                        break;
                    } else if (trim.equals("auth-int")) {
                        this.qopVariant = 1;
                    } else {
                        LOG.warn(new StringBuffer("Unsupported qop detected: ").append(trim).toString());
                        z = true;
                    }
                }
            }
            if (!z || this.qopVariant != 0) {
                this.cnonce = createCnonce();
                this.complete = true;
                return;
            }
            throw new MalformedChallengeException("None of the qop methods is supported");
        }
    }
}
