package org.apache.commons.httpclient.methods.multipart;

import java.io.InputStream;

public interface PartSource {
    InputStream createInputStream();

    String getFileName();

    long getLength();
}
