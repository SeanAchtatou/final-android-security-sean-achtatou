package org.apache.commons.httpclient;

import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.common.b.e;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Hashtable;
import java.util.Locale;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.util.EncodingUtil;

public class URI implements Serializable, Cloneable, Comparable {
    protected static final BitSet IPv4address;
    protected static final BitSet IPv6address;
    protected static final BitSet IPv6reference;
    protected static final BitSet URI_reference;
    protected static final BitSet abs_path;
    protected static final BitSet absoluteURI;
    public static final BitSet allowed_IPv6reference;
    public static final BitSet allowed_abs_path;
    public static final BitSet allowed_authority;
    public static final BitSet allowed_fragment;
    public static final BitSet allowed_host;
    public static final BitSet allowed_opaque_part;
    public static final BitSet allowed_query;
    public static final BitSet allowed_reg_name;
    public static final BitSet allowed_rel_path;
    public static final BitSet allowed_userinfo;
    public static final BitSet allowed_within_authority;
    public static final BitSet allowed_within_path;
    public static final BitSet allowed_within_query;
    public static final BitSet allowed_within_userinfo;
    protected static final BitSet alpha = new BitSet(256);
    protected static final BitSet alphanum;
    protected static final BitSet authority;
    public static final BitSet control = new BitSet(256);
    protected static String defaultDocumentCharset = null;
    protected static String defaultDocumentCharsetByLocale = null;
    protected static String defaultDocumentCharsetByPlatform = null;
    protected static String defaultProtocolCharset = e.f;
    public static final BitSet delims;
    protected static final BitSet digit = new BitSet(256);
    public static final BitSet disallowed_opaque_part;
    public static final BitSet disallowed_rel_path;
    protected static final BitSet domainlabel = toplabel;
    protected static final BitSet escaped;
    protected static final BitSet fragment = uric;
    protected static final BitSet hex;
    protected static final BitSet hier_part;
    protected static final BitSet host;
    protected static final BitSet hostname;
    protected static final BitSet hostport;
    protected static final BitSet mark;
    protected static final BitSet net_path;
    protected static final BitSet opaque_part;
    protected static final BitSet param = pchar;
    protected static final BitSet path;
    protected static final BitSet path_segments;
    protected static final BitSet pchar;
    protected static final BitSet percent;
    protected static final BitSet port = digit;
    protected static final BitSet query = uric;
    protected static final BitSet reg_name;
    protected static final BitSet rel_path;
    protected static final BitSet rel_segment;
    protected static final BitSet relativeURI;
    protected static final BitSet reserved;
    protected static final char[] rootPath = {'/'};
    protected static final BitSet scheme;
    protected static final BitSet segment;
    static final long serialVersionUID = 604752400577948726L;
    protected static final BitSet server;
    public static final BitSet space;
    protected static final BitSet toplabel;
    protected static final BitSet unreserved;
    public static final BitSet unwise;
    protected static final BitSet uric;
    protected static final BitSet uric_no_slash;
    protected static final BitSet userinfo;
    public static final BitSet within_userinfo;
    protected char[] _authority;
    protected char[] _fragment;
    protected char[] _host;
    protected boolean _is_IPv4address;
    protected boolean _is_IPv6reference;
    protected boolean _is_abs_path;
    protected boolean _is_hier_part;
    protected boolean _is_hostname;
    protected boolean _is_net_path;
    protected boolean _is_opaque_part;
    protected boolean _is_reg_name;
    protected boolean _is_rel_path;
    protected boolean _is_server;
    protected char[] _opaque;
    protected char[] _path;
    protected int _port;
    protected char[] _query;
    protected char[] _scheme;
    protected char[] _uri;
    protected char[] _userinfo;
    protected int hash;
    protected String protocolCharset;

    public class DefaultCharsetChanged extends RuntimeException {
        public static final int DOCUMENT_CHARSET = 2;
        public static final int PROTOCOL_CHARSET = 1;
        public static final int UNKNOWN = 0;
        private String reason;
        private int reasonCode;

        public DefaultCharsetChanged(int i, String str) {
            super(str);
            this.reason = str;
            this.reasonCode = i;
        }

        public String getReason() {
            return this.reason;
        }

        public int getReasonCode() {
            return this.reasonCode;
        }
    }

    public class LocaleToCharsetMap {
        private static final Hashtable LOCALE_TO_CHARSET_MAP;

        static {
            Hashtable hashtable = new Hashtable();
            LOCALE_TO_CHARSET_MAP = hashtable;
            hashtable.put("ar", "ISO-8859-6");
            LOCALE_TO_CHARSET_MAP.put("be", "ISO-8859-5");
            LOCALE_TO_CHARSET_MAP.put("bg", "ISO-8859-5");
            LOCALE_TO_CHARSET_MAP.put("ca", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("cs", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("da", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("de", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("el", "ISO-8859-7");
            LOCALE_TO_CHARSET_MAP.put("en", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("es", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("et", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("fi", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("fr", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("hr", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("hu", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("is", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("it", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("iw", "ISO-8859-8");
            LOCALE_TO_CHARSET_MAP.put("ja", "Shift_JIS");
            LOCALE_TO_CHARSET_MAP.put("ko", "EUC-KR");
            LOCALE_TO_CHARSET_MAP.put("lt", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("lv", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("mk", "ISO-8859-5");
            LOCALE_TO_CHARSET_MAP.put("nl", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("no", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("pl", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("pt", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("ro", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("ru", "ISO-8859-5");
            LOCALE_TO_CHARSET_MAP.put("sh", "ISO-8859-5");
            LOCALE_TO_CHARSET_MAP.put("sk", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("sl", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("sq", "ISO-8859-2");
            LOCALE_TO_CHARSET_MAP.put("sr", "ISO-8859-5");
            LOCALE_TO_CHARSET_MAP.put("sv", "ISO-8859-1");
            LOCALE_TO_CHARSET_MAP.put("tr", "ISO-8859-9");
            LOCALE_TO_CHARSET_MAP.put("uk", "ISO-8859-5");
            LOCALE_TO_CHARSET_MAP.put("zh", "GB2312");
            LOCALE_TO_CHARSET_MAP.put("zh_TW", "Big5");
        }

        public static String getCharset(Locale locale) {
            String str = (String) LOCALE_TO_CHARSET_MAP.get(locale.toString());
            return str != null ? str : (String) LOCALE_TO_CHARSET_MAP.get(locale.getLanguage());
        }
    }

    static {
        defaultDocumentCharset = null;
        defaultDocumentCharsetByLocale = null;
        defaultDocumentCharsetByPlatform = null;
        Locale locale = Locale.getDefault();
        if (locale != null) {
            String charset = LocaleToCharsetMap.getCharset(locale);
            defaultDocumentCharsetByLocale = charset;
            defaultDocumentCharset = charset;
        }
        try {
            defaultDocumentCharsetByPlatform = System.getProperty("file.encoding");
        } catch (SecurityException e) {
        }
        if (defaultDocumentCharset == null) {
            defaultDocumentCharset = defaultDocumentCharsetByPlatform;
        }
        BitSet bitSet = new BitSet(256);
        percent = bitSet;
        bitSet.set(37);
        for (int i = 48; i <= 57; i++) {
            digit.set(i);
        }
        for (int i2 = 97; i2 <= 122; i2++) {
            alpha.set(i2);
        }
        for (int i3 = 65; i3 <= 90; i3++) {
            alpha.set(i3);
        }
        BitSet bitSet2 = new BitSet(256);
        alphanum = bitSet2;
        bitSet2.or(alpha);
        alphanum.or(digit);
        BitSet bitSet3 = new BitSet(256);
        hex = bitSet3;
        bitSet3.or(digit);
        for (int i4 = 97; i4 <= 102; i4++) {
            hex.set(i4);
        }
        for (int i5 = 65; i5 <= 70; i5++) {
            hex.set(i5);
        }
        BitSet bitSet4 = new BitSet(256);
        escaped = bitSet4;
        bitSet4.or(percent);
        escaped.or(hex);
        BitSet bitSet5 = new BitSet(256);
        mark = bitSet5;
        bitSet5.set(45);
        mark.set(95);
        mark.set(46);
        mark.set(33);
        mark.set(126);
        mark.set(42);
        mark.set(39);
        mark.set(40);
        mark.set(41);
        BitSet bitSet6 = new BitSet(256);
        unreserved = bitSet6;
        bitSet6.or(alphanum);
        unreserved.or(mark);
        BitSet bitSet7 = new BitSet(256);
        reserved = bitSet7;
        bitSet7.set(59);
        reserved.set(47);
        reserved.set(63);
        reserved.set(58);
        reserved.set(64);
        reserved.set(38);
        reserved.set(61);
        reserved.set(43);
        reserved.set(36);
        reserved.set(44);
        BitSet bitSet8 = new BitSet(256);
        uric = bitSet8;
        bitSet8.or(reserved);
        uric.or(unreserved);
        uric.or(escaped);
        BitSet bitSet9 = new BitSet(256);
        pchar = bitSet9;
        bitSet9.or(unreserved);
        pchar.or(escaped);
        pchar.set(58);
        pchar.set(64);
        pchar.set(38);
        pchar.set(61);
        pchar.set(43);
        pchar.set(36);
        pchar.set(44);
        BitSet bitSet10 = new BitSet(256);
        segment = bitSet10;
        bitSet10.or(pchar);
        segment.set(59);
        segment.or(param);
        BitSet bitSet11 = new BitSet(256);
        path_segments = bitSet11;
        bitSet11.set(47);
        path_segments.or(segment);
        BitSet bitSet12 = new BitSet(256);
        abs_path = bitSet12;
        bitSet12.set(47);
        abs_path.or(path_segments);
        BitSet bitSet13 = new BitSet(256);
        uric_no_slash = bitSet13;
        bitSet13.or(unreserved);
        uric_no_slash.or(escaped);
        uric_no_slash.set(59);
        uric_no_slash.set(63);
        uric_no_slash.set(59);
        uric_no_slash.set(64);
        uric_no_slash.set(38);
        uric_no_slash.set(61);
        uric_no_slash.set(43);
        uric_no_slash.set(36);
        uric_no_slash.set(44);
        BitSet bitSet14 = new BitSet(256);
        opaque_part = bitSet14;
        bitSet14.or(uric_no_slash);
        opaque_part.or(uric);
        BitSet bitSet15 = new BitSet(256);
        path = bitSet15;
        bitSet15.or(abs_path);
        path.or(opaque_part);
        BitSet bitSet16 = new BitSet(256);
        IPv4address = bitSet16;
        bitSet16.or(digit);
        IPv4address.set(46);
        BitSet bitSet17 = new BitSet(256);
        IPv6address = bitSet17;
        bitSet17.or(hex);
        IPv6address.set(58);
        IPv6address.or(IPv4address);
        BitSet bitSet18 = new BitSet(256);
        IPv6reference = bitSet18;
        bitSet18.set(91);
        IPv6reference.or(IPv6address);
        IPv6reference.set(93);
        BitSet bitSet19 = new BitSet(256);
        toplabel = bitSet19;
        bitSet19.or(alphanum);
        toplabel.set(45);
        BitSet bitSet20 = new BitSet(256);
        hostname = bitSet20;
        bitSet20.or(toplabel);
        hostname.set(46);
        BitSet bitSet21 = new BitSet(256);
        host = bitSet21;
        bitSet21.or(hostname);
        host.or(IPv6reference);
        BitSet bitSet22 = new BitSet(256);
        hostport = bitSet22;
        bitSet22.or(host);
        hostport.set(58);
        hostport.or(port);
        BitSet bitSet23 = new BitSet(256);
        userinfo = bitSet23;
        bitSet23.or(unreserved);
        userinfo.or(escaped);
        userinfo.set(59);
        userinfo.set(58);
        userinfo.set(38);
        userinfo.set(61);
        userinfo.set(43);
        userinfo.set(36);
        userinfo.set(44);
        BitSet bitSet24 = new BitSet(256);
        within_userinfo = bitSet24;
        bitSet24.or(userinfo);
        within_userinfo.clear(59);
        within_userinfo.clear(58);
        within_userinfo.clear(64);
        within_userinfo.clear(63);
        within_userinfo.clear(47);
        BitSet bitSet25 = new BitSet(256);
        server = bitSet25;
        bitSet25.or(userinfo);
        server.set(64);
        server.or(hostport);
        BitSet bitSet26 = new BitSet(256);
        reg_name = bitSet26;
        bitSet26.or(unreserved);
        reg_name.or(escaped);
        reg_name.set(36);
        reg_name.set(44);
        reg_name.set(59);
        reg_name.set(58);
        reg_name.set(64);
        reg_name.set(38);
        reg_name.set(61);
        reg_name.set(43);
        BitSet bitSet27 = new BitSet(256);
        authority = bitSet27;
        bitSet27.or(server);
        authority.or(reg_name);
        BitSet bitSet28 = new BitSet(256);
        scheme = bitSet28;
        bitSet28.or(alpha);
        scheme.or(digit);
        scheme.set(43);
        scheme.set(45);
        scheme.set(46);
        BitSet bitSet29 = new BitSet(256);
        rel_segment = bitSet29;
        bitSet29.or(unreserved);
        rel_segment.or(escaped);
        rel_segment.set(59);
        rel_segment.set(64);
        rel_segment.set(38);
        rel_segment.set(61);
        rel_segment.set(43);
        rel_segment.set(36);
        rel_segment.set(44);
        BitSet bitSet30 = new BitSet(256);
        rel_path = bitSet30;
        bitSet30.or(rel_segment);
        rel_path.or(abs_path);
        BitSet bitSet31 = new BitSet(256);
        net_path = bitSet31;
        bitSet31.set(47);
        net_path.or(authority);
        net_path.or(abs_path);
        BitSet bitSet32 = new BitSet(256);
        hier_part = bitSet32;
        bitSet32.or(net_path);
        hier_part.or(abs_path);
        hier_part.or(query);
        BitSet bitSet33 = new BitSet(256);
        relativeURI = bitSet33;
        bitSet33.or(net_path);
        relativeURI.or(abs_path);
        relativeURI.or(rel_path);
        relativeURI.or(query);
        BitSet bitSet34 = new BitSet(256);
        absoluteURI = bitSet34;
        bitSet34.or(scheme);
        absoluteURI.set(58);
        absoluteURI.or(hier_part);
        absoluteURI.or(opaque_part);
        BitSet bitSet35 = new BitSet(256);
        URI_reference = bitSet35;
        bitSet35.or(absoluteURI);
        URI_reference.or(relativeURI);
        URI_reference.set(35);
        URI_reference.or(fragment);
        for (int i6 = 0; i6 <= 31; i6++) {
            control.set(i6);
        }
        control.set(127);
        BitSet bitSet36 = new BitSet(256);
        space = bitSet36;
        bitSet36.set(32);
        BitSet bitSet37 = new BitSet(256);
        delims = bitSet37;
        bitSet37.set(60);
        delims.set(62);
        delims.set(35);
        delims.set(37);
        delims.set(34);
        BitSet bitSet38 = new BitSet(256);
        unwise = bitSet38;
        bitSet38.set(123);
        unwise.set(125);
        unwise.set(124);
        unwise.set(92);
        unwise.set(94);
        unwise.set(91);
        unwise.set(93);
        unwise.set(96);
        BitSet bitSet39 = new BitSet(256);
        disallowed_rel_path = bitSet39;
        bitSet39.or(uric);
        disallowed_rel_path.andNot(rel_path);
        BitSet bitSet40 = new BitSet(256);
        disallowed_opaque_part = bitSet40;
        bitSet40.or(uric);
        disallowed_opaque_part.andNot(opaque_part);
        BitSet bitSet41 = new BitSet(256);
        allowed_authority = bitSet41;
        bitSet41.or(authority);
        allowed_authority.clear(37);
        BitSet bitSet42 = new BitSet(256);
        allowed_opaque_part = bitSet42;
        bitSet42.or(opaque_part);
        allowed_opaque_part.clear(37);
        BitSet bitSet43 = new BitSet(256);
        allowed_reg_name = bitSet43;
        bitSet43.or(reg_name);
        allowed_reg_name.clear(37);
        BitSet bitSet44 = new BitSet(256);
        allowed_userinfo = bitSet44;
        bitSet44.or(userinfo);
        allowed_userinfo.clear(37);
        BitSet bitSet45 = new BitSet(256);
        allowed_within_userinfo = bitSet45;
        bitSet45.or(within_userinfo);
        allowed_within_userinfo.clear(37);
        BitSet bitSet46 = new BitSet(256);
        allowed_IPv6reference = bitSet46;
        bitSet46.or(IPv6reference);
        allowed_IPv6reference.clear(91);
        allowed_IPv6reference.clear(93);
        BitSet bitSet47 = new BitSet(256);
        allowed_host = bitSet47;
        bitSet47.or(hostname);
        allowed_host.or(allowed_IPv6reference);
        BitSet bitSet48 = new BitSet(256);
        allowed_within_authority = bitSet48;
        bitSet48.or(server);
        allowed_within_authority.or(reg_name);
        allowed_within_authority.clear(59);
        allowed_within_authority.clear(58);
        allowed_within_authority.clear(64);
        allowed_within_authority.clear(63);
        allowed_within_authority.clear(47);
        BitSet bitSet49 = new BitSet(256);
        allowed_abs_path = bitSet49;
        bitSet49.or(abs_path);
        allowed_abs_path.andNot(percent);
        allowed_abs_path.clear(43);
        BitSet bitSet50 = new BitSet(256);
        allowed_rel_path = bitSet50;
        bitSet50.or(rel_path);
        allowed_rel_path.clear(37);
        allowed_rel_path.clear(43);
        BitSet bitSet51 = new BitSet(256);
        allowed_within_path = bitSet51;
        bitSet51.or(abs_path);
        allowed_within_path.clear(47);
        allowed_within_path.clear(59);
        allowed_within_path.clear(61);
        allowed_within_path.clear(63);
        BitSet bitSet52 = new BitSet(256);
        allowed_query = bitSet52;
        bitSet52.or(uric);
        allowed_query.clear(37);
        BitSet bitSet53 = new BitSet(256);
        allowed_within_query = bitSet53;
        bitSet53.or(allowed_query);
        allowed_within_query.andNot(reserved);
        BitSet bitSet54 = new BitSet(256);
        allowed_fragment = bitSet54;
        bitSet54.or(uric);
        allowed_fragment.clear(37);
    }

    protected URI() {
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
    }

    public URI(String str) {
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        parseUriReference(str, false);
    }

    public URI(String str, String str2) {
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        this.protocolCharset = str2;
        parseUriReference(str, false);
    }

    public URI(String str, String str2, String str3) {
        char[] cArr = null;
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        if (str == null) {
            throw new URIException(1, "scheme required");
        }
        char[] charArray = str.toLowerCase().toCharArray();
        if (validate(charArray, scheme)) {
            this._scheme = charArray;
            this._opaque = encode(str2, allowed_opaque_part, getProtocolCharset());
            this._is_opaque_part = true;
            this._fragment = str3 != null ? str3.toCharArray() : cArr;
            setURI();
            return;
        }
        throw new URIException(1, "incorrect scheme");
    }

    public URI(String str, String str2, String str3, int i) {
        this(str, str2, str3, i, null, null, null);
    }

    public URI(String str, String str2, String str3, int i, String str4) {
        this(str, str2, str3, i, str4, null, null);
    }

    public URI(String str, String str2, String str3, int i, String str4, String str5) {
        this(str, str2, str3, i, str4, str5, null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public URI(java.lang.String r7, java.lang.String r8, java.lang.String r9, int r10, java.lang.String r11, java.lang.String r12, java.lang.String r13) {
        /*
            r6 = this;
            if (r9 != 0) goto L_0x000c
            r2 = 0
        L_0x0003:
            r0 = r6
            r1 = r7
            r3 = r11
            r4 = r12
            r5 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x000c:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            if (r8 == 0) goto L_0x0049
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.StringBuffer r0 = r0.append(r8)
            r2 = 64
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x0026:
            java.lang.StringBuffer r0 = r1.append(r0)
            java.lang.StringBuffer r1 = r0.append(r9)
            r0 = -1
            if (r10 == r0) goto L_0x004c
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            java.lang.String r2 = ":"
            r0.<init>(r2)
            java.lang.StringBuffer r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
        L_0x0040:
            java.lang.StringBuffer r0 = r1.append(r0)
            java.lang.String r2 = r0.toString()
            goto L_0x0003
        L_0x0049:
            java.lang.String r0 = ""
            goto L_0x0026
        L_0x004c:
            java.lang.String r0 = ""
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.URI.<init>(java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String):void");
    }

    public URI(String str, String str2, String str3, String str4) {
        this(str, str2, str3, (String) null, str4);
    }

    public URI(String str, String str2, String str3, String str4, String str5) {
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        StringBuffer stringBuffer = new StringBuffer();
        if (str != null) {
            stringBuffer.append(str);
            stringBuffer.append(':');
        }
        if (str2 != null) {
            stringBuffer.append("//");
            stringBuffer.append(str2);
        }
        if (str3 != null) {
            if (!(str == null && str2 == null) && !str3.startsWith(CookieSpec.PATH_DELIM)) {
                throw new URIException(1, "abs_path requested");
            }
            stringBuffer.append(str3);
        }
        if (str4 != null) {
            stringBuffer.append('?');
            stringBuffer.append(str4);
        }
        if (str5 != null) {
            stringBuffer.append('#');
            stringBuffer.append(str5);
        }
        parseUriReference(stringBuffer.toString(), false);
    }

    public URI(String str, boolean z) {
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        parseUriReference(str, z);
    }

    public URI(String str, boolean z, String str2) {
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        this.protocolCharset = str2;
        parseUriReference(str, z);
    }

    public URI(URI uri, String str) {
        this(uri, new URI(str));
    }

    public URI(URI uri, String str, boolean z) {
        this(uri, new URI(str, z));
    }

    public URI(URI uri, URI uri2) {
        boolean z = false;
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        if (uri._scheme == null) {
            throw new URIException(1, "base URI required");
        }
        if (uri._scheme != null) {
            this._scheme = uri._scheme;
            this._authority = uri._authority;
            this._is_net_path = uri._is_net_path;
        }
        if (uri._is_opaque_part || uri2._is_opaque_part) {
            this._scheme = uri._scheme;
            this._is_opaque_part = (uri._is_opaque_part || uri2._is_opaque_part) ? true : z;
            this._opaque = uri2._opaque;
            this._fragment = uri2._fragment;
            setURI();
            return;
        }
        boolean equals = Arrays.equals(uri._scheme, uri2._scheme);
        if (uri2._scheme != null && (!equals || uri2._authority != null)) {
            this._scheme = uri2._scheme;
            this._is_net_path = uri2._is_net_path;
            this._authority = uri2._authority;
            if (uri2._is_server) {
                this._is_server = uri2._is_server;
                this._userinfo = uri2._userinfo;
                this._host = uri2._host;
                this._port = uri2._port;
            } else if (uri2._is_reg_name) {
                this._is_reg_name = uri2._is_reg_name;
            }
            this._is_abs_path = uri2._is_abs_path;
            this._is_rel_path = uri2._is_rel_path;
            this._path = uri2._path;
        } else if (uri._authority != null && uri2._scheme == null) {
            this._is_net_path = uri._is_net_path;
            this._authority = uri._authority;
            if (uri._is_server) {
                this._is_server = uri._is_server;
                this._userinfo = uri._userinfo;
                this._host = uri._host;
                this._port = uri._port;
            } else if (uri._is_reg_name) {
                this._is_reg_name = uri._is_reg_name;
            }
        }
        if (uri2._authority != null) {
            this._is_net_path = uri2._is_net_path;
            this._authority = uri2._authority;
            if (uri2._is_server) {
                this._is_server = uri2._is_server;
                this._userinfo = uri2._userinfo;
                this._host = uri2._host;
                this._port = uri2._port;
            } else if (uri2._is_reg_name) {
                this._is_reg_name = uri2._is_reg_name;
            }
            this._is_abs_path = uri2._is_abs_path;
            this._is_rel_path = uri2._is_rel_path;
            this._path = uri2._path;
        }
        if (uri2._authority == null && (uri2._scheme == null || equals)) {
            if ((uri2._path == null || uri2._path.length == 0) && uri2._query == null) {
                this._path = uri._path;
                this._query = uri._query;
            } else {
                this._path = resolvePath(uri._path, uri2._path);
            }
        }
        if (uri2._query != null) {
            this._query = uri2._query;
        }
        if (uri2._fragment != null) {
            this._fragment = uri2._fragment;
        }
        setURI();
        parseUriReference(new String(this._uri), true);
    }

    public URI(char[] cArr) {
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        parseUriReference(new String(cArr), true);
    }

    public URI(char[] cArr, String str) {
        this.hash = 0;
        this._uri = null;
        this.protocolCharset = null;
        this._scheme = null;
        this._opaque = null;
        this._authority = null;
        this._userinfo = null;
        this._host = null;
        this._port = -1;
        this._path = null;
        this._query = null;
        this._fragment = null;
        this.protocolCharset = str;
        parseUriReference(new String(cArr), true);
    }

    protected static String decode(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Component array of chars may not be null");
        }
        try {
            return EncodingUtil.getString(URLCodec.decodeUrl(EncodingUtil.getAsciiBytes(str)), str2);
        } catch (DecoderException e) {
            throw new URIException(e.getMessage());
        }
    }

    protected static String decode(char[] cArr, String str) {
        if (cArr != null) {
            return decode(new String(cArr), str);
        }
        throw new IllegalArgumentException("Component array of chars may not be null");
    }

    protected static char[] encode(String str, BitSet bitSet, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Original string may not be null");
        } else if (bitSet != null) {
            return EncodingUtil.getAsciiString(URLCodec.encodeUrl(bitSet, EncodingUtil.getBytes(str, str2))).toCharArray();
        } else {
            throw new IllegalArgumentException("Allowed bitset may not be null");
        }
    }

    public static String getDefaultDocumentCharset() {
        return defaultDocumentCharset;
    }

    public static String getDefaultDocumentCharsetByLocale() {
        return defaultDocumentCharsetByLocale;
    }

    public static String getDefaultDocumentCharsetByPlatform() {
        return defaultDocumentCharsetByPlatform;
    }

    public static String getDefaultProtocolCharset() {
        return defaultProtocolCharset;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
    }

    public static void setDefaultDocumentCharset(String str) {
        defaultDocumentCharset = str;
        throw new DefaultCharsetChanged(2, "the default document charset changed");
    }

    public static void setDefaultProtocolCharset(String str) {
        defaultProtocolCharset = str;
        throw new DefaultCharsetChanged(1, "the default protocol charset changed");
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
    }

    public synchronized Object clone() {
        URI uri;
        uri = (URI) super.clone();
        uri._uri = this._uri;
        uri._scheme = this._scheme;
        uri._opaque = this._opaque;
        uri._authority = this._authority;
        uri._userinfo = this._userinfo;
        uri._host = this._host;
        uri._port = this._port;
        uri._path = this._path;
        uri._query = this._query;
        uri._fragment = this._fragment;
        uri.protocolCharset = this.protocolCharset;
        uri._is_hier_part = this._is_hier_part;
        uri._is_opaque_part = this._is_opaque_part;
        uri._is_net_path = this._is_net_path;
        uri._is_abs_path = this._is_abs_path;
        uri._is_rel_path = this._is_rel_path;
        uri._is_reg_name = this._is_reg_name;
        uri._is_server = this._is_server;
        uri._is_hostname = this._is_hostname;
        uri._is_IPv4address = this._is_IPv4address;
        uri._is_IPv6reference = this._is_IPv6reference;
        return uri;
    }

    public int compareTo(Object obj) {
        URI uri = (URI) obj;
        if (!equals(this._authority, uri.getRawAuthority())) {
            return -1;
        }
        return toString().compareTo(uri.toString());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof URI)) {
            return false;
        }
        URI uri = (URI) obj;
        if (!equals(this._scheme, uri._scheme)) {
            return false;
        }
        if (!equals(this._opaque, uri._opaque)) {
            return false;
        }
        if (!equals(this._authority, uri._authority)) {
            return false;
        }
        if (!equals(this._path, uri._path)) {
            return false;
        }
        if (!equals(this._query, uri._query)) {
            return false;
        }
        return equals(this._fragment, uri._fragment);
    }

    /* access modifiers changed from: protected */
    public boolean equals(char[] cArr, char[] cArr2) {
        if (cArr == null && cArr2 == null) {
            return true;
        }
        if (cArr == null || cArr2 == null || cArr.length != cArr2.length) {
            return false;
        }
        for (int i = 0; i < cArr.length; i++) {
            if (cArr[i] != cArr2[i]) {
                return false;
            }
        }
        return true;
    }

    public String getAboveHierPath() {
        char[] rawAboveHierPath = getRawAboveHierPath();
        if (rawAboveHierPath == null) {
            return null;
        }
        return decode(rawAboveHierPath, getProtocolCharset());
    }

    public String getAuthority() {
        if (this._authority == null) {
            return null;
        }
        return decode(this._authority, getProtocolCharset());
    }

    public String getCurrentHierPath() {
        char[] rawCurrentHierPath = getRawCurrentHierPath();
        if (rawCurrentHierPath == null) {
            return null;
        }
        return decode(rawCurrentHierPath, getProtocolCharset());
    }

    public String getEscapedAboveHierPath() {
        char[] rawAboveHierPath = getRawAboveHierPath();
        if (rawAboveHierPath == null) {
            return null;
        }
        return new String(rawAboveHierPath);
    }

    public String getEscapedAuthority() {
        if (this._authority == null) {
            return null;
        }
        return new String(this._authority);
    }

    public String getEscapedCurrentHierPath() {
        char[] rawCurrentHierPath = getRawCurrentHierPath();
        if (rawCurrentHierPath == null) {
            return null;
        }
        return new String(rawCurrentHierPath);
    }

    public String getEscapedFragment() {
        if (this._fragment == null) {
            return null;
        }
        return new String(this._fragment);
    }

    public String getEscapedName() {
        char[] rawName = getRawName();
        if (rawName == null) {
            return null;
        }
        return new String(rawName);
    }

    public String getEscapedPath() {
        char[] rawPath = getRawPath();
        if (rawPath == null) {
            return null;
        }
        return new String(rawPath);
    }

    public String getEscapedPathQuery() {
        char[] rawPathQuery = getRawPathQuery();
        if (rawPathQuery == null) {
            return null;
        }
        return new String(rawPathQuery);
    }

    public String getEscapedQuery() {
        if (this._query == null) {
            return null;
        }
        return new String(this._query);
    }

    public String getEscapedURI() {
        if (this._uri == null) {
            return null;
        }
        return new String(this._uri);
    }

    public String getEscapedURIReference() {
        char[] rawURIReference = getRawURIReference();
        if (rawURIReference == null) {
            return null;
        }
        return new String(rawURIReference);
    }

    public String getEscapedUserinfo() {
        if (this._userinfo == null) {
            return null;
        }
        return new String(this._userinfo);
    }

    public String getFragment() {
        if (this._fragment == null) {
            return null;
        }
        return decode(this._fragment, getProtocolCharset());
    }

    public String getHost() {
        if (this._host != null) {
            return decode(this._host, getProtocolCharset());
        }
        return null;
    }

    public String getName() {
        if (getRawName() == null) {
            return null;
        }
        return decode(getRawName(), getProtocolCharset());
    }

    public String getPath() {
        char[] rawPath = getRawPath();
        if (rawPath == null) {
            return null;
        }
        return decode(rawPath, getProtocolCharset());
    }

    public String getPathQuery() {
        char[] rawPathQuery = getRawPathQuery();
        if (rawPathQuery == null) {
            return null;
        }
        return decode(rawPathQuery, getProtocolCharset());
    }

    public int getPort() {
        return this._port;
    }

    public String getProtocolCharset() {
        return this.protocolCharset != null ? this.protocolCharset : defaultProtocolCharset;
    }

    public String getQuery() {
        if (this._query == null) {
            return null;
        }
        return decode(this._query, getProtocolCharset());
    }

    public char[] getRawAboveHierPath() {
        char[] rawCurrentHierPath = getRawCurrentHierPath();
        if (rawCurrentHierPath == null) {
            return null;
        }
        return getRawCurrentHierPath(rawCurrentHierPath);
    }

    public char[] getRawAuthority() {
        return this._authority;
    }

    public char[] getRawCurrentHierPath() {
        if (this._path == null) {
            return null;
        }
        return getRawCurrentHierPath(this._path);
    }

    /* access modifiers changed from: protected */
    public char[] getRawCurrentHierPath(char[] cArr) {
        if (this._is_opaque_part) {
            throw new URIException(1, "no hierarchy level");
        } else if (cArr == null) {
            throw new URIException(1, "empty path");
        } else {
            String str = new String(cArr);
            int indexOf = str.indexOf(47);
            int lastIndexOf = str.lastIndexOf(47);
            return lastIndexOf == 0 ? rootPath : (indexOf == lastIndexOf || lastIndexOf == -1) ? cArr : str.substring(0, lastIndexOf).toCharArray();
        }
    }

    public char[] getRawFragment() {
        return this._fragment;
    }

    public char[] getRawHost() {
        return this._host;
    }

    public char[] getRawName() {
        int i;
        if (this._path == null) {
            return null;
        }
        int length = this._path.length - 1;
        while (true) {
            if (length < 0) {
                i = 0;
                break;
            } else if (this._path[length] == '/') {
                i = length + 1;
                break;
            } else {
                length--;
            }
        }
        int length2 = this._path.length - i;
        char[] cArr = new char[length2];
        System.arraycopy(this._path, i, cArr, 0, length2);
        return cArr;
    }

    public char[] getRawPath() {
        return this._is_opaque_part ? this._opaque : this._path;
    }

    public char[] getRawPathQuery() {
        if (this._path == null && this._query == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        if (this._path != null) {
            stringBuffer.append(this._path);
        }
        if (this._query != null) {
            stringBuffer.append('?');
            stringBuffer.append(this._query);
        }
        return stringBuffer.toString().toCharArray();
    }

    public char[] getRawQuery() {
        return this._query;
    }

    public char[] getRawScheme() {
        return this._scheme;
    }

    public char[] getRawURI() {
        return this._uri;
    }

    public char[] getRawURIReference() {
        return this._fragment == null ? this._uri : this._uri == null ? this._fragment : new StringBuffer().append(new String(this._uri)).append("#").append(new String(this._fragment)).toString().toCharArray();
    }

    public char[] getRawUserinfo() {
        return this._userinfo;
    }

    public String getScheme() {
        if (this._scheme == null) {
            return null;
        }
        return new String(this._scheme);
    }

    public String getURI() {
        if (this._uri == null) {
            return null;
        }
        return decode(this._uri, getProtocolCharset());
    }

    public String getURIReference() {
        char[] rawURIReference = getRawURIReference();
        if (rawURIReference == null) {
            return null;
        }
        return decode(rawURIReference, getProtocolCharset());
    }

    public String getUserinfo() {
        if (this._userinfo == null) {
            return null;
        }
        return decode(this._userinfo, getProtocolCharset());
    }

    public boolean hasAuthority() {
        return this._authority != null || this._is_net_path;
    }

    public boolean hasFragment() {
        return this._fragment != null;
    }

    public boolean hasQuery() {
        return this._query != null;
    }

    public boolean hasUserinfo() {
        return this._userinfo != null;
    }

    public int hashCode() {
        if (this.hash == 0) {
            char[] cArr = this._uri;
            if (cArr != null) {
                for (char c : cArr) {
                    this.hash = (this.hash * 31) + c;
                }
            }
            char[] cArr2 = this._fragment;
            if (cArr2 != null) {
                for (char c2 : cArr2) {
                    this.hash = (this.hash * 31) + c2;
                }
            }
        }
        return this.hash;
    }

    /* access modifiers changed from: protected */
    public int indexFirstOf(String str, String str2) {
        return indexFirstOf(str, str2, -1);
    }

    /* access modifiers changed from: protected */
    public int indexFirstOf(String str, String str2, int i) {
        if (str == null || str.length() == 0) {
            return -1;
        }
        if (str2 == null || str2.length() == 0) {
            return -1;
        }
        if (i < 0) {
            i = 0;
        } else if (i > str.length()) {
            return -1;
        }
        int length = str.length();
        char[] charArray = str2.toCharArray();
        for (char indexOf : charArray) {
            int indexOf2 = str.indexOf(indexOf, i);
            if (indexOf2 >= 0 && indexOf2 < length) {
                length = indexOf2;
            }
        }
        if (length == str.length()) {
            return -1;
        }
        return length;
    }

    /* access modifiers changed from: protected */
    public int indexFirstOf(char[] cArr, char c) {
        return indexFirstOf(cArr, c, 0);
    }

    /* access modifiers changed from: protected */
    public int indexFirstOf(char[] cArr, char c, int i) {
        if (cArr == null || cArr.length == 0) {
            return -1;
        }
        if (i < 0) {
            i = 0;
        } else if (i > cArr.length) {
            return -1;
        }
        while (i < cArr.length) {
            if (cArr[i] == c) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public boolean isAbsPath() {
        return this._is_abs_path;
    }

    public boolean isAbsoluteURI() {
        return this._scheme != null;
    }

    public boolean isHierPart() {
        return this._is_hier_part;
    }

    public boolean isHostname() {
        return this._is_hostname;
    }

    public boolean isIPv4address() {
        return this._is_IPv4address;
    }

    public boolean isIPv6reference() {
        return this._is_IPv6reference;
    }

    public boolean isNetPath() {
        return this._is_net_path || this._authority != null;
    }

    public boolean isOpaquePart() {
        return this._is_opaque_part;
    }

    public boolean isRegName() {
        return this._is_reg_name;
    }

    public boolean isRelPath() {
        return this._is_rel_path;
    }

    public boolean isRelativeURI() {
        return this._scheme == null;
    }

    public boolean isServer() {
        return this._is_server;
    }

    public void normalize() {
        if (isAbsPath()) {
            this._path = normalize(this._path);
            setURI();
        }
    }

    /* access modifiers changed from: protected */
    public char[] normalize(char[] cArr) {
        int lastIndexOf;
        if (cArr == null) {
            return null;
        }
        String str = new String(cArr);
        if (str.startsWith("./")) {
            str = str.substring(1);
        } else if (str.startsWith("../")) {
            str = str.substring(2);
        } else if (str.startsWith("..")) {
            str = str.substring(2);
        }
        while (true) {
            int indexOf = str.indexOf("/./");
            if (indexOf == -1) {
                break;
            }
            str = new StringBuffer().append(str.substring(0, indexOf)).append(str.substring(indexOf + 2)).toString();
        }
        if (str.endsWith("/.")) {
            str = str.substring(0, str.length() - 1);
        }
        String str2 = str;
        int i = 0;
        while (true) {
            int indexOf2 = str2.indexOf("/../", i);
            if (indexOf2 == -1) {
                break;
            }
            int lastIndexOf2 = str2.lastIndexOf(47, indexOf2 - 1);
            if (lastIndexOf2 >= 0) {
                str2 = new StringBuffer().append(str2.substring(0, lastIndexOf2)).append(str2.substring(indexOf2 + 3)).toString();
            } else {
                i = indexOf2 + 3;
            }
        }
        if (str2.endsWith("/..") && (lastIndexOf = str2.lastIndexOf(47, str2.length() - 4)) >= 0) {
            str2 = str2.substring(0, lastIndexOf + 1);
        }
        while (true) {
            int indexOf3 = str2.indexOf("/../");
            if (indexOf3 != -1 && str2.lastIndexOf(47, indexOf3 - 1) < 0) {
                str2 = str2.substring(indexOf3 + 3);
            }
        }
        if (str2.endsWith("/..") && str2.lastIndexOf(47, str2.length() - 4) < 0) {
            str2 = CookieSpec.PATH_DELIM;
        }
        return str2.toCharArray();
    }

    /* access modifiers changed from: protected */
    public void parseAuthority(String str, boolean z) {
        int i;
        int indexOf;
        boolean z2;
        this._is_IPv6reference = false;
        this._is_IPv4address = false;
        this._is_hostname = false;
        this._is_server = false;
        this._is_reg_name = false;
        String protocolCharset2 = getProtocolCharset();
        int indexOf2 = str.indexOf(64);
        if (indexOf2 != -1) {
            this._userinfo = z ? str.substring(0, indexOf2).toCharArray() : encode(str.substring(0, indexOf2), allowed_userinfo, protocolCharset2);
            i = indexOf2 + 1;
        } else {
            i = 0;
        }
        if (str.indexOf(91, i) >= i) {
            int indexOf3 = str.indexOf(93, i);
            if (indexOf3 == -1) {
                throw new URIException(1, "IPv6reference");
            }
            indexOf = indexOf3 + 1;
            this._host = z ? str.substring(i, indexOf).toCharArray() : encode(str.substring(i, indexOf), allowed_IPv6reference, protocolCharset2);
            this._is_IPv6reference = true;
            z2 = true;
        } else {
            indexOf = str.indexOf(58, i);
            if (indexOf == -1) {
                indexOf = str.length();
                z2 = false;
            } else {
                z2 = true;
            }
            this._host = str.substring(i, indexOf).toCharArray();
            if (validate(this._host, IPv4address)) {
                this._is_IPv4address = true;
            } else if (validate(this._host, hostname)) {
                this._is_hostname = true;
            } else {
                this._is_reg_name = true;
            }
        }
        if (this._is_reg_name) {
            this._is_IPv6reference = false;
            this._is_IPv4address = false;
            this._is_hostname = false;
            this._is_server = false;
            if (z) {
                this._authority = str.toCharArray();
                if (!validate(this._authority, reg_name)) {
                    throw new URIException("Invalid authority");
                }
                return;
            }
            this._authority = encode(str, allowed_reg_name, protocolCharset2);
            return;
        }
        if (str.length() - 1 > indexOf && z2 && str.charAt(indexOf) == ':') {
            try {
                this._port = Integer.parseInt(str.substring(indexOf + 1));
            } catch (NumberFormatException e) {
                throw new URIException(1, "invalid port number");
            }
        }
        StringBuffer stringBuffer = new StringBuffer();
        if (this._userinfo != null) {
            stringBuffer.append(this._userinfo);
            stringBuffer.append('@');
        }
        if (this._host != null) {
            stringBuffer.append(this._host);
            if (this._port != -1) {
                stringBuffer.append(':');
                stringBuffer.append(this._port);
            }
        }
        this._authority = stringBuffer.toString().toCharArray();
        this._is_server = true;
    }

    /* access modifiers changed from: protected */
    public void parseUriReference(String str, boolean z) {
        int i;
        int i2;
        int i3;
        char[] encode;
        URI uri;
        if (str == null) {
            throw new URIException("URI-Reference required");
        }
        String trim = str.trim();
        int length = trim.length();
        if (length > 0) {
            if (validate(new char[]{trim.charAt(0)}, delims) && length >= 2) {
                if (validate(new char[]{trim.charAt(length - 1)}, delims)) {
                    trim = trim.substring(1, length - 1);
                    length -= 2;
                }
            }
        }
        int indexOf = trim.indexOf(58);
        int indexOf2 = trim.indexOf(47);
        boolean z2 = (indexOf <= 0 && !trim.startsWith("//")) || (indexOf2 >= 0 && indexOf2 < indexOf);
        int indexFirstOf = indexFirstOf(trim, z2 ? "/?#" : ":/?#", 0);
        if (indexFirstOf == -1) {
            indexFirstOf = 0;
        }
        if (indexFirstOf <= 0 || indexFirstOf >= length || trim.charAt(indexFirstOf) != ':') {
            i = 0;
        } else {
            char[] charArray = trim.substring(0, indexFirstOf).toLowerCase().toCharArray();
            if (validate(charArray, scheme)) {
                this._scheme = charArray;
                indexFirstOf++;
                i = indexFirstOf;
            } else {
                throw new URIException("incorrect scheme");
            }
        }
        this._is_hier_part = false;
        this._is_rel_path = false;
        this._is_abs_path = false;
        this._is_net_path = false;
        if (indexFirstOf >= 0 && indexFirstOf < length && trim.charAt(indexFirstOf) == '/') {
            this._is_hier_part = true;
            if (indexFirstOf + 2 < length && trim.charAt(indexFirstOf + 1) == '/' && !z2) {
                i2 = indexFirstOf(trim, "/?#", indexFirstOf + 2);
                if (i2 == -1) {
                    i2 = trim.substring(indexFirstOf + 2).length() == 0 ? indexFirstOf + 2 : trim.length();
                }
                parseAuthority(trim.substring(indexFirstOf + 2, i2), z);
                this._is_net_path = true;
                indexFirstOf = i2;
            }
            if (i2 == indexFirstOf) {
                this._is_abs_path = true;
            }
        }
        if (i2 < length) {
            i3 = indexFirstOf(trim, "?#", i2);
            if (i3 == -1) {
                i3 = trim.length();
            }
            if (!this._is_abs_path) {
                if ((!z && prevalidate(trim.substring(i2, i3), disallowed_rel_path)) || (z && validate(trim.substring(i2, i3).toCharArray(), rel_path))) {
                    this._is_rel_path = true;
                } else if ((z || !prevalidate(trim.substring(i2, i3), disallowed_opaque_part)) && (!z || !validate(trim.substring(i2, i3).toCharArray(), opaque_part))) {
                    this._path = null;
                } else {
                    this._is_opaque_part = true;
                }
            }
            String substring = trim.substring(i2, i3);
            if (z) {
                setRawPath(substring.toCharArray());
            } else {
                setPath(substring);
            }
        }
        String protocolCharset2 = getProtocolCharset();
        if (i3 >= 0 && i3 + 1 < length && trim.charAt(i3) == '?') {
            int indexOf3 = trim.indexOf(35, i3 + 1);
            if (indexOf3 == -1) {
                indexOf3 = trim.length();
            }
            if (z) {
                this._query = trim.substring(i3 + 1, indexOf3).toCharArray();
                if (!validate(this._query, uric)) {
                    throw new URIException("Invalid query");
                }
            } else {
                this._query = encode(trim.substring(i3 + 1, indexOf3), allowed_query, protocolCharset2);
            }
            i3 = indexOf3;
        }
        if (i3 >= 0 && i3 + 1 <= length && trim.charAt(i3) == '#') {
            if (i3 + 1 == length) {
                encode = PoiTypeDef.All.toCharArray();
                uri = this;
            } else if (z) {
                encode = trim.substring(i3 + 1).toCharArray();
                uri = this;
            } else {
                encode = encode(trim.substring(i3 + 1), allowed_fragment, protocolCharset2);
                uri = this;
            }
            uri._fragment = encode;
        }
        setURI();
    }

    /* access modifiers changed from: protected */
    public boolean prevalidate(String str, BitSet bitSet) {
        if (str == null) {
            return false;
        }
        char[] charArray = str.toCharArray();
        for (char c : charArray) {
            if (bitSet.get(c)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public char[] removeFragmentIdentifier(char[] cArr) {
        if (cArr == null) {
            return null;
        }
        int indexOf = new String(cArr).indexOf(35);
        return indexOf != -1 ? new String(cArr).substring(0, indexOf).toCharArray() : cArr;
    }

    /* access modifiers changed from: protected */
    public char[] resolvePath(char[] cArr, char[] cArr2) {
        String str = cArr == null ? PoiTypeDef.All : new String(cArr);
        if (cArr2 == null || cArr2.length == 0) {
            return normalize(cArr);
        }
        if (cArr2[0] == '/') {
            return normalize(cArr2);
        }
        int lastIndexOf = str.lastIndexOf(47);
        if (lastIndexOf != -1) {
            str.substring(0, lastIndexOf + 1).toCharArray();
        }
        StringBuffer stringBuffer = new StringBuffer(str.length() + cArr2.length);
        stringBuffer.append(lastIndexOf != -1 ? str.substring(0, lastIndexOf + 1) : CookieSpec.PATH_DELIM);
        stringBuffer.append(cArr2);
        return normalize(stringBuffer.toString().toCharArray());
    }

    public void setEscapedAuthority(String str) {
        parseAuthority(str, true);
        setURI();
    }

    public void setEscapedFragment(String str) {
        if (str == null) {
            this._fragment = null;
            this.hash = 0;
            return;
        }
        setRawFragment(str.toCharArray());
    }

    public void setEscapedPath(String str) {
        if (str == null) {
            this._opaque = null;
            this._path = null;
            setURI();
            return;
        }
        setRawPath(str.toCharArray());
    }

    public void setEscapedQuery(String str) {
        if (str == null) {
            this._query = null;
            setURI();
            return;
        }
        setRawQuery(str.toCharArray());
    }

    public void setFragment(String str) {
        if (str == null || str.length() == 0) {
            this._fragment = str == null ? null : str.toCharArray();
            this.hash = 0;
            return;
        }
        this._fragment = encode(str, allowed_fragment, getProtocolCharset());
        this.hash = 0;
    }

    public void setPath(String str) {
        if (str == null || str.length() == 0) {
            char[] charArray = str == null ? null : str.toCharArray();
            this._opaque = charArray;
            this._path = charArray;
            setURI();
            return;
        }
        String protocolCharset2 = getProtocolCharset();
        if (this._is_net_path || this._is_abs_path) {
            this._path = encode(str, allowed_abs_path, protocolCharset2);
        } else if (this._is_rel_path) {
            StringBuffer stringBuffer = new StringBuffer(str.length());
            int indexOf = str.indexOf(47);
            if (indexOf == 0) {
                throw new URIException(1, "incorrect relative path");
            }
            if (indexOf > 0) {
                stringBuffer.append(encode(str.substring(0, indexOf), allowed_rel_path, protocolCharset2));
                stringBuffer.append(encode(str.substring(indexOf), allowed_abs_path, protocolCharset2));
            } else {
                stringBuffer.append(encode(str, allowed_rel_path, protocolCharset2));
            }
            this._path = stringBuffer.toString().toCharArray();
        } else if (this._is_opaque_part) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.insert(0, encode(str.substring(0, 1), uric_no_slash, protocolCharset2));
            stringBuffer2.insert(1, encode(str.substring(1), uric, protocolCharset2));
            this._opaque = stringBuffer2.toString().toCharArray();
        } else {
            throw new URIException(1, "incorrect path");
        }
        setURI();
    }

    public void setQuery(String str) {
        if (str == null || str.length() == 0) {
            this._query = str == null ? null : str.toCharArray();
            setURI();
            return;
        }
        setRawQuery(encode(str, allowed_query, getProtocolCharset()));
    }

    public void setRawAuthority(char[] cArr) {
        parseAuthority(new String(cArr), true);
        setURI();
    }

    public void setRawFragment(char[] cArr) {
        if (cArr == null || cArr.length == 0) {
            this._fragment = cArr;
            this.hash = 0;
        } else if (!validate(cArr, fragment)) {
            throw new URIException(3, "escaped fragment not valid");
        } else {
            this._fragment = cArr;
            this.hash = 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.indexFirstOf(char[], char):int
     arg types: [char[], int]
     candidates:
      org.apache.commons.httpclient.URI.indexFirstOf(java.lang.String, java.lang.String):int
      org.apache.commons.httpclient.URI.indexFirstOf(char[], char):int */
    public void setRawPath(char[] cArr) {
        if (cArr == null || cArr.length == 0) {
            this._opaque = cArr;
            this._path = cArr;
            setURI();
            return;
        }
        char[] removeFragmentIdentifier = removeFragmentIdentifier(cArr);
        if (this._is_net_path || this._is_abs_path) {
            if (removeFragmentIdentifier[0] != '/') {
                throw new URIException(1, "not absolute path");
            } else if (!validate(removeFragmentIdentifier, abs_path)) {
                throw new URIException(3, "escaped absolute path not valid");
            } else {
                this._path = removeFragmentIdentifier;
            }
        } else if (this._is_rel_path) {
            int indexFirstOf = indexFirstOf(removeFragmentIdentifier, '/');
            if (indexFirstOf == 0) {
                throw new URIException(1, "incorrect path");
            } else if ((indexFirstOf <= 0 || validate(removeFragmentIdentifier, 0, indexFirstOf - 1, rel_segment) || validate(removeFragmentIdentifier, indexFirstOf, -1, abs_path)) && (indexFirstOf >= 0 || validate(removeFragmentIdentifier, 0, -1, rel_segment))) {
                this._path = removeFragmentIdentifier;
            } else {
                throw new URIException(3, "escaped relative path not valid");
            }
        } else if (!this._is_opaque_part) {
            throw new URIException(1, "incorrect path");
        } else if (uric_no_slash.get(removeFragmentIdentifier[0]) || validate(removeFragmentIdentifier, 1, -1, uric)) {
            this._opaque = removeFragmentIdentifier;
        } else {
            throw new URIException(3, "escaped opaque part not valid");
        }
        setURI();
    }

    public void setRawQuery(char[] cArr) {
        if (cArr == null || cArr.length == 0) {
            this._query = cArr;
            setURI();
            return;
        }
        char[] removeFragmentIdentifier = removeFragmentIdentifier(cArr);
        if (!validate(removeFragmentIdentifier, query)) {
            throw new URIException(3, "escaped query not valid");
        }
        this._query = removeFragmentIdentifier;
        setURI();
    }

    /* access modifiers changed from: protected */
    public void setURI() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this._scheme != null) {
            stringBuffer.append(this._scheme);
            stringBuffer.append(':');
        }
        if (this._is_net_path) {
            stringBuffer.append("//");
            if (this._authority != null) {
                stringBuffer.append(this._authority);
            }
        }
        if (this._opaque != null && this._is_opaque_part) {
            stringBuffer.append(this._opaque);
        } else if (!(this._path == null || this._path.length == 0)) {
            stringBuffer.append(this._path);
        }
        if (this._query != null) {
            stringBuffer.append('?');
            stringBuffer.append(this._query);
        }
        this._uri = stringBuffer.toString().toCharArray();
        this.hash = 0;
    }

    public String toString() {
        return getEscapedURI();
    }

    /* access modifiers changed from: protected */
    public boolean validate(char[] cArr, int i, int i2, BitSet bitSet) {
        if (i2 == -1) {
            i2 = cArr.length - 1;
        }
        while (i <= i2) {
            if (!bitSet.get(cArr[i])) {
                return false;
            }
            i++;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean validate(char[] cArr, BitSet bitSet) {
        return validate(cArr, 0, -1, bitSet);
    }
}
