package org.apache.commons.httpclient.protocol;

import java.net.InetAddress;
import java.net.Socket;
import org.apache.commons.httpclient.params.HttpConnectionParams;

public interface ProtocolSocketFactory {
    Socket createSocket(String str, int i);

    Socket createSocket(String str, int i, InetAddress inetAddress, int i2);

    Socket createSocket(String str, int i, InetAddress inetAddress, int i2, HttpConnectionParams httpConnectionParams);
}
