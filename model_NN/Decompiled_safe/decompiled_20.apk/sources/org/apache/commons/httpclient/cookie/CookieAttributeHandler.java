package org.apache.commons.httpclient.cookie;

import org.apache.commons.httpclient.Cookie;

public interface CookieAttributeHandler {
    boolean match(Cookie cookie, CookieOrigin cookieOrigin);

    void parse(Cookie cookie, String str);

    void validate(Cookie cookie, CookieOrigin cookieOrigin);
}
