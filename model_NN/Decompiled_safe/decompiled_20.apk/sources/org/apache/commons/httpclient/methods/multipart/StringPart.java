package org.apache.commons.httpclient.methods.multipart;

import java.io.OutputStream;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class StringPart extends PartBase {
    public static final String DEFAULT_CHARSET = "US-ASCII";
    public static final String DEFAULT_CONTENT_TYPE = "text/plain";
    public static final String DEFAULT_TRANSFER_ENCODING = "8bit";
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$methods$multipart$StringPart;
    private byte[] content;
    private String value;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$methods$multipart$StringPart == null) {
            cls = class$("org.apache.commons.httpclient.methods.multipart.StringPart");
            class$org$apache$commons$httpclient$methods$multipart$StringPart = cls;
        } else {
            cls = class$org$apache$commons$httpclient$methods$multipart$StringPart;
        }
        LOG = LogFactory.getLog(cls);
    }

    public StringPart(String str, String str2) {
        this(str, str2, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StringPart(String str, String str2, String str3) {
        super(str, DEFAULT_CONTENT_TYPE, str3 == null ? "US-ASCII" : str3, DEFAULT_TRANSFER_ENCODING);
        if (str2 == null) {
            throw new IllegalArgumentException("Value may not be null");
        } else if (str2.indexOf(0) != -1) {
            throw new IllegalArgumentException("NULs may not be present in string parts");
        } else {
            this.value = str2;
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    private byte[] getContent() {
        if (this.content == null) {
            this.content = EncodingUtil.getBytes(this.value, getCharSet());
        }
        return this.content;
    }

    /* access modifiers changed from: protected */
    public long lengthOfData() {
        LOG.trace("enter lengthOfData()");
        return (long) getContent().length;
    }

    /* access modifiers changed from: protected */
    public void sendData(OutputStream outputStream) {
        LOG.trace("enter sendData(OutputStream)");
        outputStream.write(getContent());
    }

    public void setCharSet(String str) {
        super.setCharSet(str);
        this.content = null;
    }
}
