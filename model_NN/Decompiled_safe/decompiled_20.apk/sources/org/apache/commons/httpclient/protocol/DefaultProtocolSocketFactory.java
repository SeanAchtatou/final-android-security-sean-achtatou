package org.apache.commons.httpclient.protocol;

import java.net.InetAddress;
import java.net.Socket;
import org.apache.commons.httpclient.params.HttpConnectionParams;

public class DefaultProtocolSocketFactory implements ProtocolSocketFactory {
    private static final DefaultProtocolSocketFactory factory = new DefaultProtocolSocketFactory();

    static DefaultProtocolSocketFactory getSocketFactory() {
        return factory;
    }

    public Socket createSocket(String str, int i) {
        return new Socket(str, i);
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return new Socket(str, i, inetAddress, i2);
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2, HttpConnectionParams httpConnectionParams) {
        if (httpConnectionParams == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        int connectionTimeout = httpConnectionParams.getConnectionTimeout();
        if (connectionTimeout == 0) {
            return createSocket(str, i, inetAddress, i2);
        }
        Socket createSocket = ReflectionSocketFactory.createSocket("javax.net.SocketFactory", str, i, inetAddress, i2, connectionTimeout);
        return createSocket == null ? ControllerThreadSocketFactory.createSocket(this, str, i, inetAddress, i2, connectionTimeout) : createSocket;
    }

    public boolean equals(Object obj) {
        return obj != null && obj.getClass().equals(getClass());
    }

    public int hashCode() {
        return getClass().hashCode();
    }
}
