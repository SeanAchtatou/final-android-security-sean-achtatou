package org.apache.commons.httpclient.params;

import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpVersion;
import org.apache.commons.httpclient.cookie.CookiePolicy;

public class DefaultHttpParamsFactory implements HttpParamsFactory {
    static Class class$org$apache$commons$httpclient$SimpleHttpConnectionManager;
    private HttpParams httpParams;

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public HttpParams createParams() {
        Class cls;
        String str;
        String str2;
        String str3;
        HttpClientParams httpClientParams = new HttpClientParams(null);
        httpClientParams.setParameter(HttpMethodParams.USER_AGENT, "Jakarta Commons-HttpClient/3.1");
        httpClientParams.setVersion(HttpVersion.HTTP_1_1);
        if (class$org$apache$commons$httpclient$SimpleHttpConnectionManager == null) {
            cls = class$("org.apache.commons.httpclient.SimpleHttpConnectionManager");
            class$org$apache$commons$httpclient$SimpleHttpConnectionManager = cls;
        } else {
            cls = class$org$apache$commons$httpclient$SimpleHttpConnectionManager;
        }
        httpClientParams.setConnectionManagerClass(cls);
        httpClientParams.setCookiePolicy(CookiePolicy.DEFAULT);
        httpClientParams.setHttpElementCharset("US-ASCII");
        httpClientParams.setContentCharset("ISO-8859-1");
        httpClientParams.setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList("EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"));
        httpClientParams.setParameter(HttpMethodParams.DATE_PATTERNS, arrayList);
        try {
            str = System.getProperty("httpclient.useragent");
        } catch (SecurityException e) {
            str = null;
        }
        if (str != null) {
            httpClientParams.setParameter(HttpMethodParams.USER_AGENT, str);
        }
        try {
            str2 = System.getProperty(HttpState.PREEMPTIVE_PROPERTY);
        } catch (SecurityException e2) {
            str2 = null;
        }
        if (str2 != null) {
            String lowerCase = str2.trim().toLowerCase();
            if (lowerCase.equals("true")) {
                httpClientParams.setParameter(HttpClientParams.PREEMPTIVE_AUTHENTICATION, Boolean.TRUE);
            } else if (lowerCase.equals(HttpState.PREEMPTIVE_DEFAULT)) {
                httpClientParams.setParameter(HttpClientParams.PREEMPTIVE_AUTHENTICATION, Boolean.FALSE);
            }
        }
        try {
            str3 = System.getProperty("apache.commons.httpclient.cookiespec");
        } catch (SecurityException e3) {
            str3 = null;
        }
        if (str3 != null) {
            if ("COMPATIBILITY".equalsIgnoreCase(str3)) {
                httpClientParams.setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
            } else if ("NETSCAPE_DRAFT".equalsIgnoreCase(str3)) {
                httpClientParams.setCookiePolicy(CookiePolicy.NETSCAPE);
            } else if ("RFC2109".equalsIgnoreCase(str3)) {
                httpClientParams.setCookiePolicy(CookiePolicy.RFC_2109);
            }
        }
        return httpClientParams;
    }

    public synchronized HttpParams getDefaultParams() {
        if (this.httpParams == null) {
            this.httpParams = createParams();
        }
        return this.httpParams;
    }
}
