package org.apache.commons.httpclient;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.WeakHashMap;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.util.IdleConnectionHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MultiThreadedHttpConnectionManager implements HttpConnectionManager {
    private static WeakHashMap ALL_CONNECTION_MANAGERS = new WeakHashMap();
    public static final int DEFAULT_MAX_HOST_CONNECTIONS = 2;
    public static final int DEFAULT_MAX_TOTAL_CONNECTIONS = 20;
    private static final Log LOG;
    private static final ReferenceQueue REFERENCE_QUEUE = new ReferenceQueue();
    private static ReferenceQueueThread REFERENCE_QUEUE_THREAD;
    private static final Map REFERENCE_TO_CONNECTION_SOURCE = new HashMap();
    static Class class$org$apache$commons$httpclient$MultiThreadedHttpConnectionManager;
    private ConnectionPool connectionPool = new ConnectionPool(this, null);
    private HttpConnectionManagerParams params = new HttpConnectionManagerParams();
    private volatile boolean shutdown = false;

    /* renamed from: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    class ConnectionPool {
        private LinkedList freeConnections;
        private IdleConnectionHandler idleConnectionHandler;
        private final Map mapHosts;
        private int numConnections;
        private final MultiThreadedHttpConnectionManager this$0;
        private LinkedList waitingThreads;

        private ConnectionPool(MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager) {
            this.this$0 = multiThreadedHttpConnectionManager;
            this.freeConnections = new LinkedList();
            this.waitingThreads = new LinkedList();
            this.mapHosts = new HashMap();
            this.idleConnectionHandler = new IdleConnectionHandler();
            this.numConnections = 0;
        }

        ConnectionPool(MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager, AnonymousClass1 r2) {
            this(multiThreadedHttpConnectionManager);
        }

        static int access$200(ConnectionPool connectionPool) {
            return connectionPool.numConnections;
        }

        static LinkedList access$300(ConnectionPool connectionPool) {
            return connectionPool.freeConnections;
        }

        static LinkedList access$500(ConnectionPool connectionPool) {
            return connectionPool.waitingThreads;
        }

        private synchronized void deleteConnection(HttpConnection httpConnection) {
            HostConfiguration access$1100 = MultiThreadedHttpConnectionManager.access$1100(this.this$0, httpConnection);
            if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer("Reclaiming connection, hostConfig=").append(access$1100).toString());
            }
            httpConnection.close();
            HostConnectionPool hostPool = getHostPool(access$1100, true);
            hostPool.freeConnections.remove(httpConnection);
            hostPool.numConnections--;
            this.numConnections--;
            if (hostPool.numConnections == 0 && hostPool.waitingThreads.isEmpty()) {
                this.mapHosts.remove(access$1100);
            }
            this.idleConnectionHandler.remove(httpConnection);
        }

        public synchronized void closeIdleConnections(long j) {
            this.idleConnectionHandler.closeIdleConnections(j);
        }

        public synchronized HttpConnection createConnection(HostConfiguration hostConfiguration) {
            HttpConnectionWithReference httpConnectionWithReference;
            HostConnectionPool hostPool = getHostPool(hostConfiguration, true);
            if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer("Allocating new connection, hostConfig=").append(hostConfiguration).toString());
            }
            httpConnectionWithReference = new HttpConnectionWithReference(hostConfiguration);
            httpConnectionWithReference.getParams().setDefaults(MultiThreadedHttpConnectionManager.access$800(this.this$0));
            httpConnectionWithReference.setHttpConnectionManager(this.this$0);
            this.numConnections++;
            hostPool.numConnections++;
            MultiThreadedHttpConnectionManager.access$900(httpConnectionWithReference, hostConfiguration, this);
            return httpConnectionWithReference;
        }

        public synchronized void deleteClosedConnections() {
            Iterator it = this.freeConnections.iterator();
            while (it.hasNext()) {
                HttpConnection httpConnection = (HttpConnection) it.next();
                if (!httpConnection.isOpen()) {
                    it.remove();
                    deleteConnection(httpConnection);
                }
            }
        }

        public synchronized void deleteLeastUsedConnection() {
            HttpConnection httpConnection = (HttpConnection) this.freeConnections.removeFirst();
            if (httpConnection != null) {
                deleteConnection(httpConnection);
            } else if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug("Attempted to reclaim an unused connection but there were none.");
            }
        }

        public void freeConnection(HttpConnection httpConnection) {
            HostConfiguration access$1100 = MultiThreadedHttpConnectionManager.access$1100(this.this$0, httpConnection);
            if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer("Freeing connection, hostConfig=").append(access$1100).toString());
            }
            synchronized (this) {
                if (MultiThreadedHttpConnectionManager.access$1200(this.this$0)) {
                    httpConnection.close();
                    return;
                }
                HostConnectionPool hostPool = getHostPool(access$1100, true);
                hostPool.freeConnections.add(httpConnection);
                if (hostPool.numConnections == 0) {
                    MultiThreadedHttpConnectionManager.access$700().error(new StringBuffer("Host connection pool not found, hostConfig=").append(access$1100).toString());
                    hostPool.numConnections = 1;
                }
                this.freeConnections.add(httpConnection);
                MultiThreadedHttpConnectionManager.access$1300((HttpConnectionWithReference) httpConnection);
                if (this.numConnections == 0) {
                    MultiThreadedHttpConnectionManager.access$700().error(new StringBuffer("Host connection pool not found, hostConfig=").append(access$1100).toString());
                    this.numConnections = 1;
                }
                this.idleConnectionHandler.add(httpConnection);
                notifyWaitingThread(hostPool);
            }
        }

        public synchronized HttpConnection getFreeConnection(HostConfiguration hostConfiguration) {
            HttpConnectionWithReference httpConnectionWithReference;
            httpConnectionWithReference = null;
            HostConnectionPool hostPool = getHostPool(hostConfiguration, false);
            if (hostPool != null && hostPool.freeConnections.size() > 0) {
                httpConnectionWithReference = (HttpConnectionWithReference) hostPool.freeConnections.removeLast();
                this.freeConnections.remove(httpConnectionWithReference);
                MultiThreadedHttpConnectionManager.access$900(httpConnectionWithReference, hostConfiguration, this);
                if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                    MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer("Getting free connection, hostConfig=").append(hostConfiguration).toString());
                }
                this.idleConnectionHandler.remove(httpConnectionWithReference);
            } else if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer("There were no free connections to get, hostConfig=").append(hostConfiguration).toString());
            }
            return httpConnectionWithReference;
        }

        public synchronized HostConnectionPool getHostPool(HostConfiguration hostConfiguration, boolean z) {
            HostConnectionPool hostConnectionPool;
            MultiThreadedHttpConnectionManager.access$700().trace("enter HttpConnectionManager.ConnectionPool.getHostPool(HostConfiguration)");
            hostConnectionPool = (HostConnectionPool) this.mapHosts.get(hostConfiguration);
            if (hostConnectionPool == null && z) {
                hostConnectionPool = new HostConnectionPool(null);
                hostConnectionPool.hostConfiguration = hostConfiguration;
                this.mapHosts.put(hostConfiguration, hostConnectionPool);
            }
            return hostConnectionPool;
        }

        public synchronized void handleLostConnection(HostConfiguration hostConfiguration) {
            HostConnectionPool hostPool = getHostPool(hostConfiguration, true);
            hostPool.numConnections--;
            if (hostPool.numConnections == 0 && hostPool.waitingThreads.isEmpty()) {
                this.mapHosts.remove(hostConfiguration);
            }
            this.numConnections--;
            notifyWaitingThread(hostConfiguration);
        }

        public synchronized void notifyWaitingThread(HostConfiguration hostConfiguration) {
            notifyWaitingThread(getHostPool(hostConfiguration, true));
        }

        public synchronized void notifyWaitingThread(HostConnectionPool hostConnectionPool) {
            WaitingThread waitingThread = null;
            if (hostConnectionPool.waitingThreads.size() > 0) {
                if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                    MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer("Notifying thread waiting on host pool, hostConfig=").append(hostConnectionPool.hostConfiguration).toString());
                }
                waitingThread = (WaitingThread) hostConnectionPool.waitingThreads.removeFirst();
                this.waitingThreads.remove(waitingThread);
            } else if (this.waitingThreads.size() > 0) {
                if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                    MultiThreadedHttpConnectionManager.access$700().debug("No-one waiting on host pool, notifying next waiting thread.");
                }
                waitingThread = (WaitingThread) this.waitingThreads.removeFirst();
                waitingThread.hostConnectionPool.waitingThreads.remove(waitingThread);
            } else if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug("Notifying no-one, there are no waiting threads");
            }
            if (waitingThread != null) {
                waitingThread.interruptedByConnectionPool = true;
                waitingThread.thread.interrupt();
            }
        }

        public synchronized void shutdown() {
            Iterator it = this.freeConnections.iterator();
            while (it.hasNext()) {
                it.remove();
                ((HttpConnection) it.next()).close();
            }
            MultiThreadedHttpConnectionManager.access$600(this);
            Iterator it2 = this.waitingThreads.iterator();
            while (it2.hasNext()) {
                WaitingThread waitingThread = (WaitingThread) it2.next();
                it2.remove();
                waitingThread.interruptedByConnectionPool = true;
                waitingThread.thread.interrupt();
            }
            this.mapHosts.clear();
            this.idleConnectionHandler.removeAll();
        }
    }

    class ConnectionSource {
        public ConnectionPool connectionPool;
        public HostConfiguration hostConfiguration;

        private ConnectionSource() {
        }

        ConnectionSource(AnonymousClass1 r1) {
            this();
        }
    }

    class HostConnectionPool {
        public LinkedList freeConnections;
        public HostConfiguration hostConfiguration;
        public int numConnections;
        public LinkedList waitingThreads;

        private HostConnectionPool() {
            this.freeConnections = new LinkedList();
            this.waitingThreads = new LinkedList();
            this.numConnections = 0;
        }

        HostConnectionPool(AnonymousClass1 r1) {
            this();
        }
    }

    class HttpConnectionAdapter extends HttpConnection {
        private HttpConnection wrappedConnection;

        public HttpConnectionAdapter(HttpConnection httpConnection) {
            super(httpConnection.getHost(), httpConnection.getPort(), httpConnection.getProtocol());
            this.wrappedConnection = httpConnection;
        }

        public void close() {
            if (hasConnection()) {
                this.wrappedConnection.close();
            }
        }

        public boolean closeIfStale() {
            if (hasConnection()) {
                return this.wrappedConnection.closeIfStale();
            }
            return false;
        }

        public void flushRequestOutputStream() {
            if (hasConnection()) {
                this.wrappedConnection.flushRequestOutputStream();
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public String getHost() {
            if (hasConnection()) {
                return this.wrappedConnection.getHost();
            }
            return null;
        }

        public HttpConnectionManager getHttpConnectionManager() {
            if (hasConnection()) {
                return this.wrappedConnection.getHttpConnectionManager();
            }
            return null;
        }

        public InputStream getLastResponseInputStream() {
            if (hasConnection()) {
                return this.wrappedConnection.getLastResponseInputStream();
            }
            return null;
        }

        public InetAddress getLocalAddress() {
            if (hasConnection()) {
                return this.wrappedConnection.getLocalAddress();
            }
            return null;
        }

        public HttpConnectionParams getParams() {
            if (hasConnection()) {
                return this.wrappedConnection.getParams();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public int getPort() {
            if (hasConnection()) {
                return this.wrappedConnection.getPort();
            }
            return -1;
        }

        public Protocol getProtocol() {
            if (hasConnection()) {
                return this.wrappedConnection.getProtocol();
            }
            return null;
        }

        public String getProxyHost() {
            if (hasConnection()) {
                return this.wrappedConnection.getProxyHost();
            }
            return null;
        }

        public int getProxyPort() {
            if (hasConnection()) {
                return this.wrappedConnection.getProxyPort();
            }
            return -1;
        }

        public OutputStream getRequestOutputStream() {
            if (hasConnection()) {
                return this.wrappedConnection.getRequestOutputStream();
            }
            return null;
        }

        public InputStream getResponseInputStream() {
            if (hasConnection()) {
                return this.wrappedConnection.getResponseInputStream();
            }
            return null;
        }

        public int getSendBufferSize() {
            if (hasConnection()) {
                return this.wrappedConnection.getSendBufferSize();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public int getSoTimeout() {
            if (hasConnection()) {
                return this.wrappedConnection.getSoTimeout();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public String getVirtualHost() {
            if (hasConnection()) {
                return this.wrappedConnection.getVirtualHost();
            }
            throw new IllegalStateException("Connection has been released");
        }

        /* access modifiers changed from: package-private */
        public HttpConnection getWrappedConnection() {
            return this.wrappedConnection;
        }

        /* access modifiers changed from: protected */
        public boolean hasConnection() {
            return this.wrappedConnection != null;
        }

        public boolean isOpen() {
            if (hasConnection()) {
                return this.wrappedConnection.isOpen();
            }
            return false;
        }

        public boolean isProxied() {
            if (hasConnection()) {
                return this.wrappedConnection.isProxied();
            }
            return false;
        }

        public boolean isResponseAvailable() {
            if (hasConnection()) {
                return this.wrappedConnection.isResponseAvailable();
            }
            return false;
        }

        public boolean isResponseAvailable(int i) {
            if (hasConnection()) {
                return this.wrappedConnection.isResponseAvailable(i);
            }
            return false;
        }

        public boolean isSecure() {
            if (hasConnection()) {
                return this.wrappedConnection.isSecure();
            }
            return false;
        }

        public boolean isStaleCheckingEnabled() {
            if (hasConnection()) {
                return this.wrappedConnection.isStaleCheckingEnabled();
            }
            return false;
        }

        public boolean isTransparent() {
            if (hasConnection()) {
                return this.wrappedConnection.isTransparent();
            }
            return false;
        }

        public void open() {
            if (hasConnection()) {
                this.wrappedConnection.open();
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void print(String str) {
            if (hasConnection()) {
                this.wrappedConnection.print(str);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void print(String str, String str2) {
            if (hasConnection()) {
                this.wrappedConnection.print(str, str2);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void printLine() {
            if (hasConnection()) {
                this.wrappedConnection.printLine();
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void printLine(String str) {
            if (hasConnection()) {
                this.wrappedConnection.printLine(str);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void printLine(String str, String str2) {
            if (hasConnection()) {
                this.wrappedConnection.printLine(str, str2);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public String readLine() {
            if (hasConnection()) {
                return this.wrappedConnection.readLine();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public String readLine(String str) {
            if (hasConnection()) {
                return this.wrappedConnection.readLine(str);
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void releaseConnection() {
            if (!isLocked() && hasConnection()) {
                HttpConnection httpConnection = this.wrappedConnection;
                this.wrappedConnection = null;
                httpConnection.releaseConnection();
            }
        }

        public void setConnectionTimeout(int i) {
            if (hasConnection()) {
                this.wrappedConnection.setConnectionTimeout(i);
            }
        }

        public void setHost(String str) {
            if (hasConnection()) {
                this.wrappedConnection.setHost(str);
            }
        }

        public void setHttpConnectionManager(HttpConnectionManager httpConnectionManager) {
            if (hasConnection()) {
                this.wrappedConnection.setHttpConnectionManager(httpConnectionManager);
            }
        }

        public void setLastResponseInputStream(InputStream inputStream) {
            if (hasConnection()) {
                this.wrappedConnection.setLastResponseInputStream(inputStream);
            }
        }

        public void setLocalAddress(InetAddress inetAddress) {
            if (hasConnection()) {
                this.wrappedConnection.setLocalAddress(inetAddress);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setParams(HttpConnectionParams httpConnectionParams) {
            if (hasConnection()) {
                this.wrappedConnection.setParams(httpConnectionParams);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setPort(int i) {
            if (hasConnection()) {
                this.wrappedConnection.setPort(i);
            }
        }

        public void setProtocol(Protocol protocol) {
            if (hasConnection()) {
                this.wrappedConnection.setProtocol(protocol);
            }
        }

        public void setProxyHost(String str) {
            if (hasConnection()) {
                this.wrappedConnection.setProxyHost(str);
            }
        }

        public void setProxyPort(int i) {
            if (hasConnection()) {
                this.wrappedConnection.setProxyPort(i);
            }
        }

        public void setSendBufferSize(int i) {
            if (hasConnection()) {
                this.wrappedConnection.setSendBufferSize(i);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setSoTimeout(int i) {
            if (hasConnection()) {
                this.wrappedConnection.setSoTimeout(i);
            }
        }

        public void setSocketTimeout(int i) {
            if (hasConnection()) {
                this.wrappedConnection.setSocketTimeout(i);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setStaleCheckingEnabled(boolean z) {
            if (hasConnection()) {
                this.wrappedConnection.setStaleCheckingEnabled(z);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setVirtualHost(String str) {
            if (hasConnection()) {
                this.wrappedConnection.setVirtualHost(str);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void shutdownOutput() {
            if (hasConnection()) {
                this.wrappedConnection.shutdownOutput();
            }
        }

        public void tunnelCreated() {
            if (hasConnection()) {
                this.wrappedConnection.tunnelCreated();
            }
        }

        public void write(byte[] bArr) {
            if (hasConnection()) {
                this.wrappedConnection.write(bArr);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void write(byte[] bArr, int i, int i2) {
            if (hasConnection()) {
                this.wrappedConnection.write(bArr, i, i2);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void writeLine() {
            if (hasConnection()) {
                this.wrappedConnection.writeLine();
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void writeLine(byte[] bArr) {
            if (hasConnection()) {
                this.wrappedConnection.writeLine(bArr);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }
    }

    class HttpConnectionWithReference extends HttpConnection {
        public WeakReference reference = new WeakReference(this, MultiThreadedHttpConnectionManager.access$1500());

        public HttpConnectionWithReference(HostConfiguration hostConfiguration) {
            super(hostConfiguration);
        }
    }

    class ReferenceQueueThread extends Thread {
        private volatile boolean shutdown = false;

        public ReferenceQueueThread() {
            setDaemon(true);
            setName("MultiThreadedHttpConnectionManager cleanup");
        }

        private void handleReference(Reference reference) {
            ConnectionSource connectionSource;
            synchronized (MultiThreadedHttpConnectionManager.access$1400()) {
                connectionSource = (ConnectionSource) MultiThreadedHttpConnectionManager.access$1400().remove(reference);
            }
            if (connectionSource != null) {
                if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                    MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer("Connection reclaimed by garbage collector, hostConfig=").append(connectionSource.hostConfiguration).toString());
                }
                connectionSource.connectionPool.handleLostConnection(connectionSource.hostConfiguration);
            }
        }

        public void run() {
            while (!this.shutdown) {
                try {
                    Reference remove = MultiThreadedHttpConnectionManager.access$1500().remove();
                    if (remove != null) {
                        handleReference(remove);
                    }
                } catch (InterruptedException e) {
                    MultiThreadedHttpConnectionManager.access$700().debug("ReferenceQueueThread interrupted", e);
                }
            }
        }

        public void shutdown() {
            this.shutdown = true;
            interrupt();
        }
    }

    class WaitingThread {
        public HostConnectionPool hostConnectionPool;
        public boolean interruptedByConnectionPool;
        public Thread thread;

        private WaitingThread() {
            this.interruptedByConnectionPool = false;
        }

        WaitingThread(AnonymousClass1 r1) {
            this();
        }
    }

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$MultiThreadedHttpConnectionManager == null) {
            cls = class$("org.apache.commons.httpclient.MultiThreadedHttpConnectionManager");
            class$org$apache$commons$httpclient$MultiThreadedHttpConnectionManager = cls;
        } else {
            cls = class$org$apache$commons$httpclient$MultiThreadedHttpConnectionManager;
        }
        LOG = LogFactory.getLog(cls);
    }

    public MultiThreadedHttpConnectionManager() {
        synchronized (ALL_CONNECTION_MANAGERS) {
            ALL_CONNECTION_MANAGERS.put(this, null);
        }
    }

    static HostConfiguration access$1100(MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager, HttpConnection httpConnection) {
        return multiThreadedHttpConnectionManager.configurationForConnection(httpConnection);
    }

    static boolean access$1200(MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager) {
        return multiThreadedHttpConnectionManager.shutdown;
    }

    static void access$1300(HttpConnectionWithReference httpConnectionWithReference) {
        removeReferenceToConnection(httpConnectionWithReference);
    }

    static Map access$1400() {
        return REFERENCE_TO_CONNECTION_SOURCE;
    }

    static ReferenceQueue access$1500() {
        return REFERENCE_QUEUE;
    }

    static void access$600(ConnectionPool connectionPool2) {
        shutdownCheckedOutConnections(connectionPool2);
    }

    static Log access$700() {
        return LOG;
    }

    static HttpConnectionManagerParams access$800(MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager) {
        return multiThreadedHttpConnectionManager.params;
    }

    static void access$900(HttpConnectionWithReference httpConnectionWithReference, HostConfiguration hostConfiguration, ConnectionPool connectionPool2) {
        storeReferenceToConnection(httpConnectionWithReference, hostConfiguration, connectionPool2);
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    private HostConfiguration configurationForConnection(HttpConnection httpConnection) {
        HostConfiguration hostConfiguration = new HostConfiguration();
        hostConfiguration.setHost(httpConnection.getHost(), httpConnection.getPort(), httpConnection.getProtocol());
        if (httpConnection.getLocalAddress() != null) {
            hostConfiguration.setLocalAddress(httpConnection.getLocalAddress());
        }
        if (httpConnection.getProxyHost() != null) {
            hostConfiguration.setProxy(httpConnection.getProxyHost(), httpConnection.getProxyPort());
        }
        return hostConfiguration;
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0147 A[SYNTHETIC, Splitter:B:76:0x0147] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x00ad A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.commons.httpclient.HttpConnection doGetConnection(org.apache.commons.httpclient.HostConfiguration r18, long r19) {
        /*
            r17 = this;
            r3 = 0
            r0 = r17
            org.apache.commons.httpclient.params.HttpConnectionManagerParams r2 = r0.params
            r0 = r18
            int r9 = r2.getMaxConnectionsPerHost(r0)
            r0 = r17
            org.apache.commons.httpclient.params.HttpConnectionManagerParams r2 = r0.params
            int r10 = r2.getMaxTotalConnections()
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r11 = r0.connectionPool
            monitor-enter(r11)
            org.apache.commons.httpclient.HostConfiguration r12 = new org.apache.commons.httpclient.HostConfiguration     // Catch:{ all -> 0x0043 }
            r0 = r18
            r12.<init>(r0)     // Catch:{ all -> 0x0043 }
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r2 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            r4 = 1
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$HostConnectionPool r13 = r2.getHostPool(r12, r4)     // Catch:{ all -> 0x0043 }
            r5 = 0
            r6 = 0
            int r2 = (r19 > r6 ? 1 : (r19 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x0046
            r2 = 1
        L_0x0030:
            r6 = 0
            r8 = r3
        L_0x0033:
            if (r8 != 0) goto L_0x016a
            r0 = r17
            boolean r3 = r0.shutdown     // Catch:{ all -> 0x0043 }
            if (r3 == 0) goto L_0x0048
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0043 }
            java.lang.String r3 = "Connection factory has been shutdown."
            r2.<init>(r3)     // Catch:{ all -> 0x0043 }
            throw r2     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r2 = move-exception
            monitor-exit(r11)
            throw r2
        L_0x0046:
            r2 = 0
            goto L_0x0030
        L_0x0048:
            java.util.LinkedList r3 = r13.freeConnections     // Catch:{ all -> 0x0043 }
            int r3 = r3.size()     // Catch:{ all -> 0x0043 }
            if (r3 <= 0) goto L_0x005a
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r3 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            org.apache.commons.httpclient.HttpConnection r3 = r3.getFreeConnection(r12)     // Catch:{ all -> 0x0043 }
            r8 = r3
            goto L_0x0033
        L_0x005a:
            int r3 = r13.numConnections     // Catch:{ all -> 0x0043 }
            if (r3 >= r9) goto L_0x0072
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r3 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            int r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$200(r3)     // Catch:{ all -> 0x0043 }
            if (r3 >= r10) goto L_0x0072
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r3 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            org.apache.commons.httpclient.HttpConnection r3 = r3.createConnection(r12)     // Catch:{ all -> 0x0043 }
            r8 = r3
            goto L_0x0033
        L_0x0072:
            int r3 = r13.numConnections     // Catch:{ all -> 0x0043 }
            if (r3 >= r9) goto L_0x0095
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r3 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            java.util.LinkedList r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$300(r3)     // Catch:{ all -> 0x0043 }
            int r3 = r3.size()     // Catch:{ all -> 0x0043 }
            if (r3 <= 0) goto L_0x0095
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r3 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            r3.deleteLeastUsedConnection()     // Catch:{ all -> 0x0043 }
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r3 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            org.apache.commons.httpclient.HttpConnection r3 = r3.createConnection(r12)     // Catch:{ all -> 0x0043 }
            r8 = r3
            goto L_0x0033
        L_0x0095:
            if (r2 == 0) goto L_0x00d7
            r3 = 0
            int r3 = (r19 > r3 ? 1 : (r19 == r3 ? 0 : -1))
            if (r3 > 0) goto L_0x00d7
            org.apache.commons.httpclient.ConnectionPoolTimeoutException r3 = new org.apache.commons.httpclient.ConnectionPoolTimeoutException     // Catch:{ InterruptedException -> 0x00a5 }
            java.lang.String r4 = "Timeout waiting for connection"
            r3.<init>(r4)     // Catch:{ InterruptedException -> 0x00a5 }
            throw r3     // Catch:{ InterruptedException -> 0x00a5 }
        L_0x00a5:
            r3 = move-exception
            r15 = r3
            r3 = r6
            r6 = r15
        L_0x00a9:
            boolean r7 = r5.interruptedByConnectionPool     // Catch:{ all -> 0x00bc }
            if (r7 != 0) goto L_0x0147
            org.apache.commons.logging.Log r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.LOG     // Catch:{ all -> 0x00bc }
            java.lang.String r4 = "Interrupted while waiting for connection"
            r3.debug(r4, r6)     // Catch:{ all -> 0x00bc }
            java.lang.IllegalThreadStateException r3 = new java.lang.IllegalThreadStateException     // Catch:{ all -> 0x00bc }
            java.lang.String r4 = "Interrupted while waiting in MultiThreadedHttpConnectionManager"
            r3.<init>(r4)     // Catch:{ all -> 0x00bc }
            throw r3     // Catch:{ all -> 0x00bc }
        L_0x00bc:
            r3 = move-exception
        L_0x00bd:
            boolean r4 = r5.interruptedByConnectionPool     // Catch:{ all -> 0x0043 }
            if (r4 != 0) goto L_0x00d1
            java.util.LinkedList r4 = r13.waitingThreads     // Catch:{ all -> 0x0043 }
            r4.remove(r5)     // Catch:{ all -> 0x0043 }
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r4 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            java.util.LinkedList r4 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$500(r4)     // Catch:{ all -> 0x0043 }
            r4.remove(r5)     // Catch:{ all -> 0x0043 }
        L_0x00d1:
            if (r2 == 0) goto L_0x00d6
            java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0043 }
        L_0x00d6:
            throw r3     // Catch:{ all -> 0x0043 }
        L_0x00d7:
            org.apache.commons.logging.Log r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.LOG     // Catch:{ InterruptedException -> 0x00a5 }
            boolean r3 = r3.isDebugEnabled()     // Catch:{ InterruptedException -> 0x00a5 }
            if (r3 == 0) goto L_0x00f3
            org.apache.commons.logging.Log r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.LOG     // Catch:{ InterruptedException -> 0x00a5 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ InterruptedException -> 0x00a5 }
            java.lang.String r14 = "Unable to get a connection, waiting..., hostConfig="
            r4.<init>(r14)     // Catch:{ InterruptedException -> 0x00a5 }
            java.lang.StringBuffer r4 = r4.append(r12)     // Catch:{ InterruptedException -> 0x00a5 }
            java.lang.String r4 = r4.toString()     // Catch:{ InterruptedException -> 0x00a5 }
            r3.debug(r4)     // Catch:{ InterruptedException -> 0x00a5 }
        L_0x00f3:
            if (r5 != 0) goto L_0x0143
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$WaitingThread r4 = new org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$WaitingThread     // Catch:{ InterruptedException -> 0x00a5 }
            r3 = 0
            r4.<init>(r3)     // Catch:{ InterruptedException -> 0x00a5 }
            r4.hostConnectionPool = r13     // Catch:{ InterruptedException -> 0x0170, all -> 0x016c }
            java.lang.Thread r3 = java.lang.Thread.currentThread()     // Catch:{ InterruptedException -> 0x0170, all -> 0x016c }
            r4.thread = r3     // Catch:{ InterruptedException -> 0x0170, all -> 0x016c }
            r5 = r4
        L_0x0104:
            if (r2 == 0) goto L_0x017a
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ InterruptedException -> 0x00a5 }
        L_0x010a:
            java.util.LinkedList r6 = r13.waitingThreads     // Catch:{ InterruptedException -> 0x0177 }
            r6.addLast(r5)     // Catch:{ InterruptedException -> 0x0177 }
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r6 = r0.connectionPool     // Catch:{ InterruptedException -> 0x0177 }
            java.util.LinkedList r6 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$500(r6)     // Catch:{ InterruptedException -> 0x0177 }
            r6.addLast(r5)     // Catch:{ InterruptedException -> 0x0177 }
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r6 = r0.connectionPool     // Catch:{ InterruptedException -> 0x0177 }
            r0 = r19
            r6.wait(r0)     // Catch:{ InterruptedException -> 0x0177 }
            boolean r6 = r5.interruptedByConnectionPool     // Catch:{ all -> 0x0043 }
            if (r6 != 0) goto L_0x0137
            java.util.LinkedList r6 = r13.waitingThreads     // Catch:{ all -> 0x0043 }
            r6.remove(r5)     // Catch:{ all -> 0x0043 }
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r6 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            java.util.LinkedList r6 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$500(r6)     // Catch:{ all -> 0x0043 }
            r6.remove(r5)     // Catch:{ all -> 0x0043 }
        L_0x0137:
            if (r2 == 0) goto L_0x0167
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0043 }
            long r6 = r6 - r3
            long r19 = r19 - r6
            r6 = r3
            goto L_0x0033
        L_0x0143:
            r3 = 0
            r5.interruptedByConnectionPool = r3     // Catch:{ InterruptedException -> 0x00a5 }
            goto L_0x0104
        L_0x0147:
            boolean r6 = r5.interruptedByConnectionPool     // Catch:{ all -> 0x0043 }
            if (r6 != 0) goto L_0x015b
            java.util.LinkedList r6 = r13.waitingThreads     // Catch:{ all -> 0x0043 }
            r6.remove(r5)     // Catch:{ all -> 0x0043 }
            r0 = r17
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r6 = r0.connectionPool     // Catch:{ all -> 0x0043 }
            java.util.LinkedList r6 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$500(r6)     // Catch:{ all -> 0x0043 }
            r6.remove(r5)     // Catch:{ all -> 0x0043 }
        L_0x015b:
            if (r2 == 0) goto L_0x0167
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0043 }
            long r6 = r6 - r3
            long r19 = r19 - r6
            r6 = r3
            goto L_0x0033
        L_0x0167:
            r6 = r3
            goto L_0x0033
        L_0x016a:
            monitor-exit(r11)     // Catch:{ all -> 0x0043 }
            return r8
        L_0x016c:
            r3 = move-exception
            r5 = r4
            goto L_0x00bd
        L_0x0170:
            r3 = move-exception
            r5 = r4
            r15 = r6
            r6 = r3
            r3 = r15
            goto L_0x00a9
        L_0x0177:
            r6 = move-exception
            goto L_0x00a9
        L_0x017a:
            r3 = r6
            goto L_0x010a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.doGetConnection(org.apache.commons.httpclient.HostConfiguration, long):org.apache.commons.httpclient.HttpConnection");
    }

    private static void removeReferenceToConnection(HttpConnectionWithReference httpConnectionWithReference) {
        synchronized (REFERENCE_TO_CONNECTION_SOURCE) {
            REFERENCE_TO_CONNECTION_SOURCE.remove(httpConnectionWithReference.reference);
        }
    }

    public static void shutdownAll() {
        synchronized (REFERENCE_TO_CONNECTION_SOURCE) {
            synchronized (ALL_CONNECTION_MANAGERS) {
                MultiThreadedHttpConnectionManager[] multiThreadedHttpConnectionManagerArr = (MultiThreadedHttpConnectionManager[]) ALL_CONNECTION_MANAGERS.keySet().toArray(new MultiThreadedHttpConnectionManager[ALL_CONNECTION_MANAGERS.size()]);
                for (int i = 0; i < multiThreadedHttpConnectionManagerArr.length; i++) {
                    if (multiThreadedHttpConnectionManagerArr[i] != null) {
                        multiThreadedHttpConnectionManagerArr[i].shutdown();
                    }
                }
            }
            if (REFERENCE_QUEUE_THREAD != null) {
                REFERENCE_QUEUE_THREAD.shutdown();
                REFERENCE_QUEUE_THREAD = null;
            }
            REFERENCE_TO_CONNECTION_SOURCE.clear();
        }
    }

    private static void shutdownCheckedOutConnections(ConnectionPool connectionPool2) {
        ArrayList arrayList = new ArrayList();
        synchronized (REFERENCE_TO_CONNECTION_SOURCE) {
            Iterator it = REFERENCE_TO_CONNECTION_SOURCE.keySet().iterator();
            while (it.hasNext()) {
                Reference reference = (Reference) it.next();
                if (((ConnectionSource) REFERENCE_TO_CONNECTION_SOURCE.get(reference)).connectionPool == connectionPool2) {
                    it.remove();
                    HttpConnection httpConnection = (HttpConnection) reference.get();
                    if (httpConnection != null) {
                        arrayList.add(httpConnection);
                    }
                }
            }
        }
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            HttpConnection httpConnection2 = (HttpConnection) it2.next();
            httpConnection2.close();
            httpConnection2.setHttpConnectionManager(null);
            httpConnection2.releaseConnection();
        }
    }

    private static void storeReferenceToConnection(HttpConnectionWithReference httpConnectionWithReference, HostConfiguration hostConfiguration, ConnectionPool connectionPool2) {
        ConnectionSource connectionSource = new ConnectionSource(null);
        connectionSource.connectionPool = connectionPool2;
        connectionSource.hostConfiguration = hostConfiguration;
        synchronized (REFERENCE_TO_CONNECTION_SOURCE) {
            if (REFERENCE_QUEUE_THREAD == null) {
                ReferenceQueueThread referenceQueueThread = new ReferenceQueueThread();
                REFERENCE_QUEUE_THREAD = referenceQueueThread;
                referenceQueueThread.start();
            }
            REFERENCE_TO_CONNECTION_SOURCE.put(httpConnectionWithReference.reference, connectionSource);
        }
    }

    public void closeIdleConnections(long j) {
        this.connectionPool.closeIdleConnections(j);
        deleteClosedConnections();
    }

    public void deleteClosedConnections() {
        this.connectionPool.deleteClosedConnections();
    }

    public HttpConnection getConnection(HostConfiguration hostConfiguration) {
        while (true) {
            try {
                break;
            } catch (ConnectionPoolTimeoutException e) {
                LOG.debug("Unexpected exception while waiting for connection", e);
            }
        }
        return getConnectionWithTimeout(hostConfiguration, 0);
    }

    public HttpConnection getConnection(HostConfiguration hostConfiguration, long j) {
        LOG.trace("enter HttpConnectionManager.getConnection(HostConfiguration, long)");
        try {
            return getConnectionWithTimeout(hostConfiguration, j);
        } catch (ConnectionPoolTimeoutException e) {
            throw new HttpException(e.getMessage());
        }
    }

    public HttpConnection getConnectionWithTimeout(HostConfiguration hostConfiguration, long j) {
        LOG.trace("enter HttpConnectionManager.getConnectionWithTimeout(HostConfiguration, long)");
        if (hostConfiguration == null) {
            throw new IllegalArgumentException("hostConfiguration is null");
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("HttpConnectionManager.getConnection:  config = ").append(hostConfiguration).append(", timeout = ").append(j).toString());
        }
        return new HttpConnectionAdapter(doGetConnection(hostConfiguration, j));
    }

    public int getConnectionsInPool() {
        int access$200;
        synchronized (this.connectionPool) {
            access$200 = ConnectionPool.access$200(this.connectionPool);
        }
        return access$200;
    }

    public int getConnectionsInPool(HostConfiguration hostConfiguration) {
        int i = 0;
        synchronized (this.connectionPool) {
            HostConnectionPool hostPool = this.connectionPool.getHostPool(hostConfiguration, false);
            if (hostPool != null) {
                i = hostPool.numConnections;
            }
        }
        return i;
    }

    public int getConnectionsInUse() {
        return getConnectionsInPool();
    }

    public int getConnectionsInUse(HostConfiguration hostConfiguration) {
        return getConnectionsInPool(hostConfiguration);
    }

    public int getMaxConnectionsPerHost() {
        return this.params.getDefaultMaxConnectionsPerHost();
    }

    public int getMaxTotalConnections() {
        return this.params.getMaxTotalConnections();
    }

    public HttpConnectionManagerParams getParams() {
        return this.params;
    }

    public boolean isConnectionStaleCheckingEnabled() {
        return this.params.isStaleCheckingEnabled();
    }

    public void releaseConnection(HttpConnection httpConnection) {
        LOG.trace("enter HttpConnectionManager.releaseConnection(HttpConnection)");
        if (httpConnection instanceof HttpConnectionAdapter) {
            httpConnection = ((HttpConnectionAdapter) httpConnection).getWrappedConnection();
        }
        SimpleHttpConnectionManager.finishLastResponse(httpConnection);
        this.connectionPool.freeConnection(httpConnection);
    }

    public void setConnectionStaleCheckingEnabled(boolean z) {
        this.params.setStaleCheckingEnabled(z);
    }

    public void setMaxConnectionsPerHost(int i) {
        this.params.setDefaultMaxConnectionsPerHost(i);
    }

    public void setMaxTotalConnections(int i) {
        this.params.setMaxTotalConnections(i);
    }

    public void setParams(HttpConnectionManagerParams httpConnectionManagerParams) {
        if (httpConnectionManagerParams == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = httpConnectionManagerParams;
    }

    public synchronized void shutdown() {
        synchronized (this.connectionPool) {
            if (!this.shutdown) {
                this.shutdown = true;
                this.connectionPool.shutdown();
            }
        }
    }
}
