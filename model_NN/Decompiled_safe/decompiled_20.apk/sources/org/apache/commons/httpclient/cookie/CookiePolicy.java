package org.apache.commons.httpclient.cookie;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class CookiePolicy {
    public static final String BROWSER_COMPATIBILITY = "compatibility";
    public static final int COMPATIBILITY = 0;
    public static final String DEFAULT = "default";
    public static final String IGNORE_COOKIES = "ignoreCookies";
    protected static final Log LOG;
    public static final String NETSCAPE = "netscape";
    public static final int NETSCAPE_DRAFT = 1;
    public static final int RFC2109 = 2;
    public static final int RFC2965 = 3;
    public static final String RFC_2109 = "rfc2109";
    public static final String RFC_2965 = "rfc2965";
    private static Map SPECS = Collections.synchronizedMap(new HashMap());
    static Class class$org$apache$commons$httpclient$cookie$CookiePolicy;
    static Class class$org$apache$commons$httpclient$cookie$CookieSpecBase;
    static Class class$org$apache$commons$httpclient$cookie$IgnoreCookiesSpec;
    static Class class$org$apache$commons$httpclient$cookie$NetscapeDraftSpec;
    static Class class$org$apache$commons$httpclient$cookie$RFC2109Spec;
    static Class class$org$apache$commons$httpclient$cookie$RFC2965Spec;
    private static int defaultPolicy = 2;

    static {
        Class cls;
        Class cls2;
        Class cls3;
        Class cls4;
        Class cls5;
        Class cls6;
        Class cls7;
        if (class$org$apache$commons$httpclient$cookie$RFC2109Spec == null) {
            cls = class$("org.apache.commons.httpclient.cookie.RFC2109Spec");
            class$org$apache$commons$httpclient$cookie$RFC2109Spec = cls;
        } else {
            cls = class$org$apache$commons$httpclient$cookie$RFC2109Spec;
        }
        registerCookieSpec(DEFAULT, cls);
        if (class$org$apache$commons$httpclient$cookie$RFC2109Spec == null) {
            cls2 = class$("org.apache.commons.httpclient.cookie.RFC2109Spec");
            class$org$apache$commons$httpclient$cookie$RFC2109Spec = cls2;
        } else {
            cls2 = class$org$apache$commons$httpclient$cookie$RFC2109Spec;
        }
        registerCookieSpec(RFC_2109, cls2);
        if (class$org$apache$commons$httpclient$cookie$RFC2965Spec == null) {
            cls3 = class$("org.apache.commons.httpclient.cookie.RFC2965Spec");
            class$org$apache$commons$httpclient$cookie$RFC2965Spec = cls3;
        } else {
            cls3 = class$org$apache$commons$httpclient$cookie$RFC2965Spec;
        }
        registerCookieSpec(RFC_2965, cls3);
        if (class$org$apache$commons$httpclient$cookie$CookieSpecBase == null) {
            cls4 = class$("org.apache.commons.httpclient.cookie.CookieSpecBase");
            class$org$apache$commons$httpclient$cookie$CookieSpecBase = cls4;
        } else {
            cls4 = class$org$apache$commons$httpclient$cookie$CookieSpecBase;
        }
        registerCookieSpec(BROWSER_COMPATIBILITY, cls4);
        if (class$org$apache$commons$httpclient$cookie$NetscapeDraftSpec == null) {
            cls5 = class$("org.apache.commons.httpclient.cookie.NetscapeDraftSpec");
            class$org$apache$commons$httpclient$cookie$NetscapeDraftSpec = cls5;
        } else {
            cls5 = class$org$apache$commons$httpclient$cookie$NetscapeDraftSpec;
        }
        registerCookieSpec(NETSCAPE, cls5);
        if (class$org$apache$commons$httpclient$cookie$IgnoreCookiesSpec == null) {
            cls6 = class$("org.apache.commons.httpclient.cookie.IgnoreCookiesSpec");
            class$org$apache$commons$httpclient$cookie$IgnoreCookiesSpec = cls6;
        } else {
            cls6 = class$org$apache$commons$httpclient$cookie$IgnoreCookiesSpec;
        }
        registerCookieSpec(IGNORE_COOKIES, cls6);
        if (class$org$apache$commons$httpclient$cookie$CookiePolicy == null) {
            cls7 = class$("org.apache.commons.httpclient.cookie.CookiePolicy");
            class$org$apache$commons$httpclient$cookie$CookiePolicy = cls7;
        } else {
            cls7 = class$org$apache$commons$httpclient$cookie$CookiePolicy;
        }
        LOG = LogFactory.getLog(cls7);
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public static CookieSpec getCompatibilitySpec() {
        return getSpecByPolicy(0);
    }

    public static CookieSpec getCookieSpec(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        }
        Class cls = (Class) SPECS.get(str.toLowerCase());
        if (cls != null) {
            try {
                return (CookieSpec) cls.newInstance();
            } catch (Exception e) {
                LOG.error(new StringBuffer("Error initializing cookie spec: ").append(str).toString(), e);
                throw new IllegalStateException(new StringBuffer().append(str).append(" cookie spec implemented by ").append(cls.getName()).append(" could not be initialized").toString());
            }
        } else {
            throw new IllegalStateException(new StringBuffer("Unsupported cookie spec ").append(str).toString());
        }
    }

    public static int getDefaultPolicy() {
        return defaultPolicy;
    }

    public static CookieSpec getDefaultSpec() {
        try {
            return getCookieSpec(DEFAULT);
        } catch (IllegalStateException e) {
            LOG.warn("Default cookie policy is not registered");
            return new RFC2109Spec();
        }
    }

    public static String[] getRegisteredCookieSpecs() {
        return (String[]) SPECS.keySet().toArray(new String[SPECS.size()]);
    }

    public static CookieSpec getSpecByPolicy(int i) {
        switch (i) {
            case 0:
                return new CookieSpecBase();
            case 1:
                return new NetscapeDraftSpec();
            case 2:
                return new RFC2109Spec();
            case 3:
                return new RFC2965Spec();
            default:
                return getDefaultSpec();
        }
    }

    public static CookieSpec getSpecByVersion(int i) {
        switch (i) {
            case 0:
                return new NetscapeDraftSpec();
            case 1:
                return new RFC2109Spec();
            default:
                return getDefaultSpec();
        }
    }

    public static void registerCookieSpec(String str, Class cls) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        } else if (cls == null) {
            throw new IllegalArgumentException("Cookie spec class may not be null");
        } else {
            SPECS.put(str.toLowerCase(), cls);
        }
    }

    public static void setDefaultPolicy(int i) {
        defaultPolicy = i;
    }

    public static void unregisterCookieSpec(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        }
        SPECS.remove(str.toLowerCase());
    }
}
