package org.apache.commons.httpclient.cookie;

import java.util.Comparator;
import org.apache.commons.httpclient.Cookie;

public class CookiePathComparator implements Comparator {
    private String normalizePath(Cookie cookie) {
        String path = cookie.getPath();
        if (path == null) {
            path = CookieSpec.PATH_DELIM;
        }
        return !path.endsWith(CookieSpec.PATH_DELIM) ? new StringBuffer().append(path).append(CookieSpec.PATH_DELIM).toString() : path;
    }

    public int compare(Object obj, Object obj2) {
        String normalizePath = normalizePath((Cookie) obj);
        String normalizePath2 = normalizePath((Cookie) obj2);
        if (normalizePath.equals(normalizePath2)) {
            return 0;
        }
        if (normalizePath.startsWith(normalizePath2)) {
            return -1;
        }
        return normalizePath2.startsWith(normalizePath) ? 1 : 0;
    }
}
