package org.apache.commons.httpclient;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.amap.api.search.poisearch.PoiTypeDef;
import com.umeng.common.b.e;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.util.Collection;
import org.apache.commons.httpclient.auth.AuthState;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.cookie.CookieVersionSupport;
import org.apache.commons.httpclient.cookie.MalformedCookieException;
import org.apache.commons.httpclient.cookie.RFC2109Spec;
import org.apache.commons.httpclient.cookie.RFC2965Spec;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.httpclient.util.ExceptionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class HttpMethodBase implements HttpMethod {
    private static final int DEFAULT_INITIAL_BUFFER_SIZE = 4096;
    private static final Log LOG;
    private static final int RESPONSE_WAIT_TIME_MS = 3000;
    static Class class$org$apache$commons$httpclient$HttpMethodBase;
    private volatile boolean aborted = false;
    private boolean connectionCloseForced = false;
    private CookieSpec cookiespec = null;
    private boolean doAuthentication = true;
    protected HttpVersion effectiveVersion = null;
    private boolean followRedirects = false;
    private AuthState hostAuthState = new AuthState();
    private HttpHost httphost = null;
    private MethodRetryHandler methodRetryHandler;
    private HttpMethodParams params = new HttpMethodParams();
    private String path = null;
    private AuthState proxyAuthState = new AuthState();
    private String queryString = null;
    private int recoverableExceptionCount = 0;
    private HeaderGroup requestHeaders = new HeaderGroup();
    private boolean requestSent = false;
    private byte[] responseBody = null;
    private HttpConnection responseConnection = null;
    private HeaderGroup responseHeaders = new HeaderGroup();
    private InputStream responseStream = null;
    private HeaderGroup responseTrailerHeaders = new HeaderGroup();
    protected StatusLine statusLine = null;
    private boolean used = false;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HttpMethodBase == null) {
            cls = class$("org.apache.commons.httpclient.HttpMethodBase");
            class$org$apache$commons$httpclient$HttpMethodBase = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HttpMethodBase;
        }
        LOG = LogFactory.getLog(cls);
    }

    public HttpMethodBase() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      org.apache.commons.httpclient.URI.<init>(java.lang.String, java.lang.String, java.lang.String):void
      org.apache.commons.httpclient.URI.<init>(org.apache.commons.httpclient.URI, java.lang.String, boolean):void
      org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0058, code lost:
        if (r5.equals(com.amap.api.search.poisearch.PoiTypeDef.All) != false) goto L_0x005a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HttpMethodBase(java.lang.String r5) {
        /*
            r4 = this;
            r3 = 1
            r2 = 0
            r1 = 0
            r4.<init>()
            org.apache.commons.httpclient.HeaderGroup r0 = new org.apache.commons.httpclient.HeaderGroup
            r0.<init>()
            r4.requestHeaders = r0
            r4.statusLine = r1
            org.apache.commons.httpclient.HeaderGroup r0 = new org.apache.commons.httpclient.HeaderGroup
            r0.<init>()
            r4.responseHeaders = r0
            org.apache.commons.httpclient.HeaderGroup r0 = new org.apache.commons.httpclient.HeaderGroup
            r0.<init>()
            r4.responseTrailerHeaders = r0
            r4.path = r1
            r4.queryString = r1
            r4.responseStream = r1
            r4.responseConnection = r1
            r4.responseBody = r1
            r4.followRedirects = r2
            r4.doAuthentication = r3
            org.apache.commons.httpclient.params.HttpMethodParams r0 = new org.apache.commons.httpclient.params.HttpMethodParams
            r0.<init>()
            r4.params = r0
            org.apache.commons.httpclient.auth.AuthState r0 = new org.apache.commons.httpclient.auth.AuthState
            r0.<init>()
            r4.hostAuthState = r0
            org.apache.commons.httpclient.auth.AuthState r0 = new org.apache.commons.httpclient.auth.AuthState
            r0.<init>()
            r4.proxyAuthState = r0
            r4.used = r2
            r4.recoverableExceptionCount = r2
            r4.httphost = r1
            r4.connectionCloseForced = r2
            r4.effectiveVersion = r1
            r4.aborted = r2
            r4.requestSent = r2
            r4.cookiespec = r1
            if (r5 == 0) goto L_0x005a
            java.lang.String r0 = ""
            boolean r0 = r5.equals(r0)     // Catch:{ URIException -> 0x006e }
            if (r0 == 0) goto L_0x005c
        L_0x005a:
            java.lang.String r5 = "/"
        L_0x005c:
            org.apache.commons.httpclient.params.HttpMethodParams r0 = r4.getParams()     // Catch:{ URIException -> 0x006e }
            java.lang.String r0 = r0.getUriCharset()     // Catch:{ URIException -> 0x006e }
            org.apache.commons.httpclient.URI r1 = new org.apache.commons.httpclient.URI     // Catch:{ URIException -> 0x006e }
            r2 = 1
            r1.<init>(r5, r2, r0)     // Catch:{ URIException -> 0x006e }
            r4.setURI(r1)     // Catch:{ URIException -> 0x006e }
            return
        L_0x006e:
            r0 = move-exception
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r3 = "Invalid uri '"
            r2.<init>(r3)
            java.lang.StringBuffer r2 = r2.append(r5)
            java.lang.String r3 = "': "
            java.lang.StringBuffer r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.HttpMethodBase.<init>(java.lang.String):void");
    }

    private static boolean canResponseHaveBody(int i) {
        LOG.trace("enter HttpMethodBase.canResponseHaveBody(int)");
        return ((i >= 100 && i <= 199) || i == 204 || i == 304) ? false : true;
    }

    private void checkExecuteConditions(HttpState httpState, HttpConnection httpConnection) {
        if (httpState == null) {
            throw new IllegalArgumentException("HttpState parameter may not be null");
        } else if (httpConnection == null) {
            throw new IllegalArgumentException("HttpConnection parameter may not be null");
        } else if (this.aborted) {
            throw new IllegalStateException("Method has been aborted");
        } else if (!validate()) {
            throw new ProtocolException("HttpMethodBase object not valid");
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    private void ensureConnectionRelease() {
        if (this.responseConnection != null) {
            this.responseConnection.releaseConnection();
            this.responseConnection = null;
        }
    }

    protected static String generateRequestLine(HttpConnection httpConnection, String str, String str2, String str3, String str4) {
        LOG.trace("enter HttpMethodBase.generateRequestLine(HttpConnection, String, String, String, String)");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        stringBuffer.append(" ");
        if (!httpConnection.isTransparent()) {
            Protocol protocol = httpConnection.getProtocol();
            stringBuffer.append(protocol.getScheme().toLowerCase());
            stringBuffer.append("://");
            stringBuffer.append(httpConnection.getHost());
            if (!(httpConnection.getPort() == -1 || httpConnection.getPort() == protocol.getDefaultPort())) {
                stringBuffer.append(":");
                stringBuffer.append(httpConnection.getPort());
            }
        }
        if (str2 == null) {
            stringBuffer.append(CookieSpec.PATH_DELIM);
        } else {
            if (!httpConnection.isTransparent() && !str2.startsWith(CookieSpec.PATH_DELIM)) {
                stringBuffer.append(CookieSpec.PATH_DELIM);
            }
            stringBuffer.append(str2);
        }
        if (str3 != null) {
            if (str3.indexOf("?") != 0) {
                stringBuffer.append("?");
            }
            stringBuffer.append(str3);
        }
        stringBuffer.append(" ");
        stringBuffer.append(str4);
        stringBuffer.append("\r\n");
        return stringBuffer.toString();
    }

    private CookieSpec getCookieSpec(HttpState httpState) {
        if (this.cookiespec == null) {
            int cookiePolicy = httpState.getCookiePolicy();
            if (cookiePolicy == -1) {
                this.cookiespec = CookiePolicy.getCookieSpec(this.params.getCookiePolicy());
            } else {
                this.cookiespec = CookiePolicy.getSpecByPolicy(cookiePolicy);
            }
            this.cookiespec.setValidDateFormats((Collection) this.params.getParameter(HttpMethodParams.DATE_PATTERNS));
        }
        return this.cookiespec;
    }

    private String getRequestLine(HttpConnection httpConnection) {
        return generateRequestLine(httpConnection, getName(), getPath(), getQueryString(), this.effectiveVersion.toString());
    }

    private InputStream readResponseBody(HttpConnection httpConnection) {
        InputStream contentLengthInputStream;
        LOG.trace("enter HttpMethodBase.readResponseBody(HttpConnection)");
        this.responseBody = null;
        InputStream responseInputStream = httpConnection.getResponseInputStream();
        InputStream wireLogInputStream = Wire.CONTENT_WIRE.enabled() ? new WireLogInputStream(responseInputStream, Wire.CONTENT_WIRE) : responseInputStream;
        boolean canResponseHaveBody = canResponseHaveBody(this.statusLine.getStatusCode());
        Header firstHeader = this.responseHeaders.getFirstHeader("Transfer-Encoding");
        if (firstHeader != null) {
            String value = firstHeader.getValue();
            if (!"chunked".equalsIgnoreCase(value) && !"identity".equalsIgnoreCase(value) && LOG.isWarnEnabled()) {
                LOG.warn(new StringBuffer("Unsupported transfer encoding: ").append(value).toString());
            }
            HeaderElement[] elements = firstHeader.getElements();
            int length = elements.length;
            if (length <= 0 || !"chunked".equalsIgnoreCase(elements[length - 1].getName())) {
                LOG.info("Response content is not chunk-encoded");
                setConnectionCloseForced(true);
                contentLengthInputStream = wireLogInputStream;
            } else if (httpConnection.isResponseAvailable(httpConnection.getParams().getSoTimeout())) {
                contentLengthInputStream = new ChunkedInputStream(wireLogInputStream, this);
            } else if (getParams().isParameterTrue(HttpMethodParams.STRICT_TRANSFER_ENCODING)) {
                throw new ProtocolException("Chunk-encoded body declared but not sent");
            } else {
                LOG.warn("Chunk-encoded body missing");
                contentLengthInputStream = null;
            }
        } else {
            long responseContentLength = getResponseContentLength();
            if (responseContentLength == -1) {
                if (canResponseHaveBody && this.effectiveVersion.greaterEquals(HttpVersion.HTTP_1_1)) {
                    Header firstHeader2 = this.responseHeaders.getFirstHeader("Connection");
                    if (!"close".equalsIgnoreCase(firstHeader2 != null ? firstHeader2.getValue() : null)) {
                        LOG.info("Response content length is not known");
                        setConnectionCloseForced(true);
                    }
                }
                contentLengthInputStream = wireLogInputStream;
            } else {
                contentLengthInputStream = new ContentLengthInputStream(wireLogInputStream, responseContentLength);
            }
        }
        if (!canResponseHaveBody) {
            contentLengthInputStream = null;
        }
        return contentLengthInputStream != null ? new AutoCloseInputStream(contentLengthInputStream, new ResponseConsumedWatcher(this) {
            private final HttpMethodBase this$0;

            {
                this.this$0 = r1;
            }

            public void responseConsumed() {
                this.this$0.responseBodyConsumed();
            }
        }) : contentLengthInputStream;
    }

    private boolean responseAvailable() {
        return (this.responseBody == null && this.responseStream == null) ? false : true;
    }

    public void abort() {
        if (!this.aborted) {
            this.aborted = true;
            HttpConnection httpConnection = this.responseConnection;
            if (httpConnection != null) {
                httpConnection.close();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void addCookieRequestHeader(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.addCookieRequestHeader(HttpState, HttpConnection)");
        Header[] headers = getRequestHeaderGroup().getHeaders("Cookie");
        for (Header header : headers) {
            if (header.isAutogenerated()) {
                getRequestHeaderGroup().removeHeader(header);
            }
        }
        CookieSpec cookieSpec = getCookieSpec(httpState);
        String virtualHost = this.params.getVirtualHost();
        if (virtualHost == null) {
            virtualHost = httpConnection.getHost();
        }
        Cookie[] match = cookieSpec.match(virtualHost, httpConnection.getPort(), getPath(), httpConnection.isSecure(), httpState.getCookies());
        if (match != null && match.length > 0) {
            if (getParams().isParameterTrue(HttpMethodParams.SINGLE_COOKIE_HEADER)) {
                getRequestHeaderGroup().addHeader(new Header("Cookie", cookieSpec.formatCookies(match), true));
            } else {
                for (Cookie formatCookie : match) {
                    getRequestHeaderGroup().addHeader(new Header("Cookie", cookieSpec.formatCookie(formatCookie), true));
                }
            }
            if (cookieSpec instanceof CookieVersionSupport) {
                CookieVersionSupport cookieVersionSupport = (CookieVersionSupport) cookieSpec;
                int version = cookieVersionSupport.getVersion();
                boolean z = false;
                for (Cookie version2 : match) {
                    if (version != version2.getVersion()) {
                        z = true;
                    }
                }
                if (z) {
                    getRequestHeaderGroup().addHeader(cookieVersionSupport.getVersionHeader());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void addHostRequestHeader(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.addHostRequestHeader(HttpState, HttpConnection)");
        String virtualHost = this.params.getVirtualHost();
        if (virtualHost != null) {
            LOG.debug(new StringBuffer("Using virtual host name: ").append(virtualHost).toString());
        } else {
            virtualHost = httpConnection.getHost();
        }
        int port = httpConnection.getPort();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Adding Host request header");
        }
        if (httpConnection.getProtocol().getDefaultPort() != port) {
            virtualHost = new StringBuffer().append(virtualHost).append(":").append(port).toString();
        }
        setRequestHeader("Host", virtualHost);
    }

    /* access modifiers changed from: protected */
    public void addProxyConnectionHeader(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.addProxyConnectionHeader(HttpState, HttpConnection)");
        if (!httpConnection.isTransparent() && getRequestHeader("Proxy-Connection") == null) {
            addRequestHeader("Proxy-Connection", "Keep-Alive");
        }
    }

    public void addRequestHeader(String str, String str2) {
        addRequestHeader(new Header(str, str2));
    }

    public void addRequestHeader(Header header) {
        LOG.trace("HttpMethodBase.addRequestHeader(Header)");
        if (header == null) {
            LOG.debug("null header value ignored");
        } else {
            getRequestHeaderGroup().addHeader(header);
        }
    }

    /* access modifiers changed from: protected */
    public void addRequestHeaders(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.addRequestHeaders(HttpState, HttpConnection)");
        addUserAgentRequestHeader(httpState, httpConnection);
        addHostRequestHeader(httpState, httpConnection);
        addCookieRequestHeader(httpState, httpConnection);
        addProxyConnectionHeader(httpState, httpConnection);
    }

    public void addResponseFooter(Header header) {
        getResponseTrailerHeaderGroup().addHeader(header);
    }

    /* access modifiers changed from: protected */
    public void addUserAgentRequestHeader(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.addUserAgentRequestHeaders(HttpState, HttpConnection)");
        if (getRequestHeader("User-Agent") == null) {
            String str = (String) getParams().getParameter(HttpMethodParams.USER_AGENT);
            if (str == null) {
                str = "Jakarta Commons-HttpClient";
            }
            setRequestHeader("User-Agent", str);
        }
    }

    /* access modifiers changed from: protected */
    public void checkNotUsed() {
        if (this.used) {
            throw new IllegalStateException("Already used.");
        }
    }

    /* access modifiers changed from: protected */
    public void checkUsed() {
        if (!this.used) {
            throw new IllegalStateException("Not Used.");
        }
    }

    public int execute(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.execute(HttpState, HttpConnection)");
        this.responseConnection = httpConnection;
        checkExecuteConditions(httpState, httpConnection);
        this.statusLine = null;
        this.connectionCloseForced = false;
        httpConnection.setLastResponseInputStream(null);
        if (this.effectiveVersion == null) {
            this.effectiveVersion = this.params.getVersion();
        }
        writeRequest(httpState, httpConnection);
        this.requestSent = true;
        readResponse(httpState, httpConnection);
        this.used = true;
        return this.statusLine.getStatusCode();
    }

    /* access modifiers changed from: package-private */
    public void fakeResponse(StatusLine statusLine2, HeaderGroup headerGroup, InputStream inputStream) {
        this.used = true;
        this.statusLine = statusLine2;
        this.responseHeaders = headerGroup;
        this.responseBody = null;
        this.responseStream = inputStream;
    }

    public String getAuthenticationRealm() {
        return this.hostAuthState.getRealm();
    }

    /* access modifiers changed from: protected */
    public String getContentCharSet(Header header) {
        NameValuePair parameterByName;
        LOG.trace("enter getContentCharSet( Header contentheader )");
        String str = null;
        if (header != null) {
            HeaderElement[] elements = header.getElements();
            if (elements.length == 1 && (parameterByName = elements[0].getParameterByName("charset")) != null) {
                str = parameterByName.getValue();
            }
        }
        if (str == null) {
            str = getParams().getContentCharset();
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Default charset used: ").append(str).toString());
            }
        }
        return str;
    }

    public boolean getDoAuthentication() {
        return this.doAuthentication;
    }

    public HttpVersion getEffectiveVersion() {
        return this.effectiveVersion;
    }

    public boolean getFollowRedirects() {
        return this.followRedirects;
    }

    public AuthState getHostAuthState() {
        return this.hostAuthState;
    }

    public HostConfiguration getHostConfiguration() {
        HostConfiguration hostConfiguration = new HostConfiguration();
        hostConfiguration.setHost(this.httphost);
        return hostConfiguration;
    }

    public MethodRetryHandler getMethodRetryHandler() {
        return this.methodRetryHandler;
    }

    public abstract String getName();

    public HttpMethodParams getParams() {
        return this.params;
    }

    public String getPath() {
        return (this.path == null || this.path.equals(PoiTypeDef.All)) ? CookieSpec.PATH_DELIM : this.path;
    }

    public AuthState getProxyAuthState() {
        return this.proxyAuthState;
    }

    public String getProxyAuthenticationRealm() {
        return this.proxyAuthState.getRealm();
    }

    public String getQueryString() {
        return this.queryString;
    }

    public int getRecoverableExceptionCount() {
        return this.recoverableExceptionCount;
    }

    public String getRequestCharSet() {
        return getContentCharSet(getRequestHeader("Content-Type"));
    }

    public Header getRequestHeader(String str) {
        if (str == null) {
            return null;
        }
        return getRequestHeaderGroup().getCondensedHeader(str);
    }

    /* access modifiers changed from: protected */
    public HeaderGroup getRequestHeaderGroup() {
        return this.requestHeaders;
    }

    public Header[] getRequestHeaders() {
        return getRequestHeaderGroup().getAllHeaders();
    }

    public Header[] getRequestHeaders(String str) {
        return getRequestHeaderGroup().getHeaders(str);
    }

    public byte[] getResponseBody() {
        InputStream responseBodyAsStream;
        if (this.responseBody == null && (responseBodyAsStream = getResponseBodyAsStream()) != null) {
            long responseContentLength = getResponseContentLength();
            if (responseContentLength > 2147483647L) {
                throw new IOException(new StringBuffer("Content too large to be buffered: ").append(responseContentLength).append(" bytes").toString());
            }
            int intParameter = getParams().getIntParameter(HttpMethodParams.BUFFER_WARN_TRIGGER_LIMIT, AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START);
            if (responseContentLength == -1 || responseContentLength > ((long) intParameter)) {
                LOG.warn("Going to buffer response body of large or unknown size. Using getResponseBodyAsStream instead is recommended.");
            }
            LOG.debug("Buffering response body");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(responseContentLength > 0 ? (int) responseContentLength : 4096);
            byte[] bArr = new byte[4096];
            while (true) {
                int read = responseBodyAsStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byteArrayOutputStream.close();
            setResponseStream(null);
            this.responseBody = byteArrayOutputStream.toByteArray();
        }
        return this.responseBody;
    }

    public byte[] getResponseBody(int i) {
        InputStream responseBodyAsStream;
        if (i < 0) {
            throw new IllegalArgumentException("maxlen must be positive");
        }
        if (this.responseBody == null && (responseBodyAsStream = getResponseBodyAsStream()) != null) {
            long responseContentLength = getResponseContentLength();
            if (responseContentLength == -1 || responseContentLength <= ((long) i)) {
                LOG.debug("Buffering response body");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(responseContentLength > 0 ? (int) responseContentLength : 4096);
                byte[] bArr = new byte[2048];
                int i2 = 0;
                do {
                    int read = responseBodyAsStream.read(bArr, 0, Math.min(bArr.length, i - i2));
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                    i2 += read;
                } while (i2 < i);
                setResponseStream(null);
                if (i2 != i || responseBodyAsStream.read() == -1) {
                    this.responseBody = byteArrayOutputStream.toByteArray();
                } else {
                    throw new HttpContentTooLargeException(new StringBuffer("Content-Length not known but larger than ").append(i).toString(), i);
                }
            } else {
                throw new HttpContentTooLargeException(new StringBuffer("Content-Length is ").append(responseContentLength).toString(), i);
            }
        }
        return this.responseBody;
    }

    public InputStream getResponseBodyAsStream() {
        if (this.responseStream != null) {
            return this.responseStream;
        }
        if (this.responseBody == null) {
            return null;
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.responseBody);
        LOG.debug("re-creating response stream from byte array");
        return byteArrayInputStream;
    }

    public String getResponseBodyAsString() {
        byte[] responseBody2 = responseAvailable() ? getResponseBody() : null;
        if (responseBody2 != null) {
            return EncodingUtil.getString(responseBody2, getResponseCharSet());
        }
        return null;
    }

    public String getResponseBodyAsString(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("maxlen must be positive");
        }
        byte[] responseBody2 = responseAvailable() ? getResponseBody(i) : null;
        if (responseBody2 != null) {
            return EncodingUtil.getString(responseBody2, getResponseCharSet());
        }
        return null;
    }

    public String getResponseCharSet() {
        return getContentCharSet(getResponseHeader("Content-Type"));
    }

    public long getResponseContentLength() {
        long j = -1;
        Header[] headers = getResponseHeaderGroup().getHeaders("Content-Length");
        if (headers.length != 0) {
            if (headers.length > 1) {
                LOG.warn("Multiple content-length headers detected");
            }
            int length = headers.length - 1;
            while (length >= 0) {
                try {
                    j = Long.parseLong(headers[length].getValue());
                    break;
                } catch (NumberFormatException e) {
                    if (LOG.isWarnEnabled()) {
                        LOG.warn(new StringBuffer("Invalid content-length value: ").append(e.getMessage()).toString());
                    }
                    length--;
                }
            }
        }
        return j;
    }

    public Header getResponseFooter(String str) {
        if (str == null) {
            return null;
        }
        return getResponseTrailerHeaderGroup().getCondensedHeader(str);
    }

    public Header[] getResponseFooters() {
        return getResponseTrailerHeaderGroup().getAllHeaders();
    }

    public Header getResponseHeader(String str) {
        if (str == null) {
            return null;
        }
        return getResponseHeaderGroup().getCondensedHeader(str);
    }

    /* access modifiers changed from: protected */
    public HeaderGroup getResponseHeaderGroup() {
        return this.responseHeaders;
    }

    public Header[] getResponseHeaders() {
        return getResponseHeaderGroup().getAllHeaders();
    }

    public Header[] getResponseHeaders(String str) {
        return getResponseHeaderGroup().getHeaders(str);
    }

    /* access modifiers changed from: protected */
    public InputStream getResponseStream() {
        return this.responseStream;
    }

    /* access modifiers changed from: protected */
    public HeaderGroup getResponseTrailerHeaderGroup() {
        return this.responseTrailerHeaders;
    }

    public int getStatusCode() {
        return this.statusLine.getStatusCode();
    }

    public StatusLine getStatusLine() {
        return this.statusLine;
    }

    public String getStatusText() {
        return this.statusLine.getReasonPhrase();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      org.apache.commons.httpclient.URI.<init>(java.lang.String, java.lang.String, java.lang.String):void
      org.apache.commons.httpclient.URI.<init>(org.apache.commons.httpclient.URI, java.lang.String, boolean):void
      org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void */
    public URI getURI() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.httphost != null) {
            stringBuffer.append(this.httphost.getProtocol().getScheme());
            stringBuffer.append("://");
            stringBuffer.append(this.httphost.getHostName());
            int port = this.httphost.getPort();
            if (!(port == -1 || port == this.httphost.getProtocol().getDefaultPort())) {
                stringBuffer.append(":");
                stringBuffer.append(port);
            }
        }
        stringBuffer.append(this.path);
        if (this.queryString != null) {
            stringBuffer.append('?');
            stringBuffer.append(this.queryString);
        }
        return new URI(stringBuffer.toString(), true, getParams().getUriCharset());
    }

    public boolean hasBeenUsed() {
        return this.used;
    }

    public boolean isAborted() {
        return this.aborted;
    }

    /* access modifiers changed from: protected */
    public boolean isConnectionCloseForced() {
        return this.connectionCloseForced;
    }

    public boolean isHttp11() {
        return this.params.getVersion().equals(HttpVersion.HTTP_1_1);
    }

    public boolean isRequestSent() {
        return this.requestSent;
    }

    public boolean isStrictMode() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void processCookieHeaders(CookieSpec cookieSpec, Header[] headerArr, HttpState httpState, HttpConnection httpConnection) {
        Cookie[] cookieArr;
        LOG.trace("enter HttpMethodBase.processCookieHeaders(Header[], HttpState, HttpConnection)");
        String virtualHost = this.params.getVirtualHost();
        if (virtualHost == null) {
            virtualHost = httpConnection.getHost();
        }
        for (Header header : headerArr) {
            try {
                cookieArr = cookieSpec.parse(virtualHost, httpConnection.getPort(), getPath(), httpConnection.isSecure(), header);
            } catch (MalformedCookieException e) {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(new StringBuffer("Invalid cookie header: \"").append(header.getValue()).append("\". ").append(e.getMessage()).toString());
                }
                cookieArr = null;
            }
            if (cookieArr != null) {
                for (Cookie cookie : cookieArr) {
                    try {
                        cookieSpec.validate(virtualHost, httpConnection.getPort(), getPath(), httpConnection.isSecure(), cookie);
                        httpState.addCookie(cookie);
                        if (LOG.isDebugEnabled()) {
                            LOG.debug(new StringBuffer("Cookie accepted: \"").append(cookieSpec.formatCookie(cookie)).append("\"").toString());
                        }
                    } catch (MalformedCookieException e2) {
                        if (LOG.isWarnEnabled()) {
                            LOG.warn(new StringBuffer("Cookie rejected: \"").append(cookieSpec.formatCookie(cookie)).append("\". ").append(e2.getMessage()).toString());
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void processResponseBody(HttpState httpState, HttpConnection httpConnection) {
    }

    /* access modifiers changed from: protected */
    public void processResponseHeaders(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.processResponseHeaders(HttpState, HttpConnection)");
        CookieSpec cookieSpec = getCookieSpec(httpState);
        processCookieHeaders(cookieSpec, getResponseHeaderGroup().getHeaders(RFC2109Spec.SET_COOKIE_KEY), httpState, httpConnection);
        if ((cookieSpec instanceof CookieVersionSupport) && ((CookieVersionSupport) cookieSpec).getVersion() > 0) {
            processCookieHeaders(cookieSpec, getResponseHeaderGroup().getHeaders(RFC2965Spec.SET_COOKIE2_KEY), httpState, httpConnection);
        }
    }

    /* access modifiers changed from: protected */
    public void processStatusLine(HttpState httpState, HttpConnection httpConnection) {
    }

    /* access modifiers changed from: protected */
    public void readResponse(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.readResponse(HttpState, HttpConnection)");
        while (this.statusLine == null) {
            readStatusLine(httpState, httpConnection);
            processStatusLine(httpState, httpConnection);
            readResponseHeaders(httpState, httpConnection);
            processResponseHeaders(httpState, httpConnection);
            int statusCode = this.statusLine.getStatusCode();
            if (statusCode >= 100 && statusCode < 200) {
                if (LOG.isInfoEnabled()) {
                    LOG.info(new StringBuffer("Discarding unexpected response: ").append(this.statusLine.toString()).toString());
                }
                this.statusLine = null;
            }
        }
        readResponseBody(httpState, httpConnection);
        processResponseBody(httpState, httpConnection);
    }

    /* access modifiers changed from: protected */
    public void readResponseBody(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.readResponseBody(HttpState, HttpConnection)");
        InputStream readResponseBody = readResponseBody(httpConnection);
        if (readResponseBody == null) {
            responseBodyConsumed();
            return;
        }
        httpConnection.setLastResponseInputStream(readResponseBody);
        setResponseStream(readResponseBody);
    }

    /* access modifiers changed from: protected */
    public void readResponseHeaders(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.readResponseHeaders(HttpState,HttpConnection)");
        getResponseHeaderGroup().clear();
        getResponseHeaderGroup().setHeaders(HttpParser.parseHeaders(httpConnection.getResponseInputStream(), getParams().getHttpElementCharset()));
    }

    /* access modifiers changed from: protected */
    public void readStatusLine(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.readStatusLine(HttpState, HttpConnection)");
        int intParameter = getParams().getIntParameter(HttpMethodParams.STATUS_LINE_GARBAGE_LIMIT, Integer.MAX_VALUE);
        int i = 0;
        while (true) {
            String readLine = httpConnection.readLine(getParams().getHttpElementCharset());
            if (readLine == null && i == 0) {
                throw new NoHttpResponseException(new StringBuffer("The server ").append(httpConnection.getHost()).append(" failed to respond").toString());
            }
            if (Wire.HEADER_WIRE.enabled()) {
                Wire.HEADER_WIRE.input(new StringBuffer().append(readLine).append("\r\n").toString());
            }
            if (readLine != null && StatusLine.startsWithHTTP(readLine)) {
                this.statusLine = new StatusLine(readLine);
                String httpVersion = this.statusLine.getHttpVersion();
                if (!getParams().isParameterFalse(HttpMethodParams.UNAMBIGUOUS_STATUS_LINE) || !httpVersion.equals("HTTP")) {
                    this.effectiveVersion = HttpVersion.parse(httpVersion);
                    return;
                }
                getParams().setVersion(HttpVersion.HTTP_1_0);
                if (LOG.isWarnEnabled()) {
                    LOG.warn(new StringBuffer("Ambiguous status line (HTTP protocol version missing):").append(this.statusLine.toString()).toString());
                    return;
                }
                return;
            } else if (readLine != null && i < intParameter) {
                i++;
            }
        }
        throw new ProtocolException(new StringBuffer("The server ").append(httpConnection.getHost()).append(" failed to respond with a valid HTTP response").toString());
    }

    public void recycle() {
        LOG.trace("enter HttpMethodBase.recycle()");
        releaseConnection();
        this.path = null;
        this.followRedirects = false;
        this.doAuthentication = true;
        this.queryString = null;
        getRequestHeaderGroup().clear();
        getResponseHeaderGroup().clear();
        getResponseTrailerHeaderGroup().clear();
        this.statusLine = null;
        this.effectiveVersion = null;
        this.aborted = false;
        this.used = false;
        this.params = new HttpMethodParams();
        this.responseBody = null;
        this.recoverableExceptionCount = 0;
        this.connectionCloseForced = false;
        this.hostAuthState.invalidate();
        this.proxyAuthState.invalidate();
        this.cookiespec = null;
        this.requestSent = false;
    }

    public void releaseConnection() {
        try {
            if (this.responseStream != null) {
                try {
                    this.responseStream.close();
                } catch (IOException e) {
                }
            }
        } finally {
            ensureConnectionRelease();
        }
    }

    public void removeRequestHeader(String str) {
        Header[] headers = getRequestHeaderGroup().getHeaders(str);
        for (Header removeHeader : headers) {
            getRequestHeaderGroup().removeHeader(removeHeader);
        }
    }

    public void removeRequestHeader(Header header) {
        if (header != null) {
            getRequestHeaderGroup().removeHeader(header);
        }
    }

    /* access modifiers changed from: protected */
    public void responseBodyConsumed() {
        this.responseStream = null;
        if (this.responseConnection != null) {
            this.responseConnection.setLastResponseInputStream(null);
            if (shouldCloseConnection(this.responseConnection)) {
                this.responseConnection.close();
            } else {
                try {
                    if (this.responseConnection.isResponseAvailable()) {
                        if (getParams().isParameterTrue(HttpMethodParams.WARN_EXTRA_INPUT)) {
                            LOG.warn("Extra response data detected - closing connection");
                        }
                        this.responseConnection.close();
                    }
                } catch (IOException e) {
                    LOG.warn(e.getMessage());
                    this.responseConnection.close();
                }
            }
        }
        this.connectionCloseForced = false;
        ensureConnectionRelease();
    }

    /* access modifiers changed from: protected */
    public void setConnectionCloseForced(boolean z) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("Force-close connection: ").append(z).toString());
        }
        this.connectionCloseForced = z;
    }

    public void setDoAuthentication(boolean z) {
        this.doAuthentication = z;
    }

    public void setFollowRedirects(boolean z) {
        this.followRedirects = z;
    }

    public void setHostConfiguration(HostConfiguration hostConfiguration) {
        if (hostConfiguration != null) {
            this.httphost = new HttpHost(hostConfiguration.getHost(), hostConfiguration.getPort(), hostConfiguration.getProtocol());
        } else {
            this.httphost = null;
        }
    }

    public void setHttp11(boolean z) {
        if (z) {
            this.params.setVersion(HttpVersion.HTTP_1_1);
        } else {
            this.params.setVersion(HttpVersion.HTTP_1_0);
        }
    }

    public void setMethodRetryHandler(MethodRetryHandler methodRetryHandler2) {
        this.methodRetryHandler = methodRetryHandler2;
    }

    public void setParams(HttpMethodParams httpMethodParams) {
        if (httpMethodParams == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = httpMethodParams;
    }

    public void setPath(String str) {
        this.path = str;
    }

    public void setQueryString(String str) {
        this.queryString = str;
    }

    public void setQueryString(NameValuePair[] nameValuePairArr) {
        LOG.trace("enter HttpMethodBase.setQueryString(NameValuePair[])");
        this.queryString = EncodingUtil.formUrlEncode(nameValuePairArr, e.f);
    }

    public void setRequestHeader(String str, String str2) {
        setRequestHeader(new Header(str, str2));
    }

    public void setRequestHeader(Header header) {
        Header[] headers = getRequestHeaderGroup().getHeaders(header.getName());
        for (Header removeHeader : headers) {
            getRequestHeaderGroup().removeHeader(removeHeader);
        }
        getRequestHeaderGroup().addHeader(header);
    }

    /* access modifiers changed from: protected */
    public void setResponseStream(InputStream inputStream) {
        this.responseStream = inputStream;
    }

    public void setStrictMode(boolean z) {
        if (z) {
            this.params.makeStrict();
        } else {
            this.params.makeLenient();
        }
    }

    public void setURI(URI uri) {
        if (uri.isAbsoluteURI()) {
            this.httphost = new HttpHost(uri);
        }
        setPath(uri.getPath() == null ? CookieSpec.PATH_DELIM : uri.getEscapedPath());
        setQueryString(uri.getEscapedQuery());
    }

    /* access modifiers changed from: protected */
    public boolean shouldCloseConnection(HttpConnection httpConnection) {
        if (isConnectionCloseForced()) {
            LOG.debug("Should force-close connection.");
            return true;
        }
        Header header = null;
        if (!httpConnection.isTransparent()) {
            header = this.responseHeaders.getFirstHeader("proxy-connection");
        }
        if (header == null) {
            header = this.responseHeaders.getFirstHeader("connection");
        }
        if (header == null) {
            header = this.requestHeaders.getFirstHeader("connection");
        }
        if (header != null) {
            if (header.getValue().equalsIgnoreCase("close")) {
                if (!LOG.isDebugEnabled()) {
                    return true;
                }
                LOG.debug(new StringBuffer("Should close connection in response to directive: ").append(header.getValue()).toString());
                return true;
            } else if (header.getValue().equalsIgnoreCase("keep-alive")) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(new StringBuffer("Should NOT close connection in response to directive: ").append(header.getValue()).toString());
                }
                return false;
            } else if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Unknown directive: ").append(header.toExternalForm()).toString());
            }
        }
        LOG.debug("Resorting to protocol version default close connection policy");
        if (this.effectiveVersion.greaterEquals(HttpVersion.HTTP_1_1)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Should NOT close connection, using ").append(this.effectiveVersion.toString()).toString());
            }
        } else if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("Should close connection, using ").append(this.effectiveVersion.toString()).toString());
        }
        return this.effectiveVersion.lessEquals(HttpVersion.HTTP_1_0);
    }

    public boolean validate() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void writeRequest(HttpState httpState, HttpConnection httpConnection) {
        String str = null;
        LOG.trace("enter HttpMethodBase.writeRequest(HttpState, HttpConnection)");
        writeRequestLine(httpState, httpConnection);
        writeRequestHeaders(httpState, httpConnection);
        httpConnection.writeLine();
        if (Wire.HEADER_WIRE.enabled()) {
            Wire.HEADER_WIRE.output("\r\n");
        }
        HttpVersion version = getParams().getVersion();
        Header requestHeader = getRequestHeader("Expect");
        if (requestHeader != null) {
            str = requestHeader.getValue();
        }
        if (str != null && str.compareToIgnoreCase("100-continue") == 0) {
            if (version.greaterEquals(HttpVersion.HTTP_1_1)) {
                httpConnection.flushRequestOutputStream();
                int soTimeout = httpConnection.getParams().getSoTimeout();
                try {
                    httpConnection.setSocketTimeout(RESPONSE_WAIT_TIME_MS);
                    readStatusLine(httpState, httpConnection);
                    processStatusLine(httpState, httpConnection);
                    readResponseHeaders(httpState, httpConnection);
                    processResponseHeaders(httpState, httpConnection);
                    if (this.statusLine.getStatusCode() == 100) {
                        this.statusLine = null;
                        LOG.debug("OK to continue received");
                    } else {
                        httpConnection.setSocketTimeout(soTimeout);
                        return;
                    }
                } catch (InterruptedIOException e) {
                    if (!ExceptionUtil.isSocketTimeoutException(e)) {
                        throw e;
                    }
                    removeRequestHeader("Expect");
                    LOG.info("100 (continue) read timeout. Resume sending the request");
                } finally {
                    httpConnection.setSocketTimeout(soTimeout);
                }
            } else {
                removeRequestHeader("Expect");
                LOG.info("'Expect: 100-continue' handshake is only supported by HTTP/1.1 or higher");
            }
        }
        writeRequestBody(httpState, httpConnection);
        httpConnection.flushRequestOutputStream();
    }

    /* access modifiers changed from: protected */
    public boolean writeRequestBody(HttpState httpState, HttpConnection httpConnection) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void writeRequestHeaders(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.writeRequestHeaders(HttpState,HttpConnection)");
        addRequestHeaders(httpState, httpConnection);
        String httpElementCharset = getParams().getHttpElementCharset();
        Header[] requestHeaders2 = getRequestHeaders();
        for (Header externalForm : requestHeaders2) {
            String externalForm2 = externalForm.toExternalForm();
            if (Wire.HEADER_WIRE.enabled()) {
                Wire.HEADER_WIRE.output(externalForm2);
            }
            httpConnection.print(externalForm2, httpElementCharset);
        }
    }

    /* access modifiers changed from: protected */
    public void writeRequestLine(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter HttpMethodBase.writeRequestLine(HttpState, HttpConnection)");
        String requestLine = getRequestLine(httpConnection);
        if (Wire.HEADER_WIRE.enabled()) {
            Wire.HEADER_WIRE.output(requestLine);
        }
        httpConnection.print(requestLine, getParams().getHttpElementCharset());
    }
}
