package org.apache.commons.httpclient;

import org.apache.commons.httpclient.protocol.Protocol;

public class ProxyHost extends HttpHost {
    public ProxyHost(String str) {
        this(str, -1);
    }

    public ProxyHost(String str, int i) {
        super(str, i, Protocol.getProtocol("http"));
    }

    public ProxyHost(ProxyHost proxyHost) {
        super(proxyHost);
    }

    public Object clone() {
        return (ProxyHost) super.clone();
    }
}
