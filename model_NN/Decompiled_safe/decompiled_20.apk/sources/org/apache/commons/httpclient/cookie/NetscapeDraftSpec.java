package org.apache.commons.httpclient.cookie;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HeaderElement;
import org.apache.commons.httpclient.NameValuePair;

public class NetscapeDraftSpec extends CookieSpecBase {
    private static boolean isSpecialDomain(String str) {
        String upperCase = str.toUpperCase();
        return upperCase.endsWith(".COM") || upperCase.endsWith(".EDU") || upperCase.endsWith(".NET") || upperCase.endsWith(".GOV") || upperCase.endsWith(".MIL") || upperCase.endsWith(".ORG") || upperCase.endsWith(".INT");
    }

    public boolean domainMatch(String str, String str2) {
        return str.endsWith(str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, boolean):void
      org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, boolean):void */
    public Cookie[] parse(String str, int i, String str2, boolean z, String str3) {
        String str4;
        LOG.trace("enter NetscapeDraftSpec.parse(String, port, path, boolean, Header)");
        if (str == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (str.trim().equals(PoiTypeDef.All)) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (i < 0) {
            throw new IllegalArgumentException(new StringBuffer("Invalid port: ").append(i).toString());
        } else if (str2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else if (str3 == null) {
            throw new IllegalArgumentException("Header may not be null.");
        } else {
            if (str2.trim().equals(PoiTypeDef.All)) {
                str2 = CookieSpec.PATH_DELIM;
            }
            String lowerCase = str.toLowerCase();
            int lastIndexOf = str2.lastIndexOf(CookieSpec.PATH_DELIM);
            if (lastIndexOf >= 0) {
                if (lastIndexOf == 0) {
                    lastIndexOf = 1;
                }
                str4 = str2.substring(0, lastIndexOf);
            } else {
                str4 = str2;
            }
            HeaderElement headerElement = new HeaderElement(str3.toCharArray());
            Cookie cookie = new Cookie(lowerCase, headerElement.getName(), headerElement.getValue(), str4, (Date) null, false);
            NameValuePair[] parameters = headerElement.getParameters();
            if (parameters != null) {
                for (NameValuePair parseAttribute : parameters) {
                    parseAttribute(parseAttribute, cookie);
                }
            }
            return new Cookie[]{cookie};
        }
    }

    public void parseAttribute(NameValuePair nameValuePair, Cookie cookie) {
        if (nameValuePair == null) {
            throw new IllegalArgumentException("Attribute may not be null.");
        } else if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null.");
        } else {
            String lowerCase = nameValuePair.getName().toLowerCase();
            String value = nameValuePair.getValue();
            if (!lowerCase.equals("expires")) {
                super.parseAttribute(nameValuePair, cookie);
            } else if (value == null) {
                throw new MalformedCookieException("Missing value for expires attribute");
            } else {
                try {
                    cookie.setExpiryDate(new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss z", Locale.US).parse(value));
                } catch (ParseException e) {
                    throw new MalformedCookieException(new StringBuffer("Invalid expires attribute: ").append(e.getMessage()).toString());
                }
            }
        }
    }

    public void validate(String str, int i, String str2, boolean z, Cookie cookie) {
        LOG.trace("enterNetscapeDraftCookieProcessor RCF2109CookieProcessor.validate(Cookie)");
        super.validate(str, i, str2, z, cookie);
        if (str.indexOf(".") >= 0) {
            int countTokens = new StringTokenizer(cookie.getDomain(), ".").countTokens();
            if (isSpecialDomain(cookie.getDomain())) {
                if (countTokens < 2) {
                    throw new MalformedCookieException(new StringBuffer("Domain attribute \"").append(cookie.getDomain()).append("\" violates the Netscape cookie specification for special domains").toString());
                }
            } else if (countTokens < 3) {
                throw new MalformedCookieException(new StringBuffer("Domain attribute \"").append(cookie.getDomain()).append("\" violates the Netscape cookie specification").toString());
            }
        }
    }
}
