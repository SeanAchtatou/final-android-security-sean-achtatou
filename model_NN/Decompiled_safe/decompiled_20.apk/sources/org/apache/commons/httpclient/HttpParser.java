package org.apache.commons.httpclient;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HttpParser {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$HttpParser;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HttpParser == null) {
            cls = class$("org.apache.commons.httpclient.HttpParser");
            class$org$apache$commons$httpclient$HttpParser = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HttpParser;
        }
        LOG = LogFactory.getLog(cls);
    }

    private HttpParser() {
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public static Header[] parseHeaders(InputStream inputStream) {
        LOG.trace("enter HeaderParser.parseHeaders(InputStream, String)");
        return parseHeaders(inputStream, "US-ASCII");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0081  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.apache.commons.httpclient.Header[] parseHeaders(java.io.InputStream r8, java.lang.String r9) {
        /*
            r0 = 0
            r7 = 32
            r6 = 0
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpParser.LOG
            java.lang.String r2 = "enter HeaderParser.parseHeaders(InputStream, String)"
            r1.trace(r2)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r1 = r0
        L_0x0011:
            java.lang.String r3 = readLine(r8, r9)
            if (r3 == 0) goto L_0x007f
            java.lang.String r4 = r3.trim()
            int r4 = r4.length()
            if (r4 <= 0) goto L_0x007f
            char r4 = r3.charAt(r6)
            if (r4 == r7) goto L_0x002f
            char r4 = r3.charAt(r6)
            r5 = 9
            if (r4 != r5) goto L_0x003c
        L_0x002f:
            if (r0 == 0) goto L_0x0011
            r0.append(r7)
            java.lang.String r3 = r3.trim()
            r0.append(r3)
            goto L_0x0011
        L_0x003c:
            if (r1 == 0) goto L_0x004a
            org.apache.commons.httpclient.Header r4 = new org.apache.commons.httpclient.Header
            java.lang.String r0 = r0.toString()
            r4.<init>(r1, r0)
            r2.add(r4)
        L_0x004a:
            java.lang.String r0 = ":"
            int r4 = r3.indexOf(r0)
            if (r4 >= 0) goto L_0x0067
            org.apache.commons.httpclient.ProtocolException r0 = new org.apache.commons.httpclient.ProtocolException
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = "Unable to parse header: "
            r1.<init>(r2)
            java.lang.StringBuffer r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0067:
            java.lang.String r0 = r3.substring(r6, r4)
            java.lang.String r1 = r0.trim()
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            int r4 = r4 + 1
            java.lang.String r3 = r3.substring(r4)
            java.lang.String r3 = r3.trim()
            r0.<init>(r3)
            goto L_0x0011
        L_0x007f:
            if (r1 == 0) goto L_0x008d
            org.apache.commons.httpclient.Header r3 = new org.apache.commons.httpclient.Header
            java.lang.String r0 = r0.toString()
            r3.<init>(r1, r0)
            r2.add(r3)
        L_0x008d:
            int r0 = r2.size()
            org.apache.commons.httpclient.Header[] r0 = new org.apache.commons.httpclient.Header[r0]
            java.lang.Object[] r0 = r2.toArray(r0)
            org.apache.commons.httpclient.Header[] r0 = (org.apache.commons.httpclient.Header[]) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.HttpParser.parseHeaders(java.io.InputStream, java.lang.String):org.apache.commons.httpclient.Header[]");
    }

    public static String readLine(InputStream inputStream) {
        LOG.trace("enter HttpParser.readLine(InputStream)");
        return readLine(inputStream, "US-ASCII");
    }

    public static String readLine(InputStream inputStream, String str) {
        LOG.trace("enter HttpParser.readLine(InputStream, String)");
        byte[] readRawLine = readRawLine(inputStream);
        if (readRawLine == null) {
            return null;
        }
        int length = readRawLine.length;
        int i = (length <= 0 || readRawLine[length + -1] != 10) ? 0 : (length <= 1 || readRawLine[length + -2] != 13) ? 1 : 2;
        String string = EncodingUtil.getString(readRawLine, 0, length - i, str);
        if (!Wire.HEADER_WIRE.enabled()) {
            return string;
        }
        Wire.HEADER_WIRE.input(i == 2 ? new StringBuffer().append(string).append("\r\n").toString() : i == 1 ? new StringBuffer().append(string).append("\n").toString() : string);
        return string;
    }

    public static byte[] readRawLine(InputStream inputStream) {
        int read;
        LOG.trace("enter HttpParser.readRawLine()");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        do {
            read = inputStream.read();
            if (read < 0) {
                break;
            }
            byteArrayOutputStream.write(read);
        } while (read != 10);
        if (byteArrayOutputStream.size() == 0) {
            return null;
        }
        return byteArrayOutputStream.toByteArray();
    }
}
