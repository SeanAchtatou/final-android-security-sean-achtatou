package org.apache.commons.httpclient;

import org.apache.commons.httpclient.params.HttpConnectionManagerParams;

public interface HttpConnectionManager {
    void closeIdleConnections(long j);

    HttpConnection getConnection(HostConfiguration hostConfiguration);

    HttpConnection getConnection(HostConfiguration hostConfiguration, long j);

    HttpConnection getConnectionWithTimeout(HostConfiguration hostConfiguration, long j);

    HttpConnectionManagerParams getParams();

    void releaseConnection(HttpConnection httpConnection);

    void setParams(HttpConnectionManagerParams httpConnectionManagerParams);
}
