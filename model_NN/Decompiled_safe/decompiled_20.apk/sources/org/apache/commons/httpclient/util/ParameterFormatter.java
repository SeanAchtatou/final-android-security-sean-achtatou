package org.apache.commons.httpclient.util;

import org.apache.commons.httpclient.NameValuePair;

public class ParameterFormatter {
    private static final char[] SEPARATORS = {'(', ')', '<', '>', '@', ',', ';', ':', '\\', '\"', '/', '[', ']', '?', '=', '{', '}', ' ', 9};
    private static final char[] UNSAFE_CHARS = {'\"', '\\'};
    private boolean alwaysUseQuotes = true;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuffer.insert(int, float):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, boolean):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, double):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.Object):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, long):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, int):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.CharSequence):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.String):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char[]):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer} */
    public static void formatValue(StringBuffer stringBuffer, String str, boolean z) {
        int i = 0;
        if (stringBuffer == null) {
            throw new IllegalArgumentException("String buffer may not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("Value buffer may not be null");
        } else if (z) {
            stringBuffer.append('\"');
            while (i < str.length()) {
                char charAt = str.charAt(i);
                if (isUnsafeChar(charAt)) {
                    stringBuffer.append('\\');
                }
                stringBuffer.append(charAt);
                i++;
            }
            stringBuffer.append('\"');
        } else {
            int length = stringBuffer.length();
            boolean z2 = false;
            while (i < str.length()) {
                char charAt2 = str.charAt(i);
                if (isSeparator(charAt2)) {
                    z2 = true;
                }
                if (isUnsafeChar(charAt2)) {
                    stringBuffer.append('\\');
                }
                stringBuffer.append(charAt2);
                i++;
            }
            if (z2) {
                stringBuffer.insert(length, '\"');
                stringBuffer.append('\"');
            }
        }
    }

    private static boolean isOneOf(char[] cArr, char c) {
        for (char c2 : cArr) {
            if (c == c2) {
                return true;
            }
        }
        return false;
    }

    private static boolean isSeparator(char c) {
        return isOneOf(SEPARATORS, c);
    }

    private static boolean isUnsafeChar(char c) {
        return isOneOf(UNSAFE_CHARS, c);
    }

    public String format(NameValuePair nameValuePair) {
        StringBuffer stringBuffer = new StringBuffer();
        format(stringBuffer, nameValuePair);
        return stringBuffer.toString();
    }

    public void format(StringBuffer stringBuffer, NameValuePair nameValuePair) {
        if (stringBuffer == null) {
            throw new IllegalArgumentException("String buffer may not be null");
        } else if (nameValuePair == null) {
            throw new IllegalArgumentException("Parameter may not be null");
        } else {
            stringBuffer.append(nameValuePair.getName());
            String value = nameValuePair.getValue();
            if (value != null) {
                stringBuffer.append("=");
                formatValue(stringBuffer, value, this.alwaysUseQuotes);
            }
        }
    }

    public boolean isAlwaysUseQuotes() {
        return this.alwaysUseQuotes;
    }

    public void setAlwaysUseQuotes(boolean z) {
        this.alwaysUseQuotes = z;
    }
}
