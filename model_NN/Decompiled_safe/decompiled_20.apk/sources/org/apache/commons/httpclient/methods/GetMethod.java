package org.apache.commons.httpclient.methods;

import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GetMethod extends HttpMethodBase {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$methods$GetMethod;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$methods$GetMethod == null) {
            cls = class$("org.apache.commons.httpclient.methods.GetMethod");
            class$org$apache$commons$httpclient$methods$GetMethod = cls;
        } else {
            cls = class$org$apache$commons$httpclient$methods$GetMethod;
        }
        LOG = LogFactory.getLog(cls);
    }

    public GetMethod() {
        setFollowRedirects(true);
    }

    public GetMethod(String str) {
        super(str);
        LOG.trace("enter GetMethod(String)");
        setFollowRedirects(true);
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public String getName() {
        return "GET";
    }

    public void recycle() {
        LOG.trace("enter GetMethod.recycle()");
        super.recycle();
        setFollowRedirects(true);
    }
}
