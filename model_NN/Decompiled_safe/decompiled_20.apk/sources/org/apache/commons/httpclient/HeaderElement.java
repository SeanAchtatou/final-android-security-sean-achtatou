package org.apache.commons.httpclient;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.util.ParameterParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HeaderElement extends NameValuePair {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$HeaderElement;
    private NameValuePair[] parameters;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HeaderElement == null) {
            cls = class$("org.apache.commons.httpclient.HeaderElement");
            class$org$apache$commons$httpclient$HeaderElement = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HeaderElement;
        }
        LOG = LogFactory.getLog(cls);
    }

    public HeaderElement() {
        this((String) null, (String) null, (NameValuePair[]) null);
    }

    public HeaderElement(String str, String str2) {
        this(str, str2, (NameValuePair[]) null);
    }

    public HeaderElement(String str, String str2, NameValuePair[] nameValuePairArr) {
        super(str, str2);
        this.parameters = null;
        this.parameters = nameValuePairArr;
    }

    public HeaderElement(char[] cArr) {
        this(cArr, 0, cArr.length);
    }

    public HeaderElement(char[] cArr, int i, int i2) {
        this();
        if (cArr != null) {
            List parse = new ParameterParser().parse(cArr, i, i2, ';');
            if (parse.size() > 0) {
                NameValuePair nameValuePair = (NameValuePair) parse.remove(0);
                setName(nameValuePair.getName());
                setValue(nameValuePair.getValue());
                if (parse.size() > 0) {
                    this.parameters = (NameValuePair[]) parse.toArray(new NameValuePair[parse.size()]);
                }
            }
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public static final HeaderElement[] parse(String str) {
        LOG.trace("enter HeaderElement.parse(String)");
        return str == null ? new HeaderElement[0] : parseElements(str.toCharArray());
    }

    public static final HeaderElement[] parseElements(String str) {
        LOG.trace("enter HeaderElement.parseElements(String)");
        return str == null ? new HeaderElement[0] : parseElements(str.toCharArray());
    }

    public static final HeaderElement[] parseElements(char[] cArr) {
        boolean z;
        LOG.trace("enter HeaderElement.parseElements(char[])");
        if (cArr == null) {
            return new HeaderElement[0];
        }
        ArrayList arrayList = new ArrayList();
        int length = cArr.length;
        boolean z2 = false;
        int i = 0;
        int i2 = 0;
        while (i2 < length) {
            char c = cArr[i2];
            if (c == '\"') {
                z = !z2;
            } else {
                z = z2;
            }
            HeaderElement headerElement = null;
            if (!z && c == ',') {
                headerElement = new HeaderElement(cArr, i, i2);
                i = i2 + 1;
            } else if (i2 == length - 1) {
                headerElement = new HeaderElement(cArr, i, length);
            }
            if (!(headerElement == null || headerElement.getName() == null)) {
                arrayList.add(headerElement);
            }
            i2++;
            z2 = z;
        }
        return (HeaderElement[]) arrayList.toArray(new HeaderElement[arrayList.size()]);
    }

    public NameValuePair getParameterByName(String str) {
        LOG.trace("enter HeaderElement.getParameterByName(String)");
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        NameValuePair[] parameters2 = getParameters();
        if (parameters2 != null) {
            for (NameValuePair nameValuePair : parameters2) {
                if (nameValuePair.getName().equalsIgnoreCase(str)) {
                    return nameValuePair;
                }
            }
        }
        return null;
    }

    public NameValuePair[] getParameters() {
        return this.parameters;
    }
}
