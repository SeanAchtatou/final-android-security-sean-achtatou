package org.apache.commons.httpclient.util;

public final class TimeoutController {

    public class TimeoutException extends Exception {
    }

    private TimeoutController() {
    }

    public static void execute(Runnable runnable, long j) {
        Thread thread = new Thread(runnable, "Timeout guard");
        thread.setDaemon(true);
        execute(thread, j);
    }

    public static void execute(Thread thread, long j) {
        thread.start();
        try {
            thread.join(j);
        } catch (InterruptedException e) {
        }
        if (thread.isAlive()) {
            thread.interrupt();
            throw new TimeoutException();
        }
    }
}
