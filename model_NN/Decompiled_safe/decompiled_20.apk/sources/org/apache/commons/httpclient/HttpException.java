package org.apache.commons.httpclient;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

public class HttpException extends IOException {
    static Class class$java$lang$Throwable;
    private final Throwable cause;
    private String reason;
    private int reasonCode;

    public HttpException() {
        this.reasonCode = HttpStatus.SC_OK;
        this.cause = null;
    }

    public HttpException(String str) {
        super(str);
        this.reasonCode = HttpStatus.SC_OK;
        this.cause = null;
    }

    public HttpException(String str, Throwable th) {
        super(str);
        Class cls;
        Class cls2;
        this.reasonCode = HttpStatus.SC_OK;
        this.cause = th;
        try {
            Class[] clsArr = new Class[1];
            if (class$java$lang$Throwable == null) {
                cls = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls;
            } else {
                cls = class$java$lang$Throwable;
            }
            clsArr[0] = cls;
            if (class$java$lang$Throwable == null) {
                cls2 = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls2;
            } else {
                cls2 = class$java$lang$Throwable;
            }
            cls2.getMethod("initCause", clsArr).invoke(this, th);
        } catch (Exception e) {
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public Throwable getCause() {
        return this.cause;
    }

    public String getReason() {
        return this.reason;
    }

    public int getReasonCode() {
        return this.reasonCode;
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream printStream) {
        try {
            getClass().getMethod("getStackTrace", new Class[0]);
            super.printStackTrace(printStream);
        } catch (Exception e) {
            super.printStackTrace(printStream);
            if (this.cause != null) {
                printStream.print("Caused by: ");
                this.cause.printStackTrace(printStream);
            }
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        try {
            getClass().getMethod("getStackTrace", new Class[0]);
            super.printStackTrace(printWriter);
        } catch (Exception e) {
            super.printStackTrace(printWriter);
            if (this.cause != null) {
                printWriter.print("Caused by: ");
                this.cause.printStackTrace(printWriter);
            }
        }
    }

    public void setReason(String str) {
        this.reason = str;
    }

    public void setReasonCode(int i) {
        this.reasonCode = i;
    }
}
