package org.apache.commons.httpclient;

import com.amap.api.location.LocationManagerProxy;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.httpclient.auth.AuthChallengeException;
import org.apache.commons.httpclient.auth.AuthChallengeParser;
import org.apache.commons.httpclient.auth.AuthChallengeProcessor;
import org.apache.commons.httpclient.auth.AuthScheme;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.auth.AuthState;
import org.apache.commons.httpclient.auth.AuthenticationException;
import org.apache.commons.httpclient.auth.CredentialsNotAvailableException;
import org.apache.commons.httpclient.auth.CredentialsProvider;
import org.apache.commons.httpclient.params.HostParams;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

class HttpMethodDirector {
    private static final Log LOG;
    public static final String PROXY_AUTH_CHALLENGE = "Proxy-Authenticate";
    public static final String PROXY_AUTH_RESP = "Proxy-Authorization";
    public static final String WWW_AUTH_CHALLENGE = "WWW-Authenticate";
    public static final String WWW_AUTH_RESP = "Authorization";
    static Class class$org$apache$commons$httpclient$HttpMethodDirector;
    private AuthChallengeProcessor authProcessor = null;
    private HttpConnection conn;
    private ConnectMethod connectMethod;
    private HttpConnectionManager connectionManager;
    private HostConfiguration hostConfiguration;
    private HttpClientParams params;
    private Set redirectLocations = null;
    private boolean releaseConnection = false;
    private HttpState state;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HttpMethodDirector == null) {
            cls = class$("org.apache.commons.httpclient.HttpMethodDirector");
            class$org$apache$commons$httpclient$HttpMethodDirector = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HttpMethodDirector;
        }
        LOG = LogFactory.getLog(cls);
    }

    public HttpMethodDirector(HttpConnectionManager httpConnectionManager, HostConfiguration hostConfiguration2, HttpClientParams httpClientParams, HttpState httpState) {
        this.connectionManager = httpConnectionManager;
        this.hostConfiguration = hostConfiguration2;
        this.params = httpClientParams;
        this.state = httpState;
        this.authProcessor = new AuthChallengeProcessor(this.params);
    }

    private void applyConnectionParams(HttpMethod httpMethod) {
        Object parameter = httpMethod.getParams().getParameter("http.socket.timeout");
        if (parameter == null) {
            parameter = this.conn.getParams().getParameter("http.socket.timeout");
        }
        this.conn.setSocketTimeout(parameter != null ? ((Integer) parameter).intValue() : 0);
    }

    private void authenticate(HttpMethod httpMethod) {
        try {
            if (this.conn.isProxied() && !this.conn.isSecure()) {
                authenticateProxy(httpMethod);
            }
            authenticateHost(httpMethod);
        } catch (AuthenticationException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private void authenticateHost(HttpMethod httpMethod) {
        AuthState hostAuthState;
        AuthScheme authScheme;
        if (!cleanAuthHeaders(httpMethod, "Authorization") || (authScheme = (hostAuthState = httpMethod.getHostAuthState()).getAuthScheme()) == null) {
            return;
        }
        if (hostAuthState.isAuthRequested() || !authScheme.isConnectionBased()) {
            String virtualHost = httpMethod.getParams().getVirtualHost();
            if (virtualHost == null) {
                virtualHost = this.conn.getHost();
            }
            AuthScope authScope = new AuthScope(virtualHost, this.conn.getPort(), authScheme.getRealm(), authScheme.getSchemeName());
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Authenticating with ").append(authScope).toString());
            }
            Credentials credentials = this.state.getCredentials(authScope);
            if (credentials != null) {
                String authenticate = authScheme.authenticate(credentials, httpMethod);
                if (authenticate != null) {
                    httpMethod.addRequestHeader(new Header("Authorization", authenticate, true));
                }
            } else if (LOG.isWarnEnabled()) {
                LOG.warn(new StringBuffer("Required credentials not available for ").append(authScope).toString());
                if (httpMethod.getHostAuthState().isPreemptive()) {
                    LOG.warn("Preemptive authentication requested but no default credentials available");
                }
            }
        }
    }

    private void authenticateProxy(HttpMethod httpMethod) {
        AuthState proxyAuthState;
        AuthScheme authScheme;
        if (!cleanAuthHeaders(httpMethod, "Proxy-Authorization") || (authScheme = (proxyAuthState = httpMethod.getProxyAuthState()).getAuthScheme()) == null) {
            return;
        }
        if (proxyAuthState.isAuthRequested() || !authScheme.isConnectionBased()) {
            AuthScope authScope = new AuthScope(this.conn.getProxyHost(), this.conn.getProxyPort(), authScheme.getRealm(), authScheme.getSchemeName());
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Authenticating with ").append(authScope).toString());
            }
            Credentials proxyCredentials = this.state.getProxyCredentials(authScope);
            if (proxyCredentials != null) {
                String authenticate = authScheme.authenticate(proxyCredentials, httpMethod);
                if (authenticate != null) {
                    httpMethod.addRequestHeader(new Header("Proxy-Authorization", authenticate, true));
                }
            } else if (LOG.isWarnEnabled()) {
                LOG.warn(new StringBuffer("Required proxy credentials not available for ").append(authScope).toString());
                if (httpMethod.getProxyAuthState().isPreemptive()) {
                    LOG.warn("Preemptive authentication requested but no default proxy credentials available");
                }
            }
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    private boolean cleanAuthHeaders(HttpMethod httpMethod, String str) {
        Header[] requestHeaders = httpMethod.getRequestHeaders(str);
        boolean z = true;
        for (Header header : requestHeaders) {
            if (header.isAutogenerated()) {
                httpMethod.removeRequestHeader(header);
            } else {
                z = false;
            }
        }
        return z;
    }

    private boolean executeConnect() {
        int statusCode;
        this.connectMethod = new ConnectMethod(this.hostConfiguration);
        this.connectMethod.getParams().setDefaults(this.hostConfiguration.getParams());
        while (true) {
            if (!this.conn.isOpen()) {
                this.conn.open();
            }
            if (this.params.isAuthenticationPreemptive() || this.state.isAuthenticationPreemptive()) {
                LOG.debug("Preemptively sending default basic credentials");
                this.connectMethod.getProxyAuthState().setPreemptive();
                this.connectMethod.getProxyAuthState().setAuthAttempted(true);
            }
            try {
                authenticateProxy(this.connectMethod);
            } catch (AuthenticationException e) {
                LOG.error(e.getMessage(), e);
            }
            applyConnectionParams(this.connectMethod);
            this.connectMethod.execute(this.state, this.conn);
            statusCode = this.connectMethod.getStatusCode();
            AuthState proxyAuthState = this.connectMethod.getProxyAuthState();
            proxyAuthState.setAuthRequested(statusCode == 407);
            if (!(proxyAuthState.isAuthRequested() && processAuthenticationResponse(this.connectMethod))) {
                break;
            } else if (this.connectMethod.getResponseBodyAsStream() != null) {
                this.connectMethod.getResponseBodyAsStream().close();
            }
        }
        if (statusCode < 200 || statusCode >= 300) {
            this.conn.close();
            return false;
        }
        this.conn.tunnelCreated();
        this.connectMethod = null;
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00be, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c5, code lost:
        if (r9.conn.isOpen() != false) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c7, code lost:
        org.apache.commons.httpclient.HttpMethodDirector.LOG.debug("Closing the connection.");
        r9.conn.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d3, code lost:
        r9.releaseConnection = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d5, code lost:
        throw r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00be A[ExcHandler: RuntimeException (r1v0 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:2:0x0004] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void executeWithRetry(org.apache.commons.httpclient.HttpMethod r10) {
        /*
            r9 = this;
            r8 = 1
            r5 = 0
        L_0x0002:
            int r5 = r5 + 1
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            boolean r1 = r1.isTraceEnabled()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            if (r1 == 0) goto L_0x0026
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            java.lang.String r3 = "Attempt number "
            r2.<init>(r3)     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            java.lang.StringBuffer r2 = r2.append(r5)     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            java.lang.String r3 = " to process request"
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            java.lang.String r2 = r2.toString()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            r1.trace(r2)     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
        L_0x0026:
            org.apache.commons.httpclient.HttpConnection r1 = r9.conn     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            org.apache.commons.httpclient.params.HttpConnectionParams r1 = r1.getParams()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            boolean r1 = r1.isStaleCheckingEnabled()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            if (r1 == 0) goto L_0x0037
            org.apache.commons.httpclient.HttpConnection r1 = r9.conn     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            r1.closeIfStale()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
        L_0x0037:
            org.apache.commons.httpclient.HttpConnection r1 = r9.conn     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            boolean r1 = r1.isOpen()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            if (r1 != 0) goto L_0x005f
            org.apache.commons.httpclient.HttpConnection r1 = r9.conn     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            r1.open()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            org.apache.commons.httpclient.HttpConnection r1 = r9.conn     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            boolean r1 = r1.isProxied()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            if (r1 == 0) goto L_0x005f
            org.apache.commons.httpclient.HttpConnection r1 = r9.conn     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            boolean r1 = r1.isSecure()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            if (r1 == 0) goto L_0x005f
            boolean r1 = r10 instanceof org.apache.commons.httpclient.ConnectMethod     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            if (r1 != 0) goto L_0x005f
            boolean r1 = r9.executeConnect()     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            if (r1 != 0) goto L_0x005f
        L_0x005e:
            return
        L_0x005f:
            r9.applyConnectionParams(r10)     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            org.apache.commons.httpclient.HttpState r1 = r9.state     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            org.apache.commons.httpclient.HttpConnection r2 = r9.conn     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            r10.execute(r1, r2)     // Catch:{ HttpException -> 0x006a, IOException -> 0x0084, RuntimeException -> 0x00be }
            goto L_0x005e
        L_0x006a:
            r1 = move-exception
            throw r1     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
        L_0x006c:
            r1 = move-exception
            org.apache.commons.httpclient.HttpConnection r2 = r9.conn
            boolean r2 = r2.isOpen()
            if (r2 == 0) goto L_0x0081
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG
            java.lang.String r3 = "Closing the connection."
            r2.debug(r3)
            org.apache.commons.httpclient.HttpConnection r2 = r9.conn
            r2.close()
        L_0x0081:
            r9.releaseConnection = r8
            throw r1
        L_0x0084:
            r1 = move-exception
            r7 = r1
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r2 = "Closing the connection."
            r1.debug(r2)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            org.apache.commons.httpclient.HttpConnection r1 = r9.conn     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            r1.close()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            boolean r1 = r10 instanceof org.apache.commons.httpclient.HttpMethodBase     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            if (r1 == 0) goto L_0x00d6
            r0 = r10
            org.apache.commons.httpclient.HttpMethodBase r0 = (org.apache.commons.httpclient.HttpMethodBase) r0     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            r1 = r0
            org.apache.commons.httpclient.MethodRetryHandler r1 = r1.getMethodRetryHandler()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            if (r1 == 0) goto L_0x00d6
            org.apache.commons.httpclient.HttpConnection r3 = r9.conn     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            org.apache.commons.httpclient.HttpRecoverableException r4 = new org.apache.commons.httpclient.HttpRecoverableException     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r2 = r7.getMessage()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            r4.<init>(r2)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            boolean r6 = r10.isRequestSent()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            r2 = r10
            boolean r1 = r1.retryMethod(r2, r3, r4, r5, r6)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            if (r1 != 0) goto L_0x00d6
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r2 = "Method retry handler returned false. Automatic recovery will not be attempted"
            r1.debug(r2)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            throw r7     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
        L_0x00be:
            r1 = move-exception
            org.apache.commons.httpclient.HttpConnection r2 = r9.conn
            boolean r2 = r2.isOpen()
            if (r2 == 0) goto L_0x00d3
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG
            java.lang.String r3 = "Closing the connection."
            r2.debug(r3)
            org.apache.commons.httpclient.HttpConnection r2 = r9.conn
            r2.close()
        L_0x00d3:
            r9.releaseConnection = r8
            throw r1
        L_0x00d6:
            org.apache.commons.httpclient.params.HttpMethodParams r1 = r10.getParams()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r2 = "http.method.retry-handler"
            java.lang.Object r1 = r1.getParameter(r2)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            org.apache.commons.httpclient.HttpMethodRetryHandler r1 = (org.apache.commons.httpclient.HttpMethodRetryHandler) r1     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            if (r1 != 0) goto L_0x00e9
            org.apache.commons.httpclient.DefaultHttpMethodRetryHandler r1 = new org.apache.commons.httpclient.DefaultHttpMethodRetryHandler     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            r1.<init>()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
        L_0x00e9:
            boolean r1 = r1.retryMethod(r10, r7, r5)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            if (r1 != 0) goto L_0x00f7
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r2 = "Method retry handler returned false. Automatic recovery will not be attempted"
            r1.debug(r2)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            throw r7     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
        L_0x00f7:
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            boolean r1 = r1.isInfoEnabled()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            if (r1 == 0) goto L_0x0129
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r3 = "I/O exception ("
            r2.<init>(r3)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.Class r3 = r7.getClass()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r3 = r3.getName()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r3 = ") caught when processing request: "
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r3 = r7.getMessage()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            r1.info(r2)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
        L_0x0129:
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            if (r1 == 0) goto L_0x013a
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r2 = r7.getMessage()     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            r1.debug(r2, r7)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
        L_0x013a:
            org.apache.commons.logging.Log r1 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            java.lang.String r2 = "Retrying request"
            r1.info(r2)     // Catch:{ IOException -> 0x006c, RuntimeException -> 0x00be }
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.HttpMethodDirector.executeWithRetry(org.apache.commons.httpclient.HttpMethod):void");
    }

    private void fakeResponse(HttpMethod httpMethod) {
        LOG.debug("CONNECT failed, fake the response for the original method");
        if (httpMethod instanceof HttpMethodBase) {
            ((HttpMethodBase) httpMethod).fakeResponse(this.connectMethod.getStatusLine(), this.connectMethod.getResponseHeaderGroup(), this.connectMethod.getResponseBodyAsStream());
            httpMethod.getProxyAuthState().setAuthScheme(this.connectMethod.getProxyAuthState().getAuthScheme());
            this.connectMethod = null;
            return;
        }
        this.releaseConnection = true;
        LOG.warn("Unable to fake response on method as it is not derived from HttpMethodBase.");
    }

    private boolean isAuthenticationNeeded(HttpMethod httpMethod) {
        httpMethod.getHostAuthState().setAuthRequested(httpMethod.getStatusCode() == 401);
        httpMethod.getProxyAuthState().setAuthRequested(httpMethod.getStatusCode() == 407);
        if (!httpMethod.getHostAuthState().isAuthRequested() && !httpMethod.getProxyAuthState().isAuthRequested()) {
            return false;
        }
        LOG.debug("Authorization required");
        if (httpMethod.getDoAuthentication()) {
            return true;
        }
        LOG.info("Authentication requested but doAuthentication is disabled");
        return false;
    }

    private boolean isRedirectNeeded(HttpMethod httpMethod) {
        switch (httpMethod.getStatusCode()) {
            case HttpStatus.SC_MOVED_PERMANENTLY /*301*/:
            case HttpStatus.SC_MOVED_TEMPORARILY /*302*/:
            case HttpStatus.SC_SEE_OTHER /*303*/:
            case HttpStatus.SC_TEMPORARY_REDIRECT /*307*/:
                LOG.debug("Redirect required");
                return httpMethod.getFollowRedirects();
            case HttpStatus.SC_NOT_MODIFIED /*304*/:
            case HttpStatus.SC_USE_PROXY /*305*/:
            case 306:
            default:
                return false;
        }
    }

    private boolean processAuthenticationResponse(HttpMethod httpMethod) {
        LOG.trace("enter HttpMethodBase.processAuthenticationResponse(HttpState, HttpConnection)");
        try {
            switch (httpMethod.getStatusCode()) {
                case HttpStatus.SC_UNAUTHORIZED /*401*/:
                    return processWWWAuthChallenge(httpMethod);
                case HttpStatus.SC_PROXY_AUTHENTICATION_REQUIRED /*407*/:
                    return processProxyAuthChallenge(httpMethod);
                default:
                    return false;
            }
        } catch (Exception e) {
            if (!LOG.isErrorEnabled()) {
                return false;
            }
            LOG.error(e.getMessage(), e);
            return false;
        }
    }

    private boolean processProxyAuthChallenge(HttpMethod httpMethod) {
        AuthState proxyAuthState = httpMethod.getProxyAuthState();
        Map parseChallenges = AuthChallengeParser.parseChallenges(httpMethod.getResponseHeaders("Proxy-Authenticate"));
        if (parseChallenges.isEmpty()) {
            LOG.debug("Proxy authentication challenge(s) not found");
            return false;
        }
        AuthScheme authScheme = null;
        try {
            authScheme = this.authProcessor.processChallenge(proxyAuthState, parseChallenges);
        } catch (AuthChallengeException e) {
            if (LOG.isWarnEnabled()) {
                LOG.warn(e.getMessage());
            }
        }
        if (authScheme == null) {
            return false;
        }
        AuthScope authScope = new AuthScope(this.conn.getProxyHost(), this.conn.getProxyPort(), authScheme.getRealm(), authScheme.getSchemeName());
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("Proxy authentication scope: ").append(authScope).toString());
        }
        if (!proxyAuthState.isAuthAttempted() || !authScheme.isComplete()) {
            proxyAuthState.setAuthAttempted(true);
            Credentials proxyCredentials = this.state.getProxyCredentials(authScope);
            if (proxyCredentials == null) {
                proxyCredentials = promptForProxyCredentials(authScheme, httpMethod.getParams(), authScope);
            }
            if (proxyCredentials != null) {
                return true;
            }
            if (!LOG.isInfoEnabled()) {
                return false;
            }
            LOG.info(new StringBuffer("No credentials available for ").append(authScope).toString());
            return false;
        } else if (promptForProxyCredentials(authScheme, httpMethod.getParams(), authScope) != null) {
            return true;
        } else {
            if (!LOG.isInfoEnabled()) {
                return false;
            }
            LOG.info(new StringBuffer("Failure authenticating with ").append(authScope).toString());
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      org.apache.commons.httpclient.URI.<init>(java.lang.String, java.lang.String, java.lang.String):void
      org.apache.commons.httpclient.URI.<init>(org.apache.commons.httpclient.URI, java.lang.String, boolean):void
      org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void */
    private boolean processRedirectResponse(HttpMethod httpMethod) {
        URI uri;
        Header responseHeader = httpMethod.getResponseHeader(LocationManagerProxy.KEY_LOCATION_CHANGED);
        if (responseHeader == null) {
            LOG.error(new StringBuffer("Received redirect response ").append(httpMethod.getStatusCode()).append(" but no location header").toString());
            return false;
        }
        String value = responseHeader.getValue();
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("Redirect requested to location '").append(value).append("'").toString());
        }
        try {
            URI uri2 = new URI(this.conn.getProtocol().getScheme(), (String) null, this.conn.getHost(), this.conn.getPort(), httpMethod.getPath());
            URI uri3 = new URI(value, true, httpMethod.getParams().getUriCharset());
            if (!uri3.isRelativeURI()) {
                httpMethod.getParams().setDefaults(this.params);
                uri = uri3;
            } else if (this.params.isParameterTrue(HttpClientParams.REJECT_RELATIVE_REDIRECT)) {
                LOG.warn(new StringBuffer("Relative redirect location '").append(value).append("' not allowed").toString());
                return false;
            } else {
                LOG.debug("Redirect URI is not absolute - parsing as relative");
                uri = new URI(uri2, uri3);
            }
            httpMethod.setURI(uri);
            this.hostConfiguration.setHost(uri);
            if (this.params.isParameterFalse(HttpClientParams.ALLOW_CIRCULAR_REDIRECTS)) {
                if (this.redirectLocations == null) {
                    this.redirectLocations = new HashSet();
                }
                this.redirectLocations.add(uri2);
                try {
                    if (uri.hasQuery()) {
                        uri.setQuery(null);
                    }
                    if (this.redirectLocations.contains(uri)) {
                        throw new CircularRedirectException(new StringBuffer("Circular redirect to '").append(uri).append("'").toString());
                    }
                } catch (URIException e) {
                    return false;
                }
            }
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Redirecting from '").append(uri2.getEscapedURI()).append("' to '").append(uri.getEscapedURI()).toString());
            }
            httpMethod.getHostAuthState().invalidate();
            return true;
        } catch (URIException e2) {
            throw new InvalidRedirectLocationException(new StringBuffer("Invalid redirect location: ").append(value).toString(), value, e2);
        }
    }

    private boolean processWWWAuthChallenge(HttpMethod httpMethod) {
        AuthState hostAuthState = httpMethod.getHostAuthState();
        Map parseChallenges = AuthChallengeParser.parseChallenges(httpMethod.getResponseHeaders("WWW-Authenticate"));
        if (parseChallenges.isEmpty()) {
            LOG.debug("Authentication challenge(s) not found");
            return false;
        }
        AuthScheme authScheme = null;
        try {
            authScheme = this.authProcessor.processChallenge(hostAuthState, parseChallenges);
        } catch (AuthChallengeException e) {
            if (LOG.isWarnEnabled()) {
                LOG.warn(e.getMessage());
            }
        }
        if (authScheme == null) {
            return false;
        }
        String virtualHost = httpMethod.getParams().getVirtualHost();
        if (virtualHost == null) {
            virtualHost = this.conn.getHost();
        }
        AuthScope authScope = new AuthScope(virtualHost, this.conn.getPort(), authScheme.getRealm(), authScheme.getSchemeName());
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("Authentication scope: ").append(authScope).toString());
        }
        if (!hostAuthState.isAuthAttempted() || !authScheme.isComplete()) {
            hostAuthState.setAuthAttempted(true);
            Credentials credentials = this.state.getCredentials(authScope);
            if (credentials == null) {
                credentials = promptForCredentials(authScheme, httpMethod.getParams(), authScope);
            }
            if (credentials != null) {
                return true;
            }
            if (LOG.isInfoEnabled()) {
                LOG.info(new StringBuffer("No credentials available for ").append(authScope).toString());
            }
            return false;
        } else if (promptForCredentials(authScheme, httpMethod.getParams(), authScope) != null) {
            return true;
        } else {
            if (LOG.isInfoEnabled()) {
                LOG.info(new StringBuffer("Failure authenticating with ").append(authScope).toString());
            }
            return false;
        }
    }

    private Credentials promptForCredentials(AuthScheme authScheme, HttpParams httpParams, AuthScope authScope) {
        Credentials credentials;
        LOG.debug("Credentials required");
        CredentialsProvider credentialsProvider = (CredentialsProvider) httpParams.getParameter(CredentialsProvider.PROVIDER);
        if (credentialsProvider != null) {
            try {
                credentials = credentialsProvider.getCredentials(authScheme, authScope.getHost(), authScope.getPort(), false);
            } catch (CredentialsNotAvailableException e) {
                LOG.warn(e.getMessage());
                credentials = null;
            }
            if (credentials == null) {
                return credentials;
            }
            this.state.setCredentials(authScope, credentials);
            if (!LOG.isDebugEnabled()) {
                return credentials;
            }
            LOG.debug(new StringBuffer().append(authScope).append(" new credentials given").toString());
            return credentials;
        }
        LOG.debug("Credentials provider not available");
        return null;
    }

    private Credentials promptForProxyCredentials(AuthScheme authScheme, HttpParams httpParams, AuthScope authScope) {
        Credentials credentials;
        LOG.debug("Proxy credentials required");
        CredentialsProvider credentialsProvider = (CredentialsProvider) httpParams.getParameter(CredentialsProvider.PROVIDER);
        if (credentialsProvider != null) {
            try {
                credentials = credentialsProvider.getCredentials(authScheme, authScope.getHost(), authScope.getPort(), true);
            } catch (CredentialsNotAvailableException e) {
                LOG.warn(e.getMessage());
                credentials = null;
            }
            if (credentials == null) {
                return credentials;
            }
            this.state.setProxyCredentials(authScope, credentials);
            if (!LOG.isDebugEnabled()) {
                return credentials;
            }
            LOG.debug(new StringBuffer().append(authScope).append(" new credentials given").toString());
            return credentials;
        }
        LOG.debug("Proxy credentials provider not available");
        return null;
    }

    /* JADX INFO: finally extract failed */
    public void executeMethod(HttpMethod httpMethod) {
        int i;
        boolean z;
        if (httpMethod == null) {
            throw new IllegalArgumentException("Method may not be null");
        }
        this.hostConfiguration.getParams().setDefaults(this.params);
        httpMethod.getParams().setDefaults(this.hostConfiguration.getParams());
        Collection<Header> collection = (Collection) this.hostConfiguration.getParams().getParameter(HostParams.DEFAULT_HEADERS);
        if (collection != null) {
            for (Header addRequestHeader : collection) {
                httpMethod.addRequestHeader(addRequestHeader);
            }
        }
        try {
            int intParameter = this.params.getIntParameter(HttpClientParams.MAX_REDIRECTS, 100);
            int i2 = 0;
            while (true) {
                if (this.conn != null && !this.hostConfiguration.hostEquals(this.conn)) {
                    this.conn.setLocked(false);
                    this.conn.releaseConnection();
                    this.conn = null;
                }
                if (this.conn == null) {
                    this.conn = this.connectionManager.getConnectionWithTimeout(this.hostConfiguration, this.params.getConnectionManagerTimeout());
                    this.conn.setLocked(true);
                    if (this.params.isAuthenticationPreemptive() || this.state.isAuthenticationPreemptive()) {
                        LOG.debug("Preemptively sending default basic credentials");
                        httpMethod.getHostAuthState().setPreemptive();
                        httpMethod.getHostAuthState().setAuthAttempted(true);
                        if (this.conn.isProxied() && !this.conn.isSecure()) {
                            httpMethod.getProxyAuthState().setPreemptive();
                            httpMethod.getProxyAuthState().setAuthAttempted(true);
                        }
                    }
                }
                authenticate(httpMethod);
                executeWithRetry(httpMethod);
                if (this.connectMethod == null) {
                    if (!isRedirectNeeded(httpMethod) || !processRedirectResponse(httpMethod)) {
                        i = i2;
                        z = false;
                    } else {
                        int i3 = i2 + 1;
                        if (i3 >= intParameter) {
                            LOG.error("Narrowly avoided an infinite loop in execute");
                            throw new RedirectException(new StringBuffer("Maximum redirects (").append(intParameter).append(") exceeded").toString());
                        }
                        if (LOG.isDebugEnabled()) {
                            LOG.debug(new StringBuffer("Execute redirect ").append(i3).append(" of ").append(intParameter).toString());
                        }
                        i = i3;
                        z = true;
                    }
                    if (isAuthenticationNeeded(httpMethod) && processAuthenticationResponse(httpMethod)) {
                        LOG.debug("Retry authentication");
                        z = true;
                    }
                    if (!z) {
                        break;
                    }
                    if (httpMethod.getResponseBodyAsStream() != null) {
                        httpMethod.getResponseBodyAsStream().close();
                    }
                    i2 = i;
                } else {
                    fakeResponse(httpMethod);
                    break;
                }
            }
            if (this.conn != null) {
                this.conn.setLocked(false);
            }
            if ((this.releaseConnection || httpMethod.getResponseBodyAsStream() == null) && this.conn != null) {
                this.conn.releaseConnection();
            }
        } catch (Throwable th) {
            if (this.conn != null) {
                this.conn.setLocked(false);
            }
            if ((this.releaseConnection || httpMethod.getResponseBodyAsStream() == null) && this.conn != null) {
                this.conn.releaseConnection();
            }
            throw th;
        }
    }

    public HttpConnectionManager getConnectionManager() {
        return this.connectionManager;
    }

    public HostConfiguration getHostConfiguration() {
        return this.hostConfiguration;
    }

    public HttpParams getParams() {
        return this.params;
    }

    public HttpState getState() {
        return this.state;
    }
}
