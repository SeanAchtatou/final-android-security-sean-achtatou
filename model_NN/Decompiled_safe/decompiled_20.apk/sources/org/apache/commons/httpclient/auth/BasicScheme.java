package org.apache.commons.httpclient.auth;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BasicScheme extends RFC2617Scheme {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$auth$BasicScheme;
    private boolean complete = false;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$auth$BasicScheme == null) {
            cls = class$("org.apache.commons.httpclient.auth.BasicScheme");
            class$org$apache$commons$httpclient$auth$BasicScheme = cls;
        } else {
            cls = class$org$apache$commons$httpclient$auth$BasicScheme;
        }
        LOG = LogFactory.getLog(cls);
    }

    public BasicScheme() {
    }

    public BasicScheme(String str) {
        super(str);
    }

    public static String authenticate(UsernamePasswordCredentials usernamePasswordCredentials) {
        return authenticate(usernamePasswordCredentials, "ISO-8859-1");
    }

    public static String authenticate(UsernamePasswordCredentials usernamePasswordCredentials, String str) {
        LOG.trace("enter BasicScheme.authenticate(UsernamePasswordCredentials, String)");
        if (usernamePasswordCredentials == null) {
            throw new IllegalArgumentException("Credentials may not be null");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("charset may not be null or empty");
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(usernamePasswordCredentials.getUserName());
            stringBuffer.append(":");
            stringBuffer.append(usernamePasswordCredentials.getPassword());
            return new StringBuffer("Basic ").append(EncodingUtil.getAsciiString(Base64.encodeBase64(EncodingUtil.getBytes(stringBuffer.toString(), str)))).toString();
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public String authenticate(Credentials credentials, String str, String str2) {
        LOG.trace("enter BasicScheme.authenticate(Credentials, String, String)");
        try {
            return authenticate((UsernamePasswordCredentials) credentials);
        } catch (ClassCastException e) {
            throw new InvalidCredentialsException(new StringBuffer("Credentials cannot be used for basic authentication: ").append(credentials.getClass().getName()).toString());
        }
    }

    public String authenticate(Credentials credentials, HttpMethod httpMethod) {
        LOG.trace("enter BasicScheme.authenticate(Credentials, HttpMethod)");
        if (httpMethod == null) {
            throw new IllegalArgumentException("Method may not be null");
        }
        try {
            return authenticate((UsernamePasswordCredentials) credentials, httpMethod.getParams().getCredentialCharset());
        } catch (ClassCastException e) {
            throw new InvalidCredentialsException(new StringBuffer("Credentials cannot be used for basic authentication: ").append(credentials.getClass().getName()).toString());
        }
    }

    public String getSchemeName() {
        return AuthState.PREEMPTIVE_AUTH_SCHEME;
    }

    public boolean isComplete() {
        return this.complete;
    }

    public boolean isConnectionBased() {
        return false;
    }

    public void processChallenge(String str) {
        super.processChallenge(str);
        this.complete = true;
    }
}
