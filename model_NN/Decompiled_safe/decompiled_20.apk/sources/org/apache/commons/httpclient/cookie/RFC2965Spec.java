package org.apache.commons.httpclient.cookie;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HeaderElement;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.util.ParameterFormatter;

public class RFC2965Spec extends CookieSpecBase implements CookieVersionSupport {
    private static final Comparator PATH_COMPOARATOR = new CookiePathComparator();
    public static final String SET_COOKIE2_KEY = "set-cookie2";
    private final List attribHandlerList;
    private final Map attribHandlerMap;
    private final ParameterFormatter formatter = new ParameterFormatter();
    private final CookieSpec rfc2109;

    /* renamed from: org.apache.commons.httpclient.cookie.RFC2965Spec$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    class Cookie2DomainAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private Cookie2DomainAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        Cookie2DomainAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookieOrigin == null) {
                throw new IllegalArgumentException("Cookie origin may not be null");
            } else {
                String lowerCase = cookieOrigin.getHost().toLowerCase();
                String domain = cookie.getDomain();
                return this.this$0.domainMatch(lowerCase, domain) && lowerCase.substring(0, lowerCase.length() - domain.length()).indexOf(46) == -1;
            }
        }

        public void parse(Cookie cookie, String str) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (str == null) {
                throw new MalformedCookieException("Missing value for domain attribute");
            } else if (str.trim().equals(PoiTypeDef.All)) {
                throw new MalformedCookieException("Blank value for domain attribute");
            } else {
                String lowerCase = str.toLowerCase();
                if (!lowerCase.startsWith(".")) {
                    lowerCase = new StringBuffer(".").append(lowerCase).toString();
                }
                cookie.setDomain(lowerCase);
                cookie.setDomainAttributeSpecified(true);
            }
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookieOrigin == null) {
                throw new IllegalArgumentException("Cookie origin may not be null");
            } else {
                String lowerCase = cookieOrigin.getHost().toLowerCase();
                if (cookie.getDomain() == null) {
                    throw new MalformedCookieException("Invalid cookie state: domain not specified");
                }
                String lowerCase2 = cookie.getDomain().toLowerCase();
                if (cookie.isDomainAttributeSpecified()) {
                    if (!lowerCase2.startsWith(".")) {
                        throw new MalformedCookieException(new StringBuffer("Domain attribute \"").append(cookie.getDomain()).append("\" violates RFC 2109: domain must start with a dot").toString());
                    }
                    int indexOf = lowerCase2.indexOf(46, 1);
                    if ((indexOf < 0 || indexOf == lowerCase2.length() - 1) && !lowerCase2.equals(".local")) {
                        throw new MalformedCookieException(new StringBuffer("Domain attribute \"").append(cookie.getDomain()).append("\" violates RFC 2965: the value contains no embedded dots and the value is not .local").toString());
                    } else if (!this.this$0.domainMatch(lowerCase, lowerCase2)) {
                        throw new MalformedCookieException(new StringBuffer("Domain attribute \"").append(cookie.getDomain()).append("\" violates RFC 2965: effective host name does not domain-match domain attribute.").toString());
                    } else if (lowerCase.substring(0, lowerCase.length() - lowerCase2.length()).indexOf(46) != -1) {
                        throw new MalformedCookieException(new StringBuffer("Domain attribute \"").append(cookie.getDomain()).append("\" violates RFC 2965: effective host minus domain may not contain any dots").toString());
                    }
                } else if (!cookie.getDomain().equals(lowerCase)) {
                    throw new MalformedCookieException(new StringBuffer("Illegal domain attribute: \"").append(cookie.getDomain()).append("\".Domain of origin: \"").append(lowerCase).append("\"").toString());
                }
            }
        }
    }

    class Cookie2MaxageAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private Cookie2MaxageAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        Cookie2MaxageAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            return true;
        }

        public void parse(Cookie cookie, String str) {
            int i;
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (str == null) {
                throw new MalformedCookieException("Missing value for max-age attribute");
            } else {
                try {
                    i = Integer.parseInt(str);
                } catch (NumberFormatException e) {
                    i = -1;
                }
                if (i < 0) {
                    throw new MalformedCookieException("Invalid max-age attribute.");
                }
                cookie.setExpiryDate(new Date(System.currentTimeMillis() + (((long) i) * 1000)));
            }
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
        }
    }

    class Cookie2PathAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private Cookie2PathAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        Cookie2PathAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookieOrigin == null) {
                throw new IllegalArgumentException("Cookie origin may not be null");
            } else {
                String path = cookieOrigin.getPath();
                if (cookie.getPath() == null) {
                    CookieSpecBase.LOG.warn("Invalid cookie state: path attribute is null.");
                    return false;
                }
                if (path.trim().equals(PoiTypeDef.All)) {
                    path = CookieSpec.PATH_DELIM;
                }
                return this.this$0.pathMatch(path, cookie.getPath());
            }
        }

        public void parse(Cookie cookie, String str) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (str == null) {
                throw new MalformedCookieException("Missing value for path attribute");
            } else if (str.trim().equals(PoiTypeDef.All)) {
                throw new MalformedCookieException("Blank value for path attribute");
            } else {
                cookie.setPath(str);
                cookie.setPathAttributeSpecified(true);
            }
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookieOrigin == null) {
                throw new IllegalArgumentException("Cookie origin may not be null");
            } else {
                String path = cookieOrigin.getPath();
                if (path == null) {
                    throw new IllegalArgumentException("Path of origin host may not be null.");
                } else if (cookie.getPath() == null) {
                    throw new MalformedCookieException("Invalid cookie state: path attribute is null.");
                } else {
                    if (path.trim().equals(PoiTypeDef.All)) {
                        path = CookieSpec.PATH_DELIM;
                    }
                    if (!this.this$0.pathMatch(path, cookie.getPath())) {
                        throw new MalformedCookieException(new StringBuffer("Illegal path attribute \"").append(cookie.getPath()).append("\". Path of origin: \"").append(path).append("\"").toString());
                    }
                }
            }
        }
    }

    class Cookie2PortAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private Cookie2PortAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        Cookie2PortAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookieOrigin == null) {
                throw new IllegalArgumentException("Cookie origin may not be null");
            } else if (!(cookie instanceof Cookie2)) {
                return false;
            } else {
                Cookie2 cookie2 = (Cookie2) cookie;
                int port = cookieOrigin.getPort();
                if (cookie2.isPortAttributeSpecified()) {
                    if (cookie2.getPorts() != null) {
                        return RFC2965Spec.access$1000(this.this$0, port, cookie2.getPorts());
                    }
                    CookieSpecBase.LOG.warn("Invalid cookie state: port not specified");
                    return false;
                }
            }
        }

        public void parse(Cookie cookie, String str) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookie instanceof Cookie2) {
                Cookie2 cookie2 = (Cookie2) cookie;
                if (str == null || str.trim().equals(PoiTypeDef.All)) {
                    cookie2.setPortAttributeBlank(true);
                } else {
                    cookie2.setPorts(RFC2965Spec.access$900(this.this$0, str));
                }
                cookie2.setPortAttributeSpecified(true);
            }
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookieOrigin == null) {
                throw new IllegalArgumentException("Cookie origin may not be null");
            } else if (cookie instanceof Cookie2) {
                Cookie2 cookie2 = (Cookie2) cookie;
                int port = cookieOrigin.getPort();
                if (cookie2.isPortAttributeSpecified() && !RFC2965Spec.access$1000(this.this$0, port, cookie2.getPorts())) {
                    throw new MalformedCookieException("Port attribute violates RFC 2965: Request port not found in cookie's port list.");
                }
            }
        }
    }

    class Cookie2VersionAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private Cookie2VersionAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        Cookie2VersionAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            return true;
        }

        public void parse(Cookie cookie, String str) {
            int i;
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookie instanceof Cookie2) {
                Cookie2 cookie2 = (Cookie2) cookie;
                if (str == null) {
                    throw new MalformedCookieException("Missing value for version attribute");
                }
                try {
                    i = Integer.parseInt(str);
                } catch (NumberFormatException e) {
                    i = -1;
                }
                if (i < 0) {
                    throw new MalformedCookieException("Invalid cookie version.");
                }
                cookie2.setVersion(i);
                cookie2.setVersionAttributeSpecified(true);
            }
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if ((cookie instanceof Cookie2) && !((Cookie2) cookie).isVersionAttributeSpecified()) {
                throw new MalformedCookieException("Violates RFC 2965. Version attribute is required.");
            }
        }
    }

    class CookieCommentAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private CookieCommentAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        CookieCommentAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            return true;
        }

        public void parse(Cookie cookie, String str) {
            cookie.setComment(str);
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
        }
    }

    class CookieCommentUrlAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private CookieCommentUrlAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        CookieCommentUrlAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            return true;
        }

        public void parse(Cookie cookie, String str) {
            if (cookie instanceof Cookie2) {
                ((Cookie2) cookie).setCommentURL(str);
            }
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
        }
    }

    class CookieDiscardAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private CookieDiscardAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        CookieDiscardAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            return true;
        }

        public void parse(Cookie cookie, String str) {
            if (cookie instanceof Cookie2) {
                ((Cookie2) cookie).setDiscard(true);
            }
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
        }
    }

    class CookieSecureAttributeHandler implements CookieAttributeHandler {
        private final RFC2965Spec this$0;

        private CookieSecureAttributeHandler(RFC2965Spec rFC2965Spec) {
            this.this$0 = rFC2965Spec;
        }

        CookieSecureAttributeHandler(RFC2965Spec rFC2965Spec, AnonymousClass1 r2) {
            this(rFC2965Spec);
        }

        public boolean match(Cookie cookie, CookieOrigin cookieOrigin) {
            if (cookie == null) {
                throw new IllegalArgumentException("Cookie may not be null");
            } else if (cookieOrigin != null) {
                return cookie.getSecure() == cookieOrigin.isSecure();
            } else {
                throw new IllegalArgumentException("Cookie origin may not be null");
            }
        }

        public void parse(Cookie cookie, String str) {
            cookie.setSecure(true);
        }

        public void validate(Cookie cookie, CookieOrigin cookieOrigin) {
        }
    }

    public RFC2965Spec() {
        this.formatter.setAlwaysUseQuotes(true);
        this.attribHandlerMap = new HashMap(10);
        this.attribHandlerList = new ArrayList(10);
        this.rfc2109 = new RFC2109Spec();
        registerAttribHandler(Cookie2.PATH, new Cookie2PathAttributeHandler(this, null));
        registerAttribHandler(Cookie2.DOMAIN, new Cookie2DomainAttributeHandler(this, null));
        registerAttribHandler(Cookie2.PORT, new Cookie2PortAttributeHandler(this, null));
        registerAttribHandler(Cookie2.MAXAGE, new Cookie2MaxageAttributeHandler(this, null));
        registerAttribHandler(Cookie2.SECURE, new CookieSecureAttributeHandler(this, null));
        registerAttribHandler(Cookie2.COMMENT, new CookieCommentAttributeHandler(this, null));
        registerAttribHandler(Cookie2.COMMENTURL, new CookieCommentUrlAttributeHandler(this, null));
        registerAttribHandler(Cookie2.DISCARD, new CookieDiscardAttributeHandler(this, null));
        registerAttribHandler(Cookie2.VERSION, new Cookie2VersionAttributeHandler(this, null));
    }

    static boolean access$1000(RFC2965Spec rFC2965Spec, int i, int[] iArr) {
        return rFC2965Spec.portMatch(i, iArr);
    }

    static int[] access$900(RFC2965Spec rFC2965Spec, String str) {
        return rFC2965Spec.parsePortAttribute(str);
    }

    private String createPortAttribute(int[] iArr) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                stringBuffer.append(",");
            }
            stringBuffer.append(iArr[i]);
        }
        return stringBuffer.toString();
    }

    private void doFormatCookie2(Cookie2 cookie2, StringBuffer stringBuffer) {
        String name = cookie2.getName();
        String value = cookie2.getValue();
        if (value == null) {
            value = PoiTypeDef.All;
        }
        this.formatter.format(stringBuffer, new NameValuePair(name, value));
        if (cookie2.getDomain() != null && cookie2.isDomainAttributeSpecified()) {
            stringBuffer.append("; ");
            this.formatter.format(stringBuffer, new NameValuePair("$Domain", cookie2.getDomain()));
        }
        if (cookie2.getPath() != null && cookie2.isPathAttributeSpecified()) {
            stringBuffer.append("; ");
            this.formatter.format(stringBuffer, new NameValuePair("$Path", cookie2.getPath()));
        }
        if (cookie2.isPortAttributeSpecified()) {
            String str = PoiTypeDef.All;
            if (!cookie2.isPortAttributeBlank()) {
                str = createPortAttribute(cookie2.getPorts());
            }
            stringBuffer.append("; ");
            this.formatter.format(stringBuffer, new NameValuePair("$Port", str));
        }
    }

    private static String getEffectiveHost(String str) {
        String lowerCase = str.toLowerCase();
        return str.indexOf(46) < 0 ? new StringBuffer().append(lowerCase).append(".local").toString() : lowerCase;
    }

    private int[] parsePortAttribute(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, ",");
        int[] iArr = new int[stringTokenizer.countTokens()];
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            try {
                iArr[i] = Integer.parseInt(stringTokenizer.nextToken().trim());
                if (iArr[i] < 0) {
                    throw new MalformedCookieException("Invalid Port attribute.");
                }
                i++;
            } catch (NumberFormatException e) {
                throw new MalformedCookieException(new StringBuffer("Invalid Port attribute: ").append(e.getMessage()).toString());
            }
        }
        return iArr;
    }

    private boolean portMatch(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i == i2) {
                return true;
            }
        }
        return false;
    }

    public boolean domainMatch(String str, String str2) {
        return str.equals(str2) || (str2.startsWith(".") && str.endsWith(str2));
    }

    /* access modifiers changed from: protected */
    public CookieAttributeHandler findAttribHandler(String str) {
        return (CookieAttributeHandler) this.attribHandlerMap.get(str);
    }

    public String formatCookie(Cookie cookie) {
        LOG.trace("enter RFC2965Spec.formatCookie(Cookie)");
        if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (!(cookie instanceof Cookie2)) {
            return this.rfc2109.formatCookie(cookie);
        } else {
            Cookie2 cookie2 = (Cookie2) cookie;
            int version = cookie2.getVersion();
            StringBuffer stringBuffer = new StringBuffer();
            this.formatter.format(stringBuffer, new NameValuePair("$Version", Integer.toString(version)));
            stringBuffer.append("; ");
            doFormatCookie2(cookie2, stringBuffer);
            return stringBuffer.toString();
        }
    }

    public String formatCookies(Cookie[] cookieArr) {
        boolean z;
        LOG.trace("enter RFC2965Spec.formatCookieHeader(Cookie[])");
        if (cookieArr == null) {
            throw new IllegalArgumentException("Cookies may not be null");
        }
        int i = -1;
        int i2 = 0;
        while (true) {
            if (i2 >= cookieArr.length) {
                z = false;
                break;
            }
            Cookie cookie = cookieArr[i2];
            if (!(cookie instanceof Cookie2)) {
                z = true;
                break;
            }
            if (cookie.getVersion() > i) {
                i = cookie.getVersion();
            }
            i2++;
        }
        if (i < 0) {
            i = 0;
        }
        if (z || i <= 0) {
            return this.rfc2109.formatCookies(cookieArr);
        }
        Arrays.sort(cookieArr, PATH_COMPOARATOR);
        StringBuffer stringBuffer = new StringBuffer();
        this.formatter.format(stringBuffer, new NameValuePair("$Version", Integer.toString(i)));
        for (Cookie cookie2 : cookieArr) {
            stringBuffer.append("; ");
            doFormatCookie2((Cookie2) cookie2, stringBuffer);
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public CookieAttributeHandler getAttribHandler(String str) {
        CookieAttributeHandler findAttribHandler = findAttribHandler(str);
        if (findAttribHandler != null) {
            return findAttribHandler;
        }
        throw new IllegalStateException(new StringBuffer("Handler not registered for ").append(str).append(" attribute.").toString());
    }

    /* access modifiers changed from: protected */
    public Iterator getAttribHandlerIterator() {
        return this.attribHandlerList.iterator();
    }

    public int getVersion() {
        return 1;
    }

    public Header getVersionHeader() {
        ParameterFormatter parameterFormatter = new ParameterFormatter();
        StringBuffer stringBuffer = new StringBuffer();
        parameterFormatter.format(stringBuffer, new NameValuePair("$Version", Integer.toString(getVersion())));
        return new Header("Cookie2", stringBuffer.toString(), true);
    }

    public boolean match(String str, int i, String str2, boolean z, Cookie cookie) {
        LOG.trace("enter RFC2965.match(String, int, String, boolean, Cookie");
        if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (!(cookie instanceof Cookie2)) {
            return this.rfc2109.match(str, i, str2, z, cookie);
        } else {
            if (cookie.isPersistent() && cookie.isExpired()) {
                return false;
            }
            CookieOrigin cookieOrigin = new CookieOrigin(getEffectiveHost(str), i, str2, z);
            Iterator attribHandlerIterator = getAttribHandlerIterator();
            while (attribHandlerIterator.hasNext()) {
                if (!((CookieAttributeHandler) attribHandlerIterator.next()).match(cookie, cookieOrigin)) {
                    return false;
                }
            }
            return true;
        }
    }

    public Cookie[] parse(String str, int i, String str2, boolean z, String str3) {
        LOG.trace("enter RFC2965Spec.parse(String, int, String, boolean, String)");
        if (str == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (str.trim().equals(PoiTypeDef.All)) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (i < 0) {
            throw new IllegalArgumentException(new StringBuffer("Invalid port: ").append(i).toString());
        } else if (str2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else if (str3 == null) {
            throw new IllegalArgumentException("Header may not be null.");
        } else {
            String str4 = str2.trim().equals(PoiTypeDef.All) ? CookieSpec.PATH_DELIM : str2;
            String effectiveHost = getEffectiveHost(str);
            HeaderElement[] parseElements = HeaderElement.parseElements(str3.toCharArray());
            LinkedList linkedList = new LinkedList();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= parseElements.length) {
                    return (Cookie[]) linkedList.toArray(new Cookie[linkedList.size()]);
                }
                HeaderElement headerElement = parseElements[i3];
                try {
                    Cookie2 cookie2 = new Cookie2(effectiveHost, headerElement.getName(), headerElement.getValue(), str4, null, false, new int[]{i});
                    NameValuePair[] parameters = headerElement.getParameters();
                    if (parameters != null) {
                        HashMap hashMap = new HashMap(parameters.length);
                        for (int length = parameters.length - 1; length >= 0; length--) {
                            NameValuePair nameValuePair = parameters[length];
                            hashMap.put(nameValuePair.getName().toLowerCase(), nameValuePair);
                        }
                        for (Map.Entry value : hashMap.entrySet()) {
                            parseAttribute((NameValuePair) value.getValue(), cookie2);
                        }
                    }
                    linkedList.add(cookie2);
                    i2 = i3 + 1;
                } catch (IllegalArgumentException e) {
                    throw new MalformedCookieException(e.getMessage());
                }
            }
        }
    }

    public Cookie[] parse(String str, int i, String str2, boolean z, Header header) {
        LOG.trace("enter RFC2965.parse(String, int, String, boolean, Header)");
        if (header == null) {
            throw new IllegalArgumentException("Header may not be null.");
        } else if (header.getName() == null) {
            throw new IllegalArgumentException("Header name may not be null.");
        } else if (header.getName().equalsIgnoreCase(SET_COOKIE2_KEY)) {
            return parse(str, i, str2, z, header.getValue());
        } else if (header.getName().equalsIgnoreCase(RFC2109Spec.SET_COOKIE_KEY)) {
            return this.rfc2109.parse(str, i, str2, z, header.getValue());
        } else {
            throw new MalformedCookieException("Header name is not valid. RFC 2965 supports \"set-cookie\" and \"set-cookie2\" headers.");
        }
    }

    public void parseAttribute(NameValuePair nameValuePair, Cookie cookie) {
        if (nameValuePair == null) {
            throw new IllegalArgumentException("Attribute may not be null.");
        } else if (nameValuePair.getName() == null) {
            throw new IllegalArgumentException("Attribute Name may not be null.");
        } else if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null.");
        } else {
            String lowerCase = nameValuePair.getName().toLowerCase();
            String value = nameValuePair.getValue();
            CookieAttributeHandler findAttribHandler = findAttribHandler(lowerCase);
            if (findAttribHandler != null) {
                findAttribHandler.parse(cookie, value);
            } else if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Unrecognized cookie attribute: ").append(nameValuePair.toString()).toString());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void registerAttribHandler(String str, CookieAttributeHandler cookieAttributeHandler) {
        if (str == null) {
            throw new IllegalArgumentException("Attribute name may not be null");
        } else if (cookieAttributeHandler == null) {
            throw new IllegalArgumentException("Attribute handler may not be null");
        } else {
            if (!this.attribHandlerList.contains(cookieAttributeHandler)) {
                this.attribHandlerList.add(cookieAttributeHandler);
            }
            this.attribHandlerMap.put(str, cookieAttributeHandler);
        }
    }

    public void validate(String str, int i, String str2, boolean z, Cookie cookie) {
        LOG.trace("enter RFC2965Spec.validate(String, int, String, boolean, Cookie)");
        if (!(cookie instanceof Cookie2)) {
            this.rfc2109.validate(str, i, str2, z, cookie);
        } else if (cookie.getName().indexOf(32) != -1) {
            throw new MalformedCookieException("Cookie name may not contain blanks");
        } else if (cookie.getName().startsWith("$")) {
            throw new MalformedCookieException("Cookie name may not start with $");
        } else {
            CookieOrigin cookieOrigin = new CookieOrigin(getEffectiveHost(str), i, str2, z);
            Iterator attribHandlerIterator = getAttribHandlerIterator();
            while (attribHandlerIterator.hasNext()) {
                ((CookieAttributeHandler) attribHandlerIterator.next()).validate(cookie, cookieOrigin);
            }
        }
    }
}
