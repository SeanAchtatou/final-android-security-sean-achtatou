package org.apache.commons.httpclient;

import java.security.Provider;
import java.security.Security;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HttpClient {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$HttpClient;
    private HostConfiguration hostConfiguration;
    private HttpConnectionManager httpConnectionManager;
    private HttpClientParams params;
    private HttpState state;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HttpClient == null) {
            cls = class$("org.apache.commons.httpclient.HttpClient");
            class$org$apache$commons$httpclient$HttpClient = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HttpClient;
        }
        Log log = LogFactory.getLog(cls);
        LOG = log;
        if (log.isDebugEnabled()) {
            try {
                LOG.debug(new StringBuffer("Java version: ").append(System.getProperty("java.version")).toString());
                LOG.debug(new StringBuffer("Java vendor: ").append(System.getProperty("java.vendor")).toString());
                LOG.debug(new StringBuffer("Java class path: ").append(System.getProperty("java.class.path")).toString());
                LOG.debug(new StringBuffer("Operating system name: ").append(System.getProperty("os.name")).toString());
                LOG.debug(new StringBuffer("Operating system architecture: ").append(System.getProperty("os.arch")).toString());
                LOG.debug(new StringBuffer("Operating system version: ").append(System.getProperty("os.version")).toString());
                Provider[] providers = Security.getProviders();
                for (Provider provider : providers) {
                    LOG.debug(new StringBuffer().append(provider.getName()).append(" ").append(provider.getVersion()).append(": ").append(provider.getInfo()).toString());
                }
            } catch (SecurityException e) {
            }
        }
    }

    public HttpClient() {
        this(new HttpClientParams());
    }

    public HttpClient(HttpConnectionManager httpConnectionManager2) {
        this(new HttpClientParams(), httpConnectionManager2);
    }

    public HttpClient(HttpClientParams httpClientParams) {
        this.state = new HttpState();
        this.params = null;
        this.hostConfiguration = new HostConfiguration();
        if (httpClientParams == null) {
            throw new IllegalArgumentException("Params may not be null");
        }
        this.params = httpClientParams;
        this.httpConnectionManager = null;
        Class connectionManagerClass = httpClientParams.getConnectionManagerClass();
        if (connectionManagerClass != null) {
            try {
                this.httpConnectionManager = (HttpConnectionManager) connectionManagerClass.newInstance();
            } catch (Exception e) {
                LOG.warn("Error instantiating connection manager class, defaulting to SimpleHttpConnectionManager", e);
            }
        }
        if (this.httpConnectionManager == null) {
            this.httpConnectionManager = new SimpleHttpConnectionManager();
        }
        if (this.httpConnectionManager != null) {
            this.httpConnectionManager.getParams().setDefaults(this.params);
        }
    }

    public HttpClient(HttpClientParams httpClientParams, HttpConnectionManager httpConnectionManager2) {
        this.state = new HttpState();
        this.params = null;
        this.hostConfiguration = new HostConfiguration();
        if (httpConnectionManager2 == null) {
            throw new IllegalArgumentException("httpConnectionManager cannot be null");
        } else if (httpClientParams == null) {
            throw new IllegalArgumentException("Params may not be null");
        } else {
            this.params = httpClientParams;
            this.httpConnectionManager = httpConnectionManager2;
            this.httpConnectionManager.getParams().setDefaults(this.params);
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public int executeMethod(HostConfiguration hostConfiguration2, HttpMethod httpMethod) {
        LOG.trace("enter HttpClient.executeMethod(HostConfiguration,HttpMethod)");
        return executeMethod(hostConfiguration2, httpMethod, null);
    }

    public int executeMethod(HostConfiguration hostConfiguration2, HttpMethod httpMethod, HttpState httpState) {
        HostConfiguration hostConfiguration3;
        LOG.trace("enter HttpClient.executeMethod(HostConfiguration,HttpMethod,HttpState)");
        if (httpMethod == null) {
            throw new IllegalArgumentException("HttpMethod parameter may not be null");
        }
        HostConfiguration hostConfiguration4 = getHostConfiguration();
        if (hostConfiguration2 == null) {
            hostConfiguration2 = hostConfiguration4;
        }
        URI uri = httpMethod.getURI();
        if (hostConfiguration2 == hostConfiguration4 || uri.isAbsoluteURI()) {
            hostConfiguration3 = (HostConfiguration) hostConfiguration2.clone();
            if (uri.isAbsoluteURI()) {
                hostConfiguration3.setHost(uri);
            }
        } else {
            hostConfiguration3 = hostConfiguration2;
        }
        HttpConnectionManager httpConnectionManager2 = getHttpConnectionManager();
        HttpClientParams httpClientParams = this.params;
        if (httpState == null) {
            httpState = getState();
        }
        new HttpMethodDirector(httpConnectionManager2, hostConfiguration3, httpClientParams, httpState).executeMethod(httpMethod);
        return httpMethod.getStatusCode();
    }

    public int executeMethod(HttpMethod httpMethod) {
        LOG.trace("enter HttpClient.executeMethod(HttpMethod)");
        return executeMethod(null, httpMethod, null);
    }

    public String getHost() {
        return this.hostConfiguration.getHost();
    }

    public synchronized HostConfiguration getHostConfiguration() {
        return this.hostConfiguration;
    }

    public synchronized HttpConnectionManager getHttpConnectionManager() {
        return this.httpConnectionManager;
    }

    public HttpClientParams getParams() {
        return this.params;
    }

    public int getPort() {
        return this.hostConfiguration.getPort();
    }

    public synchronized HttpState getState() {
        return this.state;
    }

    public synchronized boolean isStrictMode() {
        return false;
    }

    public synchronized void setConnectionTimeout(int i) {
        this.httpConnectionManager.getParams().setConnectionTimeout(i);
    }

    public synchronized void setHostConfiguration(HostConfiguration hostConfiguration2) {
        this.hostConfiguration = hostConfiguration2;
    }

    public synchronized void setHttpConnectionFactoryTimeout(long j) {
        this.params.setConnectionManagerTimeout(j);
    }

    public synchronized void setHttpConnectionManager(HttpConnectionManager httpConnectionManager2) {
        this.httpConnectionManager = httpConnectionManager2;
        if (this.httpConnectionManager != null) {
            this.httpConnectionManager.getParams().setDefaults(this.params);
        }
    }

    public void setParams(HttpClientParams httpClientParams) {
        if (httpClientParams == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = httpClientParams;
    }

    public synchronized void setState(HttpState httpState) {
        this.state = httpState;
    }

    public synchronized void setStrictMode(boolean z) {
        if (z) {
            this.params.makeStrict();
        } else {
            this.params.makeLenient();
        }
    }

    public synchronized void setTimeout(int i) {
        this.params.setSoTimeout(i);
    }
}
