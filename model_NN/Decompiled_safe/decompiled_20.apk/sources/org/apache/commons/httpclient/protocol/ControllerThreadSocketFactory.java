package org.apache.commons.httpclient.protocol;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.util.TimeoutController;

public final class ControllerThreadSocketFactory {

    public abstract class SocketTask implements Runnable {
        private IOException exception;
        private Socket socket;

        static IOException access$000(SocketTask socketTask) {
            return socketTask.exception;
        }

        public abstract void doit();

        /* access modifiers changed from: protected */
        public Socket getSocket() {
            return this.socket;
        }

        public void run() {
            try {
                doit();
            } catch (IOException e) {
                this.exception = e;
            }
        }

        /* access modifiers changed from: protected */
        public void setSocket(Socket socket2) {
            this.socket = socket2;
        }
    }

    private ControllerThreadSocketFactory() {
    }

    public static Socket createSocket(SocketTask socketTask, int i) {
        try {
            TimeoutController.execute(socketTask, (long) i);
            Socket socket = socketTask.getSocket();
            if (SocketTask.access$000(socketTask) == null) {
                return socket;
            }
            throw SocketTask.access$000(socketTask);
        } catch (TimeoutController.TimeoutException e) {
            throw new ConnectTimeoutException(new StringBuffer("The host did not accept the connection within timeout of ").append(i).append(" ms").toString());
        }
    }

    public static Socket createSocket(ProtocolSocketFactory protocolSocketFactory, String str, int i, InetAddress inetAddress, int i2, int i3) {
        AnonymousClass1 r0 = new SocketTask(protocolSocketFactory, str, i, inetAddress, i2) {
            private final String val$host;
            private final InetAddress val$localAddress;
            private final int val$localPort;
            private final int val$port;
            private final ProtocolSocketFactory val$socketfactory;

            {
                this.val$socketfactory = r1;
                this.val$host = r2;
                this.val$port = r3;
                this.val$localAddress = r4;
                this.val$localPort = r5;
            }

            public void doit() {
                setSocket(this.val$socketfactory.createSocket(this.val$host, this.val$port, this.val$localAddress, this.val$localPort));
            }
        };
        try {
            TimeoutController.execute(r0, (long) i3);
            Socket socket = r0.getSocket();
            if (SocketTask.access$000(r0) == null) {
                return socket;
            }
            throw SocketTask.access$000(r0);
        } catch (TimeoutController.TimeoutException e) {
            throw new ConnectTimeoutException(new StringBuffer("The host did not accept the connection within timeout of ").append(i3).append(" ms").toString());
        }
    }
}
