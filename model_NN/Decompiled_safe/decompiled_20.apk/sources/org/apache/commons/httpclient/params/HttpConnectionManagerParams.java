package org.apache.commons.httpclient.params;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.HostConfiguration;

public class HttpConnectionManagerParams extends HttpConnectionParams {
    public static final String MAX_HOST_CONNECTIONS = "http.connection-manager.max-per-host";
    public static final String MAX_TOTAL_CONNECTIONS = "http.connection-manager.max-total";

    public int getDefaultMaxConnectionsPerHost() {
        return getMaxConnectionsPerHost(HostConfiguration.ANY_HOST_CONFIGURATION);
    }

    public int getMaxConnectionsPerHost(HostConfiguration hostConfiguration) {
        Map map = (Map) getParameter(MAX_HOST_CONNECTIONS);
        if (map == null) {
            return 2;
        }
        Integer num = (Integer) map.get(hostConfiguration);
        if (num == null && hostConfiguration != HostConfiguration.ANY_HOST_CONFIGURATION) {
            return getMaxConnectionsPerHost(HostConfiguration.ANY_HOST_CONFIGURATION);
        }
        if (num == null) {
            return 2;
        }
        return num.intValue();
    }

    public int getMaxTotalConnections() {
        return getIntParameter(MAX_TOTAL_CONNECTIONS, 20);
    }

    public void setDefaultMaxConnectionsPerHost(int i) {
        setMaxConnectionsPerHost(HostConfiguration.ANY_HOST_CONFIGURATION, i);
    }

    public void setMaxConnectionsPerHost(HostConfiguration hostConfiguration, int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("maxHostConnections must be greater than 0");
        }
        Map map = (Map) getParameter(MAX_HOST_CONNECTIONS);
        HashMap hashMap = map == null ? new HashMap() : new HashMap(map);
        hashMap.put(hostConfiguration, new Integer(i));
        setParameter(MAX_HOST_CONNECTIONS, hashMap);
    }

    public void setMaxTotalConnections(int i) {
        setIntParameter(MAX_TOTAL_CONNECTIONS, i);
    }
}
