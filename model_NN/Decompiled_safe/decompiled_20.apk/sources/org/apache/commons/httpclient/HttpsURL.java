package org.apache.commons.httpclient;

import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.util.URIUtil;

public class HttpsURL extends HttpURL {
    public static final int DEFAULT_PORT = 443;
    public static final char[] DEFAULT_SCHEME;
    public static final int _default_port = 443;
    public static final char[] _default_scheme;
    static final long serialVersionUID = 887844277028676648L;

    static {
        char[] cArr = {'h', 't', 't', 'p', 's'};
        DEFAULT_SCHEME = cArr;
        _default_scheme = cArr;
    }

    protected HttpsURL() {
    }

    public HttpsURL(String str) {
        parseUriReference(str, false);
        checkValid();
    }

    public HttpsURL(String str, int i, String str2) {
        this((String) null, str, i, str2, (String) null, (String) null);
    }

    public HttpsURL(String str, int i, String str2, String str3) {
        this((String) null, str, i, str2, str3, (String) null);
    }

    public HttpsURL(String str, String str2) {
        this.protocolCharset = str2;
        parseUriReference(str, false);
        checkValid();
    }

    public HttpsURL(String str, String str2, int i, String str3) {
        this(str, str2, i, str3, (String) null, (String) null);
    }

    public HttpsURL(String str, String str2, int i, String str3, String str4) {
        this(str, str2, i, str3, str4, (String) null);
    }

    public HttpsURL(String str, String str2, int i, String str3, String str4, String str5) {
        StringBuffer stringBuffer = new StringBuffer();
        if (!(str == null && str2 == null && i == -1)) {
            this._scheme = DEFAULT_SCHEME;
            stringBuffer.append(_default_scheme);
            stringBuffer.append("://");
            if (str != null) {
                stringBuffer.append(str);
                stringBuffer.append('@');
            }
            if (str2 != null) {
                stringBuffer.append(URIUtil.encode(str2, URI.allowed_host));
                if (!(i == -1 && i == 443)) {
                    stringBuffer.append(':');
                    stringBuffer.append(i);
                }
            }
        }
        if (str3 != null) {
            if (scheme == null || str3.startsWith(CookieSpec.PATH_DELIM)) {
                stringBuffer.append(URIUtil.encode(str3, URI.allowed_abs_path));
            } else {
                throw new URIException(1, "abs_path requested");
            }
        }
        if (str4 != null) {
            stringBuffer.append('?');
            stringBuffer.append(URIUtil.encode(str4, URI.allowed_query));
        }
        if (str5 != null) {
            stringBuffer.append('#');
            stringBuffer.append(URIUtil.encode(str5, URI.allowed_fragment));
        }
        parseUriReference(stringBuffer.toString(), true);
        checkValid();
    }

    public HttpsURL(String str, String str2, String str3) {
        this(str, str2, str3, -1, null, null, null);
    }

    public HttpsURL(String str, String str2, String str3, int i) {
        this(str, str2, str3, i, null, null, null);
    }

    public HttpsURL(String str, String str2, String str3, int i, String str4) {
        this(str, str2, str3, i, str4, null, null);
    }

    public HttpsURL(String str, String str2, String str3, int i, String str4, String str5) {
        this(str, str2, str3, i, str4, str5, null);
    }

    public HttpsURL(String str, String str2, String str3, int i, String str4, String str5, String str6) {
        this(HttpURL.toUserinfo(str, str2), str3, i, str4, str5, str6);
    }

    public HttpsURL(String str, String str2, String str3, String str4) {
        this((String) null, str, -1, str2, str3, str4);
    }

    public HttpsURL(String str, String str2, String str3, String str4, String str5) {
        this(str, str2, -1, str3, str4, str5);
    }

    public HttpsURL(HttpsURL httpsURL, String str) {
        this(httpsURL, new HttpsURL(str));
    }

    public HttpsURL(HttpsURL httpsURL, HttpsURL httpsURL2) {
        super(httpsURL, httpsURL2);
        checkValid();
    }

    public HttpsURL(char[] cArr) {
        parseUriReference(new String(cArr), true);
        checkValid();
    }

    public HttpsURL(char[] cArr, String str) {
        this.protocolCharset = str;
        parseUriReference(new String(cArr), true);
        checkValid();
    }

    /* access modifiers changed from: protected */
    public void checkValid() {
        if (!equals(this._scheme, DEFAULT_SCHEME) && this._scheme != null) {
            throw new URIException(1, "wrong class use");
        }
    }

    public int getPort() {
        if (this._port == -1) {
            return 443;
        }
        return this._port;
    }

    public char[] getRawScheme() {
        if (this._scheme == null) {
            return null;
        }
        return DEFAULT_SCHEME;
    }

    public String getScheme() {
        if (this._scheme == null) {
            return null;
        }
        return new String(DEFAULT_SCHEME);
    }
}
