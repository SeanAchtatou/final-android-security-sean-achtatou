package org.apache.commons.httpclient.methods;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.HttpConnection;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MultipartPostMethod extends ExpectContinueMethod {
    private static final Log LOG;
    public static final String MULTIPART_FORM_CONTENT_TYPE = "multipart/form-data";
    static Class class$org$apache$commons$httpclient$methods$MultipartPostMethod;
    private final List parameters = new ArrayList();

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$methods$MultipartPostMethod == null) {
            cls = class$("org.apache.commons.httpclient.methods.MultipartPostMethod");
            class$org$apache$commons$httpclient$methods$MultipartPostMethod = cls;
        } else {
            cls = class$org$apache$commons$httpclient$methods$MultipartPostMethod;
        }
        LOG = LogFactory.getLog(cls);
    }

    public MultipartPostMethod() {
    }

    public MultipartPostMethod(String str) {
        super(str);
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void addContentLengthRequestHeader(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter EntityEnclosingMethod.addContentLengthRequestHeader(HttpState, HttpConnection)");
        if (getRequestHeader("Content-Length") == null) {
            addRequestHeader("Content-Length", String.valueOf(getRequestContentLength()));
        }
        removeRequestHeader("Transfer-Encoding");
    }

    /* access modifiers changed from: protected */
    public void addContentTypeRequestHeader(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter EntityEnclosingMethod.addContentTypeRequestHeader(HttpState, HttpConnection)");
        if (!this.parameters.isEmpty()) {
            StringBuffer stringBuffer = new StringBuffer(MULTIPART_FORM_CONTENT_TYPE);
            if (Part.getBoundary() != null) {
                stringBuffer.append("; boundary=");
                stringBuffer.append(Part.getBoundary());
            }
            setRequestHeader("Content-Type", stringBuffer.toString());
        }
    }

    public void addParameter(String str, File file) {
        LOG.trace("enter MultipartPostMethod.addParameter(String parameterName, File parameterFile)");
        this.parameters.add(new FilePart(str, file));
    }

    public void addParameter(String str, String str2) {
        LOG.trace("enter addParameter(String parameterName, String parameterValue)");
        this.parameters.add(new StringPart(str, str2));
    }

    public void addParameter(String str, String str2, File file) {
        LOG.trace("enter MultipartPostMethod.addParameter(String parameterName, String fileName, File parameterFile)");
        this.parameters.add(new FilePart(str, str2, file));
    }

    public void addPart(Part part) {
        LOG.trace("enter addPart(Part part)");
        this.parameters.add(part);
    }

    /* access modifiers changed from: protected */
    public void addRequestHeaders(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter MultipartPostMethod.addRequestHeaders(HttpState state, HttpConnection conn)");
        super.addRequestHeaders(httpState, httpConnection);
        addContentLengthRequestHeader(httpState, httpConnection);
        addContentTypeRequestHeader(httpState, httpConnection);
    }

    public String getName() {
        return "POST";
    }

    public Part[] getParts() {
        return (Part[]) this.parameters.toArray(new Part[this.parameters.size()]);
    }

    /* access modifiers changed from: protected */
    public long getRequestContentLength() {
        LOG.trace("enter MultipartPostMethod.getRequestContentLength()");
        return Part.getLengthOfParts(getParts());
    }

    /* access modifiers changed from: protected */
    public boolean hasRequestContent() {
        return true;
    }

    public void recycle() {
        LOG.trace("enter MultipartPostMethod.recycle()");
        super.recycle();
        this.parameters.clear();
    }

    /* access modifiers changed from: protected */
    public boolean writeRequestBody(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter MultipartPostMethod.writeRequestBody(HttpState state, HttpConnection conn)");
        Part.sendParts(httpConnection.getRequestOutputStream(), getParts());
        return true;
    }
}
