package org.apache.commons.httpclient;

import com.amap.api.search.route.Route;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.httpclient.util.ExceptionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ChunkedInputStream extends InputStream {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$ChunkedInputStream;
    private boolean bof;
    private int chunkSize;
    private boolean closed;
    private boolean eof;
    private InputStream in;
    private HttpMethod method;
    private int pos;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$ChunkedInputStream == null) {
            cls = class$("org.apache.commons.httpclient.ChunkedInputStream");
            class$org$apache$commons$httpclient$ChunkedInputStream = cls;
        } else {
            cls = class$org$apache$commons$httpclient$ChunkedInputStream;
        }
        LOG = LogFactory.getLog(cls);
    }

    public ChunkedInputStream(InputStream inputStream) {
        this(inputStream, null);
    }

    public ChunkedInputStream(InputStream inputStream, HttpMethod httpMethod) {
        this.bof = true;
        this.eof = false;
        this.closed = false;
        this.method = null;
        if (inputStream == null) {
            throw new IllegalArgumentException("InputStream parameter may not be null");
        }
        this.in = inputStream;
        this.method = httpMethod;
        this.pos = 0;
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    static void exhaustInputStream(InputStream inputStream) {
        do {
        } while (inputStream.read(new byte[1024]) >= 0);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static int getChunkSizeFromInputStream(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        char c = 0;
        while (c != 65535) {
            int read = inputStream.read();
            if (read == -1) {
                throw new IOException("chunked stream ended unexpectedly");
            }
            switch (c) {
                case 0:
                    switch (read) {
                        case Route.DrivingNoFastRoad:
                            c = 1;
                            break;
                        case 34:
                            c = 2;
                        default:
                            byteArrayOutputStream.write(read);
                            break;
                    }
                case 1:
                    if (read == 10) {
                        c = 65535;
                        break;
                    } else {
                        throw new IOException("Protocol violation: Unexpected single newline character in chunk size");
                    }
                case 2:
                    switch (read) {
                        case 34:
                            c = 0;
                            byteArrayOutputStream.write(read);
                            break;
                        case 92:
                            byteArrayOutputStream.write(inputStream.read());
                            break;
                        default:
                            byteArrayOutputStream.write(read);
                            break;
                    }
                default:
                    throw new RuntimeException("assertion failed");
            }
        }
        String asciiString = EncodingUtil.getAsciiString(byteArrayOutputStream.toByteArray());
        int indexOf = asciiString.indexOf(59);
        String trim = indexOf > 0 ? asciiString.substring(0, indexOf).trim() : asciiString.trim();
        try {
            return Integer.parseInt(trim.trim(), 16);
        } catch (NumberFormatException e) {
            throw new IOException(new StringBuffer("Bad chunk size: ").append(trim).toString());
        }
    }

    private void nextChunk() {
        if (!this.bof) {
            readCRLF();
        }
        this.chunkSize = getChunkSizeFromInputStream(this.in);
        this.bof = false;
        this.pos = 0;
        if (this.chunkSize == 0) {
            this.eof = true;
            parseTrailerHeaders();
        }
    }

    private void parseTrailerHeaders() {
        String str = "US-ASCII";
        try {
            if (this.method != null) {
                str = this.method.getParams().getHttpElementCharset();
            }
            Header[] parseHeaders = HttpParser.parseHeaders(this.in, str);
            if (this.method != null) {
                for (Header addResponseFooter : parseHeaders) {
                    this.method.addResponseFooter(addResponseFooter);
                }
            }
        } catch (HttpException e) {
            LOG.error("Error parsing trailer headers", e);
            IOException iOException = new IOException(e.getMessage());
            ExceptionUtil.initCause(iOException, e);
            throw iOException;
        }
    }

    private void readCRLF() {
        int read = this.in.read();
        int read2 = this.in.read();
        if (read != 13 || read2 != 10) {
            throw new IOException(new StringBuffer("CRLF expected at end of chunk: ").append(read).append(CookieSpec.PATH_DELIM).append(read2).toString());
        }
    }

    public void close() {
        if (!this.closed) {
            try {
                if (!this.eof) {
                    exhaustInputStream(this);
                }
            } finally {
                this.eof = true;
                this.closed = true;
            }
        }
    }

    public int read() {
        if (this.closed) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.eof) {
            return -1;
        } else {
            if (this.pos >= this.chunkSize) {
                nextChunk();
                if (this.eof) {
                    return -1;
                }
            }
            this.pos++;
            return this.in.read();
        }
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.closed) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.eof) {
            return -1;
        } else {
            if (this.pos >= this.chunkSize) {
                nextChunk();
                if (this.eof) {
                    return -1;
                }
            }
            int read = this.in.read(bArr, i, Math.min(i2, this.chunkSize - this.pos));
            this.pos += read;
            return read;
        }
    }
}
