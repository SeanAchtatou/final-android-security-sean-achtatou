package org.apache.commons.httpclient.auth;

import java.util.Map;

public abstract class RFC2617Scheme implements AuthScheme {
    private Map params = null;

    public RFC2617Scheme() {
    }

    public RFC2617Scheme(String str) {
        processChallenge(str);
    }

    public String getID() {
        return getRealm();
    }

    public String getParameter(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Parameter name may not be null");
        } else if (this.params == null) {
            return null;
        } else {
            return (String) this.params.get(str.toLowerCase());
        }
    }

    /* access modifiers changed from: protected */
    public Map getParameters() {
        return this.params;
    }

    public String getRealm() {
        return getParameter("realm");
    }

    public void processChallenge(String str) {
        if (!AuthChallengeParser.extractScheme(str).equalsIgnoreCase(getSchemeName())) {
            throw new MalformedChallengeException(new StringBuffer("Invalid ").append(getSchemeName()).append(" challenge: ").append(str).toString());
        }
        this.params = AuthChallengeParser.extractParams(str);
    }
}
