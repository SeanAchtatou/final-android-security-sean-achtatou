package org.apache.commons.httpclient.cookie;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HeaderElement;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.util.DateParseException;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CookieSpecBase implements CookieSpec {
    protected static final Log LOG;
    static Class class$org$apache$commons$httpclient$cookie$CookieSpec;
    private Collection datepatterns = null;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$cookie$CookieSpec == null) {
            cls = class$("org.apache.commons.httpclient.cookie.CookieSpec");
            class$org$apache$commons$httpclient$cookie$CookieSpec = cls;
        } else {
            cls = class$org$apache$commons$httpclient$cookie$CookieSpec;
        }
        LOG = LogFactory.getLog(cls);
    }

    private static void addInPathOrder(List list, Cookie cookie) {
        int i;
        int i2 = 0;
        while (true) {
            i = i2;
            if (i >= list.size() || cookie.compare(cookie, (Cookie) list.get(i)) > 0) {
                list.add(i, cookie);
            } else {
                i2 = i + 1;
            }
        }
        list.add(i, cookie);
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public boolean domainMatch(String str, String str2) {
        if (str.equals(str2)) {
            return true;
        }
        if (!str2.startsWith(".")) {
            str2 = new StringBuffer(".").append(str2).toString();
        }
        return str.endsWith(str2) || str.equals(str2.substring(1));
    }

    public String formatCookie(Cookie cookie) {
        LOG.trace("enter CookieSpecBase.formatCookie(Cookie)");
        if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(cookie.getName());
        stringBuffer.append("=");
        String value = cookie.getValue();
        if (value != null) {
            stringBuffer.append(value);
        }
        return stringBuffer.toString();
    }

    public Header formatCookieHeader(Cookie cookie) {
        LOG.trace("enter CookieSpecBase.formatCookieHeader(Cookie)");
        return new Header("Cookie", formatCookie(cookie));
    }

    public Header formatCookieHeader(Cookie[] cookieArr) {
        LOG.trace("enter CookieSpecBase.formatCookieHeader(Cookie[])");
        return new Header("Cookie", formatCookies(cookieArr));
    }

    public String formatCookies(Cookie[] cookieArr) {
        LOG.trace("enter CookieSpecBase.formatCookies(Cookie[])");
        if (cookieArr == null) {
            throw new IllegalArgumentException("Cookie array may not be null");
        } else if (cookieArr.length == 0) {
            throw new IllegalArgumentException("Cookie array may not be empty");
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < cookieArr.length; i++) {
                if (i > 0) {
                    stringBuffer.append("; ");
                }
                stringBuffer.append(formatCookie(cookieArr[i]));
            }
            return stringBuffer.toString();
        }
    }

    public Collection getValidDateFormats() {
        return this.datepatterns;
    }

    public boolean match(String str, int i, String str2, boolean z, Cookie cookie) {
        LOG.trace("enter CookieSpecBase.match(String, int, String, boolean, Cookie");
        if (str == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (str.trim().equals(PoiTypeDef.All)) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (i < 0) {
            throw new IllegalArgumentException(new StringBuffer("Invalid port: ").append(i).toString());
        } else if (str2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else {
            if (str2.trim().equals(PoiTypeDef.All)) {
                str2 = CookieSpec.PATH_DELIM;
            }
            String lowerCase = str.toLowerCase();
            if (cookie.getDomain() == null) {
                LOG.warn("Invalid cookie state: domain not specified");
                return false;
            } else if (cookie.getPath() == null) {
                LOG.warn("Invalid cookie state: path not specified");
                return false;
            } else if ((cookie.getExpiryDate() == null || cookie.getExpiryDate().after(new Date())) && domainMatch(lowerCase, cookie.getDomain()) && pathMatch(str2, cookie.getPath())) {
                return !cookie.getSecure() || z;
            } else {
                return false;
            }
        }
    }

    public Cookie[] match(String str, int i, String str2, boolean z, Cookie[] cookieArr) {
        LOG.trace("enter CookieSpecBase.match(String, int, String, boolean, Cookie[])");
        if (cookieArr == null) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= cookieArr.length) {
                return (Cookie[]) linkedList.toArray(new Cookie[linkedList.size()]);
            }
            if (match(str, i, str2, z, cookieArr[i3])) {
                addInPathOrder(linkedList, cookieArr[i3]);
            }
            i2 = i3 + 1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, boolean):void
      org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, boolean):void */
    public Cookie[] parse(String str, int i, String str2, boolean z, String str3) {
        String str4;
        HeaderElement[] parseElements;
        LOG.trace("enter CookieSpecBase.parse(String, port, path, boolean, Header)");
        if (str == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (str.trim().equals(PoiTypeDef.All)) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (i < 0) {
            throw new IllegalArgumentException(new StringBuffer("Invalid port: ").append(i).toString());
        } else if (str2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else if (str3 == null) {
            throw new IllegalArgumentException("Header may not be null.");
        } else {
            if (str2.trim().equals(PoiTypeDef.All)) {
                str2 = CookieSpec.PATH_DELIM;
            }
            String lowerCase = str.toLowerCase();
            int lastIndexOf = str2.lastIndexOf(CookieSpec.PATH_DELIM);
            if (lastIndexOf >= 0) {
                if (lastIndexOf == 0) {
                    lastIndexOf = 1;
                }
                str4 = str2.substring(0, lastIndexOf);
            } else {
                str4 = str2;
            }
            boolean z2 = false;
            int indexOf = str3.toLowerCase().indexOf("expires=");
            if (indexOf != -1) {
                int i2 = indexOf + 8;
                int indexOf2 = str3.indexOf(";", i2);
                if (indexOf2 == -1) {
                    indexOf2 = str3.length();
                }
                try {
                    DateUtil.parseDate(str3.substring(i2, indexOf2), this.datepatterns);
                    z2 = true;
                } catch (DateParseException e) {
                }
            }
            if (z2) {
                parseElements = new HeaderElement[]{new HeaderElement(str3.toCharArray())};
            } else {
                parseElements = HeaderElement.parseElements(str3.toCharArray());
            }
            Cookie[] cookieArr = new Cookie[parseElements.length];
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 >= parseElements.length) {
                    return cookieArr;
                }
                HeaderElement headerElement = parseElements[i4];
                try {
                    Cookie cookie = new Cookie(lowerCase, headerElement.getName(), headerElement.getValue(), str4, (Date) null, false);
                    NameValuePair[] parameters = headerElement.getParameters();
                    if (parameters != null) {
                        for (NameValuePair parseAttribute : parameters) {
                            parseAttribute(parseAttribute, cookie);
                        }
                    }
                    cookieArr[i4] = cookie;
                    i3 = i4 + 1;
                } catch (IllegalArgumentException e2) {
                    throw new MalformedCookieException(e2.getMessage());
                }
            }
        }
    }

    public Cookie[] parse(String str, int i, String str2, boolean z, Header header) {
        LOG.trace("enter CookieSpecBase.parse(String, port, path, boolean, String)");
        if (header == null) {
            throw new IllegalArgumentException("Header may not be null.");
        }
        return parse(str, i, str2, z, header.getValue());
    }

    public void parseAttribute(NameValuePair nameValuePair, Cookie cookie) {
        if (nameValuePair == null) {
            throw new IllegalArgumentException("Attribute may not be null.");
        } else if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null.");
        } else {
            String lowerCase = nameValuePair.getName().toLowerCase();
            String value = nameValuePair.getValue();
            if (lowerCase.equals(Cookie2.PATH)) {
                if (value == null || value.trim().equals(PoiTypeDef.All)) {
                    value = CookieSpec.PATH_DELIM;
                }
                cookie.setPath(value);
                cookie.setPathAttributeSpecified(true);
            } else if (lowerCase.equals(Cookie2.DOMAIN)) {
                if (value == null) {
                    throw new MalformedCookieException("Missing value for domain attribute");
                } else if (value.trim().equals(PoiTypeDef.All)) {
                    throw new MalformedCookieException("Blank value for domain attribute");
                } else {
                    cookie.setDomain(value);
                    cookie.setDomainAttributeSpecified(true);
                }
            } else if (lowerCase.equals(Cookie2.MAXAGE)) {
                if (value == null) {
                    throw new MalformedCookieException("Missing value for max-age attribute");
                }
                try {
                    cookie.setExpiryDate(new Date(System.currentTimeMillis() + (((long) Integer.parseInt(value)) * 1000)));
                } catch (NumberFormatException e) {
                    throw new MalformedCookieException(new StringBuffer("Invalid max-age attribute: ").append(e.getMessage()).toString());
                }
            } else if (lowerCase.equals(Cookie2.SECURE)) {
                cookie.setSecure(true);
            } else if (lowerCase.equals(Cookie2.COMMENT)) {
                cookie.setComment(value);
            } else if (lowerCase.equals("expires")) {
                if (value == null) {
                    throw new MalformedCookieException("Missing value for expires attribute");
                }
                try {
                    cookie.setExpiryDate(DateUtil.parseDate(value, this.datepatterns));
                } catch (DateParseException e2) {
                    LOG.debug("Error parsing cookie date", e2);
                    throw new MalformedCookieException(new StringBuffer("Unable to parse expiration date parameter: ").append(value).toString());
                }
            } else if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Unrecognized cookie attribute: ").append(nameValuePair.toString()).toString());
            }
        }
    }

    public boolean pathMatch(String str, String str2) {
        boolean startsWith = str.startsWith(str2);
        return (!startsWith || str.length() == str2.length() || str2.endsWith(CookieSpec.PATH_DELIM)) ? startsWith : str.charAt(str2.length()) == CookieSpec.PATH_DELIM_CHAR;
    }

    public void setValidDateFormats(Collection collection) {
        this.datepatterns = collection;
    }

    public void validate(String str, int i, String str2, boolean z, Cookie cookie) {
        LOG.trace("enter CookieSpecBase.validate(String, port, path, boolean, Cookie)");
        if (str == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (str.trim().equals(PoiTypeDef.All)) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (i < 0) {
            throw new IllegalArgumentException(new StringBuffer("Invalid port: ").append(i).toString());
        } else if (str2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else {
            if (str2.trim().equals(PoiTypeDef.All)) {
                str2 = CookieSpec.PATH_DELIM;
            }
            String lowerCase = str.toLowerCase();
            if (cookie.getVersion() < 0) {
                throw new MalformedCookieException(new StringBuffer("Illegal version number ").append(cookie.getValue()).toString());
            }
            if (lowerCase.indexOf(".") >= 0) {
                if (!lowerCase.endsWith(cookie.getDomain())) {
                    String domain = cookie.getDomain();
                    if (domain.startsWith(".")) {
                        domain = domain.substring(1, domain.length());
                    }
                    if (!lowerCase.equals(domain)) {
                        throw new MalformedCookieException(new StringBuffer("Illegal domain attribute \"").append(cookie.getDomain()).append("\". Domain of origin: \"").append(lowerCase).append("\"").toString());
                    }
                }
            } else if (!lowerCase.equals(cookie.getDomain())) {
                throw new MalformedCookieException(new StringBuffer("Illegal domain attribute \"").append(cookie.getDomain()).append("\". Domain of origin: \"").append(lowerCase).append("\"").toString());
            }
            if (!str2.startsWith(cookie.getPath())) {
                throw new MalformedCookieException(new StringBuffer("Illegal path attribute \"").append(cookie.getPath()).append("\". Path of origin: \"").append(str2).append("\"").toString());
            }
        }
    }
}
