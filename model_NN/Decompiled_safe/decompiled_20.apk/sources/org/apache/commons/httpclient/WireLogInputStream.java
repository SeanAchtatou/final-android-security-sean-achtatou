package org.apache.commons.httpclient;

import java.io.FilterInputStream;
import java.io.InputStream;

class WireLogInputStream extends FilterInputStream {
    private InputStream in;
    private Wire wire;

    public WireLogInputStream(InputStream inputStream, Wire wire2) {
        super(inputStream);
        this.in = inputStream;
        this.wire = wire2;
    }

    public int read() {
        int read = this.in.read();
        if (read > 0) {
            this.wire.input(read);
        }
        return read;
    }

    public int read(byte[] bArr) {
        int read = this.in.read(bArr);
        if (read > 0) {
            this.wire.input(bArr, 0, read);
        }
        return read;
    }

    public int read(byte[] bArr, int i, int i2) {
        int read = this.in.read(bArr, i, i2);
        if (read > 0) {
            this.wire.input(bArr, i, read);
        }
        return read;
    }
}
