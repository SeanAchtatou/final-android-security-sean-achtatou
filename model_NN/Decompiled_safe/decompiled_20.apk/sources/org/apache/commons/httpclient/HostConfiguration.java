package org.apache.commons.httpclient;

import java.net.InetAddress;
import org.apache.commons.httpclient.params.HostParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.util.LangUtils;

public class HostConfiguration implements Cloneable {
    public static final HostConfiguration ANY_HOST_CONFIGURATION = new HostConfiguration();
    private HttpHost host = null;
    private InetAddress localAddress = null;
    private HostParams params = new HostParams();
    private ProxyHost proxyHost = null;

    public HostConfiguration() {
    }

    public HostConfiguration(HostConfiguration hostConfiguration) {
        init(hostConfiguration);
    }

    private void init(HostConfiguration hostConfiguration) {
        synchronized (hostConfiguration) {
            try {
                if (hostConfiguration.host != null) {
                    this.host = (HttpHost) hostConfiguration.host.clone();
                } else {
                    this.host = null;
                }
                if (hostConfiguration.proxyHost != null) {
                    this.proxyHost = (ProxyHost) hostConfiguration.proxyHost.clone();
                } else {
                    this.proxyHost = null;
                }
                this.localAddress = hostConfiguration.getLocalAddress();
                this.params = (HostParams) hostConfiguration.getParams().clone();
            } catch (CloneNotSupportedException e) {
                throw new IllegalArgumentException("Host configuration could not be cloned");
            }
        }
    }

    public Object clone() {
        try {
            HostConfiguration hostConfiguration = (HostConfiguration) super.clone();
            hostConfiguration.init(this);
            return hostConfiguration;
        } catch (CloneNotSupportedException e) {
            throw new IllegalArgumentException("Host configuration could not be cloned");
        }
    }

    public synchronized boolean equals(Object obj) {
        boolean z = true;
        synchronized (this) {
            if (!(obj instanceof HostConfiguration)) {
                z = false;
            } else if (obj != this) {
                HostConfiguration hostConfiguration = (HostConfiguration) obj;
                if (!LangUtils.equals(this.host, hostConfiguration.host) || !LangUtils.equals(this.proxyHost, hostConfiguration.proxyHost) || !LangUtils.equals(this.localAddress, hostConfiguration.localAddress)) {
                    z = false;
                }
            }
        }
        return z;
    }

    public synchronized String getHost() {
        return this.host != null ? this.host.getHostName() : null;
    }

    public synchronized String getHostURL() {
        if (this.host == null) {
            throw new IllegalStateException("Host must be set to create a host URL");
        }
        return this.host.toURI();
    }

    public synchronized InetAddress getLocalAddress() {
        return this.localAddress;
    }

    public HostParams getParams() {
        return this.params;
    }

    public synchronized int getPort() {
        return this.host != null ? this.host.getPort() : -1;
    }

    public synchronized Protocol getProtocol() {
        return this.host != null ? this.host.getProtocol() : null;
    }

    public synchronized String getProxyHost() {
        return this.proxyHost != null ? this.proxyHost.getHostName() : null;
    }

    public synchronized int getProxyPort() {
        return this.proxyHost != null ? this.proxyHost.getPort() : -1;
    }

    public synchronized String getVirtualHost() {
        return this.params.getVirtualHost();
    }

    public synchronized int hashCode() {
        return LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(17, this.host), this.proxyHost), this.localAddress);
    }

    public synchronized boolean hostEquals(HttpConnection httpConnection) {
        boolean z = false;
        synchronized (this) {
            if (httpConnection == null) {
                throw new IllegalArgumentException("Connection may not be null");
            } else if (this.host != null && this.host.getHostName().equalsIgnoreCase(httpConnection.getHost())) {
                if (this.host.getPort() == httpConnection.getPort() && this.host.getProtocol().equals(httpConnection.getProtocol()) && (this.localAddress == null ? httpConnection.getLocalAddress() == null : this.localAddress.equals(httpConnection.getLocalAddress()))) {
                    z = true;
                }
            }
        }
        return z;
    }

    public synchronized boolean isHostSet() {
        return this.host != null;
    }

    public synchronized boolean isProxySet() {
        return this.proxyHost != null;
    }

    public synchronized boolean proxyEquals(HttpConnection httpConnection) {
        boolean z = true;
        synchronized (this) {
            if (httpConnection == null) {
                throw new IllegalArgumentException("Connection may not be null");
            } else if (this.proxyHost != null) {
                if (!this.proxyHost.getHostName().equalsIgnoreCase(httpConnection.getProxyHost()) || this.proxyHost.getPort() != httpConnection.getProxyPort()) {
                    z = false;
                }
            } else if (httpConnection.getProxyHost() != null) {
                z = false;
            }
        }
        return z;
    }

    public synchronized void setHost(String str) {
        Protocol protocol = Protocol.getProtocol("http");
        setHost(str, protocol.getDefaultPort(), protocol);
    }

    public synchronized void setHost(String str, int i) {
        setHost(str, i, Protocol.getProtocol("http"));
    }

    public synchronized void setHost(String str, int i, String str2) {
        this.host = new HttpHost(str, i, Protocol.getProtocol(str2));
    }

    public synchronized void setHost(String str, int i, Protocol protocol) {
        if (str == null) {
            throw new IllegalArgumentException("host must not be null");
        } else if (protocol == null) {
            throw new IllegalArgumentException("protocol must not be null");
        } else {
            this.host = new HttpHost(str, i, protocol);
        }
    }

    public synchronized void setHost(String str, String str2, int i, Protocol protocol) {
        setHost(str, i, protocol);
        this.params.setVirtualHost(str2);
    }

    public synchronized void setHost(HttpHost httpHost) {
        this.host = httpHost;
    }

    public synchronized void setHost(URI uri) {
        try {
            setHost(uri.getHost(), uri.getPort(), uri.getScheme());
        } catch (URIException e) {
            throw new IllegalArgumentException(e.toString());
        }
    }

    public synchronized void setLocalAddress(InetAddress inetAddress) {
        this.localAddress = inetAddress;
    }

    public void setParams(HostParams hostParams) {
        if (hostParams == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = hostParams;
    }

    public synchronized void setProxy(String str, int i) {
        this.proxyHost = new ProxyHost(str, i);
    }

    public synchronized void setProxyHost(ProxyHost proxyHost2) {
        this.proxyHost = proxyHost2;
    }

    public synchronized String toString() {
        StringBuffer stringBuffer;
        boolean z = false;
        stringBuffer = new StringBuffer(50);
        stringBuffer.append("HostConfiguration[");
        if (this.host != null) {
            stringBuffer.append("host=").append(this.host);
            z = true;
        }
        if (this.proxyHost != null) {
            if (z) {
                stringBuffer.append(", ");
            } else {
                z = true;
            }
            stringBuffer.append("proxyHost=").append(this.proxyHost);
        }
        if (this.localAddress != null) {
            if (z) {
                stringBuffer.append(", ");
            }
            stringBuffer.append("localAddress=").append(this.localAddress);
            stringBuffer.append(", ");
            stringBuffer.append("params=").append(this.params);
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
}
