package org.apache.commons.httpclient.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {
    private static final Collection DEFAULT_PATTERNS = Arrays.asList("EEE MMM d HH:mm:ss yyyy", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE, dd MMM yyyy HH:mm:ss zzz");
    private static final Date DEFAULT_TWO_DIGIT_YEAR_START;
    private static final TimeZone GMT = TimeZone.getTimeZone("GMT");
    public static final String PATTERN_ASCTIME = "EEE MMM d HH:mm:ss yyyy";
    public static final String PATTERN_RFC1036 = "EEEE, dd-MMM-yy HH:mm:ss zzz";
    public static final String PATTERN_RFC1123 = "EEE, dd MMM yyyy HH:mm:ss zzz";

    static {
        Calendar instance = Calendar.getInstance();
        instance.set(2000, 0, 1, 0, 0);
        DEFAULT_TWO_DIGIT_YEAR_START = instance.getTime();
    }

    private DateUtil() {
    }

    public static String formatDate(Date date) {
        return formatDate(date, "EEE, dd MMM yyyy HH:mm:ss zzz");
    }

    public static String formatDate(Date date, String str) {
        if (date == null) {
            throw new IllegalArgumentException("date is null");
        } else if (str == null) {
            throw new IllegalArgumentException("pattern is null");
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str, Locale.US);
            simpleDateFormat.setTimeZone(GMT);
            return simpleDateFormat.format(date);
        }
    }

    public static Date parseDate(String str) {
        return parseDate(str, null, null);
    }

    public static Date parseDate(String str, Collection collection) {
        return parseDate(str, collection, null);
    }

    public static Date parseDate(String str, Collection collection, Date date) {
        if (str == null) {
            throw new IllegalArgumentException("dateValue is null");
        }
        if (collection == null) {
            collection = DEFAULT_PATTERNS;
        }
        if (date == null) {
            date = DEFAULT_TWO_DIGIT_YEAR_START;
        }
        if (str.length() > 1 && str.startsWith("'") && str.endsWith("'")) {
            str = str.substring(1, str.length() - 1);
        }
        SimpleDateFormat simpleDateFormat = null;
        Iterator it = collection.iterator();
        while (true) {
            SimpleDateFormat simpleDateFormat2 = simpleDateFormat;
            if (it.hasNext()) {
                String str2 = (String) it.next();
                if (simpleDateFormat2 == null) {
                    SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat(str2, Locale.US);
                    simpleDateFormat3.setTimeZone(TimeZone.getTimeZone("GMT"));
                    simpleDateFormat3.set2DigitYearStart(date);
                    simpleDateFormat = simpleDateFormat3;
                } else {
                    simpleDateFormat2.applyPattern(str2);
                    simpleDateFormat = simpleDateFormat2;
                }
                try {
                    return simpleDateFormat.parse(str);
                } catch (ParseException e) {
                }
            } else {
                throw new DateParseException(new StringBuffer("Unable to parse the date ").append(str).toString());
            }
        }
    }
}
