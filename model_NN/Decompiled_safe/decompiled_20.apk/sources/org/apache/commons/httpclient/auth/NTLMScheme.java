package org.apache.commons.httpclient.auth;

import com.amap.api.search.poisearch.PoiTypeDef;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NTCredentials;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NTLMScheme implements AuthScheme {
    private static final int FAILED = Integer.MAX_VALUE;
    private static final int INITIATED = 1;
    private static final Log LOG;
    private static final int TYPE1_MSG_GENERATED = 2;
    private static final int TYPE2_MSG_RECEIVED = 3;
    private static final int TYPE3_MSG_GENERATED = 4;
    private static final int UNINITIATED = 0;
    static Class class$org$apache$commons$httpclient$auth$NTLMScheme;
    private String ntlmchallenge;
    private int state;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$auth$NTLMScheme == null) {
            cls = class$("org.apache.commons.httpclient.auth.NTLMScheme");
            class$org$apache$commons$httpclient$auth$NTLMScheme = cls;
        } else {
            cls = class$org$apache$commons$httpclient$auth$NTLMScheme;
        }
        LOG = LogFactory.getLog(cls);
    }

    public NTLMScheme() {
        this.ntlmchallenge = null;
        this.state = 0;
    }

    public NTLMScheme(String str) {
        this.ntlmchallenge = null;
        processChallenge(str);
    }

    public static String authenticate(NTCredentials nTCredentials, String str) {
        LOG.trace("enter NTLMScheme.authenticate(NTCredentials, String)");
        if (nTCredentials == null) {
            throw new IllegalArgumentException("Credentials may not be null");
        }
        return new StringBuffer("NTLM ").append(new NTLM().getResponseFor(str, nTCredentials.getUserName(), nTCredentials.getPassword(), nTCredentials.getHost(), nTCredentials.getDomain())).toString();
    }

    public static String authenticate(NTCredentials nTCredentials, String str, String str2) {
        LOG.trace("enter NTLMScheme.authenticate(NTCredentials, String)");
        if (nTCredentials == null) {
            throw new IllegalArgumentException("Credentials may not be null");
        }
        NTLM ntlm = new NTLM();
        ntlm.setCredentialCharset(str2);
        return new StringBuffer("NTLM ").append(ntlm.getResponseFor(str, nTCredentials.getUserName(), nTCredentials.getPassword(), nTCredentials.getHost(), nTCredentials.getDomain())).toString();
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public String authenticate(Credentials credentials, String str, String str2) {
        LOG.trace("enter NTLMScheme.authenticate(Credentials, String, String)");
        try {
            return authenticate((NTCredentials) credentials, this.ntlmchallenge);
        } catch (ClassCastException e) {
            throw new InvalidCredentialsException(new StringBuffer("Credentials cannot be used for NTLM authentication: ").append(credentials.getClass().getName()).toString());
        }
    }

    public String authenticate(Credentials credentials, HttpMethod httpMethod) {
        String str;
        LOG.trace("enter NTLMScheme.authenticate(Credentials, HttpMethod)");
        if (this.state == 0) {
            throw new IllegalStateException("NTLM authentication process has not been initiated");
        }
        try {
            NTCredentials nTCredentials = (NTCredentials) credentials;
            NTLM ntlm = new NTLM();
            ntlm.setCredentialCharset(httpMethod.getParams().getCredentialCharset());
            if (this.state == 1 || this.state == FAILED) {
                str = ntlm.getType1Message(nTCredentials.getHost(), nTCredentials.getDomain());
                this.state = 2;
            } else {
                str = ntlm.getType3Message(nTCredentials.getUserName(), nTCredentials.getPassword(), nTCredentials.getHost(), nTCredentials.getDomain(), ntlm.parseType2Message(this.ntlmchallenge));
                this.state = 4;
            }
            return new StringBuffer("NTLM ").append(str).toString();
        } catch (ClassCastException e) {
            throw new InvalidCredentialsException(new StringBuffer("Credentials cannot be used for NTLM authentication: ").append(credentials.getClass().getName()).toString());
        }
    }

    public String getID() {
        return this.ntlmchallenge;
    }

    public String getParameter(String str) {
        if (str != null) {
            return null;
        }
        throw new IllegalArgumentException("Parameter name may not be null");
    }

    public String getRealm() {
        return null;
    }

    public String getSchemeName() {
        return "ntlm";
    }

    public boolean isComplete() {
        return this.state == 4 || this.state == FAILED;
    }

    public boolean isConnectionBased() {
        return true;
    }

    public void processChallenge(String str) {
        if (!AuthChallengeParser.extractScheme(str).equalsIgnoreCase(getSchemeName())) {
            throw new MalformedChallengeException(new StringBuffer("Invalid NTLM challenge: ").append(str).toString());
        }
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            this.ntlmchallenge = str.substring(indexOf, str.length()).trim();
            this.state = 3;
            return;
        }
        this.ntlmchallenge = PoiTypeDef.All;
        if (this.state == 0) {
            this.state = 1;
        } else {
            this.state = FAILED;
        }
    }
}
