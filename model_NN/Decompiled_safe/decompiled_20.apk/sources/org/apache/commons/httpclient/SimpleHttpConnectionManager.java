package org.apache.commons.httpclient;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SimpleHttpConnectionManager implements HttpConnectionManager {
    private static final Log LOG;
    private static final String MISUSE_MESSAGE = "SimpleHttpConnectionManager being used incorrectly.  Be sure that HttpMethod.releaseConnection() is always called and that only one thread and/or method is using this connection manager at a time.";
    static Class class$org$apache$commons$httpclient$SimpleHttpConnectionManager;
    private boolean alwaysClose = false;
    protected HttpConnection httpConnection;
    private long idleStartTime = Long.MAX_VALUE;
    private volatile boolean inUse = false;
    private HttpConnectionManagerParams params = new HttpConnectionManagerParams();

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$SimpleHttpConnectionManager == null) {
            cls = class$("org.apache.commons.httpclient.SimpleHttpConnectionManager");
            class$org$apache$commons$httpclient$SimpleHttpConnectionManager = cls;
        } else {
            cls = class$org$apache$commons$httpclient$SimpleHttpConnectionManager;
        }
        LOG = LogFactory.getLog(cls);
    }

    public SimpleHttpConnectionManager() {
    }

    public SimpleHttpConnectionManager(boolean z) {
        this.alwaysClose = z;
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    static void finishLastResponse(HttpConnection httpConnection2) {
        InputStream lastResponseInputStream = httpConnection2.getLastResponseInputStream();
        if (lastResponseInputStream != null) {
            httpConnection2.setLastResponseInputStream(null);
            try {
                lastResponseInputStream.close();
            } catch (IOException e) {
                httpConnection2.close();
            }
        }
    }

    public void closeIdleConnections(long j) {
        if (this.idleStartTime <= System.currentTimeMillis() - j) {
            this.httpConnection.close();
        }
    }

    public HttpConnection getConnection(HostConfiguration hostConfiguration) {
        return getConnection(hostConfiguration, 0);
    }

    public HttpConnection getConnection(HostConfiguration hostConfiguration, long j) {
        return getConnectionWithTimeout(hostConfiguration, j);
    }

    public HttpConnection getConnectionWithTimeout(HostConfiguration hostConfiguration, long j) {
        if (this.httpConnection == null) {
            this.httpConnection = new HttpConnection(hostConfiguration);
            this.httpConnection.setHttpConnectionManager(this);
            this.httpConnection.getParams().setDefaults(this.params);
        } else if (!hostConfiguration.hostEquals(this.httpConnection) || !hostConfiguration.proxyEquals(this.httpConnection)) {
            if (this.httpConnection.isOpen()) {
                this.httpConnection.close();
            }
            this.httpConnection.setHost(hostConfiguration.getHost());
            this.httpConnection.setPort(hostConfiguration.getPort());
            this.httpConnection.setProtocol(hostConfiguration.getProtocol());
            this.httpConnection.setLocalAddress(hostConfiguration.getLocalAddress());
            this.httpConnection.setProxyHost(hostConfiguration.getProxyHost());
            this.httpConnection.setProxyPort(hostConfiguration.getProxyPort());
        } else {
            finishLastResponse(this.httpConnection);
        }
        this.idleStartTime = Long.MAX_VALUE;
        if (this.inUse) {
            LOG.warn(MISUSE_MESSAGE);
        }
        this.inUse = true;
        return this.httpConnection;
    }

    public HttpConnectionManagerParams getParams() {
        return this.params;
    }

    public boolean isConnectionStaleCheckingEnabled() {
        return this.params.isStaleCheckingEnabled();
    }

    public void releaseConnection(HttpConnection httpConnection2) {
        if (httpConnection2 != this.httpConnection) {
            throw new IllegalStateException("Unexpected release of an unknown connection.");
        }
        if (this.alwaysClose) {
            this.httpConnection.close();
        } else {
            finishLastResponse(this.httpConnection);
        }
        this.inUse = false;
        this.idleStartTime = System.currentTimeMillis();
    }

    public void setConnectionStaleCheckingEnabled(boolean z) {
        this.params.setStaleCheckingEnabled(z);
    }

    public void setParams(HttpConnectionManagerParams httpConnectionManagerParams) {
        if (httpConnectionManagerParams == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = httpConnectionManagerParams;
    }

    public void shutdown() {
        this.httpConnection.close();
    }
}
