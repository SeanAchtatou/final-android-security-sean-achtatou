package org.apache.commons.httpclient.auth;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.httpclient.params.HttpParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class AuthChallengeProcessor {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$auth$AuthChallengeProcessor;
    private HttpParams params = null;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$auth$AuthChallengeProcessor == null) {
            cls = class$("org.apache.commons.httpclient.auth.AuthChallengeProcessor");
            class$org$apache$commons$httpclient$auth$AuthChallengeProcessor = cls;
        } else {
            cls = class$org$apache$commons$httpclient$auth$AuthChallengeProcessor;
        }
        LOG = LogFactory.getLog(cls);
    }

    public AuthChallengeProcessor(HttpParams httpParams) {
        if (httpParams == null) {
            throw new IllegalArgumentException("Parameter collection may not be null");
        }
        this.params = httpParams;
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public final AuthScheme processChallenge(AuthState authState, Map map) {
        if (authState == null) {
            throw new IllegalArgumentException("Authentication state may not be null");
        } else if (map == null) {
            throw new IllegalArgumentException("Challenge map may not be null");
        } else {
            if (authState.isPreemptive() || authState.getAuthScheme() == null) {
                authState.setAuthScheme(selectAuthScheme(map));
            }
            AuthScheme authScheme = authState.getAuthScheme();
            String schemeName = authScheme.getSchemeName();
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Using authentication scheme: ").append(schemeName).toString());
            }
            String str = (String) map.get(schemeName.toLowerCase());
            if (str == null) {
                throw new AuthenticationException(new StringBuffer().append(schemeName).append(" authorization challenge expected, but not found").toString());
            }
            authScheme.processChallenge(str);
            LOG.debug("Authorization challenge processed");
            return authScheme;
        }
    }

    public final AuthScheme selectAuthScheme(Map map) {
        AuthScheme authScheme;
        if (map == null) {
            throw new IllegalArgumentException("Challenge map may not be null");
        }
        Collection collection = (Collection) this.params.getParameter(AuthPolicy.AUTH_SCHEME_PRIORITY);
        if (collection == null || collection.isEmpty()) {
            collection = AuthPolicy.getDefaultAuthPrefs();
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("Supported authentication schemes in the order of preference: ").append(collection).toString());
        }
        Iterator it = collection.iterator();
        while (true) {
            if (!it.hasNext()) {
                authScheme = null;
                break;
            }
            String str = (String) it.next();
            if (((String) map.get(str.toLowerCase())) != null) {
                if (LOG.isInfoEnabled()) {
                    LOG.info(new StringBuffer().append(str).append(" authentication scheme selected").toString());
                }
                try {
                    authScheme = AuthPolicy.getAuthScheme(str);
                } catch (IllegalStateException e) {
                    throw new AuthChallengeException(e.getMessage());
                }
            } else if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer("Challenge for ").append(str).append(" authentication scheme not available").toString());
            }
        }
        if (authScheme != null) {
            return authScheme;
        }
        throw new AuthChallengeException(new StringBuffer("Unable to respond to any of these challenges: ").append(map).toString());
    }
}
