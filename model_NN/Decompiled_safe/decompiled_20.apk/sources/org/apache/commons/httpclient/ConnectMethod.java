package org.apache.commons.httpclient;

import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ConnectMethod extends HttpMethodBase {
    private static final Log LOG;
    public static final String NAME = "CONNECT";
    static Class class$org$apache$commons$httpclient$ConnectMethod;
    private final HostConfiguration targethost;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$ConnectMethod == null) {
            cls = class$("org.apache.commons.httpclient.ConnectMethod");
            class$org$apache$commons$httpclient$ConnectMethod = cls;
        } else {
            cls = class$org$apache$commons$httpclient$ConnectMethod;
        }
        LOG = LogFactory.getLog(cls);
    }

    public ConnectMethod() {
        this.targethost = null;
    }

    public ConnectMethod(HostConfiguration hostConfiguration) {
        if (hostConfiguration == null) {
            throw new IllegalArgumentException("Target host may not be null");
        }
        this.targethost = hostConfiguration;
    }

    public ConnectMethod(HttpMethod httpMethod) {
        this.targethost = null;
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void addCookieRequestHeader(HttpState httpState, HttpConnection httpConnection) {
    }

    /* access modifiers changed from: protected */
    public void addRequestHeaders(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter ConnectMethod.addRequestHeaders(HttpState, HttpConnection)");
        addUserAgentRequestHeader(httpState, httpConnection);
        addHostRequestHeader(httpState, httpConnection);
        addProxyConnectionHeader(httpState, httpConnection);
    }

    public int execute(HttpState httpState, HttpConnection httpConnection) {
        LOG.trace("enter ConnectMethod.execute(HttpState, HttpConnection)");
        int execute = super.execute(httpState, httpConnection);
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer("CONNECT status code ").append(execute).toString());
        }
        return execute;
    }

    public String getName() {
        return NAME;
    }

    public String getPath() {
        if (this.targethost == null) {
            return CookieSpec.PATH_DELIM;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.targethost.getHost());
        int port = this.targethost.getPort();
        if (port == -1) {
            port = this.targethost.getProtocol().getDefaultPort();
        }
        stringBuffer.append(':');
        stringBuffer.append(port);
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      org.apache.commons.httpclient.URI.<init>(java.lang.String, java.lang.String, java.lang.String):void
      org.apache.commons.httpclient.URI.<init>(org.apache.commons.httpclient.URI, java.lang.String, boolean):void
      org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void */
    public URI getURI() {
        return new URI(getPath(), true, getParams().getUriCharset());
    }

    /* access modifiers changed from: protected */
    public boolean shouldCloseConnection(HttpConnection httpConnection) {
        if (getStatusCode() != 200) {
            return super.shouldCloseConnection(httpConnection);
        }
        Header header = null;
        if (!httpConnection.isTransparent()) {
            header = getResponseHeader("proxy-connection");
        }
        if (header == null) {
            header = getResponseHeader("connection");
        }
        if (header != null && header.getValue().equalsIgnoreCase("close") && LOG.isWarnEnabled()) {
            LOG.warn(new StringBuffer("Invalid header encountered '").append(header.toExternalForm()).append("' in response ").append(getStatusLine().toString()).toString());
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void writeRequestLine(HttpState httpState, HttpConnection httpConnection) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getName());
        stringBuffer.append(' ');
        if (this.targethost != null) {
            stringBuffer.append(getPath());
        } else {
            int port = httpConnection.getPort();
            if (port == -1) {
                port = httpConnection.getProtocol().getDefaultPort();
            }
            stringBuffer.append(httpConnection.getHost());
            stringBuffer.append(':');
            stringBuffer.append(port);
        }
        stringBuffer.append(" ");
        stringBuffer.append(getEffectiveVersion());
        String stringBuffer2 = stringBuffer.toString();
        httpConnection.printLine(stringBuffer2, getParams().getHttpElementCharset());
        if (Wire.HEADER_WIRE.enabled()) {
            Wire.HEADER_WIRE.output(stringBuffer2);
        }
    }
}
