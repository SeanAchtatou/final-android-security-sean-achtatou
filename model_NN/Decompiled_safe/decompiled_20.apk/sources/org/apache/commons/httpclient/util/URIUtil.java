package org.apache.commons.httpclient.util;

import java.util.BitSet;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class URIUtil {
    protected static final BitSet empty = new BitSet(1);

    public class Coder extends URI {
        protected Coder() {
        }

        public static String decode(char[] cArr, String str) {
            return URI.decode(cArr, str);
        }

        public static char[] encode(String str, BitSet bitSet, String str2) {
            return URI.encode(str, bitSet, str2);
        }

        public static String replace(String str, char c, char c2) {
            StringBuffer stringBuffer = new StringBuffer(str.length());
            int i = 0;
            while (true) {
                int indexOf = str.indexOf(c);
                if (indexOf >= 0) {
                    stringBuffer.append(str.substring(0, indexOf));
                    stringBuffer.append(c2);
                } else {
                    stringBuffer.append(str.substring(i));
                }
                if (indexOf < 0) {
                    return stringBuffer.toString();
                }
                i = indexOf;
            }
        }

        public static String replace(String str, char[] cArr, char[] cArr2) {
            for (int length = cArr.length; length > 0; length--) {
                str = replace(str, cArr[length], cArr2[length]);
            }
            return str;
        }

        public static boolean verifyEscaped(char[] cArr) {
            int i = 0;
            while (i < cArr.length) {
                char c = cArr[i];
                if (c > 128) {
                    return false;
                }
                if (c == '%') {
                    int i2 = i + 1;
                    if (Character.digit(cArr[i2], 16) == -1) {
                        return false;
                    }
                    i = i2 + 1;
                    if (Character.digit(cArr[i], 16) == -1) {
                        return false;
                    }
                }
                i++;
            }
            return true;
        }
    }

    public static String decode(String str) {
        try {
            return EncodingUtil.getString(URLCodec.decodeUrl(EncodingUtil.getAsciiBytes(str)), URI.getDefaultProtocolCharset());
        } catch (DecoderException e) {
            throw new URIException(e.getMessage());
        }
    }

    public static String decode(String str, String str2) {
        return Coder.decode(str.toCharArray(), str2);
    }

    public static String encode(String str, BitSet bitSet) {
        return encode(str, bitSet, URI.getDefaultProtocolCharset());
    }

    public static String encode(String str, BitSet bitSet, String str2) {
        return EncodingUtil.getAsciiString(URLCodec.encodeUrl(bitSet, EncodingUtil.getBytes(str, str2)));
    }

    public static String encodeAll(String str) {
        return encodeAll(str, URI.getDefaultProtocolCharset());
    }

    public static String encodeAll(String str, String str2) {
        return encode(str, empty, str2);
    }

    public static String encodePath(String str) {
        return encodePath(str, URI.getDefaultProtocolCharset());
    }

    public static String encodePath(String str, String str2) {
        return encode(str, URI.allowed_abs_path, str2);
    }

    public static String encodePathQuery(String str) {
        return encodePathQuery(str, URI.getDefaultProtocolCharset());
    }

    public static String encodePathQuery(String str, String str2) {
        int indexOf = str.indexOf(63);
        return indexOf < 0 ? encode(str, URI.allowed_abs_path, str2) : new StringBuffer().append(encode(str.substring(0, indexOf), URI.allowed_abs_path, str2)).append('?').append(encode(str.substring(indexOf + 1), URI.allowed_query, str2)).toString();
    }

    public static String encodeQuery(String str) {
        return encodeQuery(str, URI.getDefaultProtocolCharset());
    }

    public static String encodeQuery(String str, String str2) {
        return encode(str, URI.allowed_query, str2);
    }

    public static String encodeWithinAuthority(String str) {
        return encodeWithinAuthority(str, URI.getDefaultProtocolCharset());
    }

    public static String encodeWithinAuthority(String str, String str2) {
        return encode(str, URI.allowed_within_authority, str2);
    }

    public static String encodeWithinPath(String str) {
        return encodeWithinPath(str, URI.getDefaultProtocolCharset());
    }

    public static String encodeWithinPath(String str, String str2) {
        return encode(str, URI.allowed_within_path, str2);
    }

    public static String encodeWithinQuery(String str) {
        return encodeWithinQuery(str, URI.getDefaultProtocolCharset());
    }

    public static String encodeWithinQuery(String str, String str2) {
        return encode(str, URI.allowed_within_query, str2);
    }

    public static String getFromPath(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf("//");
        int indexOf2 = str.indexOf(CookieSpec.PATH_DELIM, (indexOf < 0 || str.lastIndexOf(CookieSpec.PATH_DELIM, indexOf + -1) >= 0) ? 0 : indexOf + 2);
        return indexOf2 < 0 ? indexOf >= 0 ? CookieSpec.PATH_DELIM : str : str.substring(indexOf2);
    }

    public static String getName(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        String path = getPath(str);
        int lastIndexOf = path.lastIndexOf(CookieSpec.PATH_DELIM);
        return lastIndexOf >= 0 ? path.substring(lastIndexOf + 1, path.length()) : path;
    }

    public static String getPath(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf("//");
        int indexOf2 = str.indexOf(CookieSpec.PATH_DELIM, (indexOf < 0 || str.lastIndexOf(CookieSpec.PATH_DELIM, indexOf + -1) >= 0) ? 0 : indexOf + 2);
        int length = str.length();
        if (str.indexOf(63, indexOf2) != -1) {
            length = str.indexOf(63, indexOf2);
        }
        if (str.lastIndexOf("#") > indexOf2 && str.lastIndexOf("#") < length) {
            length = str.lastIndexOf("#");
        }
        return indexOf2 < 0 ? indexOf >= 0 ? CookieSpec.PATH_DELIM : str : str.substring(indexOf2, length);
    }

    public static String getPathQuery(String str) {
        if (str == null) {
            return null;
        }
        int indexOf = str.indexOf("//");
        int indexOf2 = str.indexOf(CookieSpec.PATH_DELIM, (indexOf < 0 || str.lastIndexOf(CookieSpec.PATH_DELIM, indexOf + -1) >= 0) ? 0 : indexOf + 2);
        int length = str.length();
        if (str.lastIndexOf("#") > indexOf2) {
            length = str.lastIndexOf("#");
        }
        return indexOf2 < 0 ? indexOf >= 0 ? CookieSpec.PATH_DELIM : str : str.substring(indexOf2, length);
    }

    public static String getQuery(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        int indexOf = str.indexOf("//");
        int indexOf2 = str.indexOf(CookieSpec.PATH_DELIM, (indexOf < 0 || str.lastIndexOf(CookieSpec.PATH_DELIM, indexOf + -1) >= 0) ? 0 : indexOf + 2);
        int length = str.length();
        int indexOf3 = str.indexOf("?", indexOf2);
        if (indexOf3 < 0) {
            return null;
        }
        int i = indexOf3 + 1;
        if (str.lastIndexOf("#") > i) {
            length = str.lastIndexOf("#");
        }
        if (i < 0 || i == length) {
            return null;
        }
        return str.substring(i, length);
    }
}
