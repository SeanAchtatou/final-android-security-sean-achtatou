package org.apache.commons.httpclient.protocol;

import java.net.Socket;

public interface SecureProtocolSocketFactory extends ProtocolSocketFactory {
    Socket createSocket(Socket socket, String str, int i, boolean z);
}
