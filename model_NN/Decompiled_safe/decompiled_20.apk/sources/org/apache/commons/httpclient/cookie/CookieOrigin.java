package org.apache.commons.httpclient.cookie;

import com.amap.api.search.poisearch.PoiTypeDef;

public final class CookieOrigin {
    private final String host;
    private final String path;
    private final int port;
    private final boolean secure;

    public CookieOrigin(String str, int i, String str2, boolean z) {
        if (str == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (str.trim().equals(PoiTypeDef.All)) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (i < 0) {
            throw new IllegalArgumentException(new StringBuffer("Invalid port: ").append(i).toString());
        } else if (str2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else {
            this.host = str;
            this.port = i;
            this.path = str2;
            this.secure = z;
        }
    }

    public final String getHost() {
        return this.host;
    }

    public final String getPath() {
        return this.path;
    }

    public final int getPort() {
        return this.port;
    }

    public final boolean isSecure() {
        return this.secure;
    }
}
