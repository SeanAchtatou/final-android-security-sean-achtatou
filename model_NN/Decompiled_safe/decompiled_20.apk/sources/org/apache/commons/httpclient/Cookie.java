package org.apache.commons.httpclient;

import com.amap.api.search.poisearch.PoiTypeDef;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.util.LangUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Cookie extends NameValuePair implements Serializable, Comparator {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$Cookie;
    private String cookieComment;
    private String cookieDomain;
    private Date cookieExpiryDate;
    private String cookiePath;
    private int cookieVersion;
    private boolean hasDomainAttribute;
    private boolean hasPathAttribute;
    private boolean isSecure;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$Cookie == null) {
            cls = class$("org.apache.commons.httpclient.Cookie");
            class$org$apache$commons$httpclient$Cookie = cls;
        } else {
            cls = class$org$apache$commons$httpclient$Cookie;
        }
        LOG = LogFactory.getLog(cls);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, boolean):void
     arg types: [?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, boolean):void
      org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, boolean):void */
    public Cookie() {
        this((String) null, "noname", (String) null, (String) null, (Date) null, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, boolean):void
      org.apache.commons.httpclient.Cookie.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, boolean):void */
    public Cookie(String str, String str2, String str3) {
        this(str, str2, str3, (String) null, (Date) null, false);
    }

    public Cookie(String str, String str2, String str3, String str4, int i, boolean z) {
        this(str, str2, str3, str4, (Date) null, z);
        if (i < -1) {
            throw new IllegalArgumentException(new StringBuffer("Invalid max age:  ").append(Integer.toString(i)).toString());
        } else if (i >= 0) {
            setExpiryDate(new Date(System.currentTimeMillis() + (((long) i) * 1000)));
        }
    }

    public Cookie(String str, String str2, String str3, String str4, Date date, boolean z) {
        super(str2, str3);
        this.hasPathAttribute = false;
        this.hasDomainAttribute = false;
        this.cookieVersion = 0;
        LOG.trace("enter Cookie(String, String, String, String, Date, boolean)");
        if (str2 == null) {
            throw new IllegalArgumentException("Cookie name may not be null");
        } else if (str2.trim().equals(PoiTypeDef.All)) {
            throw new IllegalArgumentException("Cookie name may not be blank");
        } else {
            setPath(str4);
            setDomain(str);
            setExpiryDate(date);
            setSecure(z);
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public int compare(Object obj, Object obj2) {
        LOG.trace("enter Cookie.compare(Object, Object)");
        if (!(obj instanceof Cookie)) {
            throw new ClassCastException(obj.getClass().getName());
        } else if (!(obj2 instanceof Cookie)) {
            throw new ClassCastException(obj2.getClass().getName());
        } else {
            Cookie cookie = (Cookie) obj;
            Cookie cookie2 = (Cookie) obj2;
            if (cookie.getPath() == null && cookie2.getPath() == null) {
                return 0;
            }
            return cookie.getPath() == null ? !cookie2.getPath().equals(CookieSpec.PATH_DELIM) ? -1 : 0 : cookie2.getPath() == null ? !cookie.getPath().equals(CookieSpec.PATH_DELIM) ? 1 : 0 : cookie.getPath().compareTo(cookie2.getPath());
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Cookie)) {
            return false;
        }
        Cookie cookie = (Cookie) obj;
        return LangUtils.equals(getName(), cookie.getName()) && LangUtils.equals(this.cookieDomain, cookie.cookieDomain) && LangUtils.equals(this.cookiePath, cookie.cookiePath);
    }

    public String getComment() {
        return this.cookieComment;
    }

    public String getDomain() {
        return this.cookieDomain;
    }

    public Date getExpiryDate() {
        return this.cookieExpiryDate;
    }

    public String getPath() {
        return this.cookiePath;
    }

    public boolean getSecure() {
        return this.isSecure;
    }

    public int getVersion() {
        return this.cookieVersion;
    }

    public int hashCode() {
        return LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(17, getName()), this.cookieDomain), this.cookiePath);
    }

    public boolean isDomainAttributeSpecified() {
        return this.hasDomainAttribute;
    }

    public boolean isExpired() {
        return this.cookieExpiryDate != null && this.cookieExpiryDate.getTime() <= System.currentTimeMillis();
    }

    public boolean isExpired(Date date) {
        return this.cookieExpiryDate != null && this.cookieExpiryDate.getTime() <= date.getTime();
    }

    public boolean isPathAttributeSpecified() {
        return this.hasPathAttribute;
    }

    public boolean isPersistent() {
        return this.cookieExpiryDate != null;
    }

    public void setComment(String str) {
        this.cookieComment = str;
    }

    public void setDomain(String str) {
        if (str != null) {
            int indexOf = str.indexOf(":");
            if (indexOf != -1) {
                str = str.substring(0, indexOf);
            }
            this.cookieDomain = str.toLowerCase();
        }
    }

    public void setDomainAttributeSpecified(boolean z) {
        this.hasDomainAttribute = z;
    }

    public void setExpiryDate(Date date) {
        this.cookieExpiryDate = date;
    }

    public void setPath(String str) {
        this.cookiePath = str;
    }

    public void setPathAttributeSpecified(boolean z) {
        this.hasPathAttribute = z;
    }

    public void setSecure(boolean z) {
        this.isSecure = z;
    }

    public void setVersion(int i) {
        this.cookieVersion = i;
    }

    public String toExternalForm() {
        return (getVersion() > 0 ? CookiePolicy.getDefaultSpec() : CookiePolicy.getCookieSpec(CookiePolicy.NETSCAPE)).formatCookie(this);
    }

    public String toString() {
        return toExternalForm();
    }
}
