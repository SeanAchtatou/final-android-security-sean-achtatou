package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChallengesController extends RequestController {
    private List<Challenge> b;
    private Integer c;

    private static class a extends Request {
        private final Game a;
        private final Integer b;
        private final String d;
        private final User e;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, User user, String str, Integer num) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: aGame should be set");
            }
            this.a = game;
            this.e = user;
            this.d = str;
            this.b = num;
        }

        public String a() {
            return String.format("/service/games/%s/challenges", this.a.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.d != null) {
                    jSONObject.put("search_list_id", this.d);
                }
                if (this.e != null) {
                    jSONObject.put("user_id", this.e.getIdentifier());
                }
                if (this.b != null) {
                    jSONObject.put("mode", this.b);
                }
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid challenge data", e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__1_0_0
    public ChallengesController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public ChallengesController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.b = Collections.emptyList();
        this.c = null;
    }

    private void a(String str) {
        a aVar = new a(c(), a(), e(), str, this.c);
        h();
        a(aVar);
    }

    private void a(List<Challenge> list) {
        this.b = Collections.unmodifiableList(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int f = response.f();
        if (f != 200) {
            throw new Exception("Request failed with status:" + f);
        }
        JSONArray jSONArray = ((JSONObject) response.a()).getJSONArray("challenges");
        ArrayList arrayList = new ArrayList();
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            arrayList.add(new Challenge(jSONArray.getJSONObject(i).getJSONObject("challenge")));
        }
        a(arrayList);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_0_0
    public List<Challenge> getChallenges() {
        return this.b;
    }

    @PublishedFor__1_0_0
    public Integer getMode() {
        return this.c;
    }

    @PublishedFor__1_0_0
    public void loadChallengeHistory() {
        a("#history");
    }

    @PublishedFor__1_0_0
    public void loadOpenChallenges() {
        a("#open");
    }

    @PublishedFor__1_0_0
    public void setMode(Integer num) {
        this.c = num;
    }
}
