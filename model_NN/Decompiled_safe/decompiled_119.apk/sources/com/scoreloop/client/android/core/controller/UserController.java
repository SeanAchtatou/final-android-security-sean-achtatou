package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.util.HashMap;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserController extends RequestController {
    private static final Pattern b = Pattern.compile(".*taken.*", 2);
    private User c;

    private static class a extends c {
        private final Game d;

        public a(RequestCompletionCallback requestCompletionCallback, User user, Game game) {
            super(requestCompletionCallback, game, user);
            this.d = game;
        }

        public String a() {
            return String.format("/service/games/%s/users/%s/context", this.d.getIdentifier(), this.b.getIdentifier());
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    private static class b extends d {
        public b(RequestCompletionCallback requestCompletionCallback, User user, User user2) {
            super(requestCompletionCallback, null, user, user2);
        }

        public String a() {
            return String.format("/service/users/%s/buddies", this.d.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("buddy_id", this.b.getIdentifier());
                jSONObject.put("buddyhood", jSONObject2);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException();
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    private static class c extends Request {
        protected Game a;
        protected User b;

        public c(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
            super(requestCompletionCallback);
            this.b = user;
            this.a = game;
        }

        public String a() {
            if (this.a == null || this.a.getIdentifier() == null) {
                return String.format("/service/users/%s", this.b.getIdentifier());
            }
            return String.format("/service/games/%s/users/%s", this.a.getIdentifier(), this.b.getIdentifier());
        }

        public JSONObject b() {
            return null;
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    private static class d extends c {
        protected User d;

        public d(RequestCompletionCallback requestCompletionCallback, Game game, User user, User user2) {
            super(requestCompletionCallback, game, user);
            this.d = user2;
        }

        public String a() {
            if (this.a == null || this.a.getIdentifier() == null) {
                return String.format("/service/users/%s/detail", this.b.getIdentifier());
            }
            return String.format("/service/games/%s/users/%s/detail", this.a.getIdentifier(), this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.d != null) {
                    jSONObject.put("reference_user_id", this.d.getIdentifier());
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid user data");
            }
        }
    }

    private static class e extends d {
        public e(RequestCompletionCallback requestCompletionCallback, User user, User user2) {
            super(requestCompletionCallback, null, user, user2);
        }

        public String a() {
            return String.format("/service/users/%s/buddies/%s", this.d.getIdentifier(), this.b.getIdentifier());
        }

        public JSONObject b() {
            return null;
        }

        public RequestMethod c() {
            return RequestMethod.DELETE;
        }
    }

    private static class f extends c {
        private String d;
        private String e;

        public f(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
            super(requestCompletionCallback, game, user);
            this.d = user.getLogin();
            this.e = user.getEmailAddress();
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                this.b.setLogin(this.d);
                this.b.setEmailAddress(this.e);
                jSONObject.put("user", this.b.e());
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid user data", e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.PUT;
        }
    }

    private static class g extends c {
        public g(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
            super(requestCompletionCallback, game, user);
        }

        public String a() {
            if (this.a == null || this.a.getIdentifier() == null) {
                return String.format("/service/users/%s/buddies", this.b.getIdentifier());
            }
            return String.format("/service/users/%s/buddies", this.b.getIdentifier());
        }
    }

    private static class h extends c {
        private final Game d;

        public h(RequestCompletionCallback requestCompletionCallback, User user, Game game) {
            super(requestCompletionCallback, game, user);
            this.d = game;
        }

        public String a() {
            return String.format("/service/games/%s/users/%s/context", this.d.getIdentifier(), this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.b != null) {
                    jSONObject.put("context", JSONUtils.a(this.b.getContext()));
                    jSONObject.put("version", this.b.f());
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid user data");
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    @PublishedFor__1_0_0
    public UserController(UserControllerObserver userControllerObserver) {
        this(null, userControllerObserver);
    }

    @PublishedFor__1_0_0
    public UserController(Session session, UserControllerObserver userControllerObserver) {
        super(session, userControllerObserver);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int f2 = response.f();
        RequestMethod c2 = request.c();
        JSONObject e2 = response.e();
        switch (f2) {
            case 200:
            case 201:
                switch (c2) {
                    case PUT:
                    case POST:
                    case GET:
                        if (!e2.has("user") && !e2.has("context")) {
                            throw new Exception("Request failed");
                        }
                    case DELETE:
                        if (!e2.has("buddy_id")) {
                            throw new Exception("Request failed");
                        }
                        break;
                }
                if (c2 != RequestMethod.DELETE) {
                    User user = getUser();
                    if (e2.has("user")) {
                        user.a(e2.getJSONObject("user"));
                    }
                    if (e2.has("context")) {
                        user.setContext(JSONUtils.a(e2.getJSONObject("context")));
                        user.e(e2.getString("version"));
                    }
                }
                return true;
            case 409:
                User user2 = getUser();
                HashMap hashMap = new HashMap();
                hashMap.put("oldUserContext", new HashMap(user2.getContext()));
                user2.setContext(JSONUtils.a(e2.getJSONObject("context")));
                user2.e(e2.getString("version"));
                hashMap.put("newUserContext", new HashMap(user2.getContext()));
                throw new Exception("Request failed");
            case 422:
                if (c2 == RequestMethod.PUT || c2 == RequestMethod.POST) {
                    JSONObject jSONObject = e2.getJSONObject("error");
                    JSONArray jSONArray = jSONObject.get("args") instanceof JSONArray ? jSONObject.getJSONArray("args") : null;
                    int length = jSONArray == null ? 0 : jSONArray.length();
                    boolean z = false;
                    boolean z2 = false;
                    for (int i = 0; i < length; i++) {
                        JSONArray jSONArray2 = jSONArray.getJSONArray(i);
                        String string = jSONArray2.getString(0);
                        String string2 = jSONArray2.getString(1);
                        if ("email".equalsIgnoreCase(string.trim())) {
                            if (b.matcher(string2).matches()) {
                                z = true;
                                z2 = true;
                            } else {
                                z = true;
                            }
                        }
                    }
                    if (!z) {
                        ((UserControllerObserver) b()).userControllerDidFailOnUsernameAlreadyTaken(this);
                        return false;
                    } else if (z2) {
                        ((UserControllerObserver) b()).userControllerDidFailOnEmailAlreadyTaken(this);
                        return false;
                    } else {
                        ((UserControllerObserver) b()).userControllerDidFailOnInvalidEmailFormat(this);
                        return false;
                    }
                } else {
                    throw new Exception("Request failed");
                }
            default:
                throw new Exception("Request failed");
        }
    }

    @PublishedFor__1_0_0
    public void addAsBuddy() {
        User user = getUser();
        User e2 = e();
        if (user.a(d())) {
            b(new Exception("User you're trying to add as buddy is a current session user"));
            return;
        }
        b bVar = new b(c(), user, e2);
        h();
        a(bVar);
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_0_0
    public User getUser() {
        if (this.c == null) {
            this.c = super.e();
            if (this.c == null) {
                throw new IllegalStateException("No session user assigned");
            }
        }
        return this.c;
    }

    @PublishedFor__1_0_0
    public void loadBuddies() {
        g gVar = new g(c(), a(), getUser());
        h();
        a(gVar);
    }

    @PublishedFor__1_0_0
    public void loadUser() {
        c cVar = new c(c(), a(), getUser());
        h();
        a(cVar);
    }

    @PublishedFor__1_0_0
    public void loadUserContext() {
        if (a() == null) {
            throw new IllegalStateException("user context is being held within a null game, therefore it's kinda hard to retrieve");
        }
        a aVar = new a(c(), getUser(), a());
        h();
        a(aVar);
    }

    @PublishedFor__1_0_0
    public void loadUserDetail() {
        User user = getUser();
        User e2 = e();
        if (user.a(d())) {
            e2 = null;
        }
        d dVar = new d(c(), a(), user, e2);
        h();
        a(dVar);
    }

    @PublishedFor__1_0_0
    public void loadUserWithIdentifier(String str) {
        if (str == null) {
            throw new IllegalArgumentException("anIdentifier parameter cannot be null");
        }
        this.c = new User();
        this.c.d(str);
        loadUser();
    }

    @PublishedFor__1_0_0
    public void removeAsBuddy() {
        e eVar = new e(c(), getUser(), e());
        h();
        a(eVar);
    }

    @PublishedFor__1_0_0
    public void setUser(User user) {
        this.c = user;
    }

    @PublishedFor__1_0_0
    public void submitUser() {
        f fVar = new f(c(), a(), getUser());
        h();
        a(fVar);
    }

    @PublishedFor__1_0_0
    public void submitUserContext() {
        if (a() == null) {
            throw new IllegalStateException("trying to put user context into a null game...");
        }
        h hVar = new h(c(), getUser(), a());
        h();
        a(hVar);
    }
}
