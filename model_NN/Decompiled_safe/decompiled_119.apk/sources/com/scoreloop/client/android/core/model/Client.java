package com.scoreloop.client.android.core.model;

import android.content.Context;
import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.server.Server;
import com.scoreloop.client.android.core.utils.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.BitSet;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;

public class Client implements SessionObserver {
    private final BitSet a;
    private final ClientObserver b;
    private final Server c;
    private final Session d;

    @PublishedFor__1_0_0
    public Client(Context context, String str, String str2, ClientObserver clientObserver) {
        this(context, str, str2, clientObserver, "https://api.scoreloop.com/bayeux");
    }

    Client(Context context, String str, String str2, ClientObserver clientObserver, String str3) {
        Game game;
        this.a = new BitSet();
        this.b = clientObserver;
        if (str == null || str2 == null) {
            game = null;
        } else {
            game = new Game(str, str2);
            game.setVersion("1.0");
        }
        try {
            this.c = new Server(new URL(str3));
            if (game != null) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("id", game.getIdentifier());
                    jSONObject2.put("secret", game.b());
                    jSONObject2.put("version", game.getVersion());
                    jSONObject.put("game", jSONObject2);
                    this.c.a(jSONObject);
                } catch (JSONException e) {
                    throw new IllegalStateException(e);
                }
            }
            this.d = new Session(this, this.c);
            this.d.a(game);
            this.d.a(context);
            Session.a(this.d);
            this.d.a().a(context);
            Properties a2 = context != null ? a(context) : null;
            if (a2 != null) {
                Money.a(a2.getProperty("currency.code"));
                Money.b(a2.getProperty("currency.symbol"));
                Money.c(a2.getProperty("currency.name.singular"));
                Money.d(a2.getProperty("currency.name.plural"));
            }
        } catch (MalformedURLException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private Game a() {
        return this.d.getGame();
    }

    private Properties a(Context context) {
        try {
            InputStream open = context.getAssets().open("scoreloop.properties");
            try {
                Properties properties = new Properties();
                properties.load(open);
                return properties;
            } catch (IOException e) {
                Logger.c("Client", "Failed to load scoreloop.properties file");
                return null;
            }
        } catch (IOException e2) {
            Logger.c("Client", "No scoreloop.properties file found");
            return null;
        }
    }

    @PublishedFor__1_0_0
    public Range getGameLevels() {
        if (a() != null) {
            return !a().hasLevels() ? new Range(0, 1) : new Range(a().getMinLevel().intValue(), a().getLevelCount().intValue());
        }
        throw new IllegalStateException("canno access game levels without a game");
    }

    @PublishedFor__1_0_0
    public Range getGameModes() {
        if (a() != null) {
            return (a() == null || !a().hasModes()) ? new Range(0, 1) : new Range(a().getMinMode().intValue(), a().getModeCount().intValue());
        }
        throw new IllegalStateException("cannot access game modes without a game");
    }

    @PublishedFor__1_0_0
    public Session getSession() {
        return this.d;
    }

    @PublishedFor__1_0_0
    public void setGameLevels(Range range) {
        if (a() == null) {
            throw new IllegalStateException("cannot access game levels without a game");
        }
        a().setMinLevel(Integer.valueOf(range.a()));
        a().setMaxLevel(Integer.valueOf(range.b()));
    }

    @PublishedFor__1_0_0
    public void setGameModes(Range range) {
        if (a() == null) {
            throw new IllegalStateException("canno access game modes without a game");
        }
        a().setMinMode(Integer.valueOf(range.a()));
        a().setMaxMode(Integer.valueOf(range.b()));
    }
}
