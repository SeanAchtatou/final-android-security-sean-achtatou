package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.utils.Formats;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class User {
    private static Map<String, b> a = new HashMap();
    private Map<String, JSONObject> A;
    private Activity B;
    private String C;
    private a b;
    private boolean c;
    private String d;
    private String e;
    private String f;
    private Integer g;
    private String h;
    private List<User> i;
    private Map<String, Object> j;
    private String k;
    private Details l;
    private Date m;
    private String n;
    private String o;
    private Gender p;
    private String q;
    private a r;
    private Date s;
    private String t;
    private String u;
    private Integer v;
    private b w;
    private a x;
    private List<SearchList> y;
    private List<SearchList> z;

    public class Details {
        private Double b;
        private Integer c;
        private Integer d;

        public Details() {
        }

        /* access modifiers changed from: package-private */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("winning_probability")) {
                this.b = Double.valueOf(jSONObject.optDouble("winning_probability"));
            }
            if (jSONObject.has("challenges_lost")) {
                this.d = Integer.valueOf(jSONObject.optInt("challenges_lost"));
            }
            if (jSONObject.has("challenges_won")) {
                this.c = Integer.valueOf(jSONObject.optInt("challenges_won"));
            }
        }

        @PublishedFor__1_0_0
        public Integer getChallengesLost() {
            return this.d;
        }

        @PublishedFor__1_0_0
        public Integer getChallengesWon() {
            return this.c;
        }

        @PublishedFor__1_0_0
        public Double getWinningProbability() {
            return this.b;
        }
    }

    public enum Gender {
        FEMALE("f"),
        MALE("m"),
        UNKNOWN("?");
        
        private String a;

        private Gender(String str) {
            this.a = str;
        }

        public String getJSONString() {
            return this.a;
        }
    }

    static class a {
        private final String a;
        private final int b;
        private final int c;

        public a(JSONObject jSONObject) throws JSONException {
            this.a = jSONObject.getString("decoration");
            this.b = jSONObject.getInt("threshold");
            this.c = jSONObject.getInt("value");
        }
    }

    private enum b {
        anonymous,
        active,
        deleted,
        passive,
        pending,
        suspended
    }

    static {
        a.put("anonymous", b.anonymous);
        a.put("active", b.active);
        a.put("deleted", b.deleted);
        a.put("passive", b.passive);
        a.put("pending", b.pending);
        a.put("suspended", b.suspended);
    }

    public User() {
        this.p = Gender.UNKNOWN;
        this.A = new HashMap();
        this.l = new Details();
    }

    public User(JSONObject jSONObject) throws JSONException {
        this();
        a(jSONObject);
    }

    private Integer g() {
        return this.g;
    }

    private String h() {
        return this.h;
    }

    private b i() {
        return this.w;
    }

    public Money a() {
        return (g() == null || h() == null) ? new Money("SLD", new BigDecimal(0)) : new Money(h(), new BigDecimal(g().intValue()));
    }

    public void a(String str) {
        this.n = str;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("id")) {
            this.q = jSONObject.getString("id");
        }
        if (jSONObject.has("login")) {
            this.t = jSONObject.optString("login");
        }
        if (jSONObject.has("email")) {
            this.o = jSONObject.optString("email");
        }
        if (jSONObject.has("state")) {
            String lowerCase = jSONObject.optString("state").toLowerCase();
            if (!a.containsKey(lowerCase)) {
                throw new IllegalStateException("could not parse json representation of User due to unknown state given: '" + lowerCase + "'");
            }
            this.w = a.get(lowerCase);
        }
        if (jSONObject.has("device_id")) {
            this.n = jSONObject.optString("device_id");
        }
        if (jSONObject.has("gender")) {
            String string = jSONObject.getString("gender");
            if ("m".equalsIgnoreCase(string)) {
                this.p = Gender.MALE;
            } else if ("f".equalsIgnoreCase(string)) {
                this.p = Gender.FEMALE;
            } else {
                this.p = Gender.UNKNOWN;
            }
        }
        if (jSONObject.has("date_of_birth")) {
            if (jSONObject.isNull("date_of_birth")) {
                this.m = null;
            } else {
                try {
                    this.m = Formats.b.parse(jSONObject.getString("date_of_birth"));
                } catch (ParseException e2) {
                    throw new JSONException("Invalid format of the birth date");
                }
            }
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("balance");
        if (optJSONObject != null) {
            this.g = Integer.valueOf(optJSONObject.getInt("amount"));
            this.h = optJSONObject.getString("currency");
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("avatar");
        if (optJSONObject2 != null) {
            this.f = optJSONObject2.getString("head");
            this.e = optJSONObject2.getString("hair");
            this.d = optJSONObject2.getString("body");
        }
        SocialProvider.b(this, jSONObject);
        JSONObject optJSONObject3 = jSONObject.optJSONObject("skill");
        if (optJSONObject3 != null) {
            this.v = Integer.valueOf(optJSONObject3.getInt("value"));
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject(Game.CHARACTERISTIC_AGILITY);
        if (optJSONObject4 != null) {
            this.b = new a(optJSONObject4);
        }
        JSONObject optJSONObject5 = jSONObject.optJSONObject(Game.CHARACTERISTIC_STRATEGY);
        if (optJSONObject5 != null) {
            this.x = new a(optJSONObject5);
        }
        JSONObject optJSONObject6 = jSONObject.optJSONObject(Game.CHARACTERISTIC_KNOWLEDGE);
        if (optJSONObject6 != null) {
            this.r = new a(optJSONObject6);
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("score_lists");
        if (optJSONArray != null) {
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                arrayList.add(new SearchList(optJSONArray.getJSONObject(i2)));
            }
            this.y = arrayList;
        }
        JSONArray optJSONArray2 = jSONObject.optJSONArray("challenge_lists");
        if (optJSONArray2 != null) {
            ArrayList arrayList2 = new ArrayList();
            for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                arrayList2.add(new SearchList(optJSONArray2.getJSONObject(i3)));
            }
            this.z = arrayList2;
        }
        if (jSONObject.has("last_active_at")) {
            try {
                this.s = Formats.a.parse(jSONObject.getString("last_active_at"));
            } catch (ParseException e3) {
                throw new JSONException("Invalid format of the 'last active at' date");
            }
        }
        if (JSONUtils.a(jSONObject, "last_activity")) {
            this.B = new Activity(jSONObject.getJSONObject("last_activity"));
        }
        if (JSONUtils.a(jSONObject, "nationality")) {
            this.C = jSONObject.getString("nationality");
        }
        JSONArray optJSONArray3 = jSONObject.optJSONArray("buddies");
        if (optJSONArray3 != null) {
            ArrayList arrayList3 = new ArrayList();
            for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                arrayList3.add(new User(optJSONArray3.getJSONObject(i4)));
            }
            this.i = arrayList3;
        }
        this.l.a(jSONObject);
    }

    public void a(JSONObject jSONObject, String str) {
        if (jSONObject != null) {
            this.A.put(str, jSONObject);
        } else {
            this.A.remove(str);
        }
    }

    public void a(boolean z2) {
        this.c = z2;
    }

    public boolean a(Session session) {
        return session.getUser().equals(this);
    }

    public String b() {
        return this.n;
    }

    public boolean b(String str) {
        return this.A.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    public List<SearchList> c() {
        return this.y;
    }

    public JSONObject c(String str) {
        return this.A.get(str);
    }

    public JSONObject d() {
        return this.A.get(SocialProvider.FACEBOOK_IDENTIFIER);
    }

    public void d(String str) {
        this.q = str;
    }

    public JSONObject e() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", this.q);
        jSONObject.put("login", this.t);
        jSONObject.put("device_id", this.n);
        jSONObject.put("password", this.u);
        jSONObject.put("password_confirmation", this.u);
        jSONObject.put("email", this.o);
        if (this.p != Gender.UNKNOWN) {
            jSONObject.put("gender", this.p.getJSONString());
        }
        if (this.m != null) {
            jSONObject.put("date_of_birth", Formats.b.format(this.m));
        }
        if (!(this.d == null && this.e == null && this.d == null)) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject.put("avatar", jSONObject2);
            jSONObject2.put("hair", this.e);
            jSONObject2.put("head", this.f);
            jSONObject2.put("body", this.d);
        }
        if (this.C != null) {
            jSONObject.put("nationality", this.C);
        }
        SocialProvider.c(this, jSONObject);
        return jSONObject;
    }

    public void e(String str) {
        this.k = str;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof User)) {
            return super.equals(obj);
        }
        User user = (User) obj;
        if (getIdentifier() != null && user.getIdentifier() != null) {
            return getIdentifier().equalsIgnoreCase(user.getIdentifier());
        }
        String login = getLogin();
        String login2 = user.getLogin();
        return (login == null || login2 == null || !login.equalsIgnoreCase(login2)) ? false : true;
    }

    public String f() {
        return this.k;
    }

    @PublishedFor__1_0_0
    public List<User> getBuddyUsers() {
        return this.i;
    }

    @PublishedFor__1_0_0
    public Map<String, Object> getContext() {
        return this.j;
    }

    @PublishedFor__1_0_0
    public Date getDateOfBirth() {
        return this.m;
    }

    @PublishedFor__1_0_0
    public Details getDetail() {
        return this.l;
    }

    @PublishedFor__1_0_0
    public String getDisplayName() {
        if (this.t != null && !this.t.equals("")) {
            return this.t;
        }
        if (d() == null) {
            return (this.o == null || this.o.equals("")) ? "somebody" : this.o;
        }
        try {
            return String.format("%s %s", d().getString("first_name"), d().getString("last_name"));
        } catch (JSONException e2) {
            return "somebody";
        }
    }

    @PublishedFor__1_0_0
    public String getEmailAddress() {
        return this.o;
    }

    @PublishedFor__1_0_0
    public String getIdentifier() {
        return this.q;
    }

    @PublishedFor__1_0_0
    public Date getLastActiveAt() {
        return this.s;
    }

    @PublishedFor__1_0_0
    public Activity getLastActivity() {
        return this.B;
    }

    @PublishedFor__1_0_0
    public String getLogin() {
        return this.t;
    }

    @PublishedFor__1_0_0
    public Integer getSkillValue() {
        return this.v;
    }

    public int hashCode() {
        return getLogin() == null ? "".hashCode() : getLogin().hashCode();
    }

    @PublishedFor__1_0_0
    public boolean isActive() {
        return b.active.equals(i());
    }

    @PublishedFor__1_0_0
    public boolean isAnonymous() {
        return b.anonymous.equals(i());
    }

    @PublishedFor__1_0_0
    public boolean isAuthenticated() {
        return this.c;
    }

    @PublishedFor__1_0_0
    public boolean isChallengable() {
        return isAnonymous() || isPassive() || isPending() || isActive();
    }

    @PublishedFor__1_0_0
    public boolean isConnectedToSocialProviderWithIdentifier(String str) {
        SocialProvider socialProviderForIdentifier = SocialProvider.getSocialProviderForIdentifier(str);
        if (socialProviderForIdentifier != null) {
            return socialProviderForIdentifier.isUserConnected(this);
        }
        throw new IllegalArgumentException("could not find provider for id: '" + str + "'");
    }

    @PublishedFor__1_0_0
    public boolean isPassive() {
        return b.passive.equals(i());
    }

    @PublishedFor__1_0_0
    public boolean isPending() {
        return b.pending.equals(i());
    }

    @PublishedFor__1_0_0
    public void setContext(Map<String, Object> map) {
        this.j = map;
    }

    @PublishedFor__1_0_0
    public void setDateOfBirth(Date date) {
        this.m = date;
    }

    @PublishedFor__1_0_0
    public void setEmailAddress(String str) {
        this.o = str;
    }

    @PublishedFor__1_0_0
    public void setLogin(String str) {
        this.t = str;
    }

    @PublishedFor__1_0_0
    public void setPassword(String str) {
        this.u = str;
    }

    public String toString() {
        return getLogin() == null ? getIdentifier() == null ? "[empty user]" : getIdentifier() : getLogin();
    }
}
