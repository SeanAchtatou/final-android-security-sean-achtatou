package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GamesController extends RequestController {
    private final d<Game> b = new d<>();
    private boolean c = true;
    private String d = null;
    private User e = null;

    private class a extends Request {
        private final Device b;
        private final int d;
        private final int e;
        private final String f;
        private final User g;

        public a(RequestCompletionCallback requestCompletionCallback, User user, String str, Device device, int i, int i2) {
            super(requestCompletionCallback);
            this.d = i;
            this.e = i2;
            this.g = user;
            this.f = str;
            this.b = device;
        }

        public String a() {
            if (this.g == null) {
                return "/service/games";
            }
            return String.format("/service/users/%s/games", this.g.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("offset", this.d);
                jSONObject.put("per_page", this.e);
                if (this.f != null) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("id", this.f);
                    if (this.b != null) {
                        SearchSpec searchSpec = new SearchSpec();
                        searchSpec.a(new b("playable_by_device", h.EXACT, this.b.a()));
                        jSONObject2.put("definition", searchSpec.a());
                    }
                    jSONObject.put("search_list", jSONObject2);
                }
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException(e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__1_1_0
    public GamesController(RequestControllerObserver requestControllerObserver) {
        super(null, requestControllerObserver);
    }

    @PublishedFor__1_1_0
    public GamesController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    private void a(int i) {
        h();
        this.b.a(i);
        a(new a(c(), this.e, this.d, this.c ? d().a() : null, i, this.b.a()));
    }

    private void a(String str, User user) {
        if (this.d != str) {
            j();
            this.d = str;
            this.e = user;
        }
        a(0);
    }

    private void j() {
        this.e = null;
        this.d = null;
        this.b.j();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        if (response.f() != 200 || response.d() == null) {
            throw new Exception("Request failed");
        }
        ArrayList arrayList = new ArrayList();
        JSONArray d2 = response.d();
        int length = d2.length();
        for (int i = 0; i < length; i++) {
            arrayList.add(new Game(d2.getJSONObject(i).getJSONObject("game")));
        }
        this.b.a(arrayList);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_1_0
    public List<Game> getGames() {
        return this.b.d();
    }

    @PublishedFor__1_1_0
    public boolean getLoadsDevicesPlatformOnly() {
        return this.c;
    }

    @PublishedFor__1_1_0
    public int getRangeLength() {
        return this.b.f();
    }

    @PublishedFor__1_1_0
    public boolean hasNextRange() {
        return this.b.h();
    }

    @PublishedFor__1_1_0
    public boolean hasPreviousRange() {
        return this.b.i();
    }

    @PublishedFor__1_1_0
    public void loadNextRange() {
        if (!hasNextRange()) {
            throw new IllegalStateException("There's no next range");
        } else if (this.b.g()) {
            a(this.b.c());
        } else {
            a(0);
        }
    }

    @PublishedFor__1_1_0
    public void loadPreviousRange() {
        if (!hasPreviousRange()) {
            throw new IllegalStateException("There's no previous range");
        } else if (this.b.g()) {
            a(this.b.e());
        } else {
            a(0);
        }
    }

    @PublishedFor__1_1_0
    public void loadRangeForBuddies() {
        a("#buddy_games", e());
    }

    @PublishedFor__1_1_0
    public void loadRangeForFeatured() {
        a("#featured_games", (User) null);
    }

    @PublishedFor__1_1_0
    public void loadRangeForNew() {
        a("#new_games", (User) null);
    }

    @PublishedFor__1_1_0
    public void loadRangeForPopular() {
        a("#popular_games", (User) null);
    }

    @PublishedFor__1_1_0
    public void loadRangeForUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("user must not be null");
        }
        if (!user.equals(this.e)) {
            j();
            this.e = user;
        }
        a(0);
    }

    @PublishedFor__1_1_0
    public void setLoadsDevicesPlatformOnly(boolean z) {
        this.c = z;
    }

    @PublishedFor__1_1_0
    public void setRangeLength(int i) {
        this.b.b(i);
    }
}
