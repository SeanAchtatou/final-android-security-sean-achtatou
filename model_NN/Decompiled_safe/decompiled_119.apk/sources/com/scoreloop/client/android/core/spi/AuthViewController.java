package com.scoreloop.client.android.core.spi;

import com.scoreloop.client.android.core.model.Session;

public abstract class AuthViewController {
    private Observer a;
    private Session b;

    public interface Observer {
        void a(Throwable th);

        void b();

        void c();

        void d();
    }

    public AuthViewController(Session session, Observer observer) {
        this.b = session;
        this.a = observer;
    }

    public Observer d() {
        return this.a;
    }
}
