package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.model.Message;
import com.scoreloop.client.android.core.model.MessageReceiver;
import com.scoreloop.client.android.core.model.MessageReceiverInterface;
import com.scoreloop.client.android.core.model.MessageTargetInterface;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageController extends RequestController {
    @PublishedFor__1_1_0
    public static final Object EMAIL_RECEIVER = new b();
    @PublishedFor__1_0_0
    public static final Object INVITATION_TARGET = new a();
    private Message b;

    private static class a implements MessageTargetInterface {
        private a() {
        }

        public String a() {
            return "invitation";
        }

        public String getIdentifier() {
            return null;
        }
    }

    private static class b implements MessageReceiverInterface {
        private b() {
        }

        public String b() {
            return "email";
        }
    }

    private class c extends Request {
        private Message b;

        public c(Message message, RequestCompletionCallback requestCompletionCallback) {
            super(requestCompletionCallback);
            this.b = message.a();
        }

        public String a() {
            return String.format("/service/games/%s/users/%s/message", MessageController.this.a().getIdentifier(), MessageController.this.e().getIdentifier());
        }

        public JSONObject b() {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("message", this.b.e());
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    @PublishedFor__1_0_0
    public MessageController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public MessageController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    private Message j() {
        if (this.b == null) {
            this.b = new Message();
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        return true;
    }

    @PublishedFor__1_0_0
    public void addReceiverWithUsers(Object obj, List<User> list) {
        if (obj == null || !(obj instanceof MessageReceiverInterface)) {
            throw new IllegalArgumentException("invalid receiver");
        }
        if (obj == EMAIL_RECEIVER) {
            if (list == null || list.size() == 0) {
                throw new IllegalArgumentException("some users should be passed when specifying EMAIL_RECEIVER");
            }
        } else if (!SocialProvider.a(obj.getClass())) {
            throw new IllegalArgumentException("we don't support such provider");
        }
        ArrayList arrayList = null;
        if (list != null) {
            arrayList = new ArrayList(list);
        }
        j().a(new MessageReceiver((MessageReceiverInterface) obj, arrayList));
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_0_0
    public Object getTarget() {
        return j().b();
    }

    @PublishedFor__1_0_0
    public String getText() {
        return j().c();
    }

    @PublishedFor__1_0_0
    public boolean isSubmitAllowed() {
        if (getTarget() == null) {
            return false;
        }
        if (j().d().isEmpty()) {
            return false;
        }
        for (MessageReceiver a2 : j().d()) {
            MessageReceiverInterface a3 = a2.a();
            if ((a3 instanceof SocialProvider) && !((SocialProvider) a3).isUserConnected(e())) {
                return false;
            }
        }
        return true;
    }

    @PublishedFor__1_0_0
    public void setTarget(Object obj) {
        if (obj == null || !(obj instanceof MessageTargetInterface)) {
            throw new IllegalArgumentException("invalid target");
        }
        j().a((MessageTargetInterface) obj);
    }

    @PublishedFor__1_0_0
    public void setText(String str) {
        j().a(str);
    }

    @PublishedFor__1_0_0
    public void submitMessage() {
        if (!isSubmitAllowed()) {
            throw new IllegalStateException("submitting is not allowed");
        }
        h();
        a(new c(j(), c()));
    }
}
