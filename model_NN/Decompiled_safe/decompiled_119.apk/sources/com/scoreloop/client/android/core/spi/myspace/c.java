package com.scoreloop.client.android.core.spi.myspace;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.scoreloop.client.android.core.ui.ScoreloopCustomDialog;

class c extends ScoreloopCustomDialog {
    private String a;
    /* access modifiers changed from: private */
    public a b;

    interface a {
        void a(String str);
    }

    c(Context context, a aVar, String str) {
        super(context);
        this.b = aVar;
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        WebView webView = new WebView(getContext());
        webView.loadUrl(this.a);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.addView(webView);
        final EditText editText = new EditText(getContext());
        editText.setWidth(100);
        linearLayout.addView(editText);
        Button button = new Button(getContext());
        button.setWidth(100);
        button.setText("submit");
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                c.this.b.a(editText.getText().toString());
            }
        });
        linearLayout.addView(button);
        setContentView(linearLayout);
    }
}
