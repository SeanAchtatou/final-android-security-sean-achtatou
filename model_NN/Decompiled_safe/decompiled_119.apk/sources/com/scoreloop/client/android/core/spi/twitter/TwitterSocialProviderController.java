package com.scoreloop.client.android.core.spi.twitter;

import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.spi.AuthRequest;
import com.scoreloop.client.android.core.spi.AuthRequestDelegate;
import com.scoreloop.client.android.core.spi.AuthViewController;

public class TwitterSocialProviderController extends SocialProviderController implements AuthRequestDelegate, AuthViewController.Observer {
    private c b;
    private a c;
    private c d;

    public TwitterSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b = new c(this);
        this.b.b();
    }

    public void a(AuthRequest authRequest) {
        if (authRequest == this.b) {
            this.c = new a(g(), this);
            this.c.a(this.b.c());
            this.c.a(e());
        } else if (authRequest == this.d) {
            getSocialProvider().a(h(), this.d.c(), this.d.d(), this.d.e());
            i();
        } else {
            throw new IllegalStateException("unexpected request");
        }
    }

    public void a(AuthRequest authRequest, Throwable th) {
        f().socialProviderControllerDidFail(this, th);
    }

    public void a(Throwable th) {
        f().socialProviderControllerDidFail(this, th);
    }

    public void b() {
        f().socialProviderControllerDidCancel(this);
    }

    public void c() {
        this.d = new c(this);
        this.d.a(this.b.c(), this.b.d());
    }

    public void d() {
        f().socialProviderControllerDidEnterInvalidCredentials(this);
    }
}
