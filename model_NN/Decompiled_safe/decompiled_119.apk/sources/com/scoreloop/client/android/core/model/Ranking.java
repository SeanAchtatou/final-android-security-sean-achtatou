package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import org.json.JSONException;
import org.json.JSONObject;

public class Ranking {
    private Integer a;
    private Integer b;

    public void a(JSONObject jSONObject) throws JSONException {
        this.a = Integer.valueOf(jSONObject.getInt("rank"));
        this.b = Integer.valueOf(jSONObject.getInt("total"));
        if (this.a.intValue() == 0) {
            this.a = null;
        }
    }

    @PublishedFor__1_0_0
    public Integer getRank() {
        return this.a;
    }

    @PublishedFor__1_1_0
    public Integer getTotal() {
        return this.b;
    }
}
