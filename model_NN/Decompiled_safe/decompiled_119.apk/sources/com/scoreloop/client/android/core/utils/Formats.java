package com.scoreloop.client.android.core.utils;

import java.text.SimpleDateFormat;

public abstract class Formats {
    public static final SimpleDateFormat a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    public static final SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd");
}
