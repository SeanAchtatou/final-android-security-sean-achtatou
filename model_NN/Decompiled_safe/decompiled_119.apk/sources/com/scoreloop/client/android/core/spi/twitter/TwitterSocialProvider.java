package com.scoreloop.client.android.core.spi.twitter;

import com.scoreloop.client.android.core.model.SocialProvider;

public class TwitterSocialProvider extends SocialProvider {
    public static String a = "com.twitter.v1";

    public Class<?> a() {
        return TwitterSocialProviderController.class;
    }

    public String getIdentifier() {
        return a;
    }
}
