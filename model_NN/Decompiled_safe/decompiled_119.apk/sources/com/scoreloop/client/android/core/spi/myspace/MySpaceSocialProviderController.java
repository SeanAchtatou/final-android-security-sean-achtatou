package com.scoreloop.client.android.core.spi.myspace;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.spi.AuthViewController;

public class MySpaceSocialProviderController extends SocialProviderController implements AuthViewController.Observer {
    private b b = null;

    @PublishedFor__1_0_0
    public MySpaceSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b = new b(g(), this);
        this.b.a(e());
    }

    public void a(Throwable th) {
        f().socialProviderControllerDidFail(this, th);
    }

    public void b() {
        f().socialProviderControllerDidCancel(this);
    }

    public void c() {
        getSocialProvider().a(g().getUser(), this.b.a(), this.b.b(), this.b.c());
        i();
    }

    public void d() {
        f().socialProviderControllerDidEnterInvalidCredentials(this);
    }
}
