package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

class a extends RequestController {
    /* access modifiers changed from: private */
    public c b;
    private final RequestControllerObserver c = new b();

    /* renamed from: com.scoreloop.client.android.core.controller.a$a  reason: collision with other inner class name */
    private static class C0000a extends Request {
        private final Device a;
        private final Game b;

        public C0000a(RequestCompletionCallback requestCompletionCallback, Game game, Device device) {
            super(requestCompletionCallback);
            this.b = game;
            this.a = device;
        }

        public String a() {
            if (this.b == null) {
                return "/service/session";
            }
            return String.format("/service/games/%s/session", this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("device_id", this.a.a());
                jSONObject.put("user", jSONObject2);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid device data");
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    private final class b implements RequestControllerObserver {
        static final /* synthetic */ boolean a = (!a.class.desiredAssertionStatus());

        private b() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            Session d = a.this.d();
            if (a || d.c() == Session.State.AUTHENTICATING) {
                d.a(Session.State.FAILED);
                a.this.a(exc);
                c unused = a.this.b = (c) null;
                return;
            }
            throw new AssertionError();
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            Session d = a.this.d();
            if (!a && d.c() != Session.State.AUTHENTICATING) {
                throw new AssertionError();
            } else if (d.a().a() != null) {
                d.getUser().a(d.a().a());
                c unused = a.this.b = (c) null;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    a(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    private List<Money> a(Money money) {
        String d = money.d();
        return money.compareTo(new Money(d, new BigDecimal(10000))) < 0 ? a(d) : money.compareTo(new Money(d, new BigDecimal(100000))) < 0 ? c(d) : b(d);
    }

    private List<Money> a(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(100)));
        arrayList.add(new Money(str, new BigDecimal(200)));
        arrayList.add(new Money(str, new BigDecimal(500)));
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2000)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        return arrayList;
    }

    private List<Money> b(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2500)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        arrayList.add(new Money(str, new BigDecimal(10000)));
        arrayList.add(new Money(str, new BigDecimal(50000)));
        arrayList.add(new Money(str, new BigDecimal(100000)));
        return arrayList;
    }

    private List<Money> c(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(500)));
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2500)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        arrayList.add(new Money(str, new BigDecimal(10000)));
        arrayList.add(new Money(str, new BigDecimal(20000)));
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        Session d = d();
        int f = response.f();
        JSONObject e = response.e();
        JSONObject optJSONObject = e.optJSONObject("user");
        if ((f == 200 || f == 201) && optJSONObject != null) {
            User user = d.getUser();
            user.a(optJSONObject);
            user.a(true);
            SocialProvider.a(user, optJSONObject);
            d.a(a(user.a()));
            d.a(e);
            d.a(Session.State.AUTHENTICATED);
            return true;
        }
        d.a(Session.State.FAILED);
        throw new Exception("Session authentication request failed with status: " + f);
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        Session d = d();
        Device a = d.a();
        if (d.c() == Session.State.FAILED) {
            a.a((String) null);
        }
        C0000a aVar = new C0000a(c(), a(), a);
        if (a.a() == null && this.b == null) {
            this.b = new c(d(), this.c);
        }
        h();
        d.a(Session.State.AUTHENTICATING);
        if (a.a() == null) {
            this.b.k();
        }
        a(aVar);
    }
}
