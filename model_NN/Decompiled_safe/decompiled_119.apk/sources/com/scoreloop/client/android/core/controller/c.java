package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.Logger;
import java.nio.channels.IllegalSelectorException;
import org.json.JSONException;
import org.json.JSONObject;

class c extends RequestController {
    private Request b;
    private Device c;
    private Request d;

    private static class a extends Request {
        private final Device a;
        private final b b;

        public a(RequestCompletionCallback requestCompletionCallback, Device device, b bVar) {
            super(requestCompletionCallback);
            this.a = device;
            this.b = bVar;
        }

        public String a() {
            return "/service/device";
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                switch (this.b) {
                    case VERIFY:
                        jSONObject.put("uuid", this.a.f());
                        jSONObject.put("system", this.a.b());
                        break;
                    case CREATE:
                        jSONObject.put("device", this.a.g());
                        break;
                    case RESET:
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("uuid", this.a.f());
                        jSONObject2.put("id", this.a.a());
                        jSONObject2.put("system", this.a.b());
                        jSONObject2.put("state", "freed");
                        jSONObject.put("device", jSONObject2);
                        break;
                    case UPDATE:
                        jSONObject.put("device", this.a.g());
                        break;
                    default:
                        throw new IllegalSelectorException();
                }
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid device data", e);
            }
        }

        public RequestMethod c() {
            switch (this.b) {
                case VERIFY:
                    return RequestMethod.GET;
                case CREATE:
                    return RequestMethod.POST;
                case RESET:
                case UPDATE:
                    return RequestMethod.PUT;
                default:
                    throw new IllegalSelectorException();
            }
        }

        public b p() {
            return this.b;
        }
    }

    enum b {
        CREATE(21),
        RESET(22),
        UPDATE(23),
        VERIFY(20);
        
        private final int a;

        private b(int i) {
            this.a = i;
        }
    }

    c(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = session.a();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int f = response.f();
        JSONObject optJSONObject = response.e().optJSONObject("device");
        if (((a) request).p() == b.VERIFY) {
            if (f == 404) {
                return false;
            }
            if (this.b != null) {
                this.b.m();
            }
            this.b = null;
        }
        if ((f == 200 || f == 201) && optJSONObject != null) {
            this.c.a(optJSONObject.getString("id"));
            if ("freed".equalsIgnoreCase(optJSONObject.optString("state"))) {
                this.c.a(Device.State.FREED);
            } else {
                this.c.a(f == 200 ? Device.State.VERIFIED : Device.State.CREATED);
            }
            return true;
        }
        throw new Exception("Request failed with status: " + f);
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void h() {
        Logger.a("DeviceController", "reset()");
        super.h();
        if (this.d != null) {
            if (!this.d.l()) {
                Logger.a("DeviceController", "reset() - canceling verify request");
                d().b().b(this.d);
            }
            this.d = null;
        }
    }

    /* access modifiers changed from: package-private */
    public Device j() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void k() {
        h();
        this.d = new a(c(), j(), b.VERIFY);
        a(this.d);
        this.b = new a(c(), j(), b.CREATE);
        a(this.b);
    }
}
