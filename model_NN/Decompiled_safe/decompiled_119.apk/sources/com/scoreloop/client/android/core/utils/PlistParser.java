package com.scoreloop.client.android.core.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Node;

public class PlistParser {
    public static Integer a(Object obj) {
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        throw new IllegalArgumentException();
    }

    private List<Object> a(Node node) {
        ArrayList arrayList = new ArrayList();
        for (Node firstChild = node.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
            if (firstChild.getNodeType() == 1) {
                arrayList.add(d(firstChild));
            }
        }
        return arrayList;
    }

    private Object b(Node node) {
        String str;
        Object obj;
        HashMap hashMap = new HashMap();
        Node firstChild = node.getFirstChild();
        while (firstChild != null) {
            while (true) {
                if (firstChild == null) {
                    str = null;
                    break;
                } else if (firstChild.getNodeType() == 1) {
                    str = d(d(firstChild));
                    break;
                } else {
                    firstChild = firstChild.getNextSibling();
                }
            }
            if (firstChild == null) {
                break;
            }
            Node nextSibling = firstChild.getNextSibling();
            while (true) {
                if (nextSibling == null) {
                    obj = null;
                    break;
                } else if (nextSibling.getNodeType() == 1) {
                    obj = d(nextSibling);
                    break;
                } else {
                    nextSibling = nextSibling.getNextSibling();
                }
            }
            hashMap.put(str, obj);
            firstChild = nextSibling.getNextSibling();
        }
        return hashMap;
    }

    public static List<Object> b(Object obj) {
        if (obj instanceof List) {
            return (List) obj;
        }
        throw new IllegalArgumentException();
    }

    private Integer c(Node node) {
        return Integer.valueOf(e(node));
    }

    public static Map<String, Object> c(Object obj) {
        if (obj instanceof Map) {
            return (Map) obj;
        }
        throw new IllegalArgumentException();
    }

    private Object d(Node node) {
        for (Node node2 = node; node2 != null; node2 = node2.getNextSibling()) {
            if (node2.getNodeType() == 1) {
                String nodeName = node2.getNodeName();
                if (nodeName.equalsIgnoreCase("dict")) {
                    return b(node2);
                }
                if (nodeName.equalsIgnoreCase("array")) {
                    return a(node2);
                }
                if (nodeName.equalsIgnoreCase("key")) {
                    return e(node2);
                }
                if (nodeName.equalsIgnoreCase("string")) {
                    return e(node2);
                }
                if (nodeName.equalsIgnoreCase("integer")) {
                    return c(node2);
                }
                throw new IllegalStateException("unknown node name: " + nodeName);
            }
        }
        return null;
    }

    public static String d(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        throw new IllegalArgumentException();
    }

    private String e(Node node) {
        for (Node firstChild = node.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
            if (firstChild.getNodeType() == 3) {
                return firstChild.getNodeValue();
            }
        }
        return null;
    }

    public Object a(InputStream inputStream) {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            newInstance.setIgnoringComments(true);
            return d(newInstance.newDocumentBuilder().parse(inputStream).getElementsByTagName("plist").item(0).getFirstChild());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
