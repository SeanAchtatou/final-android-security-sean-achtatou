package com.scoreloop.client.android.core.controller;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchSpec {
    private final List<b> a;
    private final i b;

    public SearchSpec() {
        this(null);
    }

    public SearchSpec(i iVar) {
        this.a = new ArrayList();
        this.b = iVar;
    }

    public JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (this.b != null) {
            jSONObject.put("order_by", this.b.a());
            jSONObject.put("order_as", this.b.b());
        }
        if (this.a != null) {
            JSONObject jSONObject2 = new JSONObject();
            for (b next : this.a) {
                jSONObject2.put(next.a(), next.b());
            }
            jSONObject.put("conditions", jSONObject2);
        }
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("search", jSONObject);
        return jSONObject3;
    }

    public void a(b bVar) {
        this.a.add(bVar);
    }
}
