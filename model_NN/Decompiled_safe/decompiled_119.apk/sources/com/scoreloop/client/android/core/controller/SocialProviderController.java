package com.scoreloop.client.android.core.controller;

import android.app.Activity;
import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import java.lang.reflect.InvocationTargetException;

public abstract class SocialProviderController {
    static final /* synthetic */ boolean a = (!SocialProviderController.class.desiredAssertionStatus());
    private Activity b;
    private final SocialProviderControllerObserver c;
    private SocialProvider d;
    private a e;
    private RequestControllerObserver f;
    private final Session g;
    private UserController h;

    class a implements RequestControllerObserver {
        a() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            SocialProviderController.this.f().socialProviderControllerDidFail(SocialProviderController.this, exc);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            if (requestController.getClass().isAssignableFrom(a.class)) {
                SocialProviderController.this.b();
            }
        }
    }

    protected SocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        this.g = session;
        this.c = socialProviderControllerObserver;
    }

    private void a(SocialProvider socialProvider) {
        this.d = socialProvider;
    }

    /* access modifiers changed from: private */
    public void b() {
        if (getSocialProvider().isUserConnected(h())) {
            f().socialProviderControllerDidSucceed(this);
        } else {
            a();
        }
    }

    @PublishedFor__1_0_0
    public static SocialProviderController getSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver, SocialProvider socialProvider) {
        if (socialProviderControllerObserver == null || socialProvider == null) {
            throw new IllegalArgumentException("observer and provider argumetns must not be null");
        }
        Session currentSession = session == null ? Session.getCurrentSession() : session;
        if (a || currentSession != null) {
            Class<?> a2 = socialProvider.a();
            if (a2 == null) {
                throw new IllegalArgumentException();
            }
            try {
                try {
                    SocialProviderController socialProviderController = (SocialProviderController) a2.getConstructor(Session.class, SocialProviderControllerObserver.class).newInstance(currentSession, socialProviderControllerObserver);
                    socialProviderController.a(socialProvider);
                    return socialProviderController;
                } catch (IllegalArgumentException e2) {
                    throw new IllegalStateException(e2);
                } catch (InstantiationException e3) {
                    throw new IllegalStateException(e3);
                } catch (IllegalAccessException e4) {
                    throw new IllegalStateException(e4);
                } catch (InvocationTargetException e5) {
                    throw new IllegalStateException(e5);
                }
            } catch (SecurityException e6) {
                throw new IllegalStateException(e6);
            } catch (NoSuchMethodException e7) {
                throw new IllegalStateException(e7);
            }
        } else {
            throw new AssertionError();
        }
    }

    @PublishedFor__1_0_0
    public static SocialProviderController getSocialProviderController(String str, SocialProviderControllerObserver socialProviderControllerObserver) {
        return getSocialProviderController(null, socialProviderControllerObserver, SocialProvider.getSocialProviderForIdentifier(str));
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public void a(Exception exc) {
        if (exc == null) {
            f().socialProviderControllerDidSucceed(this);
        } else {
            f().socialProviderControllerDidFail(this, exc);
        }
    }

    @PublishedFor__1_0_0
    public final void connect(Activity activity) {
        this.b = activity;
        this.f = new a();
        if (g() == null || !g().isAuthenticated()) {
            if (this.e == null) {
                this.e = new a(g(), this.f);
            }
            this.e.j();
            return;
        }
        b();
    }

    /* access modifiers changed from: protected */
    public Activity e() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public SocialProviderControllerObserver f() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public Session g() {
        return this.g;
    }

    @PublishedFor__1_0_0
    public SocialProvider getSocialProvider() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public User h() {
        return this.g.getUser();
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.h == null) {
            this.h = new UserController(g(), new UserControllerObserver() {
                public void requestControllerDidFail(RequestController requestController, Exception exc) {
                    SocialProviderController.this.a(new IllegalStateException("submit user failed"));
                }

                public void requestControllerDidReceiveResponse(RequestController requestController) {
                    SocialProviderController.this.a((Exception) null);
                }

                public void userControllerDidFailOnEmailAlreadyTaken(UserController userController) {
                    SocialProviderController.this.a(new IllegalStateException("submit user failed"));
                }

                public void userControllerDidFailOnInvalidEmailFormat(UserController userController) {
                    SocialProviderController.this.a(new IllegalStateException("submit user failed"));
                }

                public void userControllerDidFailOnUsernameAlreadyTaken(UserController userController) {
                    SocialProviderController.this.a(new IllegalStateException("submit user failed"));
                }
            });
        }
        this.h.setUser(h());
        this.h.submitUser();
    }
}
