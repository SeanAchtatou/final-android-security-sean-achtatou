package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.utils.Formats;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Score implements MessageTargetInterface {
    private String a;
    private String b;
    private Integer c;
    private Double d;
    private Integer e;
    private Double f;
    private Integer g;
    private Date h;
    private User i;
    private Map<String, Object> j;

    @PublishedFor__1_0_0
    public Score(Double d2, Map<String, Object> map) {
        this.f = d2;
        if (map != null) {
            this.j = new HashMap(map);
            this.d = (Double) this.j.get(Game.CONTEXT_KEY_MINOR_RESULT);
            this.c = (Integer) this.j.get(Game.CONTEXT_KEY_LEVEL);
            this.e = (Integer) this.j.get(Game.CONTEXT_KEY_MODE);
            this.j.remove(Game.CONTEXT_KEY_MINOR_RESULT);
            this.j.remove(Game.CONTEXT_KEY_LEVEL);
            this.j.remove(Game.CONTEXT_KEY_MODE);
        }
    }

    public Score(JSONObject jSONObject) throws JSONException {
        a(jSONObject);
    }

    @PublishedFor__1_0_0
    public static boolean areModesEqual(Score score, Score score2) {
        if (score != null && score2 != null) {
            return areModesEqual(score.getMode(), score2.getMode());
        }
        throw new IllegalArgumentException();
    }

    @PublishedFor__1_0_0
    public static boolean areModesEqual(Integer num, Score score) {
        if (score != null) {
            return areModesEqual(num, score.getMode());
        }
        throw new IllegalArgumentException();
    }

    @PublishedFor__1_0_0
    public static boolean areModesEqual(Integer num, Integer num2) {
        if (num != null) {
            return num.equals(num2);
        }
        throw new IllegalArgumentException();
    }

    public String a() {
        return "score";
    }

    public void a(User user) {
        this.i = user;
    }

    public void a(Integer num) {
        this.g = num;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("id")) {
            this.b = jSONObject.getString("id");
        }
        if (jSONObject.has("device_id")) {
            this.a = jSONObject.getString("device_id");
        }
        if (jSONObject.has("result")) {
            this.f = Double.valueOf(jSONObject.getDouble("result"));
        }
        if (jSONObject.has("minor_result")) {
            this.d = Double.valueOf(jSONObject.getDouble("minor_result"));
        }
        if (jSONObject.has("level")) {
            this.c = Integer.valueOf(jSONObject.getInt("level"));
        }
        if (jSONObject.has("minor_result")) {
            this.e = Integer.valueOf(jSONObject.getInt("mode"));
        }
        if (jSONObject.has("updated_at")) {
            try {
                this.h = Formats.a.parse(jSONObject.getString("updated_at"));
            } catch (ParseException e2) {
                throw new JSONException("Invalid format of the update date");
            }
        }
        if (jSONObject.has("user")) {
            this.i = new User(jSONObject.getJSONObject("user"));
        }
        if (jSONObject.has("context")) {
            this.j = jSONObject.isNull("context") ? null : JSONUtils.a(jSONObject.getJSONObject("context"));
        }
    }

    public JSONObject b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (this.a == null && this.i != null) {
            this.a = this.i.b();
        }
        jSONObject.put("device_id", this.a);
        jSONObject.put("result", this.f);
        jSONObject.put("level", this.c);
        if (this.i != null) {
            jSONObject.put("user_id", this.i.getIdentifier());
        }
        if (this.h != null) {
            jSONObject.put("updated_at", Formats.a.format(this.h));
        }
        jSONObject.put("mode", this.e);
        jSONObject.put("minor_result", this.d);
        if (this.j != null) {
            jSONObject.put("context", JSONUtils.a(this.j));
        }
        return jSONObject;
    }

    @PublishedFor__1_0_0
    public Map<String, Object> getContext() {
        return this.j;
    }

    @PublishedFor__1_0_0
    public String getIdentifier() {
        return this.b;
    }

    @PublishedFor__1_0_0
    public Integer getLevel() {
        return this.c;
    }

    @PublishedFor__1_0_0
    public Double getMinorResult() {
        return this.d;
    }

    @PublishedFor__1_0_0
    public Integer getMode() {
        return this.e;
    }

    @PublishedFor__1_0_0
    public Integer getRank() {
        return this.g;
    }

    @PublishedFor__1_0_0
    public Double getResult() {
        return this.f;
    }

    @PublishedFor__1_0_0
    public Date getUpdatedAt() {
        return this.h;
    }

    @PublishedFor__1_0_0
    public User getUser() {
        return this.i;
    }

    @PublishedFor__1_0_0
    public void setContext(Map<String, Object> map) {
        this.j = map;
    }

    @PublishedFor__1_0_0
    public void setLevel(Integer num) {
        this.c = num;
    }

    @PublishedFor__1_0_0
    public void setMinorResult(Double d2) {
        this.d = d2;
    }

    @PublishedFor__1_0_0
    public void setMode(Integer num) {
        this.e = num;
    }

    @PublishedFor__1_0_0
    public void setResult(Double d2) {
        this.f = d2;
    }
}
