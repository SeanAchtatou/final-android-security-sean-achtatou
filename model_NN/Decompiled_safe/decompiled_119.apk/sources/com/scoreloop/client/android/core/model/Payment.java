package com.scoreloop.client.android.core.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Payment {
    private String a;
    private Money b;
    private State c;

    public enum State {
        CREATED,
        BOOKED,
        CANCELED,
        FAILED
    }

    public Payment(JSONObject jSONObject) throws JSONException {
        a(jSONObject);
    }

    public static String c() {
        return "payment";
    }

    public String a() {
        return this.a;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("id")) {
            this.a = jSONObject.getString("id");
        }
        if (jSONObject.has("credit_money")) {
            this.b.a(jSONObject.getJSONObject("credit_money"));
        }
        if (jSONObject.has("state")) {
        }
    }

    public State b() {
        return this.c;
    }
}
