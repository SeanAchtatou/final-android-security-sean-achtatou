package com.scoreloop.client.android.core.model;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Message {
    private MessageTargetInterface a;
    private String b;
    private List<MessageReceiver> c = new ArrayList();

    private JSONArray f() {
        JSONArray jSONArray = new JSONArray();
        for (MessageReceiver b2 : d()) {
            try {
                jSONArray.put(b2.b());
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
        return jSONArray;
    }

    private JSONObject g() {
        if (b() == null) {
            throw new IllegalStateException();
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("id", b().getIdentifier());
            jSONObject.put("target_type", b().a());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public Message a() {
        Message message = new Message();
        message.a(b());
        message.a(c());
        for (MessageReceiver a2 : d()) {
            message.a(a2);
        }
        return message;
    }

    public void a(MessageReceiver messageReceiver) {
        if (!this.c.contains(messageReceiver)) {
            this.c.add(messageReceiver);
        }
    }

    public void a(MessageTargetInterface messageTargetInterface) {
        if (messageTargetInterface == null) {
            throw new IllegalArgumentException();
        }
        this.a = messageTargetInterface;
    }

    public void a(String str) {
        this.b = str;
    }

    public MessageTargetInterface b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public List<MessageReceiver> d() {
        return this.c;
    }

    public JSONObject e() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (c() != null) {
            jSONObject.put("text", c());
        }
        jSONObject.put("target", g());
        jSONObject.put("receivers", f());
        return jSONObject;
    }
}
