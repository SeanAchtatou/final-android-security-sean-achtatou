package com.scoreloop.client.android.core.controller;

class i {
    private final String a;
    private final a b;

    enum a {
        ASCENDING("ASC"),
        DESCENDING("DESC");
        
        private final String a;

        private a(String str) {
            this.a = str;
        }

        public String toString() {
            return this.a;
        }
    }

    public i(String str, a aVar) {
        this.a = str;
        this.b = aVar;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b.toString();
    }
}
