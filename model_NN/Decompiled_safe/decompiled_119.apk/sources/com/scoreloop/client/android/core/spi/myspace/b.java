package com.scoreloop.client.android.core.spi.myspace;

import android.app.Activity;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.spi.AuthViewController;
import com.scoreloop.client.android.core.spi.myspace.a;
import com.scoreloop.client.android.core.spi.myspace.c;
import com.scoreloop.client.android.core.utils.HTTPUtils;
import com.scoreloop.client.android.core.utils.JSONUtils;
import com.scoreloop.client.android.core.utils.Logger;
import com.scoreloop.client.android.core.utils.OAuthBuilder;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

class b extends AuthViewController {
    private Activity a;
    private String b;
    private String c;
    private String d;
    /* access modifiers changed from: private */
    public a e;
    private String f;
    private String g;
    private String h;

    b(Session session, AuthViewController.Observer observer) {
        super(session, observer);
    }

    private JSONObject a(String str, String str2) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("username", str);
        jSONObject.put("password", str2);
        return jSONObject;
    }

    private void a(Exception exc) {
        this.e.dismiss();
        d().a(exc);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        try {
            a(this.c, this.d, b("https://roa.myspace.com/roa/09/messaging/login/" + this.b + "/" + str));
        } catch (MalformedURLException e2) {
            throw new IllegalStateException(e2);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, HttpPut httpPut) {
        try {
            this.c = str;
            this.d = str2;
            JSONObject jSONObject = new JSONObject(HTTPUtils.a(new DefaultHttpClient().execute(b(str, str2, httpPut))));
            Logger.a("myspace response parsed json:", jSONObject.toString(4));
            if (a(jSONObject)) {
                c(jSONObject);
            } else if (b(jSONObject)) {
                d(jSONObject);
            } else {
                f();
            }
        } catch (UnsupportedEncodingException e2) {
            a(e2);
        } catch (URISyntaxException e3) {
            a(e3);
        } catch (JSONException e4) {
            a(e4);
        } catch (ClientProtocolException e5) {
            a((Exception) e5);
        } catch (IOException e6) {
            a(e6);
        }
    }

    private boolean a(JSONObject jSONObject) {
        return JSONUtils.a(jSONObject, "token") && JSONUtils.a(jSONObject, "tokenSecret") && JSONUtils.a(jSONObject, "userId");
    }

    /* access modifiers changed from: private */
    public HttpPut b(String str) throws MalformedURLException {
        OAuthBuilder oAuthBuilder = new OAuthBuilder();
        oAuthBuilder.a(new URL(str));
        oAuthBuilder.a("73c6e22c33e14b56a329e7a19b40914c");
        return oAuthBuilder.b("814d340412384e27b0d480add625fb4391d5cefac08e431f9797c3c9fa94caaf", null);
    }

    private HttpPut b(String str, String str2, HttpPut httpPut) throws URISyntaxException, JSONException, UnsupportedEncodingException {
        httpPut.setHeader("Content-Type", "application/json");
        httpPut.setEntity(new StringEntity(a(str, str2).toString()));
        return httpPut;
    }

    private boolean b(JSONObject jSONObject) throws JSONException {
        if (!JSONUtils.a(jSONObject, "captcha")) {
            return false;
        }
        JSONObject jSONObject2 = jSONObject.getJSONObject("captcha");
        return JSONUtils.a(jSONObject2, "captchaImageUri") && JSONUtils.a(jSONObject2, "captchaGuid");
    }

    private void c(JSONObject jSONObject) throws JSONException {
        Logger.a("myspace response parser", "MYSPACE: valid user credentials acquired");
        this.f = jSONObject.getString("token");
        this.g = jSONObject.getString("tokenSecret");
        this.h = jSONObject.getString("userId");
        this.e.dismiss();
        d().c();
    }

    private void d(JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject("captcha");
        Logger.a("myspace response parser", "MYSPACE: captcha request aquired");
        String string = jSONObject2.getString("captchaImageUri");
        this.b = jSONObject2.getString("captchaGuid");
        c cVar = new c(e(), new c.a() {
            public void a(String str) {
                b.this.a(str);
            }
        }, string);
        this.e.dismiss();
        cVar.show();
    }

    private Activity e() {
        return this.a;
    }

    private void f() {
        this.e.dismiss();
        d().d();
    }

    public String a() {
        return this.f;
    }

    public void a(Activity activity) {
        this.a = activity;
        this.e = new a(e(), new a.C0002a() {
            public void a() {
                b.this.e.dismiss();
                b.this.d().b();
            }

            public void a(String str, String str2) {
                try {
                    b.this.a(str, str2, b.this.b("https://roa.myspace.com/roa/09/messaging/login"));
                } catch (MalformedURLException e) {
                    throw new IllegalStateException(e);
                }
            }
        });
        this.e.show();
    }

    public String b() {
        return this.g;
    }

    public String c() {
        return this.h;
    }
}
