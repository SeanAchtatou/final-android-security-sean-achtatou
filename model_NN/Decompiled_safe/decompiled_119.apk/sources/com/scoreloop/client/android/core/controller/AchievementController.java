package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import org.json.JSONException;
import org.json.JSONObject;

public class AchievementController extends RequestController {
    private Achievement b;

    private static final class a extends Request {
        private final Achievement a;
        private final Game b;
        private final User d;

        public a(RequestCompletionCallback requestCompletionCallback, User user, Game game, Achievement achievement) {
            super(requestCompletionCallback);
            this.d = user;
            this.b = game;
            this.a = achievement;
        }

        public String a() {
            return String.format("/service/games/%s/achievements", this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                this.a.a(this.d.getIdentifier());
                jSONObject.put(this.a.a(), this.a.a(false));
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid achievement data", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    @PublishedFor__1_1_0
    public AchievementController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_1_0
    public AchievementController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.b = null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int f = response.f();
        if (f == 200 || f == 201) {
            this.b.c();
            return true;
        }
        throw new Exception("invalid status code" + f);
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_1_0
    public Achievement getAchievement() {
        return this.b;
    }

    @PublishedFor__1_1_0
    public void setAchievement(Achievement achievement) {
        if (achievement == null) {
            throw new IllegalArgumentException("achievement must not be null");
        }
        this.b = achievement;
    }

    @PublishedFor__1_1_0
    public void submitAchievement() {
        if (this.b == null) {
            throw new IllegalStateException("you have to set an achievement first");
        }
        a aVar = new a(c(), e(), a(), this.b);
        h();
        a(aVar);
    }
}
