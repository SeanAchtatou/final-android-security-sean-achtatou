package com.scoreloop.client.android.core.controller;

import java.util.Collection;
import org.json.JSONArray;

class b {
    private final String a;
    private final h b;
    private final Object c;

    public b(String str, h hVar, String str2) {
        this.a = str;
        this.b = hVar;
        this.c = str2;
    }

    public b(String str, h hVar, Collection<String> collection) {
        this.a = str;
        this.b = hVar;
        this.c = new JSONArray((Collection) collection);
    }

    public String a() {
        String name = this.b.getName();
        if (name == null) {
            return this.a;
        }
        return String.format("%s_%s", this.a, name);
    }

    public Object b() {
        return this.c;
    }
}
