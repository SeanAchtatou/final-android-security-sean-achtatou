package com.scoreloop.client.android.core.model;

import android.content.Context;
import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.server.Server;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class Session {
    private static Session a;
    private final Device b = new Device();
    private Game c;
    private final SessionObserver d;
    private final Server e;
    private String f;
    private Challenge g;
    private List<Money> h = new ArrayList();
    private State i = State.INITIAL;
    private final User j = new User();
    private Context k;
    private String l;

    public enum State {
        AUTHENTICATED,
        AUTHENTICATING,
        DEVICE_KNOWN,
        DEVICE_UNKNOWN,
        DEVICE_VERIFIED,
        FAILED,
        INITIAL,
        READY
    }

    public Session(SessionObserver sessionObserver, Server server) {
        this.e = server;
        this.d = sessionObserver;
    }

    static void a(Session session) {
        a = session;
    }

    @PublishedFor__1_0_0
    public static Session getCurrentSession() {
        return a;
    }

    public Device a() {
        return this.b;
    }

    public void a(Context context) {
        this.k = context;
    }

    public void a(Game game) {
        this.c = game;
    }

    public void a(State state) {
        this.i = state;
    }

    public void a(List<Money> list) {
        this.h = list;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        if (JSONUtils.a(jSONObject, "direct_pay_url")) {
            this.f = jSONObject.getString("direct_pay_url");
        }
        if (JSONUtils.a(jSONObject, "slapp_download_url")) {
            this.l = jSONObject.getString("slapp_download_url");
        }
        if (JSONUtils.a(jSONObject, "characteristic")) {
            String string = jSONObject.getString("characteristic");
            if (this.c != null) {
                this.c.a(string);
            }
        }
    }

    public Server b() {
        return this.e;
    }

    public State c() {
        return this.i;
    }

    public Context d() {
        return this.k;
    }

    @PublishedFor__1_0_0
    public Money getBalance() {
        return getUser().a();
    }

    @PublishedFor__1_0_0
    public Challenge getChallenge() {
        return this.g;
    }

    @PublishedFor__1_0_0
    public List<Money> getChallengeStakes() {
        return this.h;
    }

    @PublishedFor__1_0_0
    public Game getGame() {
        return this.c;
    }

    @PublishedFor__1_1_0
    public String getPaymentUrl() {
        return this.f;
    }

    @PublishedFor__1_0_0
    public List<SearchList> getScoreSearchLists() {
        return getUser().c();
    }

    @PublishedFor__1_1_0
    public String getScoreloopAppDownloadUrl() {
        return this.l;
    }

    @PublishedFor__1_0_0
    public User getUser() {
        return this.j;
    }

    @PublishedFor__1_0_0
    public boolean isAuthenticated() {
        return this.i == State.AUTHENTICATED;
    }

    @PublishedFor__1_0_0
    public void setChallenge(Challenge challenge) {
        this.g = challenge;
    }
}
