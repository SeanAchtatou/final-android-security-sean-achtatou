package com.scoreloop.client.android.core.controller;

import android.os.Handler;
import android.os.Message;
import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import org.json.JSONException;
import org.json.JSONObject;

public class ChallengeController extends RequestController {
    private Challenge b;
    private b c;

    private static class a extends Request {
        protected Challenge a;
        protected Game b;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, Challenge challenge) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: aGame should be set");
            }
            this.b = game;
            this.a = challenge;
        }

        public String a() {
            return String.format("/service/games/%s/challenges", this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("challenge", this.a.b());
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid challenge data", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    private static class b extends Handler {
        private ChallengeController a;

        public b(ChallengeController challengeController) {
            this.a = challengeController;
        }

        public void handleMessage(Message message) {
            ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) message.obj;
            switch (message.what) {
                case 1:
                    challengeControllerObserver.challengeControllerDidFailToAcceptChallenge(this.a);
                    return;
                case 2:
                    challengeControllerObserver.challengeControllerDidFailToRejectChallenge(this.a);
                    return;
                case 3:
                    challengeControllerObserver.challengeControllerDidFailOnInsufficientBalance(this.a);
                    return;
                default:
                    return;
            }
        }
    }

    private static class c extends a {
        public c(RequestCompletionCallback requestCompletionCallback, Game game, Challenge challenge) {
            super(requestCompletionCallback, game, challenge);
        }

        public String a() {
            return String.format("/service/games/%s/challenges/%s", this.b.getIdentifier(), this.a.getIdentifier());
        }

        public RequestMethod c() {
            return RequestMethod.PUT;
        }
    }

    @PublishedFor__1_0_0
    public ChallengeController(ChallengeControllerObserver challengeControllerObserver) {
        this(null, challengeControllerObserver);
    }

    @PublishedFor__1_0_0
    public ChallengeController(Session session, ChallengeControllerObserver challengeControllerObserver) {
        super(session, challengeControllerObserver);
        this.c = new b(this);
    }

    private void a(int i, RequestControllerObserver requestControllerObserver) {
        Message obtainMessage = this.c.obtainMessage(i);
        obtainMessage.obj = requestControllerObserver;
        obtainMessage.sendToTarget();
    }

    private void a(Score score, User user) {
        if (score != null && score.getUser() == null && user != null && user.a(d())) {
            score.a(e());
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) b();
        int f = response.f();
        if (f == 200 || f == 201) {
            JSONObject jSONObject = response.e().getJSONObject("challenge");
            Challenge challenge = getChallenge();
            challenge.a(jSONObject);
            User e = e();
            if ((e.equals(challenge.getContender()) && !challenge.isCreated()) || (e.equals(challenge.getContestant()) && (challenge.isRejected() || challenge.isComplete()))) {
                d().setChallenge(null);
            }
            challengeControllerObserver.requestControllerDidReceiveResponse(this);
            return false;
        }
        Integer a2 = a(response.e());
        if (a2 == null) {
            throw new Exception("Request failed with status:" + f);
        }
        switch (a2.intValue()) {
            case 22:
            case 27:
                d().setChallenge(null);
                challengeControllerObserver.challengeControllerDidFailToAcceptChallenge(this);
                return false;
            case 23:
            case 25:
            case 26:
            default:
                d().setChallenge(null);
                challengeControllerObserver.requestControllerDidFail(this, new IllegalArgumentException("error of status: " + f + " and code: " + a2));
                return false;
            case 24:
                challengeControllerObserver.challengeControllerDidFailOnInsufficientBalance(this);
                return false;
        }
    }

    @PublishedFor__1_0_0
    public void acceptChallenge() {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) b();
        if (d().getBalance().compareTo(getChallenge().getStake()) < 0) {
            a(3, challengeControllerObserver);
        } else if (!getChallenge().isPlayableForUser(e())) {
            d().setChallenge(null);
            a(1, challengeControllerObserver);
        } else {
            getChallenge().a(e(), true);
            submitChallenge();
        }
    }

    @PublishedFor__1_0_0
    public void createChallenge(Money money, User user) {
        if (money == null) {
            throw new IllegalArgumentException("aSomeMoney parameter cannot be null");
        } else if (!d().isAuthenticated()) {
            throw new IllegalStateException("session needs to be authenticated before calling ChallengeController.createChallenge");
        } else {
            User e = e();
            if (e.a().compareTo(money) < 0) {
                throw new IllegalStateException("User's balance is not sufficient");
            } else if (e.equals(user)) {
                throw new IllegalStateException("User cannot challenge himself");
            } else {
                Challenge challenge = new Challenge(money);
                challenge.setContender(e);
                challenge.setContestant(user);
                this.b = challenge;
                d().setChallenge(challenge);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_0_0
    public Challenge getChallenge() {
        if (this.b == null) {
            this.b = d().getChallenge();
        }
        return this.b;
    }

    @PublishedFor__1_0_0
    public void rejectChallenge() {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) b();
        if (!getChallenge().isAssigned() || !getChallenge().getContestant().equals(e())) {
            d().setChallenge(null);
            a(2, challengeControllerObserver);
            return;
        }
        getChallenge().a(e(), false);
        submitChallenge();
    }

    @PublishedFor__1_0_0
    public void setChallenge(Challenge challenge) {
        this.b = challenge;
    }

    @PublishedFor__1_0_0
    public void submitChallenge() {
        Challenge challenge = getChallenge();
        if (challenge == null) {
            throw new IllegalStateException("Set the challenge first");
        }
        a(challenge.getContenderScore(), challenge.getContender());
        a(challenge.getContestantScore(), challenge.getContestant());
        Request aVar = challenge.getIdentifier() == null ? new a(c(), a(), challenge) : new c(c(), a(), challenge);
        d().setChallenge(challenge);
        h();
        a(aVar);
    }

    @PublishedFor__1_0_0
    public void submitChallengeScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("aScore parameter can't be null");
        }
        Challenge challenge = getChallenge();
        if (challenge == null) {
            throw new IllegalStateException("no challenge to submit score to");
        }
        User user = score.getUser();
        if (user == null) {
            score.a(e());
        } else if (!user.a(d())) {
            throw new IllegalStateException("User is not participating in the challenge");
        }
        if (challenge == null || challenge.getMode() == score.getMode()) {
            challenge.a(score);
            submitChallenge();
            return;
        }
        throw new IllegalStateException("Score mode does not match challenge mode");
    }
}
