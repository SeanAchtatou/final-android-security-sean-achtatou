package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Ranking;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ScoresController extends RequestController {
    private RankingController b;
    private SearchList c;
    /* access modifiers changed from: private */
    public Score d;
    private Integer e;
    private b f;
    private d<Score> g;

    private class a implements RequestControllerObserver {
        private a() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            ScoresController.this.b().requestControllerDidFail(ScoresController.this, exc);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            Ranking ranking = ((RankingController) requestController).getRanking();
            if (ScoresController.this.d != null && ScoresController.this.d.getRank() == null) {
                ScoresController.this.d.a(ranking.getRank());
            }
            ScoresController.this.a(ranking.getRank());
        }
    }

    private static class b extends Request {
        private final Game a;
        private final Integer b;
        private int d;
        private final int e;
        private final SearchList f;
        private final User g;

        public b(RequestCompletionCallback requestCompletionCallback, Game game, SearchList searchList, User user, Integer num, int i, int i2) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: null game");
            }
            this.a = game;
            this.f = searchList;
            this.g = user;
            this.b = num;
            this.e = i;
            this.d = i2;
        }

        public String a() {
            return String.format("/service/games/%s/scores", this.a.getIdentifier());
        }

        public void a(int i) {
            this.d = i;
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                if (this.f != null) {
                    jSONObject.putOpt("search_list_id", this.f.getIdentifier());
                }
                jSONObject.put("user_id", this.g.getIdentifier());
                jSONObject.put("offset", this.d);
                jSONObject.put("per_page", this.e);
                if (this.b != null) {
                    jSONObject.put("mode", this.b);
                }
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid challenge data", e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__1_0_0
    public ScoresController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public ScoresController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = null;
        this.d = null;
        this.e = null;
        this.g = new d<>();
        this.c = SearchList.getDefaultScoreSearchList();
    }

    private void a(int i) {
        this.g.a(i);
        b bVar = new b(c(), a(), getSearchList(), e(), getMode(), this.g.a(), i);
        h();
        a(bVar);
    }

    /* access modifiers changed from: private */
    public void a(Integer num) {
        int i;
        if (num != null) {
            i = Math.max(1, num.intValue() - (getRangeLength() / 2));
        } else {
            i = 1;
        }
        b(i - 1);
        b bVar = this.f;
        this.f = null;
        h();
        a(bVar);
    }

    private void b(int i) {
        if (this.f == null) {
            throw new IllegalStateException("_nextRequest must not be null");
        }
        this.g.c(i);
        this.f.a(i);
    }

    private RankingController j() {
        if (this.b == null) {
            this.b = new RankingController(d(), new a());
        }
        return this.b;
    }

    private void k() {
        this.g.a(0);
        this.f = new b(c(), a(), getSearchList(), e(), getMode(), this.g.a(), 0);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int i;
        if (response.f() != 200) {
            h();
            throw new Exception("Request failed, returned status: " + response.f());
        }
        JSONArray jSONArray = response.e().getJSONArray("scores");
        Integer rank = this.d == null ? null : this.d.getRank();
        ArrayList arrayList = new ArrayList();
        int b2 = this.g.b() + 1;
        int i2 = 0;
        while (i2 < jSONArray.length()) {
            int i3 = i2 + 1;
            Score score = new Score(jSONArray.getJSONObject(i2).getJSONObject("score"));
            if (rank == null || b2 != rank.intValue() || (this.d.getIdentifier() != null && score.getIdentifier().equals(this.d.getIdentifier()))) {
                i = b2;
            } else {
                i = b2 + 1;
                score.a(Integer.valueOf(b2));
                arrayList.add(this.d);
            }
            b2 = i + 1;
            score.a(Integer.valueOf(i));
            arrayList.add(score);
            i2 = i3;
        }
        this.g.a(arrayList);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_0_0
    public Integer getMode() {
        return this.e;
    }

    @PublishedFor__1_0_0
    public int getRangeLength() {
        return this.g.f();
    }

    @PublishedFor__1_0_0
    public List<Score> getScores() {
        return this.g.d();
    }

    @PublishedFor__1_0_0
    public SearchList getSearchList() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.b != null) {
            this.b.h();
        }
        super.h();
    }

    @PublishedFor__1_0_0
    public boolean hasNextRange() {
        return this.g.h();
    }

    @PublishedFor__1_0_0
    public boolean hasPreviousRange() {
        return this.g.i();
    }

    @PublishedFor__1_0_0
    public void loadNextRange() {
        if (!hasNextRange()) {
            throw new IllegalStateException("There's no next range");
        } else if (this.g.g()) {
            a(this.g.c());
        } else {
            a(0);
        }
    }

    @PublishedFor__1_0_0
    public void loadPreviousRange() {
        if (!hasPreviousRange()) {
            throw new IllegalStateException("There's no previous range");
        } else if (this.g.g()) {
            a(this.g.e());
        } else {
            a(0);
        }
    }

    @PublishedFor__1_0_0
    public void loadRangeAtRank(int i) {
        if (i < 1) {
            throw new IllegalArgumentException("rank must be a positive integer");
        }
        a(i - 1);
    }

    @PublishedFor__1_0_0
    public void loadRangeForScore(Score score) {
        h();
        k();
        RankingController j = j();
        j.setSearchList(getSearchList());
        this.d = score;
        this.d.a((Integer) null);
        j.loadRankingForScore(score);
    }

    @PublishedFor__1_0_0
    public void loadRangeForScoreResult(Double d2, Map<String, Object> map) {
        loadRangeForScore(new Score(d2, map));
    }

    @PublishedFor__1_0_0
    public void loadRangeForUser(User user) {
        h();
        k();
        this.d = null;
        RankingController j = j();
        j.setSearchList(getSearchList());
        j.loadRankingForUserInGameMode(user, getMode());
    }

    @PublishedFor__1_0_0
    public void setMode(Integer num) {
        this.e = num;
    }

    @PublishedFor__1_0_0
    public void setRangeLength(int i) {
        this.g.b(i);
    }

    @PublishedFor__1_0_0
    public void setSearchList(SearchList searchList) {
        if (this.c != searchList) {
            this.c = searchList;
            if (this.d != null) {
                this.d.a((Integer) null);
            }
        }
    }
}
