package com.scoreloop.client.android.core.model;

public class NewsItem {
    static final /* synthetic */ boolean a = (!NewsItem.class.desiredAssertionStatus());
    private String b;

    public String a() {
        if (a || this.b != null) {
            return this.b;
        }
        throw new AssertionError();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return a().equals(((NewsItem) obj).a());
    }

    public int hashCode() {
        return a().hashCode();
    }
}
