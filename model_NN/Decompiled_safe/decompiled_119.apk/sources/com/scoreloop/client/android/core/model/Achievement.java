package com.scoreloop.client.android.core.model;

import android.graphics.Bitmap;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.utils.Formats;
import java.text.ParseException;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public final class Achievement implements MessageTargetInterface {
    private final Award a;
    private Date b;
    private String c;
    private boolean d;
    private AchievementsStore e;
    private String f;
    private int g;

    public Achievement(Award award) {
        this.a = award;
        this.g = this.a.getInitialValue();
    }

    public Achievement(AwardList awardList, String str, AchievementsStore achievementsStore) throws JSONException {
        this.e = achievementsStore;
        this.a = awardList.getAwardWithIdentifier(str);
        JSONObject a2 = this.e.a(str);
        if (a2 != null) {
            a(a2);
        } else {
            this.g = this.a.getInitialValue();
        }
    }

    public Achievement(AwardList awardList, JSONObject jSONObject) throws JSONException {
        this.a = awardList.getAwardWithIdentifier(jSONObject.getString("achievable_identifier"));
        a(jSONObject);
    }

    public static String b() {
        return "achievement";
    }

    private void e() {
        try {
            this.e.a(this.a.c(), a(true));
        } catch (JSONException e2) {
            throw new RuntimeException(e2);
        }
    }

    public String a() {
        return b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public JSONObject a(boolean z) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("achievable_list_id", getAward().b().a());
        jSONObject.put("achievable_identifier", getAward().c());
        jSONObject.put("achievable_type", getAward().f());
        jSONObject.put("achieved_count", getValue());
        jSONObject.put("id", getIdentifier());
        jSONObject.put("user_id", d());
        if (getDate() != null) {
            jSONObject.put("achieved_at", Formats.a.format(getDate()));
        }
        if (z && this.d) {
            jSONObject.put("needs_submit", true);
        }
        return jSONObject;
    }

    public void a(Achievement achievement) {
        boolean z;
        String identifier = achievement.getIdentifier();
        if (identifier == null || identifier == getIdentifier()) {
            z = false;
        } else {
            this.c = identifier;
            z = true;
        }
        int value = achievement.getValue();
        if (value > this.g) {
            this.g = value;
            this.d = false;
            z = true;
        }
        Date date = achievement.getDate();
        if (!(date == null || date == this.b)) {
            this.b = date;
            z = true;
        }
        if (z) {
            e();
        }
    }

    public void a(String str) {
        this.f = str;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        String string = jSONObject.getString("achievable_identifier");
        if (!string.equals(this.a.c())) {
            throw new JSONException("invalid achievable_identifier " + string);
        }
        String string2 = jSONObject.getString("achievable_type");
        if (!string2.equals(this.a.f())) {
            throw new JSONException("invalid achievable_type " + string2);
        }
        this.g = jSONObject.getInt("achieved_count");
        if (!this.a.isValidCounterValue(this.g)) {
            throw new JSONException("invalid achieved_count " + this.g);
        }
        if (jSONObject.has("id")) {
            this.c = jSONObject.getString("id");
        }
        if (jSONObject.has("user_id")) {
            this.f = jSONObject.getString("user_id");
        }
        if (jSONObject.has("achieved_at")) {
            try {
                this.b = Formats.a.parse(jSONObject.getString("achieved_at"));
            } catch (ParseException e2) {
                throw new JSONException("Invalid format of the update date");
            }
        }
        if (jSONObject.has("needs_submit")) {
            this.d = jSONObject.getBoolean("needs_submit");
        }
    }

    public void c() {
        this.d = false;
        e();
    }

    public String d() {
        return this.f;
    }

    @PublishedFor__1_1_0
    public Award getAward() {
        return this.a;
    }

    @PublishedFor__1_1_0
    public Date getDate() {
        return this.b;
    }

    @PublishedFor__1_1_0
    public String getIdentifier() {
        return this.c;
    }

    @PublishedFor__1_1_0
    public Bitmap getImage() {
        return isAchieved() ? getAward().getAchievedImage() : getAward().getUnachievedImage();
    }

    @PublishedFor__1_1_0
    public int getValue() {
        return this.g;
    }

    @PublishedFor__1_1_0
    public boolean isAchieved() {
        return getAward().isAchievedByValue(getValue());
    }

    @PublishedFor__1_1_0
    public boolean needsSubmit() {
        return this.d;
    }

    @PublishedFor__1_1_0
    public void setAchieved() {
        setValue(this.a.getAchievingValue());
    }

    @PublishedFor__1_1_0
    public void setValue(int i) {
        if (!this.a.isValidCounterValue(i)) {
            throw new IllegalArgumentException("invalid value, should be in: " + this.a.getCounterRange());
        } else if (i < this.g) {
            throw new IllegalArgumentException("the value to set must not be less than the old value: " + this.g);
        } else if (this.e == null) {
            throw new IllegalStateException("setValue() on achievement only allowed for session user");
        } else {
            boolean isAchieved = isAchieved();
            this.g = i;
            if (isAchieved() != isAchieved) {
                this.d = true;
                this.b = new Date();
            }
            e();
        }
    }

    public String toString() {
        return "Achievement [award=" + getAward() + ", value=" + getValue() + ", isAchieved=" + isAchieved() + "]";
    }
}
