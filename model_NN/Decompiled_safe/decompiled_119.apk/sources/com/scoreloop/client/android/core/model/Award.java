package com.scoreloop.client.android.core.model;

import android.graphics.Bitmap;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;

public final class Award {
    private Bitmap a = null;
    private final AwardList b;
    private final Range c;
    private final String d;
    private final int e;
    private String f = "";
    private String g = "";
    private final Money h;
    private Bitmap i = null;

    Award(AwardList awardList, String str, Range range, int i2, Money money) {
        if (awardList == null || str == null || range == null || i2 < 0 || money == null) {
            throw new IllegalArgumentException();
        }
        this.b = awardList;
        this.d = str;
        this.c = range;
        this.e = i2;
        this.h = money;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return "Award." + c() + ".achieved.png";
    }

    /* access modifiers changed from: package-private */
    public void a(Bitmap bitmap) {
        this.a = bitmap;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public AwardList b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void b(Bitmap bitmap) {
        this.i = bitmap;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.g = str;
    }

    public String c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return "Award." + c() + ".description";
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return "Award." + c() + ".title";
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.d.equals(((Award) obj).d);
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return "Award";
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return "Award." + c() + ".unachieved.png";
    }

    @PublishedFor__1_1_0
    public Bitmap getAchievedImage() {
        return this.a;
    }

    @PublishedFor__1_1_0
    public int getAchievingValue() {
        return this.c.b();
    }

    @PublishedFor__1_1_0
    public Range getCounterRange() {
        return this.c;
    }

    @PublishedFor__1_1_0
    public int getInitialValue() {
        return this.c.getLocation();
    }

    @PublishedFor__1_1_0
    public String getLocalizedDescription() {
        return this.f;
    }

    @PublishedFor__1_1_0
    public String getLocalizedTitle() {
        return this.g;
    }

    @PublishedFor__1_1_0
    public Money getRewardMoney() {
        return this.h;
    }

    @PublishedFor__1_1_0
    public Bitmap getUnachievedImage() {
        return this.i;
    }

    public int hashCode() {
        int i2 = 1 * 31;
        return ((this.b.a().hashCode() + 31) * 31) + this.d.hashCode();
    }

    @PublishedFor__1_1_0
    public boolean isAchievedByValue(int i2) {
        return i2 >= getAchievingValue();
    }

    @PublishedFor__1_1_0
    public boolean isValidCounterValue(int i2) {
        return getInitialValue() <= i2 && i2 <= getAchievingValue();
    }

    public String toString() {
        return "Award [id=" + c() + "]";
    }
}
