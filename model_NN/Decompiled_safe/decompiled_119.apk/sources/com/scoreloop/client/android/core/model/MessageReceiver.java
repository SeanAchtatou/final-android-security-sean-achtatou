package com.scoreloop.client.android.core.model;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageReceiver {
    private MessageReceiverInterface a;
    private List<User> b;

    public MessageReceiver(MessageReceiverInterface messageReceiverInterface, List<User> list) {
        this.a = messageReceiverInterface;
        this.b = list;
    }

    public MessageReceiverInterface a() {
        return this.a;
    }

    public JSONObject b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("receiver_type", a().b());
        JSONArray jSONArray = new JSONArray();
        if (this.b != null && this.b.size() > 0) {
            for (User e : this.b) {
                jSONArray.put(e.e());
            }
            jSONObject.put("users", jSONArray);
        }
        return jSONObject;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof MessageReceiver)) {
            return super.equals(obj);
        }
        MessageReceiver messageReceiver = (MessageReceiver) obj;
        if (messageReceiver == this) {
            return true;
        }
        return a().equals(messageReceiver.a());
    }

    public int hashCode() {
        return a().hashCode();
    }
}
