package com.scoreloop.client.android.core.server;

import android.os.Handler;
import android.os.Message;
import com.scoreloop.client.android.core.server.a;
import com.scoreloop.client.android.core.utils.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class f extends Thread implements BayeuxConnectionObserver {
    static final /* synthetic */ boolean a = (!f.class.desiredAssertionStatus());
    private static int i = 0;
    private boolean b;
    private final Handler c;
    private a d;
    private volatile Request e;
    private volatile a.C0001a f;
    private volatile boolean g;
    private JSONObject h = null;

    f(Handler handler) {
        this.c = handler;
        this.g = true;
        StringBuilder append = new StringBuilder().append(getClass().getSimpleName());
        int i2 = i;
        i = i2 + 1;
        setName(append.append(i2).toString());
        setDaemon(true);
    }

    private void a(Response response, int i2, Exception exc) {
        synchronized (this) {
            this.e = null;
            this.f = null;
        }
        Message obtainMessage = this.c.obtainMessage(i2);
        if (i2 == 1) {
            obtainMessage.obj = response;
        } else {
            obtainMessage.obj = exc;
        }
        obtainMessage.sendToTarget();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        synchronized (this) {
            this.g = false;
            if (this.f != null) {
                this.f.a();
            } else {
                notify();
            }
        }
        try {
            join();
        } catch (InterruptedException e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Request request) {
        boolean z = false;
        synchronized (this) {
            if (this.e != null) {
                throw new IllegalStateException("Previous request not finished yet");
            } else if (!request.l()) {
                this.e = request;
                this.f = this.d.a();
                this.f.a(this.e.h(), this.e.a(), this.e.b(), this.e.g());
                notify();
            } else {
                z = true;
            }
        }
        if (z) {
            Logger.a("ServerCommThread", "startRequest(): Request was cancelled early");
            a(null, 2, null);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.d = aVar;
    }

    public void a(a aVar, Integer num, Object obj, String str, int i2) {
        this.b = true;
        a(new Response(obj, str, i2, num), 1, null);
    }

    public void a(a aVar, JSONObject jSONObject) {
    }

    public void a(JSONObject jSONObject) {
        this.h = jSONObject;
    }

    public JSONObject b(a aVar, JSONObject jSONObject) {
        try {
            if (this.h != null) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("ext");
                JSONArray names = this.h.names();
                int length = names.length();
                for (int i2 = 0; i2 < length; i2++) {
                    String string = names.getString(i2);
                    jSONObject2.put(string, this.h.get(string));
                }
            }
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Should never happen");
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Logger.a("ServerCommThread", "abortCurrentRequest() called");
        synchronized (this) {
            if (this.f != null) {
                this.f.a();
            }
        }
    }

    public void run() {
        Logger.a("ServerCommThread", "run(): entering loop");
        while (this.g) {
            try {
                synchronized (this) {
                    if (this.f == null && this.g) {
                        wait();
                    }
                    if (this.f != null) {
                        try {
                            Logger.a("ServerCommThread", "run(): got bayeux request to process");
                            this.b = false;
                            this.d.a(this.f);
                            if (!a && !this.b) {
                                throw new AssertionError();
                            }
                        } catch (c e2) {
                            Logger.a("ServerCommThread", "run(): Request processing was interrupted\n" + e2);
                            a(null, 2, e2);
                        } catch (e e3) {
                            Logger.a("ServerCommThread", "run(): publish() failed\n" + e3);
                            a(null, 3, e3);
                        } catch (Exception e4) {
                            Logger.a("ServerCommThread", "run(): publish() failed\n" + e4);
                            a(null, 3, e4);
                        }
                    }
                }
            } catch (InterruptedException e5) {
                Logger.a("ServerCommThread", "run(): interrupted while waiting for request");
            }
        }
        Logger.a("ServerCommThread", "run(): exitting loop");
    }
}
