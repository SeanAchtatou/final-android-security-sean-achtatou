package com.scoreloop.client.android.core.spi.twitter;

import android.content.Context;
import android.view.KeyEvent;
import android.webkit.WebView;
import com.scoreloop.client.android.core.ui.WebViewDialog;
import java.net.MalformedURLException;
import java.net.URL;

class b extends WebViewDialog {
    private a a;

    b(Context context, int i, a aVar) {
        super(context, i);
        this.a = aVar;
    }

    private void c() {
        this.a.d().b();
        a();
        dismiss();
    }

    private void d() {
        this.a.d().a(new IllegalStateException("unparsable twitter response"));
        a();
        dismiss();
    }

    /* access modifiers changed from: protected */
    public void a(WebView webView, String str) {
        try {
            URL url = new URL(str);
            if (url.getHost().equalsIgnoreCase("www.scoreloop.com") && url.getPath().equalsIgnoreCase("/twitter/oauth")) {
                String[] split = url.getQuery().split("=");
                String str2 = split[0];
                if (str2.equalsIgnoreCase("done")) {
                    c();
                } else if (split.length == 2) {
                    String str3 = split[1];
                    if (!str2.equalsIgnoreCase("oauth_token")) {
                        return;
                    }
                    if (str3.equals(this.a.a())) {
                        this.a.d().c();
                        dismiss();
                        return;
                    }
                    d();
                } else {
                    d();
                }
            }
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            c();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
