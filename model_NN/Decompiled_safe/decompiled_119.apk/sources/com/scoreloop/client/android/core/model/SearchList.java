package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchList {
    private final String a;
    private String b;

    public SearchList(String str) {
        this.a = str;
    }

    SearchList(String str, String str2) {
        this(str);
        this.b = str2;
    }

    public SearchList(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.getString("id");
        this.b = jSONObject.getString("name");
    }

    static SearchList a(String str) {
        List<SearchList> c;
        if (str == null) {
            throw new IllegalArgumentException();
        }
        Session currentSession = Session.getCurrentSession();
        if (!(currentSession == null || (c = currentSession.getUser().c()) == null)) {
            for (SearchList next : c) {
                if (str.equalsIgnoreCase(next.getIdentifier())) {
                    return next;
                }
            }
        }
        return new SearchList(str, "");
    }

    @PublishedFor__1_0_0
    public static SearchList getBuddiesScoreSearchList() {
        return a("701bb990-80d8-11de-8a39-0800200c9a66");
    }

    @PublishedFor__1_0_0
    public static SearchList getDefaultScoreSearchList() {
        List<SearchList> c;
        Session currentSession = Session.getCurrentSession();
        return (currentSession == null || (c = currentSession.getUser().c()) == null || c.size() <= 0) ? getGlobalScoreSearchList() : c.get(0);
    }

    @PublishedFor__1_0_0
    public static SearchList getGlobalScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a1");
    }

    @PublishedFor__1_0_0
    public static SearchList getLocalScoreSearchList() {
        return a("#local");
    }

    @PublishedFor__1_0_0
    public static SearchList getTwentyFourHourScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a2");
    }

    @PublishedFor__1_0_0
    public static SearchList getUserCountryLocationScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a3");
    }

    @PublishedFor__1_0_0
    public static SearchList getUserNationalityScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a4");
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SearchList)) {
            return super.equals(obj);
        }
        SearchList searchList = (SearchList) obj;
        if (getIdentifier() != null && searchList.getIdentifier() != null) {
            return getIdentifier().equalsIgnoreCase(searchList.getIdentifier());
        }
        throw new IllegalStateException();
    }

    @PublishedFor__1_0_0
    public String getIdentifier() {
        return this.a;
    }

    @PublishedFor__1_0_0
    public String getName() {
        return this.b;
    }

    public int hashCode() {
        if (getIdentifier() != null) {
            return getIdentifier().hashCode();
        }
        throw new IllegalStateException();
    }

    @PublishedFor__1_0_0
    public void setName(String str) {
        this.b = str;
    }

    @PublishedFor__1_0_0
    public String toString() {
        return getName();
    }
}
