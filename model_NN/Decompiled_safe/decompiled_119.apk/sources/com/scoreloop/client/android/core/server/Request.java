package com.scoreloop.client.android.core.server;

import org.json.JSONObject;

public class Request {
    private static int a = 0;
    static final /* synthetic */ boolean c = (!Request.class.desiredAssertionStatus());
    private final RequestCompletionCallback b;
    private String d;
    private JSONObject e;
    private Exception f;
    private JSONObject g;
    private final int h = d();
    private RequestMethod i = RequestMethod.GET;
    private Response j;
    private State k = State.IDLE;
    private Object l;

    public enum State {
        CANCELLED,
        COMPLETED,
        ENQUEUED,
        EXECUTING,
        FAILED,
        IDLE
    }

    public Request(RequestCompletionCallback requestCompletionCallback) {
        this.b = requestCompletionCallback;
    }

    public static int d() {
        int i2 = a;
        a = i2 + 1;
        return i2;
    }

    public String a() {
        return this.d;
    }

    public void a(Response response) {
        if (c || this.k == State.EXECUTING) {
            this.k = State.COMPLETED;
            this.j = response;
            this.f = null;
            return;
        }
        throw new AssertionError();
    }

    public void a(Exception exc) {
        if (c || this.k == State.EXECUTING) {
            this.k = State.FAILED;
            this.j = null;
            this.f = exc;
            return;
        }
        throw new AssertionError();
    }

    public void a(Object obj) {
        this.l = obj;
    }

    public void a(JSONObject jSONObject) {
        this.g = jSONObject;
    }

    public JSONObject b() {
        return this.e;
    }

    public RequestMethod c() {
        return this.i;
    }

    public RequestCompletionCallback e() {
        if (this.b != null) {
            return this.b;
        }
        throw new IllegalStateException();
    }

    public Exception f() {
        return this.f;
    }

    public JSONObject g() {
        return this.g;
    }

    public int h() {
        return this.h;
    }

    public Response i() {
        return this.j;
    }

    public synchronized State j() {
        return this.k;
    }

    public Object k() {
        return this.l;
    }

    public boolean l() {
        State j2 = j();
        return j2 == State.COMPLETED || j2 == State.CANCELLED || j2 == State.FAILED;
    }

    public void m() {
        if (c || this.k == State.EXECUTING || this.k == State.ENQUEUED) {
            this.k = State.CANCELLED;
            this.j = null;
            this.f = null;
            return;
        }
        throw new AssertionError();
    }

    public void n() {
        if (c || this.k == State.IDLE) {
            this.k = State.ENQUEUED;
            return;
        }
        throw new AssertionError();
    }

    public void o() {
        if (c || this.k == State.IDLE || this.k == State.ENQUEUED) {
            this.k = State.EXECUTING;
            return;
        }
        throw new AssertionError();
    }
}
