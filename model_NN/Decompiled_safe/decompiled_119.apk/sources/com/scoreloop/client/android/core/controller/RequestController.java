package com.scoreloop.client.android.core.controller;

import android.os.Handler;
import android.os.Message;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class RequestController {
    static final /* synthetic */ boolean a = (!RequestController.class.desiredAssertionStatus());
    private final a b;
    private Exception c;
    private final RequestControllerObserver d;
    /* access modifiers changed from: private */
    public Request e;
    private final Session f;
    /* access modifiers changed from: private */
    public a g;

    private class a implements RequestCompletionCallback {
        private a() {
        }

        public void a(Request request) {
            RequestController.this.c(null);
            switch (request.j()) {
                case COMPLETED:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request completed: ", request);
                    try {
                        if (RequestController.this.a(request, request.i())) {
                            RequestController.this.k();
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        RequestController.this.d(e);
                        return;
                    }
                case FAILED:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request failed: ", request);
                    RequestController.this.d(request.f());
                    return;
                case CANCELLED:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request cancelled: ", request);
                    RequestController.this.d(new RequestCancelledException());
                    return;
                default:
                    throw new IllegalStateException("onRequestCompleted called for not completed request");
            }
        }

        public void b(Request request) {
        }
    }

    private class b extends Handler {
        private final Exception b;
        private final boolean c;
        private final RequestControllerObserver d;

        public b(RequestControllerObserver requestControllerObserver, boolean z, Exception exc) {
            this.d = requestControllerObserver;
            this.b = exc;
            this.c = z;
        }

        public void handleMessage(Message message) {
            if (this.c) {
                this.d.requestControllerDidFail(RequestController.this, this.b);
            } else {
                this.d.requestControllerDidReceiveResponse(RequestController.this);
            }
        }
    }

    private class c implements RequestControllerObserver {
        private c() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            Logger.a("RequestController", "Session authentication failed, failing _request");
            RequestController.this.e.a(exc);
            RequestController.this.e.e().a(RequestController.this.e);
            a unused = RequestController.this.g = (a) null;
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            a unused = RequestController.this.g = (a) null;
        }
    }

    RequestController(Session session, RequestControllerObserver requestControllerObserver) {
        if (requestControllerObserver == null) {
            throw new IllegalArgumentException("observer parameter cannot be null");
        }
        if (session == null) {
            this.f = Session.getCurrentSession();
        } else {
            this.f = session;
        }
        if (a || this.f != null) {
            this.d = requestControllerObserver;
            this.b = new a();
            i();
            return;
        }
        throw new AssertionError();
    }

    static Integer a(JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2;
        if (!jSONObject.has("error") || (jSONObject2 = jSONObject.getJSONObject("error")) == null) {
            return null;
        }
        return Integer.valueOf(jSONObject2.getInt("code"));
    }

    private void b(Request request) {
        this.e = request;
    }

    /* access modifiers changed from: private */
    public void d(Exception exc) {
        Logger.a("onRequestCompleted", "failed with exception: ", exc);
        c(exc);
        this.d.requestControllerDidFail(this, exc);
    }

    private Request j() {
        return this.e;
    }

    /* access modifiers changed from: private */
    public void k() {
        Logger.a("RequestController.invokeDidReceiveResponse", " observer = ", this.d);
        this.d.requestControllerDidReceiveResponse(this);
    }

    /* access modifiers changed from: package-private */
    public final Game a() {
        return d().getGame();
    }

    /* access modifiers changed from: package-private */
    public void a(Request request) {
        Session.State c2;
        if (!(!g() || (c2 = d().c()) == Session.State.AUTHENTICATED || c2 == Session.State.AUTHENTICATING)) {
            if (this.g == null) {
                this.g = new a(d(), new c());
            }
            this.g.j();
        }
        b(request);
        this.f.b().a(request);
    }

    /* access modifiers changed from: package-private */
    public void a(Exception exc) {
        Request j = j();
        if (j != null) {
            j.a(exc);
            d(exc);
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(Request request, Response response) throws Exception;

    /* access modifiers changed from: package-private */
    public RequestControllerObserver b() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void b(Exception exc) {
        Logger.a("RequestController.invokeDelayedDidReceiveResponse", " observer = ", this.d);
        new b(this.d, true, exc).obtainMessage().sendToTarget();
    }

    /* access modifiers changed from: package-private */
    public a c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void c(Exception exc) {
        this.c = exc;
    }

    /* access modifiers changed from: package-private */
    public final Session d() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final User e() {
        return d().getUser();
    }

    /* access modifiers changed from: protected */
    public void f() {
        Logger.a("RequestController.invokeDelayedDidReceiveResponse", " observer = ", this.d);
        new b(this.d, false, null).obtainMessage().sendToTarget();
    }

    /* access modifiers changed from: package-private */
    public abstract boolean g();

    /* access modifiers changed from: package-private */
    public void h() {
        c(null);
        if (this.e != null) {
            if (!this.e.l()) {
                this.f.b().b(this.e);
            }
            this.e = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (a() == null) {
            throw new IllegalStateException("we do not allow game id to be null at all, please initialize Client with valid game id and secret");
        }
    }
}
