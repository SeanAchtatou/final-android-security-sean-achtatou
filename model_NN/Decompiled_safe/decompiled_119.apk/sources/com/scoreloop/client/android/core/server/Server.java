package com.scoreloop.client.android.core.server;

import android.os.Handler;
import android.os.Message;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.utils.Base64;
import com.scoreloop.client.android.core.utils.Logger;
import java.net.URL;
import java.nio.channels.IllegalSelectorException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import org.json.JSONException;
import org.json.JSONObject;

public class Server {
    static final /* synthetic */ boolean a = (!Server.class.desiredAssertionStatus());
    private final URL b;
    private final f c = new f(this.d);
    private final a d = new a();
    private a e;
    /* access modifiers changed from: private */
    public Request f;
    private final LinkedList<Request> g = new LinkedList<>();

    private class a extends Handler {
        private a() {
        }

        public void handleMessage(Message message) {
            Request a2 = Server.this.f;
            Request unused = Server.this.f = null;
            if (a2.j() != Request.State.CANCELLED) {
                switch (message.what) {
                    case 1:
                        Response response = (Response) message.obj;
                        Integer b = response.b();
                        if (b != null && b.intValue() == a2.h()) {
                            a2.a(response);
                            break;
                        } else {
                            a2.a(new Exception("Invalid response ID, expected:" + a2.h() + ", but was:" + b));
                            break;
                        }
                        break;
                    case 2:
                        a2.a((Exception) message.obj);
                        break;
                    case 3:
                        a2.a((Exception) message.obj);
                        break;
                    default:
                        throw new IllegalStateException("Unknown message type");
                }
                a2.e().a(a2);
            }
            if (Server.this.f == null) {
                Server.this.d();
            }
        }
    }

    public Server(URL url) {
        this.b = url;
        this.c.setPriority(1);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            instance.reset();
            instance.update("https://api.scoreloop.com/bayeux/android/v2".getBytes());
            byte[] digest = instance.digest();
            instance.reset();
            instance.update("https://www.scoreloop.com/android/updates".getBytes());
            byte[] digest2 = instance.digest();
            byte[] bArr = new byte[16];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) ((digest[(i + 6) % digest.length] ^ digest2[(i + 3) % digest2.length]) ^ 62);
            }
            this.e = new a(b(), this.c, bArr);
            for (int i2 = 0; i2 < digest.length; i2++) {
                digest[i2] = (byte) (digest[i2] ^ 26);
            }
            this.e.b(Base64.a(digest));
            for (int i3 = 0; i3 < digest2.length; i3++) {
                digest2[i3] = (byte) (digest2[i3] ^ 53);
            }
            this.e.a(Base64.a(digest2));
            this.c.a(this.e);
            this.c.start();
        } catch (NoSuchAlgorithmException e2) {
            throw new IllegalStateException();
        }
    }

    private void c() {
        if (this.f != null) {
            Logger.a("Server", "doCancelCurrentRequest canceling request: ", this.f);
            this.f.m();
            this.f.e().a(this.f);
            this.c.b();
        }
    }

    private void c(Request request) {
        Logger.a("Server", "startProcessingRequest: " + request.toString());
        if (!a && request == null) {
            throw new AssertionError();
        } else if (!a && request.l()) {
            throw new AssertionError();
        } else if (a || this.f == null) {
            this.f = request;
            this.f.e().b(this.f);
            this.f.o();
            this.c.a(this.f);
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        Request poll;
        if (a || this.f == null) {
            do {
                poll = this.g.poll();
                if (poll != null && !poll.l()) {
                    c(poll);
                    return;
                }
            } while (poll != null);
            return;
        }
        throw new AssertionError();
    }

    public void a() {
        this.c.a();
    }

    public void a(Request request) {
        Logger.a("Server", "addRequest: ", request);
        if (request.j() == Request.State.ENQUEUED || request.j() == Request.State.EXECUTING) {
            throw new IllegalStateException("Request already enqueued or executing");
        } else if (request.a() == null) {
            throw new IllegalStateException("Request channel is not set");
        } else if (request.c() == null) {
            throw new IllegalStateException("Request method is not set");
        } else {
            if (request.g() == null) {
                request.a(new JSONObject());
            }
            try {
                request.g().put("method", request.c().toString());
                if (this.f != null || !this.g.isEmpty()) {
                    request.n();
                    this.g.add(request);
                    return;
                }
                c(request);
            } catch (JSONException e2) {
                throw new IllegalSelectorException();
            }
        }
    }

    public void a(JSONObject jSONObject) {
        this.c.a(jSONObject);
    }

    /* access modifiers changed from: package-private */
    public URL b() {
        return this.b;
    }

    public void b(Request request) {
        Logger.a("Server", "cancelRequest: ", request);
        if (this.f == request) {
            c();
            return;
        }
        request.m();
        request.e().a(request);
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        a();
        super.finalize();
    }
}
