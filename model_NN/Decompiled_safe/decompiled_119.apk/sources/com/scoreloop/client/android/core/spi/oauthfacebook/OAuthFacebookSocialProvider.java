package com.scoreloop.client.android.core.spi.oauthfacebook;

import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import org.json.JSONException;
import org.json.JSONObject;

public final class OAuthFacebookSocialProvider extends SocialProvider {
    public Class<?> a() {
        return OAuthFacebookSocialProviderController.class;
    }

    /* access modifiers changed from: package-private */
    public void a(User user, String str) {
        JSONObject jSONObject = null;
        if (str != null) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("access_token", str);
                jSONObject = jSONObject2;
            } catch (JSONException e) {
            }
        }
        user.a(jSONObject, c());
    }

    public String getIdentifier() {
        return SocialProvider.FACEBOOK_IDENTIFIER;
    }
}
