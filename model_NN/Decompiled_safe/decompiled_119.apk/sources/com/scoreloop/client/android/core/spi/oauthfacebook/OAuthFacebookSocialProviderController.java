package com.scoreloop.client.android.core.spi.oauthfacebook;

import android.os.Bundle;
import android.webkit.CookieManager;
import com.a.a.a;
import com.a.a.c;
import com.a.a.d;
import com.a.a.e;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public final class OAuthFacebookSocialProviderController extends SocialProviderController {
    private static final String[] b = {"publish_stream", "read_stream", "offline_access"};

    public OAuthFacebookSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    private static String a(InputStream inputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 1000);
        for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
            sb.append(readLine);
        }
        inputStream.close();
        return sb.toString();
    }

    private static String a(String str, Bundle bundle) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str + "?" + a.a(bundle)).openConnection();
        httpURLConnection.setRequestProperty("User-Agent", "FacebookConnect");
        httpURLConnection.setRequestProperty("Cookie", CookieManager.getInstance().getCookie("facebook.com"));
        return a(httpURLConnection.getInputStream());
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        String str2;
        try {
            Bundle bundle = new Bundle();
            bundle.putString("user_id", h().getIdentifier());
            String[] split = a("http://apps.facebook.com/scoreloop/scoreloop/sdk", bundle).split("SL123456789#");
            if (split.length > 1) {
                String[] split2 = split[1].split("#", 2);
                if (split2.length > 1) {
                    str2 = split2[0];
                    if (str2.equalsIgnoreCase("ok")) {
                        b().a(h(), str);
                        i();
                        return;
                    }
                } else {
                    str2 = "";
                }
                throw new IllegalArgumentException("GOT app's boundary but the message was != OK : " + str2);
            }
            throw new IllegalArgumentException("DID NOT FIND APP BOUNDARY IN THE RESPONSE");
        } catch (Exception e) {
            f().socialProviderControllerDidFail(this, e);
        }
    }

    private OAuthFacebookSocialProvider b() {
        return (OAuthFacebookSocialProvider) getSocialProvider();
    }

    /* access modifiers changed from: protected */
    public void a() {
        final d dVar = new d();
        dVar.a(e(), "95076661cafb5979188d7a687991d339", b, new d.a() {
            public void a() {
                OAuthFacebookSocialProviderController.this.f().socialProviderControllerDidCancel(OAuthFacebookSocialProviderController.this);
            }

            public void a(Bundle bundle) {
                String b2 = dVar.b();
                if (b2 != null) {
                    OAuthFacebookSocialProviderController.this.a(b2);
                } else {
                    OAuthFacebookSocialProviderController.this.f().socialProviderControllerDidFail(OAuthFacebookSocialProviderController.this, new IllegalStateException("completed w/o token"));
                }
            }

            public void a(c cVar) {
                OAuthFacebookSocialProviderController.this.f().socialProviderControllerDidFail(OAuthFacebookSocialProviderController.this, cVar);
            }

            public void a(e eVar) {
                OAuthFacebookSocialProviderController.this.f().socialProviderControllerDidFail(OAuthFacebookSocialProviderController.this, eVar);
            }
        });
    }
}
