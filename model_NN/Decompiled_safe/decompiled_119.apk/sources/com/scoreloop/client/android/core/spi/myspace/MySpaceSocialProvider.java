package com.scoreloop.client.android.core.spi.myspace;

import com.scoreloop.client.android.core.model.SocialProvider;

public class MySpaceSocialProvider extends SocialProvider {
    public Class<?> a() {
        return MySpaceSocialProviderController.class;
    }

    public String getIdentifier() {
        return SocialProvider.MYSPACE_IDENTIFIER;
    }
}
