package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.controller.AddressBook;
import com.scoreloop.client.android.core.controller.i;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UsersController extends RequestController {
    private int b;
    private boolean c;
    private SearchSpec d;
    private String e;
    private String f;
    private LoginSearchOperator g;
    private List<User> h;
    private Integer i;

    @PublishedFor__1_0_0
    public enum LoginSearchOperator {
        EXACT_MATCH,
        LIKE,
        PREFIX
    }

    private static class a extends Request {
        static final Object a = new Object();
        private User b;
        private Game d;

        public a(RequestCompletionCallback requestCompletionCallback, User user, Game game) {
            super(requestCompletionCallback);
            this.b = user;
            this.d = game;
            a(a);
        }

        public String a() {
            return String.format("/service/users/%s/games/%s/buddies", this.b.getIdentifier(), this.d.getIdentifier());
        }

        public JSONObject b() {
            return null;
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    private class b extends Request {
        private final Game b;
        private final int d;
        private final boolean e;
        private User f;
        private c g;

        public b(RequestCompletionCallback requestCompletionCallback, Game game, boolean z, int i) {
            super(requestCompletionCallback);
            this.b = game;
            this.e = z;
            this.d = i;
        }

        public String a() {
            if (this.e) {
                return "/service/users";
            }
            return String.format("/service/games/%s/users", this.b.getIdentifier());
        }

        public void a(c cVar) {
            this.g = cVar;
        }

        public void a(User user) {
            this.f = user;
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("limit", this.d);
                if (this.g != null) {
                    if (this.f != null) {
                        this.g.a(this.f.getIdentifier());
                    }
                    jSONObject.put("search_list", this.g.a());
                }
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid data", e2);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    private static final class c {
        private String a;
        private SearchSpec b;
        private String c;

        c(String str, SearchSpec searchSpec, String str2) {
            this.a = str;
            this.b = searchSpec;
            this.c = str2;
        }

        /* access modifiers changed from: package-private */
        public JSONObject a() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("name", this.a);
                jSONObject.put("definition", this.b.a());
                jSONObject.put("id", this.c);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid search spec data");
            }
        }

        /* access modifiers changed from: package-private */
        public void a(String str) {
            if (this.b != null) {
                this.b.a(new b("reference_user_id", h.EXACT, str));
            }
        }
    }

    @PublishedFor__1_0_0
    public UsersController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public UsersController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.b = 25;
        this.c = true;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = LoginSearchOperator.PREFIX;
        this.h = Collections.emptyList();
        this.i = null;
    }

    private b a(String str, SearchSpec searchSpec) {
        this.f = null;
        this.e = str;
        this.d = searchSpec;
        return j();
    }

    private void a(List<User> list, Integer num) {
        this.h = Collections.unmodifiableList(list);
        this.i = num;
    }

    private b j() {
        b bVar = new b(c(), a(), this.c, this.b);
        if (this.f == null && this.d == null) {
            throw new IllegalStateException("Set a search list or use one of searchBy.. methods first");
        }
        bVar.a(new c(this.e, this.d, this.f));
        return bVar;
    }

    private SearchSpec k() {
        SearchSpec searchSpec = new SearchSpec(new i("login", i.a.ASCENDING));
        if (!this.c) {
            searchSpec.a(new b("skills_game_id", h.IS, a().getIdentifier()));
        }
        return searchSpec;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int i2 = 0;
        int f2 = response.f();
        if (f2 != 200) {
            throw new Exception("Request failed with status:" + f2);
        }
        ArrayList arrayList = new ArrayList();
        Integer num = null;
        if (request.k() == a.a) {
            JSONArray jSONArray = response.e().getJSONObject("user").getJSONArray("buddies");
            int length = jSONArray.length();
            while (i2 < length) {
                arrayList.add(new User(jSONArray.getJSONObject(i2)));
                i2++;
            }
        } else if (response.c()) {
            JSONArray d2 = response.d();
            int length2 = d2.length();
            while (i2 < length2) {
                arrayList.add(new User(d2.getJSONObject(i2).getJSONObject("user")));
                i2++;
            }
        } else {
            num = Integer.valueOf(response.e().getInt("users_count"));
        }
        a(arrayList, num);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_0_0
    public int getCountOfUsers() {
        return this.i != null ? this.i.intValue() : getUsers().size();
    }

    @PublishedFor__1_0_0
    public int getSearchLimit() {
        return this.b;
    }

    @PublishedFor__1_0_0
    public LoginSearchOperator getSearchOperator() {
        return this.g;
    }

    @PublishedFor__1_0_0
    public boolean getSearchesGlobal() {
        return this.c;
    }

    @PublishedFor__1_0_0
    public List<User> getUsers() {
        return this.h;
    }

    @PublishedFor__1_1_0
    public boolean isMaxUserCount() {
        return getCountOfUsers() >= 999;
    }

    @PublishedFor__1_0_0
    public boolean isOverLimit() {
        return this.i != null;
    }

    @PublishedFor__1_1_0
    public void loadBuddiesForGame(Game game) {
        if (game == null) {
            throw new IllegalArgumentException("The game parameter must not be null");
        }
        a aVar = new a(c(), e(), game);
        h();
        a(aVar);
    }

    @PublishedFor__1_0_0
    public void searchByEmail(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Parameter anEmail cannot be null");
        }
        SearchSpec k = k();
        k.a(new b("email", h.EXACT, str));
        b a2 = a("UserEMailSearch", k);
        h();
        a(a2);
    }

    @PublishedFor__1_0_0
    public void searchByLocalAddressBook() {
        SearchSpec k = k();
        AddressBook a2 = AddressBook.a();
        a2.a(AddressBook.HashAlgorithm.MD5);
        a2.a("shwu2831j78s");
        List<String> a3 = a2.a(d().d());
        if (a3.size() == 0) {
            a(Collections.emptyList(), (Integer) null);
            f();
            return;
        }
        if (a3.size() == 1) {
            k.a(new b("email_digest", h.EQUALS_ANY, a3.get(0)));
        } else {
            k.a(new b("email_digest", h.EQUALS_ANY, a3));
        }
        b a4 = a("UserAddressBookSearch", k);
        h();
        a(a4);
    }

    @PublishedFor__1_0_0
    public void searchByLogin(String str) {
        h hVar;
        if (str == null) {
            throw new IllegalArgumentException("Parameter aLogin cannot be null");
        }
        SearchSpec k = k();
        if (this.g != null) {
            switch (this.g) {
                case LIKE:
                    hVar = h.LIKE;
                    break;
                case EXACT_MATCH:
                    hVar = h.EXACT;
                    break;
                default:
                    hVar = h.BEGINS_WITH;
                    break;
            }
        } else {
            hVar = h.BEGINS_WITH;
        }
        k.a(new b("login", hVar, str));
        b a2 = a("UserLoginSearch", k);
        h();
        a(a2);
    }

    @PublishedFor__1_0_0
    public void searchBySocialProvider(SocialProvider socialProvider) {
        if (socialProvider == null) {
            throw new IllegalArgumentException("Parameter aSocialProvider cannot be null");
        }
        SearchSpec k = k();
        k.a(new b("social_provider_id", h.EXACT, socialProvider.getIdentifier()));
        b a2 = a("#user_social_provider_search", k);
        a2.a(e());
        h();
        a(a2);
    }

    @PublishedFor__1_0_0
    public void setSearchLimit(int i2) {
        this.b = i2;
    }

    @PublishedFor__1_0_0
    public void setSearchOperator(LoginSearchOperator loginSearchOperator) {
        if (loginSearchOperator == null) {
            throw new IllegalArgumentException("Parameter aSearchOperator cannot be null");
        }
        this.g = loginSearchOperator;
    }

    @PublishedFor__1_0_0
    public void setSearchesGlobal(boolean z) {
        if (a() != null || z) {
            this.c = z;
            return;
        }
        throw new IllegalArgumentException("cannot search not globally without game being set");
    }
}
