package com.scoreloop.client.android.core.controller;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class NewsItemController {
    public static final Policy a = new e();
    public static final Policy b = new f();
    public static final Policy c = new g();
    public static final Policy d = new ChainedPolicy(new Policy[]{c, b});
    private static final Set<String> e = new HashSet();

    public static final class ChainedPolicy implements Policy {
        Policy[] a;

        ChainedPolicy(Policy[] policyArr) {
            this.a = policyArr;
        }
    }

    public interface Policy {
    }

    public static final class RequestNextItemCanceledException extends RuntimeException {
        private static final long serialVersionUID = 1;
    }

    public enum State {
        IDLE,
        PENDING
    }

    static {
        Collections.addAll(e, "image/png", "image/x-png", "image/jpeg");
    }
}
