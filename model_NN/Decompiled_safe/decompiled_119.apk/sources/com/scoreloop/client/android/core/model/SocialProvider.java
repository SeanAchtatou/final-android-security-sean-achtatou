package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.spi.myspace.MySpaceSocialProvider;
import com.scoreloop.client.android.core.spi.oauthfacebook.OAuthFacebookSocialProvider;
import com.scoreloop.client.android.core.spi.twitter.TwitterSocialProvider;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class SocialProvider implements MessageReceiverInterface {
    @PublishedFor__1_0_0
    public static final String FACEBOOK_IDENTIFIER = "com.facebook.v1";
    @PublishedFor__1_0_0
    public static final String MYSPACE_IDENTIFIER = "com.myspace.v1";
    @PublishedFor__1_0_0
    public static final String TWITTER_IDENTIFIER = TwitterSocialProvider.a;
    private static List<SocialProvider> a = null;

    public static void a(User user, JSONObject jSONObject) {
        for (SocialProvider d : getSupportedProviders()) {
            d.d(user, jSONObject);
        }
    }

    public static boolean a(Class<?> cls) {
        for (SocialProvider socialProvider : getSupportedProviders()) {
            if (socialProvider.getClass().equals(cls)) {
                return true;
            }
        }
        return false;
    }

    static void b(User user, JSONObject jSONObject) {
        for (SocialProvider e : getSupportedProviders()) {
            e.e(user, jSONObject);
        }
    }

    static void c(User user, JSONObject jSONObject) {
        for (SocialProvider f : getSupportedProviders()) {
            f.f(user, jSONObject);
        }
    }

    @PublishedFor__1_0_0
    public static SocialProvider getSocialProviderForIdentifier(String str) {
        for (SocialProvider next : getSupportedProviders()) {
            if (next.getIdentifier().equalsIgnoreCase(str)) {
                return next;
            }
        }
        return null;
    }

    @PublishedFor__1_0_0
    public static List<SocialProvider> getSupportedProviders() {
        if (a == null) {
            a = new ArrayList();
            a.add(new MySpaceSocialProvider());
            a.add(new TwitterSocialProvider());
            a.add(new OAuthFacebookSocialProvider());
        }
        return a;
    }

    public abstract Class<?> a();

    public void a(User user, String str, String str2, String str3) {
        if (user == null || str == null || str2 == null || str3 == null) {
            throw new IllegalArgumentException();
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("token", str);
            jSONObject.put("token_secret", str2);
            jSONObject.put("uid", str3);
            user.a(jSONObject, c());
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public String b() {
        return getIdentifier();
    }

    /* access modifiers changed from: protected */
    public String c() {
        return getIdentifier();
    }

    /* access modifiers changed from: protected */
    public String d() {
        return getName();
    }

    /* access modifiers changed from: protected */
    public void d(User user, JSONObject jSONObject) {
        if (user == null || jSONObject == null) {
            throw new IllegalArgumentException();
        }
        JSONObject jSONObject2 = null;
        if (JSONUtils.a(jSONObject, d())) {
            try {
                jSONObject2 = jSONObject.getJSONObject(d());
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
        user.a(jSONObject2, c());
    }

    /* access modifiers changed from: protected */
    public void e(User user, JSONObject jSONObject) {
        if (user == null || jSONObject == null) {
            throw new IllegalArgumentException();
        } else if (JSONUtils.a(jSONObject, d())) {
            try {
                user.a(jSONObject.getJSONObject(d()), c());
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void f(User user, JSONObject jSONObject) {
        JSONObject c = user.c(c());
        if (c != null) {
            try {
                jSONObject.put(d(), c);
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    @PublishedFor__1_0_0
    public abstract String getIdentifier();

    @PublishedFor__1_0_0
    public final String getName() {
        return getIdentifier().split("\\.")[1];
    }

    @PublishedFor__1_0_0
    public final Integer getVersion() {
        return new Integer(getIdentifier().split("\\.")[2].substring(1));
    }

    @PublishedFor__1_0_0
    public boolean isUserConnected(User user) {
        return user.b(c());
    }
}
