package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Payment;
import com.scoreloop.client.android.core.model.PaymentCredential;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentController extends RequestController {
    private PaymentCredential b;
    private Payment c;
    private boolean d;

    private abstract class a extends Request {
        protected final Game a;
        protected final User b;
        protected final PaymentCredential d;
        protected final JSONObject e;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, User user, PaymentCredential paymentCredential, JSONObject jSONObject) {
            super(requestCompletionCallback);
            this.a = game;
            this.b = user;
            this.d = paymentCredential;
            this.e = jSONObject;
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                JSONObject p = p();
                if (this.e != null) {
                    Iterator<String> keys = this.e.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        p.put(next, this.e.get(next));
                    }
                }
                jSONObject.put("payment", p);
                return jSONObject;
            } catch (JSONException e2) {
                throw new IllegalStateException("Invalid payment data", e2);
            }
        }

        /* access modifiers changed from: protected */
        public abstract JSONObject p();

        /* access modifiers changed from: protected */
        public abstract c q();
    }

    private class b extends a {
        protected final String g;

        public b(RequestCompletionCallback requestCompletionCallback, Game game, User user, PaymentCredential paymentCredential, JSONObject jSONObject, String str) {
            super(requestCompletionCallback, game, user, paymentCredential, jSONObject);
            this.g = str;
        }

        public String a() {
            return String.format("/service/games/%s/users/%s/payments/%s", this.a.getIdentifier(), this.b.getIdentifier(), this.g);
        }

        public RequestMethod c() {
            return RequestMethod.PUT;
        }

        /* access modifiers changed from: protected */
        public JSONObject p() {
            return this.d.a();
        }

        /* access modifiers changed from: protected */
        public c q() {
            return c.SubmitPayment;
        }
    }

    private enum c {
        StartPayment,
        SubmitPayment
    }

    public PaymentController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    private void j() {
        b bVar = new b(c(), a(), e(), this.b, null, this.c.a());
        h();
        a(bVar);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        boolean z;
        if (response.f() == 200) {
            Payment payment = new Payment(response.e().getJSONObject(Payment.c()));
            switch (((a) request).q()) {
                case StartPayment:
                    if (this.c.b() != Payment.State.CREATED) {
                        throw new IllegalStateException("Payment state should be CREATED");
                    }
                    break;
                case SubmitPayment:
                    if (this.c.b() != Payment.State.BOOKED) {
                        throw new IllegalStateException("Payment state should be BOOKED");
                    }
                    break;
                default:
                    throw new IllegalStateException("Invalid request type");
            }
            this.c = payment;
            z = true;
        } else {
            z = false;
        }
        if (!this.d) {
            return true;
        }
        this.d = false;
        if (z) {
            j();
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void h() {
        this.d = false;
        super.h();
    }
}
