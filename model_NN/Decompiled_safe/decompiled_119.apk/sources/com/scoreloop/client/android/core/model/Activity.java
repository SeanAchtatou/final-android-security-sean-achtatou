package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.utils.Formats;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.text.ParseException;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class Activity {
    private Date a;
    private String b;
    private String c;

    public Activity(JSONObject jSONObject) throws JSONException {
        a(jSONObject);
    }

    private String a() {
        return this.c;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        if (JSONUtils.a(jSONObject, "message")) {
            this.b = jSONObject.getString("message");
        }
        if (JSONUtils.a(jSONObject, "updated_at")) {
            try {
                this.a = Formats.a.parse(jSONObject.getString("updated_at"));
            } catch (ParseException e) {
                throw new JSONException(e.getMessage());
            }
        }
        if (JSONUtils.a(jSONObject, "target_type")) {
            this.c = jSONObject.getString("target_type");
        } else {
            this.c = null;
        }
    }

    @PublishedFor__1_0_0
    public Date getDate() {
        return this.a;
    }

    @PublishedFor__1_0_0
    public String getMessage() {
        return this.b;
    }

    public String toString() {
        return getDate() + " ; " + getMessage() + " ; " + a();
    }
}
