package com.scoreloop.client.android.core.model;

import android.content.Context;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AwardList {
    private final List<Award> a = new ArrayList();
    private final Map<String, Award> b = new HashMap();
    private String c;

    public static AwardList a(Context context, String str, String str2) {
        if (context != null && str != null) {
            return new a(context).a(str, str2);
        }
        throw new IllegalArgumentException("you have to provide context and a game");
    }

    public String a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(Award award) {
        String c2 = award.c();
        if (this.b.containsKey(c2)) {
            throw new IllegalStateException("award with same identifier already added");
        }
        this.a.add(award);
        this.b.put(c2, award);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.c = str;
    }

    @PublishedFor__1_1_0
    public Award getAwardWithIdentifier(String str) {
        if (str == null) {
            throw new IllegalArgumentException("identifier argument must not be null");
        }
        Award award = this.b.get(str);
        if (award != null) {
            return award;
        }
        throw new IllegalArgumentException("invalid identifier: " + str);
    }

    @PublishedFor__1_1_0
    public List<Award> getAwards() {
        return this.a;
    }
}
