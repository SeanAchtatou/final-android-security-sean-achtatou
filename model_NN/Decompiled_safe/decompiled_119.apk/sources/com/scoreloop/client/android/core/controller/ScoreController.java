package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class ScoreController extends RequestController {
    private Score b;
    private boolean c;
    private ChallengeController d;

    private class a implements ChallengeControllerObserver {
        private a() {
        }

        public void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController) {
            throw new IllegalStateException();
        }

        public void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController) {
            throw new IllegalStateException();
        }

        public void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController) {
            throw new IllegalStateException();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            ScoreController.this.b().requestControllerDidFail(ScoreController.this, exc);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            ScoreController.this.b().requestControllerDidReceiveResponse(ScoreController.this);
        }
    }

    private static class b extends Request {
        private final Game a;
        private final Score b;

        public b(RequestCompletionCallback requestCompletionCallback, Game game, Score score) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: null game passed");
            }
            this.a = game;
            this.b = score;
        }

        public String a() {
            return String.format("/service/games/%s/scores", this.a.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("score", this.b.b());
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid score data", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    @PublishedFor__1_0_0
    public ScoreController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public ScoreController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = false;
    }

    private void a(Score score) {
        this.b = score;
    }

    private void j() {
        if (d().getChallenge() != null) {
            if (this.c) {
                if (this.d == null) {
                    this.d = new ChallengeController(d(), new a());
                }
                this.d.submitChallengeScore(this.b);
                return;
            }
            Logger.c("It seems that a challenge is in progess. Submitted score won't get associated with that challenge. Call setShouldSubmitScoreForChallenge(true); to make this go away.");
        }
        this.b.a(e());
        b bVar = new b(c(), a(), this.b);
        h();
        a(bVar);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int f = response.f();
        JSONObject jSONObject = response.e().getJSONObject("score");
        if (getScore() == null) {
            a(new Score(jSONObject));
        } else {
            getScore().a(jSONObject);
        }
        if (f == 200 || f == 201) {
            return true;
        }
        throw new Exception("Request failed");
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_0_0
    public Score getScore() {
        return this.b;
    }

    @PublishedFor__1_0_0
    public void setShouldSubmitScoreForChallenge(boolean z) {
        this.c = z;
    }

    @PublishedFor__1_0_0
    public boolean shouldSubmitScoreForChallenge() {
        return this.c;
    }

    @PublishedFor__1_0_0
    public void submitScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("aScore parameter cannot be null");
        }
        a(score);
        j();
    }
}
