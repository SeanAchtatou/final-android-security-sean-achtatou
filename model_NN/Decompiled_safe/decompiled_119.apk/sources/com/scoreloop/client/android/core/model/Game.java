package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import org.json.JSONException;
import org.json.JSONObject;

public class Game implements MessageTargetInterface {
    @PublishedFor__1_1_0
    public static final String CHARACTERISTIC_AGILITY = "agility";
    @PublishedFor__1_1_0
    public static final String CHARACTERISTIC_KNOWLEDGE = "knowledge";
    @PublishedFor__1_1_0
    public static final String CHARACTERISTIC_STRATEGY = "strategy";
    @PublishedFor__1_0_0
    public static final String CONTEXT_KEY_LEVEL = "SLContextKeyLevel";
    @PublishedFor__1_0_0
    public static final String CONTEXT_KEY_MINOR_RESULT = "SLContextKeyMinorResult";
    @PublishedFor__1_0_0
    public static final String CONTEXT_KEY_MODE = "SLContextKeyMode";
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private Integer f;
    private Integer g;
    private Integer h;
    private Integer i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;

    Game(String str, String str2) {
        if (str == null || str2 == null) {
            throw new IllegalArgumentException("id and secret must be passed");
        }
        this.d = str;
        this.k = str2;
    }

    public Game(JSONObject jSONObject) throws JSONException {
        a(jSONObject);
    }

    public String a() {
        return "game";
    }

    public void a(String str) {
        this.a = str;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("id")) {
            this.d = jSONObject.getString("id");
        }
        if (jSONObject.has("name")) {
            this.j = jSONObject.getString("name");
        }
        if (jSONObject.has("min_level")) {
            this.h = Integer.valueOf(jSONObject.getInt("min_level"));
        }
        if (jSONObject.has("max_level")) {
            this.f = Integer.valueOf(jSONObject.getInt("max_level"));
        }
        if (jSONObject.has("mode_count")) {
            this.i = 0;
            this.g = Integer.valueOf(jSONObject.getInt("mode_count") - 1);
        }
        if (jSONObject.has("characteristic")) {
            this.a = jSONObject.getString("characteristic");
        }
        if (jSONObject.has("description")) {
            this.b = jSONObject.getString("description");
        }
        if (jSONObject.has("version")) {
            this.m = jSONObject.getString("version");
        }
        if (jSONObject.has("image_url")) {
            this.e = jSONObject.getString("image_url");
        }
        if (jSONObject.has("download_url")) {
            this.c = jSONObject.getString("download_url");
        }
        if (jSONObject.has("state")) {
            this.l = jSONObject.getString("state");
        }
        if (jSONObject.has("publisher_name")) {
            this.n = jSONObject.getString("publisher_name");
        }
    }

    public String b() {
        return this.k;
    }

    @PublishedFor__1_1_0
    public String getCharacteristic() {
        return this.a;
    }

    @PublishedFor__1_1_0
    public String getDescription() {
        return this.b;
    }

    @PublishedFor__1_1_0
    public String getDownloadUrl() {
        return this.c;
    }

    @PublishedFor__1_0_0
    public String getIdentifier() {
        return this.d;
    }

    @PublishedFor__1_1_0
    public String getImageUrl() {
        return this.e;
    }

    @PublishedFor__1_0_0
    public Integer getLevelCount() {
        if (!hasLevels()) {
            return 1;
        }
        return Integer.valueOf(getMaxLevel().intValue() - getMinLevel().intValue());
    }

    @PublishedFor__1_0_0
    public Integer getMaxLevel() {
        return this.f;
    }

    @PublishedFor__1_0_0
    public Integer getMaxMode() {
        return this.g;
    }

    @PublishedFor__1_0_0
    public Integer getMinLevel() {
        return this.h;
    }

    @PublishedFor__1_0_0
    public Integer getMinMode() {
        return this.i;
    }

    @PublishedFor__1_0_0
    public Integer getModeCount() {
        if (!hasModes()) {
            return 1;
        }
        return Integer.valueOf(getMaxMode().intValue() - getMinMode().intValue());
    }

    @PublishedFor__1_0_0
    public String getName() {
        return this.j;
    }

    @PublishedFor__1_1_0
    public String getPublisherName() {
        return this.n;
    }

    @PublishedFor__1_0_0
    public String getVersion() {
        return this.m;
    }

    @PublishedFor__1_0_0
    public boolean hasLevels() {
        return (getMinLevel() == null || getMaxLevel() == null) ? false : true;
    }

    @PublishedFor__1_0_0
    public boolean hasModes() {
        return (getMinMode() == null || getMaxMode() == null) ? false : true;
    }

    @PublishedFor__1_0_0
    public void setMaxLevel(Integer num) {
        this.f = num;
    }

    @PublishedFor__1_0_0
    public void setMaxMode(Integer num) {
        this.g = num;
    }

    @PublishedFor__1_0_0
    public void setMinLevel(Integer num) {
        this.h = num;
    }

    @PublishedFor__1_0_0
    public void setMinMode(Integer num) {
        this.i = num;
    }

    @PublishedFor__1_0_0
    public void setName(String str) {
        this.j = str;
    }

    @PublishedFor__1_0_0
    public void setVersion(String str) {
        this.m = str;
    }
}
