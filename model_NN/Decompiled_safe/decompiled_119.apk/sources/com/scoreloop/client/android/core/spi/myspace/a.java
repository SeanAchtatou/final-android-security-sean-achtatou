package com.scoreloop.client.android.core.spi.myspace;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.scoreloop.client.android.core.ui.ScoreloopCustomDialog;
import com.scoreloop.client.android.core.utils.HTTPUtils;
import com.scoreloop.client.android.core.utils.Logger;
import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.http.client.ClientProtocolException;

class a extends ScoreloopCustomDialog {
    /* access modifiers changed from: private */
    public C0002a a;

    /* renamed from: com.scoreloop.client.android.core.spi.myspace.a$a  reason: collision with other inner class name */
    interface C0002a {
        void a();

        void a(String str, String str2);
    }

    a(Context context, C0002a aVar) {
        super(context);
        this.a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap a(Bitmap bitmap, int i, int i2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        int rgb = Color.rgb(25, 70, 155);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundColor(rgb);
        linearLayout.setGravity(17);
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(HTTPUtils.a("http://community.scoreloop.com/images/myspace/android_login.png"));
        } catch (ClientProtocolException e) {
            Logger.a("could not load background for myspace credentials dialog...");
            e.printStackTrace();
        } catch (URISyntaxException e2) {
            throw new IllegalStateException(e2);
        } catch (IOException e3) {
            Logger.a("could not load background for myspace credentials dialog...");
            e3.printStackTrace();
        }
        ImageView imageView = new ImageView(getContext());
        if (bitmap != null) {
            imageView.setImageBitmap(a(bitmap, 209, 53));
        }
        setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                a.this.a.a();
            }
        });
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        final EditText editText = new EditText(getContext());
        editText.setWidth(150);
        editText.setHint("E-Mail");
        final EditText editText2 = new EditText(getContext());
        editText2.setWidth(150);
        editText2.setTransformationMethod(PasswordTransformationMethod.getInstance());
        editText2.setHint("Password");
        TextView textView = new TextView(getContext());
        textView.setText("Connect MySpace with this app to find your friends and post stories to MySpace");
        textView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        textView.setBackgroundColor(rgb);
        textView.setSingleLine(false);
        Button button = new Button(getContext());
        button.setWidth(60);
        button.setText("Login");
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                a.this.a.a(editText.getText().toString(), editText2.getText().toString());
            }
        });
        linearLayout.addView(imageView);
        linearLayout.addView(editText);
        linearLayout.addView(editText2);
        linearLayout.addView(button);
        linearLayout.addView(textView);
        linearLayout.setPadding(50, 100, 50, 50);
        setContentView(linearLayout);
    }
}
