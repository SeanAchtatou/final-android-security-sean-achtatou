package com.scoreloop.client.android.core.model;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import org.json.JSONException;
import org.json.JSONObject;

public class Device {
    private Context a;
    private String b;
    private State c = State.UNKNOWN;

    public enum State {
        CREATED(201),
        FREED(1),
        UNKNOWN(0),
        VERIFIED(200);
        
        private final int a;

        private State(int i) {
            this.a = i;
        }

        public int intValue() {
            return this.a;
        }
    }

    public String a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        this.a = context;
    }

    public void a(State state) {
        this.c = state;
    }

    public void a(String str) {
        this.b = str;
    }

    public String b() {
        return Build.MODEL;
    }

    public String c() {
        return null;
    }

    public String d() {
        return "Android";
    }

    public String e() {
        return Build.VERSION.RELEASE;
    }

    public String f() {
        if (this.a != null) {
            return ((TelephonyManager) this.a.getSystemService("phone")).getDeviceId();
        }
        throw new IllegalStateException("please use setAndroidContext before calling this method");
    }

    public JSONObject g() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", this.b);
        jSONObject.put("uuid", f());
        jSONObject.put("name", c());
        jSONObject.put("system_name", d());
        jSONObject.put("system_version", e());
        jSONObject.put("model", b());
        return jSONObject;
    }
}
