package com.scoreloop.client.android.core.server;

class e extends Exception {
    private static final long serialVersionUID = -2669708330654835714L;

    public e() {
    }

    public e(String str) {
        super(str);
    }

    public e(String str, Throwable th) {
        super(str, th);
    }

    public e(Throwable th) {
        super(th);
    }
}
