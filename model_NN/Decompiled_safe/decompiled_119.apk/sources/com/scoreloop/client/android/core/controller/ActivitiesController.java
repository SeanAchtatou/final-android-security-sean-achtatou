package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.model.Activity;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;

public class ActivitiesController extends RequestController {
    private List<Activity> b;

    private enum a {
        buddy,
        game,
        user,
        other
    }

    private class b extends Request {
        private final Game b;
        private final a d;
        private final User e;

        public b(RequestCompletionCallback requestCompletionCallback, Game game, User user, a aVar) {
            super(requestCompletionCallback);
            this.e = user;
            this.d = aVar;
            this.b = game;
        }

        public String a() {
            switch (this.d) {
                case buddy:
                    if (this.e == null) {
                        throw new IllegalStateException("internal error: no _user set");
                    }
                    return String.format("/service/users/%s/buddies/last_activities", this.e.getIdentifier());
                case game:
                    if (this.b == null) {
                        throw new IllegalStateException("internal error: no _game set");
                    }
                    return String.format("/service/games/%s/activities", this.b.getIdentifier());
                case user:
                    if (this.e == null) {
                        throw new IllegalStateException("internal error: no _user set");
                    }
                    return String.format("/service/users/%s/activities", this.e.getIdentifier());
                default:
                    return "/service/activities";
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__1_0_0
    public ActivitiesController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public ActivitiesController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.b = Collections.emptyList();
    }

    private void a(List<Activity> list) {
        this.b = Collections.unmodifiableList(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        if (response.f() == 200) {
            ArrayList arrayList = new ArrayList();
            JSONArray d = response.d();
            for (int i = 0; i < d.length(); i++) {
                arrayList.add(new Activity(d.getJSONObject(i).getJSONObject("activity")));
            }
            a(arrayList);
            return true;
        }
        throw new IllegalStateException("invalid response status: " + response.f());
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    @PublishedFor__1_0_0
    public List<Activity> getActivities() {
        return this.b;
    }

    @PublishedFor__1_1_0
    public void loadActivitiesForUser(User user) {
        h();
        a(new b(c(), a(), user, a.user));
    }

    @PublishedFor__1_0_0
    public void loadBuddyActivities() {
        h();
        a(new b(c(), a(), e(), a.buddy));
    }

    @PublishedFor__1_0_0
    public void loadGameActivities() {
        if (a() == null) {
            throw new IllegalArgumentException("using loadGameActivities does not make sense without gameID being set on AcitiviesController instance");
        }
        h();
        a(new b(c(), a(), null, a.game));
    }
}
