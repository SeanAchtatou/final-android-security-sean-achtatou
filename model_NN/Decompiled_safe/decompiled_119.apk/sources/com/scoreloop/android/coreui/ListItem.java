package com.scoreloop.android.coreui;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.User;

class ListItem {
    private Game game;
    private String label;
    private Score score;
    private int tag;
    private User user;

    ListItem(Game game2) {
        this.game = game2;
    }

    ListItem(Score score2) {
        this.score = score2;
    }

    ListItem(String label2) {
        this.label = label2;
    }

    ListItem(String label2, int tag2) {
        this.label = label2;
        this.tag = tag2;
    }

    ListItem(User user2) {
        this.user = user2;
    }

    /* access modifiers changed from: package-private */
    public Game getGame() {
        return this.game;
    }

    /* access modifiers changed from: package-private */
    public String getLabel() {
        return this.label;
    }

    /* access modifiers changed from: package-private */
    public Score getScore() {
        return this.score;
    }

    /* access modifiers changed from: package-private */
    public int getTag() {
        return this.tag;
    }

    /* access modifiers changed from: package-private */
    public User getUser() {
        return this.user;
    }

    /* access modifiers changed from: package-private */
    public boolean isSpecialItem() {
        return this.user == null && this.score == null && this.game == null;
    }
}
