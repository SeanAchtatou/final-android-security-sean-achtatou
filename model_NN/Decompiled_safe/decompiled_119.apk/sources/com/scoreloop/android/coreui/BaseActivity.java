package com.scoreloop.android.coreui;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.controller.UserControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

abstract class BaseActivity extends ActivityGroup {
    static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    static final int DIALOG_ERROR_NETWORK = 0;
    static final int DIALOG_ERROR_SOCIAL_FAILED = 5;
    static final int DIALOG_ERROR_SOCIAL_USER_CANCEL = 4;
    static final int DIALOG_ERROR_SOCIAL_USER_INVALID = 6;
    static final int DIALOG_ERROR_USER_EMAIL_ALREADY_TAKEN = 1;
    static final int DIALOG_ERROR_USER_INVALID_EMAIL_FORMAT = 2;
    static final int DIALOG_ERROR_USER_NAME_ALREADY_TAKEN = 3;
    static final int MENU_GAMES = 2;
    static final int MENU_HIGHSCORES = 0;
    static final int MENU_PROFILE = 1;
    static final SocialProvider facebookProvider = SocialProvider.getSocialProviderForIdentifier(SocialProvider.FACEBOOK_IDENTIFIER);
    /* access modifiers changed from: private */
    public static Map<String, Drawable> map = Collections.synchronizedMap(new HashMap());
    static final SocialProvider myspaceProvider = SocialProvider.getSocialProviderForIdentifier(SocialProvider.MYSPACE_IDENTIFIER);
    /* access modifiers changed from: private */
    public static Runnable notify;
    static final SocialProvider twitterProvider = SocialProvider.getSocialProviderForIdentifier(SocialProvider.TWITTER_IDENTIFIER);
    /* access modifiers changed from: private */
    public final Handler handler = new Handler();
    private boolean shouldShowDialogs;

    BaseActivity() {
    }

    private final class SocialConnectObserver implements SocialProviderControllerObserver {
        private SocialConnectObserver() {
        }

        /* synthetic */ SocialConnectObserver(BaseActivity baseActivity, SocialConnectObserver socialConnectObserver) {
            this();
        }

        public void socialProviderControllerDidCancel(SocialProviderController controller) {
            BaseActivity.this.showDialogSafe(4);
            BaseActivity.this.onSocialProviderConnectionStatusChange(controller.getSocialProvider());
        }

        public void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController controller) {
            BaseActivity.this.showDialogSafe(6);
            BaseActivity.this.onSocialProviderConnectionStatusChange(controller.getSocialProvider());
        }

        public void socialProviderControllerDidFail(SocialProviderController controller, Throwable error) {
            BaseActivity.this.showDialogSafe(5);
            BaseActivity.this.onSocialProviderConnectionStatusChange(controller.getSocialProvider());
        }

        public void socialProviderControllerDidSucceed(SocialProviderController controller) {
            BaseActivity.this.onSocialProviderConnectionStatusChange(controller.getSocialProvider());
        }
    }

    class GenericListItemAdapter extends ArrayAdapter<ListItem> {
        ImageView image;
        ListItem listItem;
        TextView text0;
        TextView text1;
        TextView text2;

        public GenericListItemAdapter(Context context, int resource, List<ListItem> objects) {
            super(context, resource, objects);
        }

        /* access modifiers changed from: package-private */
        public View init(int position, View convertView) {
            if (convertView == null) {
                convertView = BaseActivity.this.getLayoutInflater().inflate((int) R.layout.sl_list_item, (ViewGroup) null);
            }
            this.listItem = (ListItem) getItem(position);
            this.text0 = (TextView) convertView.findViewById(R.id.text0);
            this.text1 = (TextView) convertView.findViewById(R.id.text1);
            this.text2 = (TextView) convertView.findViewById(R.id.text2);
            this.image = (ImageView) convertView.findViewById(R.id.image);
            return convertView;
        }
    }

    class UserGenericObserver implements UserControllerObserver {
        UserGenericObserver() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
        }

        public void userControllerDidFailOnEmailAlreadyTaken(UserController controller) {
        }

        public void userControllerDidFailOnInvalidEmailFormat(UserController controller) {
        }

        public void userControllerDidFailOnUsernameAlreadyTaken(UserController controller) {
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        ImageView imageView;
        if (hasFocus && (imageView = (ImageView) getTopActivity().findViewById(R.id.progress_indicator)) != null && imageView.getVisibility() == 0) {
            ((AnimationDrawable) imageView.getBackground()).start();
        }
    }

    private Dialog createErrorDialog(int resId) {
        Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(1);
        View view = getLayoutInflater().inflate((int) R.layout.sl_dialog_custom, (ViewGroup) null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);
        ((TextView) view.findViewById(R.id.message)).setText(getString(resId));
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return createErrorDialog(R.string.sl_error_message_network);
            case 1:
                return createErrorDialog(R.string.sl_error_message_email_already_taken);
            case 2:
                return createErrorDialog(R.string.sl_error_message_invalid_email);
            case 3:
                return createErrorDialog(R.string.sl_error_message_name_already_taken);
            case 4:
                return createErrorDialog(R.string.sl_error_message_user_cancel);
            case 5:
                return createErrorDialog(R.string.sl_error_message_connect_failed);
            case 6:
                return createErrorDialog(R.string.sl_error_message_user_invalid);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.shouldShowDialogs = false;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.shouldShowDialogs = true;
    }

    /* access modifiers changed from: package-private */
    public void connectToSocialProvider(SocialProvider socialProvider) {
        if (socialProvider.isUserConnected(Session.getCurrentSession().getUser())) {
            onSocialProviderConnectionStatusChange(socialProvider);
        } else {
            SocialProviderController.getSocialProviderController(socialProvider.getIdentifier(), new SocialConnectObserver(this, null)).connect(this);
        }
    }

    /* access modifiers changed from: package-private */
    public Drawable getDrawable(final String url) {
        if (!map.containsKey(url)) {
            map.put(url, getResources().getDrawable(R.drawable.sl_game_default));
            new Thread() {
                public void run() {
                    try {
                        BaseActivity.map.put(url, Drawable.createFromStream(new DefaultHttpClient().execute(new HttpGet(url)).getEntity().getContent(), "src"));
                    } catch (MalformedURLException e) {
                        BaseActivity.map.remove(url);
                    } catch (IOException e2) {
                        BaseActivity.map.remove(url);
                    }
                    BaseActivity.this.handler.post(BaseActivity.notify);
                }
            }.start();
        }
        return map.get(url);
    }

    /* access modifiers changed from: package-private */
    public Activity getTopActivity() {
        return getParent() == null ? this : getParent();
    }

    /* access modifiers changed from: package-private */
    public <T> boolean isEmpty(T t) {
        return t == null || "".equals(t.toString().trim());
    }

    /* access modifiers changed from: package-private */
    public void onSocialProviderConnectionStatusChange(SocialProvider socialProvider) {
    }

    /* access modifiers changed from: package-private */
    public void setNotify(Runnable notify2) {
        notify = notify2;
    }

    /* access modifiers changed from: package-private */
    public void setProgressIndicator(boolean visible) {
        ImageView imageView = (ImageView) getTopActivity().findViewById(R.id.progress_indicator);
        if (visible) {
            imageView.setVisibility(0);
        } else {
            imageView.setVisibility(4);
        }
    }

    /* access modifiers changed from: package-private */
    public void showDialogSafe(int res) {
        if (this.shouldShowDialogs) {
            showDialog(res);
        }
    }

    /* access modifiers changed from: package-private */
    public void showToast(String message) {
        View view = getLayoutInflater().inflate((int) R.layout.sl_dialog_custom, (ViewGroup) null);
        ((TextView) view.findViewById(R.id.message)).setText(message);
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(0);
        toast.setView(view);
        toast.show();
    }

    /* access modifiers changed from: package-private */
    public void updateHeading(String heading, boolean showIcon) {
        ((TextView) findViewById(R.id.heading_text)).setText(heading);
        if (!showIcon) {
            ((ImageView) findViewById(R.id.icon_image)).setVisibility(8);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateStatusBar() {
        if (Session.getCurrentSession().isAuthenticated()) {
            ((TextView) getTopActivity().findViewById(R.id.login_text)).setText(Session.getCurrentSession().getUser().getLogin());
        }
    }
}
