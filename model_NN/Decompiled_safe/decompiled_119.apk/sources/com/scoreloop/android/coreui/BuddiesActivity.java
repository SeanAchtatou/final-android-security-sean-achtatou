package com.scoreloop.android.coreui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.android.coreui.BaseActivity;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import java.util.ArrayList;
import java.util.List;

public class BuddiesActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public ListItemAdapter adapter;
    private LinearLayout addButton;
    private UserController buddiesRequestController;
    private ListView usersListView;

    private class BuddiesRequestObserver extends BaseActivity.UserGenericObserver {
        private BuddiesRequestObserver() {
            super();
        }

        /* synthetic */ BuddiesRequestObserver(BuddiesActivity buddiesActivity, BuddiesRequestObserver buddiesRequestObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            BuddiesActivity.this.showDialogSafe(0);
            BuddiesActivity.this.setProgressIndicator(false);
            BuddiesActivity.this.blockUI(false);
            BuddiesActivity.this.adapter.clear();
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            BuddiesActivity.this.updateStatusBar();
            BuddiesActivity.this.adapter.clear();
            List<User> buddies = Session.getCurrentSession().getUser().getBuddyUsers();
            if (!buddies.isEmpty()) {
                for (User buddy : buddies) {
                    BuddiesActivity.this.adapter.add(new ListItem(buddy));
                }
            }
            BuddiesActivity.this.blockUI(false);
            BuddiesActivity.this.setProgressIndicator(false);
        }
    }

    private class ListItemAdapter extends BaseActivity.GenericListItemAdapter {
        public ListItemAdapter(Context context, int resource, List<ListItem> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View convertView2 = init(position, convertView);
            this.text0.setVisibility(8);
            if (this.listItem.isSpecialItem()) {
                this.text1.setText(this.listItem.getLabel());
            } else {
                this.text1.setText(this.listItem.getUser().getLogin());
            }
            this.text2.setVisibility(8);
            return convertView2;
        }
    }

    /* access modifiers changed from: private */
    public void blockUI(boolean flg) {
        this.addButton.setEnabled(!flg);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_buddies);
        this.usersListView = (ListView) findViewById(R.id.list_view);
        this.usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                ListItem listItem = (ListItem) adapter.getItemAtPosition(position);
                if (!listItem.isSpecialItem()) {
                    ScoreloopManager.setUser(listItem.getUser());
                    BuddiesActivity.this.startActivity(new Intent(BuddiesActivity.this, UserActivity.class));
                }
            }
        });
        this.addButton = (LinearLayout) findViewById(R.id.button_add);
        this.addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((ProfileActivity) BuddiesActivity.this.getParent()).setSubContent(2);
            }
        });
        ((ImageView) findViewById(R.id.image)).setImageDrawable(getResources().getDrawable(R.drawable.sl_user_add));
        ((TextView) findViewById(R.id.text0)).setVisibility(8);
        ((TextView) findViewById(R.id.text1)).setText((int) R.string.sl_buddies_add);
        ((TextView) findViewById(R.id.text2)).setVisibility(8);
        this.adapter = new ListItemAdapter(this, R.layout.sl_buddies, new ArrayList());
        this.usersListView.setAdapter((ListAdapter) this.adapter);
        this.buddiesRequestController = new UserController(new BuddiesRequestObserver(this, null));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        blockUI(true);
        setProgressIndicator(true);
        this.adapter.clear();
        this.adapter.add(new ListItem(getResources().getString(R.string.sl_loading)));
        this.buddiesRequestController.loadBuddies();
    }
}
