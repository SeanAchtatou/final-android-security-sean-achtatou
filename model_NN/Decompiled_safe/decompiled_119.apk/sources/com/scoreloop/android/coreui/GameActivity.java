package com.scoreloop.android.coreui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.android.coreui.BaseActivity;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UsersController;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import java.util.ArrayList;
import java.util.List;

public class GameActivity extends BaseActivity {
    private static final int SEARCH_LIMIT = 100;
    /* access modifiers changed from: private */
    public ListItemAdapter adapter;
    private LinearLayout detailsLayout;
    /* access modifiers changed from: private */
    public Game game;
    /* access modifiers changed from: private */
    public ImageView iconImage;
    private ListView listView;
    /* access modifiers changed from: private */
    public UsersController usersController;

    private class ListItemAdapter extends BaseActivity.GenericListItemAdapter {
        public ListItemAdapter(Context context, int resource, List<ListItem> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View convertView2 = init(position, convertView);
            this.text0.setVisibility(8);
            this.text2.setVisibility(8);
            if (this.listItem.isSpecialItem()) {
                this.text1.setText(this.listItem.getLabel());
                this.image.setVisibility(8);
            } else {
                this.text1.setText(this.listItem.getUser().getLogin());
                this.image.setVisibility(0);
            }
            return convertView2;
        }
    }

    private class UsersControllerObserver implements RequestControllerObserver {
        private UsersControllerObserver() {
        }

        /* synthetic */ UsersControllerObserver(GameActivity gameActivity, UsersControllerObserver usersControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                GameActivity.this.showDialogSafe(0);
                GameActivity.this.setProgressIndicator(false);
                GameActivity.this.adapter.clear();
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            GameActivity.this.updateStatusBar();
            GameActivity.this.adapter.clear();
            if (!GameActivity.this.usersController.isOverLimit()) {
                List<User> users = GameActivity.this.usersController.getUsers();
                if (!users.isEmpty()) {
                    for (User user : users) {
                        GameActivity.this.adapter.add(new ListItem(user));
                    }
                } else {
                    GameActivity.this.adapter.add(new ListItem(GameActivity.this.getResources().getString(R.string.sl_no_results_found)));
                }
                GameActivity.this.setProgressIndicator(false);
            } else if (GameActivity.this.usersController.isMaxUserCount()) {
                GameActivity.this.showToast(GameActivity.this.getResources().getString(R.string.sl_found_too_many_users));
            } else {
                GameActivity.this.showToast(String.format(GameActivity.this.getResources().getString(R.string.sl_found_many_users_format), Integer.valueOf(GameActivity.this.usersController.getCountOfUsers())));
            }
        }
    }

    /* access modifiers changed from: private */
    public void switchToTab(int tab) {
        if (tab == 0) {
            this.detailsLayout.setVisibility(0);
            this.listView.setVisibility(8);
            return;
        }
        this.detailsLayout.setVisibility(8);
        this.listView.setVisibility(0);
        if (this.adapter.isEmpty()) {
            setProgressIndicator(true);
            this.adapter.add(new ListItem(getResources().getString(R.string.sl_loading)));
            this.usersController.loadBuddiesForGame(this.game);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_game);
        updateStatusBar();
        updateHeading(getString(R.string.sl_games), false);
        this.game = ScoreloopManager.getGame();
        TextView nameText = (TextView) findViewById(R.id.name_text);
        nameText.setText(this.game.getName());
        nameText.setTypeface(Typeface.DEFAULT, 1);
        ((TextView) findViewById(R.id.publisher_text)).setText(this.game.getPublisherName());
        this.iconImage = (ImageView) findViewById(R.id.image_view);
        this.iconImage.setImageDrawable(getDrawable(this.game.getImageUrl()));
        this.detailsLayout = (LinearLayout) findViewById(R.id.details_layout);
        TextView descriptionText = (TextView) findViewById(R.id.description_text);
        String description = this.game.getDescription();
        if (!"".equals(description)) {
            descriptionText.setText(description);
        }
        ((Button) findViewById(R.id.get_game_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GameActivity.this.game.getDownloadUrl())));
            }
        });
        this.listView = (ListView) findViewById(R.id.list_view);
        final SegmentedView segmentedView = (SegmentedView) findViewById(R.id.segments);
        segmentedView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GameActivity.this.switchToTab(segmentedView.getSelectedSegment());
            }
        });
        this.adapter = new ListItemAdapter(this, R.layout.sl_game, new ArrayList());
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.usersController = new UsersController(new UsersControllerObserver(this, null));
        this.usersController.setSearchLimit(SEARCH_LIMIT);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setNotify(new Runnable() {
            public void run() {
                GameActivity.this.iconImage.setImageDrawable(GameActivity.this.getDrawable(GameActivity.this.game.getImageUrl()));
            }
        });
    }
}
