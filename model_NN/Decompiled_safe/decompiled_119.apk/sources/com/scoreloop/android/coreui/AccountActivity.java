package com.scoreloop.android.coreui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.controller.UserControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;

public class AccountActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public EditText emailEdit;
    /* access modifiers changed from: private */
    public EditText loginEdit;
    private String oldEmail;
    private String oldLogin;
    private Button updateButton;
    /* access modifiers changed from: private */
    public UserController userController;

    private class UserUpdateObserver implements UserControllerObserver {
        private UserUpdateObserver() {
        }

        /* synthetic */ UserUpdateObserver(AccountActivity accountActivity, UserUpdateObserver userUpdateObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            AccountActivity.this.onErrorUpdateUIAndResetData(0, false);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            AccountActivity.this.setProgressIndicator(false);
            AccountActivity.this.blockUI(false);
            AccountActivity.this.setUIToSessionUser();
        }

        public void userControllerDidFailOnEmailAlreadyTaken(UserController controller) {
            AccountActivity.this.onErrorUpdateUIAndResetData(1, true);
        }

        public void userControllerDidFailOnInvalidEmailFormat(UserController controller) {
            AccountActivity.this.onErrorUpdateUIAndResetData(2, true);
        }

        public void userControllerDidFailOnUsernameAlreadyTaken(UserController controller) {
            AccountActivity.this.onErrorUpdateUIAndResetData(3, true);
        }
    }

    /* access modifiers changed from: private */
    public void blockUI(boolean flg) {
        boolean z;
        boolean z2;
        boolean z3;
        Button button = this.updateButton;
        if (flg) {
            z = false;
        } else {
            z = true;
        }
        button.setEnabled(z);
        EditText editText = this.loginEdit;
        if (flg) {
            z2 = false;
        } else {
            z2 = true;
        }
        editText.setEnabled(z2);
        EditText editText2 = this.emailEdit;
        if (flg) {
            z3 = false;
        } else {
            z3 = true;
        }
        editText2.setEnabled(z3);
    }

    /* access modifiers changed from: private */
    public void onErrorUpdateUIAndResetData(int error, boolean enableUI) {
        setProgressIndicator(false);
        if (enableUI) {
            blockUI(false);
        }
        showDialogSafe(error);
        setSessionUserToOldData();
        setUIToSessionUser();
    }

    /* access modifiers changed from: private */
    public void setOldDataToSessionUser() {
        User user = Session.getCurrentSession().getUser();
        this.oldLogin = user.getLogin();
        this.oldEmail = user.getEmailAddress();
    }

    private void setSessionUserToOldData() {
        User user = Session.getCurrentSession().getUser();
        user.setLogin(this.oldLogin);
        user.setEmailAddress(this.oldEmail);
    }

    /* access modifiers changed from: private */
    public void setUIToSessionUser() {
        User user = Session.getCurrentSession().getUser();
        this.loginEdit.setText(user.getLogin());
        this.emailEdit.setText(user.getEmailAddress());
        updateStatusBar();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_account);
        this.userController = new UserController(new UserUpdateObserver(this, null));
        updateStatusBar();
        this.loginEdit = (EditText) findViewById(R.id.login_edit);
        this.emailEdit = (EditText) findViewById(R.id.email_edit);
        this.updateButton = (Button) findViewById(R.id.update_button);
        this.updateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AccountActivity.this.setOldDataToSessionUser();
                String login = AccountActivity.this.loginEdit.getText().toString().trim();
                String email = AccountActivity.this.emailEdit.getText().toString().trim();
                AccountActivity.this.loginEdit.setText(login);
                AccountActivity.this.emailEdit.setText(email);
                User user = Session.getCurrentSession().getUser();
                user.setLogin(login);
                user.setEmailAddress(email);
                AccountActivity.this.blockUI(true);
                AccountActivity.this.setProgressIndicator(true);
                AccountActivity.this.userController.submitUser();
            }
        });
        ((ImageButton) findViewById(R.id.slapp_download_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String downloadUrl = Session.getCurrentSession().getScoreloopAppDownloadUrl();
                if (downloadUrl != null) {
                    AccountActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(downloadUrl)));
                }
            }
        });
        setOldDataToSessionUser();
        blockUI(true);
        setProgressIndicator(true);
        this.userController.loadUser();
    }
}
