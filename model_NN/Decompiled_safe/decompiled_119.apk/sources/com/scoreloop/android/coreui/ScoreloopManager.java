package com.scoreloop.android.coreui;

import android.content.Context;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoreController;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Range;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.User;

public abstract class ScoreloopManager {
    static final int GAME_MODE_MIN = 0;
    static Client client;
    private static Game game;
    private static ScoreController scoreController;
    private static User user;

    public static Score getScore() {
        return scoreController.getScore();
    }

    public static void init(Context context, String gameID, String gameSecret) {
        if (client == null) {
            client = new Client(context, gameID, gameSecret, null);
        }
    }

    public static void setNumberOfModes(int modeCount) {
        if (client != null) {
            client.setGameModes(new Range(0, modeCount));
            return;
        }
        throw new IllegalStateException("client object is null. has ScoreloopManager.init() been called?");
    }

    public static void submitScore(int scoreValue, int gameMode, RequestControllerObserver observer) {
        Score score = new Score(Double.valueOf((double) scoreValue), null);
        score.setMode(Integer.valueOf(gameMode));
        scoreController = new ScoreController(observer);
        scoreController.submitScore(score);
    }

    public static void submitScore(int scoreValue, RequestControllerObserver observer) {
        submitScore(scoreValue, 0, observer);
    }

    static Game getGame() {
        return game;
    }

    static User getUser() {
        return user;
    }

    static void setGame(Game game2) {
        game = game2;
    }

    static void setUser(User user2) {
        user = user2;
    }
}
