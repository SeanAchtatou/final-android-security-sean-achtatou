package com.scoreloop.android.coreui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.android.coreui.BaseActivity;
import com.scoreloop.client.android.core.controller.GamesController;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Game;
import java.util.ArrayList;
import java.util.List;

public class GamesActivity extends BaseActivity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$android$coreui$GamesActivity$Tab = null;
    private static final int NR_OF_TABS = 3;
    private static final int RANGE_LENGTH_FEATURED = 1;
    private static final int RANGE_LENGTH_OTHER = 10;
    /* access modifiers changed from: private */
    public final ListItemAdapter[] adapter = new ListItemAdapter[3];
    /* access modifiers changed from: private */
    public Game featuredGame;
    /* access modifiers changed from: private */
    public FrameLayout featuredLayout;
    /* access modifiers changed from: private */
    public final GamesController[] gamesController = new GamesController[3];
    /* access modifiers changed from: private */
    public GamesController gamesFeaturedController;
    private ListView gamesListView;
    /* access modifiers changed from: private */
    public ImageView iconImage;
    /* access modifiers changed from: private */
    public FrameLayout subheadingLayout;
    private Tab tab;

    private enum Tab {
        POPULAR,
        NEW,
        FRIENDS
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$android$coreui$GamesActivity$Tab() {
        int[] iArr = $SWITCH_TABLE$com$scoreloop$android$coreui$GamesActivity$Tab;
        if (iArr == null) {
            iArr = new int[Tab.values().length];
            try {
                iArr[Tab.FRIENDS.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Tab.NEW.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Tab.POPULAR.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$scoreloop$android$coreui$GamesActivity$Tab = iArr;
        }
        return iArr;
    }

    private class GamesControllerObserver implements RequestControllerObserver {
        private int target;

        private GamesControllerObserver() {
        }

        /* synthetic */ GamesControllerObserver(GamesActivity gamesActivity, GamesControllerObserver gamesControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                GamesActivity.this.showDialogSafe(0);
                GamesActivity.this.setProgressIndicator(false);
                GamesActivity.this.adapter[this.target].clear();
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            GamesActivity.this.updateStatusBar();
            GamesActivity.this.adapter[this.target].clear();
            List<Game> games = GamesActivity.this.gamesController[this.target].getGames();
            if (!games.isEmpty()) {
                for (Game game : games) {
                    GamesActivity.this.adapter[this.target].add(new ListItem(game));
                }
            } else {
                GamesActivity.this.adapter[this.target].add(new ListItem(GamesActivity.this.getResources().getString(R.string.sl_no_results_found)));
            }
            GamesActivity.this.setProgressIndicator(false);
        }

        /* access modifiers changed from: private */
        public void setTarget(int target2) {
            this.target = target2;
        }
    }

    private class GamesFeaturedControllerObserver implements RequestControllerObserver {
        private GamesFeaturedControllerObserver() {
        }

        /* synthetic */ GamesFeaturedControllerObserver(GamesActivity gamesActivity, GamesFeaturedControllerObserver gamesFeaturedControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                GamesActivity.this.subheadingLayout.setVisibility(8);
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            GamesActivity.this.updateStatusBar();
            List<Game> games = GamesActivity.this.gamesFeaturedController.getGames();
            if (!games.isEmpty()) {
                ((TextView) GamesActivity.this.findViewById(R.id.loading_text)).setVisibility(8);
                GamesActivity.this.featuredLayout.setVisibility(0);
                GamesActivity.this.featuredGame = games.get(0);
                ((TextView) GamesActivity.this.findViewById(R.id.name_text)).setText(GamesActivity.this.featuredGame.getName());
                ((TextView) GamesActivity.this.findViewById(R.id.publisher_text)).setText(GamesActivity.this.featuredGame.getPublisherName());
                ((ImageView) GamesActivity.this.findViewById(R.id.image_view)).setImageDrawable(GamesActivity.this.getDrawable(GamesActivity.this.featuredGame.getImageUrl()));
                return;
            }
            GamesActivity.this.subheadingLayout.setVisibility(8);
        }
    }

    private class ListItemAdapter extends BaseActivity.GenericListItemAdapter {
        public ListItemAdapter(Context context, int resource, List<ListItem> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View convertView2 = init(position, convertView);
            if (this.listItem.isSpecialItem()) {
                this.text0.setText("");
                this.text1.setText(this.listItem.getLabel());
                this.text2.setVisibility(8);
                this.image.setVisibility(8);
            } else {
                this.text0.setVisibility(8);
                this.text1.setText(this.listItem.getGame().getName());
                this.text1.setTypeface(Typeface.DEFAULT, 1);
                this.text2.setText(this.listItem.getGame().getPublisherName());
                this.text2.setVisibility(0);
                this.image.setVisibility(0);
                this.image.setImageDrawable(GamesActivity.this.getDrawable(this.listItem.getGame().getImageUrl()));
            }
            return convertView2;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, (int) R.string.sl_profile).setIcon((int) R.drawable.sl_menu_profile);
        menu.add(0, 0, 0, (int) R.string.sl_highscores).setIcon((int) R.drawable.sl_menu_highscores);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                startActivity(new Intent(this, HighscoresActivity.class).setFlags(67108864));
                finish();
                return true;
            case 1:
                startActivity(new Intent(this, ProfileActivity.class).setFlags(67108864));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: private */
    public void switchToTab(Tab newTab) {
        this.tab = newTab;
        this.gamesListView.setAdapter((ListAdapter) this.adapter[this.tab.ordinal()]);
        if (this.adapter[this.tab.ordinal()].isEmpty()) {
            setProgressIndicator(true);
            this.adapter[this.tab.ordinal()].add(new ListItem(getResources().getString(R.string.sl_loading)));
            switch ($SWITCH_TABLE$com$scoreloop$android$coreui$GamesActivity$Tab()[this.tab.ordinal()]) {
                case 1:
                    this.gamesController[this.tab.ordinal()].loadRangeForPopular();
                    return;
                case 2:
                    this.gamesController[this.tab.ordinal()].loadRangeForNew();
                    return;
                case 3:
                    this.gamesController[this.tab.ordinal()].loadRangeForBuddies();
                    return;
                default:
                    this.gamesController[0].loadRangeForPopular();
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_games);
        updateStatusBar();
        updateHeading(getString(R.string.sl_games), false);
        this.subheadingLayout = (FrameLayout) findViewById(R.id.subheading_layout);
        this.featuredLayout = (FrameLayout) findViewById(R.id.featured_layout);
        this.featuredLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScoreloopManager.setGame(GamesActivity.this.featuredGame);
                GamesActivity.this.startActivity(new Intent(GamesActivity.this, GameActivity.class));
            }
        });
        this.iconImage = (ImageView) findViewById(R.id.image_view);
        this.gamesListView = (ListView) findViewById(R.id.list_view);
        this.gamesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                ListItem listItem = (ListItem) adapter.getItemAtPosition(position);
                if (!listItem.isSpecialItem()) {
                    ScoreloopManager.setGame(listItem.getGame());
                    GamesActivity.this.startActivity(new Intent(GamesActivity.this, GameActivity.class));
                }
            }
        });
        final SegmentedView segmentedView = (SegmentedView) findViewById(R.id.segments);
        segmentedView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GamesActivity.this.switchToTab(Tab.values()[segmentedView.getSelectedSegment()]);
            }
        });
        this.tab = Tab.POPULAR;
        for (int i = 0; i < 3; i++) {
            GamesControllerObserver observer = new GamesControllerObserver(this, null);
            observer.setTarget(i);
            this.gamesController[i] = new GamesController(observer);
            this.gamesController[i].setRangeLength(10);
            this.adapter[i] = new ListItemAdapter(this, R.layout.sl_games, new ArrayList());
        }
        this.gamesFeaturedController = new GamesController(new GamesFeaturedControllerObserver(this, null));
        this.gamesFeaturedController.setRangeLength(1);
        this.gamesFeaturedController.loadRangeForFeatured();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setNotify(new Runnable() {
            public void run() {
                if (GamesActivity.this.featuredGame != null) {
                    GamesActivity.this.iconImage.setImageDrawable(GamesActivity.this.getDrawable(GamesActivity.this.featuredGame.getImageUrl()));
                }
                for (int i = 0; i < 3; i++) {
                    GamesActivity.this.adapter[i].notifyDataSetChanged();
                }
            }
        });
        if (this.featuredGame != null) {
            this.iconImage.setImageDrawable(getDrawable(this.featuredGame.getImageUrl()));
        }
        switchToTab(this.tab);
    }
}
