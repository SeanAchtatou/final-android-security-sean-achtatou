package com.scoreloop.android.coreui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.android.coreui.BaseActivity;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoresController;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HighscoresActivity extends BaseActivity {
    private static final int DIALOG_GAME_MODE = 1000;
    private static final int FIXED_OFFSET = 1;
    private static final int RANGE_LENGTH = 20;
    private static final int RANK_TOP = 1;
    private static final int TAG_NEXT = 2;
    private static final int TAG_PREV = 1;
    private static final int TAG_TOP = 0;
    /* access modifiers changed from: private */
    public static final SearchList[] searchList = {SearchList.getGlobalScoreSearchList(), SearchList.getTwentyFourHourScoreSearchList(), SearchList.getBuddiesScoreSearchList()};
    /* access modifiers changed from: private */
    public ListItemAdapter adapter;
    /* access modifiers changed from: private */
    public View dividerView;
    /* access modifiers changed from: private */
    public LoadingType loadingType;
    /* access modifiers changed from: private */
    public TextView modeText;
    /* access modifiers changed from: private */
    public LinearLayout myScoreView;
    /* access modifiers changed from: private */
    public ScoresController scoresController;
    /* access modifiers changed from: private */
    public ListView scoresListView;

    enum LoadingType {
        ME,
        OTHER
    }

    private class ListItemAdapter extends BaseActivity.GenericListItemAdapter {
        public ListItemAdapter(Context context, int resource, List<ListItem> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View convertView2 = init(position, convertView);
            if (this.listItem.isSpecialItem()) {
                this.image.setVisibility(8);
                this.text0.setText("");
                this.text2.setVisibility(8);
                this.text1.setText(this.listItem.getLabel());
            } else {
                this.image.setVisibility(0);
                this.text0.setText(this.listItem.getScore().getRank() + ".");
                this.text1.setText(this.listItem.getScore().getUser().getLogin());
                this.text2.setVisibility(0);
                this.text2.setText(new StringBuilder().append(this.listItem.getScore().getResult().intValue()).toString());
                this.text2.setTypeface(Typeface.DEFAULT, 1);
            }
            if (!this.listItem.isSpecialItem()) {
                HighscoresActivity.this.highlightView(convertView2, this.listItem.getScore().getUser().equals(Session.getCurrentSession().getUser()));
            } else {
                HighscoresActivity.this.highlightView(convertView2, false);
            }
            return convertView2;
        }
    }

    private class ScoresControllerObserver implements RequestControllerObserver {
        private ScoresControllerObserver() {
        }

        /* synthetic */ ScoresControllerObserver(HighscoresActivity highscoresActivity, ScoresControllerObserver scoresControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                HighscoresActivity.this.showDialogSafe(0);
                HighscoresActivity.this.setProgressIndicator(false);
                HighscoresActivity.this.blockUI(false);
                HighscoresActivity.this.adapter.clear();
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            HighscoresActivity.this.updateStatusBar();
            HighscoresActivity.this.adapter.clear();
            List<Score> scores = HighscoresActivity.this.scoresController.getScores();
            if (!scores.isEmpty()) {
                for (Score score : scores) {
                    HighscoresActivity.this.adapter.add(new ListItem(score));
                }
            } else {
                HighscoresActivity.this.adapter.add(new ListItem(HighscoresActivity.this.getResources().getString(R.string.sl_no_results_found)));
            }
            if (HighscoresActivity.this.scoresController.hasPreviousRange()) {
                HighscoresActivity.this.adapter.insert(new ListItem(HighscoresActivity.this.getResources().getString(R.string.sl_prev), 1), 0);
                HighscoresActivity.this.adapter.insert(new ListItem(HighscoresActivity.this.getResources().getString(R.string.sl_top), 0), 0);
            }
            if (HighscoresActivity.this.scoresController.hasNextRange()) {
                HighscoresActivity.this.adapter.add(new ListItem(HighscoresActivity.this.getResources().getString(R.string.sl_next), 2));
            }
            Score myScore = null;
            User currentUser = Session.getCurrentSession().getUser();
            int idx = 0;
            Iterator<Score> it = scores.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Score score2 = it.next();
                if (score2 != null && score2.getUser().equals(currentUser)) {
                    myScore = score2;
                    break;
                }
                idx++;
            }
            if (HighscoresActivity.this.loadingType == LoadingType.ME) {
                if (myScore != null) {
                    HighscoresActivity.this.scoresListView.setSelection(idx < 1 ? 0 : idx - 1);
                } else {
                    HighscoresActivity.this.showToast(HighscoresActivity.this.getResources().getString(R.string.sl_not_on_highscore_list));
                }
            }
            if (myScore != null) {
                ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.text0)).setText(myScore.getRank() + ".");
                ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.text1)).setText(myScore.getUser().getLogin());
                ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.text2)).setText(new StringBuilder().append(myScore.getResult().intValue()).toString());
                ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.text2)).setTypeface(Typeface.DEFAULT, 1);
                HighscoresActivity.this.myScoreView.setVisibility(8);
                HighscoresActivity.this.dividerView.setVisibility(8);
            } else if (((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.text0)).getText().toString().equals("")) {
                HighscoresActivity.this.myScoreView.setVisibility(8);
                HighscoresActivity.this.dividerView.setVisibility(8);
            } else {
                HighscoresActivity.this.myScoreView.setVisibility(0);
                HighscoresActivity.this.dividerView.setVisibility(0);
            }
            HighscoresActivity.this.blockUI(false);
            HighscoresActivity.this.setProgressIndicator(false);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, (int) R.string.sl_profile).setIcon((int) R.drawable.sl_menu_profile);
        menu.add(0, 2, 0, (int) R.string.sl_games).setIcon((int) R.drawable.sl_menu_games);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                startActivity(new Intent(this, ProfileActivity.class).setFlags(67108864));
                finish();
                return true;
            case 2:
                startActivity(new Intent(this, GamesActivity.class).setFlags(67108864));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: private */
    public void blockUI(boolean flg) {
        boolean z;
        boolean z2;
        ListView listView = this.scoresListView;
        if (flg) {
            z = false;
        } else {
            z = true;
        }
        listView.setEnabled(z);
        LinearLayout linearLayout = this.myScoreView;
        if (flg) {
            z2 = false;
        } else {
            z2 = true;
        }
        linearLayout.setEnabled(z2);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: private */
    public void highlightView(View view, boolean flg) {
        int c = flg ? getResources().getColor(R.color.sl_color_sl) : getResources().getColor(R.color.sl_color_foreground);
        ((TextView) view.findViewById(R.id.text0)).setTextColor(c);
        ((TextView) view.findViewById(R.id.text1)).setTextColor(c);
        ((TextView) view.findViewById(R.id.text2)).setTextColor(c);
    }

    /* access modifiers changed from: private */
    public void loadRangeForMe() {
        this.loadingType = LoadingType.ME;
        blockUI(true);
        setProgressIndicator(true);
        this.adapter.clear();
        this.adapter.add(new ListItem(getResources().getString(R.string.sl_loading)));
        this.scoresController.loadRangeForUser(Session.getCurrentSession().getUser());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_highscores);
        updateStatusBar();
        updateHeading(getString(R.string.sl_highscores), false);
        this.dividerView = findViewById(R.id.divider);
        this.dividerView.setVisibility(8);
        this.myScoreView = (LinearLayout) findViewById(R.id.myscore_view);
        this.myScoreView.setVisibility(8);
        ((TextView) this.myScoreView.findViewById(R.id.text0)).setText("");
        highlightView(this.myScoreView, true);
        this.myScoreView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighscoresActivity.this.loadRangeForMe();
            }
        });
        this.scoresListView = (ListView) findViewById(R.id.list_view);
        this.scoresListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ListItem listItem = (ListItem) adapterView.getItemAtPosition(position);
                if (listItem.isSpecialItem()) {
                    HighscoresActivity.this.loadingType = LoadingType.OTHER;
                    HighscoresActivity.this.blockUI(true);
                    HighscoresActivity.this.setProgressIndicator(true);
                    HighscoresActivity.this.adapter.clear();
                    HighscoresActivity.this.adapter.add(new ListItem(HighscoresActivity.this.getResources().getString(R.string.sl_loading)));
                    switch (listItem.getTag()) {
                        case 0:
                            HighscoresActivity.this.scoresController.loadRangeAtRank(1);
                            return;
                        case 1:
                            HighscoresActivity.this.scoresController.loadPreviousRange();
                            return;
                        case 2:
                            HighscoresActivity.this.scoresController.loadNextRange();
                            return;
                        default:
                            return;
                    }
                } else {
                    User user = listItem.getScore().getUser();
                    if (!user.equals(Session.getCurrentSession().getUser())) {
                        ScoreloopManager.setUser(user);
                        HighscoresActivity.this.startActivity(new Intent(HighscoresActivity.this, UserActivity.class));
                    }
                }
            }
        });
        FrameLayout modeLayout = (FrameLayout) findViewById(R.id.layout_mode);
        if (ScoreloopManager.client.getGameModes().getLength() > 1) {
            modeLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HighscoresActivity.this.showDialogSafe(HighscoresActivity.DIALOG_GAME_MODE);
                }
            });
        } else {
            modeLayout.setVisibility(8);
        }
        this.modeText = (TextView) findViewById(R.id.mode_text);
        this.modeText.setText(getResources().getStringArray(R.array.sl_game_modes)[0].toString());
        final SegmentedView segmentedView = (SegmentedView) findViewById(R.id.search_list_segments);
        segmentedView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.text0)).setText("");
                HighscoresActivity.this.scoresController.setSearchList(HighscoresActivity.searchList[segmentedView.getSelectedSegment()]);
                HighscoresActivity.this.loadRangeForMe();
            }
        });
        this.adapter = new ListItemAdapter(this, R.layout.sl_highscores, new ArrayList());
        this.scoresListView.setAdapter((ListAdapter) this.adapter);
        this.scoresController = new ScoresController(new ScoresControllerObserver(this, null));
        this.scoresController.setRangeLength(RANGE_LENGTH);
        loadRangeForMe();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = super.onCreateDialog(id);
        if (dialog != null) {
            return dialog;
        }
        switch (id) {
            case DIALOG_GAME_MODE /*1000*/:
                return new AlertDialog.Builder(this).setItems((int) R.array.sl_game_modes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        HighscoresActivity.this.modeText.setText(HighscoresActivity.this.getResources().getStringArray(R.array.sl_game_modes)[position].toString());
                        HighscoresActivity.this.scoresController.setMode(Integer.valueOf(position));
                        ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.text0)).setText("");
                        HighscoresActivity.this.loadRangeForMe();
                    }
                }).create();
            default:
                return null;
        }
    }
}
