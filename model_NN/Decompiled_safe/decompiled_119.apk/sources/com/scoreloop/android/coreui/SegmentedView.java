package com.scoreloop.android.coreui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

class SegmentedView extends LinearLayout {
    private int selectedSegment;

    public SegmentedView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: private */
    public void onInnerClick(View view) {
        for (int i = 0; i < getChildCount(); i++) {
            if (getChildAt(i) == view) {
                switchToSegment(i);
                performClick();
                return;
            }
        }
    }

    private void switchToSegment(int segment) {
        this.selectedSegment = segment;
        int i = 0;
        while (i < getChildCount()) {
            getChildAt(i).setEnabled(i != this.selectedSegment);
            i++;
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    SegmentedView.this.onInnerClick(view);
                }
            });
        }
        if (getChildCount() != 0) {
            switchToSegment(0);
        }
    }

    /* access modifiers changed from: package-private */
    public int getSelectedSegment() {
        return this.selectedSegment;
    }
}
