package com.scoreloop.android.coreui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.client.android.core.controller.MessageController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import java.util.HashMap;
import java.util.Map;

public class PostScoreActivity extends BaseActivity {
    private CheckBox facebookCheckbox;
    /* access modifiers changed from: private */
    public Map<SocialProvider, CheckBox> map;
    /* access modifiers changed from: private */
    public final RequestControllerObserver messageControllerObserver = new RequestControllerObserver() {
        public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
            PostScoreActivity.this.showDialogSafe(0);
            PostScoreActivity.this.finish();
        }

        public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            PostScoreActivity.this.showToast(PostScoreActivity.this.getResources().getString(R.string.sl_post_success));
            PostScoreActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public EditText messageEditText;
    private CheckBox myspaceCheckbox;
    private Button noButton;
    private Button postButton;
    private CheckBox twitterCheckbox;

    private void blockUI(boolean flg) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        this.facebookCheckbox.setEnabled(!flg);
        CheckBox checkBox = this.myspaceCheckbox;
        if (flg) {
            z = false;
        } else {
            z = true;
        }
        checkBox.setEnabled(z);
        CheckBox checkBox2 = this.twitterCheckbox;
        if (flg) {
            z2 = false;
        } else {
            z2 = true;
        }
        checkBox2.setEnabled(z2);
        Button button = this.postButton;
        if (flg) {
            z3 = false;
        } else {
            z3 = true;
        }
        button.setEnabled(z3);
        Button button2 = this.noButton;
        if (flg) {
            z4 = false;
        } else {
            z4 = true;
        }
        button2.setEnabled(z4);
    }

    /* access modifiers changed from: private */
    public void handleOnCheckedChanged(boolean isChecked, SocialProvider socialProvider) {
        if (isChecked) {
            blockUI(true);
            connectToSocialProvider(socialProvider);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_post_score);
        this.messageEditText = (EditText) findViewById(R.id.message_edittext);
        this.noButton = (Button) findViewById(R.id.cancel_button);
        this.noButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostScoreActivity.this.finish();
            }
        });
        this.postButton = (Button) findViewById(R.id.ok_button);
        this.postButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageController messageController = new MessageController(PostScoreActivity.this.messageControllerObserver);
                messageController.setTarget(ScoreloopManager.getScore());
                for (SocialProvider provider : PostScoreActivity.this.map.keySet()) {
                    if (((CheckBox) PostScoreActivity.this.map.get(provider)).isChecked()) {
                        messageController.addReceiverWithUsers(provider, null);
                    }
                }
                messageController.setText(PostScoreActivity.this.messageEditText.getText().toString());
                if (messageController.isSubmitAllowed()) {
                    messageController.submitMessage();
                }
            }
        });
        this.facebookCheckbox = (CheckBox) findViewById(R.id.facebook_checkbox);
        this.facebookCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PostScoreActivity.this.handleOnCheckedChanged(isChecked, PostScoreActivity.facebookProvider);
            }
        });
        this.myspaceCheckbox = (CheckBox) findViewById(R.id.myspace_checkbox);
        this.myspaceCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PostScoreActivity.this.handleOnCheckedChanged(isChecked, PostScoreActivity.myspaceProvider);
            }
        });
        this.twitterCheckbox = (CheckBox) findViewById(R.id.twitter_checkbox);
        this.twitterCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PostScoreActivity.this.handleOnCheckedChanged(isChecked, PostScoreActivity.twitterProvider);
            }
        });
        this.map = new HashMap();
        this.map.put(facebookProvider, this.facebookCheckbox);
        this.map.put(myspaceProvider, this.myspaceCheckbox);
        this.map.put(twitterProvider, this.twitterCheckbox);
    }

    /* access modifiers changed from: package-private */
    public void onSocialProviderConnectionStatusChange(SocialProvider socialProvider) {
        blockUI(false);
        if (!socialProvider.isUserConnected(Session.getCurrentSession().getUser())) {
            this.map.get(socialProvider).setChecked(false);
        }
    }
}
