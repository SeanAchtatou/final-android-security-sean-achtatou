package com.scoreloop.android.coreui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.android.coreui.BaseActivity;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.controller.UsersController;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import java.util.ArrayList;
import java.util.List;

public class BuddiesAddActivity extends BaseActivity {
    private static final int ADDRESSBOOK = 3;
    private static final int DIALOG_LOGIN = 1000;
    private static final int FACEBOOK = 0;
    private static final int LOGIN = 4;
    private static final int MYSPACE = 1;
    private static final int TWITTER = 2;
    /* access modifiers changed from: private */
    public static int[] imgRes = {R.drawable.sl_facebook, R.drawable.sl_myspace, R.drawable.sl_twitter, R.drawable.sl_addressbook, R.drawable.sl_login};
    /* access modifiers changed from: private */
    public int buddiesAdded;
    private UserController buddyAddController;
    /* access modifiers changed from: private */
    public List<User> usersFound;
    /* access modifiers changed from: private */
    public UsersController usersSearchController;

    private class BuddyAddObserver extends BaseActivity.UserGenericObserver {
        private BuddyAddObserver() {
            super();
        }

        /* synthetic */ BuddyAddObserver(BuddiesAddActivity buddiesAddActivity, BuddyAddObserver buddyAddObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            BuddiesAddActivity.this.showDialogSafe(0);
            BuddiesAddActivity.this.setProgressIndicator(false);
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public void requestControllerDidReceiveResponse(RequestController requestController) {
            BuddiesAddActivity buddiesAddActivity = BuddiesAddActivity.this;
            buddiesAddActivity.buddiesAdded = buddiesAddActivity.buddiesAdded + 1;
            if (!BuddiesAddActivity.this.addNextBuddy()) {
                BuddiesAddActivity.this.setProgressIndicator(false);
                BuddiesAddActivity.this.showToast(String.format(BuddiesAddActivity.this.getResources().getString(BuddiesAddActivity.this.buddiesAdded == 1 ? R.string.sl_buddies_added_one_format : R.string.sl_buddies_added_other_format), Integer.valueOf(BuddiesAddActivity.this.buddiesAdded)));
                ((ProfileActivity) BuddiesAddActivity.this.getParent()).onBackPressed();
            }
        }
    }

    private class ListItemAdapter extends BaseActivity.GenericListItemAdapter {
        public ListItemAdapter(Context context, int resource, List<ListItem> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View convertView2 = init(position, convertView);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.image.getLayoutParams();
            layoutParams.height = -2;
            layoutParams.width = -2;
            this.image.setImageDrawable(BuddiesAddActivity.this.getResources().getDrawable(BuddiesAddActivity.imgRes[position]));
            this.text0.setVisibility(8);
            this.text1.setText(this.listItem.getLabel());
            this.text2.setVisibility(8);
            return convertView2;
        }
    }

    private class LoginDialog extends Dialog implements View.OnClickListener {
        private EditText loginEdit;
        private Button okButton;

        LoginDialog(Context context) {
            super(context);
        }

        public void onClick(View v) {
            if (v.getId() == R.id.button_ok) {
                dismiss();
                BuddiesAddActivity.this.handleDialogClick(this.loginEdit.getText().toString());
            }
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView((int) R.layout.sl_dialog_login);
            setTitle(BuddiesAddActivity.this.getResources().getString(R.string.sl_sl_login));
            this.loginEdit = (EditText) findViewById(R.id.edit_login);
            this.okButton = (Button) findViewById(R.id.button_ok);
            this.okButton.setOnClickListener(this);
        }
    }

    private class UsersSearchObserver implements RequestControllerObserver {
        private UsersSearchObserver() {
        }

        /* synthetic */ UsersSearchObserver(BuddiesAddActivity buddiesAddActivity, UsersSearchObserver usersSearchObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            BuddiesAddActivity.this.showDialogSafe(0);
            BuddiesAddActivity.this.setProgressIndicator(false);
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public void requestControllerDidReceiveResponse(RequestController requestController) {
            BuddiesAddActivity.this.setProgressIndicator(false);
            if (!BuddiesAddActivity.this.usersSearchController.isOverLimit()) {
                BuddiesAddActivity.this.usersFound = new ArrayList(BuddiesAddActivity.this.usersSearchController.getUsers());
                if (BuddiesAddActivity.this.usersFound.isEmpty()) {
                    BuddiesAddActivity.this.showToast(BuddiesAddActivity.this.getResources().getString(R.string.sl_found_no_user));
                    ((ProfileActivity) BuddiesAddActivity.this.getParent()).onBackPressed();
                    return;
                }
                BuddiesAddActivity.this.setProgressIndicator(true);
                BuddiesAddActivity.this.buddiesAdded = 0;
                if (!BuddiesAddActivity.this.addNextBuddy()) {
                    BuddiesAddActivity.this.setProgressIndicator(false);
                    BuddiesAddActivity.this.showToast(BuddiesAddActivity.this.getResources().getString(R.string.sl_found_no_user));
                    ((ProfileActivity) BuddiesAddActivity.this.getParent()).onBackPressed();
                }
            } else if (BuddiesAddActivity.this.usersSearchController.isMaxUserCount()) {
                BuddiesAddActivity.this.showToast(BuddiesAddActivity.this.getResources().getString(R.string.sl_found_too_many_users));
                ((ProfileActivity) BuddiesAddActivity.this.getParent()).onBackPressed();
            } else {
                BuddiesAddActivity.this.showToast(String.format(BuddiesAddActivity.this.getResources().getString(R.string.sl_found_many_users_format), Integer.valueOf(BuddiesAddActivity.this.usersSearchController.getCountOfUsers())));
                ((ProfileActivity) BuddiesAddActivity.this.getParent()).onBackPressed();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0034 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean addNextBuddy() {
        /*
            r2 = this;
        L_0x0000:
            com.scoreloop.client.android.core.model.User r0 = r2.popUser()
            if (r0 == 0) goto L_0x0026
            com.scoreloop.client.android.core.model.Session r1 = com.scoreloop.client.android.core.model.Session.getCurrentSession()
            com.scoreloop.client.android.core.model.User r1 = r1.getUser()
            boolean r1 = r1.equals(r0)
            if (r1 != 0) goto L_0x0000
            com.scoreloop.client.android.core.model.Session r1 = com.scoreloop.client.android.core.model.Session.getCurrentSession()
            com.scoreloop.client.android.core.model.User r1 = r1.getUser()
            java.util.List r1 = r1.getBuddyUsers()
            boolean r1 = r1.contains(r0)
            if (r1 != 0) goto L_0x0000
        L_0x0026:
            if (r0 == 0) goto L_0x0034
            com.scoreloop.client.android.core.controller.UserController r1 = r2.buddyAddController
            r1.setUser(r0)
            com.scoreloop.client.android.core.controller.UserController r1 = r2.buddyAddController
            r1.addAsBuddy()
            r1 = 1
        L_0x0033:
            return r1
        L_0x0034:
            r1 = 0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.android.coreui.BuddiesAddActivity.addNextBuddy():boolean");
    }

    /* access modifiers changed from: private */
    public void handleDialogClick(String login) {
        setProgressIndicator(true);
        this.usersSearchController.setSearchOperator(UsersController.LoginSearchOperator.EXACT_MATCH);
        this.usersSearchController.searchByLogin(login);
    }

    /* access modifiers changed from: private */
    public void handleItemClick(int position) {
        switch (position) {
            case 0:
                handleSocialItemClick(facebookProvider);
                return;
            case 1:
                handleSocialItemClick(myspaceProvider);
                return;
            case 2:
                handleSocialItemClick(twitterProvider);
                return;
            case 3:
                setProgressIndicator(true);
                this.usersSearchController.searchByLocalAddressBook();
                return;
            case 4:
                showDialogSafe(DIALOG_LOGIN);
                return;
            default:
                return;
        }
    }

    private void handleSocialItemClick(SocialProvider socialProvider) {
        setProgressIndicator(true);
        connectToSocialProvider(socialProvider);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    private User popUser() {
        if (this.usersFound == null) {
            return null;
        }
        if (this.usersFound.isEmpty()) {
            return null;
        }
        return this.usersFound.remove(0);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_buddies_add);
        ListView listView = (ListView) findViewById(R.id.list_view);
        ListItemAdapter adapter = new ListItemAdapter(this, R.layout.sl_buddies_add, new ArrayList());
        listView.setAdapter((ListAdapter) adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                BuddiesAddActivity.this.handleItemClick(position);
            }
        });
        adapter.add(new ListItem(getResources().getString(R.string.sl_facebook)));
        adapter.add(new ListItem(getResources().getString(R.string.sl_myspace)));
        adapter.add(new ListItem(getResources().getString(R.string.sl_twitter)));
        adapter.add(new ListItem(getResources().getString(R.string.sl_addressbook)));
        adapter.add(new ListItem(getResources().getString(R.string.sl_sl_login)));
        this.usersSearchController = new UsersController(new UsersSearchObserver(this, null));
        this.usersSearchController.setSearchesGlobal(true);
        this.buddyAddController = new UserController(new BuddyAddObserver(this, null));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = super.onCreateDialog(id);
        if (dialog != null) {
            return dialog;
        }
        switch (id) {
            case DIALOG_LOGIN /*1000*/:
                return new LoginDialog(this);
            default:
                return null;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    /* access modifiers changed from: package-private */
    public void onSocialProviderConnectionStatusChange(SocialProvider socialProvider) {
        setProgressIndicator(false);
        if (socialProvider.isUserConnected(Session.getCurrentSession().getUser())) {
            setProgressIndicator(true);
            this.usersSearchController.searchBySocialProvider(socialProvider);
            return;
        }
        ((ProfileActivity) getParent()).onBackPressed();
    }
}
