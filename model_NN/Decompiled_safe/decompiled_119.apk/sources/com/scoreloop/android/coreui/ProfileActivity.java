package com.scoreloop.android.coreui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.rjblackbox.droid.reflex.lite.R;

public class ProfileActivity extends BaseActivity {
    private static final int CONTENT_ACCOUNT = 0;
    private static final int CONTENT_BUDDIES = 1;
    private static final int CONTENT_BUDDIES_ADD = 2;
    private int currentSubContent;

    public void onBackPressed() {
        if (this.currentSubContent == 2) {
            setSubContent(1);
        } else {
            finish();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, (int) R.string.sl_highscores).setIcon((int) R.drawable.sl_menu_highscores);
        menu.add(0, 2, 0, (int) R.string.sl_games).setIcon((int) R.drawable.sl_menu_games);
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        onBackPressed();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                startActivity(new Intent(this, HighscoresActivity.class).setFlags(67108864));
                finish();
                return true;
            case 1:
            default:
                return super.onOptionsItemSelected(item);
            case 2:
                startActivity(new Intent(this, GamesActivity.class).setFlags(67108864));
                finish();
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_profile);
        updateStatusBar();
        updateHeading(getString(R.string.sl_profile), true);
        final SegmentedView segmentedView = (SegmentedView) findViewById(R.id.segmented_view);
        segmentedView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ProfileActivity.this.setSubContent(segmentedView.getSelectedSegment());
            }
        });
        setSubContent(0);
    }

    /* access modifiers changed from: package-private */
    public void setSubContent(int index) {
        Class<? extends Activity> clazz;
        LinearLayout view = (LinearLayout) findViewById(R.id.activity_container);
        if (view.getChildCount() != 0) {
            view.removeViewAt(0);
        }
        switch (index) {
            case 0:
                clazz = AccountActivity.class;
                break;
            case 1:
                clazz = BuddiesActivity.class;
                break;
            case 2:
                clazz = BuddiesAddActivity.class;
                break;
            default:
                clazz = AccountActivity.class;
                break;
        }
        view.addView(getLocalActivityManager().startActivity(clazz.toString(), new Intent(this, clazz).addFlags(67108864)).getDecorView(), new ViewGroup.LayoutParams(-1, -1));
        this.currentSubContent = index;
    }
}
