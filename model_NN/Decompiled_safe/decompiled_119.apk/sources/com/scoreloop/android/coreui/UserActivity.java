package com.scoreloop.android.coreui;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.rjblackbox.droid.reflex.lite.R;
import com.scoreloop.android.coreui.BaseActivity;
import com.scoreloop.client.android.core.controller.GamesController;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserActivity extends BaseActivity {
    private static final int NR_OF_RECENT_GAMES = 3;
    /* access modifiers changed from: private */
    public ListItemAdapter adapter;
    /* access modifiers changed from: private */
    public Button buddyButton;
    /* access modifiers changed from: private */
    public boolean buddyButtonIsRemove;
    private LinearLayout detailsLayout;
    /* access modifiers changed from: private */
    public TextView friendsNumberText;
    /* access modifiers changed from: private */
    public GamesController gamesController;
    /* access modifiers changed from: private */
    public TextView lastActiveText;
    private ListView listView;
    /* access modifiers changed from: private */
    public final ImageView[] recentGameImage = new ImageView[3];
    /* access modifiers changed from: private */
    public List<Game> recentGames;
    /* access modifiers changed from: private */
    public User user;

    private class GamesControllerObserver implements RequestControllerObserver {
        private GamesControllerObserver() {
        }

        /* synthetic */ GamesControllerObserver(UserActivity userActivity, GamesControllerObserver gamesControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                UserActivity.this.showDialogSafe(0);
                UserActivity.this.adapter.clear();
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            UserActivity.this.updateStatusBar();
            UserActivity.this.adapter.clear();
            List<Game> games = UserActivity.this.gamesController.getGames();
            if (!games.isEmpty()) {
                for (Game game : games) {
                    UserActivity.this.adapter.add(new ListItem(game));
                }
                ((LinearLayout) UserActivity.this.findViewById(R.id.recent_games_layout)).setVisibility(0);
                List<Game> recentGamesCandidates = new ArrayList<>(UserActivity.this.gamesController.getGames());
                UserActivity.this.recentGames = new ArrayList();
                Random random = new Random();
                int j = Math.min(recentGamesCandidates.size(), 3);
                for (int i = 0; i < j; i++) {
                    Game recentGame = (Game) recentGamesCandidates.get(random.nextInt(recentGamesCandidates.size()));
                    recentGamesCandidates.remove(recentGame);
                    UserActivity.this.recentGames.add(recentGame);
                    UserActivity.this.recentGameImage[i].setImageDrawable(UserActivity.this.getDrawable(recentGame.getImageUrl()));
                }
                return;
            }
            UserActivity.this.adapter.add(new ListItem(UserActivity.this.getResources().getString(R.string.sl_no_results_found)));
        }
    }

    private class ListItemAdapter extends BaseActivity.GenericListItemAdapter {
        public ListItemAdapter(Context context, int resource, List<ListItem> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View convertView2 = init(position, convertView);
            if (this.listItem.isSpecialItem()) {
                this.text0.setText("");
                this.text1.setText(this.listItem.getLabel());
                this.text2.setVisibility(8);
                this.image.setVisibility(8);
            } else {
                this.text0.setVisibility(8);
                this.text1.setText(this.listItem.getGame().getName());
                this.text1.setTypeface(Typeface.DEFAULT, 1);
                this.text2.setText(this.listItem.getGame().getPublisherName());
                this.text2.setVisibility(0);
                this.image.setVisibility(0);
                this.image.setImageDrawable(UserActivity.this.getDrawable(this.listItem.getGame().getImageUrl()));
            }
            return convertView2;
        }
    }

    private class ManageBuddyObserver extends BaseActivity.UserGenericObserver {
        private ManageBuddyObserver() {
            super();
        }

        /* synthetic */ ManageBuddyObserver(UserActivity userActivity, ManageBuddyObserver manageBuddyObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                UserActivity.this.setProgressIndicator(false);
                UserActivity.this.showDialogSafe(0);
                UserActivity.this.finish();
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            UserActivity.this.setProgressIndicator(false);
            UserActivity.this.finish();
        }
    }

    private class MyBuddiesObserver extends BaseActivity.UserGenericObserver {
        private MyBuddiesObserver() {
            super();
        }

        /* synthetic */ MyBuddiesObserver(UserActivity userActivity, MyBuddiesObserver myBuddiesObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                UserActivity.this.showDialogSafe(0);
                UserActivity.this.buddyButton.setText(UserActivity.this.getString(R.string.sl_empty));
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            UserActivity.this.updateStatusBar();
            UserActivity.this.buddyButton.setEnabled(true);
            if (Session.getCurrentSession().getUser().getBuddyUsers().contains(UserActivity.this.user)) {
                UserActivity.this.buddyButtonIsRemove = true;
                UserActivity.this.buddyButton.setText(UserActivity.this.getString(R.string.sl_remove_friend));
                return;
            }
            UserActivity.this.buddyButton.setText(UserActivity.this.getString(R.string.sl_add_friend));
        }
    }

    private class UserBuddiesObserver extends BaseActivity.UserGenericObserver {
        private UserBuddiesObserver() {
            super();
        }

        /* synthetic */ UserBuddiesObserver(UserActivity userActivity, UserBuddiesObserver userBuddiesObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                UserActivity.this.showDialogSafe(0);
                UserActivity.this.friendsNumberText.setText(String.format(UserActivity.this.getString(R.string.sl_friends_number_format), UserActivity.this.getString(R.string.sl_empty)));
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            UserActivity.this.updateStatusBar();
            UserActivity.this.friendsNumberText.setText(String.format(UserActivity.this.getString(R.string.sl_friends_number_format), Integer.valueOf(UserActivity.this.user.getBuddyUsers().size())));
        }
    }

    private class UserDetailObserver extends BaseActivity.UserGenericObserver {
        private UserDetailObserver() {
            super();
        }

        /* synthetic */ UserDetailObserver(UserActivity userActivity, UserDetailObserver userDetailObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!(exception instanceof RequestCancelledException)) {
                UserActivity.this.showDialogSafe(0);
                UserActivity.this.setProgressIndicator(false);
                updateUI();
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            UserActivity.this.setProgressIndicator(false);
            UserActivity.this.updateStatusBar();
            updateUI();
        }

        private void updateUI() {
            UserActivity.this.lastActiveText.setText(String.format(UserActivity.this.getString(R.string.sl_last_active_format), UserActivity.this.isEmpty(UserActivity.this.user.getLastActiveAt()) ? UserActivity.this.getString(R.string.sl_empty) : UserActivity.DEFAULT_DATE_FORMAT.format(UserActivity.this.user.getLastActiveAt())));
        }
    }

    /* access modifiers changed from: private */
    public void switchToTab(int tab) {
        if (tab == 0) {
            this.detailsLayout.setVisibility(0);
            this.listView.setVisibility(8);
            return;
        }
        this.detailsLayout.setVisibility(8);
        this.listView.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_user);
        updateStatusBar();
        updateHeading(getString(R.string.sl_people), false);
        this.user = ScoreloopManager.getUser();
        TextView loginText = (TextView) findViewById(R.id.name_text);
        loginText.setText(this.user.getLogin());
        loginText.setTypeface(Typeface.DEFAULT, 1);
        this.detailsLayout = (LinearLayout) findViewById(R.id.details_layout);
        this.friendsNumberText = (TextView) findViewById(R.id.friends_number_text);
        this.friendsNumberText.setText(String.format(getString(R.string.sl_friends_number_format), getString(R.string.sl_loading)));
        this.lastActiveText = (TextView) findViewById(R.id.last_active_text);
        this.lastActiveText.setText(String.format(getString(R.string.sl_last_active_format), getString(R.string.sl_loading)));
        this.recentGameImage[0] = (ImageView) findViewById(R.id.image_view_0);
        this.recentGameImage[1] = (ImageView) findViewById(R.id.image_view_1);
        this.recentGameImage[2] = (ImageView) findViewById(R.id.image_view_2);
        this.buddyButton = (Button) findViewById(R.id.buddy_button);
        this.buddyButton.setEnabled(false);
        this.buddyButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserActivity.this.setProgressIndicator(true);
                UserController userController = new UserController(new ManageBuddyObserver(UserActivity.this, null));
                userController.setUser(UserActivity.this.user);
                if (UserActivity.this.buddyButtonIsRemove) {
                    userController.removeAsBuddy();
                } else {
                    userController.addAsBuddy();
                }
            }
        });
        this.listView = (ListView) findViewById(R.id.list_view);
        final SegmentedView segmentedView = (SegmentedView) findViewById(R.id.segments);
        segmentedView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UserActivity.this.switchToTab(segmentedView.getSelectedSegment());
            }
        });
        this.adapter = new ListItemAdapter(this, R.layout.sl_user, new ArrayList());
        this.adapter.add(new ListItem(getResources().getString(R.string.sl_loading)));
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.gamesController = new GamesController(new GamesControllerObserver(this, null));
        this.gamesController.loadRangeForUser(this.user);
        new UserController(new MyBuddiesObserver(this, null)).loadBuddies();
        UserController userBuddiesController = new UserController(new UserBuddiesObserver(this, null));
        userBuddiesController.setUser(this.user);
        userBuddiesController.loadBuddies();
        UserController userDetailController = new UserController(new UserDetailObserver(this, null));
        userDetailController.setUser(this.user);
        userDetailController.loadUserDetail();
        setProgressIndicator(true);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setNotify(new Runnable() {
            public void run() {
                UserActivity.this.adapter.notifyDataSetChanged();
                int i = 0;
                for (Game recentGame : UserActivity.this.recentGames) {
                    UserActivity.this.recentGameImage[i].setImageDrawable(UserActivity.this.getDrawable(recentGame.getImageUrl()));
                    i++;
                }
            }
        });
    }
}
