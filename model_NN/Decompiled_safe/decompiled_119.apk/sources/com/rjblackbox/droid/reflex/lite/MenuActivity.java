package com.rjblackbox.droid.reflex.lite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import com.admob.android.ads.AdView;
import com.scoreloop.android.coreui.HighscoresActivity;
import com.scoreloop.android.coreui.ProfileActivity;
import com.scoreloop.android.coreui.ScoreloopManager;

public class MenuActivity extends Activity implements View.OnClickListener {
    private static final int DIFFICULTY_MENU = 1;
    private static final int MAIN_MENU = 0;
    private int activeLayout;
    AdView adview;
    private ImageButton casualButton;
    private ImageButton easyButton;
    private ImageButton hardButton;
    private ImageButton highscoreButton;
    private ImageButton mediumButton;
    private ImageButton profileButton;
    private ImageButton unlimitedButton;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        ScoreloopManager.init(this, GameActivity.GAME_ID, GameActivity.GAME_SECRET);
        ScoreloopManager.setNumberOfModes(3);
        prepareMainMenu();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.activeLayout == 1) {
            prepareMainMenu();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i = null;
        switch (item.getItemId()) {
            case R.id.menu_how_to_play /*2131427415*/:
                i = new Intent(this, HelpActivity.class);
                break;
            case R.id.menu_about /*2131427416*/:
                i = new Intent(this, AboutActivity.class);
                break;
        }
        startActivity(i);
        return super.onOptionsItemSelected(item);
    }

    private void prepareMainMenu() {
        this.activeLayout = 0;
        setContentView((int) R.layout.main_menu);
        this.unlimitedButton = (ImageButton) findViewById(R.id.button_unlimited);
        this.casualButton = (ImageButton) findViewById(R.id.button_casual);
        this.profileButton = (ImageButton) findViewById(R.id.button_profile);
        this.highscoreButton = (ImageButton) findViewById(R.id.button_highscore);
        this.unlimitedButton.setEnabled(false);
        this.adview = (AdView) findViewById(R.id.menu_ads);
        this.adview.setVisibility(0);
        this.unlimitedButton.setOnClickListener(this);
        this.casualButton.setOnClickListener(this);
        this.profileButton.setOnClickListener(this);
        this.highscoreButton.setOnClickListener(this);
    }

    private void prepareDifficultyMenu() {
        this.activeLayout = 1;
        setContentView((int) R.layout.difficulty_menu);
        this.easyButton = (ImageButton) findViewById(R.id.button_easy);
        this.mediumButton = (ImageButton) findViewById(R.id.button_medium);
        this.hardButton = (ImageButton) findViewById(R.id.button_hard);
        this.adview = (AdView) findViewById(R.id.difficulty_menu_ads);
        this.adview.setVisibility(0);
        this.easyButton.setOnClickListener(this);
        this.mediumButton.setOnClickListener(this);
        this.hardButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent i = null;
        switch (v.getId()) {
            case R.id.button_medium /*2131427329*/:
                i = new Intent(this, GameActivity.class);
                i.putExtra("difficulty", 1);
                break;
            case R.id.button_easy /*2131427330*/:
                i = new Intent(this, GameActivity.class);
                i.putExtra("difficulty", 0);
                break;
            case R.id.button_hard /*2131427331*/:
                i = new Intent(this, GameActivity.class);
                i.putExtra("difficulty", 2);
                break;
            case R.id.button_casual /*2131427351*/:
                prepareDifficultyMenu();
                break;
            case R.id.button_profile /*2131427352*/:
                i = new Intent(this, ProfileActivity.class);
                break;
            case R.id.button_highscore /*2131427353*/:
                i = new Intent(this, HighscoresActivity.class);
                break;
        }
        if (i != null) {
            startActivity(i);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        switch (this.activeLayout) {
            case 0:
                finish();
                return true;
            case 1:
                prepareMainMenu();
                this.activeLayout = 0;
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }
}
