package com.rjblackbox.droid.reflex.lite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.admob.android.ads.AdView;
import com.flurry.android.FlurryAgent;
import com.scoreloop.android.coreui.ScoreloopManager;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;

public class GameActivity extends Activity implements View.OnClickListener {
    public static final String GAME_ID = "b2dd5a50-2b17-4526-b645-6617e8408fa7";
    public static final String GAME_SECRET = "prOFs7AJvZFl1ltj/f27PSppMqGuCEKPu4uhLxPibosP1S9NRul4QQ==";
    /* access modifiers changed from: private */
    public ImageView activeComment;
    /* access modifiers changed from: private */
    public ImageView activeImage;
    private AdView adview;
    private boolean allowClicks = false;
    private Typeface animeace;
    /* access modifiers changed from: private */
    public ImageView comment1;
    /* access modifiers changed from: private */
    public ImageView comment2;
    /* access modifiers changed from: private */
    public ViewFlipper commentFlipper;
    /* access modifiers changed from: private */
    public TextView currentAccuracy;
    /* access modifiers changed from: private */
    public long gameDelay;
    /* access modifiers changed from: private */
    public Handler gameHandler;
    /* access modifiers changed from: private */
    public int gameOver;
    /* access modifiers changed from: private */
    public GameRunnable gameRunnable;
    /* access modifiers changed from: private */
    public int gameState;
    private int hit;
    /* access modifiers changed from: private */
    public ImageView image1;
    /* access modifiers changed from: private */
    public ImageView image2;
    /* access modifiers changed from: private */
    public ViewFlipper imageFlipper;
    /* access modifiers changed from: private */
    public boolean isFirstImage = true;
    /* access modifiers changed from: private */
    public ImageButton leftButton;
    private TextView level;
    /* access modifiers changed from: private */
    public int levelUp;
    /* access modifiers changed from: private */
    public int miss;
    /* access modifiers changed from: private */
    public ImageView myComment;
    /* access modifiers changed from: private */
    public Game myGame;
    /* access modifiers changed from: private */
    public SoundPool pool = new SoundPool(3, 3, 0);
    /* access modifiers changed from: private */
    public boolean responded = true;
    /* access modifiers changed from: private */
    public int retry;
    private ImageButton rightButton;
    private TextView score;
    /* access modifiers changed from: private */
    public ProgressDialog scoreSubmissionDialog;
    /* access modifiers changed from: private */
    public boolean showingPausedDialog = false;
    /* access modifiers changed from: private */
    public TextView targetAccuracy;
    /* access modifiers changed from: private */
    public int yeah;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.game);
        ScoreloopManager.init(this, GAME_ID, GAME_SECRET);
        ScoreloopManager.setNumberOfModes(3);
        this.hit = this.pool.load(this, R.raw.hit, 1);
        this.miss = this.pool.load(this, R.raw.miss, 1);
        this.levelUp = this.pool.load(this, R.raw.level_up, 1);
        this.yeah = this.pool.load(this, R.raw.yeah, 1);
        this.retry = this.pool.load(this, R.raw.retry, 1);
        this.gameOver = this.pool.load(this, R.raw.game_over, 1);
        this.animeace = Typeface.createFromAsset(getAssets(), "fonts/mangatemple.ttf");
        this.myGame = new Game(this, getIntent().getExtras().getInt("difficulty"));
        this.gameDelay = this.myGame.getTiming();
        this.adview = (AdView) findViewById(R.id.game_ads);
        this.adview.setVisibility(0);
        this.level = (TextView) findViewById(R.id.level);
        this.score = (TextView) findViewById(R.id.score);
        this.targetAccuracy = (TextView) findViewById(R.id.target_accuracy);
        this.currentAccuracy = (TextView) findViewById(R.id.current_accuracy);
        this.imageFlipper = (ViewFlipper) findViewById(R.id.image_flipper);
        this.image1 = (ImageView) findViewById(R.id.image1);
        this.image2 = (ImageView) findViewById(R.id.image2);
        this.commentFlipper = (ViewFlipper) findViewById(R.id.comment_flipper);
        this.comment1 = (ImageView) findViewById(R.id.comment1);
        this.comment2 = (ImageView) findViewById(R.id.comment2);
        this.leftButton = (ImageButton) findViewById(R.id.left_button);
        this.rightButton = (ImageButton) findViewById(R.id.right_button);
        this.targetAccuracy.setTypeface(this.animeace);
        this.currentAccuracy.setTypeface(this.animeace);
        this.score.setTypeface(this.animeace);
        this.level.setTypeface(this.animeace);
        this.level.setText(String.format("Level %d", Integer.valueOf(this.myGame.getLevel())));
        this.imageFlipper.setInAnimation(AnimationUtils.loadAnimation(this, 17432578));
        this.imageFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, 17432579));
        this.commentFlipper.setInAnimation(AnimationUtils.loadAnimation(this, 17432578));
        this.commentFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, 17432579));
        this.leftButton.setOnClickListener(this);
        this.rightButton.setOnClickListener(this);
        this.image1.setImageBitmap(this.myGame.START);
        this.activeImage = this.image2;
        this.activeComment = this.comment2;
        disableButtons();
        this.gameHandler = new Handler();
        this.gameRunnable = new GameRunnable(this, null);
        this.gameState = 10;
        this.gameHandler.postDelayed(this.gameRunnable, this.gameDelay);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && this.gameState == 10) {
            this.gameHandler.removeCallbacks(this.gameRunnable);
            this.gameState = 11;
            this.showingPausedDialog = true;
            showPausedDialog();
            return true;
        } else if (keyCode == 4 && this.gameState == 12) {
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "TQY3Z5IIH8RDJVR7R657");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.gameState = this.gameState == 12 ? 12 : 11;
        this.gameHandler.removeCallbacks(this.gameRunnable);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!(this.gameState == 12 || this.gameState == 10 || this.showingPausedDialog)) {
            this.showingPausedDialog = true;
            showPausedDialog();
        }
        Log.d(Game.TAG, new StringBuilder(String.valueOf(this.gameState)).toString());
    }

    private class GameRunnable implements Runnable {
        private Bitmap nextImage;

        private GameRunnable() {
        }

        /* synthetic */ GameRunnable(GameActivity gameActivity, GameRunnable gameRunnable) {
            this();
        }

        public void run() {
            GameActivity.this.disallowClicks();
            if (GameActivity.this.isFirstImage) {
                GameActivity.this.targetAccuracy.setText(String.format("Target %d%%", Integer.valueOf(GameActivity.this.myGame.getTargetAccuracy())));
            } else if (!GameActivity.this.leftButton.isEnabled()) {
                GameActivity.this.enableButtons();
            }
            GameActivity.this.currentAccuracy.setText(String.format("My Accuracy %d%%", Integer.valueOf(GameActivity.this.myGame.getAccuracy())));
            if (!GameActivity.this.responded) {
                GameActivity.this.myGame.userResponse(null);
                GameActivity.this.myComment.setImageBitmap(GameActivity.this.myGame.getFeedbackImage());
                GameActivity.this.pool.play(GameActivity.this.miss, 1.0f, 1.0f, 1, 0, 1.0f);
            }
            GameActivity.this.myComment = GameActivity.this.activeComment;
            this.nextImage = GameActivity.this.myGame.getNextImage();
            GameActivity.this.activeImage.setImageBitmap(this.nextImage);
            GameActivity.this.activeComment.setImageBitmap(null);
            GameActivity.this.activeImage = GameActivity.this.activeImage == GameActivity.this.image1 ? GameActivity.this.image2 : GameActivity.this.image1;
            GameActivity.this.activeComment = GameActivity.this.activeComment == GameActivity.this.comment1 ? GameActivity.this.comment2 : GameActivity.this.comment1;
            if (this.nextImage != null) {
                GameActivity.this.imageFlipper.showNext();
                GameActivity.this.commentFlipper.showNext();
                if (!GameActivity.this.isFirstImage) {
                    GameActivity.this.allowClicks();
                    GameActivity.this.responded = false;
                } else {
                    GameActivity.this.isFirstImage = false;
                    GameActivity.this.responded = true;
                }
                GameActivity.this.gameHandler.removeCallbacks(GameActivity.this.gameRunnable);
                GameActivity.this.gameHandler.postDelayed(GameActivity.this.gameRunnable, GameActivity.this.gameDelay);
                return;
            }
            GameActivity.this.activeComment = GameActivity.this.activeComment == GameActivity.this.comment1 ? GameActivity.this.comment2 : GameActivity.this.comment1;
            if (GameActivity.this.myGame.getAccuracy() < GameActivity.this.myGame.getTargetAccuracy()) {
                if (GameActivity.this.myGame.getLives() > 0) {
                    GameActivity.this.activeComment.setImageBitmap(GameActivity.this.myGame.LIFE_LOST);
                    GameActivity.this.pool.play(GameActivity.this.retry, 1.0f, 1.0f, 1, 0, 1.0f);
                } else {
                    GameActivity.this.activeComment.setImageBitmap(GameActivity.this.myGame.GAME_OVER);
                    GameActivity.this.pool.play(GameActivity.this.gameOver, 1.0f, 1.0f, 1, 0, 1.0f);
                }
            } else if (!GameActivity.this.myGame.isLastLevel()) {
                GameActivity.this.activeComment.setImageBitmap(GameActivity.this.myGame.LEVEL_UP);
                GameActivity.this.pool.play(GameActivity.this.levelUp, 1.0f, 1.0f, 1, 0, 1.0f);
            } else if (GameActivity.this.myGame.isLastLevel()) {
                GameActivity.this.activeComment.setImageBitmap(GameActivity.this.myGame.WINNER);
                GameActivity.this.pool.play(GameActivity.this.yeah, 1.0f, 1.0f, 1, 0, 1.0f);
            }
            GameActivity.this.levelComplete();
        }

        public void reset() {
            GameActivity.this.image1.setImageBitmap(null);
            GameActivity.this.image2.setImageBitmap(null);
            GameActivity.this.comment1.setImageBitmap(null);
            GameActivity.this.comment2.setImageBitmap(null);
            GameActivity.this.activeImage = GameActivity.this.image2;
            GameActivity.this.activeComment = GameActivity.this.comment2;
            GameActivity.this.imageFlipper.setDisplayedChild(0);
            GameActivity.this.commentFlipper.setDisplayedChild(0);
            GameActivity.this.gameHandler.removeCallbacks(GameActivity.this.gameRunnable);
            GameActivity.this.responded = true;
            GameActivity.this.isFirstImage = true;
            GameActivity.this.gameDelay = GameActivity.this.myGame.getTiming();
        }
    }

    /* access modifiers changed from: private */
    public void updateUI() {
        this.image1.setImageBitmap(this.myGame.START);
        this.level.setText(String.format("Level %d", Integer.valueOf(this.myGame.getLevel())));
        this.score.setText(Integer.toString(this.myGame.getScore()));
        this.currentAccuracy.setText(String.format("My Accuracy %d%%", Integer.valueOf(this.myGame.getAccuracy())));
        this.leftButton.setVisibility(0);
        this.rightButton.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void levelComplete() {
        this.gameState = 12;
        this.gameHandler.removeCallbacks(this.gameRunnable);
        this.leftButton.setVisibility(4);
        this.rightButton.setVisibility(4);
        disableButtons();
        this.imageFlipper.showNext();
        this.commentFlipper.showNext();
        this.gameHandler.postDelayed(new DialogRunnable(this, null), 1500);
    }

    private class DialogRunnable implements Runnable {
        private DialogRunnable() {
        }

        /* synthetic */ DialogRunnable(GameActivity gameActivity, DialogRunnable dialogRunnable) {
            this();
        }

        public void run() {
            boolean cleared = GameActivity.this.myGame.getAccuracy() >= GameActivity.this.myGame.getTargetAccuracy();
            if (cleared && !GameActivity.this.myGame.isLastLevel()) {
                GameActivity.this.showLevelUpDialog();
            } else if (!cleared) {
                if (GameActivity.this.myGame.getLives() > 0) {
                    GameActivity.this.showReplayDialog();
                } else {
                    GameActivity.this.showTryAgainDialog();
                }
            } else if (cleared && GameActivity.this.myGame.isLastLevel()) {
                GameActivity.this.showWinnerDialog();
            }
        }
    }

    /* access modifiers changed from: private */
    public void allowClicks() {
        this.allowClicks = true;
    }

    /* access modifiers changed from: private */
    public void disallowClicks() {
        this.allowClicks = false;
    }

    /* access modifiers changed from: private */
    public void enableButtons() {
        this.leftButton.setEnabled(true);
        this.rightButton.setEnabled(true);
    }

    private void disableButtons() {
        this.leftButton.setEnabled(false);
        this.rightButton.setEnabled(false);
    }

    public void onClick(View v) {
        boolean scored = false;
        if (!this.responded && this.allowClicks) {
            switch (v.getId()) {
                case R.id.left_button:
                    scored = this.myGame.userResponse(true);
                    break;
                case R.id.right_button:
                    scored = this.myGame.userResponse(false);
                    break;
            }
            if (scored) {
                this.pool.play(this.hit, 1.0f, 1.0f, 1, 0, 1.0f);
            } else {
                this.pool.play(this.miss, 1.0f, 1.0f, 1, 0, 1.0f);
            }
            this.responded = true;
            this.myComment.setImageBitmap(this.myGame.getFeedbackImage());
            this.score.setText(String.format("%d", Integer.valueOf(this.myGame.getScore())));
            this.currentAccuracy.setText(String.format("My Accuracy %d%%", Integer.valueOf(this.myGame.getAccuracy())));
        }
    }

    private class ScoreSubmitObserver implements RequestControllerObserver {
        private ScoreSubmitObserver() {
        }

        /* synthetic */ ScoreSubmitObserver(GameActivity gameActivity, ScoreSubmitObserver scoreSubmitObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController rc, Exception e) {
            GameActivity.this.scoreSubmissionDialog.dismiss();
            Toast.makeText(GameActivity.this, "Score submission failed.", 1).show();
            GameActivity.this.finish();
        }

        public void requestControllerDidReceiveResponse(RequestController rc) {
            GameActivity.this.scoreSubmissionDialog.dismiss();
            Toast.makeText(GameActivity.this, "Score submission successful.", 1).show();
            GameActivity.this.finish();
        }
    }

    /* access modifiers changed from: private */
    public void submitScore() {
        this.scoreSubmissionDialog = ProgressDialog.show(this, null, "Submitting Score...", true);
        this.scoreSubmissionDialog.setCancelable(false);
        ScoreloopManager.submitScore(this.myGame.getScore(), this.myGame.getDifficulty(), new ScoreSubmitObserver(this, null));
    }

    private void showPausedDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setCancelable(false);
        dialog.setTitle("Game Paused");
        dialog.setMessage(String.format("What do you want to do?", new Object[0]));
        dialog.setButton("Resume", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.gameState = 10;
                GameActivity.this.gameHandler.postDelayed(GameActivity.this.gameRunnable, GameActivity.this.gameDelay / 2);
                GameActivity.this.showingPausedDialog = false;
            }
        });
        dialog.setButton2("Main Menu", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.startActivity(new Intent(GameActivity.this, MenuActivity.class));
                GameActivity.this.finish();
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void showWinnerDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setCancelable(false);
        dialog.setTitle("You Won!");
        dialog.setMessage(String.format("You scored %d points.", Integer.valueOf(this.myGame.getScore())));
        dialog.setButton("Submit Score", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.submitScore();
            }
        });
        dialog.setButton2("Main Menu", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.startActivity(new Intent(GameActivity.this, MenuActivity.class));
                GameActivity.this.finish();
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void showReplayDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setCancelable(false);
        dialog.setTitle("Life Lost");
        dialog.setMessage(String.format("Retries left: %d", Integer.valueOf(this.myGame.getLives())));
        dialog.setButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.myGame.retry();
                GameActivity.this.gameRunnable.reset();
                GameActivity.this.updateUI();
                GameActivity.this.gameState = 10;
                GameActivity.this.gameHandler.postDelayed(GameActivity.this.gameRunnable, GameActivity.this.gameDelay);
            }
        });
        dialog.setButton2("Main Menu", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.startActivity(new Intent(GameActivity.this, MenuActivity.class));
                GameActivity.this.finish();
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void showTryAgainDialog() {
        String message;
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setCancelable(false);
        dialog.setTitle("Game Over");
        if (this.myGame.getScore() > 0) {
            message = String.format("Your scored %d points.", Integer.valueOf(this.myGame.getScore()));
            dialog.setButton("Submit Score", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    GameActivity.this.submitScore();
                }
            });
        } else {
            message = "What do you want to do?";
            dialog.setButton("Play Again", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    GameActivity.this.myGame.playAgain();
                    GameActivity.this.gameRunnable.reset();
                    GameActivity.this.updateUI();
                    GameActivity.this.gameState = 10;
                    GameActivity.this.gameHandler.postDelayed(GameActivity.this.gameRunnable, GameActivity.this.gameDelay);
                }
            });
        }
        dialog.setMessage(message);
        dialog.setButton2("Main Menu", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.startActivity(new Intent(GameActivity.this, MenuActivity.class));
                GameActivity.this.finish();
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void showLevelUpDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setCancelable(false);
        View v = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.score_board, (ViewGroup) null);
        dialog.setTitle(String.format("Level %d Cleared", Integer.valueOf(this.myGame.getLevel())));
        dialog.setView(v);
        dialog.setButton("Next Level", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.myGame.levelUp();
                GameActivity.this.gameRunnable.reset();
                GameActivity.this.updateUI();
                GameActivity.this.gameState = 10;
                GameActivity.this.gameHandler.postDelayed(GameActivity.this.gameRunnable, GameActivity.this.gameDelay);
            }
        });
        dialog.setButton2("Main Menu", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.startActivity(new Intent(GameActivity.this, MenuActivity.class));
                GameActivity.this.finish();
            }
        });
        if (this.myGame.getScore() > 0) {
            dialog.setButton3("Submit Score", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    GameActivity.this.submitScore();
                }
            });
        }
        ((TextView) v.findViewById(R.id.dialog_title_nq)).setTypeface(this.animeace);
        ((TextView) v.findViewById(R.id.dialog_title_score)).setTypeface(this.animeace);
        ((TextView) v.findViewById(R.id.dialog_title_correct)).setTypeface(this.animeace);
        ((TextView) v.findViewById(R.id.dialog_title_accuracy)).setTypeface(this.animeace);
        ((TextView) v.findViewById(R.id.dialog_title_x5)).setTypeface(this.animeace);
        ((TextView) v.findViewById(R.id.dialog_title_x10)).setTypeface(this.animeace);
        TextView nQTV = (TextView) v.findViewById(R.id.dialog_nq);
        TextView scoreTV = (TextView) v.findViewById(R.id.dialog_score);
        TextView answersTV = (TextView) v.findViewById(R.id.dialog_correct);
        TextView accuracyTV = (TextView) v.findViewById(R.id.dialog_accuracy);
        TextView x5TV = (TextView) v.findViewById(R.id.dialog_x5);
        TextView x10TV = (TextView) v.findViewById(R.id.dialog_x10);
        nQTV.setTypeface(this.animeace);
        scoreTV.setTypeface(this.animeace);
        answersTV.setTypeface(this.animeace);
        accuracyTV.setTypeface(this.animeace);
        x5TV.setTypeface(this.animeace);
        x10TV.setTypeface(this.animeace);
        nQTV.setText(Integer.toString(this.myGame.getNQ()));
        scoreTV.setText(Integer.toString(this.myGame.getScore()));
        answersTV.setText(Integer.toString(this.myGame.getCorrectAnswers()));
        accuracyTV.setText(String.format("%d%%", Integer.valueOf(this.myGame.getAccuracy())));
        x5TV.setText(Integer.toString(this.myGame.getX5BonusCount()));
        x10TV.setText(Integer.toString(this.myGame.getX10BonusCount()));
        dialog.show();
    }
}
