package com.rjblackbox.droid.reflex.lite;

public final class R {

    public static final class array {
        public static final int sl_game_modes = 2131099648;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int sl_color_background = 2131034112;
        public static final int sl_color_background_clickable = 2131034113;
        public static final int sl_color_divider = 2131034117;
        public static final int sl_color_foreground = 2131034114;
        public static final int sl_color_foreground_lite = 2131034115;
        public static final int sl_color_sl = 2131034116;
        public static final int sl_selector_button_text_color = 2131034118;
        public static final int sl_selector_tab_text_color = 2131034119;
    }

    public static final class dimen {
        public static final int sl_clickable_height = 2131296257;
        public static final int sl_edit_text_height = 2131296256;
        public static final int sl_list_image_size = 2131296260;
        public static final int sl_list_item_height = 2131296259;
        public static final int sl_medium_height = 2131296258;
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int button_casual = 2130837505;
        public static final int button_casual_down = 2130837506;
        public static final int button_casual_selector = 2130837507;
        public static final int button_easy = 2130837508;
        public static final int button_easy_down = 2130837509;
        public static final int button_easy_selector = 2130837510;
        public static final int button_hard = 2130837511;
        public static final int button_hard_down = 2130837512;
        public static final int button_hard_selector = 2130837513;
        public static final int button_highscore = 2130837514;
        public static final int button_highscore_disabled = 2130837515;
        public static final int button_highscore_down = 2130837516;
        public static final int button_highscore_selector = 2130837517;
        public static final int button_medium = 2130837518;
        public static final int button_medium_down = 2130837519;
        public static final int button_medium_selector = 2130837520;
        public static final int button_profile = 2130837521;
        public static final int button_profile_down = 2130837522;
        public static final int button_profile_selector = 2130837523;
        public static final int button_unlimited = 2130837524;
        public static final int button_unlimited_disabled = 2130837525;
        public static final int button_unlimited_down = 2130837526;
        public static final int button_unlimited_selector = 2130837527;
        public static final int comment_game_over = 2130837528;
        public static final int comment_hit = 2130837529;
        public static final int comment_level_up = 2130837530;
        public static final int comment_life_lost = 2130837531;
        public static final int comment_lost = 2130837532;
        public static final int comment_miss = 2130837533;
        public static final int comment_rpp_logo = 2130837534;
        public static final int comment_start = 2130837535;
        public static final int comment_winner = 2130837536;
        public static final int comment_x10_bonus = 2130837537;
        public static final int comment_x5_bonus = 2130837538;
        public static final int game_apparatus = 2130837539;
        public static final int game_arrow = 2130837540;
        public static final int game_basketball = 2130837541;
        public static final int game_bottle = 2130837542;
        public static final int game_bowling_ball = 2130837543;
        public static final int game_camera = 2130837544;
        public static final int game_clouds = 2130837545;
        public static final int game_crimson_shield = 2130837546;
        public static final int game_dove = 2130837547;
        public static final int game_eight_ball = 2130837548;
        public static final int game_fire_warning = 2130837549;
        public static final int game_fleur_de_lis = 2130837550;
        public static final int game_flower_foot = 2130837551;
        public static final int game_half_kiwi = 2130837552;
        public static final int game_leaf = 2130837553;
        public static final int game_roo_sign = 2130837554;
        public static final int game_snooker_ball = 2130837555;
        public static final int game_television = 2130837556;
        public static final int game_usb_sign = 2130837557;
        public static final int game_wine_glass = 2130837558;
        public static final int how_to_play = 2130837559;
        public static final int icon = 2130837560;
        public static final int pattern_lazy_daisies = 2130837561;
        public static final int right = 2130837562;
        public static final int right_disabled = 2130837563;
        public static final int right_pressed = 2130837564;
        public static final int right_selector = 2130837565;
        public static final int sl_addressbook = 2130837566;
        public static final int sl_arrow_down = 2130837567;
        public static final int sl_arrow_right = 2130837568;
        public static final int sl_button_background = 2130837569;
        public static final int sl_button_background_disabled = 2130837570;
        public static final int sl_button_background_selector = 2130837571;
        public static final int sl_checkbox_checked = 2130837572;
        public static final int sl_checkbox_checked_disabled = 2130837573;
        public static final int sl_checkbox_selector = 2130837574;
        public static final int sl_checkbox_unchecked = 2130837575;
        public static final int sl_divider = 2130837576;
        public static final int sl_edit_background = 2130837577;
        public static final int sl_facebook = 2130837578;
        public static final int sl_game_default = 2130837579;
        public static final int sl_heading_background = 2130837580;
        public static final int sl_icon_slapp = 2130837581;
        public static final int sl_list_background_selector = 2130837582;
        public static final int sl_list_selector = 2130837583;
        public static final int sl_login = 2130837584;
        public static final int sl_logo = 2130837585;
        public static final int sl_low_background = 2130837586;
        public static final int sl_menu_games = 2130837587;
        public static final int sl_menu_highscores = 2130837588;
        public static final int sl_menu_profile = 2130837589;
        public static final int sl_myspace = 2130837590;
        public static final int sl_progress = 2130837591;
        public static final int sl_progress_1 = 2130837592;
        public static final int sl_progress_2 = 2130837593;
        public static final int sl_progress_3 = 2130837594;
        public static final int sl_status_bar_background = 2130837595;
        public static final int sl_tab_background = 2130837596;
        public static final int sl_tab_background_disabled = 2130837597;
        public static final int sl_tab_background_selector = 2130837598;
        public static final int sl_twitter = 2130837599;
        public static final int sl_user = 2130837600;
        public static final int sl_user_add = 2130837601;
        public static final int tiled_lazy_daisies = 2130837602;
        public static final int wrong = 2130837603;
        public static final int wrong_disabled = 2130837604;
        public static final int wrong_pressed = 2130837605;
        public static final int wrong_selector = 2130837606;
    }

    public static final class id {
        public static final int about_text = 2131427328;
        public static final int activity_container = 2131427405;
        public static final int buddy_button = 2131427414;
        public static final int button_add = 2131427372;
        public static final int button_casual = 2131427351;
        public static final int button_easy = 2131427330;
        public static final int button_hard = 2131427331;
        public static final int button_highscore = 2131427353;
        public static final int button_medium = 2131427329;
        public static final int button_ok = 2131427376;
        public static final int button_profile = 2131427352;
        public static final int button_unlimited = 2131427350;
        public static final int cancel_button = 2131427402;
        public static final int center_helper = 2131427339;
        public static final int center_view = 2131427349;
        public static final int comment1 = 2131427346;
        public static final int comment2 = 2131427347;
        public static final int comment_flipper = 2131427345;
        public static final int current_accuracy = 2131427340;
        public static final int description_text = 2131427382;
        public static final int details_layout = 2131427381;
        public static final int dialog_accuracy = 2131427362;
        public static final int dialog_correct = 2131427360;
        public static final int dialog_nq = 2131427358;
        public static final int dialog_score = 2131427356;
        public static final int dialog_title_accuracy = 2131427361;
        public static final int dialog_title_correct = 2131427359;
        public static final int dialog_title_nq = 2131427357;
        public static final int dialog_title_score = 2131427355;
        public static final int dialog_title_x10 = 2131427365;
        public static final int dialog_title_x5 = 2131427363;
        public static final int dialog_x10 = 2131427366;
        public static final int dialog_x5 = 2131427364;
        public static final int difficulty_menu_ads = 2131427333;
        public static final int divider = 2131427392;
        public static final int edit_login = 2131427375;
        public static final int email_edit = 2131427368;
        public static final int facebook_checkbox = 2131427398;
        public static final int featured_layout = 2131427386;
        public static final int friends_number_text = 2131427408;
        public static final int game_ads = 2131427334;
        public static final int get_game_button = 2131427383;
        public static final int heading_text = 2131427387;
        public static final int help_text = 2131427348;
        public static final int icon_image = 2131427388;
        public static final int image = 2131427395;
        public static final int image1 = 2131427343;
        public static final int image2 = 2131427344;
        public static final int image_flipper = 2131427342;
        public static final int image_view = 2131427377;
        public static final int image_view_0 = 2131427411;
        public static final int image_view_1 = 2131427412;
        public static final int image_view_2 = 2131427413;
        public static final int last_active_text = 2131427409;
        public static final int layout_mode = 2131427389;
        public static final int left_button = 2131427335;
        public static final int level = 2131427337;
        public static final int list_view = 2131427373;
        public static final int loading_text = 2131427385;
        public static final int login_edit = 2131427367;
        public static final int login_text = 2131427407;
        public static final int logo = 2131427332;
        public static final int menu_about = 2131427416;
        public static final int menu_ads = 2131427354;
        public static final int menu_how_to_play = 2131427415;
        public static final int message = 2131427374;
        public static final int message_edittext = 2131427401;
        public static final int mode_text = 2131427390;
        public static final int myscore_view = 2131427393;
        public static final int myspace_checkbox = 2131427399;
        public static final int name_text = 2131427378;
        public static final int ok_button = 2131427403;
        public static final int progress_indicator = 2131427406;
        public static final int publisher_text = 2131427379;
        public static final int recent_games_layout = 2131427410;
        public static final int right_button = 2131427336;
        public static final int score = 2131427338;
        public static final int search_list_segments = 2131427391;
        public static final int segmented_view = 2131427404;
        public static final int segments = 2131427380;
        public static final int slapp_button = 2131427370;
        public static final int slapp_download_button = 2131427371;
        public static final int subheading_layout = 2131427384;
        public static final int target_accuracy = 2131427341;
        public static final int text0 = 2131427394;
        public static final int text1 = 2131427396;
        public static final int text2 = 2131427397;
        public static final int twitter_checkbox = 2131427400;
        public static final int update_button = 2131427369;
    }

    public static final class layout {
        public static final int about_activity = 2130903040;
        public static final int difficulty_menu = 2130903041;
        public static final int game = 2130903042;
        public static final int help_activity = 2130903043;
        public static final int main_menu = 2130903044;
        public static final int score_board = 2130903045;
        public static final int sl_account = 2130903046;
        public static final int sl_buddies = 2130903047;
        public static final int sl_buddies_add = 2130903048;
        public static final int sl_dialog_custom = 2130903049;
        public static final int sl_dialog_login = 2130903050;
        public static final int sl_game = 2130903051;
        public static final int sl_games = 2130903052;
        public static final int sl_heading = 2130903053;
        public static final int sl_highscores = 2130903054;
        public static final int sl_list_item = 2130903055;
        public static final int sl_post_score = 2130903056;
        public static final int sl_profile = 2130903057;
        public static final int sl_status_bar = 2130903058;
        public static final int sl_user = 2130903059;
    }

    public static final class menu {
        public static final int menu = 2131361792;
    }

    public static final class raw {
        public static final int game_over = 2130968576;
        public static final int hit = 2130968577;
        public static final int level_up = 2130968578;
        public static final int miss = 2130968579;
        public static final int retry = 2130968580;
        public static final int yeah = 2130968581;
    }

    public static final class string {
        public static final int about_text = 2131165243;
        public static final int about_title = 2131165242;
        public static final int app_name = 2131165240;
        public static final int app_version = 2131165241;
        public static final int hello = 2131165239;
        public static final int help_text = 2131165245;
        public static final int help_title = 2131165244;
        public static final int sl_account = 2131165192;
        public static final int sl_add_friend = 2131165228;
        public static final int sl_addressbook = 2131165210;
        public static final int sl_buddies_add = 2131165202;
        public static final int sl_buddies_added_one_format = 2131165200;
        public static final int sl_buddies_added_other_format = 2131165201;
        public static final int sl_details = 2131165218;
        public static final int sl_disclaimer = 2131165230;
        public static final int sl_email = 2131165188;
        public static final int sl_empty = 2131165224;
        public static final int sl_error_message_connect_failed = 2131165237;
        public static final int sl_error_message_email_already_taken = 2131165234;
        public static final int sl_error_message_invalid_email = 2131165235;
        public static final int sl_error_message_name_already_taken = 2131165233;
        public static final int sl_error_message_network = 2131165232;
        public static final int sl_error_message_user_cancel = 2131165236;
        public static final int sl_error_message_user_invalid = 2131165238;
        public static final int sl_facebook = 2131165207;
        public static final int sl_find = 2131165211;
        public static final int sl_found_many_users_format = 2131165198;
        public static final int sl_found_no_user = 2131165197;
        public static final int sl_found_too_many_users = 2131165199;
        public static final int sl_friends = 2131165193;
        public static final int sl_friends_games = 2131165217;
        public static final int sl_friends_number_format = 2131165225;
        public static final int sl_friends_playing = 2131165219;
        public static final int sl_games = 2131165214;
        public static final int sl_get_game = 2131165221;
        public static final int sl_get_slapp = 2131165231;
        public static final int sl_global = 2131165194;
        public static final int sl_highscores = 2131165185;
        public static final int sl_last_active_format = 2131165226;
        public static final int sl_loading = 2131165213;
        public static final int sl_login = 2131165187;
        public static final int sl_myspace = 2131165208;
        public static final int sl_new = 2131165216;
        public static final int sl_next = 2131165190;
        public static final int sl_no_details = 2131165220;
        public static final int sl_no_results_found = 2131165222;
        public static final int sl_no_thanks = 2131165206;
        public static final int sl_not_on_highscore_list = 2131165196;
        public static final int sl_people = 2131165223;
        public static final int sl_popular = 2131165215;
        public static final int sl_post = 2131165205;
        public static final int sl_post_success = 2131165212;
        public static final int sl_prev = 2131165189;
        public static final int sl_profile = 2131165184;
        public static final int sl_recent_games = 2131165227;
        public static final int sl_remove_friend = 2131165229;
        public static final int sl_share_score = 2131165204;
        public static final int sl_sl_login = 2131165203;
        public static final int sl_top = 2131165191;
        public static final int sl_twentyfour = 2131165195;
        public static final int sl_twitter = 2131165209;
        public static final int sl_update_profile = 2131165186;
    }

    public static final class style {
        public static final int TransparentDialog = 2131230720;
        public static final int sl_heading = 2131230721;
        public static final int sl_list_item = 2131230724;
        public static final int sl_non_clickable = 2131230725;
        public static final int sl_normal = 2131230723;
        public static final int sl_title_bar = 2131230722;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
