package com.rjblackbox.droid.reflex.lite;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class HelpActivity extends Activity {
    private TextView helpTV;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help_activity);
        this.helpTV = (TextView) findViewById(R.id.help_text);
        this.helpTV.setText(getString(R.string.help_text));
    }
}
