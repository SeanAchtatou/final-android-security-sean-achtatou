package com.rjblackbox.droid.reflex.lite;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class Game {
    private static final int BA = 50;
    public static final int CASUAL_EASY = 0;
    public static final int CASUAL_HARD = 2;
    public static final int CASUAL_MEDIUM = 1;
    public static final int HIGH_SCORE_THRESHOLD = 0;
    private static final int IMAGES_PER_LEVEL = 5;
    public static final int LEVEL_COMPLETE = 12;
    private static final int[] LEVEL_IMAGES;
    private static final int MAX_LEVELS = 20;
    public static final int PAUSED = 11;
    private static final int[] REQD_ACCURACY;
    private static final int RIGHT = 1;
    public static final int RUNNING = 10;
    public static final String TAG = "Reflex++ Game";
    private static final int WRONG = 0;
    public final Bitmap BONUS_10;
    public final Bitmap BONUS_5;
    public final Bitmap GAME_OVER;
    public final Bitmap HIT;
    public final Bitmap LEVEL_UP;
    public final Bitmap LIFE_LOST;
    public final Bitmap LOST;
    public final Bitmap MISS;
    public final Bitmap START;
    public final Bitmap WINNER;
    private int chainAnswers;
    private int correctAnswers;
    private Bitmap currentDrawable;
    private int difficulty;
    private Bitmap feedbackImage;
    private HashMap<Integer, Bitmap> imageCache;
    private Bitmap lastDrawable;
    private int lastLevelScore = 0;
    private int level = 1;
    private ArrayList<Bitmap> levelDrawables;
    private int lives = getNumberOfLives();
    private int point = getDifficultyPoint();
    private int qCount = getNQuestions();
    private int qDispatched = -1;
    private Random random;
    private Resources res;
    private int score = 0;
    private long timing = getDifficultyTiming();
    private int x10BonusCount;
    private int x5BonusCount;

    static {
        int[] iArr = new int[MAX_LEVELS];
        // fill-array-data instruction
        iArr[0] = 2130837539;
        iArr[1] = 2130837540;
        iArr[2] = 2130837541;
        iArr[3] = 2130837542;
        iArr[4] = 2130837543;
        iArr[5] = 2130837544;
        iArr[6] = 2130837545;
        iArr[7] = 2130837546;
        iArr[8] = 2130837547;
        iArr[9] = 2130837548;
        iArr[10] = 2130837549;
        iArr[11] = 2130837550;
        iArr[12] = 2130837551;
        iArr[13] = 2130837552;
        iArr[14] = 2130837553;
        iArr[15] = 2130837554;
        iArr[16] = 2130837555;
        iArr[17] = 2130837556;
        iArr[18] = 2130837557;
        iArr[19] = 2130837558;
        LEVEL_IMAGES = iArr;
        int[] iArr2 = new int[MAX_LEVELS];
        // fill-array-data instruction
        iArr2[0] = 70;
        iArr2[1] = 70;
        iArr2[2] = 70;
        iArr2[3] = 70;
        iArr2[4] = 70;
        iArr2[5] = 75;
        iArr2[6] = 75;
        iArr2[7] = 75;
        iArr2[8] = 75;
        iArr2[9] = 75;
        iArr2[10] = 80;
        iArr2[11] = 80;
        iArr2[12] = 80;
        iArr2[13] = 80;
        iArr2[14] = 80;
        iArr2[15] = 90;
        iArr2[16] = 90;
        iArr2[17] = 90;
        iArr2[18] = 90;
        iArr2[19] = 90;
        REQD_ACCURACY = iArr2;
    }

    public Game(Context ctx, int difficulty2) {
        this.difficulty = difficulty2;
        this.res = ctx.getResources();
        this.random = new Random();
        this.imageCache = new HashMap<>();
        this.levelDrawables = getLevelDrawables();
        this.START = BitmapFactory.decodeResource(this.res, R.drawable.comment_start);
        this.HIT = BitmapFactory.decodeResource(this.res, R.drawable.comment_hit);
        this.MISS = BitmapFactory.decodeResource(this.res, R.drawable.comment_miss);
        this.LOST = BitmapFactory.decodeResource(this.res, R.drawable.comment_lost);
        this.BONUS_5 = BitmapFactory.decodeResource(this.res, R.drawable.comment_x5_bonus);
        this.BONUS_10 = BitmapFactory.decodeResource(this.res, R.drawable.comment_x10_bonus);
        this.LEVEL_UP = BitmapFactory.decodeResource(this.res, R.drawable.comment_level_up);
        this.WINNER = BitmapFactory.decodeResource(this.res, R.drawable.comment_winner);
        this.LIFE_LOST = BitmapFactory.decodeResource(this.res, R.drawable.comment_life_lost);
        this.GAME_OVER = BitmapFactory.decodeResource(this.res, R.drawable.comment_game_over);
    }

    public void levelUp() {
        this.level++;
        this.lastLevelScore = this.score;
        this.timing = getDifficultyTiming();
        this.qCount = getNQuestions();
        this.qDispatched = -1;
        this.correctAnswers = 0;
        this.chainAnswers = 0;
        this.x5BonusCount = 0;
        this.x10BonusCount = 0;
        this.levelDrawables = getLevelDrawables();
    }

    public boolean isLastLevel() {
        return this.level >= MAX_LEVELS;
    }

    public void retry() {
        this.qDispatched = -1;
        this.correctAnswers = 0;
        this.chainAnswers = 0;
        this.x5BonusCount = 0;
        this.x10BonusCount = 0;
        this.score = this.lastLevelScore;
        this.lives--;
    }

    public void playAgain() {
        this.level = 1;
        this.score = 0;
        this.lastLevelScore = 0;
        this.lives = getNumberOfLives();
        this.point = getDifficultyPoint();
        this.timing = getDifficultyTiming();
        this.qCount = getNQuestions();
        this.qDispatched = -1;
        this.correctAnswers = 0;
        this.chainAnswers = 0;
        this.x5BonusCount = 0;
        this.x10BonusCount = 0;
    }

    public boolean userResponse(Boolean response) {
        boolean z;
        boolean scored = false;
        if (this.lastDrawable == this.currentDrawable) {
            z = true;
        } else {
            z = false;
        }
        Boolean answer = Boolean.valueOf(z);
        if (response == null) {
            this.feedbackImage = this.LOST;
            this.chainAnswers = 0;
        } else if (response == answer) {
            this.chainAnswers++;
            if (this.chainAnswers == 5) {
                this.x5BonusCount++;
                incrementScore(this.point + (this.point * 5));
                this.feedbackImage = this.BONUS_5;
            } else if (this.chainAnswers == 10) {
                this.x10BonusCount++;
                incrementScore(this.point + (this.point * 10));
                this.feedbackImage = this.BONUS_10;
                this.chainAnswers = 0;
            } else {
                incrementScore(this.point);
                this.feedbackImage = this.HIT;
            }
            this.correctAnswers++;
            scored = true;
        } else if (response != answer) {
            this.feedbackImage = this.MISS;
            this.chainAnswers = 0;
        }
        Log.d(TAG, "Score: " + this.score);
        return scored;
    }

    public int getNQ() {
        return this.qCount;
    }

    public Bitmap getFeedbackImage() {
        return this.feedbackImage;
    }

    public Bitmap getNextImage() {
        Bitmap b;
        int n = this.levelDrawables.size();
        if (this.lastDrawable == null) {
            this.lastDrawable = this.levelDrawables.get(this.random.nextInt(n));
            b = this.lastDrawable;
        } else {
            int answer = this.random.nextInt(2);
            if (this.currentDrawable != null) {
                this.lastDrawable = this.currentDrawable;
            }
            if (answer == 1) {
                this.currentDrawable = this.lastDrawable;
                Log.d(TAG, "Choosing right answer");
            } else if (answer == 0) {
                while (true) {
                    if (this.currentDrawable != this.lastDrawable && this.currentDrawable != null) {
                        break;
                    }
                    this.currentDrawable = this.levelDrawables.get(this.random.nextInt(n));
                }
                Log.d(TAG, "Choosing wrong answer");
            }
            b = this.currentDrawable;
        }
        if (this.qDispatched == this.qCount) {
            return null;
        }
        this.qDispatched++;
        return b;
    }

    public int getDifficulty() {
        return this.difficulty;
    }

    public long getTiming() {
        return this.timing;
    }

    public int getX5BonusCount() {
        return this.x5BonusCount;
    }

    public int getX10BonusCount() {
        return this.x10BonusCount;
    }

    public int getCorrectAnswers() {
        return this.correctAnswers;
    }

    public int getTargetAccuracy() {
        return REQD_ACCURACY[this.level - 1];
    }

    public int getLives() {
        return this.lives;
    }

    public int getAccuracy() {
        int nQ = this.qDispatched;
        if (nQ < 1) {
            nQ = 1;
        }
        return (this.correctAnswers * 100) / nQ;
    }

    public int getLevel() {
        return this.level;
    }

    public int getScore() {
        return this.score;
    }

    private void incrementScore(int points) {
        this.score += points;
    }

    private ArrayList<Bitmap> getLevelDrawables() {
        if (!this.imageCache.isEmpty()) {
            this.imageCache.clear();
        }
        while (this.imageCache.size() < 5) {
            int resId = LEVEL_IMAGES[this.random.nextInt(LEVEL_IMAGES.length)];
            this.imageCache.put(Integer.valueOf(resId), BitmapFactory.decodeResource(this.res, resId));
            Log.d(TAG, "Resource: " + resId);
        }
        Log.d(TAG, String.valueOf(this.imageCache.size()) + " images");
        ArrayList<Bitmap> result = new ArrayList<>(this.imageCache.values());
        Collections.shuffle(result);
        return result;
    }

    private long getDifficultyTiming() {
        int baseTiming = 0;
        switch (this.difficulty) {
            case 0:
                baseTiming = 1800;
                break;
            case 1:
                baseTiming = 1400;
                break;
            case 2:
                baseTiming = 1100;
                break;
        }
        return (long) (baseTiming - (this.level * 10));
    }

    private int getNumberOfLives() {
        switch (this.difficulty) {
            case 1:
                return 3;
            case 2:
                return 2;
            default:
                return 4;
        }
    }

    private int getDifficultyPoint() {
        switch (this.difficulty) {
            case 1:
                return 15;
            case 2:
                return MAX_LEVELS;
            default:
                return 10;
        }
    }

    private int getNQuestions() {
        return (this.level * 5) + 0;
    }
}
