package com.rjblackbox.droid.reflex.lite;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class AboutActivity extends Activity {
    private TextView aboutTV;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about_activity);
        this.aboutTV = (TextView) findViewById(R.id.about_text);
        this.aboutTV.setAutoLinkMask(15);
        this.aboutTV.setText(String.format(getString(R.string.about_text), getString(R.string.app_name), getString(R.string.app_version)));
    }
}
