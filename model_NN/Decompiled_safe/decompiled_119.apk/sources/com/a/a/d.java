package com.a.a;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieSyncManager;

public class d {
    protected static String a = "https://graph.facebook.com/oauth/authorize";
    protected static String b = "https://www.facebook.com/connect/uiserver.php";
    protected static String c = "https://graph.facebook.com/";
    protected static String d = "https://api.facebook.com/restserver.php";
    private String e = null;
    private long f = 0;

    public interface a {
        void a();

        void a(Bundle bundle);

        void a(c cVar);

        void a(e eVar);
    }

    public void a(long j) {
        this.f = j;
    }

    public void a(Context context, String str, Bundle bundle, a aVar) {
        String str2;
        if (str.equals("login")) {
            str2 = a;
            bundle.putString("type", "user_agent");
            bundle.putString("redirect_uri", "fbconnect://success");
        } else {
            str2 = b;
            bundle.putString("method", str);
            bundle.putString("next", "fbconnect://success");
        }
        bundle.putString("display", "touch");
        if (a()) {
            bundle.putString("access_token", b());
        }
        String str3 = str2 + "?" + a.a(bundle);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            a.a(context, "Error", "Application requires permission to access the Internet");
        } else {
            new b(context, str3, aVar).show();
        }
    }

    public void a(Context context, String str, String[] strArr, final a aVar) {
        Bundle bundle = new Bundle();
        bundle.putString("client_id", str);
        if (strArr.length > 0) {
            bundle.putString("scope", TextUtils.join(",", strArr));
        }
        CookieSyncManager.createInstance(context);
        a(context, "login", bundle, new a() {
            public void a() {
                Log.d("Facebook-authorize", "Login cancelled");
                aVar.a();
            }

            public void a(Bundle bundle) {
                CookieSyncManager.getInstance().sync();
                d.this.a(bundle.getString("access_token"));
                d.this.b(bundle.getString("expires_in"));
                if (d.this.a()) {
                    Log.d("Facebook-authorize", "Login Success! access_token=" + d.this.b() + " expires=" + d.this.c());
                    aVar.a(bundle);
                    return;
                }
                a(new e("failed to receive access_token"));
            }

            public void a(c cVar) {
                Log.d("Facebook-authorize", "Login failed: " + cVar);
                aVar.a(cVar);
            }

            public void a(e eVar) {
                Log.d("Facebook-authorize", "Login failed: " + eVar);
                aVar.a(eVar);
            }
        });
    }

    public void a(String str) {
        this.e = str;
    }

    public boolean a() {
        return b() != null && (c() == 0 || System.currentTimeMillis() < c());
    }

    public String b() {
        return this.e;
    }

    public void b(String str) {
        if (str != null && !str.equals("0")) {
            a(System.currentTimeMillis() + ((long) (Integer.parseInt(str) * 1000)));
        }
    }

    public long c() {
        return this.f;
    }
}
