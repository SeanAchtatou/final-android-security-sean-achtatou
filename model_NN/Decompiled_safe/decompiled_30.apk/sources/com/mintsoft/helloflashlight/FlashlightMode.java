package com.mintsoft.helloflashlight;

public enum FlashlightMode {
    Lighting,
    EnergySaving
}
