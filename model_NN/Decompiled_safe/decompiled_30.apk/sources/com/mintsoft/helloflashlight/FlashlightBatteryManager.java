package com.mintsoft.helloflashlight;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class FlashlightBatteryManager extends BroadcastReceiver {
    private static final int[] BATTERY_LEVEL_STEPS;
    private final HelloFlashlightActivity mActivity;
    private int mBatteryLevel = -1;
    private long mChangingTimeSpan;
    private int mPreviousBatteryLevel = -1;
    private long mPreviousChangingTime;

    static {
        int[] iArr = new int[7];
        iArr[1] = 10;
        iArr[2] = 20;
        iArr[3] = 40;
        iArr[4] = 60;
        iArr[5] = 80;
        iArr[6] = 100;
        BATTERY_LEVEL_STEPS = iArr;
    }

    public FlashlightBatteryManager(HelloFlashlightActivity activity) {
        this.mActivity = activity;
        this.mPreviousChangingTime = System.currentTimeMillis();
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
            int level = intent.getIntExtra("level", 0);
            if (3 == intent.getIntExtra("health", 1)) {
                this.mActivity.exitForOverheat();
            }
            updateBatteryLevel(level);
        }
    }

    public void updateBatteryLevel(int level) {
        this.mPreviousBatteryLevel = this.mBatteryLevel;
        this.mBatteryLevel = level;
        if (this.mBatteryLevel != this.mPreviousBatteryLevel) {
            long currentTime = System.currentTimeMillis();
            this.mChangingTimeSpan = currentTime - this.mPreviousChangingTime;
            this.mPreviousChangingTime = currentTime;
            this.mActivity.invalidate();
        }
    }

    public String getBatteryLevelText() {
        if (this.mBatteryLevel < 0 || this.mBatteryLevel > 100) {
            return "N/A";
        }
        return String.valueOf(this.mBatteryLevel) + "%";
    }

    public Bitmap getBatteryLevelIcon() {
        int nearest = BATTERY_LEVEL_STEPS[0];
        for (int step : BATTERY_LEVEL_STEPS) {
            if (Math.abs(this.mBatteryLevel - step) < Math.abs(this.mBatteryLevel - nearest)) {
                nearest = step;
            }
        }
        Resources res = this.mActivity.getResources();
        return BitmapFactory.decodeResource(res, res.getIdentifier("ic_battery_" + nearest, "drawable", this.mActivity.getPackageName()));
    }

    public String getEstimatedLastingTime() {
        if (this.mBatteryLevel == -1 || this.mPreviousBatteryLevel == -1 || this.mBatteryLevel == this.mPreviousBatteryLevel) {
            return this.mActivity.getString(R.string.calculating);
        }
        int levelDelta = this.mPreviousBatteryLevel - this.mBatteryLevel;
        if (levelDelta < 0) {
            return this.mActivity.getString(R.string.charging);
        }
        int estimatedLastingSeconds = (((int) (this.mChangingTimeSpan / 1000)) * 100) / levelDelta;
        if (estimatedLastingSeconds < 60) {
            return "< 1 " + this.mActivity.getString(R.string.time_unit_min);
        }
        if (estimatedLastingSeconds < 120) {
            return "1 " + this.mActivity.getString(R.string.time_unit_min);
        }
        if (estimatedLastingSeconds < 3600) {
            return String.valueOf(estimatedLastingSeconds / 60) + " " + this.mActivity.getString(R.string.time_unit_mins);
        }
        if (estimatedLastingSeconds > 18000) {
            return "> 5 " + this.mActivity.getString(R.string.time_unit_hrs);
        }
        return String.valueOf(estimatedLastingSeconds / 3600) + " " + this.mActivity.getString(R.string.time_unit_hrs);
    }
}
