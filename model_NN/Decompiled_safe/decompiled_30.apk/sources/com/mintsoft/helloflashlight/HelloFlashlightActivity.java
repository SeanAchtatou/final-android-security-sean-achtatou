package com.mintsoft.helloflashlight;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class HelloFlashlightActivity extends Activity {
    private static final int MENU_ITEM_ID_HELP = 2;
    private static final int MENU_ITEM_ID_QUIT = 3;
    private static final int MENU_ITEM_ID_SETTINGS = 1;
    protected static final String PREFS_NAME = "com_mintsoft_helloflashlight_prefs";
    private static final String PREF_FIRST_TIME = "com_mintsoft_helloflashlight_pref_first_time";
    private ImageView mBatteryLevelIcon;
    private TextView mBatteryLevelTextView;
    private final FlashlightBatteryManager mBatteryManager = new FlashlightBatteryManager(this);
    private TextView mBrightnessLevelTextView;
    private final FlashlightBrightnessManager mBrightnessManager = new FlashlightBrightnessManager(this);
    private RelativeLayout mContentViewLinearLayout;
    private TextView mEstimatedLastingTimeTextView;
    private final GestureDetector mGestureDetector = new GestureDetector(this.mBrightnessManager);
    private AlertDialog mHelpDialog;
    private FlashlightMode mMode = FlashlightMode.Lighting;
    private RelativeLayout mStatusBarLinearLayout;
    private PowerManager.WakeLock mWakeLock;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        findViews();
        this.mHelpDialog = new AlertDialog.Builder(this).setTitle((int) R.string.menu_help).setMessage((int) R.string.help_content).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
        SharedPreferences prefs = getSharedPreferences("com_mintsoft_helloflashlight_prefs", 0);
        if (prefs.getBoolean(PREF_FIRST_TIME, true)) {
            prefs.edit().putBoolean(PREF_FIRST_TIME, false).commit();
            this.mHelpDialog.show();
        }
    }

    private void findViews() {
        this.mContentViewLinearLayout = (RelativeLayout) findViewById(R.id.contentViewRelativeLayout);
        this.mStatusBarLinearLayout = (RelativeLayout) findViewById(R.id.statusBarRelativeLayout);
        this.mBrightnessLevelTextView = (TextView) findViewById(R.id.brightnessLevelTextView);
        this.mBatteryLevelIcon = (ImageView) findViewById(R.id.batteryLevelIcon);
        this.mBatteryLevelTextView = (TextView) findViewById(R.id.batteryLevelTextView);
        this.mEstimatedLastingTimeTextView = (TextView) findViewById(R.id.estimatedLastingTimeTextView);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, (int) MENU_ITEM_ID_HELP, 0, (int) R.string.menu_help);
        menu.add(0, (int) MENU_ITEM_ID_QUIT, 0, (int) R.string.menu_quit);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ITEM_ID_SETTINGS /*1*/:
            default:
                return true;
            case MENU_ITEM_ID_HELP /*2*/:
                this.mHelpDialog.show();
                return true;
            case MENU_ITEM_ID_QUIT /*3*/:
                finish();
                return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mMode != FlashlightMode.EnergySaving) {
            if (keyCode == 25) {
                this.mBrightnessManager.decrementBrightness();
            } else if (keyCode == 24) {
                this.mBrightnessManager.incrementBrightness();
            } else if (keyCode != 4 || this.mMode != FlashlightMode.EnergySaving) {
                return super.onKeyDown(keyCode, event);
            } else {
                switchMode();
            }
            return MENU_ITEM_ID_SETTINGS;
        } else if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        } else {
            switchMode();
            return MENU_ITEM_ID_SETTINGS;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.mGestureDetector.onTouchEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        registerReceiver(this.mBatteryManager, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(10, "helloflashlight");
        this.mBrightnessManager.restoreBrightness();
        this.mWakeLock.acquire();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mMode == FlashlightMode.Lighting) {
            this.mBrightnessManager.backupBrightness();
        }
        this.mWakeLock.release();
        try {
            unregisterReceiver(this.mBatteryManager);
        } catch (Exception e) {
            Log.e("helloflashlight", "", e);
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.mHelpDialog.isShowing()) {
            this.mHelpDialog.dismiss();
        }
        super.onStop();
    }

    public FlashlightMode getMode() {
        return this.mMode;
    }

    public void switchMode() {
        if (this.mMode == FlashlightMode.Lighting) {
            this.mBrightnessManager.backupBrightness();
            this.mBrightnessManager.minimizeBrightness();
            this.mContentViewLinearLayout.setBackgroundColor(-16777216);
            this.mStatusBarLinearLayout.setVisibility(8);
            this.mMode = FlashlightMode.EnergySaving;
            return;
        }
        this.mBrightnessManager.restoreBrightness();
        this.mContentViewLinearLayout.setBackgroundColor(-1);
        this.mStatusBarLinearLayout.setVisibility(0);
        invalidate();
        this.mMode = FlashlightMode.Lighting;
    }

    public void invalidate() {
        this.mBrightnessLevelTextView.setText(this.mBrightnessManager.brightnessLevelText());
        this.mBatteryLevelIcon.setImageBitmap(this.mBatteryManager.getBatteryLevelIcon());
        this.mBatteryLevelTextView.setText(this.mBatteryManager.getBatteryLevelText());
        this.mEstimatedLastingTimeTextView.setText(this.mBatteryManager.getEstimatedLastingTime());
    }

    public void exitForOverheat() {
        Toast.makeText(this, getString(R.string.battery_overheat_exit), 0).show();
        finish();
    }
}
