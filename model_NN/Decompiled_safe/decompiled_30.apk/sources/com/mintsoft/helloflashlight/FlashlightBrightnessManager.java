package com.mintsoft.helloflashlight;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

public class FlashlightBrightnessManager implements GestureDetector.OnGestureListener {
    private static final float BRIGHTNESS_DECREMENT_DELTA = -0.1f;
    private static final float BRIGHTNESS_INCREMENT_DELTA = 0.2f;
    private static final float MIN_BRIGHTNESS = 0.1f;
    public static final String PREFS_NAME = "com_mintsoft_helloflashlight_prefs";
    private static final String PREF_BRIGHTNESS = "com_mintsoft_helloflashlight_pref_brightness";
    private final HelloFlashlightActivity mActivity;
    private float mBrightnessLevel = -1.0f;

    public FlashlightBrightnessManager(HelloFlashlightActivity activity) {
        this.mActivity = activity;
    }

    public String brightnessLevelText() {
        if (this.mBrightnessLevel < 0.0f || this.mBrightnessLevel > 1.0f) {
            return "N/A";
        }
        return String.valueOf(Math.round(100.0f * this.mBrightnessLevel)) + "%";
    }

    public void backupBrightness() {
        this.mActivity.getSharedPreferences(PREFS_NAME, 0).edit().putFloat(PREF_BRIGHTNESS, this.mBrightnessLevel).commit();
    }

    public void restoreBrightness() {
        this.mBrightnessLevel = this.mActivity.getSharedPreferences(PREFS_NAME, 0).getFloat(PREF_BRIGHTNESS, 1.0f);
        refreshBrightnessLevel();
    }

    public void incrementBrightness() {
        addBrightness(BRIGHTNESS_INCREMENT_DELTA);
    }

    public void decrementBrightness() {
        addBrightness(BRIGHTNESS_DECREMENT_DELTA);
    }

    public void maximizeBrightness() {
        addBrightness(10.0f);
    }

    public void minimizeBrightness() {
        addBrightness(-10.0f);
    }

    private void addBrightness(float delta) {
        this.mBrightnessLevel = ensureBrightnessRange(this.mBrightnessLevel + delta);
        refreshBrightnessLevel();
        this.mActivity.invalidate();
    }

    private void refreshBrightnessLevel() {
        Window win = this.mActivity.getWindow();
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.screenBrightness = this.mBrightnessLevel;
        win.setAttributes(lp);
    }

    private float ensureBrightnessRange(float value) {
        if (value > 1.0f) {
            return 1.0f;
        }
        if (value < MIN_BRIGHTNESS) {
            return MIN_BRIGHTNESS;
        }
        return value;
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (this.mActivity.getMode() == FlashlightMode.Lighting && Math.abs(e1.getY() - e2.getY()) > 150.0f && Math.abs(velocityY) > 200.0f) {
            if (e1.getY() > e2.getY()) {
                incrementBrightness();
            } else {
                decrementBrightness();
            }
            return true;
        } else if (Math.abs(e1.getX() - e2.getX()) <= 200.0f || Math.abs(velocityX) <= 400.0f) {
            return false;
        } else {
            this.mActivity.switchMode();
            return true;
        }
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }
}
