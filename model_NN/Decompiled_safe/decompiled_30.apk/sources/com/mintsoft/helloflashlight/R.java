package com.mintsoft.helloflashlight;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ic_battery_0 = 2130837504;
        public static final int ic_battery_10 = 2130837505;
        public static final int ic_battery_100 = 2130837506;
        public static final int ic_battery_20 = 2130837507;
        public static final int ic_battery_40 = 2130837508;
        public static final int ic_battery_60 = 2130837509;
        public static final int ic_battery_80 = 2130837510;
        public static final int ic_brightness = 2130837511;
        public static final int icon = 2130837512;
    }

    public static final class id {
        public static final int batteryLevelIcon = 2131034117;
        public static final int batteryLevelTextView = 2131034116;
        public static final int brightnessLevelIcon = 2131034114;
        public static final int brightnessLevelTextView = 2131034115;
        public static final int contentViewRelativeLayout = 2131034112;
        public static final int estimatedLastingTimeTextView = 2131034120;
        public static final int savingEnergyModeTextView = 2131034118;
        public static final int statusBarRelativeLayout = 2131034113;
        public static final int textView1 = 2131034121;
        public static final int textView2 = 2131034119;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int battery_overheat_exit = 2130968588;
        public static final int calculating = 2130968580;
        public static final int charging = 2130968581;
        public static final int energy_saving_mode = 2130968577;
        public static final int estimated_lasting_time = 2130968579;
        public static final int help_content = 2130968589;
        public static final int menu_help = 2130968586;
        public static final int menu_quit = 2130968587;
        public static final int menu_settings = 2130968585;
        public static final int slide_horizontally_to_exit = 2130968578;
        public static final int time_unit_hrs = 2130968584;
        public static final int time_unit_min = 2130968582;
        public static final int time_unit_mins = 2130968583;
    }
}
