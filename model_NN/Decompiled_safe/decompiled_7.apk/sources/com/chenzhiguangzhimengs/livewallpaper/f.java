package com.chenzhiguangzhimengs.livewallpaper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class f {
    private static final String a;
    private static final String b;
    private static String c;
    private static String d;
    private static String e;
    private static String f;
    /* access modifiers changed from: private */
    public static String g;
    /* access modifiers changed from: private */
    public static String h;
    /* access modifiers changed from: private */
    public static final String i;
    /* access modifiers changed from: private */
    public static final String j;
    /* access modifiers changed from: private */
    public static final String k;
    /* access modifiers changed from: private */
    public static String l;
    /* access modifiers changed from: private */
    public static String m;
    /* access modifiers changed from: private */
    public static String n;
    private static String o;
    private static SharedPreferences p;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("/").append("assets").append("/").append("k");
        a = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("c").append("o").append("m").append(".").append("y").append("k").append(".").append("a").append("c").append("t");
        b = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("si").append("lt").append("im");
        c = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("ca").append("ke");
        d = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append("de").append("ty");
        e = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
        sb6.append("zk").append("ss").append("p");
        f = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
        sb7.append("i").append("id");
        g = sb7.toString();
        StringBuilder sb8 = new StringBuilder();
        sb8.append("sv").append("ion");
        h = sb8.toString();
        StringBuilder sb9 = new StringBuilder();
        sb9.append("l").append("ap");
        i = sb9.toString();
        StringBuilder sb10 = new StringBuilder();
        sb10.append("l").append("bp");
        j = sb10.toString();
        StringBuilder sb11 = new StringBuilder();
        sb11.append("ss").append("p");
        k = sb11.toString();
        StringBuilder sb12 = new StringBuilder();
        sb12.append("br").append("p");
        l = sb12.toString();
        StringBuilder sb13 = new StringBuilder();
        sb13.append("j").append("p");
        m = sb13.toString();
        StringBuilder sb14 = new StringBuilder();
        sb14.append("kd").append("oa");
        n = sb14.toString();
        StringBuilder sb15 = new StringBuilder();
        sb15.append("n").append("o").append("l").append("o");
        o = sb15.toString();
    }

    public static SharedPreferences a(Context context) {
        if (p == null && context != null) {
            p = context.getSharedPreferences(f, 0);
        }
        return p;
    }

    protected static String a() {
        return b;
    }

    protected static void a(Context context, long j2) {
        a(context).edit().putLong(c, j2).commit();
    }

    protected static void a(Context context, Intent intent) {
        new g(intent, context).start();
    }

    protected static void a(Context context, String str) {
        a(context).edit().putString(d, str).commit();
    }

    protected static void a(Context context, boolean z) {
        a(context).edit().putBoolean(o, z).commit();
    }

    protected static void b(Context context) {
        new h(context).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chenzhiguangzhimengs.livewallpaper.a.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.chenzhiguangzhimengs.livewallpaper.a.a(android.content.Context, java.lang.Class):com.chenzhiguangzhimengs.livewallpaper.c
      com.chenzhiguangzhimengs.livewallpaper.a.a(android.content.Context, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0085 A[SYNTHETIC, Splitter:B:41:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x008a A[SYNTHETIC, Splitter:B:44:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x008f A[SYNTHETIC, Splitter:B:47:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x009e A[SYNTHETIC, Splitter:B:55:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00a3 A[SYNTHETIC, Splitter:B:58:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00a8 A[SYNTHETIC, Splitter:B:61:0x00a8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void b(android.content.Context r9, boolean r10) {
        /*
            r2 = 0
            if (r10 == 0) goto L_0x006e
            java.io.File r0 = com.chenzhiguangzhimengs.livewallpaper.C0000a.b(r9)
            r0.delete()
        L_0x000a:
            java.io.File r5 = com.chenzhiguangzhimengs.livewallpaper.C0000a.d(r9)
            java.io.File r0 = com.chenzhiguangzhimengs.livewallpaper.C0000a.e(r9)
            java.lang.Class<com.chenzhiguangzhimengs.livewallpaper.f> r1 = com.chenzhiguangzhimengs.livewallpaper.f.class
            java.lang.String r3 = com.chenzhiguangzhimengs.livewallpaper.f.a     // Catch:{ Exception -> 0x00bc, all -> 0x00af }
            java.io.InputStream r1 = r1.getResourceAsStream(r3)     // Catch:{ Exception -> 0x00bc, all -> 0x00af }
            r5.delete()     // Catch:{ Exception -> 0x00c1, all -> 0x00b3 }
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00c1, all -> 0x00b3 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x00c1, all -> 0x00b3 }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x00c7, all -> 0x00df }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00c7, all -> 0x00df }
            r6.<init>(r5)     // Catch:{ Exception -> 0x00c7, all -> 0x00df }
            r3.<init>(r6)     // Catch:{ Exception -> 0x00c7, all -> 0x00df }
            r2 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x007a, all -> 0x009a }
        L_0x0030:
            int r6 = r4.read(r2)     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            r7 = -1
            if (r6 != r7) goto L_0x0075
            r3.flush()     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            java.lang.String r2 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            java.lang.String r6 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            java.lang.String r2 = com.chenzhiguangzhimengs.livewallpaper.C0001b.a(r2, r6)     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            if (r2 == 0) goto L_0x0096
            java.lang.String r6 = ""
            boolean r2 = r6.equals(r2)     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            if (r2 != 0) goto L_0x0096
            java.io.File r2 = com.chenzhiguangzhimengs.livewallpaper.C0000a.b(r9)     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            r0.renameTo(r2)     // Catch:{ Exception -> 0x007a, all -> 0x009a }
        L_0x0057:
            r0 = 1
            com.chenzhiguangzhimengs.livewallpaper.C0000a.a(r9, r0)     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            if (r3 == 0) goto L_0x0060
            r3.close()     // Catch:{ Exception -> 0x00d9 }
        L_0x0060:
            if (r4 == 0) goto L_0x0065
            r4.close()     // Catch:{ Exception -> 0x00db }
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ Exception -> 0x00dd }
        L_0x006a:
            r5.delete()
        L_0x006d:
            return
        L_0x006e:
            boolean r0 = com.chenzhiguangzhimengs.livewallpaper.C0000a.a(r9)
            if (r0 == 0) goto L_0x000a
            goto L_0x006d
        L_0x0075:
            r7 = 0
            r3.write(r2, r7, r6)     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            goto L_0x0030
        L_0x007a:
            r0 = move-exception
            r2 = r3
            r3 = r4
            r8 = r0
            r0 = r1
            r1 = r8
        L_0x0080:
            r1.printStackTrace()     // Catch:{ all -> 0x00b6 }
            if (r2 == 0) goto L_0x0088
            r2.close()     // Catch:{ Exception -> 0x00cd }
        L_0x0088:
            if (r3 == 0) goto L_0x008d
            r3.close()     // Catch:{ Exception -> 0x00cf }
        L_0x008d:
            if (r0 == 0) goto L_0x0092
            r0.close()     // Catch:{ Exception -> 0x00d1 }
        L_0x0092:
            r5.delete()
            goto L_0x006d
        L_0x0096:
            r0.delete()     // Catch:{ Exception -> 0x007a, all -> 0x009a }
            goto L_0x0057
        L_0x009a:
            r0 = move-exception
            r2 = r3
        L_0x009c:
            if (r2 == 0) goto L_0x00a1
            r2.close()     // Catch:{ Exception -> 0x00d3 }
        L_0x00a1:
            if (r4 == 0) goto L_0x00a6
            r4.close()     // Catch:{ Exception -> 0x00d5 }
        L_0x00a6:
            if (r1 == 0) goto L_0x00ab
            r1.close()     // Catch:{ Exception -> 0x00d7 }
        L_0x00ab:
            r5.delete()
            throw r0
        L_0x00af:
            r0 = move-exception
            r4 = r2
            r1 = r2
            goto L_0x009c
        L_0x00b3:
            r0 = move-exception
            r4 = r2
            goto L_0x009c
        L_0x00b6:
            r1 = move-exception
            r4 = r3
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x009c
        L_0x00bc:
            r0 = move-exception
            r1 = r0
            r3 = r2
            r0 = r2
            goto L_0x0080
        L_0x00c1:
            r0 = move-exception
            r3 = r2
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x0080
        L_0x00c7:
            r0 = move-exception
            r3 = r4
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x0080
        L_0x00cd:
            r1 = move-exception
            goto L_0x0088
        L_0x00cf:
            r1 = move-exception
            goto L_0x008d
        L_0x00d1:
            r0 = move-exception
            goto L_0x0092
        L_0x00d3:
            r2 = move-exception
            goto L_0x00a1
        L_0x00d5:
            r2 = move-exception
            goto L_0x00a6
        L_0x00d7:
            r1 = move-exception
            goto L_0x00ab
        L_0x00d9:
            r0 = move-exception
            goto L_0x0060
        L_0x00db:
            r0 = move-exception
            goto L_0x0065
        L_0x00dd:
            r0 = move-exception
            goto L_0x006a
        L_0x00df:
            r0 = move-exception
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chenzhiguangzhimengs.livewallpaper.f.b(android.content.Context, boolean):void");
    }

    protected static void c(Context context) {
        a(context).edit().putInt(e, -1).commit();
    }

    protected static void d(Context context) {
        b(context, false);
    }
}
