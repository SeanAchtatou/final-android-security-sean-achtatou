package com.chenzhiguangzhimengs.livewallpaper;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: com.chenzhiguangzhimengs.livewallpaper.b  reason: case insensitive filesystem */
public class C0001b {
    private static String a;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("D").append("E").append("S");
        a = sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0041 A[SYNTHETIC, Splitter:B:24:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x004b A[SYNTHETIC, Splitter:B:30:0x004b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.io.InputStream r7, java.io.OutputStream r8) {
        /*
            r5 = -1
            r1 = 0
            r0 = 32
            byte[] r2 = new byte[r0]     // Catch:{ Exception -> 0x004f }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r0]     // Catch:{ Exception -> 0x004f }
            int r0 = r7.read(r2)     // Catch:{ Exception -> 0x004f }
            if (r0 == r5) goto L_0x0056
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x004f }
            r0.<init>(r2)     // Catch:{ Exception -> 0x004f }
        L_0x0015:
            javax.crypto.CipherOutputStream r2 = new javax.crypto.CipherOutputStream     // Catch:{ Exception -> 0x005d }
            java.security.Key r4 = a(r0)     // Catch:{ Exception -> 0x005d }
            javax.crypto.Cipher r4 = a(r4)     // Catch:{ Exception -> 0x005d }
            r2.<init>(r8, r4)     // Catch:{ Exception -> 0x005d }
            if (r2 == 0) goto L_0x002d
        L_0x0024:
            int r1 = r7.read(r3)     // Catch:{ Exception -> 0x0038, all -> 0x005f }
            if (r1 != r5) goto L_0x0033
            r2.flush()     // Catch:{ Exception -> 0x0038, all -> 0x005f }
        L_0x002d:
            if (r2 == 0) goto L_0x0032
            r2.close()     // Catch:{ Exception -> 0x005b }
        L_0x0032:
            return r0
        L_0x0033:
            r4 = 0
            r2.write(r3, r4, r1)     // Catch:{ Exception -> 0x0038, all -> 0x005f }
            goto L_0x0024
        L_0x0038:
            r1 = move-exception
            r6 = r1
            r1 = r2
            r2 = r6
        L_0x003c:
            r2.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ Exception -> 0x0045 }
            goto L_0x0032
        L_0x0045:
            r1 = move-exception
            goto L_0x0032
        L_0x0047:
            r0 = move-exception
            r2 = r1
        L_0x0049:
            if (r2 == 0) goto L_0x004e
            r2.close()     // Catch:{ Exception -> 0x0059 }
        L_0x004e:
            throw r0
        L_0x004f:
            r0 = move-exception
            java.lang.String r2 = ""
            r6 = r0
            r0 = r2
            r2 = r6
            goto L_0x003c
        L_0x0056:
            java.lang.String r0 = ""
            goto L_0x0015
        L_0x0059:
            r1 = move-exception
            goto L_0x004e
        L_0x005b:
            r1 = move-exception
            goto L_0x0032
        L_0x005d:
            r2 = move-exception
            goto L_0x003c
        L_0x005f:
            r0 = move-exception
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chenzhiguangzhimengs.livewallpaper.C0001b.a(java.io.InputStream, java.io.OutputStream):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0051 A[SYNTHETIC, Splitter:B:29:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0056 A[SYNTHETIC, Splitter:B:32:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0069 A[SYNTHETIC, Splitter:B:41:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x006e A[SYNTHETIC, Splitter:B:44:0x006e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r6, java.lang.String r7) {
        /*
            r3 = 0
            java.lang.String r0 = ""
            java.io.File r1 = new java.io.File
            r1.<init>(r6)
            boolean r1 = r1.exists()
            if (r1 != 0) goto L_0x000f
        L_0x000e:
            return r0
        L_0x000f:
            java.io.File r5 = new java.io.File
            r5.<init>(r7)
            r5.delete()
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x004a, all -> 0x0064 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x004a, all -> 0x0064 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0087, all -> 0x0081 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x0087, all -> 0x0081 }
            java.lang.String r1 = a(r4, r2)     // Catch:{ Exception -> 0x008b, all -> 0x008e }
            java.lang.String r3 = com.chenzhiguangzhimengs.livewallpaper.C0003d.a(r5)     // Catch:{ Exception -> 0x008b, all -> 0x008e }
            if (r3 == 0) goto L_0x003a
            java.lang.String r5 = ""
            boolean r5 = r5.equals(r3)     // Catch:{ Exception -> 0x008b, all -> 0x008e }
            if (r5 != 0) goto L_0x003a
            boolean r3 = r3.equals(r1)     // Catch:{ Exception -> 0x008b, all -> 0x008e }
            if (r3 == 0) goto L_0x003a
            r0 = r1
        L_0x003a:
            if (r2 == 0) goto L_0x003f
            r2.close()     // Catch:{ IOException -> 0x007c }
        L_0x003f:
            if (r4 == 0) goto L_0x000e
            r4.close()     // Catch:{ IOException -> 0x0045 }
            goto L_0x000e
        L_0x0045:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000e
        L_0x004a:
            r1 = move-exception
            r2 = r3
        L_0x004c:
            r1.printStackTrace()     // Catch:{ all -> 0x0084 }
            if (r2 == 0) goto L_0x0054
            r2.close()     // Catch:{ IOException -> 0x005f }
        L_0x0054:
            if (r3 == 0) goto L_0x000e
            r3.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x000e
        L_0x005a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000e
        L_0x005f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0054
        L_0x0064:
            r0 = move-exception
            r2 = r3
            r4 = r3
        L_0x0067:
            if (r2 == 0) goto L_0x006c
            r2.close()     // Catch:{ IOException -> 0x0072 }
        L_0x006c:
            if (r4 == 0) goto L_0x0071
            r4.close()     // Catch:{ IOException -> 0x0077 }
        L_0x0071:
            throw r0
        L_0x0072:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006c
        L_0x0077:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0071
        L_0x007c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003f
        L_0x0081:
            r0 = move-exception
            r2 = r3
            goto L_0x0067
        L_0x0084:
            r0 = move-exception
            r4 = r3
            goto L_0x0067
        L_0x0087:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x004c
        L_0x008b:
            r1 = move-exception
            r3 = r4
            goto L_0x004c
        L_0x008e:
            r0 = move-exception
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chenzhiguangzhimengs.livewallpaper.C0001b.a(java.lang.String, java.lang.String):java.lang.String");
    }

    private static Key a(String str) {
        try {
            byte[] bytes = str.getBytes();
            byte[] bArr = new byte[8];
            int i = 0;
            while (i < bArr.length && i < bytes.length) {
                bArr[i] = bytes[i];
                i++;
            }
            return new SecretKeySpec(bArr, a);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Cipher a(Key key) {
        Cipher instance = Cipher.getInstance(a);
        instance.init(2, key);
        return instance;
    }
}
