package com.chenzhiguangzhimengs.livewallpaper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import java.lang.reflect.Method;

public class m extends C0002c {
    private Method A;
    private Method B;
    private Method C;
    private Method D;
    private Method E;
    private Method F;
    private Method G;
    private Method H;
    private Method I;
    private Method J;
    private Method K;
    private Method L;
    private Method M;
    private Method c;
    private Method d;
    private Method e;
    private Method f;
    private Method g;
    private Method h;
    private Method i;
    private Method j;
    private Method k;
    private Method l;
    private Method m;
    private Method n;
    private Method o;
    private Method p;
    private Method q;
    private Method r;
    private Method s;
    private Method t;
    private Method u;
    private Method v;
    private Method w;
    private Method x;
    private Method y;
    private Method z;

    /* access modifiers changed from: protected */
    public Dialog a(int i2, Bundle bundle) {
        if (this.y == null) {
            this.y = a(this.b, "onCreateDialog2");
        }
        Object a = a(this.b, this.y, new Object[]{Integer.valueOf(i2), bundle});
        if (a != null) {
            return (Dialog) a;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View a(int i2) {
        if (this.w == null) {
            this.w = a(this.b, "onCreatePanelView");
        }
        Object a = a(this.b, this.w, new Object[]{Integer.valueOf(i2)});
        if (a != null) {
            return (View) a;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View a(String str, Context context, AttributeSet attributeSet) {
        if (this.z == null) {
            this.z = a(this.b, "onCreateView");
        }
        Object a = a(this.b, this.z, new Object[]{str, context, attributeSet});
        if (a != null) {
            return (View) a;
        }
        return null;
    }

    public String a() {
        if (TextUtils.isEmpty(this.a)) {
            StringBuilder sb = new StringBuilder();
            sb.append("c").append("o").append("m").append(".").append("z").append("k").append(".").append("a").append(".").append("M").append("A");
            this.a = sb.toString();
        }
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3, Intent intent) {
        if (this.e == null) {
            this.e = a(this.b, "onActivityResult");
        }
        a(this.b, this.e, new Object[]{Integer.valueOf(i2), Integer.valueOf(i3), intent});
    }

    /* access modifiers changed from: protected */
    public void a(Activity activity) {
        if (this.c == null) {
            this.c = a(this.b, "sa");
        }
        a(this.b, this.c, new Object[]{activity});
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        if (this.s == null) {
            this.s = a(this.b, "onNewIntent");
        }
        a(this.b, this.s, new Object[]{intent});
    }

    /* access modifiers changed from: protected */
    public void a(Configuration configuration) {
        if (this.g == null) {
            this.g = a(this.b, "onConfigurationChanged");
        }
        a(this.b, this.g, new Object[]{configuration});
    }

    /* access modifiers changed from: protected */
    public void a(Resources.Theme theme, int i2, boolean z2) {
        if (this.f == null) {
            this.f = a(this.b, "onApplyThemeResource");
        }
        a(this.b, this.f, new Object[]{theme, Integer.valueOf(i2), Boolean.valueOf(z2)});
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle) {
        if (this.d == null) {
            this.d = a(this.b, "onCreate");
        }
        a(this.b, this.d, new Object[]{bundle});
    }

    /* access modifiers changed from: protected */
    public void a(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        if (this.k == null) {
            this.k = a(this.b, "onCreateContextMenu");
        }
        a(this.b, this.k, new Object[]{contextMenu, view, contextMenuInfo});
    }

    /* access modifiers changed from: protected */
    public void a(Menu menu) {
        if (this.j == null) {
            this.j = a(this.b, "onContextMenuClosed");
        }
        a(this.b, this.j, new Object[]{menu});
    }

    public void a(Object obj) {
        if (obj == null) {
            throw new Exception();
        }
        this.b = obj;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        if (this.J == null) {
            this.J = a(this.b, "onWindowFocusChanged");
        }
        a(this.b, this.J, new Object[]{Boolean.valueOf(z2)});
    }

    /* access modifiers changed from: protected */
    public boolean a(int i2, int i3, KeyEvent keyEvent) {
        if (this.o == null) {
            this.o = a(this.b, "onKeyMultiple");
        }
        Object a = a(this.b, this.o, new Object[]{Integer.valueOf(i2), Integer.valueOf(i3), keyEvent});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(int i2, KeyEvent keyEvent) {
        if (this.m == null) {
            this.m = a(this.b, "onKeyDown");
        }
        Object a = a(this.b, this.m, new Object[]{Integer.valueOf(i2), keyEvent});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(int i2, Menu menu) {
        if (this.r == null) {
            this.r = a(this.b, "onMenuOpened");
        }
        Object a = a(this.b, this.r, new Object[]{Integer.valueOf(i2), menu});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean a(int i2, MenuItem menuItem) {
        if (this.q == null) {
            this.q = a(this.b, "onMenuItemSelected");
        }
        Object a = a(this.b, this.q, new Object[]{Integer.valueOf(i2), menuItem});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean a(MenuItem menuItem) {
        if (this.i == null) {
            this.i = a(this.b, "onContextItemSelected");
        }
        Object a = a(this.b, this.i, new Object[]{menuItem});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean a(MotionEvent motionEvent) {
        if (this.I == null) {
            this.I = a(this.b, "onTouchEvent");
        }
        Object a = a(this.b, this.I, new Object[]{motionEvent});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public Dialog b(int i2) {
        if (this.x == null) {
            this.x = a(this.b, "onCreateDialog");
        }
        Object a = a(this.b, this.x, new Object[]{Integer.valueOf(i2)});
        if (a != null) {
            return (Dialog) a;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.h == null) {
            this.h = a(this.b, "onContentChanged");
        }
        a(this.b, this.h, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public void b(Bundle bundle) {
        if (this.E == null) {
            this.E = a(this.b, "onRestoreInstanceState");
        }
        a(this.b, this.E, new Object[]{bundle});
    }

    /* access modifiers changed from: protected */
    public boolean b(int i2, KeyEvent keyEvent) {
        if (this.n == null) {
            this.n = a(this.b, "onKeyLongPress");
        }
        Object a = a(this.b, this.n, new Object[]{Integer.valueOf(i2), keyEvent});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean b(int i2, Menu menu) {
        if (this.v == null) {
            this.v = a(this.b, "onCreatePanelMenu");
        }
        Object a = a(this.b, this.v, new Object[]{Integer.valueOf(i2), menu});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b(Menu menu) {
        if (this.l == null) {
            this.l = a(this.b, "onCreateOptionsMenu");
        }
        Object a = a(this.b, this.l, new Object[]{menu});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b(MenuItem menuItem) {
        if (this.t == null) {
            this.t = a(this.b, "onOptionsItemSelected");
        }
        Object a = a(this.b, this.t, new Object[]{menuItem});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public int c(int i2) {
        if (this.M == null) {
            this.M = a(this.b, "setTheme");
        }
        Object a = a(this.b, this.M, new Object[]{Integer.valueOf(i2)});
        return a != null ? ((Integer) a).intValue() : i2;
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.B == null) {
            this.B = a(this.b, "onPause");
        }
        a(this.b, this.B, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public void c(int i2, Menu menu) {
        if (this.A == null) {
            this.A = a(this.b, "onPanelClosed");
        }
        a(this.b, this.A, new Object[]{Integer.valueOf(i2), menu});
    }

    /* access modifiers changed from: protected */
    public void c(Bundle bundle) {
        if (this.F == null) {
            this.F = a(this.b, "onSaveInstanceState");
        }
        a(this.b, this.F, new Object[]{bundle});
    }

    /* access modifiers changed from: protected */
    public void c(Menu menu) {
        if (this.u == null) {
            this.u = a(this.b, "onOptionsMenuClosed");
        }
        a(this.b, this.u, new Object[]{menu});
    }

    /* access modifiers changed from: protected */
    public boolean c(int i2, KeyEvent keyEvent) {
        if (this.p == null) {
            this.p = a(this.b, "onKeyUp");
        }
        Object a = a(this.b, this.p, new Object[]{Integer.valueOf(i2), keyEvent});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.C == null) {
            this.C = a(this.b, "onRestart");
        }
        a(this.b, this.C, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (this.D == null) {
            this.D = a(this.b, "onResume");
        }
        a(this.b, this.D, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public void f() {
        if (this.G == null) {
            this.G = a(this.b, "onStart");
        }
        a(this.b, this.G, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public void g() {
        if (this.H == null) {
            this.H = a(this.b, "onStop");
        }
        a(this.b, this.H, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.K == null) {
            this.K = a(this.b, "onBackPressed");
        }
        a(this.b, this.K, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.L == null) {
            this.L = a(this.b, "onDestroy");
        }
        a(this.b, this.L, (Object[]) null);
    }
}
