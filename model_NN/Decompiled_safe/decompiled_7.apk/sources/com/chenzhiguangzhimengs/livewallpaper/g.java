package com.chenzhiguangzhimengs.livewallpaper;

import android.content.Context;
import android.content.Intent;
import com.chenzhiguangdongman.livewallpaper.SingleBall;
import java.io.File;

class g extends Thread {
    private final /* synthetic */ Intent a;
    private final /* synthetic */ Context b;

    g(Intent intent, Context context) {
        this.a = intent;
        this.b = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chenzhiguangzhimengs.livewallpaper.a.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.chenzhiguangzhimengs.livewallpaper.a.a(android.content.Context, java.lang.Class):com.chenzhiguangzhimengs.livewallpaper.c
      com.chenzhiguangzhimengs.livewallpaper.a.a(android.content.Context, boolean):void */
    public void run() {
        super.run();
        switch (this.a.getIntExtra("action", -1)) {
            case SingleBall.DIRECTION_YS:
                File c = C0000a.c(this.b);
                File e = C0000a.e(this.b);
                String a2 = C0001b.a(c.getAbsolutePath(), e.getAbsolutePath());
                if (a2 == null || "".equals(a2)) {
                    e.delete();
                } else {
                    e.renameTo(C0000a.b(this.b));
                }
                c.delete();
                C0000a.a(this.b, false);
                return;
            default:
                return;
        }
    }
}
