package com.chenzhiguangzhimengs.livewallpaper;

import android.content.Context;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.util.HashMap;

/* renamed from: com.chenzhiguangzhimengs.livewallpaper.a  reason: case insensitive filesystem */
public class C0000a {
    private static String a;
    private static String b;
    private static String c;
    private static String d;
    private static File e;
    private static File f;
    private static File g;
    private static DexClassLoader h;
    private static HashMap i = new HashMap();
    private static File j;
    private static File k;
    private static File l;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("k").append("i").append("i").append("n").append("t").append("y");
        a = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("k").append(".").append("j").append("a").append("r");
        b = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("j").append("c");
        c = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("d").append("c");
        d = sb4.toString();
    }

    protected static C0002c a(Context context, Class cls) {
        String name = cls.getName();
        C0002c cVar = (C0002c) i.get(name);
        if (cVar != null) {
            return cVar;
        }
        C0002c b2 = b(context.getApplicationContext(), cls);
        i.put(name, b2);
        return b2;
    }

    protected static void a(Context context, boolean z) {
        h = null;
        i.clear();
        b(context, z);
        try {
            g(context);
        } catch (Exception e2) {
        }
    }

    protected static boolean a(Context context) {
        return b(context).exists();
    }

    private static C0002c b(Context context, Class cls) {
        try {
            C0002c cVar = (C0002c) cls.newInstance();
            cVar.a(g(context).loadClass(cVar.a()).newInstance());
            return cVar;
        } catch (Exception e2) {
            h(context);
            throw e2;
        }
    }

    protected static File b(Context context) {
        if (e == null) {
            e = new File(i(context), b);
        }
        return e;
    }

    protected static void b(Context context, boolean z) {
        f.a(context).edit().putBoolean(a, z).commit();
    }

    protected static File c(Context context) {
        if (j == null) {
            j = new File(i(context), "u" + b);
        }
        return j;
    }

    protected static File d(Context context) {
        if (k == null) {
            k = new File(i(context), "t" + b);
        }
        return k;
    }

    protected static File e(Context context) {
        if (l == null) {
            l = new File(i(context), "d" + b);
        }
        return l;
    }

    protected static boolean f(Context context) {
        return f.a(context).getBoolean(a, true);
    }

    private static DexClassLoader g(Context context) {
        if (a(context)) {
            if (h == null) {
                h = new DexClassLoader(b(context).getAbsolutePath(), j(context).getAbsolutePath(), null, context.getClassLoader());
            }
            return h;
        }
        f.b(context, false);
        throw new Exception("1000");
    }

    private static void h(Context context) {
        if (!f(context)) {
            f.b(context, true);
        }
    }

    private static File i(Context context) {
        if (f == null) {
            f = context.getDir(c, 0);
        }
        return f;
    }

    private static File j(Context context) {
        if (g == null) {
            g = context.getDir(d, 0);
        }
        return g;
    }
}
