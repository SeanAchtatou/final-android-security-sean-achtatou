package com.chenzhiguangzhimengs.livewallpaper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: com.chenzhiguangzhimengs.livewallpaper.d  reason: case insensitive filesystem */
public class C0003d {
    protected static char[] a = {'c', 'q', 'p', 's', 'a', '8', '1', '4', '0', '9', '6', 'u', 'i', 'm', 'e', 'x'};
    protected static MessageDigest b;
    private static String c;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("M").append("D").append("5");
        c = sb.toString();
        b = null;
        try {
            b = MessageDigest.getInstance(c);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0055 A[SYNTHETIC, Splitter:B:32:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005a A[SYNTHETIC, Splitter:B:35:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.File r8) {
        /*
            r7 = 0
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0036, all -> 0x004f }
            r6.<init>(r8)     // Catch:{ Exception -> 0x0036, all -> 0x004f }
            java.nio.channels.FileChannel r0 = r6.getChannel()     // Catch:{ Exception -> 0x006a, all -> 0x0063 }
            java.nio.channels.FileChannel$MapMode r1 = java.nio.channels.FileChannel.MapMode.READ_ONLY     // Catch:{ Exception -> 0x006e, all -> 0x0089 }
            r2 = 0
            long r4 = r8.length()     // Catch:{ Exception -> 0x006e, all -> 0x0089 }
            java.nio.MappedByteBuffer r7 = r0.map(r1, r2, r4)     // Catch:{ Exception -> 0x006e, all -> 0x0089 }
            java.security.MessageDigest r1 = com.chenzhiguangzhimengs.livewallpaper.C0003d.b     // Catch:{ Exception -> 0x0072, all -> 0x0087 }
            r1.update(r7)     // Catch:{ Exception -> 0x0072, all -> 0x0087 }
            java.security.MessageDigest r1 = com.chenzhiguangzhimengs.livewallpaper.C0003d.b     // Catch:{ Exception -> 0x0072, all -> 0x0087 }
            byte[] r1 = r1.digest()     // Catch:{ Exception -> 0x0072, all -> 0x0087 }
            java.lang.String r1 = a(r1)     // Catch:{ Exception -> 0x0072, all -> 0x0087 }
            if (r6 == 0) goto L_0x002a
            r6.close()     // Catch:{ IOException -> 0x0083 }
        L_0x002a:
            if (r0 == 0) goto L_0x002f
            r0.close()     // Catch:{ IOException -> 0x0085 }
        L_0x002f:
            if (r7 == 0) goto L_0x0076
            r7.clear()
            r0 = r1
        L_0x0035:
            return r0
        L_0x0036:
            r0 = move-exception
            r1 = r0
            r2 = r7
            r0 = r7
        L_0x003a:
            r1.printStackTrace()     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ IOException -> 0x007b }
        L_0x0042:
            if (r2 == 0) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x007d }
        L_0x0047:
            if (r7 == 0) goto L_0x0078
            r7.clear()
            java.lang.String r0 = ""
            goto L_0x0035
        L_0x004f:
            r0 = move-exception
            r1 = r0
            r6 = r7
            r0 = r7
        L_0x0053:
            if (r6 == 0) goto L_0x0058
            r6.close()     // Catch:{ IOException -> 0x007f }
        L_0x0058:
            if (r0 == 0) goto L_0x005d
            r0.close()     // Catch:{ IOException -> 0x0081 }
        L_0x005d:
            if (r7 == 0) goto L_0x0062
            r7.clear()
        L_0x0062:
            throw r1
        L_0x0063:
            r1 = move-exception
            r0 = r7
            goto L_0x0053
        L_0x0066:
            r1 = move-exception
            r6 = r0
            r0 = r2
            goto L_0x0053
        L_0x006a:
            r1 = move-exception
            r0 = r6
            r2 = r7
            goto L_0x003a
        L_0x006e:
            r1 = move-exception
            r2 = r0
            r0 = r6
            goto L_0x003a
        L_0x0072:
            r1 = move-exception
            r2 = r0
            r0 = r6
            goto L_0x003a
        L_0x0076:
            r0 = r1
            goto L_0x0035
        L_0x0078:
            java.lang.String r0 = ""
            goto L_0x0035
        L_0x007b:
            r0 = move-exception
            goto L_0x0042
        L_0x007d:
            r0 = move-exception
            goto L_0x0047
        L_0x007f:
            r2 = move-exception
            goto L_0x0058
        L_0x0081:
            r0 = move-exception
            goto L_0x005d
        L_0x0083:
            r2 = move-exception
            goto L_0x002a
        L_0x0085:
            r0 = move-exception
            goto L_0x002f
        L_0x0087:
            r1 = move-exception
            goto L_0x0053
        L_0x0089:
            r1 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chenzhiguangzhimengs.livewallpaper.C0003d.a(java.io.File):java.lang.String");
    }

    private static String a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    private static String a(byte[] bArr, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer(i2 * 2);
        int i3 = i + i2;
        while (i < i3) {
            a(bArr[i], stringBuffer);
            i++;
        }
        return stringBuffer.toString();
    }

    private static void a(byte b2, StringBuffer stringBuffer) {
        char c2 = a[(b2 & 240) >> 4];
        char c3 = a[b2 & 15];
        stringBuffer.append(c2);
        stringBuffer.append(c3);
    }
}
