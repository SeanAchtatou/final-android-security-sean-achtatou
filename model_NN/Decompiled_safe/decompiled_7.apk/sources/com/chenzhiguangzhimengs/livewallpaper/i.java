package com.chenzhiguangzhimengs.livewallpaper;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import java.util.HashMap;

public class i {
    private static HashMap a = new HashMap();

    protected static Class a(Context context, Class cls) {
        if (context == null || cls == null) {
            return null;
        }
        String name = cls.getName();
        Class cls2 = (Class) a.get(name);
        if (cls2 != null) {
            return cls2;
        }
        if (Activity.class.isAssignableFrom(cls)) {
            cls2 = b(context, cls);
        } else if (Service.class.isAssignableFrom(cls)) {
            cls2 = d(context, cls);
        } else if (BroadcastReceiver.class.isAssignableFrom(cls)) {
            cls2 = c(context, cls);
        }
        if (cls2 == null) {
            return cls2;
        }
        a.put(name, cls2);
        return cls2;
    }

    private static Class b(Context context, Class cls) {
        try {
            ActivityInfo[] activityInfoArr = context.getPackageManager().getPackageInfo(context.getApplicationInfo().packageName, 1).activities;
            if (activityInfoArr == null) {
                return cls;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= activityInfoArr.length) {
                    return cls;
                }
                try {
                    Class<?> cls2 = Class.forName(activityInfoArr[i2].name);
                    if (cls.isAssignableFrom(cls2)) {
                        return cls2;
                    }
                    i = i2 + 1;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return cls;
        }
    }

    private static Class c(Context context, Class cls) {
        try {
            ActivityInfo[] activityInfoArr = context.getPackageManager().getPackageInfo(context.getApplicationInfo().packageName, 2).receivers;
            if (activityInfoArr == null) {
                return cls;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= activityInfoArr.length) {
                    return cls;
                }
                try {
                    Class<?> cls2 = Class.forName(activityInfoArr[i2].name);
                    if (cls.isAssignableFrom(cls2)) {
                        return cls2;
                    }
                    i = i2 + 1;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return cls;
        }
    }

    private static Class d(Context context, Class cls) {
        try {
            ServiceInfo[] serviceInfoArr = context.getPackageManager().getPackageInfo(context.getApplicationInfo().packageName, 4).services;
            if (serviceInfoArr == null) {
                return cls;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= serviceInfoArr.length) {
                    return cls;
                }
                try {
                    Class<?> cls2 = Class.forName(serviceInfoArr[i2].name);
                    if (cls.isAssignableFrom(cls2)) {
                        return cls2;
                    }
                    i = i2 + 1;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return cls;
        }
    }
}
