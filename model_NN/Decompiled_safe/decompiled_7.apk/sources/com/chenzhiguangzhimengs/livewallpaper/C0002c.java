package com.chenzhiguangzhimengs.livewallpaper;

import java.lang.reflect.Method;

/* renamed from: com.chenzhiguangzhimengs.livewallpaper.c  reason: case insensitive filesystem */
public abstract class C0002c {
    protected String a;
    protected Object b;

    /* access modifiers changed from: protected */
    public Object a(Object obj, Method method, Object[] objArr) {
        if (obj == null || method == null) {
            return null;
        }
        try {
            method.setAccessible(true);
            return method.invoke(obj, objArr);
        } catch (Exception e) {
            return null;
        }
    }

    public abstract String a();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chenzhiguangzhimengs.livewallpaper.c.a(java.lang.Class, java.lang.String):java.lang.Class[]
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      com.chenzhiguangzhimengs.livewallpaper.c.a(java.lang.Object, java.lang.String):java.lang.reflect.Method
      com.chenzhiguangzhimengs.livewallpaper.c.a(java.lang.Class, java.lang.String):java.lang.Class[] */
    /* access modifiers changed from: protected */
    public Method a(Object obj, String str) {
        try {
            Class<?> cls = obj.getClass();
            return cls.getMethod(str, a((Class) cls, str));
        } catch (Exception e) {
            return null;
        }
    }

    public abstract void a(Object obj);

    /* access modifiers changed from: protected */
    public Class[] a(Class cls, String str) {
        Method[] declaredMethods = cls.getDeclaredMethods();
        int length = declaredMethods.length;
        Class<?>[] clsArr = null;
        for (int i = 0; i < length; i++) {
            if (declaredMethods[i].getName().equals(str)) {
                clsArr = declaredMethods[i].getParameterTypes();
            }
        }
        return clsArr;
    }
}
