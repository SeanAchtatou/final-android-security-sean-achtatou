package com.chenzhiguangzhimengs.livewallpaper;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;

public class ZKS extends Service {
    private o a;

    private o a() {
        if (this.a == null) {
            this.a = (o) C0000a.a(this, o.class);
            this.a.a((Service) this);
        }
        return this.a;
    }

    public IBinder onBind(Intent intent) {
        try {
            return a().a(intent);
        } catch (Exception e) {
            return null;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            a().a(configuration);
        } catch (Exception e) {
        }
    }

    public void onCreate() {
        try {
            a().b();
        } catch (Exception e) {
        }
    }

    public void onDestroy() {
        try {
            a().d();
        } catch (Exception e) {
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        try {
            a().c();
        } catch (Exception e) {
        }
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        try {
            a().c(intent);
        } catch (Exception e) {
        }
    }

    public void onStart(Intent intent, int i) {
        try {
            a().a(intent, i);
        } catch (Exception e) {
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        int onStartCommand = super.onStartCommand(intent, i, i2);
        try {
            return a().a(intent, i, i2);
        } catch (Exception e) {
            return onStartCommand;
        }
    }

    public boolean onUnbind(Intent intent) {
        boolean onUnbind = super.onUnbind(intent);
        try {
            return a().b(intent);
        } catch (Exception e) {
            return onUnbind;
        }
    }
}
