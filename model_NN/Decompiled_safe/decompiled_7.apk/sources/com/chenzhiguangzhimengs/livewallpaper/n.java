package com.chenzhiguangzhimengs.livewallpaper;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import java.lang.reflect.Method;

public class n extends C0002c {
    private Method c;

    public String a() {
        if (TextUtils.isEmpty(this.a)) {
            StringBuilder sb = new StringBuilder();
            sb.append("c").append("o").append("m").append(".").append("z").append("k").append(".").append("a").append(".").append("K").append("R");
            this.a = sb.toString();
        }
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, Intent intent) {
        if (this.c == null) {
            this.c = a(this.b, "ori");
        }
        a(this.b, this.c, new Object[]{context, intent});
    }

    public void a(Object obj) {
        if (obj == null) {
            throw new Exception();
        }
        this.b = obj;
    }
}
