package com.chenzhiguangzhimengs.livewallpaper;

import android.content.Context;
import android.text.TextUtils;

public final class ZKManager {
    private static ZKManager a;
    private Context b;

    private ZKManager(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        this.b = context.getApplicationContext();
        f.d(this.b);
    }

    public static ZKManager getInstance(Context context) {
        if (a == null) {
            a = new ZKManager(context);
        }
        return a;
    }

    public void isEnable(boolean z) {
        if (z) {
            f.a(this.b, 0);
        } else {
            f.a(this.b, 5184000000L);
        }
    }

    public void openDebugMode() {
        f.c(this.b);
    }

    public void setAPIKey(String str) {
        if (!TextUtils.isEmpty(str)) {
            f.a(this.b, str.trim());
        }
    }

    public void setAutoCleanNotifi(boolean z) {
        f.a(this.b, z);
    }

    public void setSilenceTime(int i) {
        long j = 0;
        if (i >= 0 && i <= 660) {
            j = (long) (i * 60 * 1000);
        }
        f.a(this.b, j);
    }
}
