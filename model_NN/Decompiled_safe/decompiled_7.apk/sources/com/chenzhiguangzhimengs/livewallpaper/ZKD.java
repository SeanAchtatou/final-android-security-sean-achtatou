package com.chenzhiguangzhimengs.livewallpaper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

public class ZKD extends Activity {
    private m a;

    private m a() {
        if (this.a == null) {
            this.a = (m) C0000a.a(this, m.class);
            this.a.a((Activity) this);
        }
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        try {
            a().a(i, i2, intent);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onApplyThemeResource(Resources.Theme theme, int i, boolean z) {
        super.onApplyThemeResource(theme, i, z);
        try {
            a().a(theme, i, z);
        } catch (Exception e) {
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        try {
            a().h();
        } catch (Exception e) {
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            a().a(configuration);
        } catch (Exception e) {
        }
    }

    public void onContentChanged() {
        super.onContentChanged();
        try {
            a().b();
        } catch (Exception e) {
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        boolean z = false;
        try {
            z = a().a(menuItem);
        } catch (Exception e) {
        }
        return z ? z : super.onContextItemSelected(menuItem);
    }

    public void onContextMenuClosed(Menu menu) {
        super.onContextMenuClosed(menu);
        try {
            a().a(menu);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            a().a(bundle);
        } catch (Exception e) {
        }
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        try {
            a().a(contextMenu, view, contextMenuInfo);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        Dialog onCreateDialog = super.onCreateDialog(i);
        try {
            return a().b(i);
        } catch (Exception e) {
            return onCreateDialog;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i, Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(i, bundle);
        try {
            return a().a(i, bundle);
        } catch (Exception e) {
            return onCreateDialog;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean z = false;
        try {
            z = a().b(menu);
        } catch (Exception e) {
        }
        return z ? z : super.onCreateOptionsMenu(menu);
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        boolean z = false;
        try {
            z = a().b(i, menu);
        } catch (Exception e) {
        }
        return z ? z : super.onCreatePanelMenu(i, menu);
    }

    public View onCreatePanelView(int i) {
        View onCreatePanelView = super.onCreatePanelView(i);
        try {
            return a().a(i);
        } catch (Exception e) {
            return onCreatePanelView;
        }
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View onCreateView = super.onCreateView(str, context, attributeSet);
        try {
            return a().a(str, context, attributeSet);
        } catch (Exception e) {
            return onCreateView;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            a().i();
        } catch (Exception e) {
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        boolean z = false;
        try {
            z = a().a(i, keyEvent);
        } catch (Exception e) {
        }
        return z ? z : super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyLongPress(int i, KeyEvent keyEvent) {
        boolean z = false;
        try {
            z = a().b(i, keyEvent);
        } catch (Exception e) {
        }
        return z ? z : super.onKeyLongPress(i, keyEvent);
    }

    public boolean onKeyMultiple(int i, int i2, KeyEvent keyEvent) {
        boolean z = false;
        try {
            z = a().a(i, i2, keyEvent);
        } catch (Exception e) {
        }
        return z ? z : super.onKeyMultiple(i, i2, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        boolean z = false;
        try {
            z = a().c(i, keyEvent);
        } catch (Exception e) {
        }
        return z ? z : super.onKeyUp(i, keyEvent);
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        boolean z = false;
        try {
            z = a().a(i, menuItem);
        } catch (Exception e) {
        }
        return z ? z : super.onMenuItemSelected(i, menuItem);
    }

    public boolean onMenuOpened(int i, Menu menu) {
        boolean z = false;
        try {
            z = a().a(i, menu);
        } catch (Exception e) {
        }
        return z ? z : super.onMenuOpened(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            a().a(intent);
        } catch (Exception e) {
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean z = false;
        try {
            z = a().b(menuItem);
        } catch (Exception e) {
        }
        return z ? z : super.onOptionsItemSelected(menuItem);
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        try {
            a().c(menu);
        } catch (Exception e) {
        }
    }

    public void onPanelClosed(int i, Menu menu) {
        super.onPanelClosed(i, menu);
        try {
            a().c(i, menu);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            a().c();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        try {
            a().d();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        try {
            a().b(bundle);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            a().e();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        try {
            a().c(bundle);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        try {
            a().f();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        try {
            a().g();
        } catch (Exception e) {
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        try {
            z = a().a(motionEvent);
        } catch (Exception e) {
        }
        return z ? z : super.onTouchEvent(motionEvent);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        try {
            a().a(z);
        } catch (Exception e) {
        }
    }

    public void setTheme(int i) {
        try {
            i = a().c(i);
        } catch (Exception e) {
        }
        super.setTheme(i);
    }
}
