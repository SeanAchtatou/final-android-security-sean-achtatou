package com.chenzhiguangdongman.livewallpaper;

public class info {
    float realheight;
    float realwidth;
    String text;

    public info(String text2, float realwidth2, float realheight2) {
        this.text = text2;
        this.realwidth = realwidth2;
        this.realheight = realheight2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public float getRealwidth() {
        return this.realwidth;
    }

    public void setRealwidth(float realwidth2) {
        this.realwidth = realwidth2;
    }

    public float getRealheight() {
        return this.realheight;
    }

    public void setRealheight(float realheight2) {
        this.realheight = realheight2;
    }
}
