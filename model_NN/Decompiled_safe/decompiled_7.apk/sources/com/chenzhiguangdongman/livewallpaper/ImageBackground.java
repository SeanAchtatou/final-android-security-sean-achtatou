package com.chenzhiguangdongman.livewallpaper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import java.util.ArrayList;
import java.util.List;

public class ImageBackground extends Activity {
    Bitmap bitmap1;
    Bitmap bitmap2;
    private SharedPreferences.Editor editor;
    private GridView gridview;
    int[] picture = {R.drawable.as, R.drawable.bs, R.drawable.cs, R.drawable.ds, R.drawable.es, R.drawable.fs, R.drawable.gs, R.drawable.hs, R.drawable.is, R.drawable.ls, R.drawable.ms, R.drawable.ns};
    /* access modifiers changed from: private */
    public List<Picture> picturelist = new ArrayList();
    int selectnumber = 0;
    private SharedPreferences sharedpreference;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.backgroundphoto);
        this.sharedpreference = getSharedPreferences("MyPreference", 0);
        this.gridview = (GridView) findViewById(R.id.gridView1);
        this.bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.checkfist);
        this.bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.checked);
        final ImageAdapter imageAdapter = new ImageAdapter(this, this.picture, this.bitmap1);
        this.gridview.setAdapter((ListAdapter) imageAdapter);
        for (int i = 0; i < this.picturelist.size(); i++) {
            int index = this.sharedpreference.getInt("key" + i, 20);
            if (index != 20) {
                this.picturelist.get(index).bitmap = this.bitmap2;
                imageAdapter.notifyDataSetChanged();
            }
        }
        this.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (((Picture) ImageBackground.this.picturelist.get(arg2)).bitmap == ImageBackground.this.bitmap1) {
                    ((Picture) ImageBackground.this.picturelist.get(arg2)).bitmap = ImageBackground.this.bitmap2;
                    imageAdapter.notifyDataSetChanged();
                } else if (((Picture) ImageBackground.this.picturelist.get(arg2)).bitmap == ImageBackground.this.bitmap2) {
                    ((Picture) ImageBackground.this.picturelist.get(arg2)).bitmap = ImageBackground.this.bitmap1;
                    imageAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        this.editor = this.sharedpreference.edit();
        this.editor.clear().commit();
        if (keyCode == 4) {
            for (int i = 0; i < this.picturelist.size(); i++) {
                if (this.picturelist.get(i).bitmap == this.bitmap2) {
                    this.editor.putInt("key" + i, i);
                    this.selectnumber++;
                }
            }
            this.editor.putInt("xuanchang", this.selectnumber);
            this.editor.putInt("changdu", this.picturelist.size());
            this.editor.commit();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    class ImageAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater inflater;

        public ImageAdapter(Context context2, int[] image1, Bitmap bitmap) {
            this.context = context2;
            this.inflater = LayoutInflater.from(context2);
            for (int picture : image1) {
                ImageBackground.this.picturelist.add(new Picture(picture, bitmap));
            }
        }

        public int getCount() {
            return ImageBackground.this.picturelist.size();
        }

        public Object getItem(int arg0) {
            return ImageBackground.this.picturelist.get(arg0);
        }

        public long getItemId(int arg0) {
            return (long) arg0;
        }

        public View getView(int arg0, View arg1, ViewGroup arg2) {
            ViewHolder viewholder = new ViewHolder();
            if (arg1 == null) {
                arg1 = this.inflater.inflate((int) R.layout.imageadapter, (ViewGroup) null);
                viewholder.imageview1 = (ImageView) arg1.findViewById(R.id.imageView1);
                viewholder.imageview2 = (ImageView) arg1.findViewById(R.id.imageView2);
                arg1.setTag(viewholder);
            } else {
                viewholder = (ViewHolder) arg1.getTag();
            }
            viewholder.imageview2.setImageBitmap(((Picture) ImageBackground.this.picturelist.get(arg0)).bitmap);
            viewholder.imageview1.setImageResource(((Picture) ImageBackground.this.picturelist.get(arg0)).imageid1);
            return arg1;
        }
    }

    class ViewHolder {
        ImageView imageview1;
        ImageView imageview2;

        ViewHolder() {
        }
    }

    class Picture {
        /* access modifiers changed from: private */
        public Bitmap bitmap;
        /* access modifiers changed from: private */
        public int imageid1;

        public Picture(int imageid12, Bitmap bitmap2) {
            this.imageid1 = imageid12;
            this.bitmap = bitmap2;
        }

        public int getImageid1() {
            return this.imageid1;
        }

        public void setImageid1(int imageid12) {
            this.imageid1 = imageid12;
        }

        public Bitmap getbitmap() {
            return this.bitmap;
        }

        public void sebitmap(Bitmap bitmap2) {
            this.bitmap = bitmap2;
        }
    }
}
