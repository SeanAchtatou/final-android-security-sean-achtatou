package com.chenzhiguangdongman.livewallpaper;

public class Constant {
    public static final int BLUE_BALL_SIZE = 35;
    public static final int BLUE_XSPAN = 1;
    public static final int BLUE_YSPAN = 1;
    public static final int GREEN_BALL_SIZE = 35;
    public static final int GREEN_XSPAN = 1;
    public static final int GREEN_YSPAN = 1;
    public static final int MOVE_TIME = 10;
    public static int SCREEN_HEIGHT = 0;
    public static int SCREEN_WIDTH = 0;
    public static final int YELLOW_BALL_SIZE = 35;
    public static final int YELLOW_XSPAN = 1;
    public static final int YELLOW_YSPAN = 1;
    public static final int p1_ball_size = 34;
    public static final int p1_xspan = 1;
    public static final int p1_yspan = 1;
    public static final int p3_ball_size = 35;
    public static final int p3_xspan = 1;
    public static final int p3_yspan = 1;
    public static final int p4_ball_size = 35;
    public static final int p4_xspan = 1;
    public static final int p4_yspan = 1;
    public static final int p6_ball_size = 35;
    public static final int p6_xspan = 1;
    public static final int p6_yspan = 1;
    public static final int star1_ball_size = 35;
    public static final int star1_xspan = 1;
    public static final int star1_yspan = 1;
    public static final int star2_ball_size = 35;
    public static final int star2_xspan = 1;
    public static final int star2_yspan = 1;
    public static final int star3_ball_size = 33;
    public static final int star3_xspan = 1;
    public static final int star3_yspan = 1;
}
