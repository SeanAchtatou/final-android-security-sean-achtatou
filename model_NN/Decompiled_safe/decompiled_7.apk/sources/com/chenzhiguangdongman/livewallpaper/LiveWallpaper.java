package com.chenzhiguangdongman.livewallpaper;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Handler;
import android.preference.Preference;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class LiveWallpaper extends WallpaperService {
    public static final String SHARED_PREFS_NAME = "com.chenzhiguang.livewallpaper_a0b";
    static int col = 0;
    static int h1;
    static String loveText = "やめろ ";
    static int row = 0;
    static int textCount = 19;
    static int texti = 0;
    static int w1;
    static int wordCount = 9;
    int SccrentHeight;
    int SccrentWight;
    Bitmap blueBallBitmap;
    BallEngine ce;
    DisplayMetrics dm;
    DisplayMetrics dm2;
    double endtime;
    Bitmap greenBallBitmap;
    Handler hd = new Handler();
    int i = 0;
    int[] image = {R.drawable.a, R.drawable.b, R.drawable.c, R.drawable.d, R.drawable.e, R.drawable.f, R.drawable.g, R.drawable.h, R.drawable.i, R.drawable.l, R.drawable.m, R.drawable.n};
    ArrayList<info> list = new ArrayList<>();
    int numberselect = 0;
    Bitmap p1;
    Bitmap p3;
    Bitmap p4;
    Bitmap p6;
    Paint painttext = new Paint();
    int[] picture;
    int selecteditem;
    int seletedi = 0;
    int seletedindex = 0;
    Set<String> set = new HashSet();
    String[] setgetString;
    SharedPreferences sharedpreference;
    double startime;
    long timevalues = 2;
    int type = 3;
    Bitmap yellowBallBitmap;

    public WallpaperService.Engine onCreateEngine() {
        this.ce = new BallEngine();
        return this.ce;
    }

    public void initBitmaphongxin() {
        this.yellowBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.flower2);
        this.blueBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.flower4);
        this.greenBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.flower5);
        this.p1 = BitmapFactory.decodeResource(getResources(), R.drawable.p1);
        this.p3 = BitmapFactory.decodeResource(getResources(), R.drawable.p3);
        this.p4 = BitmapFactory.decodeResource(getResources(), R.drawable.p4);
    }

    public void initBitmapxingixng() {
        this.yellowBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.s1);
        this.blueBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.s2);
        this.greenBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.s3);
        this.p1 = BitmapFactory.decodeResource(getResources(), R.drawable.s4);
        this.p3 = BitmapFactory.decodeResource(getResources(), R.drawable.s5);
        this.p4 = BitmapFactory.decodeResource(getResources(), R.drawable.s6);
    }

    public void initBitmappaopao() {
        this.yellowBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bubble2);
        this.blueBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bubble3);
        this.greenBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bubble4);
        this.p1 = BitmapFactory.decodeResource(getResources(), R.drawable.bubble5);
        this.p3 = BitmapFactory.decodeResource(getResources(), R.drawable.bubble6);
        this.p4 = BitmapFactory.decodeResource(getResources(), R.drawable.bubble7);
    }

    public void initBitmapyinghua() {
        this.yellowBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.f1);
        this.blueBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.f2);
        this.greenBallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.f3);
        this.p1 = BitmapFactory.decodeResource(getResources(), R.drawable.f4);
        this.p3 = BitmapFactory.decodeResource(getResources(), R.drawable.f5);
        this.p4 = BitmapFactory.decodeResource(getResources(), R.drawable.p6);
    }

    public class BallEngine extends WallpaperService.Engine implements SharedPreferences.OnSharedPreferenceChangeListener {
        AllBalls allBalls;
        BallGoThread bgThread;
        LiveWallpaper father;
        boolean ifDraw;
        private final Runnable mDrawCube = new Runnable() {
            public void run() {
                if (LiveWallpaper.texti == LiveWallpaper.loveText.length() - 1) {
                    LiveWallpaper.texti = 0;
                    LiveWallpaper.row = 0;
                    LiveWallpaper.col = 0;
                    LiveWallpaper.this.list.clear();
                } else {
                    if (LiveWallpaper.textCount == 20) {
                        LiveWallpaper.texti++;
                        LiveWallpaper.col++;
                        LiveWallpaper.textCount = 0;
                    }
                    LiveWallpaper.textCount++;
                }
                BallEngine.this.drawBalls();
            }
        };
        private final Paint paint = new Paint();
        Preference pref;
        SharedPreferences prefs;

        public BallEngine() {
            super(LiveWallpaper.this);
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            LiveWallpaper.this.startime = (double) System.currentTimeMillis();
            this.paint.setAntiAlias(true);
            LiveWallpaper.this.initBitmapxingixng();
            LiveWallpaper.this.dm2 = LiveWallpaper.this.getResources().getDisplayMetrics();
            LiveWallpaper.this.SccrentHeight = LiveWallpaper.this.dm2.heightPixels;
            LiveWallpaper.this.SccrentWight = LiveWallpaper.this.dm2.widthPixels;
            this.prefs = LiveWallpaper.this.getSharedPreferences(LiveWallpaper.SHARED_PREFS_NAME, 0);
            this.prefs.registerOnSharedPreferenceChangeListener(this);
            onSharedPreferenceChanged(this.prefs, null);
            LiveWallpaper.this.dm = new DisplayMetrics();
            ((WindowManager) LiveWallpaper.this.getSystemService("window")).getDefaultDisplay().getMetrics(LiveWallpaper.this.dm);
            LiveWallpaper.w1 = LiveWallpaper.this.dm.widthPixels;
            LiveWallpaper.h1 = LiveWallpaper.this.dm.heightPixels;
            setTouchEventsEnabled(true);
        }

        public void onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
        }

        public void onDestroy() {
            super.onDestroy();
        }

        private void type() {
            this.allBalls = new AllBalls(new int[]{35, 35, 35, 34, 35, 35}, new Bitmap[]{LiveWallpaper.this.yellowBallBitmap, LiveWallpaper.this.blueBallBitmap, LiveWallpaper.this.greenBallBitmap, LiveWallpaper.this.p1, LiveWallpaper.this.p3, LiveWallpaper.this.p4}, new int[]{1, 1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1, 1});
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            System.out.println(String.valueOf(LiveWallpaper.this.type) + "佛挡杀佛");
            Constant.SCREEN_HEIGHT = height;
            Constant.SCREEN_WIDTH = width;
            type();
            super.onSurfaceChanged(holder, format, width, height);
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
        }

        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
        }

        public void onVisibilityChanged(boolean visible) {
            this.ifDraw = visible;
            if (this.ifDraw) {
                if (LiveWallpaper.this.type == 0) {
                    LiveWallpaper.this.initBitmaphongxin();
                } else if (LiveWallpaper.this.type == 1) {
                    LiveWallpaper.this.initBitmappaopao();
                } else if (LiveWallpaper.this.type == 2) {
                    LiveWallpaper.this.initBitmapyinghua();
                } else if (LiveWallpaper.this.type == 3) {
                    LiveWallpaper.this.initBitmapxingixng();
                }
                type();
                this.bgThread = new BallGoThread(this.allBalls);
                this.bgThread.start();
                LiveWallpaper.this.hd.postDelayed(this.mDrawCube, 10);
                return;
            }
            this.bgThread.ballGoFlag = false;
        }

        public void drawPast(Canvas c, Paint p) {
            if (LiveWallpaper.this.list.size() > 0) {
                for (int j = 0; j < LiveWallpaper.this.list.size(); j++) {
                    info in = LiveWallpaper.this.list.get(j);
                    c.drawText(in.getText(), in.getRealwidth(), in.getRealheight(), p);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        /* access modifiers changed from: package-private */
        public void drawBalls() {
            Bitmap bitmap;
            LiveWallpaper.this.painttext.setStrokeWidth(4.0f);
            LiveWallpaper.this.painttext.setTextSize(16.0f);
            LiveWallpaper.this.painttext.setAntiAlias(true);
            LiveWallpaper.this.painttext.setTypeface(Typeface.create(Typeface.MONOSPACE, 1));
            LiveWallpaper.this.sharedpreference = LiveWallpaper.this.getSharedPreferences("MyPreference", 0);
            LiveWallpaper.this.numberselect = LiveWallpaper.this.sharedpreference.getInt("changdu", 0);
            int xuanchang = LiveWallpaper.this.sharedpreference.getInt("xuanchang", 0);
            SurfaceHolder holder = getSurfaceHolder();
            if (xuanchang > 0) {
                LiveWallpaper.this.endtime = (double) System.currentTimeMillis();
                for (int i = 0; i < LiveWallpaper.this.numberselect; i++) {
                    LiveWallpaper.this.seletedindex = LiveWallpaper.this.sharedpreference.getInt("key" + i, 20);
                    if (LiveWallpaper.this.seletedindex != 20) {
                        LiveWallpaper.this.set.add(String.valueOf(LiveWallpaper.this.seletedindex));
                    }
                }
                LiveWallpaper.this.setgetString = (String[]) LiveWallpaper.this.set.toArray(new String[]{String.valueOf(LiveWallpaper.this.set.size())});
                LiveWallpaper.this.selecteditem = Integer.parseInt(LiveWallpaper.this.setgetString[LiveWallpaper.this.seletedi]);
                double newtime2 = new BigDecimal((LiveWallpaper.this.endtime - LiveWallpaper.this.startime) / 1000.0d).setScale(1, 4).doubleValue();
                if (newtime2 / ((double) LiveWallpaper.this.timevalues) >= 1.0d && newtime2 % ((double) LiveWallpaper.this.timevalues) == 0.0d) {
                    LiveWallpaper.this.seletedi++;
                    if (LiveWallpaper.this.seletedi == xuanchang) {
                        LiveWallpaper.this.seletedi = 0;
                    }
                }
                bitmap = BitmapFactory.decodeResource(LiveWallpaper.this.getResources(), LiveWallpaper.this.image[LiveWallpaper.this.selecteditem]);
            } else {
                bitmap = BitmapFactory.decodeResource(LiveWallpaper.this.getResources(), LiveWallpaper.this.image[0]);
            }
            Matrix matrix = new Matrix();
            matrix.postScale(((float) LiveWallpaper.this.SccrentWight) / ((float) bitmap.getWidth()), ((float) LiveWallpaper.this.SccrentHeight) / ((float) bitmap.getHeight()));
            Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            try {
                Canvas canvas = holder.lockCanvas();
                if (canvas != null) {
                    canvas.save();
                    canvas.drawBitmap(bitmap2, 0.0f, 0.0f, (Paint) null);
                    this.allBalls.drawSelf(canvas, this.paint);
                    canvas.restore();
                    drawText1 text = new drawText1(LiveWallpaper.texti, (double) LiveWallpaper.w1, (double) LiveWallpaper.h1);
                    Paint p1 = new Paint();
                    p1.setTextSize(16.0f);
                    p1.setColor(-16777216);
                    p1.setStrokeWidth(4.0f);
                    p1.setAntiAlias(true);
                    p1.setTypeface(Typeface.create(Typeface.MONOSPACE, 1));
                    drawPast(canvas, p1);
                    LiveWallpaper.this.painttext.setColor(Color.argb(LiveWallpaper.textCount * 12, 0, 0, 0));
                    canvas.drawText(text.getText(), text.getX(), text.getY(), LiveWallpaper.this.painttext);
                    if (LiveWallpaper.textCount == 19) {
                        LiveWallpaper.this.list.add(new info(text.getText(), text.getX(), text.getY()));
                    }
                }
                if (canvas != null) {
                    holder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (0 != 0) {
                    holder.unlockCanvasAndPost(null);
                }
            } catch (Throwable th) {
                if (0 != 0) {
                    holder.unlockCanvasAndPost(null);
                }
                throw th;
            }
            if (this.ifDraw) {
                LiveWallpaper.this.hd.postDelayed(this.mDrawCube, 10);
            }
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key != null) {
                if (key.equals("type")) {
                    LiveWallpaper.this.type = Integer.parseInt(this.prefs.getString(LiveWallpaper.this.getResources().getString(R.string.type), "3"));
                }
                if (key.equals("time")) {
                    LiveWallpaper.this.timevalues = (long) Integer.parseInt(this.prefs.getString(LiveWallpaper.this.getResources().getString(R.string.time), "2"));
                }
                if (key.equals("inputText")) {
                    LiveWallpaper.loveText = this.prefs.getString(LiveWallpaper.this.getResources().getString(R.string.inputText), LiveWallpaper.loveText);
                    LiveWallpaper.loveText = String.valueOf(LiveWallpaper.loveText) + "   ";
                    LiveWallpaper.texti = 0;
                    LiveWallpaper.row = 0;
                    LiveWallpaper.col = 0;
                    LiveWallpaper.this.list.clear();
                }
                if (key.equals("wordCount")) {
                    LiveWallpaper.wordCount = Integer.parseInt(this.prefs.getString(LiveWallpaper.this.getResources().getString(R.string.wordCount), "9"));
                    LiveWallpaper.texti = 0;
                    LiveWallpaper.row = 0;
                    LiveWallpaper.col = 0;
                    LiveWallpaper.this.list.clear();
                }
                if (key.equals("reset")) {
                    LiveWallpaper.loveText = "やめろ";
                    LiveWallpaper.wordCount = 9;
                    LiveWallpaper.this.timevalues = 2;
                    LiveWallpaper.texti = 0;
                    LiveWallpaper.row = 0;
                    LiveWallpaper.col = 0;
                    LiveWallpaper.this.list.clear();
                }
            }
        }
    }
}
