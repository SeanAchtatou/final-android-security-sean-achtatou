package com.chenzhiguangdongman.livewallpaper;

public final class R {

    public static final class array {
        public static final int entries = 2131034112;
        public static final int timeentries = 2131034114;
        public static final int timevalues = 2131034115;
        public static final int values = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int a = 2130837504;
        public static final int as = 2130837505;
        public static final int b = 2130837506;
        public static final int bs = 2130837507;
        public static final int bubble2 = 2130837508;
        public static final int bubble3 = 2130837509;
        public static final int bubble4 = 2130837510;
        public static final int bubble5 = 2130837511;
        public static final int bubble6 = 2130837512;
        public static final int bubble7 = 2130837513;
        public static final int c = 2130837514;
        public static final int checked = 2130837515;
        public static final int checkfist = 2130837516;
        public static final int cs = 2130837517;
        public static final int d = 2130837518;
        public static final int ds = 2130837519;
        public static final int e = 2130837520;
        public static final int es = 2130837521;
        public static final int f = 2130837522;
        public static final int f1 = 2130837523;
        public static final int f2 = 2130837524;
        public static final int f3 = 2130837525;
        public static final int f4 = 2130837526;
        public static final int f5 = 2130837527;
        public static final int flower2 = 2130837528;
        public static final int flower4 = 2130837529;
        public static final int flower5 = 2130837530;
        public static final int fs = 2130837531;
        public static final int g = 2130837532;
        public static final int gs = 2130837533;
        public static final int h = 2130837534;
        public static final int hs = 2130837535;
        public static final int i = 2130837536;
        public static final int ic_yytj = 2130837537;
        public static final int is = 2130837538;
        public static final int l = 2130837539;
        public static final int ls = 2130837540;
        public static final int m = 2130837541;
        public static final int ms = 2130837542;
        public static final int n = 2130837543;
        public static final int ns = 2130837544;
        public static final int p1 = 2130837545;
        public static final int p3 = 2130837546;
        public static final int p4 = 2130837547;
        public static final int p6 = 2130837548;
        public static final int s1 = 2130837549;
        public static final int s2 = 2130837550;
        public static final int s3 = 2130837551;
        public static final int s4 = 2130837552;
        public static final int s5 = 2130837553;
        public static final int s6 = 2130837554;
    }

    public static final class id {
        public static final int buttons = 2131165187;
        public static final int gridView1 = 2131165184;
        public static final int imageView1 = 2131165185;
        public static final int imageView2 = 2131165186;
        public static final int textView1 = 2131165188;
    }

    public static final class layout {
        public static final int backgroundphoto = 2130903040;
        public static final int imageadapter = 2130903041;
        public static final int main = 2130903042;
        public static final int mainactivity = 2130903043;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int description = 2131099650;
        public static final int hello = 2131099648;
        public static final int inputText = 2131099654;
        public static final int leaf1Count = 2131099653;
        public static final int reset = 2131099656;
        public static final int settings = 2131099652;
        public static final int time = 2131099657;
        public static final int type = 2131099658;
        public static final int wallpaper_settings = 2131099651;
        public static final int wordCount = 2131099655;
    }

    public static final class xml {
        public static final int livewallpaper = 2130968576;
        public static final int settings = 2130968577;
    }
}
