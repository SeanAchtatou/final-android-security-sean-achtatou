package com.chenzhiguangdongman.livewallpaper;

public class BallGoThread extends Thread {
    AllBalls allBalls;
    boolean ballGoFlag = true;

    public BallGoThread(AllBalls allBalls2) {
        this.allBalls = allBalls2;
    }

    public void run() {
        while (this.ballGoFlag) {
            this.allBalls.go();
            try {
                Thread.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
