package com.chenzhiguangdongman.livewallpaper;

public class drawText1 {
    double height;
    int i;
    double realheight;
    double realwidth;
    double width;

    public drawText1(int i2, double width2, double height2) {
        this.i = i2;
        this.width = width2;
        this.height = height2;
        if (((13.0d * height2) / 21.0d) + ((double) (LiveWallpaper.col * 18)) > height2 || LiveWallpaper.col == LiveWallpaper.wordCount) {
            LiveWallpaper.row++;
            LiveWallpaper.col = 0;
        }
    }

    public String getText() {
        return LiveWallpaper.loveText.substring(this.i, this.i + 1);
    }

    public float getX() {
        return (float) ((LiveWallpaper.row * 25) + 20);
    }

    public float getY() {
        return ((((float) this.height) * 13.0f) / 21.0f) + ((float) (LiveWallpaper.col * 18));
    }

    public int getLength() {
        return LiveWallpaper.loveText.length();
    }
}
