package com.chenzhiguangdongman.livewallpaper;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.jmp.sfc.uti.JuMiManager;

public class MainActivity extends Activity {
    Button button;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mainactivity);
        this.button = (Button) findViewById(R.id.buttons);
        try {
            new JuMiManager().startService(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                WallpaperManager.getInstance(MainActivity.this);
                intent.setAction("android.service.wallpaper.LIVE_WALLPAPER_CHOOSER");
                MainActivity.this.startActivity(intent);
                MainActivity.this.finish();
            }
        });
    }
}
