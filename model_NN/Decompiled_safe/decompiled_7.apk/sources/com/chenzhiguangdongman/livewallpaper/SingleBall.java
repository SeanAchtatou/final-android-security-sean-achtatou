package com.chenzhiguangdongman.livewallpaper;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class SingleBall {
    public static final int DIRECTION_YS = 0;
    public static final int DIRECTION_YX = 3;
    public static final int DIRECTION_ZS = 1;
    public static final int DIRECTION_ZX = 2;
    Bitmap bitmap;
    int direction;
    int size;
    int x;
    int xSpan = 2;
    int y;
    int ySpan = 2;

    public SingleBall(int x2, int y2, int size2, int direction2, Bitmap bitmap2, int xSpan2, int ySpan2) {
        this.x = x2;
        this.y = y2;
        this.size = size2;
        this.bitmap = bitmap2;
        this.direction = direction2;
        this.xSpan = xSpan2;
        this.ySpan = ySpan2;
    }

    /* access modifiers changed from: package-private */
    public void drawSelf(Canvas canvas, Paint paint) {
        canvas.drawBitmap(this.bitmap, (float) this.x, (float) this.y, paint);
    }

    /* access modifiers changed from: package-private */
    public void go() {
        switch (this.direction) {
            case DIRECTION_YS /*0*/:
                int tempX = this.x + this.xSpan;
                int tempY = this.y - this.ySpan;
                if (isCollideWithRight(tempX, tempY)) {
                    this.direction = 1;
                    return;
                } else if (isCollideWithUp(tempX, tempY)) {
                    this.direction = 3;
                    return;
                } else {
                    this.x = tempX;
                    this.y = tempY;
                    return;
                }
            case 1:
                int tempX2 = this.x - this.xSpan;
                int tempY2 = this.y - this.ySpan;
                if (isCollideWithLeft(tempX2, tempY2)) {
                    this.direction = 0;
                    return;
                } else if (isCollideWithUp(tempX2, tempY2)) {
                    this.direction = 2;
                    return;
                } else {
                    this.x = tempX2;
                    this.y = tempY2;
                    return;
                }
            case DIRECTION_ZX /*2*/:
                int tempX3 = this.x - this.xSpan;
                int tempY3 = this.y + this.ySpan;
                if (isCollideWithLeft(tempX3, tempY3)) {
                    this.direction = 3;
                    return;
                } else if (isCollideWithDown(tempX3, tempY3)) {
                    this.direction = 1;
                    return;
                } else {
                    this.x = tempX3;
                    this.y = tempY3;
                    return;
                }
            case DIRECTION_YX /*3*/:
                int tempX4 = this.x + this.xSpan;
                int tempY4 = this.y + this.ySpan;
                if (isCollideWithRight(tempX4, tempY4)) {
                    this.direction = 2;
                    return;
                } else if (isCollideWithDown(tempX4, tempY4)) {
                    this.direction = 0;
                    return;
                } else {
                    this.x = tempX4;
                    this.y = tempY4;
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isCollideWithRight(int tempX, int tempY) {
        return tempX <= 0 || ((double) tempX) >= ((double) Constant.SCREEN_WIDTH) - (((double) this.size) * 1.5d);
    }

    /* access modifiers changed from: package-private */
    public boolean isCollideWithUp(int tempX, int tempY) {
        return tempY <= 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isCollideWithLeft(int tempX, int tempY) {
        return tempX <= 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isCollideWithDown(int tempX, int tempY) {
        return tempY <= 0 || ((double) tempY) >= ((double) Constant.SCREEN_HEIGHT) - (((double) this.size) * 1.5d);
    }
}
