package com.chenzhiguangdongman.livewallpaper;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.ArrayList;
import java.util.Iterator;

public class AllBalls {
    ArrayList<SingleBall> alSingleBall = new ArrayList<>();
    Bitmap[] ballsBitmap;
    int[] ballsSize;
    int[] ballsXSpan;
    int[] ballsYSpan;

    public AllBalls(int[] ballsSize2, Bitmap[] ballsBitmap2, int[] ballsXSpan2, int[] ballsYSpan2) {
        this.ballsSize = ballsSize2;
        this.ballsBitmap = ballsBitmap2;
        this.ballsXSpan = ballsXSpan2;
        this.ballsYSpan = ballsYSpan2;
        for (int i = 0; i < ballsSize2.length; i++) {
            this.alSingleBall.add(new SingleBall((int) (Math.random() * ((double) (Constant.SCREEN_WIDTH - ballsSize2[i]))), (int) (Math.random() * ((double) (Constant.SCREEN_HEIGHT - ballsSize2[i]))), ballsSize2[i], ((int) Math.random()) * 4, ballsBitmap2[i], ballsXSpan2[i], ballsYSpan2[i]));
        }
    }

    public void drawSelf(Canvas canvas, Paint paint) {
        Iterator<SingleBall> it = this.alSingleBall.iterator();
        while (it.hasNext()) {
            it.next().drawSelf(canvas, paint);
        }
    }

    public void go() {
        Iterator<SingleBall> it = this.alSingleBall.iterator();
        while (it.hasNext()) {
            it.next().go();
        }
    }
}
