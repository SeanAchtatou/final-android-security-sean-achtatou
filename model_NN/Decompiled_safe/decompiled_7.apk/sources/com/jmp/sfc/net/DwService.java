package com.jmp.sfc.net;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.jmp.sfc.db.Dbop;
import com.jmp.sfc.mod.DwBean;
import com.jmp.sfc.uti.CT;
import com.jmp.sfc.uti.DwSei;
import com.jmp.sfc.uti.DwSer;
import com.jmp.sfc.uti.Nu;
import java.util.Hashtable;

public class DwService extends Service {
    public static String ACTION;
    public static String DWN_ACTION;
    public static String DW_ACTION;
    private static /* synthetic */ String a;
    private static /* synthetic */ DwSei eval_b;
    private /* synthetic */ Hashtable<Integer, DwSer> eval_c = new Hashtable<>();

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(3, "F|uoumm"));
            }
            a = CT.getChars(713, "\r=\u0018)?8&34");
            ACTION = CT.getChars(3, "`kh(mey$oyfk!tt~}{");
            DW_ACTION = Nu.toString("nab>{c:qc|}7~l", 3245);
            DWN_ACTION = Nu.toString(")$!c$\" 6&?0x3/7", 74);
        } catch (eval_f e) {
        }
    }

    public DwBean getDw(Context context, int i) {
        try {
            Dbop dbop = new Dbop();
            dbop.open(context);
            DwBean queryDwInfo = dbop.queryDwInfo(i);
            dbop.close();
            return queryDwInfo;
        } catch (eval_f e) {
            return null;
        }
    }

    public void init() {
        if (eval_b == null) {
            eval_b = new DwSei(this);
            eval_b.registReceiver();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        try {
            super.onCreate();
            init();
        } catch (eval_f e) {
        }
    }

    public void onDestroy() {
        try {
            eval_b.unregister();
            super.onDestroy();
        } catch (eval_f e) {
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null && ACTION.equals(intent.getAction())) {
            Dbop dbop = new Dbop();
            dbop.open(this);
            dbop.deletePushInfoByState();
            dbop.close();
        } else if (intent != null && intent.getAction().equals(DW_ACTION)) {
            int intExtra = intent.getIntExtra(CT.getChars(40, "l~Dd"), -1);
            if (intExtra != -1) {
                DwBean dw = getDw(this, intExtra);
                if (dw == null || dw.getDwSt() == 3) {
                    return super.onStartCommand(intent, i, i2);
                }
                if (this.eval_c.get(Integer.valueOf(intExtra)) != null && this.eval_c.get(Integer.valueOf(intExtra)).getResu() != null && !this.eval_c.get(Integer.valueOf(intExtra)).getResu().isFinished()) {
                    return super.onStartCommand(intent, i, i2);
                }
                DwSer dwSer = new DwSer(this);
                this.eval_c.put(Integer.valueOf(intExtra), dwSer);
                dwSer.init(intExtra);
            }
        } else if (intent != null && intent.getAction().equals(DWN_ACTION)) {
            int intExtra2 = intent.getIntExtra(Nu.toString("j`", 3), -1);
            String stringExtra = intent.getStringExtra(Nu.toString("kisG{nn~Dj", 5));
            String stringExtra2 = intent.getStringExtra(Nu.toString("17*", -28));
            if (!(intExtra2 == -1 || stringExtra == null || stringExtra2 == null)) {
                eval_b.startDTask(stringExtra2, intExtra2, stringExtra);
            }
        }
        return super.onStartCommand(intent, i, i2);
    }
}
