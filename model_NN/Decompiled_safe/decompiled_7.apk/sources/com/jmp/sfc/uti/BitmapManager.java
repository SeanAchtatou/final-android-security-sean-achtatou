package com.jmp.sfc.uti;

import android.graphics.Bitmap;
import android.widget.ImageView;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public enum BitmapManager {
    INSTANCE;
    
    private final /* synthetic */ WeakHashMap<String, Bitmap> a = new WeakHashMap<>();
    private final /* synthetic */ ExecutorService eval_b = Executors.newFixedThreadPool(3);
    private /* synthetic */ Map<ImageView, String> eval_c = Collections.synchronizedMap(new WeakHashMap());
    private /* synthetic */ int eval_d;
    private /* synthetic */ ImageView eval_e;
    private /* synthetic */ ImageView eval_f;
    private final /* synthetic */ WeakHashMap<String, Bitmap> eval_g = new WeakHashMap<>(2097152);

    /* access modifiers changed from: public */
    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(165, "@~wa{oo"));
            }
        } catch (eval_j e) {
        }
    }

    public void loadBitmap(String str, ImageView imageView) {
        try {
            String substring = str.substring(str.lastIndexOf("/") + 1, str.length());
            String a2 = a(str, substring);
            this.eval_c.put(imageView, a2);
            Bitmap eval_b2 = eval_b(a2, substring);
            if (eval_b2 == null || eval_b2.isRecycled()) {
                imageView.setVisibility(8);
                a(str, imageView, substring);
                return;
            }
            a(0);
            imageView.setImageBitmap(eval_b2);
        } catch (eval_j e) {
        }
    }

    public void loadBitmap(String str, ImageView imageView, int i) {
        try {
            String substring = str.substring(str.lastIndexOf("/") + 1);
            String a2 = a(str, substring);
            this.eval_c.put(imageView, a2);
            Bitmap eval_b2 = eval_b(a2, substring);
            if (eval_b2 == null || eval_b2.isRecycled()) {
                if (i != 0) {
                    imageView.setImageResource(i);
                    imageView.setVisibility(0);
                } else {
                    imageView.setVisibility(4);
                }
                a(str, imageView, substring, i);
                return;
            }
            imageView.setImageBitmap(eval_b2);
            imageView.setVisibility(0);
        } catch (eval_j e) {
        }
    }

    public void loadBitmap(String str, ImageView imageView, String str2) {
        try {
            String a2 = a(str, str2);
            this.eval_c.put(imageView, a2);
            Bitmap eval_b2 = eval_b(a2, str2);
            if (eval_b2 == null || eval_b2.isRecycled()) {
                imageView.setImageResource(this.eval_d);
                a(str, imageView, str2);
                return;
            }
            imageView.setImageBitmap(eval_b2);
        } catch (eval_j e) {
        }
    }

    public void loadRemoteBitmap(String str, ImageView imageView, int i) {
        String substring = str.substring(str.lastIndexOf("/") + 1);
        String a2 = a(str, substring);
        this.eval_c.put(imageView, a2);
        Bitmap eval_c2 = eval_c(a2, substring);
        if (eval_c2 == null || eval_c2.isRecycled()) {
            if (i != 0) {
                imageView.setImageResource(i);
                imageView.setVisibility(0);
            } else {
                imageView.setVisibility(4);
            }
            eval_b(str, imageView, substring, i);
            return;
        }
        imageView.setImageBitmap(eval_c2);
        imageView.setVisibility(0);
    }

    public boolean putBitmap2LruCache(String str, Bitmap bitmap) {
        if (bitmap == null) {
            return false;
        }
        synchronized (this.eval_g) {
            this.eval_g.put(str, bitmap);
        }
        return true;
    }

    public void setImageViews(ImageView imageView, ImageView imageView2) {
        try {
            this.eval_e = imageView;
            this.eval_f = imageView2;
        } catch (eval_j e) {
        }
    }

    public void setPlaceholder(int i) {
        try {
            this.eval_d = i;
        } catch (eval_j e) {
        }
    }
}
