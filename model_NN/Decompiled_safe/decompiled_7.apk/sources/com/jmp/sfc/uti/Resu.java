package com.jmp.sfc.uti;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.chenzhiguangdongman.livewallpaper.SingleBall;
import com.jmp.sfc.db.Dbop;
import com.jmp.sfc.mod.DwBean;
import com.jmp.sfc.net.DwService;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;

public class Resu extends Thread {
    private /* synthetic */ URL a;
    /* access modifiers changed from: private */
    public /* synthetic */ File eval_b;
    private /* synthetic */ long eval_c;
    private /* synthetic */ long eval_d;
    private /* synthetic */ long eval_e;
    private /* synthetic */ boolean eval_f = false;
    private /* synthetic */ long eval_g = 0;
    /* access modifiers changed from: private */
    public /* synthetic */ int eval_h;
    private /* synthetic */ String eval_i;
    private /* synthetic */ String eval_j;
    /* access modifiers changed from: private */
    public /* synthetic */ Context eval_k;
    private /* synthetic */ NotificationManager eval_l;
    private /* synthetic */ Notification eval_m;
    private /* synthetic */ Handler eval_n = new Handler() {
        static {
            try {
                if (System.currentTimeMillis() >= 1385222400000L) {
                    throw new RuntimeException(Nu.toString("F|uoumm", 3));
                }
            } catch (eval_v e) {
            }
        }

        public void handleMessage(Message message) {
            try {
                switch (message.what) {
                    case 1:
                        long[] jArr = (long[]) message.obj;
                        long j = jArr[0];
                        long j2 = jArr[1];
                        Resu.this.refNoti((int) ((((float) j) / ((float) j2)) * 100.0f), Resu.this.eval_h);
                        return;
                    case SingleBall.DIRECTION_ZX:
                        Resu.this.compNoti();
                        Resu.this.eval_k.startActivity(DwSei.getFileIntent(Resu.this.eval_b));
                        return;
                    default:
                        return;
                }
            } catch (eval_v e) {
            }
        }
    };

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(4, "A}vnzln"));
            }
        } catch (eval_v e) {
        }
    }

    public Resu(URL url, File file, long j, long j2, int i, String str, String str2, Context context) {
        this.a = url;
        this.eval_b = file;
        this.eval_c = j;
        this.eval_e = j;
        this.eval_d = j2;
        this.eval_h = i;
        this.eval_i = str;
        this.eval_j = str2;
        this.eval_k = context;
        opNoti((int) ((((float) j) / ((float) j2)) * 100.0f), i);
    }

    public static void saveDownloadSate(Context context, DwBean dwBean) {
        Dbop dbop = new Dbop();
        dbop.open(context);
        if (dbop.getCurrentDwCount(dwBean.getDwCode()) <= 0) {
            dbop.insertDwData(dwBean);
        } else {
            dbop.updateDwData(dwBean);
        }
        dbop.close();
    }

    public void cancelNot(int i) {
        try {
            this.eval_l = (NotificationManager) this.eval_k.getSystemService(Nu.toString(">>&:2<56,055", -48));
            this.eval_l.cancel(i);
        } catch (eval_v e) {
        }
    }

    public void compNoti() {
        try {
            Intent intent = new Intent(this.eval_k, DwService.class);
            intent.putExtra(CT.getChars(-7, "=-\u00153"), this.eval_h);
            intent.setAction(DwService.DW_ACTION);
            this.eval_m.flags |= 32;
            this.eval_m.contentIntent = PendingIntent.getService(this.eval_k, this.eval_h, intent, 268435456);
            this.eval_m.setLatestEventInfo(this.eval_k, this.eval_i, Nu.toString("\u0016烷冴寙袔\u000f", 109), this.eval_m.contentIntent);
            this.eval_l.notify(this.eval_h, this.eval_m);
        } catch (eval_v e) {
        }
    }

    public long getDownloadSize() {
        return this.eval_g;
    }

    public boolean isFinished() {
        return this.eval_f;
    }

    public void opNoti(int i, int i2) {
        if (this.eval_l == null) {
            this.eval_l = (NotificationManager) this.eval_k.getSystemService(CT.getChars(301, "ca{yw{puaxv"));
            this.eval_m = new Notification();
        }
        this.eval_m.icon = 17301633;
        this.eval_m.tickerText = this.eval_i;
        this.eval_m.when = System.currentTimeMillis();
        this.eval_m.flags |= 32;
        Intent intent = new Intent(this.eval_k, DwService.class);
        intent.putExtra(CT.getChars(893, "9)\u0011o"), i2);
        intent.setAction(DwService.DW_ACTION);
        this.eval_m.contentIntent = PendingIntent.getService(this.eval_k, i2, intent, 268435456);
        this.eval_m.setLatestEventInfo(this.eval_k, this.eval_i, CT.getChars(5, "^不轺严)*") + i + CT.getChars(114, "}bde\u000b"), this.eval_m.contentIntent);
        this.eval_l.notify(i2, this.eval_m);
    }

    public void refNoti(int i, int i2) {
        this.eval_m.setLatestEventInfo(this.eval_k, this.eval_i, CT.getChars(6, "]丌轵两*+") + i + Nu.toString("0112^", 31), this.eval_m.contentIntent);
        if (this.eval_l == null) {
            this.eval_l = (NotificationManager) this.eval_k.getSystemService(CT.getChars(47, "ae{u}vwcqvt"));
        }
        this.eval_l.notify(i2, this.eval_m);
    }

    public void run() {
        this.eval_f = false;
        byte[] bArr = new byte[BitmapUtil.KB];
        try {
            URLConnection openConnection = this.a.openConnection();
            openConnection.setAllowUserInteraction(true);
            openConnection.setRequestProperty(CT.getChars(2209, "Scmc`"), CT.getChars(22, "tnl|i&") + this.eval_c + "-" + this.eval_d);
            RandomAccessFile randomAccessFile = new RandomAccessFile(this.eval_b, Nu.toString("vr", 4));
            randomAccessFile.seek(this.eval_c);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
            int i = 0;
            while (this.eval_e < this.eval_d) {
                int read = bufferedInputStream.read(bArr, 0, BitmapUtil.KB);
                if (read == -1) {
                    break;
                }
                randomAccessFile.write(bArr, 0, read);
                this.eval_e += (long) read;
                if (this.eval_e > this.eval_d) {
                    this.eval_g += (((long) read) - (this.eval_e - this.eval_d)) + 1;
                } else {
                    this.eval_g += (long) read;
                }
                i++;
                if (i == 50) {
                    Utils.sendMsg(1, this.eval_n, new long[]{this.eval_e, this.eval_d});
                    i = 0;
                }
                if (this.eval_e == this.eval_d) {
                    Utils.sendMsg(1, this.eval_n, new long[]{this.eval_e, this.eval_d});
                    i = 0;
                    DwBean dwBean = new DwBean();
                    dwBean.setDwSt(1);
                    dwBean.setDwNo(this.eval_h);
                    dwBean.setDwCode(this.eval_j);
                    dwBean.setDwFn(null);
                    dwBean.setDwFs(-1);
                    dwBean.setDwLi(null);
                    dwBean.setDwNotId(-1);
                    dwBean.setDwTi(this.eval_i);
                    saveDownloadSate(this.eval_k, dwBean);
                    String packageName = DwSei.getPackageName(this.eval_k, this.eval_b.getAbsolutePath());
                    if (packageName != null && !"".equals(packageName)) {
                        DwSei.orderPack.put(packageName, this.eval_j);
                    }
                    Utils.sendMsg(2, this.eval_n);
                }
            }
            bufferedInputStream.close();
            randomAccessFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            this.eval_f = true;
        }
    }
}
