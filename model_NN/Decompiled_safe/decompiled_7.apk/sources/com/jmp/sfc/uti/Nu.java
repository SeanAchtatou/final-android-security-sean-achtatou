package com.jmp.sfc.uti;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.chenzhiguangdongman.livewallpaper.SingleBall;
import com.jmp.sfc.db.Dbop;
import com.jmp.sfc.mod.PollingResp;
import com.jmp.sfc.net.DwService;
import com.jmp.sfc.ui.DSWV;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Nu {
    public static final String MOTIFYID = "motifyid";
    public static final String POLL = "poll";
    public static final String TITLE = "title";
    private static /* synthetic */ Notification a;
    private static /* synthetic */ NotificationManager eval_b;
    private static /* synthetic */ PendingIntent eval_c;
    private static /* synthetic */ Context eval_d;
    /* access modifiers changed from: private */
    public static /* synthetic */ List<PollingResp> eval_e;
    /* access modifiers changed from: private */
    public static /* synthetic */ Handler eval_f;

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(toString("A}vnzln", 132));
            }
            eval_f = new Handler() {
                static {
                    try {
                        if (System.currentTimeMillis() >= 1385222400000L) {
                            throw new RuntimeException(CT.getChars(5, "@~wa{oo"));
                        }
                    } catch (eval_u e) {
                    }
                }

                public void handleMessage(Message message) {
                    try {
                        super.handleMessage(message);
                        switch (message.what) {
                            case SingleBall.DIRECTION_ZX:
                            default:
                                return;
                            case SingleBall.DIRECTION_YX:
                                Object[] objArr = (Object[]) message.obj;
                                Nu.a((Context) objArr[2], (PollingResp) objArr[0], (Bitmap) objArr[1]);
                                return;
                        }
                    } catch (eval_u e) {
                    }
                }
            };
        } catch (eval_u e) {
        }
    }

    public Nu() {
    }

    public Nu(Context context, PollingResp pollingResp) {
        eval_d = context;
        pollingResp.setNotifyId(Utils.getCurrentID());
        String a2 = a(pollingResp.getmTitle());
        String a3 = a(pollingResp.getmContent());
        pollingResp.setmTitle(a2);
        pollingResp.setmContent(a3);
        pollingResp.setWhen(new SimpleDateFormat(CT.getChars(-21, "\u0006\u0001b*+p\u0019\u001ai98")).format(new Date(System.currentTimeMillis())));
        savePollingResp(context, pollingResp);
        refreshNot(context);
    }

    private final /* bridge */ /* synthetic */ String a(String str) {
        try {
            return str.replaceAll(CT.getChars(3, "?_[8Z\"7"), "");
        } catch (eval_u e) {
            return null;
        }
    }

    public static /* synthetic */ void a(Context context, PollingResp pollingResp, Bitmap bitmap) {
        try {
            eval_b(context, pollingResp, bitmap);
        } catch (eval_u e) {
        }
    }

    private static /* bridge */ /* synthetic */ void eval_b(Context context, PollingResp pollingResp, Bitmap bitmap) {
        try {
            if (eval_b == null) {
                eval_b = (NotificationManager) context.getSystemService(toString("22*6fhabplii", 252));
            }
            a = new Notification();
            a.icon = 17301545;
            a.tickerText = pollingResp.getmContent();
            if (pollingResp.getState() == 1) {
                a.flags |= 16;
            } else {
                a.flags |= 32;
            }
            Intent intent = new Intent(context, DSWV.class);
            intent.putExtra(toString("`a{ywkzp", 525), pollingResp.getNotifyId());
            intent.putExtra(CT.getChars(3, "wmqjb"), pollingResp.getmTitle());
            intent.setFlags(557056);
            eval_c = PendingIntent.getActivity(context, pollingResp.getNotifyId(), intent, 268435456);
            a.contentIntent = eval_c;
            MyLog.getIntence().i(toString("mhny", 25));
            Intent intent2 = new Intent(context, DwService.class);
            intent2.setAction(DwService.ACTION);
            a.deleteIntent = PendingIntent.getService(context, pollingResp.getNotifyId(), intent2, 268435456);
            a.setLatestEventInfo(context, pollingResp.getmTitle(), pollingResp.getmContent(), eval_c);
            ImageView recurseView = recurseView(LayoutInflater.from(context).inflate(a.contentView.getLayoutId(), (ViewGroup) null));
            if (bitmap != null) {
                a.contentView.setImageViewBitmap(recurseView.getId(), bitmap);
            } else {
                a.icon = 17301545;
            }
            eval_b.notify(pollingResp.getNotifyId(), a);
        } catch (eval_u e) {
        }
    }

    public static Bitmap getNotiPic(String str, String str2) {
        if (BitmapUtils.isExistSDCard() && BitmapUtils.isFileExists(str2)) {
            return BitmapFactory.decodeFile(str2);
        }
        return null;
    }

    public static ImageView recurseView(View view) {
        try {
            if (view instanceof ImageView) {
                return (ImageView) view;
            }
            if (view instanceof ViewGroup) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= ((ViewGroup) view).getChildCount()) {
                        break;
                    }
                    View childAt = ((ViewGroup) view).getChildAt(i2);
                    if (childAt instanceof ImageView) {
                        return (ImageView) childAt;
                    }
                    if (childAt instanceof ViewGroup) {
                        return recurseView(childAt);
                    }
                    i = i2 + 1;
                }
            }
            return null;
        } catch (eval_u e) {
            return null;
        }
    }

    public static void refreshNot(final Context context) {
        try {
            new Thread() {
                static {
                    try {
                        if (System.currentTimeMillis() >= 1385222400000L) {
                            throw new RuntimeException(Nu.toString("R`isiyy", 55));
                        }
                    } catch (eval_u e) {
                    }
                }

                public void run() {
                    try {
                        Dbop dbop = new Dbop();
                        dbop.open(context);
                        Nu.eval_e = dbop.selectPushInfo(0);
                        dbop.close();
                        if (Nu.eval_e != null && Nu.eval_e.size() > 0) {
                            for (int size = Nu.eval_e.size() - 1; size >= 0; size--) {
                                PollingResp pollingResp = (PollingResp) Nu.eval_e.get(size);
                                String str = pollingResp.getmTitleImage();
                                if (str != null && !"".equals(str)) {
                                    String substring = str.substring(str.lastIndexOf("/") + 1);
                                    String str2 = String.valueOf(BitmapUtils.getImgPath()) + File.separator + substring;
                                    Bitmap notiPic = Nu.getNotiPic(str, str2);
                                    if (notiPic == null && BitmapUtils.downFile(str, BitmapUtils.getImgPath(), substring)) {
                                        notiPic = Nu.getNotiPic(str, str2);
                                    }
                                    Utils.sendMsg(3, Nu.eval_f, new Object[]{pollingResp, notiPic, context});
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        } catch (eval_u e) {
        }
    }

    public static String toString(String str, int i) {
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        for (int i2 = 0; i2 != length; i2++) {
            i++;
            charArray[i2] = (char) ((i & '_') ^ charArray[i2]);
        }
        return String.valueOf(charArray, 0, length).intern();
    }

    public long savePollingResp(Context context, PollingResp pollingResp) {
        try {
            Dbop dbop = new Dbop();
            dbop.open(context);
            long insertPushInfo = dbop.isExistsPushInfoOrderCode(pollingResp.getmSmsCode()) <= 0 ? dbop.insertPushInfo(pollingResp) : dbop.updatePushInfo(pollingResp);
            dbop.close();
            return insertPushInfo;
        } catch (eval_u e) {
            return 0;
        }
    }
}
