package com.jmp.sfc.uti;

import android.util.Log;

public class MyLog {
    public static boolean ISDEBUG = true;
    private static /* synthetic */ String a;
    private static /* synthetic */ MyLog eval_b;

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(1127, "\u000209#9))"));
            }
            a = CT.getChars(4, "NpKnx|yc");
            eval_b = new MyLog();
        } catch (eval_t e) {
        }
    }

    private /* synthetic */ MyLog() {
    }

    public static MyLog getIntence() {
        return eval_b;
    }

    public void e(String str) {
        if (ISDEBUG) {
            Log.e(a, str);
        }
    }

    public void i(String str) {
        if (ISDEBUG) {
            Log.i(a, str);
        }
    }

    public void w(String str) {
        if (ISDEBUG) {
            Log.w(a, str);
        }
    }
}
