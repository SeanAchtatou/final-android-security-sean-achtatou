package com.jmp.sfc.uti;

import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class BitmapUtils {
    private static /* synthetic */ String a = null;

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(3339, "Nt}g}uu"));
            }
        } catch (eval_l e) {
        }
    }

    public static boolean downFile(String str, String str2, String str3) {
        if (!isExistSDCard()) {
            return false;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.connect();
            int contentLength = httpURLConnection.getContentLength();
            InputStream inputStream = httpURLConnection.getInputStream();
            if (contentLength <= 0) {
                return false;
            }
            if (inputStream == null) {
                return false;
            }
            File file = new File(str2, str3);
            if (!file.exists()) {
                File parentFile = file.getParentFile();
                if (!parentFile.exists()) {
                    parentFile.mkdirs();
                }
                file.createNewFile();
            } else if (file.exists()) {
                file.delete();
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[10240];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    return true;
                }
                fileOutputStream.write(bArr, 0, read);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getImgPath() {
        if (a == null) {
            a = Environment.getExternalStorageDirectory() + File.separator + Nu.toString("iqho", 3) + File.separator + Nu.toString("vmf", -97);
        }
        return a;
    }

    public static boolean isExistSDCard() {
        try {
            return Environment.getExternalStorageState().equals(CT.getChars(104, "%&?%8(*"));
        } catch (eval_l e) {
            return false;
        }
    }

    public static boolean isFileExists(String str) {
        try {
            return new File(str).exists();
        } catch (eval_l e) {
            return false;
        }
    }
}
