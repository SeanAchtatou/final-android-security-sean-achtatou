package com.jmp.sfc.uti;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import java.util.WeakHashMap;

public class BitmapUtil {
    public static final int BUFFER = 12288;
    public static final int KB = 1024;
    public static final int MB = 1048576;
    private static /* synthetic */ WeakHashMap<String, Bitmap> a;

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(4, "A}vnzln"));
            }
            a = new WeakHashMap<>();
        } catch (eval_k e) {
        }
    }

    private static /* bridge */ /* synthetic */ void a() {
        for (Bitmap next : a.values()) {
            if (next != null && !next.isRecycled()) {
                next.recycle();
            }
        }
        a.clear();
        System.gc();
    }

    public static Bitmap getBitmap(String str) {
        int i = 1;
        if (str == null || "".equals(str)) {
            return null;
        }
        if (!new File(str).exists()) {
            return null;
        }
        Bitmap bitmap = a.get(str);
        if (bitmap != null && !bitmap.isRecycled()) {
            return bitmap;
        }
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inJustDecodeBounds = false;
            int i2 = options.outHeight >= options.outWidth ? options.outHeight / 1280 : options.outWidth / 800;
            if (i2 >= 1) {
                i = i2;
            }
            options.inSampleSize = i;
            options.inDither = false;
            options.inTempStorage = new byte[BUFFER];
            options.inPurgeable = true;
            Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
            a.put(str, decodeFile);
            return decodeFile;
        } catch (OutOfMemoryError e) {
            if (bitmap != null) {
                bitmap.recycle();
            }
            a();
            return null;
        }
    }
}
