package com.jmp.sfc.uti;

import android.content.Context;

public class MResource {
    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(191, "Z81+1!!"));
            }
        } catch (eval_s e) {
        }
    }

    public static int getIdByName(Context context, String str, String str2) {
        try {
            Class<?>[] classes = Class.forName(String.valueOf(context.getPackageName()) + Nu.toString("i\u001a", 71)).getClasses();
            Class<?> cls = null;
            int i = 0;
            while (true) {
                if (i >= classes.length) {
                    break;
                } else if (classes[i].getName().split(CT.getChars(6, "Z#"))[1].equals(str)) {
                    cls = classes[i];
                    break;
                } else {
                    i++;
                }
            }
            if (cls != null) {
                return cls.getField(str2).getInt(cls);
            }
            return 0;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return 0;
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            return 0;
        } catch (SecurityException e3) {
            e3.printStackTrace();
            return 0;
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
            return 0;
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
            return 0;
        }
    }

    public static int[] getIdsByName(Context context, String str, String str2) {
        Class<?> cls;
        try {
            Class<?>[] classes = Class.forName(String.valueOf(context.getPackageName()) + CT.getChars(3, "-V")).getClasses();
            int i = 0;
            while (true) {
                if (i >= classes.length) {
                    cls = null;
                    break;
                } else if (classes[i].getName().split(CT.getChars(357, "\u0019b"))[1].equals(str)) {
                    cls = classes[i];
                    break;
                } else {
                    i++;
                }
            }
            if (cls == null || cls.getField(str2).get(cls) == null || !cls.getField(str2).get(cls).getClass().isArray()) {
                return null;
            }
            return (int[]) cls.getField(str2).get(cls);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            return null;
        } catch (SecurityException e3) {
            e3.printStackTrace();
            return null;
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
            return null;
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
            return null;
        }
    }
}
