package com.jmp.sfc.uti;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import com.jmp.sfc.db.Dbop;
import com.jmp.sfc.mod.DwBean;
import com.jmp.sfc.net.DwService;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class DwSer {
    private /* synthetic */ Context a;
    private /* synthetic */ Resu eval_b;

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(Nu.toString("A}vnzln", 4));
            }
        } catch (eval_q e) {
        }
    }

    public DwSer(Context context) {
        this.a = context;
    }

    public Resu getResu() {
        return this.eval_b;
    }

    public void init(int i) {
        if (i != -1) {
            Dbop dbop = new Dbop();
            dbop.open(this.a);
            DwBean queryDwInfo = dbop.queryDwInfo(i);
            dbop.close();
            if (queryDwInfo != null) {
                restart(queryDwInfo);
            }
        }
    }

    public void opNoti(int i, int i2, String str) {
        try {
            Notification notification = new Notification();
            notification.icon = 17301633;
            notification.tickerText = str;
            notification.when = System.currentTimeMillis();
            notification.flags |= 16;
            Intent intent = new Intent(this.a, DwService.class);
            intent.putExtra(CT.getChars(175, "kg_}"), i2);
            intent.setAction(DwService.DW_ACTION);
            notification.setLatestEventInfo(this.a, str, Nu.toString("]炾凳宀裏V", 6), PendingIntent.getService(this.a, i2, intent, 268435456));
            ((NotificationManager) this.a.getSystemService(CT.getChars(30, "ppthdjgdrngg"))).notify(i2, notification);
        } catch (eval_q e) {
        }
    }

    public void restart(DwBean dwBean) {
        long length;
        URL url;
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(), dwBean.getDwFn());
        long dwFs = dwBean.getDwFs();
        if (!file.exists()) {
            File parentFile = file.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();
            }
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            length = 0;
        } else {
            length = file.exists() ? file.length() : 0;
        }
        if (length > 0 && length == dwFs) {
            String packageName = DwSei.getPackageName(this.a, file.getAbsolutePath());
            if (packageName != null && !"".equals(packageName)) {
                DwSei.orderPack.put(packageName, dwBean.getDwCode());
            }
            int i = (int) ((((float) length) / ((float) dwFs)) * 100.0f);
            this.a.startActivity(DwSei.getFileIntent(file));
        } else if (!Utils.isNetworkConnected(this.a)) {
            Utils.DisplayToast(Nu.toString("诲还探罙绕", 5), this.a);
        } else {
            try {
                url = new URL(dwBean.getDwLi());
            } catch (MalformedURLException e2) {
                e2.printStackTrace();
                url = null;
            }
            if (url != null && file.exists() && dwFs != 0) {
                this.eval_b = new Resu(url, file, length, dwBean.getDwFs(), dwBean.getDwNo(), dwBean.getDwTi(), dwBean.getDwCode(), this.a);
                int dwFs2 = (int) ((((float) length) / ((float) dwBean.getDwFs())) * 100.0f);
                this.eval_b.start();
            }
        }
    }

    public void setResu(Resu resu) {
        try {
            this.eval_b = resu;
        } catch (eval_q e) {
        }
    }
}
