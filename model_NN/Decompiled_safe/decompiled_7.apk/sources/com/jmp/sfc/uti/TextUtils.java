package com.jmp.sfc.uti;

import android.content.Context;
import com.jmp.sfc.mod.PollingResp;

public class TextUtils {
    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(Nu.toString("Cx`xnh", 6));
            }
        } catch (eval_x e) {
        }
    }

    public static boolean checkString(String str) {
        try {
            return str.matches(CT.getChars(39, "\\8$3V&"));
        } catch (eval_x e) {
            return false;
        }
    }

    public static int getDelayTime(String str) {
        if (str == null || "".endsWith(str)) {
            outputLog(str);
            return 0;
        }
        String trim = str.trim();
        if (!trim.contains("{") || !trim.contains("}")) {
            return 0;
        }
        String substring = trim.substring(trim.indexOf("{") + 1, trim.lastIndexOf("}"));
        if (!substring.contains("^")) {
            return 0;
        }
        String[] split = substring.split(Nu.toString("\u0015\u0014", 105));
        if (split.length == 2) {
            return Integer.parseInt(split[1]) * 60 * 1000;
        }
        return 0;
    }

    public static void outputLog(String str) {
        try {
            if (CT.getChars(-91, "5678").equals(str)) {
                MyLog.getIntence().i(CT.getChars(3, "泫冈头责"));
            } else if (CT.getChars(19, "#$%'").equals(str)) {
                MyLog.getIntence().i(CT.getChars(-30, "沪减扔勚"));
            } else if (Nu.toString("4561", 4).equals(str)) {
                MyLog.getIntence().i(Nu.toString("乘筰呟杕勸嘲寒儴匒诰", 1525));
            } else if (Nu.toString("678>", 6).equals(str)) {
                MyLog.getIntence().i(CT.getChars(2, "量夎泬冉"));
            } else if (Nu.toString("#$%.", 51).equals(str)) {
                MyLog.getIntence().i(CT.getChars(6, "敶捩夌琏趏旽"));
            } else if (CT.getChars(4, "456>").equals(str)) {
                MyLog.getIntence().i(Nu.toString("釈夋泯冄", 5));
            } else if (Nu.toString("vwyy", 70).equals(str)) {
                MyLog.getIntence().i(CT.getChars(123, "醖契掍仺"));
            } else if (str == null || "".equals(str)) {
                MyLog.getIntence().i(Nu.toString("跆斲", 67));
            }
        } catch (eval_x e) {
        }
    }

    public static int parseClick(String str) {
        if (str == null) {
            return 0;
        }
        try {
            if ("".endsWith(str)) {
                return 0;
            }
            String trim = str.trim();
            if (!trim.contains("{") || !trim.contains("}")) {
                return 0;
            }
            String substring = trim.substring(trim.indexOf("{") + 1, trim.lastIndexOf("}"));
            if (CT.getChars(64, "pqrp").equals(substring)) {
                return 1;
            }
            return (CT.getChars(3, "3452").equals(substring) || Nu.toString("cdec", -13).equals(substring) || Nu.toString("6781", 6).equals(substring)) ? 2 : 0;
        } catch (eval_x e) {
            return 0;
        }
    }

    public static PollingResp parseDpData(String str) {
        Exception e;
        PollingResp pollingResp;
        if (str == null || "".endsWith(str)) {
            return null;
        }
        try {
            if (!str.contains("{") || !str.contains("}")) {
                return null;
            }
            String substring = str.substring(str.indexOf("{") + 1, str.lastIndexOf("}"));
            if (substring.contains("^")) {
                String[] split = substring.replace("^", CT.getChars(124, "~")).split(CT.getChars(-20, "on"));
                return new PollingResp(CT.getChars(305, "!\"#$"), split[0], split[1], split[3], split[2], split[4], split[5], parseLong(split[6]));
            }
            pollingResp = new PollingResp();
            try {
                pollingResp.setmResultCode(substring);
                return pollingResp;
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            e = e3;
            pollingResp = null;
            e.printStackTrace();
            return pollingResp;
        }
    }

    public static int parseInt(String str) {
        try {
            if (checkString(str)) {
                return Integer.parseInt(str);
            }
            return -1;
        } catch (eval_x e) {
            return 0;
        }
    }

    public static boolean parseLogin(String str, Context context) {
        if (str == null || "".endsWith(str)) {
            outputLog(str);
            return false;
        } else if (str.equals(CT.getChars(130, "VJI@IR\\"))) {
            return false;
        } else {
            String trim = str.trim();
            if (trim.contains("{") && trim.contains("}")) {
                String substring = trim.substring(trim.indexOf("{") + 1, trim.lastIndexOf("}"));
                if (substring.contains("^")) {
                    substring = substring.split(Nu.toString("\u000f\n", -13))[0];
                }
                outputLog(substring);
                if (Nu.toString("o012", -1).equals(substring) || CT.getChars(86, "fghl").equals(substring)) {
                    return false;
                }
                if (CT.getChars(85, "efga").equals(substring)) {
                    SharedUtils.getIntence().saveLoginState(context);
                    return true;
                }
                try {
                    if (CT.getChars(6, "6788").equals(substring)) {
                        SharedUtils.getIntence().saveLoginState(context);
                        return true;
                    } else if (!Nu.toString("4567", 4).equals(substring) && !Nu.toString("#$%%", 147).equals(substring) && !Nu.toString("? !&", -81).equals(substring) && !Nu.toString("1231", 1).equals(substring) && !CT.getChars(1849, ")*+*").equals(substring) && !CT.getChars(15, "? !*").equals(substring) && !CT.getChars(8, "89;;").equals(substring)) {
                        SharedUtils.getIntence().saveLogin(context, substring);
                        return true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return false;
        }
    }

    public static long parseLong(String str) {
        try {
            if (checkString(str)) {
                return Long.parseLong(str);
            }
            return -1;
        } catch (eval_x e) {
            return 0;
        }
    }

    public static String parsePRCode(String str) {
        if (str == null || "".equals(str)) {
            return null;
        }
        try {
            if (!str.contains("{") || !str.contains("}")) {
                return null;
            }
            return str.substring(str.indexOf("{") + 1, str.lastIndexOf("}"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String parsePtCode(String str) {
        if (str == null || "".equals(str)) {
            return null;
        }
        try {
            if (!str.contains("{") || !str.contains("}")) {
                return null;
            }
            return str.substring(str.indexOf("{") + 1, str.lastIndexOf("}"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isRepeat(String str, String str2) {
        try {
            return !str.equals(str2);
        } catch (eval_x e) {
            return false;
        }
    }
}
