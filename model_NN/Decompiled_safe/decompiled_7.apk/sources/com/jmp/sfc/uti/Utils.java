package com.jmp.sfc.uti;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import java.lang.reflect.Field;

public class Utils {
    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(5, "@~wa{oo"));
            }
        } catch (eval_z e) {
        }
    }

    public static void DisplayToast(String str, Context context) {
        try {
            Toast.makeText(context, str, 0).show();
        } catch (eval_z e) {
        }
    }

    public static String getAppKeyInfo(Context context) {
        try {
            String string = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(CT.getChars(589, ",>?;4+"));
            return string.contains(CT.getChars(2255, "`")) ? string.substring(2, string.length()) : Nu.toString("}z", 73);
        } catch (Exception e) {
            Exception exc = e;
            String chars = CT.getChars(3, "741");
            exc.printStackTrace();
            return chars;
        }
    }

    public static int getCurrentID() {
        try {
            String valueOf = String.valueOf(System.currentTimeMillis());
            return Integer.parseInt(valueOf.substring(valueOf.length() - 8, valueOf.length()));
        } catch (eval_z e) {
            return 0;
        }
    }

    public static String getDeveloperInfo(Context context) {
        try {
            String string = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(Nu.toString("!#1-%%;)?%*)", -59));
            return string.contains(Nu.toString("lk", 323)) ? string.substring(2, string.length()) : CT.getChars(97, "urw");
        } catch (Exception e) {
            Exception exc = e;
            String nu = Nu.toString("jo4", 126);
            exc.printStackTrace();
            return nu;
        }
    }

    public static int getIdByReflection(Context context, String str, String str2) {
        try {
            Field field = Class.forName(String.valueOf(context.getPackageName()) + Nu.toString("7H?", 665) + str).getField(str2);
            return Integer.parseInt(field.get(field.getName()).toString());
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Nu.toString("4774>?)7)iu{", 1911));
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
        if (connectivityManager.getNetworkInfo(1).isConnected()) {
            return true;
        }
        return networkInfo.isConnected();
    }

    public static void savePreparedInt(SharedPreferences.Editor editor, String str, String str2, int i) {
        try {
            editor.putInt(str2, i);
            editor.commit();
        } catch (eval_z e) {
        }
    }

    public static void sendMsg(int i, Handler handler) {
        try {
            Message message = new Message();
            message.what = i;
            handler.sendMessage(message);
        } catch (eval_z e) {
        }
    }

    public static void sendMsg(int i, Handler handler, Object obj) {
        try {
            Message message = new Message();
            message.what = i;
            message.obj = obj;
            handler.sendMessage(message);
        } catch (eval_z e) {
        }
    }
}
