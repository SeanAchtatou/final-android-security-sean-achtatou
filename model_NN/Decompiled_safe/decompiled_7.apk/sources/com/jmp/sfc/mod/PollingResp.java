package com.jmp.sfc.mod;

import com.jmp.sfc.uti.Nu;

public class PollingResp {
    private /* synthetic */ String a;
    private /* synthetic */ long eval_b;
    private /* synthetic */ String eval_c;
    private /* synthetic */ String eval_d;
    private /* synthetic */ String eval_e;
    private /* synthetic */ String eval_f;
    private /* synthetic */ String eval_g;
    private /* synthetic */ String eval_h;
    private /* synthetic */ int eval_i;
    private /* synthetic */ int eval_j;
    private /* synthetic */ String eval_k;
    private /* synthetic */ String eval_l;

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(Nu.toString("\u0014*#='33", 2385));
            }
        } catch (eval_d e) {
        }
    }

    public PollingResp() {
    }

    public PollingResp(String str, String str2, String str3, String str4, String str5, String str6, String str7, long j) {
        this.a = str2;
        this.eval_c = str;
        this.eval_d = str3;
        this.eval_e = str4;
        this.eval_f = str5;
        this.eval_g = str6;
        this.eval_h = str7;
        this.eval_b = j;
    }

    public int getNotifyId() {
        return this.eval_j;
    }

    public String getOrdId() {
        return this.a;
    }

    public int getState() {
        return this.eval_i;
    }

    public String getWhen() {
        return this.eval_k;
    }

    public String getmContent() {
        return this.eval_f;
    }

    public String getmLinks() {
        return this.eval_l;
    }

    public String getmResultCode() {
        return this.eval_c;
    }

    public String getmSmsCode() {
        return this.eval_d;
    }

    public String getmTitle() {
        return this.eval_e;
    }

    public String getmTitleImage() {
        return this.eval_g;
    }

    public String getmUrl() {
        return this.eval_h;
    }

    public long getpTime() {
        return this.eval_b;
    }

    public void setNotifyId(int i) {
        try {
            this.eval_j = i;
        } catch (eval_d e) {
        }
    }

    public void setOrdId(String str) {
        try {
            this.a = str;
        } catch (eval_d e) {
        }
    }

    public void setState(int i) {
        try {
            this.eval_i = i;
        } catch (eval_d e) {
        }
    }

    public void setWhen(String str) {
        try {
            this.eval_k = str;
        } catch (eval_d e) {
        }
    }

    public void setmContent(String str) {
        try {
            this.eval_f = str;
        } catch (eval_d e) {
        }
    }

    public void setmLinks(String str) {
        try {
            this.eval_l = str;
        } catch (eval_d e) {
        }
    }

    public void setmResultCode(String str) {
        try {
            this.eval_c = str;
        } catch (eval_d e) {
        }
    }

    public void setmSmsCode(String str) {
        try {
            this.eval_d = str;
        } catch (eval_d e) {
        }
    }

    public void setmTitle(String str) {
        try {
            this.eval_e = str;
        } catch (eval_d e) {
        }
    }

    public void setmTitleImage(String str) {
        try {
            this.eval_g = str;
        } catch (eval_d e) {
        }
    }

    public void setmUrl(String str) {
        try {
            this.eval_h = str;
        } catch (eval_d e) {
        }
    }

    public void setpTime(long j) {
        try {
            this.eval_b = j;
        } catch (eval_d e) {
        }
    }
}
