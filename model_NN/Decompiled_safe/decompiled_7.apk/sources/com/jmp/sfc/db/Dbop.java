package com.jmp.sfc.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.jmp.sfc.mod.DwBean;
import com.jmp.sfc.mod.PollingResp;
import com.jmp.sfc.uti.CT;
import com.jmp.sfc.uti.Nu;
import java.util.ArrayList;
import java.util.List;

public class Dbop {
    private static /* synthetic */ String a;
    /* access modifiers changed from: private */
    public static /* synthetic */ String eval_b;
    /* access modifiers changed from: private */
    public static /* synthetic */ int eval_c = 4;
    private static /* synthetic */ String eval_d;
    /* access modifiers changed from: private */
    public static /* synthetic */ String eval_e;
    private static /* synthetic */ String eval_f;
    /* access modifiers changed from: private */
    public static /* synthetic */ String eval_g;
    private static /* synthetic */ String eval_h;
    /* access modifiers changed from: private */
    public static /* synthetic */ String eval_i;
    private static /* synthetic */ String eval_j;
    /* access modifiers changed from: private */
    public static /* synthetic */ String eval_k;
    private static /* synthetic */ DataBaseHelper eval_l;
    private /* synthetic */ SQLiteDatabase eval_m;

    public static class DataBaseHelper extends SQLiteOpenHelper {
        static {
            try {
                if (System.currentTimeMillis() >= 1385222400000L) {
                    throw new RuntimeException(CT.getChars(3, "F|uoumm"));
                }
            } catch (a e) {
            }
        }

        public DataBaseHelper(Context context) {
            super(context, Dbop.eval_b, (SQLiteDatabase.CursorFactory) null, Dbop.eval_c);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            try {
                sQLiteDatabase.execSQL(Dbop.eval_e);
                sQLiteDatabase.execSQL(Dbop.eval_g);
                sQLiteDatabase.execSQL(Dbop.eval_i);
                sQLiteDatabase.execSQL(Dbop.eval_k);
            } catch (a e) {
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    static {
        try {
            if (System.currentTimeMillis() >= 1385222400000L) {
                throw new RuntimeException(CT.getChars(3, "F|uoumm"));
            }
            a = CT.getChars(551, "CjFz");
            eval_b = CT.getChars(-52, "\u0006 >\u0010\u0014\u0013");
            eval_d = Nu.toString("[y~fF~w}L@tt{}", 651);
            eval_e = CT.getChars(47, "lbtsgq5bvzu;u{>q/5b&<,53;i\u001a>?%\u0007!6>\r\u000757:2xq3?|40+%&'1d\u000b\t\u0013h\u0007\u001f\u0007\u0000m\u001e\u001d\u0019\u001c\u0013\u0001\ru\u001d\u0012\u0001y;.(271csgnakr+ff~bjtGk0x|gqrse4zuy=jzxu.wmqjb(}osx!m`~ew}`5br`m6ouirz\t,#$!e2\"0=f>>!n;5)&'!7#=y35(89:2m5+!+\u0012.%,j?)5:f");
            eval_f = Nu.toString("[}gaw{tPt}sBj~\"-'", 50);
            eval_g = Nu.toString("eumh~n,yom|t2zr5xxl9cunjl HlppdjkAgldSyom|t:zp5yl|}~n=PP\u0014a\f\u0016\b\tf\u0017\u001a\u0000\u0007\n\u001e\u0014n\u0004\u0015\br2!!9>6:(>180+l(,%+e2\"0=c", 6);
            eval_h = Nu.toString("&4\u001b1'%$,", -62);
            eval_i = CT.getChars(4, "gwcf|l*mobj0xt3zzb7}ashhn>{w^vbfic/am*bbykhuc2][A6YMUV;LOWR\u0001\u0013\u001bc\u000f\u0000\u001fg)<>$%#-=5<7= y2 \u00166z22);8%3n'3\u0011/g<,2?`)9\u00039q&6,!z3/\u001f4{(8&+,euEw%oi|lmn~!jxCe2zzasp}k6k^q{e!vf|q*cGeEi.f~ewtqg?");
            eval_j = CT.getChars(4, "tlbX|hhgi");
            eval_k = CT.getChars(46, "m}upfv4awut|:rz=pp4a';-624h9#/\u00139/-<4z:0u?9,<=>.}\u0010\u0010\u0014a\f\u0016\b\tf\u0017\u001a\u0000\u0007\n\u001e\u0014n\u0004\u0015\br2!!9>6:(>180+,qkgP}r'|lr }Zf}t2zzasp}k3");
        } catch (a e) {
        }
    }

    public void close() {
        try {
            this.eval_m.close();
            eval_l.close();
        } catch (a e) {
        }
    }

    public void closeCursor(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    public int delDw(String str) {
        try {
            return this.eval_m.delete(eval_h, Nu.toString("3/\u001a5?9`a", 1271), new String[]{str});
        } catch (a e) {
            return 0;
        }
    }

    public int deleteInstallInfo(String str) {
        try {
            return this.eval_m.delete(eval_f, CT.getChars(4, "mk`h56"), new String[]{str});
        } catch (a e) {
            return 0;
        }
    }

    public int deletePushInfo(String str) {
        try {
            return this.eval_m.delete(eval_d, Nu.toString("'%?%+7\u00064lm", 713), new String[]{str});
        } catch (a e) {
            return 0;
        }
    }

    public int deletePushInfoByState() {
        try {
            return this.eval_m.delete(eval_d, Nu.toString(">:.$4ol", 333), new String[]{"1"});
        } catch (a e) {
            return 0;
        }
    }

    public int deletePushInfoByc(String str) {
        try {
            return this.eval_m.delete(eval_d, Nu.toString("s~vv)*", 48), new String[]{str});
        } catch (a e) {
            return 0;
        }
    }

    public long getCurrentDwCount(String str) {
        Cursor query = this.eval_m.query(eval_h, new String[]{Nu.toString("firf}\"!%", 5)}, CT.getChars(3, "gsFicm45"), new String[]{String.valueOf(str)}, null, null, null);
        long j = query.moveToFirst() ? query.getLong(0) : -1;
        closeCursor(query);
        return j;
    }

    public long getCurrentPushInfoCount() {
        Cursor query = this.eval_m.query(eval_d, new String[]{Nu.toString("uxmwn364", 54)}, CT.getChars(3, "ppdrb56"), new String[]{"0"}, null, null, null);
        long j = query.moveToFirst() ? query.getLong(0) : -1;
        closeCursor(query);
        return j;
    }

    public SQLiteDatabase getmDb() {
        return this.eval_m;
    }

    public long insertDwData(DwBean dwBean) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Nu.toString("kSqk\t%", 59), Integer.valueOf(dwBean.getDwNotId()));
            contentValues.put(Nu.toString("aqSa", 5), dwBean.getDwTi());
            contentValues.put(Nu.toString("`rJn", 4), dwBean.getDwLi());
            contentValues.put(CT.getChars(-26, "\"0\u001b="), Integer.valueOf(dwBean.getDwSt()));
            contentValues.put(Nu.toString("k[m", 1947), Long.valueOf(dwBean.getDwFs()));
            contentValues.put(Nu.toString("kg_}", 47), Integer.valueOf(dwBean.getDwNo()));
            contentValues.put(CT.getChars(6, "bpNg"), dwBean.getDwFn());
            contentValues.put(CT.getChars(-35, "9)\u001c/%'"), dwBean.getDwCode());
            return this.eval_m.insert(eval_h, Nu.toString("=!9:\u001475/62\u0015?<k", 115), contentValues);
        } catch (a e) {
            return 0;
        }
    }

    public long insertInstallInfo(String str) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(CT.getChars(210, ";=2:"), str);
            return this.eval_m.insert(eval_f, CT.getChars(5, "kskdJegy``Gqry"), contentValues);
        } catch (a e) {
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public long insertPushInfo(PollingResp pollingResp) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(CT.getChars(1, "bmga"), pollingResp.getmSmsCode());
            contentValues.put(Nu.toString("?%9\"*", 1739), pollingResp.getmTitle());
            contentValues.put(CT.getChars(4, "gjhsmg~"), pollingResp.getmContent());
            contentValues.put(CT.getChars(3, "wmqjbAdkli"), pollingResp.getmTitleImage());
            contentValues.put(Nu.toString("ptk", 5), pollingResp.getmUrl());
            contentValues.put(CT.getChars(4, "smci\\`gn"), pollingResp.getWhen());
            contentValues.put(CT.getChars(957, "nj~4$"), (Integer) 0);
            contentValues.put(Nu.toString("-+1/!1\u0000.", 195), Integer.valueOf(pollingResp.getNotifyId()));
            return this.eval_m.insert(eval_d, Nu.toString("}ayzTwuovrU|k", 19), contentValues);
        } catch (a e) {
            return 0;
        }
    }

    public long isExistsPushID() {
        Cursor query = this.eval_m.query(eval_j, new String[]{Nu.toString("gjsi|! \"", 132)}, CT.getChars(123, "28`a"), new String[]{"1"}, null, null, null);
        long j = -1;
        if (query.moveToFirst()) {
            j = query.getLong(0);
        }
        closeCursor(query);
        return j;
    }

    public long isExistsPushInfoOrderCode(String str) {
        Cursor query = this.eval_m.query(eval_d, new String[]{Nu.toString("gjsi|! \"", 4)}, Nu.toString("ehll74", 6), new String[]{str}, null, null, null);
        long j = -1;
        if (query.moveToFirst()) {
            j = query.getLong(0);
        }
        closeCursor(query);
        return j;
    }

    public void open(Context context) {
        try {
            eval_l = new DataBaseHelper(context);
            this.eval_m = eval_l.getWritableDatabase();
        } catch (a e) {
        }
    }

    public DwBean queryDwInfo(int i) {
        DwBean dwBean = null;
        Cursor query = this.eval_m.query(eval_h, new String[]{CT.getChars(-5, "?+\u00131"), Nu.toString("=-\u001d/", 1113), CT.getChars(149, "qa[q"), CT.getChars(4, "`rUs"), CT.getChars(1815, "so_t"), Nu.toString("m}Hcik", 41), Nu.toString("%5\r+1\u000f#", -31), CT.getChars(31, "{wUk")}, CT.getChars(1767, "#?\u0007%vs"), new String[]{String.valueOf(i)}, null, null, null);
        if (query.moveToFirst()) {
            dwBean = new DwBean();
            dwBean.setDwNo(query.getInt(0));
            dwBean.setDwFs(query.getLong(1));
            dwBean.setDwLi(query.getString(2));
            dwBean.setDwSt(query.getInt(3));
            dwBean.setDwFn(query.getString(4));
            dwBean.setDwCode(query.getString(5));
            dwBean.setDwNotId(query.getInt(6));
            dwBean.setDwTi(query.getString(7));
        }
        closeCursor(query);
        return dwBean;
    }

    public List<String> queryInstallInfo() {
        ArrayList arrayList = null;
        Cursor query = this.eval_m.query(eval_f, new String[]{CT.getChars(6, "oinf")}, null, null, null, null, null);
        if (query.moveToFirst()) {
            arrayList = new ArrayList();
            do {
                arrayList.add(query.getString(0));
            } while (query.moveToNext());
        }
        closeCursor(query);
        return arrayList;
    }

    public String queryPID() {
        String str = null;
        Cursor query = this.eval_m.query(eval_j, new String[]{Nu.toString("?95\u0006+ ", -17)}, Nu.toString("zp()", 51), new String[]{"1"}, null, null, null);
        if (query.moveToFirst()) {
            str = query.getString(0);
        }
        closeCursor(query);
        return str;
    }

    public DwBean selDwInf(String str) {
        DwBean dwBean = null;
        Cursor query = this.eval_m.query(eval_h, new String[]{CT.getChars(819, "wc[y"), Nu.toString("gsCu", 131), CT.getChars(172, "hzBf"), Nu.toString("vdGa", 50), CT.getChars(43, "o{K`"), CT.getChars(-61, "'3\u0006)#-"), Nu.toString("soWuoUy", 55), Nu.toString("aqSa", 5)}, Nu.toString("bpKfnn12", 6), new String[]{str}, null, null, null);
        if (query.moveToFirst()) {
            dwBean = new DwBean();
            dwBean.setDwNo(query.getInt(0));
            dwBean.setDwFs(query.getLong(1));
            dwBean.setDwLi(query.getString(2));
            dwBean.setDwSt(query.getInt(3));
            dwBean.setDwFn(query.getString(4));
            dwBean.setDwCode(query.getString(5));
            dwBean.setDwNotId(query.getInt(6));
            dwBean.setDwTi(query.getString(7));
        }
        closeCursor(query);
        return dwBean;
    }

    public DwBean selDwInfBySt(String str, int i) {
        DwBean dwBean = null;
        Cursor query = this.eval_m.query(eval_h, new String[]{Nu.toString("gsKi", 3), CT.getChars(329, "-=\r?"), Nu.toString("{7\r+", -65), Nu.toString("#?\u001a>", 455), Nu.toString("ueUz", 145), Nu.toString("ueP{qs", 177), Nu.toString("qaYwmS", 693), Nu.toString("bp\\`", 134)}, CT.getChars(-60, " 2\u0005(,,wtl, +p5%\u0000 hi"), new String[]{str, String.valueOf(i)}, null, null, null);
        if (query.moveToFirst()) {
            dwBean = new DwBean();
            dwBean.setDwNo(query.getInt(0));
            dwBean.setDwFs(query.getLong(1));
            dwBean.setDwLi(query.getString(2));
            dwBean.setDwSt(query.getInt(3));
            dwBean.setDwFn(query.getString(4));
            dwBean.setDwCode(query.getString(5));
            dwBean.setDwNotId(query.getInt(6));
            dwBean.setDwTi(query.getString(7));
        }
        closeCursor(query);
        return dwBean;
    }

    public List<DwBean> selDwInfBySt(int i) {
        ArrayList arrayList = new ArrayList();
        Cursor query = this.eval_m.query(eval_h, new String[]{CT.getChars(245, "1!\u00197"), CT.getChars(-50, "*8\u0016\""), CT.getChars(2623, "{7\r+"), Nu.toString("bp[}", 6), CT.getChars(925, "yiYn"), Nu.toString("kgR}wq", 47), Nu.toString("1!\u00197-\u0013?", 85), Nu.toString("!1\u0013!", 69)}, Nu.toString("bp[}74", 6), new String[]{String.valueOf(i)}, null, null, null);
        if (query.moveToFirst()) {
            do {
                DwBean dwBean = new DwBean();
                dwBean.setDwNo(query.getInt(0));
                dwBean.setDwFs(query.getLong(1));
                dwBean.setDwLi(query.getString(2));
                dwBean.setDwSt(query.getInt(3));
                dwBean.setDwFn(query.getString(4));
                dwBean.setDwCode(query.getString(5));
                dwBean.setDwNotId(query.getInt(6));
                dwBean.setDwTi(query.getString(7));
                arrayList.add(dwBean);
            } while (query.moveToNext());
        }
        closeCursor(query);
        return arrayList;
    }

    public List<PollingResp> selectPushInfo(int i) {
        String[] strArr;
        ArrayList arrayList = new ArrayList();
        String[] strArr2 = {CT.getChars(1739, "(#)+"), CT.getChars(-61, "7-1*\""), CT.getChars(4, "gjhsmg~"), CT.getChars(166, "rn|eoBalij"), Nu.toString("vvi", 3), CT.getChars(231, "4<(>."), Nu.toString("ca{ywkZp", 3213), Nu.toString("3-#)\u001c '.", 68)};
        String str = "";
        if (i == 0) {
            strArr = null;
            str = null;
        } else if (i == 1) {
            str = CT.getChars(2193, "bfr`p+(");
            strArr = new String[]{"1"};
        } else {
            strArr = null;
        }
        Cursor query = this.eval_m.query(eval_d, strArr2, str, strArr, null, null, CT.getChars(119, " 0<4\u000f50;DDQ@"), null);
        if (query.moveToFirst()) {
            do {
                PollingResp pollingResp = new PollingResp();
                pollingResp.setmSmsCode(query.getString(0));
                pollingResp.setmTitle(query.getString(1));
                pollingResp.setmContent(query.getString(2));
                pollingResp.setmTitleImage(query.getString(3));
                pollingResp.setmUrl(query.getString(4));
                pollingResp.setState(query.getInt(5));
                pollingResp.setNotifyId(query.getInt(6));
                pollingResp.setWhen(query.getString(7));
                arrayList.add(pollingResp);
            } while (query.moveToPrevious());
        }
        closeCursor(query);
        return arrayList;
    }

    public PollingResp selectPushInfoFornotifyId(int i) {
        PollingResp pollingResp = null;
        Cursor query = this.eval_m.query(eval_d, new String[]{CT.getChars(459, "(#)+"), Nu.toString("rn|eo", 6), CT.getChars(-32, "#.,7!+2"), Nu.toString("yg{|t[~urs", 1325), CT.getChars(585, "<8'"), Nu.toString("88,:*", 715), Nu.toString("kisaosBh", 5), Nu.toString("9'5?\u0006:90", 78)}, Nu.toString("ecygiiXv.+5", 171), new String[]{String.valueOf(i)}, null, null, null, null);
        if (query.moveToFirst()) {
            pollingResp = new PollingResp();
            pollingResp.setmSmsCode(query.getString(0));
            pollingResp.setmTitle(query.getString(1));
            pollingResp.setmContent(query.getString(2));
            pollingResp.setmTitleImage(query.getString(3));
            pollingResp.setmUrl(query.getString(4));
            pollingResp.setState(query.getInt(5));
            pollingResp.setNotifyId(query.getInt(6));
            pollingResp.setWhen(query.getString(7));
        }
        closeCursor(query);
        return pollingResp;
    }

    public long updateDwData(DwBean dwBean) {
        ContentValues contentValues = new ContentValues();
        if (dwBean.getDwTi() != null && !dwBean.getDwTi().equals("")) {
            contentValues.put(Nu.toString("bp\\`", 6), dwBean.getDwTi());
        }
        if (dwBean.getDwLi() != null && !dwBean.getDwLi().equals("")) {
            contentValues.put(CT.getChars(693, "qa[q"), dwBean.getDwLi());
        }
        if (dwBean.getDwSt() != -1) {
            contentValues.put(CT.getChars(-64, "$6\u00117"), Integer.valueOf(dwBean.getDwSt()));
        }
        if (dwBean.getDwFs() > -1) {
            contentValues.put(CT.getChars(137, "m}M"), Long.valueOf(dwBean.getDwFs()));
        }
        if (dwBean.getDwFn() != null && "".equals(dwBean.getDwFn())) {
            contentValues.put(CT.getChars(13, "iyI~"), dwBean.getDwFn());
        }
        if (dwBean.getDwCode() != null && "".equals(dwBean.getDwCode())) {
            contentValues.put(Nu.toString("?+\u001e1;e", 1403), dwBean.getDwCode());
        }
        if (dwBean.getDwNotId() != -1) {
            contentValues.put(CT.getChars(5, "aqIg}Co"), Integer.valueOf(dwBean.getDwNotId()));
        }
        return (long) this.eval_m.update(eval_h, contentValues, CT.getChars(59, "kSq\""), new String[]{String.valueOf(dwBean.getDwNo())});
    }

    public long updatePID(String str, long j, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Nu.toString(")3?\b%*", -39), str);
        contentValues.put(Nu.toString("3\u0010,+\"", -29), Long.valueOf(j));
        if (i == 0) {
            return this.eval_m.insert(eval_j, CT.getChars(98, ",6()\u0005($<'%\u0004,-$"), contentValues);
        }
        return (long) this.eval_m.update(eval_j, contentValues, Nu.toString("hf>;", 129), new String[]{"1"});
    }

    public long updatePushInfo(PollingResp pollingResp) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(CT.getChars(6, "rn|eo"), pollingResp.getmTitle());
            contentValues.put(Nu.toString("ehf}oex", 6), pollingResp.getmContent());
            contentValues.put(CT.getChars(2583, "cqmv~Upxe"), pollingResp.getmTitleImage());
            contentValues.put(Nu.toString("hls", 61), pollingResp.getmUrl());
            contentValues.put(CT.getChars(76, ";%+!\u00048?6"), pollingResp.getWhen());
            return (long) this.eval_m.update(eval_d, contentValues, Nu.toString("ficm45", 1189), new String[]{pollingResp.getmSmsCode()});
        } catch (a e) {
            return 0;
        }
    }

    public long updateState(int i) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Nu.toString("wqgsm", 4), "1");
            return (long) this.eval_m.update(eval_d, contentValues, CT.getChars(24, "vvnrzdw{=>"), new String[]{String.valueOf(i)});
        } catch (a e) {
            return 0;
        }
    }
}
