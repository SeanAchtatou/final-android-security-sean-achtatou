package com.iua.unla;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class h {
    private static String a;
    private static String b;
    private static String c;
    private static String d;
    private static String e;
    /* access modifiers changed from: private */
    public static String f;
    /* access modifiers changed from: private */
    public static String g;
    /* access modifiers changed from: private */
    public static String h;
    /* access modifiers changed from: private */
    public static String i;
    /* access modifiers changed from: private */
    public static String j;
    /* access modifiers changed from: private */
    public static String k;
    private static String l;
    private static String m;
    private static SharedPreferences n;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("s").append("i").append("l").append("t").append("i").append("m");
        a = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("c").append("a").append("k").append("e");
        b = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("d").append("e").append("t").append("y");
        c = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("g").append("a").append("p").append("t");
        d = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append("u").append("n").append("s").append("s").append("p");
        e = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
        sb6.append("i").append("i").append("d");
        f = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
        sb7.append("s").append("v").append("i").append("o").append("n");
        g = sb7.toString();
        StringBuilder sb8 = new StringBuilder();
        sb8.append("u").append("s").append("p");
        h = sb8.toString();
        StringBuilder sb9 = new StringBuilder();
        sb9.append("b").append("r").append("p");
        i = sb9.toString();
        StringBuilder sb10 = new StringBuilder();
        sb10.append("j").append("p");
        j = sb10.toString();
        StringBuilder sb11 = new StringBuilder();
        sb11.append("k").append("d").append("o").append("a");
        k = sb11.toString();
        StringBuilder sb12 = new StringBuilder();
        sb12.append("/").append("l").append("i").append("b").append("/").append("u");
        m = sb12.toString();
        StringBuilder sb13 = new StringBuilder();
        sb13.append("c").append("o").append("m").append(".").append("u").append("l").append("k").append(".").append("a").append("c").append("t");
        l = sb13.toString();
    }

    public static SharedPreferences a(Context context) {
        if (n == null && context != null) {
            n = context.getSharedPreferences(e, 0);
        }
        return n;
    }

    protected static String a() {
        return l;
    }

    protected static void a(Context context, long j2) {
        a(context).edit().putLong(a, j2).commit();
    }

    protected static void a(Context context, Intent intent) {
        new i(intent, context).start();
    }

    protected static void a(Context context, String str) {
        a(context).edit().putString(b, str).commit();
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x0083 A[SYNTHETIC, Splitter:B:40:0x0083] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0088 A[SYNTHETIC, Splitter:B:43:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x008d A[SYNTHETIC, Splitter:B:46:0x008d] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x009c A[SYNTHETIC, Splitter:B:54:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00a1 A[SYNTHETIC, Splitter:B:57:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00a6 A[SYNTHETIC, Splitter:B:60:0x00a6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(android.content.Context r9, boolean r10) {
        /*
            r2 = 0
            if (r10 == 0) goto L_0x006c
            java.io.File r0 = com.iua.unla.b.b(r9)
            r0.delete()
        L_0x000a:
            java.io.File r5 = com.iua.unla.b.d(r9)
            java.io.File r0 = com.iua.unla.b.e(r9)
            java.lang.Class<com.iua.unla.h> r1 = com.iua.unla.h.class
            java.lang.String r3 = h()     // Catch:{ Exception -> 0x00ba, all -> 0x00ad }
            java.io.InputStream r1 = r1.getResourceAsStream(r3)     // Catch:{ Exception -> 0x00ba, all -> 0x00ad }
            r5.delete()     // Catch:{ Exception -> 0x00bf, all -> 0x00b1 }
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00bf, all -> 0x00b1 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x00bf, all -> 0x00b1 }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x00c5, all -> 0x00dd }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00c5, all -> 0x00dd }
            r6.<init>(r5)     // Catch:{ Exception -> 0x00c5, all -> 0x00dd }
            r3.<init>(r6)     // Catch:{ Exception -> 0x00c5, all -> 0x00dd }
            r2 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
        L_0x0032:
            int r6 = r4.read(r2)     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            r7 = -1
            if (r6 != r7) goto L_0x0073
            r3.flush()     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            java.lang.String r2 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            java.lang.String r6 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            java.lang.String r2 = com.iua.unla.c.a(r2, r6)     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            if (r2 == 0) goto L_0x0094
            java.lang.String r6 = ""
            boolean r2 = r6.equals(r2)     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            if (r2 != 0) goto L_0x0094
            java.io.File r2 = com.iua.unla.b.b(r9)     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            r0.renameTo(r2)     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
        L_0x0059:
            if (r3 == 0) goto L_0x005e
            r3.close()     // Catch:{ Exception -> 0x00d7 }
        L_0x005e:
            if (r4 == 0) goto L_0x0063
            r4.close()     // Catch:{ Exception -> 0x00d9 }
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ Exception -> 0x00db }
        L_0x0068:
            r5.delete()
        L_0x006b:
            return
        L_0x006c:
            boolean r0 = com.iua.unla.b.a(r9)
            if (r0 == 0) goto L_0x000a
            goto L_0x006b
        L_0x0073:
            r7 = 0
            r3.write(r2, r7, r6)     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            goto L_0x0032
        L_0x0078:
            r0 = move-exception
            r2 = r3
            r3 = r4
            r8 = r0
            r0 = r1
            r1 = r8
        L_0x007e:
            r1.printStackTrace()     // Catch:{ all -> 0x00b4 }
            if (r2 == 0) goto L_0x0086
            r2.close()     // Catch:{ Exception -> 0x00cb }
        L_0x0086:
            if (r3 == 0) goto L_0x008b
            r3.close()     // Catch:{ Exception -> 0x00cd }
        L_0x008b:
            if (r0 == 0) goto L_0x0090
            r0.close()     // Catch:{ Exception -> 0x00cf }
        L_0x0090:
            r5.delete()
            goto L_0x006b
        L_0x0094:
            r0.delete()     // Catch:{ Exception -> 0x0078, all -> 0x0098 }
            goto L_0x0059
        L_0x0098:
            r0 = move-exception
            r2 = r3
        L_0x009a:
            if (r2 == 0) goto L_0x009f
            r2.close()     // Catch:{ Exception -> 0x00d1 }
        L_0x009f:
            if (r4 == 0) goto L_0x00a4
            r4.close()     // Catch:{ Exception -> 0x00d3 }
        L_0x00a4:
            if (r1 == 0) goto L_0x00a9
            r1.close()     // Catch:{ Exception -> 0x00d5 }
        L_0x00a9:
            r5.delete()
            throw r0
        L_0x00ad:
            r0 = move-exception
            r4 = r2
            r1 = r2
            goto L_0x009a
        L_0x00b1:
            r0 = move-exception
            r4 = r2
            goto L_0x009a
        L_0x00b4:
            r1 = move-exception
            r4 = r3
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x009a
        L_0x00ba:
            r0 = move-exception
            r1 = r0
            r3 = r2
            r0 = r2
            goto L_0x007e
        L_0x00bf:
            r0 = move-exception
            r3 = r2
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x007e
        L_0x00c5:
            r0 = move-exception
            r3 = r4
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x007e
        L_0x00cb:
            r1 = move-exception
            goto L_0x0086
        L_0x00cd:
            r1 = move-exception
            goto L_0x008b
        L_0x00cf:
            r0 = move-exception
            goto L_0x0090
        L_0x00d1:
            r2 = move-exception
            goto L_0x009f
        L_0x00d3:
            r2 = move-exception
            goto L_0x00a4
        L_0x00d5:
            r1 = move-exception
            goto L_0x00a9
        L_0x00d7:
            r0 = move-exception
            goto L_0x005e
        L_0x00d9:
            r0 = move-exception
            goto L_0x0063
        L_0x00db:
            r0 = move-exception
            goto L_0x0068
        L_0x00dd:
            r0 = move-exception
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iua.unla.h.a(android.content.Context, boolean):void");
    }

    protected static void b(Context context) {
        new j(context).start();
    }

    protected static void b(Context context, long j2) {
        a(context).edit().putLong(d, j2).commit();
    }

    protected static void c(Context context) {
        a(context).edit().putInt(c, -1).commit();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.iua.unla.h.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.iua.unla.h.a(android.content.Context, long):void
      com.iua.unla.h.a(android.content.Context, android.content.Intent):void
      com.iua.unla.h.a(android.content.Context, java.lang.String):void
      com.iua.unla.h.a(android.content.Context, boolean):void */
    protected static void d(Context context) {
        a(context, false);
    }

    private static String h() {
        return m;
    }
}
