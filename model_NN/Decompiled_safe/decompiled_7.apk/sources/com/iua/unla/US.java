package com.iua.unla;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;

public class US extends Service {
    protected static final String a = US.class.getName();
    private g b;

    private g a() {
        if (this.b == null) {
            this.b = (g) b.a(this, g.class);
            this.b.a((Service) this);
        }
        return this.b;
    }

    public IBinder onBind(Intent intent) {
        try {
            return a().a(intent);
        } catch (Exception e) {
            return null;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            a().a(configuration);
        } catch (Exception e) {
        }
    }

    public void onCreate() {
        try {
            a().b();
        } catch (Exception e) {
        }
    }

    public void onDestroy() {
        try {
            a().d();
        } catch (Exception e) {
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        try {
            a().c();
        } catch (Exception e) {
        }
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        try {
            a().c(intent);
        } catch (Exception e) {
        }
    }

    public void onStart(Intent intent, int i) {
        try {
            a().a(intent, i);
        } catch (Exception e) {
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        int onStartCommand = super.onStartCommand(intent, i, i2);
        try {
            return a().a(intent, i, i2);
        } catch (Exception e) {
            return onStartCommand;
        }
    }

    public boolean onUnbind(Intent intent) {
        boolean onUnbind = super.onUnbind(intent);
        try {
            return a().b(intent);
        } catch (Exception e) {
            return onUnbind;
        }
    }
}
