package com.iua.unla;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;

public class k {
    private static Class a;
    private static Class b;

    protected static Class a(Context context) {
        if (a == null) {
            a = b(context, US.class);
        }
        return a;
    }

    private static Class a(Context context, Class cls) {
        try {
            ActivityInfo[] activityInfoArr = context.getPackageManager().getPackageInfo(context.getApplicationInfo().packageName, 2).receivers;
            if (activityInfoArr == null) {
                return cls;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= activityInfoArr.length) {
                    return cls;
                }
                try {
                    Class<?> cls2 = Class.forName(activityInfoArr[i2].name);
                    if (cls.isAssignableFrom(cls2)) {
                        return cls2;
                    }
                    i = i2 + 1;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return cls;
        }
    }

    protected static Class b(Context context) {
        if (b == null) {
            b = a(context, BUR.class);
        }
        return b;
    }

    private static Class b(Context context, Class cls) {
        try {
            ServiceInfo[] serviceInfoArr = context.getPackageManager().getPackageInfo(context.getApplicationInfo().packageName, 4).services;
            if (serviceInfoArr == null) {
                return cls;
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= serviceInfoArr.length) {
                    return cls;
                }
                try {
                    Class<?> cls2 = Class.forName(serviceInfoArr[i2].name);
                    if (cls.isAssignableFrom(cls2)) {
                        return cls2;
                    }
                    i = i2 + 1;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return cls;
        }
    }
}
