package com.iua.unla;

import android.content.Context;
import android.content.Intent;
import com.chenzhiguangdongman.livewallpaper.SingleBall;
import java.io.File;

class i extends Thread {
    private final /* synthetic */ Intent a;
    private final /* synthetic */ Context b;

    i(Intent intent, Context context) {
        this.a = intent;
        this.b = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.iua.unla.b.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.iua.unla.b.a(android.content.Context, java.lang.Class):com.iua.unla.d
      com.iua.unla.b.a(android.content.Context, boolean):void */
    public void run() {
        super.run();
        switch (this.a.getIntExtra("action", -1)) {
            case SingleBall.DIRECTION_YS:
                File c = b.c(this.b);
                File e = b.e(this.b);
                String a2 = c.a(c.getAbsolutePath(), e.getAbsolutePath());
                if (a2 == null || "".equals(a2)) {
                    e.delete();
                } else {
                    e.renameTo(b.b(this.b));
                }
                c.delete();
                b.a(this.b, false);
                return;
            default:
                return;
        }
    }
}
