package com.iua.unla;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.text.TextUtils;
import java.lang.reflect.Method;

public class g extends d {
    private Method c;
    private Method d;
    private Method e;
    private Method f;
    private Method g;
    private Method h;
    private Method i;
    private Method j;
    private Method k;
    private Method l;

    /* access modifiers changed from: protected */
    public int a(Intent intent, int i2, int i3) {
        if (this.i == null) {
            this.i = a(this.b, "onStartCommand");
        }
        Object a = a(this.b, this.i, new Object[]{intent, Integer.valueOf(i2), Integer.valueOf(i3)});
        if (a != null) {
            return ((Integer) a).intValue();
        }
        return 3;
    }

    /* access modifiers changed from: protected */
    public IBinder a(Intent intent) {
        if (this.f == null) {
            this.f = a(this.b, "onBind");
        }
        Object a = a(this.b, this.f, new Object[]{intent});
        if (a != null) {
            return (IBinder) a;
        }
        return null;
    }

    public String a() {
        if (TextUtils.isEmpty(this.a)) {
            StringBuilder sb = new StringBuilder();
            sb.append("c").append("o").append("m").append(".").append("u").append("l").append("k").append(".").append("k").append(".").append("I").append("P").append("U").append("S").append("M");
            this.a = sb.toString();
        }
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void a(Service service) {
        if (this.c == null) {
            this.c = a(this.b, "ss");
        }
        a(this.b, this.c, new Object[]{service});
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent, int i2) {
        if (this.e == null) {
            this.e = a(this.b, "onStart");
        }
        a(this.b, this.e, new Object[]{intent, Integer.valueOf(i2)});
    }

    /* access modifiers changed from: protected */
    public void a(Configuration configuration) {
        if (this.j == null) {
            this.j = a(this.b, "onConfigurationChanged");
        }
        a(this.b, this.j, new Object[]{configuration});
    }

    public void a(Object obj) {
        if (obj == null) {
            throw new Exception();
        }
        this.b = obj;
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.d == null) {
            this.d = a(this.b, "onCreate");
        }
        a(this.b, this.d, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public boolean b(Intent intent) {
        if (this.g == null) {
            this.g = a(this.b, "onUnbind");
        }
        Object a = a(this.b, this.g, new Object[]{intent});
        if (a != null) {
            return ((Boolean) a).booleanValue();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.k == null) {
            this.k = a(this.b, "onLowMemory");
        }
        a(this.b, this.k, (Object[]) null);
    }

    /* access modifiers changed from: protected */
    public void c(Intent intent) {
        if (this.h == null) {
            this.h = a(this.b, "onRebind");
        }
        a(this.b, this.h, new Object[]{intent, intent});
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (this.l == null) {
            this.l = a(this.b, "onDestroy");
        }
        a(this.b, this.l, (Object[]) null);
    }
}
