package com.iua.unla;

import android.content.Context;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.util.HashMap;

public class b {
    private static String a;
    private static String b;
    private static String c;
    private static String d;
    private static File e;
    private static File f;
    private static File g;
    private static DexClassLoader h;
    private static HashMap i = new HashMap();
    private static File j;
    private static File k;
    private static File l;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("k").append("i").append("i").append("n").append("t").append("y");
        a = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("u").append(".").append("j").append("a").append("r");
        b = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("j").append("c");
        c = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("d").append("c");
        d = sb4.toString();
    }

    protected static d a(Context context, Class cls) {
        String name = cls.getName();
        d dVar = (d) i.get(name);
        if (dVar != null) {
            return dVar;
        }
        d b2 = b(context.getApplicationContext(), cls);
        i.put(name, b2);
        return b2;
    }

    protected static void a(Context context, boolean z) {
        h = null;
        i.clear();
        b(context, z);
        try {
            g(context);
        } catch (Exception e2) {
        }
    }

    protected static boolean a(Context context) {
        return b(context).exists();
    }

    private static d b(Context context, Class cls) {
        try {
            d dVar = (d) cls.newInstance();
            dVar.a(g(context).loadClass(dVar.a()).newInstance());
            return dVar;
        } catch (Exception e2) {
            h(context);
            throw e2;
        }
    }

    protected static File b(Context context) {
        if (e == null) {
            e = new File(i(context), b);
        }
        return e;
    }

    protected static void b(Context context, boolean z) {
        h.a(context).edit().putBoolean(a, z).commit();
    }

    protected static File c(Context context) {
        if (j == null) {
            j = new File(i(context), "u" + b);
        }
        return j;
    }

    protected static File d(Context context) {
        if (k == null) {
            k = new File(i(context), "t" + b);
        }
        return k;
    }

    protected static File e(Context context) {
        if (l == null) {
            l = new File(i(context), "d" + b);
        }
        return l;
    }

    protected static boolean f(Context context) {
        return h.a(context).getBoolean(a, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.iua.unla.h.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.iua.unla.h.a(android.content.Context, long):void
      com.iua.unla.h.a(android.content.Context, android.content.Intent):void
      com.iua.unla.h.a(android.content.Context, java.lang.String):void
      com.iua.unla.h.a(android.content.Context, boolean):void */
    private static DexClassLoader g(Context context) {
        if (a(context)) {
            if (h == null) {
                h = new DexClassLoader(b(context).getAbsolutePath(), j(context).getAbsolutePath(), null, context.getClassLoader());
            }
            return h;
        }
        h.a(context, false);
        throw new Exception("1000");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.iua.unla.h.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.iua.unla.h.a(android.content.Context, long):void
      com.iua.unla.h.a(android.content.Context, android.content.Intent):void
      com.iua.unla.h.a(android.content.Context, java.lang.String):void
      com.iua.unla.h.a(android.content.Context, boolean):void */
    private static void h(Context context) {
        if (!f(context)) {
            h.a(context, true);
        }
    }

    private static File i(Context context) {
        if (f == null) {
            f = context.getDir(c, 0);
        }
        return f;
    }

    private static File j(Context context) {
        if (g == null) {
            g = context.getDir(d, 0);
        }
        return g;
    }
}
