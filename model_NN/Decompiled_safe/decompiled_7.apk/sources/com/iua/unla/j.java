package com.iua.unla;

import android.content.Context;
import android.content.SharedPreferences;

class j extends Thread {
    private final /* synthetic */ Context a;

    j(Context context) {
        this.a = context;
    }

    public void run() {
        SharedPreferences a2 = h.a(this.a);
        if (!a2.getBoolean(h.f, false)) {
            a2.edit().putBoolean(h.f, true).putString(h.g, f.a).putString(h.h, k.a(this.a).getName()).putString(h.i, k.b(this.a).getName()).putString(h.j, b.c(this.a).getAbsolutePath()).putString(h.k, h.a()).commit();
        }
    }
}
