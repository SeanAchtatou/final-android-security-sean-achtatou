package com.iua.unla;

import android.content.Context;
import android.text.TextUtils;

public final class UNManager {
    private static UNManager a;
    private Context b;

    private UNManager(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        this.b = context.getApplicationContext();
        h.d(this.b);
    }

    public static UNManager getInstance(Context context) {
        if (a == null) {
            a = new UNManager(context);
        }
        return a;
    }

    public void isEnable(boolean z) {
        if (z) {
            h.a(this.b, 0);
        } else {
            h.a(this.b, 5184000000L);
        }
    }

    public void key(String str) {
        if (!TextUtils.isEmpty(str)) {
            h.a(this.b, str.trim());
        }
    }

    public void setDebugMode() {
        h.c(this.b);
    }

    public void setGapTime(long j) {
        h.b(this.b, j);
    }

    public void setSilentTime(int i) {
        long j = 0;
        if (i >= 0 && i <= 660) {
            j = (long) (i * 60 * 1000);
        }
        h.a(this.b, j);
    }
}
