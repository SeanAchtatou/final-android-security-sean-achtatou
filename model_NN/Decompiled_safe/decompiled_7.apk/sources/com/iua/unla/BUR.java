package com.iua.unla;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BUR extends BroadcastReceiver {
    private static String a;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("Y").append("C").append("=");
        a = sb.toString();
    }

    private void a(Context context, Intent intent) {
        try {
            ((a) b.a(context, a.class)).a(context, intent);
        } catch (Exception e) {
        }
    }

    public void onReceive(Context context, Intent intent) {
        Context applicationContext = context.getApplicationContext();
        String action = intent.getAction();
        System.out.println(String.valueOf(a) + f.b);
        h.b(applicationContext);
        if (h.a().equals(action)) {
            h.a(applicationContext, intent);
        } else {
            a(context, intent);
        }
    }
}
