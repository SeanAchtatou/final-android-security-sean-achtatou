package org.anddev.andengine.extension.physics.box2d.util.hull;

import com.badlogic.gdx.math.Vector2;

public interface IHullAlgorithm {
    int computeHull(Vector2[] vector2Arr);
}
