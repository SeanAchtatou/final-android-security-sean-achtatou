package org.anddev.andengine.extension.multiplayer.protocol.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import org.anddev.andengine.extension.multiplayer.protocol.server.SocketServerDiscoveryServer;
import org.anddev.andengine.extension.multiplayer.protocol.shared.IDiscoveryData;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.pool.GenericPool;

public class SocketServerDiscoveryClient<T extends IDiscoveryData> {
    public static final int LOCALPORT_DEFAULT = 9998;
    protected static final int TIMEOUT_DEFAULT = 5000;
    private final InetAddress mDiscoveryBroadcastInetAddress;
    private final GenericPool<T> mDiscoveryDataPool;
    private final int mDiscoveryPort;
    private final DatagramPacket mDiscoveryRequestDatagramPacket;
    private final byte[] mDiscoveryResponseData;
    private final DatagramPacket mDiscoveryResponseDatagramPacket;
    private final ExecutorService mExecutorService;
    private final int mLocalPort;
    private final ISocketServerDiscoveryClientListener<T> mSocketServerDiscoveryClientListener;
    protected AtomicBoolean mTerminated;
    private int mTimeout;

    public interface ISocketServerDiscoveryClientListener<T extends IDiscoveryData> {
        void onDiscovery(SocketServerDiscoveryClient<T> socketServerDiscoveryClient, T t);

        void onException(SocketServerDiscoveryClient<T> socketServerDiscoveryClient, Throwable th);

        void onTimeout(SocketServerDiscoveryClient<T> socketServerDiscoveryClient, SocketTimeoutException socketTimeoutException);
    }

    public SocketServerDiscoveryClient(byte[] pDiscoveryBroadcastIPAddress, Class<? extends T> pDiscoveryDataClass, ISocketServerDiscoveryClientListener<T> pSocketServerDiscoveryClientListener) throws UnknownHostException {
        this(pDiscoveryBroadcastIPAddress, SocketServerDiscoveryServer.DISCOVERYPORT_DEFAULT, LOCALPORT_DEFAULT, pDiscoveryDataClass, pSocketServerDiscoveryClientListener);
    }

    public SocketServerDiscoveryClient(byte[] pDiscoveryBroadcastIPAddress, int pDiscoveryPort, Class<? extends T> pDiscoveryDataClass, ISocketServerDiscoveryClientListener<T> pSocketServerDiscoveryClientListener) throws UnknownHostException {
        this(pDiscoveryBroadcastIPAddress, pDiscoveryPort, LOCALPORT_DEFAULT, pDiscoveryDataClass, pSocketServerDiscoveryClientListener);
    }

    public SocketServerDiscoveryClient(byte[] pDiscoveryBroadcastIPAddress, int pDiscoveryPort, int pLocalPort, final Class<? extends T> pDiscoveryDataClass, ISocketServerDiscoveryClientListener<T> pSocketServerDiscoveryClientListener) throws UnknownHostException {
        this.mTerminated = new AtomicBoolean(false);
        this.mTimeout = TIMEOUT_DEFAULT;
        this.mExecutorService = Executors.newSingleThreadExecutor();
        this.mDiscoveryResponseData = new byte[PVRTexture.FLAG_BUMPMAP];
        this.mDiscoveryResponseDatagramPacket = new DatagramPacket(this.mDiscoveryResponseData, this.mDiscoveryResponseData.length);
        this.mDiscoveryPort = pDiscoveryPort;
        this.mLocalPort = pLocalPort;
        this.mSocketServerDiscoveryClientListener = pSocketServerDiscoveryClientListener;
        this.mDiscoveryBroadcastInetAddress = InetAddress.getByAddress(pDiscoveryBroadcastIPAddress);
        byte[] out = SocketServerDiscoveryServer.MAGIC_IDENTIFIER;
        this.mDiscoveryRequestDatagramPacket = new DatagramPacket(out, out.length, this.mDiscoveryBroadcastInetAddress, this.mDiscoveryPort);
        this.mDiscoveryDataPool = new GenericPool<T>() {
            /* access modifiers changed from: protected */
            public T onAllocatePoolItem() {
                try {
                    return (IDiscoveryData) pDiscoveryDataClass.newInstance();
                } catch (Throwable th) {
                    Debug.e(th);
                    return null;
                }
            }
        };
    }

    public int getDiscoveryPort() {
        return this.mDiscoveryPort;
    }

    public int getLocalPort() {
        return this.mLocalPort;
    }

    public int getTimeout() {
        return this.mTimeout;
    }

    public void setTimeout(int pTimeout) {
        this.mTimeout = pTimeout;
    }

    public InetAddress getDiscoveryBroadcastInetAddress() {
        return this.mDiscoveryBroadcastInetAddress;
    }

    public void discoverAsync() throws IllegalStateException {
        if (this.mTerminated.get()) {
            throw new IllegalStateException(String.valueOf(getClass().getSimpleName()) + " was already terminated.");
        }
        this.mExecutorService.execute(new Runnable() {
            public void run() {
                SocketServerDiscoveryClient.this.discover();
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x0028=Splitter:B:14:0x0028, B:9:0x001d=Splitter:B:9:0x001d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void discover() {
        /*
            r5 = this;
            r0 = 0
            java.net.DatagramSocket r1 = new java.net.DatagramSocket     // Catch:{ SocketTimeoutException -> 0x001b, Throwable -> 0x0026 }
            int r4 = r5.mLocalPort     // Catch:{ SocketTimeoutException -> 0x001b, Throwable -> 0x0026 }
            r1.<init>(r4)     // Catch:{ SocketTimeoutException -> 0x001b, Throwable -> 0x0026 }
            r4 = 1
            r1.setBroadcast(r4)     // Catch:{ SocketTimeoutException -> 0x003d, Throwable -> 0x0039, all -> 0x0036 }
            r5.sendDiscoveryRequest(r1)     // Catch:{ SocketTimeoutException -> 0x003d, Throwable -> 0x0039, all -> 0x0036 }
            byte[] r2 = r5.receiveDiscoveryResponseData(r1)     // Catch:{ SocketTimeoutException -> 0x003d, Throwable -> 0x0039, all -> 0x0036 }
            r5.handleDiscoveryResponseData(r2)     // Catch:{ SocketTimeoutException -> 0x003d, Throwable -> 0x0039, all -> 0x0036 }
            org.anddev.andengine.util.SocketUtils.closeSocket(r1)
            r0 = r1
        L_0x001a:
            return
        L_0x001b:
            r4 = move-exception
            r3 = r4
        L_0x001d:
            org.anddev.andengine.extension.multiplayer.protocol.client.SocketServerDiscoveryClient$ISocketServerDiscoveryClientListener<T> r4 = r5.mSocketServerDiscoveryClientListener     // Catch:{ all -> 0x0031 }
            r4.onTimeout(r5, r3)     // Catch:{ all -> 0x0031 }
            org.anddev.andengine.util.SocketUtils.closeSocket(r0)
            goto L_0x001a
        L_0x0026:
            r4 = move-exception
            r3 = r4
        L_0x0028:
            org.anddev.andengine.extension.multiplayer.protocol.client.SocketServerDiscoveryClient$ISocketServerDiscoveryClientListener<T> r4 = r5.mSocketServerDiscoveryClientListener     // Catch:{ all -> 0x0031 }
            r4.onException(r5, r3)     // Catch:{ all -> 0x0031 }
            org.anddev.andengine.util.SocketUtils.closeSocket(r0)
            goto L_0x001a
        L_0x0031:
            r4 = move-exception
        L_0x0032:
            org.anddev.andengine.util.SocketUtils.closeSocket(r0)
            throw r4
        L_0x0036:
            r4 = move-exception
            r0 = r1
            goto L_0x0032
        L_0x0039:
            r4 = move-exception
            r3 = r4
            r0 = r1
            goto L_0x0028
        L_0x003d:
            r4 = move-exception
            r3 = r4
            r0 = r1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.extension.multiplayer.protocol.client.SocketServerDiscoveryClient.discover():void");
    }

    private void sendDiscoveryRequest(DatagramSocket datagramSocket) throws IOException {
        datagramSocket.send(this.mDiscoveryRequestDatagramPacket);
    }

    /* access modifiers changed from: protected */
    public byte[] receiveDiscoveryResponseData(DatagramSocket datagramSocket) throws SocketException, IOException {
        datagramSocket.setSoTimeout(this.mTimeout);
        datagramSocket.receive(this.mDiscoveryResponseDatagramPacket);
        byte[] discoveryResponseData = new byte[this.mDiscoveryResponseDatagramPacket.getLength()];
        System.arraycopy(this.mDiscoveryResponseDatagramPacket.getData(), this.mDiscoveryResponseDatagramPacket.getOffset(), discoveryResponseData, 0, this.mDiscoveryResponseDatagramPacket.getLength());
        return discoveryResponseData;
    }

    private void handleDiscoveryResponseData(byte[] pDiscoveryResponseData) {
        T discoveryResponse = (IDiscoveryData) this.mDiscoveryDataPool.obtainPoolItem();
        try {
            IDiscoveryData.DiscoveryDataFactory.read(pDiscoveryResponseData, discoveryResponse);
            this.mSocketServerDiscoveryClientListener.onDiscovery(this, discoveryResponse);
        } catch (Throwable th) {
            this.mSocketServerDiscoveryClientListener.onException(this, th);
        }
        this.mDiscoveryDataPool.recyclePoolItem(discoveryResponse);
    }

    public void terminate() {
        if (!this.mTerminated.getAndSet(true)) {
            onTerminate();
        }
    }

    private void onTerminate() {
        this.mExecutorService.shutdownNow();
    }
}
