package org.anddev.andengine.extension.multiplayer.protocol.adt.message;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class EmptyMessage extends Message {
    public EmptyMessage() {
    }

    public EmptyMessage(DataInputStream pDataInputStream) throws IOException {
    }

    /* access modifiers changed from: protected */
    public void onWriteTransmissionData(DataOutputStream pDataOutputStream) throws IOException {
    }

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
    }
}
