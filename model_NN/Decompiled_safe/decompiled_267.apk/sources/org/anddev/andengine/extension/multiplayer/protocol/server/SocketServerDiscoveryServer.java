package org.anddev.andengine.extension.multiplayer.protocol.server;

import android.os.Process;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.anddev.andengine.extension.multiplayer.protocol.shared.IDiscoveryData;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.util.ArrayUtils;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.SocketUtils;

public abstract class SocketServerDiscoveryServer<T extends IDiscoveryData> extends Thread {
    public static final int DISCOVERYPORT_DEFAULT = 9999;
    public static final byte[] MAGIC_IDENTIFIER = {65, 110, 100, 69, 110, 103, 105, 110, 101, 45, 83, 111, 99, 107, 101, 116, 83, 101, 114, 118, 101, 114, 68, 105, 115, 99, 111, 118, 101, 114, 121};
    private DatagramSocket mDatagramSocket;
    private final int mDiscoveryPort;
    private final byte[] mDiscoveryRequestData;
    private final DatagramPacket mDiscoveryRequestDatagramPacket;
    protected AtomicBoolean mRunning;
    protected ISocketServerDiscoveryServerListener<T> mSocketServerDiscoveryServerListener;
    protected AtomicBoolean mTerminated;

    /* access modifiers changed from: protected */
    public abstract T onCreateDiscoveryResponse();

    public SocketServerDiscoveryServer(int pDiscoveryPort) {
        this(pDiscoveryPort, new ISocketServerDiscoveryServerListener.DefaultSocketServerDiscoveryServerListener());
    }

    public SocketServerDiscoveryServer(int pDiscoveryPort, ISocketServerDiscoveryServerListener<T> pSocketServerDiscoveryServerListener) {
        this.mDiscoveryRequestData = new byte[PVRTexture.FLAG_BUMPMAP];
        this.mDiscoveryRequestDatagramPacket = new DatagramPacket(this.mDiscoveryRequestData, this.mDiscoveryRequestData.length);
        this.mRunning = new AtomicBoolean(false);
        this.mTerminated = new AtomicBoolean(false);
        this.mDiscoveryPort = pDiscoveryPort;
        this.mSocketServerDiscoveryServerListener = pSocketServerDiscoveryServerListener;
        initName();
    }

    private void initName() {
        setName(getClass().getName());
    }

    public boolean isRunning() {
        return this.mRunning.get();
    }

    public boolean isTerminated() {
        return this.mTerminated.get();
    }

    public int getDiscoveryPort() {
        return this.mDiscoveryPort;
    }

    public boolean hasSocketServerDiscoveryServerListener() {
        return this.mSocketServerDiscoveryServerListener != null;
    }

    public ISocketServerDiscoveryServerListener<T> getSocketServerDiscoveryServerListener() {
        return this.mSocketServerDiscoveryServerListener;
    }

    public void run() {
        try {
            onStart();
            this.mRunning.set(true);
            Process.setThreadPriority(1);
            while (!Thread.interrupted() && this.mRunning.get() && !this.mTerminated.get()) {
                this.mDatagramSocket.receive(this.mDiscoveryRequestDatagramPacket);
                if (verifyDiscoveryRequest(this.mDiscoveryRequestDatagramPacket)) {
                    onDiscovered(this.mDiscoveryRequestDatagramPacket);
                    sendDiscoveryResponse(this.mDiscoveryRequestDatagramPacket);
                }
            }
            terminate();
        } catch (Throwable th) {
            try {
                onException(th);
            } finally {
                terminate();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        terminate();
        super.finalize();
    }

    /* access modifiers changed from: protected */
    public boolean verifyDiscoveryRequest(DatagramPacket pDiscoveryRequest) {
        return ArrayUtils.equals(MAGIC_IDENTIFIER, 0, pDiscoveryRequest.getData(), pDiscoveryRequest.getOffset(), MAGIC_IDENTIFIER.length);
    }

    /* access modifiers changed from: protected */
    public void onDiscovered(DatagramPacket pDiscoveryRequest) throws IOException {
        this.mSocketServerDiscoveryServerListener.onDiscovered(this, pDiscoveryRequest.getAddress(), pDiscoveryRequest.getPort());
    }

    /* access modifiers changed from: protected */
    public void sendDiscoveryResponse(DatagramPacket pDatagramPacket) throws IOException {
        byte[] discoveryResponseData = IDiscoveryData.DiscoveryDataFactory.write(onCreateDiscoveryResponse());
        this.mDatagramSocket.send(new DatagramPacket(discoveryResponseData, discoveryResponseData.length, pDatagramPacket.getAddress(), pDatagramPacket.getPort()));
    }

    /* access modifiers changed from: protected */
    public void onStart() throws SocketException {
        this.mDatagramSocket = new DatagramSocket(this.mDiscoveryPort);
        this.mSocketServerDiscoveryServerListener.onStarted(this);
    }

    /* access modifiers changed from: protected */
    public void onTerminate() {
        SocketUtils.closeSocket(this.mDatagramSocket);
        this.mSocketServerDiscoveryServerListener.onTerminated(this);
    }

    /* access modifiers changed from: protected */
    public void onException(Throwable pThrowable) {
        this.mSocketServerDiscoveryServerListener.onException(this, pThrowable);
    }

    public void terminate() {
        if (!this.mTerminated.getAndSet(true)) {
            this.mRunning.set(false);
            interrupt();
            onTerminate();
        }
    }

    public interface ISocketServerDiscoveryServerListener<T extends IDiscoveryData> {
        void onDiscovered(SocketServerDiscoveryServer<T> socketServerDiscoveryServer, InetAddress inetAddress, int i);

        void onException(SocketServerDiscoveryServer<T> socketServerDiscoveryServer, Throwable th);

        void onStarted(SocketServerDiscoveryServer<T> socketServerDiscoveryServer);

        void onTerminated(SocketServerDiscoveryServer<T> socketServerDiscoveryServer);

        public static class DefaultSocketServerDiscoveryServerListener<T extends IDiscoveryData> implements ISocketServerDiscoveryServerListener<T> {
            public void onStarted(SocketServerDiscoveryServer<T> pSocketServerDiscoveryServer) {
                Debug.d("SocketServerDiscoveryServer started on discoveryPort: " + pSocketServerDiscoveryServer.getDiscoveryPort());
            }

            public void onTerminated(SocketServerDiscoveryServer<T> pSocketServerDiscoveryServer) {
                Debug.d("SocketServerDiscoveryServer terminated on discoveryPort: " + pSocketServerDiscoveryServer.getDiscoveryPort());
            }

            public void onDiscovered(SocketServerDiscoveryServer<T> socketServerDiscoveryServer, InetAddress pInetAddress, int pPort) {
                Debug.d("SocketServerDiscoveryServer discovered by: " + pInetAddress.getHostAddress() + ":" + pPort);
            }

            public void onException(SocketServerDiscoveryServer<T> socketServerDiscoveryServer, Throwable pThrowable) {
                Debug.e(pThrowable);
            }
        }
    }
}
