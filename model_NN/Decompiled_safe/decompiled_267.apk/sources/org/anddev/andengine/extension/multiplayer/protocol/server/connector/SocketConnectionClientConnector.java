package org.anddev.andengine.extension.multiplayer.protocol.server.connector;

import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.server.IClientMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.server.connector.ClientConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.SocketConnection;
import org.anddev.andengine.util.Debug;

public class SocketConnectionClientConnector extends ClientConnector<SocketConnection> {

    public interface ISocketConnectionClientConnectorListener extends ClientConnector.IClientConnectorListener<SocketConnection> {
    }

    public SocketConnectionClientConnector(SocketConnection pSocketConnection) throws IOException {
        super(pSocketConnection);
    }

    public SocketConnectionClientConnector(SocketConnection pSocketConnection, IClientMessageReader<SocketConnection> pClientMessageReader) throws IOException {
        super(pSocketConnection, pClientMessageReader);
    }

    public static class DefaultSocketConnectionClientConnectorListener implements ISocketConnectionClientConnectorListener {
        public /* bridge */ /* synthetic */ void onStarted(Connector connector) {
            onStarted((ClientConnector<SocketConnection>) ((ClientConnector) connector));
        }

        public /* bridge */ /* synthetic */ void onTerminated(Connector connector) {
            onTerminated((ClientConnector<SocketConnection>) ((ClientConnector) connector));
        }

        public void onStarted(ClientConnector<SocketConnection> pClientConnector) {
            Debug.d("Accepted Client-Connection from: '" + pClientConnector.getConnection().getSocket().getInetAddress().getHostAddress());
        }

        public void onTerminated(ClientConnector<SocketConnection> pClientConnector) {
            Debug.d("Closed Client-Connection from: '" + pClientConnector.getConnection().getSocket().getInetAddress().getHostAddress());
        }
    }
}
