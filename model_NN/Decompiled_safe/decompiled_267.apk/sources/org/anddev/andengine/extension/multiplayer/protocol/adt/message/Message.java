package org.anddev.andengine.extension.multiplayer.protocol.adt.message;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class Message implements IMessage {
    /* access modifiers changed from: protected */
    public abstract void onReadTransmissionData(DataInputStream dataInputStream) throws IOException;

    /* access modifiers changed from: protected */
    public abstract void onWriteTransmissionData(DataOutputStream dataOutputStream) throws IOException;

    /* access modifiers changed from: protected */
    public void onAppendTransmissionDataForToString(StringBuilder pStringBuilder) {
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName()).append("[getFlag()=").append((int) getFlag());
        onAppendTransmissionDataForToString(sb);
        sb.append("]");
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return getFlag() == ((Message) obj).getFlag();
    }

    public void write(DataOutputStream pDataOutputStream) throws IOException {
        pDataOutputStream.writeShort(getFlag());
        onWriteTransmissionData(pDataOutputStream);
    }

    public void read(DataInputStream pDataInputStream) throws IOException {
        onReadTransmissionData(pDataInputStream);
    }
}
