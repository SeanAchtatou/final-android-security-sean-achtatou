package org.anddev.andengine.extension.svg.adt;

import org.anddev.andengine.extension.svg.util.constants.ISVGConstants;

public class SVGGroup implements ISVGConstants {
    private final boolean mHasTransform;
    private final boolean mHidden;
    private final SVGProperties mSVGProperties;
    private final SVGGroup mSVGroupParent;

    public SVGGroup(SVGGroup pSVGroupParent, SVGProperties pSVGProperties, boolean pHasTransform) {
        this.mSVGroupParent = pSVGroupParent;
        this.mSVGProperties = pSVGProperties;
        this.mHasTransform = pHasTransform;
        this.mHidden = (this.mSVGroupParent != null && this.mSVGroupParent.isHidden()) || isDisplayNone();
    }

    public boolean hasTransform() {
        return this.mHasTransform;
    }

    public SVGProperties getSVGProperties() {
        return this.mSVGProperties;
    }

    public boolean isHidden() {
        return this.mHidden;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.extension.svg.adt.SVGProperties.getStringProperty(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      org.anddev.andengine.extension.svg.adt.SVGProperties.getStringProperty(java.lang.String, java.lang.String):java.lang.String
      org.anddev.andengine.extension.svg.adt.SVGProperties.getStringProperty(java.lang.String, boolean):java.lang.String */
    private boolean isDisplayNone() {
        return ISVGConstants.VALUE_NONE.equals(this.mSVGProperties.getStringProperty(ISVGConstants.ATTRIBUTE_DISPLAY, false));
    }
}
