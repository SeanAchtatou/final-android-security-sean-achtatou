package org.anddev.andengine.extension.multiplayer.protocol.client.connector;

import java.io.IOException;
import org.anddev.andengine.extension.multiplayer.protocol.client.IServerMessageReader;
import org.anddev.andengine.extension.multiplayer.protocol.client.connector.ServerConnector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.Connector;
import org.anddev.andengine.extension.multiplayer.protocol.shared.SocketConnection;
import org.anddev.andengine.util.Debug;

public class SocketConnectionServerConnector extends ServerConnector<SocketConnection> {

    public interface ISocketConnectionServerConnectorListener extends ServerConnector.IServerConnectorListener<SocketConnection> {
    }

    public SocketConnectionServerConnector(SocketConnection pConnection, ISocketConnectionServerConnectorListener pSocketConnectionServerConnectorListener) throws IOException {
        super(pConnection, pSocketConnectionServerConnectorListener);
    }

    public SocketConnectionServerConnector(SocketConnection pConnection, IServerMessageReader<SocketConnection> pServerMessageReader, ISocketConnectionServerConnectorListener pSocketConnectionServerConnectorListener) throws IOException {
        super(pConnection, pServerMessageReader, pSocketConnectionServerConnectorListener);
    }

    public static class DefaultSocketConnectionServerConnectorListener implements ISocketConnectionServerConnectorListener {
        public /* bridge */ /* synthetic */ void onStarted(Connector connector) {
            onStarted((ServerConnector<SocketConnection>) ((ServerConnector) connector));
        }

        public /* bridge */ /* synthetic */ void onTerminated(Connector connector) {
            onTerminated((ServerConnector<SocketConnection>) ((ServerConnector) connector));
        }

        public void onStarted(ServerConnector<SocketConnection> pServerConnector) {
            Debug.d("Accepted Server-Connection from: '" + pServerConnector.getConnection().getSocket().getInetAddress().getHostAddress());
        }

        public void onTerminated(ServerConnector<SocketConnection> pServerConnector) {
            Debug.d("Closed Server-Connection from: '" + pServerConnector.getConnection().getSocket().getInetAddress().getHostAddress());
        }
    }
}
