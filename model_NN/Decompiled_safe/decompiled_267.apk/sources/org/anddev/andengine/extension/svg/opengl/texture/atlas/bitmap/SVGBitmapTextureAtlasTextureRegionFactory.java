package org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap;

import android.content.Context;
import org.anddev.andengine.extension.svg.adt.ISVGColorMapper;
import org.anddev.andengine.extension.svg.adt.SVG;
import org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.source.SVGAssetBitmapTextureAtlasSource;
import org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.source.SVGBaseBitmapTextureAtlasSource;
import org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.source.SVGResourceBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.ITextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.buildable.BuildableTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.opengl.texture.source.ITextureAtlasSource;

public class SVGBitmapTextureAtlasTextureRegionFactory {
    private static String sAssetBasePath = "";
    private static boolean sCreateTextureRegionBuffersManaged;
    private static float sScaleFactor = 1.0f;

    public static void setAssetBasePath(String pAssetBasePath) {
        if (pAssetBasePath.endsWith("/") || pAssetBasePath.length() == 0) {
            sAssetBasePath = pAssetBasePath;
            return;
        }
        throw new IllegalArgumentException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    public static void setScaleFactor(float pScaleFactor) {
        if (pScaleFactor > 0.0f) {
            sScaleFactor = pScaleFactor;
            return;
        }
        throw new IllegalArgumentException("pScaleFactor must be greater than zero.");
    }

    public static void setCreateTextureRegionBuffersManaged(boolean pCreateTextureRegionBuffersManaged) {
        sCreateTextureRegionBuffersManaged = pCreateTextureRegionBuffersManaged;
    }

    public static void reset() {
        setAssetBasePath("");
        setCreateTextureRegionBuffersManaged(false);
    }

    private static int applyScaleFactor(int pInt) {
        return Math.round(((float) pInt) * sScaleFactor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion}
     arg types: [org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas, org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource, int, int, boolean]
     candidates:
      org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.atlas.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion
      SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion} */
    public static TextureRegion createFromSVG(BitmapTextureAtlas pBitmapTextureAtlas, SVG pSVG, int pWidth, int pHeight, int pTexturePositionX, int pTexturePositionY) {
        return TextureRegionFactory.createFromSource((ITextureAtlas) pBitmapTextureAtlas, (ITextureAtlasSource) new SVGBaseBitmapTextureAtlasSource(pSVG, applyScaleFactor(pWidth), applyScaleFactor(pHeight)), pTexturePositionX, pTexturePositionY, sCreateTextureRegionBuffersManaged);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion}
     arg types: [org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas, org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource, int, int, int, int, boolean]
     candidates:
      org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.atlas.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion
      SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion} */
    public static TiledTextureRegion createTiledFromSVG(BitmapTextureAtlas pBitmapTextureAtlas, SVG pSVG, int pWidth, int pHeight, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows) {
        return TextureRegionFactory.createTiledFromSource((ITextureAtlas) pBitmapTextureAtlas, (ITextureAtlasSource) new SVGBaseBitmapTextureAtlasSource(pSVG, applyScaleFactor(pWidth), applyScaleFactor(pHeight)), pTexturePositionX, pTexturePositionY, pTileColumns, pTileRows, sCreateTextureRegionBuffersManaged);
    }

    public static TextureRegion createFromAsset(BitmapTextureAtlas pBitmapTextureAtlas, Context pContext, String pAssetPath, int pWidth, int pHeight, int pTexturePositionX, int pTexturePositionY) {
        return createFromAsset(pBitmapTextureAtlas, pContext, pAssetPath, pWidth, pHeight, null, pTexturePositionX, pTexturePositionY);
    }

    public static TiledTextureRegion createTiledFromAsset(BitmapTextureAtlas pBitmapTextureAtlas, Context pContext, String pAssetPath, int pWidth, int pHeight, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows) {
        return createTiledFromAsset(pBitmapTextureAtlas, pContext, pAssetPath, pWidth, pHeight, null, pTexturePositionX, pTexturePositionY, pTileColumns, pTileRows);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion}
     arg types: [org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas, org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource, int, int, boolean]
     candidates:
      org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.atlas.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion
      SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion} */
    public static TextureRegion createFromAsset(BitmapTextureAtlas pBitmapTextureAtlas, Context pContext, String pAssetPath, int pWidth, int pHeight, ISVGColorMapper pSVGColorMapper, int pTexturePositionX, int pTexturePositionY) {
        return TextureRegionFactory.createFromSource((ITextureAtlas) pBitmapTextureAtlas, (ITextureAtlasSource) new SVGAssetBitmapTextureAtlasSource(pContext, String.valueOf(sAssetBasePath) + pAssetPath, applyScaleFactor(pWidth), applyScaleFactor(pHeight), pSVGColorMapper), pTexturePositionX, pTexturePositionY, sCreateTextureRegionBuffersManaged);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion}
     arg types: [org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas, org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.source.SVGAssetBitmapTextureAtlasSource, int, int, int, int, boolean]
     candidates:
      org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.atlas.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion
      SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion} */
    public static TiledTextureRegion createTiledFromAsset(BitmapTextureAtlas pBitmapTextureAtlas, Context pContext, String pAssetPath, int pWidth, int pHeight, ISVGColorMapper pSVGColorMapper, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows) {
        SVGAssetBitmapTextureAtlasSource sVGAssetBitmapTextureAtlasSource = new SVGAssetBitmapTextureAtlasSource(pContext, String.valueOf(sAssetBasePath) + pAssetPath, applyScaleFactor(pWidth), applyScaleFactor(pHeight), pSVGColorMapper);
        return TextureRegionFactory.createTiledFromSource((ITextureAtlas) pBitmapTextureAtlas, (ITextureAtlasSource) sVGAssetBitmapTextureAtlasSource, pTexturePositionX, pTexturePositionY, pTileColumns, pTileRows, sCreateTextureRegionBuffersManaged);
    }

    public static TextureRegion createFromResource(BitmapTextureAtlas pBitmapTextureAtlas, Context pContext, int pRawResourceID, int pWidth, int pHeight, int pTexturePositionX, int pTexturePositionY) {
        return createFromResource(pBitmapTextureAtlas, pContext, pRawResourceID, pWidth, pHeight, null, pTexturePositionX, pTexturePositionY);
    }

    public static TiledTextureRegion createTiledFromResource(BitmapTextureAtlas pBitmapTextureAtlas, Context pContext, int pRawResourceID, int pWidth, int pHeight, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows) {
        return createTiledFromResource(pBitmapTextureAtlas, pContext, pRawResourceID, pWidth, pHeight, null, pTexturePositionX, pTexturePositionY, pTileColumns, pTileRows);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion}
     arg types: [org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas, org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource, int, int, boolean]
     candidates:
      org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.atlas.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion
      SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, boolean):org.anddev.andengine.opengl.texture.region.TextureRegion} */
    public static TextureRegion createFromResource(BitmapTextureAtlas pBitmapTextureAtlas, Context pContext, int pRawResourceID, int pWidth, int pHeight, ISVGColorMapper pSVGColorMapper, int pTexturePositionX, int pTexturePositionY) {
        return TextureRegionFactory.createFromSource((ITextureAtlas) pBitmapTextureAtlas, (ITextureAtlasSource) new SVGResourceBitmapTextureAtlasSource(pContext, applyScaleFactor(pHeight), pRawResourceID, applyScaleFactor(pWidth), pSVGColorMapper), pTexturePositionX, pTexturePositionY, sCreateTextureRegionBuffersManaged);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion}
     arg types: [org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas, org.anddev.andengine.extension.svg.opengl.texture.atlas.bitmap.source.SVGResourceBitmapTextureAtlasSource, int, int, int, int, boolean]
     candidates:
      org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.atlas.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion
      SimpleMethodDetails{org.anddev.andengine.opengl.texture.region.TextureRegionFactory.createTiledFromSource(org.anddev.andengine.opengl.texture.ITextureAtlas, org.anddev.andengine.opengl.texture.source.ITextureAtlasSource, int, int, int, int, boolean):org.anddev.andengine.opengl.texture.region.TiledTextureRegion} */
    public static TiledTextureRegion createTiledFromResource(BitmapTextureAtlas pBitmapTextureAtlas, Context pContext, int pRawResourceID, int pWidth, int pHeight, ISVGColorMapper pSVGColorMapper, int pTexturePositionX, int pTexturePositionY, int pTileColumns, int pTileRows) {
        SVGResourceBitmapTextureAtlasSource sVGResourceBitmapTextureAtlasSource = new SVGResourceBitmapTextureAtlasSource(pContext, applyScaleFactor(pHeight), pRawResourceID, applyScaleFactor(pWidth), pSVGColorMapper);
        return TextureRegionFactory.createTiledFromSource((ITextureAtlas) pBitmapTextureAtlas, (ITextureAtlasSource) sVGResourceBitmapTextureAtlasSource, pTexturePositionX, pTexturePositionY, pTileColumns, pTileRows, sCreateTextureRegionBuffersManaged);
    }

    public static TextureRegion createFromSVG(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, SVG pSVG, int pWidth, int pHeight) {
        return BuildableTextureAtlasTextureRegionFactory.createFromSource(pBuildableBitmapTextureAtlas, new SVGBaseBitmapTextureAtlasSource(pSVG, applyScaleFactor(pWidth), applyScaleFactor(pHeight)), sCreateTextureRegionBuffersManaged);
    }

    public static TiledTextureRegion createTiledFromSVG(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, SVG pSVG, int pWidth, int pHeight, int pTileColumns, int pTileRows) {
        return BuildableTextureAtlasTextureRegionFactory.createTiledFromSource(pBuildableBitmapTextureAtlas, new SVGBaseBitmapTextureAtlasSource(pSVG, applyScaleFactor(pWidth), applyScaleFactor(pHeight)), pTileColumns, pTileRows, sCreateTextureRegionBuffersManaged);
    }

    public static TextureRegion createFromAsset(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, Context pContext, String pAssetPath, int pWidth, int pHeight) {
        return createFromAsset(pBuildableBitmapTextureAtlas, pContext, pAssetPath, pWidth, pHeight, null);
    }

    public static TiledTextureRegion createTiledFromAsset(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, Context pContext, String pAssetPath, int pWidth, int pHeight, int pTileColumns, int pTileRows) {
        return createTiledFromAsset(pBuildableBitmapTextureAtlas, pContext, pAssetPath, pWidth, pHeight, null, pTileColumns, pTileRows);
    }

    public static TextureRegion createFromAsset(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, Context pContext, String pAssetPath, int pWidth, int pHeight, ISVGColorMapper pSVGColorMapper) {
        return BuildableTextureAtlasTextureRegionFactory.createFromSource(pBuildableBitmapTextureAtlas, new SVGAssetBitmapTextureAtlasSource(pContext, String.valueOf(sAssetBasePath) + pAssetPath, applyScaleFactor(pWidth), applyScaleFactor(pHeight), pSVGColorMapper), sCreateTextureRegionBuffersManaged);
    }

    public static TiledTextureRegion createTiledFromAsset(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, Context pContext, String pAssetPath, int pWidth, int pHeight, ISVGColorMapper pSVGColorMapper, int pTileColumns, int pTileRows) {
        return BuildableTextureAtlasTextureRegionFactory.createTiledFromSource(pBuildableBitmapTextureAtlas, new SVGAssetBitmapTextureAtlasSource(pContext, String.valueOf(sAssetBasePath) + pAssetPath, applyScaleFactor(pWidth), applyScaleFactor(pHeight), pSVGColorMapper), pTileColumns, pTileRows, sCreateTextureRegionBuffersManaged);
    }

    public static TextureRegion createFromResource(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, Context pContext, int pRawResourceID, int pWidth, int pHeight) {
        return createFromResource(pBuildableBitmapTextureAtlas, pContext, pRawResourceID, pWidth, pHeight, null);
    }

    public static TiledTextureRegion createTiledFromResource(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, Context pContext, int pRawResourceID, int pWidth, int pHeight, int pTileColumns, int pTileRows) {
        return createTiledFromResource(pBuildableBitmapTextureAtlas, pContext, pRawResourceID, pWidth, pHeight, null, pTileColumns, pTileRows);
    }

    public static TextureRegion createFromResource(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, Context pContext, int pRawResourceID, int pWidth, int pHeight, ISVGColorMapper pSVGColorMapper) {
        return BuildableTextureAtlasTextureRegionFactory.createFromSource(pBuildableBitmapTextureAtlas, new SVGResourceBitmapTextureAtlasSource(pContext, applyScaleFactor(pHeight), pRawResourceID, applyScaleFactor(pWidth), pSVGColorMapper), sCreateTextureRegionBuffersManaged);
    }

    public static TiledTextureRegion createTiledFromResource(BuildableBitmapTextureAtlas pBuildableBitmapTextureAtlas, Context pContext, int pRawResourceID, int pWidth, int pHeight, ISVGColorMapper pSVGColorMapper, int pTileColumns, int pTileRows) {
        return BuildableTextureAtlasTextureRegionFactory.createTiledFromSource(pBuildableBitmapTextureAtlas, new SVGResourceBitmapTextureAtlasSource(pContext, applyScaleFactor(pHeight), pRawResourceID, applyScaleFactor(pWidth), pSVGColorMapper), pTileColumns, pTileRows, sCreateTextureRegionBuffersManaged);
    }
}
