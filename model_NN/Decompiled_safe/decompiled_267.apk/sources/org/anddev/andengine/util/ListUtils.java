package org.anddev.andengine.util;

import java.util.ArrayList;

public class ListUtils {
    public static <T> ArrayList<? extends T> toList(Object obj) {
        ArrayList<T> out = new ArrayList<>();
        out.add(obj);
        return out;
    }

    public static <T> ArrayList<? extends T> toList(Object... pItems) {
        ArrayList<T> out = new ArrayList<>();
        for (Object add : pItems) {
            out.add(add);
        }
        return out;
    }
}
