package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseDurationModifier<T> extends BaseModifier<T> {
    protected final float mDuration;
    private float mSecondsElapsed;

    /* access modifiers changed from: protected */
    public abstract void onManagedInitialize(Object obj);

    /* access modifiers changed from: protected */
    public abstract void onManagedUpdate(float f, Object obj);

    public BaseDurationModifier(float pDuration) {
        this.mDuration = pDuration;
    }

    public BaseDurationModifier(float pDuration, IModifier.IModifierListener<T> pModifierListener) {
        super(pModifierListener);
        this.mDuration = pDuration;
    }

    protected BaseDurationModifier(BaseDurationModifier baseDurationModifier) {
        this(baseDurationModifier.mDuration);
    }

    public float getSecondsElapsed() {
        return this.mSecondsElapsed;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public final float onUpdate(float pSecondsElapsed, T pItem) {
        float secondsElapsedUsed;
        if (this.mFinished) {
            return 0.0f;
        }
        if (this.mSecondsElapsed == 0.0f) {
            onManagedInitialize(pItem);
            onModifierStarted(pItem);
        }
        if (this.mSecondsElapsed + pSecondsElapsed < this.mDuration) {
            secondsElapsedUsed = pSecondsElapsed;
        } else {
            secondsElapsedUsed = this.mDuration - this.mSecondsElapsed;
        }
        this.mSecondsElapsed += secondsElapsedUsed;
        onManagedUpdate(secondsElapsedUsed, pItem);
        if (this.mDuration != -1.0f && this.mSecondsElapsed >= this.mDuration) {
            this.mSecondsElapsed = this.mDuration;
            this.mFinished = true;
            onModifierFinished(pItem);
        }
        return secondsElapsedUsed;
    }

    public void reset() {
        this.mFinished = false;
        this.mSecondsElapsed = 0.0f;
    }
}
