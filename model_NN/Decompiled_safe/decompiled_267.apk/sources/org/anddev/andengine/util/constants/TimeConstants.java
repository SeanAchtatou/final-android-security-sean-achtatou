package org.anddev.andengine.util.constants;

public interface TimeConstants {
    public static final int DAYSPERMONTH = 30;
    public static final int DAYSPERWEEK = 7;
    public static final int HOURSPERDAY = 24;
    public static final long MICROSECONDSPERMILLISECOND = 1000;
    public static final int MICROSECONDSPERSECOND = 1000000;
    public static final int MILLISECONDSPERSECOND = 1000;
    public static final int MINUTESPERHOUR = 60;
    public static final int MONTHSPERYEAR = 12;
    public static final long NANOSECONDSPERMICROSECOND = 1000;
    public static final long NANOSECONDSPERMILLISECOND = 1000000;
    public static final long NANOSECONDSPERSECOND = 1000000000;
    public static final int SECONDSPERDAY = 86400;
    public static final int SECONDSPERHOUR = 3600;
    public static final float SECONDSPERMICROSECOND = 1.0E-6f;
    public static final float SECONDSPERMILLISECOND = 0.001f;
    public static final int SECONDSPERMINUTE = 60;
    public static final int SECONDSPERMONTH = 2592000;
    public static final float SECONDSPERNANOSECOND = 1.0E-9f;
    public static final int SECONDSPERWEEK = 604800;
    public static final int SECONDSPERYEAR = 31104000;
}
