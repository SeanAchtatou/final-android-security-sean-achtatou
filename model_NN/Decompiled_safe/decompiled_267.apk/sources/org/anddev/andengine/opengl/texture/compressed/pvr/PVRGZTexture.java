package org.anddev.andengine.opengl.texture.compressed.pvr;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;

public abstract class PVRGZTexture extends PVRTexture {
    /* access modifiers changed from: protected */
    public abstract InputStream getInputStream() throws IOException;

    public PVRGZTexture(PVRTexture.PVRTextureFormat pPVRTextureFormat) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat);
    }

    public PVRGZTexture(PVRTexture.PVRTextureFormat pPVRTextureFormat, ITexture.ITextureStateListener pTextureStateListener) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat, pTextureStateListener);
    }

    public PVRGZTexture(PVRTexture.PVRTextureFormat pPVRTextureFormat, TextureOptions pTextureOptions) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat, pTextureOptions);
    }

    public PVRGZTexture(PVRTexture.PVRTextureFormat pPVRTextureFormat, TextureOptions pTextureOptions, ITexture.ITextureStateListener pTextureStateListener) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat, pTextureOptions, pTextureStateListener);
    }

    /* access modifiers changed from: protected */
    public final InputStream onGetInputStream() throws IOException {
        return new GZIPInputStream(getInputStream());
    }
}
