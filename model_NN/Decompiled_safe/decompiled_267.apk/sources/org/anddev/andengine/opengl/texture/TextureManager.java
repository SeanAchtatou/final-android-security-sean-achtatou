package org.anddev.andengine.opengl.texture;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.util.Debug;

public class TextureManager {
    private final ArrayList<ITexture> mTexturesLoaded = new ArrayList<>();
    private final HashSet<ITexture> mTexturesManaged = new HashSet<>();
    private final ArrayList<ITexture> mTexturesToBeLoaded = new ArrayList<>();
    private final ArrayList<ITexture> mTexturesToBeUnloaded = new ArrayList<>();

    /* access modifiers changed from: protected */
    public void clear() {
        this.mTexturesToBeLoaded.clear();
        this.mTexturesLoaded.clear();
        this.mTexturesManaged.clear();
    }

    public boolean loadTexture(ITexture pTexture) {
        if (this.mTexturesManaged.contains(pTexture)) {
            this.mTexturesToBeUnloaded.remove(pTexture);
            return false;
        }
        this.mTexturesManaged.add(pTexture);
        this.mTexturesToBeLoaded.add(pTexture);
        return true;
    }

    public boolean unloadTexture(ITexture pTexture) {
        if (!this.mTexturesManaged.contains(pTexture)) {
            return false;
        }
        if (this.mTexturesLoaded.contains(pTexture)) {
            this.mTexturesToBeUnloaded.add(pTexture);
        } else if (this.mTexturesToBeLoaded.remove(pTexture)) {
            this.mTexturesManaged.remove(pTexture);
        }
        return true;
    }

    public void loadTextures(ITexture... pTextures) {
        for (int i = pTextures.length - 1; i >= 0; i--) {
            loadTexture(pTextures[i]);
        }
    }

    public void unloadTextures(ITexture... pTextures) {
        for (int i = pTextures.length - 1; i >= 0; i--) {
            unloadTexture(pTextures[i]);
        }
    }

    public void reloadTextures() {
        Iterator<ITexture> it = this.mTexturesManaged.iterator();
        while (it.hasNext()) {
            it.next().setLoadedToHardware(false);
        }
        this.mTexturesToBeLoaded.addAll(this.mTexturesLoaded);
        this.mTexturesLoaded.clear();
        this.mTexturesManaged.removeAll(this.mTexturesToBeUnloaded);
        this.mTexturesToBeUnloaded.clear();
    }

    public void updateTextures(GL10 pGL) {
        HashSet<ITexture> texturesManaged = this.mTexturesManaged;
        ArrayList<ITexture> texturesLoaded = this.mTexturesLoaded;
        ArrayList<ITexture> texturesToBeLoaded = this.mTexturesToBeLoaded;
        ArrayList<ITexture> texturesToBeUnloaded = this.mTexturesToBeUnloaded;
        int textursLoadedCount = texturesLoaded.size();
        if (textursLoadedCount > 0) {
            for (int i = textursLoadedCount - 1; i >= 0; i--) {
                ITexture textureToBeReloaded = texturesLoaded.get(i);
                if (textureToBeReloaded.isUpdateOnHardwareNeeded()) {
                    try {
                        textureToBeReloaded.reloadToHardware(pGL);
                    } catch (IOException e) {
                        Debug.e(e);
                    }
                }
            }
        }
        int texturesToBeLoadedCount = texturesToBeLoaded.size();
        if (texturesToBeLoadedCount > 0) {
            for (int i2 = texturesToBeLoadedCount - 1; i2 >= 0; i2--) {
                ITexture textureToBeLoaded = texturesToBeLoaded.remove(i2);
                if (!textureToBeLoaded.isLoadedToHardware()) {
                    try {
                        textureToBeLoaded.loadToHardware(pGL);
                    } catch (IOException e2) {
                        Debug.e(e2);
                    }
                }
                texturesLoaded.add(textureToBeLoaded);
            }
        }
        int texturesToBeUnloadedCount = texturesToBeUnloaded.size();
        if (texturesToBeUnloadedCount > 0) {
            for (int i3 = texturesToBeUnloadedCount - 1; i3 >= 0; i3--) {
                ITexture textureToBeUnloaded = texturesToBeUnloaded.remove(i3);
                if (textureToBeUnloaded.isLoadedToHardware()) {
                    textureToBeUnloaded.unloadFromHardware(pGL);
                }
                texturesLoaded.remove(textureToBeUnloaded);
                texturesManaged.remove(textureToBeUnloaded);
            }
        }
        if (texturesToBeLoadedCount > 0 || texturesToBeUnloadedCount > 0) {
            System.gc();
        }
    }
}
