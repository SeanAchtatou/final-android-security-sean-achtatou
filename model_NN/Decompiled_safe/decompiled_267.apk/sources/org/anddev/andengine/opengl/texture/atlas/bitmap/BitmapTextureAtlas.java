package org.anddev.andengine.opengl.texture.atlas.bitmap;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.ITextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.TextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.Debug;

public class BitmapTextureAtlas extends TextureAtlas<IBitmapTextureAtlasSource> {
    private final BitmapTextureFormat mBitmapTextureFormat;

    public BitmapTextureAtlas(int pWidth, int pHeight) {
        this(pWidth, pHeight, BitmapTextureFormat.RGBA_8888);
    }

    public BitmapTextureAtlas(int pWidth, int pHeight, BitmapTextureFormat pBitmapTextureFormat) {
        this(pWidth, pHeight, pBitmapTextureFormat, TextureOptions.DEFAULT, null);
    }

    public BitmapTextureAtlas(int pWidth, int pHeight, ITextureAtlas.ITextureAtlasStateListener<IBitmapTextureAtlasSource> pTextureAtlasStateListener) {
        this(pWidth, pHeight, BitmapTextureFormat.RGBA_8888, TextureOptions.DEFAULT, pTextureAtlasStateListener);
    }

    public BitmapTextureAtlas(int pWidth, int pHeight, BitmapTextureFormat pBitmapTextureFormat, ITextureAtlas.ITextureAtlasStateListener<IBitmapTextureAtlasSource> pTextureAtlasStateListener) {
        this(pWidth, pHeight, pBitmapTextureFormat, TextureOptions.DEFAULT, pTextureAtlasStateListener);
    }

    public BitmapTextureAtlas(int pWidth, int pHeight, TextureOptions pTextureOptions) throws IllegalArgumentException {
        this(pWidth, pHeight, BitmapTextureFormat.RGBA_8888, pTextureOptions, null);
    }

    public BitmapTextureAtlas(int pWidth, int pHeight, BitmapTextureFormat pBitmapTextureFormat, TextureOptions pTextureOptions) throws IllegalArgumentException {
        this(pWidth, pHeight, pBitmapTextureFormat, pTextureOptions, null);
    }

    public BitmapTextureAtlas(int pWidth, int pHeight, TextureOptions pTextureOptions, ITextureAtlas.ITextureAtlasStateListener<IBitmapTextureAtlasSource> pTextureAtlasStateListener) throws IllegalArgumentException {
        this(pWidth, pHeight, BitmapTextureFormat.RGBA_8888, pTextureOptions, pTextureAtlasStateListener);
    }

    public BitmapTextureAtlas(int pWidth, int pHeight, BitmapTextureFormat pBitmapTextureFormat, TextureOptions pTextureOptions, ITextureAtlas.ITextureAtlasStateListener<IBitmapTextureAtlasSource> pTextureAtlasStateListener) throws IllegalArgumentException {
        super(pWidth, pHeight, pBitmapTextureFormat.getPixelFormat(), pTextureOptions, pTextureAtlasStateListener);
        this.mBitmapTextureFormat = pBitmapTextureFormat;
    }

    public BitmapTextureFormat getBitmapTextureFormat() {
        return this.mBitmapTextureFormat;
    }

    /* access modifiers changed from: protected */
    public void writeTextureToHardware(GL10 pGL) {
        Bitmap.Config bitmapConfig = this.mBitmapTextureFormat.getBitmapConfig();
        int glFormat = this.mPixelFormat.getGLFormat();
        int glType = this.mPixelFormat.getGLType();
        boolean preMultipyAlpha = this.mTextureOptions.mPreMultipyAlpha;
        ArrayList arrayList = this.mTextureAtlasSources;
        int textureSourceCount = arrayList.size();
        for (int j = 0; j < textureSourceCount; j++) {
            IBitmapTextureAtlasSource bitmapTextureAtlasSource = (IBitmapTextureAtlasSource) arrayList.get(j);
            if (bitmapTextureAtlasSource != null) {
                Bitmap bitmap = bitmapTextureAtlasSource.onLoadBitmap(bitmapConfig);
                if (bitmap == null) {
                    try {
                        throw new IllegalArgumentException(String.valueOf(bitmapTextureAtlasSource.getClass().getSimpleName()) + ": " + bitmapTextureAtlasSource.toString() + " returned a null Bitmap.");
                    } catch (IllegalArgumentException e) {
                        IllegalArgumentException iae = e;
                        Debug.e("Error loading: " + bitmapTextureAtlasSource.toString(), iae);
                        if (getTextureStateListener() != null) {
                            getTextureStateListener().onTextureAtlasSourceLoadExeption(this, bitmapTextureAtlasSource, iae);
                        } else {
                            throw iae;
                        }
                    }
                } else {
                    if (preMultipyAlpha) {
                        GLUtils.texSubImage2D(3553, 0, bitmapTextureAtlasSource.getTexturePositionX(), bitmapTextureAtlasSource.getTexturePositionY(), bitmap, glFormat, glType);
                    } else {
                        GLHelper.glTexSubImage2D(pGL, 3553, 0, bitmapTextureAtlasSource.getTexturePositionX(), bitmapTextureAtlasSource.getTexturePositionY(), bitmap, this.mPixelFormat);
                    }
                    bitmap.recycle();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void bindTextureOnHardware(GL10 pGL) {
        super.bindTextureOnHardware(pGL);
        sendPlaceholderBitmapToHardware();
    }

    private void sendPlaceholderBitmapToHardware() {
        Bitmap textureBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, this.mBitmapTextureFormat.getBitmapConfig());
        GLUtils.texImage2D(3553, 0, textureBitmap, 0);
        textureBitmap.recycle();
    }

    public enum BitmapTextureFormat {
        RGBA_4444(Bitmap.Config.ARGB_4444, Texture.PixelFormat.RGBA_4444),
        RGBA_8888(Bitmap.Config.ARGB_8888, Texture.PixelFormat.RGBA_8888),
        RGB_565(Bitmap.Config.RGB_565, Texture.PixelFormat.RGB_565),
        A_8(Bitmap.Config.ALPHA_8, Texture.PixelFormat.A_8);
        
        private final Bitmap.Config mBitmapConfig;
        private final Texture.PixelFormat mPixelFormat;

        private BitmapTextureFormat(Bitmap.Config pBitmapConfig, Texture.PixelFormat pPixelFormat) {
            this.mBitmapConfig = pBitmapConfig;
            this.mPixelFormat = pPixelFormat;
        }

        public Bitmap.Config getBitmapConfig() {
            return this.mBitmapConfig;
        }

        public Texture.PixelFormat getPixelFormat() {
            return this.mPixelFormat;
        }
    }
}
