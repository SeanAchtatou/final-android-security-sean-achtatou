package org.anddev.andengine.entity.layer.tiled.tmx;

import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class TMXObject implements TMXConstants {
    private final int mHeight;
    private final String mName;
    private final TMXProperties<TMXObjectProperty> mTMXObjectProperties;
    private final String mType;
    private final int mWidth;
    private final int mX;
    private final int mY;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: org.anddev.andengine.util.SAXUtils.getIntAttributeOrThrow(org.xml.sax.Attributes, java.lang.String):int in method: org.anddev.andengine.entity.layer.tiled.tmx.TMXObject.<init>(org.xml.sax.Attributes):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: org.anddev.andengine.util.SAXUtils.getIntAttributeOrThrow(org.xml.sax.Attributes, java.lang.String):int
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:528)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public TMXObject(org.xml.sax.Attributes r1) {
        /*
            r3 = this;
            r2 = 0
            r3.<init>()
            org.anddev.andengine.entity.layer.tiled.tmx.TMXProperties r0 = new org.anddev.andengine.entity.layer.tiled.tmx.TMXProperties
            r0.<init>()
            r3.mTMXObjectProperties = r0
            java.lang.String r0 = ""
            java.lang.String r1 = "name"
            java.lang.String r0 = r4.getValue(r0, r1)
            r3.mName = r0
            java.lang.String r0 = ""
            java.lang.String r1 = "type"
            java.lang.String r0 = r4.getValue(r0, r1)
            r3.mType = r0
            java.lang.String r0 = "x"
            int r0 = org.anddev.andengine.util.SAXUtils.getIntAttributeOrThrow(r4, r0)
            r3.mX = r0
            java.lang.String r0 = "y"
            int r0 = org.anddev.andengine.util.SAXUtils.getIntAttributeOrThrow(r4, r0)
            r3.mY = r0
            java.lang.String r0 = "width"
            int r0 = org.anddev.andengine.util.SAXUtils.getIntAttribute(r4, r0, r2)
            r3.mWidth = r0
            java.lang.String r0 = "height"
            int r0 = org.anddev.andengine.util.SAXUtils.getIntAttribute(r4, r0, r2)
            r3.mHeight = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.entity.layer.tiled.tmx.TMXObject.<init>(org.xml.sax.Attributes):void");
    }

    public String getName() {
        return this.mName;
    }

    public String getType() {
        return this.mType;
    }

    public int getX() {
        return this.mX;
    }

    public int getY() {
        return this.mY;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public void addTMXObjectProperty(TMXObjectProperty pTMXObjectProperty) {
        this.mTMXObjectProperties.add(pTMXObjectProperty);
    }

    public TMXProperties<TMXObjectProperty> getTMXObjectProperties() {
        return this.mTMXObjectProperties;
    }
}
