package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.SequenceModifier;

public class SequenceEntityModifier extends SequenceModifier<IEntity> implements IEntityModifier {

    public interface ISubSequenceShapeModifierListener extends SequenceModifier.ISubSequenceModifierListener<IEntity> {
    }

    public SequenceEntityModifier(IEntityModifier... pEntityModifiers) throws IllegalArgumentException {
        super(pEntityModifiers);
    }

    public SequenceEntityModifier(ISubSequenceShapeModifierListener pSubSequenceShapeModifierListener, IEntityModifier... pEntityModifiers) throws IllegalArgumentException {
        super(pSubSequenceShapeModifierListener, pEntityModifiers);
    }

    public SequenceEntityModifier(IEntityModifier.IEntityModifierListener pEntityModifierListener, IEntityModifier... pEntityModifiers) throws IllegalArgumentException {
        super(pEntityModifierListener, pEntityModifiers);
    }

    public SequenceEntityModifier(ISubSequenceShapeModifierListener pSubSequenceShapeModifierListener, IEntityModifier.IEntityModifierListener pEntityModifierListener, IEntityModifier... pEntityModifiers) throws IllegalArgumentException {
        super(pSubSequenceShapeModifierListener, pEntityModifierListener, pEntityModifiers);
    }

    protected SequenceEntityModifier(SequenceEntityModifier pSequenceShapeModifier) throws IModifier.CloneNotSupportedException {
        super(pSequenceShapeModifier);
    }

    public SequenceEntityModifier clone() throws IModifier.CloneNotSupportedException {
        return new SequenceEntityModifier(this);
    }
}
