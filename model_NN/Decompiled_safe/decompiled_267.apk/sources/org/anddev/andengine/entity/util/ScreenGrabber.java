package org.anddev.andengine.entity.util;

import android.graphics.Bitmap;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.extension.svg.util.constants.ColorUtils;

public class ScreenGrabber extends Entity {
    private int mGrabHeight;
    private int mGrabWidth;
    private int mGrabX;
    private int mGrabY;
    private IScreenGrabberCallback mScreenGrabCallback;
    private boolean mScreenGrabPending = false;

    public interface IScreenGrabberCallback {
        void onScreenGrabFailed(Exception exc);

        void onScreenGrabbed(Bitmap bitmap);
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        if (this.mScreenGrabPending) {
            try {
                this.mScreenGrabCallback.onScreenGrabbed(grab(this.mGrabX, this.mGrabY, this.mGrabWidth, this.mGrabHeight, pGL));
            } catch (Exception e) {
                this.mScreenGrabCallback.onScreenGrabFailed(e);
            }
            this.mScreenGrabPending = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
    }

    public void reset() {
    }

    public void grab(int pGrabWidth, int pGrabHeight, IScreenGrabberCallback pScreenGrabCallback) {
        grab(0, 0, pGrabWidth, pGrabHeight, pScreenGrabCallback);
    }

    public void grab(int pGrabX, int pGrabY, int pGrabWidth, int pGrabHeight, IScreenGrabberCallback pScreenGrabCallback) {
        this.mGrabX = pGrabX;
        this.mGrabY = pGrabY;
        this.mGrabWidth = pGrabWidth;
        this.mGrabHeight = pGrabHeight;
        this.mScreenGrabCallback = pScreenGrabCallback;
        this.mScreenGrabPending = true;
    }

    /* JADX INFO: Multiple debug info for r20v2 int: [D('greenAlpha' int), D('pixel' int)] */
    private static Bitmap grab(int pGrabX, int pGrabY, int pGrabWidth, int pGrabHeight, GL10 pGL) {
        int[] source = new int[((pGrabY + pGrabHeight) * pGrabWidth)];
        IntBuffer sourceBuffer = IntBuffer.wrap(source);
        sourceBuffer.position(0);
        pGL.glReadPixels(pGrabX, 0, pGrabWidth, pGrabY + pGrabHeight, 6408, 5121, sourceBuffer);
        int[] pixels = new int[(pGrabWidth * pGrabHeight)];
        int y = 0;
        while (true) {
            int y2 = y;
            if (y2 >= pGrabHeight) {
                return Bitmap.createBitmap(pixels, pGrabWidth, pGrabHeight, Bitmap.Config.ARGB_8888);
            }
            int blue = 0;
            while (true) {
                int x = blue;
                if (x >= pGrabWidth) {
                    break;
                }
                int pixel = source[((pGrabY + y2) * pGrabWidth) + x];
                pixels[(((pGrabHeight - y2) - 1) * pGrabWidth) + x] = ((16711680 & pixel) >> 16) | (pixel & -16711936) | ((pixel & ColorUtils.COLOR_MASK_32BIT_ARGB_B) << 16);
                blue = x + 1;
            }
            y = y2 + 1;
        }
    }
}
