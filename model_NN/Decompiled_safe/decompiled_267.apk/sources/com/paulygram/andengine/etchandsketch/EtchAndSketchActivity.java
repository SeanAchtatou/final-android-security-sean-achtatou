package com.paulygram.andengine.etchandsketch;

import android.widget.Toast;
import java.util.Random;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.extension.input.touch.controller.MultiTouch;
import org.anddev.andengine.extension.input.touch.controller.MultiTouchController;
import org.anddev.andengine.extension.input.touch.exception.MultiTouchException;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.ui.activity.LayoutGameActivity;
import org.anddev.andengine.util.MathUtils;

public class EtchAndSketchActivity extends LayoutGameActivity {
    private static final int AD_SHOW_AFTER_SEC = 45;
    private static final int AD_SHOW_FOR_SEC = 30;
    private static final int CAMERA_HEIGHT = 480;
    private static final int CAMERA_WIDTH = 720;
    private static final int CONTROLLER_HORIZONTAL = 0;
    private static final int CONTROLLER_VERTICAL = 1;
    private static final int FRAME_OFFSET = 55;
    private static final int LAYER_BACKGROUND = 0;
    private static final int LAYER_GAMEBOARD = 1;
    private static final int MOVE_HORIZONTAL_LEFT = 1;
    private static final int MOVE_HORIZONTAL_NONE = 0;
    private static final int MOVE_HORIZONTAL_RIGHT = 2;
    private static final int MOVE_VERTICAL_DOWN = 2;
    private static final int MOVE_VERTICAL_NONE = 0;
    private static final int MOVE_VERTICAL_UP = 1;
    private static final int PLAY_AREA_HEIGHT = 343;
    private static final int PLAY_AREA_WIDTH = 610;
    private static final long RANDOM_SEED = 1234567890;
    private static final int ROTATION_CLOCKWISE = 1;
    private static final int ROTATION_STATIONARY = 0;
    /* access modifiers changed from: private */
    public int directionX;
    /* access modifiers changed from: private */
    public int directionY;
    private double lastHorizontalRadian = 0.0d;
    /* access modifiers changed from: private */
    public int lastLinePositionX;
    /* access modifiers changed from: private */
    public int lastLinePositionY;
    private double lastVerticalRadian = 0.0d;
    private BitmapTextureAtlas mBackgroundTexture;
    private TextureRegion mBackgroundTextureRegion;
    private BitmapTextureAtlas mBitmapTextureAtlas;
    private Camera mCamera;
    private TextureRegion mFaceTextureRegion;
    private Font mFont;
    private BitmapTextureAtlas mFontTexture;
    private ChangeableText mHorizontalXValueText;
    private ChangeableText mHorizontalYValueText;
    /* access modifiers changed from: private */
    public int mMovementModifier = 5;
    private TextureRegion mOnScreenControlBaseTextureRegion;
    private TextureRegion mOnScreenControlKnobTextureRegion;
    private BitmapTextureAtlas mOnScreenControlTexture;
    private boolean mPlaceOnScreenControlsAtDifferentVerticalLocations = false;
    final Random mRandom = new Random(RANDOM_SEED);
    private Scene mScene;
    /* access modifiers changed from: private */
    public float mSecondsElappsed;
    /* access modifiers changed from: private */
    public int nextLinePositionX;
    /* access modifiers changed from: private */
    public int nextLinePositionY;

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, 720.0f, 480.0f);
        Engine engine = new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(720.0f, 480.0f), this.mCamera));
        try {
            if (MultiTouch.isSupported(this)) {
                engine.setTouchController(new MultiTouchController());
                if (!MultiTouch.isSupportedDistinct(this)) {
                    this.mPlaceOnScreenControlsAtDifferentVerticalLocations = true;
                    Toast.makeText(this, "MultiTouch detected, but your device has problems distinguishing between fingers.\n\nControls are placed at different vertical locations.", 1).show();
                }
            } else {
                Toast.makeText(this, "Sorry your device does NOT support MultiTouch!\n\n(Falling back to SingleTouch.)\n\nControls are placed at different vertical locations.", 1).show();
            }
        } catch (MultiTouchException e) {
            Toast.makeText(this, "Sorry your Android Version does NOT support MultiTouch!\n\n(Falling back to SingleTouch.)\n\nControls are placed at different vertical locations.", 1).show();
        }
        return engine;
    }

    public void onLoadResources() {
        try {
            FontFactory.setAssetBasePath("font/");
            this.mFontTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_TWIDDLE, (int) PVRTexture.FLAG_TWIDDLE, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            this.mFont = FontFactory.createFromAsset(this.mFontTexture, this, "Droid.ttf", 32.0f, true, -1);
            this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
            this.mEngine.getFontManager().loadFont(this.mFont);
            BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
            this.mBitmapTextureAtlas = new BitmapTextureAtlas(32, 32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            this.mBackgroundTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_TWIDDLE, TextureOptions.DEFAULT);
            this.mBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBackgroundTexture, this, "red_board.png", 0, 0);
            this.mOnScreenControlTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
            this.mOnScreenControlBaseTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_base.png", 0, 0);
            this.mOnScreenControlKnobTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_knob.png", 128, 0);
            this.mEngine.getTextureManager().loadTextures(this.mBitmapTextureAtlas, this.mOnScreenControlTexture);
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, "Illegal argument throw from new BitmapTextureAtlas.)", 1).show();
        } catch (Exception e2) {
            Toast.makeText(this, "Exception: " + e2.toString(), 1).show();
        }
    }

    public Scene onLoadScene() {
        int y2;
        this.mEngine.registerUpdateHandler(new FPSLogger());
        Scene scene = new Scene();
        scene.setBackground(new ColorBackground(1.0f, 0.0f, 0.0f));
        int y1 = CAMERA_HEIGHT - this.mOnScreenControlBaseTextureRegion.getHeight();
        final Scene scene2 = scene;
        AnalogOnScreenControl horizontalOnScreenControl = new AnalogOnScreenControl(0.0f, (float) y1, this.mCamera, this.mOnScreenControlBaseTextureRegion, this.mOnScreenControlKnobTextureRegion, 0.1f, new AnalogOnScreenControl.IAnalogOnScreenControlListener() {
            public void onControlChange(BaseOnScreenControl pBaseOnScreenControl, float pValueX, float pValueY) {
                EtchAndSketchActivity.this.directionX = EtchAndSketchActivity.this.getHorizontalDirection(0.0f, 0.0f, pValueX, pValueY);
                if (EtchAndSketchActivity.this.directionX == 2) {
                    if (EtchAndSketchActivity.this.lastLinePositionX + EtchAndSketchActivity.this.mMovementModifier >= 665) {
                        EtchAndSketchActivity.this.nextLinePositionX = 664;
                    } else {
                        EtchAndSketchActivity.this.nextLinePositionX = EtchAndSketchActivity.this.lastLinePositionX + EtchAndSketchActivity.this.mMovementModifier;
                    }
                } else if (EtchAndSketchActivity.this.directionX != 1) {
                    EtchAndSketchActivity.this.nextLinePositionX = EtchAndSketchActivity.this.lastLinePositionX;
                } else if (EtchAndSketchActivity.this.lastLinePositionX - EtchAndSketchActivity.this.mMovementModifier <= EtchAndSketchActivity.FRAME_OFFSET) {
                    EtchAndSketchActivity.this.nextLinePositionX = 56;
                } else {
                    EtchAndSketchActivity.this.nextLinePositionX = EtchAndSketchActivity.this.lastLinePositionX - EtchAndSketchActivity.this.mMovementModifier;
                }
                Line line = new Line((float) EtchAndSketchActivity.this.lastLinePositionX, (float) EtchAndSketchActivity.this.lastLinePositionY, (float) EtchAndSketchActivity.this.nextLinePositionX, (float) EtchAndSketchActivity.this.lastLinePositionY, 3.0f);
                line.setColor(0.3f, 0.3f, 0.3f);
                scene2.attachChild(line);
                EtchAndSketchActivity.this.lastLinePositionX = EtchAndSketchActivity.this.nextLinePositionX;
            }

            public void onControlClick(AnalogOnScreenControl pAnalogOnScreenControl) {
            }
        });
        horizontalOnScreenControl.getControlBase().setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        horizontalOnScreenControl.getControlBase().setAlpha(0.5f);
        scene.setChildScene(horizontalOnScreenControl);
        if (this.mPlaceOnScreenControlsAtDifferentVerticalLocations) {
            y2 = 0;
        } else {
            y2 = y1;
        }
        final Scene scene3 = scene;
        AnalogOnScreenControl verticalOnScreenControl = new AnalogOnScreenControl((float) (CAMERA_WIDTH - this.mOnScreenControlBaseTextureRegion.getWidth()), (float) y2, this.mCamera, this.mOnScreenControlBaseTextureRegion, this.mOnScreenControlKnobTextureRegion, 0.1f, new AnalogOnScreenControl.IAnalogOnScreenControlListener() {
            public void onControlChange(BaseOnScreenControl pBaseOnScreenControl, float pValueX, float pValueY) {
                EtchAndSketchActivity.this.directionY = EtchAndSketchActivity.this.getVerticalDirection(0.0f, 0.0f, pValueX, pValueY);
                if (EtchAndSketchActivity.this.directionY == 2) {
                    if (EtchAndSketchActivity.this.lastLinePositionY + EtchAndSketchActivity.this.mMovementModifier >= 398) {
                        EtchAndSketchActivity.this.nextLinePositionY = 397;
                    } else {
                        EtchAndSketchActivity.this.nextLinePositionY = EtchAndSketchActivity.this.lastLinePositionY + EtchAndSketchActivity.this.mMovementModifier;
                    }
                } else if (EtchAndSketchActivity.this.directionY != 1) {
                    EtchAndSketchActivity.this.nextLinePositionY = EtchAndSketchActivity.this.lastLinePositionY;
                } else if (EtchAndSketchActivity.this.nextLinePositionY - EtchAndSketchActivity.this.mMovementModifier <= EtchAndSketchActivity.FRAME_OFFSET) {
                    EtchAndSketchActivity.this.nextLinePositionY = 56;
                } else {
                    EtchAndSketchActivity.this.nextLinePositionY = EtchAndSketchActivity.this.lastLinePositionY - EtchAndSketchActivity.this.mMovementModifier;
                }
                Line line = new Line((float) EtchAndSketchActivity.this.lastLinePositionX, (float) EtchAndSketchActivity.this.lastLinePositionY, (float) EtchAndSketchActivity.this.lastLinePositionX, (float) EtchAndSketchActivity.this.nextLinePositionY, 3.0f);
                line.setColor(0.3f, 0.3f, 0.3f);
                scene3.attachChild(line);
                EtchAndSketchActivity.this.lastLinePositionY = EtchAndSketchActivity.this.nextLinePositionY;
            }

            public void onControlClick(AnalogOnScreenControl pAnalogOnScreenControl) {
            }
        });
        verticalOnScreenControl.getControlBase().setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        verticalOnScreenControl.getControlBase().setAlpha(0.5f);
        horizontalOnScreenControl.setChildScene(verticalOnScreenControl);
        scene.attachChild(makeColoredRectangle(55.0f, 55.0f, 610.0f, 343.0f, 0.8f, 0.8f, 0.8f));
        this.lastLinePositionX = 70;
        this.lastLinePositionY = 70;
        Line line = new Line((float) this.lastLinePositionX, (float) this.lastLinePositionY, (float) this.lastLinePositionX, (float) this.lastLinePositionY, 3.0f);
        line.setColor(1.0f, 1.0f, 1.0f);
        scene.attachChild(line);
        this.mHorizontalXValueText = new ChangeableText(115.0f, 5.0f, this.mFont, "Etch and Sketch by PaulyGram", "Etch and Sketch by PaulyGram".length());
        this.mHorizontalXValueText.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.mHorizontalXValueText.setAlpha(0.5f);
        scene.attachChild(this.mHorizontalXValueText);
        this.mHorizontalYValueText = new ChangeableText(200.0f, 410.0f, this.mFont, "www.paulygram.com", "www.paulygram.com".length());
        this.mHorizontalYValueText.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        this.mHorizontalYValueText.setAlpha(0.5f);
        scene.attachChild(this.mHorizontalYValueText);
        this.mScene = scene;
        return scene;
    }

    public void onLoadComplete() {
        this.mScene.registerUpdateHandler(new IUpdateHandler() {
            private boolean isAdVisible = false;

            public void reset() {
                EtchAndSketchActivity.this.mSecondsElappsed = 0.0f;
            }

            public void onUpdate(float pSecondsElapsed) {
                EtchAndSketchActivity etchAndSketchActivity = EtchAndSketchActivity.this;
                etchAndSketchActivity.mSecondsElappsed = etchAndSketchActivity.mSecondsElappsed + pSecondsElapsed;
                if (!this.isAdVisible && EtchAndSketchActivity.this.mSecondsElappsed >= 45.0f) {
                    this.isAdVisible = true;
                    EtchAndSketchActivity.this.togglePlayAdView();
                    reset();
                } else if (this.isAdVisible && EtchAndSketchActivity.this.mSecondsElappsed >= 30.0f) {
                    this.isAdVisible = false;
                    EtchAndSketchActivity.this.togglePlayAdView();
                    reset();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void togglePlayAdView() {
        runOnUiThread(new Runnable() {
            public void run() {
            }
        });
    }

    /* access modifiers changed from: private */
    public int getHorizontalDirection(float X1, float Y1, float X2, float Y2) {
        int retVal;
        double currentHorizontalRadian = rotationInRadians(X1, Y1, X2, Y2);
        if (X2 == 0.0f && Y2 == 0.0f) {
            return 0;
        }
        if (this.lastHorizontalRadian < currentHorizontalRadian) {
            retVal = 2;
        } else {
            retVal = 1;
        }
        this.lastHorizontalRadian = currentHorizontalRadian;
        return retVal;
    }

    /* access modifiers changed from: private */
    public int getVerticalDirection(float X1, float Y1, float X2, float Y2) {
        int retVal;
        double currentVerticalRadian = rotationInRadians(X1, Y1, X2, Y2);
        if (X2 == 0.0f && Y2 == 0.0f) {
            return 0;
        }
        if (this.lastVerticalRadian < currentVerticalRadian) {
            retVal = 2;
        } else {
            retVal = 1;
        }
        this.lastVerticalRadian = currentVerticalRadian;
        return retVal;
    }

    private double rotationInRadians(float X1, float Y1, float X2, float Y2) {
        return (double) MathUtils.atan2(Y2 - Y1, X2 - X1);
    }

    private Rectangle makeColoredRectangle(float pX, float pY, float pX2, float pY2, float pRed, float pGreen, float pBlue) {
        Rectangle coloredRect = new Rectangle(pX, pY, pX2, pY2);
        coloredRect.setColor(pRed, pGreen, pBlue);
        return coloredRect;
    }

    /* access modifiers changed from: protected */
    public int getLayoutID() {
        return R.layout.main;
    }

    /* access modifiers changed from: protected */
    public int getRenderSurfaceViewID() {
        return R.id.game_rendersurfaceview;
    }
}
