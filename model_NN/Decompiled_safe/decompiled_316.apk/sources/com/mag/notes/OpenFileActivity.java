package com.mag.notes;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class OpenFileActivity extends Activity {
    /* access modifiers changed from: private */
    public String changedText;
    /* access modifiers changed from: private */
    public String fileName;
    private TextView fileNameTextView;
    /* access modifiers changed from: private */
    public EditText openEditText;
    /* access modifiers changed from: private */
    public Button saveButton;
    /* access modifiers changed from: private */
    public String tempText;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.openfile);
        this.fileNameTextView = (TextView) findViewById(R.id.fileNameTextView);
        this.openEditText = (EditText) findViewById(R.id.openEditText);
        this.saveButton = (Button) findViewById(R.id.saveButton);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.fileName = extras.getString("title_of_file");
            this.fileNameTextView.setText(this.fileName);
            this.openEditText.setText(extras.getString("text_from_file"));
        }
        this.tempText = this.openEditText.getText().toString();
        Log.d("EditText Value", this.tempText);
        this.openEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable arg0) {
                OpenFileActivity.this.changedText = arg0.toString().trim();
                Log.d("Changed Value", OpenFileActivity.this.changedText);
                if (arg0.length() == 0 || OpenFileActivity.this.tempText.equals(OpenFileActivity.this.changedText)) {
                    OpenFileActivity.this.saveButton.setVisibility(8);
                } else {
                    OpenFileActivity.this.saveButton.setVisibility(0);
                }
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        this.saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                File path = Environment.getExternalStoragePublicDirectory("my_notepad");
                Log.d("File Name", path + OpenFileActivity.this.fileName);
                try {
                    FileOutputStream fos = new FileOutputStream(path + "/" + OpenFileActivity.this.fileName);
                    Log.d("File Content", OpenFileActivity.this.openEditText.getText().toString());
                    fos.write(OpenFileActivity.this.openEditText.getText().toString().getBytes());
                    fos.flush();
                    fos.close();
                    Toast.makeText(OpenFileActivity.this, "Text Saved!", 0).show();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        });
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_main);
        AdWhirlLayout awl = new AdWhirlLayout(this, getString(R.string.adwhirl_key));
        RelativeLayout.LayoutParams awllp = new RelativeLayout.LayoutParams(-1, 53);
        awllp.addRule(12);
        rl.addView(awl, awllp);
        rl.invalidate();
    }
}
