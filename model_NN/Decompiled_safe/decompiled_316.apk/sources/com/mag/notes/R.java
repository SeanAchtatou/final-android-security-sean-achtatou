package com.mag.notes;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int app_icon_72px = 2130837504;
        public static final int background = 2130837505;
        public static final int btn_delete = 2130837506;
        public static final int btn_new = 2130837507;
        public static final int btn_save = 2130837508;
        public static final int cb_delete = 2130837509;
        public static final int delete_normal = 2130837510;
        public static final int delete_pressed = 2130837511;
        public static final int ic_list_delete_selected = 2130837512;
        public static final int ic_list_delete_unselect = 2130837513;
        public static final int ic_menu_clear = 2130837514;
        public static final int ic_menu_delete = 2130837515;
        public static final int ic_menu_exit = 2130837516;
        public static final int ic_menu_save = 2130837517;
        public static final int icon = 2130837518;
        public static final int new_file_normal = 2130837519;
        public static final int new_file_pressed = 2130837520;
        public static final int save_icon_normal = 2130837521;
        public static final int save_icon_pressed = 2130837522;
        public static final int splash_screen_notes = 2130837523;
        public static final int title_bg = 2130837524;
        public static final int title_bg_blue = 2130837525;
    }

    public static final class id {
        public static final int clear = 2131099664;
        public static final int deleteButton = 2131099650;
        public static final int deleteFilesListView = 2131099652;
        public static final int deletemany = 2131099665;
        public static final int exit = 2131099666;
        public static final int fileNameTextView = 2131099661;
        public static final int filesListView = 2131099651;
        public static final int iconSaveButton = 2131099656;
        public static final int newButton = 2131099660;
        public static final int newFileEditText = 2131099658;
        public static final int newfilefilenameeditText = 2131099657;
        public static final int newfilenameTextView = 2131099655;
        public static final int newfiletitle = 2131099653;
        public static final int openEditText = 2131099663;
        public static final int relativeLayout = 2131099654;
        public static final int rl_main = 2131099648;
        public static final int saveButton = 2131099662;
        public static final int titleTextView = 2131099659;
        public static final int title_layout = 2131099649;
    }

    public static final class layout {
        public static final int checkable_list_item = 2130903040;
        public static final int list_item = 2130903041;
        public static final int main = 2130903042;
        public static final int newfile = 2130903043;
        public static final int notes_title = 2130903044;
        public static final int openfile = 2130903045;
        public static final int splash = 2130903046;
    }

    public static final class menu {
        public static final int newfilemenu = 2131034112;
        public static final int notesmenu = 2131034113;
    }

    public static final class string {
        public static final int adwhirl_key = 2130968578;
        public static final int app_name = 2130968576;
        public static final int title = 2130968577;
    }
}
