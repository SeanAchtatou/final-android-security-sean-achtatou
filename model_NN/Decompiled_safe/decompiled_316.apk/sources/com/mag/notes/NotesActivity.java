package com.mag.notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.util.AdWhirlUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class NotesActivity extends Activity {
    private File currentFile;
    /* access modifiers changed from: private */
    public Button deleteButton;
    /* access modifiers changed from: private */
    public ListView deleteFilesListView;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> deleteListAdapter;
    /* access modifiers changed from: private */
    public ArrayList<String> fileArrayList;
    private ListView filesListView;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> listItemAdapter;
    private Button newButton;
    /* access modifiers changed from: private */
    public File path;
    /* access modifiers changed from: private */
    public ArrayList<Integer> positionList;
    private TextView titleTextView;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.newButton = (Button) findViewById(R.id.newButton);
        this.titleTextView = (TextView) findViewById(R.id.titleTextView);
        this.titleTextView.setText("Notes");
        this.filesListView = (ListView) findViewById(R.id.filesListView);
        this.deleteFilesListView = (ListView) findViewById(R.id.deleteFilesListView);
        this.positionList = new ArrayList<>();
        this.newButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NotesActivity.this.startActivity(new Intent(NotesActivity.this, NewFileActivity.class));
            }
        });
        this.path = Environment.getExternalStoragePublicDirectory("my_notepad");
        if (!this.path.exists()) {
            this.path.mkdirs();
        }
        String[] files = this.path.list();
        this.fileArrayList = new ArrayList<>();
        for (String f : files) {
            this.fileArrayList.add(f);
        }
        this.listItemAdapter = new ArrayAdapter<>(this, (int) R.layout.list_item, this.fileArrayList);
        for (int i = 0; i < files.length; i++) {
            registerForContextMenu(this.filesListView);
        }
        this.filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long id) {
                File filePath = new File(NotesActivity.this.path + "/" + ((String) NotesActivity.this.fileArrayList.get(position)));
                StringBuffer strContent = new StringBuffer("");
                FileInputStream fin = null;
                try {
                    fin = new FileInputStream(filePath);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                while (true) {
                    try {
                        int ch = fin.read();
                        if (ch == -1) {
                            break;
                        }
                        strContent.append((char) ch);
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
                fin.close();
                String titlename = ((String) NotesActivity.this.fileArrayList.get(position)).toString();
                Intent myintent = new Intent(NotesActivity.this, OpenFileActivity.class);
                myintent.setFlags(67108864);
                myintent.putExtra("title_of_file", titlename);
                myintent.putExtra("text_from_file", strContent.toString());
                NotesActivity.this.startActivity(myintent);
            }
        });
        this.deleteFilesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (view instanceof CheckedTextView) {
                    if (!((CheckedTextView) view).isChecked()) {
                        NotesActivity.this.deleteButton.setVisibility(0);
                        NotesActivity.this.positionList.add(Integer.valueOf(position));
                    } else {
                        NotesActivity.this.positionList.remove(Integer.valueOf(position));
                    }
                    if (NotesActivity.this.positionList.size() == 0) {
                        NotesActivity.this.deleteButton.setVisibility(8);
                    }
                    Log.d("Position List on item Select", NotesActivity.this.positionList.toString());
                }
            }
        });
        this.deleteButton = (Button) findViewById(R.id.deleteButton);
        this.deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                List<String> tempFileList = NotesActivity.this.fileArrayList;
                Collections.sort(NotesActivity.this.positionList, Collections.reverseOrder());
                while (NotesActivity.this.positionList.size() > 0) {
                    new File(NotesActivity.this.path + "/" + ((String) tempFileList.get(((Integer) NotesActivity.this.positionList.get(0)).intValue()))).delete();
                    NotesActivity.this.fileArrayList.remove(Integer.parseInt(((Integer) NotesActivity.this.positionList.get(0)).toString()));
                    NotesActivity.this.positionList.remove(0);
                }
                NotesActivity.this.deleteButton.setVisibility(8);
                for (int i = 0; i < NotesActivity.this.deleteListAdapter.getCount(); i++) {
                    NotesActivity.this.deleteFilesListView.setItemChecked(i, false);
                }
                NotesActivity.this.listItemAdapter.notifyDataSetChanged();
                NotesActivity.this.deleteListAdapter.notifyDataSetChanged();
                Toast.makeText(NotesActivity.this, "Files Deleted", 0).show();
                if (NotesActivity.this.fileArrayList.isEmpty()) {
                    NotesActivity.this.deleteButton.setVisibility(8);
                }
            }
        });
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_main);
        AdWhirlLayout awl = new AdWhirlLayout(this, getString(R.string.adwhirl_key));
        RelativeLayout.LayoutParams awllp = new RelativeLayout.LayoutParams(-1, 53);
        awllp.addRule(12);
        rl.addView(awl, awllp);
        rl.invalidate();
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        menu.setHeaderTitle("Operations");
        menu.add(1, 1, 0, "Delete");
        menu.add(2, 2, 1, "Share");
        menu.add(3, 3, 2, "Info");
    }

    public boolean onContextItemSelected(MenuItem item) {
        int i = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        this.currentFile = new File(this.path + "/" + this.fileArrayList.get(i));
        switch (item.getItemId()) {
            case 1:
                delete(i);
                return true;
            case 2:
                share(i);
                return true;
            case AdWhirlUtil.NETWORK_TYPE_VIDEOEGG:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("File Info");
                alert.setMessage("File Name: " + this.currentFile.getName() + "\n" + "Location: " + this.path + "\n" + "Size: " + this.currentFile.length() + "bytes" + "\n" + "Last Modified: " + new Date(this.currentFile.lastModified()));
                alert.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                alert.show();
                return true;
            default:
                return false;
        }
    }

    public void delete(int i) {
        this.currentFile.delete();
        Toast.makeText(this, "File Deleted", 0).show();
        this.fileArrayList.remove(i);
        this.listItemAdapter.notifyDataSetChanged();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notesmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.deletemany /*2131099665*/:
                deletemany();
                return true;
            case R.id.exit /*2131099666*/:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void share(int i) {
        this.path = Environment.getExternalStoragePublicDirectory("my_notepad");
        File filePath = new File(this.path + "/" + this.path.list()[i]);
        StringBuffer strContent = new StringBuffer("");
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                int ch = fin.read();
                if (ch == -1) {
                    break;
                }
                strContent.append((char) ch);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        fin.close();
        Intent contactIntent = new Intent("android.intent.action.SEND");
        contactIntent.setType("text/plain");
        contactIntent.putExtra("android.intent.extra.TEXT", strContent.toString());
        startActivity(Intent.createChooser(contactIntent, "Share with"));
    }

    public void deletemany() {
        this.titleTextView.setText("Delete");
        this.newButton.setVisibility(8);
        this.filesListView.setVisibility(8);
        this.deleteListAdapter = new ArrayAdapter<>(this, (int) R.layout.checkable_list_item, this.fileArrayList);
        this.deleteFilesListView.setAdapter((ListAdapter) this.deleteListAdapter);
        this.deleteFilesListView.setChoiceMode(2);
        this.deleteFilesListView.setVisibility(0);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.deletemany);
        MenuItem item1 = menu.findItem(R.id.exit);
        if (this.deleteFilesListView.isShown()) {
            item.setVisible(false);
            item1.setVisible(false);
        } else {
            item.setVisible(true);
            item1.setVisible(true);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        String[] files = this.path.list();
        this.fileArrayList = new ArrayList<>();
        for (String f : files) {
            this.fileArrayList.add(f);
        }
        this.listItemAdapter = new ArrayAdapter<>(this, (int) R.layout.list_item, this.fileArrayList);
        for (int i = 0; i < files.length; i++) {
            this.filesListView.setAdapter((ListAdapter) this.listItemAdapter);
        }
        this.listItemAdapter.notifyDataSetChanged();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.deleteFilesListView.isShown()) {
                this.listItemAdapter.notifyDataSetChanged();
                this.titleTextView.setText("Notes");
                this.newButton.setVisibility(0);
                this.deleteButton.setVisibility(8);
                this.deleteFilesListView.setVisibility(8);
                this.filesListView.setVisibility(0);
            } else {
                finish();
            }
        }
        return false;
    }
}
