package com.mag.notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class NewFileActivity extends Activity {
    private static final String EXTENSION = ".txt";
    private static final String FOLDER = "my_notepad";
    private static final String OUTPUT_PATH = "/sdcard/my_notepad";
    /* access modifiers changed from: private */
    public EditText editText;
    /* access modifiers changed from: private */
    public EditText fileName;
    private Button iconSave;
    /* access modifiers changed from: private */
    public RelativeLayout re;
    /* access modifiers changed from: private */
    public String saveFileName;
    /* access modifiers changed from: private */
    public String text;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.newfile);
        this.editText = (EditText) findViewById(R.id.newFileEditText);
        this.fileName = (EditText) findViewById(R.id.newfilefilenameeditText);
        this.iconSave = (Button) findViewById(R.id.iconSaveButton);
        this.re = (RelativeLayout) findViewById(R.id.relativeLayout);
        this.editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable arg0) {
                NewFileActivity.this.text = NewFileActivity.this.editText.getText().toString().trim();
                if (arg0.length() == 0 || NewFileActivity.this.text.length() == 0) {
                    NewFileActivity.this.re.setVisibility(8);
                } else {
                    NewFileActivity.this.re.setVisibility(0);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        this.iconSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    NewFileActivity.this.saveFileName = NewFileActivity.this.fileName.getText().toString().trim();
                    if (NewFileActivity.this.saveFileName.length() == 0) {
                        AlertDialog.Builder noNameAlert = new AlertDialog.Builder(NewFileActivity.this);
                        noNameAlert.setTitle("File Name Required");
                        noNameAlert.setMessage("Do Enter the File Name");
                        noNameAlert.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                        noNameAlert.show();
                        return;
                    }
                    String fName = String.valueOf(NewFileActivity.this.saveFileName) + NewFileActivity.EXTENSION;
                    File outputDir = new File(NewFileActivity.OUTPUT_PATH);
                    if (!outputDir.exists()) {
                        outputDir.mkdirs();
                    }
                    if (NewFileActivity.this.fileExists(String.valueOf(NewFileActivity.this.saveFileName) + NewFileActivity.EXTENSION, Environment.getExternalStoragePublicDirectory(NewFileActivity.FOLDER).list())) {
                        NewFileActivity.this.showFileExistsDialog();
                    } else {
                        NewFileActivity.this.saveFile(fName, NewFileActivity.this.editText.getText().toString());
                    }
                } else {
                    Toast.makeText(NewFileActivity.this, "No SD Card", 0).show();
                }
            }
        });
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_main);
        AdWhirlLayout awl = new AdWhirlLayout(this, getString(R.string.adwhirl_key));
        RelativeLayout.LayoutParams awllp = new RelativeLayout.LayoutParams(-1, 53);
        awllp.addRule(12);
        rl.addView(awl, awllp);
        rl.invalidate();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.newfilemenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.clear /*2131099664*/:
                this.editText.setText("");
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: private */
    public boolean fileExists(String targetFileName, String[] existingFiles) {
        for (String file : existingFiles) {
            if (file.equalsIgnoreCase(targetFileName)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void showFileExistsDialog() {
        AlertDialog.Builder alertAbout = new AlertDialog.Builder(this);
        alertAbout.setTitle("File Exists");
        alertAbout.setMessage("Do change the File Name");
        alertAbout.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        alertAbout.show();
    }

    /* access modifiers changed from: private */
    public void saveFile(String fileName2, String content) {
        try {
            FileOutputStream fos = new FileOutputStream(new File("/sdcard/my_notepad/" + fileName2));
            fos.write(content.getBytes());
            fos.flush();
            fos.close();
            Toast.makeText(this, "Text Saved!", 0).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
