package com.mag.notes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

public class SplashActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.splash);
        new CountDownTimer(1500, 500) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                SplashActivity.this.startActivity(new Intent(SplashActivity.this, NotesActivity.class));
                SplashActivity.this.finish();
            }
        }.start();
    }
}
