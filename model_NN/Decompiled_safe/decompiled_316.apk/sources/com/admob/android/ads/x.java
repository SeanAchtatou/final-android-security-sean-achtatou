package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.admob.android.ads.InterstitialAd;

/* compiled from: AdMobButtonView */
public final class x extends RelativeLayout {
    private int a;
    private int b;
    private ImageView c;
    private float d = getResources().getDisplayMetrics().density;

    public x(Context context, View view, int i, int i2, Bitmap bitmap) {
        super(context);
        this.b = i;
        this.a = i2;
        setClickable(true);
        setFocusable(true);
        this.c = new ImageView(context);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
        bitmapDrawable.setBounds(0, 0, (int) (((float) i) * this.d), (int) (((float) i2) * this.d));
        this.c.setImageDrawable(bitmapDrawable);
        this.c.setVisibility(4);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (((float) i) * this.d), (int) (((float) i2) * this.d));
        layoutParams.addRule(13);
        addView(view, layoutParams);
        addView(this.c, layoutParams);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "dispatchTouchEvent: action=" + action + " x=" + motionEvent.getX() + " y=" + motionEvent.getY());
        }
        if (action == 0) {
            a(true);
        } else if (action == 2) {
            a(new Rect(0, 0, (int) (((float) this.b) * this.d), (int) (((float) this.a) * this.d)).contains((int) motionEvent.getX(), (int) motionEvent.getY()));
        } else if (action == 1) {
            a(false);
        } else if (action == 3) {
            a(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    private void a(boolean z) {
        if (z) {
            this.c.setVisibility(0);
        } else {
            this.c.setVisibility(4);
        }
    }
}
