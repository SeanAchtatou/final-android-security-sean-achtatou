package com.aetrion.flickr.commons;

import java.util.Date;

public class Institution {
    private static final long serialVersionUID = 12;
    Date dateLaunch;
    String flickrUrl;
    String id;
    String licenseUrl;
    String name;
    String siteUrl;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public Date getDateLaunch() {
        return this.dateLaunch;
    }

    public void setDateLaunch(Date dateLaunch2) {
        this.dateLaunch = dateLaunch2;
    }

    public void setDateLaunch(long date) {
        setDateLaunch(new Date(date));
    }

    public void setDateLaunch(String date) {
        if (date != null && !"".equals(date)) {
            setDateLaunch(Long.parseLong(date) * 1000);
        }
    }

    public String getSiteUrl() {
        return this.siteUrl;
    }

    public void setSiteUrl(String siteUrl2) {
        this.siteUrl = siteUrl2;
    }

    public String getLicenseUrl() {
        return this.licenseUrl;
    }

    public void setLicenseUrl(String licenseUrl2) {
        this.licenseUrl = licenseUrl2;
    }

    public String getFlickrUrl() {
        return this.flickrUrl;
    }

    public void setFlickrUrl(String flickrUrl2) {
        this.flickrUrl = flickrUrl2;
    }
}
