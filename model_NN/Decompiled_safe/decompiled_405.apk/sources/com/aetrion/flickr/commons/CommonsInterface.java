package com.aetrion.flickr.commons;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CommonsInterface {
    public static final String METHOD_GET_INSTITUTIONS = "flickr.commons.getInstitutions";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public CommonsInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public ArrayList getInstitutions() throws FlickrException, IOException, SAXException {
        ArrayList institutions = new ArrayList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_INSTITUTIONS));
        parameters.add(new Parameter("api_key", this.apiKey));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList mNodes = response.getPayload().getElementsByTagName("institution");
        for (int i = 0; i < mNodes.getLength(); i++) {
            institutions.add(parseInstitution((Element) mNodes.item(i)));
        }
        return institutions;
    }

    private Institution parseInstitution(Element mElement) {
        Institution inst = new Institution();
        inst.setId(mElement.getAttribute("nsid"));
        inst.setDateLaunch(mElement.getAttribute("date_launch"));
        inst.setName(XMLUtilities.getChildValue(mElement, "name"));
        Element element = (Element) mElement.getElementsByTagName("urlss").item(0);
        NodeList urlNodes = mElement.getElementsByTagName("url");
        for (int i = 0; i < urlNodes.getLength(); i++) {
            Element urlElement = (Element) urlNodes.item(i);
            if (urlElement.getAttribute("type").equals("site")) {
                inst.setSiteUrl(XMLUtilities.getValue(urlElement));
            } else if (urlElement.getAttribute("type").equals(Extras.LICENSE)) {
                inst.setLicenseUrl(XMLUtilities.getValue(urlElement));
            } else if (urlElement.getAttribute("type").equals("flickr")) {
                inst.setFlickrUrl(XMLUtilities.getValue(urlElement));
            }
        }
        return inst;
    }
}
