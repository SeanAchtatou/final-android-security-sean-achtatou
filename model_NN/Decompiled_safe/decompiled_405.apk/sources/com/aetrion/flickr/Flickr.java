package com.aetrion.flickr;

import com.aetrion.flickr.activity.ActivityInterface;
import com.aetrion.flickr.auth.Auth;
import com.aetrion.flickr.auth.AuthInterface;
import com.aetrion.flickr.blogs.BlogsInterface;
import com.aetrion.flickr.commons.CommonsInterface;
import com.aetrion.flickr.contacts.ContactsInterface;
import com.aetrion.flickr.favorites.FavoritesInterface;
import com.aetrion.flickr.groups.GroupsInterface;
import com.aetrion.flickr.groups.members.MembersInterface;
import com.aetrion.flickr.groups.pools.PoolsInterface;
import com.aetrion.flickr.interestingness.InterestingnessInterface;
import com.aetrion.flickr.machinetags.MachinetagsInterface;
import com.aetrion.flickr.panda.PandaInterface;
import com.aetrion.flickr.people.PeopleInterface;
import com.aetrion.flickr.photos.PhotosInterface;
import com.aetrion.flickr.photos.comments.CommentsInterface;
import com.aetrion.flickr.photos.geo.GeoInterface;
import com.aetrion.flickr.photos.licenses.LicensesInterface;
import com.aetrion.flickr.photos.notes.NotesInterface;
import com.aetrion.flickr.photos.transform.TransformInterface;
import com.aetrion.flickr.photos.upload.UploadInterface;
import com.aetrion.flickr.photosets.PhotosetsInterface;
import com.aetrion.flickr.photosets.comments.PhotosetsCommentsInterface;
import com.aetrion.flickr.places.PlacesInterface;
import com.aetrion.flickr.prefs.PrefsInterface;
import com.aetrion.flickr.reflection.ReflectionInterface;
import com.aetrion.flickr.tags.TagsInterface;
import com.aetrion.flickr.test.TestInterface;
import com.aetrion.flickr.uploader.Uploader;
import com.aetrion.flickr.urls.UrlsInterface;
import javax.xml.parsers.ParserConfigurationException;

public class Flickr {
    public static final int ACCURACY_CITY = 11;
    public static final int ACCURACY_COUNTRY = 3;
    public static final int ACCURACY_REGION = 6;
    public static final int ACCURACY_STREET = 16;
    public static final int ACCURACY_WORLD = 1;
    public static final String CONTENTTYPE_OTHER = "3";
    public static final String CONTENTTYPE_PHOTO = "1";
    public static final String CONTENTTYPE_SCREENSHOT = "2";
    public static final String DEFAULT_HOST = "api.flickr.com";
    public static final int PRIVACY_LEVEL_FAMILY = 3;
    public static final int PRIVACY_LEVEL_FRIENDS = 2;
    public static final int PRIVACY_LEVEL_FRIENDS_FAMILY = 4;
    public static final int PRIVACY_LEVEL_NO_FILTER = 0;
    public static final int PRIVACY_LEVEL_PRIVATE = 5;
    public static final int PRIVACY_LEVEL_PUBLIC = 1;
    public static final String SAFETYLEVEL_MODERATE = "2";
    public static final String SAFETYLEVEL_RESTRICTED = "3";
    public static final String SAFETYLEVEL_SAFE = "1";
    public static boolean debugRequest = false;
    public static boolean debugStream = false;
    public static boolean tracing = false;
    private ActivityInterface activityInterface;
    private String apiKey;
    private Auth auth;
    private AuthInterface authInterface;
    private BlogsInterface blogsInterface;
    private CommentsInterface commentsInterface;
    private CommonsInterface commonsInterface;
    private ContactsInterface contactsInterface;
    private FavoritesInterface favoritesInterface;
    private GeoInterface geoInterface;
    private GroupsInterface groupsInterface;
    private InterestingnessInterface interestingnessInterface;
    private LicensesInterface licensesInterface;
    private MachinetagsInterface machinetagsInterface;
    private MembersInterface membersInterface;
    private NotesInterface notesInterface;
    private PandaInterface pandaInterface;
    private PeopleInterface peopleInterface;
    private PhotosInterface photosInterface;
    private PhotosetsCommentsInterface photosetsCommentsInterface;
    private PhotosetsInterface photosetsInterface;
    private PlacesInterface placesInterface;
    private PoolsInterface poolsInterface;
    private PrefsInterface prefsInterface;
    private ReflectionInterface reflectionInterface;
    private String sharedSecret;
    private TagsInterface tagsInterface;
    private TestInterface testInterface;
    private TransformInterface transformInterface;
    private Transport transport;
    private UploadInterface uploadInterface;
    private Uploader uploader;
    private UrlsInterface urlsInterface;

    public Flickr(String apiKey2) {
        setApiKey(apiKey2);
        try {
            setTransport(new REST(DEFAULT_HOST));
        } catch (ParserConfigurationException e) {
            ParserConfigurationException e2 = e;
            throw new RuntimeException(e2.getMessage(), e2);
        }
    }

    public Flickr(String apiKey2, Transport transport2) {
        setApiKey(apiKey2);
        setTransport(transport2);
    }

    public Flickr(String apiKey2, String sharedSecret2, Transport transport2) {
        setApiKey(apiKey2);
        setSharedSecret(sharedSecret2);
        setTransport(transport2);
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(String apiKey2) {
        if (apiKey2 == null) {
            throw new IllegalArgumentException("API key must not be null");
        }
        this.apiKey = apiKey2;
    }

    public void setAuth(Auth auth2) {
        this.auth = auth2;
    }

    public Auth getAuth() {
        return this.auth;
    }

    public String getSharedSecret() {
        return this.sharedSecret;
    }

    public void setSharedSecret(String sharedSecret2) {
        if (sharedSecret2 == null) {
            throw new IllegalArgumentException("Shared-Secret must not be null");
        }
        this.sharedSecret = sharedSecret2;
    }

    public Transport getTransport() {
        return this.transport;
    }

    public void setTransport(Transport transport2) {
        if (transport2 == null) {
            throw new IllegalArgumentException("Transport must not be null");
        }
        this.transport = transport2;
    }

    public AuthInterface getAuthInterface() {
        if (this.authInterface == null) {
            this.authInterface = new AuthInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.authInterface;
    }

    public ActivityInterface getActivityInterface() {
        if (this.activityInterface == null) {
            this.activityInterface = new ActivityInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.activityInterface;
    }

    public synchronized BlogsInterface getBlogsInterface() {
        if (this.blogsInterface == null) {
            this.blogsInterface = new BlogsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.blogsInterface;
    }

    public CommentsInterface getCommentsInterface() {
        if (this.commentsInterface == null) {
            this.commentsInterface = new CommentsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.commentsInterface;
    }

    public CommonsInterface getCommonsInterface() {
        if (this.commonsInterface == null) {
            this.commonsInterface = new CommonsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.commonsInterface;
    }

    public ContactsInterface getContactsInterface() {
        if (this.contactsInterface == null) {
            this.contactsInterface = new ContactsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.contactsInterface;
    }

    public FavoritesInterface getFavoritesInterface() {
        if (this.favoritesInterface == null) {
            this.favoritesInterface = new FavoritesInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.favoritesInterface;
    }

    public GeoInterface getGeoInterface() {
        if (this.geoInterface == null) {
            this.geoInterface = new GeoInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.geoInterface;
    }

    public GroupsInterface getGroupsInterface() {
        if (this.groupsInterface == null) {
            this.groupsInterface = new GroupsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.groupsInterface;
    }

    public synchronized InterestingnessInterface getInterestingnessInterface() {
        if (this.interestingnessInterface == null) {
            this.interestingnessInterface = new InterestingnessInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.interestingnessInterface;
    }

    public LicensesInterface getLicensesInterface() {
        if (this.licensesInterface == null) {
            this.licensesInterface = new LicensesInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.licensesInterface;
    }

    public MachinetagsInterface getMachinetagsInterface() {
        if (this.machinetagsInterface == null) {
            this.machinetagsInterface = new MachinetagsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.machinetagsInterface;
    }

    public MembersInterface getMembersInterface() {
        if (this.membersInterface == null) {
            this.membersInterface = new MembersInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.membersInterface;
    }

    public NotesInterface getNotesInterface() {
        if (this.notesInterface == null) {
            this.notesInterface = new NotesInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.notesInterface;
    }

    public PandaInterface getPandaInterface() {
        if (this.pandaInterface == null) {
            this.pandaInterface = new PandaInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.pandaInterface;
    }

    public PoolsInterface getPoolsInterface() {
        if (this.poolsInterface == null) {
            this.poolsInterface = new PoolsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.poolsInterface;
    }

    public PeopleInterface getPeopleInterface() {
        if (this.peopleInterface == null) {
            this.peopleInterface = new PeopleInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.peopleInterface;
    }

    public PhotosInterface getPhotosInterface() {
        if (this.photosInterface == null) {
            this.photosInterface = new PhotosInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.photosInterface;
    }

    public PhotosetsCommentsInterface getPhotosetsCommentsInterface() {
        if (this.photosetsCommentsInterface == null) {
            this.photosetsCommentsInterface = new PhotosetsCommentsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.photosetsCommentsInterface;
    }

    public PhotosetsInterface getPhotosetsInterface() {
        if (this.photosetsInterface == null) {
            this.photosetsInterface = new PhotosetsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.photosetsInterface;
    }

    public PlacesInterface getPlacesInterface() {
        if (this.placesInterface == null) {
            this.placesInterface = new PlacesInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.placesInterface;
    }

    public PrefsInterface getPrefsInterface() {
        if (this.prefsInterface == null) {
            this.prefsInterface = new PrefsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.prefsInterface;
    }

    public ReflectionInterface getReflectionInterface() {
        if (this.reflectionInterface == null) {
            this.reflectionInterface = new ReflectionInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.reflectionInterface;
    }

    public TagsInterface getTagsInterface() {
        if (this.tagsInterface == null) {
            this.tagsInterface = new TagsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.tagsInterface;
    }

    public TestInterface getTestInterface() {
        if (this.testInterface == null) {
            this.testInterface = new TestInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.testInterface;
    }

    public TransformInterface getTransformInterface() {
        if (this.transformInterface == null) {
            this.transformInterface = new TransformInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.transformInterface;
    }

    public UploadInterface getUploadInterface() {
        if (this.uploadInterface == null) {
            this.uploadInterface = new UploadInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.uploadInterface;
    }

    public Uploader getUploader() {
        if (this.uploader == null) {
            this.uploader = new Uploader(this.apiKey, this.sharedSecret);
        }
        return this.uploader;
    }

    public UrlsInterface getUrlsInterface() {
        if (this.urlsInterface == null) {
            this.urlsInterface = new UrlsInterface(this.apiKey, this.sharedSecret, this.transport);
        }
        return this.urlsInterface;
    }
}
