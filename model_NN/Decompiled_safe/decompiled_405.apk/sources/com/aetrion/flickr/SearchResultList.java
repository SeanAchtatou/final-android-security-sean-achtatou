package com.aetrion.flickr;

import java.util.ArrayList;

public class SearchResultList extends ArrayList {
    private static final long serialVersionUID = -7962319033867024935L;
    private int page;
    private int pages;
    private int perPage;
    private int total;

    public int getPage() {
        return this.page;
    }

    public void setPage(int page2) {
        this.page = page2;
    }

    public void setPage(String page2) {
        if (page2 != null && page2.length() != 0) {
            setPage(Integer.parseInt(page2));
        }
    }

    public int getPages() {
        return this.pages;
    }

    public void setPages(int pages2) {
        this.pages = pages2;
    }

    public void setPages(String pages2) {
        if (pages2 != null && pages2.length() != 0) {
            setPages(Integer.parseInt(pages2));
        }
    }

    public int getPerPage() {
        return this.perPage;
    }

    public void setPerPage(int perPage2) {
        this.perPage = perPage2;
    }

    public void setPerPage(String perPage2) {
        if (perPage2 != null && perPage2.length() != 0) {
            setPerPage(Integer.parseInt(perPage2));
        }
    }

    public int getTotal() {
        return this.total;
    }

    public void setTotal(int total2) {
        this.total = total2;
    }

    public void setTotal(String total2) {
        if (total2 != null && total2.length() != 0) {
            setTotal(Integer.parseInt(total2));
        }
    }
}
