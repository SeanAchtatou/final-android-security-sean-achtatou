package com.aetrion.flickr.machinetags;

public class Pair {
    private static final long serialVersionUID = 12;
    String namespace;
    String predicate;
    int usage;

    public String getNamespace() {
        return this.namespace;
    }

    public void setNamespace(String namespace2) {
        this.namespace = namespace2;
    }

    public String getPredicate() {
        return this.predicate;
    }

    public void setPredicate(String predicate2) {
        this.predicate = predicate2;
    }

    public int getUsage() {
        return this.usage;
    }

    public void setUsage(String predicates) {
        try {
            setUsage(Integer.parseInt(predicates));
        } catch (NumberFormatException e) {
        }
    }

    public void setUsage(int usage2) {
        this.usage = usage2;
    }

    public String getValue() {
        return this.namespace + ":" + this.predicate;
    }
}
