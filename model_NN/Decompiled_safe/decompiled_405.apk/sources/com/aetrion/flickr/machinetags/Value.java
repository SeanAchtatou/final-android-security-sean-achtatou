package com.aetrion.flickr.machinetags;

import java.util.Date;

public class Value {
    private static final long serialVersionUID = 12;
    Date firstAdded;
    Date lastAdded;
    String namespace;
    String predicate;
    int usage;
    String value;

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public int getUsage() {
        return this.usage;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public void setNamespace(String namespace2) {
        this.namespace = namespace2;
    }

    public String getPredicate() {
        return this.predicate;
    }

    public void setPredicate(String predicate2) {
        this.predicate = predicate2;
    }

    public void setFirstAdded(Date date) {
        this.firstAdded = date;
    }

    public void setFirstAdded(long datePosted) {
        setFirstAdded(new Date(datePosted));
    }

    public void setFirstAdded(String timestamp) {
        if (timestamp != null && !"".equals(timestamp)) {
            setFirstAdded(Long.parseLong(timestamp) * 1000);
        }
    }

    public void setLastAdded(Date date) {
        this.lastAdded = date;
    }

    public void setLastAdded(long date) {
        setLastAdded(new Date(date));
    }

    public void setLastAdded(String timestamp) {
        if (timestamp != null && !"".equals(timestamp)) {
            setLastAdded(Long.parseLong(timestamp) * 1000);
        }
    }

    public void setUsage(String predicates) {
        try {
            setUsage(Integer.parseInt(predicates));
        } catch (NumberFormatException e) {
        }
    }

    public void setUsage(int usage2) {
        this.usage = usage2;
    }
}
