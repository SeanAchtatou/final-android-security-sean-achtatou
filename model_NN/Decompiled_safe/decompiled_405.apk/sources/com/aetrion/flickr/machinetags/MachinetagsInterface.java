package com.aetrion.flickr.machinetags;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MachinetagsInterface {
    private static final String METHOD_GET_NAMESPACES = "flickr.machinetags.getNamespaces";
    private static final String METHOD_GET_PAIRS = "flickr.machinetags.getPairs";
    private static final String METHOD_GET_PREDICATES = "flickr.machinetags.getPredicates";
    private static final String METHOD_GET_RECENTVALUES = "flickr.machinetags.getRecentValues";
    private static final String METHOD_GET_VALUES = "flickr.machinetags.getValues";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public MachinetagsInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public NamespacesList getNamespaces(String predicate, int perPage, int page) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        NamespacesList nsList = new NamespacesList();
        parameters.add(new Parameter("method", METHOD_GET_NAMESPACES));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (predicate != null) {
            parameters.add(new Parameter("predicate", predicate));
        }
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", "" + perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", "" + page));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList nsNodes = response.getPayload().getElementsByTagName("namespace");
        nsList.setPage("1");
        nsList.setPages("1");
        nsList.setPerPage("" + nsNodes.getLength());
        nsList.setTotal("" + nsNodes.getLength());
        for (int i = 0; i < nsNodes.getLength(); i++) {
            nsList.add(parseNamespace((Element) nsNodes.item(i)));
        }
        return nsList;
    }

    public NamespacesList getPairs(String namespace, String predicate, int perPage, int page) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        NamespacesList nsList = new NamespacesList();
        parameters.add(new Parameter("method", METHOD_GET_PAIRS));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (namespace != null) {
            parameters.add(new Parameter("namespace", namespace));
        }
        if (predicate != null) {
            parameters.add(new Parameter("predicate", predicate));
        }
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", "" + perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", "" + page));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element nsElement = response.getPayload();
        NodeList nsNodes = nsElement.getElementsByTagName("pair");
        nsList.setPage(nsElement.getAttribute("page"));
        nsList.setPages(nsElement.getAttribute("pages"));
        nsList.setPerPage(nsElement.getAttribute("perPage"));
        nsList.setTotal("" + nsNodes.getLength());
        for (int i = 0; i < nsNodes.getLength(); i++) {
            nsList.add(parsePair((Element) nsNodes.item(i)));
        }
        return nsList;
    }

    public NamespacesList getPredicates(String namespace, int perPage, int page) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        NamespacesList nsList = new NamespacesList();
        parameters.add(new Parameter("method", METHOD_GET_PREDICATES));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (namespace != null) {
            parameters.add(new Parameter("namespace", namespace));
        }
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", "" + perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", "" + page));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element nsElement = response.getPayload();
        NodeList nsNodes = nsElement.getElementsByTagName("predicate");
        nsList.setPage(nsElement.getAttribute("page"));
        nsList.setPages(nsElement.getAttribute("pages"));
        nsList.setPerPage(nsElement.getAttribute("perPage"));
        nsList.setTotal("" + nsNodes.getLength());
        for (int i = 0; i < nsNodes.getLength(); i++) {
            nsList.add(parsePredicate((Element) nsNodes.item(i)));
        }
        return nsList;
    }

    public NamespacesList getValues(String namespace, String predicate, int perPage, int page) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        NamespacesList valuesList = new NamespacesList();
        parameters.add(new Parameter("method", METHOD_GET_VALUES));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (namespace != null) {
            parameters.add(new Parameter("namespace", namespace));
        }
        if (predicate != null) {
            parameters.add(new Parameter("predicate", predicate));
        }
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", "" + perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", "" + page));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element nsElement = response.getPayload();
        NodeList nsNodes = nsElement.getElementsByTagName("value");
        valuesList.setPage(nsElement.getAttribute("page"));
        valuesList.setPages(nsElement.getAttribute("pages"));
        valuesList.setPerPage(nsElement.getAttribute("perPage"));
        valuesList.setTotal("" + nsNodes.getLength());
        for (int i = 0; i < nsNodes.getLength(); i++) {
            valuesList.add(parseValue((Element) nsNodes.item(i)));
        }
        return valuesList;
    }

    public NamespacesList getRecentValues(String namespace, String predicate, Date addedSince) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        NamespacesList valuesList = new NamespacesList();
        parameters.add(new Parameter("method", METHOD_GET_RECENTVALUES));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (namespace != null) {
            parameters.add(new Parameter("namespace", namespace));
        }
        if (predicate != null) {
            parameters.add(new Parameter("predicate", predicate));
        }
        if (addedSince != null) {
            parameters.add(new Parameter("added_since", new Long(addedSince.getTime() / 1000)));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element nsElement = response.getPayload();
        NodeList nsNodes = nsElement.getElementsByTagName("value");
        valuesList.setPage(nsElement.getAttribute("page"));
        valuesList.setPages(nsElement.getAttribute("pages"));
        valuesList.setPerPage(nsElement.getAttribute("perPage"));
        valuesList.setTotal("" + nsNodes.getLength());
        for (int i = 0; i < nsNodes.getLength(); i++) {
            valuesList.add(parseValue((Element) nsNodes.item(i)));
        }
        return valuesList;
    }

    private Value parseValue(Element nsElement) {
        Value value = new Value();
        value.setUsage(nsElement.getAttribute("usage"));
        value.setNamespace(nsElement.getAttribute("namespace"));
        value.setPredicate(nsElement.getAttribute("predicate"));
        value.setFirstAdded(nsElement.getAttribute("first_added"));
        value.setLastAdded(nsElement.getAttribute("last_added"));
        value.setValue(XMLUtilities.getValue(nsElement));
        return value;
    }

    private Predicate parsePredicate(Element nsElement) {
        Predicate predicate = new Predicate();
        predicate.setUsage(nsElement.getAttribute("usage"));
        predicate.setNamespaces(nsElement.getAttribute("namespaces"));
        predicate.setValue(XMLUtilities.getValue(nsElement));
        return predicate;
    }

    private Namespace parseNamespace(Element nsElement) {
        Namespace ns = new Namespace();
        ns.setUsage(nsElement.getAttribute("usage"));
        ns.setPredicates(nsElement.getAttribute("predicates"));
        ns.setValue(XMLUtilities.getValue(nsElement));
        return ns;
    }

    private Pair parsePair(Element nsElement) {
        Pair pair = new Pair();
        pair.setUsage(nsElement.getAttribute("usage"));
        pair.setNamespace(nsElement.getAttribute("namespace"));
        pair.setPredicate(nsElement.getAttribute("predicate"));
        return pair;
    }
}
