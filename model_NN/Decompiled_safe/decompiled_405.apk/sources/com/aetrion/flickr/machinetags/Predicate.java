package com.aetrion.flickr.machinetags;

public class Predicate {
    private static final long serialVersionUID = 12;
    int namespaces;
    int usage;
    String value;

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public int getUsage() {
        return this.usage;
    }

    public void setUsage(String predicates) {
        try {
            setUsage(Integer.parseInt(predicates));
        } catch (NumberFormatException e) {
        }
    }

    public void setUsage(int usage2) {
        this.usage = usage2;
    }

    public int getNamespaces() {
        return this.namespaces;
    }

    public void setNamespaces(String namespaces2) {
        try {
            setNamespaces(Integer.parseInt(namespaces2));
        } catch (NumberFormatException e) {
        }
    }

    public void setNamespaces(int namespaces2) {
        this.namespaces = namespaces2;
    }
}
