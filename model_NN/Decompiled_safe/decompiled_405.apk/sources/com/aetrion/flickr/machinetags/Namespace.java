package com.aetrion.flickr.machinetags;

public class Namespace {
    private static final long serialVersionUID = 12;
    private int predicates;
    private int usage;
    private String value;

    public int getUsage() {
        return this.usage;
    }

    public void setUsage(String usage2) {
        try {
            setUsage(Integer.parseInt(usage2));
        } catch (NumberFormatException e) {
        }
    }

    public void setUsage(int usage2) {
        this.usage = usage2;
    }

    public int getPredicates() {
        return this.predicates;
    }

    public void setPredicates(String predicates2) {
        try {
            setPredicates(Integer.parseInt(predicates2));
        } catch (NumberFormatException e) {
        }
    }

    public void setPredicates(int predicates2) {
        this.predicates = predicates2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }
}
