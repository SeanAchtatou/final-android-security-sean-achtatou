package com.aetrion.flickr;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class ProxyAuthenticator extends Authenticator {
    String passWord = "";
    String userName = "";

    public ProxyAuthenticator(String userName2, String passWord2) {
        this.userName = userName2;
        this.passWord = passWord2;
    }

    /* access modifiers changed from: protected */
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(this.userName, this.passWord.toCharArray());
    }
}
