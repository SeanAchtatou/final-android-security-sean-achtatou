package com.aetrion.flickr;

import java.util.Comparator;

public class ParameterAlphaComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        return ((Parameter) o1).getName().compareTo(((Parameter) o2).getName());
    }
}
