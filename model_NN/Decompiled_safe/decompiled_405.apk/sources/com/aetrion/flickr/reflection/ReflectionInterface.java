package com.aetrion.flickr.reflection;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReflectionInterface {
    public static final String METHOD_GET_METHODS = "flickr.reflection.getMethods";
    public static final String METHOD_GET_METHOD_INFO = "flickr.reflection.getMethodInfo";
    private String apiKey;
    private String sharedSecret;
    private Transport transport;

    public ReflectionInterface(String apiKey2, String sharedSecret2, Transport transport2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transport = transport2;
    }

    public Method getMethodInfo(String methodName) throws IOException, SAXException, FlickrException {
        Element child;
        Element child2;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Parameter("method", METHOD_GET_METHOD_INFO));
        arrayList.add(new Parameter("api_key", this.apiKey));
        arrayList.add(new Parameter("method_name", methodName));
        arrayList.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, arrayList)));
        Response response = this.transport.get(this.transport.getPath(), arrayList);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element methodElement = response.getPayload();
        Method method = new Method();
        method.setName(methodElement.getAttribute("name"));
        method.setNeedsLogin("1".equals(methodElement.getAttribute("needslogin")));
        method.setNeedsSigning("1".equals(methodElement.getAttribute("needssigning")));
        String requiredPermsStr = methodElement.getAttribute("requiredperms");
        if (requiredPermsStr != null && requiredPermsStr.length() > 0) {
            try {
                method.setRequiredPerms(Integer.parseInt(requiredPermsStr));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        method.setDescription(XMLUtilities.getChildValue(methodElement, "description"));
        method.setResponse(XMLUtilities.getChildValue(methodElement, "response"));
        method.setExplanation(XMLUtilities.getChildValue(methodElement, "explanation"));
        ArrayList arrayList2 = new ArrayList();
        Element argumentsElement = XMLUtilities.getChild(methodElement, "arguments");
        if (argumentsElement == null && (child2 = XMLUtilities.getChild((Element) methodElement.getParentNode(), "arguments")) != null) {
            argumentsElement = child2;
        }
        NodeList argumentElements = argumentsElement.getElementsByTagName("argument");
        for (int i = 0; i < argumentElements.getLength(); i++) {
            Argument argument = new Argument();
            Element argumentElement = (Element) argumentElements.item(i);
            argument.setName(argumentElement.getAttribute("name"));
            argument.setOptional("1".equals(argumentElement.getAttribute("optional")));
            argument.setDescription(XMLUtilities.getValue(argumentElement));
            arrayList2.add(argument);
        }
        method.setArguments(arrayList2);
        Element errorsElement = XMLUtilities.getChild(methodElement, "errors");
        if (errorsElement == null && (child = XMLUtilities.getChild((Element) methodElement.getParentNode(), "errors")) != null) {
            errorsElement = child;
        }
        ArrayList arrayList3 = new ArrayList();
        NodeList errorElements = errorsElement.getElementsByTagName("error");
        for (int i2 = 0; i2 < errorElements.getLength(); i2++) {
            Error error = new Error();
            Element errorElement = (Element) errorElements.item(i2);
            error.setCode(errorElement.getAttribute("code"));
            error.setMessage(errorElement.getAttribute("message"));
            error.setExplaination(XMLUtilities.getValue(errorElement));
            arrayList3.add(error);
        }
        method.setErrors(arrayList3);
        return method;
    }

    public Collection getMethods() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_METHODS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.get(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element methodsElement = response.getPayload();
        List methods = new ArrayList();
        NodeList methodElements = methodsElement.getElementsByTagName("method");
        for (int i = 0; i < methodElements.getLength(); i++) {
            methods.add(XMLUtilities.getValue((Element) methodElements.item(i)));
        }
        return methods;
    }
}
