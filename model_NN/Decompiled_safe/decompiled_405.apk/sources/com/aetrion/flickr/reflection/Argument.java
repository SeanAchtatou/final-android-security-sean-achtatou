package com.aetrion.flickr.reflection;

public class Argument {
    private String description;
    private String name;
    private boolean optional;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public boolean isOptional() {
        return this.optional;
    }

    public void setOptional(boolean optional2) {
        this.optional = optional2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }
}
