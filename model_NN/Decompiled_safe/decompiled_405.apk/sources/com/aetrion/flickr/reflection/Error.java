package com.aetrion.flickr.reflection;

public class Error {
    private int code;
    private String explaination;
    private String message;

    public int getCode() {
        return this.code;
    }

    public void setCode(int code2) {
        this.code = code2;
    }

    public void setCode(String code2) {
        setCode(Integer.parseInt(code2));
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public String getExplaination() {
        return this.explaination;
    }

    public void setExplaination(String explaination2) {
        this.explaination = explaination2;
    }
}
