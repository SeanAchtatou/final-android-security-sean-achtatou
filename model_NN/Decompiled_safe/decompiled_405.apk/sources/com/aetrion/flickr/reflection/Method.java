package com.aetrion.flickr.reflection;

import java.util.ArrayList;
import java.util.Collection;

public class Method {
    public static final int READ_PERMISSION = 1;
    public static final int WRITE_PERMISSION = 2;
    private Collection arguments;
    private String description;
    private Collection errors;
    private String explanation;
    private String name;
    private boolean needsLogin;
    private boolean needsSigning;
    private int requiredPerms;
    private String response;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public boolean needsLogin() {
        return this.needsLogin;
    }

    public void setNeedsLogin(boolean needsLogin2) {
        this.needsLogin = needsLogin2;
    }

    public boolean needsSigning() {
        return this.needsSigning;
    }

    public void setNeedsSigning(boolean needsSigning2) {
        this.needsSigning = needsSigning2;
    }

    public int getRequiredPerms() {
        return this.requiredPerms;
    }

    public void setRequiredPerms(int reqiredPerms) {
        this.requiredPerms = reqiredPerms;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response2) {
        this.response = response2;
    }

    public String getExplanation() {
        return this.explanation;
    }

    public void setExplanation(String explanation2) {
        this.explanation = explanation2;
    }

    public Collection getArguments() {
        if (this.arguments == null) {
            this.arguments = new ArrayList();
        }
        return this.arguments;
    }

    public void setArguments(Collection arguments2) {
        this.arguments = arguments2;
    }

    public Collection getErrors() {
        if (this.errors == null) {
            this.errors = new ArrayList();
        }
        return this.errors;
    }

    public void setErrors(Collection errors2) {
        this.errors = errors2;
    }
}
