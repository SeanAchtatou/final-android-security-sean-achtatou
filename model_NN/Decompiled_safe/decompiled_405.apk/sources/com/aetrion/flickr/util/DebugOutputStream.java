package com.aetrion.flickr.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DebugOutputStream extends FilterOutputStream {
    private OutputStream debugOut;

    public DebugOutputStream(OutputStream out, OutputStream debugOut2) {
        super(out);
        this.debugOut = debugOut2;
    }

    public void write(int b) throws IOException {
        super.write(b);
        this.debugOut.write((char) b);
    }

    public void write(byte[] b) throws IOException {
        super.write(b);
        this.debugOut.write(b);
    }

    public void write(byte[] b, int offset, int length) throws IOException {
        super.write(b, offset, length);
        this.debugOut.write(b, offset, length);
    }
}
