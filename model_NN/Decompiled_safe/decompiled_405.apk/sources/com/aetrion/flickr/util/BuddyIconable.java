package com.aetrion.flickr.util;

public interface BuddyIconable {
    String getBuddyIconUrl();

    int getIconFarm();

    int getIconServer();

    void setIconFarm(int i);

    void setIconFarm(String str);

    void setIconServer(int i);

    void setIconServer(String str);
}
