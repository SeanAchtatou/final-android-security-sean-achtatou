package com.aetrion.flickr.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DebugInputStream extends FilterInputStream {
    private OutputStream debugOut;

    public DebugInputStream(InputStream in, OutputStream debugOut2) {
        super(in);
        this.debugOut = debugOut2;
    }

    public int read() throws IOException {
        int c = super.read();
        this.debugOut.write((char) c);
        return c;
    }

    public int read(byte[] b) throws IOException {
        int readCount = super.read(b);
        for (int i = 0; i < readCount; i++) {
            this.debugOut.write((char) b[i]);
        }
        return readCount;
    }

    public int read(byte[] b, int offset, int length) throws IOException {
        int readCount = super.read(b, offset, length);
        int readTo = offset + readCount;
        for (int i = offset; i < readTo; i++) {
            this.debugOut.write((char) b[i]);
        }
        return readCount;
    }
}
