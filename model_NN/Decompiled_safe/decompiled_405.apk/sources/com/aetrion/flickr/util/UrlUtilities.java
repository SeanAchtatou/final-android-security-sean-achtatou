package com.aetrion.flickr.util;

import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.auth.AuthInterface;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

public class UrlUtilities {
    public static final String UTF8 = "UTF-8";

    public static URL buildUrl(String host, int port, String path, List parameters) throws MalformedURLException {
        StringBuffer buffer = new StringBuffer();
        buffer.append("http://");
        buffer.append(host);
        if (port > 0) {
            buffer.append(":");
            buffer.append(port);
        }
        if (path == null) {
            path = "/";
        }
        buffer.append(path);
        Iterator iter = parameters.iterator();
        if (iter.hasNext()) {
            buffer.append("?");
        }
        while (iter.hasNext()) {
            Parameter p = (Parameter) iter.next();
            buffer.append(p.getName());
            buffer.append("=");
            Object value = p.getValue();
            if (value != null) {
                String string = value.toString();
                try {
                    string = URLEncoder.encode(string, UTF8);
                } catch (UnsupportedEncodingException e) {
                }
                buffer.append(string);
            }
            if (iter.hasNext()) {
                buffer.append("&");
            }
        }
        return new URL(buffer.toString());
    }

    public static URL buildPostUrl(String host, int port, String path) throws MalformedURLException {
        StringBuffer buffer = new StringBuffer();
        buffer.append("http://");
        buffer.append(host);
        if (port > 0) {
            buffer.append(":");
            buffer.append(port);
        }
        if (path == null) {
            path = "/";
        }
        buffer.append(path);
        return new URL(buffer.toString());
    }

    private static String getMethod(List parameters) {
        Iterator iter = parameters.iterator();
        while (iter.hasNext()) {
            Parameter parameter = (Parameter) iter.next();
            if ("method".equals(parameter.getName())) {
                return String.valueOf(parameter.getValue());
            }
        }
        return null;
    }

    private static boolean ignoreMethod(String method) {
        if (method == null || !AuthInterface.METHOD_CHECK_TOKEN.equals(method)) {
            return false;
        }
        return true;
    }

    public static String createBuddyIconUrl(int iconFarm, int iconServer, String id) {
        if (iconServer > 0) {
            return "http://farm" + iconFarm + ".static.flickr.com/" + iconServer + "/buddyicons/" + id + ".jpg";
        }
        return "http://www.flickr.com/images/buddyicon.jpg";
    }
}
