package com.aetrion.flickr.util;

import com.aetrion.flickr.auth.Auth;
import com.aetrion.flickr.people.User;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class FileAuthStore implements AuthStore {
    private File authStoreDir;
    private Map auths = new HashMap();

    public FileAuthStore(File authStoreDir2) throws IOException {
        this.authStoreDir = authStoreDir2;
        if (!authStoreDir2.exists()) {
            authStoreDir2.mkdir();
        }
        if (!authStoreDir2.canRead()) {
            throw new IOException("Cannot read " + authStoreDir2.getCanonicalPath());
        }
        load();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.aetrion.flickr.auth.Auth} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void load() throws java.io.IOException {
        /*
            r7 = this;
            java.io.File r5 = r7.authStoreDir
            com.aetrion.flickr.util.FileAuthStore$AuthFilenameFilter r6 = new com.aetrion.flickr.util.FileAuthStore$AuthFilenameFilter
            r6.<init>()
            java.io.File[] r1 = r5.listFiles(r6)
            r4 = 0
        L_0x000c:
            int r5 = r1.length
            if (r4 >= r5) goto L_0x0046
            r5 = r1[r4]
            boolean r5 = r5.isFile()
            if (r5 == 0) goto L_0x0043
            r5 = r1[r4]
            boolean r5 = r5.canRead()
            if (r5 == 0) goto L_0x0043
            java.io.ObjectInputStream r3 = new java.io.ObjectInputStream
            java.io.FileInputStream r5 = new java.io.FileInputStream
            r6 = r1[r4]
            r5.<init>(r6)
            r3.<init>(r5)
            r2 = 0
            java.lang.Object r5 = r3.readObject()     // Catch:{ ClassCastException -> 0x0049, ClassNotFoundException -> 0x0047 }
            r0 = r5
            com.aetrion.flickr.auth.Auth r0 = (com.aetrion.flickr.auth.Auth) r0     // Catch:{ ClassCastException -> 0x0049, ClassNotFoundException -> 0x0047 }
            r2 = r0
        L_0x0034:
            if (r2 == 0) goto L_0x0043
            java.util.Map r5 = r7.auths
            com.aetrion.flickr.people.User r6 = r2.getUser()
            java.lang.String r6 = r6.getId()
            r5.put(r6, r2)
        L_0x0043:
            int r4 = r4 + 1
            goto L_0x000c
        L_0x0046:
            return
        L_0x0047:
            r5 = move-exception
            goto L_0x0034
        L_0x0049:
            r5 = move-exception
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aetrion.flickr.util.FileAuthStore.load():void");
    }

    public void store(Auth token) throws IOException {
        this.auths.put(token.getUser().getId(), token);
        File outFile = new File(this.authStoreDir, token.getUser().getId() + ".auth");
        outFile.createNewFile();
        ObjectOutputStream authStream = new ObjectOutputStream(new FileOutputStream(outFile));
        authStream.writeObject(token);
        authStream.flush();
        authStream.close();
    }

    public Auth retrieve(String nsid) {
        return (Auth) this.auths.get(nsid);
    }

    public Auth[] retrieveAll() {
        return (Auth[]) this.auths.values().toArray(new Auth[this.auths.size()]);
    }

    public void clearAll() {
        this.auths.clear();
        File[] auths2 = this.authStoreDir.listFiles(new AuthFilenameFilter());
        for (File delete : auths2) {
            delete.delete();
        }
    }

    public void clear(String nsid) {
        this.auths.remove(nsid);
        File auth = new File(this.authStoreDir, nsid + ".auth");
        if (auth.exists()) {
            auth.delete();
        }
    }

    static class AuthFilenameFilter implements FilenameFilter {
        private static final String suffix = ".auth";

        AuthFilenameFilter() {
        }

        public boolean accept(File dir, String name) {
            if (name.endsWith(suffix)) {
                return true;
            }
            return false;
        }
    }

    public static void main(String[] args) throws Exception {
        FileAuthStore fas = new FileAuthStore(new File(System.getProperty("user.home") + File.separatorChar + "flickrauth"));
        Auth a = new Auth();
        User u = new User();
        u.setId("THISISMYNSID");
        a.setUser(u);
        fas.store(a);
        System.out.println(new FileAuthStore(new File(System.getProperty("user.home") + File.separatorChar + "flickrauth")).retrieve("THISISMYNSID").getUser().getId());
    }
}
