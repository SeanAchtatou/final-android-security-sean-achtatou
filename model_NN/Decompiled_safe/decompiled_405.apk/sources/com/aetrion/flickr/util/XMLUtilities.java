package com.aetrion.flickr.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class XMLUtilities {
    private XMLUtilities() {
    }

    public static Collection getChildElements(Node node) {
        List elements = new ArrayList();
        NodeList nodes = node.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node childNode = nodes.item(i);
            if (childNode instanceof Element) {
                elements.add(childNode);
            }
        }
        return elements;
    }

    public static String getValue(Element element) {
        Node dataNode;
        if (element == null || (dataNode = element.getFirstChild()) == null) {
            return null;
        }
        return ((Text) dataNode).getData();
    }

    public static Element getChild(Element element, String name) {
        return (Element) element.getElementsByTagName(name).item(0);
    }

    public static String getChildValue(Element element, String name) {
        return getValue(getChild(element, name));
    }

    public static int getIntAttribute(Element el, String name) {
        String s = el.getAttribute(name);
        if (s == null || s.length() <= 0) {
            return 0;
        }
        return Integer.parseInt(s);
    }

    public static boolean getBooleanAttribute(Element el, String name) {
        String s = el.getAttribute(name);
        if (s == null || "0".equals(s)) {
            return false;
        }
        if ("1".equals(s)) {
            return true;
        }
        return new Boolean(s).booleanValue();
    }
}
