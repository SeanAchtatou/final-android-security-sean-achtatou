package com.aetrion.flickr.util;

import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

public class ImageUtilities {
    private static final int DEFAULT_IMAGE_TYPE = 1;

    public BufferedImage bufferImage(Image image) {
        return bufferImage(image, 1);
    }

    public BufferedImage bufferImage(Image image, int type) {
        BufferedImage bufferedImage = new BufferedImage(image.getWidth((ImageObserver) null), image.getHeight((ImageObserver) null), type);
        bufferedImage.createGraphics().drawImage(image, (AffineTransform) null, (ImageObserver) null);
        waitForImage(bufferedImage);
        return bufferedImage;
    }

    private void waitForImage(BufferedImage bufferedImage) {
        final ImageLoadStatus imageLoadStatus = new ImageLoadStatus();
        bufferedImage.getHeight(new ImageObserver() {
            public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
                if (infoflags != 32) {
                    return false;
                }
                imageLoadStatus.heightDone = true;
                return true;
            }
        });
        bufferedImage.getWidth(new ImageObserver() {
            public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
                if (infoflags != 32) {
                    return false;
                }
                imageLoadStatus.widthDone = true;
                return true;
            }
        });
        while (!imageLoadStatus.widthDone && !imageLoadStatus.heightDone) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
            }
        }
    }

    class ImageLoadStatus {
        public boolean heightDone = false;
        public boolean widthDone = false;

        ImageLoadStatus() {
        }
    }
}
