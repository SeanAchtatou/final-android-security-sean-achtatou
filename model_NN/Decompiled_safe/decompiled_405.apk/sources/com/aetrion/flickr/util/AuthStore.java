package com.aetrion.flickr.util;

import com.aetrion.flickr.auth.Auth;
import java.io.IOException;

public interface AuthStore {
    void clear(String str);

    void clearAll();

    Auth retrieve(String str);

    Auth[] retrieveAll();

    void store(Auth auth) throws IOException;
}
