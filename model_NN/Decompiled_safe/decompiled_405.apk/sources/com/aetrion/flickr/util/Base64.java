package com.aetrion.flickr.util;

public final class Base64 {
    private static final int BASELENGTH = 255;
    private static final int EIGHTBIT = 8;
    private static final int FOURBYTE = 4;
    private static final int LOOKUPLENGTH = 64;
    private static final byte PAD = 61;
    private static final int SIGN = -128;
    private static final int SIXBIT = 6;
    private static final int SIXTEENBIT = 16;
    private static final int TWENTYFOURBITGROUP = 24;
    private static byte[] base64Alphabet = new byte[BASELENGTH];
    private static final boolean fDebug = false;
    private static byte[] lookUpBase64Alphabet = new byte[64];

    static {
        for (int i = 0; i < BASELENGTH; i++) {
            base64Alphabet[i] = -1;
        }
        for (int i2 = 90; i2 >= 65; i2--) {
            base64Alphabet[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 122; i3 >= 97; i3--) {
            base64Alphabet[i3] = (byte) ((i3 - 97) + 26);
        }
        for (int i4 = 57; i4 >= 48; i4--) {
            base64Alphabet[i4] = (byte) ((i4 - 48) + 52);
        }
        base64Alphabet[43] = 62;
        base64Alphabet[47] = 63;
        for (int i5 = 0; i5 <= 25; i5++) {
            lookUpBase64Alphabet[i5] = (byte) (i5 + 65);
        }
        int i6 = 26;
        int j = 0;
        while (i6 <= 51) {
            lookUpBase64Alphabet[i6] = (byte) (j + 97);
            i6++;
            j++;
        }
        int i7 = 52;
        int j2 = 0;
        while (i7 <= 61) {
            lookUpBase64Alphabet[i7] = (byte) (j2 + 48);
            i7++;
            j2++;
        }
        lookUpBase64Alphabet[62] = 43;
        lookUpBase64Alphabet[63] = 47;
    }

    protected static boolean isWhiteSpace(byte octect) {
        return octect == 32 || octect == 13 || octect == 10 || octect == 9;
    }

    protected static boolean isPad(byte octect) {
        return octect == 61;
    }

    protected static boolean isData(byte octect) {
        return base64Alphabet[octect] != -1;
    }

    public static boolean isBase64(String isValidString) {
        if (isValidString == null) {
            return false;
        }
        return isArrayByteBase64(isValidString.getBytes());
    }

    public static boolean isBase64(byte octect) {
        return isWhiteSpace(octect) || isPad(octect) || isData(octect);
    }

    public static synchronized byte[] removeWhiteSpace(byte[] data) {
        byte[] bArr;
        int j;
        synchronized (Base64.class) {
            if (data == null) {
                bArr = null;
            } else {
                int newSize = 0;
                for (byte isWhiteSpace : data) {
                    if (!isWhiteSpace(isWhiteSpace)) {
                        newSize++;
                    }
                }
                if (newSize == len) {
                    bArr = data;
                } else {
                    byte[] arrayWithoutSpaces = new byte[newSize];
                    int i = 0;
                    int j2 = 0;
                    while (i < len) {
                        if (isWhiteSpace(data[i])) {
                            j = j2;
                        } else {
                            j = j2 + 1;
                            arrayWithoutSpaces[j2] = data[i];
                        }
                        i++;
                        j2 = j;
                    }
                    bArr = arrayWithoutSpaces;
                }
            }
        }
        return bArr;
    }

    public static synchronized boolean isArrayByteBase64(byte[] arrayOctect) {
        boolean z;
        synchronized (Base64.class) {
            z = getDecodedDataLength(arrayOctect) >= 0;
        }
        return z;
    }

    /* JADX INFO: Multiple debug info for r3v1 int: [D('b3' byte), D('dataIndex' int)] */
    /* JADX INFO: Multiple debug info for r4v1 int: [D('encodedIndex' int), D('dataIndex' int)] */
    /* JADX INFO: Multiple debug info for r19v3 byte: [D('b2' byte), D('binaryData' byte[])] */
    /* JADX INFO: Multiple debug info for r3v3 byte: [D('l' byte), D('dataIndex' int)] */
    /* JADX INFO: Multiple debug info for r19v6 byte: [D('binaryData' byte[]), D('b1' byte)] */
    public static synchronized byte[] encode(byte[] binaryData) {
        byte[] encodedData;
        byte val2;
        byte[] binaryData2;
        byte val3;
        synchronized (Base64.class) {
            if (binaryData == null) {
                binaryData2 = null;
            } else {
                int lengthDataBits = binaryData.length * 8;
                int fewerThan24bits = lengthDataBits % TWENTYFOURBITGROUP;
                int numberTriplets = lengthDataBits / TWENTYFOURBITGROUP;
                if (fewerThan24bits != 0) {
                    encodedData = new byte[((numberTriplets + 1) * 4)];
                } else {
                    encodedData = new byte[(numberTriplets * 4)];
                }
                byte k = 0;
                byte l = 0;
                byte b1 = 0;
                byte b2 = 0;
                int i = 0;
                while (i < numberTriplets) {
                    int dataIndex = i * 3;
                    b1 = binaryData[dataIndex];
                    b2 = binaryData[dataIndex + 1];
                    byte b3 = binaryData[dataIndex + 2];
                    l = (byte) (b2 & 15);
                    k = (byte) (b1 & 3);
                    int encodedIndex = i * 4;
                    byte val1 = (b1 & Byte.MIN_VALUE) == 0 ? (byte) (b1 >> 2) : (byte) ((b1 >> 2) ^ 192);
                    byte val22 = (b2 & Byte.MIN_VALUE) == 0 ? (byte) (b2 >> 4) : (byte) ((b2 >> 4) ^ 240);
                    if ((b3 & Byte.MIN_VALUE) == 0) {
                        val3 = (byte) (b3 >> 6);
                    } else {
                        val3 = (byte) ((b3 >> 6) ^ 252);
                    }
                    encodedData[encodedIndex] = lookUpBase64Alphabet[val1];
                    encodedData[encodedIndex + 1] = lookUpBase64Alphabet[val22 | (k << 4)];
                    encodedData[encodedIndex + 2] = lookUpBase64Alphabet[val3 | (l << 2)];
                    encodedData[encodedIndex + 3] = lookUpBase64Alphabet[b3 & 63];
                    i++;
                }
                int dataIndex2 = i * 3;
                int dataIndex3 = i * 4;
                if (fewerThan24bits == 8) {
                    byte b12 = binaryData[dataIndex2];
                    byte k2 = (byte) (b12 & 3);
                    encodedData[dataIndex3] = lookUpBase64Alphabet[(b12 & SIGN) == 0 ? (byte) (b12 >> 2) : (byte) ((b12 >> 2) ^ 192)];
                    encodedData[dataIndex3 + 1] = lookUpBase64Alphabet[k2 << 4];
                    encodedData[dataIndex3 + 2] = PAD;
                    encodedData[dataIndex3 + 3] = PAD;
                } else if (fewerThan24bits == 16) {
                    byte b13 = binaryData[dataIndex2];
                    byte b22 = binaryData[dataIndex2 + 1];
                    int dataIndex4 = (byte) (b22 & 15);
                    byte k3 = (byte) (b13 & 3);
                    byte val12 = (b13 & Byte.MIN_VALUE) == 0 ? (byte) (b13 >> 2) : (byte) ((b13 >> 2) ^ 192);
                    if ((b22 & SIGN) == 0) {
                        val2 = (byte) (b22 >> 4);
                    } else {
                        val2 = (byte) ((b22 >> 4) ^ 240);
                    }
                    encodedData[dataIndex3] = lookUpBase64Alphabet[val12];
                    encodedData[dataIndex3 + 1] = lookUpBase64Alphabet[val2 | (k3 << 4)];
                    encodedData[dataIndex3 + 2] = lookUpBase64Alphabet[dataIndex4 << 2];
                    encodedData[dataIndex3 + 3] = PAD;
                }
                binaryData2 = encodedData;
            }
        }
        return binaryData2;
    }

    /* JADX INFO: Multiple debug info for r2v1 int: [D('b2' byte), D('dataIndex' int)] */
    /* JADX INFO: Multiple debug info for r2v2 byte: [D('dataIndex' int), D('d2' byte)] */
    /* JADX INFO: Multiple debug info for r19v12 byte: [D('d1' byte), D('b1' byte)] */
    /* JADX INFO: Multiple debug info for r2v3 byte: [D('b2' byte), D('d2' byte)] */
    /* JADX INFO: Multiple debug info for r3v3 byte: [D('dataIndex' int), D('d3' byte)] */
    /* JADX INFO: Multiple debug info for r4v5 byte: [D('d4' byte), D('dataIndex' int)] */
    /* JADX INFO: Multiple debug info for r3v4 byte: [D('b3' byte), D('d3' byte)] */
    /* JADX INFO: Multiple debug info for r3v9 byte: [D('b3' byte), D('d3' byte)] */
    /* JADX INFO: Multiple debug info for r4v14 byte: [D('d4' byte), D('b4' byte)] */
    /* JADX INFO: Multiple debug info for r19v31 int: [D('encodedIndex' int), D('b1' byte)] */
    /* JADX INFO: Multiple debug info for r19v34 int: [D('b1' byte), D('dataIndex' int)] */
    public static synchronized byte[] decode(byte[] base64Data) {
        byte[] base64Data2;
        synchronized (Base64.class) {
            if (base64Data == null) {
                base64Data2 = null;
            } else {
                byte[] normalizedBase64Data = removeWhiteSpace(base64Data);
                if (normalizedBase64Data.length % 4 != 0) {
                    base64Data2 = null;
                } else {
                    int numberQuadruple = normalizedBase64Data.length / 4;
                    if (numberQuadruple == 0) {
                        base64Data2 = new byte[0];
                    } else {
                        byte d2 = 0;
                        byte d3 = 0;
                        byte d4 = 0;
                        int dataIndex = 0;
                        byte[] decodedData = new byte[(numberQuadruple * 3)];
                        int i = 0;
                        int i2 = 0;
                        while (i < numberQuadruple - 1) {
                            int dataIndex2 = dataIndex + 1;
                            byte d1 = normalizedBase64Data[dataIndex];
                            if (isData(d1) != 0) {
                                int dataIndex3 = dataIndex2 + 1;
                                d2 = normalizedBase64Data[dataIndex2];
                                if (isData(d2)) {
                                    dataIndex2 = dataIndex3 + 1;
                                    d3 = normalizedBase64Data[dataIndex3];
                                    if (isData(d3)) {
                                        dataIndex = dataIndex2 + 1;
                                        d4 = normalizedBase64Data[dataIndex2];
                                        if (isData(d4)) {
                                            byte b1 = base64Alphabet[d1];
                                            byte b2 = base64Alphabet[d2];
                                            byte b3 = base64Alphabet[d3];
                                            byte b4 = base64Alphabet[d4];
                                            int encodedIndex = i2 + 1;
                                            decodedData[i2] = (byte) ((b1 << 2) | (b2 >> 4));
                                            int encodedIndex2 = encodedIndex + 1;
                                            decodedData[encodedIndex] = (byte) (((b2 & 15) << 4) | ((b3 >> 2) & 15));
                                            decodedData[encodedIndex2] = (byte) ((b3 << 6) | b4);
                                            i++;
                                            i2 = encodedIndex2 + 1;
                                        }
                                    }
                                }
                            }
                            base64Data2 = null;
                        }
                        int dataIndex4 = dataIndex + 1;
                        byte d12 = normalizedBase64Data[dataIndex];
                        if (isData(d12) != 0) {
                            int dataIndex5 = dataIndex4 + 1;
                            byte d22 = normalizedBase64Data[dataIndex4];
                            if (isData(d22) != 0) {
                                byte b12 = base64Alphabet[d12];
                                byte d23 = base64Alphabet[d22];
                                int dataIndex6 = dataIndex5 + 1;
                                byte d32 = normalizedBase64Data[dataIndex5];
                                int i3 = dataIndex6 + 1;
                                byte d42 = normalizedBase64Data[dataIndex6];
                                if (isData(d32) != 0 && isData(d42)) {
                                    byte d33 = base64Alphabet[d32];
                                    byte b42 = base64Alphabet[d42];
                                    int encodedIndex3 = i2 + 1;
                                    decodedData[i2] = (byte) ((b12 << 2) | (d23 >> 4));
                                    int encodedIndex4 = encodedIndex3 + 1;
                                    decodedData[encodedIndex3] = (byte) (((d23 & 15) << 4) | ((d33 >> 2) & 15));
                                    int i4 = encodedIndex4 + 1;
                                    decodedData[encodedIndex4] = (byte) ((d33 << 6) | b42);
                                    base64Data2 = decodedData;
                                } else if (!isPad(d32) || !isPad(d42)) {
                                    if (isPad(d32) || isPad(d42) == 0) {
                                        base64Data2 = null;
                                    } else {
                                        byte d34 = base64Alphabet[d32];
                                        if ((d34 & 3) != 0) {
                                            base64Data2 = null;
                                        } else {
                                            byte[] tmp = new byte[((i * 3) + 2)];
                                            System.arraycopy(decodedData, 0, tmp, 0, i * 3);
                                            tmp[i2] = (byte) ((b12 << 2) | (d23 >> 4));
                                            tmp[i2 + 1] = (byte) (((d23 & 15) << 4) | ((d34 >> 2) & 15));
                                            base64Data2 = tmp;
                                        }
                                    }
                                } else if ((d23 & 15) != 0) {
                                    base64Data2 = null;
                                } else {
                                    byte[] tmp2 = new byte[((i * 3) + 1)];
                                    System.arraycopy(decodedData, 0, tmp2, 0, i * 3);
                                    tmp2[i2] = (byte) ((b12 << 2) | (d23 >> 4));
                                    base64Data2 = tmp2;
                                }
                            }
                        }
                        base64Data2 = null;
                    }
                }
            }
        }
        return base64Data2;
    }

    public static synchronized int getDecodedDataLength(byte[] base64Data) {
        int length;
        synchronized (Base64.class) {
            if (base64Data == null) {
                length = -1;
            } else if (base64Data.length == 0) {
                length = 0;
            } else {
                byte[] decodedData = decode(base64Data);
                length = decodedData == null ? -1 : decodedData.length;
            }
        }
        return length;
    }
}
