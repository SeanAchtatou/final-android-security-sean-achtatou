package com.aetrion.flickr.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Pattern;

public class StringUtilities {
    public static final Pattern getterPattern = Pattern.compile("^is|^get");

    private StringUtilities() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aetrion.flickr.util.StringUtilities.join(java.lang.String[], java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String[], java.lang.String, int]
     candidates:
      com.aetrion.flickr.util.StringUtilities.join(java.util.Collection, java.lang.String, boolean):java.lang.String
      com.aetrion.flickr.util.StringUtilities.join(java.lang.String[], java.lang.String, boolean):java.lang.String */
    public static String join(String[] s, String delimiter) {
        return join(s, delimiter, false);
    }

    public static String join(String[] s, String delimiter, boolean doQuote) {
        return join(Arrays.asList(s), delimiter, doQuote);
    }

    public static String join(Collection s, String delimiter, boolean doQuote) {
        StringBuffer buffer = new StringBuffer();
        Iterator iter = s.iterator();
        while (iter.hasNext()) {
            if (doQuote) {
                buffer.append("\"" + iter.next() + "\"");
            } else {
                buffer.append(iter.next());
            }
            if (iter.hasNext()) {
                buffer.append(delimiter);
            }
        }
        return buffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aetrion.flickr.util.StringUtilities.join(java.util.Collection, java.lang.String, boolean):java.lang.String
     arg types: [java.util.Collection, java.lang.String, int]
     candidates:
      com.aetrion.flickr.util.StringUtilities.join(java.lang.String[], java.lang.String, boolean):java.lang.String
      com.aetrion.flickr.util.StringUtilities.join(java.util.Collection, java.lang.String, boolean):java.lang.String */
    public static String join(Collection s, String delimiter) {
        return join(s, delimiter, false);
    }
}
