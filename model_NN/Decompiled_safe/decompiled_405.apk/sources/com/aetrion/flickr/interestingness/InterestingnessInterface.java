package com.aetrion.flickr.interestingness;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.photos.PhotoList;
import com.aetrion.flickr.photos.PhotoUtils;
import com.aetrion.flickr.util.StringUtilities;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class InterestingnessInterface {
    private static final ThreadLocal DATE_FORMATS = new ThreadLocal() {
        /* access modifiers changed from: protected */
        public synchronized Object initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
    private static final String KEY_API_KEY = "api_key";
    private static final String KEY_DATE = "date";
    private static final String KEY_EXTRAS = "extras";
    private static final String KEY_METHOD = "method";
    private static final String KEY_PAGE = "page";
    private static final String KEY_PER_PAGE = "per_page";
    public static final String METHOD_GET_LIST = "flickr.interestingness.getList";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public InterestingnessInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public PhotoList getList(String date, Set extras, int perPage, int page) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PhotoList photos = new PhotoList();
        parameters.add(new Parameter(KEY_METHOD, "flickr.interestingness.getList"));
        parameters.add(new Parameter(KEY_API_KEY, this.apiKey));
        if (date != null) {
            parameters.add(new Parameter(KEY_DATE, date));
        }
        if (extras != null) {
            parameters.add(new Parameter("extras", StringUtilities.join(extras, ",")));
        }
        if (perPage > 0) {
            parameters.add(new Parameter(KEY_PER_PAGE, String.valueOf(perPage)));
        }
        if (page > 0) {
            parameters.add(new Parameter(KEY_PAGE, String.valueOf(page)));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element photosElement = response.getPayload();
        photos.setPage(photosElement.getAttribute(KEY_PAGE));
        photos.setPages(photosElement.getAttribute("pages"));
        photos.setPerPage(photosElement.getAttribute("perpage"));
        photos.setTotal(photosElement.getAttribute("total"));
        NodeList photoNodes = photosElement.getElementsByTagName("photo");
        for (int i = 0; i < photoNodes.getLength(); i++) {
            photos.add(PhotoUtils.createPhoto((Element) photoNodes.item(i)));
        }
        return photos;
    }

    public PhotoList getList(Date date, Set extras, int perPage, int page) throws FlickrException, IOException, SAXException {
        String dateString = null;
        if (date != null) {
            dateString = ((DateFormat) DATE_FORMATS.get()).format(date);
        }
        return getList(dateString, extras, perPage, page);
    }

    public PhotoList getList() throws FlickrException, IOException, SAXException {
        return getList((String) null, Extras.ALL_EXTRAS, 500, 1);
    }
}
