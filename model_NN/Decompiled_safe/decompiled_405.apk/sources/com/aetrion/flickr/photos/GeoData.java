package com.aetrion.flickr.photos;

import com.aetrion.flickr.util.StringUtilities;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GeoData {
    private static final long serialVersionUID = 12;
    private int accuracy;
    private float latitude;
    private float longitude;

    public GeoData() {
    }

    public GeoData(String longitudeStr, String latitudeStr, String accuracyStr) {
        this.longitude = Float.parseFloat(longitudeStr);
        this.latitude = Float.parseFloat(latitudeStr);
        this.accuracy = Integer.parseInt(accuracyStr);
    }

    public int getAccuracy() {
        return this.accuracy;
    }

    public void setAccuracy(int accuracy2) {
        this.accuracy = accuracy2;
    }

    public float getLatitude() {
        return this.latitude;
    }

    public void setLatitude(float latitude2) {
        this.latitude = latitude2;
    }

    public float getLongitude() {
        return this.longitude;
    }

    public void setLongitude(float longitude2) {
        this.longitude = longitude2;
    }

    public String toString() {
        return "GeoData[longitude=" + this.longitude + " latitude=" + this.latitude + " accuracy=" + this.accuracy + "]";
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        GeoData test = (GeoData) obj;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                try {
                    Object res = method[i].invoke(this, null);
                    Object resTest = method[i].invoke(test, null);
                    String retType = method[i].getReturnType().toString();
                    if (retType.indexOf("class") == 0) {
                        if (!(res == null || resTest == null || res.equals(resTest))) {
                            return false;
                        }
                    } else if (retType.equals("int")) {
                        if (!((Integer) res).equals((Integer) resTest)) {
                            return false;
                        }
                    } else if (!retType.equals("float")) {
                        System.out.println(method[i].getName() + "|" + method[i].getReturnType().toString());
                    } else if (!((Float) res).equals((Float) resTest)) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    System.out.println("GeoData equals " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                } catch (Exception e3) {
                    System.out.println("GeoData equals " + method[i].getName() + " " + e3);
                }
            }
        }
        return true;
    }

    public int hashCode() {
        return 1 + new Float(this.longitude).hashCode() + new Float(this.latitude).hashCode() + new Integer(this.accuracy).hashCode();
    }
}
