package com.aetrion.flickr.photos;

import com.aetrion.flickr.people.User;
import com.aetrion.flickr.tags.Tag;
import com.aetrion.flickr.util.XMLUtilities;
import com.techcasita.android.creative.cloudprint.CloudPrintActivity;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public final class PhotoUtils {
    private static final long serialVersionUID = 12;

    private PhotoUtils() {
    }

    private static String getAttribute(String name, Element firstElement, Element secondElement) {
        String val = firstElement.getAttribute(name);
        if (val.length() != 0 || secondElement == null) {
            return val;
        }
        return secondElement.getAttribute(name);
    }

    public static final Photo createPhoto(Element photoElement) {
        return createPhoto(photoElement, null);
    }

    /* JADX INFO: Multiple debug info for r11v2 com.aetrion.flickr.people.User: [D('e' java.lang.IndexOutOfBoundsException), D('owner' com.aetrion.flickr.people.User)] */
    /* JADX INFO: Multiple debug info for r10v10 java.lang.String: [D('latitude' java.lang.String), D('photoElement' org.w3c.dom.Element)] */
    /* JADX INFO: Multiple debug info for r11v81 com.aetrion.flickr.photos.Permissions: [D('permissions' com.aetrion.flickr.photos.Permissions), D('permissionsNodes' org.w3c.dom.NodeList)] */
    /* JADX INFO: Multiple debug info for r11v94 com.aetrion.flickr.people.User: [D('defaultElement' org.w3c.dom.Element), D('owner' com.aetrion.flickr.people.User)] */
    /* JADX INFO: Multiple debug info for r2v19 java.lang.String: [D('urlTmp' java.lang.String), D('username' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v147 com.aetrion.flickr.people.User: [D('ownerElement' org.w3c.dom.Element), D('owner' com.aetrion.flickr.people.User)] */
    public static final Photo createPhoto(Element photoElement, Element defaultElement) {
        String latitude;
        String longitude;
        String accuracy;
        String accuracy2;
        Photo photo = new Photo();
        photo.setId(photoElement.getAttribute("id"));
        photo.setPlaceId(photoElement.getAttribute("place_id"));
        photo.setSecret(photoElement.getAttribute("secret"));
        photo.setServer(photoElement.getAttribute("server"));
        photo.setFarm(photoElement.getAttribute("farm"));
        photo.setRotation(photoElement.getAttribute("rotation"));
        photo.setFavorite("1".equals(photoElement.getAttribute("isfavorite")));
        photo.setLicense(photoElement.getAttribute(Extras.LICENSE));
        photo.setOriginalFormat(photoElement.getAttribute("originalformat"));
        photo.setOriginalSecret(photoElement.getAttribute("originalsecret"));
        photo.setIconServer(photoElement.getAttribute("iconserver"));
        photo.setIconFarm(photoElement.getAttribute("iconfarm"));
        photo.setDateTaken(photoElement.getAttribute("datetaken"));
        photo.setDatePosted(photoElement.getAttribute("dateupload"));
        photo.setLastUpdate(photoElement.getAttribute("lastupdate"));
        photo.setDateAdded(photoElement.getAttribute("dateadded"));
        photo.setOriginalWidth(photoElement.getAttribute("o_width"));
        photo.setOriginalHeight(photoElement.getAttribute("o_height"));
        photo.setMedia(photoElement.getAttribute(Extras.MEDIA));
        photo.setMediaStatus(photoElement.getAttribute("media_status"));
        photo.setPathAlias(photoElement.getAttribute("pathalias"));
        List sizes = new ArrayList();
        String urlTmp = photoElement.getAttribute(Extras.URL_T);
        if (urlTmp.startsWith("http")) {
            Size sizeT = new Size();
            sizeT.setLabel(0);
            sizeT.setSource(urlTmp);
            sizes.add(sizeT);
        }
        String urlTmp2 = photoElement.getAttribute(Extras.URL_S);
        if (urlTmp2.startsWith("http")) {
            Size sizeT2 = new Size();
            sizeT2.setLabel(2);
            sizeT2.setSource(urlTmp2);
            sizes.add(sizeT2);
        }
        String urlTmp3 = photoElement.getAttribute(Extras.URL_SQ);
        if (urlTmp3.startsWith("http")) {
            Size sizeT3 = new Size();
            sizeT3.setLabel(1);
            sizeT3.setSource(urlTmp3);
            sizes.add(sizeT3);
        }
        String urlTmp4 = photoElement.getAttribute(Extras.URL_M);
        if (urlTmp4.startsWith("http")) {
            Size sizeT4 = new Size();
            sizeT4.setLabel(3);
            sizeT4.setSource(urlTmp4);
            sizes.add(sizeT4);
        }
        String urlTmp5 = photoElement.getAttribute(Extras.URL_L);
        if (urlTmp5.startsWith("http")) {
            Size sizeT5 = new Size();
            sizeT5.setLabel(4);
            sizeT5.setSource(urlTmp5);
            sizes.add(sizeT5);
        }
        String urlTmp6 = photoElement.getAttribute(Extras.URL_O);
        if (urlTmp6.startsWith("http")) {
            Size sizeT6 = new Size();
            sizeT6.setLabel(5);
            sizeT6.setSource(urlTmp6);
            sizes.add(sizeT6);
        }
        if (sizes.size() > 0) {
            photo.setSizes(sizes);
        }
        try {
            if (photo.getOriginalFormat().equals("")) {
                photo.setOriginalFormat("jpg");
            }
        } catch (NullPointerException e) {
            photo.setOriginalFormat("jpg");
        }
        try {
            Element ownerElement = (Element) photoElement.getElementsByTagName("owner").item(0);
            if (ownerElement == null) {
                User owner = new User();
                owner.setId(getAttribute("owner", photoElement, defaultElement));
                owner.setUsername(getAttribute("ownername", photoElement, defaultElement));
                photo.setOwner(owner);
                photo.setUrl("http://flickr.com/photos/" + owner.getId() + "/" + photo.getId());
            } else {
                User owner2 = new User();
                owner2.setId(ownerElement.getAttribute("nsid"));
                String username = ownerElement.getAttribute("username");
                String ownername = ownerElement.getAttribute("ownername");
                if (username != null && !"".equals(username)) {
                    owner2.setUsername(username);
                } else if (ownername != null) {
                    if (!"".equals(ownername)) {
                        owner2.setUsername(ownername);
                    }
                }
                owner2.setUsername(ownerElement.getAttribute("username"));
                owner2.setRealName(ownerElement.getAttribute("realname"));
                owner2.setLocation(ownerElement.getAttribute("location"));
                photo.setOwner(owner2);
                photo.setUrl("http://flickr.com/photos/" + owner2.getId() + "/" + photo.getId());
            }
        } catch (IndexOutOfBoundsException e2) {
            User owner3 = new User();
            owner3.setId(photoElement.getAttribute("owner"));
            owner3.setUsername(photoElement.getAttribute("ownername"));
            photo.setOwner(owner3);
            photo.setUrl("http://flickr.com/photos/" + owner3.getId() + "/" + photo.getId());
        }
        try {
            photo.setTitle(XMLUtilities.getChildValue(photoElement, CloudPrintActivity.EXTRA_TITLE));
            if (photo.getTitle() == null) {
                photo.setTitle(photoElement.getAttribute(CloudPrintActivity.EXTRA_TITLE));
            }
        } catch (IndexOutOfBoundsException e3) {
            photo.setTitle(photoElement.getAttribute(CloudPrintActivity.EXTRA_TITLE));
        }
        try {
            photo.setDescription(XMLUtilities.getChildValue(photoElement, "description"));
        } catch (IndexOutOfBoundsException e4) {
        }
        try {
            Element visibilityElement = (Element) photoElement.getElementsByTagName("visibility").item(0);
            photo.setPublicFlag("1".equals(visibilityElement.getAttribute("ispublic")));
            photo.setFriendFlag("1".equals(visibilityElement.getAttribute("isfriend")));
            photo.setFamilyFlag("1".equals(visibilityElement.getAttribute("isfamily")));
        } catch (IndexOutOfBoundsException e5) {
        } catch (NullPointerException e6) {
            photo.setPublicFlag("1".equals(photoElement.getAttribute("ispublic")));
            photo.setFriendFlag("1".equals(photoElement.getAttribute("isfriend")));
            photo.setFamilyFlag("1".equals(photoElement.getAttribute("isfamily")));
        }
        try {
            Element datesElement = XMLUtilities.getChild(photoElement, "dates");
            photo.setDatePosted(datesElement.getAttribute("posted"));
            photo.setDateTaken(datesElement.getAttribute("taken"));
            photo.setTakenGranularity(datesElement.getAttribute("takengranularity"));
            photo.setLastUpdate(datesElement.getAttribute("lastupdate"));
        } catch (IndexOutOfBoundsException e7) {
            photo.setDateTaken(photoElement.getAttribute("datetaken"));
        } catch (NullPointerException e8) {
            photo.setDateTaken(photoElement.getAttribute("datetaken"));
        }
        NodeList permissionsNodes = photoElement.getElementsByTagName("permissions");
        if (permissionsNodes.getLength() > 0) {
            Element permissionsElement = (Element) permissionsNodes.item(0);
            Permissions permissions = new Permissions();
            permissions.setComment(permissionsElement.getAttribute("permcomment"));
            permissions.setAddmeta(permissionsElement.getAttribute("permaddmeta"));
        }
        try {
            Element editabilityElement = (Element) photoElement.getElementsByTagName("editability").item(0);
            Editability editability = new Editability();
            editability.setComment("1".equals(editabilityElement.getAttribute("cancomment")));
            editability.setAddmeta("1".equals(editabilityElement.getAttribute("canaddmeta")));
            photo.setEditability(editability);
        } catch (IndexOutOfBoundsException | NullPointerException e9) {
        }
        try {
            photo.setComments(((Text) ((Element) photoElement.getElementsByTagName("comments").item(0)).getFirstChild()).getData());
        } catch (IndexOutOfBoundsException | NullPointerException e10) {
        }
        try {
            List notes = new ArrayList();
            NodeList noteNodes = ((Element) photoElement.getElementsByTagName("notes").item(0)).getElementsByTagName("note");
            for (int i = 0; i < noteNodes.getLength(); i++) {
                Element noteElement = (Element) noteNodes.item(i);
                Note note = new Note();
                note.setId(noteElement.getAttribute("id"));
                note.setAuthor(noteElement.getAttribute("author"));
                note.setAuthorName(noteElement.getAttribute("authorname"));
                note.setBounds(noteElement.getAttribute("x"), noteElement.getAttribute("y"), noteElement.getAttribute("w"), noteElement.getAttribute("h"));
                note.setText(noteElement.getTextContent());
                notes.add(note);
            }
            photo.setNotes(notes);
        } catch (IndexOutOfBoundsException e11) {
            photo.setNotes(new ArrayList());
        } catch (NullPointerException e12) {
            photo.setNotes(new ArrayList());
        }
        try {
            List tags = new ArrayList();
            String tagsAttr = photoElement.getAttribute(Extras.TAGS);
            if (!tagsAttr.equals("")) {
                String[] values = tagsAttr.split("\\s+");
                for (String value : values) {
                    Tag tag = new Tag();
                    tag.setValue(value);
                    tags.add(tag);
                }
            } else {
                try {
                    NodeList tagNodes = ((Element) photoElement.getElementsByTagName(Extras.TAGS).item(0)).getElementsByTagName("tag");
                    for (int i2 = 0; i2 < tagNodes.getLength(); i2++) {
                        Element tagElement = (Element) tagNodes.item(i2);
                        Tag tag2 = new Tag();
                        tag2.setId(tagElement.getAttribute("id"));
                        tag2.setAuthor(tagElement.getAttribute("author"));
                        tag2.setRaw(tagElement.getAttribute("raw"));
                        tag2.setValue(((Text) tagElement.getFirstChild()).getData());
                        tags.add(tag2);
                    }
                } catch (IndexOutOfBoundsException e13) {
                }
            }
            photo.setTags(tags);
        } catch (NullPointerException e14) {
            photo.setTags(new ArrayList());
        }
        try {
            List urls = new ArrayList();
            NodeList urlNodes = ((Element) photoElement.getElementsByTagName("urls").item(0)).getElementsByTagName("url");
            for (int i3 = 0; i3 < urlNodes.getLength(); i3++) {
                Element urlElement = (Element) urlNodes.item(i3);
                PhotoUrl photoUrl = new PhotoUrl();
                photoUrl.setType(urlElement.getAttribute("type"));
                photoUrl.setUrl(XMLUtilities.getValue(urlElement));
                if (photoUrl.getType().equals("photopage")) {
                    photo.setUrl(photoUrl.getUrl());
                }
            }
            photo.setUrls(urls);
        } catch (IndexOutOfBoundsException e15) {
        } catch (NullPointerException e16) {
            photo.setUrls(new ArrayList());
        }
        String longitude2 = null;
        String latitude2 = null;
        try {
            Element geoElement = (Element) photoElement.getElementsByTagName("location").item(0);
            longitude2 = geoElement.getAttribute("longitude");
            latitude2 = geoElement.getAttribute("latitude");
            accuracy2 = geoElement.getAttribute("accuracy");
            accuracy = latitude2;
            longitude = longitude2;
        } catch (IndexOutOfBoundsException e17) {
            longitude = longitude2;
            accuracy = latitude2;
            accuracy2 = null;
        } catch (NullPointerException e18) {
            try {
                longitude2 = photoElement.getAttribute("longitude");
                String latitude3 = photoElement.getAttribute("latitude");
                try {
                    accuracy2 = photoElement.getAttribute("accuracy");
                    accuracy = latitude3;
                    longitude = longitude2;
                } catch (NullPointerException e19) {
                    latitude = latitude3;
                    longitude = longitude2;
                    accuracy = latitude;
                    accuracy2 = null;
                    photo.setGeoData(new GeoData(longitude, accuracy, accuracy2));
                    return photo;
                }
            } catch (NullPointerException e20) {
                latitude = latitude2;
                longitude = longitude2;
                accuracy = latitude;
                accuracy2 = null;
                photo.setGeoData(new GeoData(longitude, accuracy, accuracy2));
                return photo;
            }
        }
        if (longitude != null && accuracy != null && longitude.length() > 0 && accuracy.length() > 0 && (!"0".equals(longitude) || !"0".equals(accuracy))) {
            photo.setGeoData(new GeoData(longitude, accuracy, accuracy2));
        }
        return photo;
    }

    public static final PhotoList createPhotoList(Element photosElement) {
        PhotoList photos = new PhotoList();
        photos.setPage(photosElement.getAttribute("page"));
        photos.setPages(photosElement.getAttribute("pages"));
        photos.setPerPage(photosElement.getAttribute("perpage"));
        photos.setTotal(photosElement.getAttribute("total"));
        NodeList photoNodes = photosElement.getElementsByTagName("photo");
        for (int i = 0; i < photoNodes.getLength(); i++) {
            photos.add(createPhoto((Element) photoNodes.item(i)));
        }
        return photos;
    }
}
