package com.aetrion.flickr.photos;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.util.StringUtilities;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

public class SearchParameters {
    public static final ThreadLocal DATE_FORMATS = new ThreadLocal() {
        /* access modifiers changed from: protected */
        public synchronized Object initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
    public static int DATE_POSTED_ASC = 1;
    public static int DATE_POSTED_DESC = 0;
    public static int DATE_TAKEN_ASC = 3;
    public static int DATE_TAKEN_DESC = 2;
    public static int INTERESTINGNESS_ASC = 5;
    public static int INTERESTINGNESS_DESC = 4;
    public static final ThreadLocal MYSQL_DATE_FORMATS = new ThreadLocal() {
        /* access modifiers changed from: protected */
        public synchronized Object initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };
    public static int RELEVANCE = 6;
    private static final long serialVersionUID = 12;
    private int accuracy = 0;
    private String[] bbox;
    private String contacts;
    private Set extras;
    private String groupId;
    private boolean hasGeo = false;
    private Date interestingnessDate;
    private String latitude;
    private String license;
    private String longitude;
    private String machineTagMode;
    private String[] machineTags;
    private Date maxTakenDate;
    private Date maxUploadDate;
    private String media;
    private Date minTakenDate;
    private Date minUploadDate;
    private String placeId;
    private int radius = -1;
    private String radiusUnits;
    private String safeSearch;
    private int sort = 0;
    private String tagMode;
    private String[] tags;
    private String text;
    private String userId;
    private String woeId;

    public void setAccuracy(int accuracy2) {
        this.accuracy = accuracy2;
    }

    public int getAccuracy() {
        return this.accuracy;
    }

    public String getGroupId() {
        return this.groupId;
    }

    public void setGroupId(String groupId2) {
        this.groupId = groupId2;
    }

    public void setHasGeo(boolean hasGeo2) {
        this.hasGeo = hasGeo2;
    }

    public boolean getHasGeo() {
        return this.hasGeo;
    }

    public String[] getTags() {
        return this.tags;
    }

    public void setTags(String[] tags2) {
        this.tags = tags2;
    }

    public String getTagMode() {
        return this.tagMode;
    }

    public void setTagMode(String tagMode2) {
        this.tagMode = tagMode2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public Date getMinUploadDate() {
        return this.minUploadDate;
    }

    public void setMinUploadDate(Date minUploadDate2) {
        this.minUploadDate = minUploadDate2;
    }

    public Date getMaxUploadDate() {
        return this.maxUploadDate;
    }

    public void setMaxUploadDate(Date maxUploadDate2) {
        this.maxUploadDate = maxUploadDate2;
    }

    public Date getMinTakenDate() {
        return this.minTakenDate;
    }

    public void setMinTakenDate(Date minTakenDate2) {
        this.minTakenDate = minTakenDate2;
    }

    public Date getMaxTakenDate() {
        return this.maxTakenDate;
    }

    public void setMaxTakenDate(Date maxTakenDate2) {
        this.maxTakenDate = maxTakenDate2;
    }

    public String getLicense() {
        return this.license;
    }

    public void setLicense(String license2) {
        this.license = license2;
    }

    public Date getInterestingnessDate() {
        return this.interestingnessDate;
    }

    public void setInterestingnessDate(Date intrestingnessDate) {
        this.interestingnessDate = intrestingnessDate;
    }

    public void setMachineTags(String[] tags2) {
        this.machineTags = tags2;
    }

    public String[] getMachineTags() {
        return this.machineTags;
    }

    public void setMachineTagMode(String tagMode2) {
        this.machineTagMode = tagMode2;
    }

    public String getMachineTagMode() {
        return this.machineTagMode;
    }

    public void setExtras(Set extras2) {
        this.extras = extras2;
    }

    public void setBBox(String minimum_longitude, String minimum_latitude, String maximum_longitude, String maximum_latitude) {
        this.bbox = new String[]{minimum_longitude, minimum_latitude, maximum_longitude, maximum_latitude};
    }

    public String[] getBBox() {
        return this.bbox;
    }

    public void setSafeSearch(String level) {
        this.safeSearch = level;
    }

    public String getSafeSearch() {
        return this.safeSearch;
    }

    public int getSort() {
        return this.sort;
    }

    public void setSort(int order) {
        this.sort = order;
    }

    public String getPlaceId() {
        return this.placeId;
    }

    public void setPlaceId(String placeId2) {
        this.placeId = placeId2;
    }

    public String getWoeId() {
        return this.woeId;
    }

    public void setWoeId(String woeId2) {
        this.woeId = woeId2;
    }

    public String getMedia() {
        return this.media;
    }

    public void setMedia(String media2) throws FlickrException {
        if (media2.equals("all") || media2.equals("photos") || media2.equals("videos")) {
            this.media = media2;
            return;
        }
        throw new FlickrException("0", "Media type is not valid.");
    }

    public String getContacts() {
        return this.contacts;
    }

    public void setContacts(String contacts2) {
        this.contacts = contacts2;
    }

    public Collection getAsParameters() {
        ArrayList arrayList = new ArrayList();
        String lat = getLatitude();
        if (lat != null) {
            arrayList.add(new Parameter("lat", lat));
        }
        String lon = getLongitude();
        if (lon != null) {
            arrayList.add(new Parameter("lon", lon));
        }
        int radius2 = getRadius();
        if (radius2 > 0) {
            arrayList.add(new Parameter("radius", (long) radius2));
        }
        String radiusUnits2 = getRadiusUnits();
        if (radiusUnits2 != null) {
            arrayList.add(new Parameter("radius_units", radiusUnits2));
        }
        String media2 = getMedia();
        if (media2 != null) {
            arrayList.add(new Parameter(Extras.MEDIA, media2));
        }
        String userId2 = getUserId();
        if (userId2 != null) {
            arrayList.add(new Parameter("user_id", userId2));
            String contacts2 = getContacts();
            if (contacts2 != null) {
                arrayList.add(new Parameter("contacts", contacts2));
            }
        }
        String groupId2 = getGroupId();
        if (groupId2 != null) {
            arrayList.add(new Parameter("group_id", groupId2));
        }
        String[] tags2 = getTags();
        if (tags2 != null) {
            arrayList.add(new Parameter(Extras.TAGS, StringUtilities.join(tags2, ",")));
        }
        String tagMode2 = getTagMode();
        if (tagMode2 != null) {
            arrayList.add(new Parameter("tag_mode", tagMode2));
        }
        String[] mtags = getMachineTags();
        if (mtags != null) {
            arrayList.add(new Parameter(Extras.MACHINE_TAGS, StringUtilities.join(mtags, ",")));
        }
        String mtagMode = getMachineTagMode();
        if (mtagMode != null) {
            arrayList.add(new Parameter("machine_tag_mode", mtagMode));
        }
        String text2 = getText();
        if (text2 != null) {
            arrayList.add(new Parameter("text", text2));
        }
        Date minUploadDate2 = getMinUploadDate();
        if (minUploadDate2 != null) {
            arrayList.add(new Parameter("min_upload_date", new Long(minUploadDate2.getTime() / 1000)));
        }
        Date maxUploadDate2 = getMaxUploadDate();
        if (maxUploadDate2 != null) {
            arrayList.add(new Parameter("max_upload_date", new Long(maxUploadDate2.getTime() / 1000)));
        }
        Date minTakenDate2 = getMinTakenDate();
        if (minTakenDate2 != null) {
            arrayList.add(new Parameter("min_taken_date", ((DateFormat) MYSQL_DATE_FORMATS.get()).format(minTakenDate2)));
        }
        Date maxTakenDate2 = getMaxTakenDate();
        if (maxTakenDate2 != null) {
            arrayList.add(new Parameter("max_taken_date", ((DateFormat) MYSQL_DATE_FORMATS.get()).format(maxTakenDate2)));
        }
        String license2 = getLicense();
        if (license2 != null) {
            arrayList.add(new Parameter(Extras.LICENSE, license2));
        }
        Date intrestingnessDate = getInterestingnessDate();
        if (intrestingnessDate != null) {
            arrayList.add(new Parameter("date", ((DateFormat) DATE_FORMATS.get()).format(intrestingnessDate)));
        }
        String[] bbox2 = getBBox();
        if (bbox2 != null) {
            arrayList.add(new Parameter("bbox", StringUtilities.join(bbox2, ",")));
            if (this.accuracy > 0) {
                arrayList.add(new Parameter("accuracy", (long) this.accuracy));
            }
        } else {
            String woeId2 = getWoeId();
            if (woeId2 != null) {
                arrayList.add(new Parameter("woe_id", woeId2));
            }
        }
        String safeSearch2 = getSafeSearch();
        if (safeSearch2 != null) {
            arrayList.add(new Parameter("safe_search", safeSearch2));
        }
        if (getHasGeo()) {
            arrayList.add(new Parameter("has_geo", "true"));
        }
        if (this.extras != null && !this.extras.isEmpty()) {
            arrayList.add(new Parameter(Extras.KEY_EXTRAS, StringUtilities.join(this.extras, ",")));
        }
        if (this.sort != DATE_POSTED_DESC) {
            String sortArg = null;
            if (this.sort == DATE_POSTED_ASC) {
                sortArg = "date-posted-asc";
            }
            if (this.sort == DATE_TAKEN_DESC) {
                sortArg = "date-taken-desc";
            }
            if (this.sort == DATE_TAKEN_ASC) {
                sortArg = "date-taken-asc";
            }
            if (this.sort == INTERESTINGNESS_DESC) {
                sortArg = "interestingness-desc";
            }
            if (this.sort == INTERESTINGNESS_ASC) {
                sortArg = "interestingness-asc";
            }
            if (this.sort == RELEVANCE) {
                sortArg = "relevance";
            }
            if (sortArg != null) {
                arrayList.add(new Parameter("sort", sortArg));
            }
        }
        return arrayList;
    }

    public void setLatitude(String lat) {
        this.latitude = lat;
    }

    public void setRadius(int r) {
        this.radius = r;
    }

    public void setLongitude(String lon) {
        this.longitude = lon;
    }

    public void setRadiusUnits(String units) {
        this.radiusUnits = units;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public int getRadius() {
        return this.radius;
    }

    public String getRadiusUnits() {
        return this.radiusUnits;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }
}
