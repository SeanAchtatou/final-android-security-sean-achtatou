package com.aetrion.flickr.photos.comments;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.photos.PhotoList;
import com.aetrion.flickr.photos.PhotoUtils;
import com.aetrion.flickr.photos.PhotosInterface;
import com.aetrion.flickr.util.StringUtilities;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CommentsInterface {
    public static final String METHOD_ADD_COMMENT = "flickr.photos.comments.addComment";
    public static final String METHOD_DELETE_COMMENT = "flickr.photos.comments.deleteComment";
    public static final String METHOD_EDIT_COMMENT = "flickr.photos.comments.editComment";
    public static final String METHOD_GET_LIST = "flickr.photos.comments.getList";
    public static final String METHOD_GET_RECENT = "flickr.photos.comments.getRecentForContacts";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public CommentsInterface(String apiKey2, String sharedSecret2, Transport transport) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transport;
    }

    public String addComment(String photoId, String commentText) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_ADD_COMMENT));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("comment_text", commentText));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayload().getAttribute("id");
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public void deleteComment(String commentId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_DELETE_COMMENT));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("comment_id", commentId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void editComment(String commentId, String commentText) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_EDIT_COMMENT));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("comment_id", commentId));
        parameters.add(new Parameter("comment_text", commentText));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public List getList(String photoId) throws FlickrException, IOException, SAXException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Parameter("method", METHOD_GET_LIST));
        arrayList.add(new Parameter("api_key", this.apiKey));
        arrayList.add(new Parameter("photo_id", photoId));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), arrayList);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        List comments = new ArrayList();
        NodeList commentNodes = response.getPayload().getElementsByTagName("comment");
        int n = commentNodes.getLength();
        for (int i = 0; i < n; i++) {
            Comment comment = new Comment();
            Element commentElement = (Element) commentNodes.item(i);
            comment.setId(commentElement.getAttribute("id"));
            comment.setAuthor(commentElement.getAttribute("author"));
            comment.setAuthorName(commentElement.getAttribute("authorname"));
            comment.setPermaLink(commentElement.getAttribute("permalink"));
            long unixTime = 0;
            try {
                unixTime = Long.parseLong(commentElement.getAttribute("datecreate"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            comment.setDateCreate(new Date(1000 * unixTime));
            comment.setPermaLink(commentElement.getAttribute("permalink"));
            comment.setText(XMLUtilities.getValue(commentElement));
            comments.add(comment);
        }
        return comments;
    }

    public PhotoList getRecentForContacts(Date lastComment, ArrayList contactsFilter, Set extras, int perPage, int page) throws FlickrException, IOException, SAXException {
        PhotoList photos = new PhotoList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", PhotosInterface.METHOD_GET_NOT_IN_SET));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (lastComment != null) {
            parameters.add(new Parameter("last_comment", String.valueOf(lastComment.getTime() / 1000)));
        }
        if (extras != null && !extras.isEmpty()) {
            parameters.add(new Parameter(Extras.KEY_EXTRAS, StringUtilities.join(extras, ",")));
        }
        if (contactsFilter != null && !contactsFilter.isEmpty()) {
            parameters.add(new Parameter("contacts_filter", StringUtilities.join(contactsFilter, ",")));
        }
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", (long) perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", (long) page));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element photosElement = response.getPayload();
        photos.setPage(photosElement.getAttribute("page"));
        photos.setPages(photosElement.getAttribute("pages"));
        photos.setPerPage(photosElement.getAttribute("perpage"));
        photos.setTotal(photosElement.getAttribute("total"));
        NodeList photoElements = photosElement.getElementsByTagName("photo");
        for (int i = 0; i < photoElements.getLength(); i++) {
            photos.add(PhotoUtils.createPhoto((Element) photoElements.item(i)));
        }
        return photos;
    }
}
