package com.aetrion.flickr.photos.geo;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.photos.GeoData;
import com.aetrion.flickr.photos.PhotoList;
import com.aetrion.flickr.photos.PhotoUtils;
import com.aetrion.flickr.util.StringUtilities;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GeoInterface {
    public static final String METHOD_BATCH_CORRECT_LOCATION = "flickr.photos.geo.batchCorrectLocation";
    public static final String METHOD_CORRECT_LOCATION = "flickr.photos.geo.correctLocation";
    public static final String METHOD_GET_LOCATION = "flickr.photos.geo.getLocation";
    public static final String METHOD_GET_PERMS = "flickr.photos.geo.getPerms";
    public static final String METHOD_PHOTOS_FOR_LOCATION = "flickr.photos.geo.photosForLocation";
    public static final String METHOD_REMOVE_LOCATION = "flickr.photos.geo.removeLocation";
    public static final String METHOD_SET_CONTEXT = "flickr.photos.geo.setContext";
    public static final String METHOD_SET_LOCATION = "flickr.photos.geo.setLocation";
    public static final String METHOD_SET_PERMS = "flickr.photos.geo.setPerms";
    private String apiKey;
    private String sharedSecret;
    private Transport transport;

    public GeoInterface(String apiKey2, String sharedSecret2, Transport transport2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transport = transport2;
    }

    public GeoData getLocation(String photoId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_LOCATION));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        Response response = this.transport.get(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element locationElement = XMLUtilities.getChild(response.getPayload(), "location");
        return new GeoData(locationElement.getAttribute("longitude"), locationElement.getAttribute("latitude"), locationElement.getAttribute("accuracy"));
    }

    public GeoPermissions getPerms(String photoId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_PERMS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.get(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        GeoPermissions perms = new GeoPermissions();
        Element permsElement = response.getPayload();
        perms.setPublic("1".equals(permsElement.getAttribute("ispublic")));
        perms.setContact("1".equals(permsElement.getAttribute("iscontact")));
        perms.setFriend("1".equals(permsElement.getAttribute("isfriend")));
        perms.setFamily("1".equals(permsElement.getAttribute("isfamily")));
        return perms;
    }

    public void removeLocation(String photoId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_REMOVE_LOCATION));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void setLocation(String photoId, GeoData location) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_SET_LOCATION));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("lat", String.valueOf(location.getLatitude())));
        parameters.add(new Parameter("lon", String.valueOf(location.getLongitude())));
        if (location.getAccuracy() > 0) {
            parameters.add(new Parameter("accuracy", String.valueOf(location.getAccuracy())));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void setPerms(String photoId, GeoPermissions perms) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_SET_PERMS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("is_public", perms.isPublic() ? "1" : "0"));
        parameters.add(new Parameter("is_contact", perms.isContact() ? "1" : "0"));
        parameters.add(new Parameter("is_friend", perms.isFriend() ? "1" : "0"));
        parameters.add(new Parameter("is_family", perms.isFamily() ? "1" : "0"));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void batchCorrectLocation(GeoData location, String placeId, String woeId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_BATCH_CORRECT_LOCATION));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        parameters.add(new Parameter("lat", Float.valueOf(location.getLatitude())));
        parameters.add(new Parameter("lon", Float.valueOf(location.getLongitude())));
        parameters.add(new Parameter("accuracy", (long) location.getAccuracy()));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void correctLocation(String photoId, String placeId, String woeId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_CORRECT_LOCATION));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public PhotoList photosForLocation(GeoData location, Set extras, int perPage, int page) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        PhotoList photos = new PhotoList();
        parameters.add(new Parameter("method", METHOD_PHOTOS_FOR_LOCATION));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (extras.size() > 0) {
            parameters.add(new Parameter(Extras.KEY_EXTRAS, StringUtilities.join(extras, ",")));
        }
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", (long) perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", (long) page));
        }
        parameters.add(new Parameter("lat", Float.valueOf(location.getLatitude())));
        parameters.add(new Parameter("lon", Float.valueOf(location.getLongitude())));
        parameters.add(new Parameter("accuracy", (long) location.getAccuracy()));
        Response response = this.transport.get(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element photosElement = response.getPayload();
        photos.setPage(photosElement.getAttribute("page"));
        photos.setPages(photosElement.getAttribute("pages"));
        photos.setPerPage(photosElement.getAttribute("perpage"));
        photos.setTotal(photosElement.getAttribute("total"));
        NodeList photoElements = photosElement.getElementsByTagName("photo");
        for (int i = 0; i < photoElements.getLength(); i++) {
            photos.add(PhotoUtils.createPhoto((Element) photoElements.item(i)));
        }
        return photos;
    }

    public void setContext(String photoId, int context) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_SET_CONTEXT));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("context", "" + context));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }
}
