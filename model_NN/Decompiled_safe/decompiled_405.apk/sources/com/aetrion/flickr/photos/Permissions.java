package com.aetrion.flickr.photos;

import com.aetrion.flickr.util.StringUtilities;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Permissions {
    private static final long serialVersionUID = 12;
    private int addmeta = 0;
    private int comment = 0;
    private boolean familyFlag;
    private boolean friendFlag;
    private String id;
    private boolean publicFlag;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public boolean isPublicFlag() {
        return this.publicFlag;
    }

    public void setPublicFlag(boolean publicFlag2) {
        this.publicFlag = publicFlag2;
    }

    public boolean isFriendFlag() {
        return this.friendFlag;
    }

    public void setFriendFlag(boolean friendFlag2) {
        this.friendFlag = friendFlag2;
    }

    public boolean isFamilyFlag() {
        return this.familyFlag;
    }

    public void setFamilyFlag(boolean familyFlag2) {
        this.familyFlag = familyFlag2;
    }

    public int getComment() {
        return this.comment;
    }

    public void setComment(int comment2) {
        this.comment = comment2;
    }

    public void setComment(String comment2) {
        if (comment2 != null) {
            setComment(Integer.parseInt(comment2));
        }
    }

    public int getAddmeta() {
        return this.addmeta;
    }

    public void setAddmeta(int addmeta2) {
        this.addmeta = addmeta2;
    }

    public void setAddmeta(String addmeta2) {
        if (addmeta2 != null) {
            setAddmeta(Integer.parseInt(addmeta2));
        }
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Permissions test = (Permissions) obj;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                try {
                    Object res = method[i].invoke(this, null);
                    Object resTest = method[i].invoke(test, null);
                    String retType = method[i].getReturnType().toString();
                    if (retType.indexOf("class") == 0) {
                        if (!(res == null || resTest == null || res.equals(resTest))) {
                            return false;
                        }
                    } else if (retType.equals("int")) {
                        if (!((Integer) res).equals((Integer) resTest)) {
                            return false;
                        }
                    } else if (!retType.equals("boolean")) {
                        System.out.println(method[i].getName() + "|" + method[i].getReturnType().toString());
                    } else if (!((Boolean) res).equals((Boolean) resTest)) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    System.out.println("equals " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                } catch (Exception e3) {
                    System.out.println("equals " + method[i].getName() + " " + e3);
                }
            }
        }
        return true;
    }

    public int hashCode() {
        return 87 + this.id.hashCode() + new Integer(this.comment).hashCode() + new Integer(this.addmeta).hashCode() + new Boolean(this.publicFlag).hashCode() + new Boolean(this.friendFlag).hashCode() + new Boolean(this.familyFlag).hashCode();
    }
}
