package com.aetrion.flickr.photos.geo;

public class GeoPermissions {
    private static final long serialVersionUID = 12;
    private boolean contact;
    private boolean family;
    private boolean friend;
    private boolean pub;

    public boolean isContact() {
        return this.contact;
    }

    public void setContact(boolean enable) {
        this.contact = enable;
    }

    public boolean isFamily() {
        return this.family;
    }

    public void setFamily(boolean enable) {
        this.family = enable;
    }

    public boolean isFriend() {
        return this.friend;
    }

    public void setFriend(boolean enable) {
        this.friend = enable;
    }

    public boolean isPublic() {
        return this.pub;
    }

    public void setPublic(boolean enable) {
        this.pub = enable;
    }
}
