package com.aetrion.flickr.photos.licenses;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.photos.Extras;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LicensesInterface {
    public static final String METHOD_GET_INFO = "flickr.photos.licenses.getInfo";
    public static final String METHOD_SET_LICENSE = "flickr.photos.licenses.setLicense";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public LicensesInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public Collection getInfo() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_INFO));
        parameters.add(new Parameter("api_key", this.apiKey));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        List licenses = new ArrayList();
        NodeList licenseElements = response.getPayload().getElementsByTagName(Extras.LICENSE);
        for (int i = 0; i < licenseElements.getLength(); i++) {
            Element licenseElement = (Element) licenseElements.item(i);
            License license = new License();
            license.setId(licenseElement.getAttribute("id"));
            license.setName(licenseElement.getAttribute("name"));
            license.setUrl(licenseElement.getAttribute("url"));
            licenses.add(license);
        }
        return licenses;
    }

    public void setLicense(String photoId, int licenseId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_SET_LICENSE));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("license_id", (long) licenseId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }
}
