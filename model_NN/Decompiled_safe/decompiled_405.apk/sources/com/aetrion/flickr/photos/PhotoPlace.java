package com.aetrion.flickr.photos;

public class PhotoPlace {
    public static final int POOL = 2;
    public static final int SET = 1;
    private static final long serialVersionUID = 12;
    private String id;
    private int kind;
    private String title;

    public PhotoPlace(int kind2, String id2, String title2) {
        setKind(kind2);
        this.id = id2;
        this.title = title2;
    }

    public PhotoPlace(String kind2, String id2, String title2) {
        setKind(kind2);
        this.id = id2;
        this.title = title2;
    }

    public String getId() {
        return this.id;
    }

    public int getKind() {
        return this.kind;
    }

    /* access modifiers changed from: protected */
    public void setKind(int kind2) {
        this.kind = kind2;
    }

    /* access modifiers changed from: protected */
    public void setKind(String kindStr) {
        if ("pool".equalsIgnoreCase(kindStr)) {
            setKind(2);
        } else if ("set".equalsIgnoreCase(kindStr)) {
            setKind(1);
        } else {
            throw new IllegalArgumentException("Invalid kind [" + kindStr + "]");
        }
    }

    public String getTitle() {
        return this.title;
    }

    public String getKindAsString() {
        switch (this.kind) {
            case 1:
                return "set";
            case 2:
                return "pool";
            default:
                return "unknown(" + this.kind + ")";
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof PhotoPlace)) {
            return false;
        }
        PhotoPlace other = (PhotoPlace) obj;
        if (other.kind != this.kind) {
            return false;
        }
        if (this.id != null) {
            if (!this.id.equals(other.id)) {
                return false;
            }
        } else if (other.id != null) {
            return false;
        }
        if (this.title != null) {
            if (!this.title.equals(other.title)) {
                return false;
            }
        } else if (other.title != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.id != null) {
            return this.id.hashCode();
        }
        return 0;
    }

    public String toString() {
        return getClass().getName() + "[" + getKindAsString() + " id=\"" + this.id + "\" title=\"" + this.title + "\"]";
    }
}
