package com.aetrion.flickr.photos.comments;

import java.util.Date;

public class Comment {
    private static final long serialVersionUID = 12;
    String author;
    String authorName;
    Date dateCreate;
    String id;
    String permaLink;
    String text;

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author2) {
        this.author = author2;
    }

    public String getAuthorName() {
        return this.authorName;
    }

    public void setAuthorName(String authorName2) {
        this.authorName = authorName2;
    }

    public Date getDateCreate() {
        return this.dateCreate;
    }

    public void setDateCreate(Date dateCreate2) {
        this.dateCreate = dateCreate2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getPermaLink() {
        return this.permaLink;
    }

    public void setPermaLink(String permaLink2) {
        this.permaLink = permaLink2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }
}
