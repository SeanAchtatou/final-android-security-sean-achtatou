package com.aetrion.flickr.photos;

import java.util.Date;

public class Photocount {
    private static final long serialVersionUID = 12;
    private int count;
    private Date fromDate;
    private Date toDate;

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        this.count = count2;
    }

    public void setCount(String count2) {
        if (count2 != null) {
            setCount(Integer.parseInt(count2));
        }
    }

    public Date getFromDate() {
        return this.fromDate;
    }

    public void setFromDate(Date fromDate2) {
        this.fromDate = fromDate2;
    }

    public void setFromDate(long fromDate2) {
        setFromDate(new Date(fromDate2));
    }

    public void setFromDate(String fromDate2) {
        if (fromDate2 != null) {
            setFromDate(Long.parseLong(fromDate2));
        }
    }

    public Date getToDate() {
        return this.toDate;
    }

    public void setToDate(Date toDate2) {
        this.toDate = toDate2;
    }

    public void setToDate(long toDate2) {
        setToDate(new Date(toDate2));
    }

    public void setToDate(String toDate2) {
        if (toDate2 != null) {
            setToDate(Long.parseLong(toDate2));
        }
    }
}
