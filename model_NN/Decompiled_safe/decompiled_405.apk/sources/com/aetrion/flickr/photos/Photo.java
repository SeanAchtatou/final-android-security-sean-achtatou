package com.aetrion.flickr.photos;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.people.User;
import com.aetrion.flickr.tags.Tag;
import com.aetrion.flickr.util.IOUtilities;
import com.aetrion.flickr.util.StringUtilities;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;

public class Photo {
    private static final ThreadLocal DATE_FORMATS = new ThreadLocal() {
        /* access modifiers changed from: protected */
        public synchronized Object initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };
    private static final String DEFAULT_ORIGINAL_IMAGE_SUFFIX = "_o.jpg";
    private static final String LARGE_IMAGE_SUFFIX = "_b.jpg";
    private static final String MEDIUM_IMAGE_SUFFIX = ".jpg";
    private static final String SMALL_IMAGE_SUFFIX = "_m.jpg";
    private static final String SMALL_SQUARE_IMAGE_SUFFIX = "_s.jpg";
    private static final String THUMBNAIL_IMAGE_SUFFIX = "_t.jpg";
    private static final long serialVersionUID = 12;
    private int comments;
    private Date dateAdded;
    private Date datePosted;
    private Date dateTaken;
    private String description;
    private Editability editability;
    private boolean familyFlag;
    private String farm;
    private boolean favorite;
    private boolean friendFlag;
    private GeoData geoData;
    private String iconFarm;
    private String iconServer;
    private String id;
    private Size largeSize;
    private Date lastUpdate;
    private String license;
    private String media;
    private String mediaStatus;
    private Size mediumSize;
    private Collection notes;
    private String originalFormat;
    private int originalHeight;
    private String originalSecret;
    private Size originalSize;
    private int originalWidth;
    private User owner;
    private String pathAlias;
    private Permissions permissions;
    private String placeId;
    private boolean primary;
    private boolean publicFlag;
    private int rotation;
    private String secret;
    private String server;
    private Size smallSize;
    private Size squareSize;
    private Collection tags;
    private String takenGranularity;
    private Size thumbnailSize;
    private String title;
    private String url;
    private Collection urls;
    private int views = -1;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public User getOwner() {
        return this.owner;
    }

    public void setOwner(User owner2) {
        this.owner = owner2;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret2) {
        this.secret = secret2;
    }

    public String getFarm() {
        return this.farm;
    }

    public void setFarm(String farm2) {
        this.farm = farm2;
    }

    public String getServer() {
        return this.server;
    }

    public void setServer(String server2) {
        this.server = server2;
    }

    public boolean isFavorite() {
        return this.favorite;
    }

    public void setFavorite(boolean favorite2) {
        this.favorite = favorite2;
    }

    public String getLicense() {
        return this.license;
    }

    public void setLicense(String license2) {
        this.license = license2;
    }

    public boolean isPrimary() {
        return this.primary;
    }

    public void setPrimary(boolean primary2) {
        this.primary = primary2;
    }

    public void setPrimary(String primary2) {
        setPrimary("1".equals(primary2));
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public boolean isPublicFlag() {
        return this.publicFlag;
    }

    public void setPublicFlag(boolean publicFlag2) {
        this.publicFlag = publicFlag2;
    }

    public boolean isFriendFlag() {
        return this.friendFlag;
    }

    public void setFriendFlag(boolean friendFlag2) {
        this.friendFlag = friendFlag2;
    }

    public boolean isFamilyFlag() {
        return this.familyFlag;
    }

    public void setFamilyFlag(boolean familyFlag2) {
        this.familyFlag = familyFlag2;
    }

    public Date getDateAdded() {
        return this.dateAdded;
    }

    public void setDateAdded(Date dateAdded2) {
        this.dateAdded = dateAdded2;
    }

    public void setDateAdded(long dateAdded2) {
        setDateAdded(new Date(dateAdded2));
    }

    public void setDateAdded(String dateAdded2) {
        if (dateAdded2 != null && !"".equals(dateAdded2)) {
            setDateAdded(Long.parseLong(dateAdded2) * 1000);
        }
    }

    public Date getDatePosted() {
        return this.datePosted;
    }

    public void setDatePosted(Date datePosted2) {
        this.datePosted = datePosted2;
    }

    public void setDatePosted(long datePosted2) {
        setDatePosted(new Date(datePosted2));
    }

    public void setDatePosted(String datePosted2) {
        if (datePosted2 != null && !"".equals(datePosted2)) {
            setDatePosted(Long.parseLong(datePosted2) * 1000);
        }
    }

    public Date getDateTaken() {
        return this.dateTaken;
    }

    public void setDateTaken(Date dateTaken2) {
        this.dateTaken = dateTaken2;
    }

    public void setDateTaken(String dateTaken2) {
        if (dateTaken2 != null && !"".equals(dateTaken2)) {
            try {
                setDateTaken(((DateFormat) DATE_FORMATS.get()).parse(dateTaken2));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public Date getLastUpdate() {
        return this.lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate2) {
        this.lastUpdate = lastUpdate2;
    }

    public void setLastUpdate(String lastUpdateStr) {
        if (lastUpdateStr != null && !"".equals(lastUpdateStr)) {
            setLastUpdate(new Date(1000 * Long.parseLong(lastUpdateStr)));
        }
    }

    public String getTakenGranularity() {
        return this.takenGranularity;
    }

    public void setTakenGranularity(String takenGranularity2) {
        this.takenGranularity = takenGranularity2;
    }

    public Permissions getPermissions() {
        return this.permissions;
    }

    public void setPermissions(Permissions permissions2) {
        this.permissions = permissions2;
    }

    public Editability getEditability() {
        return this.editability;
    }

    public void setEditability(Editability editability2) {
        this.editability = editability2;
    }

    public int getComments() {
        return this.comments;
    }

    public void setComments(int comments2) {
        this.comments = comments2;
    }

    public void setComments(String comments2) {
        if (comments2 != null) {
            setComments(Integer.parseInt(comments2));
        }
    }

    public Collection getNotes() {
        return this.notes;
    }

    public void setNotes(Collection notes2) {
        this.notes = notes2;
    }

    public Collection getTags() {
        return this.tags;
    }

    public void setTags(Collection tags2) {
        this.tags = tags2;
    }

    public Collection getUrls() {
        return this.urls;
    }

    public void setUrls(Collection urls2) {
        this.urls = urls2;
    }

    public void setViews(String views2) {
        if (views2 != null) {
            try {
                setViews(Integer.parseInt(views2));
            } catch (NumberFormatException e) {
                setViews(-1);
            }
        }
    }

    public void setViews(int views2) {
        this.views = views2;
    }

    public int getViews() {
        return this.views;
    }

    public void setRotation(String rotation2) {
        if (rotation2 != null) {
            try {
                setRotation(Integer.parseInt(rotation2));
            } catch (NumberFormatException e) {
                setRotation(-1);
            }
        }
    }

    public void setRotation(int rotation2) {
        this.rotation = rotation2;
    }

    public int getRotation() {
        return this.rotation;
    }

    public String getIconServer() {
        return this.iconServer;
    }

    public void setIconServer(String iconServer2) {
        this.iconServer = iconServer2;
    }

    public String getIconFarm() {
        return this.iconFarm;
    }

    public void setIconFarm(String iconFarm2) {
        this.iconFarm = iconFarm2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public GeoData getGeoData() {
        return this.geoData;
    }

    public void setGeoData(GeoData geoData2) {
        this.geoData = geoData2;
    }

    public boolean hasGeoData() {
        return this.geoData != null;
    }

    public String getOriginalFormat() {
        return this.originalFormat;
    }

    public void setOriginalFormat(String originalFormat2) {
        this.originalFormat = originalFormat2;
    }

    public String getOriginalSecret() {
        return this.originalSecret;
    }

    public void setOriginalSecret(String originalSecret2) {
        this.originalSecret = originalSecret2;
    }

    public BufferedImage getOriginalImage() throws IOException, FlickrException {
        if (this.originalFormat != null) {
            return getOriginalImage("_o." + this.originalFormat);
        }
        return getOriginalImage(DEFAULT_ORIGINAL_IMAGE_SUFFIX);
    }

    public InputStream getOriginalAsStream() throws IOException, FlickrException {
        if (this.originalFormat != null) {
            return getOriginalImageAsStream("_o." + this.originalFormat);
        }
        return getOriginalImageAsStream(DEFAULT_ORIGINAL_IMAGE_SUFFIX);
    }

    public String getOriginalUrl() throws FlickrException {
        if (this.originalSize != null) {
            return this.originalSize.getSource();
        }
        if (this.originalFormat != null) {
            return ((Object) getOriginalBaseImageUrl()) + "_o." + this.originalFormat;
        }
        return ((Object) getOriginalBaseImageUrl()) + DEFAULT_ORIGINAL_IMAGE_SUFFIX;
    }

    public BufferedImage getSmallSquareImage() throws IOException {
        return getImage(SMALL_SQUARE_IMAGE_SUFFIX);
    }

    public InputStream getSmallSquareAsInputStream() throws IOException {
        return getImageAsStream(SMALL_SQUARE_IMAGE_SUFFIX);
    }

    public String getSmallSquareUrl() {
        if (this.squareSize == null) {
            return ((Object) getBaseImageUrl()) + SMALL_SQUARE_IMAGE_SUFFIX;
        }
        return this.squareSize.getSource();
    }

    public BufferedImage getThumbnailImage() throws IOException {
        return getImage(THUMBNAIL_IMAGE_SUFFIX);
    }

    public InputStream getThumbnailAsInputStream() throws IOException {
        return getImageAsStream(THUMBNAIL_IMAGE_SUFFIX);
    }

    public String getThumbnailUrl() {
        if (this.thumbnailSize == null) {
            return ((Object) getBaseImageUrl()) + THUMBNAIL_IMAGE_SUFFIX;
        }
        return this.thumbnailSize.getSource();
    }

    public BufferedImage getSmallImage() throws IOException {
        return getImage(SMALL_IMAGE_SUFFIX);
    }

    public InputStream getSmallAsInputStream() throws IOException {
        return getImageAsStream(SMALL_IMAGE_SUFFIX);
    }

    public String getSmallUrl() {
        if (this.smallSize == null) {
            return ((Object) getBaseImageUrl()) + SMALL_IMAGE_SUFFIX;
        }
        return this.smallSize.getSource();
    }

    public BufferedImage getMediumImage() throws IOException {
        return getImage(MEDIUM_IMAGE_SUFFIX);
    }

    public InputStream getMediumAsStream() throws IOException {
        return getImageAsStream(MEDIUM_IMAGE_SUFFIX);
    }

    public String getMediumUrl() {
        if (this.mediumSize == null) {
            return ((Object) getBaseImageUrl()) + MEDIUM_IMAGE_SUFFIX;
        }
        return this.mediumSize.getSource();
    }

    public BufferedImage getLargeImage() throws IOException {
        return getImage(LARGE_IMAGE_SUFFIX);
    }

    public InputStream getLargeAsStream() throws IOException {
        return getImageAsStream(LARGE_IMAGE_SUFFIX);
    }

    public String getLargeUrl() {
        if (this.largeSize == null) {
            return ((Object) getBaseImageUrl()) + LARGE_IMAGE_SUFFIX;
        }
        return this.largeSize.getSource();
    }

    private BufferedImage getImage(String suffix) throws IOException {
        StringBuffer buffer = getBaseImageUrl();
        buffer.append(suffix);
        return _getImage(buffer.toString());
    }

    private BufferedImage getOriginalImage(String suffix) throws IOException, FlickrException {
        StringBuffer buffer = getOriginalBaseImageUrl();
        buffer.append(suffix);
        return _getImage(buffer.toString());
    }

    private BufferedImage _getImage(String urlStr) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(urlStr).openConnection();
        conn.connect();
        InputStream in = null;
        try {
            in = conn.getInputStream();
            return ImageIO.read(in);
        } finally {
            IOUtilities.close(in);
        }
    }

    private InputStream getImageAsStream(String suffix) throws IOException {
        StringBuffer buffer = getBaseImageUrl();
        buffer.append(suffix);
        return _getImageAsStream(buffer.toString());
    }

    private InputStream getOriginalImageAsStream(String suffix) throws IOException, FlickrException {
        StringBuffer buffer = getOriginalBaseImageUrl();
        buffer.append(suffix);
        return _getImageAsStream(buffer.toString());
    }

    private InputStream _getImageAsStream(String urlStr) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(urlStr).openConnection();
        conn.connect();
        return conn.getInputStream();
    }

    private StringBuffer getBaseImageUrl() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(_getBaseImageUrl());
        buffer.append(getSecret());
        return buffer;
    }

    private StringBuffer getOriginalBaseImageUrl() throws FlickrException, NullPointerException {
        StringBuffer buffer = new StringBuffer();
        buffer.append(_getBaseImageUrl());
        if (getOriginalSecret().length() > 8) {
            buffer.append(getOriginalSecret());
            return buffer;
        }
        throw new FlickrException("0", "OriginalUrl not available because of missing originalsecret.");
    }

    private StringBuffer _getBaseImageUrl() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("http://farm");
        buffer.append(getFarm());
        buffer.append(".static.flickr.com/");
        buffer.append(getServer());
        buffer.append("/");
        buffer.append(getId());
        buffer.append("_");
        return buffer;
    }

    public String getPlaceId() {
        return this.placeId;
    }

    public void setPlaceId(String placeId2) {
        this.placeId = placeId2;
    }

    public String getMedia() {
        return this.media;
    }

    public void setMedia(String media2) {
        this.media = media2;
    }

    public String getMediaStatus() {
        return this.mediaStatus;
    }

    public void setMediaStatus(String mediaStatus2) {
        this.mediaStatus = mediaStatus2;
    }

    public int getOriginalWidth() {
        return this.originalWidth;
    }

    public void setOriginalWidth(String originalWidth2) {
        try {
            setOriginalWidth(Integer.parseInt(originalWidth2));
        } catch (NumberFormatException e) {
        }
    }

    public void setOriginalWidth(int originalWidth2) {
        this.originalWidth = originalWidth2;
    }

    public int getOriginalHeight() {
        return this.originalHeight;
    }

    public void setOriginalHeight(String originalHeight2) {
        try {
            setOriginalHeight(Integer.parseInt(originalHeight2));
        } catch (NumberFormatException e) {
        }
    }

    public void setOriginalHeight(int originalHeight2) {
        this.originalHeight = originalHeight2;
    }

    public void setSizes(Collection sizes) {
        Iterator it = sizes.iterator();
        while (it.hasNext()) {
            Size size = (Size) it.next();
            if (size.getLabel() == 2) {
                this.smallSize = size;
            } else if (size.getLabel() == 1) {
                this.squareSize = size;
            } else if (size.getLabel() == 0) {
                this.thumbnailSize = size;
            } else if (size.getLabel() == 3) {
                this.mediumSize = size;
            } else if (size.getLabel() == 4) {
                this.largeSize = size;
            } else if (size.getLabel() == 5) {
                this.originalSize = size;
            }
        }
    }

    public Size getSquareSize() {
        return this.squareSize;
    }

    public Size getSmallSize() {
        return this.smallSize;
    }

    public Size getThumbnailSize() {
        return this.thumbnailSize;
    }

    public Size getMediumSize() {
        return this.mediumSize;
    }

    public Size getLargeSize() {
        return this.largeSize;
    }

    public Size getOriginalSize() {
        return this.originalSize;
    }

    public String getPathAlias() {
        return this.pathAlias;
    }

    public void setPathAlias(String pathAlias2) {
        this.pathAlias = pathAlias2;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Photo test = (Photo) obj;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                try {
                    Object res = method[i].invoke(this, null);
                    Object resTest = method[i].invoke(test, null);
                    String retType = method[i].getReturnType().toString();
                    if (retType.indexOf("class") == 0) {
                        if (res == null || resTest == null) {
                            if (!(res == null && resTest == null) && (res == null || resTest == null)) {
                                return false;
                            }
                        } else if (!res.equals(resTest)) {
                            return false;
                        }
                    } else if (retType.equals("int")) {
                        if (!((Integer) res).equals((Integer) resTest)) {
                            return false;
                        }
                    } else if (retType.equals("boolean")) {
                        if (!((Boolean) res).equals((Boolean) resTest)) {
                            return false;
                        }
                    } else if (!retType.equals("interface java.util.Collection")) {
                        System.out.println("Photo#equals() missing type " + method[i].getName() + "|" + method[i].getReturnType().toString());
                    } else if (res != null && resTest != null) {
                        List col = (List) res;
                        List colTest = (List) resTest;
                        if (col.size() != colTest.size()) {
                            return false;
                        }
                        for (int j = 0; j < col.size(); j++) {
                            Object tobj1 = col.get(j);
                            Object tobj2 = colTest.get(j);
                            if (tobj1 instanceof Tag) {
                                if (!((Tag) tobj1).equals((Tag) tobj2)) {
                                    return false;
                                }
                            } else if (tobj1 instanceof PhotoUrl) {
                                if (!((PhotoUrl) tobj1).equals((PhotoUrl) tobj2)) {
                                    return false;
                                }
                            } else if (!(tobj1 instanceof Note)) {
                                System.out.println("Photo unhandled object: " + tobj1.getClass().getName());
                            } else if (!((Note) tobj1).equals((Note) tobj2)) {
                                return false;
                            }
                        }
                        continue;
                    } else if (res == null && resTest != null) {
                        return false;
                    } else {
                        if (res != null && resTest == null) {
                            return false;
                        }
                    }
                } catch (IllegalAccessException e) {
                    System.out.println("equals " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                } catch (Exception e3) {
                    System.out.println("equals " + method[i].getName() + " " + e3);
                }
            }
        }
        return true;
    }

    public int hashCode() {
        int hash = 1;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                Object res = null;
                try {
                    res = method[i].invoke(this, null);
                } catch (IllegalAccessException e) {
                    System.out.println("Photo hashCode " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                }
                if (res != null) {
                    if (res instanceof Boolean) {
                        hash += ((Boolean) res).hashCode();
                    } else if (res instanceof Integer) {
                        hash += ((Integer) res).hashCode();
                    } else if (res instanceof String) {
                        hash += ((String) res).hashCode();
                    } else if (res instanceof Editability) {
                        hash += ((Editability) res).hashCode();
                    } else if (res instanceof GeoData) {
                        hash += ((GeoData) res).hashCode();
                    } else if (res instanceof Size) {
                        hash += ((Size) res).hashCode();
                    } else if (res instanceof Permissions) {
                        hash += ((Permissions) res).hashCode();
                    } else if (res instanceof User) {
                        hash += ((User) res).hashCode();
                    } else if (res instanceof ArrayList) {
                        hash += ((ArrayList) res).hashCode();
                    } else {
                        System.out.println("Photo hashCode unrecognised object: " + res.getClass().getName());
                    }
                }
            }
        }
        return hash;
    }
}
