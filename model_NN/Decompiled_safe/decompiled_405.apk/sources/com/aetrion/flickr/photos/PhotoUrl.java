package com.aetrion.flickr.photos;

public class PhotoUrl {
    private static final long serialVersionUID = 12;
    private String type;
    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }
}
