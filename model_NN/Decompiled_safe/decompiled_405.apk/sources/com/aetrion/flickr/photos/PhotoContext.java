package com.aetrion.flickr.photos;

public class PhotoContext {
    private static final long serialVersionUID = 12;
    private Photo nextPhoto;
    private Photo previousPhoto;

    public Photo getPreviousPhoto() {
        return this.previousPhoto;
    }

    public void setPreviousPhoto(Photo previousPhoto2) {
        this.previousPhoto = previousPhoto2;
    }

    public Photo getNextPhoto() {
        return this.nextPhoto;
    }

    public void setNextPhoto(Photo nextPhoto2) {
        this.nextPhoto = nextPhoto2;
    }
}
