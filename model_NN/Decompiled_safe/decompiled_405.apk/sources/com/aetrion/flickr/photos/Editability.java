package com.aetrion.flickr.photos;

import com.aetrion.flickr.util.StringUtilities;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Editability {
    private static final long serialVersionUID = 12;
    private boolean addmeta;
    private boolean comment;

    public boolean isComment() {
        return this.comment;
    }

    public void setComment(boolean comment2) {
        this.comment = comment2;
    }

    public boolean isAddmeta() {
        return this.addmeta;
    }

    public void setAddmeta(boolean addmeta2) {
        this.addmeta = addmeta2;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Editability test = (Editability) obj;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                try {
                    Object res = method[i].invoke(this, null);
                    Object resTest = method[i].invoke(test, null);
                    String retType = method[i].getReturnType().toString();
                    if (retType.indexOf("class") == 0) {
                        if (!(res == null || resTest == null || res.equals(resTest))) {
                            return false;
                        }
                    } else if (retType.equals("int")) {
                        if (!((Integer) res).equals((Integer) resTest)) {
                            return false;
                        }
                    } else if (!retType.equals("boolean")) {
                        System.out.println(method[i].getName() + "|" + method[i].getReturnType().toString());
                    } else if (!((Boolean) res).equals((Boolean) resTest)) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    System.out.println("equals " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                } catch (Exception e3) {
                    System.out.println("equals " + method[i].getName() + " " + e3);
                }
            }
        }
        return true;
    }

    public int hashCode() {
        return 1 + new Boolean(this.comment).hashCode() + new Boolean(this.addmeta).hashCode();
    }
}
