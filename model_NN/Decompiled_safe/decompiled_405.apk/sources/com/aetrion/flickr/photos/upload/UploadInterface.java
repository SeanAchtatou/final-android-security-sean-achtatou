package com.aetrion.flickr.photos.upload;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class UploadInterface {
    public static final String METHOD_CHECK_TICKETS = "flickr.photos.upload.checkTickets";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public UploadInterface(String apiKey2, String sharedSecret2, Transport transport) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transport;
    }

    public List checkTickets(Set tickets) throws IOException, SAXException, FlickrException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Parameter("method", METHOD_CHECK_TICKETS));
        arrayList.add(new Parameter("api_key", this.apiKey));
        StringBuffer sb = new StringBuffer();
        for (Object obj : tickets) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            if (obj instanceof Ticket) {
                sb.append(((Ticket) obj).getTicketId());
            } else {
                sb.append(obj);
            }
        }
        arrayList.add(new Parameter("tickets", sb.toString()));
        arrayList.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, arrayList)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), arrayList);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        List list = new ArrayList();
        NodeList ticketNodes = response.getPayload().getElementsByTagName("ticket");
        int n = ticketNodes.getLength();
        for (int i = 0; i < n; i++) {
            Element ticketElement = (Element) ticketNodes.item(i);
            String id = ticketElement.getAttribute("id");
            String complete = ticketElement.getAttribute("complete");
            boolean invalid = "1".equals(ticketElement.getAttribute("invalid"));
            String photoId = ticketElement.getAttribute("photoid");
            Ticket info = new Ticket();
            info.setTicketId(id);
            info.setInvalid(invalid);
            info.setStatus(Integer.parseInt(complete));
            info.setPhotoId(photoId);
            list.add(info);
        }
        return list;
    }
}
