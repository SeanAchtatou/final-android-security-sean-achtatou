package com.aetrion.flickr.photos;

public class Exif {
    private static final long serialVersionUID = 12;
    private String clean;
    private String label;
    private String raw;
    private String tag;
    private String tagspace;
    private String tagspaceId;

    public String getTagspace() {
        return this.tagspace;
    }

    public void setTagspace(String tagspace2) {
        this.tagspace = tagspace2;
    }

    public String getTagspaceId() {
        return this.tagspaceId;
    }

    public void setTagspaceId(String tagspaceId2) {
        this.tagspaceId = tagspaceId2;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag2) {
        this.tag = tag2;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label2) {
        this.label = label2;
    }

    public String getRaw() {
        return this.raw;
    }

    public void setRaw(String raw2) {
        this.raw = raw2;
    }

    public String getClean() {
        return this.clean;
    }

    public void setClean(String clean2) {
        this.clean = clean2;
    }
}
