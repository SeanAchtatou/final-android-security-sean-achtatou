package com.aetrion.flickr.photos.upload;

public class Ticket {
    public static final int COMPLETED = 1;
    public static final int FAILED = 2;
    public static final int UNCOMPLETED = 0;
    private boolean invalid;
    private String photoId;
    private int status;
    private String ticketId;

    public boolean isInvalid() {
        return this.invalid;
    }

    public void setInvalid(boolean invalid2) {
        this.invalid = invalid2;
    }

    public String getPhotoId() {
        return this.photoId;
    }

    public void setPhotoId(String photoId2) {
        this.photoId = photoId2;
    }

    public String getTicketId() {
        return this.ticketId;
    }

    public void setTicketId(String ticketId2) {
        this.ticketId = ticketId2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int complete) {
        this.status = complete;
    }

    public boolean hasCompleted() {
        return this.status == 1;
    }

    public boolean hasFailed() {
        return this.status == 2;
    }

    public boolean isBusy() {
        return this.status == 0;
    }
}
