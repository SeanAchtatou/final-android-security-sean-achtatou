package com.aetrion.flickr.photos;

import com.aetrion.flickr.util.StringUtilities;
import java.awt.Rectangle;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Note {
    private static final long serialVersionUID = 12;
    private String author;
    private String authorName;
    private Rectangle bounds;
    private String id;
    private String text;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author2) {
        this.author = author2;
    }

    public String getAuthorName() {
        return this.authorName;
    }

    public void setAuthorName(String authorName2) {
        this.authorName = authorName2;
    }

    public Rectangle getBounds() {
        return this.bounds;
    }

    public void setBounds(Rectangle bounds2) {
        this.bounds = bounds2;
    }

    public void setBounds(String x, String y, String w, String h) {
        setBounds(new Rectangle(Integer.parseInt(x), Integer.parseInt(y), Integer.parseInt(w), Integer.parseInt(h)));
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Note test = (Note) obj;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                try {
                    Object res = method[i].invoke(this, null);
                    Object resTest = method[i].invoke(test, null);
                    String retType = method[i].getReturnType().toString();
                    if (retType.indexOf("class") == 0) {
                        if (!(res == null || resTest == null || res.equals(resTest))) {
                            return false;
                        }
                    } else if (retType.equals("int")) {
                        if (!((Integer) res).equals((Integer) resTest)) {
                            return false;
                        }
                    } else if (!retType.equals("boolean")) {
                        System.out.println(method[i].getName() + "|" + method[i].getReturnType().toString());
                    } else if (!((Boolean) res).equals((Boolean) resTest)) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    System.out.println("equals " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                } catch (Exception e3) {
                    System.out.println("equals " + method[i].getName() + " " + e3);
                }
            }
        }
        return true;
    }

    public int hashCode() {
        int hash = 1;
        if (this.id != null) {
            hash = 1 + this.id.hashCode();
        }
        if (this.author != null) {
            hash += this.author.hashCode();
        }
        if (this.authorName != null) {
            hash += this.authorName.hashCode();
        }
        if (this.bounds != null) {
            hash += this.bounds.hashCode();
        }
        if (this.text != null) {
            return hash + this.text.hashCode();
        }
        return hash;
    }
}
