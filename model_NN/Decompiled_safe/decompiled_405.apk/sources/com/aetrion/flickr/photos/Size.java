package com.aetrion.flickr.photos;

import com.aetrion.flickr.util.StringUtilities;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Size {
    public static final int LARGE = 4;
    public static final int MEDIUM = 3;
    public static final int ORIGINAL = 5;
    public static final int SMALL = 2;
    public static final int SQUARE = 1;
    public static final int THUMB = 0;
    private static final long serialVersionUID = 12;
    private int height;
    private int label;
    private String source;
    private String url;
    private int width;

    public int getLabel() {
        return this.label;
    }

    public void setLabel(String label2) {
        if (label2.equals("Square")) {
            setLabel(1);
        } else if (label2.equals("Thumbnail")) {
            setLabel(0);
        } else if (label2.equals("Small")) {
            setLabel(2);
        } else if (label2.equals("Medium")) {
            setLabel(3);
        } else if (label2.equals("Large")) {
            setLabel(4);
        } else if (label2.equals("Original")) {
            setLabel(5);
        }
    }

    public void setLabel(int label2) {
        this.label = label2;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public void setWidth(String width2) {
        if (width2 != null) {
            setWidth(Integer.parseInt(width2));
        }
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }

    public void setHeight(String height2) {
        if (height2 != null) {
            setHeight(Integer.parseInt(height2));
        }
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source2) {
        this.source = source2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Size test = (Size) obj;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                try {
                    Object res = method[i].invoke(this, null);
                    Object resTest = method[i].invoke(test, null);
                    String retType = method[i].getReturnType().toString();
                    if (retType.indexOf("class") == 0) {
                        if (!(res == null || resTest == null || res.equals(resTest))) {
                            return false;
                        }
                    } else if (!retType.equals("int")) {
                        System.out.println(method[i].getName() + "|" + method[i].getReturnType().toString());
                    } else if (!((Integer) res).equals((Integer) resTest)) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    System.out.println("Size equals " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                } catch (Exception e3) {
                    System.out.println("Size equals " + method[i].getName() + " " + e3);
                }
            }
        }
        return true;
    }

    public int hashCode() {
        int hash = 1 + new Integer(this.label).hashCode() + new Integer(this.width).hashCode() + new Integer(this.height).hashCode();
        if (this.source != null) {
            hash += this.source.hashCode();
        }
        if (this.url != null) {
            return hash + this.url.hashCode();
        }
        return hash;
    }
}
