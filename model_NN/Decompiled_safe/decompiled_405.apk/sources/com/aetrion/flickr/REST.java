package com.aetrion.flickr;

import com.aetrion.flickr.util.Base64;
import com.aetrion.flickr.util.DebugInputStream;
import com.aetrion.flickr.util.IOUtilities;
import com.aetrion.flickr.util.UrlUtilities;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class REST extends Transport {
    public static final String PATH = "/services/rest/";
    private static final String UTF8 = "UTF-8";
    private static Object mutex = new Object();
    private DocumentBuilder builder;
    private boolean proxyAuth;
    private String proxyPassword;
    private String proxyUser;

    public REST() throws ParserConfigurationException {
        this.proxyAuth = false;
        this.proxyUser = "";
        this.proxyPassword = "";
        setTransportType(Transport.REST);
        setHost(Flickr.DEFAULT_HOST);
        setPath(PATH);
        setResponseClass(RESTResponse.class);
        this.builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }

    public REST(String host) throws ParserConfigurationException {
        this();
        setHost(host);
    }

    public REST(String host, int port) throws ParserConfigurationException {
        this();
        setHost(host);
        setPort(port);
    }

    public void setProxy(String proxyHost, int proxyPort) {
        System.setProperty("http.proxySet", "true");
        System.setProperty("http.proxyHost", proxyHost);
        System.setProperty("http.proxyPort", "" + proxyPort);
    }

    public void setProxy(String proxyHost, int proxyPort, String username, String password) {
        setProxy(proxyHost, proxyPort);
        this.proxyAuth = true;
        this.proxyUser = username;
        this.proxyPassword = password;
    }

    public Response get(String path, List parameters) throws IOException, SAXException {
        Response response;
        URL url = UrlUtilities.buildUrl(getHost(), getPort(), path, parameters);
        if (Flickr.debugRequest) {
            System.out.println("GET: " + url);
        }
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        if (this.proxyAuth) {
            conn.setRequestProperty("Proxy-Authorization", "Basic " + getProxyCredentials());
        }
        conn.connect();
        InputStream in = null;
        try {
            if (Flickr.debugStream) {
                in = new DebugInputStream(conn.getInputStream(), System.out);
            } else {
                in = conn.getInputStream();
            }
            synchronized (mutex) {
                Document document = this.builder.parse(in);
                response = (Response) this.responseClass.newInstance();
                response.parse(document);
            }
            IOUtilities.close(in);
            return response;
        } catch (IllegalAccessException e) {
            try {
                throw new RuntimeException(e);
            } catch (Throwable th) {
                IOUtilities.close(in);
                throw th;
            }
        } catch (InstantiationException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.aetrion.flickr.Response post(java.lang.String r23, java.util.List r24, boolean r25) throws java.io.IOException, org.xml.sax.SAXException {
        /*
            r22 = this;
            com.aetrion.flickr.RequestContext r16 = com.aetrion.flickr.RequestContext.getRequestContext()
            java.lang.String r19 = r22.getHost()
            int r20 = r22.getPort()
            r0 = r19
            r1 = r20
            r2 = r23
            java.net.URL r18 = com.aetrion.flickr.util.UrlUtilities.buildPostUrl(r0, r1, r2)
            r7 = 0
            java.lang.String r6 = "---------------------------7d273f7a0d3"
            java.net.URLConnection r8 = r18.openConnection()     // Catch:{ all -> 0x00e5 }
            r0 = r8
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x00e5 }
            r7 = r0
            r0 = r22
            boolean r0 = r0.proxyAuth     // Catch:{ all -> 0x00e5 }
            r19 = r0
            if (r19 == 0) goto L_0x004a
            java.lang.String r19 = "Proxy-Authorization"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            r20.<init>()     // Catch:{ all -> 0x00e5 }
            java.lang.String r21 = "Basic "
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ all -> 0x00e5 }
            java.lang.String r21 = r22.getProxyCredentials()     // Catch:{ all -> 0x00e5 }
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ all -> 0x00e5 }
            java.lang.String r20 = r20.toString()     // Catch:{ all -> 0x00e5 }
            r0 = r7
            r1 = r19
            r2 = r20
            r0.setRequestProperty(r1, r2)     // Catch:{ all -> 0x00e5 }
        L_0x004a:
            r19 = 1
            r0 = r7
            r1 = r19
            r0.setDoOutput(r1)     // Catch:{ all -> 0x00e5 }
            java.lang.String r19 = "POST"
            r0 = r7
            r1 = r19
            r0.setRequestMethod(r1)     // Catch:{ all -> 0x00e5 }
            if (r25 == 0) goto L_0x007c
            java.lang.String r19 = "Content-Type"
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            r20.<init>()     // Catch:{ all -> 0x00e5 }
            java.lang.String r21 = "multipart/form-data; boundary="
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ all -> 0x00e5 }
            r0 = r20
            r1 = r6
            java.lang.StringBuilder r20 = r0.append(r1)     // Catch:{ all -> 0x00e5 }
            java.lang.String r20 = r20.toString()     // Catch:{ all -> 0x00e5 }
            r0 = r7
            r1 = r19
            r2 = r20
            r0.setRequestProperty(r1, r2)     // Catch:{ all -> 0x00e5 }
        L_0x007c:
            r7.connect()     // Catch:{ all -> 0x00e5 }
            r13 = 0
            boolean r19 = com.aetrion.flickr.Flickr.debugRequest     // Catch:{ all -> 0x00e0 }
            if (r19 == 0) goto L_0x00ec
            java.io.DataOutputStream r14 = new java.io.DataOutputStream     // Catch:{ all -> 0x00e0 }
            com.aetrion.flickr.util.DebugOutputStream r19 = new com.aetrion.flickr.util.DebugOutputStream     // Catch:{ all -> 0x00e0 }
            java.io.OutputStream r20 = r7.getOutputStream()     // Catch:{ all -> 0x00e0 }
            java.io.PrintStream r21 = java.lang.System.out     // Catch:{ all -> 0x00e0 }
            r19.<init>(r20, r21)     // Catch:{ all -> 0x00e0 }
            r0 = r14
            r1 = r19
            r0.<init>(r1)     // Catch:{ all -> 0x00e0 }
            r13 = r14
        L_0x0098:
            if (r25 == 0) goto L_0x00fa
            java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e0 }
            r19.<init>()     // Catch:{ all -> 0x00e0 }
            java.lang.String r20 = "--"
            java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ all -> 0x00e0 }
            r0 = r19
            r1 = r6
            java.lang.StringBuilder r19 = r0.append(r1)     // Catch:{ all -> 0x00e0 }
            java.lang.String r20 = "\r\n"
            java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ all -> 0x00e0 }
            java.lang.String r19 = r19.toString()     // Catch:{ all -> 0x00e0 }
            r0 = r13
            r1 = r19
            r0.writeBytes(r1)     // Catch:{ all -> 0x00e0 }
            java.util.Iterator r12 = r24.iterator()     // Catch:{ all -> 0x00e0 }
        L_0x00c0:
            boolean r19 = r12.hasNext()     // Catch:{ all -> 0x00e0 }
            if (r19 == 0) goto L_0x0145
            java.lang.Object r15 = r12.next()     // Catch:{ all -> 0x00e0 }
            com.aetrion.flickr.Parameter r15 = (com.aetrion.flickr.Parameter) r15     // Catch:{ all -> 0x00e0 }
            java.lang.String r19 = r15.getName()     // Catch:{ all -> 0x00e0 }
            java.lang.Object r20 = r15.getValue()     // Catch:{ all -> 0x00e0 }
            r0 = r22
            r1 = r19
            r2 = r20
            r3 = r13
            r4 = r6
            r0.writeParam(r1, r2, r3, r4)     // Catch:{ all -> 0x00e0 }
            goto L_0x00c0
        L_0x00e0:
            r19 = move-exception
            com.aetrion.flickr.util.IOUtilities.close(r13)     // Catch:{ all -> 0x00e5 }
            throw r19     // Catch:{ all -> 0x00e5 }
        L_0x00e5:
            r19 = move-exception
            if (r7 == 0) goto L_0x00eb
            r7.disconnect()
        L_0x00eb:
            throw r19
        L_0x00ec:
            java.io.DataOutputStream r14 = new java.io.DataOutputStream     // Catch:{ all -> 0x00e0 }
            java.io.OutputStream r19 = r7.getOutputStream()     // Catch:{ all -> 0x00e0 }
            r0 = r14
            r1 = r19
            r0.<init>(r1)     // Catch:{ all -> 0x00e0 }
            r13 = r14
            goto L_0x0098
        L_0x00fa:
            java.util.Iterator r12 = r24.iterator()     // Catch:{ all -> 0x00e0 }
        L_0x00fe:
            boolean r19 = r12.hasNext()     // Catch:{ all -> 0x00e0 }
            if (r19 == 0) goto L_0x013f
            java.lang.Object r15 = r12.next()     // Catch:{ all -> 0x00e0 }
            com.aetrion.flickr.Parameter r15 = (com.aetrion.flickr.Parameter) r15     // Catch:{ all -> 0x00e0 }
            java.lang.String r19 = r15.getName()     // Catch:{ all -> 0x00e0 }
            r0 = r13
            r1 = r19
            r0.writeBytes(r1)     // Catch:{ all -> 0x00e0 }
            java.lang.String r19 = "="
            r0 = r13
            r1 = r19
            r0.writeBytes(r1)     // Catch:{ all -> 0x00e0 }
            java.lang.Object r19 = r15.getValue()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r19 = java.lang.String.valueOf(r19)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r20 = "UTF-8"
            java.lang.String r19 = java.net.URLEncoder.encode(r19, r20)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r0 = r13
            r1 = r19
            r0.writeBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
        L_0x0130:
            boolean r19 = r12.hasNext()     // Catch:{ all -> 0x00e0 }
            if (r19 == 0) goto L_0x00fe
            java.lang.String r19 = "&"
            r0 = r13
            r1 = r19
            r0.writeBytes(r1)     // Catch:{ all -> 0x00e0 }
            goto L_0x00fe
        L_0x013f:
            com.aetrion.flickr.auth.Auth r5 = r16.getAuth()     // Catch:{ all -> 0x00e0 }
            if (r5 == 0) goto L_0x0145
        L_0x0145:
            r13.flush()     // Catch:{ all -> 0x00e0 }
            com.aetrion.flickr.util.IOUtilities.close(r13)     // Catch:{ all -> 0x00e5 }
            r10 = 0
            boolean r19 = com.aetrion.flickr.Flickr.debugStream     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
            if (r19 == 0) goto L_0x0193
            com.aetrion.flickr.util.DebugInputStream r11 = new com.aetrion.flickr.util.DebugInputStream     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
            java.io.InputStream r19 = r7.getInputStream()     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
            java.io.PrintStream r20 = java.lang.System.out     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
            r0 = r11
            r1 = r19
            r2 = r20
            r0.<init>(r1, r2)     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
            r10 = r11
        L_0x0161:
            r17 = 0
            java.lang.Object r19 = com.aetrion.flickr.REST.mutex     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
            monitor-enter(r19)     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
            r0 = r22
            javax.xml.parsers.DocumentBuilder r0 = r0.builder     // Catch:{ all -> 0x0198 }
            r20 = r0
            r0 = r20
            r1 = r10
            org.w3c.dom.Document r8 = r0.parse(r1)     // Catch:{ all -> 0x0198 }
            r0 = r22
            java.lang.Class r0 = r0.responseClass     // Catch:{ all -> 0x0198 }
            r20 = r0
            java.lang.Object r18 = r20.newInstance()     // Catch:{ all -> 0x0198 }
            r0 = r18
            com.aetrion.flickr.Response r0 = (com.aetrion.flickr.Response) r0     // Catch:{ all -> 0x0198 }
            r17 = r0
            r0 = r17
            r1 = r8
            r0.parse(r1)     // Catch:{ all -> 0x0198 }
            monitor-exit(r19)     // Catch:{ all -> 0x0198 }
            com.aetrion.flickr.util.IOUtilities.close(r10)     // Catch:{ all -> 0x00e5 }
            if (r7 == 0) goto L_0x0192
            r7.disconnect()
        L_0x0192:
            return r17
        L_0x0193:
            java.io.InputStream r10 = r7.getInputStream()     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
            goto L_0x0161
        L_0x0198:
            r20 = move-exception
            monitor-exit(r19)     // Catch:{ all -> 0x0198 }
            throw r20     // Catch:{ IllegalAccessException -> 0x019b, InstantiationException -> 0x01ac }
        L_0x019b:
            r19 = move-exception
            r9 = r19
            java.lang.RuntimeException r19 = new java.lang.RuntimeException     // Catch:{ all -> 0x01a7 }
            r0 = r19
            r1 = r9
            r0.<init>(r1)     // Catch:{ all -> 0x01a7 }
            throw r19     // Catch:{ all -> 0x01a7 }
        L_0x01a7:
            r19 = move-exception
            com.aetrion.flickr.util.IOUtilities.close(r10)     // Catch:{ all -> 0x00e5 }
            throw r19     // Catch:{ all -> 0x00e5 }
        L_0x01ac:
            r19 = move-exception
            r9 = r19
            java.lang.RuntimeException r19 = new java.lang.RuntimeException     // Catch:{ all -> 0x01a7 }
            r0 = r19
            r1 = r9
            r0.<init>(r1)     // Catch:{ all -> 0x01a7 }
            throw r19     // Catch:{ all -> 0x01a7 }
        L_0x01b8:
            r19 = move-exception
            goto L_0x0130
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aetrion.flickr.REST.post(java.lang.String, java.util.List, boolean):com.aetrion.flickr.Response");
    }

    private void writeParam(String name, Object value, DataOutputStream out, String boundary) throws IOException {
        if (value instanceof InputStream) {
            out.writeBytes("Content-Disposition: form-data; name=\"" + name + "\"; filename=\"image.jpg\";\r\n");
            out.writeBytes("Content-Type: image/jpeg\r\n\r\n");
            InputStream in = (InputStream) value;
            byte[] buf = new byte[512];
            while (in.read(buf) != -1) {
                out.write(buf);
            }
            out.writeBytes("\r\n--" + boundary + "\r\n");
        } else if (value instanceof byte[]) {
            out.writeBytes("Content-Disposition: form-data; name=\"" + name + "\"; filename=\"image.jpg\";\r\n");
            out.writeBytes("Content-Type: image/jpeg\r\n\r\n");
            out.write((byte[]) ((byte[]) value));
            out.writeBytes("\r\n--" + boundary + "\r\n");
        } else {
            out.writeBytes("Content-Disposition: form-data; name=\"" + name + "\"\r\n\r\n");
            out.write(((String) value).getBytes("UTF-8"));
            out.writeBytes("\r\n--" + boundary + "\r\n");
        }
    }

    public boolean isProxyAuth() {
        return this.proxyAuth;
    }

    public String getProxyCredentials() {
        return new String(Base64.encode((this.proxyUser + ":" + this.proxyPassword).getBytes()));
    }
}
