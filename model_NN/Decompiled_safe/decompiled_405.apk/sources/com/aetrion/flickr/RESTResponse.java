package com.aetrion.flickr;

import com.aetrion.flickr.util.XMLUtilities;
import java.util.Collection;
import java.util.Iterator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class RESTResponse implements Response {
    private String errorCode;
    private String errorMessage;
    private Collection payload;
    private String stat;

    public void parse(Document document) {
        Element rspElement = document.getDocumentElement();
        rspElement.normalize();
        this.stat = rspElement.getAttribute("stat");
        if ("ok".equals(this.stat)) {
            this.payload = XMLUtilities.getChildElements(rspElement);
        } else if ("fail".equals(this.stat)) {
            Element errElement = (Element) rspElement.getElementsByTagName("err").item(0);
            this.errorCode = errElement.getAttribute("code");
            this.errorMessage = errElement.getAttribute("msg");
        }
    }

    public String getStat() {
        return this.stat;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Element getPayload() {
        Iterator iter = this.payload.iterator();
        if (iter.hasNext()) {
            return (Element) iter.next();
        }
        throw new RuntimeException("REST response payload has no elements");
    }

    public Collection getPayloadCollection() {
        return this.payload;
    }

    public boolean isError() {
        return this.errorCode != null;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
}
