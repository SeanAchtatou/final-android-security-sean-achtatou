package com.aetrion.flickr.people;

import com.aetrion.flickr.contacts.OnlineStatus;
import com.aetrion.flickr.util.BuddyIconable;
import com.aetrion.flickr.util.StringUtilities;
import com.aetrion.flickr.util.UrlUtilities;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User implements Serializable, BuddyIconable {
    private static final ThreadLocal DATE_FORMATS = new ThreadLocal() {
        /* access modifiers changed from: protected */
        public synchronized Object initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };
    private static final long serialVersionUID = 12;
    private boolean admin;
    private String awayMessage;
    private long bandwidthMax;
    private long bandwidthUsed;
    private Date faveDate;
    private long filesizeMax;
    private int iconFarm;
    private int iconServer;
    private String id;
    private String location;
    private String mbox_sha1sum;
    private OnlineStatus online;
    private int photosCount;
    private Date photosFirstDate;
    private Date photosFirstDateTaken;
    private boolean pro;
    private String realName;
    private String username;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public boolean isAdmin() {
        return this.admin;
    }

    public void setAdmin(boolean admin2) {
        this.admin = admin2;
    }

    public boolean isPro() {
        return this.pro;
    }

    public void setPro(boolean pro2) {
        this.pro = pro2;
    }

    public int getIconFarm() {
        return this.iconFarm;
    }

    public void setIconFarm(int iconFarm2) {
        this.iconFarm = iconFarm2;
    }

    public void setIconFarm(String iconFarm2) {
        if (iconFarm2 != null) {
            setIconFarm(Integer.parseInt(iconFarm2));
        }
    }

    public int getIconServer() {
        return this.iconServer;
    }

    public void setIconServer(int iconServer2) {
        this.iconServer = iconServer2;
    }

    public void setIconServer(String iconServer2) {
        if (iconServer2 != null) {
            setIconServer(Integer.parseInt(iconServer2));
        }
    }

    public String getRealName() {
        return this.realName;
    }

    public void setRealName(String realName2) {
        this.realName = realName2;
    }

    public String getLocation() {
        return this.location;
    }

    public String getBuddyIconUrl() {
        return UrlUtilities.createBuddyIconUrl(this.iconFarm, this.iconServer, this.id);
    }

    public void setLocation(String location2) {
        this.location = location2;
    }

    public Date getPhotosFirstDate() {
        return this.photosFirstDate;
    }

    public void setPhotosFirstDate(Date photosFirstDate2) {
        this.photosFirstDate = photosFirstDate2;
    }

    public void setPhotosFirstDate(long photosFirstDate2) {
        setPhotosFirstDate(new Date(photosFirstDate2));
    }

    public void setPhotosFirstDate(String photosFirstDate2) {
        if (photosFirstDate2 != null) {
            setPhotosFirstDate(Long.parseLong(photosFirstDate2) * 1000);
        }
    }

    public Date getPhotosFirstDateTaken() {
        return this.photosFirstDateTaken;
    }

    public void setPhotosFirstDateTaken(Date photosFirstDateTaken2) {
        this.photosFirstDateTaken = photosFirstDateTaken2;
    }

    public void setPhotosFirstDateTaken(String photosFirstDateTaken2) {
        if (photosFirstDateTaken2 != null) {
            try {
                setPhotosFirstDateTaken(((DateFormat) DATE_FORMATS.get()).parse(photosFirstDateTaken2));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void setFaveDate(String faveDate2) {
        setFaveDate(Long.parseLong(faveDate2) * 1000);
    }

    public void setFaveDate(long faveDate2) {
        setFaveDate(new Date(faveDate2));
    }

    public void setFaveDate(Date faveDate2) {
        this.faveDate = faveDate2;
    }

    public Date getFaveDate() {
        return this.faveDate;
    }

    public int getPhotosCount() {
        return this.photosCount;
    }

    public void setPhotosCount(int photosCount2) {
        this.photosCount = photosCount2;
    }

    public void setPhotosCount(String photosCount2) {
        if (photosCount2 != null) {
            setPhotosCount(Integer.parseInt(photosCount2));
        }
    }

    public OnlineStatus getOnline() {
        return this.online;
    }

    public void setOnline(OnlineStatus online2) {
        this.online = online2;
    }

    public String getAwayMessage() {
        return this.awayMessage;
    }

    public void setAwayMessage(String awayMessage2) {
        this.awayMessage = awayMessage2;
    }

    public long getBandwidthMax() {
        return this.bandwidthMax;
    }

    public void setBandwidthMax(long bandwidthMax2) {
        this.bandwidthMax = bandwidthMax2;
    }

    public void setBandwidthMax(String bandwidthMax2) {
        if (bandwidthMax2 != null) {
            setBandwidthMax(Long.parseLong(bandwidthMax2));
        }
    }

    public long getBandwidthUsed() {
        return this.bandwidthUsed;
    }

    public void setBandwidthUsed(long bandwidthUsed2) {
        this.bandwidthUsed = bandwidthUsed2;
    }

    public void setBandwidthUsed(String bandwidthUsed2) {
        if (bandwidthUsed2 != null) {
            setBandwidthUsed(Long.parseLong(bandwidthUsed2));
        }
    }

    public long getFilesizeMax() {
        return this.filesizeMax;
    }

    public void setFilesizeMax(long filesizeMax2) {
        this.filesizeMax = filesizeMax2;
    }

    public void setFilesizeMax(String filesizeMax2) {
        if (filesizeMax2 != null) {
            setFilesizeMax(Long.parseLong(filesizeMax2));
        }
    }

    public void setMbox_sha1sum(String mbox_sha1sum2) {
        this.mbox_sha1sum = mbox_sha1sum2;
    }

    public String getMbox_sha1sum() {
        return this.mbox_sha1sum;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        User test = (User) obj;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                try {
                    Object res = method[i].invoke(this, null);
                    Object resTest = method[i].invoke(test, null);
                    String retType = method[i].getReturnType().toString();
                    if (retType.indexOf("class") == 0) {
                        if (res == null || resTest == null) {
                            if (!(res == null && resTest == null) && (res == null || resTest == null)) {
                                return false;
                            }
                        } else if (!res.equals(resTest)) {
                            return false;
                        }
                    } else if (retType.equals("int")) {
                        if (!((Integer) res).equals((Integer) resTest)) {
                            return false;
                        }
                    } else if (retType.equals("boolean")) {
                        if (!((Boolean) res).equals((Boolean) resTest)) {
                            return false;
                        }
                    } else if (!retType.equals("long")) {
                        System.out.println("User#equals() missing type " + method[i].getName() + "|" + method[i].getReturnType().toString());
                    } else if (!((Long) res).equals((Long) resTest)) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    System.out.println("equals " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                } catch (Exception e3) {
                    System.out.println("equals " + method[i].getName() + " " + e3);
                }
            }
        }
        return true;
    }

    public int hashCode() {
        int hash = 1;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                Object res = null;
                try {
                    res = method[i].invoke(this, null);
                } catch (IllegalAccessException e) {
                    System.out.println("hashCode " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                }
                if (res != null) {
                    if (res instanceof Boolean) {
                        hash += ((Boolean) res).hashCode();
                    } else if (res instanceof Integer) {
                        hash += ((Integer) res).hashCode();
                    } else if (res instanceof String) {
                        hash += ((String) res).hashCode();
                    } else if (res instanceof Long) {
                        hash += ((Long) res).hashCode();
                    } else if (res instanceof OnlineStatus) {
                        hash += ((OnlineStatus) res).hashCode();
                    } else {
                        System.out.println("User hashCode unrecognised object: " + res.getClass().getName());
                    }
                }
            }
        }
        return hash;
    }
}
