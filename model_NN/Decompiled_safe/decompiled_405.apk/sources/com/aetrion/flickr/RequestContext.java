package com.aetrion.flickr;

import com.aetrion.flickr.auth.Auth;
import java.util.ArrayList;
import java.util.List;

public class RequestContext {
    private static RequestContextThreadLocal threadLocal = new RequestContextThreadLocal();
    private Auth auth;
    private List extras;
    private String sharedSecret;

    public static RequestContext getRequestContext() {
        return (RequestContext) threadLocal.get();
    }

    public Auth getAuth() {
        return this.auth;
    }

    public void setAuth(Auth auth2) {
        this.auth = auth2;
    }

    public String getSharedSecret() {
        return this.sharedSecret;
    }

    public void setSharedSecret(String sharedSecret2) {
        this.sharedSecret = sharedSecret2;
    }

    public List getExtras() {
        if (this.extras == null) {
            this.extras = new ArrayList();
        }
        return this.extras;
    }

    public void setExtras(List extras2) {
        this.extras = extras2;
    }

    private static class RequestContextThreadLocal extends ThreadLocal {
        private RequestContextThreadLocal() {
        }

        /* access modifiers changed from: protected */
        public Object initialValue() {
            return new RequestContext();
        }
    }
}
