package com.aetrion.flickr.contacts;

import com.aetrion.flickr.util.BuddyIconable;
import com.aetrion.flickr.util.UrlUtilities;

public class Contact implements BuddyIconable {
    private static final long serialVersionUID = 12;
    private String awayMessage;
    private boolean family;
    private boolean friend;
    private int iconFarm;
    private int iconServer;
    private String id;
    private boolean ignored;
    private OnlineStatus online;
    private String realName;
    private String username;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getRealName() {
        return this.realName;
    }

    public void setRealName(String realName2) {
        this.realName = realName2;
    }

    public boolean isFriend() {
        return this.friend;
    }

    public void setFriend(boolean friend2) {
        this.friend = friend2;
    }

    public boolean isFamily() {
        return this.family;
    }

    public void setFamily(boolean family2) {
        this.family = family2;
    }

    public boolean isIgnored() {
        return this.ignored;
    }

    public void setIgnored(boolean ignored2) {
        this.ignored = ignored2;
    }

    public OnlineStatus getOnline() {
        return this.online;
    }

    public void setOnline(OnlineStatus online2) {
        this.online = online2;
    }

    public String getAwayMessage() {
        return this.awayMessage;
    }

    public void setAwayMessage(String awayMessage2) {
        this.awayMessage = awayMessage2;
    }

    public String getBuddyIconUrl() {
        return UrlUtilities.createBuddyIconUrl(this.iconFarm, this.iconServer, this.id);
    }

    public int getIconFarm() {
        return this.iconFarm;
    }

    public void setIconFarm(int iconFarm2) {
        this.iconFarm = iconFarm2;
    }

    public void setIconFarm(String iconFarm2) {
        setIconFarm(Integer.parseInt(iconFarm2));
    }

    public int getIconServer() {
        return this.iconServer;
    }

    public void setIconServer(int iconServer2) {
        this.iconServer = iconServer2;
    }

    public void setIconServer(String iconServer2) {
        setIconServer(Integer.parseInt(iconServer2));
    }
}
