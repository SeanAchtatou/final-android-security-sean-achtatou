package com.aetrion.flickr.contacts;

import java.io.Serializable;

public class OnlineStatus implements Serializable {
    public static final OnlineStatus AWAY = new OnlineStatus(1);
    public static final int AWAY_TYPE = 1;
    public static final OnlineStatus OFFLINE = new OnlineStatus(0);
    public static final int OFFLINE_TYPE = 0;
    public static final OnlineStatus ONLINE = new OnlineStatus(2);
    public static final int ONLINE_TYPE = 2;
    public static final OnlineStatus UNKNOWN = new OnlineStatus(100);
    public static final int UNKNOWN_TYPE = 100;
    private static final long serialVersionUID = 12;
    private int type;

    private OnlineStatus(int type2) {
        this.type = type2;
    }

    public int getType() {
        return this.type;
    }

    public static OnlineStatus fromType(int type2) {
        switch (type2) {
            case 0:
                return OFFLINE;
            case 1:
                return AWAY;
            case 2:
                return ONLINE;
            case UNKNOWN_TYPE /*100*/:
                return UNKNOWN;
            default:
                throw new IllegalArgumentException("Unsupported online type: " + type2);
        }
    }

    public static OnlineStatus fromType(String type2) {
        if (type2 == null || "".equals(type2)) {
            return UNKNOWN;
        }
        return fromType(Integer.parseInt(type2));
    }
}
