package com.aetrion.flickr;

import java.io.IOException;
import java.util.List;
import org.xml.sax.SAXException;

public abstract class Transport {
    public static final String REST = "REST";
    public static final String SOAP = "SOAP";
    private String host;
    private String path;
    private int port = 80;
    protected Class responseClass;
    private String transportType;

    public abstract Response get(String str, List list) throws IOException, SAXException;

    public abstract Response post(String str, List list, boolean z) throws IOException, SAXException;

    public String getHost() {
        return this.host;
    }

    public void setHost(String host2) {
        this.host = host2;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port2) {
        this.port = port2;
    }

    public String getTransportType() {
        return this.transportType;
    }

    public void setTransportType(String transport) {
        this.transportType = transport;
    }

    public Response post(String path2, List parameters) throws IOException, SAXException {
        return post(path2, parameters, false);
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public Class getResponseClass() {
        return this.responseClass;
    }

    public void setResponseClass(Class responseClass2) {
        if (responseClass2 == null) {
            throw new IllegalArgumentException("The response Class cannot be null");
        }
        this.responseClass = responseClass2;
    }
}
