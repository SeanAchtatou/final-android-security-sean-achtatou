package com.aetrion.flickr.urls;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.groups.Group;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class UrlsInterface {
    public static final String METHOD_GET_GROUP = "flickr.urls.getGroup";
    public static final String METHOD_GET_USER_PHOTOS = "flickr.urls.getUserPhotos";
    public static final String METHOD_GET_USER_PROFILE = "flickr.urls.getUserProfile";
    public static final String METHOD_LOOKUP_GROUP = "flickr.urls.lookupGroup";
    public static final String METHOD_LOOKUP_USER = "flickr.urls.lookupUser";
    private String apiKey;
    private String sharedSecret;
    private Transport transport;

    public UrlsInterface(String apiKey2, String sharedSecret2, Transport transportAPI) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transport = transportAPI;
    }

    public String getGroup(String groupId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_GROUP));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("group_id", groupId));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayload().getAttribute("url");
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public String getUserPhotos(String userId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_USER_PHOTOS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("user_id", userId));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayload().getAttribute("url");
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public String getUserProfile(String userId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_USER_PROFILE));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("user_id", userId));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayload().getAttribute("url");
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public Group lookupGroup(String url) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_LOOKUP_GROUP));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("url", url));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Group group = new Group();
        Element payload = response.getPayload();
        group.setId(payload.getAttribute("id"));
        group.setName(((Text) ((Element) payload.getElementsByTagName("groupname").item(0)).getFirstChild()).getData());
        return group;
    }

    public String lookupUser(String url) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_LOOKUP_USER));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("url", url));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (!response.isError()) {
            return ((Text) ((Element) response.getPayload().getElementsByTagName("username").item(0)).getFirstChild()).getData();
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }
}
