package com.aetrion.flickr;

public class FlickrException extends Exception {
    private static final long serialVersionUID = 7958091410349084831L;
    private String errorCode;
    private String errorMessage;

    public FlickrException(String errorCode2, String errorMessage2) {
        super(errorCode2 + ": " + errorMessage2);
        this.errorCode = errorCode2;
        this.errorMessage = errorMessage2;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
}
