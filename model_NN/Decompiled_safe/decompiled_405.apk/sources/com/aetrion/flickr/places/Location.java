package com.aetrion.flickr.places;

public class Location {
    private static final long serialVersionUID = 12;
    private Place country = null;
    private Place county = null;
    private double latitude = 0.0d;
    private Place locality = null;
    private double longitude = 0.0d;
    private String placeId = "";
    private int placeType = 0;
    private String placeUrl = "";
    private Place region = null;
    private String woeId = "";

    public String getPlaceId() {
        return this.placeId;
    }

    public void setPlaceId(String placeId2) {
        this.placeId = placeId2;
    }

    public String getPlaceUrl() {
        return this.placeUrl;
    }

    public void setPlaceUrl(String placeUrl2) {
        this.placeUrl = placeUrl2;
    }

    public Place getLocality() {
        return this.locality;
    }

    public void setLocality(Place locality2) {
        this.locality = locality2;
    }

    public Place getCounty() {
        return this.county;
    }

    public void setCounty(Place county2) {
        this.county = county2;
    }

    public Place getRegion() {
        return this.region;
    }

    public void setRegion(Place region2) {
        this.region = region2;
    }

    public Place getCountry() {
        return this.country;
    }

    public void setCountry(Place country2) {
        this.country = country2;
    }

    public String getWoeId() {
        return this.woeId;
    }

    public void setWoeId(String woeId2) {
        this.woeId = woeId2;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        try {
            setLatitude(Double.parseDouble(latitude2));
        } catch (NumberFormatException e) {
        }
    }

    public void setLatitude(double latitude2) {
        this.latitude = latitude2;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        try {
            setLongitude(Double.parseDouble(longitude2));
        } catch (NumberFormatException e) {
        }
    }

    public void setLongitude(double longitude2) {
        this.longitude = longitude2;
    }

    public int getPlaceType() {
        return this.placeType;
    }

    public void setPlaceType(int placeType2) {
        this.placeType = placeType2;
    }
}
