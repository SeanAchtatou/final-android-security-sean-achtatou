package com.aetrion.flickr.places;

public class PlaceType {
    private static final long serialVersionUID = 12;
    int placeTypeId;
    String placeTypeName;

    public int getPlaceTypeId() {
        return this.placeTypeId;
    }

    public void setPlaceTypeId(String placeTypeId2) {
        try {
            setPlaceTypeId(Integer.parseInt(placeTypeId2));
        } catch (NumberFormatException e) {
        }
    }

    public void setPlaceTypeId(int placeTypeId2) {
        this.placeTypeId = placeTypeId2;
    }

    public String getPlaceTypeName() {
        return this.placeTypeName;
    }

    public void setPlaceTypeName(String placeTypeName2) {
        this.placeTypeName = placeTypeName2;
    }
}
