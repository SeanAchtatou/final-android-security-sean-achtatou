package com.aetrion.flickr.places;

public class Place {
    public static final int TYPE_CONTINENT = 29;
    public static final int TYPE_COUNTRY = 12;
    public static final int TYPE_COUNTY = 9;
    public static final int TYPE_LOCALITY = 7;
    public static final int TYPE_NEIGHBOURHOOD = 22;
    public static final int TYPE_REGION = 8;
    public static final int TYPE_UNSET = 0;
    private static final long serialVersionUID = 12;
    private double latitude = 0.0d;
    private double longitude = 0.0d;
    private String name = "";
    private int photoCount = 0;
    private String placeId = "";
    private int placeType = 0;
    private String placeUrl = "";
    private String woeId = "";

    public Place() {
    }

    public Place(String placeId2, String name2) {
        this.name = name2;
        this.placeId = placeId2;
    }

    public Place(String placeId2, String name2, int placeType2) {
        this.name = name2;
        this.placeId = placeId2;
        this.placeType = placeType2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getPlaceId() {
        return this.placeId;
    }

    public void setPlaceId(String placeId2) {
        this.placeId = placeId2;
    }

    public int getPlaceType() {
        return this.placeType;
    }

    public void setPlaceType(int placeType2) {
        this.placeType = placeType2;
    }

    public void setPlaceType(String placeType2) {
        try {
            setPlaceType(Integer.parseInt(placeType2));
        } catch (NumberFormatException e) {
        }
    }

    public String getPlaceUrl() {
        return this.placeUrl;
    }

    public void setPlaceUrl(String placeUrl2) {
        this.placeUrl = placeUrl2;
    }

    public String getWoeId() {
        return this.woeId;
    }

    public void setWoeId(String woeId2) {
        this.woeId = woeId2;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        try {
            setLatitude(Double.parseDouble(latitude2));
        } catch (NumberFormatException e) {
        }
    }

    public void setLatitude(double latitude2) {
        this.latitude = latitude2;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        try {
            setLongitude(Double.parseDouble(longitude2));
        } catch (NumberFormatException e) {
        }
    }

    public void setLongitude(double longitude2) {
        this.longitude = longitude2;
    }

    public int getPhotoCount() {
        return this.photoCount;
    }

    public void setPhotoCount(String photoCount2) {
        try {
            setPhotoCount(Integer.parseInt(photoCount2));
        } catch (NumberFormatException e) {
        }
    }

    public void setPhotoCount(int photoCount2) {
        this.photoCount = photoCount2;
    }
}
