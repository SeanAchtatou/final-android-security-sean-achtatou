package com.aetrion.flickr.places;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.photos.SearchParameters;
import com.aetrion.flickr.tags.Tag;
import com.aetrion.flickr.util.StringUtilities;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PlacesInterface {
    private static final String METHOD_FIND = "flickr.places.find";
    private static final String METHOD_FIND_BY_LATLON = "flickr.places.findByLatLon";
    private static final String METHOD_GET_CHILDREN_WITH_PHOTOS_PUBLIC = "flickr.places.getChildrenWithPhotosPublic";
    private static final String METHOD_GET_INFO = "flickr.places.getInfo";
    private static final String METHOD_GET_INFO_BY_URL = "flickr.places.getInfoByUrl";
    private static final String METHOD_GET_PLACETYPES = "flickr.places.getPlaceTypes";
    private static final String METHOD_GET_SHAPEHISTORY = "flickr.places.getShapeHistory";
    private static final String METHOD_GET_TOP_PLACES_LIST = "flickr.places.getTopPlacesList";
    private static final String METHOD_PLACES_FOR_BOUNDINGBOX = "flickr.places.placesForBoundingBox";
    private static final String METHOD_PLACES_FOR_CONTACTS = "flickr.places.placesForContacts";
    private static final String METHOD_PLACES_FOR_TAGS = "flickr.places.placesForTags";
    private static final String METHOD_PLACES_FOR_USER = "flickr.places.placesForUser";
    private static final String METHOD_RESOLVE_PLACE_ID = "flickr.places.resolvePlaceId";
    private static final String METHOD_RESOLVE_PLACE_URL = "flickr.places.resolvePlaceURL";
    private static final String METHOD_TAGS_FOR_PLACE = "flickr.places.tagsForPlace";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public PlacesInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public PlacesList find(String query) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PlacesList placesList = new PlacesList();
        parameters.add(new Parameter("method", METHOD_FIND));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("query", query));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList placesNodes = response.getPayload().getElementsByTagName("place");
        placesList.setPage("1");
        placesList.setPages("1");
        placesList.setPerPage("" + placesNodes.getLength());
        placesList.setTotal("" + placesNodes.getLength());
        for (int i = 0; i < placesNodes.getLength(); i++) {
            placesList.add(parsePlace((Element) placesNodes.item(i)));
        }
        return placesList;
    }

    public PlacesList findByLatLon(double latitude, double longitude, int accuracy) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PlacesList placesList = new PlacesList();
        parameters.add(new Parameter("method", METHOD_FIND_BY_LATLON));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("lat", "" + latitude));
        parameters.add(new Parameter("lon", "" + longitude));
        parameters.add(new Parameter("accuracy", "" + accuracy));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList placesNodes = response.getPayload().getElementsByTagName("place");
        placesList.setPage("1");
        placesList.setPages("1");
        placesList.setPerPage("" + placesNodes.getLength());
        placesList.setTotal("" + placesNodes.getLength());
        for (int i = 0; i < placesNodes.getLength(); i++) {
            placesList.add(parsePlace((Element) placesNodes.item(i)));
        }
        return placesList;
    }

    public PlacesList getChildrenWithPhotosPublic(String placeId, String woeId) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PlacesList placesList = new PlacesList();
        parameters.add(new Parameter("method", METHOD_GET_CHILDREN_WITH_PHOTOS_PUBLIC));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList placesNodes = response.getPayload().getElementsByTagName("place");
        placesList.setPage("1");
        placesList.setPages("1");
        placesList.setPerPage("" + placesNodes.getLength());
        placesList.setTotal("" + placesNodes.getLength());
        for (int i = 0; i < placesNodes.getLength(); i++) {
            placesList.add(parsePlace((Element) placesNodes.item(i)));
        }
        return placesList;
    }

    public Location getInfo(String placeId, String woeId) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        new Location();
        parameters.add(new Parameter("method", METHOD_GET_INFO));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return parseLocation(response.getPayload());
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public Location getInfoByUrl(String url) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        new Location();
        parameters.add(new Parameter("method", METHOD_GET_INFO_BY_URL));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("url", url));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return parseLocation(response.getPayload());
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public ArrayList getPlaceTypes() throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        new PlacesList();
        parameters.add(new Parameter("method", METHOD_GET_PLACETYPES));
        parameters.add(new Parameter("api_key", this.apiKey));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        ArrayList placeTypeList = new ArrayList();
        NodeList placeTypeNodes = response.getPayload().getElementsByTagName("place_type");
        for (int i = 0; i < placeTypeNodes.getLength(); i++) {
            Element placeTypeElement = (Element) placeTypeNodes.item(i);
            PlaceType placeType = new PlaceType();
            placeType.setPlaceTypeId(placeTypeElement.getAttribute("id"));
            placeType.setPlaceTypeName(XMLUtilities.getValue(placeTypeElement));
            placeTypeList.add(placeType);
        }
        return placeTypeList;
    }

    public ArrayList getShapeHistory(String placeId, String woeId) throws FlickrException, IOException, SAXException {
        ArrayList shapeList = new ArrayList();
        List parameters = new ArrayList();
        new Location();
        parameters.add(new Parameter("method", METHOD_GET_SHAPEHISTORY));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element payload = response.getPayload();
        return shapeList;
    }

    public PlacesList getTopPlacesList(int placeType, Date date, String placeId, String woeId) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PlacesList placesList = new PlacesList();
        parameters.add(new Parameter("method", METHOD_GET_TOP_PLACES_LIST));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("place_type", intPlaceTypeToString(placeType)));
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        if (date != null) {
            parameters.add(new Parameter("date", ((DateFormat) SearchParameters.DATE_FORMATS.get()).format(date)));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList placesNodes = response.getPayload().getElementsByTagName("place");
        placesList.setPage("1");
        placesList.setPages("1");
        placesList.setPerPage("" + placesNodes.getLength());
        placesList.setTotal("" + placesNodes.getLength());
        for (int i = 0; i < placesNodes.getLength(); i++) {
            placesList.add(parsePlace((Element) placesNodes.item(i)));
        }
        return placesList;
    }

    public PlacesList placesForBoundingBox(int placeType, String bbox) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PlacesList placesList = new PlacesList();
        parameters.add(new Parameter("method", METHOD_PLACES_FOR_BOUNDINGBOX));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("place_type", intPlaceTypeToString(placeType)));
        parameters.add(new Parameter("bbox", bbox));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList placesNodes = response.getPayload().getElementsByTagName("place");
        placesList.setPage("1");
        placesList.setPages("1");
        placesList.setPerPage("" + placesNodes.getLength());
        placesList.setTotal("" + placesNodes.getLength());
        for (int i = 0; i < placesNodes.getLength(); i++) {
            placesList.add(parsePlace((Element) placesNodes.item(i)));
        }
        return placesList;
    }

    public PlacesList placesForContacts(int placeType, String placeId, String woeId, String threshold, String contacts) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PlacesList placesList = new PlacesList();
        parameters.add(new Parameter("method", METHOD_PLACES_FOR_CONTACTS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("place_type", intPlaceTypeToString(placeType)));
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        if (threshold != null) {
            parameters.add(new Parameter("threshold", threshold));
        }
        if (contacts != null) {
            parameters.add(new Parameter("contacts", contacts));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList placesNodes = response.getPayload().getElementsByTagName("place");
        placesList.setPage("1");
        placesList.setPages("1");
        placesList.setPerPage("" + placesNodes.getLength());
        placesList.setTotal("" + placesNodes.getLength());
        for (int i = 0; i < placesNodes.getLength(); i++) {
            placesList.add(parsePlace((Element) placesNodes.item(i)));
        }
        return placesList;
    }

    public PlacesList placesForTags(int placeTypeId, String woeId, String placeId, String threshold, String[] tags, String tagMode, String machineTags, String machineTagMode, Date minUploadDate, Date maxUploadDate, Date minTakenDate, Date maxTakenDate) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PlacesList placesList = new PlacesList();
        parameters.add(new Parameter("method", METHOD_PLACES_FOR_TAGS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("place_type_id", (long) placeTypeId));
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (threshold != null) {
            parameters.add(new Parameter("threshold", threshold));
        }
        if (tags != null) {
            parameters.add(new Parameter(Extras.TAGS, StringUtilities.join(tags, ",")));
        }
        if (tagMode != null) {
            parameters.add(new Parameter("tag_mode", tagMode));
        }
        if (machineTags != null) {
            parameters.add(new Parameter(Extras.MACHINE_TAGS, machineTags));
        }
        if (machineTagMode != null) {
            parameters.add(new Parameter("machine_tag_mode", machineTagMode));
        }
        if (minUploadDate != null) {
            parameters.add(new Parameter("min_upload_date", new Long(minUploadDate.getTime() / 1000)));
        }
        if (maxUploadDate != null) {
            parameters.add(new Parameter("max_upload_date", new Long(maxUploadDate.getTime() / 1000)));
        }
        if (minTakenDate != null) {
            parameters.add(new Parameter("min_taken_date", ((DateFormat) SearchParameters.MYSQL_DATE_FORMATS.get()).format(minTakenDate)));
        }
        if (maxTakenDate != null) {
            parameters.add(new Parameter("max_taken_date", ((DateFormat) SearchParameters.MYSQL_DATE_FORMATS.get()).format(maxTakenDate)));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList placesNodes = response.getPayload().getElementsByTagName("place");
        placesList.setPage("1");
        placesList.setPages("1");
        placesList.setPerPage("" + placesNodes.getLength());
        placesList.setTotal("" + placesNodes.getLength());
        for (int i = 0; i < placesNodes.getLength(); i++) {
            placesList.add(parsePlace((Element) placesNodes.item(i)));
        }
        return placesList;
    }

    public PlacesList placesForUser(int placeType, String woeId, String placeId, String threshold, Date minUploadDate, Date maxUploadDate, Date minTakenDate, Date maxTakenDate) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        PlacesList placesList = new PlacesList();
        parameters.add(new Parameter("method", METHOD_PLACES_FOR_USER));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("place_type", intPlaceTypeToString(placeType)));
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        if (threshold != null) {
            parameters.add(new Parameter("threshold", threshold));
        }
        if (minUploadDate != null) {
            parameters.add(new Parameter("min_upload_date", new Long(minUploadDate.getTime() / 1000)));
        }
        if (maxUploadDate != null) {
            parameters.add(new Parameter("max_upload_date", new Long(maxUploadDate.getTime() / 1000)));
        }
        if (minTakenDate != null) {
            parameters.add(new Parameter("min_taken_date", ((DateFormat) SearchParameters.MYSQL_DATE_FORMATS.get()).format(minTakenDate)));
        }
        if (maxTakenDate != null) {
            parameters.add(new Parameter("max_taken_date", ((DateFormat) SearchParameters.MYSQL_DATE_FORMATS.get()).format(maxTakenDate)));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList placesNodes = response.getPayload().getElementsByTagName("place");
        placesList.setPage("1");
        placesList.setPages("1");
        placesList.setPerPage("" + placesNodes.getLength());
        placesList.setTotal("" + placesNodes.getLength());
        for (int i = 0; i < placesNodes.getLength(); i++) {
            placesList.add(parsePlace((Element) placesNodes.item(i)));
        }
        return placesList;
    }

    public Location resolvePlaceId(String placeId) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_RESOLVE_PLACE_ID));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("place_id", placeId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return parseLocation(response.getPayload());
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public Location resolvePlaceURL(String flickrPlacesUrl) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_RESOLVE_PLACE_URL));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("url", flickrPlacesUrl));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return parseLocation(response.getPayload());
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public ArrayList tagsForPlace(String woeId, String placeId, Date minUploadDate, Date maxUploadDate, Date minTakenDate, Date maxTakenDate) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        ArrayList tagsList = new ArrayList();
        parameters.add(new Parameter("method", METHOD_TAGS_FOR_PLACE));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (woeId != null) {
            parameters.add(new Parameter("woe_id", woeId));
        }
        if (placeId != null) {
            parameters.add(new Parameter("place_id", placeId));
        }
        if (minUploadDate != null) {
            parameters.add(new Parameter("min_upload_date", new Long(minUploadDate.getTime() / 1000)));
        }
        if (maxUploadDate != null) {
            parameters.add(new Parameter("max_upload_date", new Long(maxUploadDate.getTime() / 1000)));
        }
        if (minTakenDate != null) {
            parameters.add(new Parameter("min_taken_date", ((DateFormat) SearchParameters.MYSQL_DATE_FORMATS.get()).format(minTakenDate)));
        }
        if (maxTakenDate != null) {
            parameters.add(new Parameter("max_taken_date", ((DateFormat) SearchParameters.MYSQL_DATE_FORMATS.get()).format(maxTakenDate)));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList tagsNodes = response.getPayload().getElementsByTagName("tag");
        for (int i = 0; i < tagsNodes.getLength(); i++) {
            Element tagElement = (Element) tagsNodes.item(i);
            Tag tag = new Tag();
            tag.setCount(tagElement.getAttribute("count"));
            tag.setValue(XMLUtilities.getValue(tagElement));
            tagsList.add(tag);
        }
        return tagsList;
    }

    private Location parseLocation(Element locationElement) {
        Location location = new Location();
        location.setPlaceId(locationElement.getAttribute("place_id"));
        location.setPlaceUrl(locationElement.getAttribute("place_url"));
        location.setWoeId(locationElement.getAttribute("woeid"));
        location.setLatitude(locationElement.getAttribute("latitude"));
        location.setLongitude(locationElement.getAttribute("longitude"));
        location.setPlaceType(stringPlaceTypeToInt(locationElement.getAttribute("place_type")));
        location.setLocality(parseLocationPlace((Element) locationElement.getElementsByTagName("locality").item(0), 7));
        location.setCounty(parseLocationPlace((Element) locationElement.getElementsByTagName("county").item(0), 9));
        location.setRegion(parseLocationPlace((Element) locationElement.getElementsByTagName("region").item(0), 8));
        location.setCountry(parseLocationPlace((Element) locationElement.getElementsByTagName("country").item(0), 12));
        return location;
    }

    private Place parseLocationPlace(Element element, int type) {
        Place place = new Place();
        place.setName(XMLUtilities.getValue(element));
        place.setPlaceId(element.getAttribute("place_id"));
        place.setPlaceUrl(element.getAttribute("place_url"));
        place.setWoeId(element.getAttribute("woeid"));
        place.setLatitude(element.getAttribute("latitude"));
        place.setLongitude(element.getAttribute("longitude"));
        place.setPlaceType(type);
        return place;
    }

    private Place parsePlace(Element placeElement) {
        Place place = new Place();
        place.setPlaceId(placeElement.getAttribute("place_id"));
        place.setPlaceUrl(placeElement.getAttribute("place_url"));
        place.setWoeId(placeElement.getAttribute("woeid"));
        place.setLatitude(placeElement.getAttribute("latitude"));
        place.setLongitude(placeElement.getAttribute("longitude"));
        place.setPhotoCount(placeElement.getAttribute("photo_count"));
        String attribute = placeElement.getAttribute("place_type");
        place.setPlaceType(placeElement.getAttribute("place_type_id"));
        place.setName(XMLUtilities.getValue(placeElement));
        return place;
    }

    private int stringPlaceTypeToInt(String typeString) {
        if (typeString.equals("locality")) {
            return 7;
        }
        if (typeString.equals("county")) {
            return 9;
        }
        if (typeString.equals("region")) {
            return 8;
        }
        if (typeString.equals("country")) {
            return 12;
        }
        if (typeString.equals("continent")) {
            return 29;
        }
        if (typeString.equals("neighbourhood")) {
            return 22;
        }
        return 0;
    }

    public String intPlaceTypeToString(int placeType) throws FlickrException {
        if (placeType == 12) {
            return "country";
        }
        if (placeType == 8) {
            return "region";
        }
        if (placeType == 7) {
            return "locality";
        }
        if (placeType == 29) {
            return "continent";
        }
        if (placeType == 22) {
            return "neighbourhood";
        }
        throw new FlickrException("33", "Not a valid place type");
    }
}
