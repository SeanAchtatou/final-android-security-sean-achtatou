package com.aetrion.flickr.photosets;

import java.util.Collection;

public class Photosets {
    private static final long serialVersionUID = 12;
    private boolean canCreate;
    private Collection photosets;

    public boolean isCanCreate() {
        return this.canCreate;
    }

    public void setCanCreate(boolean canCreate2) {
        this.canCreate = canCreate2;
    }

    public Collection getPhotosets() {
        return this.photosets;
    }

    public void setPhotosets(Collection photosets2) {
        this.photosets = photosets2;
    }
}
