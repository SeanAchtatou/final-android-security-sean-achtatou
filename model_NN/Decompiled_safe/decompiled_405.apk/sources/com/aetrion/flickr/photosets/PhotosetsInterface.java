package com.aetrion.flickr.photosets;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.people.User;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.photos.Photo;
import com.aetrion.flickr.photos.PhotoContext;
import com.aetrion.flickr.photos.PhotoList;
import com.aetrion.flickr.photos.PhotoUtils;
import com.aetrion.flickr.util.StringUtilities;
import com.aetrion.flickr.util.XMLUtilities;
import com.techcasita.android.creative.cloudprint.CloudPrintActivity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PhotosetsInterface {
    public static final String METHOD_ADD_PHOTO = "flickr.photosets.addPhoto";
    public static final String METHOD_CREATE = "flickr.photosets.create";
    public static final String METHOD_DELETE = "flickr.photosets.delete";
    public static final String METHOD_EDIT_META = "flickr.photosets.editMeta";
    public static final String METHOD_EDIT_PHOTOS = "flickr.photosets.editPhotos";
    public static final String METHOD_GET_CONTEXT = "flickr.photosets.getContext";
    public static final String METHOD_GET_INFO = "flickr.photosets.getInfo";
    public static final String METHOD_GET_LIST = "flickr.photosets.getList";
    public static final String METHOD_GET_PHOTOS = "flickr.photosets.getPhotos";
    public static final String METHOD_ORDER_SETS = "flickr.photosets.orderSets";
    public static final String METHOD_REMOVE_PHOTO = "flickr.photosets.removePhoto";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public PhotosetsInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public void addPhoto(String photosetId, String photoId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_ADD_PHOTO));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_id", photosetId));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public Photoset create(String title, String description, String primaryPhotoId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_CREATE));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter(CloudPrintActivity.EXTRA_TITLE, title));
        parameters.add(new Parameter("description", description));
        parameters.add(new Parameter("primary_photo_id", primaryPhotoId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element photosetElement = response.getPayload();
        Photoset photoset = new Photoset();
        photoset.setId(photosetElement.getAttribute("id"));
        photoset.setUrl(photosetElement.getAttribute("url"));
        return photoset;
    }

    public void delete(String photosetId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_DELETE));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_id", photosetId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void editMeta(String photosetId, String title, String description) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_EDIT_META));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_id", photosetId));
        parameters.add(new Parameter(CloudPrintActivity.EXTRA_TITLE, title));
        if (description != null) {
            parameters.add(new Parameter("description", description));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void editPhotos(String photosetId, String primaryPhotoId, String[] photoIds) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_EDIT_PHOTOS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_id", photosetId));
        parameters.add(new Parameter("primary_photo_id", primaryPhotoId));
        parameters.add(new Parameter("photo_ids", StringUtilities.join(photoIds, ",")));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public PhotoContext getContext(String photoId, String photosetId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_CONTEXT));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("photoset_id", photosetId));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        PhotoContext photoContext = new PhotoContext();
        for (Element element : response.getPayloadCollection()) {
            String elementName = element.getTagName();
            if (elementName.equals("prevphoto")) {
                Photo photo = new Photo();
                photo.setId(element.getAttribute("id"));
                photoContext.setPreviousPhoto(photo);
            } else if (elementName.equals("nextphoto")) {
                Photo photo2 = new Photo();
                photo2.setId(element.getAttribute("id"));
                photoContext.setNextPhoto(photo2);
            } else if (!elementName.equals("count")) {
                System.err.println("unsupported element name: " + elementName);
            }
        }
        return photoContext;
    }

    public Photoset getInfo(String photosetId) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_INFO));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_id", photosetId));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element photosetElement = response.getPayload();
        Photoset photoset = new Photoset();
        photoset.setId(photosetElement.getAttribute("id"));
        User owner = new User();
        owner.setId(photosetElement.getAttribute("owner"));
        photoset.setOwner(owner);
        Photo primaryPhoto = new Photo();
        primaryPhoto.setId(photosetElement.getAttribute("primary"));
        primaryPhoto.setSecret(photosetElement.getAttribute("secret"));
        primaryPhoto.setServer(photosetElement.getAttribute("server"));
        primaryPhoto.setFarm(photosetElement.getAttribute("farm"));
        photoset.setPrimaryPhoto(primaryPhoto);
        photoset.setSecret(photosetElement.getAttribute("secret"));
        photoset.setServer(photosetElement.getAttribute("server"));
        photoset.setFarm(photosetElement.getAttribute("farm"));
        photoset.setPhotoCount(photosetElement.getAttribute("photos"));
        photoset.setTitle(XMLUtilities.getChildValue(photosetElement, CloudPrintActivity.EXTRA_TITLE));
        photoset.setDescription(XMLUtilities.getChildValue(photosetElement, "description"));
        photoset.setPrimaryPhoto(primaryPhoto);
        return photoset;
    }

    public Photosets getList(String userId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_LIST));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (userId != null) {
            parameters.add(new Parameter("user_id", userId));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Photosets photosetsObject = new Photosets();
        Element photosetsElement = response.getPayload();
        List photosets = new ArrayList();
        NodeList photosetElements = photosetsElement.getElementsByTagName("photoset");
        for (int i = 0; i < photosetElements.getLength(); i++) {
            Element photosetElement = (Element) photosetElements.item(i);
            Photoset photoset = new Photoset();
            photoset.setId(photosetElement.getAttribute("id"));
            User owner = new User();
            owner.setId(photosetElement.getAttribute("owner"));
            photoset.setOwner(owner);
            Photo primaryPhoto = new Photo();
            primaryPhoto.setId(photosetElement.getAttribute("primary"));
            primaryPhoto.setSecret(photosetElement.getAttribute("secret"));
            primaryPhoto.setServer(photosetElement.getAttribute("server"));
            primaryPhoto.setFarm(photosetElement.getAttribute("farm"));
            photoset.setPrimaryPhoto(primaryPhoto);
            photoset.setSecret(photosetElement.getAttribute("secret"));
            photoset.setServer(photosetElement.getAttribute("server"));
            photoset.setFarm(photosetElement.getAttribute("farm"));
            photoset.setPhotoCount(photosetElement.getAttribute("photos"));
            photoset.setTitle(XMLUtilities.getChildValue(photosetElement, CloudPrintActivity.EXTRA_TITLE));
            photoset.setDescription(XMLUtilities.getChildValue(photosetElement, "description"));
            photosets.add(photoset);
        }
        photosetsObject.setPhotosets(photosets);
        return photosetsObject;
    }

    public PhotoList getPhotos(String photosetId, Set extras, int privacy_filter, int perPage, int page) throws IOException, SAXException, FlickrException {
        PhotoList photos = new PhotoList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_PHOTOS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_id", photosetId));
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", new Integer(perPage)));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", new Integer(page)));
        }
        if (privacy_filter > 0) {
            parameters.add(new Parameter("privacy_filter", "" + privacy_filter));
        }
        if (extras != null && !extras.isEmpty()) {
            parameters.add(new Parameter(Extras.KEY_EXTRAS, StringUtilities.join(extras, ",")));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element photoset = response.getPayload();
        NodeList photoElements = photoset.getElementsByTagName("photo");
        photos.setPage(photoset.getAttribute("page"));
        photos.setPages(photoset.getAttribute("pages"));
        photos.setPerPage(photoset.getAttribute("per_page"));
        photos.setTotal(photoset.getAttribute("total"));
        for (int i = 0; i < photoElements.getLength(); i++) {
            photos.add(PhotoUtils.createPhoto((Element) photoElements.item(i), photoset));
        }
        return photos;
    }

    public PhotoList getPhotos(String photosetId, int perPage, int page) throws IOException, SAXException, FlickrException {
        return getPhotos(photosetId, Extras.MIN_EXTRAS, 0, perPage, page);
    }

    public void orderSets(String[] photosetIds) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_ORDER_SETS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_ids", StringUtilities.join(photosetIds, ",")));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void removePhoto(String photosetId, String photoId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_REMOVE_PHOTO));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_id", photosetId));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }
}
