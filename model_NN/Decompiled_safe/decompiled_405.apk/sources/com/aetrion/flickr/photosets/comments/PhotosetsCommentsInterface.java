package com.aetrion.flickr.photosets.comments;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.photos.comments.Comment;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PhotosetsCommentsInterface {
    public static final String METHOD_ADD_COMMENT = "flickr.photosets.comments.addComment";
    public static final String METHOD_DELETE_COMMENT = "flickr.photosets.comments.deleteComment";
    public static final String METHOD_EDIT_COMMENT = "flickr.photosets.comments.editComment";
    public static final String METHOD_GET_LIST = "flickr.photosets.comments.getList";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public PhotosetsCommentsInterface(String apiKey2, String sharedSecret2, Transport transport) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transport;
    }

    public String addComment(String photosetId, String commentText) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_ADD_COMMENT));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photoset_id", photosetId));
        parameters.add(new Parameter("comment_text", commentText));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayload().getAttribute("id");
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public void deleteComment(String commentId) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_DELETE_COMMENT));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("comment_id", commentId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void editComment(String commentId, String commentText) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_EDIT_COMMENT));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("comment_id", commentId));
        parameters.add(new Parameter("comment_text", commentText));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.post(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public List getList(String photosetId) throws IOException, SAXException, FlickrException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Parameter("method", METHOD_GET_LIST));
        arrayList.add(new Parameter("api_key", this.apiKey));
        arrayList.add(new Parameter("photoset_id", photosetId));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), arrayList);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        List comments = new ArrayList();
        NodeList commentNodes = response.getPayload().getElementsByTagName("comment");
        int n = commentNodes.getLength();
        for (int i = 0; i < n; i++) {
            Comment comment = new Comment();
            Element commentElement = (Element) commentNodes.item(i);
            comment.setId(commentElement.getAttribute("id"));
            comment.setAuthor(commentElement.getAttribute("author"));
            comment.setAuthorName(commentElement.getAttribute("authorname"));
            comment.setPermaLink(commentElement.getAttribute("permalink"));
            long unixTime = 0;
            try {
                unixTime = Long.parseLong(commentElement.getAttribute("datecreate"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            comment.setDateCreate(new Date(1000 * unixTime));
            comment.setText(XMLUtilities.getValue(commentElement));
            comments.add(comment);
        }
        return comments;
    }
}
