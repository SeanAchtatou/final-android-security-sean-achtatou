package com.aetrion.flickr.photosets;

import com.aetrion.flickr.people.User;
import com.aetrion.flickr.photos.Photo;

public class Photoset {
    private static final long serialVersionUID = 12;
    private String description;
    private String farm;
    private String id;
    private User owner;
    private int photoCount;
    private Photo primaryPhoto;
    private String secret;
    private String server;
    private String title;
    private String url;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getUrl() {
        if (this.url != null) {
            return this.url;
        }
        StringBuffer sb = new StringBuffer();
        sb.append("http://www.flickr.com/photos/");
        sb.append(getOwner().getId());
        sb.append("/sets/");
        sb.append(getId());
        sb.append("/");
        return sb.toString();
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public User getOwner() {
        return this.owner;
    }

    public void setOwner(User owner2) {
        this.owner = owner2;
    }

    public Photo getPrimaryPhoto() {
        return this.primaryPhoto;
    }

    public void setPrimaryPhoto(Photo primaryPhoto2) {
        this.primaryPhoto = primaryPhoto2;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret2) {
        this.secret = secret2;
    }

    public String getServer() {
        return this.server;
    }

    public void setServer(String server2) {
        this.server = server2;
    }

    public String getFarm() {
        return this.farm;
    }

    public void setFarm(String farm2) {
        this.farm = farm2;
    }

    public int getPhotoCount() {
        return this.photoCount;
    }

    public void setPhotoCount(int photoCount2) {
        this.photoCount = photoCount2;
    }

    public void setPhotoCount(String photoCount2) {
        if (photoCount2 != null) {
            setPhotoCount(Integer.parseInt(photoCount2));
        }
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }
}
