package com.aetrion.flickr;

import com.aetrion.flickr.util.XMLUtilities;
import java.util.Collection;
import java.util.Iterator;
import org.apache.axis.message.SOAPBody;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.axis.message.SOAPFault;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SOAPResponse implements Response {
    private SOAPEnvelope envelope;
    private String errorCode;
    private String errorMessage;
    private Collection payload;
    private String stat;

    public SOAPResponse(SOAPEnvelope envelope2) {
        this.envelope = envelope2;
    }

    public void parse(Document document) {
        try {
            SOAPBody body = this.envelope.getBody();
            if (Flickr.debugStream) {
                System.out.println("SOAP RESPONSE.parse");
                System.out.println(body.getAsString());
            }
            SOAPFault fault = body.getFault();
            if (fault != null) {
                System.err.println("FAULT: " + fault.getAsString());
                this.errorCode = fault.getFaultCode();
                this.errorMessage = fault.getFaultString();
                return;
            }
            Iterator i = body.getChildElements();
            while (i.hasNext()) {
                Element bodyelement = (Element) i.next();
                bodyelement.normalize();
                this.payload = XMLUtilities.getChildElements(bodyelement);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStat() {
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Element getPayload() {
        Iterator iter = this.payload.iterator();
        if (iter.hasNext()) {
            return (Element) iter.next();
        }
        throw new RuntimeException("SOAP response payload has no elements");
    }

    public Collection getPayloadCollection() {
        return this.payload;
    }

    public boolean isError() {
        return this.errorCode != null;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
}
