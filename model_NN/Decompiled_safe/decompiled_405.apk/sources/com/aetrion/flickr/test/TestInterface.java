package com.aetrion.flickr.test;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.people.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class TestInterface {
    public static final String METHOD_ECHO = "flickr.test.echo";
    public static final String METHOD_LOGIN = "flickr.test.login";
    public static final String METHOD_NULL = "flickr.test.null";
    private String apiKey;
    private String sharedSecret;
    private Transport transport;

    public TestInterface(String apiKey2, String sharedSecret2, Transport transportAPI) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transport = transportAPI;
    }

    public Collection echo(Collection params) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_ECHO));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.addAll(params);
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayloadCollection();
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public User login() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_LOGIN));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element userElement = response.getPayload();
        User user = new User();
        user.setId(userElement.getAttribute("id"));
        user.setUsername(((Text) ((Element) userElement.getElementsByTagName("username").item(0)).getFirstChild()).getData());
        return user;
    }

    public void null_() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_NULL));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.get(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }
}
