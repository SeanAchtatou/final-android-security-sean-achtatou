package com.aetrion.flickr.panda;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.photos.PhotoList;
import com.aetrion.flickr.photos.PhotoUtils;
import com.aetrion.flickr.util.StringUtilities;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PandaInterface {
    private static final String METHOD_GET_LIST = "flickr.panda.getList";
    private static final String METHOD_GET_PHOTOS = "flickr.panda.getPhotos";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public PandaInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public ArrayList getList() throws FlickrException, IOException, SAXException {
        ArrayList pandas = new ArrayList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_LIST));
        parameters.add(new Parameter("api_key", this.apiKey));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList pandaNodes = response.getPayload().getElementsByTagName("panda");
        for (int i = 0; i < pandaNodes.getLength(); i++) {
            Panda panda = new Panda();
            panda.setName(XMLUtilities.getValue((Element) pandaNodes.item(i)));
            pandas.add(panda);
        }
        return pandas;
    }

    public PhotoList getPhotos(Panda panda, Set extras, int perPage, int page) throws FlickrException, IOException, SAXException {
        new ArrayList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_PHOTOS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("panda_name", panda.getName()));
        if (extras != null && !extras.isEmpty()) {
            parameters.add(new Parameter(Extras.KEY_EXTRAS, StringUtilities.join(extras, ",")));
        }
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", (long) perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", (long) page));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return PhotoUtils.createPhotoList(response.getPayload());
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }
}
