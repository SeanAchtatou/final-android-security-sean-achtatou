package com.aetrion.flickr;

public class Parameter {
    private String name;
    private Object value;

    public Parameter(String name2, Object value2) {
        this.name = name2;
        this.value = value2;
    }

    public Parameter(String name2, long value2) {
        this.name = name2;
        this.value = new Long(value2);
    }

    public String getName() {
        return this.name;
    }

    public Object getValue() {
        return this.value;
    }
}
