package com.aetrion.flickr.blogs;

import java.math.BigDecimal;

public class Blog {
    private static final long serialVersionUID = 12;
    private BigDecimal id;
    private String name;
    private boolean needPassword;
    private String url;

    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id2) {
        this.id = id2;
    }

    public void setId(String id2) {
        if (id2 != null) {
            setId(new BigDecimal(id2));
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public boolean isNeedPassword() {
        return this.needPassword;
    }

    public void setNeedPassword(boolean needPassword2) {
        this.needPassword = needPassword2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }
}
