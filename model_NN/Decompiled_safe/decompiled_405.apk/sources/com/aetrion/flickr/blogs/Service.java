package com.aetrion.flickr.blogs;

public class Service {
    private static final long serialVersionUID = 12;
    private String id;
    private String name;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }
}
