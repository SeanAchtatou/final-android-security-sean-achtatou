package com.aetrion.flickr;

import java.util.Collection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface Response {
    String getErrorCode();

    String getErrorMessage();

    Element getPayload();

    Collection getPayloadCollection();

    boolean isError();

    void parse(Document document);
}
