package com.aetrion.flickr.auth;

import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.ParameterAlphaComparator;
import com.aetrion.flickr.RequestContext;
import com.aetrion.flickr.util.ByteUtilities;
import com.aetrion.flickr.util.UrlUtilities;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class AuthUtilities {
    public static String getSignature(List parameters) {
        return getSignature(RequestContext.getRequestContext().getSharedSecret(), parameters);
    }

    public static String getMultipartSignature(List parameters) {
        return getMultipartSignature(RequestContext.getRequestContext().getSharedSecret(), parameters);
    }

    public static String getSignature(String sharedSecret, List params) {
        addAuthToken(params);
        StringBuffer buffer = new StringBuffer();
        buffer.append(sharedSecret);
        Collections.sort(params, new ParameterAlphaComparator());
        Iterator iter = params.iterator();
        while (iter.hasNext()) {
            Parameter param = (Parameter) iter.next();
            buffer.append(param.getName());
            buffer.append(param.getValue());
        }
        try {
            return ByteUtilities.toHexString(MessageDigest.getInstance("MD5").digest(buffer.toString().getBytes(UrlUtilities.UTF8)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static String getMultipartSignature(String sharedSecret, List params) {
        List ignoreParameters = new ArrayList();
        ignoreParameters.add("photo");
        addAuthToken(params);
        StringBuffer buffer = new StringBuffer();
        buffer.append(sharedSecret);
        Collections.sort(params, new ParameterAlphaComparator());
        Iterator iter = params.iterator();
        while (iter.hasNext()) {
            Parameter param = (Parameter) iter.next();
            if (!ignoreParameters.contains(param.getName().toLowerCase())) {
                buffer.append(param.getName());
                buffer.append(param.getValue());
            }
        }
        try {
            return ByteUtilities.toHexString(MessageDigest.getInstance("MD5").digest(buffer.toString().getBytes(UrlUtilities.UTF8)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static void addAuthToken(List params) {
        String authToken;
        Iterator it = params.iterator();
        boolean tokenFlag = false;
        while (it.hasNext()) {
            if (((Parameter) it.next()).getName().equals("auth_token")) {
                tokenFlag = true;
            }
        }
        if (!tokenFlag && RequestContext.getRequestContext().getAuth() != null && (authToken = RequestContext.getRequestContext().getAuth().getToken()) != null && !authToken.equals("")) {
            params.add(new Parameter("auth_token", authToken));
        }
    }
}
