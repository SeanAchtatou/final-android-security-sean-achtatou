package com.aetrion.flickr.auth;

import com.aetrion.flickr.people.User;
import java.io.Serializable;

public class Auth implements Serializable {
    private static final long serialVersionUID = -2254618470673679663L;
    private Permission permission;
    private String token;
    private User user;

    public String getToken() {
        return this.token;
    }

    public void setToken(String token2) {
        this.token = token2;
    }

    public Permission getPermission() {
        return this.permission;
    }

    public void setPermission(Permission permission2) {
        this.permission = permission2;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user2) {
        this.user = user2;
    }
}
