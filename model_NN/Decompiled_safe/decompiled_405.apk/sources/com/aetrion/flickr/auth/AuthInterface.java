package com.aetrion.flickr.auth;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.people.User;
import com.aetrion.flickr.util.UrlUtilities;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class AuthInterface {
    public static final String METHOD_CHECK_TOKEN = "flickr.auth.checkToken";
    public static final String METHOD_GET_FROB = "flickr.auth.getFrob";
    public static final String METHOD_GET_FULL_TOKEN = "flickr.auth.getFullToken";
    public static final String METHOD_GET_TOKEN = "flickr.auth.getToken";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public AuthInterface(String apiKey2, String sharedSecret2, Transport transport) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transport;
    }

    public Auth checkToken(String authToken) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_CHECK_TOKEN));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("auth_token", authToken));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Auth auth = new Auth();
        Element authElement = response.getPayload();
        auth.setToken(XMLUtilities.getChildValue(authElement, "token"));
        auth.setPermission(Permission.fromString(XMLUtilities.getChildValue(authElement, "perms")));
        Element userElement = XMLUtilities.getChild(authElement, "user");
        User user = new User();
        user.setId(userElement.getAttribute("nsid"));
        user.setUsername(userElement.getAttribute("username"));
        user.setRealName(userElement.getAttribute("fullname"));
        auth.setUser(user);
        return auth;
    }

    public Auth getFullToken(String miniToken) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_FULL_TOKEN));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("mini_token", miniToken));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Auth auth = new Auth();
        Element authElement = response.getPayload();
        auth.setToken(XMLUtilities.getChildValue(authElement, "token"));
        auth.setPermission(Permission.fromString(XMLUtilities.getChildValue(authElement, "perms")));
        Element userElement = XMLUtilities.getChild(authElement, "user");
        User user = new User();
        user.setId(userElement.getAttribute("nsid"));
        user.setUsername(userElement.getAttribute("username"));
        user.setRealName(userElement.getAttribute("fullname"));
        auth.setUser(user);
        return auth;
    }

    public String getFrob() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_FROB));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return XMLUtilities.getValue(response.getPayload());
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public Auth getToken(String frob) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_TOKEN));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("frob", frob));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Auth auth = new Auth();
        Element authElement = response.getPayload();
        auth.setToken(XMLUtilities.getChildValue(authElement, "token"));
        auth.setPermission(Permission.fromString(XMLUtilities.getChildValue(authElement, "perms")));
        Element userElement = XMLUtilities.getChild(authElement, "user");
        User user = new User();
        user.setId(userElement.getAttribute("nsid"));
        user.setUsername(userElement.getAttribute("username"));
        user.setRealName(userElement.getAttribute("fullname"));
        auth.setUser(user);
        return auth;
    }

    public URL buildAuthenticationUrl(Permission permission, String frob) throws MalformedURLException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("perms", permission.toString()));
        parameters.add(new Parameter("frob", frob));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        return UrlUtilities.buildUrl(this.transportAPI.getHost(), this.transportAPI.getPort(), "/services/auth/", parameters);
    }
}
