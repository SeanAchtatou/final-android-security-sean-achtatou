package com.aetrion.flickr.auth;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Permission implements Serializable {
    public static final Permission DELETE = new Permission(3);
    public static final int DELETE_TYPE = 3;
    public static final Permission NONE = new Permission(0);
    public static final int NONE_TYPE = 0;
    public static final Permission READ = new Permission(1);
    public static final int READ_TYPE = 1;
    public static final Permission WRITE = new Permission(2);
    public static final int WRITE_TYPE = 2;
    private static final long serialVersionUID = -5384461370301078353L;
    private static final Map stringToPermissionMap = new HashMap();
    private int type;

    static {
        stringToPermissionMap.put("none", NONE);
        stringToPermissionMap.put("read", READ);
        stringToPermissionMap.put("write", WRITE);
        stringToPermissionMap.put("delete", DELETE);
    }

    private Permission(int type2) {
        this.type = type2;
    }

    public int getType() {
        return this.type;
    }

    public static Permission fromString(String permission) {
        return (Permission) stringToPermissionMap.get(permission.toLowerCase());
    }

    public String toString() {
        switch (this.type) {
            case 0:
                return "none";
            case 1:
                return "read";
            case 2:
                return "write";
            case 3:
                return "delete";
            default:
                throw new IllegalStateException("Unsupported type: " + this.type);
        }
    }
}
