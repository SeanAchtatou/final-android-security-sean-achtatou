package com.aetrion.flickr.activity;

import java.util.Collection;

public class Item {
    private int comments = 0;
    private int commentsNew = 0;
    private int commentsOld = 0;
    private Collection events;
    private String farm;
    private int faves = 0;
    private String id;
    private int more = 0;
    private int notes = 0;
    private int notesNew = 0;
    private int notesOld = 0;
    private String owner;
    private String secret;
    private String server;
    private String title;
    private String type;
    private int views = 0;

    public String getFarm() {
        return this.farm;
    }

    public void setFarm(String farm2) {
        this.farm = farm2;
    }

    public int getFaves() {
        return this.faves;
    }

    public void setFaves(int faves2) {
        this.faves = faves2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public int getMore() {
        return this.more;
    }

    public void setMore(int more2) {
        this.more = more2;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner2) {
        this.owner = owner2;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret2) {
        this.secret = secret2;
    }

    public String getServer() {
        return this.server;
    }

    public void setServer(String server2) {
        this.server = server2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public int getViews() {
        return this.views;
    }

    public void setViews(int views2) {
        this.views = views2;
    }

    public Collection getEvents() {
        return this.events;
    }

    public void setEvents(Collection events2) {
        this.events = events2;
    }

    public int getComments() {
        return this.comments;
    }

    public void setComments(int comments2) {
        this.comments = comments2;
    }

    public int getNotes() {
        return this.notes;
    }

    public void setNotes(int notes2) {
        this.notes = notes2;
    }

    public int getCommentsNew() {
        return this.commentsNew;
    }

    public void setCommentsNew(int commentsNew2) {
        this.commentsNew = commentsNew2;
    }

    public int getCommentsOld() {
        return this.commentsOld;
    }

    public void setCommentsOld(int commentsOld2) {
        this.commentsOld = commentsOld2;
    }

    public int getNotesNew() {
        return this.notesNew;
    }

    public void setNotesNew(int notesNew2) {
        this.notesNew = notesNew2;
    }

    public int getNotesOld() {
        return this.notesOld;
    }

    public void setNotesOld(int notesOld2) {
        this.notesOld = notesOld2;
    }
}
