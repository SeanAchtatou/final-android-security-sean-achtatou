package com.aetrion.flickr.activity;

import java.util.Date;

public class Event {
    private Date dateadded;
    private String id;
    private String type;
    private String user;
    private String username;
    private String value;

    public Date getDateadded() {
        return this.dateadded;
    }

    public void setDateadded(Date dateadded2) {
        this.dateadded = dateadded2;
    }

    public void setDateadded(String dateAdded) {
        if (dateAdded != null && !"".equals(dateAdded)) {
            setDateadded(new Date(Long.parseLong(dateAdded) * 1000));
        }
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user2) {
        this.user = user2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }
}
