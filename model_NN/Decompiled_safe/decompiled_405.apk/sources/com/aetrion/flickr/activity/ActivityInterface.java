package com.aetrion.flickr.activity;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.util.XMLUtilities;
import com.techcasita.android.creative.cloudprint.CloudPrintActivity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ActivityInterface {
    public static final String METHOD_USER_COMMENTS = "flickr.activity.userComments";
    public static final String METHOD_USER_PHOTOS = "flickr.activity.userPhotos";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public ActivityInterface(String apiKey2, String sharedSecret2, Transport transport) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transport;
    }

    public ItemList userComments(int perPage, int page) throws IOException, SAXException, FlickrException {
        ItemList items = new ItemList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_USER_COMMENTS));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", "" + perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", "" + page));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element itemList = response.getPayload();
        NodeList itemElements = itemList.getElementsByTagName("item");
        items.setPage(itemList.getAttribute("page"));
        items.setPages(itemList.getAttribute("pages"));
        items.setPerPage(itemList.getAttribute("perpage"));
        items.setTotal(itemList.getAttribute("total"));
        for (int i = 0; i < itemElements.getLength(); i++) {
            items.add(createItem((Element) itemElements.item(i)));
        }
        return items;
    }

    public ItemList userPhotos(int perPage, int page, String timeframe) throws IOException, SAXException, FlickrException {
        ItemList items = new ItemList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_USER_PHOTOS));
        parameters.add(new Parameter("api_key", this.apiKey));
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", "" + perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", "" + page));
        }
        if (timeframe != null) {
            if (checkTimeframeArg(timeframe)) {
                parameters.add(new Parameter("timeframe", timeframe));
            } else {
                throw new FlickrException("0", "Timeframe-argument to getUserPhotos() not valid");
            }
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element itemList = response.getPayload();
        NodeList itemElements = itemList.getElementsByTagName("item");
        items.setPage(itemList.getAttribute("page"));
        items.setPages(itemList.getAttribute("pages"));
        items.setPerPage(itemList.getAttribute("perpage"));
        items.setTotal(itemList.getAttribute("total"));
        for (int i = 0; i < itemElements.getLength(); i++) {
            items.add(createItem((Element) itemElements.item(i)));
        }
        return items;
    }

    private Item createItem(Element itemElement) {
        Item item = new Item();
        item.setId(itemElement.getAttribute("id"));
        item.setSecret(itemElement.getAttribute("secret"));
        item.setType(itemElement.getAttribute("type"));
        item.setTitle(XMLUtilities.getChildValue(itemElement, CloudPrintActivity.EXTRA_TITLE));
        item.setFarm(itemElement.getAttribute("farm"));
        item.setServer(itemElement.getAttribute("server"));
        try {
            item.setComments(XMLUtilities.getIntAttribute(itemElement, "comments"));
            item.setNotes(XMLUtilities.getIntAttribute(itemElement, "notes"));
        } catch (Exception e) {
        }
        try {
            item.setCommentsOld(XMLUtilities.getIntAttribute(itemElement, "commentsold"));
            item.setCommentsNew(XMLUtilities.getIntAttribute(itemElement, "commentsnew"));
            item.setNotesOld(XMLUtilities.getIntAttribute(itemElement, "notesold"));
            item.setNotesNew(XMLUtilities.getIntAttribute(itemElement, "notesnew"));
        } catch (Exception e2) {
        }
        item.setViews(XMLUtilities.getIntAttribute(itemElement, Extras.VIEWS));
        item.setFaves(XMLUtilities.getIntAttribute(itemElement, "faves"));
        item.setMore(XMLUtilities.getIntAttribute(itemElement, "more"));
        try {
            List events = new ArrayList();
            NodeList eventNodes = ((Element) itemElement.getElementsByTagName("activity").item(0)).getElementsByTagName("event");
            for (int i = 0; i < eventNodes.getLength(); i++) {
                Element eventElement = (Element) eventNodes.item(i);
                Event event = new Event();
                event.setType(eventElement.getAttribute("type"));
                if (event.getType().equals("comment")) {
                    event.setId(eventElement.getAttribute("commentid"));
                } else if (event.getType().equals("note")) {
                    event.setId(eventElement.getAttribute("noteid"));
                } else if (event.getType().equals("fave")) {
                }
                event.setUser(eventElement.getAttribute("user"));
                event.setUsername(eventElement.getAttribute("username"));
                event.setDateadded(eventElement.getAttribute("dateadded"));
                event.setValue(XMLUtilities.getValue(eventElement));
                events.add(event);
            }
            item.setEvents(events);
        } catch (NullPointerException e3) {
        }
        return item;
    }

    public boolean checkTimeframeArg(String timeframe) {
        if (Pattern.compile("\\d*(d|h)", 2).matcher(timeframe).matches()) {
            return true;
        }
        return false;
    }
}
