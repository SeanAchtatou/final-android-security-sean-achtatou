package com.aetrion.flickr.tags;

import com.aetrion.flickr.util.StringUtilities;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Tag {
    private static final long serialVersionUID = 12;
    private String author;
    private String authorName;
    private int count;
    private String id;
    private String raw;
    private String value;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author2) {
        this.author = author2;
    }

    public String getAuthorName() {
        return this.authorName;
    }

    public void setAuthorName(String authorName2) {
        this.authorName = authorName2;
    }

    public String getRaw() {
        return this.raw;
    }

    public void setRaw(String raw2) {
        this.raw = raw2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        this.count = count2;
    }

    public void setCount(String count2) {
        setCount(Integer.parseInt(count2));
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Tag test = (Tag) obj;
        Method[] method = getClass().getMethods();
        for (int i = 0; i < method.length; i++) {
            if (StringUtilities.getterPattern.matcher(method[i].getName()).find() && !method[i].getName().equals("getClass")) {
                try {
                    Object res = method[i].invoke(this, null);
                    Object resTest = method[i].invoke(test, null);
                    String retType = method[i].getReturnType().toString();
                    if (retType.indexOf("class") == 0) {
                        if (!(res == null || resTest == null || res.equals(resTest))) {
                            return false;
                        }
                    } else if (!retType.equals("int")) {
                        System.out.println(method[i].getName() + "|" + method[i].getReturnType().toString());
                    } else if (!((Integer) res).equals((Integer) resTest)) {
                        return false;
                    }
                } catch (IllegalAccessException e) {
                    System.out.println("Size equals " + method[i].getName() + " " + e);
                } catch (InvocationTargetException e2) {
                } catch (Exception e3) {
                    System.out.println("Size equals " + method[i].getName() + " " + e3);
                }
            }
        }
        return true;
    }

    public int hashCode() {
        int hash = 1 + new Integer(this.count).hashCode();
        if (this.value != null) {
            hash += this.value.hashCode();
        }
        if (this.raw != null) {
            hash += this.raw.hashCode();
        }
        if (this.author != null) {
            hash += this.author.hashCode();
        }
        if (this.authorName != null) {
            hash += this.authorName.hashCode();
        }
        if (this.id != null) {
            return hash + this.id.hashCode();
        }
        return hash;
    }
}
