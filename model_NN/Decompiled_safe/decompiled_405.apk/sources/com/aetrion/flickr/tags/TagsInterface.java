package com.aetrion.flickr.tags;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.photos.Photo;
import com.aetrion.flickr.photos.PhotoList;
import com.aetrion.flickr.photos.PhotoUtils;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class TagsInterface {
    public static final String METHOD_GET_CLUSTERS = "flickr.tags.getClusters";
    public static final String METHOD_GET_CLUSTER_PHOTOS = "flickr.tags.getClusterPhotos";
    public static final String METHOD_GET_HOT_LIST = "flickr.tags.getHotList";
    public static final String METHOD_GET_LIST_PHOTO = "flickr.tags.getListPhoto";
    public static final String METHOD_GET_LIST_USER = "flickr.tags.getListUser";
    public static final String METHOD_GET_LIST_USER_POPULAR = "flickr.tags.getListUserPopular";
    public static final String METHOD_GET_LIST_USER_RAW = "flickr.tags.getListUserRaw";
    public static final String METHOD_GET_RELATED = "flickr.tags.getRelated";
    public static final String PERIOD_DAY = "day";
    public static final String PERIOD_WEEK = "week";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public TagsInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public ClusterList getClusters(String searchTag) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_CLUSTERS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("tag", searchTag));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        ClusterList clusters = new ClusterList();
        NodeList clusterElements = response.getPayload().getElementsByTagName("cluster");
        for (int i = 0; i < clusterElements.getLength(); i++) {
            Cluster cluster = new Cluster();
            NodeList tagElements = ((Element) clusterElements.item(i)).getElementsByTagName("tag");
            for (int j = 0; j < tagElements.getLength(); j++) {
                Tag tag = new Tag();
                tag.setValue(((Text) tagElements.item(j).getFirstChild()).getData());
                cluster.addTag(tag);
            }
            clusters.addCluster(cluster);
        }
        return clusters;
    }

    public PhotoList getClusterPhotos(String tag, String clusterId) throws IOException, SAXException, FlickrException {
        PhotoList photos = new PhotoList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_CLUSTER_PHOTOS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("tag", tag));
        parameters.add(new Parameter("cluster_id", clusterId));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList photoNodes = response.getPayload().getElementsByTagName("photo");
        photos.setPage("1");
        photos.setPages("1");
        photos.setPerPage("" + photoNodes.getLength());
        photos.setTotal("" + photoNodes.getLength());
        for (int i = 0; i < photoNodes.getLength(); i++) {
            photos.add(PhotoUtils.createPhoto((Element) photoNodes.item(i)));
        }
        return photos;
    }

    public Collection getHotList(String period, int count) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_HOT_LIST));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("period", period));
        parameters.add(new Parameter("count", "" + count));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element tagsElement = response.getPayload();
        List tags = new ArrayList();
        NodeList tagElements = tagsElement.getElementsByTagName("tag");
        for (int i = 0; i < tagElements.getLength(); i++) {
            Element tagElement = (Element) tagElements.item(i);
            HotlistTag tag = new HotlistTag();
            tag.setScore(tagElement.getAttribute("score"));
            tag.setValue(((Text) tagElement.getFirstChild()).getData());
            tags.add(tag);
        }
        return tags;
    }

    public Photo getListPhoto(String photoId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_LIST_PHOTO));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element photoElement = response.getPayload();
        Photo photo = new Photo();
        photo.setId(photoElement.getAttribute("id"));
        List tags = new ArrayList();
        NodeList tagElements = ((Element) photoElement.getElementsByTagName(Extras.TAGS).item(0)).getElementsByTagName("tag");
        for (int i = 0; i < tagElements.getLength(); i++) {
            Element tagElement = (Element) tagElements.item(i);
            Tag tag = new Tag();
            tag.setId(tagElement.getAttribute("id"));
            tag.setAuthor(tagElement.getAttribute("author"));
            tag.setAuthorName(tagElement.getAttribute("authorname"));
            tag.setRaw(tagElement.getAttribute("raw"));
            tag.setValue(((Text) tagElement.getFirstChild()).getData());
            tags.add(tag);
        }
        photo.setTags(tags);
        return photo;
    }

    public Collection getListUser(String userId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_LIST_USER));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("user_id", userId));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element whoElement = response.getPayload();
        List tags = new ArrayList();
        NodeList tagElements = ((Element) whoElement.getElementsByTagName(Extras.TAGS).item(0)).getElementsByTagName("tag");
        for (int i = 0; i < tagElements.getLength(); i++) {
            Tag tag = new Tag();
            tag.setValue(((Text) ((Element) tagElements.item(i)).getFirstChild()).getData());
            tags.add(tag);
        }
        return tags;
    }

    public Collection getListUserPopular(String userId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_LIST_USER_POPULAR));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("user_id", userId));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element whoElement = response.getPayload();
        List tags = new ArrayList();
        NodeList tagElements = ((Element) whoElement.getElementsByTagName(Extras.TAGS).item(0)).getElementsByTagName("tag");
        for (int i = 0; i < tagElements.getLength(); i++) {
            Element tagElement = (Element) tagElements.item(i);
            Tag tag = new Tag();
            tag.setCount(tagElement.getAttribute("count"));
            tag.setValue(((Text) tagElement.getFirstChild()).getData());
            tags.add(tag);
        }
        return tags;
    }

    public Collection getListUserRaw(String tagVal) throws IOException, SAXException, FlickrException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Parameter("method", METHOD_GET_LIST_USER_RAW));
        arrayList.add(new Parameter("api_key", this.apiKey));
        if (tagVal != null) {
            arrayList.add(new Parameter("tag", tagVal));
        }
        Response response = this.transportAPI.get(this.transportAPI.getPath(), arrayList);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element whoElement = response.getPayload();
        List tags = new ArrayList();
        NodeList tagElements = ((Element) whoElement.getElementsByTagName(Extras.TAGS).item(0)).getElementsByTagName("tag");
        for (int i = 0; i < tagElements.getLength(); i++) {
            Element tagElement = (Element) tagElements.item(i);
            TagRaw tag = new TagRaw();
            tag.setClean(tagElement.getAttribute("clean"));
            NodeList rawElements = tagElement.getElementsByTagName("raw");
            for (int j = 0; j < rawElements.getLength(); j++) {
                tag.addRaw(((Text) ((Element) rawElements.item(j)).getFirstChild()).getData());
            }
            tags.add(tag);
        }
        return tags;
    }

    public RelatedTagsList getRelated(String tag) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_RELATED));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("tag", tag));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element tagsElement = response.getPayload();
        RelatedTagsList tags = new RelatedTagsList();
        tags.setSource(tagsElement.getAttribute("source"));
        NodeList tagElements = tagsElement.getElementsByTagName("tag");
        for (int i = 0; i < tagElements.getLength(); i++) {
            Tag t = new Tag();
            t.setValue(XMLUtilities.getValue((Element) tagElements.item(i)));
            tags.add(t);
        }
        return tags;
    }
}
