package com.aetrion.flickr.tags;

import java.util.ArrayList;
import java.util.List;

public class TagRaw {
    private static final long serialVersionUID = 12;
    String clean;
    String owner;
    List raw = new ArrayList();

    public String getClean() {
        return this.clean;
    }

    public void setClean(String clean2) {
        this.clean = clean2;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner2) {
        this.owner = owner2;
    }

    public List getRaw() {
        return this.raw;
    }

    public void addRaw(String rawStr) {
        this.raw.add(rawStr);
    }
}
