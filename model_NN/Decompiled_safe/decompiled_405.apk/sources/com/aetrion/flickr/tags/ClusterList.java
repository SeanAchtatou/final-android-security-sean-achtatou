package com.aetrion.flickr.tags;

import java.util.ArrayList;

public class ClusterList {
    private static final long serialVersionUID = -5289011879992607535L;
    ArrayList<Cluster> clusters = new ArrayList<>();

    public void addCluster(Cluster cluster) {
        this.clusters.add(cluster);
    }

    public ArrayList<Cluster> getClusters() {
        return this.clusters;
    }
}
