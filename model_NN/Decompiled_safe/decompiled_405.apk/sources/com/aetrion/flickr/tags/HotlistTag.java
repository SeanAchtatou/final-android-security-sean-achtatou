package com.aetrion.flickr.tags;

public class HotlistTag {
    private static final long serialVersionUID = 12;
    private int score = 0;
    private String value;

    public int getScore() {
        return this.score;
    }

    public void setScore(int score2) {
        this.score = score2;
    }

    public void setScore(String score2) {
        setScore(Integer.parseInt(score2));
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }
}
