package com.aetrion.flickr.tags;

import java.util.ArrayList;

public class Cluster {
    private static final long serialVersionUID = 12;
    private ArrayList<Tag> tags = new ArrayList<>();

    public void addTag(Tag tag) {
        this.tags.add(tag);
    }

    public ArrayList<Tag> getTags() {
        return this.tags;
    }

    public void setTags(ArrayList<Tag> tags2) {
        this.tags = tags2;
    }
}
