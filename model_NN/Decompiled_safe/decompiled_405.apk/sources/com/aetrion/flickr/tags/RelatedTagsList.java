package com.aetrion.flickr.tags;

import java.util.ArrayList;

public class RelatedTagsList extends ArrayList {
    private static final long serialVersionUID = 12;
    private String source;

    public String getSource() {
        return this.source;
    }

    public void setSource(String source2) {
        this.source = source2;
    }
}
