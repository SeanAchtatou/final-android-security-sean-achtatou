package com.aetrion.flickr.prefs;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.SAXException;

public class PrefsInterface {
    public static final String METHOD_GET_CONTENT_TYPE = "flickr.prefs.getContentType";
    public static final String METHOD_GET_GEO_PERMS = "flickr.prefs.getGeoPerms";
    public static final String METHOD_GET_HIDDEN = "flickr.prefs.getHidden";
    public static final String METHOD_GET_PRIVACY = "flickr.prefs.getPrivacy";
    public static final String METHOD_GET_SAFETY_LEVEL = "flickr.prefs.getSafetyLevel";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public PrefsInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public String getContentType() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_CONTENT_TYPE));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayload().getAttribute("content_type");
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public int getGeoPerms() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_GEO_PERMS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        try {
            return Integer.parseInt(response.getPayload().getAttribute("geoperms"));
        } catch (NumberFormatException e) {
            throw new FlickrException("0", "Unable to parse geoPermission");
        }
    }

    public boolean getHidden() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_HIDDEN));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayload().getAttribute("hidden").equals("1");
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public String getSafetyLevel() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_SAFETY_LEVEL));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return response.getPayload().getAttribute("safety_level");
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }

    public int getPrivacy() throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_PRIVACY));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (!response.isError()) {
            return Integer.parseInt(response.getPayload().getAttribute("privacy"));
        }
        throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
    }
}
