package com.aetrion.flickr;

import com.aetrion.flickr.util.UrlUtilities;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.axis.utils.XMLUtils;
import org.xml.sax.SAXException;

public class SOAP extends Transport {
    public static final String BODYELEMENT = "FlickrRequest";
    public static final String PATH = "/services/soap/";
    public static final String URN = "urn:flickr";

    public SOAP() throws ParserConfigurationException {
        setTransportType(Transport.SOAP);
        setResponseClass(SOAPResponse.class);
        setPath(PATH);
    }

    public SOAP(String host) throws ParserConfigurationException {
        this();
        setHost(host);
    }

    public SOAP(String host, int port) throws ParserConfigurationException {
        this();
        setHost(host);
        setPort(port);
    }

    public Response get(String path, List parameters) throws IOException, SAXException {
        return post(path, parameters);
    }

    public Response post(String path, List parameters, boolean multipart) throws IOException, SAXException {
        URL url = UrlUtilities.buildUrl(getHost(), getPort(), path, Collections.EMPTY_LIST);
        try {
            SOAPEnvelope env = new SOAPEnvelope();
            env.addNamespaceDeclaration("xsi", "http://www.w3.org/1999/XMLSchema-instance");
            env.addNamespaceDeclaration("xsd", "http://www.w3.org/1999/XMLSchema");
            SOAPBodyElement body = new SOAPBodyElement(env.createName(BODYELEMENT, "x", URN));
            body.addChildElement(new SOAPBodyElement(XMLUtils.StringToElement("", "format", "soap2")));
            Iterator i = parameters.iterator();
            while (i.hasNext()) {
                Parameter p = (Parameter) i.next();
                body.addChildElement(new SOAPBodyElement(XMLUtils.StringToElement("", p.getName(), p.getValue().toString())));
            }
            env.addBodyElement(body);
            if (Flickr.debugStream) {
                System.out.println("SOAP ENVELOPE:");
                System.out.println(env.toString());
            }
            Call call = new Service().createCall();
            call.setTargetEndpointAddress(url);
            SOAPEnvelope envelope = call.invoke(env);
            if (Flickr.debugStream) {
                System.out.println("SOAP RESPONSE:");
                System.out.println(envelope.toString());
            }
            SOAPResponse response = new SOAPResponse(envelope);
            response.parse(null);
            return response;
        } catch (SOAPException e) {
            SOAPException se = e;
            se.printStackTrace();
            throw new RuntimeException((Throwable) se);
        } catch (ServiceException e2) {
            ServiceException se2 = e2;
            se2.printStackTrace();
            throw new RuntimeException((Throwable) se2);
        }
    }
}
