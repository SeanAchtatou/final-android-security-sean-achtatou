package com.aetrion.flickr.groups;

import java.util.Collection;

public class Category {
    private static final long serialVersionUID = 12;
    private Collection groups;
    private String name;
    private String path;
    private String pathIds;
    private Collection subcategories;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public String getPathIds() {
        return this.pathIds;
    }

    public void setPathIds(String pathIds2) {
        this.pathIds = pathIds2;
    }

    public Collection getSubcategories() {
        return this.subcategories;
    }

    public void setSubcategories(Collection subcategories2) {
        this.subcategories = subcategories2;
    }

    public Collection getGroups() {
        return this.groups;
    }

    public void setGroups(Collection groups2) {
        this.groups = groups2;
    }
}
