package com.aetrion.flickr.groups;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.util.XMLUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GroupsInterface {
    public static final String METHOD_BROWSE = "flickr.groups.browse";
    public static final String METHOD_GET_ACTIVE_LIST = "flickr.groups.getActiveList";
    public static final String METHOD_GET_INFO = "flickr.groups.getInfo";
    public static final String METHOD_SEARCH = "flickr.groups.search";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public GroupsInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public Category browse(String catId) throws IOException, SAXException, FlickrException {
        List subcategories = new ArrayList();
        List groups = new ArrayList();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Parameter("method", METHOD_BROWSE));
        arrayList.add(new Parameter("api_key", this.apiKey));
        if (catId != null) {
            arrayList.add(new Parameter("cat_id", catId));
        }
        arrayList.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, arrayList)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), arrayList);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element categoryElement = response.getPayload();
        Category category = new Category();
        category.setName(categoryElement.getAttribute("name"));
        category.setPath(categoryElement.getAttribute("path"));
        category.setPathIds(categoryElement.getAttribute("pathids"));
        NodeList subcatNodes = categoryElement.getElementsByTagName("subcat");
        for (int i = 0; i < subcatNodes.getLength(); i++) {
            Element node = (Element) subcatNodes.item(i);
            Subcategory subcategory = new Subcategory();
            subcategory.setId(Integer.parseInt(node.getAttribute("id")));
            subcategory.setName(node.getAttribute("name"));
            subcategory.setCount(Integer.parseInt(node.getAttribute("count")));
            subcategories.add(subcategory);
        }
        NodeList groupNodes = categoryElement.getElementsByTagName("group");
        for (int i2 = 0; i2 < groupNodes.getLength(); i2++) {
            Element node2 = (Element) groupNodes.item(i2);
            Group group = new Group();
            group.setId(node2.getAttribute("nsid"));
            group.setName(node2.getAttribute("name"));
            group.setMembers(node2.getAttribute("members"));
            groups.add(group);
        }
        category.setGroups(groups);
        category.setSubcategories(subcategories);
        return category;
    }

    public Group getInfo(String groupId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_INFO));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("group_id", groupId));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element groupElement = response.getPayload();
        Group group = new Group();
        group.setId(groupElement.getAttribute("id"));
        group.setIconFarm(groupElement.getAttribute("iconfarm"));
        group.setIconServer(groupElement.getAttribute("iconserver"));
        group.setLang(groupElement.getAttribute("lang"));
        group.setPoolModerated(!groupElement.getAttribute("ispoolmoderated").equals("0"));
        group.setName(XMLUtilities.getChildValue(groupElement, "name"));
        group.setDescription(XMLUtilities.getChildValue(groupElement, "description"));
        group.setMembers(XMLUtilities.getChildValue(groupElement, "members"));
        group.setPrivacy(XMLUtilities.getChildValue(groupElement, "privacy"));
        NodeList throttleNodes = groupElement.getElementsByTagName("throttle");
        int n = throttleNodes.getLength();
        if (n == 1) {
            Element throttleElement = (Element) throttleNodes.item(0);
            Throttle throttle = new Throttle();
            group.setThrottle(throttle);
            throttle.setMode(throttleElement.getAttribute("mode"));
            String countStr = throttleElement.getAttribute("count");
            String remainingStr = throttleElement.getAttribute("remaining");
            if (countStr != null && countStr.length() > 0) {
                throttle.setCount(Integer.parseInt(countStr));
            }
            if (remainingStr != null && remainingStr.length() > 0) {
                throttle.setRemaining(Integer.parseInt(remainingStr));
            }
        } else if (n > 1) {
            System.err.println("WARNING: more than one throttle element in group");
        }
        return group;
    }

    public Collection search(String text, int perPage, int page) throws FlickrException, IOException, SAXException {
        GroupList groupList = new GroupList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_SEARCH));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("text", text));
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", new Integer(perPage)));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", new Integer(page)));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element groupsElement = response.getPayload();
        NodeList groupNodes = groupsElement.getElementsByTagName("group");
        groupList.setPage(XMLUtilities.getIntAttribute(groupsElement, "page"));
        groupList.setPages(XMLUtilities.getIntAttribute(groupsElement, "pages"));
        groupList.setPerPage(XMLUtilities.getIntAttribute(groupsElement, "perpage"));
        groupList.setTotal(XMLUtilities.getIntAttribute(groupsElement, "total"));
        for (int i = 0; i < groupNodes.getLength(); i++) {
            Element groupElement = (Element) groupNodes.item(i);
            Group group = new Group();
            group.setId(groupElement.getAttribute("nsid"));
            group.setName(groupElement.getAttribute("name"));
            groupList.add(group);
        }
        return groupList;
    }
}
