package com.aetrion.flickr.groups;

public class Throttle {
    private static final long serialVersionUID = 12;
    private int count;
    private String mode;
    private int remaining;

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        this.count = count2;
    }

    public String getMode() {
        return this.mode;
    }

    public void setMode(String mode2) {
        this.mode = mode2;
    }

    public int getRemaining() {
        return this.remaining;
    }

    public void setRemaining(int remaining2) {
        this.remaining = remaining2;
    }
}
