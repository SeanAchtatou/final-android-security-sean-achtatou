package com.aetrion.flickr.groups.members;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.util.StringUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MembersInterface {
    public static final String METHOD_GET_LIST = "flickr.groups.members.getList";
    private String apiKey;
    private String sharedSecret;
    private Transport transportAPI;

    public MembersInterface(String apiKey2, String sharedSecret2, Transport transportAPI2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transportAPI = transportAPI2;
    }

    public MembersList getList(String groupId, Set memberTypes, int perPage, int page) throws FlickrException, IOException, SAXException {
        MembersList members = new MembersList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_LIST));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("group_id", groupId));
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", "" + perPage));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", "" + page));
        }
        if (memberTypes != null) {
            parameters.add(new Parameter("membertypes", StringUtilities.join(memberTypes, ",")));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transportAPI.get(this.transportAPI.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element mElement = response.getPayload();
        members.setPage(mElement.getAttribute("page"));
        members.setPages(mElement.getAttribute("pages"));
        members.setPerPage(mElement.getAttribute("perpage"));
        members.setTotal(mElement.getAttribute("total"));
        NodeList mNodes = mElement.getElementsByTagName("member");
        for (int i = 0; i < mNodes.getLength(); i++) {
            members.add(parseMember((Element) mNodes.item(i)));
        }
        return members;
    }

    private Member parseMember(Element mElement) {
        Member member = new Member();
        member.setId(mElement.getAttribute("nsid"));
        member.setUserName(mElement.getAttribute("username"));
        member.setIconServer(mElement.getAttribute("iconserver"));
        member.setIconFarm(mElement.getAttribute("iconfarm"));
        member.setMemberType(mElement.getAttribute("membertype"));
        return member;
    }
}
