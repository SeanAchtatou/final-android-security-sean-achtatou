package com.aetrion.flickr.groups.pools;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.Response;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.groups.Group;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.photos.Photo;
import com.aetrion.flickr.photos.PhotoContext;
import com.aetrion.flickr.photos.PhotoList;
import com.aetrion.flickr.photos.PhotoUtils;
import com.aetrion.flickr.util.StringUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PoolsInterface {
    public static final String METHOD_ADD = "flickr.groups.pools.add";
    public static final String METHOD_GET_CONTEXT = "flickr.groups.pools.getContext";
    public static final String METHOD_GET_GROUPS = "flickr.groups.pools.getGroups";
    public static final String METHOD_GET_PHOTOS = "flickr.groups.pools.getPhotos";
    public static final String METHOD_REMOVE = "flickr.groups.pools.remove";
    private String apiKey;
    private String sharedSecret;
    private Transport transport;

    public PoolsInterface(String apiKey2, String sharedSecret2, Transport transport2) {
        this.apiKey = apiKey2;
        this.sharedSecret = sharedSecret2;
        this.transport = transport2;
    }

    public void add(String photoId, String groupId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_ADD));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("group_id", groupId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public PhotoContext getContext(String photoId, String groupId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_GROUPS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("group_id", groupId));
        Response response = this.transport.get(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        PhotoContext photoContext = new PhotoContext();
        for (Element element : response.getPayloadCollection()) {
            String elementName = element.getTagName();
            if (elementName.equals("prevphoto")) {
                Photo photo = new Photo();
                photo.setId(element.getAttribute("id"));
                photoContext.setPreviousPhoto(photo);
            } else if (elementName.equals("nextphoto")) {
                Photo photo2 = new Photo();
                photo2.setId(element.getAttribute("id"));
                photoContext.setNextPhoto(photo2);
            } else {
                System.err.println("unsupported element name: " + elementName);
            }
        }
        return photoContext;
    }

    public Collection getGroups() throws IOException, SAXException, FlickrException {
        List groups = new ArrayList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_GROUPS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.get(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        NodeList groupNodes = response.getPayload().getElementsByTagName("group");
        for (int i = 0; i < groupNodes.getLength(); i++) {
            Element groupElement = (Element) groupNodes.item(i);
            Group group = new Group();
            group.setId(groupElement.getAttribute("id"));
            group.setName(groupElement.getAttribute("name"));
            group.setAdmin("1".equals(groupElement.getAttribute("admin")));
            group.setPrivacy(groupElement.getAttribute("privacy"));
            groups.add(group);
        }
        return groups;
    }

    public PhotoList getPhotos(String groupId, String[] tags, Set extras, int perPage, int page) throws IOException, SAXException, FlickrException {
        PhotoList photos = new PhotoList();
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_GET_PHOTOS));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("group_id", groupId));
        if (tags != null) {
            parameters.add(new Parameter(Extras.TAGS, StringUtilities.join(tags, " ")));
        }
        if (perPage > 0) {
            parameters.add(new Parameter("per_page", new Integer(perPage)));
        }
        if (page > 0) {
            parameters.add(new Parameter("page", new Integer(page)));
        }
        if (extras != null) {
            StringBuffer sb = new StringBuffer();
            int i = 0;
            for (Object append : extras) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(append);
                i++;
            }
            parameters.add(new Parameter(Extras.KEY_EXTRAS, sb.toString()));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.get(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
        Element photosElement = response.getPayload();
        photos.setPage(photosElement.getAttribute("page"));
        photos.setPages(photosElement.getAttribute("pages"));
        photos.setPerPage(photosElement.getAttribute("perpage"));
        photos.setTotal(photosElement.getAttribute("total"));
        NodeList photoNodes = photosElement.getElementsByTagName("photo");
        for (int i2 = 0; i2 < photoNodes.getLength(); i2++) {
            photos.add(PhotoUtils.createPhoto((Element) photoNodes.item(i2)));
        }
        return photos;
    }

    public PhotoList getPhotos(String groupId, String[] tags, int perPage, int page) throws IOException, SAXException, FlickrException {
        return getPhotos(groupId, tags, Extras.MIN_EXTRAS, perPage, page);
    }

    public void remove(String photoId, String groupId) throws IOException, SAXException, FlickrException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("method", METHOD_REMOVE));
        parameters.add(new Parameter("api_key", this.apiKey));
        parameters.add(new Parameter("photo_id", photoId));
        parameters.add(new Parameter("group_id", groupId));
        parameters.add(new Parameter("api_sig", AuthUtilities.getSignature(this.sharedSecret, parameters)));
        Response response = this.transport.post(this.transport.getPath(), parameters);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        }
    }
}
