package com.aetrion.flickr.groups;

import com.aetrion.flickr.Flickr;
import com.aetrion.flickr.util.BuddyIconable;
import com.aetrion.flickr.util.UrlUtilities;

public class Group implements BuddyIconable {
    private static final long serialVersionUID = 12;
    private boolean admin;
    private String chatId;
    private String description;
    private boolean eighteenPlus;
    private int iconFarm;
    private int iconServer;
    private String id;
    private int inChat;
    private String lang;
    private int members;
    private String name;
    private int online;
    private int photoCount;
    private boolean poolModerated;
    private String privacy;
    private Throttle throttle;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public int getMembers() {
        return this.members;
    }

    public void setMembers(int members2) {
        this.members = members2;
    }

    public void setMembers(String members2) {
        if (members2 != null) {
            try {
                setMembers(Integer.parseInt(members2));
            } catch (NumberFormatException e) {
                setMembers(0);
                if (Flickr.tracing) {
                    System.out.println("trace: Group.setMembers(String) encountered a number format exception.  members set to 0");
                }
            }
        }
    }

    public int getOnline() {
        return this.online;
    }

    public void setOnline(int online2) {
        this.online = online2;
    }

    public void setOnline(String online2) {
        if (online2 != null) {
            try {
                setOnline(Integer.parseInt(online2));
            } catch (NumberFormatException e) {
                setOnline(0);
                if (Flickr.tracing) {
                    System.out.println("trace: Group.setOnline(String) encountered a number format exception.  online set to 0");
                }
            }
        }
    }

    public String getChatId() {
        return this.chatId;
    }

    public void setChatId(String chatId2) {
        this.chatId = chatId2;
    }

    public int getInChat() {
        return this.inChat;
    }

    public void setInChat(int inChat2) {
        this.inChat = inChat2;
    }

    public void setInChat(String inChat2) {
        if (inChat2 != null) {
            try {
                setInChat(Integer.parseInt(inChat2));
            } catch (NumberFormatException e) {
                setInChat(0);
                if (Flickr.tracing) {
                    System.out.println("trace: Group.setInChat(String) encountered a number format exception.  InChat set to 0");
                }
            }
        }
    }

    public String getPrivacy() {
        return this.privacy;
    }

    public void setPrivacy(String privacy2) {
        this.privacy = privacy2;
    }

    public boolean isAdmin() {
        return this.admin;
    }

    public void setAdmin(boolean admin2) {
        this.admin = admin2;
    }

    public int getPhotoCount() {
        return this.photoCount;
    }

    public void setPhotoCount(int photoCount2) {
        this.photoCount = photoCount2;
    }

    public void setPhotoCount(String photoCount2) {
        if (photoCount2 != null) {
            try {
                setPhotoCount(Integer.parseInt(photoCount2));
            } catch (NumberFormatException e) {
                setPhotoCount(0);
                if (Flickr.tracing) {
                    System.out.println("trace: Group.setPhotoCount(String) encountered a number format exception.  PhotoCount set to 0");
                }
            }
        }
    }

    public boolean isEighteenPlus() {
        return this.eighteenPlus;
    }

    public void setEighteenPlus(boolean eighteenPlus2) {
        this.eighteenPlus = eighteenPlus2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getLang() {
        return this.lang;
    }

    public void setLang(String lang2) {
        this.lang = lang2;
    }

    public boolean isPoolModerated() {
        return this.poolModerated;
    }

    public void setPoolModerated(boolean poolModerated2) {
        this.poolModerated = poolModerated2;
    }

    public int getIconFarm() {
        return this.iconFarm;
    }

    public void setIconFarm(int iconFarm2) {
        this.iconFarm = iconFarm2;
    }

    public void setIconFarm(String iconFarm2) {
        if (iconFarm2 != null) {
            setIconFarm(Integer.parseInt(iconFarm2));
        }
    }

    public int getIconServer() {
        return this.iconServer;
    }

    public void setIconServer(int iconServer2) {
        this.iconServer = iconServer2;
    }

    public void setIconServer(String iconServer2) {
        if (iconServer2 != null) {
            setIconServer(Integer.parseInt(iconServer2));
        }
    }

    public String getBuddyIconUrl() {
        return UrlUtilities.createBuddyIconUrl(this.iconFarm, this.iconServer, this.id);
    }

    public Throttle getThrottle() {
        return this.throttle;
    }

    public void setThrottle(Throttle throttle2) {
        this.throttle = throttle2;
    }
}
