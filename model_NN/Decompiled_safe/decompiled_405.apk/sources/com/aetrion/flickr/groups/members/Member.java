package com.aetrion.flickr.groups.members;

public class Member {
    public static final String TYPE_ADMIN = "4";
    public static final String TYPE_MEMBER = "2";
    public static final String TYPE_MODERATOR = "3";
    private static final long serialVersionUID = 12;
    private int iconFarm = -1;
    private int iconServer = -1;
    private String id;
    private String memberType;
    private String userName;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public int getIconFarm() {
        return this.iconFarm;
    }

    public void setIconFarm(String iconFarm2) {
        if (iconFarm2 != null) {
            setIconFarm(Integer.parseInt(iconFarm2));
        }
    }

    public void setIconFarm(int iconFarm2) {
        this.iconFarm = iconFarm2;
    }

    public int getIconServer() {
        return this.iconServer;
    }

    public void setIconServer(String iconServer2) {
        if (iconServer2 != null) {
            setIconServer(Integer.parseInt(iconServer2));
        }
    }

    public void setIconServer(int iconServer2) {
        this.iconServer = iconServer2;
    }

    public String getMemberType() {
        return this.memberType;
    }

    public void setMemberType(String memberType2) {
        this.memberType = memberType2;
    }
}
