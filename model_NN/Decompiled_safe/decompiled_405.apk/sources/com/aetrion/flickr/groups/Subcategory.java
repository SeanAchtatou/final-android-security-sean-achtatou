package com.aetrion.flickr.groups;

public class Subcategory {
    private static final long serialVersionUID = 12;
    private int count;
    private int id;
    private String name;

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        this.count = count2;
    }
}
