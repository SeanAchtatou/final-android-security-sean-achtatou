package com.aetrion.flickr.uploader;

import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.Parameter;
import com.aetrion.flickr.REST;
import com.aetrion.flickr.Transport;
import com.aetrion.flickr.auth.AuthUtilities;
import com.aetrion.flickr.photos.Extras;
import com.aetrion.flickr.util.StringUtilities;
import com.techcasita.android.creative.cloudprint.CloudPrintActivity;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class Uploader {
    private String apiKey;
    private String sharedSecret;
    private Transport transport;

    public Uploader(String apiKey2, String sharedSecret2) {
        try {
            this.apiKey = apiKey2;
            this.sharedSecret = sharedSecret2;
            this.transport = new REST();
            this.transport.setResponseClass(UploaderResponse.class);
        } catch (ParserConfigurationException e) {
            ParserConfigurationException e2 = e;
            throw new RuntimeException(e2.getMessage(), e2);
        }
    }

    public String upload(byte[] data, UploadMetaData metaData) throws FlickrException, IOException, SAXException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("api_key", this.apiKey));
        String title = metaData.getTitle();
        if (title != null) {
            parameters.add(new Parameter(CloudPrintActivity.EXTRA_TITLE, title));
        }
        String description = metaData.getDescription();
        if (description != null) {
            parameters.add(new Parameter("description", description));
        }
        Collection tags = metaData.getTags();
        if (tags != null) {
            parameters.add(new Parameter(Extras.TAGS, StringUtilities.join(tags, " ")));
        }
        parameters.add(new Parameter("is_public", metaData.isPublicFlag() ? "1" : "0"));
        parameters.add(new Parameter("is_family", metaData.isFamilyFlag() ? "1" : "0"));
        parameters.add(new Parameter("is_friend", metaData.isFriendFlag() ? "1" : "0"));
        parameters.add(new Parameter("photo", data));
        if (metaData.isHidden() != null) {
            parameters.add(new Parameter("hidden", metaData.isHidden().booleanValue() ? "1" : "0"));
        }
        if (metaData.getSafetyLevel() != null) {
            parameters.add(new Parameter("safety_level", metaData.getSafetyLevel()));
        }
        parameters.add(new Parameter("async", metaData.isAsync() ? "1" : "0"));
        if (metaData.getContentType() != null) {
            parameters.add(new Parameter("content_type", metaData.getContentType()));
        }
        parameters.add(new Parameter("api_sig", AuthUtilities.getMultipartSignature(this.sharedSecret, parameters)));
        UploaderResponse response = (UploaderResponse) this.transport.post("/services/upload/", parameters, true);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        } else if (metaData.isAsync()) {
            return response.getTicketId();
        } else {
            return response.getPhotoId();
        }
    }

    public String upload(InputStream in, UploadMetaData metaData) throws IOException, FlickrException, SAXException {
        List parameters = new ArrayList();
        parameters.add(new Parameter("api_key", this.apiKey));
        String title = metaData.getTitle();
        if (title != null) {
            parameters.add(new Parameter(CloudPrintActivity.EXTRA_TITLE, title));
        }
        String description = metaData.getDescription();
        if (description != null) {
            parameters.add(new Parameter("description", description));
        }
        Collection tags = metaData.getTags();
        if (tags != null) {
            parameters.add(new Parameter(Extras.TAGS, StringUtilities.join(tags, " ")));
        }
        parameters.add(new Parameter("is_public", metaData.isPublicFlag() ? "1" : "0"));
        parameters.add(new Parameter("is_family", metaData.isFamilyFlag() ? "1" : "0"));
        parameters.add(new Parameter("is_friend", metaData.isFriendFlag() ? "1" : "0"));
        parameters.add(new Parameter("async", metaData.isAsync() ? "1" : "0"));
        parameters.add(new Parameter("photo", in));
        parameters.add(new Parameter("api_sig", AuthUtilities.getMultipartSignature(this.sharedSecret, parameters)));
        UploaderResponse response = (UploaderResponse) this.transport.post("/services/upload/", parameters, true);
        if (response.isError()) {
            throw new FlickrException(response.getErrorCode(), response.getErrorMessage());
        } else if (metaData.isAsync()) {
            return response.getTicketId();
        } else {
            return response.getPhotoId();
        }
    }
}
