package com.aetrion.flickr.uploader;

import com.aetrion.flickr.Response;
import java.util.Collection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class UploaderResponse implements Response {
    private String errorCode;
    private String errorMessage;
    private String photoId;
    private Element responsePayLoad;
    private String status;
    private String ticketId;

    public void parse(Document document) {
        this.responsePayLoad = document.getDocumentElement();
        this.status = this.responsePayLoad.getAttribute("stat");
        if ("ok".equals(this.status)) {
            Element photoIdElement = (Element) this.responsePayLoad.getElementsByTagName("photoid").item(0);
            if (photoIdElement != null) {
                this.photoId = ((Text) photoIdElement.getFirstChild()).getData();
            } else {
                this.photoId = null;
            }
            Element ticketIdElement = (Element) this.responsePayLoad.getElementsByTagName("ticketid").item(0);
            if (ticketIdElement != null) {
                this.ticketId = ((Text) ticketIdElement.getFirstChild()).getData();
            } else {
                this.ticketId = null;
            }
        } else {
            Element errElement = (Element) this.responsePayLoad.getElementsByTagName("err").item(0);
            this.errorCode = errElement.getAttribute("code");
            this.errorMessage = errElement.getAttribute("msg");
        }
    }

    public String getStatus() {
        return this.status;
    }

    public String getPhotoId() {
        return this.photoId;
    }

    public String getTicketId() {
        return this.ticketId;
    }

    public boolean isError() {
        return this.errorMessage != null;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public Element getPayload() {
        return this.responsePayLoad;
    }

    public Collection getPayloadCollection() {
        return null;
    }
}
