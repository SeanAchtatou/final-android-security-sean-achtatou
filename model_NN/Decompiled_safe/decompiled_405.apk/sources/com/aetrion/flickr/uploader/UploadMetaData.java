package com.aetrion.flickr.uploader;

import java.util.Collection;

public class UploadMetaData {
    private boolean async = false;
    private String contentType;
    private String description;
    private boolean familyFlag;
    private boolean friendFlag;
    private Boolean hidden;
    private boolean publicFlag;
    private String safetyLevel;
    private Collection tags;
    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public Collection getTags() {
        return this.tags;
    }

    public void setTags(Collection tags2) {
        this.tags = tags2;
    }

    public boolean isPublicFlag() {
        return this.publicFlag;
    }

    public void setPublicFlag(boolean publicFlag2) {
        this.publicFlag = publicFlag2;
    }

    public boolean isFriendFlag() {
        return this.friendFlag;
    }

    public void setFriendFlag(boolean friendFlag2) {
        this.friendFlag = friendFlag2;
    }

    public boolean isFamilyFlag() {
        return this.familyFlag;
    }

    public void setFamilyFlag(boolean familyFlag2) {
        this.familyFlag = familyFlag2;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType2) {
        this.contentType = contentType2;
    }

    public Boolean isHidden() {
        return this.hidden;
    }

    public void setHidden(Boolean hidden2) {
        this.hidden = hidden2;
    }

    public String getSafetyLevel() {
        return this.safetyLevel;
    }

    public void setSafetyLevel(String safetyLevel2) {
        this.safetyLevel = safetyLevel2;
    }

    public boolean isAsync() {
        return this.async;
    }

    public void setAsync(boolean async2) {
        this.async = async2;
    }
}
