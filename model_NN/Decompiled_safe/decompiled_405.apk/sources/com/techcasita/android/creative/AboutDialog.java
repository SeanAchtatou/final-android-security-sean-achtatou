package com.techcasita.android.creative;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AboutDialog {
    Context mContext;

    public AboutDialog(Context context) {
        this.mContext = context;
    }

    public AlertDialog create() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setMessage((int) R.string.tos_content).setTitle((int) R.string.about_dialog_title).setIcon((int) R.drawable.ic_about).setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }
}
