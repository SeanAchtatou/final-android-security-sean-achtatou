package com.techcasita.android.creative;

public final class R {

    public static final class array {
        public static final int brushes = 2131034112;
        public static final int printers = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int ic_about = 2130837504;
        public static final int ic_brush = 2130837505;
        public static final int ic_colors = 2130837506;
        public static final int ic_launcher = 2130837507;
        public static final int ic_menu_about = 2130837508;
        public static final int ic_menu_blur = 2130837509;
        public static final int ic_menu_brush = 2130837510;
        public static final int ic_menu_cloud = 2130837511;
        public static final int ic_menu_colorpicker = 2130837512;
        public static final int ic_menu_contrast = 2130837513;
        public static final int ic_menu_emboss = 2130837514;
        public static final int ic_menu_erase = 2130837515;
        public static final int ic_menu_flickr = 2130837516;
        public static final int ic_menu_help = 2130837517;
        public static final int ic_menu_pics = 2130837518;
        public static final int ic_menu_print = 2130837519;
        public static final int ic_menu_share = 2130837520;
        public static final int ic_menu_users = 2130837521;
        public static final int ic_menu_wiper = 2130837522;
        public static final int ic_printer = 2130837523;
    }

    public static final class id {
        public static final int mi_about = 2131165190;
        public static final int mi_brushes = 2131165186;
        public static final int mi_color = 2131165185;
        public static final int mi_group = 2131165194;
        public static final int mi_help = 2131165191;
        public static final int mi_print = 2131165189;
        public static final int mi_save = 2131165193;
        public static final int mi_share = 2131165192;
        public static final int mi_upload = 2131165188;
        public static final int mi_wipe = 2131165187;
        public static final int webview = 2131165184;
    }

    public static final class layout {
        public static final int print_dialog = 2130903040;
    }

    public static final class menu {
        public static final int menu = 2131099648;
    }

    public static final class string {
        public static final int GA_UA = 2130968614;
        public static final int HP_PRINT_PACKAGE = 2130968615;
        public static final int about_dialog_title = 2130968586;
        public static final int app_name = 2130968576;
        public static final int better = 2130968610;
        public static final int brush_dialog_title = 2130968589;
        public static final int cancel = 2130968579;
        public static final int color_dialog_title = 2130968588;
        public static final int doc_title = 2130968577;
        public static final int download_notice_text = 2130968582;
        public static final int download_notice_title = 2130968581;
        public static final int flickr_credentials = 2130968608;
        public static final int flickr_group_url = 2130968611;
        public static final int flickr_grp = 2130968585;
        public static final int flickr_title = 2130968609;
        public static final int flickr_uploaded_ko = 2130968613;
        public static final int flickr_uploaded_ok = 2130968612;
        public static final int help_content = 2130968584;
        public static final int help_dialog_title = 2130968587;
        public static final int mi_about = 2130968603;
        public static final int mi_blur = 2130968595;
        public static final int mi_brushes = 2130968593;
        public static final int mi_color = 2130968592;
        public static final int mi_emboss = 2130968594;
        public static final int mi_erase = 2130968596;
        public static final int mi_gcp = 2130968601;
        public static final int mi_group = 2130968606;
        public static final int mi_help = 2130968604;
        public static final int mi_hpp = 2130968602;
        public static final int mi_print = 2130968600;
        public static final int mi_save = 2130968605;
        public static final int mi_share = 2130968599;
        public static final int mi_srctop = 2130968597;
        public static final int mi_upload = 2130968607;
        public static final int mi_wipe = 2130968598;
        public static final int ok = 2130968578;
        public static final int print_GCP_title = 2130968591;
        public static final int print_dialog_title = 2130968590;
        public static final int saved = 2130968580;
        public static final int tos_content = 2130968583;
    }
}
