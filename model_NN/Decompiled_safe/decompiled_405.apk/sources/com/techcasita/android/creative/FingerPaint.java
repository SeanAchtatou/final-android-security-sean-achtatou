package com.techcasita.android.creative;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.techcasita.android.creative.BrushPickerDialog;
import com.techcasita.android.creative.ColorPickerDialog;
import com.techcasita.android.creative.PrintPickerDialog;
import com.techcasita.android.creative.UploaderDialog;
import com.techcasita.android.creative.cloudprint.CloudPrintActivity;
import com.techcasita.android.creative.cloudprint.PrintDialogActivity;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FingerPaint extends Activity implements ColorPickerDialog.OnColorChangedListener, BrushPickerDialog.OnBrushChangedListener, PrintPickerDialog.OnPrinterSelectedListener, UploaderDialog.OnUploadListener {
    private static final String ACTION_PRINT = "org.androidprinting.intent.action.PRINT";
    private static final int BACKGROUND = -13421773;
    private static final String PARCEL_BITMAP = "PARCEL_BITMAP";
    private static final String PARCEL_HELP_SHOWN = "HELP_SHOWN";
    private static final Intent PRINT_INTENT = new Intent(ACTION_PRINT).addCategory("android.intent.category.DEFAULT").setType("image/jpeg");
    private static final int REQUEST_CODE_GCP = 1003;
    private static final int REQUEST_CODE_HPP = 1004;
    private static final int REQUEST_CODE_INSTALL = 1002;
    private static final int REQUEST_CODE_SHARE = 1001;
    private MaskFilter mBlur;
    private int mBrush;
    private MaskFilter mEmboss;
    private boolean mHasiPrint;
    /* access modifiers changed from: private */
    public Paint mPaint;
    private long mTimer;
    private GoogleAnalyticsTracker mTracker;
    private MyView mView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mTracker = GoogleAnalyticsTracker.getInstance();
        this.mTracker.start(getResources().getString(R.string.GA_UA), 60, this);
        this.mTracker.trackPageView("/FingerPaint");
        this.mTimer = System.currentTimeMillis();
        MyView myView = new MyView(this, savedInstanceState);
        this.mView = myView;
        setContentView(myView);
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setDither(true);
        this.mPaint.setColor(-65536);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeJoin(Paint.Join.ROUND);
        this.mPaint.setStrokeCap(Paint.Cap.ROUND);
        this.mPaint.setStrokeWidth(12.0f);
        this.mEmboss = new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 6.0f, 3.5f);
        this.mBlur = new BlurMaskFilter(8.0f, BlurMaskFilter.Blur.NORMAL);
        this.mHasiPrint = isCallable(this, PRINT_INTENT);
        if (savedInstanceState == null || !savedInstanceState.containsKey(PARCEL_HELP_SHOWN) || !savedInstanceState.getBoolean(PARCEL_HELP_SHOWN)) {
            new HelpDialog(this).create().show();
            this.mTracker.trackEvent("Printer", "HP iPrint", "installed", this.mHasiPrint ? 1 : 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.mHasiPrint = isCallable(this, PRINT_INTENT);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(PARCEL_BITMAP, this.mView.mBitmap);
        outState.putBoolean(PARCEL_HELP_SHOWN, true);
    }

    public void colorChanged(int color) {
        this.mPaint.setColor(color);
        this.mTracker.trackEvent("Color", "changed", "to", color);
    }

    public void brushChanged(int brush) {
        this.mBrush = brush;
        switch (brush) {
            case 0:
                this.mPaint.setMaskFilter(null);
                this.mTracker.trackEvent("Brush", "changed", "normal", 0);
                return;
            case 1:
                this.mPaint.setMaskFilter(this.mEmboss);
                this.mTracker.trackEvent("Brush", "changed", "emboss", 0);
                return;
            case 2:
                this.mPaint.setMaskFilter(this.mBlur);
                this.mTracker.trackEvent("Brush", "changed", "blur", 0);
                return;
            case 3:
                this.mPaint.setMaskFilter(null);
                this.mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                this.mTracker.trackEvent("Brush", "changed", "erase", 0);
                return;
            case 4:
                this.mPaint.setMaskFilter(null);
                this.mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
                this.mPaint.setAlpha(128);
                this.mTracker.trackEvent("Brush", "changed", "blend", 0);
                return;
            default:
                Log.w(FingerPaint.class.getSimpleName(), "brushChanged called without valid brush");
                return;
        }
    }

    public void printerSelected(int printer) {
        switch (printer) {
            case 0:
                if (!this.mHasiPrint) {
                    installAPK(getResources().getString(R.string.HP_PRINT_PACKAGE));
                    return;
                }
                this.mTracker.trackEvent("FingerPaint", "print", "HPiPrint", 0);
                Intent intent = new Intent(ACTION_PRINT);
                intent.setDataAndType(saveBitmap(this, this.mView.mBitmap), "image/*");
                startActivityForResult(intent, REQUEST_CODE_HPP);
                return;
            case 1:
                this.mTracker.trackEvent("FingerPaint", "print", "GCP", 0);
                Intent printIntent = new Intent(this, PrintDialogActivity.class);
                printIntent.setDataAndType(saveBitmap(this, this.mView.mBitmap), "image/jpeg");
                printIntent.putExtra(CloudPrintActivity.EXTRA_TITLE, "FingerPaint");
                startActivityForResult(printIntent, REQUEST_CODE_GCP);
                return;
            default:
                return;
        }
    }

    public void uploaded(String msg) {
        Toast.makeText(this, msg, 1).show();
    }

    public class MyView extends ImageView {
        private static final float TOUCH_TOLERANCE = 4.0f;
        /* access modifiers changed from: private */
        public Bitmap mBitmap;
        private Paint mBitmapPaint;
        private Canvas mCanvas;
        private Path mPath;
        private float mX;
        private float mY;

        public MyView(Context c, Bundle bundle) {
            super(c);
            Display display = ((WindowManager) FingerPaint.this.getSystemService("window")).getDefaultDisplay();
            if (bundle == null || !bundle.containsKey(FingerPaint.PARCEL_BITMAP)) {
                this.mBitmap = Bitmap.createBitmap(display.getWidth(), display.getHeight(), Bitmap.Config.ARGB_8888);
            } else {
                this.mBitmap = Bitmap.createScaledBitmap((Bitmap) bundle.getParcelable(FingerPaint.PARCEL_BITMAP), display.getWidth(), display.getHeight(), true);
            }
            this.mCanvas = new Canvas(this.mBitmap);
            this.mPath = new Path();
            this.mBitmapPaint = new Paint(4);
        }

        /* access modifiers changed from: package-private */
        public void reset() {
            Display display = ((WindowManager) FingerPaint.this.getSystemService("window")).getDefaultDisplay();
            this.mBitmap = Bitmap.createBitmap(display.getWidth(), display.getHeight(), Bitmap.Config.ARGB_8888);
            this.mCanvas.setBitmap(this.mBitmap);
            this.mPath = new Path();
            this.mBitmapPaint = new Paint(4);
            invalidate();
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            invalidate();
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            canvas.drawColor((int) FingerPaint.BACKGROUND);
            canvas.drawBitmap(this.mBitmap, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.drawPath(this.mPath, FingerPaint.this.mPaint);
        }

        private void touch_start(float x, float y) {
            this.mPath.reset();
            this.mPath.moveTo(x, y);
            this.mX = x;
            this.mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - this.mX);
            float dy = Math.abs(y - this.mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                this.mPath.quadTo(this.mX, this.mY, (this.mX + x) / 2.0f, (this.mY + y) / 2.0f);
                this.mX = x;
                this.mY = y;
            }
        }

        private void touch_up() {
            this.mPath.lineTo(this.mX, this.mY);
            this.mCanvas.drawPath(this.mPath, FingerPaint.this.mPaint);
            this.mPath.reset();
        }

        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case 0:
                    touch_start(x, y);
                    invalidate();
                    return true;
                case 1:
                    touch_up();
                    invalidate();
                    return true;
                case 2:
                    touch_move(x, y);
                    invalidate();
                    return true;
                default:
                    return true;
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        this.mPaint.setXfermode(null);
        this.mPaint.setAlpha(255);
        switch (item.getItemId()) {
            case R.id.mi_color:
                new ColorPickerDialog(this, this, this.mPaint.getColor()).show();
                return true;
            case R.id.mi_brushes:
                new BrushPickerDialog(this, this, this.mBrush).create().show();
                return true;
            case R.id.mi_wipe:
                this.mView.reset();
                this.mTracker.trackEvent("Canvas", "reset", null, 0);
                this.mTimer = System.currentTimeMillis();
                return true;
            case R.id.mi_upload:
                this.mTracker.trackEvent("Upload", "flickr", "time_painted", (int) ((System.currentTimeMillis() - this.mTimer) / 1000));
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                this.mView.mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                if (bao.toByteArray().length < 100000) {
                    Toast.makeText(this, (int) R.string.better, 1).show();
                } else {
                    new UploaderDialog(this, this, bao.toByteArray()).create().show();
                }
                return true;
            case R.id.mi_print:
                this.mTracker.trackEvent("FingerPaint", "print", "time_painted", (int) ((System.currentTimeMillis() - this.mTimer) / 1000));
                new PrintPickerDialog(this, this).create().show();
                return true;
            case R.id.mi_about:
                new AboutDialog(this).create().show();
                this.mTracker.trackEvent("About", "displayed", null, 0);
                return true;
            case R.id.mi_help:
                new HelpDialog(this).create().show();
                return true;
            case R.id.mi_share:
                Intent i = new Intent("android.intent.action.SEND");
                i.setType("image/jpeg");
                i.putExtra("android.intent.extra.STREAM", saveBitmap(this, this.mView.mBitmap));
                this.mTracker.trackEvent("FingerPaint", "share", "time_painted", (int) ((System.currentTimeMillis() - this.mTimer) / 1000));
                startActivityForResult(i, REQUEST_CODE_SHARE);
                return true;
            case R.id.mi_save:
                saveBitmap(this, this.mView.mBitmap);
                Toast.makeText(this, (int) R.string.saved, 0).show();
                return true;
            case R.id.mi_group:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getResources().getString(R.string.flickr_group_url))));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_SHARE /*1001*/:
                this.mTracker.trackEvent("FingerPaint", "share", "result", resultCode == -1 ? 1 : 0);
                return;
            case REQUEST_CODE_INSTALL /*1002*/:
                if (resultCode == -1) {
                    this.mTracker.trackEvent("install", "HPiPrint", "result", 1);
                    return;
                } else {
                    this.mTracker.trackEvent("install", "HPiPrint", "result", 0);
                    return;
                }
            case REQUEST_CODE_GCP /*1003*/:
            case REQUEST_CODE_HPP /*1004*/:
                this.mTracker.trackEvent("FingerPaint", "print", "result", resultCode == -1 ? 1 : 0);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mTracker.stop();
    }

    /* access modifiers changed from: package-private */
    public void installAPK(final String packageName) {
        this.mTracker.trackEvent("FingerPaint", "install", "HPiPrint", 0);
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.download_notice_title).setMessage((int) R.string.download_notice_text).setCancelable(false).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    FingerPaint.this.startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + packageName)), FingerPaint.REQUEST_CODE_INSTALL);
                }
            }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.show();
        } catch (Exception e) {
            Log.v(FingerPaint.class.getSimpleName(), "Create/start of install package failed.");
            Log.e(FingerPaint.class.getSimpleName(), e.getMessage());
        }
    }

    public static Uri saveBitmap(Context ctx, Bitmap bmp) {
        ContentValues values = new ContentValues();
        values.put(CloudPrintActivity.EXTRA_TITLE, ((int) R.string.app_name) + String.valueOf(System.currentTimeMillis()));
        values.put("_display_name", Integer.valueOf((int) R.string.doc_title));
        values.put("date_added", Long.valueOf(System.currentTimeMillis()));
        values.put("mime_type", "image/jpeg");
        Uri uri = ctx.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        try {
            OutputStream outStream = ctx.getContentResolver().openOutputStream(uri);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
            outStream.flush();
            outStream.close();
            return uri;
        } catch (IOException e) {
            Log.e(FingerPaint.class.getSimpleName(), e.toString());
            return null;
        }
    }

    public static boolean isCallable(Context ctx, Intent intent) {
        return ctx.getPackageManager().queryIntentActivities(intent, 0).size() > 0;
    }
}
