package com.techcasita.android.creative.cloudprint;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.techcasita.android.creative.R;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class CloudPrintActivity extends Activity {
    private static final String CLOSE_POST_MESSAGE_NAME = "cp-dialog-on-close";
    public static final String EXTRA_TITLE = "title";
    private static final String JS_INTERFACE = "AndroidPrintDialog";
    private static final String PRINT_DIALOG_URL = "http://www.google.com/cloudprint/dialog.html";
    Intent cloudPrintIntent;
    private WebView dialogWebView;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.print_dialog);
        this.dialogWebView = (WebView) findViewById(R.id.webview);
        this.cloudPrintIntent = getIntent();
        this.dialogWebView.getSettings().setJavaScriptEnabled(true);
        this.dialogWebView.setWebViewClient(new PrintDialogWebClient());
        this.dialogWebView.addJavascriptInterface(new PrintDialogJavaScriptInterface(), JS_INTERFACE);
        this.dialogWebView.loadUrl(PRINT_DIALOG_URL);
    }

    final class PrintDialogJavaScriptInterface {
        PrintDialogJavaScriptInterface() {
        }

        public String getType() {
            return "dataUrl";
        }

        public String getTitle() {
            return CloudPrintActivity.this.cloudPrintIntent.hasExtra(CloudPrintActivity.EXTRA_TITLE) ? CloudPrintActivity.this.cloudPrintIntent.getExtras().getString(CloudPrintActivity.EXTRA_TITLE) : CloudPrintActivity.this.cloudPrintIntent.getAction();
        }

        public String getContent() {
            try {
                InputStream is = CloudPrintActivity.this.getContentResolver().openInputStream(CloudPrintActivity.this.cloudPrintIntent.getData());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                for (int n = is.read(buffer); n >= 0; n = is.read(buffer)) {
                    baos.write(buffer, 0, n);
                }
                is.close();
                baos.flush();
                return "data:" + CloudPrintActivity.this.cloudPrintIntent.getType() + ";base64," + Base64.encodeToString(baos.toByteArray(), 0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "";
            } catch (IOException e2) {
                e2.printStackTrace();
                return "";
            }
        }

        public void onPostMessage(String message) {
            if (message.startsWith(CloudPrintActivity.CLOSE_POST_MESSAGE_NAME)) {
                CloudPrintActivity.this.finish();
            }
        }
    }

    private final class PrintDialogWebClient extends WebViewClient {
        private PrintDialogWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }

        public void onPageFinished(WebView view, String url) {
            if (CloudPrintActivity.PRINT_DIALOG_URL.equals(url)) {
                view.loadUrl("javascript:printDialog.setPrintDocument(printDialog.createPrintDocument(window.AndroidPrintDialog.getType(),window.AndroidPrintDialog.getTitle(),window.AndroidPrintDialog.getContent()))");
                view.loadUrl("javascript:window.addEventListener('message',function(evt){window.AndroidPrintDialog.onPostMessage(evt.data)}, false)");
            }
        }
    }
}
