package com.techcasita.android.creative;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;

public class HelpDialog {
    Context mContext;

    public HelpDialog(Context context) {
        this.mContext = context;
    }

    public AlertDialog create() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setMessage((int) R.string.help_content).setTitle((int) R.string.help_dialog_title).setIcon((int) R.drawable.ic_about).setCancelable(true).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                HelpDialog.this.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(HelpDialog.this.mContext.getResources().getString(R.string.flickr_group_url))));
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                HelpDialog.this.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(HelpDialog.this.mContext.getResources().getString(R.string.flickr_group_url))));
            }
        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }
}
