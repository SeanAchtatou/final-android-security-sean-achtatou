package com.techcasita.android.creative;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class BrushPickerDialog {
    public static final int BLEND_BRUSH = 4;
    public static final int BLUR_BRUSH = 2;
    public static final int EMBOSS_BRUSH = 1;
    public static final int ERASE_BRUSH = 3;
    public static final int NORMAL_BRUSH = 0;
    private int mBrush;
    private Context mContext;
    /* access modifiers changed from: private */
    public OnBrushChangedListener mListener;

    public interface OnBrushChangedListener {
        void brushChanged(int i);
    }

    public BrushPickerDialog(Context context, OnBrushChangedListener listener, int brush) {
        this.mContext = context;
        this.mListener = listener;
        this.mBrush = brush;
    }

    public AlertDialog create() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.brush_dialog_title);
        builder.setIcon((int) R.drawable.ic_brush);
        builder.setSingleChoiceItems(this.mContext.getResources().getStringArray(R.array.brushes), this.mBrush, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                BrushPickerDialog.this.mListener.brushChanged(item);
                dialog.dismiss();
            }
        });
        return builder.create();
    }
}
