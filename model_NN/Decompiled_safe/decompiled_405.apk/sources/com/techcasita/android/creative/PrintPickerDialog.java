package com.techcasita.android.creative;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class PrintPickerDialog {
    private Context mContext;
    /* access modifiers changed from: private */
    public OnPrinterSelectedListener mListener;

    public interface OnPrinterSelectedListener {
        void printerSelected(int i);
    }

    public PrintPickerDialog(Context context, OnPrinterSelectedListener listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    public AlertDialog create() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.print_dialog_title);
        builder.setIcon((int) R.drawable.ic_printer);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(this.mContext.getResources().getStringArray(R.array.printers), -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                PrintPickerDialog.this.mListener.printerSelected(item);
                dialog.dismiss();
            }
        });
        return builder.create();
    }
}
