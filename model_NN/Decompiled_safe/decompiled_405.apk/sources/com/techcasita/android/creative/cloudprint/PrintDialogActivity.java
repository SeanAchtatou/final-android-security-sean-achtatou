package com.techcasita.android.creative.cloudprint;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.techcasita.android.creative.R;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class PrintDialogActivity extends Activity {
    private static final String CLOSE_POST_MESSAGE_NAME = "cp-dialog-on-close";
    private static final String JS_INTERFACE = "AndroidPrintDialog";
    private static final String PRINT_DIALOG_URL = "http://www.google.com/cloudprint/dialog.html";
    private static final int ZXING_SCAN_REQUEST = 65743;
    private static final String ZXING_URL = "http://zxing.appspot.com";
    Intent cloudPrintIntent;
    private WebView dialogWebView;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.print_dialog);
        this.dialogWebView = (WebView) findViewById(R.id.webview);
        this.cloudPrintIntent = getIntent();
        this.dialogWebView.getSettings().setJavaScriptEnabled(true);
        this.dialogWebView.setWebViewClient(new PrintDialogWebClient());
        this.dialogWebView.addJavascriptInterface(new PrintDialogJavaScriptInterface(), JS_INTERFACE);
        this.dialogWebView.loadUrl(PRINT_DIALOG_URL);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == ZXING_SCAN_REQUEST && resultCode == -1) {
            this.dialogWebView.loadUrl(intent.getStringExtra("SCAN_RESULT"));
        }
    }

    final class PrintDialogJavaScriptInterface {
        PrintDialogJavaScriptInterface() {
        }

        public String getType() {
            return "dataUrl";
        }

        public String getTitle() {
            return PrintDialogActivity.this.cloudPrintIntent.getExtras().getString(CloudPrintActivity.EXTRA_TITLE);
        }

        public String getContent() {
            try {
                InputStream is = PrintDialogActivity.this.getContentResolver().openInputStream(PrintDialogActivity.this.cloudPrintIntent.getData());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                for (int n = is.read(buffer); n >= 0; n = is.read(buffer)) {
                    baos.write(buffer, 0, n);
                }
                is.close();
                baos.flush();
                return "data:" + PrintDialogActivity.this.cloudPrintIntent.getType() + ";base64," + Base64.encodeToString(baos.toByteArray(), 0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "";
            } catch (IOException e2) {
                e2.printStackTrace();
                return "";
            }
        }

        public void onPostMessage(String message) {
            if (message.startsWith(PrintDialogActivity.CLOSE_POST_MESSAGE_NAME)) {
                PrintDialogActivity.this.finish();
            }
        }
    }

    private final class PrintDialogWebClient extends WebViewClient {
        private PrintDialogWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(PrintDialogActivity.ZXING_URL)) {
                Intent intentScan = new Intent("com.google.zxing.client.android.SCAN");
                intentScan.putExtra("SCAN_MODE", "QR_CODE_MODE");
                try {
                    PrintDialogActivity.this.startActivityForResult(intentScan, PrintDialogActivity.ZXING_SCAN_REQUEST);
                    return false;
                } catch (ActivityNotFoundException e) {
                    view.loadUrl(url);
                    return false;
                }
            } else {
                view.loadUrl(url);
                return false;
            }
        }

        public void onPageFinished(WebView view, String url) {
            if (PrintDialogActivity.PRINT_DIALOG_URL.equals(url)) {
                view.loadUrl("javascript:printDialog.setPrintDocument(printDialog.createPrintDocument(window.AndroidPrintDialog.getType(),window.AndroidPrintDialog.getTitle(),window.AndroidPrintDialog.getContent()))");
                view.loadUrl("javascript:window.addEventListener('message',function(evt){window.AndroidPrintDialog.onPostMessage(evt.data)}, false)");
            }
        }
    }
}
