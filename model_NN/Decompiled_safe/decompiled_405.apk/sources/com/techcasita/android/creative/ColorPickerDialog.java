package com.techcasita.android.creative;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class ColorPickerDialog extends Dialog {
    private int mInitialColor;
    /* access modifiers changed from: private */
    public OnColorChangedListener mListener;

    public interface OnColorChangedListener {
        void colorChanged(int i);
    }

    private static class ColorPickerView extends View {
        private static int CENTER_RADIUS = 50;
        private static int CENTER_X = 200;
        private static int CENTER_Y = 200;
        private static final float PI = 3.1415927f;
        private static int STROKE_WIDTH = 50;
        private Paint mCenterPaint;
        private final int[] mColors = {-65536, -65281, -16776961, -16711681, -16711936, -256, -65536};
        private boolean mHighlightCenter;
        private OnColorChangedListener mListener;
        private Paint mPaint;
        private boolean mTrackingCenter;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void}
         arg types: [int, int, int[], ?[OBJECT, ARRAY]]
         candidates:
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long, long):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int, int):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long[], float[]):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void} */
        ColorPickerView(Context c, OnColorChangedListener l, int size, int color) {
            super(c);
            CENTER_Y = size;
            CENTER_X = size;
            this.mListener = l;
            Shader s = new SweepGradient(0.0f, 0.0f, this.mColors, (float[]) null);
            this.mPaint = new Paint(1);
            this.mPaint.setShader(s);
            this.mPaint.setStyle(Paint.Style.STROKE);
            this.mPaint.setStrokeWidth((float) STROKE_WIDTH);
            this.mCenterPaint = new Paint(1);
            this.mCenterPaint.setColor(color);
            this.mCenterPaint.setStrokeWidth(5.0f);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            float r = ((float) CENTER_X) - (this.mPaint.getStrokeWidth() * 0.5f);
            canvas.translate((float) CENTER_X, (float) CENTER_Y);
            canvas.drawOval(new RectF(-r, -r, r, r), this.mPaint);
            canvas.drawCircle(0.0f, 0.0f, (float) CENTER_RADIUS, this.mCenterPaint);
            if (this.mTrackingCenter) {
                int c = this.mCenterPaint.getColor();
                this.mCenterPaint.setStyle(Paint.Style.STROKE);
                if (this.mHighlightCenter) {
                    this.mCenterPaint.setAlpha(255);
                } else {
                    this.mCenterPaint.setAlpha(128);
                }
                canvas.drawCircle(0.0f, 0.0f, ((float) CENTER_RADIUS) + this.mCenterPaint.getStrokeWidth(), this.mCenterPaint);
                this.mCenterPaint.setStyle(Paint.Style.FILL);
                this.mCenterPaint.setColor(c);
            }
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(CENTER_X * 2, CENTER_Y * 2);
        }

        private int ave(int s, int d, float p) {
            return Math.round(((float) (d - s)) * p) + s;
        }

        private int interpColor(int[] colors, float unit) {
            if (unit <= 0.0f) {
                return colors[0];
            }
            if (unit >= 1.0f) {
                return colors[colors.length - 1];
            }
            float p = unit * ((float) (colors.length - 1));
            int i = (int) p;
            float p2 = p - ((float) i);
            int c0 = colors[i];
            int c1 = colors[i + 1];
            return Color.argb(ave(Color.alpha(c0), Color.alpha(c1), p2), ave(Color.red(c0), Color.red(c1), p2), ave(Color.green(c0), Color.green(c1), p2), ave(Color.blue(c0), Color.blue(c1), p2));
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0048  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTouchEvent(android.view.MotionEvent r12) {
            /*
                r11 = this;
                r10 = 0
                r9 = 1
                float r5 = r12.getX()
                int r6 = com.techcasita.android.creative.ColorPickerDialog.ColorPickerView.CENTER_X
                float r6 = (float) r6
                float r3 = r5 - r6
                float r5 = r12.getY()
                int r6 = com.techcasita.android.creative.ColorPickerDialog.ColorPickerView.CENTER_Y
                float r6 = (float) r6
                float r4 = r5 - r6
                float r5 = r3 * r3
                float r6 = r4 * r4
                float r5 = r5 + r6
                double r5 = (double) r5
                double r5 = java.lang.Math.sqrt(r5)
                int r7 = com.techcasita.android.creative.ColorPickerDialog.ColorPickerView.CENTER_RADIUS
                double r7 = (double) r7
                int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                if (r5 > 0) goto L_0x002e
                r1 = r9
            L_0x0026:
                int r5 = r12.getAction()
                switch(r5) {
                    case 0: goto L_0x0030;
                    case 1: goto L_0x006b;
                    case 2: goto L_0x003a;
                    default: goto L_0x002d;
                }
            L_0x002d:
                return r9
            L_0x002e:
                r1 = r10
                goto L_0x0026
            L_0x0030:
                r11.mTrackingCenter = r1
                if (r1 == 0) goto L_0x003a
                r11.mHighlightCenter = r9
                r11.invalidate()
                goto L_0x002d
            L_0x003a:
                boolean r5 = r11.mTrackingCenter
                if (r5 == 0) goto L_0x0048
                boolean r5 = r11.mHighlightCenter
                if (r5 == r1) goto L_0x002d
                r11.mHighlightCenter = r1
                r11.invalidate()
                goto L_0x002d
            L_0x0048:
                double r5 = (double) r4
                double r7 = (double) r3
                double r5 = java.lang.Math.atan2(r5, r7)
                float r0 = (float) r5
                r5 = 1086918619(0x40c90fdb, float:6.2831855)
                float r2 = r0 / r5
                r5 = 0
                int r5 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
                if (r5 >= 0) goto L_0x005c
                r5 = 1065353216(0x3f800000, float:1.0)
                float r2 = r2 + r5
            L_0x005c:
                android.graphics.Paint r5 = r11.mCenterPaint
                int[] r6 = r11.mColors
                int r6 = r11.interpColor(r6, r2)
                r5.setColor(r6)
                r11.invalidate()
                goto L_0x002d
            L_0x006b:
                boolean r5 = r11.mTrackingCenter
                if (r5 == 0) goto L_0x002d
                if (r1 == 0) goto L_0x007c
                com.techcasita.android.creative.ColorPickerDialog$OnColorChangedListener r5 = r11.mListener
                android.graphics.Paint r6 = r11.mCenterPaint
                int r6 = r6.getColor()
                r5.colorChanged(r6)
            L_0x007c:
                r11.mTrackingCenter = r10
                r11.invalidate()
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.techcasita.android.creative.ColorPickerDialog.ColorPickerView.onTouchEvent(android.view.MotionEvent):boolean");
        }
    }

    public ColorPickerDialog(Context context, OnColorChangedListener listener, int initialColor) {
        super(context);
        this.mListener = listener;
        this.mInitialColor = initialColor;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnColorChangedListener l = new OnColorChangedListener() {
            public void colorChanged(int color) {
                ColorPickerDialog.this.mListener.colorChanged(color);
                ColorPickerDialog.this.dismiss();
            }
        };
        WindowManager wm = (WindowManager) getContext().getSystemService("window");
        setContentView(new ColorPickerView(getContext(), l, Math.min(wm.getDefaultDisplay().getWidth(), wm.getDefaultDisplay().getHeight()) / 3, this.mInitialColor));
        setTitle((int) R.string.color_dialog_title);
    }
}
