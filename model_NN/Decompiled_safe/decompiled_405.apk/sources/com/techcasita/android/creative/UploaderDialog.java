package com.techcasita.android.creative;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.aetrion.flickr.Flickr;
import com.aetrion.flickr.REST;
import com.aetrion.flickr.RequestContext;
import com.aetrion.flickr.auth.Auth;
import com.aetrion.flickr.auth.Permission;
import com.aetrion.flickr.uploader.UploadMetaData;
import com.aetrion.flickr.uploader.Uploader;

public class UploaderDialog {
    private static final int MIN = 10000;
    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            int total = msg.what;
            UploaderDialog.this.mDialog.setProgress(total);
            if (total >= 100) {
                UploaderDialog.this.mDialog.dismiss();
                UploaderDialog.this.mThread.setState(0);
                UploaderDialog.this.mListener.uploaded(UploaderDialog.this.mMessage);
            }
        }
    };
    /* access modifiers changed from: private */
    public final byte[] mBytes;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public ProgressDialog mDialog;
    /* access modifiers changed from: private */
    public final OnUploadListener mListener;
    /* access modifiers changed from: private */
    public String mMessage;
    /* access modifiers changed from: private */
    public String mName = "John Dow";
    /* access modifiers changed from: private */
    public UploadThread mThread;

    public interface OnUploadListener {
        void uploaded(String str);
    }

    public UploaderDialog(Context context, OnUploadListener listener, byte[] ba) {
        this.mContext = context;
        this.mListener = listener;
        this.mBytes = ba;
        this.mMessage = context.getResources().getString(R.string.flickr_uploaded_ok);
        for (Account account : AccountManager.get(this.mContext).getAccounts()) {
            this.mName = account.name;
            if (this.mName != null && this.mName.length() > 0) {
                break;
            }
        }
        int i = this.mName.indexOf("@");
        if (i > 0) {
            this.mName = this.mName.substring(0, i);
        }
        Log.v(UploaderDialog.class.getSimpleName(), "BMP Size =" + ba.length);
    }

    /* access modifiers changed from: protected */
    public Dialog create() {
        this.mDialog = new ProgressDialog(this.mContext);
        this.mDialog.setProgressStyle(1);
        this.mDialog.setMessage("Uploading...");
        this.mThread = new UploadThread(this.handler);
        this.mThread.start();
        return this.mDialog;
    }

    private class UploadThread extends Thread {
        private static final transient String GID = "1690480@N25";
        private static final transient String Key = "258f5f54900c56bb8c890db0fbee8668";
        static final int STATE_DONE = 0;
        static final int STATE_RUNNING = 1;
        private static final transient String Secret = "8096ec07f87f0686";
        private static final transient String Token = "72157627229820656-882948d4afb3362e";
        final Handler mHandler;
        int mState;

        UploadThread(Handler h) {
            this.mHandler = h;
        }

        public void run() {
            this.mState = 1;
            while (this.mState == 1) {
                try {
                    Flickr flickr = new Flickr(Key, Secret, new REST());
                    Uploader uploader = new Uploader(Key, Secret);
                    RequestContext requestContext = RequestContext.getRequestContext();
                    this.mHandler.sendEmptyMessage(10);
                    Auth auth = new Auth();
                    auth.setPermission(Permission.WRITE);
                    auth.setToken(Token);
                    this.mHandler.sendEmptyMessage(20);
                    requestContext.setAuth(auth);
                    this.mHandler.sendEmptyMessage(25);
                    UploadMetaData meta = new UploadMetaData();
                    meta.setSafetyLevel("1");
                    meta.setAsync(false);
                    meta.setContentType("1");
                    meta.setDescription(UploaderDialog.this.mContext.getResources().getString(R.string.flickr_credentials) + " " + UploaderDialog.this.mName);
                    meta.setTitle(UploaderDialog.this.mContext.getResources().getString(R.string.flickr_title) + " " + UploaderDialog.this.mName);
                    meta.setPublicFlag(true);
                    this.mHandler.sendEmptyMessage(45);
                    String photoid = uploader.upload(UploaderDialog.this.mBytes, meta);
                    this.mHandler.sendEmptyMessage(75);
                    if (photoid != null && photoid.length() > 0) {
                        flickr.getPoolsInterface().add(photoid, GID);
                    }
                } catch (Exception e) {
                    String unused = UploaderDialog.this.mMessage = UploaderDialog.this.mContext.getResources().getString(R.string.flickr_uploaded_ko) + " : " + e.getMessage();
                } finally {
                    this.mState = 0;
                    this.mHandler.sendEmptyMessage(100);
                }
            }
        }

        public void setState(int state) {
            this.mState = state;
        }
    }
}
