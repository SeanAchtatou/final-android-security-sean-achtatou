package com.google.api.client.googleapis.auth.authsub;

import com.google.api.client.http.HttpExecuteIntercepter;
import com.google.api.client.http.HttpRequest;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;

@Deprecated
final class AuthSubIntercepter implements HttpExecuteIntercepter {
    private final PrivateKey privateKey;
    private final String token;

    AuthSubIntercepter(String token2, PrivateKey privateKey2) {
        this.token = token2;
        this.privateKey = privateKey2;
    }

    public void intercept(HttpRequest request) throws IOException {
        String header;
        try {
            String token2 = this.token;
            PrivateKey privateKey2 = this.privateKey;
            if (token2 == null) {
                header = null;
            } else if (privateKey2 == null) {
                header = AuthSub.getAuthorizationHeaderValue(token2);
            } else {
                header = AuthSub.getAuthorizationHeaderValue(token2, privateKey2, request.method.name(), request.url.build());
            }
            request.headers.authorization = header;
        } catch (GeneralSecurityException e) {
            IOException wrap = new IOException();
            wrap.initCause(e);
            throw wrap;
        }
    }
}
