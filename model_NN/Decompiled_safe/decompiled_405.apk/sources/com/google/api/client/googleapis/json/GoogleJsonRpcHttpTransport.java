package com.google.api.client.googleapis.json;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.rpc2.JsonRpcRequest;
import java.io.IOException;
import java.util.List;

public final class GoogleJsonRpcHttpTransport {
    public String accept = this.contentType;
    public String contentType = "application/json-rpc";
    public JsonFactory jsonFactory;
    public GenericUrl rpcServerUrl;
    public HttpTransport transport;

    public HttpRequest buildPostRequest(JsonRpcRequest request) {
        return internalExecute(request);
    }

    public HttpRequest buildPostRequest(List<JsonRpcRequest> requests) {
        return internalExecute(requests);
    }

    private HttpRequest internalExecute(Object data) {
        JsonHttpContent content = new JsonHttpContent();
        content.jsonFactory = this.jsonFactory;
        content.contentType = this.contentType;
        content.data = data;
        try {
            HttpRequest httpRequest = this.transport.createRequestFactory().buildPostRequest(this.rpcServerUrl, content);
            httpRequest.headers.accept = this.accept;
            return httpRequest;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
