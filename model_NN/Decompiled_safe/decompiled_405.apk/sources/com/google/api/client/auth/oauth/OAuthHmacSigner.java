package com.google.api.client.auth.oauth;

import com.google.api.client.auth.HmacSha;
import java.security.GeneralSecurityException;

public final class OAuthHmacSigner implements OAuthSigner {
    public String clientSharedSecret;
    public String tokenSharedSecret;

    public String getSignatureMethod() {
        return "HMAC-SHA1";
    }

    public String computeSignature(String signatureBaseString) throws GeneralSecurityException {
        StringBuilder keyBuf = new StringBuilder();
        String clientSharedSecret2 = this.clientSharedSecret;
        if (clientSharedSecret2 != null) {
            keyBuf.append(OAuthParameters.escape(clientSharedSecret2));
        }
        keyBuf.append('&');
        String tokenSharedSecret2 = this.tokenSharedSecret;
        if (tokenSharedSecret2 != null) {
            keyBuf.append(OAuthParameters.escape(tokenSharedSecret2));
        }
        return HmacSha.sign(keyBuf.toString(), signatureBaseString);
    }
}
