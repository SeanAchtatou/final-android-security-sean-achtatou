package com.google.api.client.http;

import com.google.api.client.util.Data;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.Strings;
import com.google.api.client.util.Types;
import com.google.api.client.util.escape.CharEscapers;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public final class UrlEncodedContent implements HttpContent {
    private byte[] content;
    public String contentType = UrlEncodedParser.CONTENT_TYPE;
    public Object data;

    public String getEncoding() {
        return null;
    }

    public long getLength() {
        return (long) computeContent().length;
    }

    public String getType() {
        return this.contentType;
    }

    public void writeTo(OutputStream out) throws IOException {
        out.write(computeContent());
    }

    private byte[] computeContent() {
        if (this.content == null) {
            StringBuilder buf = new StringBuilder();
            boolean first = true;
            for (Map.Entry<String, Object> nameValueEntry : Data.mapOf(this.data).entrySet()) {
                Object value = nameValueEntry.getValue();
                if (value != null) {
                    String name = CharEscapers.escapeUri((String) nameValueEntry.getKey());
                    Class<?> cls = value.getClass();
                    if ((value instanceof Iterable) || cls.isArray()) {
                        for (Object repeatedValue : Types.iterableOf(value)) {
                            first = appendParam(first, buf, name, repeatedValue);
                        }
                    } else {
                        first = appendParam(first, buf, name, value);
                    }
                }
            }
            this.content = Strings.toBytesUtf8(buf.toString());
        }
        return this.content;
    }

    private static boolean appendParam(boolean first, StringBuilder buf, String name, Object value) {
        if (value != null && !Data.isNull(value)) {
            if (first) {
                first = false;
            } else {
                buf.append('&');
            }
            buf.append(name);
            String stringValue = CharEscapers.escapeUri(value instanceof Enum ? FieldInfo.of((Enum) value).getName() : value.toString());
            if (stringValue.length() != 0) {
                buf.append('=').append(stringValue);
            }
        }
        return first;
    }

    public boolean retrySupported() {
        return true;
    }
}
