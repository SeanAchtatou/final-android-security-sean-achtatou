package com.google.api.client.util;

import com.google.common.base.Preconditions;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.WeakHashMap;

public class FieldInfo {
    private static final Map<Field, FieldInfo> CACHE = new WeakHashMap();
    @Deprecated
    public final Field field;
    @Deprecated
    public final boolean isFinal;
    @Deprecated
    public final boolean isPrimitive;
    @Deprecated
    public final String name;
    @Deprecated
    public final Class<?> type;

    public static FieldInfo of(Enum<?> enumValue) {
        boolean z;
        try {
            FieldInfo result = of(enumValue.getClass().getField(enumValue.name()));
            if (result != null) {
                z = true;
            } else {
                z = false;
            }
            Preconditions.checkArgument(z, "enum constant missing @Value or @NullValue annotation: %s", new Object[]{enumValue});
            return result;
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.api.client.util.FieldInfo of(java.lang.reflect.Field r9) {
        /*
            r8 = 0
            if (r9 != 0) goto L_0x0005
            r6 = r8
        L_0x0004:
            return r6
        L_0x0005:
            java.util.Map<java.lang.reflect.Field, com.google.api.client.util.FieldInfo> r6 = com.google.api.client.util.FieldInfo.CACHE
            monitor-enter(r6)
            java.util.Map<java.lang.reflect.Field, com.google.api.client.util.FieldInfo> r7 = com.google.api.client.util.FieldInfo.CACHE     // Catch:{ all -> 0x0070 }
            java.lang.Object r0 = r7.get(r9)     // Catch:{ all -> 0x0070 }
            com.google.api.client.util.FieldInfo r0 = (com.google.api.client.util.FieldInfo) r0     // Catch:{ all -> 0x0070 }
            boolean r2 = r9.isEnumConstant()     // Catch:{ all -> 0x0070 }
            if (r0 != 0) goto L_0x0048
            if (r2 != 0) goto L_0x0022
            int r7 = r9.getModifiers()     // Catch:{ all -> 0x0070 }
            boolean r7 = java.lang.reflect.Modifier.isStatic(r7)     // Catch:{ all -> 0x0070 }
            if (r7 != 0) goto L_0x0048
        L_0x0022:
            if (r2 == 0) goto L_0x005a
            java.lang.Class<com.google.api.client.util.Value> r7 = com.google.api.client.util.Value.class
            java.lang.annotation.Annotation r5 = r9.getAnnotation(r7)     // Catch:{ all -> 0x0070 }
            com.google.api.client.util.Value r5 = (com.google.api.client.util.Value) r5     // Catch:{ all -> 0x0070 }
            if (r5 == 0) goto L_0x004b
            java.lang.String r1 = r5.value()     // Catch:{ all -> 0x0070 }
        L_0x0032:
            java.lang.String r7 = "##default"
            boolean r7 = r7.equals(r1)     // Catch:{ all -> 0x0070 }
            if (r7 == 0) goto L_0x003e
            java.lang.String r1 = r9.getName()     // Catch:{ all -> 0x0070 }
        L_0x003e:
            com.google.api.client.util.FieldInfo r0 = new com.google.api.client.util.FieldInfo     // Catch:{ all -> 0x0070 }
            r0.<init>(r9, r1)     // Catch:{ all -> 0x0070 }
            java.util.Map<java.lang.reflect.Field, com.google.api.client.util.FieldInfo> r7 = com.google.api.client.util.FieldInfo.CACHE     // Catch:{ all -> 0x0070 }
            r7.put(r9, r0)     // Catch:{ all -> 0x0070 }
        L_0x0048:
            monitor-exit(r6)     // Catch:{ all -> 0x0070 }
            r6 = r0
            goto L_0x0004
        L_0x004b:
            java.lang.Class<com.google.api.client.util.NullValue> r7 = com.google.api.client.util.NullValue.class
            java.lang.annotation.Annotation r4 = r9.getAnnotation(r7)     // Catch:{ all -> 0x0070 }
            com.google.api.client.util.NullValue r4 = (com.google.api.client.util.NullValue) r4     // Catch:{ all -> 0x0070 }
            if (r4 == 0) goto L_0x0057
            r1 = 0
            goto L_0x0032
        L_0x0057:
            monitor-exit(r6)     // Catch:{ all -> 0x0070 }
            r6 = r8
            goto L_0x0004
        L_0x005a:
            java.lang.Class<com.google.api.client.util.Key> r7 = com.google.api.client.util.Key.class
            java.lang.annotation.Annotation r3 = r9.getAnnotation(r7)     // Catch:{ all -> 0x0070 }
            com.google.api.client.util.Key r3 = (com.google.api.client.util.Key) r3     // Catch:{ all -> 0x0070 }
            if (r3 != 0) goto L_0x0067
            monitor-exit(r6)     // Catch:{ all -> 0x0070 }
            r6 = r8
            goto L_0x0004
        L_0x0067:
            java.lang.String r1 = r3.value()     // Catch:{ all -> 0x0070 }
            r7 = 1
            r9.setAccessible(r7)     // Catch:{ all -> 0x0070 }
            goto L_0x0032
        L_0x0070:
            r7 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0070 }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.api.client.util.FieldInfo.of(java.lang.reflect.Field):com.google.api.client.util.FieldInfo");
    }

    FieldInfo(Field field2, String name2) {
        this.field = field2;
        this.name = name2 == null ? null : name2.intern();
        this.isFinal = Modifier.isFinal(field2.getModifiers());
        this.type = field2.getType();
        this.isPrimitive = Data.isPrimitive(getType());
    }

    public Field getField() {
        return this.field;
    }

    public String getName() {
        return this.name;
    }

    public Class<?> getType() {
        return this.field.getType();
    }

    public Type getGenericType() {
        return this.field.getGenericType();
    }

    public boolean isFinal() {
        return Modifier.isFinal(this.field.getModifiers());
    }

    public boolean isPrimitive() {
        return this.isPrimitive;
    }

    public Object getValue(Object obj) {
        return getFieldValue(this.field, obj);
    }

    public void setValue(Object obj, Object value) {
        setFieldValue(this.field, obj, value);
    }

    public ClassInfo getClassInfo() {
        return ClassInfo.of(this.field.getDeclaringClass());
    }

    public <T extends Enum<T>> T enumValue() {
        return Enum.valueOf(this.field.getDeclaringClass(), this.field.getName());
    }

    @Deprecated
    public static boolean isPrimitive(Class<?> fieldClass) {
        return fieldClass.isPrimitive() || fieldClass == Character.class || fieldClass == String.class || fieldClass == Integer.class || fieldClass == Long.class || fieldClass == Short.class || fieldClass == Byte.class || fieldClass == Float.class || fieldClass == Double.class || fieldClass == BigInteger.class || fieldClass == BigDecimal.class || fieldClass == DateTime.class || fieldClass == Boolean.class;
    }

    @Deprecated
    public static boolean isPrimitive(Object fieldValue) {
        return fieldValue == null || isPrimitive(fieldValue.getClass());
    }

    public static Object getFieldValue(Field field2, Object obj) {
        try {
            return field2.get(obj);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void setFieldValue(Field field2, Object obj, Object value) {
        if (Modifier.isFinal(field2.getModifiers())) {
            Object finalValue = getFieldValue(field2, obj);
            if (value == null) {
                if (finalValue == null) {
                    return;
                }
            } else if (value.equals(finalValue)) {
                return;
            }
            throw new IllegalArgumentException("expected final value <" + finalValue + "> but was <" + value + "> on " + field2.getName() + " field in " + obj.getClass().getName());
        }
        try {
            field2.set(obj, value);
        } catch (SecurityException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @Deprecated
    public static Object parsePrimitiveValue(Class<?> primitiveClass, String stringValue) {
        if (stringValue == null || primitiveClass == null || primitiveClass == String.class) {
            return stringValue;
        }
        if (primitiveClass == Character.class || primitiveClass == Character.TYPE) {
            if (stringValue.length() == 1) {
                return Character.valueOf(stringValue.charAt(0));
            }
            throw new IllegalArgumentException("expected type Character/char but got " + primitiveClass);
        } else if (primitiveClass == Boolean.class || primitiveClass == Boolean.TYPE) {
            return Boolean.valueOf(stringValue);
        } else {
            if (primitiveClass == Byte.class || primitiveClass == Byte.TYPE) {
                return Byte.valueOf(stringValue);
            }
            if (primitiveClass == Short.class || primitiveClass == Short.TYPE) {
                return Short.valueOf(stringValue);
            }
            if (primitiveClass == Integer.class || primitiveClass == Integer.TYPE) {
                return Integer.valueOf(stringValue);
            }
            if (primitiveClass == Long.class || primitiveClass == Long.TYPE) {
                return Long.valueOf(stringValue);
            }
            if (primitiveClass == Float.class || primitiveClass == Float.TYPE) {
                return Float.valueOf(stringValue);
            }
            if (primitiveClass == Double.class || primitiveClass == Double.TYPE) {
                return Double.valueOf(stringValue);
            }
            if (primitiveClass == DateTime.class) {
                return DateTime.parseRfc3339(stringValue);
            }
            if (primitiveClass == BigInteger.class) {
                return new BigInteger(stringValue);
            }
            if (primitiveClass == BigDecimal.class) {
                return new BigDecimal(stringValue);
            }
            throw new IllegalArgumentException("expected primitive class, but got: " + primitiveClass);
        }
    }
}
