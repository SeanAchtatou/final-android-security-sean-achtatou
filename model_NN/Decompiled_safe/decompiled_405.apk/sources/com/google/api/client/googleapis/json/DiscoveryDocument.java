package com.google.api.client.googleapis.json;

import com.google.api.client.util.ArrayMap;
import com.google.api.client.util.Key;
import java.util.Map;

@Deprecated
public final class DiscoveryDocument {
    public final APIDefinition apiDefinition = new APIDefinition();

    public static final class APIDefinition extends ArrayMap<String, ServiceDefinition> {
    }

    public static final class ServiceMethod {
        @Key
        public String httpMethod;
        @Key
        public final String methodType = "rest";
        @Key
        public Map<String, ServiceParameter> parameters;
        @Key
        public String pathUrl;
    }

    public static final class ServiceParameter {
        @Key
        public boolean required;
    }

    public static final class ServiceResource {
        @Key
        public Map<String, ServiceMethod> methods;
    }

    public static final class ServiceDefinition {
        @Key
        public String baseUrl;
        @Key
        public Map<String, ServiceResource> resources;

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public ServiceMethod getResourceMethod(String methodIdentifier) {
            int dot = methodIdentifier.indexOf(46);
            String resourceName = methodIdentifier.substring(0, dot);
            String methodName = methodIdentifier.substring(dot + 1);
            ServiceResource resource = this.resources.get(resourceName);
            if (resource == null) {
                return null;
            }
            return resource.methods.get(methodName);
        }

        /* access modifiers changed from: package-private */
        public String getResourceUrl(String methodIdentifier) {
            return this.baseUrl + getResourceMethod(methodIdentifier).pathUrl;
        }
    }

    DiscoveryDocument() {
    }
}
