package com.google.api.client.http.javanet;

import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.LowLevelHttpResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class NetHttpResponse extends LowLevelHttpResponse {
    private final HttpURLConnection connection;
    private final ArrayList<String> headerNames = new ArrayList<>();
    private final ArrayList<String> headerValues = new ArrayList<>();
    private final int responseCode;
    private final String responseMessage;

    NetHttpResponse(HttpURLConnection connection2) throws IOException {
        this.connection = connection2;
        this.responseCode = connection2.getResponseCode();
        this.responseMessage = connection2.getResponseMessage();
        List<String> headerNames2 = this.headerNames;
        List<String> headerValues2 = this.headerValues;
        for (Map.Entry<String, List<String>> entry : connection2.getHeaderFields().entrySet()) {
            String key = (String) entry.getKey();
            if (key != null) {
                for (String value : (List) entry.getValue()) {
                    if (value != null) {
                        headerNames2.add(key);
                        headerValues2.add(value);
                    }
                }
            }
        }
    }

    public int getStatusCode() {
        return this.responseCode;
    }

    public InputStream getContent() throws IOException {
        HttpURLConnection connection2 = this.connection;
        return HttpResponse.isSuccessStatusCode(this.responseCode) ? connection2.getInputStream() : connection2.getErrorStream();
    }

    public String getContentEncoding() {
        return this.connection.getContentEncoding();
    }

    public long getContentLength() {
        String string = this.connection.getHeaderField("Content-Length");
        if (string == null) {
            return -1;
        }
        return Long.parseLong(string);
    }

    public String getContentType() {
        return this.connection.getHeaderField("Content-Type");
    }

    public String getReasonPhrase() {
        return this.responseMessage;
    }

    public String getStatusLine() {
        String result = this.connection.getHeaderField(0);
        if (result == null || !result.startsWith("HTTP/1.")) {
            return null;
        }
        return result;
    }

    public int getHeaderCount() {
        return this.headerNames.size();
    }

    public String getHeaderName(int index) {
        return this.headerNames.get(index);
    }

    public String getHeaderValue(int index) {
        return this.headerValues.get(index);
    }

    public void disconnect() {
        this.connection.disconnect();
    }
}
