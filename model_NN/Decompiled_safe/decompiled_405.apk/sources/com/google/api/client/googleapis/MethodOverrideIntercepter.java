package com.google.api.client.googleapis;

import com.google.api.client.http.HttpExecuteIntercepter;
import com.google.api.client.http.HttpMethod;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.InputStreamContent;
import java.util.EnumSet;

@Deprecated
public class MethodOverrideIntercepter implements HttpExecuteIntercepter {
    public EnumSet<HttpMethod> override = EnumSet.of(HttpMethod.DELETE, HttpMethod.HEAD, HttpMethod.PATCH, HttpMethod.PUT);

    public void intercept(HttpRequest request) {
        if (overrideThisMethod(request)) {
            HttpMethod method = request.method;
            request.method = HttpMethod.POST;
            request.headers.set("X-HTTP-Method-Override", method.name());
            if (request.content == null) {
                InputStreamContent content = new InputStreamContent();
                content.setByteArrayInput(new byte[0]);
                request.content = content;
            }
        }
    }

    private boolean overrideThisMethod(HttpRequest request) {
        HttpMethod method = request.method;
        if (method != HttpMethod.GET && method != HttpMethod.POST && this.override.contains(method)) {
            return true;
        }
        switch (method) {
            case PATCH:
                return !request.transport.supportsPatch();
            case HEAD:
                return !request.transport.supportsHead();
            default:
                return false;
        }
    }
}
