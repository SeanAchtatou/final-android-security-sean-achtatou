package com.google.api.client.googleapis;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.util.Data;
import com.google.api.client.util.Key;
import com.google.api.client.util.escape.CharEscapers;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import java.util.HashMap;
import java.util.Map;

public class GoogleUrl extends GenericUrl {
    @Key
    public String alt;
    @Key
    public String fields;
    @Key
    public String key;
    @Key
    public Boolean prettyprint;
    @Key
    public String userip;

    public GoogleUrl() {
    }

    public GoogleUrl(String encodedUrl) {
        super(encodedUrl);
    }

    public GoogleUrl clone() {
        return (GoogleUrl) super.clone();
    }

    public static GoogleUrl create(String encodedServerUrl, String pathTemplate, Object parameters) throws IllegalArgumentException {
        GoogleUrl url = new GoogleUrl(encodedServerUrl);
        HashMap<String, Object> requestMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : Data.mapOf(parameters).entrySet()) {
            Object value = entry.getValue();
            if (value != null) {
                requestMap.put(entry.getKey(), value);
            }
        }
        url.appendRawPath(expandUriTemplates(pathTemplate, requestMap));
        url.putAll(requestMap);
        return url;
    }

    @VisibleForTesting
    static String expandUriTemplates(String pathUri, HashMap<String, Object> variableMap) throws IllegalArgumentException {
        boolean z;
        boolean z2;
        StringBuilder pathBuf = new StringBuilder();
        int cur = 0;
        int length = pathUri.length();
        while (true) {
            if (cur >= length) {
                break;
            }
            int next = pathUri.indexOf(123, cur);
            if (next == -1) {
                pathBuf.append(pathUri.substring(cur));
                break;
            }
            pathBuf.append(pathUri.substring(cur, next));
            int close = pathUri.indexOf(125, next + 2);
            String varName = pathUri.substring(next + 1, close);
            cur = close + 1;
            if (variableMap != null) {
                z = true;
            } else {
                z = false;
            }
            Preconditions.checkArgument(z, "no variable map supplied for parameterize path: %s", new Object[]{varName});
            Object value = variableMap.remove(varName);
            if (value != null) {
                z2 = true;
            } else {
                z2 = false;
            }
            Preconditions.checkArgument(z2, "missing required path parameter: %s", new Object[]{varName});
            pathBuf.append(CharEscapers.escapeUriPath(value.toString()));
        }
        return pathBuf.toString();
    }
}
