package com.google.api.client.http;

import java.io.IOException;

public final class HttpRequestFactory {
    public final HttpRequestInitializer initializer;
    public final HttpTransport transport;

    HttpRequestFactory(HttpTransport transport2, HttpRequestInitializer initializer2) {
        this.transport = transport2;
        this.initializer = initializer2;
    }

    public HttpRequest buildRequest(HttpMethod method, GenericUrl url, HttpContent content) throws IOException {
        HttpRequest request = this.transport.buildRequest();
        if (this.initializer != null) {
            this.initializer.initialize(request);
        }
        request.method = method;
        request.url = url;
        request.content = content;
        return request;
    }

    public HttpRequest buildDeleteRequest(GenericUrl url) throws IOException {
        return buildRequest(HttpMethod.DELETE, url, null);
    }

    public HttpRequest buildGetRequest(GenericUrl url) throws IOException {
        return buildRequest(HttpMethod.GET, url, null);
    }

    public HttpRequest buildPostRequest(GenericUrl url, HttpContent content) throws IOException {
        return buildRequest(HttpMethod.POST, url, content);
    }

    public HttpRequest buildPutRequest(GenericUrl url, HttpContent content) throws IOException {
        return buildRequest(HttpMethod.PUT, url, content);
    }

    public HttpRequest buildPatchRequest(GenericUrl url, HttpContent content) throws IOException {
        return buildRequest(HttpMethod.PATCH, url, content);
    }

    public HttpRequest buildHeadRequest(GenericUrl url) throws IOException {
        return buildRequest(HttpMethod.HEAD, url, null);
    }
}
