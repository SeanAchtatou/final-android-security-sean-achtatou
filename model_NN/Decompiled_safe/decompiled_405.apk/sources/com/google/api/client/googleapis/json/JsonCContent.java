package com.google.api.client.googleapis.json;

import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonEncoding;
import com.google.api.client.json.JsonGenerator;
import java.io.IOException;
import java.io.OutputStream;

public final class JsonCContent extends JsonHttpContent {
    public void writeTo(OutputStream out) throws IOException {
        JsonGenerator generator = this.jsonFactory.createJsonGenerator(out, JsonEncoding.UTF8);
        generator.writeStartObject();
        generator.writeFieldName("data");
        generator.serialize(this.data);
        generator.writeEndObject();
        generator.flush();
    }
}
