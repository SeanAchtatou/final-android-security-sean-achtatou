package com.google.api.client.http;

import java.io.IOException;

@Deprecated
public interface HttpExecuteIntercepter {
    void intercept(HttpRequest httpRequest) throws IOException;
}
