package com.google.api.client.testing.json;

import com.google.api.client.json.JsonEncoding;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonGenerator;
import com.google.api.client.util.Key;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import junit.framework.TestCase;

public abstract class AbstractJsonGeneratorTest extends TestCase {
    private static final String JSON_ENTRY = "{\"title\":\"foo\"}";
    private static final String JSON_FEED = "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}";

    public static final class Feed {
        @Key
        public Collection<Entry> entries = new ArrayList();
    }

    /* access modifiers changed from: protected */
    public abstract JsonFactory newFactory();

    public AbstractJsonGeneratorTest(String name) {
        super(name);
    }

    public final void testGenerateEntry() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JsonGenerator generator = newFactory().createJsonGenerator(out, JsonEncoding.UTF8);
        generator.serialize(new Entry("foo"));
        generator.flush();
        assertEquals(JSON_ENTRY, new String(out.toByteArray()));
    }

    public final void testGenerateFeed() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JsonGenerator generator = newFactory().createJsonGenerator(out, JsonEncoding.UTF8);
        Feed feed = new Feed();
        feed.entries.add(new Entry("foo"));
        feed.entries.add(new Entry("bar"));
        generator.serialize(feed);
        generator.flush();
        assertEquals(JSON_FEED, new String(out.toByteArray()));
    }

    public static final class Entry {
        @Key
        public String title;

        Entry(String title2) {
            this.title = title2;
        }
    }
}
