package com.google.api.client.googleapis.json;

import com.google.api.client.googleapis.GoogleUrl;
import com.google.api.client.googleapis.json.DiscoveryDocument;
import com.google.api.client.http.HttpMethod;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.CustomizeJsonParser;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonParser;
import com.google.common.base.Preconditions;
import java.io.IOException;

@Deprecated
public final class GoogleApi {
    public HttpTransport discoveryTransport;
    public GoogleUrl discoveryUrl = new GoogleUrl("https://www.googleapis.com/discovery/0.1/describe");
    public JsonFactory jsonFactory;
    public String name;
    public DiscoveryDocument.ServiceDefinition serviceDefinition;
    public HttpTransport transport;
    public String version;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.api.client.util.GenericData.put(java.lang.Object, java.lang.Object):java.lang.Object
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object */
    public void load() throws IOException {
        GoogleUrl url = this.discoveryUrl.clone();
        url.put("api", (Object) this.name);
        JsonParser parser = JsonCParser.parserForResponse(this.jsonFactory, this.discoveryTransport.createRequestFactory().buildGetRequest(url).execute());
        parser.skipToKey(this.name);
        DiscoveryDocument doc = new DiscoveryDocument();
        parser.parseAndClose(doc.apiDefinition, (CustomizeJsonParser) null);
        this.serviceDefinition = (DiscoveryDocument.ServiceDefinition) doc.apiDefinition.get(this.version);
        Preconditions.checkNotNull(this.serviceDefinition, "version not found: %s", new Object[]{this.version});
    }

    public HttpRequest buildRequest(String fullyQualifiedMethodName, Object parameters) throws IOException {
        Preconditions.checkNotNull(this.name);
        Preconditions.checkNotNull(this.version);
        Preconditions.checkNotNull(this.discoveryTransport);
        Preconditions.checkNotNull(fullyQualifiedMethodName);
        if (this.serviceDefinition == null) {
            load();
        }
        DiscoveryDocument.ServiceMethod method = this.serviceDefinition.getResourceMethod(fullyQualifiedMethodName);
        Preconditions.checkNotNull(method, "method not found: %s", new Object[]{fullyQualifiedMethodName});
        HttpRequest request = this.discoveryTransport.buildRequest();
        request.method = HttpMethod.valueOf(method.httpMethod);
        request.url = GoogleUrl.create(this.serviceDefinition.baseUrl, method.pathUrl, parameters);
        return request;
    }
}
