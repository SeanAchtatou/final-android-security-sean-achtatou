package com.google.api.client.googleapis;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpMethod;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import java.io.IOException;
import java.util.EnumSet;

public final class MethodOverride implements HttpExecuteInterceptor, HttpRequestInitializer {
    private final EnumSet<HttpMethod> override;

    public MethodOverride() {
        this.override = EnumSet.noneOf(HttpMethod.class);
    }

    public MethodOverride(EnumSet<HttpMethod> override2) {
        this.override = override2.clone();
    }

    public void initialize(HttpRequest request) {
        request.interceptor = this;
    }

    public void intercept(HttpRequest request) throws IOException {
        if (overrideThisMethod(request)) {
            HttpMethod method = request.method;
            request.method = HttpMethod.POST;
            request.headers.set("X-HTTP-Method-Override", method.name());
            if (request.content == null || request.content.getLength() == 0) {
                request.content = new ByteArrayContent(" ");
            }
        }
    }

    private boolean overrideThisMethod(HttpRequest request) {
        HttpMethod method = request.method;
        if (method != HttpMethod.GET && method != HttpMethod.POST && this.override.contains(method)) {
            return true;
        }
        switch (method) {
            case PATCH:
                return !request.transport.supportsPatch();
            case HEAD:
                return !request.transport.supportsHead();
            default:
                return false;
        }
    }
}
