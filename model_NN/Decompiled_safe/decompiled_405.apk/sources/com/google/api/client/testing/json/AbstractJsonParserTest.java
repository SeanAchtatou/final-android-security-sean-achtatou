package com.google.api.client.testing.json;

import com.google.api.client.json.CustomizeJsonParser;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonParser;
import com.google.api.client.json.JsonString;
import com.google.api.client.json.JsonToken;
import com.google.api.client.util.ArrayMap;
import com.google.api.client.util.Data;
import com.google.api.client.util.Key;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.techcasita.android.creative.cloudprint.CloudPrintActivity;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import junit.framework.TestCase;

public abstract class AbstractJsonParserTest extends TestCase {
    static final String ANY_TYPE = "{\"arr\":[1],\"bool\":true,\"nul\":null,\"num\":5,\"obj\":{\"key\":\"value\"},\"str\":\"value\"}";
    static final String ARRAY_TYPE = "{\"arr\":[4,5],\"arr2\":[[1,2],[3]],\"integerArr\":[6,7]}";
    static final String COLLECTION_TYPE = "{\"arr\":[[\"a\",\"b\"],[\"c\"]]}";
    static final String CONTAINED_MAP = "{\"map\":{\"title\":\"foo\"}}";
    static final String DOUBLE_LIST_TYPE_VARIABLE_TYPE = "{\"arr\":[null,[null,[1.0]]],\"list\":[null,[null,[1.0]]],\"nullValue\":null,\"value\":[1.0]}";
    static final String ENUM_VALUE = "{\"nullValue\":null,\"otherValue\":\"other\",\"value\":\"VALUE\"}";
    static final String FLOAT_MAP_TYPE_VARIABLE_TYPE = "{\"arr\":[null,[null,{\"a\":1.0}]],\"list\":[null,[null,{\"a\":1.0}]],\"nullValue\":null,\"value\":{\"a\":1.0}}";
    static final String INTEGER_TYPE_VARIABLE_TYPE = "{\"arr\":[null,[null,1]],\"list\":[null,[null,1]],\"nullValue\":null,\"value\":1}";
    static final String INT_ARRAY_TYPE_VARIABLE_TYPE = "{\"arr\":[null,[null,[1]]],\"list\":[null,[null,[1]]],\"nullValue\":null,\"value\":[1]}";
    private static final String JSON_ENTRY = "{\"title\":\"foo\"}";
    private static final String JSON_FEED = "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}";
    static final String MAP_TYPE = "{\"value\":[{\"map1\":{\"k1\":1,\"k2\":2},\"map2\":{\"kk1\":3,\"kk2\":4}}]}";
    static final String NULL_VALUE = "{\"arr\":[null],\"arr2\":[null,[null]],\"value\":null}";
    static final String NUMBER_TYPES = "{\"bigDecimalValue\":1.0,\"bigIntegerValue\":1,\"byteObjValue\":1,\"byteValue\":1,\"doubleObjValue\":1.0,\"doubleValue\":1.0,\"floatObjValue\":1.0,\"floatValue\":1.0,\"intObjValue\":1,\"intValue\":1,\"longObjValue\":1,\"longValue\":1,\"shortObjValue\":1,\"shortValue\":1,\"yetAnotherBigDecimalValue\":1}";
    static final String NUMBER_TYPES_AS_STRING = "{\"bigDecimalValue\":\"1.0\",\"bigIntegerValue\":\"1\",\"byteObjValue\":\"1\",\"byteValue\":\"1\",\"doubleObjValue\":\"1.0\",\"doubleValue\":\"1.0\",\"floatObjValue\":\"1.0\",\"floatValue\":\"1.0\",\"intObjValue\":\"1\",\"intValue\":\"1\",\"longObjValue\":\"1\",\"longValue\":\"1\",\"shortObjValue\":\"1\",\"shortValue\":\"1\",\"yetAnotherBigDecimalValue\":\"1\"}";
    static final String TYPE_VARS = "{\"y\":{\"z\":{\"f\":[\"abc\"]}}}";
    static final String WILDCARD_TYPE = "{\"lower\":[[1,2,3]],\"map\":{\"v\":1},\"mapInWild\":[{\"v\":1}],\"mapUpper\":{\"v\":1},\"simple\":[[1,2,3]],\"upper\":[[1,2,3]]}";

    public static class A {
        @Key
        public Map<String, String> map;
    }

    public static class AnyType {
        @Key
        public Object arr;
        @Key
        public Object bool;
        @Key
        public Object nul;
        @Key
        public Object num;
        @Key
        public Object obj;
        @Key
        public Object str;
    }

    public static class ArrayType {
        @Key
        int[] arr;
        @Key
        int[][] arr2;
        @Key
        public Integer[] integerArr;
    }

    public static class CollectionOfCollectionType {
        @Key
        public LinkedList<LinkedList<String>> arr;
    }

    public static class DoubleListTypeVariableType extends TypeVariableType<List<Double>> {
    }

    public enum E {
        VALUE,
        OTHER_VALUE,
        NULL,
        IGNORED_VALUE
    }

    public static class Entry {
        @Key
        public String title;
    }

    public static class EnumValue {
        @Key
        public E nullValue;
        @Key
        public E otherValue;
        @Key
        public E value;
    }

    public static class Feed {
        @Key
        public Collection<Entry> entries;
    }

    public static class FloatMapTypeVariableType extends TypeVariableType<Map<String, Float>> {
    }

    public static class IntArrayTypeVariableType extends TypeVariableType<int[]> {
    }

    public static class IntegerTypeVariableType extends TypeVariableType<Integer> {
    }

    public static class MapOfMapType {
        @Key
        public Map<String, Map<String, Integer>>[] value;
    }

    public static class NumberTypes {
        @Key("yetAnotherBigDecimalValue")
        BigDecimal anotherBigDecimalValue;
        @Key
        BigDecimal bigDecimalValue;
        @Key
        BigInteger bigIntegerValue;
        @Key
        Byte byteObjValue;
        @Key
        byte byteValue;
        @Key
        Double doubleObjValue;
        @Key
        double doubleValue;
        @Key
        Float floatObjValue;
        @Key
        float floatValue;
        @Key
        Integer intObjValue;
        @Key
        int intValue;
        @Key
        Long longObjValue;
        @Key
        long longValue;
        @Key
        Short shortObjValue;
        @Key
        short shortValue;
    }

    public static class NumberTypesAsString {
        @JsonString
        @Key("yetAnotherBigDecimalValue")
        BigDecimal anotherBigDecimalValue;
        @JsonString
        @Key
        BigDecimal bigDecimalValue;
        @JsonString
        @Key
        BigInteger bigIntegerValue;
        @JsonString
        @Key
        Byte byteObjValue;
        @JsonString
        @Key
        byte byteValue;
        @JsonString
        @Key
        Double doubleObjValue;
        @JsonString
        @Key
        double doubleValue;
        @JsonString
        @Key
        Float floatObjValue;
        @JsonString
        @Key
        float floatValue;
        @JsonString
        @Key
        Integer intObjValue;
        @JsonString
        @Key
        int intValue;
        @JsonString
        @Key
        Long longObjValue;
        @JsonString
        @Key
        long longValue;
        @JsonString
        @Key
        Short shortObjValue;
        @JsonString
        @Key
        short shortValue;
    }

    public static class StringNullValue {
        @Key
        public String[] arr;
        @Key
        public String[][] arr2;
        @Key
        public String value;
    }

    public static class TypeVariableType<T> {
        @Key
        public T[][] arr;
        @Key
        public LinkedList<LinkedList<T>> list;
        @Key
        public T nullValue;
        @Key
        public T value;
    }

    public static class TypeVariablesPassedAround extends X<LinkedList<String>> {
    }

    public static class WildCardTypes {
        @Key
        public Collection<? super Integer>[] lower;
        @Key
        public Map<String, ?> map;
        @Key
        public Collection<? super TreeMap<String, ? extends Integer>> mapInWild;
        @Key
        public Map<String, ? extends Integer> mapUpper;
        @Key
        public Collection<?>[] simple;
        @Key
        public Collection<? extends Integer>[] upper;
    }

    public static class X<XT> {
        @Key
        Y<XT> y;
    }

    public static class Y<YT> {
        @Key
        Z<YT> z;
    }

    public static class Z<ZT> {
        @Key
        ZT f;
    }

    /* access modifiers changed from: protected */
    public abstract JsonFactory newFactory();

    public AbstractJsonParserTest(String name) {
        super(name);
    }

    public void testParse_emptyMap() throws IOException {
        JsonParser parser = newFactory().createJsonParser("{}");
        parser.nextToken();
        assertTrue(((HashMap) parser.parseAndClose(HashMap.class, (CustomizeJsonParser) null)).isEmpty());
    }

    public void testParse_emptyGenericJson() throws IOException {
        JsonParser parser = newFactory().createJsonParser("{}");
        parser.nextToken();
        assertTrue(((GenericJson) parser.parseAndClose(GenericJson.class, (CustomizeJsonParser) null)).isEmpty());
    }

    public void testParseEntry() throws Exception {
        JsonParser parser = newFactory().createJsonParser(JSON_ENTRY);
        parser.nextToken();
        assertEquals("foo", ((Entry) parser.parseAndClose(Entry.class, (CustomizeJsonParser) null)).title);
    }

    public void testParseFeed() throws Exception {
        JsonParser parser = newFactory().createJsonParser(JSON_FEED);
        parser.nextToken();
        Iterator<Entry> iterator = ((Feed) parser.parseAndClose(Feed.class, (CustomizeJsonParser) null)).entries.iterator();
        assertEquals("foo", iterator.next().title);
        assertEquals("bar", iterator.next().title);
        assertFalse(iterator.hasNext());
    }

    public void testParseEntryAsMap() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_ENTRY);
        parser.nextToken();
        HashMap<String, Object> map = (HashMap) parser.parseAndClose(HashMap.class, (CustomizeJsonParser) null);
        assertEquals("foo", map.remove(CloudPrintActivity.EXTRA_TITLE));
        assertTrue(map.isEmpty());
    }

    public void testSkipToKey_missingEmpty() throws IOException {
        JsonParser parser = newFactory().createJsonParser("{}");
        parser.nextToken();
        parser.skipToKey("missing");
        assertEquals(JsonToken.END_OBJECT, parser.getCurrentToken());
    }

    public void testSkipToKey_missing() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_ENTRY);
        parser.nextToken();
        parser.skipToKey("missing");
        assertEquals(JsonToken.END_OBJECT, parser.getCurrentToken());
    }

    public void testSkipToKey_found() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_ENTRY);
        parser.nextToken();
        parser.skipToKey(CloudPrintActivity.EXTRA_TITLE);
        assertEquals(JsonToken.VALUE_STRING, parser.getCurrentToken());
        assertEquals("foo", parser.getText());
    }

    public void testSkipToKey_startWithFieldName() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_ENTRY);
        parser.nextToken();
        parser.nextToken();
        parser.skipToKey(CloudPrintActivity.EXTRA_TITLE);
        assertEquals(JsonToken.VALUE_STRING, parser.getCurrentToken());
        assertEquals("foo", parser.getText());
    }

    public void testSkipChildren_string() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_ENTRY);
        parser.nextToken();
        parser.skipToKey(CloudPrintActivity.EXTRA_TITLE);
        parser.skipChildren();
        assertEquals(JsonToken.VALUE_STRING, parser.getCurrentToken());
        assertEquals("foo", parser.getText());
    }

    public void testSkipChildren_object() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_ENTRY);
        parser.nextToken();
        parser.skipChildren();
        assertEquals(JsonToken.END_OBJECT, parser.getCurrentToken());
    }

    public void testSkipChildren_array() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_FEED);
        parser.nextToken();
        parser.skipToKey("entries");
        parser.skipChildren();
        assertEquals(JsonToken.END_ARRAY, parser.getCurrentToken());
    }

    public void testNextToken() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_FEED);
        assertEquals(JsonToken.START_OBJECT, parser.nextToken());
        assertEquals(JsonToken.FIELD_NAME, parser.nextToken());
        assertEquals(JsonToken.START_ARRAY, parser.nextToken());
        assertEquals(JsonToken.START_OBJECT, parser.nextToken());
        assertEquals(JsonToken.FIELD_NAME, parser.nextToken());
        assertEquals(JsonToken.VALUE_STRING, parser.nextToken());
        assertEquals(JsonToken.END_OBJECT, parser.nextToken());
        assertEquals(JsonToken.START_OBJECT, parser.nextToken());
        assertEquals(JsonToken.FIELD_NAME, parser.nextToken());
        assertEquals(JsonToken.VALUE_STRING, parser.nextToken());
        assertEquals(JsonToken.END_OBJECT, parser.nextToken());
        assertEquals(JsonToken.END_ARRAY, parser.nextToken());
        assertEquals(JsonToken.END_OBJECT, parser.nextToken());
        assertNull(parser.nextToken());
    }

    public void testCurrentToken() throws IOException {
        JsonParser parser = newFactory().createJsonParser(JSON_FEED);
        assertNull(parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.START_OBJECT, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.FIELD_NAME, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.START_ARRAY, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.START_OBJECT, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.FIELD_NAME, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.VALUE_STRING, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.END_OBJECT, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.START_OBJECT, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.FIELD_NAME, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.VALUE_STRING, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.END_OBJECT, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.END_ARRAY, parser.getCurrentToken());
        parser.nextToken();
        assertEquals(JsonToken.END_OBJECT, parser.getCurrentToken());
        parser.nextToken();
        assertNull(parser.getCurrentToken());
    }

    public void testParse() throws IOException {
        JsonParser parser = newFactory().createJsonParser(CONTAINED_MAP);
        parser.nextToken();
        assertEquals(ImmutableMap.of(CloudPrintActivity.EXTRA_TITLE, "foo"), ((A) parser.parse(A.class, (CustomizeJsonParser) null)).map);
    }

    public void testParser_numberTypes() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(NUMBER_TYPES);
        parser.nextToken();
        assertEquals(NUMBER_TYPES, factory.toString((NumberTypes) parser.parse(NumberTypes.class, (CustomizeJsonParser) null)));
        JsonParser parser2 = factory.createJsonParser(NUMBER_TYPES_AS_STRING);
        parser2.nextToken();
        assertEquals(NUMBER_TYPES_AS_STRING, factory.toString((NumberTypesAsString) parser2.parse(NumberTypesAsString.class, (CustomizeJsonParser) null)));
        try {
            JsonParser parser3 = factory.createJsonParser(NUMBER_TYPES_AS_STRING);
            parser3.nextToken();
            parser3.parse(NumberTypes.class, (CustomizeJsonParser) null);
            fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
        }
        try {
            JsonParser parser4 = factory.createJsonParser(NUMBER_TYPES);
            parser4.nextToken();
            parser4.parse(NumberTypesAsString.class, (CustomizeJsonParser) null);
            fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException e2) {
        }
    }

    public void testToFromString() throws IOException {
        JsonFactory factory = newFactory();
        assertEquals(NUMBER_TYPES, factory.toString((NumberTypes) factory.fromString(NUMBER_TYPES, NumberTypes.class)));
    }

    public void testParser_anyType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(ANY_TYPE);
        parser.nextToken();
        assertEquals(ANY_TYPE, factory.toString((AnyType) parser.parse(AnyType.class, (CustomizeJsonParser) null)));
    }

    public void testParser_arrayType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(ARRAY_TYPE);
        parser.nextToken();
        ArrayType result = (ArrayType) parser.parse(ArrayType.class, (CustomizeJsonParser) null);
        assertEquals(ARRAY_TYPE, factory.toString(result));
        assertTrue(Arrays.equals(new int[]{4, 5}, result.arr));
        int[][] arr2 = result.arr2;
        assertEquals(2, arr2.length);
        int[] arr20 = arr2[0];
        assertEquals(2, arr20.length);
        assertEquals(1, arr20[0]);
        assertEquals(2, arr20[1]);
        int[] arr21 = arr2[1];
        assertEquals(1, arr21.length);
        assertEquals(3, arr21[0]);
        Integer[] integerArr = result.integerArr;
        assertEquals(2, integerArr.length);
        assertEquals(6, integerArr[0].intValue());
    }

    public void testParser_collectionType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(COLLECTION_TYPE);
        parser.nextToken();
        CollectionOfCollectionType result = (CollectionOfCollectionType) parser.parse(CollectionOfCollectionType.class, (CustomizeJsonParser) null);
        assertEquals(COLLECTION_TYPE, factory.toString(result));
        assertEquals("a", (String) result.arr.get(0).get(0));
    }

    public void testParser_mapType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(MAP_TYPE);
        parser.nextToken();
        MapOfMapType result = (MapOfMapType) parser.parse(MapOfMapType.class, (CustomizeJsonParser) null);
        assertEquals(MAP_TYPE, factory.toString(result));
        Map<String, Map<String, Integer>> firstMap = result.value[0];
        assertEquals(1, ((Integer) firstMap.get("map1").get("k1")).intValue());
        Map<String, Integer> map2 = firstMap.get("map2");
        assertEquals(3, ((Integer) map2.get("kk1")).intValue());
        assertEquals(4, ((Integer) map2.get("kk2")).intValue());
    }

    public void testParser_hashmapForMapType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(MAP_TYPE);
        parser.nextToken();
        HashMap<String, ArrayList<ArrayMap<String, ArrayMap<String, BigDecimal>>>> result = (HashMap) parser.parse(HashMap.class, (CustomizeJsonParser) null);
        assertEquals(MAP_TYPE, factory.toString(result));
        assertEquals(1, ((BigDecimal) result.get("value").get(0).get("map1").get("k1")).intValue());
    }

    public void testParser_wildCardType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(WILDCARD_TYPE);
        parser.nextToken();
        WildCardTypes result = (WildCardTypes) parser.parse(WildCardTypes.class, (CustomizeJsonParser) null);
        assertEquals(WILDCARD_TYPE, factory.toString(result));
        assertEquals(1, ((BigDecimal) ((ArrayList) ((Collection[]) result.simple)[0]).get(0)).intValue());
        assertEquals(1, ((Integer) ((ArrayList) ((Collection[]) result.upper)[0]).get(0)).intValue());
        assertEquals(1, ((Integer) ((ArrayList) ((Collection[]) result.lower)[0]).get(0)).intValue());
        assertEquals(1, ((BigDecimal) result.map.get("v")).intValue());
        assertEquals(1, ((Integer) result.mapUpper.get("v")).intValue());
        assertEquals(1, ((Integer) ((TreeMap) ((ArrayList) result.mapInWild).get(0)).get("v")).intValue());
    }

    public void testParser_integerTypeVariableType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(INTEGER_TYPE_VARIABLE_TYPE);
        parser.nextToken();
        IntegerTypeVariableType result = (IntegerTypeVariableType) parser.parse(IntegerTypeVariableType.class, (CustomizeJsonParser) null);
        assertEquals(INTEGER_TYPE_VARIABLE_TYPE, factory.toString(result));
        Integer[][] arr = (Integer[][]) result.arr;
        assertEquals(2, arr.length);
        assertEquals(Data.nullOf(Integer[].class), arr[0]);
        Integer[] subArr = arr[1];
        assertEquals(2, subArr.length);
        assertEquals(Data.NULL_INTEGER, subArr[0]);
        assertEquals(1, subArr[1].intValue());
        LinkedList<LinkedList<Integer>> list = result.list;
        assertEquals(2, list.size());
        assertEquals(Data.nullOf(LinkedList.class), list.get(0));
        LinkedList<Integer> subList = list.get(1);
        assertEquals(2, subList.size());
        assertEquals(Data.NULL_INTEGER, subList.get(0));
        assertEquals(1, subList.get(1).intValue());
        assertEquals(Data.NULL_INTEGER, (Integer) result.nullValue);
        assertEquals(1, ((Integer) result.value).intValue());
    }

    public void testParser_intArrayTypeVariableType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(INT_ARRAY_TYPE_VARIABLE_TYPE);
        parser.nextToken();
        IntArrayTypeVariableType result = (IntArrayTypeVariableType) parser.parse(IntArrayTypeVariableType.class, (CustomizeJsonParser) null);
        assertEquals(INT_ARRAY_TYPE_VARIABLE_TYPE, factory.toString(result));
        int[][][] arr = (int[][][]) result.arr;
        assertEquals(2, arr.length);
        assertEquals(Data.nullOf(int[][].class), arr[0]);
        int[][] subArr = arr[1];
        assertEquals(2, subArr.length);
        assertEquals(Data.nullOf(int[].class), subArr[0]);
        int[] iArr = {1};
        assertTrue(Arrays.equals(iArr, subArr[1]));
        LinkedList<LinkedList<int[]>> list = result.list;
        assertEquals(2, list.size());
        assertEquals(Data.nullOf(LinkedList.class), list.get(0));
        LinkedList<int[]> subList = list.get(1);
        assertEquals(2, subList.size());
        assertEquals(Data.nullOf(int[].class), subList.get(0));
        assertTrue(Arrays.equals(new int[]{1}, subList.get(1)));
        assertEquals(Data.nullOf(int[].class), (int[]) result.nullValue);
        assertTrue(Arrays.equals(new int[]{1}, (int[]) result.value));
    }

    public void testParser_doubleListTypeVariableType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(DOUBLE_LIST_TYPE_VARIABLE_TYPE);
        parser.nextToken();
        DoubleListTypeVariableType result = (DoubleListTypeVariableType) parser.parse(DoubleListTypeVariableType.class, (CustomizeJsonParser) null);
        assertEquals(DOUBLE_LIST_TYPE_VARIABLE_TYPE, factory.toString(result));
        List<Double>[][] arr = (List[][]) result.arr;
        assertEquals(2, arr.length);
        assertEquals(Data.nullOf(List[].class), arr[0]);
        List<Double>[] subArr = arr[1];
        assertEquals(2, subArr.length);
        assertEquals(Data.nullOf(ArrayList.class), subArr[0]);
        List<Double> arrValue = subArr[1];
        assertEquals(1, arrValue.size());
        assertEquals(Double.valueOf(1.0d), Double.valueOf(((Double) arrValue.get(0)).doubleValue()));
        LinkedList<LinkedList<List<Double>>> list = result.list;
        assertEquals(2, list.size());
        assertEquals(Data.nullOf(LinkedList.class), list.get(0));
        LinkedList<List<Double>> subList = list.get(1);
        assertEquals(2, subList.size());
        assertEquals(Data.nullOf(ArrayList.class), subList.get(0));
        assertEquals(ImmutableList.of(new Double(1.0d)), subList.get(1));
        assertEquals(Data.nullOf(ArrayList.class), (List) result.nullValue);
        assertEquals(ImmutableList.of(new Double(1.0d)), (List) result.value);
    }

    public void testParser_floatMapTypeVariableType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(FLOAT_MAP_TYPE_VARIABLE_TYPE);
        parser.nextToken();
        FloatMapTypeVariableType result = (FloatMapTypeVariableType) parser.parse(FloatMapTypeVariableType.class, (CustomizeJsonParser) null);
        assertEquals(FLOAT_MAP_TYPE_VARIABLE_TYPE, factory.toString(result));
        Map<String, Float>[][] arr = (Map[][]) result.arr;
        assertEquals(2, arr.length);
        assertEquals(Data.nullOf(Map[].class), arr[0]);
        Map<String, Float>[] subArr = arr[1];
        assertEquals(2, subArr.length);
        assertEquals(Data.nullOf(HashMap.class), subArr[0]);
        Map<String, Float> arrValue = subArr[1];
        assertEquals(1, arrValue.size());
        assertEquals(Float.valueOf(1.0f), Float.valueOf(((Float) arrValue.get("a")).floatValue()));
        LinkedList<LinkedList<Map<String, Float>>> list = result.list;
        assertEquals(2, list.size());
        assertEquals(Data.nullOf(LinkedList.class), list.get(0));
        LinkedList<Map<String, Float>> subList = list.get(1);
        assertEquals(2, subList.size());
        assertEquals(Data.nullOf(HashMap.class), subList.get(0));
        Map<String, Float> arrValue2 = subList.get(1);
        assertEquals(1, arrValue2.size());
        assertEquals(Float.valueOf(1.0f), Float.valueOf(arrValue2.get("a").floatValue()));
        assertEquals(Data.nullOf(HashMap.class), (Map) result.nullValue);
        Map<String, Float> value = (Map) result.value;
        assertEquals(1, value.size());
        assertEquals(Float.valueOf(1.0f), Float.valueOf(value.get("a").floatValue()));
    }

    public void testParser_treemapForTypeVariableType() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(INTEGER_TYPE_VARIABLE_TYPE);
        parser.nextToken();
        TreeMap<String, Object> result = (TreeMap) parser.parse(TreeMap.class, (CustomizeJsonParser) null);
        assertEquals(INTEGER_TYPE_VARIABLE_TYPE, factory.toString(result));
        ArrayList<Object> arr = (ArrayList) result.get("arr");
        assertEquals(2, arr.size());
        assertEquals(Data.nullOf(Object.class), arr.get(0));
        ArrayList<BigDecimal> subArr = (ArrayList) arr.get(1);
        assertEquals(2, subArr.size());
        assertEquals(Data.nullOf(Object.class), subArr.get(0));
        assertEquals(1, ((BigDecimal) subArr.get(1)).intValue());
        assertEquals(Data.nullOf(Object.class), result.get("nullValue"));
        assertEquals(1, ((BigDecimal) result.get("value")).intValue());
    }

    public void testParser_nullValue() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(NULL_VALUE);
        parser.nextToken();
        StringNullValue result = (StringNullValue) parser.parse(StringNullValue.class, (CustomizeJsonParser) null);
        assertEquals(NULL_VALUE, factory.toString(result));
        assertEquals(Data.NULL_STRING, result.value);
        String[] arr = result.arr;
        assertEquals(1, arr.length);
        assertEquals(Data.nullOf(String.class), arr[0]);
        String[][] arr2 = result.arr2;
        assertEquals(2, arr2.length);
        assertEquals(Data.nullOf(String[].class), arr2[0]);
        String[] subArr2 = arr2[1];
        assertEquals(1, subArr2.length);
        assertEquals(Data.NULL_STRING, subArr2[0]);
    }

    public void testParser_enumValue() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(ENUM_VALUE);
        parser.nextToken();
        EnumValue result = (EnumValue) parser.parse(EnumValue.class, (CustomizeJsonParser) null);
        assertEquals(ENUM_VALUE, factory.toString(result));
        assertEquals(E.VALUE, result.value);
        assertEquals(E.OTHER_VALUE, result.otherValue);
        assertEquals(E.NULL, result.nullValue);
    }

    public void testParser_typeVariablesPassAround() throws IOException {
        JsonFactory factory = newFactory();
        JsonParser parser = factory.createJsonParser(TYPE_VARS);
        parser.nextToken();
        TypeVariablesPassedAround result = (TypeVariablesPassedAround) parser.parse(TypeVariablesPassedAround.class, (CustomizeJsonParser) null);
        assertEquals(TYPE_VARS, factory.toString(result));
        assertEquals("abc", (String) result.y.z.f.get(0));
    }
}
