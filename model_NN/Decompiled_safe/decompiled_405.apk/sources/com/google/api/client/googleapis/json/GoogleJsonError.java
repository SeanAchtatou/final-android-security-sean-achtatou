package com.google.api.client.googleapis.json;

import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.Key;
import java.io.IOException;
import java.util.List;

public class GoogleJsonError extends GenericJson {
    @Key
    public int code;
    @Key
    public List<ErrorInfo> errors;
    @Key
    public String message;

    public static class ErrorInfo extends GenericJson {
        @Key
        public String domain;
        @Key
        public String location;
        @Key
        public String locationType;
        @Key
        public String message;
        @Key
        public String reason;
    }

    public static GoogleJsonError parse(JsonFactory jsonFactory, HttpResponse response) throws IOException {
        JsonCParser parser = new JsonCParser();
        parser.jsonFactory = jsonFactory;
        return (GoogleJsonError) parser.parse(response, GoogleJsonError.class);
    }
}
