package com.google.api.client.http.xml.atom;

import com.google.api.client.http.xml.XmlHttpParser;
import com.google.api.client.xml.atom.Atom;

public final class AtomParser extends XmlHttpParser {
    public AtomParser() {
        this.contentType = Atom.CONTENT_TYPE;
    }
}
