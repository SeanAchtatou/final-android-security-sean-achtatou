package com.google.api.client.http;

import com.google.common.base.Preconditions;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public final class FileContent extends AbstractInputStreamContent {
    private final File file;

    public FileContent(File file2) {
        Preconditions.checkNotNull(file2);
        this.file = file2;
    }

    public long getLength() {
        return this.file.length();
    }

    public boolean retrySupported() {
        return true;
    }

    /* access modifiers changed from: protected */
    public InputStream getInputStream() throws FileNotFoundException {
        return new FileInputStream(this.file);
    }
}
