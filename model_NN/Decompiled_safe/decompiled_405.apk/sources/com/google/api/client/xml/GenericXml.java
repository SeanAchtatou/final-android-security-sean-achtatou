package com.google.api.client.xml;

import com.google.api.client.util.GenericData;

public class GenericXml extends GenericData implements Cloneable {
    public String name;
    public XmlNamespaceDictionary namespaceDictionary;

    public GenericXml clone() {
        return (GenericXml) super.clone();
    }

    public String toString() {
        XmlNamespaceDictionary namespaceDictionary2 = this.namespaceDictionary;
        if (namespaceDictionary2 == null) {
            namespaceDictionary2 = new XmlNamespaceDictionary();
        }
        return namespaceDictionary2.toStringOf(this.name, this);
    }
}
