package com.google.api.client.http.javanet;

import com.google.api.client.http.HttpContent;
import com.google.api.client.http.LowLevelHttpRequest;
import com.google.api.client.http.LowLevelHttpResponse;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

final class NetHttpRequest extends LowLevelHttpRequest {
    private final HttpURLConnection connection;
    private HttpContent content;

    NetHttpRequest(String requestMethod, String url) throws IOException {
        HttpURLConnection connection2 = (HttpURLConnection) new URL(url).openConnection();
        this.connection = connection2;
        connection2.setRequestMethod(requestMethod);
        connection2.setUseCaches(false);
        connection2.setInstanceFollowRedirects(false);
    }

    public void addHeader(String name, String value) {
        this.connection.addRequestProperty(name, value);
    }

    public void setTimeout(int connectTimeout, int readTimeout) {
        this.connection.setReadTimeout(readTimeout);
        this.connection.setConnectTimeout(connectTimeout);
    }

    public LowLevelHttpResponse execute() throws IOException {
        HttpURLConnection connection2 = this.connection;
        if (this.content != null) {
            String contentType = this.content.getType();
            if (contentType != null) {
                addHeader("Content-Type", contentType);
            }
            String contentEncoding = this.content.getEncoding();
            if (contentEncoding != null) {
                addHeader("Content-Encoding", contentEncoding);
            }
            long contentLength = this.content.getLength();
            if (contentLength >= 0) {
                addHeader("Content-Length", Long.toString(contentLength));
            }
            if (contentLength != 0) {
                connection2.setDoOutput(true);
                if (contentLength < 0 || contentLength > 2147483647L) {
                    connection2.setChunkedStreamingMode(0);
                } else {
                    connection2.setFixedLengthStreamingMode((int) contentLength);
                }
                this.content.writeTo(connection2.getOutputStream());
            }
        }
        connection2.connect();
        return new NetHttpResponse(connection2);
    }

    public void setContent(HttpContent content2) {
        this.content = content2;
    }
}
