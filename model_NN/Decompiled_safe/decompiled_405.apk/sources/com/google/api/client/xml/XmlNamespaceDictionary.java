package com.google.api.client.xml;

import com.google.api.client.util.Data;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.Types;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import org.xmlpull.v1.XmlSerializer;

public final class XmlNamespaceDictionary {
    private final HashMap<String, String> namespaceAliasToUriMap = new HashMap<>();
    private final HashMap<String, String> namespaceUriToAliasMap = new HashMap<>();

    public synchronized String getAliasForUri(String uri) {
        return this.namespaceUriToAliasMap.get(Preconditions.checkNotNull(uri));
    }

    public synchronized String getUriForAlias(String alias) {
        return this.namespaceAliasToUriMap.get(Preconditions.checkNotNull(alias));
    }

    public synchronized Map<String, String> getAliasToUriMap() {
        return Collections.unmodifiableMap(this.namespaceAliasToUriMap);
    }

    public synchronized Map<String, String> getUriToAliasMap() {
        return Collections.unmodifiableMap(this.namespaceUriToAliasMap);
    }

    public synchronized XmlNamespaceDictionary set(String alias, String uri) {
        String previousUri = null;
        String previousAlias = null;
        if (uri == null) {
            if (alias != null) {
                previousUri = this.namespaceAliasToUriMap.remove(alias);
            }
        } else if (alias == null) {
            previousAlias = this.namespaceUriToAliasMap.remove(uri);
        } else {
            previousUri = this.namespaceAliasToUriMap.put(Preconditions.checkNotNull(alias), Preconditions.checkNotNull(uri));
            if (!uri.equals(previousUri)) {
                previousAlias = this.namespaceUriToAliasMap.put(uri, alias);
            } else {
                previousUri = null;
            }
        }
        if (previousUri != null) {
            this.namespaceUriToAliasMap.remove(previousUri);
        }
        if (previousAlias != null) {
            this.namespaceAliasToUriMap.remove(previousAlias);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, boolean):void
     arg types: [org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, int]
     candidates:
      com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.String, java.lang.Object):void
      com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, boolean):void */
    public String toStringOf(String elementName, Object element) {
        try {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = Xml.createSerializer();
            serializer.setOutput(writer);
            serialize(serializer, elementName, element, false);
            return writer.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void serialize(XmlSerializer serializer, String elementNamespaceUri, String elementLocalName, Object element) throws IOException {
        serialize(serializer, elementNamespaceUri, elementLocalName, element, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, boolean):void
     arg types: [org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, int]
     candidates:
      com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.String, java.lang.Object):void
      com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, boolean):void */
    public void serialize(XmlSerializer serializer, String elementName, Object element) throws IOException {
        serialize(serializer, elementName, element, true);
    }

    private void serialize(XmlSerializer serializer, String elementNamespaceUri, String elementLocalName, Object element, boolean errorOnUnknown) throws IOException {
        startDoc(serializer, element, errorOnUnknown, elementNamespaceUri).serialize(serializer, elementNamespaceUri, elementLocalName);
        serializer.endDocument();
    }

    private void serialize(XmlSerializer serializer, String elementName, Object element, boolean errorOnUnknown) throws IOException {
        startDoc(serializer, element, errorOnUnknown, null).serialize(serializer, elementName);
        serializer.endDocument();
    }

    private ElementSerializer startDoc(XmlSerializer serializer, Object element, boolean errorOnUnknown, String extraNamespace) throws IOException {
        serializer.startDocument(null, null);
        SortedSet<String> aliases = new TreeSet<>();
        computeAliases(element, aliases);
        boolean foundExtra = extraNamespace == null;
        for (String alias : aliases) {
            String uri = getNamespaceUriForAliasHandlingUnknown(errorOnUnknown, alias);
            serializer.setPrefix(alias, uri);
            if (!foundExtra && uri.equals(extraNamespace)) {
                foundExtra = true;
            }
        }
        if (!foundExtra) {
            Iterator i$ = getAliasToUriMap().entrySet().iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Map.Entry<String, String> entry = i$.next();
                if (extraNamespace.equals(entry.getValue())) {
                    serializer.setPrefix((String) entry.getKey(), extraNamespace);
                    break;
                }
            }
        }
        return new ElementSerializer(element, errorOnUnknown);
    }

    private void computeAliases(Object element, SortedSet<String> aliases) {
        String alias;
        for (Map.Entry<String, Object> entry : Data.mapOf(element).entrySet()) {
            Object value = entry.getValue();
            if (value != null) {
                String name = (String) entry.getKey();
                if (!"text()".equals(name)) {
                    int colon = name.indexOf(58);
                    boolean isAttribute = name.charAt(0) == '@';
                    if (colon != -1 || !isAttribute) {
                        if (colon == -1) {
                            alias = "";
                        } else {
                            alias = name.substring(name.charAt(0) == '@' ? 1 : 0, colon);
                        }
                        aliases.add(alias);
                    }
                    Class<?> valueClass = value.getClass();
                    if (!isAttribute && !Data.isPrimitive(valueClass)) {
                        if ((value instanceof Iterable) || valueClass.isArray()) {
                            for (Object subValue : Types.iterableOf(value)) {
                                computeAliases(subValue, aliases);
                            }
                        } else {
                            computeAliases(value, aliases);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String getNamespaceUriForAliasHandlingUnknown(boolean errorOnUnknown, String alias) {
        boolean z;
        String result = getUriForAlias(alias);
        if (result != null) {
            return result;
        }
        if (!errorOnUnknown) {
            z = true;
        } else {
            z = false;
        }
        Object[] objArr = new Object[1];
        objArr[0] = alias.length() == 0 ? "(default)" : alias;
        Preconditions.checkArgument(z, "unrecognized alias: %s", objArr);
        return "http://unknown/" + alias;
    }

    class ElementSerializer {
        final List<String> attributeNames = new ArrayList();
        final List<Object> attributeValues = new ArrayList();
        private final boolean errorOnUnknown;
        final List<String> subElementNames = new ArrayList();
        final List<Object> subElementValues = new ArrayList();
        Object textValue = null;

        ElementSerializer(Object elementValue, boolean errorOnUnknown2) {
            this.errorOnUnknown = errorOnUnknown2;
            if (!Data.isPrimitive(elementValue.getClass()) || Data.isNull(elementValue)) {
                for (Map.Entry<String, Object> entry : Data.mapOf(elementValue).entrySet()) {
                    Object fieldValue = entry.getValue();
                    if (fieldValue != null && !Data.isNull(fieldValue)) {
                        String fieldName = (String) entry.getKey();
                        if ("text()".equals(fieldName)) {
                            this.textValue = fieldValue;
                        } else if (fieldName.charAt(0) == '@') {
                            this.attributeNames.add(fieldName.substring(1));
                            this.attributeValues.add(fieldValue);
                        } else {
                            this.subElementNames.add(fieldName);
                            this.subElementValues.add(fieldValue);
                        }
                    }
                }
                return;
            }
            this.textValue = elementValue;
        }

        /* access modifiers changed from: package-private */
        public void serialize(XmlSerializer serializer, String elementName) throws IOException {
            String elementLocalName = null;
            String elementNamespaceUri = null;
            if (elementName != null) {
                int colon = elementName.indexOf(58);
                elementLocalName = elementName.substring(colon + 1);
                elementNamespaceUri = XmlNamespaceDictionary.this.getNamespaceUriForAliasHandlingUnknown(this.errorOnUnknown, colon == -1 ? "" : elementName.substring(0, colon));
            }
            serialize(serializer, elementNamespaceUri, elementLocalName);
        }

        /* access modifiers changed from: package-private */
        public void serialize(XmlSerializer serializer, String elementNamespaceUri, String elementLocalName) throws IOException {
            boolean errorOnUnknown2 = this.errorOnUnknown;
            if (elementLocalName == null) {
                if (errorOnUnknown2) {
                    throw new IllegalArgumentException("XML name not specified");
                }
                elementLocalName = "unknownName";
            }
            serializer.startTag(elementNamespaceUri, elementLocalName);
            int num = this.attributeNames.size();
            for (int i = 0; i < num; i++) {
                String attributeName = this.attributeNames.get(i);
                int colon = attributeName.indexOf(58);
                serializer.attribute(colon == -1 ? null : XmlNamespaceDictionary.this.getNamespaceUriForAliasHandlingUnknown(errorOnUnknown2, attributeName.substring(0, colon)), attributeName.substring(colon + 1), XmlNamespaceDictionary.toSerializedValue(this.attributeValues.get(i)));
            }
            if (this.textValue != null) {
                serializer.text(XmlNamespaceDictionary.toSerializedValue(this.textValue));
            }
            int num2 = this.subElementNames.size();
            for (int i2 = 0; i2 < num2; i2++) {
                Object subElementValue = this.subElementValues.get(i2);
                String subElementName = this.subElementNames.get(i2);
                Class<?> cls = subElementValue.getClass();
                if ((subElementValue instanceof Iterable) || cls.isArray()) {
                    for (Object subElement : Types.iterableOf(subElementValue)) {
                        if (subElement != null && !Data.isNull(subElement)) {
                            new ElementSerializer(subElement, errorOnUnknown2).serialize(serializer, subElementName);
                        }
                    }
                } else {
                    new ElementSerializer(subElementValue, errorOnUnknown2).serialize(serializer, subElementName);
                }
            }
            serializer.endTag(elementNamespaceUri, elementLocalName);
        }
    }

    static String toSerializedValue(Object value) {
        if (value instanceof Float) {
            Float f = (Float) value;
            if (f.floatValue() == Float.POSITIVE_INFINITY) {
                return "INF";
            }
            if (f.floatValue() == Float.NEGATIVE_INFINITY) {
                return "-INF";
            }
        }
        if (value instanceof Double) {
            Double d = (Double) value;
            if (d.doubleValue() == Double.POSITIVE_INFINITY) {
                return "INF";
            }
            if (d.doubleValue() == Double.NEGATIVE_INFINITY) {
                return "-INF";
            }
        }
        if ((value instanceof String) || (value instanceof Number) || (value instanceof Boolean)) {
            return value.toString();
        }
        if (value instanceof DateTime) {
            return ((DateTime) value).toStringRfc3339();
        }
        if (value instanceof Enum) {
            return FieldInfo.of((Enum) value).getName();
        }
        throw new IllegalArgumentException("unrecognized value type: " + value.getClass());
    }
}
