package com.google.api.client.googleapis.extensions.android2.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

public final class GoogleAccountManager {
    public static final String ACCOUNT_TYPE = "com.google";
    public final AccountManager manager;

    public GoogleAccountManager(AccountManager manager2) {
        this.manager = manager2;
    }

    public GoogleAccountManager(Context context) {
        this(AccountManager.get(context));
    }

    public Account[] getAccounts() {
        return this.manager.getAccountsByType(ACCOUNT_TYPE);
    }

    public Account getAccountByName(String accountName) {
        if (accountName != null) {
            for (Account account : getAccounts()) {
                if (accountName.equals(account.name)) {
                    return account;
                }
            }
        }
        return null;
    }

    public void invalidateAuthToken(String authToken) {
        this.manager.invalidateAuthToken(ACCOUNT_TYPE, authToken);
    }
}
