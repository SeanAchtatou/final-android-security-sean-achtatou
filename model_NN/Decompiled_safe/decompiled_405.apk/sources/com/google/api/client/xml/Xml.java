package com.google.api.client.xml;

import com.google.api.client.util.ArrayValueMap;
import com.google.api.client.util.ClassInfo;
import com.google.api.client.util.Data;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.Types;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

public class Xml {
    static final String TEXT_CONTENT = "text()";
    private static XmlPullParserFactory factory;

    private static synchronized XmlPullParserFactory getParserFactory() throws XmlPullParserException {
        XmlPullParserFactory xmlPullParserFactory;
        synchronized (Xml.class) {
            if (factory == null) {
                factory = XmlPullParserFactory.newInstance(System.getProperty("org.xmlpull.v1.XmlPullParserFactory"), null);
                factory.setNamespaceAware(true);
            }
            xmlPullParserFactory = factory;
        }
        return xmlPullParserFactory;
    }

    public static XmlSerializer createSerializer() {
        try {
            return getParserFactory().newSerializer();
        } catch (XmlPullParserException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static XmlPullParser createParser() throws XmlPullParserException {
        return getParserFactory().newPullParser();
    }

    public static String toStringOf(Object element) {
        return new XmlNamespaceDictionary().toStringOf(null, element);
    }

    private static void parseAttributeOrTextContent(String stringValue, Field field, Type valueType, List<Type> context, Object destination, GenericXml genericXml, Map<String, Object> destinationMap, String name) {
        if (field != null || genericXml != null || destinationMap != null) {
            if (field != null) {
                valueType = field.getGenericType();
            }
            setValue(parseValue(valueType, context, stringValue), field, destination, genericXml, destinationMap, name);
        }
    }

    private static void setValue(Object value, Field field, Object destination, GenericXml genericXml, Map<String, Object> destinationMap, String name) {
        if (field != null) {
            FieldInfo.setFieldValue(field, destination, value);
        } else if (genericXml != null) {
            genericXml.set(name, value);
        } else {
            destinationMap.put(name, value);
        }
    }

    public static class CustomizeParser {
        public boolean stopBeforeStartTag(String namespace, String localName) {
            return false;
        }

        public boolean stopAfterEndTag(String namespace, String localName) {
            return false;
        }
    }

    public static void parseElement(XmlPullParser parser, Object destination, XmlNamespaceDictionary namespaceDictionary, CustomizeParser customizeParser) throws IOException, XmlPullParserException {
        ArrayList<Type> context = new ArrayList<>();
        context.add(destination.getClass());
        parseElementInternal(parser, context, destination, null, namespaceDictionary, customizeParser);
    }

    /* JADX INFO: Multiple debug info for r4v5 int: [D('eventType' int), D('depth' int)] */
    /* JADX INFO: Multiple debug info for r4v6 int: [D('i' int), D('depth' int)] */
    /* JADX INFO: Multiple debug info for r4v9 int: [D('event' int), D('isStopped' boolean)] */
    /* JADX INFO: Multiple debug info for r4v29 com.google.api.client.xml.GenericXml: [D('atom' com.google.api.client.xml.GenericXml), D('contextSize' int)] */
    /* JADX INFO: Multiple debug info for r5v11 java.lang.Object: [D('field' java.lang.reflect.Field), D('list' java.util.Collection<java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r4v30 java.util.Collection<java.lang.Object>: [D('atom' com.google.api.client.xml.GenericXml), D('list' java.util.Collection<java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r4v31 com.google.api.client.util.FieldInfo: [D('contextSize' int), D('fieldInfo' com.google.api.client.util.FieldInfo)] */
    /* JADX INFO: Multiple debug info for r4v32 java.util.Collection<java.lang.Object>: [D('fieldInfo' com.google.api.client.util.FieldInfo), D('list' java.util.Collection<java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r4v35 java.lang.Object: [D('contextSize' int), D('list' java.util.Collection<java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r6v7 com.google.api.client.util.FieldInfo: [D('fieldClass' java.lang.Class<?>), D('fieldInfo' com.google.api.client.util.FieldInfo)] */
    /* JADX INFO: Multiple debug info for r18v1 java.lang.Object: [D('elementValue' java.util.Map), D('elementValue' java.lang.Object)] */
    /* JADX INFO: Multiple debug info for r12v10 int: [D('subFieldClass' java.lang.Class<?>), D('isStopped' boolean)] */
    /* JADX INFO: Multiple debug info for r6v10 java.util.Collection<java.lang.Object>: [D('collectionValue' java.util.Collection<java.lang.Object>), D('fieldInfo' com.google.api.client.util.FieldInfo)] */
    /* JADX INFO: Multiple debug info for r6v14 boolean: [D('fieldClass' java.lang.Class<?>), D('isStopped' boolean)] */
    /* JADX INFO: Multiple debug info for r5v28 java.lang.reflect.Field: [D('attributeName' java.lang.String), D('field' java.lang.reflect.Field)] */
    /* JADX INFO: Multiple debug info for r5v29 java.lang.String: [D('i' int), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v89 java.lang.String: [D('namespace' java.lang.String), D('alias' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v97 int: [D('alias' java.lang.String), D('i' int)] */
    private static boolean parseElementInternal(XmlPullParser parser, ArrayList<Type> context, Object destination, Type valueType, XmlNamespaceDictionary namespaceDictionary, CustomizeParser customizeParser) throws IOException, XmlPullParserException {
        ClassInfo classInfo;
        boolean isStopped;
        Class<?> fieldClass;
        int level;
        Type subValueType;
        Class<?> subFieldClass;
        Object elementValue;
        Type subValueType2;
        Object value;
        Field field;
        Field field2;
        String originalAlias;
        GenericXml genericXml = destination instanceof GenericXml ? (GenericXml) destination : null;
        Map<String, Object> destinationMap = (genericXml != null || !(destination instanceof Map)) ? null : Map.class.cast(destination);
        if (destinationMap != null || destination == null) {
            classInfo = null;
        } else {
            classInfo = ClassInfo.of(destination.getClass());
        }
        int eventType = parser.getEventType();
        if (parser.getEventType() == 0) {
            eventType = parser.next();
        }
        if (eventType != 2) {
            throw new IllegalArgumentException("expected start of XML element, but got something else (event type " + eventType + ")");
        }
        int depth = parser.getDepth();
        int nsStart = parser.getNamespaceCount(depth - 1);
        int nsEnd = parser.getNamespaceCount(depth);
        for (int i = nsStart; i < nsEnd; i++) {
            String namespace = parser.getNamespaceUri(i);
            if (namespaceDictionary.getAliasForUri(namespace) == null) {
                String prefix = parser.getNamespacePrefix(i);
                if (prefix == null) {
                    originalAlias = "";
                } else {
                    originalAlias = prefix;
                }
                String alias = originalAlias;
                int suffix = 1;
                while (namespaceDictionary.getUriForAlias(alias) != null) {
                    suffix++;
                    alias = originalAlias + suffix;
                }
                namespaceDictionary.set(alias, namespace);
            }
        }
        if (genericXml != null) {
            genericXml.namespaceDictionary = namespaceDictionary;
            String name = parser.getName();
            String alias2 = namespaceDictionary.getAliasForUri(parser.getNamespace());
            genericXml.name = alias2.length() == 0 ? name : alias2 + ":" + name;
        }
        if (destination != null) {
            int attributeCount = parser.getAttributeCount();
            for (int i2 = 0; i2 < attributeCount; i2++) {
                String attributeName = parser.getAttributeName(i2);
                String attributeNamespace = parser.getAttributeNamespace(i2);
                String fieldName = getFieldName(true, attributeNamespace.length() == 0 ? "" : namespaceDictionary.getAliasForUri(attributeNamespace), attributeNamespace, attributeName);
                if (classInfo == null) {
                    field2 = null;
                } else {
                    field2 = classInfo.getField(fieldName);
                }
                parseAttributeOrTextContent(parser.getAttributeValue(i2), field2, valueType, context, destination, genericXml, destinationMap, fieldName);
            }
        }
        ArrayValueMap arrayValueMap = new ArrayValueMap(destination);
        int event = 0;
        while (true) {
            int isStopped2 = event;
            switch (parser.next()) {
                case 1:
                    isStopped = true;
                    break;
                case 2:
                    if (customizeParser != null) {
                        if (customizeParser.stopBeforeStartTag(parser.getNamespace(), parser.getName())) {
                            isStopped = true;
                            break;
                        }
                    }
                    if (destination == null) {
                        parseTextContentForElement(parser, context, true, null);
                        event = isStopped2;
                    } else {
                        String namespace2 = parser.getNamespace();
                        String fieldName2 = getFieldName(false, namespaceDictionary.getAliasForUri(namespace2), namespace2, parser.getName());
                        Field field3 = classInfo == null ? null : classInfo.getField(fieldName2);
                        Type fieldType = Data.resolveWildcardTypeOrTypeVariable(context, field3 == null ? valueType : field3.getGenericType());
                        Class<?> fieldClass2 = fieldType instanceof Class ? (Class) fieldType : null;
                        if (fieldType instanceof ParameterizedType) {
                            fieldClass = Types.getRawClass((ParameterizedType) fieldType);
                        } else {
                            fieldClass = fieldClass2;
                        }
                        boolean isArray = Types.isArray(fieldType);
                        boolean ignore = field3 == null && destinationMap == null && genericXml == null;
                        if (ignore || Data.isPrimitive(fieldType)) {
                            int level2 = 1;
                            while (level2 != 0) {
                                switch (parser.next()) {
                                    case 1:
                                        isStopped = true;
                                        break;
                                    case 2:
                                        level = level2 + 1;
                                        break;
                                    case 3:
                                        level = level2 - 1;
                                        break;
                                    case 4:
                                        if (!ignore && level2 == 1) {
                                            parseAttributeOrTextContent(parser.getText(), field3, valueType, context, destination, genericXml, destinationMap, fieldName2);
                                        }
                                    default:
                                        level = level2;
                                        break;
                                }
                                level2 = level;
                            }
                            event = isStopped2;
                        } else if (fieldType == null || (fieldClass != null && Types.isAssignableToOrFrom(fieldClass, Map.class))) {
                            Map<String, Object> mapValue = Data.newMapInstance(fieldClass);
                            int contextSize = context.size();
                            if (fieldType != null) {
                                context.add(fieldType);
                            }
                            if (fieldType == null || !Map.class.isAssignableFrom(fieldClass)) {
                                subValueType = null;
                            } else {
                                subValueType = Types.getMapValueParameter(fieldType);
                            }
                            int isStopped3 = parseElementInternal(parser, context, mapValue, Data.resolveWildcardTypeOrTypeVariable(context, subValueType), namespaceDictionary, customizeParser);
                            if (fieldType != null) {
                                context.remove(contextSize);
                            }
                            if (destinationMap != null) {
                                Collection<Object> list = (Collection) destinationMap.get(fieldName2);
                                if (list == null) {
                                    list = new ArrayList<>(1);
                                    destinationMap.put(fieldName2, list);
                                }
                                list.add(mapValue);
                            } else if (field3 != null) {
                                FieldInfo fieldInfo = FieldInfo.of(field3);
                                if (fieldClass == Object.class) {
                                    Collection<Object> list2 = (Collection) fieldInfo.getValue(destination);
                                    if (list2 == null) {
                                        list2 = new ArrayList<>(1);
                                        fieldInfo.setValue(destination, list2);
                                    }
                                    list2.add(mapValue);
                                } else {
                                    fieldInfo.setValue(destination, mapValue);
                                }
                            } else {
                                GenericXml atom = (GenericXml) destination;
                                Collection<Object> list3 = (Collection) atom.get(fieldName2);
                                if (list3 == null) {
                                    list3 = new ArrayList<>(1);
                                    atom.set(fieldName2, list3);
                                }
                                list3.add(mapValue);
                            }
                            event = isStopped3;
                        } else if (isArray || Types.isAssignableToOrFrom(fieldClass, Collection.class)) {
                            FieldInfo fieldInfo2 = FieldInfo.of(field3);
                            Type subFieldType = isArray ? Types.getArrayComponentType(fieldType) : Types.getIterableParameter(fieldType);
                            Class<?> rawArrayComponentType = Types.getRawArrayComponentType(context, subFieldType);
                            Type subFieldType2 = Data.resolveWildcardTypeOrTypeVariable(context, subFieldType);
                            Class<?> subFieldClass2 = subFieldType2 instanceof Class ? (Class) subFieldType2 : null;
                            if (subFieldType2 instanceof ParameterizedType) {
                                subFieldClass = Types.getRawClass((ParameterizedType) subFieldType2);
                            } else {
                                subFieldClass = subFieldClass2;
                            }
                            if (Data.isPrimitive(subFieldType2)) {
                                elementValue = parseTextContentForElement(parser, context, false, subFieldType2);
                                event = isStopped2;
                            } else if (subFieldType2 == null || (subFieldClass != null && Types.isAssignableToOrFrom(subFieldClass, Map.class))) {
                                elementValue = Data.newMapInstance(subFieldClass);
                                int contextSize2 = context.size();
                                if (subFieldType2 != null) {
                                    context.add(subFieldType2);
                                }
                                if (subFieldType2 == null || !Map.class.isAssignableFrom(subFieldClass)) {
                                    subValueType2 = null;
                                } else {
                                    subValueType2 = Types.getMapValueParameter(subFieldType2);
                                }
                                int isStopped4 = parseElementInternal(parser, context, elementValue, Data.resolveWildcardTypeOrTypeVariable(context, subValueType2), namespaceDictionary, customizeParser);
                                if (subFieldType2 != null) {
                                    context.remove(contextSize2);
                                }
                                event = isStopped4;
                            } else {
                                elementValue = Types.newInstance(rawArrayComponentType);
                                int contextSize3 = context.size();
                                context.add(fieldType);
                                int isStopped5 = parseElementInternal(parser, context, elementValue, null, namespaceDictionary, customizeParser);
                                context.remove(contextSize3);
                                event = isStopped5;
                            }
                            if (!isArray) {
                                if (field3 == null) {
                                    value = destinationMap.get(fieldName2);
                                } else {
                                    value = fieldInfo2.getValue(destination);
                                }
                                Collection<Object> collectionValue = (Collection) value;
                                if (collectionValue == null) {
                                    collectionValue = Data.newCollectionInstance(fieldType);
                                    setValue(collectionValue, field3, destination, genericXml, destinationMap, fieldName2);
                                }
                                collectionValue.add(elementValue);
                            } else if (field3 == null) {
                                arrayValueMap.put(fieldName2, rawArrayComponentType, elementValue);
                            } else {
                                arrayValueMap.put(field3, rawArrayComponentType, elementValue);
                            }
                        } else {
                            Object value2 = Types.newInstance(fieldClass);
                            int contextSize4 = context.size();
                            context.add(fieldType);
                            int isStopped6 = parseElementInternal(parser, context, value2, null, namespaceDictionary, customizeParser);
                            context.remove(contextSize4);
                            setValue(value2, field3, destination, genericXml, destinationMap, fieldName2);
                            event = isStopped6;
                        }
                    }
                    if (event != 0 || parser.getEventType() == 1) {
                        isStopped = true;
                        break;
                    } else {
                        continue;
                    }
                    break;
                case 3:
                    if (customizeParser != null) {
                        if (customizeParser.stopAfterEndTag(parser.getNamespace(), parser.getName())) {
                            isStopped = true;
                            break;
                        }
                    }
                    isStopped = false;
                    break;
                case 4:
                    if (destination != null) {
                        if (classInfo == null) {
                            field = null;
                        } else {
                            field = classInfo.getField(TEXT_CONTENT);
                        }
                        parseAttributeOrTextContent(parser.getText(), field, valueType, context, destination, genericXml, destinationMap, TEXT_CONTENT);
                        event = isStopped2;
                        continue;
                    }
                    break;
            }
            event = isStopped2;
        }
        isStopped = true;
        arrayValueMap.setValues();
        return isStopped;
    }

    private static String getFieldName(boolean isAttribute, String alias, String namespace, String name) {
        if (!isAttribute && alias.length() == 0) {
            return name;
        }
        StringBuilder buf = new StringBuilder(alias.length() + 2 + name.length());
        if (isAttribute) {
            buf.append('@');
        }
        if (alias != "") {
            buf.append(alias).append(':');
        }
        return buf.append(name).toString();
    }

    private static Object parseTextContentForElement(XmlPullParser parser, List<Type> context, boolean ignoreTextContent, Type textContentType) throws XmlPullParserException, IOException {
        int level = 1;
        Object obj = null;
        while (level != 0) {
            switch (parser.next()) {
                case 1:
                    level = 0;
                    break;
                case 2:
                    level++;
                    break;
                case 3:
                    level--;
                    break;
                case 4:
                    if (!ignoreTextContent && level == 1) {
                        obj = parseValue(textContentType, context, parser.getText());
                        break;
                    }
            }
        }
        return obj;
    }

    private static Object parseValue(Type valueType, List<Type> context, String value) {
        Type valueType2 = Data.resolveWildcardTypeOrTypeVariable(context, valueType);
        if (valueType2 == Double.class || valueType2 == Double.TYPE) {
            if (value.equals("INF")) {
                return new Double(Double.POSITIVE_INFINITY);
            }
            if (value.equals("-INF")) {
                return new Double(Double.NEGATIVE_INFINITY);
            }
        }
        if (valueType2 == Float.class || valueType2 == Float.TYPE) {
            if (value.equals("INF")) {
                return Float.valueOf(Float.POSITIVE_INFINITY);
            }
            if (value.equals("-INF")) {
                return Float.valueOf(Float.NEGATIVE_INFINITY);
            }
        }
        return Data.parsePrimitiveValue(valueType2, value);
    }

    private Xml() {
    }
}
