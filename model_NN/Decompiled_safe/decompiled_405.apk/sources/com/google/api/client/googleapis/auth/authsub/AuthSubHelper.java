package com.google.api.client.googleapis.auth.authsub;

import com.google.api.client.googleapis.auth.AuthKeyValueParser;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.util.Key;
import java.io.IOException;
import java.security.PrivateKey;

@Deprecated
public final class AuthSubHelper {
    public HttpTransport authSubTransport;
    private PrivateKey privateKey;
    private String token;
    private HttpTransport transport;

    public static final class SessionTokenResponse {
        @Key("Token")
        public String sessionToken;
    }

    public static final class TokenInfoResponse {
        @Key("Scope")
        public String scope;
        @Key("Secure")
        public boolean secure;
        @Key("Target")
        public String target;
    }

    public void setPrivateKey(PrivateKey privateKey2) {
        if (privateKey2 != this.privateKey) {
            this.privateKey = privateKey2;
            updateAuthorizationHeaders();
        }
    }

    public void setTransport(HttpTransport transport2) {
        if (transport2 != this.transport) {
            this.transport = transport2;
            updateAuthorizationHeaders();
        }
    }

    public void setToken(String token2) {
        if (token2 != this.token) {
            this.token = token2;
            updateAuthorizationHeaders();
        }
    }

    public String exchangeForSessionToken() throws IOException {
        HttpRequest request = this.authSubTransport.createRequestFactory().buildGetRequest(new GenericUrl("https://www.google.com/accounts/AuthSubSessionToken"));
        request.addParser(AuthKeyValueParser.INSTANCE);
        String sessionToken = ((SessionTokenResponse) request.execute().parseAs(SessionTokenResponse.class)).sessionToken;
        setToken(sessionToken);
        return sessionToken;
    }

    public void revokeSessionToken() throws IOException {
        this.authSubTransport.createRequestFactory().buildGetRequest(new GenericUrl("https://www.google.com/accounts/AuthSubRevokeToken")).execute().ignore();
        setToken(null);
    }

    public TokenInfoResponse requestTokenInfo() throws IOException {
        HttpRequest request = this.authSubTransport.createRequestFactory().buildGetRequest(new GenericUrl("https://www.google.com/accounts/AuthSubTokenInfo"));
        request.addParser(AuthKeyValueParser.INSTANCE);
        return (TokenInfoResponse) request.execute().parseAs(TokenInfoResponse.class);
    }

    private void updateAuthorizationHeaders() {
        HttpTransport transport2 = this.transport;
        if (transport2 != null) {
            setAuthorizationHeaderOf(transport2);
        }
        HttpTransport authSubTransport2 = this.authSubTransport;
        if (authSubTransport2 != null) {
            setAuthorizationHeaderOf(authSubTransport2);
        }
    }

    private void setAuthorizationHeaderOf(HttpTransport transoprt) {
        this.transport.intercepters.add(new AuthSubIntercepter(this.token, this.privateKey));
    }
}
