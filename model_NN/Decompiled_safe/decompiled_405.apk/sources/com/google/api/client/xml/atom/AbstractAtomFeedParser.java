package com.google.api.client.xml.atom;

import com.google.api.client.util.Types;
import com.google.api.client.xml.Xml;
import com.google.api.client.xml.XmlNamespaceDictionary;
import com.google.api.client.xml.atom.Atom;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public abstract class AbstractAtomFeedParser<T> {
    public Class<T> feedClass;
    private boolean feedParsed;
    public InputStream inputStream;
    public XmlNamespaceDictionary namespaceDictionary;
    public XmlPullParser parser;

    /* access modifiers changed from: protected */
    public abstract Object parseEntryInternal() throws IOException, XmlPullParserException;

    public T parseFeed() throws IOException, XmlPullParserException {
        boolean close = true;
        try {
            this.feedParsed = true;
            T result = Types.newInstance(this.feedClass);
            Xml.parseElement(this.parser, result, this.namespaceDictionary, Atom.StopAtAtomEntry.INSTANCE);
            close = false;
            return result;
        } finally {
            if (close) {
                close();
            }
        }
    }

    public Object parseNextEntry() throws IOException, XmlPullParserException {
        XmlPullParser parser2 = this.parser;
        if (!this.feedParsed) {
            this.feedParsed = true;
            Xml.parseElement(parser2, null, this.namespaceDictionary, Atom.StopAtAtomEntry.INSTANCE);
        }
        boolean close = true;
        try {
            if (parser2.getEventType() == 2) {
                Object result = parseEntryInternal();
                parser2.next();
                close = false;
                return result;
            }
            if (close) {
                close();
            }
            return null;
        } finally {
            if (close) {
                close();
            }
        }
    }

    public void close() throws IOException {
        this.inputStream.close();
    }
}
