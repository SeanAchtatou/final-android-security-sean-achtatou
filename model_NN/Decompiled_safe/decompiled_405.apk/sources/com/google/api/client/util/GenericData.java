package com.google.api.client.util;

import com.google.api.client.util.DataMap;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class GenericData extends AbstractMap<String, Object> implements Cloneable {
    final ClassInfo classInfo = ClassInfo.of(getClass());
    public ArrayMap<String, Object> unknownFields = ArrayMap.create();

    public final Object get(Object name) {
        if (!(name instanceof String)) {
            return null;
        }
        String fieldName = (String) name;
        FieldInfo fieldInfo = this.classInfo.getFieldInfo(fieldName);
        if (fieldInfo != null) {
            return fieldInfo.getValue(this);
        }
        return this.unknownFields.get(fieldName);
    }

    public final Object put(String name, Object value) {
        FieldInfo fieldInfo = this.classInfo.getFieldInfo(name);
        if (fieldInfo == null) {
            return this.unknownFields.put(name, value);
        }
        Object oldValue = fieldInfo.getValue(this);
        fieldInfo.setValue(this, value);
        return oldValue;
    }

    public final void set(String name, Object value) {
        FieldInfo fieldInfo = this.classInfo.getFieldInfo(name);
        if (fieldInfo != null) {
            fieldInfo.setValue(this, value);
        } else {
            this.unknownFields.put(name, value);
        }
    }

    public final void putAll(Map<? extends String, ?> map) {
        for (Map.Entry<? extends String, ?> entry : map.entrySet()) {
            set((String) entry.getKey(), entry.getValue());
        }
    }

    public final Object remove(Object name) {
        if (!(name instanceof String)) {
            return null;
        }
        if (this.classInfo.getFieldInfo((String) name) == null) {
            return this.unknownFields.remove(name);
        }
        throw new UnsupportedOperationException();
    }

    public Set<Map.Entry<String, Object>> entrySet() {
        return new EntrySet();
    }

    public GenericData clone() {
        try {
            GenericData result = (GenericData) super.clone();
            Data.deepCopy(this, result);
            result.unknownFields = (ArrayMap) Data.clone(this.unknownFields);
            return result;
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException(e);
        }
    }

    final class EntrySet extends AbstractSet<Map.Entry<String, Object>> {
        private final DataMap.EntrySet dataEntrySet;

        EntrySet() {
            this.dataEntrySet = new DataMap(GenericData.this).entrySet();
        }

        public Iterator<Map.Entry<String, Object>> iterator() {
            return new EntryIterator(this.dataEntrySet);
        }

        public int size() {
            return GenericData.this.unknownFields.size() + this.dataEntrySet.size();
        }

        public void clear() {
            GenericData.this.unknownFields.clear();
            this.dataEntrySet.clear();
        }
    }

    final class EntryIterator implements Iterator<Map.Entry<String, Object>> {
        private final Iterator<Map.Entry<String, Object>> fieldIterator;
        private boolean startedUnknown;
        private final Iterator<Map.Entry<String, Object>> unknownIterator;

        EntryIterator(DataMap.EntrySet dataEntrySet) {
            this.fieldIterator = dataEntrySet.iterator();
            this.unknownIterator = GenericData.this.unknownFields.entrySet().iterator();
        }

        public boolean hasNext() {
            return this.fieldIterator.hasNext() || this.unknownIterator.hasNext();
        }

        /* Debug info: failed to restart local var, previous not found, register: 1 */
        public Map.Entry<String, Object> next() {
            if (!this.startedUnknown) {
                if (this.fieldIterator.hasNext()) {
                    return this.fieldIterator.next();
                }
                this.startedUnknown = true;
            }
            return this.unknownIterator.next();
        }

        public void remove() {
            if (this.startedUnknown) {
                this.unknownIterator.remove();
            }
            this.fieldIterator.remove();
        }
    }
}
