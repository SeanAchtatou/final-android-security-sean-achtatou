package com.google.api.client.util;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

@Deprecated
public final class ReflectionMap extends AbstractMap<String, Object> {
    final ClassInfo classInfo;
    private EntrySet entrySet;
    final Object object;
    final int size;

    public ReflectionMap(Object object2) {
        this.object = object2;
        ClassInfo classInfo2 = ClassInfo.of(object2.getClass());
        this.classInfo = classInfo2;
        this.size = classInfo2.getKeyCount();
    }

    public Set<Map.Entry<String, Object>> entrySet() {
        EntrySet entrySet2 = this.entrySet;
        if (entrySet2 != null) {
            return entrySet2;
        }
        EntrySet entrySet3 = new EntrySet();
        this.entrySet = entrySet3;
        return entrySet3;
    }

    final class EntrySet extends AbstractSet<Map.Entry<String, Object>> {
        EntrySet() {
        }

        public Iterator<Map.Entry<String, Object>> iterator() {
            return new EntryIterator(ReflectionMap.this.classInfo, ReflectionMap.this.object);
        }

        public int size() {
            return ReflectionMap.this.size;
        }
    }

    static final class EntryIterator implements Iterator<Map.Entry<String, Object>> {
        final ClassInfo classInfo;
        private int fieldIndex = 0;
        private final String[] fieldNames;
        private final int numFields;
        private final Object object;

        EntryIterator(ClassInfo classInfo2, Object object2) {
            this.classInfo = classInfo2;
            this.object = object2;
            Collection<String> keyNames = this.classInfo.getKeyNames();
            int size = keyNames.size();
            this.numFields = size;
            if (size == 0) {
                this.fieldNames = null;
                return;
            }
            String[] fieldNames2 = new String[size];
            this.fieldNames = fieldNames2;
            int i = 0;
            for (String keyName : keyNames) {
                fieldNames2[i] = keyName;
                i++;
            }
            Arrays.sort(fieldNames2);
        }

        public boolean hasNext() {
            return this.fieldIndex < this.numFields;
        }

        public Map.Entry<String, Object> next() {
            int fieldIndex2 = this.fieldIndex;
            if (fieldIndex2 >= this.numFields) {
                throw new NoSuchElementException();
            }
            String fieldName = this.fieldNames[fieldIndex2];
            this.fieldIndex++;
            return new Entry(this.object, fieldName);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    static final class Entry implements Map.Entry<String, Object> {
        private final ClassInfo classInfo;
        private final String fieldName;
        private Object fieldValue;
        private boolean isFieldValueComputed;
        private final Object object;

        public Entry(Object object2, String fieldName2) {
            this.classInfo = ClassInfo.of(object2.getClass());
            this.object = object2;
            this.fieldName = fieldName2;
        }

        public String getKey() {
            return this.fieldName;
        }

        public Object getValue() {
            if (this.isFieldValueComputed) {
                return this.fieldValue;
            }
            this.isFieldValueComputed = true;
            Object value = this.classInfo.getFieldInfo(this.fieldName).getValue(this.object);
            this.fieldValue = value;
            return value;
        }

        public Object setValue(Object value) {
            FieldInfo fieldInfo = this.classInfo.getFieldInfo(this.fieldName);
            Object oldValue = getValue();
            fieldInfo.setValue(this.object, value);
            this.fieldValue = value;
            return oldValue;
        }
    }
}
