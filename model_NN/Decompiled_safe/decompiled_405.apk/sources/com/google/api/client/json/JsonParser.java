package com.google.api.client.json;

import com.aetrion.flickr.Flickr;
import com.aetrion.flickr.places.Place;
import com.google.api.client.util.ClassInfo;
import com.google.api.client.util.Data;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.GenericData;
import com.google.api.client.util.Types;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public abstract class JsonParser {
    public abstract void close() throws IOException;

    public abstract BigInteger getBigIntegerValue() throws IOException;

    public abstract byte getByteValue() throws IOException;

    public abstract String getCurrentName() throws IOException;

    public abstract JsonToken getCurrentToken();

    public abstract BigDecimal getDecimalValue() throws IOException;

    public abstract double getDoubleValue() throws IOException;

    public abstract JsonFactory getFactory();

    public abstract float getFloatValue() throws IOException;

    public abstract int getIntValue() throws IOException;

    public abstract long getLongValue() throws IOException;

    public abstract short getShortValue() throws IOException;

    public abstract String getText() throws IOException;

    public abstract JsonToken nextToken() throws IOException;

    public abstract JsonParser skipChildren() throws IOException;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.JsonParser.parseAndClose(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void
     arg types: [T, com.google.api.client.json.CustomizeJsonParser]
     candidates:
      com.google.api.client.json.JsonParser.parseAndClose(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T
      com.google.api.client.json.JsonParser.parseAndClose(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void */
    public final <T> T parseAndClose(Class cls, CustomizeJsonParser customizeParser) throws IOException {
        T newInstance = Types.newInstance(cls);
        parseAndClose((Object) newInstance, customizeParser);
        return newInstance;
    }

    public final void skipToKey(String keyToFind) throws IOException {
        JsonToken curToken = startParsingObject();
        while (curToken == JsonToken.FIELD_NAME) {
            String key = getText();
            nextToken();
            if (!keyToFind.equals(key)) {
                skipChildren();
                curToken = nextToken();
            } else {
                return;
            }
        }
    }

    private JsonToken startParsingObject() throws IOException {
        JsonToken currentToken = getCurrentToken();
        if (currentToken == JsonToken.START_OBJECT) {
            currentToken = nextToken();
        }
        Preconditions.checkArgument(currentToken == JsonToken.FIELD_NAME || currentToken == JsonToken.END_OBJECT, currentToken);
        return currentToken;
    }

    public final void parseAndClose(Object destination, CustomizeJsonParser customizeParser) throws IOException {
        try {
            parse(destination, customizeParser);
        } finally {
            close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.JsonParser.parse(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void
     arg types: [T, com.google.api.client.json.CustomizeJsonParser]
     candidates:
      com.google.api.client.json.JsonParser.parse(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T
      com.google.api.client.json.JsonParser.parse(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void */
    public final <T> T parse(Class cls, CustomizeJsonParser customizeParser) throws IOException {
        T newInstance = Types.newInstance(cls);
        parse((Object) newInstance, customizeParser);
        return newInstance;
    }

    public final void parse(Object destination, CustomizeJsonParser customizeParser) throws IOException {
        ArrayList<Type> context = new ArrayList<>();
        context.add(destination.getClass());
        parse(context, destination, customizeParser);
    }

    private void parse(ArrayList<Type> context, Object destination, CustomizeJsonParser customizeParser) throws IOException {
        if (destination instanceof GenericJson) {
            ((GenericJson) destination).jsonFactory = getFactory();
        }
        JsonToken curToken = startParsingObject();
        Class<?> destinationClass = destination.getClass();
        ClassInfo classInfo = ClassInfo.of(destinationClass);
        boolean isGenericData = GenericData.class.isAssignableFrom(destinationClass);
        if (isGenericData || !Map.class.isAssignableFrom(destinationClass)) {
            while (curToken == JsonToken.FIELD_NAME) {
                String key = getText();
                JsonToken curToken2 = nextToken();
                if (customizeParser == null || !customizeParser.stopAt(destination, key)) {
                    FieldInfo fieldInfo = classInfo.getFieldInfo(key);
                    if (fieldInfo != null) {
                        if (!fieldInfo.isFinal() || fieldInfo.isPrimitive()) {
                            Field field = fieldInfo.getField();
                            int contextSize = context.size();
                            context.add(field.getGenericType());
                            Object fieldValue = parseValue(curToken2, field, fieldInfo.getGenericType(), context, destination, customizeParser);
                            context.remove(contextSize);
                            fieldInfo.setValue(destination, fieldValue);
                        } else {
                            throw new IllegalArgumentException("final array/object fields are not supported");
                        }
                    } else if (isGenericData) {
                        ((GenericData) destination).set(key, parseValue(curToken2, null, null, context, destination, customizeParser));
                    } else {
                        if (customizeParser != null) {
                            customizeParser.handleUnrecognizedKey(destination, key);
                        }
                        skipChildren();
                    }
                    curToken = nextToken();
                } else {
                    return;
                }
            }
            return;
        }
        parseMap((Map) destination, Types.getMapValueParameter(destinationClass), context, customizeParser);
    }

    public final <T> Collection<T> parseArrayAndClose(Class<?> destinationCollectionClass, Class<T> destinationItemClass, CustomizeJsonParser customizeParser) throws IOException {
        try {
            return parseArray(destinationCollectionClass, destinationItemClass, customizeParser);
        } finally {
            close();
        }
    }

    public final <T> void parseArrayAndClose(Collection<? super T> destinationCollection, Class<T> destinationItemClass, CustomizeJsonParser customizeParser) throws IOException {
        try {
            parseArray(destinationCollection, destinationItemClass, customizeParser);
        } finally {
            close();
        }
    }

    public final <T> Collection<T> parseArray(Class<?> destinationCollectionClass, Class<T> destinationItemClass, CustomizeJsonParser customizeParser) throws IOException {
        Collection<T> destinationCollection = Data.newCollectionInstance(destinationCollectionClass);
        parseArray(destinationCollection, destinationItemClass, customizeParser);
        return destinationCollection;
    }

    public final <T> void parseArray(Collection<? super T> destinationCollection, Class<T> destinationItemClass, CustomizeJsonParser customizeParser) throws IOException {
        parseArray(destinationCollection, destinationItemClass, new ArrayList(), customizeParser);
    }

    private <T> void parseArray(Collection<T> destinationCollection, Type destinationItemType, ArrayList<Type> context, CustomizeJsonParser customizeParser) throws IOException {
        while (true) {
            JsonToken listToken = nextToken();
            if (listToken != JsonToken.END_ARRAY) {
                destinationCollection.add(parseValue(listToken, null, destinationItemType, context, destinationCollection, customizeParser));
            } else {
                return;
            }
        }
    }

    private void parseMap(Map<String, Object> destinationMap, Type valueType, ArrayList<Type> context, CustomizeJsonParser customizeParser) throws IOException {
        JsonToken curToken = startParsingObject();
        while (curToken == JsonToken.FIELD_NAME) {
            String key = getText();
            JsonToken curToken2 = nextToken();
            if (customizeParser == null || !customizeParser.stopAt(destinationMap, key)) {
                destinationMap.put(key, parseValue(curToken2, null, valueType, context, destinationMap, customizeParser));
                curToken = nextToken();
            } else {
                return;
            }
        }
    }

    private final Object parseValue(JsonToken token, Field field, Type valueType, ArrayList<Type> context, Object destination, CustomizeJsonParser customizeParser) throws IOException {
        Map<String, Object> map;
        Type valueType2 = Data.resolveWildcardTypeOrTypeVariable(context, valueType);
        Class<?> valueClass = valueType2 instanceof Class ? (Class) valueType2 : null;
        if (valueType2 instanceof ParameterizedType) {
            valueClass = Types.getRawClass((ParameterizedType) valueType2);
        }
        switch (AnonymousClass1.$SwitchMap$com$google$api$client$json$JsonToken[token.ordinal()]) {
            case 1:
                boolean isArray = Types.isArray(valueType2);
                Preconditions.checkArgument(valueType2 == null || isArray || (valueClass != null && Types.isAssignableToOrFrom(valueClass, Collection.class)), "%s: expected collection or array type but got %s for field %s", new Object[]{getCurrentName(), valueType2, field});
                Collection<Object> collectionValue = null;
                if (!(customizeParser == null || field == null)) {
                    collectionValue = customizeParser.newInstanceForArray(destination, field);
                }
                if (collectionValue == null) {
                    collectionValue = Data.newCollectionInstance(valueType2);
                }
                Type subType = null;
                if (isArray) {
                    subType = Types.getArrayComponentType(valueType2);
                } else if (valueClass != null && Iterable.class.isAssignableFrom(valueClass)) {
                    subType = Types.getIterableParameter(valueType2);
                }
                Type subType2 = Data.resolveWildcardTypeOrTypeVariable(context, subType);
                parseArray(collectionValue, subType2, context, customizeParser);
                if (isArray) {
                    return Types.toArray(collectionValue, Types.getRawArrayComponentType(context, subType2));
                }
                return collectionValue;
            case 2:
                Preconditions.checkArgument(!Types.isArray(valueType2), "%s: expected object or map type but got %s for field %s", new Object[]{getCurrentName(), valueType2, field});
                Object newInstance = null;
                if (!(valueClass == null || customizeParser == null)) {
                    newInstance = customizeParser.newInstanceForObject(destination, valueClass);
                }
                boolean isMap = valueClass != null && Types.isAssignableToOrFrom(valueClass, Map.class);
                if (newInstance != null) {
                    map = newInstance;
                } else if (isMap || valueClass == null) {
                    map = Data.newMapInstance(valueClass);
                } else {
                    map = Types.newInstance(valueClass);
                }
                int contextSize = context.size();
                if (valueType2 != null) {
                    context.add(valueType2);
                }
                if (isMap && !GenericData.class.isAssignableFrom(valueClass)) {
                    Type subValueType = Map.class.isAssignableFrom(valueClass) ? Types.getMapValueParameter(valueType2) : null;
                    if (subValueType != null) {
                        parseMap((Map) map, subValueType, context, customizeParser);
                        return map;
                    }
                }
                parse(context, map, customizeParser);
                if (valueType2 != null) {
                    context.remove(contextSize);
                }
                return map;
            case 3:
            case 4:
                Preconditions.checkArgument(valueType2 == null || valueClass == Boolean.TYPE || (valueClass != null && valueClass.isAssignableFrom(Boolean.class)), "%s: expected type Boolean or boolean but got %s for field %s" + field, new Object[]{getCurrentName(), valueType2, field});
                if (token == JsonToken.VALUE_TRUE) {
                    return Boolean.TRUE;
                }
                return Boolean.FALSE;
            case 5:
            case Flickr.ACCURACY_REGION:
                Preconditions.checkArgument(field == null || field.getAnnotation(JsonString.class) == null, "%s: number type formatted as a JSON number cannot use @JsonString annotation on the field %s", new Object[]{getCurrentName(), field});
                if (valueClass == null || valueClass.isAssignableFrom(BigDecimal.class)) {
                    return getDecimalValue();
                }
                if (valueClass == BigInteger.class) {
                    return getBigIntegerValue();
                }
                if (valueClass == Double.class || valueClass == Double.TYPE) {
                    return Double.valueOf(getDoubleValue());
                }
                if (valueClass == Long.class || valueClass == Long.TYPE) {
                    return Long.valueOf(getLongValue());
                }
                if (valueClass == Float.class || valueClass == Float.TYPE) {
                    return Float.valueOf(getFloatValue());
                }
                if (valueClass == Integer.class || valueClass == Integer.TYPE) {
                    return Integer.valueOf(getIntValue());
                }
                if (valueClass == Short.class || valueClass == Short.TYPE) {
                    return Short.valueOf(getShortValue());
                }
                if (valueClass == Byte.class || valueClass == Byte.TYPE) {
                    return Byte.valueOf(getByteValue());
                }
                throw new IllegalArgumentException(getCurrentName() + ": expected numeric type but got " + valueType2 + " for field " + field);
            case Place.TYPE_LOCALITY:
                Preconditions.checkArgument(valueClass == null || !Number.class.isAssignableFrom(valueClass) || !(field == null || field.getAnnotation(JsonString.class) == null), "%s: number field formatted as a JSON string must use the @JsonString annotation: %s", new Object[]{getCurrentName(), field});
                try {
                    return Data.parsePrimitiveValue(valueType2, getText());
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(getCurrentName() + " for field " + field, e);
                }
            case Place.TYPE_REGION:
                Preconditions.checkArgument(valueClass == null || !valueClass.isPrimitive(), "%s: primitive number field but found a JSON null: %s", new Object[]{getCurrentName(), field});
                if (!(valueClass == null || (valueClass.getModifiers() & 1536) == 0)) {
                    if (Types.isAssignableToOrFrom(valueClass, Collection.class)) {
                        return Data.nullOf(Data.newCollectionInstance(valueType2).getClass());
                    }
                    if (Types.isAssignableToOrFrom(valueClass, Map.class)) {
                        return Data.nullOf(Data.newMapInstance(valueClass).getClass());
                    }
                }
                return Data.nullOf(Types.getRawArrayComponentType(context, valueType2));
            default:
                throw new IllegalArgumentException(getCurrentName() + ": unexpected JSON node type: " + token);
        }
    }

    /* renamed from: com.google.api.client.json.JsonParser$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$api$client$json$JsonToken = new int[JsonToken.values().length];

        static {
            try {
                $SwitchMap$com$google$api$client$json$JsonToken[JsonToken.START_ARRAY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$google$api$client$json$JsonToken[JsonToken.START_OBJECT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$google$api$client$json$JsonToken[JsonToken.VALUE_TRUE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$google$api$client$json$JsonToken[JsonToken.VALUE_FALSE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$google$api$client$json$JsonToken[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$google$api$client$json$JsonToken[JsonToken.VALUE_NUMBER_INT.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$google$api$client$json$JsonToken[JsonToken.VALUE_STRING.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$google$api$client$json$JsonToken[JsonToken.VALUE_NULL.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
        }
    }
}
