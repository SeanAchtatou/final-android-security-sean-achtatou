package com.google.api.client.testing.http;

import com.google.api.client.http.HttpMethod;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.LowLevelHttpRequest;
import java.io.IOException;
import java.util.EnumSet;

public class MockHttpTransport extends HttpTransport {
    public EnumSet<HttpMethod> supportedOptionalMethods = EnumSet.of(HttpMethod.HEAD, HttpMethod.PATCH);

    public LowLevelHttpRequest buildDeleteRequest(String url) throws IOException {
        return new MockLowLevelHttpRequest(url);
    }

    public LowLevelHttpRequest buildGetRequest(String url) throws IOException {
        return new MockLowLevelHttpRequest(url);
    }

    public LowLevelHttpRequest buildHeadRequest(String url) throws IOException {
        if (!supportsHead()) {
            return super.buildHeadRequest(url);
        }
        return new MockLowLevelHttpRequest(url);
    }

    public LowLevelHttpRequest buildPatchRequest(String url) throws IOException {
        if (!supportsPatch()) {
            return super.buildPatchRequest(url);
        }
        return new MockLowLevelHttpRequest(url);
    }

    public LowLevelHttpRequest buildPostRequest(String url) throws IOException {
        return new MockLowLevelHttpRequest(url);
    }

    public LowLevelHttpRequest buildPutRequest(String url) throws IOException {
        return new MockLowLevelHttpRequest(url);
    }

    public boolean supportsHead() {
        return this.supportedOptionalMethods.contains(HttpMethod.HEAD);
    }

    public boolean supportsPatch() {
        return this.supportedOptionalMethods.contains(HttpMethod.PATCH);
    }
}
