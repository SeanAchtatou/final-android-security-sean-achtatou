package com.google.api.client.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

public abstract class JsonFactory {
    public abstract JsonGenerator createJsonGenerator(OutputStream outputStream, JsonEncoding jsonEncoding) throws IOException;

    public abstract JsonGenerator createJsonGenerator(Writer writer) throws IOException;

    public abstract JsonParser createJsonParser(InputStream inputStream) throws IOException;

    public abstract JsonParser createJsonParser(Reader reader) throws IOException;

    public abstract JsonParser createJsonParser(String str) throws IOException;

    public final String toString(Object item) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        try {
            JsonGenerator generator = createJsonGenerator(byteStream, JsonEncoding.UTF8);
            generator.serialize(item);
            generator.flush();
            return byteStream.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.JsonParser.parse(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T
     arg types: [java.lang.Class<T>, ?[OBJECT, ARRAY]]
     candidates:
      com.google.api.client.json.JsonParser.parse(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void
      com.google.api.client.json.JsonParser.parse(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T */
    public final <T> T fromString(String value, Class<T> destinationClass) throws IOException {
        JsonParser parser = createJsonParser(value);
        parser.nextToken();
        return parser.parse((Class) destinationClass, (CustomizeJsonParser) null);
    }
}
