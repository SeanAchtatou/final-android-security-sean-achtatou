package com.google.api.client.util;

import com.aetrion.flickr.util.UrlUtilities;
import com.google.common.base.Charsets;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class Strings {
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    @Deprecated
    public static final Charset UTF8_CHARSET = Charsets.UTF_8;
    public static final String VERSION = "1.4.1-beta";

    public static byte[] toBytesUtf8(String string) {
        try {
            return string.getBytes(UrlUtilities.UTF8);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String fromBytesUtf8(byte[] bytes) {
        try {
            return new String(bytes, UrlUtilities.UTF8);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private Strings() {
    }
}
