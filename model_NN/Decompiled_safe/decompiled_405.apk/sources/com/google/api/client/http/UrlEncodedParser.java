package com.google.api.client.http;

import com.google.api.client.util.ArrayValueMap;
import com.google.api.client.util.ClassInfo;
import com.google.api.client.util.Data;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.GenericData;
import com.google.api.client.util.Types;
import com.google.api.client.util.escape.CharEscapers;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public final class UrlEncodedParser implements HttpParser {
    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    public String contentType = CONTENT_TYPE;
    public boolean disableContentLogging;

    public String getContentType() {
        return this.contentType;
    }

    public <T> T parse(HttpResponse response, Class cls) throws IOException {
        if (this.disableContentLogging) {
            response.disableContentLogging = true;
        }
        T newInstance = Types.newInstance(cls);
        parse(response.parseAsString(), newInstance);
        return newInstance;
    }

    /* JADX INFO: Multiple debug info for r1v7 int: [D('amp' int), D('cur' int)] */
    /* JADX INFO: Multiple debug info for r10v4 java.lang.reflect.Type: [D('type' java.lang.reflect.Type), D('name' java.lang.String)] */
    public static void parse(String content, Object data) {
        Map<Object, Object> map;
        int amp;
        String name;
        String stringValue;
        int nextEquals;
        if (content != null) {
            Class<?> clazz = data.getClass();
            ClassInfo classInfo = ClassInfo.of(clazz);
            List<Type> context = Arrays.asList(clazz);
            GenericData genericData = GenericData.class.isAssignableFrom(clazz) ? (GenericData) data : null;
            if (Map.class.isAssignableFrom(clazz)) {
                map = (Map) data;
            } else {
                map = null;
            }
            ArrayValueMap arrayValueMap = new ArrayValueMap(data);
            int length = content.length();
            int nextEquals2 = content.indexOf(61);
            for (int cur = 0; cur < length; cur = amp + 1) {
                amp = content.indexOf(38, cur);
                if (amp == -1) {
                    amp = length;
                }
                if (nextEquals2 == -1 || nextEquals2 >= amp) {
                    name = content.substring(cur, amp);
                    stringValue = "";
                    nextEquals = nextEquals2;
                } else {
                    name = content.substring(cur, nextEquals2);
                    stringValue = CharEscapers.decodeUri(content.substring(nextEquals2 + 1, amp));
                    nextEquals = content.indexOf(61, amp + 1);
                }
                String name2 = CharEscapers.decodeUri(name);
                FieldInfo fieldInfo = classInfo.getFieldInfo(name2);
                if (fieldInfo != null) {
                    Type type = Data.resolveWildcardTypeOrTypeVariable(context, fieldInfo.getGenericType());
                    if (Types.isArray(type)) {
                        Class<?> rawArrayComponentType = Types.getRawArrayComponentType(context, Types.getArrayComponentType(type));
                        arrayValueMap.put(fieldInfo.getField(), rawArrayComponentType, parseValue(rawArrayComponentType, context, stringValue));
                    } else if (Types.isAssignableToOrFrom(Types.getRawArrayComponentType(context, type), Iterable.class)) {
                        Collection<Object> collection = (Collection) fieldInfo.getValue(data);
                        if (collection == null) {
                            collection = Data.newCollectionInstance(type);
                            fieldInfo.setValue(data, collection);
                        }
                        collection.add(parseValue(type == Object.class ? null : Types.getIterableParameter(type), context, stringValue));
                    } else {
                        fieldInfo.setValue(data, parseValue(type, context, stringValue));
                    }
                } else if (map != null) {
                    ArrayList<String> listValue = (ArrayList) map.get(name2);
                    if (listValue == null) {
                        listValue = new ArrayList<>();
                        if (genericData != null) {
                            genericData.set(name2, listValue);
                        } else {
                            map.put(name2, listValue);
                        }
                    }
                    listValue.add(stringValue);
                }
                nextEquals2 = nextEquals;
            }
            arrayValueMap.setValues();
        }
    }

    private static Object parseValue(Type valueType, List<Type> context, String value) {
        return Data.parsePrimitiveValue(Data.resolveWildcardTypeOrTypeVariable(context, valueType), value);
    }
}
