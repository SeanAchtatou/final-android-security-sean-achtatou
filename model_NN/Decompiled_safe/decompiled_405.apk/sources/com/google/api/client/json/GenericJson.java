package com.google.api.client.json;

import com.google.api.client.util.GenericData;

public class GenericJson extends GenericData implements Cloneable {
    public JsonFactory jsonFactory;

    public String toString() {
        return this.jsonFactory.toString(this);
    }

    public GenericJson clone() {
        return (GenericJson) super.clone();
    }
}
