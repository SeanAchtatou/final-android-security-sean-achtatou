package com.google.api.client.http.json;

import com.google.api.client.http.HttpParser;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.CustomizeJsonParser;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonParser;
import java.io.IOException;
import java.io.InputStream;

public class JsonHttpParser implements HttpParser {
    public String contentType = Json.CONTENT_TYPE;
    public JsonFactory jsonFactory;

    public final String getContentType() {
        return this.contentType;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.JsonParser.parseAndClose(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T
     arg types: [java.lang.Class<T>, ?[OBJECT, ARRAY]]
     candidates:
      com.google.api.client.json.JsonParser.parseAndClose(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void
      com.google.api.client.json.JsonParser.parseAndClose(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T */
    public <T> T parse(HttpResponse response, Class<T> dataClass) throws IOException {
        return parserForResponse(this.jsonFactory, response).parseAndClose((Class) dataClass, (CustomizeJsonParser) null);
    }

    public static JsonParser parserForResponse(JsonFactory jsonFactory2, HttpResponse response) throws IOException {
        InputStream content = response.getContent();
        try {
            JsonParser parser = jsonFactory2.createJsonParser(content);
            parser.nextToken();
            content = null;
            return parser;
        } finally {
            if (content != null) {
                content.close();
            }
        }
    }
}
