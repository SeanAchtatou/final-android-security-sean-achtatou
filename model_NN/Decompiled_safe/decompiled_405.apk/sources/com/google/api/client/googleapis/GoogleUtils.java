package com.google.api.client.googleapis;

import com.google.api.client.http.HttpTransport;

@Deprecated
public final class GoogleUtils {
    @Deprecated
    public static MethodOverrideIntercepter useMethodOverride(HttpTransport transport) {
        transport.removeIntercepters(MethodOverrideIntercepter.class);
        MethodOverrideIntercepter result = new MethodOverrideIntercepter();
        transport.intercepters.add(0, result);
        return result;
    }

    private GoogleUtils() {
    }
}
