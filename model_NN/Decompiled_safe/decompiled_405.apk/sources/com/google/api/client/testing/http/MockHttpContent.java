package com.google.api.client.testing.http;

import com.google.api.client.http.HttpContent;
import java.io.IOException;
import java.io.OutputStream;

public class MockHttpContent implements HttpContent {
    public byte[] content = new byte[0];
    public String encoding;
    public long length = -1;
    public String type;

    public String getEncoding() {
        return this.encoding;
    }

    public long getLength() throws IOException {
        return this.length;
    }

    public String getType() {
        return this.type;
    }

    public void writeTo(OutputStream out) throws IOException {
        out.write(this.content);
    }

    public boolean retrySupported() {
        return true;
    }
}
