package com.google.api.client.testing.http;

import com.google.api.client.http.LowLevelHttpResponse;
import com.google.api.client.util.Strings;
import com.google.common.collect.Lists;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MockLowLevelHttpResponse extends LowLevelHttpResponse {
    public InputStream content;
    public String contentType;
    public ArrayList<String> headerNames = Lists.newArrayList();
    public ArrayList<String> headerValues = Lists.newArrayList();
    public int statusCode = 200;

    public void addHeader(String name, String value) {
        this.headerNames.add(name);
        this.headerValues.add(value);
    }

    public void setContent(String stringContent) {
        this.content = new ByteArrayInputStream(Strings.toBytesUtf8(stringContent));
    }

    public InputStream getContent() throws IOException {
        return this.content;
    }

    public String getContentEncoding() {
        return null;
    }

    public long getContentLength() {
        return 0;
    }

    public String getContentType() {
        return this.contentType;
    }

    public int getHeaderCount() {
        return this.headerNames.size();
    }

    public String getHeaderName(int index) {
        return this.headerNames.get(index);
    }

    public String getHeaderValue(int index) {
        return this.headerValues.get(index);
    }

    public String getReasonPhrase() {
        return null;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public String getStatusLine() {
        return null;
    }
}
