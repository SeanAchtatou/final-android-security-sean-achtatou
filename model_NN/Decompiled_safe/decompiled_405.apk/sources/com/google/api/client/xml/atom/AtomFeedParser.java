package com.google.api.client.xml.atom;

import com.google.api.client.http.HttpResponse;
import com.google.api.client.util.ClassInfo;
import com.google.api.client.xml.Xml;
import com.google.api.client.xml.XmlNamespaceDictionary;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@Deprecated
public final class AtomFeedParser<T, E> extends AbstractAtomFeedParser<T> {
    public Class<E> entryClass;

    public E parseNextEntry() throws IOException, XmlPullParserException {
        return super.parseNextEntry();
    }

    /* access modifiers changed from: protected */
    public Object parseEntryInternal() throws IOException, XmlPullParserException {
        E result = ClassInfo.newInstance(this.entryClass);
        Xml.parseElement(this.parser, result, this.namespaceDictionary, null);
        return result;
    }

    public static <T, E> AtomFeedParser<T, E> create(HttpResponse response, XmlNamespaceDictionary namespaceDictionary, Class<T> feedClass, Class<E> entryClass2) throws IOException, XmlPullParserException {
        InputStream content = response.getContent();
        try {
            Atom.checkContentType(response.contentType);
            XmlPullParser parser = Xml.createParser();
            parser.setInput(content, null);
            AtomFeedParser<T, E> result = new AtomFeedParser<>();
            result.parser = parser;
            result.inputStream = content;
            result.feedClass = feedClass;
            result.entryClass = entryClass2;
            result.namespaceDictionary = namespaceDictionary;
            content = null;
            return result;
        } finally {
            if (content != null) {
                content.close();
            }
        }
    }
}
