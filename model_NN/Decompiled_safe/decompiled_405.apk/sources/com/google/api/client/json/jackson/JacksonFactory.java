package com.google.api.client.json.jackson;

import com.aetrion.flickr.Flickr;
import com.aetrion.flickr.places.Place;
import com.google.api.client.json.JsonEncoding;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonParser;
import com.google.api.client.json.JsonToken;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import org.codehaus.jackson.JsonGenerator;

public final class JacksonFactory extends JsonFactory {
    private final org.codehaus.jackson.JsonFactory factory = new org.codehaus.jackson.JsonFactory();

    public JacksonFactory() {
        this.factory.configure(JsonGenerator.Feature.AUTO_CLOSE_JSON_CONTENT, false);
    }

    public com.google.api.client.json.JsonGenerator createJsonGenerator(OutputStream out, JsonEncoding enc) throws IOException {
        return new JacksonGenerator(this, this.factory.createJsonGenerator(out, org.codehaus.jackson.JsonEncoding.UTF8));
    }

    public com.google.api.client.json.JsonGenerator createJsonGenerator(Writer writer) throws IOException {
        return new JacksonGenerator(this, this.factory.createJsonGenerator(writer));
    }

    public JsonParser createJsonParser(Reader reader) throws IOException {
        return new JacksonParser(this, this.factory.createJsonParser(reader));
    }

    public JsonParser createJsonParser(InputStream in) throws IOException {
        return new JacksonParser(this, this.factory.createJsonParser(in));
    }

    public JsonParser createJsonParser(String value) throws IOException {
        return new JacksonParser(this, this.factory.createJsonParser(value));
    }

    static JsonToken convert(org.codehaus.jackson.JsonToken token) {
        if (token == null) {
            return null;
        }
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[token.ordinal()]) {
            case 1:
                return JsonToken.END_ARRAY;
            case 2:
                return JsonToken.START_ARRAY;
            case 3:
                return JsonToken.END_OBJECT;
            case 4:
                return JsonToken.START_OBJECT;
            case 5:
                return JsonToken.VALUE_FALSE;
            case Flickr.ACCURACY_REGION:
                return JsonToken.VALUE_TRUE;
            case Place.TYPE_LOCALITY:
                return JsonToken.VALUE_NULL;
            case Place.TYPE_REGION:
                return JsonToken.VALUE_STRING;
            case Place.TYPE_COUNTY:
                return JsonToken.VALUE_NUMBER_FLOAT;
            case 10:
                return JsonToken.VALUE_NUMBER_INT;
            case Flickr.ACCURACY_CITY:
                return JsonToken.FIELD_NAME;
            default:
                return JsonToken.NOT_AVAILABLE;
        }
    }

    /* renamed from: com.google.api.client.json.jackson.JacksonFactory$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$codehaus$jackson$JsonToken = new int[org.codehaus.jackson.JsonToken.values().length];

        static {
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.END_ARRAY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.START_ARRAY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.END_OBJECT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.START_OBJECT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.VALUE_FALSE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.VALUE_TRUE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.VALUE_NULL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.VALUE_STRING.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.VALUE_NUMBER_INT.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$org$codehaus$jackson$JsonToken[org.codehaus.jackson.JsonToken.FIELD_NAME.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
        }
    }
}
