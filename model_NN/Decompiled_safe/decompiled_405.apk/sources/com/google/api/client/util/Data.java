package com.google.api.client.util;

import com.google.common.base.Preconditions;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

public class Data {
    public static final BigDecimal NULL_BIG_DECIMAL = new BigDecimal("0");
    public static final BigInteger NULL_BIG_INTEGER = new BigInteger("0");
    public static final Boolean NULL_BOOLEAN = new Boolean(true);
    public static final Byte NULL_BYTE = new Byte((byte) 0);
    public static final Character NULL_CHARACTER = new Character(0);
    public static final DateTime NULL_DATE_TIME = new DateTime(0);
    public static final Double NULL_DOUBLE = new Double(0.0d);
    public static final Float NULL_FLOAT = new Float(0.0f);
    public static final Integer NULL_INTEGER = new Integer(0);
    public static final Long NULL_LONG = new Long(0);
    public static final Short NULL_SHORT = new Short((short) 0);
    public static final String NULL_STRING = new String();
    private static final ConcurrentHashMap<Class<?>, Object> nullCache = new ConcurrentHashMap<>();

    static {
        nullCache.put(Boolean.class, NULL_BOOLEAN);
        nullCache.put(String.class, NULL_STRING);
        nullCache.put(Character.class, NULL_CHARACTER);
        nullCache.put(Byte.class, NULL_BYTE);
        nullCache.put(Short.class, NULL_SHORT);
        nullCache.put(Integer.class, NULL_INTEGER);
        nullCache.put(Float.class, NULL_FLOAT);
        nullCache.put(Long.class, NULL_LONG);
        nullCache.put(Double.class, NULL_DOUBLE);
        nullCache.put(BigInteger.class, NULL_BIG_INTEGER);
        nullCache.put(BigDecimal.class, NULL_BIG_DECIMAL);
        nullCache.put(DateTime.class, NULL_DATE_TIME);
    }

    public static <T> T nullOf(Class<?> objClass) {
        Object tResult;
        Object result;
        T result2 = nullCache.get(objClass);
        if (result2 != null) {
            return result2;
        }
        synchronized (nullCache) {
            Object result3 = nullCache.get(objClass);
            if (result3 == null) {
                if (objClass.isArray()) {
                    int dims = 0;
                    Class<?> componentType = objClass;
                    do {
                        componentType = componentType.getComponentType();
                        dims++;
                    } while (componentType.isArray());
                    result = Array.newInstance(componentType, new int[dims]);
                } else if (objClass.isEnum()) {
                    FieldInfo fieldInfo = ClassInfo.of(objClass).getFieldInfo(null);
                    Preconditions.checkNotNull(fieldInfo, "enum missing constant with @NullValue annotation: %s", new Object[]{objClass});
                    result = fieldInfo.enumValue();
                } else {
                    result = Types.newInstance(objClass);
                }
                nullCache.put(objClass, result);
                tResult = result;
            } else {
                tResult = result3;
            }
        }
        return tResult;
    }

    public static boolean isNull(Object object) {
        return object != null && object == nullCache.get(object.getClass());
    }

    public static Map<String, Object> mapOf(Object data) {
        if (data == null || isNull(data)) {
            return Collections.emptyMap();
        }
        if (data instanceof Map) {
            return (Map) data;
        }
        return new DataMap(data);
    }

    public static <T> T clone(T data) {
        T copy;
        if (data == null || isPrimitive(data.getClass())) {
            return data;
        }
        if (data instanceof GenericData) {
            return ((GenericData) data).clone();
        }
        Class<?> dataClass = data.getClass();
        if (dataClass.isArray()) {
            copy = Array.newInstance(dataClass.getComponentType(), Array.getLength(data));
        } else if (data instanceof ArrayMap) {
            copy = ((ArrayMap) data).clone();
        } else {
            copy = Types.newInstance(dataClass);
        }
        deepCopy(data, copy);
        return copy;
    }

    /* JADX INFO: Multiple debug info for r1v1 com.google.api.client.util.ClassInfo: [D('classInfo' com.google.api.client.util.ClassInfo), D('srcClass' java.lang.Class<?>)] */
    /* JADX INFO: Multiple debug info for r2v14 com.google.api.client.util.FieldInfo: [D('fieldInfo' com.google.api.client.util.FieldInfo), D('fieldName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v1 java.util.Map: [D('dest' java.lang.Object), D('destMap' java.util.Map<java.lang.String, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r6v1 java.util.Map: [D('src' java.lang.Object), D('srcMap' java.util.Map<java.lang.String, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r6v3 java.util.Iterator: [D('i$' java.util.Iterator), D('srcMap' java.util.Map<java.lang.String, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r7v2 com.google.api.client.util.ArrayMap: [D('dest' java.lang.Object), D('destMap' com.google.api.client.util.ArrayMap<java.lang.Object, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r7v3 java.util.Iterator: [D('dest' java.lang.Object), D('i$' java.util.Iterator)] */
    /* JADX INFO: Multiple debug info for r6v12 java.util.Iterator: [D('src' java.lang.Object), D('i$' java.util.Iterator)] */
    public static void deepCopy(Object src, Object dest) {
        Object srcValue;
        Class<?> srcClass = src.getClass();
        Preconditions.checkArgument(!isPrimitive(srcClass));
        Preconditions.checkArgument(srcClass == dest.getClass());
        if (srcClass.isArray()) {
            Preconditions.checkArgument(Array.getLength(src) == Array.getLength(dest));
            int index = 0;
            for (Object value : Types.iterableOf(src)) {
                Array.set(dest, index, clone(value));
                index++;
            }
        } else if (Collection.class.isAssignableFrom(srcClass)) {
            Collection<Object> collection = (Collection) src;
            if (ArrayList.class.isAssignableFrom(srcClass)) {
                ((ArrayList) dest).ensureCapacity(collection.size());
            }
            Collection collection2 = (Collection) dest;
            for (Object srcValue2 : collection) {
                collection2.add(clone(srcValue2));
            }
        } else {
            boolean isGenericData = GenericData.class.isAssignableFrom(srcClass);
            if (isGenericData || !Map.class.isAssignableFrom(srcClass)) {
                ClassInfo classInfo = ClassInfo.of(srcClass);
                for (String fieldName : classInfo.names) {
                    FieldInfo fieldInfo = classInfo.getFieldInfo(fieldName);
                    if (!fieldInfo.isFinal() && ((!isGenericData || !fieldInfo.isPrimitive()) && (srcValue = fieldInfo.getValue(src)) != null)) {
                        fieldInfo.setValue(dest, clone(srcValue));
                    }
                }
            } else if (ArrayMap.class.isAssignableFrom(srcClass)) {
                ArrayMap<Object, Object> destMap = (ArrayMap) dest;
                ArrayMap arrayMap = (ArrayMap) src;
                int size = arrayMap.size();
                for (int i = 0; i < size; i++) {
                    destMap.set(i, clone(arrayMap.getValue(i)));
                }
            } else {
                Map<String, Object> destMap2 = (Map) dest;
                for (Map.Entry<String, Object> srcEntry : ((Map) src).entrySet()) {
                    destMap2.put(srcEntry.getKey(), clone(srcEntry.getValue()));
                }
            }
        }
    }

    public static boolean isPrimitive(Type type) {
        if (type instanceof WildcardType) {
            type = Types.getBound((WildcardType) type);
        }
        if (!(type instanceof Class)) {
            return false;
        }
        Class<?> typeClass = (Class) type;
        return typeClass.isPrimitive() || typeClass == Character.class || typeClass == String.class || typeClass == Integer.class || typeClass == Long.class || typeClass == Short.class || typeClass == Byte.class || typeClass == Float.class || typeClass == Double.class || typeClass == BigInteger.class || typeClass == BigDecimal.class || typeClass == DateTime.class || typeClass == Boolean.class;
    }

    public static boolean isValueOfPrimitiveType(Object fieldValue) {
        return fieldValue == null || isPrimitive(fieldValue.getClass());
    }

    public static Object parsePrimitiveValue(Type type, String stringValue) {
        Class<?> primitiveClass = type instanceof Class ? (Class) type : null;
        if (type == null || primitiveClass != null) {
            if (stringValue == null || primitiveClass == null || primitiveClass.isAssignableFrom(String.class)) {
                return stringValue;
            }
            if (primitiveClass == Character.class || primitiveClass == Character.TYPE) {
                if (stringValue.length() == 1) {
                    return Character.valueOf(stringValue.charAt(0));
                }
                throw new IllegalArgumentException("expected type Character/char but got " + primitiveClass);
            } else if (primitiveClass == Boolean.class || primitiveClass == Boolean.TYPE) {
                return Boolean.valueOf(stringValue);
            } else {
                if (primitiveClass == Byte.class || primitiveClass == Byte.TYPE) {
                    return Byte.valueOf(stringValue);
                }
                if (primitiveClass == Short.class || primitiveClass == Short.TYPE) {
                    return Short.valueOf(stringValue);
                }
                if (primitiveClass == Integer.class || primitiveClass == Integer.TYPE) {
                    return Integer.valueOf(stringValue);
                }
                if (primitiveClass == Long.class || primitiveClass == Long.TYPE) {
                    return Long.valueOf(stringValue);
                }
                if (primitiveClass == Float.class || primitiveClass == Float.TYPE) {
                    return Float.valueOf(stringValue);
                }
                if (primitiveClass == Double.class || primitiveClass == Double.TYPE) {
                    return Double.valueOf(stringValue);
                }
                if (primitiveClass == DateTime.class) {
                    return DateTime.parseRfc3339(stringValue);
                }
                if (primitiveClass == BigInteger.class) {
                    return new BigInteger(stringValue);
                }
                if (primitiveClass == BigDecimal.class) {
                    return new BigDecimal(stringValue);
                }
                if (primitiveClass.isEnum()) {
                    return ClassInfo.of(primitiveClass).getFieldInfo(stringValue).enumValue();
                }
            }
        }
        throw new IllegalArgumentException("expected primitive class, but got: " + type);
    }

    public static Collection<Object> newCollectionInstance(Type type) {
        if (type instanceof WildcardType) {
            type = Types.getBound((WildcardType) type);
        }
        if (type instanceof ParameterizedType) {
            type = ((ParameterizedType) type).getRawType();
        }
        Class<?> collectionClass = type instanceof Class ? (Class) type : null;
        if (type == null || (type instanceof GenericArrayType) || (collectionClass != null && (collectionClass.isArray() || collectionClass.isAssignableFrom(ArrayList.class)))) {
            return new ArrayList();
        }
        if (collectionClass.isAssignableFrom(HashSet.class)) {
            return new HashSet();
        }
        if (collectionClass.isAssignableFrom(TreeSet.class)) {
            return new TreeSet();
        }
        return (Collection) Types.newInstance(collectionClass);
    }

    public static Map<String, Object> newMapInstance(Class<?> mapClass) {
        if (mapClass == null || mapClass.isAssignableFrom(ArrayMap.class)) {
            return ArrayMap.create();
        }
        if (mapClass.isAssignableFrom(TreeMap.class)) {
            return new TreeMap();
        }
        return (Map) Types.newInstance(mapClass);
    }

    public static Type resolveWildcardTypeOrTypeVariable(List<Type> context, Type type) {
        if (type instanceof WildcardType) {
            type = Types.getBound((WildcardType) type);
        }
        while (type instanceof TypeVariable) {
            Type resolved = Types.resolveTypeVariable(context, (TypeVariable) type);
            if (resolved != null) {
                type = resolved;
            }
            if (type instanceof TypeVariable) {
                type = ((TypeVariable) type).getBounds()[0];
            }
        }
        return type;
    }
}
