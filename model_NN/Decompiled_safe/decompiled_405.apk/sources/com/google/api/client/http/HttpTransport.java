package com.google.api.client.http;

import com.google.api.client.util.ArrayMap;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public abstract class HttpTransport {
    static final Logger LOGGER = Logger.getLogger(HttpTransport.class.getName());
    @Deprecated
    final ArrayMap<String, HttpParser> contentTypeToParserMap = ArrayMap.create();
    @Deprecated
    public HttpHeaders defaultHeaders = new HttpHeaders();
    @Deprecated
    public List<HttpExecuteIntercepter> intercepters = new ArrayList(1);

    /* access modifiers changed from: protected */
    public abstract LowLevelHttpRequest buildDeleteRequest(String str) throws IOException;

    /* access modifiers changed from: protected */
    public abstract LowLevelHttpRequest buildGetRequest(String str) throws IOException;

    /* access modifiers changed from: protected */
    public abstract LowLevelHttpRequest buildPostRequest(String str) throws IOException;

    /* access modifiers changed from: protected */
    public abstract LowLevelHttpRequest buildPutRequest(String str) throws IOException;

    public final HttpRequestFactory createRequestFactory() {
        return createRequestFactory(null);
    }

    public final HttpRequestFactory createRequestFactory(HttpRequestInitializer initializer) {
        return new HttpRequestFactory(this, initializer);
    }

    @Deprecated
    public final void addParser(HttpParser parser) {
        this.contentTypeToParserMap.put(getNormalizedContentType(parser.getContentType()), parser);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    @Deprecated
    public final HttpParser getParser(String contentType) {
        if (contentType == null) {
            return null;
        }
        return this.contentTypeToParserMap.get(getNormalizedContentType(contentType));
    }

    @Deprecated
    private String getNormalizedContentType(String contentType) {
        int semicolon = contentType.indexOf(59);
        return semicolon == -1 ? contentType : contentType.substring(0, semicolon);
    }

    @Deprecated
    public final HttpRequest buildRequest() {
        return new HttpRequest(this, null);
    }

    @Deprecated
    public final HttpRequest buildDeleteRequest() {
        return new HttpRequest(this, HttpMethod.DELETE);
    }

    @Deprecated
    public final HttpRequest buildGetRequest() {
        return new HttpRequest(this, HttpMethod.GET);
    }

    @Deprecated
    public final HttpRequest buildPostRequest() {
        return new HttpRequest(this, HttpMethod.POST);
    }

    @Deprecated
    public final HttpRequest buildPutRequest() {
        return new HttpRequest(this, HttpMethod.PUT);
    }

    @Deprecated
    public final HttpRequest buildPatchRequest() {
        return new HttpRequest(this, HttpMethod.PATCH);
    }

    @Deprecated
    public final HttpRequest buildHeadRequest() {
        return new HttpRequest(this, HttpMethod.HEAD);
    }

    @Deprecated
    public final void removeIntercepters(Class<?> intercepterClass) {
        Iterator<HttpExecuteIntercepter> iterable = this.intercepters.iterator();
        while (iterable.hasNext()) {
            if (intercepterClass.isAssignableFrom(iterable.next().getClass())) {
                iterable.remove();
            }
        }
    }

    public boolean supportsHead() {
        return false;
    }

    public boolean supportsPatch() {
        return false;
    }

    /* access modifiers changed from: protected */
    public LowLevelHttpRequest buildHeadRequest(String url) throws IOException {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: protected */
    public LowLevelHttpRequest buildPatchRequest(String url) throws IOException {
        throw new UnsupportedOperationException();
    }

    public void shutdown() throws IOException {
    }
}
