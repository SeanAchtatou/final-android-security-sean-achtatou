package com.google.api.client.auth.oauth2;

import com.google.api.client.http.HttpExecuteIntercepter;
import com.google.api.client.http.HttpMethod;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.util.GenericData;
import java.util.EnumSet;

@Deprecated
public final class AccessProtectedResource {
    public static void usingAuthorizationHeader(HttpTransport transport, String accessToken) {
        new UsingAuthorizationHeader().authorize(transport, accessToken);
    }

    public static void usingQueryParameter(HttpTransport transport, String accessToken) {
        new UsingQueryParameter().authorize(transport, accessToken);
    }

    public static void usingFormEncodedBody(HttpTransport transport, String accessToken) {
        new UsingFormEncodedBody().authorize(transport, accessToken);
    }

    static abstract class AccessTokenIntercepter implements HttpExecuteIntercepter {
        String accessToken;

        AccessTokenIntercepter() {
        }

        /* access modifiers changed from: package-private */
        public void authorize(HttpTransport transport, String accessToken2) {
            this.accessToken = accessToken2;
            transport.removeIntercepters(AccessTokenIntercepter.class);
            transport.intercepters.add(this);
        }
    }

    static final class UsingAuthorizationHeader extends AccessTokenIntercepter {
        UsingAuthorizationHeader() {
        }

        public void intercept(HttpRequest request) {
            request.headers.authorization = "OAuth " + this.accessToken;
        }
    }

    static final class UsingQueryParameter extends AccessTokenIntercepter {
        UsingQueryParameter() {
        }

        public void intercept(HttpRequest request) {
            request.url.set("oauth_token", this.accessToken);
        }
    }

    static final class UsingFormEncodedBody extends AccessTokenIntercepter {
        private static final EnumSet<HttpMethod> ALLOWED_METHODS = EnumSet.of(HttpMethod.POST, HttpMethod.PUT, HttpMethod.DELETE);

        UsingFormEncodedBody() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.google.api.client.util.GenericData.put(java.lang.Object, java.lang.Object):java.lang.Object
          ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
          ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
          com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object */
        public void intercept(HttpRequest request) {
            if (!ALLOWED_METHODS.contains(request.method)) {
                throw new IllegalArgumentException("expected one of these HTTP methods: " + ALLOWED_METHODS);
            }
            UrlEncodedContent content = (UrlEncodedContent) request.content;
            if (content == null) {
                content = new UrlEncodedContent();
                request.content = content;
            }
            GenericData data = (GenericData) content.data;
            if (data == null) {
                data = new GenericData();
                content.data = data;
            }
            data.put("oauth_token", (Object) this.accessToken);
        }
    }

    private AccessProtectedResource() {
    }
}
