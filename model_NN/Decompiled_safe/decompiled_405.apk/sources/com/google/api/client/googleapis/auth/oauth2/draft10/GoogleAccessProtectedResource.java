package com.google.api.client.googleapis.auth.oauth2.draft10;

import com.google.api.client.auth.oauth2.draft10.AccessProtectedResource;
import com.google.api.client.googleapis.auth.oauth2.draft10.GoogleAccessTokenRequest;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import java.io.IOException;

public class GoogleAccessProtectedResource extends AccessProtectedResource {
    public GoogleAccessProtectedResource(String accessToken) {
        super(accessToken, AccessProtectedResource.Method.AUTHORIZATION_HEADER);
    }

    public GoogleAccessProtectedResource(String accessToken, HttpTransport transport, JsonFactory jsonFactory, String clientId, String clientSecret, String refreshToken) {
        super(accessToken, AccessProtectedResource.Method.AUTHORIZATION_HEADER, transport, jsonFactory, GoogleAccessTokenRequest.AUTHORIZATION_SERVER_URL, clientId, clientSecret, refreshToken);
    }

    /* access modifiers changed from: protected */
    public boolean executeRefreshToken() throws IOException {
        if (getRefreshToken() == null) {
            return false;
        }
        setAccessToken(new GoogleAccessTokenRequest.GoogleRefreshTokenGrant(getTransport(), getJsonFactory(), getClientId(), getClientSecret(), getRefreshToken()).execute().accessToken);
        return true;
    }
}
