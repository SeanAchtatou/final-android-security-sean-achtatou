package com.google.api.client.util;

import com.google.common.base.Preconditions;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.WeakHashMap;

public final class ClassInfo {
    private static final Map<Class<?>, ClassInfo> CACHE = new WeakHashMap();
    @Deprecated
    public final Class<?> clazz;
    private final IdentityHashMap<String, FieldInfo> nameToFieldInfoMap = new IdentityHashMap<>();
    final List<String> names;

    public static ClassInfo of(Class<?> underlyingClass) {
        ClassInfo classInfo;
        if (underlyingClass == null) {
            return null;
        }
        synchronized (CACHE) {
            classInfo = CACHE.get(underlyingClass);
            if (classInfo == null) {
                classInfo = new ClassInfo(underlyingClass);
                CACHE.put(underlyingClass, classInfo);
            }
        }
        return classInfo;
    }

    public Class<?> getUnderlyingClass() {
        return this.clazz;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public FieldInfo getFieldInfo(String name) {
        return this.nameToFieldInfoMap.get(name == null ? null : name.intern());
    }

    public Field getField(String name) {
        FieldInfo fieldInfo = getFieldInfo(name);
        if (fieldInfo == null) {
            return null;
        }
        return fieldInfo.getField();
    }

    public boolean isEnum() {
        return this.clazz.isEnum();
    }

    @Deprecated
    public int getKeyCount() {
        return this.nameToFieldInfoMap.size();
    }

    @Deprecated
    public Collection<String> getKeyNames() {
        return this.names;
    }

    public Collection<String> getNames() {
        return this.names;
    }

    @Deprecated
    public static <T> T newInstance(Class<T> clazz2) {
        try {
            return clazz2.newInstance();
        } catch (IllegalAccessException e) {
            throw handleExceptionForNewInstance(e, clazz2);
        } catch (InstantiationException e2) {
            throw handleExceptionForNewInstance(e2, clazz2);
        }
    }

    private static IllegalArgumentException handleExceptionForNewInstance(Exception e, Class<?> clazz2) {
        StringBuilder buf = new StringBuilder("unable to create new instance of class ").append(clazz2.getName());
        if (Modifier.isAbstract(clazz2.getModifiers())) {
            buf.append(" (and) because it is abstract");
        }
        if (clazz2.getEnclosingClass() != null && !Modifier.isStatic(clazz2.getModifiers())) {
            buf.append(" (and) because it is not static");
        }
        if (!Modifier.isPublic(clazz2.getModifiers())) {
            buf.append(" (and) because it is not public");
        } else {
            try {
                clazz2.getConstructor(new Class[0]);
            } catch (NoSuchMethodException e2) {
                buf.append(" (and) because it has no public default constructor");
            }
        }
        throw new IllegalArgumentException(buf.toString(), e);
    }

    @Deprecated
    public static Collection<Object> newCollectionInstance(Class<?> collectionClass) {
        if (collectionClass == null || collectionClass.isAssignableFrom(ArrayList.class)) {
            return new ArrayList();
        }
        if ((collectionClass.getModifiers() & 1536) == 0) {
            return (Collection) newInstance(collectionClass);
        }
        if (collectionClass.isAssignableFrom(HashSet.class)) {
            return new HashSet();
        }
        if (collectionClass.isAssignableFrom(TreeSet.class)) {
            return new TreeSet();
        }
        throw new IllegalArgumentException("no default collection class defined for class: " + collectionClass.getName());
    }

    @Deprecated
    public static Map<String, Object> newMapInstance(Class<?> mapClass) {
        if (mapClass != null && (mapClass.getModifiers() & 1536) == 0) {
            return (Map) newInstance(mapClass);
        }
        if (mapClass == null || mapClass.isAssignableFrom(ArrayMap.class)) {
            return ArrayMap.create();
        }
        if (mapClass.isAssignableFrom(TreeMap.class)) {
            return new TreeMap();
        }
        throw new IllegalArgumentException("no default map class defined for class: " + mapClass.getName());
    }

    @Deprecated
    public static Class<?> getCollectionParameter(Field field) {
        if (field != null) {
            Type genericType = field.getGenericType();
            if (genericType instanceof ParameterizedType) {
                Type[] typeArgs = ((ParameterizedType) genericType).getActualTypeArguments();
                if (typeArgs.length == 1 && (typeArgs[0] instanceof Class)) {
                    return (Class) typeArgs[0];
                }
            }
        }
        return null;
    }

    @Deprecated
    public static Class<?> getMapValueParameter(Field field) {
        if (field != null) {
            return getMapValueParameter(field.getGenericType());
        }
        return null;
    }

    @Deprecated
    public static Class<?> getMapValueParameter(Type genericType) {
        if (genericType instanceof ParameterizedType) {
            Type[] typeArgs = ((ParameterizedType) genericType).getActualTypeArguments();
            if (typeArgs.length == 2 && (typeArgs[1] instanceof Class)) {
                return (Class) typeArgs[1];
            }
        }
        return null;
    }

    private ClassInfo(Class<?> srcClass) {
        this.clazz = srcClass;
        TreeSet<String> nameSet = new TreeSet<>(new Comparator<String>() {
            public int compare(String s0, String s1) {
                if (s0 == s1) {
                    return 0;
                }
                if (s0 == null) {
                    return -1;
                }
                if (s1 == null) {
                    return 1;
                }
                return s0.compareTo(s1);
            }
        });
        Class<?> superClass = srcClass.getSuperclass();
        if (superClass != null) {
            ClassInfo superClassInfo = of(superClass);
            this.nameToFieldInfoMap.putAll(superClassInfo.nameToFieldInfoMap);
            nameSet.addAll(superClassInfo.names);
        }
        for (Field field : srcClass.getDeclaredFields()) {
            FieldInfo fieldInfo = FieldInfo.of(field);
            if (fieldInfo != null) {
                String fieldName = fieldInfo.getName();
                FieldInfo conflictingFieldInfo = this.nameToFieldInfoMap.get(fieldName);
                boolean z = conflictingFieldInfo == null;
                Object[] objArr = new Object[3];
                objArr[0] = fieldName;
                objArr[1] = field;
                objArr[2] = conflictingFieldInfo == null ? null : conflictingFieldInfo.getField();
                Preconditions.checkArgument(z, "two fields have the same name <%s>: %s and %s", objArr);
                this.nameToFieldInfoMap.put(fieldName, fieldInfo);
                nameSet.add(fieldName);
            }
        }
        this.names = nameSet.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(new ArrayList(nameSet));
    }
}
