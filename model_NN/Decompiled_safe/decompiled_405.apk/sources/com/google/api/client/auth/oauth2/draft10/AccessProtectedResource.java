package com.google.api.client.auth.oauth2.draft10;

import com.google.api.client.auth.oauth2.draft10.AccessTokenRequest;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpMethod;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.HttpUnsuccessfulResponseHandler;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.GenericData;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.util.EnumSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public class AccessProtectedResource implements HttpExecuteInterceptor, HttpRequestInitializer, HttpUnsuccessfulResponseHandler {
    private static final EnumSet<HttpMethod> ALLOWED_METHODS = EnumSet.of(HttpMethod.POST, HttpMethod.PUT, HttpMethod.DELETE);
    static final String HEADER_PREFIX = "OAuth ";
    static final Logger LOGGER = Logger.getLogger(AccessProtectedResource.class.getName());
    private String accessToken;
    private final String authorizationServerUrl;
    private final String clientId;
    private final String clientSecret;
    private final JsonFactory jsonFactory;
    private final Method method;
    private final String refreshToken;
    private final Lock tokenLock = new ReentrantLock();
    private final HttpTransport transport;

    public enum Method {
        AUTHORIZATION_HEADER,
        QUERY_PARAMETER,
        FORM_ENCODED_BODY
    }

    public AccessProtectedResource(String accessToken2, Method method2) {
        this.accessToken = accessToken2;
        this.method = (Method) Preconditions.checkNotNull(method2);
        this.transport = null;
        this.jsonFactory = null;
        this.authorizationServerUrl = null;
        this.clientId = null;
        this.clientSecret = null;
        this.refreshToken = null;
    }

    public AccessProtectedResource(String accessToken2, Method method2, HttpTransport transport2, JsonFactory jsonFactory2, String authorizationServerUrl2, String clientId2, String clientSecret2, String refreshToken2) {
        this.accessToken = accessToken2;
        this.method = (Method) Preconditions.checkNotNull(method2);
        this.transport = (HttpTransport) Preconditions.checkNotNull(transport2);
        this.jsonFactory = (JsonFactory) Preconditions.checkNotNull(jsonFactory2);
        this.authorizationServerUrl = (String) Preconditions.checkNotNull(authorizationServerUrl2);
        this.clientId = (String) Preconditions.checkNotNull(clientId2);
        this.clientSecret = (String) Preconditions.checkNotNull(clientSecret2);
        this.refreshToken = (String) Preconditions.checkNotNull(refreshToken2);
    }

    public final String getAccessToken() {
        this.tokenLock.lock();
        try {
            return this.accessToken;
        } finally {
            this.tokenLock.unlock();
        }
    }

    public final void setAccessToken(String accessToken2) {
        this.tokenLock.lock();
        try {
            this.accessToken = accessToken2;
            onAccessToken(accessToken2);
        } finally {
            this.tokenLock.unlock();
        }
    }

    public final Method getMethod() {
        return this.method;
    }

    public HttpTransport getTransport() {
        return this.transport;
    }

    public JsonFactory getJsonFactory() {
        return this.jsonFactory;
    }

    public String getAuthorizationServerUrl() {
        return this.authorizationServerUrl;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    public final boolean refreshToken() throws IOException {
        this.tokenLock.lock();
        try {
            return executeRefreshToken();
        } finally {
            this.tokenLock.unlock();
        }
    }

    public final void initialize(HttpRequest request) throws IOException {
        request.interceptor = this;
        request.unsuccessfulResponseHandler = this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.api.client.util.GenericData.put(java.lang.Object, java.lang.Object):java.lang.Object
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object */
    public void intercept(HttpRequest request) throws IOException {
        String accessToken2 = getAccessToken();
        if (accessToken2 != null) {
            switch (this.method) {
                case AUTHORIZATION_HEADER:
                    request.headers.authorization = HEADER_PREFIX + accessToken2;
                    return;
                case QUERY_PARAMETER:
                    request.url.set("oauth_token", accessToken2);
                    return;
                case FORM_ENCODED_BODY:
                    Preconditions.checkArgument(ALLOWED_METHODS.contains(request.method), "expected one of these HTTP methods: %s", new Object[]{ALLOWED_METHODS});
                    UrlEncodedContent content = (UrlEncodedContent) request.content;
                    if (content == null) {
                        content = new UrlEncodedContent();
                        request.content = content;
                    }
                    GenericData data = (GenericData) content.data;
                    if (data == null) {
                        data = new GenericData();
                        content.data = data;
                    }
                    data.put("oauth_token", (Object) accessToken2);
                    return;
                default:
                    return;
            }
        }
    }

    private String getAccessTokenFromRequest(HttpRequest request) {
        switch (this.method) {
            case AUTHORIZATION_HEADER:
                String header = request.headers.authorization;
                if (header == null || !header.startsWith(HEADER_PREFIX)) {
                    return null;
                }
                return header.substring(HEADER_PREFIX.length());
            case QUERY_PARAMETER:
                Object param = request.url.get("oauth_token");
                if (param == null) {
                    return null;
                }
                return param.toString();
            default:
                Object bodyParam = ((GenericData) ((UrlEncodedContent) request.content).data).get("oauth_token");
                if (bodyParam == null) {
                    return null;
                }
                return bodyParam.toString();
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean handleResponse(com.google.api.client.http.HttpRequest r6, com.google.api.client.http.HttpResponse r7, boolean r8) {
        /*
            r5 = this;
            r4 = 0
            int r2 = r7.statusCode
            r3 = 401(0x191, float:5.62E-43)
            if (r2 != r3) goto L_0x003b
            java.util.concurrent.locks.Lock r2 = r5.tokenLock     // Catch:{ HttpResponseException -> 0x002e }
            r2.lock()     // Catch:{ HttpResponseException -> 0x002e }
            java.lang.String r2 = r5.accessToken     // Catch:{ all -> 0x0027 }
            java.lang.String r3 = r5.getAccessTokenFromRequest(r6)     // Catch:{ all -> 0x0027 }
            boolean r2 = com.google.common.base.Objects.equal(r2, r3)     // Catch:{ all -> 0x0027 }
            if (r2 == 0) goto L_0x001e
            boolean r2 = r5.refreshToken()     // Catch:{ all -> 0x0027 }
            if (r2 == 0) goto L_0x0025
        L_0x001e:
            r2 = 1
        L_0x001f:
            java.util.concurrent.locks.Lock r3 = r5.tokenLock     // Catch:{ HttpResponseException -> 0x002e }
            r3.unlock()     // Catch:{ HttpResponseException -> 0x002e }
        L_0x0024:
            return r2
        L_0x0025:
            r2 = r4
            goto L_0x001f
        L_0x0027:
            r2 = move-exception
            java.util.concurrent.locks.Lock r3 = r5.tokenLock     // Catch:{ HttpResponseException -> 0x002e }
            r3.unlock()     // Catch:{ HttpResponseException -> 0x002e }
            throw r2     // Catch:{ HttpResponseException -> 0x002e }
        L_0x002e:
            r2 = move-exception
            r0 = r2
            java.util.logging.Logger r2 = com.google.api.client.auth.oauth2.draft10.AccessProtectedResource.LOGGER     // Catch:{ IOException -> 0x003d }
            com.google.api.client.http.HttpResponse r3 = r0.response     // Catch:{ IOException -> 0x003d }
            java.lang.String r3 = r3.parseAsString()     // Catch:{ IOException -> 0x003d }
            r2.severe(r3)     // Catch:{ IOException -> 0x003d }
        L_0x003b:
            r2 = r4
            goto L_0x0024
        L_0x003d:
            r2 = move-exception
            r1 = r2
            java.util.logging.Logger r2 = com.google.api.client.auth.oauth2.draft10.AccessProtectedResource.LOGGER
            java.lang.String r3 = r1.toString()
            r2.severe(r3)
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.api.client.auth.oauth2.draft10.AccessProtectedResource.handleResponse(com.google.api.client.http.HttpRequest, com.google.api.client.http.HttpResponse, boolean):boolean");
    }

    /* access modifiers changed from: protected */
    public boolean executeRefreshToken() throws IOException {
        if (this.refreshToken == null) {
            return false;
        }
        setAccessToken(new AccessTokenRequest.RefreshTokenGrant(this.transport, this.jsonFactory, this.authorizationServerUrl, this.clientId, this.clientSecret, this.refreshToken).execute().accessToken);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onAccessToken(String accessToken2) {
    }
}
