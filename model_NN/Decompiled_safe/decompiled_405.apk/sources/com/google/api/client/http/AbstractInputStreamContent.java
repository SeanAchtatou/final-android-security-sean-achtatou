package com.google.api.client.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class AbstractInputStreamContent implements HttpContent {
    private static final int BUFFER_SIZE = 2048;
    public String encoding;
    public String type;

    /* access modifiers changed from: protected */
    public abstract InputStream getInputStream() throws IOException;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void writeTo(OutputStream out) throws IOException {
        InputStream inputStream = getInputStream();
        long contentLength = getLength();
        if (contentLength < 0) {
            copy(inputStream, out);
            return;
        }
        byte[] buffer = new byte[BUFFER_SIZE];
        long remaining = contentLength;
        while (remaining > 0) {
            try {
                int read = inputStream.read(buffer, 0, (int) Math.min(2048L, remaining));
                if (read == -1) {
                    break;
                }
                out.write(buffer, 0, read);
                remaining -= (long) read;
            } catch (Throwable th) {
                inputStream.close();
                throw th;
            }
        }
        inputStream.close();
    }

    public String getEncoding() {
        return this.encoding;
    }

    public String getType() {
        return this.type;
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        try {
            byte[] tmp = new byte[BUFFER_SIZE];
            while (true) {
                int bytesRead = inputStream.read(tmp);
                if (bytesRead != -1) {
                    outputStream.write(tmp, 0, bytesRead);
                } else {
                    return;
                }
            }
        } finally {
            inputStream.close();
        }
    }
}
