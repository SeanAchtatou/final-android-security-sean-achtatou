package com.google.api.client.googleapis.json;

import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.json.JsonHttpParser;
import com.google.api.client.json.CustomizeJsonParser;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonParser;
import com.google.api.client.json.JsonToken;
import java.io.IOException;

public final class JsonCParser extends JsonHttpParser {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.JsonParser.parseAndClose(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T
     arg types: [java.lang.Class<T>, ?[OBJECT, ARRAY]]
     candidates:
      com.google.api.client.json.JsonParser.parseAndClose(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void
      com.google.api.client.json.JsonParser.parseAndClose(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T */
    public <T> T parse(HttpResponse response, Class<T> dataClass) throws IOException {
        return parserForResponse(this.jsonFactory, response).parseAndClose((Class) dataClass, (CustomizeJsonParser) null);
    }

    public static JsonParser parserForResponse(JsonFactory jsonFactory, HttpResponse response) throws IOException {
        String contentType = response.contentType;
        if (contentType == null || !contentType.startsWith(Json.CONTENT_TYPE)) {
            throw new IllegalArgumentException("Wrong content type: expected <application/json> but got <" + contentType + ">");
        }
        boolean failed = true;
        JsonParser parser = JsonHttpParser.parserForResponse(jsonFactory, response);
        try {
            parser.skipToKey(response.isSuccessStatusCode ? "data" : "error");
            if (parser.getCurrentToken() == JsonToken.END_OBJECT) {
                throw new IllegalArgumentException("data key not found");
            }
            failed = false;
            return parser;
        } finally {
            if (failed) {
                parser.close();
            }
        }
    }
}
