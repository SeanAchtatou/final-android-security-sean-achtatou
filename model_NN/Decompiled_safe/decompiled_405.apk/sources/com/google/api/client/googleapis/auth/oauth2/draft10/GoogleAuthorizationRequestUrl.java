package com.google.api.client.googleapis.auth.oauth2.draft10;

import com.google.api.client.auth.oauth2.draft10.AuthorizationRequestUrl;

public class GoogleAuthorizationRequestUrl extends AuthorizationRequestUrl {
    public static final String AUTHORIZATION_SERVER_URL = "https://accounts.google.com/o/oauth2/auth";

    public GoogleAuthorizationRequestUrl() {
        super(AUTHORIZATION_SERVER_URL);
    }

    public GoogleAuthorizationRequestUrl(String clientId, String redirectUri, String scope) {
        super(AUTHORIZATION_SERVER_URL, clientId);
        this.redirectUri = redirectUri;
        this.scope = scope;
    }
}
