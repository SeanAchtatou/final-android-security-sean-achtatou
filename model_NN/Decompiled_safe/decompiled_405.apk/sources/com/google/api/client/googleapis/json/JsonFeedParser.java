package com.google.api.client.googleapis.json;

import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.CustomizeJsonParser;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonParser;
import java.io.IOException;

public final class JsonFeedParser<T, I> extends AbstractJsonFeedParser<T> {
    private final Class<I> itemClass;

    public JsonFeedParser(JsonParser parser, Class<T> feedClass, Class<I> itemClass2) {
        super(parser, feedClass);
        this.itemClass = itemClass2;
    }

    public I parseNextItem() throws IOException {
        return super.parseNextItem();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.JsonParser.parse(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T
     arg types: [java.lang.Class<I>, ?[OBJECT, ARRAY]]
     candidates:
      com.google.api.client.json.JsonParser.parse(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void
      com.google.api.client.json.JsonParser.parse(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T */
    /* access modifiers changed from: package-private */
    public Object parseItemInternal() throws IOException {
        return this.parser.parse((Class) this.itemClass, (CustomizeJsonParser) null);
    }

    public static <T, I> JsonFeedParser<T, I> use(JsonFactory jsonFactory, HttpResponse response, Class<T> feedClass, Class<I> itemClass2) throws IOException {
        return new JsonFeedParser<>(JsonCParser.parserForResponse(jsonFactory, response), feedClass, itemClass2);
    }
}
