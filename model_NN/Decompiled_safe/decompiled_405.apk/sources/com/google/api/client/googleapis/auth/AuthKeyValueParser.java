package com.google.api.client.googleapis.auth;

import com.google.api.client.http.HttpParser;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.util.ClassInfo;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.GenericData;
import com.google.api.client.util.Types;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.Map;

public final class AuthKeyValueParser implements HttpParser {
    public static final AuthKeyValueParser INSTANCE = new AuthKeyValueParser();

    public String getContentType() {
        return "text/plain";
    }

    public <T> T parse(HttpResponse response, Class<T> dataClass) throws IOException {
        Object obj;
        T newInstance = Types.newInstance(dataClass);
        ClassInfo classInfo = ClassInfo.of(dataClass);
        response.disableContentLogging = true;
        InputStream content = response.getContent();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    return newInstance;
                }
                int equals = line.indexOf(61);
                String key = line.substring(0, equals);
                String value = line.substring(equals + 1);
                Field field = classInfo.getField(key);
                if (field != null) {
                    Class<?> fieldClass = field.getType();
                    if (fieldClass == Boolean.TYPE || fieldClass == Boolean.class) {
                        obj = Boolean.valueOf(value);
                    } else {
                        obj = value;
                    }
                    FieldInfo.setFieldValue(field, newInstance, obj);
                } else if (GenericData.class.isAssignableFrom(dataClass)) {
                    ((GenericData) newInstance).set(key, value);
                } else if (Map.class.isAssignableFrom(dataClass)) {
                    ((Map) newInstance).put(key, value);
                }
            }
        } finally {
            content.close();
        }
    }

    private AuthKeyValueParser() {
    }
}
