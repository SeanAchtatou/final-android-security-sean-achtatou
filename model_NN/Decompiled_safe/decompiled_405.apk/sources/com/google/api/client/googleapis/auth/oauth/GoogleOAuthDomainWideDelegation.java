package com.google.api.client.googleapis.auth.oauth;

import com.google.api.client.auth.oauth.OAuthParameters;
import com.google.api.client.googleapis.GoogleUrl;
import com.google.api.client.http.HttpExecuteIntercepter;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.util.Key;
import java.io.IOException;

public final class GoogleOAuthDomainWideDelegation implements HttpExecuteInterceptor, HttpExecuteIntercepter, HttpRequestInitializer {
    public OAuthParameters parameters;
    public String requestorId;

    public static final class Url extends GoogleUrl {
        @Key("xoauth_requestor_id")
        public String requestorId;

        public Url(String encodedUrl) {
            super(encodedUrl);
        }
    }

    public void initialize(HttpRequest request) {
        request.interceptor = this;
    }

    public void intercept(HttpRequest request) throws IOException {
        request.url.set("xoauth_requestor_id", this.requestorId);
        if (this.parameters != null) {
            this.parameters.intercept(request);
        }
    }

    @Deprecated
    public void signRequests(HttpTransport transport, OAuthParameters parameters2) {
        transport.intercepters.add(this);
        parameters2.signRequestsUsingAuthorizationHeader(transport);
    }
}
