package com.google.api.client.testing.http;

import com.google.api.client.http.HttpContent;
import com.google.api.client.http.LowLevelHttpRequest;
import com.google.api.client.http.LowLevelHttpResponse;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.io.IOException;

public class MockLowLevelHttpRequest extends LowLevelHttpRequest {
    public HttpContent content;
    public final ListMultimap<String, String> headers = ArrayListMultimap.create();
    public String url;

    public MockLowLevelHttpRequest() {
    }

    public MockLowLevelHttpRequest(String url2) {
        this.url = url2;
    }

    public void addHeader(String name, String value) {
        this.headers.put(name, value);
    }

    public LowLevelHttpResponse execute() throws IOException {
        return new MockLowLevelHttpResponse();
    }

    public void setContent(HttpContent content2) throws IOException {
        this.content = content2;
    }
}
