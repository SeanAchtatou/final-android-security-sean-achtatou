package com.google.api.client.googleapis.xml.atom;

import com.google.api.client.http.xml.XmlHttpParser;
import com.google.api.client.http.xml.atom.AtomContent;

public final class AtomPatchContent extends AtomContent {
    public AtomPatchContent() {
        this.contentType = XmlHttpParser.CONTENT_TYPE;
    }
}
