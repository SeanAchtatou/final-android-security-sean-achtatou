package com.google.api.client.http;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class InputStreamContent extends AbstractInputStreamContent {
    public InputStream inputStream;
    public long length = -1;

    @Deprecated
    public void setFileInput(File file) throws FileNotFoundException {
        this.inputStream = new FileInputStream(file);
        this.length = file.length();
    }

    @Deprecated
    public void setByteArrayInput(byte[] content) {
        this.inputStream = new ByteArrayInputStream(content);
        this.length = (long) content.length;
    }

    public long getLength() {
        return this.length;
    }

    public boolean retrySupported() {
        return false;
    }

    /* access modifiers changed from: protected */
    public InputStream getInputStream() {
        return this.inputStream;
    }

    @Deprecated
    public static void copy(InputStream inputStream2, OutputStream outputStream) throws IOException {
        AbstractInputStreamContent.copy(inputStream2, outputStream);
    }
}
