package com.google.api.client.googleapis.xml.atom;

import com.google.api.client.http.xml.AbstractXmlHttpContent;
import com.google.api.client.xml.atom.Atom;
import java.io.IOException;
import org.xmlpull.v1.XmlSerializer;

public final class AtomPatchRelativeToOriginalContent extends AbstractXmlHttpContent {
    public Object originalEntry;
    public Object patchedEntry;

    /* access modifiers changed from: protected */
    public void writeTo(XmlSerializer serializer) throws IOException {
        this.namespaceDictionary.serialize(serializer, Atom.ATOM_NAMESPACE, "entry", GoogleAtom.computePatch(this.patchedEntry, this.originalEntry));
    }
}
