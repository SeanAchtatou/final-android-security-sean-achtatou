package com.google.api.client.googleapis.auth.oauth2.draft10;

import com.google.api.client.auth.oauth2.draft10.AccessTokenRequest;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

public class GoogleAccessTokenRequest {
    public static final String AUTHORIZATION_SERVER_URL = "https://accounts.google.com/o/oauth2/token";

    public static class GoogleAuthorizationCodeGrant extends AccessTokenRequest.AuthorizationCodeGrant {
        public GoogleAuthorizationCodeGrant() {
            GoogleAccessTokenRequest.init(this);
        }

        public GoogleAuthorizationCodeGrant(HttpTransport transport, JsonFactory jsonFactory, String clientId, String clientSecret, String code, String redirectUri) {
            super(transport, jsonFactory, GoogleAccessTokenRequest.AUTHORIZATION_SERVER_URL, clientId, clientSecret, code, redirectUri);
            GoogleAccessTokenRequest.init(this);
        }
    }

    public static class GoogleRefreshTokenGrant extends AccessTokenRequest.RefreshTokenGrant {
        public GoogleRefreshTokenGrant() {
            GoogleAccessTokenRequest.init(this);
        }

        public GoogleRefreshTokenGrant(HttpTransport transport, JsonFactory jsonFactory, String clientId, String clientSecret, String refreshToken) {
            super(transport, jsonFactory, GoogleAccessTokenRequest.AUTHORIZATION_SERVER_URL, clientId, clientSecret, refreshToken);
            GoogleAccessTokenRequest.init(this);
        }
    }

    public static class GoogleAssertionGrant extends AccessTokenRequest.AssertionGrant {
        public GoogleAssertionGrant() {
            GoogleAccessTokenRequest.init(this);
        }

        public GoogleAssertionGrant(HttpTransport transport, JsonFactory jsonFactory, String clientSecret, String assertionType, String assertion) {
            super(transport, jsonFactory, GoogleAccessTokenRequest.AUTHORIZATION_SERVER_URL, clientSecret, assertionType, assertion);
            GoogleAccessTokenRequest.init(this);
        }
    }

    static void init(AccessTokenRequest request) {
        request.authorizationServerUrl = AUTHORIZATION_SERVER_URL;
    }

    private GoogleAccessTokenRequest() {
    }
}
