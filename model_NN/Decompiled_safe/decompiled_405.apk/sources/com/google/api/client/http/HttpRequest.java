package com.google.api.client.http;

import com.google.api.client.util.Data;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.Strings;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class HttpRequest {
    public static final String USER_AGENT_SUFFIX = "Google-API-Java-Client/1.4.1-beta";
    public int connectTimeout = 20000;
    public HttpContent content;
    private final Map<String, HttpParser> contentTypeToParserMap;
    public boolean disableContentLogging;
    public boolean enableGZipContent;
    public HttpHeaders headers;
    public HttpExecuteInterceptor interceptor;
    public HttpMethod method;
    public int numRetries = 10;
    public int readTimeout = 20000;
    public HttpHeaders responseHeaders;
    public final HttpTransport transport;
    public HttpUnsuccessfulResponseHandler unsuccessfulResponseHandler;
    public GenericUrl url;

    HttpRequest(HttpTransport transport2, HttpMethod method2) {
        this.transport = transport2;
        this.headers = transport2.defaultHeaders.clone();
        this.responseHeaders = transport2.defaultHeaders.clone();
        this.contentTypeToParserMap = transport2.contentTypeToParserMap.clone();
        this.method = method2;
    }

    @Deprecated
    public void setUrl(String encodedUrl) {
        this.url = new GenericUrl(encodedUrl);
    }

    public void addParser(HttpParser parser) {
        this.contentTypeToParserMap.put(normalizeMediaType(parser.getContentType()), parser);
    }

    public final HttpParser getParser(String contentType) {
        return this.contentTypeToParserMap.get(normalizeMediaType(contentType));
    }

    /* JADX WARNING: Removed duplicated region for block: B:86:0x0358  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0367 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.api.client.http.HttpResponse execute() throws java.io.IOException {
        /*
            r34 = this;
            r23 = 0
            r26 = 0
            r0 = r34
            int r0 = r0.numRetries
            r30 = r0
            if (r30 < 0) goto L_0x0068
            r30 = 1
        L_0x000e:
            com.google.common.base.Preconditions.checkArgument(r30)
            r0 = r34
            int r0 = r0.numRetries
            r25 = r0
            r24 = 0
            r0 = r34
            com.google.api.client.http.HttpMethod r0 = r0.method
            r30 = r0
            com.google.common.base.Preconditions.checkNotNull(r30)
            r0 = r34
            com.google.api.client.http.GenericUrl r0 = r0.url
            r30 = r0
            com.google.common.base.Preconditions.checkNotNull(r30)
        L_0x002b:
            if (r24 == 0) goto L_0x0030
            r24.ignore()
        L_0x0030:
            r0 = r34
            com.google.api.client.http.HttpExecuteInterceptor r0 = r0.interceptor
            r30 = r0
            if (r30 == 0) goto L_0x0045
            r0 = r34
            com.google.api.client.http.HttpExecuteInterceptor r0 = r0.interceptor
            r30 = r0
            r0 = r30
            r1 = r34
            r0.intercept(r1)
        L_0x0045:
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            r0 = r30
            java.util.List<com.google.api.client.http.HttpExecuteIntercepter> r0 = r0.intercepters
            r30 = r0
            java.util.Iterator r13 = r30.iterator()
        L_0x0055:
            boolean r30 = r13.hasNext()
            if (r30 == 0) goto L_0x006b
            java.lang.Object r15 = r13.next()
            com.google.api.client.http.HttpExecuteIntercepter r15 = (com.google.api.client.http.HttpExecuteIntercepter) r15
            r0 = r15
            r1 = r34
            r0.intercept(r1)
            goto L_0x0055
        L_0x0068:
            r30 = 0
            goto L_0x000e
        L_0x006b:
            r0 = r34
            com.google.api.client.http.GenericUrl r0 = r0.url
            r30 = r0
            java.lang.String r27 = r30.build()
            int[] r30 = com.google.api.client.http.HttpRequest.AnonymousClass1.$SwitchMap$com$google$api$client$http$HttpMethod
            r0 = r34
            com.google.api.client.http.HttpMethod r0 = r0.method
            r31 = r0
            int r31 = r31.ordinal()
            r30 = r30[r31]
            switch(r30) {
                case 1: goto L_0x016e;
                case 2: goto L_0x017e;
                case 3: goto L_0x019d;
                case 4: goto L_0x01bc;
                case 5: goto L_0x01cc;
                default: goto L_0x0086;
            }
        L_0x0086:
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            r0 = r30
            r1 = r27
            com.google.api.client.http.LowLevelHttpRequest r19 = r0.buildGetRequest(r1)
        L_0x0094:
            java.util.logging.Logger r18 = com.google.api.client.http.HttpTransport.LOGGER
            java.util.logging.Level r30 = java.util.logging.Level.CONFIG
            r0 = r18
            r1 = r30
            boolean r17 = r0.isLoggable(r1)
            r16 = 0
            if (r17 == 0) goto L_0x00d9
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            r16.<init>()
            java.lang.String r30 = "-------------- REQUEST  --------------"
            r0 = r16
            r1 = r30
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r31 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r30.append(r31)
            r0 = r34
            com.google.api.client.http.HttpMethod r0 = r0.method
            r30 = r0
            r0 = r16
            r1 = r30
            java.lang.StringBuilder r30 = r0.append(r1)
            r31 = 32
            java.lang.StringBuilder r30 = r30.append(r31)
            r0 = r30
            r1 = r27
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r31 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r30.append(r31)
        L_0x00d9:
            r0 = r34
            com.google.api.client.http.HttpHeaders r0 = r0.headers
            r30 = r0
            r0 = r30
            java.lang.String r0 = r0.userAgent
            r30 = r0
            if (r30 != 0) goto L_0x01dc
            r0 = r34
            com.google.api.client.http.HttpHeaders r0 = r0.headers
            r30 = r0
            java.lang.String r31 = "Google-API-Java-Client/1.4.1-beta"
            r0 = r31
            r1 = r30
            r1.userAgent = r0
        L_0x00f5:
            java.util.HashSet r12 = new java.util.HashSet
            r12.<init>()
            r0 = r34
            com.google.api.client.http.HttpHeaders r0 = r0.headers
            r30 = r0
            java.util.Set r30 = r30.entrySet()
            java.util.Iterator r13 = r30.iterator()
        L_0x0108:
            boolean r30 = r13.hasNext()
            if (r30 == 0) goto L_0x021a
            java.lang.Object r11 = r13.next()
            java.util.Map$Entry r11 = (java.util.Map.Entry) r11
            java.lang.Object r21 = r11.getKey()
            java.lang.String r21 = (java.lang.String) r21
            java.lang.String r20 = r21.toLowerCase()
            r0 = r12
            r1 = r20
            boolean r30 = r0.add(r1)
            java.lang.String r31 = "multiple headers of the same name (headers are case insensitive): %s"
            r32 = 1
            r0 = r32
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r32 = r0
            r33 = 0
            r32[r33] = r20
            com.google.common.base.Preconditions.checkArgument(r30, r31, r32)
            java.lang.Object r28 = r11.getValue()
            if (r28 == 0) goto L_0x0108
            java.lang.Class r29 = r28.getClass()
            r0 = r28
            boolean r0 = r0 instanceof java.lang.Iterable
            r30 = r0
            if (r30 != 0) goto L_0x014e
            boolean r30 = r29.isArray()
            if (r30 == 0) goto L_0x020b
        L_0x014e:
            java.lang.Iterable r30 = com.google.api.client.util.Types.iterableOf(r28)
            java.util.Iterator r14 = r30.iterator()
        L_0x0156:
            boolean r30 = r14.hasNext()
            if (r30 == 0) goto L_0x0108
            java.lang.Object r22 = r14.next()
            r0 = r18
            r1 = r16
            r2 = r19
            r3 = r21
            r4 = r22
            addHeader(r0, r1, r2, r3, r4)
            goto L_0x0156
        L_0x016e:
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            r0 = r30
            r1 = r27
            com.google.api.client.http.LowLevelHttpRequest r19 = r0.buildDeleteRequest(r1)
            goto L_0x0094
        L_0x017e:
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            boolean r30 = r30.supportsHead()
            java.lang.String r31 = "HTTP transport doesn't support HEAD"
            com.google.common.base.Preconditions.checkArgument(r30, r31)
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            r0 = r30
            r1 = r27
            com.google.api.client.http.LowLevelHttpRequest r19 = r0.buildHeadRequest(r1)
            goto L_0x0094
        L_0x019d:
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            boolean r30 = r30.supportsPatch()
            java.lang.String r31 = "HTTP transport doesn't support PATCH"
            com.google.common.base.Preconditions.checkArgument(r30, r31)
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            r0 = r30
            r1 = r27
            com.google.api.client.http.LowLevelHttpRequest r19 = r0.buildPatchRequest(r1)
            goto L_0x0094
        L_0x01bc:
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            r0 = r30
            r1 = r27
            com.google.api.client.http.LowLevelHttpRequest r19 = r0.buildPostRequest(r1)
            goto L_0x0094
        L_0x01cc:
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r30 = r0
            r0 = r30
            r1 = r27
            com.google.api.client.http.LowLevelHttpRequest r19 = r0.buildPutRequest(r1)
            goto L_0x0094
        L_0x01dc:
            java.lang.StringBuilder r30 = new java.lang.StringBuilder
            r30.<init>()
            r0 = r34
            com.google.api.client.http.HttpHeaders r0 = r0.headers
            r31 = r0
            r0 = r31
            java.lang.String r0 = r0.userAgent
            r32 = r0
            r0 = r30
            r1 = r32
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r32 = " Google-API-Java-Client/1.4.1-beta"
            r0 = r30
            r1 = r32
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r30 = r30.toString()
            r0 = r30
            r1 = r31
            r1.userAgent = r0
            goto L_0x00f5
        L_0x020b:
            r0 = r18
            r1 = r16
            r2 = r19
            r3 = r21
            r4 = r28
            addHeader(r0, r1, r2, r3, r4)
            goto L_0x0108
        L_0x021a:
            r0 = r34
            com.google.api.client.http.HttpContent r0 = r0.content
            r6 = r0
            if (r6 == 0) goto L_0x036b
            java.lang.String r8 = r6.getEncoding()
            long r9 = r6.getLength()
            java.lang.String r7 = r6.getType()
            r30 = 0
            int r30 = (r9 > r30 ? 1 : (r9 == r30 ? 0 : -1))
            if (r30 == 0) goto L_0x0368
            if (r8 != 0) goto L_0x0368
            boolean r30 = com.google.api.client.http.LogContent.isTextBasedContentType(r7)
            if (r30 == 0) goto L_0x0368
            if (r17 == 0) goto L_0x0245
            r0 = r34
            boolean r0 = r0.disableContentLogging
            r30 = r0
            if (r30 == 0) goto L_0x0251
        L_0x0245:
            java.util.logging.Level r30 = java.util.logging.Level.ALL
            r0 = r18
            r1 = r30
            boolean r30 = r0.isLoggable(r1)
            if (r30 == 0) goto L_0x0368
        L_0x0251:
            com.google.api.client.http.LogContent r5 = new com.google.api.client.http.LogContent
            r5.<init>(r6, r7, r8, r9)
        L_0x0256:
            r0 = r34
            boolean r0 = r0.enableGZipContent
            r30 = r0
            if (r30 == 0) goto L_0x026c
            com.google.api.client.http.GZipContent r6 = new com.google.api.client.http.GZipContent
            r6.<init>(r5, r7)
            java.lang.String r8 = r6.getEncoding()
            long r9 = r6.getLength()
            r5 = r6
        L_0x026c:
            if (r17 == 0) goto L_0x02e1
            if (r7 == 0) goto L_0x0293
            java.lang.StringBuilder r30 = new java.lang.StringBuilder
            r30.<init>()
            java.lang.String r31 = "Content-Type: "
            java.lang.StringBuilder r30 = r30.append(r31)
            r0 = r30
            r1 = r7
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r30 = r30.toString()
            r0 = r16
            r1 = r30
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r31 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r30.append(r31)
        L_0x0293:
            if (r8 == 0) goto L_0x02b8
            java.lang.StringBuilder r30 = new java.lang.StringBuilder
            r30.<init>()
            java.lang.String r31 = "Content-Encoding: "
            java.lang.StringBuilder r30 = r30.append(r31)
            r0 = r30
            r1 = r8
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r30 = r30.toString()
            r0 = r16
            r1 = r30
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r31 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r30.append(r31)
        L_0x02b8:
            r30 = 0
            int r30 = (r9 > r30 ? 1 : (r9 == r30 ? 0 : -1))
            if (r30 < 0) goto L_0x02e1
            java.lang.StringBuilder r30 = new java.lang.StringBuilder
            r30.<init>()
            java.lang.String r31 = "Content-Length: "
            java.lang.StringBuilder r30 = r30.append(r31)
            r0 = r30
            r1 = r9
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r30 = r30.toString()
            r0 = r16
            r1 = r30
            java.lang.StringBuilder r30 = r0.append(r1)
            java.lang.String r31 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r30.append(r31)
        L_0x02e1:
            r0 = r19
            r1 = r5
            r0.setContent(r1)
        L_0x02e7:
            if (r17 == 0) goto L_0x02f4
            java.lang.String r30 = r16.toString()
            r0 = r18
            r1 = r30
            r0.config(r1)
        L_0x02f4:
            r0 = r34
            int r0 = r0.connectTimeout
            r30 = r0
            r0 = r34
            int r0 = r0.readTimeout
            r31 = r0
            r0 = r19
            r1 = r30
            r2 = r31
            r0.setTimeout(r1, r2)
            com.google.api.client.http.HttpResponse r24 = new com.google.api.client.http.HttpResponse
            com.google.api.client.http.LowLevelHttpResponse r30 = r19.execute()
            r0 = r24
            r1 = r34
            r2 = r30
            r0.<init>(r1, r2)
            if (r25 <= 0) goto L_0x0362
            if (r5 == 0) goto L_0x0322
            boolean r30 = r5.retrySupported()
            if (r30 == 0) goto L_0x0362
        L_0x0322:
            r30 = 1
            r26 = r30
        L_0x0326:
            r23 = 0
            r0 = r24
            boolean r0 = r0.isSuccessStatusCode
            r30 = r0
            if (r30 != 0) goto L_0x034a
            r0 = r34
            com.google.api.client.http.HttpUnsuccessfulResponseHandler r0 = r0.unsuccessfulResponseHandler
            r30 = r0
            if (r30 == 0) goto L_0x034a
            r0 = r34
            com.google.api.client.http.HttpUnsuccessfulResponseHandler r0 = r0.unsuccessfulResponseHandler
            r30 = r0
            r0 = r30
            r1 = r34
            r2 = r24
            r3 = r26
            boolean r23 = r0.handleResponse(r1, r2, r3)
        L_0x034a:
            int r25 = r25 + -1
            if (r23 == 0) goto L_0x0350
            if (r26 != 0) goto L_0x002b
        L_0x0350:
            r0 = r24
            boolean r0 = r0.isSuccessStatusCode
            r30 = r0
            if (r30 != 0) goto L_0x0367
            com.google.api.client.http.HttpResponseException r30 = new com.google.api.client.http.HttpResponseException
            r0 = r30
            r1 = r24
            r0.<init>(r1)
            throw r30
        L_0x0362:
            r30 = 0
            r26 = r30
            goto L_0x0326
        L_0x0367:
            return r24
        L_0x0368:
            r5 = r6
            goto L_0x0256
        L_0x036b:
            r5 = r6
            goto L_0x02e7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.api.client.http.HttpRequest.execute():com.google.api.client.http.HttpResponse");
    }

    private static void addHeader(Logger logger, StringBuilder logbuf, LowLevelHttpRequest lowLevelHttpRequest, String name, Object value) {
        if (value != null && !Data.isNull(value)) {
            String stringValue = value instanceof Enum ? FieldInfo.of((Enum) value).getName() : value.toString();
            if (logbuf != null) {
                logbuf.append(name).append(": ");
                if (!"Authorization".equals(name) || logger.isLoggable(Level.ALL)) {
                    logbuf.append(stringValue);
                } else {
                    logbuf.append("<Not Logged>");
                }
                logbuf.append(Strings.LINE_SEPARATOR);
            }
            lowLevelHttpRequest.addHeader(name, stringValue);
        }
    }

    public static String normalizeMediaType(String mediaType) {
        if (mediaType == null) {
            return null;
        }
        int semicolon = mediaType.indexOf(59);
        return semicolon == -1 ? mediaType : mediaType.substring(0, semicolon);
    }
}
