package com.google.api.client.googleapis.json;

import com.google.api.client.json.CustomizeJsonParser;
import com.google.api.client.json.JsonParser;
import com.google.api.client.json.JsonToken;
import java.io.IOException;

public abstract class AbstractJsonFeedParser<T> {
    final Class<T> feedClass;
    private boolean feedParsed;
    final JsonParser parser;

    /* access modifiers changed from: package-private */
    public abstract Object parseItemInternal() throws IOException;

    AbstractJsonFeedParser(JsonParser parser2, Class<T> feedClass2) {
        this.parser = parser2;
        this.feedClass = feedClass2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.JsonParser.parse(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T
     arg types: [java.lang.Class<T>, com.google.api.client.googleapis.json.AbstractJsonFeedParser$StopAtItems]
     candidates:
      com.google.api.client.json.JsonParser.parse(java.lang.Object, com.google.api.client.json.CustomizeJsonParser):void
      com.google.api.client.json.JsonParser.parse(java.lang.Class, com.google.api.client.json.CustomizeJsonParser):T */
    public T parseFeed() throws IOException {
        boolean close = true;
        try {
            this.feedParsed = true;
            close = false;
            return this.parser.parse((Class) this.feedClass, (CustomizeJsonParser) new StopAtItems());
        } finally {
            if (close) {
                close();
            }
        }
    }

    final class StopAtItems extends CustomizeJsonParser {
        StopAtItems() {
        }

        public boolean stopAt(Object context, String key) {
            return "items".equals(key) && context.getClass().equals(AbstractJsonFeedParser.this.feedClass);
        }
    }

    public Object parseNextItem() throws IOException {
        JsonParser parser2 = this.parser;
        if (!this.feedParsed) {
            this.feedParsed = true;
            parser2.skipToKey("items");
        }
        boolean close = true;
        try {
            if (parser2.nextToken() == JsonToken.START_OBJECT) {
                close = false;
                return parseItemInternal();
            }
            if (close) {
                close();
            }
            return null;
        } finally {
            if (close) {
                close();
            }
        }
    }

    public void close() throws IOException {
        this.parser.close();
    }
}
