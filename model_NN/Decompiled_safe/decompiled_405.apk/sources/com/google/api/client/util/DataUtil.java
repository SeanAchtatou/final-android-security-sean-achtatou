package com.google.api.client.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@Deprecated
public class DataUtil {
    @Deprecated
    public static Map<String, Object> mapOf(Object data) {
        if (data == null) {
            return Collections.emptyMap();
        }
        if (data instanceof Map) {
            return (Map) data;
        }
        return new ReflectionMap(data);
    }

    @Deprecated
    public static <T> T clone(T data) {
        T newInstance;
        if (FieldInfo.isPrimitive((Object) data)) {
            return data;
        }
        if (data instanceof GenericData) {
            newInstance = ((GenericData) data).clone();
        } else if (data instanceof ArrayMap) {
            newInstance = ((ArrayMap) data).clone();
        } else {
            newInstance = ClassInfo.newInstance(data.getClass());
        }
        cloneInternal(data, newInstance);
        return newInstance;
    }

    /* JADX INFO: Multiple debug info for r1v1 com.google.api.client.util.ClassInfo: [D('classInfo' com.google.api.client.util.ClassInfo), D('srcClass' java.lang.Class<?>)] */
    /* JADX INFO: Multiple debug info for r2v7 com.google.api.client.util.FieldInfo: [D('fieldInfo' com.google.api.client.util.FieldInfo), D('fieldName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v1 java.util.Map: [D('dest' java.lang.Object), D('destMap' java.util.Map<java.lang.String, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r6v1 java.util.Map: [D('src' java.lang.Object), D('srcMap' java.util.Map<java.lang.String, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r6v3 java.util.Iterator: [D('i$' java.util.Iterator), D('srcMap' java.util.Map<java.lang.String, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r7v2 com.google.api.client.util.ArrayMap: [D('dest' java.lang.Object), D('destMap' com.google.api.client.util.ArrayMap<java.lang.Object, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r7v3 java.util.Iterator: [D('dest' java.lang.Object), D('i$' java.util.Iterator)] */
    static void cloneInternal(Object src, Object dest) {
        Object srcValue;
        Class<?> srcClass = src.getClass();
        if (Collection.class.isAssignableFrom(srcClass)) {
            Collection<Object> collection = (Collection) src;
            if (ArrayList.class.isAssignableFrom(srcClass)) {
                ((ArrayList) dest).ensureCapacity(collection.size());
            }
            Collection collection2 = (Collection) dest;
            for (Object srcValue2 : collection) {
                collection2.add(clone(srcValue2));
            }
            return;
        }
        boolean isGenericData = GenericData.class.isAssignableFrom(srcClass);
        if (isGenericData || !Map.class.isAssignableFrom(srcClass)) {
            ClassInfo classInfo = ClassInfo.of(srcClass);
            for (String fieldName : classInfo.getKeyNames()) {
                FieldInfo fieldInfo = classInfo.getFieldInfo(fieldName);
                if (!fieldInfo.isFinal() && ((!isGenericData || !fieldInfo.isPrimitive) && (srcValue = fieldInfo.getValue(src)) != null)) {
                    fieldInfo.setValue(dest, clone(srcValue));
                }
            }
        } else if (ArrayMap.class.isAssignableFrom(srcClass)) {
            ArrayMap<Object, Object> destMap = (ArrayMap) dest;
            ArrayMap arrayMap = (ArrayMap) src;
            int size = arrayMap.size();
            for (int i = 0; i < size; i++) {
                Object srcValue3 = arrayMap.getValue(i);
                if (!FieldInfo.isPrimitive(srcValue3)) {
                    destMap.set(i, clone(srcValue3));
                }
            }
        } else {
            Map<String, Object> destMap2 = (Map) dest;
            for (Map.Entry<String, Object> srcEntry : ((Map) src).entrySet()) {
                destMap2.put(srcEntry.getKey(), clone(srcEntry.getValue()));
            }
        }
    }

    private DataUtil() {
    }
}
