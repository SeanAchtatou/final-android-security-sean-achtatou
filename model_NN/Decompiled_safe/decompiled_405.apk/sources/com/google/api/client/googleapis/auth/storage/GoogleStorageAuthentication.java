package com.google.api.client.googleapis.auth.storage;

import com.google.api.client.auth.HmacSha;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpExecuteIntercepter;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

public final class GoogleStorageAuthentication implements HttpExecuteInterceptor, HttpRequestInitializer {
    private static final String HOST = "commondatastorage.googleapis.com";
    static final Pattern WHITESPACE_PATTERN = Pattern.compile("\\s+");
    private final String accessKey;
    private final String secret;

    public GoogleStorageAuthentication(String accessKey2, String secret2) {
        this.accessKey = accessKey2;
        this.secret = secret2;
    }

    @Deprecated
    public static void authorize(HttpTransport transport, String accessKey2, String secret2) {
        transport.removeIntercepters(Intercepter.class);
        Intercepter intercepter = new Intercepter();
        intercepter.accessKey = accessKey2;
        intercepter.secret = secret2;
        transport.intercepters.add(intercepter);
    }

    public void initialize(HttpRequest request) {
        request.interceptor = this;
    }

    public void intercept(HttpRequest request) throws IOException {
        interceptInternal(request, this.accessKey, this.secret);
    }

    @Deprecated
    static class Intercepter implements HttpExecuteIntercepter {
        String accessKey;
        String secret;

        Intercepter() {
        }

        public void intercept(HttpRequest request) throws IOException {
            GoogleStorageAuthentication.interceptInternal(request, this.accessKey, this.secret);
        }
    }

    /* JADX INFO: Multiple debug info for r1v4 com.google.api.client.http.HttpContent: [D('contentMD5' java.lang.String), D('content' com.google.api.client.http.HttpContent)] */
    /* JADX INFO: Multiple debug info for r2v2 java.lang.String: [D('i$' java.util.Iterator), D('host' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v38 java.lang.String: [D('bucket' java.lang.String), D('bucketNameLength' int)] */
    /* JADX INFO: Multiple debug info for r1v46 java.lang.Object: [D('value' java.lang.Object), D('headerEntry' java.util.Map$Entry<java.lang.String, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r1v50 java.lang.StringBuilder: [D('value' java.lang.Object), D('buf' java.lang.StringBuilder)] */
    /* JADX INFO: Multiple debug info for r1v53 java.lang.String: [D('contentType' java.lang.String), D('content' com.google.api.client.http.HttpContent)] */
    static void interceptInternal(HttpRequest request, String accessKey2, String secret2) throws IOException {
        String contentType;
        HttpHeaders headers = request.headers;
        StringBuilder messageBuf = new StringBuilder();
        messageBuf.append(request.method).append(10);
        String contentMD5 = headers.contentMD5;
        if (contentMD5 != null) {
            messageBuf.append(contentMD5);
        }
        messageBuf.append(10);
        HttpContent content = request.content;
        if (!(content == null || (contentType = content.getType()) == null)) {
            messageBuf.append(contentType);
        }
        messageBuf.append(10);
        if (headers.date != null) {
            messageBuf.append(headers.date);
        }
        messageBuf.append(10);
        TreeMap<String, String> extensions = new TreeMap<>();
        for (Map.Entry<String, Object> headerEntry : headers.entrySet()) {
            String name = ((String) headerEntry.getKey()).toLowerCase();
            Object value = headerEntry.getValue();
            if (value != null && name.startsWith("x-goog-")) {
                if (value instanceof Collection) {
                    StringBuilder buf = new StringBuilder();
                    boolean first = true;
                    for (Object repeatedValue : (Collection) value) {
                        if (first) {
                            first = false;
                        } else {
                            buf.append(',');
                        }
                        buf.append(WHITESPACE_PATTERN.matcher(repeatedValue.toString()).replaceAll(" "));
                    }
                    extensions.put(name, buf.toString());
                } else {
                    extensions.put(name, WHITESPACE_PATTERN.matcher(value.toString()).replaceAll(" "));
                }
            }
        }
        for (Map.Entry<String, String> extensionEntry : extensions.entrySet()) {
            messageBuf.append((String) extensionEntry.getKey()).append(':').append((String) extensionEntry.getValue()).append(10);
        }
        GenericUrl url = request.url;
        String host = url.host;
        if (!host.endsWith(HOST)) {
            throw new IllegalArgumentException("missing host commondatastorage.googleapis.com");
        }
        int bucketNameLength = (host.length() - HOST.length()) - 1;
        if (bucketNameLength > 0) {
            String bucket = host.substring(0, bucketNameLength);
            if (!bucket.equals("c")) {
                messageBuf.append('/').append(bucket);
            }
        }
        if (url.pathParts != null) {
            messageBuf.append(url.getRawPath());
        }
        if (url.get("acl") != null) {
            messageBuf.append("?acl");
        } else if (url.get("location") != null) {
            messageBuf.append("?location");
        } else if (url.get("logging") != null) {
            messageBuf.append("?logging");
        } else if (url.get("requestPayment") != null) {
            messageBuf.append("?requestPayment");
        } else if (url.get("torrent") != null) {
            messageBuf.append("?torrent");
        }
        try {
            request.headers.authorization = "GOOG1 " + accessKey2 + ":" + HmacSha.sign(secret2, messageBuf.toString());
        } catch (GeneralSecurityException e) {
            IOException io = new IOException();
            io.initCause(e);
            throw io;
        }
    }
}
