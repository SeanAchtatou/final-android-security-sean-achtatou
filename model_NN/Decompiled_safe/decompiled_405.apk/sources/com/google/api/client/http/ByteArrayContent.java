package com.google.api.client.http;

import com.google.api.client.util.Strings;
import com.google.common.base.Preconditions;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public final class ByteArrayContent extends AbstractInputStreamContent {
    private static final byte[] EMPTY_ARRAY = new byte[0];
    private final byte[] byteArray;

    public ByteArrayContent(byte[] array) {
        Preconditions.checkNotNull(array);
        this.byteArray = array;
    }

    public ByteArrayContent(String contentString) {
        Preconditions.checkNotNull(contentString);
        this.byteArray = Strings.toBytesUtf8(contentString);
    }

    public ByteArrayContent() {
        this.byteArray = EMPTY_ARRAY;
    }

    public long getLength() {
        return (long) this.byteArray.length;
    }

    public boolean retrySupported() {
        return true;
    }

    /* access modifiers changed from: protected */
    public InputStream getInputStream() {
        return new ByteArrayInputStream(this.byteArray);
    }
}
