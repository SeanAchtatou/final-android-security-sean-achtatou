package com.google.api.client.auth.oauth2.draft10;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.http.json.JsonHttpParser;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.GenericData;
import com.google.api.client.util.Key;
import com.google.common.base.Preconditions;
import java.io.IOException;

public class AccessTokenRequest extends GenericData {
    public String authorizationServerUrl;
    @Key("client_id")
    public String clientId;
    public String clientSecret;
    @Key("grant_type")
    public GrantType grantType;
    public JsonFactory jsonFactory;
    @Key
    public String scope;
    public HttpTransport transport;
    public boolean useBasicAuthorization;

    public enum GrantType {
        AUTHORIZATION_CODE,
        PASSWORD,
        ASSERTION,
        REFRESH_TOKEN,
        NONE
    }

    public static class AuthorizationCodeGrant extends AccessTokenRequest {
        @Key
        public String code;
        @Key("redirect_uri")
        public String redirectUri;

        public AuthorizationCodeGrant() {
            this.grantType = GrantType.AUTHORIZATION_CODE;
        }

        public AuthorizationCodeGrant(HttpTransport transport, JsonFactory jsonFactory, String authorizationServerUrl, String clientId, String clientSecret, String code2, String redirectUri2) {
            super(transport, jsonFactory, authorizationServerUrl, clientId, clientSecret);
            this.grantType = GrantType.AUTHORIZATION_CODE;
            this.code = (String) Preconditions.checkNotNull(code2);
            this.redirectUri = (String) Preconditions.checkNotNull(redirectUri2);
        }
    }

    public static class ResourceOwnerPasswordCredentialsGrant extends AccessTokenRequest {
        public String password;
        @Key
        public String username;

        public ResourceOwnerPasswordCredentialsGrant() {
            this.grantType = GrantType.PASSWORD;
        }

        public ResourceOwnerPasswordCredentialsGrant(HttpTransport transport, JsonFactory jsonFactory, String authorizationServerUrl, String clientId, String clientSecret, String username2, String password2) {
            super(transport, jsonFactory, authorizationServerUrl, clientId, clientSecret);
            this.grantType = GrantType.PASSWORD;
            this.username = (String) Preconditions.checkNotNull(username2);
            this.password = (String) Preconditions.checkNotNull(password2);
        }
    }

    public static class AssertionGrant extends AccessTokenRequest {
        @Key
        public String assertion;
        @Key("assertion_type")
        public String assertionType;

        public AssertionGrant() {
            this.grantType = GrantType.ASSERTION;
        }

        public AssertionGrant(HttpTransport transport, JsonFactory jsonFactory, String authorizationServerUrl, String clientSecret, String assertionType2, String assertion2) {
            super(transport, jsonFactory, authorizationServerUrl, clientSecret);
            this.grantType = GrantType.ASSERTION;
            this.assertionType = (String) Preconditions.checkNotNull(assertionType2);
            this.assertion = (String) Preconditions.checkNotNull(assertion2);
        }
    }

    public static class RefreshTokenGrant extends AccessTokenRequest {
        @Key("refresh_token")
        public String refreshToken;

        public RefreshTokenGrant() {
            this.grantType = GrantType.REFRESH_TOKEN;
        }

        public RefreshTokenGrant(HttpTransport transport, JsonFactory jsonFactory, String authorizationServerUrl, String clientId, String clientSecret, String refreshToken2) {
            super(transport, jsonFactory, authorizationServerUrl, clientId, clientSecret);
            this.grantType = GrantType.REFRESH_TOKEN;
            this.refreshToken = (String) Preconditions.checkNotNull(refreshToken2);
        }
    }

    public AccessTokenRequest() {
        this.grantType = GrantType.NONE;
    }

    protected AccessTokenRequest(HttpTransport transport2, JsonFactory jsonFactory2, String authorizationServerUrl2, String clientSecret2) {
        this();
        this.transport = (HttpTransport) Preconditions.checkNotNull(transport2);
        this.jsonFactory = (JsonFactory) Preconditions.checkNotNull(jsonFactory2);
        this.authorizationServerUrl = (String) Preconditions.checkNotNull(authorizationServerUrl2);
        this.clientSecret = (String) Preconditions.checkNotNull(clientSecret2);
    }

    public AccessTokenRequest(HttpTransport transport2, JsonFactory jsonFactory2, String authorizationServerUrl2, String clientId2, String clientSecret2) {
        this(transport2, jsonFactory2, authorizationServerUrl2, clientSecret2);
        this.clientId = (String) Preconditions.checkNotNull(clientId2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.api.client.util.GenericData.put(java.lang.Object, java.lang.Object):java.lang.Object
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object */
    public final HttpResponse executeUnparsed() throws IOException {
        JsonHttpParser parser = new JsonHttpParser();
        parser.jsonFactory = this.jsonFactory;
        UrlEncodedContent content = new UrlEncodedContent();
        content.data = this;
        HttpRequest request = this.transport.createRequestFactory().buildPostRequest(new GenericUrl(this.authorizationServerUrl), content);
        request.addParser(parser);
        if (this.useBasicAuthorization) {
            request.headers.setBasicAuthentication(this.clientId, this.clientSecret);
        } else {
            put("client_secret", (Object) this.clientSecret);
        }
        return request.execute();
    }

    public final AccessTokenResponse execute() throws IOException {
        return (AccessTokenResponse) executeUnparsed().parseAs(AccessTokenResponse.class);
    }
}
