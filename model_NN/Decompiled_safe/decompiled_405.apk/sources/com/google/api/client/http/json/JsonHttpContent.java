package com.google.api.client.http.json;

import com.google.api.client.http.HttpContent;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonEncoding;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonGenerator;
import java.io.IOException;
import java.io.OutputStream;

public class JsonHttpContent implements HttpContent {
    public String contentType = Json.CONTENT_TYPE;
    public Object data;
    public JsonFactory jsonFactory;

    public long getLength() {
        return -1;
    }

    public final String getEncoding() {
        return null;
    }

    public String getType() {
        return Json.CONTENT_TYPE;
    }

    public void writeTo(OutputStream out) throws IOException {
        JsonGenerator generator = this.jsonFactory.createJsonGenerator(out, JsonEncoding.UTF8);
        generator.serialize(this.data);
        generator.flush();
    }

    public boolean retrySupported() {
        return true;
    }
}
