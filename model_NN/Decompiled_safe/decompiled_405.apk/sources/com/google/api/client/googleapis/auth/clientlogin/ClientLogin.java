package com.google.api.client.googleapis.auth.clientlogin;

import com.google.api.client.googleapis.GoogleHeaders;
import com.google.api.client.googleapis.auth.AuthKeyValueParser;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.util.Key;
import java.io.IOException;

public final class ClientLogin {
    @Key
    public String accountType;
    @Key("source")
    public String applicationName;
    @Key("service")
    public String authTokenType;
    @Key("logincaptcha")
    public String captchaAnswer;
    @Key("logintoken")
    public String captchaToken;
    @Key("Passwd")
    public String password;
    public GenericUrl serverUrl = new GenericUrl("https://www.google.com");
    public HttpTransport transport;
    @Key("Email")
    public String username;

    public static final class ErrorInfo {
        @Key("CaptchaToken")
        public String captchaToken;
        @Key("CaptchaUrl")
        public String captchaUrl;
        @Key("Error")
        public String error;
        @Key("Url")
        public String url;
    }

    public static final class Response implements HttpExecuteInterceptor, HttpRequestInitializer {
        @Key("Auth")
        public String auth;

        public String getAuthorizationHeaderValue() {
            return GoogleHeaders.getGoogleLoginValue(this.auth);
        }

        @Deprecated
        public void setAuthorizationHeader(HttpTransport googleTransport) {
            googleTransport.defaultHeaders.authorization = GoogleHeaders.getGoogleLoginValue(this.auth);
        }

        public void initialize(HttpRequest request) {
            request.interceptor = this;
        }

        public void intercept(HttpRequest request) {
            request.headers.authorization = getAuthorizationHeaderValue();
        }
    }

    public Response authenticate() throws HttpResponseException, IOException {
        GenericUrl url = this.serverUrl.clone();
        url.appendRawPath("/accounts/ClientLogin");
        UrlEncodedContent content = new UrlEncodedContent();
        content.data = this;
        HttpRequest request = this.transport.createRequestFactory().buildPostRequest(url, content);
        request.addParser(AuthKeyValueParser.INSTANCE);
        request.disableContentLogging = true;
        return (Response) request.execute().parseAs(Response.class);
    }
}
