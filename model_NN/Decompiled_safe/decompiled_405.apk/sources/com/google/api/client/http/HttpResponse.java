package com.google.api.client.http;

import com.google.api.client.util.ArrayValueMap;
import com.google.api.client.util.ClassInfo;
import com.google.api.client.util.Data;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.Strings;
import com.google.api.client.util.Types;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

public final class HttpResponse {
    private InputStream content;
    public final String contentEncoding;
    private long contentLength;
    public final String contentType;
    public boolean disableContentLogging;
    public final HttpHeaders headers;
    public final boolean isSuccessStatusCode;
    public final HttpRequest request;
    private LowLevelHttpResponse response;
    public final int statusCode;
    public final String statusMessage;
    public final HttpTransport transport;

    HttpResponse(HttpRequest request2, LowLevelHttpResponse response2) {
        this.request = request2;
        this.transport = request2.transport;
        this.headers = request2.responseHeaders;
        this.response = response2;
        this.contentLength = response2.getContentLength();
        this.contentType = response2.getContentType();
        this.contentEncoding = response2.getContentEncoding();
        int code = response2.getStatusCode();
        this.statusCode = code;
        this.isSuccessStatusCode = isSuccessStatusCode(code);
        String message = response2.getReasonPhrase();
        this.statusMessage = message;
        Logger logger = HttpTransport.LOGGER;
        boolean loggable = logger.isLoggable(Level.CONFIG);
        StringBuilder logbuf = null;
        if (loggable) {
            logbuf = new StringBuilder();
            logbuf.append("-------------- RESPONSE --------------").append(Strings.LINE_SEPARATOR);
            String statusLine = response2.getStatusLine();
            if (statusLine != null) {
                logbuf.append(statusLine);
            } else {
                logbuf.append(code);
                if (message != null) {
                    logbuf.append(' ').append(message);
                }
            }
            logbuf.append(Strings.LINE_SEPARATOR);
        }
        int size = response2.getHeaderCount();
        Class<?> cls = this.headers.getClass();
        List<Type> context = Arrays.asList(cls);
        ClassInfo classInfo = ClassInfo.of(cls);
        HashMap<String, String> fieldNameMap = HttpHeaders.getFieldNameMap(cls);
        ArrayValueMap arrayValueMap = new ArrayValueMap(this.headers);
        for (int i = 0; i < size; i++) {
            String headerName = response2.getHeaderName(i);
            String headerValue = response2.getHeaderValue(i);
            if (loggable) {
                logbuf.append(headerName + ": " + headerValue).append(Strings.LINE_SEPARATOR);
            }
            String fieldName = fieldNameMap.get(headerName);
            fieldName = fieldName == null ? headerName : fieldName;
            FieldInfo fieldInfo = classInfo.getFieldInfo(fieldName);
            if (fieldInfo != null) {
                Type type = Data.resolveWildcardTypeOrTypeVariable(context, fieldInfo.getGenericType());
                if (Types.isArray(type)) {
                    Class<?> rawArrayComponentType = Types.getRawArrayComponentType(context, Types.getArrayComponentType(type));
                    arrayValueMap.put(fieldInfo.getField(), rawArrayComponentType, parseValue(rawArrayComponentType, context, headerValue));
                } else if (Types.isAssignableToOrFrom(Types.getRawArrayComponentType(context, type), Iterable.class)) {
                    Collection<Object> collection = (Collection) fieldInfo.getValue(this.headers);
                    if (collection == null) {
                        collection = Data.newCollectionInstance(type);
                        fieldInfo.setValue(this.headers, collection);
                    }
                    collection.add(parseValue(type == Object.class ? null : Types.getIterableParameter(type), context, headerValue));
                } else {
                    fieldInfo.setValue(this.headers, parseValue(type, context, headerValue));
                }
            } else {
                ArrayList<String> listValue = (ArrayList) this.headers.get(fieldName);
                if (listValue == null) {
                    listValue = new ArrayList<>();
                    this.headers.set(fieldName, listValue);
                }
                listValue.add(headerValue);
            }
        }
        arrayValueMap.setValues();
        if (loggable) {
            logger.config(logbuf.toString());
        }
    }

    private static Object parseValue(Type valueType, List<Type> context, String value) {
        return Data.parsePrimitiveValue(Data.resolveWildcardTypeOrTypeVariable(context, valueType), value);
    }

    public InputStream getContent() throws IOException {
        if (this.response == null) {
            return this.content;
        }
        InputStream content2 = this.response.getContent();
        this.response = null;
        if (content2 != null) {
            byte[] debugContentByteArray = null;
            Logger logger = HttpTransport.LOGGER;
            boolean loggable = (!this.disableContentLogging && logger.isLoggable(Level.CONFIG)) || logger.isLoggable(Level.ALL);
            if (loggable) {
                ByteArrayOutputStream debugStream = new ByteArrayOutputStream();
                AbstractInputStreamContent.copy(content2, debugStream);
                debugContentByteArray = debugStream.toByteArray();
                content2 = new ByteArrayInputStream(debugContentByteArray);
                logger.config("Response size: " + debugContentByteArray.length + " bytes");
            }
            String contentEncoding2 = this.contentEncoding;
            if (contentEncoding2 != null && contentEncoding2.contains("gzip")) {
                InputStream content3 = new GZIPInputStream(content2);
                this.contentLength = -1;
                if (loggable) {
                    ByteArrayOutputStream debugStream2 = new ByteArrayOutputStream();
                    AbstractInputStreamContent.copy(content3, debugStream2);
                    debugContentByteArray = debugStream2.toByteArray();
                    content2 = new ByteArrayInputStream(debugContentByteArray);
                } else {
                    content2 = content3;
                }
            }
            if (loggable) {
                String contentType2 = this.contentType;
                if (debugContentByteArray.length != 0 && LogContent.isTextBasedContentType(contentType2)) {
                    logger.config(new String(debugContentByteArray));
                }
            }
            this.content = content2;
        }
        return content2;
    }

    public void ignore() throws IOException {
        InputStream content2 = getContent();
        if (content2 != null) {
            content2.close();
        }
    }

    public void disconnect() throws IOException {
        this.response.disconnect();
    }

    public HttpParser getParser() {
        return this.request.getParser(this.contentType);
    }

    public <T> T parseAs(Class<T> dataClass) throws IOException {
        HttpParser parser = getParser();
        if (parser != null) {
            return parser.parse(this, dataClass);
        }
        InputStream content2 = getContent();
        if (this.contentType != null) {
            throw new IllegalArgumentException("No parser defined for Content-Type: " + this.contentType);
        } else if (content2 == null) {
            return null;
        } else {
            throw new IllegalArgumentException("Missing Content-Type header in response: " + parseAsString());
        }
    }

    public String parseAsString() throws IOException {
        InputStream content2 = getContent();
        if (content2 == null) {
            return "";
        }
        try {
            long contentLength2 = this.contentLength;
            int bufferSize = contentLength2 == -1 ? 4096 : (int) contentLength2;
            int length = 0;
            byte[] buffer = new byte[bufferSize];
            byte[] tmp = new byte[4096];
            while (true) {
                int bytesRead = content2.read(tmp);
                if (bytesRead == -1) {
                    return new String(buffer, 0, length);
                }
                if (length + bytesRead > bufferSize) {
                    bufferSize = Math.max(bufferSize << 1, length + bytesRead);
                    byte[] newbuffer = new byte[bufferSize];
                    System.arraycopy(buffer, 0, newbuffer, 0, length);
                    buffer = newbuffer;
                }
                System.arraycopy(tmp, 0, buffer, length, bytesRead);
                length += bytesRead;
            }
        } finally {
            content2.close();
        }
    }

    public static boolean isSuccessStatusCode(int statusCode2) {
        return statusCode2 >= 200 && statusCode2 < 300;
    }
}
