package com.google.api.client.auth.oauth2;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.http.json.JsonHttpParser;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.GenericData;
import com.google.api.client.util.Key;
import java.io.IOException;

@Deprecated
public class AccessTokenRequest extends GenericData {
    public String authorizationServerUrl;
    @Key("client_id")
    public String clientId;
    public String clientSecret;
    @Key("grant_type")
    public String grantType = "none";
    public JsonFactory jsonFactory;
    @Key
    public String scope;
    public HttpTransport transport;
    public boolean useBasicAuthorization = true;

    @Deprecated
    public static class AuthorizationCodeGrant extends AccessTokenRequest {
        @Key
        public String code;
        @Key("redirect_uri")
        public String redirectUri;

        public AuthorizationCodeGrant() {
            this.grantType = "authorization_code";
        }
    }

    @Deprecated
    public static class ResourceOwnerPasswordCredentialsGrant extends AccessTokenRequest {
        public String password;
        @Key
        public String username;

        public ResourceOwnerPasswordCredentialsGrant() {
            this.grantType = "password";
        }
    }

    @Deprecated
    public static class AssertionGrant extends AccessTokenRequest {
        @Key
        public String assertion;
        @Key("assertion_type")
        public String assertionType;

        public AssertionGrant() {
            this.grantType = "assertion";
        }
    }

    @Deprecated
    public static class RefreshTokenGrant extends AccessTokenRequest {
        @Key("refresh_token")
        public String refreshToken;

        public RefreshTokenGrant() {
            this.grantType = "refresh_token";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.api.client.util.GenericData.put(java.lang.Object, java.lang.Object):java.lang.Object
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.google.api.client.util.GenericData.put(java.lang.String, java.lang.Object):java.lang.Object */
    public final HttpResponse execute() throws IOException {
        JsonHttpParser parser = new JsonHttpParser();
        parser.jsonFactory = this.jsonFactory;
        UrlEncodedContent content = new UrlEncodedContent();
        content.data = this;
        HttpRequest request = this.transport.createRequestFactory().buildPostRequest(new GenericUrl(this.authorizationServerUrl), content);
        request.addParser(parser);
        if (this.useBasicAuthorization) {
            request.headers.setBasicAuthentication(this.clientId, this.clientSecret);
        } else {
            put("client_secret", (Object) this.clientSecret);
        }
        return request.execute();
    }
}
