package yoji.hitPentomino;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class Ptm extends Activity {
    /* access modifiers changed from: private */
    public TickHandler tickHandler;
    /* access modifiers changed from: private */
    public PtmView touchView;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        this.touchView = new PtmView(this);
        setContentView(this.touchView);
    }

    public void onResume() {
        super.onResume();
        this.tickHandler = new TickHandler();
        this.tickHandler.sleep(0);
    }

    public void onPause() {
        super.onPause();
        this.tickHandler = null;
    }

    public class TickHandler extends Handler {
        public TickHandler() {
        }

        public void handleMessage(Message msg) {
            Ptm.this.touchView.invalidate();
            if (Ptm.this.tickHandler != null) {
                Ptm.this.tickHandler.sleep(5);
            }
        }

        public void sleep(long delayMills) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMills);
        }
    }
}
