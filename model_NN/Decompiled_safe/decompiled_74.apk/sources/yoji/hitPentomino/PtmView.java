package yoji.hitPentomino;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import java.lang.reflect.Array;
import java.util.Random;

public class PtmView extends View {
    int[][] Matrix = ((int[][]) Array.newInstance(Integer.TYPE, 30, 20));
    String[] ans = {"090631 150721 130621 160710 120820 170431 130411 090411 150430 100820 110300 110500 <00>", "140720 080511 110711 140410 090300 050630 060430 160400 080820 170820 180630 120500 <01>", "130520 180411 090721 100500 190710 060630 070430 150400 130730 110820 090320 160700 <02>", "070820 100321 090631 130500 180801 190431 070430 070630 160720 130820 140320 160500 <03>", "070801 110510 070600 070430 130531 080321 180400 140400 160820 100830 190610 160600 <04>", "180820 070700 180410 130520 100300 140701 070430 070630 140300 190620 120820 100500 <05>", "180530 130420 130521 070430 180801 090321 170621 070630 180300 100720 090820 140700 <06>", "080731 090321 100611 130500 180801 060630 070430 180410 140630 130820 140320 180600 <07>", "130731 110321 080610 130500 180801 060630 070430 180410 160630 090820 140320 180600 <08>", "080731 130400 130621 070411 180300 060630 170731 110500 120820 190820 110320 180500 <09>", "180321 170631 180610 130730 110431 060611 070401 080620 130321 190820 080820 130500 <10>", "100800 150821 080411 110530 130321 120920 150610 150410 100321 080600 070710 130500 <11>", "070411 110821 120421 100530 080920 100321 130511 150400 130820 150920 160710 080700 <12>", "150920 110901 080710 140410 160431 090901 120531 100620 120730 070400 090320 140700 <13>", "130420 080510 150410 150830 070811 160610 090401 110400 110620 130820 120920 090800 <14>", "160410 120321 090411 110510 150901 080920 130831 100810 140410 080730 070610 150700 <15>", "080820 080501 170610 140730 130501 160421 120721 080630 100730 180820 110420 150600 <16>", "190730 100421 140721 130510 050530 150421 120700 190510 060630 170820 070820 090600 <17>", "130721 070820 120511 140530 050611 190500 160731 100720 060530 200820 180420 090500 <18>", "180820 170520 180621 110510 050530 130421 060720 090720 120720 140820 200610 070500 <19>", "070531 180421 190720 140620 050530 170820 120721 100500 200510 080711 070820 130500 <20>", "180501 180701 140611 110610 050530 120421 070500 060720 200510 130820 180820 090700 <21>", "190801 140501 090510 130610 050611 190400 070621 190610 070421 150820 070820 110700 <22>", "060820 200631 090711 130610 050530 180500 070531 110521 190820 160820 170420 120700 <23>", "110700 070400 190521 140600 050530 140801 080631 060720 180720 200820 150420 120500 <24>", "190421 060630 080520 180610 050611 100400 140500 130720 090720 200820 070820 170700 <25>", "190731 080501 120610 100620 160421 080820 180511 080630 160820 130820 110420 160600 <26>", "110801 090401 130811 080430 170300 120300 150700 080711 170610 180920 100920 140500 <27>", "080920 080401 170710 140830 130401 160321 120821 080530 100830 180920 110320 150521 <28>", "100520 080831 120510 110830 180531 140521 130821 090820 170810 070400 160320 150801 <29>", "170321 080830 100811 150410 070811 110400 130401 140820 160820 180920 090320 120701 <30>", "150630 160920 080821 110520 180810 110321 140400 170400 090400 070300 100920 130704 <31>", "060400 150620 060621 160620 120720 140421 100500 180620 080530 190410 080720 120500 <32>", "080411 150720 100410 160720 120820 140321 060731 180400 060300 190820 080820 120501 <33>", "050711 110611 170721 090730 130820 170321 100400 070720 130430 190820 070300 150401 <34>", "070820 140820 170401 070400 050430 150321 090400 100720 110430 190720 170820 130401 <35>", "040400 160400 040601 140630 120421 080620 200500 120620 080421 210720 060720 170600 <36>", "040421 140400 120600 090510 100720 120400 160631 040611 210510 070720 190720 190500 <37>", "040421 140720 200510 080610 180400 060701 110601 040630 170610 210720 100420 130500 <38>", "160720 100521 120611 170510 120400 090720 150520 200500 030630 210720 050420 050600 <39>", "180530 100720 170611 120530 130701 130421 070511 200500 030630 210720 050420 050600 <40>", "030630 180720 140511 040510 190500 070701 100500 060500 120720 210720 190420 120500 <41>", "030630 160720 140511 040510 190500 070701 060501 080500 120720 210720 190420 120500 <42>", "120510 130720 140520 070500 050720 030530 200511 050500 200720 160720 160420 080600 <43>", "030630 040501 060621 170620 190400 210610 140501 190620 120720 080720 070420 120500 <44>", "030530 060421 070621 120610 190421 090701 140500 150620 210610 050720 110420 190600 <45>", "110630 080520 130611 160510 130400 060720 180501 080610 200510 050400 180700 150600 <46>", "170701 130521 040620 070620 140701 060500 110630 090620 210610 020730 160520 190600 <47>", "100321 080901 170820 140800 130321 150920 100810 120800 060430 190710 110520 080400 <48>", "100520 160920 130421 100810 170700 090901 140831 120800 060430 190920 110320 080400 <49>", "161001 140400 160820 160630 140321 170510 120721 120930 110300 141020 100810 110500 <50>", "100430 130731 120510 150920 170910 100730 160630 110920 170410 140400 130320 130900 <51>", "110300 150410 150721 120800 161001 120620 130421 160400 100630 101030 170610 130900 <52>", "130930 140720 160631 140420 170431 130300 120521 110720 111020 171020 100510 150800 <53>", "100411 130410 160810 110800 170431 130901 110631 140400 160510 171020 121020 140700 <54>", "150411 150721 120521 160710 090430 170510 110431 130830 100920 140310 150920 100700 <55>", "090630 120321 100821 140410 160811 120521 100430 160400 140920 120920 170610 140700 <57>", "110630 140321 140610 120430 100431 120920 100820 160520 170810 170310 090630 150800 <58>", "150800 110421 110911 090910 100621 120700 130921 130500 150720 080500 120320 150500 <59>", "131020 170931 150720 130520 100630 120400 110911 120730 160620 150810 130320 160400 <60>", "150321 080901 080621 160820 140700 130901 080431 140530 100700 170620 110320 110500 <61>"};
    String[] ans415 = {"060400 150620 060621 160620 120720 140421 100500 180620 080530 190410 080720 120500 <32>", "150421 070500 120611 160620 120400 080720 100610 180500 050611 190720 070420 140600 <415-0>", "070720 150400 110621 070500 050530 110400 180500 090600 130530 190720 130720 160600 <415-1>", "050611 110511 170621 090630 130720 170421 100500 070620 130530 190720 070420 150500 <415-2>", "050630 160720 140511 060510 170500 090701 080501 100500 120720 190720 170420 120500 <415-3>", "070421 140400 180510 060530 060720 150701 160511 130630 110610 190720 110420 090600 <415-4>", "070631 150400 180510 140630 120421 050530 160600 120620 080630 190720 080420 100600 <415-5>"};
    String[] ans512 = {"080820 080501 170610 140730 130501 160421 120721 080630 100730 180820 110420 150600 <16>", "130721 140500 110711 170520 090801 100400 080530 160730 080720 180820 150420 120500 <512-0>", "070730 110820 120510 080530 100400 140521 170600 130720 100610 180820 160420 150700 <512-1>", "100421 170511 120510 080510 070711 130701 150720 160500 090720 180820 110820 140500 <512-2>", "130801 160400 140510 090530 180631 100421 120511 090711 170710 110811 070610 150700 <512-3>", "100801 140700 080710 090510 170801 070530 150531 160610 120500 180410 120420 120700 <512-4>", "170520 150521 160721 120530 080421 110820 100601 130710 080820 180820 140420 080600 <512-5>"};
    String[] ans610 = {"090631 150721 130621 160710 120820 170431 130411 090411 150430 100820 110300 110500 <00>", "160820 080611 100721 110500 150321 130531 140720 090430 170510 120820 110320 150500 <610-0>", "080530 170510 090721 090410 130400 160801 130720 120600 160300 110820 120320 150600 <610-1>", "100721 130700 090511 110610 080711 100300 120400 160400 140620 160820 170610 140400 <610-2>", "160321 100801 090511 140410 080711 100300 110601 130620 150620 170720 150820 120400 <610-3>", "090300 140300 160421 130600 110531 080630 160600 120400 100610 170820 110820 140700 <610-4>", "080530 170631 140710 140410 100801 110400 120701 160400 160820 100720 100320 130500 <610-5>"};
    Bitmap answerground;
    Bitmap answerground2;
    String[][] baseData = {new String[]{"........................", "......dddddddddd.........", "......dddddddddd.........", "......dddddddddd.........", "......dddddddddd.........", "......dddddddddd.........", "......dddddddddd........."}, new String[]{"........................", "...d dddd  ddddd.......", "...ddd dd.d ddd d.......", "...dd dd.ddd dd d.......", "...dd d.ddd dd dd.......", "...d ddd.d ddd dd.......", "...d ddd.dd  dddd......."}, new String[]{"........................", "....dddddd  ddd  d......", "....ddd dd ddd  dd......", "....dd dd dddd  dd......", "....dd ddd dd d dd......", "....d dddd d ddd d......", "....d dddd dd d dd......"}, new String[]{"........................", "....d ddd ddddd  d......", "....ddd dd dd d dd......", "....dddd  dd ddd d......", "....dddd  dd ddd d......", "....ddddd  d ddd d......", "....ddd  ddd  dddd......"}, new String[]{"........................", "....ddddd  ddd  dd......", "....ddd d ddd  ddd......", "....ddd dd dd d dd......", "....ddd dd d ddd d......", "....ddd dd d ddd d......", "....ddd ddd  ddd d......"}, new String[]{"........................", "....d dddd ddd  dd.........", "....ddd dd ddd  dd.........", "....dd ddd dd  ddd.........", "....ddd d ddd  ddd.........", "....dddd  dddd  dd.........", "....dd  ddddd  ddd........."}, new String[]{"........................", "....d dddd dd  ddd.........", "....dd d dddd  ddd.........", "....ddd d ddd  ddd.........", "....ddd d d d dddd.........", "....d ddd  ddd ddd.........", ".... ddddd  d dddd........."}, new String[]{"........................", "....dddd  ddddd  d......", "....ddddd  dd  ddd......", "....dd dd dddd  dd......", "....ddd d ddd  ddd......", "....ddd dd ddd  dd......", "....ddd  ddd  dddd......"}, new String[]{"........................", "....d  dddddddd  d.........", "....ddd  dddd  ddd.........", "....dddd  dd  dddd.........", "....dddd  dd  dddd.........", "....dddd  dd  dddd.........", "....dddd  dd  dddd........."}, new String[]{"........................", "....d  ddddd  dddd.........", "....dd  dddddd  dd.........", "....ddd  dddd  ddd.........", "....ddd  dddd  ddd.........", "....ddd  ddd  dddd.........", "....ddd  ddd  dddd........."}, new String[]{"........................", "....dd  ddddd  ddd.........", "....ddd  dddd  ddd.........", "....ddd  dddd  ddd.........", "....ddd  ddd  dddd.........", "....dddd  dd  dddd.........", "....ddddd  dd  ddd........."}, new String[]{"......................", ".....dddddddddd.......", "..... ddddddddd.......", ".....dddddddddd.......", ".....dddddddddd.......", ".....dd       d.......", ".....ddddddddd .......", ".....d dddddddd......."}, new String[]{"......................", ".....dddddddddd.......", ".....ddddddddd .......", ".....d dddddddd.......", "..... d dd d  d.......", ".....ddd  d ddd.......", ".....dddddddddd.......", ".....dddddddddd......."}, new String[]{"......................", ".....ddddd dddd.......", ".....ddd  d ddd .......", ".....d  dddd dd.......", ".....dddddddd d.......", "..... dddddddd .......", ".....dddddddddd.......", ".....dddddddddd......."}, new String[]{"......................", "..... ddddddddd.......", ".....d dddddddd.......", ".....dddddd ddd.......", ".....dd dddd dd.......", ".....ddd   dd d .......", ".....ddddddddd .......", ".....dddddddddd......."}, new String[]{"......................", "..... ddddddddd.......", ".....d dddddddd.......", ".....dddddddd d.......", ".....dd   d  d .......", ".....ddddd dddd .......", ".....dddddddddd.......", ".....dddddddddd......."}, new String[]{"......................", "......................", "...,,dddddddddddd.....", ".....dddddddddddd.....", ".....dddddddddddd.....", ".....dddddddddddd.....", ".....dddddddddddd....."}, new String[]{"......................", "......................", "...dd ddd dddddd  d...", "...ddd ddd ddd  ddd...", "...dd ddd dddd d dd...", "...ddd d ddddd d dd...", "...ddddd  dd ddd dd..."}, new String[]{"......................", "......................", "...ddd d ddd  ddddd...", "...dd ddd ddd  dddd...", "...ddd dd ddddd  dd...", "...dddd d dddddd  d...", "...ddd ddd ddd  ddd..."}, new String[]{"......................", "......................", "...ddd  dddddd dd d...", "...dddd  ddd ddd dd...", "...ddd d dddd  dddd...", "...ddd d dddd  dddd...", "...dd ddd ddd  dddd..."}, new String[]{"......................", "......................", "...ddd ddd d  ddddd...", "...dddd d dddd  ddd...", "...dddd d dddd  ddd...", "...dddd  dddd  dddd...", "...ddddd  dd  ddddd..."}, new String[]{"......................", "......................", "...dddd  dddd  dddd...", "...ddd  ddddd  dddd...", "...ddd d dddd  dddd...", "...dd ddd dddd  ddd...", "...ddd d ddd  ddddd..."}, new String[]{"......................", "......................", "...dddddd  dd  dddd...", "...ddddd  ddddd  dd...", "...ddddd d ddd  ddd...", "...dddd ddd dd  ddd...", "...ddddd d ddd  ddd..."}, new String[]{"......................", "......................", "...ddd  ddd  dddddd...", "...dddd  ddd  ddddd...", "...dd dd dddd d ddd...", "...ddd d dddd d ddd...", "...ddd dd d ddd ddd..."}, new String[]{"......................", "......................", "...ddddd  dddddd  d...", "...dddd  ddddd  ddd...", "...ddddd  ddd  dddd...", "...ddd  ddddd  dddd...", "...ddd  dddddd  ddd..."}, new String[]{"........................", "......................", "...ddddddd  dd  ddd...", "...dddddd  dd  dddd...", "...dddddd  dd  dddd...", "...dddddd  d  ddddd...", "...ddddd  ddd  dddd..."}, new String[]{"......................", "......................", ".....ddddddd ddddd....", ".....ddddddd ddddd....", ".....ddddddd ddddd....", ".....ddddddd ddddd....", ".....ddddddd ddddd...."}, new String[]{"......................", ".....ddddddd dddd.....", ".....dddddd d   d.....", "..... dd   ddddd .....", ".....d      dddd .....", ".....dddddddddddd.....", ".....d  dddd d  d .....", "..... ddddddd ddd ....."}, new String[]{"......................", ".....dddddddddddd.....", ".....dddddddddd  .....", ".....ddd    ddd  .....", ".....d       d dd.....", ".....   dddd   dd.....", "..... ddddddd ddd.....", ".....dddddddddddd....."}, new String[]{"......................", ".....       ddddd.....", ".....ddddddd   dd.....", ".....d dddddddd d.....", ".....dd  dd d   d.....", "..... ddd  d dddd.....", ".....ddddddddddd .....", ".....ddddddddddd ....."}, new String[]{"......................", ".....dddddddddddd.....", ".....  dddddddddd.....", ".....    d d  d  .....", ".....dd   d      .....", ".....ddddddddd dd.....", ".....dddd d ddddd.....", ".....ddddd dddddd....."}, new String[]{"......................", ".....dddddddddddd.....", ".....dddddddd  d .....", ".....dd ddd dd d .....", ".....   dd d dd d.....", ".....  d  ddddd d.....", ".....ddd   d  ddd.....", ".....dddddd ddddd....."}, new String[]{"......................", "......................", "...ddddddddddddddd....", "...ddddddddddddddd....", "...ddddddddddddddd....", "...ddddddddddddddd...."}, new String[]{"......................", "...ddddddd ddddddd....", "...d dddd d d   d ....", "...   dddddd    d ....", "... d     d  ddd d....", "...ddd   d ddddd d....", "...ddddddddddddddd...."}, new String[]{"......................", "...ddddddd ddddddd....", "...    dd  ddddd  ....", "...     d dddd    ....", "...dddd  dd     dd....", "   ddddd dd   dddd....", "...ddddddddddddddd...."}, new String[]{"......................", "...dddddddddddddd ....", "...ddddd dddddddd ....", "...dd  d ddd  d  d....", "...d    d        d....", "...  dd d   dd ddd....", "... dddddddddddddd...."}, new String[]{"......................", "......................", ".ddd ddd dddd ddd dd..", ".ddd ddd ddd ddd ddd..", ".ddd dddd ddd ddd dd..", ".dddddd  ddddd d ddd.."}, new String[]{"......................", "......................", ".ddd  ddddddddd  ddd..", ".ddd d dddddd d dddd..", ".ddd d ddddd ddd ddd..", ".d ddd ddddd d ddddd.."}, new String[]{"......................", "......................", ".ddd  dddddd  dddddd..", ".ddd dd ddddd dd ddd..", ".dddd dd ddddd d ddd..", ".ddddd d dddd dd ddd.."}, new String[]{"......................", "......................", ".ddddd  dddd ddd ddd..", ".ddd  dddddd dddd dd..", ".dddd  dddd ddddd dd..", ".ddd  dddddd ddd ddd.."}, new String[]{"......................", "......................", ".dddddd  ddddd  dddd..", ".dddddd  ddd  dddddd..", ".ddddd  ddddd  ddddd..", ".ddd  ddddddd  ddddd.."}, new String[]{"......................", "......................", ".ddddd  ddddd  ddddd..", ".dddd  dddddd  ddddd..", ".ddddd  dddddd  dddd..", ".dddddd  ddd  dddddd.."}, new String[]{"......................", "......................", ".ddddddd  ddd  ddddd..", ".dddddd  dddd  ddddd..", ".dddddd  ddddddd  dd..", ".dddddd  dddddd  ddd.."}, new String[]{"......................", "......................", ".dddddd  dddddddd  d..", ".dddddd  dddddd  ddd..", ".ddddddd  ddddd  ddd..", ".dddddd  dddddd  ddd.."}, new String[]{"......................", "......................", ".ddddddd  ddd  ddddd..", ".dddddd  ddddd  dddd..", ".dddddd  dddd  ddddd..", ".dddddd  ddd  dddddd.."}, new String[]{"......................", "......................", ".dddd  ddddddd  dddd..", ".dddddd  ddddd  dddd..", ".ddddddd  dddd  dddd..", ".dddddddd  dddd  ddd.."}, new String[]{"......................", "......................", "...ddddd dddddddddd...", "...ddddd dddddddddd...", "...ddddd dddddddddd...", "...ddddd dddddddddd..."}, new String[]{"......................", "......................", "......................", "dddddddddddddddddddd..", "dddddddddddddddddddd..", "dddddddddddddddddddd.."}, new String[]{"......................", "....dddddddddd........", "....ddddddd...........", "....dddddddd..........", "......................", "........dddddddddd....", ".... ddddddddddddd....", "......dddddddddddd...."}, new String[]{"......................", "....ddddddddd.........", "....ddddddddd.........", "....ddddddd...........", "......................", ".......ddddddddddd....", ".......ddddddddddd....", ".....ddddddddddddd...."}, new String[]{"......................", "........dddddddd......", "........dddddddd......", "........dddddddd......", "........ddd..ddd......", "......,,ddd..ddd......", "........dddddddd......", "........dddddddd......", "........dddddddd......"}, new String[]{"......................", "........dddddddd......", "........dddddddd......", "........dddddddd......", "........dddddddd......", "......,,dddd..dd......", "........dddd..dd......", "........dddddddd......", "........dddddddd......"}, new String[]{"......................", "........dddddddd......", "........dddddddd......", "........dddddddd......", "........dddddddd......", "........dddddddd......", "........ddddd..d......", "........ddddd..d......", "........dddddddd......"}, new String[]{"......................", "........dddddddd......", "........d.dddd.d......", "........dddddddd......", "........dddddddd......", "........dddddddd......", "........dddddddd......", "........d.dddd.d......", "........dddddddd......"}, new String[]{"......................", "........dddddddd......", "........dddddddd......", "........dd.dd.dd......", "........dddddddd......", "........dddddddd......", "........dd.dd.dd......", "........dddddddd......", "........dddddddd......"}, new String[]{"......................", ".......ddddddddd......", ".......ddddddddd......", ".......ddddddddd......", ".......ddd...ddd......", ".......ddddddddd......", ".......ddddddddd......", ".......ddddddddd......"}, new String[]{"......................", ".......ddddddddd......", ".......ddddddddd......", ".......ddddddddd......", ".......dd.d.d.dd......", ".......ddddddddd......", ".......ddddddddd......", ".......ddddddddd......"}, new String[]{"......................", ".......ddddddddd......", ".......dddd.dddd......", ".......ddddddddd......", ".......dddd.dddd......", ".......ddddddddd......", ".......dddd.dddd......", ".......ddddddddd......"}, new String[]{"......................", "........ddddd.........", ".......ddddddd........", "......ddddddddd.......", "......ddddddddd.......", "......ddddddddd.......", "......ddddddddd.......", ".......ddddddd........", "........ddddd........."}, new String[]{"......................", ".........dddddd.......", "........dddddddd......", "........dddddddd......", "........dddddddd......", "......,,dddddddd......", "........dddddddd......", "........dddddddd......", ".........dddddd......."}, new String[]{"......................", "......ddddddddd.......", ".....ddd d d ddd......", ".....d ddddddd d......", ".....ddd d d ddd......", ".....d ddddddd d......", ".....ddd d d ddd......", "......ddddddddd......."}};
    int baseNum;
    int baseNumMax = 60;
    boolean bookcoverMode = true;
    String btn = "No";
    int[] cX = new int[12];
    int[] cY = new int[12];
    Bitmap centwhite;
    int color0 = -16752385;
    int color1 = -16736081;
    int color10 = -34953;
    int color11 = -65485;
    int color2 = -16740315;
    int color3 = -4871680;
    int color4 = -256;
    int color5 = -36608;
    int color6 = -40548;
    int color7 = -8355712;
    int color8 = -16711936;
    int color9 = -16777046;
    int[][] compression = ((int[][]) Array.newInstance(Integer.TYPE, 20, 10));
    int dh;
    int[] dt = new int[12];
    int dw;
    boolean first;
    int[] getNum = new int[(this.baseNumMax + 1)];
    boolean goalMode = false;
    Bitmap goalground;
    int hH;
    String[] kind = {"6x10", "5x12", "4x15", "3x20", "8"};
    int[] order = new int[12];
    Bitmap pentground;
    Bitmap putcover;
    Random rnd = new Random();
    int[] sd = new int[12];
    int select = 1;
    String str;
    private int touchAction = -9999;
    private int touchX = 0;
    private int touchY = 0;
    int wW;
    private int x;
    private int y;

    public PtmView(Context context) {
        super(context);
        setBackgroundColor(-1);
        Resources r = context.getResources();
        setFocusable(true);
        this.pentground = BitmapFactory.decodeResource(r, R.drawable.pentground);
        this.goalground = BitmapFactory.decodeResource(r, R.drawable.goalground);
        this.centwhite = BitmapFactory.decodeResource(r, R.drawable.centwhite);
        this.putcover = BitmapFactory.decodeResource(r, R.drawable.putcover);
        this.answerground = BitmapFactory.decodeResource(r, R.drawable.answerground);
        this.answerground2 = BitmapFactory.decodeResource(r, R.drawable.answerground2);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        if (this.bookcoverMode) {
            Rect rect = new Rect(0, 0, this.putcover.getWidth(), this.putcover.getHeight());
            Rect dst = new Rect(0, 0, getWidth(), getHeight());
            canvas.drawBitmap(this.putcover, rect, dst, (Paint) null);
            this.first = true;
            this.baseNum = 0;
            this.x = 3;
            this.y = 8;
            this.hH = getHeight();
            this.wW = getWidth();
            this.dw = this.wW / 20;
            this.dh = this.hH / 11;
            this.cX[0] = 20;
            this.cY[0] = 8;
            this.dt[0] = 0;
            this.sd[0] = 0;
            this.cX[1] = 15;
            this.cY[1] = 7;
            this.dt[1] = 2;
            this.sd[1] = 1;
            this.cX[2] = 19;
            this.cY[2] = 6;
            this.dt[2] = 2;
            this.sd[2] = 0;
            this.cX[3] = 4;
            this.cY[3] = 6;
            this.dt[3] = 2;
            this.sd[3] = 0;
            this.cX[4] = 19;
            this.cY[4] = 3;
            this.dt[4] = 2;
            this.sd[4] = 1;
            this.cX[5] = 17;
            this.cY[5] = 4;
            this.dt[5] = 3;
            this.sd[5] = 1;
            this.cX[6] = 13;
            this.cY[6] = 4;
            this.dt[6] = 1;
            this.sd[6] = 1;
            this.cX[7] = 9;
            this.cY[7] = 4;
            this.dt[7] = 3;
            this.sd[7] = 0;
            this.cX[8] = 4;
            this.cY[8] = 9;
            this.dt[8] = 2;
            this.sd[8] = 0;
            this.cX[9] = 10;
            this.cY[9] = 8;
            this.dt[9] = 2;
            this.sd[9] = 0;
            this.cX[10] = 6;
            this.cY[10] = 7;
            this.dt[10] = 3;
            this.sd[10] = 0;
            this.cX[11] = 4;
            this.cY[11] = 4;
            this.dt[11] = 0;
            this.sd[10] = 0;
            this.order[0] = 3;
            this.order[1] = 1;
            this.order[2] = 7;
            this.order[3] = 0;
            this.order[4] = 4;
            this.order[5] = 5;
            this.order[6] = 8;
            this.order[7] = 2;
            this.order[8] = 6;
            this.order[9] = 9;
            this.order[10] = 10;
            this.order[11] = 11;
            this.select = 9;
        }
        this.str = "No";
        this.btn = "No";
        if (this.touchAction == 2) {
            this.str = "Mv";
            if (this.touchX > 0 && this.touchX < this.dw * 22 && this.touchY > 0 && this.touchY < this.dh * 9) {
                this.x = (this.touchX / this.dw) + 2;
                this.y = (this.touchY / this.dh) + 2;
                this.cX[this.select] = this.x;
                this.cY[this.select] = this.y;
            }
        }
        if (this.touchAction == 0) {
            this.str = "Dn";
            int oldX = this.x;
            int oldY = this.y;
            this.x = (this.touchX / this.dw) + 2;
            int y2 = (this.touchY / this.dh) + 2;
            if (this.x > 0 && this.x < 22 && y2 > 0 && y2 < 11 && this.Matrix[this.x][y2] != this.Matrix[oldX][oldY]) {
                switch (this.Matrix[this.x][y2]) {
                    case 10:
                        paint.setColor(this.color0);
                        this.select = 0;
                        break;
                    case 11:
                        paint.setColor(this.color1);
                        this.select = 1;
                        break;
                    case 12:
                        paint.setColor(this.color2);
                        this.select = 2;
                        break;
                    case 13:
                        paint.setColor(this.color3);
                        this.select = 3;
                        break;
                    case 14:
                        paint.setColor(this.color4);
                        this.select = 4;
                        break;
                    case 15:
                        paint.setColor(this.color5);
                        this.select = 5;
                        break;
                    case 16:
                        paint.setColor(this.color6);
                        this.select = 6;
                        break;
                    case 17:
                        paint.setColor(this.color7);
                        this.select = 7;
                        break;
                    case 18:
                        paint.setColor(this.color8);
                        this.select = 8;
                        break;
                    case 19:
                        paint.setColor(this.color9);
                        this.select = 9;
                        break;
                    case 20:
                        paint.setColor(this.color10);
                        this.select = 10;
                        break;
                    case 21:
                        paint.setColor(this.color11);
                        this.select = 11;
                        break;
                }
                for (int k = 0; k < 12; k++) {
                    if (this.order[k] == this.select) {
                        for (int sk = k; sk < 11; sk++) {
                            this.order[sk] = this.order[sk + 1];
                        }
                        this.order[11] = this.select;
                    }
                }
                this.cX[this.select] = this.x;
                this.cY[this.select] = y2;
            }
            this.bookcoverMode = false;
            if (this.touchX > this.dw * 14 && this.touchX < this.dh * 18 && this.touchY > (this.hH - this.dh) - (this.dh / 2) && this.touchY < this.hH) {
                int[] iArr = this.dt;
                int i = this.select;
                iArr[i] = iArr[i] + 1;
                if (this.dt[this.select] > 3) {
                    this.dt[this.select] = 0;
                }
                this.str = "No";
                for (int dly = 0; dly < 20000000; dly++) {
                }
            }
            if (this.touchX > this.dw * 10 && this.touchX < this.dw * 14 && this.touchY > (this.hH - this.dh) - (this.dh / 2) && this.touchY < this.hH) {
                int[] iArr2 = this.sd;
                int i2 = this.select;
                iArr2[i2] = iArr2[i2] + 1;
                if (this.sd[this.select] > 1) {
                    this.sd[this.select] = 0;
                }
                for (int dly2 = 0; dly2 < 30000000; dly2++) {
                }
            }
            if (this.touchX > this.dw * 7 && this.touchX < this.dw * 9 && this.touchY > this.hH - this.dh && this.touchY < this.hH * 10) {
                this.baseNum = this.baseNum + 1;
                if (this.baseNum > this.baseNumMax) {
                    this.baseNum = 0;
                }
                this.cX[0] = 2;
                this.cY[0] = 7;
                this.dt[0] = 1;
                this.sd[0] = 0;
                this.cX[1] = 2;
                this.cY[1] = 7;
                this.dt[1] = 3;
                this.sd[1] = 0;
                this.cX[2] = 2;
                this.cY[2] = 7;
                this.dt[2] = 1;
                this.sd[2] = 0;
                this.cX[3] = 2;
                this.cY[3] = 6;
                this.dt[3] = 1;
                this.sd[3] = 0;
                this.cX[4] = 2;
                this.cY[4] = 5;
                this.dt[4] = 3;
                this.sd[4] = 0;
                this.cX[5] = 2;
                this.cY[5] = 5;
                this.dt[5] = 3;
                this.sd[5] = 1;
                this.cX[6] = 2;
                this.cY[6] = 7;
                this.dt[6] = 2;
                this.sd[6] = 0;
                this.cX[7] = 2;
                this.cY[7] = 7;
                this.dt[7] = 1;
                this.sd[7] = 0;
                this.cX[8] = 2;
                this.cY[8] = 6;
                this.dt[8] = 2;
                this.sd[8] = 0;
                this.cX[9] = 2;
                this.cY[9] = 4;
                this.dt[9] = 1;
                this.sd[9] = 0;
                this.cX[10] = 2;
                this.cY[10] = 6;
                this.dt[10] = 1;
                this.sd[10] = 0;
                this.cX[11] = 2;
                this.cY[11] = 5;
                this.dt[11] = 0;
                this.sd[10] = 0;
                this.order[0] = 0;
                this.order[1] = 1;
                this.order[2] = 2;
                this.order[3] = 3;
                this.order[4] = 4;
                this.order[5] = 5;
                this.order[6] = 6;
                this.order[7] = 7;
                this.order[8] = 8;
                this.order[9] = 9;
                this.order[10] = 10;
                this.order[11] = 11;
                this.x = 3;
                this.goalMode = false;
                for (int dly3 = 0; dly3 < 30000000; dly3++) {
                }
            }
            if (this.touchX > this.dw * 4 && this.touchX < this.dw * 6 && this.touchY > this.hH - this.dh && this.touchY < this.hH * 10) {
                this.baseNum = this.baseNum - 1;
                if (this.baseNum < 0) {
                    this.baseNum = this.baseNumMax;
                }
                this.cX[0] = 2;
                this.cY[0] = 7;
                this.dt[0] = 1;
                this.sd[0] = 0;
                this.cX[1] = 2;
                this.cY[1] = 7;
                this.dt[1] = 3;
                this.sd[1] = 0;
                this.cX[2] = 2;
                this.cY[2] = 7;
                this.dt[2] = 1;
                this.sd[2] = 0;
                this.cX[3] = 2;
                this.cY[3] = 6;
                this.dt[3] = 1;
                this.sd[3] = 0;
                this.cX[4] = 2;
                this.cY[4] = 5;
                this.dt[4] = 3;
                this.sd[4] = 0;
                this.cX[5] = 2;
                this.cY[5] = 5;
                this.dt[5] = 3;
                this.sd[5] = 1;
                this.cX[6] = 2;
                this.cY[6] = 7;
                this.dt[6] = 2;
                this.sd[6] = 0;
                this.cX[7] = 2;
                this.cY[7] = 7;
                this.dt[7] = 1;
                this.sd[7] = 0;
                this.cX[8] = 2;
                this.cY[8] = 6;
                this.dt[8] = 2;
                this.sd[8] = 0;
                this.cX[9] = 2;
                this.cY[9] = 4;
                this.dt[9] = 1;
                this.sd[9] = 0;
                this.cX[10] = 2;
                this.cY[10] = 6;
                this.dt[10] = 1;
                this.sd[10] = 0;
                this.cX[11] = 2;
                this.cY[11] = 5;
                this.dt[11] = 0;
                this.sd[10] = 0;
                this.order[0] = 0;
                this.order[1] = 1;
                this.order[2] = 2;
                this.order[3] = 3;
                this.order[4] = 4;
                this.order[5] = 5;
                this.order[6] = 6;
                this.order[7] = 7;
                this.order[8] = 8;
                this.order[9] = 9;
                this.order[10] = 10;
                this.order[11] = 11;
                this.x = 3;
                this.goalMode = false;
                for (int dly4 = 0; dly4 < 30000000; dly4++) {
                }
            }
            if (this.touchX > this.dw && this.touchX < this.dw + ((this.dw * 2) / 3) && this.touchY > this.hH - this.dh && this.touchY < this.hH * 10) {
                String sw = this.ans[this.baseNum];
                if (!this.first && this.baseNum == 0) {
                    sw = this.ans610[this.rnd.nextInt(7)];
                }
                if (this.baseNum == 16) {
                    sw = this.ans512[this.rnd.nextInt(7)];
                }
                if (this.baseNum == 32) {
                    sw = this.ans415[this.rnd.nextInt(7)];
                }
                for (int i3 = 0; i3 < 12; i3++) {
                    String sww = sw.substring(i3 * 7, (i3 * 7) + 7);
                    this.cX[i3] = Integer.parseInt(sww.substring(0, 2));
                    this.cY[i3] = Integer.parseInt(sww.substring(2, 4));
                    this.dt[i3] = Integer.parseInt(sww.substring(4, 5));
                    this.sd[i3] = Integer.parseInt(sww.substring(5, 6));
                }
            }
        }
        Rect rect2 = new Rect(0, 0, this.pentground.getWidth(), this.pentground.getHeight());
        Rect dst2 = new Rect(0, 0, getWidth(), getHeight());
        canvas.drawBitmap(this.pentground, rect2, dst2, (Paint) null);
        if (this.goalMode) {
            this.getNum[this.baseNum] = 1;
            Rect rect3 = new Rect(0, 0, this.goalground.getWidth(), this.goalground.getHeight());
            Rect dst3 = new Rect(0, 0, getWidth(), getHeight());
            canvas.drawBitmap(this.answerground, rect3, dst3, (Paint) null);
            paint.setColor(Color.argb(255, 255, 50, 50));
            paint.setTextSize(100.0f);
            canvas.drawText("Goal !", (float) (this.dw * 12), (float) ((this.dh * 10) + (this.dh / 2)), paint);
            this.first = false;
        }
        for (int i4 = 0; i4 < 22; i4++) {
            for (int j = 0; j < 12; j++) {
                this.Matrix[i4][j] = 99;
            }
        }
        for (int i5 = 0; i5 < this.baseData[this.baseNum].length; i5++) {
            String sw2 = this.baseData[this.baseNum][i5];
            for (int j2 = 0; j2 < 22; j2++) {
                if (sw2.substring(j2, j2 + 1).equals("d")) {
                    this.Matrix[j2 + 2][i5 + 2] = 0;
                }
            }
        }
        for (int k2 = 0; k2 < 12; k2++) {
            switch (this.order[k2]) {
                case 0:
                    if (this.dt[0] == 0 && this.sd[0] == 0) {
                        this.Matrix[this.cX[0]][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0]] = 10;
                    }
                    if (this.dt[0] == 1 && this.sd[0] == 0) {
                        this.Matrix[this.cX[0]][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0] - 1] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] - 1] = 10;
                    }
                    if (this.dt[0] == 2 && this.sd[0] == 0) {
                        this.Matrix[this.cX[0]][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0] - 1] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] - 1] = 10;
                    }
                    if (this.dt[0] == 3 && this.sd[0] == 0) {
                        this.Matrix[this.cX[0]][this.cY[0]] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] - 1] = 10;
                    }
                    if (this.dt[0] == 0 && this.sd[0] == 1) {
                        this.Matrix[this.cX[0]][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0] - 1] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] - 1] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0]] = 10;
                    }
                    if (this.dt[0] == 1 && this.sd[0] == 1) {
                        this.Matrix[this.cX[0]][this.cY[0]] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] - 1] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0] - 1] = 10;
                    }
                    if (this.dt[0] == 2 && this.sd[0] == 1) {
                        this.Matrix[this.cX[0]][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0] + 1][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] + 1] = 10;
                    }
                    if (this.dt[0] == 3 && this.sd[0] == 1) {
                        this.Matrix[this.cX[0]][this.cY[0]] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0] + 1] = 10;
                        this.Matrix[this.cX[0] - 1][this.cY[0]] = 10;
                        this.Matrix[this.cX[0]][this.cY[0] - 1] = 10;
                        break;
                    }
                case 1:
                    if (this.dt[1] == 0 && this.sd[1] == 0) {
                        this.Matrix[this.cX[1]][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] - 1][this.cY[1] + 1] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] + 1] = 11;
                        this.Matrix[this.cX[1] + 1][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] + 2][this.cY[1]] = 11;
                    }
                    if (this.dt[1] == 1 && this.sd[1] == 0) {
                        this.Matrix[this.cX[1]][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] - 1][this.cY[1] - 1] = 11;
                        this.Matrix[this.cX[1] - 1][this.cY[1]] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] + 1] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] + 2] = 11;
                    }
                    if (this.dt[1] == 2 && this.sd[1] == 0) {
                        this.Matrix[this.cX[1]][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] - 2][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] - 1][this.cY[1]] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] - 1] = 11;
                        this.Matrix[this.cX[1] + 1][this.cY[1] - 1] = 11;
                    }
                    if (this.dt[1] == 3 && this.sd[1] == 0) {
                        this.Matrix[this.cX[1]][this.cY[1]] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] - 2] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] - 1] = 11;
                        this.Matrix[this.cX[1] + 1][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] + 1][this.cY[1] + 1] = 11;
                    }
                    if (this.dt[1] == 0 && this.sd[1] == 1) {
                        this.Matrix[this.cX[1]][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] - 1][this.cY[1] - 1] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] - 1] = 11;
                        this.Matrix[this.cX[1] + 1][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] + 2][this.cY[1]] = 11;
                    }
                    if (this.dt[1] == 1 && this.sd[1] == 1) {
                        this.Matrix[this.cX[1]][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] + 1][this.cY[1] - 1] = 11;
                        this.Matrix[this.cX[1] + 1][this.cY[1]] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] + 1] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] + 2] = 11;
                    }
                    if (this.dt[1] == 2 && this.sd[1] == 1) {
                        this.Matrix[this.cX[1]][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] - 2][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] - 1][this.cY[1]] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] + 1] = 11;
                        this.Matrix[this.cX[1] + 1][this.cY[1] + 1] = 11;
                    }
                    if (this.dt[1] == 3 && this.sd[1] == 1) {
                        this.Matrix[this.cX[1]][this.cY[1]] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] - 2] = 11;
                        this.Matrix[this.cX[1]][this.cY[1] - 1] = 11;
                        this.Matrix[this.cX[1] - 1][this.cY[1]] = 11;
                        this.Matrix[this.cX[1] - 1][this.cY[1] + 1] = 11;
                        break;
                    }
                case 2:
                    if ((this.dt[2] == 0 || this.dt[2] == 2) && this.sd[2] == 0) {
                        this.Matrix[this.cX[2]][this.cY[2]] = 12;
                        this.Matrix[this.cX[2] - 1][this.cY[2] - 1] = 12;
                        this.Matrix[this.cX[2] - 1][this.cY[2]] = 12;
                        this.Matrix[this.cX[2] + 1][this.cY[2]] = 12;
                        this.Matrix[this.cX[2] + 1][this.cY[2] + 1] = 12;
                    }
                    if ((this.dt[2] == 1 || this.dt[2] == 3) && this.sd[2] == 0) {
                        this.Matrix[this.cX[2]][this.cY[2]] = 12;
                        this.Matrix[this.cX[2] - 1][this.cY[2] + 1] = 12;
                        this.Matrix[this.cX[2]][this.cY[2] + 1] = 12;
                        this.Matrix[this.cX[2]][this.cY[2] - 1] = 12;
                        this.Matrix[this.cX[2] + 1][this.cY[2] - 1] = 12;
                    }
                    if ((this.dt[2] == 0 || this.dt[2] == 2) && this.sd[2] == 1) {
                        this.Matrix[this.cX[2]][this.cY[2]] = 12;
                        this.Matrix[this.cX[2] - 1][this.cY[2] + 1] = 12;
                        this.Matrix[this.cX[2] - 1][this.cY[2]] = 12;
                        this.Matrix[this.cX[2] + 1][this.cY[2]] = 12;
                        this.Matrix[this.cX[2] + 1][this.cY[2] - 1] = 12;
                    }
                    if ((this.dt[2] == 1 || this.dt[2] == 3) && this.sd[2] == 1) {
                        this.Matrix[this.cX[2]][this.cY[2]] = 12;
                        this.Matrix[this.cX[2] - 1][this.cY[2] - 1] = 12;
                        this.Matrix[this.cX[2]][this.cY[2] - 1] = 12;
                        this.Matrix[this.cX[2]][this.cY[2] + 1] = 12;
                        this.Matrix[this.cX[2] + 1][this.cY[2] + 1] = 12;
                        break;
                    }
                case 3:
                    if ((this.dt[3] == 0 && this.sd[3] == 0) || (this.dt[3] == 2 && this.sd[3] == 1)) {
                        this.Matrix[this.cX[3]][this.cY[3]] = 13;
                        this.Matrix[this.cX[3] - 1][this.cY[3]] = 13;
                        this.Matrix[this.cX[3] - 1][this.cY[3] + 1] = 13;
                        this.Matrix[this.cX[3]][this.cY[3] - 1] = 13;
                        this.Matrix[this.cX[3] + 1][this.cY[3] - 1] = 13;
                    }
                    if ((this.dt[3] == 1 && this.sd[3] == 0) || (this.dt[3] == 3 && this.sd[3] == 1)) {
                        this.Matrix[this.cX[3]][this.cY[3]] = 13;
                        this.Matrix[this.cX[3] - 1][this.cY[3] - 1] = 13;
                        this.Matrix[this.cX[3]][this.cY[3] - 1] = 13;
                        this.Matrix[this.cX[3] + 1][this.cY[3]] = 13;
                        this.Matrix[this.cX[3] + 1][this.cY[3] + 1] = 13;
                    }
                    if ((this.dt[3] == 2 && this.sd[3] == 0) || (this.dt[3] == 0 && this.sd[3] == 1)) {
                        this.Matrix[this.cX[3]][this.cY[3]] = 13;
                        this.Matrix[this.cX[3] - 1][this.cY[3] + 1] = 13;
                        this.Matrix[this.cX[3]][this.cY[3] + 1] = 13;
                        this.Matrix[this.cX[3] + 1][this.cY[3]] = 13;
                        this.Matrix[this.cX[3] + 1][this.cY[3] - 1] = 13;
                    }
                    if ((this.dt[3] == 3 && this.sd[3] == 0) || (this.dt[3] == 1 && this.sd[3] == 1)) {
                        this.Matrix[this.cX[3]][this.cY[3]] = 13;
                        this.Matrix[this.cX[3] - 1][this.cY[3]] = 13;
                        this.Matrix[this.cX[3] - 1][this.cY[3] - 1] = 13;
                        this.Matrix[this.cX[3]][this.cY[3] + 1] = 13;
                        this.Matrix[this.cX[3] + 1][this.cY[3] + 1] = 13;
                        break;
                    }
                case 4:
                    if (this.dt[4] == 0 && this.sd[4] == 0) {
                        this.Matrix[this.cX[4]][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] - 2][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] - 1][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] + 1][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] + 1][this.cY[4] + 1] = 14;
                    }
                    if (this.dt[4] == 1 && this.sd[4] == 0) {
                        this.Matrix[this.cX[4]][this.cY[4]] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] - 2] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] - 1] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] + 1] = 14;
                        this.Matrix[this.cX[4] - 1][this.cY[4] + 1] = 14;
                    }
                    if (this.dt[4] == 2 && this.sd[4] == 0) {
                        this.Matrix[this.cX[4]][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] + 2][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] + 1][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] - 1][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] - 1][this.cY[4] - 1] = 14;
                    }
                    if (this.dt[4] == 3 && this.sd[4] == 0) {
                        this.Matrix[this.cX[4]][this.cY[4]] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] + 2] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] + 1] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] - 1] = 14;
                        this.Matrix[this.cX[4] + 1][this.cY[4] - 1] = 14;
                    }
                    if (this.dt[4] == 0 && this.sd[4] == 1) {
                        this.Matrix[this.cX[4]][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] - 2][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] - 1][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] + 1][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] + 1][this.cY[4] - 1] = 14;
                    }
                    if (this.dt[4] == 1 && this.sd[4] == 1) {
                        this.Matrix[this.cX[4]][this.cY[4]] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] - 2] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] - 1] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] + 1] = 14;
                        this.Matrix[this.cX[4] + 1][this.cY[4] + 1] = 14;
                    }
                    if (this.dt[4] == 2 && this.sd[4] == 1) {
                        this.Matrix[this.cX[4]][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] + 2][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] + 1][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] - 1][this.cY[4]] = 14;
                        this.Matrix[this.cX[4] - 1][this.cY[4] + 1] = 14;
                    }
                    if (this.dt[4] == 3 && this.sd[4] == 1) {
                        this.Matrix[this.cX[4]][this.cY[4]] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] + 2] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] + 1] = 14;
                        this.Matrix[this.cX[4]][this.cY[4] - 1] = 14;
                        this.Matrix[this.cX[4] - 1][this.cY[4] - 1] = 14;
                        break;
                    }
                case 5:
                    if (this.dt[5] == 0 && this.sd[5] == 0) {
                        this.Matrix[this.cX[5]][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] - 2][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] - 1][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] + 1][this.cY[5]] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] + 1] = 15;
                    }
                    if (this.dt[5] == 1 && this.sd[5] == 0) {
                        this.Matrix[this.cX[5]][this.cY[5]] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] - 2] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] - 1] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] + 1] = 15;
                        this.Matrix[this.cX[5] - 1][this.cY[5]] = 15;
                    }
                    if (this.dt[5] == 2 && this.sd[5] == 0) {
                        this.Matrix[this.cX[5]][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] + 2][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] + 1][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] - 1][this.cY[5]] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] - 1] = 15;
                    }
                    if (this.dt[5] == 3 && this.sd[5] == 0) {
                        this.Matrix[this.cX[5]][this.cY[5]] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] + 2] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] + 1] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] - 1] = 15;
                        this.Matrix[this.cX[5] + 1][this.cY[5]] = 15;
                    }
                    if (this.dt[5] == 0 && this.sd[5] == 1) {
                        this.Matrix[this.cX[5]][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] - 2][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] - 1][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] + 1][this.cY[5]] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] - 1] = 15;
                    }
                    if (this.dt[5] == 1 && this.sd[5] == 1) {
                        this.Matrix[this.cX[5]][this.cY[5]] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] - 2] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] - 1] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] + 1] = 15;
                        this.Matrix[this.cX[5] + 1][this.cY[5]] = 15;
                    }
                    if (this.dt[5] == 2 && this.sd[5] == 1) {
                        this.Matrix[this.cX[5]][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] + 2][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] + 1][this.cY[5]] = 15;
                        this.Matrix[this.cX[5] - 1][this.cY[5]] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] + 1] = 15;
                    }
                    if (this.dt[5] == 3 && this.sd[5] == 1) {
                        this.Matrix[this.cX[5]][this.cY[5]] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] + 2] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] + 1] = 15;
                        this.Matrix[this.cX[5]][this.cY[5] - 1] = 15;
                        this.Matrix[this.cX[5] - 1][this.cY[5]] = 15;
                        break;
                    }
                case 6:
                    if (this.dt[6] == 0 && this.sd[6] == 0) {
                        this.Matrix[this.cX[6]][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6]] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] + 1] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] - 1] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6] - 1] = 16;
                    }
                    if (this.dt[6] == 1 && this.sd[6] == 0) {
                        this.Matrix[this.cX[6]][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6]] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] - 1] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6] + 1] = 16;
                    }
                    if (this.dt[6] == 2 && this.sd[6] == 0) {
                        this.Matrix[this.cX[6]][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6] + 1] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] + 1] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] - 1] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6]] = 16;
                    }
                    if (this.dt[6] == 3 && this.sd[6] == 0) {
                        this.Matrix[this.cX[6]][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6] - 1] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6]] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] + 1] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6]] = 16;
                    }
                    if (this.dt[6] == 0 && this.sd[6] == 1) {
                        this.Matrix[this.cX[6]][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6] - 1] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] - 1] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] + 1] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6]] = 16;
                    }
                    if (this.dt[6] == 1 && this.sd[6] == 1) {
                        this.Matrix[this.cX[6]][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6]] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] + 1] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6] - 1] = 16;
                    }
                    if (this.dt[6] == 2 && this.sd[6] == 1) {
                        this.Matrix[this.cX[6]][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6]] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] - 1] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] + 1] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6] + 1] = 16;
                    }
                    if (this.dt[6] == 3 && this.sd[6] == 1) {
                        this.Matrix[this.cX[6]][this.cY[6]] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6] + 1] = 16;
                        this.Matrix[this.cX[6] - 1][this.cY[6]] = 16;
                        this.Matrix[this.cX[6]][this.cY[6] - 1] = 16;
                        this.Matrix[this.cX[6] + 1][this.cY[6]] = 16;
                        break;
                    }
                case 7:
                    if ((this.dt[7] == 0 && this.sd[7] == 0) || (this.dt[7] == 2 && this.sd[7] == 1)) {
                        this.Matrix[this.cX[7]][this.cY[7]] = 17;
                        this.Matrix[this.cX[7] - 1][this.cY[7] - 1] = 17;
                        this.Matrix[this.cX[7]][this.cY[7] - 1] = 17;
                        this.Matrix[this.cX[7] + 1][this.cY[7] - 1] = 17;
                        this.Matrix[this.cX[7]][this.cY[7] + 1] = 17;
                    }
                    if ((this.dt[7] == 1 && this.sd[7] == 0) || (this.dt[7] == 3 && this.sd[7] == 1)) {
                        this.Matrix[this.cX[7]][this.cY[7]] = 17;
                        this.Matrix[this.cX[7] - 1][this.cY[7]] = 17;
                        this.Matrix[this.cX[7] + 1][this.cY[7] - 1] = 17;
                        this.Matrix[this.cX[7] + 1][this.cY[7]] = 17;
                        this.Matrix[this.cX[7] + 1][this.cY[7] + 1] = 17;
                    }
                    if ((this.dt[7] == 2 && this.sd[7] == 0) || (this.dt[7] == 0 && this.sd[7] == 1)) {
                        this.Matrix[this.cX[7]][this.cY[7]] = 17;
                        this.Matrix[this.cX[7]][this.cY[7] - 1] = 17;
                        this.Matrix[this.cX[7] - 1][this.cY[7] + 1] = 17;
                        this.Matrix[this.cX[7]][this.cY[7] + 1] = 17;
                        this.Matrix[this.cX[7] + 1][this.cY[7] + 1] = 17;
                    }
                    if ((this.dt[7] == 3 && this.sd[7] == 0) || (this.dt[7] == 1 && this.sd[7] == 1)) {
                        this.Matrix[this.cX[7]][this.cY[7]] = 17;
                        this.Matrix[this.cX[7] + 1][this.cY[7]] = 17;
                        this.Matrix[this.cX[7] - 1][this.cY[7] - 1] = 17;
                        this.Matrix[this.cX[7] - 1][this.cY[7]] = 17;
                        this.Matrix[this.cX[7] - 1][this.cY[7] + 1] = 17;
                        break;
                    }
                case 8:
                    if (this.dt[8] == 0 && this.sd[8] == 0) {
                        this.Matrix[this.cX[8]][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8] + 1] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8] + 1] = 18;
                    }
                    if (this.dt[8] == 1 && this.sd[8] == 0) {
                        this.Matrix[this.cX[8]][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8]][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8]][this.cY[8] + 1] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8] + 1] = 18;
                    }
                    if (this.dt[8] == 2 && this.sd[8] == 0) {
                        this.Matrix[this.cX[8]][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8] - 1] = 18;
                    }
                    if (this.dt[8] == 3 && this.sd[8] == 0) {
                        this.Matrix[this.cX[8]][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8]][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8]][this.cY[8] + 1] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8] + 1] = 18;
                    }
                    if (this.dt[8] == 2 && this.sd[8] == 1) {
                        this.Matrix[this.cX[8]][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8] + 1] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8] + 1] = 18;
                    }
                    if (this.dt[8] == 3 && this.sd[8] == 1) {
                        this.Matrix[this.cX[8]][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8]][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8]][this.cY[8] + 1] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8] + 1] = 18;
                    }
                    if (this.dt[8] == 0 && this.sd[8] == 1) {
                        this.Matrix[this.cX[8]][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8] - 1][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8] - 1] = 18;
                    }
                    if (this.dt[8] == 1 && this.sd[8] == 1) {
                        this.Matrix[this.cX[8]][this.cY[8]] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8]][this.cY[8] - 1] = 18;
                        this.Matrix[this.cX[8]][this.cY[8] + 1] = 18;
                        this.Matrix[this.cX[8] + 1][this.cY[8] + 1] = 18;
                        break;
                    }
                case 9:
                    if (this.dt[9] == 0 && this.sd[9] == 0) {
                        this.Matrix[this.cX[9]][this.cY[9]] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] + 2] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] + 1] = 19;
                        this.Matrix[this.cX[9] + 1][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] + 2][this.cY[9]] = 19;
                    }
                    if (this.dt[9] == 1 && this.sd[9] == 0) {
                        this.Matrix[this.cX[9]][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] - 2][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] - 1][this.cY[9]] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] + 1] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] + 2] = 19;
                    }
                    if (this.dt[9] == 2 && this.sd[9] == 0) {
                        this.Matrix[this.cX[9]][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] - 2][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] - 1][this.cY[9]] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] - 1] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] - 2] = 19;
                    }
                    if (this.dt[9] == 3 && this.sd[9] == 0) {
                        this.Matrix[this.cX[9]][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] + 2][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] + 1][this.cY[9]] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] - 1] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] - 2] = 19;
                    }
                    if (this.dt[9] == 3 && this.sd[9] == 1) {
                        this.Matrix[this.cX[9]][this.cY[9]] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] + 2] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] + 1] = 19;
                        this.Matrix[this.cX[9] + 1][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] + 2][this.cY[9]] = 19;
                    }
                    if (this.dt[9] == 0 && this.sd[9] == 1) {
                        this.Matrix[this.cX[9]][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] - 2][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] - 1][this.cY[9]] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] + 1] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] + 2] = 19;
                    }
                    if (this.dt[9] == 1 && this.sd[9] == 1) {
                        this.Matrix[this.cX[9]][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] - 2][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] - 1][this.cY[9]] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] - 1] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] - 2] = 19;
                    }
                    if (this.dt[9] == 2 && this.sd[9] == 1) {
                        this.Matrix[this.cX[9]][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] + 2][this.cY[9]] = 19;
                        this.Matrix[this.cX[9] + 1][this.cY[9]] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] - 1] = 19;
                        this.Matrix[this.cX[9]][this.cY[9] - 2] = 19;
                        break;
                    }
                case 10:
                    if (this.dt[10] == 0 || this.dt[10] == 2) {
                        this.Matrix[this.cX[10]][this.cY[10]] = 20;
                        this.Matrix[this.cX[10] - 2][this.cY[10]] = 20;
                        this.Matrix[this.cX[10] - 1][this.cY[10]] = 20;
                        this.Matrix[this.cX[10] + 1][this.cY[10]] = 20;
                        this.Matrix[this.cX[10] + 2][this.cY[10]] = 20;
                    }
                    if (this.dt[10] != 1 && this.dt[10] != 3) {
                        break;
                    } else {
                        this.Matrix[this.cX[10]][this.cY[10]] = 20;
                        this.Matrix[this.cX[10]][this.cY[10] - 2] = 20;
                        this.Matrix[this.cX[10]][this.cY[10] - 1] = 20;
                        this.Matrix[this.cX[10]][this.cY[10] + 1] = 20;
                        this.Matrix[this.cX[10]][this.cY[10] + 2] = 20;
                        break;
                    }
                    break;
                case 11:
                    this.Matrix[this.cX[11]][this.cY[11]] = 21;
                    this.Matrix[this.cX[11] - 1][this.cY[11]] = 21;
                    this.Matrix[this.cX[11]][this.cY[11] - 1] = 21;
                    this.Matrix[this.cX[11] + 1][this.cY[11]] = 21;
                    this.Matrix[this.cX[11]][this.cY[11] + 1] = 21;
                    break;
            }
        }
        for (int ii = 2; ii < 22; ii++) {
            for (int jj = 2; jj < 11; jj++) {
                switch (this.Matrix[ii][jj]) {
                    case 0:
                        paint.setColor(-16777216);
                        break;
                    case 10:
                        paint.setColor(this.color0);
                        break;
                    case 11:
                        paint.setColor(this.color1);
                        break;
                    case 12:
                        paint.setColor(this.color2);
                        break;
                    case 13:
                        paint.setColor(this.color3);
                        break;
                    case 14:
                        paint.setColor(this.color4);
                        break;
                    case 15:
                        paint.setColor(this.color5);
                        break;
                    case 16:
                        paint.setColor(this.color6);
                        break;
                    case 17:
                        paint.setColor(this.color7);
                        break;
                    case 18:
                        paint.setColor(this.color8);
                        break;
                    case 19:
                        paint.setColor(this.color9);
                        break;
                    case 20:
                        paint.setColor(this.color10);
                        break;
                    case 21:
                        paint.setColor(this.color11);
                        break;
                    case 99:
                        paint.setColor(Color.argb(0, 0, 0, 0));
                        break;
                }
                canvas.drawRect((float) ((ii - 2) * this.dw), (float) ((jj - 2) * this.dh), (float) (((ii - 2) * this.dw) + this.dw), (float) (((jj - 2) * this.dh) + this.dh), paint);
            }
        }
        paint.setColor(Color.argb(255, 20, 20, 20));
        paint.setTextSize(25.0f);
        canvas.drawText("+", (float) (((this.cX[this.select] - 2) * this.dw) + (this.dw / 3)), (float) (((this.cY[this.select] - 2) * this.dh) + ((this.dh * 2) / 3)), paint);
        int blackcounter = 0;
        for (int i6 = 0; i6 < this.baseData[this.baseNum].length; i6++) {
            for (int j3 = 0; j3 < 20; j3++) {
                if (this.Matrix[j3 + 2][i6 + 2] == 0) {
                    blackcounter++;
                }
            }
        }
        paint.setColor(Color.argb(255, 150, 80, 20));
        if (blackcounter == 0) {
            this.goalMode = true;
        }
        paint.setColor(Color.argb(255, 0, 0, 0));
        paint.setTextSize(24.0f);
        canvas.drawText("<" + this.baseNum + ">", (float) (this.dw / 2), (float) (this.dh / 2), paint);
        if (this.bookcoverMode) {
            Rect rect4 = new Rect(0, 0, this.putcover.getWidth(), this.putcover.getHeight());
            Rect dst4 = new Rect(0, 0, getWidth(), getHeight());
            canvas.drawBitmap(this.putcover, rect4, dst4, (Paint) null);
            paint.setColor(Color.argb(255, 4, 0, 0));
            paint.setTextSize((float) ((this.hH * 100) / 600));
            canvas.drawText("Pentomino", (float) ((this.dw * 5) + 4), (float) ((this.dh * 5) + 4), paint);
            paint.setColor(Color.argb(255, 254, 0, 0));
            canvas.drawText("Pentomino", (float) (this.dw * 5), (float) (this.dh * 5), paint);
        }
        if (this.baseNum == 0 && !this.bookcoverMode && !this.goalMode) {
            paint.setTextSize((float) ((this.wW * 24) / 1000));
            paint.setColor(Color.argb(255, 5, 4, 5));
            canvas.drawText("Please, cover the black area with 12 colored pieces,", (float) (this.dw * 4), (float) (this.dh * 8), paint);
            canvas.drawText("by sliding pieces, Rotate,Turn Over.", (float) ((this.dw * 4) - (this.dw / 2)), (float) ((this.dh * 8) + (this.dh / 2)), paint);
        }
        if (this.goalMode) {
            if ((this.baseNum >= 1 && this.baseNum <= 10) || ((this.baseNum >= 17 && this.baseNum < 26) || (this.baseNum >= 36 && this.baseNum <= 46))) {
                for (int ii2 = 0; ii2 < 10; ii2++) {
                    for (int jj2 = 0; jj2 < 20; jj2++) {
                        this.compression[jj2][ii2] = 0;
                    }
                }
                for (int i7 = 0; i7 < 10; i7++) {
                    int k3 = 0;
                    for (int j4 = 0; j4 < 20; j4++) {
                        if (!(this.Matrix[j4 + 2][i7 + 2] == 0 || this.Matrix[j4 + 2][i7 + 2] == 99)) {
                            this.compression[k3][i7] = this.Matrix[j4 + 2][i7 + 2];
                            k3++;
                        }
                    }
                }
                for (int j5 = 0; j5 < 20; j5++) {
                    int k4 = 0;
                    for (int i8 = 0; i8 < 10; i8++) {
                        if (this.compression[j5][i8] != 0) {
                            this.compression[j5][k4] = this.compression[j5][i8];
                            k4++;
                            this.compression[j5][i8] = 0;
                        }
                    }
                }
                for (int ii3 = 0; ii3 < 16; ii3++) {
                    for (int jj3 = 0; jj3 < 9; jj3++) {
                        switch (this.compression[ii3][jj3]) {
                            case 0:
                                paint.setColor(-16777216);
                                break;
                            case 10:
                                paint.setColor(this.color0);
                                break;
                            case 11:
                                paint.setColor(this.color1);
                                break;
                            case 12:
                                paint.setColor(this.color2);
                                break;
                            case 13:
                                paint.setColor(this.color3);
                                break;
                            case 14:
                                paint.setColor(this.color4);
                                break;
                            case 15:
                                paint.setColor(this.color5);
                                break;
                            case 16:
                                paint.setColor(this.color6);
                                break;
                            case 17:
                                paint.setColor(this.color7);
                                break;
                            case 18:
                                paint.setColor(this.color8);
                                break;
                            case 19:
                                paint.setColor(this.color9);
                                break;
                            case 20:
                                paint.setColor(this.color10);
                                break;
                            case 21:
                                paint.setColor(this.color11);
                                break;
                        }
                        canvas.drawRect((float) (((this.dw * ii3) / 3) + this.dw), (float) (((this.dh * jj3) / 3) + (this.dh * 8)), (float) (((this.dw * ii3) / 3) + (this.dw / 3) + 1 + this.dw), (float) (((this.dh * jj3) / 3) + (this.dh / 3) + 1 + (this.dh * 8)), paint);
                    }
                }
            }
            if ((this.baseNum >= 11 && this.baseNum <= 15) || ((this.baseNum >= 27 && this.baseNum <= 31) || (this.baseNum >= 33 && this.baseNum <= 35))) {
                for (int ii4 = 0; ii4 < 10; ii4++) {
                    for (int jj4 = 0; jj4 < 20; jj4++) {
                        this.compression[jj4][ii4] = 0;
                    }
                }
                for (int j6 = 0; j6 < 20; j6++) {
                    int k5 = 0;
                    for (int i9 = 0; i9 < 10; i9++) {
                        if (!(this.Matrix[j6 + 2][i9 + 2] == 0 || this.Matrix[j6 + 2][i9 + 2] == 99)) {
                            this.compression[j6][k5] = this.Matrix[j6 + 2][i9 + 2];
                            k5++;
                        }
                    }
                }
                for (int i10 = 0; i10 < 10; i10++) {
                    int k6 = 0;
                    for (int j7 = 0; j7 < 20; j7++) {
                        if (this.compression[j7][i10] != 0) {
                            this.compression[k6][i10] = this.compression[j7][i10];
                            k6++;
                            this.compression[j7][i10] = 0;
                        }
                    }
                }
                for (int ii5 = 0; ii5 < 15; ii5++) {
                    for (int jj5 = 0; jj5 < 9; jj5++) {
                        switch (this.compression[ii5][jj5]) {
                            case 0:
                                paint.setColor(-16777216);
                                break;
                            case 10:
                                paint.setColor(this.color0);
                                break;
                            case 11:
                                paint.setColor(this.color1);
                                break;
                            case 12:
                                paint.setColor(this.color2);
                                break;
                            case 13:
                                paint.setColor(this.color3);
                                break;
                            case 14:
                                paint.setColor(this.color4);
                                break;
                            case 15:
                                paint.setColor(this.color5);
                                break;
                            case 16:
                                paint.setColor(this.color6);
                                break;
                            case 17:
                                paint.setColor(this.color7);
                                break;
                            case 18:
                                paint.setColor(this.color8);
                                break;
                            case 19:
                                paint.setColor(this.color9);
                                break;
                            case 20:
                                paint.setColor(this.color10);
                                break;
                            case 21:
                                paint.setColor(this.color11);
                                break;
                        }
                        canvas.drawRect((float) (((this.dw * ii5) / 3) + this.dw), (float) (((this.dh * jj5) / 3) + (this.dh * 8)), (float) (((this.dw * ii5) / 3) + (this.dw / 3) + 1 + this.dw), (float) (((this.dh * jj5) / 3) + (this.dh / 3) + 1 + (this.dh * 8)), paint);
                    }
                }
            }
            paint.setColor(Color.argb(255, 254, 254, 254));
            paint.setTextSize((float) ((this.wW * 25) / 1000));
            canvas.drawText(" Prev       Next", (float) ((this.dw * 5) + (this.dw / 2)), (float) ((this.dh * 10) + (this.dh / 2)), paint);
            paint.setTextSize((float) ((this.wW * 24) / 1000));
            paint.setColor(Color.argb(255, 225, 124, 230));
            for (int kk = 0; kk < 6; kk++) {
                canvas.drawText("+", (float) (this.dw * (this.rnd.nextInt(16) + 1)), (float) (this.dh * (this.rnd.nextInt(10) + 1)), paint);
            }
            for (int k7 = 0; k7 < 10000000; k7++) {
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.touchX = (int) event.getX();
        this.touchY = (int) event.getY();
        this.touchAction = event.getAction();
        return true;
    }
}
