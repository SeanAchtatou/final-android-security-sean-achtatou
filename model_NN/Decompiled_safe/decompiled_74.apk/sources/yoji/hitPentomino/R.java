package yoji.hitPentomino;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int answerground = 2130837504;
        public static final int answerground2 = 2130837505;
        public static final int centwhite = 2130837506;
        public static final int goalground = 2130837507;
        public static final int icon = 2130837508;
        public static final int iconb = 2130837509;
        public static final int pentground = 2130837510;
        public static final int putcover = 2130837511;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
