package jp.co.nobot.libAdMaker;

import android.content.Intent;
import android.net.Uri;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Date;

final class b extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ libAdMaker f79a;

    b(libAdMaker libadmaker) {
        this.f79a = libadmaker;
    }

    public final void onLoadResource(WebView webView, String str) {
        int indexOf;
        String str2;
        if (this.f79a.f) {
            webView.stopLoading();
        } else if (this.f79a.h()) {
            if (str.indexOf("http://images.ad-maker.info/apps") == 0) {
                try {
                    str2 = d.a(String.format("%s%s%s", this.f79a.l, Long.valueOf(new Date().getTime()), "mXFTQ9fp73rqK5aaOAuQ8yP8"));
                } catch (Exception e) {
                    str2 = "";
                }
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.f79a.p);
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.f79a.q);
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.f79a.r);
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.f79a.s);
                CookieManager.getInstance().setCookie("images.ad-maker.info", this.f79a.t);
                CookieManager.getInstance().setCookie("images.ad-maker.info", "admaker_sgt=" + str2 + "; domain=" + "images.ad-maker.info");
                CookieSyncManager.getInstance().sync();
            }
            int indexOf2 = str.indexOf("expand.html");
            if (str.indexOf("exp=2") != -1 && !this.f79a.g) {
                this.f79a.g = true;
                this.f79a.c.loadUrl(str);
                this.f79a.c();
            }
            if (indexOf2 == -1 && (indexOf = str.indexOf("www/delivery/ck.php?oaparams")) != -1 && indexOf < 35) {
                try {
                    webView.stopLoading();
                    this.f79a.h.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public final void onPageFinished(WebView webView, String str) {
        String title = webView.getTitle();
        if ((title != null ? title.indexOf("404") : -1) != -1) {
            this.f79a.setVisibility(8);
            if (this.f79a.u != null) {
                this.f79a.u.a();
                return;
            }
            return;
        }
        this.f79a.k = true;
        CookieSyncManager.getInstance().sync();
        str.indexOf("exp=2");
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        this.f79a.setVisibility(8);
        if (this.f79a.u != null) {
            this.f79a.u.a();
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            webView.stopLoading();
            this.f79a.h.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }
}
