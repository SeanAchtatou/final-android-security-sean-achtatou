package jp.Adlantis.Android;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.net.MalformedURLException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;

final class af extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ DefaultRedirectHandler f46a;
    private /* synthetic */ String b;
    private /* synthetic */ q c;

    af(q qVar, DefaultRedirectHandler defaultRedirectHandler, String str) {
        this.c = qVar;
        this.f46a = defaultRedirectHandler;
        this.b = str;
    }

    public final void run() {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            defaultHttpClient.setRedirectHandler(this.f46a);
            String uri = this.c.a((Context) null, Uri.parse(this.b)).build().toString();
            Log.d(q.j, "handleHttpClickRequest=" + uri);
            defaultHttpClient.execute(new HttpGet(uri));
        } catch (MalformedURLException e) {
            Log.e(q.j, e.toString());
        } catch (IOException e2) {
            Log.e(q.j, e2.toString());
        }
    }
}
