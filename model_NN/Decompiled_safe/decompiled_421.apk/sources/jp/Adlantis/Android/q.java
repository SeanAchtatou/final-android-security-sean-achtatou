package jp.Adlantis.Android;

import a.a.a.a;
import a.a.a.a.d;
import a.a.a.b;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Vector;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class q {
    /* access modifiers changed from: private */
    public static String j = "AdManager";

    /* renamed from: a  reason: collision with root package name */
    public int f69a;
    public long b;
    public long c;
    private u[] d;
    private v e;
    private String[] f;
    private int g;
    private String h;
    private t i;
    private boolean k;
    private HashMap l;
    private String m;
    /* access modifiers changed from: private */
    public String n;
    /* access modifiers changed from: private */
    public boolean o;
    private boolean p;

    /* synthetic */ q() {
        this((byte) 0);
    }

    private q(byte b2) {
        this.f69a = 0;
        this.b = 300000;
        this.c = 10000;
        this.i = new t();
        this.n = null;
        this.o = false;
        this.p = false;
        this.e = new c();
    }

    private static Vector a(u[] uVarArr, int i2) {
        Vector vector = new Vector();
        if (uVarArr != null) {
            for (int i3 = 0; i3 < uVarArr.length; i3++) {
                u uVar = uVarArr[i3];
                if (uVar.a() == 2 ? true : uVar.a() == 1 ? uVar.a(i2) != null : false) {
                    vector.addElement(uVarArr[i3]);
                }
            }
        }
        return vector;
    }

    public static q a() {
        return a.f40a;
    }

    public static boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            try {
                NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
                if (allNetworkInfo != null) {
                    for (NetworkInfo isConnected : allNetworkInfo) {
                        if (isConnected.isConnected()) {
                            return true;
                        }
                    }
                }
            } catch (Exception e2) {
                Log.e(j, e2.toString());
            }
        }
        return false;
    }

    private static u[] a(InputStream inputStream) {
        Object obj;
        u[] uVarArr;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        try {
            obj = new d().a(bufferedReader);
        } catch (Exception e2) {
            Log.e(j, e2.toString());
            obj = null;
        }
        if (obj != null) {
            a aVar = obj instanceof a ? (a) obj : obj instanceof b ? (a) ((b) obj).get("ads") : null;
            if (aVar != null) {
                int size = aVar.size();
                u[] uVarArr2 = new u[size];
                for (int i2 = 0; i2 < size; i2++) {
                    uVarArr2[i2] = new u((HashMap) aVar.get(i2));
                }
                uVarArr = uVarArr2;
                bufferedReader.close();
                return uVarArr;
            }
            Log.i(j, "Adlantis: no ads received (this is not an error)");
        }
        uVarArr = null;
        bufferedReader.close();
        return uVarArr;
    }

    public static String b() {
        return "Ads by AdLantis";
    }

    private static String b(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e2) {
            Log.e(j, e2.toString());
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap c(android.content.Context r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.HashMap r0 = r3.l     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x0009
            java.util.HashMap r0 = r3.l     // Catch:{ all -> 0x00a3 }
            monitor-exit(r3)     // Catch:{ all -> 0x00a3 }
        L_0x0008:
            return r0
        L_0x0009:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x00a3 }
            r0.<init>()     // Catch:{ all -> 0x00a3 }
            r3.l = r0     // Catch:{ all -> 0x00a3 }
            if (r4 == 0) goto L_0x001d
            java.util.HashMap r0 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r1 = "appIdentifier"
            java.lang.String r2 = r4.getPackageName()     // Catch:{ all -> 0x00a3 }
            r0.put(r1, r2)     // Catch:{ all -> 0x00a3 }
        L_0x001d:
            java.util.HashMap r0 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r1 = "deviceClass"
            java.lang.String r2 = "android"
            r0.put(r1, r2)     // Catch:{ all -> 0x00a3 }
            java.lang.String r0 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x0044
            java.util.HashMap r1 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r2 = "deviceOsVersionFull"
            r1.put(r2, r0)     // Catch:{ all -> 0x00a3 }
            java.text.NumberFormat r1 = java.text.NumberFormat.getNumberInstance()     // Catch:{ ParseException -> 0x009e }
            java.lang.Number r0 = r1.parse(r0)     // Catch:{ ParseException -> 0x009e }
            java.lang.String r0 = r0.toString()     // Catch:{ ParseException -> 0x009e }
            java.util.HashMap r1 = r3.l     // Catch:{ ParseException -> 0x009e }
            java.lang.String r2 = "deviceOsVersion"
            r1.put(r2, r0)     // Catch:{ ParseException -> 0x009e }
        L_0x0044:
            java.lang.String r0 = android.os.Build.MODEL     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x0059
            java.lang.String r1 = "sdk"
            int r1 = r0.compareTo(r1)     // Catch:{ all -> 0x00a3 }
            if (r1 != 0) goto L_0x0052
            java.lang.String r0 = "simulator"
        L_0x0052:
            java.util.HashMap r1 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r2 = "deviceFamily"
            r1.put(r2, r0)     // Catch:{ all -> 0x00a3 }
        L_0x0059:
            java.lang.String r0 = android.os.Build.BRAND     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x0066
            java.util.HashMap r0 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r1 = "deviceBrand"
            java.lang.String r2 = android.os.Build.BRAND     // Catch:{ all -> 0x00a3 }
            r0.put(r1, r2)     // Catch:{ all -> 0x00a3 }
        L_0x0066:
            java.lang.String r0 = android.os.Build.BRAND     // Catch:{ all -> 0x00a3 }
            if (r0 == 0) goto L_0x0073
            java.util.HashMap r0 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r1 = "deviceName"
            java.lang.String r2 = android.os.Build.DEVICE     // Catch:{ all -> 0x00a3 }
            r0.put(r1, r2)     // Catch:{ all -> 0x00a3 }
        L_0x0073:
            java.lang.String r0 = b(r4)     // Catch:{ all -> 0x00a3 }
            r1 = 0
            if (r0 == 0) goto L_0x00a6
            java.lang.String r0 = jp.Adlantis.Android.am.a(r0)     // Catch:{ all -> 0x00a3 }
        L_0x007e:
            if (r0 == 0) goto L_0x0087
            java.util.HashMap r1 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r2 = "udid"
            r1.put(r2, r0)     // Catch:{ all -> 0x00a3 }
        L_0x0087:
            java.util.HashMap r0 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r1 = "sdkVersion"
            java.lang.String r2 = "1.3.2"
            r0.put(r1, r2)     // Catch:{ all -> 0x00a3 }
            java.util.HashMap r0 = r3.l     // Catch:{ all -> 0x00a3 }
            java.lang.String r1 = "sdkBuild"
            java.lang.String r2 = "261"
            r0.put(r1, r2)     // Catch:{ all -> 0x00a3 }
            monitor-exit(r3)     // Catch:{ all -> 0x00a3 }
            java.util.HashMap r0 = r3.l
            goto L_0x0008
        L_0x009e:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00a3 }
            goto L_0x0044
        L_0x00a3:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00a3 }
            throw r0
        L_0x00a6:
            r0 = r1
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.Adlantis.Android.q.c(android.content.Context):java.util.HashMap");
    }

    private u[] c(int i2) {
        Vector a2;
        synchronized (this) {
            a2 = a(this.d, i2);
        }
        u[] uVarArr = new u[a2.size()];
        a2.copyInto(uVarArr);
        return uVarArr;
    }

    private Uri d(Context context) {
        Uri.Builder a2 = a(context, (Uri) null);
        a2.scheme("http");
        a2.authority(this.e.b());
        a2.path("/sp/load_app_ads");
        a2.appendQueryParameter("callbackid", "0");
        a2.appendQueryParameter("zid", this.h);
        a2.appendQueryParameter("adl_app_flg", "1");
        if (this.m != null) {
            a2.appendQueryParameter("keywords", this.m);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        a2.appendQueryParameter("displaySize", displayMetrics.widthPixels + "x" + displayMetrics.heightPixels);
        a2.appendQueryParameter("displayDensity", Float.toString(displayMetrics.density));
        return a2.build();
    }

    /* access modifiers changed from: private */
    public boolean e(Context context) {
        Uri d2;
        boolean z;
        if (this.k) {
            return false;
        }
        this.k = true;
        if (this.f == null || this.f.length <= 0) {
            d2 = d(context);
        } else {
            d(context);
            d2 = Uri.parse(this.f[this.g]);
            this.g = (this.g + 1) % this.f.length;
        }
        try {
            ak akVar = new ak(this);
            HttpHost httpHost = new HttpHost(this.e.b(), 80, "http");
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            defaultHttpClient.addRequestInterceptor(akVar, 0);
            defaultHttpClient.getCredentialsProvider().setCredentials(new AuthScope(httpHost.getHostName(), httpHost.getPort()), new UsernamePasswordCredentials("3263", "0315"));
            String uri = d2.toString();
            Log.d(j, uri);
            this.d = a(defaultHttpClient.execute(new HttpGet(uri)).getEntity().getContent());
            if (this.d != null) {
                z = this.d.length > 0;
                this.k = false;
                return z;
            }
        } catch (MalformedURLException e2) {
            Log.e(j, e2.toString());
            z = false;
        } catch (IOException e3) {
            Log.e(j, e3.toString());
        }
        z = false;
        this.k = false;
        return z;
    }

    public final int a(int i2) {
        return a(this.d, i2).size();
    }

    public final Uri.Builder a(Context context, Uri uri) {
        Uri.Builder buildUpon = uri != null ? uri.buildUpon() : new Uri.Builder();
        am.a(buildUpon, c(context));
        return buildUpon;
    }

    public final Uri a(Context context, String str, boolean z) {
        Uri.Builder a2 = a(context, (Uri) null);
        a2.scheme("http");
        if (z) {
            a2.authority(this.e.d());
            a2.path("/ctt");
        } else {
            a2.authority(this.e.c());
            a2.path("/sp/conv");
        }
        a2.appendQueryParameter("tid", str);
        a2.appendQueryParameter("output", "js");
        return a2.build();
    }

    public final void a(Context context, String str) {
        if (!str.equals(this.n)) {
            this.n = str;
            this.o = false;
        }
        if (!this.o) {
            new ai(this, context, str).start();
        }
    }

    public final void a(Context context, o oVar) {
        if (!this.p) {
            context.getApplicationContext().registerReceiver(new d(this), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.p = true;
        }
        new ag(this, context, oVar, new aj(this, oVar)).start();
    }

    public final void a(String str) {
        this.e.a(str);
    }

    public final void a(String str, b bVar) {
        Uri parse = Uri.parse(str);
        String scheme = parse.getScheme();
        if (scheme.compareTo("http") == 0 || scheme.compareTo("https") == 0) {
            new af(this, new ae(this, new ah(this, bVar)), str).start();
            return;
        }
        Handler handler = new Handler();
        Message obtainMessage = handler.obtainMessage(0, parse);
        handler.sendMessage(obtainMessage);
        if (bVar != null) {
            bVar.a((Uri) obtainMessage.obj);
        }
    }

    public final void a(String[] strArr) {
        Log.d(j, "setting test AdRequestUrls");
        this.f = strArr;
    }

    public final void b(String str) {
        this.m = str;
    }

    public final u[] b(int i2) {
        return i2 == 2 ? c(2) : c(1);
    }

    public final t c() {
        return this.i;
    }

    public final void c(String str) {
        this.h = str;
    }

    public final String[] d() {
        return this.f;
    }

    public final String e() {
        return this.e.a();
    }

    public final String f() {
        return this.h;
    }
}
