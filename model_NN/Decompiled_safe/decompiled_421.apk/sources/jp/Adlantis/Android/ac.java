package jp.Adlantis.Android;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

final class ac implements b {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AdlantisView f43a;

    ac(AdlantisView adlantisView) {
        this.f43a = adlantisView;
    }

    public final void a(Uri uri) {
        boolean z = false;
        try {
            this.f43a.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
            z = true;
        } catch (ActivityNotFoundException e) {
            Log.e(AdlantisView.class.getSimpleName(), "activity not found for url=" + uri + " exception=" + e);
        }
        this.f43a.g.setVisibility(4);
        boolean unused = this.f43a.l = false;
        if (!z) {
            this.f43a.j();
        }
    }
}
