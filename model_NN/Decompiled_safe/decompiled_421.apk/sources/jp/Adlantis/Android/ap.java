package jp.Adlantis.Android;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import java.lang.ref.SoftReference;

final class ap extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ String f55a;
    private /* synthetic */ Handler b;
    private /* synthetic */ t c;

    ap(t tVar, String str, Handler handler) {
        this.c = tVar;
        this.f55a = str;
        this.b = handler;
    }

    public final void run() {
        Drawable a2 = t.a(this.f55a);
        if (a2 != null) {
            this.c.f71a.put(this.f55a, new SoftReference(a2));
            Log.d("AsyncImageLoader", "imageCache.size()=" + this.c.f71a.size());
            this.b.sendMessage(this.b.obtainMessage(0, a2));
        }
    }
}
