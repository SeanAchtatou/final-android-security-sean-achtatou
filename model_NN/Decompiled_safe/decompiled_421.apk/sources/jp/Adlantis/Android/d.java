package jp.Adlantis.Android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class d extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f58a;

    /* synthetic */ d(q qVar) {
        this(qVar, (byte) 0);
    }

    private d(q qVar, byte b) {
        this.f58a = qVar;
    }

    public final void onReceive(Context context, Intent intent) {
        if (q.a(context) && this.f58a.n != null && !this.f58a.o) {
            this.f58a.a(context, this.f58a.n);
        }
    }
}
