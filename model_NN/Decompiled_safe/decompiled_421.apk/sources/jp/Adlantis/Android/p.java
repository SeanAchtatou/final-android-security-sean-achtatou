package jp.Adlantis.Android;

import android.content.Context;
import android.text.TextPaint;
import android.widget.TextView;

final class p extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private float f68a = 9.0f;
    private float b = 20.0f;

    public p(Context context) {
        super(context);
    }

    private void a(String str, int i) {
        float f;
        if (i > 0) {
            int paddingLeft = (i - getPaddingLeft()) - getPaddingRight();
            float f2 = this.b;
            TextPaint paint = getPaint();
            while (true) {
                if (f2 <= this.f68a || paint.measureText(str) <= ((float) paddingLeft)) {
                    f = f2;
                } else {
                    f2 -= 1.0f;
                    if (f2 <= this.f68a) {
                        f = this.f68a;
                        break;
                    }
                    setTextSize(f2);
                }
            }
            setTextSize(f);
        }
    }

    public final void a(String str) {
        setTextSize(this.b);
        super.setText(str);
        a(str, getWidth());
    }

    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i, int i2, int i3, int i4) {
        if (i != i3) {
            a(getText().toString(), i);
        }
    }
}
