package jp.Adlantis.Android;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;

final class an extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ r f53a;
    private /* synthetic */ String b;
    private /* synthetic */ t c;

    an(t tVar, r rVar, String str) {
        this.c = tVar;
        this.f53a = rVar;
        this.b = str;
    }

    public final void handleMessage(Message message) {
        if (this.f53a != null) {
            this.f53a.a((Drawable) message.obj, this.b);
        }
    }
}
