package jp.Adlantis.Android;

import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.protocol.HttpContext;

final class ak implements HttpRequestInterceptor {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f51a;

    ak(q qVar) {
        this.f51a = qVar;
    }

    public final void process(HttpRequest httpRequest, HttpContext httpContext) {
        Credentials credentials;
        AuthState authState = (AuthState) httpContext.getAttribute("http.auth.target-scope");
        CredentialsProvider credentialsProvider = (CredentialsProvider) httpContext.getAttribute("http.auth.credentials-provider");
        HttpHost httpHost = (HttpHost) httpContext.getAttribute("http.target_host");
        if (authState.getAuthScheme() == null && (credentials = credentialsProvider.getCredentials(new AuthScope(httpHost.getHostName(), httpHost.getPort()))) != null) {
            authState.setAuthScheme(new BasicScheme());
            authState.setCredentials(credentials);
        }
    }
}
