package jp.Adlantis.Android;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;
import java.util.Map;

class AdlantisAdView extends ViewSwitcher {

    /* renamed from: a  reason: collision with root package name */
    private u f38a;
    private ImageView b;
    private p c;
    private ViewFlipper d;
    private ImageView e;
    private TextView f;

    public AdlantisAdView(Context context) {
        super(context);
        a();
    }

    public AdlantisAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        this.d = new AdlantisViewFlipper(getContext());
        addView(this.d, 0, new ViewGroup.LayoutParams(-1, -1));
        this.d.setInAnimation(AdlantisView.a());
        this.d.setOutAnimation(AdlantisView.b());
        this.e = new ImageView(getContext());
        this.e.setScaleType(ImageView.ScaleType.FIT_CENTER);
        this.d.addView(this.e, 0, new ViewGroup.LayoutParams(-1, -1));
        this.f = new TextView(getContext());
        this.f.setTextSize(20.0f);
        this.f.setTextColor(-1);
        this.f.setGravity(17);
        this.d.addView(this.f, 1, new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        addView(relativeLayout, 1, new ViewGroup.LayoutParams(-1, -1));
        float b2 = am.b(getContext());
        this.b = new ImageView(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (32.0f * b2), (int) (32.0f * b2));
        layoutParams.addRule(15, -1);
        layoutParams.setMargins((int) (5.0f * b2), 0, 0, 0);
        relativeLayout.addView(this.b, layoutParams);
        this.c = new p(getContext());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(13, -1);
        layoutParams2.addRule(9, -1);
        layoutParams2.setMargins((int) (42.0f * b2), 0, 0, 0);
        this.c.setTextSize(20.0f);
        this.c.setTextColor(-1);
        this.c.setLines(1);
        this.c.setMaxLines(1);
        relativeLayout.addView(this.c, layoutParams2);
        TextView textView = new TextView(getContext());
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(11, -1);
        layoutParams3.addRule(12, -1);
        layoutParams3.setMargins(0, 0, (int) (4.0f * b2), (int) (b2 * 1.0f));
        textView.setText(q.b());
        textView.setTextSize(12.0f);
        textView.setTextColor(-1);
        relativeLayout.addView(textView, layoutParams3);
    }

    /* access modifiers changed from: private */
    public void a(Drawable drawable) {
        if (this.b != null) {
            this.b.setImageDrawable(drawable);
        }
    }

    /* access modifiers changed from: private */
    public void a(Drawable drawable, boolean z) {
        View currentView = this.d.getCurrentView();
        if (this.e != null) {
            this.e.setImageDrawable(drawable);
        }
        if (this.e != currentView && drawable != null) {
            if (z) {
                this.d.showNext();
            } else {
                this.d.setDisplayedChild(0);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.Adlantis.Android.AdlantisAdView.a(android.graphics.drawable.Drawable, boolean):void
     arg types: [android.graphics.drawable.Drawable, int]
     candidates:
      jp.Adlantis.Android.AdlantisAdView.a(jp.Adlantis.Android.AdlantisAdView, android.graphics.drawable.Drawable):void
      jp.Adlantis.Android.AdlantisAdView.a(android.graphics.drawable.Drawable, boolean):void */
    public final void a(int i) {
        int length;
        String str;
        Map map;
        u[] b2 = q.a().b(am.a(this));
        if (b2 != null && (length = b2.length) != 0) {
            int i2 = i >= length ? 0 : i;
            u uVar = b2[i2];
            q.a().f69a = i2;
            u uVar2 = b2[(i2 + 1) % length];
            if (this.f38a != null) {
                this.f38a.d();
            }
            this.f38a = uVar;
            this.f38a.c();
            int a2 = this.f38a.a();
            if (a2 == 1) {
                setDisplayedChild(0);
                this.f.setText(this.f38a.d(this));
                Drawable a3 = q.a().c().a(this.f38a.a(this), new n(this));
                a(a3, false);
                if (a3 == null && this.f != this.d.getCurrentView()) {
                    this.d.setDisplayedChild(1);
                }
            } else if (a2 == 2) {
                setDisplayedChild(1);
                t c2 = q.a().c();
                u uVar3 = this.f38a;
                boolean a4 = am.a(getContext());
                if (uVar3.a() != 2 || (map = (Map) uVar3.get("iphone_icon")) == null) {
                    str = null;
                } else {
                    String str2 = a4 ? (String) map.get("src_2x") : null;
                    str = str2 == null ? (String) map.get("src") : str2;
                }
                a(c2.a(str, new i(this)));
                this.c.a(Uri.decode((String) this.f38a.get("string")));
            }
            q.a().c().a(uVar2.c(this), null);
        }
    }
}
