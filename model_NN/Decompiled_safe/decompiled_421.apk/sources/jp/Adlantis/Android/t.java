package jp.Adlantis.Android;

import android.graphics.drawable.Drawable;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;

public final class t {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public HashMap f71a = new HashMap();

    public static Drawable a(String str) {
        InputStream inputStream;
        Log.d("AsyncImageLoader", "loadImageFromUrl=" + str);
        if (str == null) {
            return null;
        }
        try {
            inputStream = new URL(str).openStream();
        } catch (IOException e) {
            System.out.println(e);
            inputStream = null;
        }
        if (inputStream == null) {
            return null;
        }
        try {
            return Drawable.createFromStream(inputStream, "src");
        } catch (OutOfMemoryError e2) {
            Log.e("AsyncImageLoader", "exception calling Drawable.createFromStream() " + e2);
            return null;
        }
    }

    public final Drawable a(String str, r rVar) {
        Drawable drawable;
        if (this.f71a.containsKey(str) && (drawable = (Drawable) ((SoftReference) this.f71a.get(str)).get()) != null) {
            return drawable;
        }
        new ap(this, str, new an(this, rVar, str)).start();
        return null;
    }
}
