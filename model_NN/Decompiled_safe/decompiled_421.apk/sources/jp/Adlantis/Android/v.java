package jp.Adlantis.Android;

public abstract class v {

    /* renamed from: a  reason: collision with root package name */
    protected String f73a;
    protected String b;
    protected String c;

    public abstract String a();

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.f73a = str;
    }

    public final String b() {
        return this.f73a;
    }

    public final String c() {
        return this.b;
    }

    public final String d() {
        return this.c;
    }
}
