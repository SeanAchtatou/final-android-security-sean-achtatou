package jp.Adlantis.Android;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ViewFlipper;

public class AdlantisViewFlipper extends ViewFlipper {
    public AdlantisViewFlipper(Context context) {
        super(context);
    }

    public AdlantisViewFlipper(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            Log.d("AdlantisViewFlipper", "AdlantisViewFlipper ignoring IllegalArgumentException");
            stopFlipping();
        }
    }
}
