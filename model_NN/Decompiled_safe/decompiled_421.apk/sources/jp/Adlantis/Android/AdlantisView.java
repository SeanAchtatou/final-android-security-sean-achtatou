package jp.Adlantis.Android;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

public class AdlantisView extends RelativeLayout {
    private static int k = -939524096;
    private static int o = 500;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f39a = 0;
    /* access modifiers changed from: private */
    public Handler b = new Handler();
    private long c = 5000;
    private ViewFlipper d;
    /* access modifiers changed from: private */
    public View e;
    private AdlantisAdView[] f;
    /* access modifiers changed from: private */
    public ProgressBar g;
    private boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    private int j = 0;
    /* access modifiers changed from: private */
    public boolean l;
    private Runnable m = new y(this);
    private Runnable n = new z(this);

    public AdlantisView(Context context) {
        super(context);
        m();
    }

    public AdlantisView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/jp.Adlantis.Android", "publisherID");
        if (attributeValue != null) {
            a(attributeValue);
        }
        m();
    }

    static Animation a() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration((long) o);
        return alphaAnimation;
    }

    private void a(boolean z) {
        if (z != this.h) {
            if (z) {
                this.e.setVisibility(0);
                View view = this.e;
                Animation a2 = a();
                a2.setDuration(150);
                a2.setAnimationListener(new ad(this));
                view.startAnimation(a2);
                setPressed(true);
            } else {
                View view2 = this.e;
                Animation b2 = b();
                b2.setDuration(150);
                b2.setAnimationListener(new aa(this));
                view2.startAnimation(b2);
                setPressed(false);
            }
            this.h = z;
        }
    }

    private boolean a(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        boolean z = x >= 0.0f && x <= ((float) getWidth()) && y >= 0.0f && y <= ((float) getHeight());
        return z ? motionEvent.getEdgeFlags() == 0 : z;
    }

    static Animation b() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration((long) o);
        return alphaAnimation;
    }

    static /* synthetic */ void b(AdlantisView adlantisView) {
        int h2 = adlantisView.h();
        if (adlantisView.d == null) {
            Log.w("AdlantisView", "adCountChanged called when _rootViewFlipper is not available");
        } else if (h2 > 0 && adlantisView.j == 0) {
            adlantisView.d.setVisibility(0);
            adlantisView.d.startAnimation(a());
        } else if (h2 == 0 && adlantisView.j > 0) {
            adlantisView.d.startAnimation(b());
            adlantisView.d.setVisibility(4);
        }
        adlantisView.j = h2;
    }

    private int h() {
        return q.a().a(am.a(this));
    }

    /* access modifiers changed from: private */
    public boolean i() {
        return h() > 0;
    }

    /* access modifiers changed from: private */
    public void j() {
        if (i()) {
            this.b.removeCallbacks(this.m);
            this.b.postDelayed(this.m, q.a().c);
        }
        this.b.removeCallbacks(this.n);
        this.b.postDelayed(this.n, q.a().b);
    }

    private void k() {
        this.b.removeCallbacks(this.m);
        this.b.removeCallbacks(this.n);
    }

    private String l() {
        try {
            ApplicationInfo applicationInfo = getContext().getPackageManager().getApplicationInfo(getContext().getPackageName(), 128);
            if (applicationInfo == null || applicationInfo.metaData == null) {
                return null;
            }
            if (q.a().d() == null) {
                String str = (String) applicationInfo.metaData.get("Adlantis_adRequestUrl");
                String[] strArr = {str};
                if (str != null) {
                    q.a().a(strArr);
                }
            }
            String str2 = (String) applicationInfo.metaData.get("Adlantis_keywords");
            if (str2 != null) {
                q.a().b(str2);
            }
            String str3 = (String) applicationInfo.metaData.get("Adlantis_host");
            if (str3 != null) {
                q.a().a(str3);
            }
            return (String) applicationInfo.metaData.get(q.a().e());
        } catch (PackageManager.NameNotFoundException e2) {
            Log.d("AdlantisView", "commonInit" + e2);
            return null;
        }
    }

    private void m() {
        Rect rect;
        Context context = getContext();
        setClickable(true);
        switch (am.a(this)) {
            case 2:
                rect = new Rect(0, 0, 480, 32);
                break;
            default:
                rect = new Rect(0, 0, 320, 50);
                break;
        }
        this.d = new AdlantisViewFlipper(context);
        this.d.setBackgroundColor(k);
        float b2 = am.b(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (((float) rect.width()) * b2), (int) (((float) rect.width()) * b2));
        layoutParams.addRule(13);
        if (!i()) {
            this.d.setVisibility(4);
        }
        addView(this.d, layoutParams);
        this.f = new AdlantisAdView[2];
        this.f[0] = new AdlantisAdView(context);
        this.f[1] = new AdlantisAdView(context);
        this.f[0].setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.f[1].setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.d.addView(this.f[0]);
        this.d.addView(this.f[1]);
        this.e = new View(context);
        this.e.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.e.setBackgroundColor(1728053247);
        this.e.setVisibility(4);
        addView(this.e);
        this.g = new ProgressBar(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(13);
        this.g.setLayoutParams(layoutParams2);
        this.g.setIndeterminate(true);
        this.g.setVisibility(4);
        addView(this.g);
        this.i = true;
        ao aoVar = ao.FADE;
        if (this.d != null) {
            switch (ab.f42a[aoVar.ordinal()]) {
                case 1:
                    this.d.setInAnimation(null);
                    this.d.setOutAnimation(null);
                    break;
                case 2:
                    this.d.setInAnimation(a());
                    this.d.setOutAnimation(b());
                    break;
                case 3:
                    ViewFlipper viewFlipper = this.d;
                    TranslateAnimation translateAnimation = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
                    translateAnimation.setDuration((long) o);
                    viewFlipper.setInAnimation(translateAnimation);
                    ViewFlipper viewFlipper2 = this.d;
                    TranslateAnimation translateAnimation2 = new TranslateAnimation(2, 0.0f, 2, -1.0f, 2, 0.0f, 2, 0.0f);
                    translateAnimation2.setDuration((long) o);
                    viewFlipper2.setOutAnimation(translateAnimation2);
                    break;
                case 4:
                    ViewFlipper viewFlipper3 = this.d;
                    TranslateAnimation translateAnimation3 = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
                    translateAnimation3.setDuration((long) o);
                    viewFlipper3.setInAnimation(translateAnimation3);
                    ViewFlipper viewFlipper4 = this.d;
                    TranslateAnimation translateAnimation4 = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
                    translateAnimation4.setDuration((long) o);
                    viewFlipper4.setOutAnimation(translateAnimation4);
                    break;
            }
        }
        if (i()) {
            int i2 = q.a().f69a;
            this.f39a = i2;
            a(i2);
        }
        if (q.a().f() == null) {
            String l2 = l();
            if (l2 != null) {
                a(l2);
            }
        } else if (i()) {
            j();
        } else {
            c();
        }
        if (q.a().f() == null) {
            this.b.postDelayed(new w(this), this.c);
        }
    }

    private boolean n() {
        Class<AdlantisView> cls = AdlantisView.class;
        try {
            Intent intent = new Intent(getContext(), AdlantisAdActivity.class);
            intent.putExtra("jp.Adlantis.Android.AdlantisAd", o());
            getContext().startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e2) {
            Class<AdlantisView> cls2 = AdlantisView.class;
            Log.e(cls.getSimpleName(), "failed to display expanded ad, perhaps jp.Adlantis.Android.AdlantisAdActivity is not in the manifest file?");
            Class<AdlantisView> cls3 = AdlantisView.class;
            Log.e(cls.getSimpleName(), "exception=" + e2);
            return false;
        }
    }

    private u o() {
        u[] b2 = q.a().b(am.a(this));
        if (b2 == null || b2.length <= 0 || this.f39a >= b2.length) {
            return null;
        }
        return b2[this.f39a];
    }

    public final void a(int i2) {
        int h2;
        if (this.d != null && (h2 = h()) != 0) {
            (this.d.getCurrentView() == this.f[0] ? this.f[1] : this.f[0]).a(i2 >= h2 ? 0 : i2);
            this.d.showNext();
        }
    }

    public final void a(String str) {
        q.a().c(str);
        if (q.a().f() != null) {
            c();
        }
    }

    public final void c() {
        if (q.a().f() != null) {
            q.a().a(getContext(), new x(this));
            return;
        }
        Log.e("AdlantisView", "AdlantisView: can't connect because publisherID hasn't been set.");
    }

    public final void d() {
        int h2 = h();
        if (h2 > 1) {
            this.f39a = (this.f39a + 1) % h2;
            a(this.f39a);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        j();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        k();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (o() == null) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                k();
                a(true);
                break;
            case 1:
                a(false);
                if (a(motionEvent)) {
                    u o2 = o();
                    if (o2 != null && !this.l) {
                        this.l = true;
                        this.g.setVisibility(0);
                        o2.e();
                        if (!(o2.b(this) != null ? n() : false) && o2.b() != null) {
                            q.a().a(o2.b(), new ac(this));
                            break;
                        } else {
                            this.l = false;
                            break;
                        }
                    }
                }
                j();
                break;
            case 2:
                a(a(motionEvent));
                break;
            case 3:
            case 4:
                a(false);
                j();
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        switch (i2) {
            case 0:
                j();
                return;
            case 4:
            case 8:
                a(false);
                k();
                this.g.setVisibility(4);
                return;
            default:
                return;
        }
    }
}
