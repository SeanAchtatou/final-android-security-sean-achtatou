package jp.Adlantis.Android;

import android.app.Activity;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.Serializable;
import java.util.HashMap;

public class AdlantisAdActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private RelativeLayout f37a;
    private e b;
    private ImageView c;
    /* access modifiers changed from: private */
    public u d;
    /* access modifiers changed from: private */
    public ProgressBar e;
    private Button f;

    private static u a(Bundle bundle) {
        Serializable serializable = bundle.getSerializable("jp.Adlantis.Android.AdlantisAd");
        if (serializable == null) {
            return null;
        }
        if (serializable instanceof u) {
            return (u) serializable;
        }
        if (serializable instanceof HashMap) {
            return new u((HashMap) serializable);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.b.f59a != null) {
            new Handler().postDelayed(new j(this), 1);
        }
    }

    /* access modifiers changed from: private */
    public void a(Drawable drawable) {
        if (drawable != null) {
            e eVar = this.b;
            eVar.f59a = drawable;
            eVar.invalidate();
            a();
            this.e.setVisibility(4);
            this.f.setVisibility(4);
        }
    }

    static /* synthetic */ void d(AdlantisAdActivity adlantisAdActivity) {
        if (adlantisAdActivity.c != null) {
            Rect a2 = adlantisAdActivity.b.a();
            Log.d("AdlantisAdActivity", "imageBounds=" + a2);
            if (a2 != null && a2.width() > 0 && a2.height() > 0) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) adlantisAdActivity.c.getLayoutParams();
                Log.d("AdlantisAdActivity", "setting margins to left=" + a2.left + " top=" + a2.top);
                layoutParams.setMargins(a2.left, a2.top, 0, 0);
                adlantisAdActivity.c.setLayoutParams(layoutParams);
                adlantisAdActivity.c.requestLayout();
            }
            adlantisAdActivity.c.setVisibility(0);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f37a = new RelativeLayout(this);
        this.f37a.setBackgroundColor(-12626585);
        setContentView(this.f37a);
        float b2 = am.b(this);
        TextView textView = new TextView(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(11, -1);
        layoutParams.addRule(12, -1);
        layoutParams.setMargins(0, 0, (int) (4.0f * b2), (int) (b2 * 1.0f));
        textView.setText(q.b());
        textView.setTextSize(12.0f);
        textView.setShadowLayer(1.0f, 0.0f, 1.0f, -16777216);
        this.f37a.addView(textView, layoutParams);
        this.b = new h(this, this);
        this.f37a.addView(this.b, new RelativeLayout.LayoutParams(-1, -1));
        this.b.setOnClickListener(new f(this));
        this.e = new ProgressBar(this);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(13);
        this.e.setLayoutParams(layoutParams2);
        this.e.setIndeterminate(true);
        this.e.setId(101);
        this.f37a.addView(this.e);
        this.f = new Button(this);
        this.f.setText("Cancel");
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(14);
        layoutParams3.addRule(3, 101);
        layoutParams3.setMargins(0, 10, 0, 0);
        this.f.setLayoutParams(layoutParams3);
        this.f37a.addView(this.f);
        this.f.setOnClickListener(new g(this));
        this.d = null;
        String str = null;
        if (bundle != null) {
            this.d = a(bundle);
        }
        if (this.d == null) {
            this.d = a(getIntent().getExtras());
        }
        if (this.d != null) {
            str = this.d.b(this.f37a);
        }
        if (str != null) {
            a(q.a().c().a(str, new l(this)));
        }
        if (this.c != null) {
            Log.d("AdlantisAdActivity", "initCloseBoxButton called more than once");
        } else {
            Log.d("AdlantisAdActivity", "initCloseBoxButton");
            this.c = new ImageView(this);
            this.c.setVisibility(4);
            Rect c2 = al.c();
            float b3 = am.b(this);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams((int) (((float) c2.width()) * b3), (int) (((float) c2.height()) * b3));
            layoutParams4.addRule(9, -1);
            layoutParams4.addRule(10, -1);
            this.c.setLayoutParams(layoutParams4);
            Drawable b4 = am.a(this) ? al.b() : al.a();
            if (b4 != null) {
                this.c.setImageDrawable(b4);
            }
            this.c.setOnClickListener(new m(this));
            this.f37a.addView(this.c, layoutParams4);
        }
        if (this.d != null) {
            this.d.f();
        }
    }
}
