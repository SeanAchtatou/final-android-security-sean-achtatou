package jp.Adlantis.Android;

import android.graphics.drawable.Drawable;
import android.util.Log;

final class n implements r {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AdlantisAdView f67a;

    n(AdlantisAdView adlantisAdView) {
        this.f67a = adlantisAdView;
    }

    public final void a(Drawable drawable, String str) {
        if (drawable != null) {
            Log.d("AdlantisAdView", "setAdByIndex.imageLoaded=" + str);
            this.f67a.a(drawable, true);
        }
    }
}
