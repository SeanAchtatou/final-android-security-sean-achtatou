package jp.Adlantis.Android;

import android.content.Context;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public final class am {
    static int a(View view) {
        return view.getResources().getConfiguration().orientation;
    }

    static final String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & 255);
                while (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }
                stringBuffer.append(hexString);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    static void a(Uri.Builder builder, Map map) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                builder.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    static boolean a(Context context) {
        return c(context) >= 240.0f;
    }

    static float b(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    private static float c(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        try {
            Field field = DisplayMetrics.class.getField("densityDpi");
            if (field != null) {
                return (float) field.getInt(displayMetrics);
            }
            return -1.0f;
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return -1.0f;
        }
    }
}
