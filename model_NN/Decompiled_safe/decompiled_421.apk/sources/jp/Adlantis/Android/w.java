package jp.Adlantis.Android;

import android.util.Log;
import android.widget.Toast;

final class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AdlantisView f74a;

    w(AdlantisView adlantisView) {
        this.f74a = adlantisView;
    }

    public final void run() {
        if (q.a().f() == null) {
            Toast.makeText(this.f74a.getContext(), "AdlantisView publisher id not set", 1).show();
            Log.e("AdlantisView", "AdlantisView: can't display ads because publisherID hasn't been set.");
            return;
        }
        this.f74a.c();
    }
}
