package jp.Adlantis.Android;

import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.protocol.HttpContext;

final class ae extends DefaultRedirectHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Handler f45a;
    private /* synthetic */ q b;

    ae(q qVar, Handler handler) {
        this.b = qVar;
        this.f45a = handler;
    }

    public final boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
        String str = null;
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode >= 300 && statusCode < 400) {
            Header[] headers = httpResponse.getHeaders("Location");
            if (headers.length > 0) {
                str = headers[0].getValue();
                Log.d(q.j, "location=" + str);
            }
        }
        if (str == null) {
            str = ((HttpHost) httpContext.getAttribute("http.target_host")).toURI() + ((HttpUriRequest) httpContext.getAttribute("http.request")).getURI();
        }
        this.f45a.sendMessage(this.f45a.obtainMessage(0, Uri.parse(str)));
        return false;
    }
}
