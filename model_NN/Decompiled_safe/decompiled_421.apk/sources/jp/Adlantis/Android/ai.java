package jp.Adlantis.Android;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.net.MalformedURLException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

final class ai extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Context f49a;
    private /* synthetic */ String b;
    private /* synthetic */ boolean c = false;
    private /* synthetic */ q d;

    ai(q qVar, Context context, String str) {
        this.d = qVar;
        this.f49a = context;
        this.b = str;
    }

    public final void run() {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            String uri = this.d.a(this.f49a, this.b, this.c).toString();
            Log.d(q.j, "sendConversionTag url=" + uri);
            int statusCode = defaultHttpClient.execute(new HttpGet(uri)).getStatusLine().getStatusCode();
            if (statusCode >= 300 && statusCode < 501) {
                boolean unused = this.d.o = true;
            }
        } catch (MalformedURLException e) {
            Log.e(q.j, "sendConversionTag exception=" + e.toString());
        } catch (IOException e2) {
            Log.e(q.j, "sendConversionTag exception=" + e2.toString());
        }
    }
}
