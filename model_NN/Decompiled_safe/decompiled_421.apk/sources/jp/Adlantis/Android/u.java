package jp.Adlantis.Android;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class u extends HashMap implements Serializable, Map {

    /* renamed from: a  reason: collision with root package name */
    private long f72a;
    private long b;
    /* access modifiers changed from: private */
    public boolean c;
    private boolean d;
    private boolean e = false;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public boolean g;

    public u(HashMap hashMap) {
        super(hashMap);
    }

    private String a(int i, boolean z) {
        Map a2;
        String str = null;
        if (a() != 1 || (a2 = a(i)) == null) {
            return null;
        }
        if (z) {
            str = (String) a2.get("src_2x");
        }
        return str == null ? (String) a2.get("src") : str;
    }

    static /* synthetic */ void a(u uVar) {
        uVar.d = true;
        uVar.c = false;
    }

    /* access modifiers changed from: private */
    public boolean a(String str, String str2) {
        Uri uri = null;
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            String str3 = (String) get(str);
            if (str3 != null) {
                uri = q.a().a((Context) null, Uri.parse(str3)).build();
            }
            String uri2 = uri.toString();
            if (uri2 == null) {
                return false;
            }
            int statusCode = defaultHttpClient.execute(new HttpGet(uri2)).getStatusLine().getStatusCode();
            if (statusCode >= 200 && statusCode < 400) {
                return true;
            }
            Log.e("AdlantisAd", str2 + " status=" + statusCode);
            return false;
        } catch (MalformedURLException e2) {
            Log.e("AdlantisAd", str2 + " exception=" + e2.toString());
            return false;
        } catch (IOException e3) {
            Log.e("AdlantisAd", str2 + " exception=" + e3.toString());
            return false;
        } catch (OutOfMemoryError e4) {
            Log.e("AdlantisAd", str2 + " OutOfMemoryError=" + e4.toString());
            return false;
        }
    }

    static /* synthetic */ void b(u uVar) {
        uVar.e = true;
        uVar.c = false;
    }

    private static boolean b(int i) {
        return i == 2;
    }

    private static String c(int i) {
        return b(i) ? "landscape" : "portrait";
    }

    private static int e(View view) {
        return view.getResources().getConfiguration().orientation;
    }

    public final int a() {
        return "sp_banner".compareTo((String) get("type")) == 0 ? 1 : 2;
    }

    public final String a(View view) {
        return a(e(view), am.a(view.getContext()));
    }

    public final Map a(int i) {
        Object obj;
        if (a() == 1) {
            Object obj2 = (Map) get(c(i));
            if (obj2 == null) {
                obj = get(b(i) ? "iphone_landscape" : "iphone_portrait");
            } else {
                obj = obj2;
            }
            if (obj instanceof Map) {
                return (Map) obj;
            }
        }
        return null;
    }

    public final String b() {
        return (String) get("href");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String b(android.view.View r5) {
        /*
            r4 = this;
            r3 = 0
            int r1 = e(r5)
            android.content.Context r0 = r5.getContext()
            boolean r2 = jp.Adlantis.Android.am.a(r0)
            java.lang.String r0 = "has_expand"
            java.lang.Object r0 = r4.get(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 == 0) goto L_0x0051
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0051
            java.lang.String r0 = c(r1)
            java.lang.String r1 = "expand_content"
            java.lang.Object r4 = r4.get(r1)
            java.util.Map r4 = (java.util.Map) r4
            if (r4 == 0) goto L_0x0051
            java.lang.Object r4 = r4.get(r0)
            java.util.Map r4 = (java.util.Map) r4
            r0 = r4
        L_0x0032:
            if (r0 == 0) goto L_0x004f
            if (r2 == 0) goto L_0x004d
            java.lang.String r1 = "src_2x"
            java.lang.Object r4 = r0.get(r1)
            java.lang.String r4 = (java.lang.String) r4
            r1 = r4
        L_0x003f:
            if (r1 != 0) goto L_0x004b
            java.lang.String r1 = "src"
            java.lang.Object r4 = r0.get(r1)
            java.lang.String r4 = (java.lang.String) r4
            r0 = r4
        L_0x004a:
            return r0
        L_0x004b:
            r0 = r1
            goto L_0x004a
        L_0x004d:
            r1 = r3
            goto L_0x003f
        L_0x004f:
            r0 = r3
            goto L_0x004a
        L_0x0051:
            r0 = r3
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.Adlantis.Android.u.b(android.view.View):java.lang.String");
    }

    public final String c(View view) {
        return a(e(view), am.a(view.getContext()));
    }

    public final void c() {
        this.f72a = System.nanoTime();
    }

    public final String d(View view) {
        Map a2 = a() == 1 ? a(e(view)) : null;
        if (a2 == null) {
            return null;
        }
        String str = (String) a2.get("alt");
        return str != null ? Uri.decode(str) : str;
    }

    public final void d() {
        this.b += System.nanoTime() - this.f72a;
        if (this.b >= 2000000000) {
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        if (!this.d && !this.c && !this.e) {
            new aq(this).start();
        }
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        if (!this.f && !this.g) {
            new ar(this).start();
        }
    }
}
