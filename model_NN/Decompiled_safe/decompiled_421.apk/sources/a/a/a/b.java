package a.a.a;

import java.util.HashMap;
import java.util.Map;

public final class b extends HashMap implements d, Map {
    public static String a(Map map) {
        if (map == null) {
            return "null";
        }
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = true;
        stringBuffer.append('{');
        for (Map.Entry entry : map.entrySet()) {
            if (z) {
                z = false;
            } else {
                stringBuffer.append(',');
            }
            String valueOf = String.valueOf(entry.getKey());
            Object value = entry.getValue();
            stringBuffer.append('\"');
            if (valueOf == null) {
                stringBuffer.append("null");
            } else {
                c.a(valueOf, stringBuffer);
            }
            stringBuffer.append('\"').append(':');
            stringBuffer.append(c.a(value));
            stringBuffer.toString();
        }
        stringBuffer.append('}');
        return stringBuffer.toString();
    }

    public final String a() {
        return a(this);
    }

    public final String toString() {
        return a(this);
    }
}
