package a.a.a;

import java.util.List;
import java.util.Map;

public final class c {
    public static String a(Object obj) {
        String stringBuffer;
        if (obj == null) {
            return "null";
        }
        if (!(obj instanceof String)) {
            return obj instanceof Double ? (((Double) obj).isInfinite() || ((Double) obj).isNaN()) ? "null" : obj.toString() : obj instanceof Float ? (((Float) obj).isInfinite() || ((Float) obj).isNaN()) ? "null" : obj.toString() : obj instanceof Number ? obj.toString() : obj instanceof Boolean ? obj.toString() : obj instanceof d ? ((d) obj).a() : obj instanceof Map ? b.a((Map) obj) : obj instanceof List ? a.a((List) obj) : obj.toString();
        }
        StringBuffer append = new StringBuffer().append("\"");
        String str = (String) obj;
        if (str == null) {
            stringBuffer = null;
        } else {
            StringBuffer stringBuffer2 = new StringBuffer();
            a(str, stringBuffer2);
            stringBuffer = stringBuffer2.toString();
        }
        return append.append(stringBuffer).append("\"").toString();
    }

    static void a(String str, StringBuffer stringBuffer) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                    stringBuffer.append("\\\"");
                    break;
                case '/':
                    stringBuffer.append("\\/");
                    break;
                case '\\':
                    stringBuffer.append("\\\\");
                    break;
                default:
                    if ((charAt >= 0 && charAt <= 31) || ((charAt >= 127 && charAt <= 159) || (charAt >= 8192 && charAt <= 8447))) {
                        String hexString = Integer.toHexString(charAt);
                        stringBuffer.append("\\u");
                        for (int i2 = 0; i2 < 4 - hexString.length(); i2++) {
                            stringBuffer.append('0');
                        }
                        stringBuffer.append(hexString.toUpperCase());
                        break;
                    } else {
                        stringBuffer.append(charAt);
                        break;
                    }
            }
        }
    }
}
