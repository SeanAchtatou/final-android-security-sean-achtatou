package a.a.a;

import java.util.ArrayList;
import java.util.List;

public final class a extends ArrayList implements d, List {
    public static String a(List list) {
        if (list == null) {
            return "null";
        }
        boolean z = true;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('[');
        for (Object next : list) {
            if (z) {
                z = false;
            } else {
                stringBuffer.append(',');
            }
            if (next == null) {
                stringBuffer.append("null");
            } else {
                stringBuffer.append(c.a(next));
            }
        }
        stringBuffer.append(']');
        return stringBuffer.toString();
    }

    public final String a() {
        return a(this);
    }

    public final String toString() {
        return a(this);
    }
}
