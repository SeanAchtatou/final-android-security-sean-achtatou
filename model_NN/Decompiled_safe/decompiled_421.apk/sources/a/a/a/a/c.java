package a.a.a.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public int f3a = 0;
    public Object b = null;

    public c(int i, Object obj) {
        this.f3a = i;
        this.b = obj;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        switch (this.f3a) {
            case -1:
                stringBuffer.append("END OF FILE");
                break;
            case 0:
                stringBuffer.append("VALUE(").append(this.b).append(")");
                break;
            case 1:
                stringBuffer.append("LEFT BRACE({)");
                break;
            case 2:
                stringBuffer.append("RIGHT BRACE(})");
                break;
            case 3:
                stringBuffer.append("LEFT SQUARE([)");
                break;
            case 4:
                stringBuffer.append("RIGHT SQUARE(])");
                break;
            case 5:
                stringBuffer.append("COMMA(,)");
                break;
            case 6:
                stringBuffer.append("COLON(:)");
                break;
        }
        return stringBuffer.toString();
    }
}
