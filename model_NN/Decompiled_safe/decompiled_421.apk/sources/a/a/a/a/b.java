package a.a.a.a;

public final class b extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private int f2a;
    private Object b;
    private int c;

    public b(int i, int i2, Object obj) {
        this.c = i;
        this.f2a = i2;
        this.b = obj;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        switch (this.f2a) {
            case 0:
                stringBuffer.append("Unexpected character (").append(this.b).append(") at position ").append(this.c).append(".");
                break;
            case 1:
                stringBuffer.append("Unexpected token ").append(this.b).append(" at position ").append(this.c).append(".");
                break;
            case 2:
                stringBuffer.append("Unexpected exception at position ").append(this.c).append(": ").append(this.b);
                break;
            default:
                stringBuffer.append("Unkown error at position ").append(this.c).append(".");
                break;
        }
        return stringBuffer.toString();
    }
}
