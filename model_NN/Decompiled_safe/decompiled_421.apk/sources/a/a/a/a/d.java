package a.a.a.a;

import a.a.a.a;
import a.a.a.b;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList f4a;
    private a b = new a();
    private c c = null;
    private int d = 0;

    private static int a(LinkedList linkedList) {
        if (linkedList.size() == 0) {
            return -1;
        }
        return ((Integer) linkedList.getFirst()).intValue();
    }

    public final Object a(Reader reader) {
        this.b.a(reader);
        this.c = null;
        this.d = 0;
        this.f4a = null;
        LinkedList linkedList = new LinkedList();
        LinkedList linkedList2 = new LinkedList();
        do {
            try {
                this.c = this.b.b();
                if (this.c == null) {
                    this.c = new c(-1, null);
                }
                switch (this.d) {
                    case -1:
                        throw new b(this.b.a(), 1, this.c);
                    case 0:
                        switch (this.c.f3a) {
                            case 0:
                                this.d = 1;
                                linkedList.addFirst(new Integer(this.d));
                                linkedList2.addFirst(this.c.b);
                                break;
                            case 1:
                                this.d = 2;
                                linkedList.addFirst(new Integer(this.d));
                                linkedList2.addFirst(new b());
                                break;
                            case 2:
                            default:
                                this.d = -1;
                                break;
                            case 3:
                                this.d = 3;
                                linkedList.addFirst(new Integer(this.d));
                                linkedList2.addFirst(new a());
                                break;
                        }
                    case 1:
                        if (this.c.f3a == -1) {
                            return linkedList2.removeFirst();
                        }
                        throw new b(this.b.a(), 1, this.c);
                    case 2:
                        switch (this.c.f3a) {
                            case 0:
                                if (!(this.c.b instanceof String)) {
                                    this.d = -1;
                                    break;
                                } else {
                                    linkedList2.addFirst((String) this.c.b);
                                    this.d = 4;
                                    linkedList.addFirst(new Integer(this.d));
                                    break;
                                }
                            case 1:
                            case 3:
                            case 4:
                            default:
                                this.d = -1;
                                break;
                            case 2:
                                if (linkedList2.size() <= 1) {
                                    this.d = 1;
                                    break;
                                } else {
                                    linkedList.removeFirst();
                                    linkedList2.removeFirst();
                                    this.d = a(linkedList);
                                    break;
                                }
                            case 5:
                                break;
                        }
                    case 3:
                        switch (this.c.f3a) {
                            case 0:
                                ((List) linkedList2.getFirst()).add(this.c.b);
                                break;
                            case 1:
                                b bVar = new b();
                                ((List) linkedList2.getFirst()).add(bVar);
                                this.d = 2;
                                linkedList.addFirst(new Integer(this.d));
                                linkedList2.addFirst(bVar);
                                break;
                            case 2:
                            default:
                                this.d = -1;
                                break;
                            case 3:
                                a aVar = new a();
                                ((List) linkedList2.getFirst()).add(aVar);
                                this.d = 3;
                                linkedList.addFirst(new Integer(this.d));
                                linkedList2.addFirst(aVar);
                                break;
                            case 4:
                                if (linkedList2.size() <= 1) {
                                    this.d = 1;
                                    break;
                                } else {
                                    linkedList.removeFirst();
                                    linkedList2.removeFirst();
                                    this.d = a(linkedList);
                                    break;
                                }
                            case 5:
                                break;
                        }
                    case 4:
                        switch (this.c.f3a) {
                            case 0:
                                linkedList.removeFirst();
                                ((Map) linkedList2.getFirst()).put((String) linkedList2.removeFirst(), this.c.b);
                                this.d = a(linkedList);
                                break;
                            case 1:
                                linkedList.removeFirst();
                                Map map = (Map) linkedList2.getFirst();
                                b bVar2 = new b();
                                map.put((String) linkedList2.removeFirst(), bVar2);
                                this.d = 2;
                                linkedList.addFirst(new Integer(this.d));
                                linkedList2.addFirst(bVar2);
                                break;
                            case 2:
                            case 4:
                            case 5:
                            default:
                                this.d = -1;
                                break;
                            case 3:
                                linkedList.removeFirst();
                                Map map2 = (Map) linkedList2.getFirst();
                                a aVar2 = new a();
                                map2.put((String) linkedList2.removeFirst(), aVar2);
                                this.d = 3;
                                linkedList.addFirst(new Integer(this.d));
                                linkedList2.addFirst(aVar2);
                                break;
                            case 6:
                                break;
                        }
                }
                if (this.d == -1) {
                    throw new b(this.b.a(), 1, this.c);
                }
            } catch (IOException e) {
                throw e;
            }
        } while (this.c.f3a != -1);
        throw new b(this.b.a(), 1, this.c);
    }
}
