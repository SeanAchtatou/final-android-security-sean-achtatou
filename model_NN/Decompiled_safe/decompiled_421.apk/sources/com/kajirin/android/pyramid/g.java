package com.kajirin.android.pyramid;

import android.app.Activity;
import android.content.DialogInterface;

final class g implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Activity f26a;

    g(Activity activity) {
        this.f26a = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Pyramid.g = false;
        this.f26a.setResult(-1);
    }
}
