package com.kajirin.android.pyramid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.google.ads.AdView;
import com.google.ads.f;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import jp.Adlantis.Android.AdlantisView;
import jp.co.nobot.libAdMaker.e;
import jp.co.nobot.libAdMaker.libAdMaker;

public class Pyramid extends Activity implements SensorEventListener, View.OnClickListener, e {
    private static final String A = System.getProperty("line.separator");
    private static int B = 0;
    private static int C = -1;
    private static int D;
    private static int E;
    private static MediaPlayer[] F = new MediaPlayer[9];

    /* renamed from: a  reason: collision with root package name */
    public static RelativeLayout f18a;
    public static ProgressDialog b;
    public static int c = 0;
    public static int d;
    public static int e;
    public static int f = 1;
    public static boolean g;
    public static boolean h = false;
    public static ProgressDialog i;
    public static int j = 0;
    public static int k = 1;
    public static int l = 1;
    public static int m = 0;
    public static int n = 0;
    public static int o = -999;
    public static int p = 0;
    public static int q = 0;
    public static int r = 0;
    public static int s = 1;
    public static int t = 0;
    public static int u = 0;
    public static int v = 0;
    private SensorManager G;
    private Sensor H;
    private float[] I = {0.0f, 0.0f, 0.0f};
    private float[] J = {0.0f, 0.0f, 0.0f};
    /* access modifiers changed from: private */
    public final Handler K = new Handler();
    /* access modifiers changed from: private */
    public final Runnable L = new d(this);
    private final int w = 1000;
    private Handler x = new Handler();
    private Runnable y;
    private libAdMaker z = null;

    private static String a(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        byte b2 = 12;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr2[i2] = (byte) (bArr[i2] ^ b2);
            b2 = (byte) (b2 + 1);
            if (b2 > 120) {
                b2 = 12;
            }
        }
        try {
            return new String(bArr2, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            return new String(bArr2);
        }
    }

    public static void a(int i2) {
        if (f == 1 && F[i2] != null) {
            MediaPlayer mediaPlayer = F[i2];
            mediaPlayer.setLooping(false);
            mediaPlayer.seekTo(0);
            mediaPlayer.start();
        }
    }

    private void a(Activity activity, String str, String str2, String str3) {
        g = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon((int) R.drawable.icon);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.setPositiveButton(str3, new e(this, activity));
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    public static int b(int i2) {
        String num = Integer.toString(i2);
        int length = num.length();
        int i3 = i2 < 200 ? 72 : i2 < 600 ? 23 : i2 < 1000 ? 235 : 542;
        for (int i4 = 0; i4 < length; i4++) {
            i3 = ((i3 + (num.charAt(i4) * num.charAt(i4))) % 100000) + (num.charAt(i4) * 15 * length);
        }
        return i3;
    }

    private void b() {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setBackgroundColor(-16777216);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(51);
        this.z = new libAdMaker(this);
        this.z.setHorizontalScrollBarEnabled(false);
        this.z.a(this);
        this.z.f80a = a(new byte[]{58, 63, 54});
        this.z.b = a(new byte[]{61, 56, 57, 61});
        this.z.a(a(new byte[]{100, 121, 122, Byte.MAX_VALUE, 42, 62, 61, 122, 121, 116, 113, 114, 107, 55, 123, Byte.MAX_VALUE, 49, 112, Byte.MAX_VALUE, 116, 69, 83, 12, 74, 74, 67, 73, 8, 73, 89, 90, 88, 3, 20, 31, 92, 81, 71, 81, 7, 3, 83, 7, 71, 13, 23, 82, 79, 81, 81}));
        this.z.setBackgroundColor(-16777216);
        this.z.d();
        linearLayout.addView(this.z, -2, (int) ((getApplicationContext().getResources().getDisplayMetrics().density * 50.0f) + 0.5f));
        linearLayout.addView(new a(this), -2, -2);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        f18a = relativeLayout;
        relativeLayout.setBackgroundColor(Color.argb(0, 255, 255, 255));
        f18a.setGravity(83);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12);
        layoutParams.addRule(14);
        Button button = new Button(this);
        button.setText(getText(R.string.menu_item0));
        button.setTag("123");
        button.setOnClickListener(this);
        button.setLayoutParams(layoutParams);
        button.setBackgroundDrawable(getResources().getDrawable(R.drawable.color_stateful));
        f18a.addView(button);
        f18a.setVisibility(4);
        FrameLayout frameLayout = new FrameLayout(this);
        setContentView(frameLayout);
        frameLayout.addView(linearLayout, new ViewGroup.LayoutParams(-2, -2));
        frameLayout.addView(f18a, new ViewGroup.LayoutParams(-2, -2));
    }

    private static void c(int i2) {
        if (F[i2] != null && F[i2].isPlaying()) {
            F[i2].stop();
            try {
                F[i2].prepare();
            } catch (IOException | IllegalStateException e2) {
            }
        }
    }

    public final void a() {
        String language = Locale.getDefault().getLanguage();
        if (language.equals(Locale.JAPAN.toString()) || language.equals(Locale.JAPANESE.toString())) {
            a(this, a(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), a(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), a(new byte[]{-21, -72, -116, -21, -86, -105}));
        } else {
            a(this, a(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), a(new byte[]{88, 101, 107, 47, 118, 99, 119, 118, 52, 99, 115, 101, 107, 112, 117, 117, 60, 112, 107, 108, 84, 1, 74, 66, 82, 64, 6, 70, 70, 9, 99, 69, 88, 72, 92, 65, 85, 69, 18, 80, 91, 91, 88, 82, 91, 77, 83, 84, 82, 19}), a(new byte[]{73, 117, 103, 123}));
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            switch (keyEvent.getKeyCode()) {
                case 4:
                    g = true;
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle((String) getText(R.string.app_name));
                    builder.setIcon((int) R.drawable.icon);
                    builder.setMessage((String) getText(R.string.Pyramid28));
                    builder.setPositiveButton(a(new byte[]{67, 70}), new h(this, this));
                    builder.setNegativeButton(a(new byte[]{79, 76, 64, 76, 85, 93}), new i(this, this));
                    builder.setCancelable(false);
                    builder.create();
                    builder.show();
                    return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public void onClick(View view) {
        if (((String) view.getTag()).equals("123")) {
            if (f18a.getVisibility() == 0) {
                f18a.setVisibility(4);
            }
            v = 0;
            Intent intent = new Intent(this, Setting.class);
            intent.putExtra(a(new byte[]{120, 104, 118, 123}), a(new byte[]{88, 72, 93, 91, 48, 85, 83, 71, 85}));
            startActivityForResult(intent, 0);
        }
    }

    public void onCreate(Bundle bundle) {
        boolean z2;
        NetworkInfo activeNetworkInfo;
        String language;
        int i2;
        int i3;
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFormat(-3);
        this.K.postDelayed(this.L, 10000);
        getWindow().addFlags(1024);
        B = 0;
        C = -1;
        g = true;
        ProgressDialog progressDialog = new ProgressDialog(this);
        i = progressDialog;
        progressDialog.setTitle(getText(R.string.app_name));
        i.setMessage(getText(R.string.Pyramid4));
        i.setIndeterminate(false);
        i.setProgressStyle(0);
        i.setIcon((int) R.drawable.icon);
        i.setMax(100);
        i.setCancelable(false);
        i.show();
        SharedPreferences sharedPreferences = getSharedPreferences(a(new byte[]{92, 116, 124, 110, 125, 120, 118}), 0);
        if (Setting.a(this, a(new byte[]{120, 98, 105, 104, 124, 116, 77, 99, 102, 112, 112, 114, 106, 124, 116, 120, 121}))) {
            f = 1;
        } else {
            f = 0;
        }
        if (Setting.a(this, a(new byte[]{120, 98, 105, 104, 124, 116, 77, 99, 102, 112, 112, 114, 106, 124, 116, 120, 121, 47}))) {
            t = 1;
        } else {
            t = 0;
        }
        int a2 = Setting.a(Setting.a(this, a(new byte[]{107, 108, 99, 106, 79, 97, 96, 118, 114, 112, 100, 114, 118, 122, Byte.MAX_VALUE}), a(new byte[]{107, 108, 99, 106, 32, 32})));
        k = a2;
        l = a2;
        D = sharedPreferences.getInt(a(new byte[]{75, 108, 99, 106}), 0);
        E = sharedPreferences.getInt(a(new byte[]{95, 120, 99, 72, 113, 124, 119}), 0);
        d = sharedPreferences.getInt(a(new byte[]{79, 98, 103, 97}), 0);
        e = sharedPreferences.getInt(a(new byte[]{95, 120, 99, 76, Byte.MAX_VALUE, 120, 124}), 0);
        B = sharedPreferences.getInt(a(new byte[]{79, 101, 107, 108, 123, 67}), 0);
        if (D == 0) {
            E = b(D);
        }
        if (d == 0) {
            e = b(d);
        }
        if (t == 0) {
            s = 0;
            u = 0;
        } else {
            s = 1;
            u = 1;
        }
        i.incrementProgressBy(10);
        try {
            F[0] = MediaPlayer.create(this, (int) R.raw.s27000);
            i.incrementProgressBy(3);
            F[1] = MediaPlayer.create(this, (int) R.raw.s27001);
            i.incrementProgressBy(3);
            F[2] = MediaPlayer.create(this, (int) R.raw.s27002);
            i.incrementProgressBy(3);
            F[3] = MediaPlayer.create(this, (int) R.raw.s27003);
            i.incrementProgressBy(3);
            F[4] = MediaPlayer.create(this, (int) R.raw.s27004);
            i.incrementProgressBy(3);
            F[5] = MediaPlayer.create(this, (int) R.raw.s27005);
            i.incrementProgressBy(3);
            F[6] = MediaPlayer.create(this, (int) R.raw.s27006);
            i.incrementProgressBy(3);
            F[7] = MediaPlayer.create(this, (int) R.raw.s27007);
            i.incrementProgressBy(3);
            F[8] = MediaPlayer.create(this, (int) R.raw.s27008);
            i.incrementProgressBy(3);
        } catch (Exception e2) {
        }
        setVolumeControlStream(3);
        try {
            String[] strArr = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 4096).requestedPermissions;
            int length = strArr.length;
            int i4 = 0;
            while (true) {
                if (i4 >= length) {
                    i2 = 0;
                    break;
                } else if (strArr[i4].equals("android.permission.INTERNET")) {
                    i2 = 0 + 1;
                    break;
                } else {
                    i4++;
                }
            }
            int i5 = 0;
            while (true) {
                if (i5 >= length) {
                    i3 = i2;
                    break;
                } else if (strArr[i5].equals("android.permission.ACCESS_NETWORK_STATE")) {
                    i3 = i2 + 1;
                    break;
                } else {
                    i5++;
                }
            }
            if (i3 == 2) {
                z2 = true;
                activeNetworkInfo = ((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
                if ((activeNetworkInfo == null && activeNetworkInfo.isConnected()) || !z2) {
                    language = Locale.getDefault().getLanguage();
                    if (!language.equals(Locale.JAPAN.toString()) || language.equals(Locale.JAPANESE.toString())) {
                        a(this, a(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), a(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), a(new byte[]{-21, -72, -116, -21, -86, -105}));
                    } else {
                        a(this, a(new byte[]{73, Byte.MAX_VALUE, 124, 96, 98}), a(new byte[]{88, 101, 107, 47, 118, 99, 119, 118, 52, 99, 115, 101, 107, 112, 117, 117, 60, 112, 107, 108, 84, 1, 74, 66, 82, 64, 6, 70, 70, 9, 99, 69, 88, 72, 92, 65, 85, 69, 18, 80, 91, 91, 88, 82, 91, 77, 83, 84, 82, 19}), a(new byte[]{73, 117, 103, 123}));
                        return;
                    }
                } else {
                    String language2 = Locale.getDefault().getLanguage();
                    if (language2.equals(Locale.JAPAN.toString()) || language2.equals(Locale.JAPANESE.toString())) {
                        if (b.a(101) + 0 < 50) {
                            b();
                            return;
                        }
                        LinearLayout linearLayout = new LinearLayout(this);
                        linearLayout.setBackgroundColor(-16777216);
                        linearLayout.setOrientation(1);
                        linearLayout.setGravity(51);
                        AdlantisView adlantisView = new AdlantisView(this);
                        adlantisView.a("ODE3MQ%3D%3D%0A");
                        adlantisView.setBackgroundColor(0);
                        linearLayout.addView(adlantisView, -2, (int) ((getApplicationContext().getResources().getDisplayMetrics().density * 50.0f) + 0.5f));
                        linearLayout.addView(new a(this), -2, -2);
                        RelativeLayout relativeLayout = new RelativeLayout(this);
                        f18a = relativeLayout;
                        relativeLayout.setBackgroundColor(Color.argb(0, 255, 255, 255));
                        f18a.setGravity(83);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                        layoutParams.addRule(12);
                        layoutParams.addRule(14);
                        Button button = new Button(this);
                        button.setText(getText(R.string.menu_item0));
                        button.setTag("123");
                        button.setOnClickListener(this);
                        button.setLayoutParams(layoutParams);
                        button.setBackgroundDrawable(getResources().getDrawable(R.drawable.color_stateful));
                        f18a.addView(button);
                        f18a.setVisibility(4);
                        FrameLayout frameLayout = new FrameLayout(this);
                        setContentView(frameLayout);
                        frameLayout.addView(linearLayout, new ViewGroup.LayoutParams(-2, -2));
                        frameLayout.addView(f18a, new ViewGroup.LayoutParams(-2, -2));
                        return;
                    } else if (b.a(101) + 0 < 95) {
                        b();
                        return;
                    } else {
                        LinearLayout linearLayout2 = new LinearLayout(this);
                        linearLayout2.setBackgroundColor(-16777216);
                        linearLayout2.setOrientation(1);
                        linearLayout2.setGravity(51);
                        AdView adView = new AdView(this, f.f12a, "a14d50c6a471574");
                        adView.setBackgroundColor(0);
                        linearLayout2.addView(adView, -2, (int) ((getApplicationContext().getResources().getDisplayMetrics().density * 50.0f) + 0.5f));
                        adView.a(new com.google.ads.e());
                        linearLayout2.addView(new a(this), -2, -2);
                        RelativeLayout relativeLayout2 = new RelativeLayout(this);
                        f18a = relativeLayout2;
                        relativeLayout2.setBackgroundColor(Color.argb(0, 255, 255, 255));
                        f18a.setGravity(83);
                        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
                        layoutParams2.addRule(12);
                        layoutParams2.addRule(14);
                        Button button2 = new Button(this);
                        button2.setText(getText(R.string.menu_item0));
                        button2.setTag("123");
                        button2.setOnClickListener(this);
                        button2.setLayoutParams(layoutParams2);
                        button2.setBackgroundDrawable(getResources().getDrawable(R.drawable.color_stateful));
                        f18a.addView(button2);
                        f18a.setVisibility(4);
                        FrameLayout frameLayout2 = new FrameLayout(this);
                        setContentView(frameLayout2);
                        frameLayout2.addView(linearLayout2, new ViewGroup.LayoutParams(-2, -2));
                        frameLayout2.addView(f18a, new ViewGroup.LayoutParams(-2, -2));
                        return;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e3) {
        }
        z2 = false;
        activeNetworkInfo = ((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null && activeNetworkInfo.isConnected()) {
        }
        language = Locale.getDefault().getLanguage();
        if (!language.equals(Locale.JAPAN.toString())) {
        }
        a(this, a(new byte[]{-17, -113, -90, -20, -109, -72, -15, -112, -88}), a(new byte[]{-17, -114, -101, -20, -109, -69, -15, -112, -88, -14, -97, -97, -5, -104, -67, -8, -99, -78, -3, -99, -124, -62, -95, -112, -57, -89, -103, -60, -85, -107, -55, -88, -95, -50, -83, -84, -45, -78, -70, -43, -70, -112, -47, -127, -94, -38, -69, -82, -33, -65, -78, -36, -63, -25, -95, -62, -64, -90, -60, -52, -83, -10, -49, -93, -22, -52, -83, -50, -36, -78, -45, -47, -73, -41, -36, -76, -39, -25, -71, -38, -59, -66, -34, -35}), a(new byte[]{-21, -72, -116, -21, -86, -105}));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, (int) R.string.menu_item0).setIcon(17301577);
        menu.add(0, 1, 0, (int) R.string.menu_item1).setIcon(17301568);
        menu.add(0, 2, 0, (int) R.string.menu_item2).setIcon(17301569);
        menu.add(0, 3, 0, (int) R.string.menu_item3).setIcon((int) R.drawable.favicon);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.z != null) {
                this.z.e();
                this.z = null;
            }
        } catch (Exception e2) {
        }
        if (d > 999999999) {
            d = 999999999;
        }
        SharedPreferences.Editor edit = getSharedPreferences(a(new byte[]{92, 116, 124, 110, 125, 120, 118}), 0).edit();
        E = b(D);
        e = b(d);
        edit.putInt(a(new byte[]{75, 108, 99, 106}), D);
        edit.putInt(a(new byte[]{95, 120, 99, 72, 113, 124, 119}), E);
        edit.putInt(a(new byte[]{79, 98, 103, 97}), d);
        edit.putInt(a(new byte[]{95, 120, 99, 76, Byte.MAX_VALUE, 120, 124}), e);
        edit.putInt(a(new byte[]{79, 101, 107, 108, 123, 67}), B);
        edit.commit();
        for (int i2 = 0; i2 < 9; i2++) {
            if (F[i2] != null) {
                c(i2);
                F[i2].release();
            }
        }
        if (this.H != null) {
            this.G.unregisterListener(this);
            this.H = null;
        }
        System.exit(0);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        if (f18a.getVisibility() == 0) {
            f18a.setVisibility(4);
        }
        v = 0;
        switch (menuItem.getItemId()) {
            case 0:
                Intent intent = new Intent(this, Setting.class);
                intent.putExtra(a(new byte[]{120, 104, 118, 123}), a(new byte[]{88, 72, 93, 91, 48, 85, 83, 71, 85}));
                startActivityForResult(intent, 0);
                return true;
            case 1:
                startActivityForResult(new Intent(this, Help.class), 1);
                return true;
            case 2:
                try {
                    str = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e2) {
                    str = " ";
                }
                g = true;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((String) getText(R.string.app_name));
                builder.setIcon((int) R.drawable.icon);
                builder.setMessage(String.valueOf(a(new byte[]{90, 104, 124, 124, 121, 126, 124, 41, 52})) + str + A + A + a(new byte[]{79, 98, 126, 118, 98, 120, 117, 123, 96, 53, 36, 39, 41, 40, 54, 59, 95, 124, 112, 123, 89, 105, 77, 86, 87, 64}));
                builder.setPositiveButton(a(new byte[]{67, 70}), new g(this));
                builder.setCancelable(false);
                builder.create();
                builder.show();
                return true;
            case 3:
                startActivity(new Intent(a(new byte[]{109, 99, 106, 125, Byte.MAX_VALUE, 120, 118, 61, 125, 123, 98, 114, 118, 109, 52, 122, Byte.MAX_VALUE, 105, 119, 112, 78, 15, 116, 106, 97, 114}), Uri.parse(a(new byte[]{100, 121, 122, Byte.MAX_VALUE, 42, 62, 61, 120, 117, Byte.MAX_VALUE, Byte.MAX_VALUE, 101, 113, 119, 52, 120, 115, 112}))));
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            if (this.z != null) {
                this.z.f();
            }
        } catch (Exception e2) {
        }
        g = true;
        c(0);
        c(1);
        c(2);
        if (this.H != null) {
            this.G.unregisterListener(this);
            this.H = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        ProgressDialog progressDialog = new ProgressDialog(this);
        b = progressDialog;
        progressDialog.setTitle(getText(R.string.app_name));
        b.setIndeterminate(false);
        b.setProgressStyle(0);
        b.setCancelable(false);
        b.show();
        this.y = new f(this);
        this.x.postDelayed(this.y, 1000);
        try {
            if (this.z != null) {
                this.z.d();
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        g = false;
        this.G = (SensorManager) getSystemService("sensor");
        List<Sensor> sensorList = this.G.getSensorList(1);
        if (sensorList.size() > 0) {
            this.H = sensorList.get(0);
        }
        if (this.H != null) {
            this.G.registerListener(this, this.H, 2);
        }
        try {
            if (this.z != null) {
                this.z.d();
            }
        } catch (Exception e2) {
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        this.I[0] = (fArr[0] * 0.1f) + (this.I[0] * 0.9f);
        this.I[1] = (fArr[1] * 0.1f) + (this.I[1] * 0.9f);
        this.I[2] = (fArr[2] * 0.1f) + (this.I[2] * 0.9f);
        this.J[0] = fArr[0] - this.I[0];
        this.J[1] = fArr[1] - this.I[1];
        this.J[2] = fArr[2] - this.I[2];
        u = 0;
        if (s == 0) {
            if (fArr[0] < -8.0f) {
                u = 1;
            } else if (fArr[0] > 8.0f) {
                u = 1;
            }
        } else if (fArr[0] < -2.0f) {
            u = 1;
        } else if (fArr[0] > 2.0f) {
            u = 1;
        }
        if (t == 0) {
            if (s != 0) {
                r = 1;
            }
            s = 0;
        } else if (fArr[2] < 8.0f && s != u) {
            s = u;
            r = 1;
        }
        p = 2;
        if (s == 0) {
            if (fArr[0] < -2.0f) {
                p = 1;
            } else if (fArr[0] > 2.0f) {
                p = 3;
            }
        }
        if ((Math.abs(this.J[0]) + Math.abs(this.J[1])) + Math.abs(this.J[2]) > 15.0f) {
            j = 1;
        }
    }
}
