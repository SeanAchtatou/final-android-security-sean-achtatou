package com.kajirin.android.pyramid;

import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Setting extends PreferenceActivity {
    private static int b;
    /* access modifiers changed from: private */
    public static String[] c;

    /* renamed from: a  reason: collision with root package name */
    private int f19a;
    private Preference.OnPreferenceChangeListener d = new c(this);

    public static int a(String str) {
        if (str.equals("game01")) {
            return 1;
        }
        return str.equals("game02") ? 2 : 1;
    }

    public static String a(Context context, String str, String str2) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(str, str2);
    }

    public static boolean a(Context context, String str) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(str, true);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c = new String[]{" ", (String) getText(R.string.game01), (String) getText(R.string.game02)};
        setVolumeControlStream(3);
        this.f19a = a(a(this, "game_preference", "game01"));
        b = Pyramid.t;
        addPreferencesFromResource(R.xml.preferences);
        getPreferenceManager().findPreference("game_preference").setSummary(c[this.f19a]);
        ((ListPreference) findPreference("game_preference")).setOnPreferenceChangeListener(this.d);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (a(this, "toggle_preference")) {
            Pyramid.f = 1;
        } else {
            Pyramid.f = 0;
        }
        if (a(this, "toggle_preference2")) {
            Pyramid.t = 1;
        } else {
            Pyramid.t = 0;
        }
        if (b != Pyramid.t) {
            if (Pyramid.t == 0) {
                if (Pyramid.s != 0) {
                    Pyramid.r = 1;
                }
                Pyramid.s = 0;
            } else if (Pyramid.s != Pyramid.u) {
                Pyramid.s = Pyramid.u;
                Pyramid.r = 1;
            }
        }
        Pyramid.l = a(a(this, "game_preference", "game01"));
        if (this.f19a != Pyramid.l) {
            Pyramid.c = 0;
            Pyramid.q = 1;
        }
    }
}
