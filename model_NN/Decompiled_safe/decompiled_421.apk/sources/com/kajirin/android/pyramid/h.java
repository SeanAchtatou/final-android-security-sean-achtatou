package com.kajirin.android.pyramid;

import android.app.Activity;
import android.content.DialogInterface;

final class h implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Pyramid f27a;
    private final /* synthetic */ Activity b;

    h(Pyramid pyramid, Activity activity) {
        this.f27a = pyramid;
        this.b = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.setResult(-1);
        try {
            this.f27a.finish();
        } catch (Exception e) {
        }
    }
}
