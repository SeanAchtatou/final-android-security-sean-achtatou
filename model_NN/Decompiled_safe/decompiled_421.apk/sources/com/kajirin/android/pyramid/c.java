package com.kajirin.android.pyramid;

import android.preference.Preference;

final class c implements Preference.OnPreferenceChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Setting f22a;

    c(Setting setting) {
        this.f22a = setting;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        Pyramid.l = Setting.a((String) obj);
        this.f22a.getPreferenceManager().findPreference("game_preference").setSummary(Setting.c[Pyramid.l]);
        return true;
    }
}
