package com.kajirin.android.pyramid;

import android.app.Activity;
import android.content.DialogInterface;

final class i implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Pyramid f28a;
    private final /* synthetic */ Activity b;

    i(Pyramid pyramid, Activity activity) {
        this.f28a = pyramid;
        this.b = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.setResult(0);
        Pyramid.g = false;
    }
}
