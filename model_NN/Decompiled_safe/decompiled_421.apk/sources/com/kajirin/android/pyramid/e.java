package com.kajirin.android.pyramid;

import android.app.Activity;
import android.content.DialogInterface;

final class e implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Pyramid f24a;
    private final /* synthetic */ Activity b;

    e(Pyramid pyramid, Activity activity) {
        this.f24a = pyramid;
        this.b = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.setResult(-1);
        try {
            this.f24a.finish();
        } catch (Exception e) {
        }
    }
}
