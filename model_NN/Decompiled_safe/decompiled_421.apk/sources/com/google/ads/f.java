package com.google.ads;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    public static final f f12a = new f(320, 50, "320x50_mb");
    public static final f b = new f(300, 250, "300x250_as");
    public static final f c = new f(468, 60, "468x60_as");
    public static final f d = new f(728, 90, "728x90_as");
    private int e;
    private int f;
    private String g;

    private f(int i, int i2, String str) {
        this.e = i;
        this.f = i2;
        this.g = str;
    }

    public final int a() {
        return this.e;
    }

    public final int b() {
        return this.f;
    }

    public final String toString() {
        return this.g;
    }
}
