package com.google.ads;

public enum g {
    MALE("m"),
    FEMALE("f");
    
    private String c;

    private g(String str) {
        this.c = str;
    }

    public final String toString() {
        return this.c;
    }
}
