package defpackage;

import java.util.LinkedList;

/* renamed from: e  reason: default package */
final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final h f31a;
    private final LinkedList b;
    private final int c;
    private /* synthetic */ k d;

    public e(k kVar, h hVar, LinkedList linkedList, int i) {
        this.d = kVar;
        this.f31a = hVar;
        this.b = linkedList;
        this.c = i;
    }

    public final void run() {
        this.f31a.a(this.b);
        this.f31a.a(this.c);
        this.f31a.o();
    }
}
