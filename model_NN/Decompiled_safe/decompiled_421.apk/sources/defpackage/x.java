package defpackage;

import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: x  reason: default package */
public final class x implements t {
    public final void a(h hVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("applicationTimeout");
        if (str != null) {
            try {
                hVar.a((long) (Float.parseFloat(str) * 1000.0f));
            } catch (NumberFormatException e) {
                d.b("Trying to set applicationTimeout to invalid value: " + str, e);
            }
        }
    }
}
