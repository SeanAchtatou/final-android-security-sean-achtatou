package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.c;
import com.google.ads.e;
import com.google.ads.f;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/* renamed from: k  reason: default package */
public final class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private String f81a;
    private String b = null;
    private j c;
    private h d;
    private e e;
    private WebView f;
    private String g = null;
    private LinkedList h = new LinkedList();
    private volatile boolean i;
    private c j = null;
    private boolean k = false;
    private int l = -1;
    private Thread m;

    public k(h hVar) {
        this.d = hVar;
        Activity d2 = hVar.d();
        if (d2 != null) {
            this.f = new g(d2.getApplicationContext(), null);
            this.f.setWebViewClient(new s(hVar, l.f82a, false, false));
            this.f.setVisibility(8);
            this.f.setWillNotDraw(true);
            this.c = new j(this, hVar);
            return;
        }
        this.f = null;
        this.c = null;
        d.e("activity was null while trying to create an AdLoader.");
    }

    private String a(e eVar, Activity activity) {
        Context applicationContext = activity.getApplicationContext();
        Map a2 = eVar.a(applicationContext);
        f k2 = this.d.k();
        long h2 = k2.h();
        if (h2 > 0) {
            a2.put("prl", Long.valueOf(h2));
        }
        String g2 = k2.g();
        if (g2 != null) {
            a2.put("ppcl", g2);
        }
        String f2 = k2.f();
        if (f2 != null) {
            a2.put("pcl", f2);
        }
        long e2 = k2.e();
        if (e2 > 0) {
            a2.put("pcc", Long.valueOf(e2));
        }
        a2.put("preqs", Long.valueOf(f.i()));
        String j2 = k2.j();
        if (j2 != null) {
            a2.put("pai", j2);
        }
        if (k2.k()) {
            a2.put("aoi_timeout", "true");
        }
        if (k2.m()) {
            a2.put("aoi_nofill", "true");
        }
        String p = k2.p();
        if (p != null) {
            a2.put("pit", p);
        }
        k2.a();
        k2.d();
        if (this.d.e() instanceof com.google.ads.d) {
            a2.put("format", "interstitial_mb");
        } else {
            f j3 = this.d.j();
            String fVar = j3.toString();
            if (fVar != null) {
                a2.put("format", fVar);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(j3.a()));
                hashMap.put("h", Integer.valueOf(j3.b()));
                a2.put("ad_frame", hashMap);
            }
        }
        a2.put("slotname", this.d.g());
        a2.put("js", "afma-sdk-a-v4.1.1");
        try {
            int i2 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            a2.put("msid", applicationContext.getPackageName());
            a2.put("app_name", i2 + ".android." + applicationContext.getPackageName());
            a2.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                throw new d(this, "NETWORK_ERROR");
            }
            a2.put("net", d2);
            String e3 = AdUtil.e(applicationContext);
            if (!(e3 == null || e3.length() == 0)) {
                a2.put("cap", e3);
            }
            a2.put("u_audio", Integer.valueOf(AdUtil.f(applicationContext).ordinal()));
            DisplayMetrics a3 = AdUtil.a(activity);
            a2.put("u_sd", Float.valueOf(a3.density));
            a2.put("u_h", Integer.valueOf(AdUtil.a(applicationContext, a3)));
            a2.put("u_w", Integer.valueOf(AdUtil.b(applicationContext, a3)));
            a2.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.c()) {
                a2.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(a2) + ");" + "</script></head><body></body></html>";
            d.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e4) {
            throw new b(this, "NameNotFoundException");
        }
    }

    private void a(c cVar, boolean z) {
        this.c.a();
        this.d.a(new a(this, this.d, this.f, this.c, cVar, z));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.k = true;
        notify();
    }

    public final synchronized void a(int i2) {
        this.l = i2;
    }

    public final synchronized void a(c cVar) {
        this.j = cVar;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final void a(e eVar) {
        this.e = eVar;
        this.i = false;
        this.m = new Thread(this);
        this.m.start();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.h.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.f81a = str2;
        this.b = str;
        notify();
    }

    public final synchronized void b(String str) {
        this.g = str;
        notify();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.a(com.google.ads.c, boolean):void
     arg types: [com.google.ads.c, int]
     candidates:
      k.a(com.google.ads.e, android.app.Activity):java.lang.String
      k.a(java.lang.String, java.lang.String):void
      k.a(com.google.ads.c, boolean):void */
    public final void run() {
        synchronized (this) {
            if (this.f == null || this.c == null) {
                d.e("adRequestWebView was null while trying to load an ad.");
                a(c.INTERNAL_ERROR, false);
                return;
            }
            Activity d2 = this.d.d();
            if (d2 == null) {
                d.e("activity was null while forming an ad request.");
                a(c.INTERNAL_ERROR, false);
                return;
            }
            try {
                this.d.a(new c(this, this.f, null, a(this.e, d2)));
                long m2 = this.d.m();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (m2 > 0) {
                    try {
                        wait(m2);
                    } catch (InterruptedException e2) {
                        d.a("AdLoader InterruptedException while getting the URL: " + e2);
                        return;
                    }
                }
                try {
                    if (!this.i) {
                        if (this.j != null) {
                            a(this.j, false);
                            return;
                        } else if (this.g == null) {
                            d.c("AdLoader timed out after " + m2 + "ms while getting the URL.");
                            a(c.NETWORK_ERROR, false);
                            return;
                        } else {
                            this.c.a(this.g);
                            long elapsedRealtime2 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                            if (elapsedRealtime2 > 0) {
                                try {
                                    wait(elapsedRealtime2);
                                } catch (InterruptedException e3) {
                                    d.a("AdLoader InterruptedException while getting the HTML: " + e3);
                                    return;
                                }
                            }
                            if (!this.i) {
                                if (this.j != null) {
                                    a(this.j, false);
                                    return;
                                } else if (this.b == null) {
                                    d.c("AdLoader timed out after " + m2 + "ms while getting the HTML.");
                                    a(c.NETWORK_ERROR, false);
                                    return;
                                } else {
                                    g h2 = this.d.h();
                                    this.d.i().a();
                                    this.d.a(new c(this, h2, this.f81a, this.b));
                                    long elapsedRealtime3 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                                    if (elapsedRealtime3 > 0) {
                                        try {
                                            wait(elapsedRealtime3);
                                        } catch (InterruptedException e4) {
                                            d.a("AdLoader InterruptedException while loading the HTML: " + e4);
                                            return;
                                        }
                                    }
                                    if (this.k) {
                                        this.d.a(new e(this, this.d, this.h, this.l));
                                    } else {
                                        d.c("AdLoader timed out after " + m2 + "ms while loading the HTML.");
                                        a(c.NETWORK_ERROR, true);
                                    }
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e5) {
                    d.a("An unknown error occurred in AdLoader.", e5);
                    a(c.INTERNAL_ERROR, true);
                }
            } catch (d e6) {
                d.c("Unable to connect to network: " + e6);
                a(c.NETWORK_ERROR, false);
                return;
            } catch (b e7) {
                d.c("Caught internal exception: " + e7);
                a(c.INTERNAL_ERROR, false);
                return;
            }
        }
    }
}
