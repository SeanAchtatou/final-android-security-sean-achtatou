package defpackage;

import android.webkit.WebView;

/* renamed from: c  reason: default package */
final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final String f7a;
    private final String b;
    private final WebView c;
    private /* synthetic */ k d;

    public c(k kVar, WebView webView, String str, String str2) {
        this.d = kVar;
        this.c = webView;
        this.f7a = str;
        this.b = str2;
    }

    public final void run() {
        this.c.loadDataWithBaseURL(this.f7a, this.b, "text/html", "utf-8", null);
    }
}
