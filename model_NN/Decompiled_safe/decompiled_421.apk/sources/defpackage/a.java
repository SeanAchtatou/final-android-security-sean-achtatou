package defpackage;

import android.webkit.WebView;
import com.google.ads.c;

/* renamed from: a  reason: default package */
final class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final h f0a;
    private final WebView b;
    private final j c;
    private final c d;
    private final boolean e;
    private /* synthetic */ k f;

    public a(k kVar, h hVar, WebView webView, j jVar, c cVar, boolean z) {
        this.f = kVar;
        this.f0a = hVar;
        this.b = webView;
        this.c = jVar;
        this.d = cVar;
        this.e = z;
    }

    public final void run() {
        this.b.stopLoading();
        this.b.destroy();
        this.c.a();
        if (this.e) {
            g h = this.f0a.h();
            h.stopLoading();
            h.setVisibility(8);
        }
        this.f0a.a(this.d);
    }
}
