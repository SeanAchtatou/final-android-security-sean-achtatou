package defpackage;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

/* renamed from: i  reason: default package */
public final class i {

    /* renamed from: a  reason: collision with root package name */
    private String f35a;
    private HashMap b;

    public i(Bundle bundle) {
        this.f35a = bundle.getString("action");
        Serializable serializable = bundle.getSerializable("params");
        this.b = serializable instanceof HashMap ? (HashMap) serializable : null;
    }

    private i(String str) {
        this.f35a = str;
    }

    public i(String str, HashMap hashMap) {
        this(str);
        this.b = hashMap;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("action", this.f35a);
        bundle.putSerializable("params", this.b);
        return bundle;
    }

    public final String b() {
        return this.f35a;
    }

    public final HashMap c() {
        return this.b;
    }
}
