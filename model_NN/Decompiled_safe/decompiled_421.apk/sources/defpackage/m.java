package defpackage;

import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: m  reason: default package */
public final class m implements t {
    public final void a(h hVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("u");
        if (str == null) {
            d.e("Could not get URL from click gmsg.");
        } else {
            new Thread(new u(str, webView.getContext().getApplicationContext())).start();
        }
    }
}
