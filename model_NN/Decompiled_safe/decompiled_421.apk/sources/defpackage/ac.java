package defpackage;

import com.google.ads.util.d;
import java.lang.ref.WeakReference;

/* renamed from: ac  reason: default package */
public final class ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference f5a;

    public ac(h hVar) {
        this.f5a = new WeakReference(hVar);
    }

    public final void run() {
        h hVar = (h) this.f5a.get();
        if (hVar == null) {
            d.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            hVar.u();
        }
    }
}
