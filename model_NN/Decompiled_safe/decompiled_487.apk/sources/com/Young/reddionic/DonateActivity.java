package com.Young.reddionic;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import com.Young.reddionic.BillingService;
import com.Young.reddionic.Consts;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class DonateActivity extends Activity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final String DB_INITIALIZED = "db_initialized";
    private static final int DIALOG_BILLING_NOT_SUPPORTED_ID = 2;
    private static final int DIALOG_CANNOT_CONNECT_ID = 1;
    private static final String LOG_TEXT_KEY = "DUNGEONS_LOG_TEXT";
    private static final String TAG = "Dungeons";
    static Activity act;
    Context context;
    private BillingService mBillingService;
    private Button mBuyButton;
    /* access modifiers changed from: private */
    public CatalogAdapter mCatalogAdapter;
    private DungeonsPurchaseObserver mDungeonsPurchaseObserver;
    private Handler mHandler;
    private String mItemName;
    /* access modifiers changed from: private */
    public Set<String> mOwnedItems = new HashSet();
    /* access modifiers changed from: private */
    public Cursor mOwnedItemsCursor;
    private String mPayloadContents = null;
    private PurchaseDatabase mPurchaseDatabase;
    private Spinner mSelectItemSpinner;
    /* access modifiers changed from: private */
    public final RedditSettings mSettings = new RedditSettings();
    private String mSku;
    GoogleAnalyticsTracker tracker;

    private enum Managed {
        MANAGED,
        UNMANAGED
    }

    private class DungeonsPurchaseObserver extends PurchaseObserver {
        public DungeonsPurchaseObserver(Handler handler) {
            super(DonateActivity.this, handler);
        }

        public void onBillingSupported(boolean supported) {
            if (supported) {
                DonateActivity.this.restoreDatabase();
            } else {
                DonateActivity.this.showDialog(2);
            }
        }

        public void onPurchaseStateChange(Consts.PurchaseState purchaseState, String itemId, int quantity, long purchaseTime, String developerPayload) {
            if (purchaseState == Consts.PurchaseState.PURCHASED) {
                DonateActivity.this.mOwnedItems.add(itemId);
                ArrayList<String> arguments = new ArrayList<>();
                arguments.add(DonateActivity.this.mSettings.username);
                Calendar c = Calendar.getInstance();
                arguments.add(String.valueOf(c.get(2) + 1) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + c.get(5) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + c.get(1) + "    " + c.get(10) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + c.get(12) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + c.get(13) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + c.get(14));
                arguments.add(itemId);
            }
            DonateActivity.this.mCatalogAdapter.setOwnedItems(DonateActivity.this.mOwnedItems);
            DonateActivity.this.mOwnedItemsCursor.requery();
        }

        public void onRequestPurchaseResponse(BillingService.RequestPurchase request, Consts.ResponseCode responseCode) {
            if (responseCode != Consts.ResponseCode.RESULT_OK && responseCode == Consts.ResponseCode.RESULT_USER_CANCELED) {
            }
        }

        public void onRestoreTransactionsResponse(BillingService.RestoreTransactions request, Consts.ResponseCode responseCode) {
            if (responseCode == Consts.ResponseCode.RESULT_OK) {
                SharedPreferences.Editor edit = DonateActivity.this.getPreferences(0).edit();
                edit.putBoolean(DonateActivity.DB_INITIALIZED, true);
                edit.commit();
            }
        }
    }

    private static class CatalogEntry {
        public Managed managed;
        public int nameId;
        public String sku;

        public CatalogEntry(String sku2, int nameId2, Managed managed2) {
            this.sku = sku2;
            this.nameId = nameId2;
            this.managed = managed2;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.donate);
        this.mHandler = new Handler();
        this.mDungeonsPurchaseObserver = new DungeonsPurchaseObserver(this.mHandler);
        this.mBillingService = new BillingService();
        this.mBillingService.setContext(this);
        this.mSettings.loadRedditPreferences(getApplicationContext());
        this.context = getApplicationContext();
        act = this;
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-17867595-4", this);
        this.tracker.trackPageView("/DonateActivity");
        this.mPurchaseDatabase = new PurchaseDatabase(this);
        setupWidgets();
        ResponseHandler.register(this.mDungeonsPurchaseObserver);
        if (!this.mBillingService.checkBillingSupported()) {
            showDialog(1);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        ResponseHandler.register(this.mDungeonsPurchaseObserver);
        initializeOwnedItems();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        ResponseHandler.unregister(this.mDungeonsPurchaseObserver);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mPurchaseDatabase.close();
        this.mBillingService.unbind();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return createDialog(R.string.cannot_connect_title, R.string.cannot_connect_message);
            case 2:
                return createDialog(R.string.billing_not_supported_title, R.string.billing_not_supported_message);
            default:
                return null;
        }
    }

    private Dialog createDialog(int titleId, int messageId) {
        final Uri helpUri = Uri.parse(replaceLanguageAndRegion(getString(R.string.help_url)));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titleId).setIcon(17301642).setMessage(messageId).setCancelable(false).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).setNegativeButton((int) R.string.learn_more, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DonateActivity.this.startActivity(new Intent("android.intent.action.VIEW", helpUri));
            }
        });
        return builder.create();
    }

    private String replaceLanguageAndRegion(String str) {
        if (!str.contains("%lang%") && !str.contains("%region%")) {
            return str;
        }
        Locale locale = Locale.getDefault();
        return str.replace("%lang%", locale.getLanguage().toLowerCase()).replace("%region%", locale.getCountry().toLowerCase());
    }

    private void setupWidgets() {
        this.mOwnedItemsCursor = this.mPurchaseDatabase.queryAllPurchasedItems();
        startManagingCursor(this.mOwnedItemsCursor);
        String[] strArr = {"_id", "quantity"};
    }

    /* access modifiers changed from: private */
    public void restoreDatabase() {
        if (!getPreferences(0).getBoolean(DB_INITIALIZED, false)) {
            this.mBillingService.restoreTransactions();
        }
    }

    private void initializeOwnedItems() {
        new Thread(new Runnable() {
            public void run() {
                DonateActivity.this.doInitializeOwnedItems();
            }
        }).start();
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public void doInitializeOwnedItems() {
        Cursor cursor = this.mPurchaseDatabase.queryAllPurchasedItems();
        if (cursor != null) {
            Set<String> ownedItems = new HashSet<>();
            try {
                int productIdCol = cursor.getColumnIndexOrThrow("_id");
                while (cursor.moveToNext()) {
                    ownedItems.add(cursor.getString(productIdCol));
                }
                cursor.close();
                this.mHandler.post(new Runnable() {
                    public void run() {
                    }
                });
            } catch (Throwable th) {
                cursor.close();
                throw th;
            }
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private static class CatalogAdapter extends ArrayAdapter<String> {
        private CatalogEntry[] mCatalog;
        private Set<String> mOwnedItems = new HashSet();

        public CatalogAdapter(Context context, CatalogEntry[] catalog) {
            super(context, 17367048);
            this.mCatalog = catalog;
            for (CatalogEntry element : catalog) {
                add(context.getString(element.nameId));
            }
            setDropDownViewResource(17367049);
        }

        public void setOwnedItems(Set<String> ownedItems) {
            this.mOwnedItems = ownedItems;
            notifyDataSetChanged();
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int position) {
            CatalogEntry entry = this.mCatalog[position];
            if (entry.managed != Managed.MANAGED || !this.mOwnedItems.contains(entry.sku)) {
                return true;
            }
            return false;
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view = super.getDropDownView(position, convertView, parent);
            view.setEnabled(isEnabled(position));
            return view;
        }
    }

    public void onClick(View v) {
        if (v == this.mBuyButton && !this.mBillingService.requestPurchase(this.mSku, this.mPayloadContents)) {
            showDialog(2);
        }
    }

    public void onClick1(View v) {
        if (!this.mBillingService.requestPurchase("dollar_one", null)) {
            showDialog(2);
        }
    }

    public void onClick2(View v) {
        if (!this.mBillingService.requestPurchase("dollar_two", null)) {
            showDialog(2);
        }
    }

    public void onClick5(View v) {
        if (!this.mBillingService.requestPurchase("dollar_five", null)) {
            showDialog(2);
        }
    }

    public void onClick10(View v) {
        if (!this.mBillingService.requestPurchase("dollar_ten", null)) {
            showDialog(2);
        }
    }

    public void onClick25(View v) {
        if (!this.mBillingService.requestPurchase("dollar_twenty_five", null)) {
            showDialog(2);
        }
    }

    public void onClick50(View v) {
        if (!this.mBillingService.requestPurchase("dollar_fifty", null)) {
            showDialog(2);
        }
    }

    class purchase extends AsyncTask<ArrayList<String>, Void, Boolean> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<String>[]) ((ArrayList[]) objArr));
        }

        purchase() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(ArrayList<String>... passing) {
            return Boolean.valueOf(RedditAPI.purchase(passing[0]));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            Log.e("AsyncPost", "Finished");
        }
    }

    public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
    }
}
