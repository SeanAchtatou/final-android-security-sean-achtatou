package com.Young.reddionic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.Young.reddionic.MultiDirectionSlidingDrawer;
import com.Young.reddionic.TouchListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.impl.client.DefaultHttpClient;

public class Browse extends FragmentActivity {
    static final int NUM_ITEMS = 10;
    static ArrayList<Boolean> cancelled = new ArrayList<>();
    static Context context;
    static DatabaseHelper dbh;
    static boolean dontload = false;
    static ArrayList<ArrayListFragment> listFragments = new ArrayList<>();
    static ArrayList<onScrollListener> onScrollListeners = new ArrayList<>();
    static ArrayList<ArrayAdapter> r_adapters = new ArrayList<>();
    static int redditSize = 1;
    protected static ArrayList<String> subredditList = new ArrayList<>();
    static ArrayAdapter the_adapter;
    Activity act;
    boolean backed = false;
    LinearLayout button;
    boolean cancel = false;
    ArrayList<String> checked = new ArrayList<>();
    boolean click = true;
    boolean clickedSubreddit = false;
    ListView commentList;
    int direction = 0;
    MultiDirectionSlidingDrawer drawer;
    boolean drawerOpen = false;
    boolean feedbacking = false;
    boolean firsttime = true;
    FragmentManager fragmentManager;
    TextView handle_current;
    TextView handle_next;
    TextView handle_previous;
    View headerView2;
    public ImageLoader imageLoader;
    RelativeLayout info;
    ArrayList<String> infoArrayList;
    boolean longclick = false;
    MyAdapter mAdapter;
    private final DefaultHttpClient mClient = RedditAPI.createGzipHttpClient();
    /* access modifiers changed from: private */
    public ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        /* Debug info: failed to restart local var, previous not found, register: 9 */
        public void onPageSelected(int arg0) {
            Browse.this.name_previous = null;
            Browse.this.name_current = null;
            Browse.this.name_next = null;
            if (arg0 - 1 >= 0) {
                Browse.this.name_previous = Browse.this.mAdapter.getItem(arg0 - 1).getArguments().getString("name");
            }
            if (arg0 >= 0) {
                Browse.this.name_current = Browse.this.mAdapter.getItem(arg0).getArguments().getString("name");
            }
            if (arg0 + 1 < Browse.redditSize) {
                Browse.this.name_next = Browse.this.mAdapter.getItem(arg0 + 1).getArguments().getString("name");
            }
            if (Browse.this.name_previous == null) {
                Browse.this.handle_previous.setText("");
            } else if (Browse.this.name_previous.equals("fffffffuuuuuuuuuuuu")) {
                Browse.this.handle_previous.setText("< f7u12");
            } else {
                Browse.this.handle_previous.setText("< " + Browse.this.name_previous);
            }
            if (Browse.this.name_current != null) {
                if (Browse.this.name_current.equals("fffffffuuuuuuuuuuuu")) {
                    Browse.this.handle_current.setText("f7u12");
                } else {
                    Browse.this.handle_current.setText(Browse.this.name_current);
                }
            }
            if (Browse.this.name_next == null) {
                Browse.this.handle_next.setText("");
            } else if (Browse.this.name_next.equals("fffffffuuuuuuuuuuuu")) {
                Browse.this.handle_next.setText("f7u12 >");
            } else {
                Browse.this.handle_next.setText(String.valueOf(Browse.this.name_next) + " >");
            }
            if (!RedditAPI.redditBuckets.contains(Browse.this.name_current) || Browse.this.firsttime) {
                Browse.this.firsttime = false;
                if (!Browse.this.clickedSubreddit) {
                    if (RedditAPI.currentBucket == RedditAPI.bucketSize - 1) {
                        Browse.this.threadList.get(RedditAPI.currentBucket).clear();
                        RedditAPI.currentBucket = 0;
                    } else {
                        RedditAPI.currentBucket++;
                    }
                }
                if (!RedditAPI.mAfterBucket.isEmpty()) {
                    RedditAPI.mAfterBucket.set(RedditAPI.currentBucket, null);
                }
                if (!RedditAPI.mBeforeBucket.isEmpty()) {
                    RedditAPI.mBeforeBucket.set(RedditAPI.currentBucket, null);
                }
                Browse.this.threadList.get(RedditAPI.currentBucket).clear();
                if (!RedditAPI.mThingInfoss.isEmpty()) {
                    RedditAPI.mThingInfoss.get(RedditAPI.currentBucket).clear();
                }
                RedditAPI.mLastPages.set(RedditAPI.currentBucket, false);
                Browse.r_adapters.set(RedditAPI.currentBucket, new ThreadListAdapter(Browse.context, Browse.this.threadList.get(RedditAPI.currentBucket)));
                Browse.listFragments.get(Browse.subredditList.indexOf(Browse.this.name_current)).setListAdapter(Browse.r_adapters.get(RedditAPI.currentBucket));
                RedditAPI.redditBuckets.set(RedditAPI.currentBucket, Browse.this.name_current);
                RedditAPI.mSubreddit = Browse.this.name_current;
                if (!Browse.this.rolling) {
                    if (Browse.this.running) {
                        Browse.this.threadLoader.cancel(true);
                        Browse.this.running = false;
                    }
                    if (!Browse.this.running) {
                        Browse.this.threadLoader = new ThreadLoader();
                        Browse.this.threadLoader.execute(new Void[0]);
                        return;
                    }
                    return;
                }
                return;
            }
            Browse.r_adapters.set(RedditAPI.currentBucket, new ThreadListAdapter(Browse.context, Browse.this.threadList.get(RedditAPI.currentBucket)));
            RedditAPI.currentBucket = RedditAPI.redditBuckets.indexOf(Browse.this.name_current);
            Log.e("hey", "I've been here: " + RedditAPI.currentBucket + ": " + Browse.this.name_current);
            Log.e("wt", RedditAPI.redditBuckets.toString());
            RedditAPI.mSubreddit = Browse.this.name_current;
            Log.e("What's letting you through", String.valueOf(Browse.this.running) + " & " + Browse.r_adapters.get(RedditAPI.currentBucket).isEmpty());
            if (!Browse.this.rolling && Browse.this.running) {
                Browse.this.threadLoader.cancel(true);
                Log.e("cancelling?", "yah");
                Browse.this.running = false;
            }
            if (Browse.r_adapters.get(RedditAPI.currentBucket).isEmpty()) {
                if (Browse.cancelled.get(RedditAPI.currentBucket).booleanValue()) {
                    Browse.cancelled.set(RedditAPI.currentBucket, false);
                }
                if (!Browse.this.rolling) {
                    Log.e("running", new StringBuilder(String.valueOf(Browse.this.running)).toString());
                    if (!Browse.this.running || Browse.r_adapters.get(RedditAPI.currentBucket).isEmpty()) {
                        Browse.r_adapters.set(RedditAPI.currentBucket, new ThreadListAdapter(Browse.context, Browse.this.threadList.get(RedditAPI.currentBucket)));
                        Browse.listFragments.get(RedditAPI.currentBucket).setListAdapter(Browse.r_adapters.get(RedditAPI.currentBucket));
                        Browse.this.threadLoader = new ThreadLoader();
                        Browse.this.threadLoader.execute(new Void[0]);
                    }
                }
            }
            try {
                Browse.listFragments.get(RedditAPI.currentBucket).getListView().setOnScrollListener(Browse.onScrollListeners.get(RedditAPI.currentBucket));
            } catch (Exception e) {
            }
        }
    };
    ViewPager mPager;
    String mReddit = "";
    private final RedditSettings mSettings = new RedditSettings();
    String name_current = null;
    String name_next = null;
    String name_previous = null;
    ArrayList<String> names = new ArrayList<>();
    private TouchListView.DropListener onDrop = new TouchListView.DropListener() {
        public void drop(int from, int to) {
            if (to == Browse.this.s_adapter.getCount()) {
                to--;
            }
            String item = (String) Browse.this.s_adapter.getItem(from);
            Browse.this.s_adapter.remove(item);
            Browse.this.s_adapter.insert(item, to);
            Browse.dbh.clearSubReddit();
            Iterator<String> it = Browse.subredditList.iterator();
            while (it.hasNext()) {
                Browse.dbh.insertReddit(0, it.next(), 1);
            }
        }
    };
    private TouchListView.RemoveListener onRemove = new TouchListView.RemoveListener() {
        public void remove(int which) {
            new Subscriber((String) Browse.this.s_adapter.getItem(which), "unsub").execute(new Void[0]);
            Browse.this.s_adapter.remove((String) Browse.this.s_adapter.getItem(which));
            Browse.dbh.dropSubReddit();
            Iterator<String> it = Browse.subredditList.iterator();
            while (it.hasNext()) {
                Browse.dbh.insertReddit(0, it.next(), 1);
            }
        }
    };
    boolean onRestart = false;
    public int pos;
    String postID = "";
    PopupWindow pw;
    boolean rolling = false;
    boolean running = false;
    /* access modifiers changed from: private */
    public IconicAdapter s_adapter = null;
    boolean saved = false;
    int savedBucket = -1;
    int savedCount = -1;
    SubredditLoader subredditLoader;
    boolean subredditing = false;
    String tempurl;
    View theActionBar;
    protected ArrayList<ArrayList<ThingInfo>> threadList = new ArrayList<>();
    ThreadLoader threadLoader;
    int thread_count = 0;
    GoogleAnalyticsTracker tracker;

    public static class ViewHolder {
        public LinearLayout background;
        public LinearLayout layer;
        public TextView margin;
        public int marginHeight;
        public int marginWidth;
        public TextView otherinfo;
        public ImageButton thumbnail;
        public TextView title;
        public TextView vote;
        public LinearLayout voteCover;
        public ImageView voteimg;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.browse);
        new ActionBar();
        context = getApplicationContext();
        this.act = this;
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-17867595-4", this);
        this.tracker.trackPageView("/Browse");
        this.firsttime = true;
        this.headerView2 = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.general_info, (ViewGroup) null);
        this.handle_previous = (TextView) findViewById(R.id.handle_previous);
        this.handle_current = (TextView) findViewById(R.id.handle_current);
        this.handle_next = (TextView) findViewById(R.id.handle_next);
        this.drawer = (MultiDirectionSlidingDrawer) findViewById(R.id.drawer);
        this.theActionBar = findViewById(R.id.actionbar);
        ((Button) this.theActionBar.findViewById(R.id.refresh)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Browse.this.threadList.get(RedditAPI.currentBucket).clear();
                if (!RedditAPI.mThingInfoss.isEmpty()) {
                    RedditAPI.mThingInfoss.get(RedditAPI.currentBucket).clear();
                }
                RedditAPI.mAfterBucket.set(RedditAPI.currentBucket, null);
                RedditAPI.mBeforeBucket.set(RedditAPI.currentBucket, null);
                RedditAPI.mLastAfterBucket.set(RedditAPI.currentBucket, null);
                RedditAPI.mAfterBucket.set(RedditAPI.currentBucket, null);
                RedditAPI.mLastPages.set(RedditAPI.currentBucket, false);
                Browse.r_adapters.set(RedditAPI.currentBucket, new ThreadListAdapter(Browse.context, Browse.this.threadList.get(RedditAPI.currentBucket)));
                Browse.listFragments.get(Browse.subredditList.indexOf(Browse.this.name_current)).setListAdapter(Browse.r_adapters.get(RedditAPI.currentBucket));
                RedditAPI.redditBuckets.set(RedditAPI.currentBucket, Browse.this.name_current);
                RedditAPI.mSubreddit = Browse.this.name_current;
                if (Browse.this.running) {
                    Browse.this.threadLoader.cancel(true);
                    Browse.this.running = false;
                }
                if (!Browse.this.running) {
                    Browse.this.threadLoader = new ThreadLoader();
                    Browse.this.threadLoader.execute(new Void[0]);
                }
                Browse.r_adapters.get(RedditAPI.currentBucket).notifyDataSetChanged();
            }
        });
        this.drawer.setOnDrawerOpenListener(new MultiDirectionSlidingDrawer.OnDrawerOpenListener() {
            public void onDrawerOpened() {
                Browse.this.drawerOpen = true;
                Browse.this.handle_current.setText("");
                Browse.this.handle_previous.setText("");
                Browse.this.handle_next.setText("");
                Browse.this.tracker.trackEvent("Browse", "drawer_open", "", 0);
            }
        });
        this.drawer.setOnDrawerCloseListener(new MultiDirectionSlidingDrawer.OnDrawerCloseListener() {
            public void onDrawerClosed() {
                Browse.this.drawerOpen = false;
                if (Browse.this.name_previous == null) {
                    Browse.this.handle_previous.setText("");
                } else if (Browse.this.name_previous.equals("fffffffuuuuuuuuuuuu")) {
                    Browse.this.handle_previous.setText("< 7f12u");
                } else {
                    Browse.this.handle_previous.setText("< " + Browse.this.name_previous);
                }
                if (Browse.this.name_current != null) {
                    if (Browse.this.name_current.equals("fffffffuuuuuuuuuuuu")) {
                        Browse.this.handle_current.setText("7f12u");
                    } else {
                        Browse.this.handle_current.setText(Browse.this.name_current);
                    }
                }
                if (Browse.this.name_next == null) {
                    Browse.this.handle_next.setText("");
                } else if (Browse.this.name_next.equals("fffffffuuuuuuuuuuuu")) {
                    Browse.this.handle_next.setText("7f12u >");
                } else {
                    Browse.this.handle_next.setText(String.valueOf(Browse.this.name_next) + " >");
                }
            }
        });
        resetVariables();
        TouchListView tlv = (TouchListView) findViewById(R.id.subreddit);
        tlv.addFooterView((LinearLayout) View.inflate(this, R.layout.subreddit_list_footer, null));
        this.s_adapter = new IconicAdapter();
        tlv.setAdapter((ListAdapter) this.s_adapter);
        tlv.setDropListener(this.onDrop);
        tlv.setRemoveListener(this.onRemove);
        CookieSyncManager.createInstance(getApplicationContext());
        this.mSettings.loadRedditPreferences(getApplicationContext());
    }

    private void resetVariables() {
        dbh = new DatabaseHelper(context);
        listFragments = new ArrayList<>();
        RedditAPI.mCount = 25;
        r_adapters = new ArrayList<>();
        RedditAPI.mLastPages.clear();
        RedditAPI.mThingInfoss.clear();
        onScrollListeners = new ArrayList<>();
        cancelled = new ArrayList<>();
        this.threadList = new ArrayList<>();
        this.threadList.clear();
        RedditAPI.mAfterBucket.clear();
        RedditAPI.mBeforeBucket.clear();
        RedditAPI.mLastAfterBucket.clear();
        RedditAPI.mAfterBucket.clear();
        this.backed = false;
        for (int x = 0; x < RedditAPI.bucketSize; x++) {
            r_adapters.add(null);
            RedditAPI.mLastPages.add(false);
            onScrollListeners.add(new onScrollListener());
            this.threadList.add(new ArrayList());
            cancelled.add(false);
        }
        RedditAPI.redditBuckets = new ArrayList<>();
        RedditAPI.redditBuckets.clear();
        for (int x2 = 0; x2 < RedditAPI.bucketSize; x2++) {
            RedditAPI.redditBuckets.add(null);
        }
        this.subredditLoader = new SubredditLoader();
        this.subredditLoader.execute(new Void[0]);
    }

    private void restoreLastNonConfigurationInstance() {
        this.threadList = (ArrayList) getLastNonConfigurationInstance();
    }

    public void onBackPressed() {
        if (this.drawerOpen) {
            this.drawer.animateClose();
        } else if (this.feedbacking) {
            this.pw.dismiss();
            this.feedbacking = false;
        } else if (!this.click) {
            this.info.setVisibility(0);
            this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
            this.longclick = false;
            this.click = true;
        } else {
            if (this.running) {
                this.threadLoader.cancel(true);
                this.running = false;
            }
            this.backed = true;
            super.onBackPressed();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    class FeedbackAdapter extends ArrayAdapter<String> {
        FeedbackAdapter() {
            super(Browse.context, (int) R.layout.included_list_item, Browse.this.names);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = Browse.this.getLayoutInflater().inflate((int) R.layout.included_list_item, parent, false);
            }
            TextView label = (TextView) row.findViewById(R.id.label);
            label.setText(Browse.this.names.get(position));
            label.setTag(Integer.valueOf(position));
            CheckBox checkbox = (CheckBox) row.findViewById(R.id.check);
            if (Browse.this.checked.get(position).equals("")) {
                checkbox.setChecked(false);
            } else {
                checkbox.setChecked(true);
            }
            checkbox.setTag(Integer.valueOf(position));
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* Debug info: failed to restart local var, previous not found, register: 3 */
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Browse.this.checked.set(((Integer) buttonView.getTag()).intValue(), "checked");
                    } else {
                        Browse.this.checked.set(((Integer) buttonView.getTag()).intValue(), "");
                    }
                }
            });
            return row;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.feedback /*2131034193*/:
                View popup = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.r_feedback_popup, (ViewGroup) null, false);
                this.pw = new PopupWindow(popup, -1, -1, true);
                if (this.names.size() == 0) {
                    this.names.add("Date of the first run");
                    this.names.add("Version Number");
                    this.names.add("Reddit ID");
                    this.names.add("Current View");
                    this.names.add("Device Name");
                    this.names.add("Device SDK");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                } else {
                    for (int x = 0; x < this.checked.size(); x++) {
                        this.checked.set(x, "checked");
                    }
                }
                this.pw.setBackgroundDrawable(new BitmapDrawable());
                this.pw.showAtLocation(findViewById(R.id.browse_main), 0, 0, 0);
                ((LinearLayout) popup.findViewById(R.id.requestButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.request_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView1)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Request");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.request_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.improvementButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.improvement_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView2)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Improvement");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.improvement_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.bugButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.bug_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView3)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Bug");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.bug_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                this.feedbacking = true;
                return true;
            case R.id.donate /*2131034194*/:
                Toast.makeText(context, "Almost implemented yet", 1).show();
                return true;
            default:
                return false;
        }
    }

    public void submitClick(View button2) {
        ArrayList<Object> arguments = (ArrayList) button2.getTag();
        arguments.add(((EditText) ((RelativeLayout) button2.getParent()).findViewById(R.id.text)).getText().toString());
        if (this.checked.get(0).equals("checked")) {
            arguments.add(this.mSettings.firstRunDate);
        } else {
            arguments.add("");
        }
        try {
            if (this.checked.get(1).equals("checked")) {
                arguments.add(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
            } else {
                arguments.add("");
            }
        } catch (PackageManager.NameNotFoundException e) {
            arguments.add("");
            e.printStackTrace();
        }
        if (this.checked.get(2).equals("checked")) {
            arguments.add(this.mSettings.username);
        } else {
            arguments.add("");
        }
        if (this.checked.get(3).equals("checked")) {
            View t = findViewById(R.id.browse_main);
            t.setDrawingCacheEnabled(true);
            t.destroyDrawingCache();
            arguments.add(t.getDrawingCache());
        } else {
            arguments.add("");
        }
        if (this.checked.get(4).equals("checked")) {
            arguments.add(Build.PRODUCT);
        } else {
            arguments.add("");
        }
        if (this.checked.get(5).equals("checked")) {
            arguments.add(Build.VERSION.RELEASE);
        } else {
            arguments.add("");
        }
        new Feedback().execute(arguments);
        this.pw.dismiss();
    }

    class Feedback extends AsyncTask<ArrayList<Object>, Void, Boolean> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<Object>[]) ((ArrayList[]) objArr));
        }

        Feedback() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(ArrayList<Object>... passing) {
            return Boolean.valueOf(RedditAPI.feedback(passing[0]));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            Browse.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            Browse.this.stopLoading();
            if (result.booleanValue()) {
                Toast.makeText(Browse.context, "Feedback Submitted!\nThank you for your feedback! ", 1).show();
            } else {
                Toast.makeText(Browse.context, "Feedback Failed!", 1).show();
            }
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void onMoreClick(View button2) {
        this.pw = new PopupWindow(((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.add_subreddit_popup, (ViewGroup) null, false), -1, -1, true);
        this.pw.setBackgroundDrawable(new BitmapDrawable());
        this.pw.showAtLocation(findViewById(R.id.browse_main), 0, 0, 0);
    }

    public void onVisitClick(View button2) {
        View temp = (View) button2.getParent().getParent().getParent();
        ((LinearLayout) temp.findViewById(R.id.visit_subreddit)).setVisibility(0);
        ((LinearLayout) temp.findViewById(R.id.button_subreddit)).setVisibility(8);
    }

    public void onVisit_GoClick(View button2) {
        View view = (View) button2.getParent().getParent().getParent();
        this.pw.dismiss();
    }

    public void onAddClick(View button2) {
        View temp = (View) button2.getParent().getParent().getParent();
        ((LinearLayout) temp.findViewById(R.id.add_subreddit)).setVisibility(0);
        ((LinearLayout) temp.findViewById(R.id.button_subreddit)).setVisibility(8);
    }

    public void onAdd_GoClick(View button2) {
        String name = ((EditText) ((View) button2.getParent()).findViewById(R.id.name)).getText().toString();
        new Subscriber(name, "sub").execute(new Void[0]);
        dbh.insertReddit(0, name, 1);
        subredditList.add(name);
        redditSize++;
        this.s_adapter.notifyDataSetChanged();
        int beforeB = RedditAPI.currentBucket;
        this.mPager.setCurrentItem(subredditList.size() - 2);
        this.mPager.setCurrentItem(subredditList.size() - 1);
        this.mPager.setCurrentItem(beforeB);
        this.pw.dismiss();
    }

    public void onGroupClick(View button2) {
        View temp = (View) button2.getParent().getParent().getParent();
        ((LinearLayout) temp.findViewById(R.id.group_subreddit)).setVisibility(0);
        ((LinearLayout) temp.findViewById(R.id.button_subreddit)).setVisibility(8);
    }

    public void onGroup_AddClick(View button2) {
        View view = (View) button2.getParent().getParent().getParent();
    }

    public void onGroup_GoClick(View button2) {
        View view = (View) button2.getParent().getParent().getParent();
        this.pw.dismiss();
    }

    class IconicAdapter extends ArrayAdapter<String> {
        IconicAdapter() {
            super(Browse.context, (int) R.layout.subreddit_list_item, Browse.subredditList);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = Browse.this.getLayoutInflater().inflate((int) R.layout.subreddit_list_item, parent, false);
            }
            TextView label = (TextView) row.findViewById(R.id.label);
            label.setText(Browse.subredditList.get(position));
            label.setTag(Integer.valueOf(position));
            label.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Browse.this.drawer.animateClose();
                    RedditAPI.currentBucket = ((Integer) v.getTag()).intValue();
                    Browse.this.mPager.setCurrentItem(((Integer) v.getTag()).intValue());
                }
            });
            return row;
        }
    }

    public static class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        public int getCount() {
            return Browse.redditSize;
        }

        public Fragment getItem(int position) {
            Iterator<ArrayListFragment> it = Browse.listFragments.iterator();
            while (it.hasNext()) {
                ArrayListFragment f = it.next();
                if (f.getArguments().getString("name").equals(Browse.subredditList.get(position))) {
                    return f;
                }
            }
            return ArrayListFragment.newInstance(position);
        }
    }

    public static class ArrayListFragment extends ListFragment {
        int mNum;

        /*  JADX ERROR: Method load error
            jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.StringBuilder.append(long):java.lang.StringBuilder in method: com.Young.reddionic.Browse.ArrayListFragment.onListItemClick(android.widget.ListView, android.view.View, int, long):void, dex: classes.dex
            	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
            	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
            	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:312)
            	at jadx.core.ProcessClass.process(ProcessClass.java:36)
            	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
            	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
            	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.StringBuilder.append(long):java.lang.StringBuilder
            	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
            	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
            	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
            	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
            	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
            	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
            	... 6 more
            */
        public void onListItemClick(android.widget.ListView r1, android.view.View r2, int r3, long r4) {
            /*
                r3 = this;
                java.lang.String r0 = "FragmentList"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                java.lang.String r2 = "Item clicked: "
                r1.<init>(r2)
                java.lang.StringBuilder r1 = r1.append(r7)
                java.lang.String r1 = r1.toString()
                android.util.Log.i(r0, r1)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.Browse.ArrayListFragment.onListItemClick(android.widget.ListView, android.view.View, int, long):void");
        }

        static ArrayListFragment newInstance(int num) {
            ArrayListFragment f = new ArrayListFragment();
            Bundle args = new Bundle();
            args.putInt("num", num);
            args.putString("name", Browse.subredditList.get(num));
            f.setArguments(args);
            Browse.listFragments.add(f);
            return f;
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate((int) R.layout.thread_list, container, false);
        }

        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }
    }

    public class onScrollListener implements AbsListView.OnScrollListener {
        public onScrollListener() {
        }

        public void onScroll(AbsListView view, int firstVisible, int visibleCount, int totalCount) {
            if (totalCount != 0 && firstVisible + visibleCount >= totalCount && !RedditAPI.mLastPages.get(RedditAPI.currentBucket).booleanValue() && !Browse.this.running) {
                Browse.this.threadLoader = new ThreadLoader();
                Browse.this.threadLoader.execute(new Void[0]);
            }
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 1 && !Browse.this.click) {
                Browse.this.info.setVisibility(0);
                Browse.this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
                Browse.this.longclick = false;
                Browse.this.click = true;
            }
        }
    }

    final class ThreadListAdapter extends ArrayAdapter<ThingInfo> {
        private LayoutInflater mInflater;
        public List<ThingInfo> obj;

        public ThreadListAdapter(Context context, List<ThingInfo> objects) {
            super(context, 0, objects);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            Browse.this.imageLoader = new ImageLoader(context);
            this.obj = objects;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (convertView == null) {
                view = this.mInflater.inflate((int) R.layout.thread_list_item, (ViewGroup) null);
                holder = new ViewHolder();
                holder.title = (TextView) view.findViewById(R.id.title);
                holder.otherinfo = (TextView) view.findViewById(R.id.otherinfo);
                holder.thumbnail = (ImageButton) view.findViewById(R.id.thumbnail);
                holder.vote = (TextView) view.findViewById(R.id.vote);
                holder.layer = (LinearLayout) view.findViewById(R.id.container);
                holder.background = (LinearLayout) view.findViewById(R.id.ThreadLinearLayout);
                holder.voteCover = (LinearLayout) view.findViewById(R.id.LinearLayout05);
                holder.voteimg = (ImageView) view.findViewById(R.id.voteimg);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            ThingInfo item = this.obj.get(position);
            String temp = item.getUrl();
            int isand = temp.indexOf("&");
            if (isand != -1) {
                temp = temp.substring(0, isand);
            }
            String url = item.getThumbnail();
            if (url.contains("noimage.png") || url == null) {
                if (item.getDomain().equals("imgur.com")) {
                    String newURL = String.valueOf(temp) + "s.png";
                    item.setThumbnail(newURL);
                    url = newURL;
                } else if (item.getDomain().equals("i.imgur.com")) {
                    String newURL2 = String.valueOf(temp.substring(0, temp.length() - 4)) + "s" + temp.substring(temp.length() - 4);
                    item.setThumbnail(newURL2);
                    url = newURL2;
                } else if (item.getDomain().equals("youtube.com")) {
                    String newURL3 = "http://img.youtube.com/vi/" + temp.substring(31) + "/default.jpg";
                    item.setThumbnail(newURL3);
                    url = newURL3;
                } else if (item.getDomain().equals("min.us")) {
                    String newURL4 = String.valueOf(temp.substring(0, 7)) + "k." + temp.substring(7, 14) + "i" + temp.substring(15, temp.length()) + ".png";
                    item.setThumbnail(newURL4);
                    url = newURL4;
                }
            }
            if (url.contains("noimage.png")) {
                url = "http://i.imgur.com/30mYs.png";
                item.setThumbnail(url);
            }
            if (item.getDomain().contains("self")) {
                url = "http://i.imgur.com/VeENN.png";
                item.setThumbnail(url);
            }
            if (url == null || "".equals(url)) {
                item.setThumbnail("http://i.imgur.com/30mYs.png");
                Browse.this.imageLoader.DisplayImage("http://i.imgur.com/30mYs.png", Browse.context, Browse.this.act, holder.thumbnail, 0);
            } else {
                Browse.this.imageLoader.DisplayImage(url, Browse.context, Browse.this.act, holder.thumbnail, 0);
            }
            holder.thumbnail.setTag(item);
            holder.title.setText(String.valueOf(item.getTitle()) + " [" + item.getDomain() + "]");
            String subredd = item.getSubreddit();
            if (subredd.equals("fffffffuuuuuuuuuuuu")) {
                subredd = "7f12u";
            }
            holder.otherinfo.setText(String.valueOf(item.getNum_comments()) + " comments in " + subredd);
            Browse.this.pos = position;
            holder.thumbnail.setOnClickListener(new View.OnClickListener() {
                /* Debug info: failed to restart local var, previous not found, register: 7 */
                public void onClick(View v) {
                    if (((ThingInfo) v.getTag()).getDomain().contains("self")) {
                        Browse.this.startActivity(new Intent(Browse.this.act, CommentsView.class).putExtra("url", ((ThingInfo) v.getTag()).getUrl()).putExtra("title", ((ThingInfo) v.getTag()).getTitle()).putExtra("points", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getScore())).toString()).putExtra("comments", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getNum_comments())).toString()).putExtra("likes", new StringBuilder().append(((ThingInfo) v.getTag()).getLikes()).toString()).putExtra("id", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getId())).toString()).putExtra("name", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getName())).toString()).putExtra("thumbnail", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getThumbnail())).toString()).putExtra("subreddit", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getSubreddit())).toString()).putExtra("author", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getAuthor())).toString()).putExtra("time_ago", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getCreated_utc())).toString()).putExtra("selftext", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getSelftext_html())).toString()).putExtra("url", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getUrl())).toString()));
                    } else if (((ThingInfo) v.getTag()).getDomain().contains("market.android.com") || ((ThingInfo) v.getTag()).getDomain().contains("youtube.com") || ((ThingInfo) v.getTag()).getDomain().contains("youtu.be")) {
                        Browse.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(((ThingInfo) v.getTag()).getUrl())));
                    } else {
                        Browse.this.startActivity(new Intent(Browse.this.act, Webview.class).putExtra("url", ((ThingInfo) v.getTag()).getUrl()).putExtra("title", ((ThingInfo) v.getTag()).getTitle()).putExtra("points", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getScore())).toString()).putExtra("comments", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getNum_comments())).toString()).putExtra("likes", new StringBuilder().append(((ThingInfo) v.getTag()).getLikes()).toString()).putExtra("modhash", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getId())).toString()).putExtra("name", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getName())).toString()).putExtra("thumbnail", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getThumbnail())).toString()).putExtra("subreddit", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getSubreddit())).toString()));
                        Browse.this.postID = ((ThingInfo) v.getTag()).getName();
                        Browse.this.mReddit = ((ThingInfo) v.getTag()).getSubreddit();
                    }
                }
            });
            holder.vote.setText(new StringBuilder(String.valueOf(item.getScore())).toString());
            if (item.getLikes() == null) {
                holder.voteimg.setImageResource(R.drawable.noarrow);
            } else if (item.getLikes().booleanValue()) {
                holder.voteimg.setImageResource(R.drawable.uparrow);
            } else {
                holder.voteimg.setImageResource(R.drawable.downarrow);
            }
            Browse.this.tempurl = "http://www.reddit.com" + item.getPermalink();
            holder.layer.setTag(item);
            holder.layer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Browse.this.click) {
                        Browse.this.startActivity(new Intent(Browse.this.act, CommentsView.class).putExtra("url", ((ThingInfo) v.getTag()).getUrl()).putExtra("title", ((ThingInfo) v.getTag()).getTitle()).putExtra("points", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getScore())).toString()).putExtra("comments", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getNum_comments())).toString()).putExtra("likes", new StringBuilder().append(((ThingInfo) v.getTag()).getLikes()).toString()).putExtra("id", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getId())).toString()).putExtra("name", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getName())).toString()).putExtra("thumbnail", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getThumbnail())).toString()).putExtra("subreddit", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getSubreddit())).toString()).putExtra("author", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getAuthor())).toString()).putExtra("time_ago", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getCreated_utc())).toString()).putExtra("selftext", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getSelftext_html())).toString()));
                    }
                }
            });
            holder.layer.setLongClickable(true);
            holder.layer.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(final View v) {
                    v.performHapticFeedback(0);
                    Browse.this.longclick = true;
                    if (!Browse.this.click) {
                        Browse.this.info.setVisibility(0);
                        Browse.this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
                    }
                    Browse.this.click = false;
                    Browse.this.info = (RelativeLayout) v.findViewById(R.id.info);
                    Browse.this.info.setVisibility(8);
                    Browse.this.button = (LinearLayout) v.findViewById(R.id.LinearLayout04);
                    ((ImageButton) Browse.this.button.findViewById(R.id.up)).setImageResource(R.drawable.upvote);
                    ((ImageButton) Browse.this.button.findViewById(R.id.star)).setImageResource(R.drawable.star_);
                    ((ImageButton) Browse.this.button.findViewById(R.id.reply)).setImageResource(R.drawable.hide_);
                    ((ImageButton) Browse.this.button.findViewById(R.id.share)).setImageResource(R.drawable.share_);
                    ((ImageButton) Browse.this.button.findViewById(R.id.down)).setImageResource(R.drawable.downvote);
                    Browse.this.postID = ((ThingInfo) v.getTag()).getName();
                    Browse.this.mReddit = ((ThingInfo) v.getTag()).getSubreddit();
                    Browse.this.button.setVisibility(0);
                    View findViewById = v.findViewById(R.id.up);
                    final ViewHolder viewHolder = holder;
                    findViewById.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v2) {
                            Browse.this.info.setVisibility(0);
                            Browse.this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
                            ThingInfo ti = (ThingInfo) v.getTag();
                            if (ti.getLikes() == null) {
                                ti.setLikes(true);
                                ti.setScore(ti.getScore() + 1);
                                viewHolder.vote.setText(new StringBuilder(String.valueOf(ti.getScore())).toString());
                                viewHolder.voteimg.setImageResource(R.drawable.uparrow);
                                Browse.this.direction = 1;
                            } else if (ti.getLikes().booleanValue()) {
                                ti.setLikes(null);
                                ti.setScore(ti.getScore() - 1);
                                viewHolder.vote.setText(new StringBuilder(String.valueOf(ti.getScore())).toString());
                                viewHolder.voteimg.setImageResource(R.drawable.noarrow);
                                Browse.this.direction = 0;
                            } else {
                                ti.setLikes(true);
                                ti.setScore(ti.getScore() + 2);
                                viewHolder.vote.setText(new StringBuilder(String.valueOf(ti.getScore())).toString());
                                viewHolder.voteimg.setImageResource(R.drawable.uparrow);
                                Browse.this.direction = 1;
                            }
                            new Voter().execute(new Void[0]);
                            Browse.this.click = true;
                            Browse.this.longclick = false;
                        }
                    });
                    v.findViewById(R.id.star).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v2) {
                            Browse.this.info.setVisibility(0);
                            Browse.this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
                            RedditAPI.save(Browse.this.postID.toString(), "save");
                            Browse.this.click = true;
                            Browse.this.longclick = false;
                        }
                    });
                    v.findViewById(R.id.reply).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v2) {
                            Browse.this.info.setVisibility(0);
                            Browse.this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
                            RedditAPI.hide(Browse.this.postID.toString(), "hide");
                            Browse.this.click = true;
                            Browse.this.longclick = false;
                        }
                    });
                    v.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v2) {
                            Browse.this.info.setVisibility(0);
                            Browse.this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
                            Browse.this.click = true;
                            Browse.this.longclick = false;
                        }
                    });
                    View findViewById2 = v.findViewById(R.id.down);
                    final ViewHolder viewHolder2 = holder;
                    findViewById2.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v2) {
                            Browse.this.info.setVisibility(0);
                            Browse.this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
                            ThingInfo ti = (ThingInfo) v.getTag();
                            if (ti.getLikes() == null) {
                                ti.setLikes(false);
                                ti.setScore(ti.getScore() - 1);
                                viewHolder2.vote.setText(new StringBuilder(String.valueOf(ti.getScore())).toString());
                                viewHolder2.voteimg.setImageResource(R.drawable.downarrow);
                                Browse.this.direction = -1;
                            } else if (ti.getLikes().booleanValue()) {
                                ti.setLikes(false);
                                ti.setScore(ti.getScore() - 2);
                                viewHolder2.vote.setText(new StringBuilder(String.valueOf(ti.getScore())).toString());
                                viewHolder2.voteimg.setImageResource(R.drawable.downarrow);
                                Browse.this.direction = -1;
                            } else {
                                ti.setLikes(null);
                                ti.setScore(ti.getScore() + 1);
                                viewHolder2.vote.setText(new StringBuilder(String.valueOf(ti.getScore())).toString());
                                viewHolder2.voteimg.setImageResource(R.drawable.noarrow);
                                Browse.this.direction = 0;
                            }
                            new Voter().execute(new Void[0]);
                            Browse.this.click = true;
                            Browse.this.longclick = false;
                        }
                    });
                    return false;
                }
            });
            return view;
        }
    }

    class Voter extends AsyncTask<Void, Void, Void> {
        Voter() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            RedditAPI.vote(Browse.this.mReddit, Browse.this.postID, Browse.this.direction);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
        }
    }

    class Subscriber extends AsyncTask<Void, Void, Void> {
        String theReddit;
        String theSub;

        public Subscriber(String reddit, String sub) {
            this.theReddit = reddit;
            this.theSub = sub;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground() {
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            RedditAPI.subscribe(this.theReddit, this.theSub);
            return null;
        }
    }

    class ThreadLoader extends AsyncTask<Void, Void, Void> {
        ThreadLoader() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            Browse.this.running = true;
            if (Browse.this.rolling) {
                return null;
            }
            RedditAPI.getThread(0);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            Log.e("starting", "threadLoader");
            Browse.this.running = true;
            Browse.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            try {
                Browse.this.threadList.get(RedditAPI.currentBucket).addAll(RedditAPI.mThingInfoss.get(RedditAPI.currentBucket));
                Browse.r_adapters.get(RedditAPI.currentBucket).notifyDataSetChanged();
                try {
                    Browse.listFragments.get(RedditAPI.currentBucket).getListView().setOnScrollListener(Browse.onScrollListeners.get(RedditAPI.currentBucket));
                } catch (Exception e) {
                }
                Browse.this.running = false;
                RedditAPI.mCount = 25;
            } catch (Exception e2) {
                Log.e("Exception", e2.toString());
            }
            Browse.this.running = false;
            Browse.this.stopLoading();
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            Log.e("cancelled", "yah");
            RedditAPI.mAfterBucket.set(RedditAPI.currentBucket, RedditAPI.mLastAfterBucket.get(RedditAPI.currentBucket));
            RedditAPI.mBeforeBucket.set(RedditAPI.currentBucket, RedditAPI.mLastBeforeBucket.get(RedditAPI.currentBucket));
            Browse.this.cancel = true;
            RedditAPI.mCount = 25;
            Browse.this.stopLoading();
        }
    }

    class SubredditLoader extends AsyncTask<Void, Void, Void> {
        private final ProgressDialog dialog;

        SubredditLoader() {
            this.dialog = new ProgressDialog(Browse.this.act);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            RedditAPI.getSubreddit();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            Browse.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            Browse.subredditList.clear();
            Browse.subredditList.addAll(RedditAPI.reddits);
            Browse.this.s_adapter.notifyDataSetChanged();
            Browse.redditSize = Browse.subredditList.size();
            ArrayListFragment.newInstance(0);
            RedditAPI.currentBucket = 0;
            Browse.r_adapters.set(RedditAPI.currentBucket, new ThreadListAdapter(Browse.context, Browse.this.threadList.get(RedditAPI.currentBucket)));
            Browse.listFragments.get(0).setListAdapter(Browse.r_adapters.get(RedditAPI.currentBucket));
            RedditAPI.redditBuckets.set(0, Browse.subredditList.get(0));
            Browse.this.mAdapter = new MyAdapter(Browse.this.getSupportFragmentManager());
            Browse.this.mPager = (ViewPager) Browse.this.findViewById(R.id.pager);
            Browse.this.mPager.setAdapter(Browse.this.mAdapter);
            Browse.this.mPager.setOnPageChangeListener(Browse.this.mPageChangeListener);
            Browse.this.name_current = Browse.this.mAdapter.getItem(0).getArguments().getString("name");
            if (1 < Browse.redditSize) {
                Browse.this.name_next = Browse.this.mAdapter.getItem(1).getArguments().getString("name");
            }
            if (Browse.this.name_previous == null) {
                Browse.this.handle_previous.setText("");
            } else if (Browse.this.name_previous.equals("fffffffuuuuuuuuuuuu")) {
                Browse.this.handle_previous.setText("< 7f12u");
            } else {
                Browse.this.handle_previous.setText("< " + Browse.this.name_previous);
            }
            if (Browse.this.name_current != null) {
                if (Browse.this.name_current.equals("fffffffuuuuuuuuuuuu")) {
                    Browse.this.handle_current.setText("7f12u");
                } else {
                    Browse.this.handle_current.setText(Browse.this.name_current);
                }
            }
            if (Browse.this.name_next == null) {
                Browse.this.handle_next.setText("");
            } else if (Browse.this.name_next.equals("fffffffuuuuuuuuuuuu")) {
                Browse.this.handle_next.setText("7f12u >");
            } else {
                Browse.this.handle_next.setText(String.valueOf(Browse.this.name_next) + " >");
            }
            Browse.this.rolling = true;
            for (int x = 0; x < Browse.subredditList.size(); x++) {
                Browse.this.mPager.setCurrentItem(x);
            }
            Browse.this.rolling = false;
            Browse.this.mPager.setCurrentItem(0);
            Browse.this.mPager.scrollTo(0, 0);
            if (Browse.this.savedBucket == 0) {
                Browse.this.mPager.setCurrentItem(0);
                Browse.this.mPager.scrollTo(0, 0);
            } else if (Browse.this.savedBucket != -1) {
                Browse.this.mPager.setCurrentItem(Browse.this.savedBucket);
                RedditAPI.mCount = Browse.this.savedCount;
                RedditAPI.currentBucket = RedditAPI.redditBuckets.indexOf(Browse.this.name_current);
                RedditAPI.mSubreddit = Browse.this.name_current;
                if (Browse.this.running) {
                    Browse.this.threadLoader.cancel(true);
                    Browse.this.running = false;
                }
                if (!Browse.this.running) {
                    Browse.r_adapters.set(RedditAPI.currentBucket, new ThreadListAdapter(Browse.context, Browse.this.threadList.get(RedditAPI.currentBucket)));
                    Browse.listFragments.get(RedditAPI.currentBucket).setListAdapter(Browse.r_adapters.get(RedditAPI.currentBucket));
                    Browse.this.threadLoader = new ThreadLoader();
                    Browse.this.threadLoader.execute(new Void[0]);
                }
                try {
                    Browse.listFragments.get(RedditAPI.currentBucket).getListView().setOnScrollListener(Browse.onScrollListeners.get(RedditAPI.currentBucket));
                } catch (Exception e) {
                }
            }
            try {
                if (!Browse.this.backed) {
                    Browse.listFragments.get(RedditAPI.currentBucket).getListView().setOnScrollListener(Browse.onScrollListeners.get(RedditAPI.currentBucket));
                }
            } catch (Exception e2) {
            }
            Browse.this.stopLoading();
        }
    }

    public void startLoading() {
        this.thread_count++;
        findViewById(R.id.refresh).setVisibility(8);
        findViewById(R.id.progressBar1).setVisibility(0);
    }

    public void stopLoading() {
        this.thread_count--;
        if (this.thread_count == 0) {
            findViewById(R.id.progressBar1).setVisibility(8);
            findViewById(R.id.refresh).setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        this.onRestart = true;
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("currentBucket", RedditAPI.currentBucket);
        savedInstanceState.putInt("list_count", listFragments.get(RedditAPI.currentBucket).getListAdapter().getCount());
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.saved = true;
        this.savedBucket = savedInstanceState.getInt("currentBucket");
        this.savedCount = savedInstanceState.getInt("list_count");
    }
}
