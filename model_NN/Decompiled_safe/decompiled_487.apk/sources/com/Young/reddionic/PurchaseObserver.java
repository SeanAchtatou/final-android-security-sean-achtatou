package com.Young.reddionic;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Handler;
import android.util.Log;
import com.Young.reddionic.BillingService;
import com.Young.reddionic.Consts;
import java.lang.reflect.Method;

public abstract class PurchaseObserver {
    private static final Class[] START_INTENT_SENDER_SIG = {IntentSender.class, Intent.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
    private static final String TAG = "PurchaseObserver";
    private final Activity mActivity;
    private final Handler mHandler;
    private Method mStartIntentSender;
    private Object[] mStartIntentSenderArgs = new Object[5];

    public abstract void onBillingSupported(boolean z);

    public abstract void onPurchaseStateChange(Consts.PurchaseState purchaseState, String str, int i, long j, String str2);

    public abstract void onRequestPurchaseResponse(BillingService.RequestPurchase requestPurchase, Consts.ResponseCode responseCode);

    public abstract void onRestoreTransactionsResponse(BillingService.RestoreTransactions restoreTransactions, Consts.ResponseCode responseCode);

    public PurchaseObserver(Activity activity, Handler handler) {
        this.mActivity = activity;
        this.mHandler = handler;
        initCompatibilityLayer();
    }

    private void initCompatibilityLayer() {
        try {
            this.mStartIntentSender = this.mActivity.getClass().getMethod("startIntentSender", START_INTENT_SENDER_SIG);
        } catch (SecurityException e) {
            this.mStartIntentSender = null;
        } catch (NoSuchMethodException e2) {
            this.mStartIntentSender = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void startBuyPageActivity(PendingIntent pendingIntent, Intent intent) {
        if (this.mStartIntentSender != null) {
            try {
                this.mStartIntentSenderArgs[0] = pendingIntent.getIntentSender();
                this.mStartIntentSenderArgs[1] = intent;
                this.mStartIntentSenderArgs[2] = 0;
                this.mStartIntentSenderArgs[3] = 0;
                this.mStartIntentSenderArgs[4] = 0;
                this.mStartIntentSender.invoke(this.mActivity, this.mStartIntentSenderArgs);
            } catch (Exception e) {
                Log.e(TAG, "error starting activity", e);
            }
        } else {
            try {
                pendingIntent.send(this.mActivity, 0, intent);
            } catch (PendingIntent.CanceledException e2) {
                Log.e(TAG, "error starting activity", e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void postPurchaseStateChange(Consts.PurchaseState purchaseState, String itemId, int quantity, long purchaseTime, String developerPayload) {
        final Consts.PurchaseState purchaseState2 = purchaseState;
        final String str = itemId;
        final int i = quantity;
        final long j = purchaseTime;
        final String str2 = developerPayload;
        this.mHandler.post(new Runnable() {
            public void run() {
                PurchaseObserver.this.onPurchaseStateChange(purchaseState2, str, i, j, str2);
            }
        });
    }
}
