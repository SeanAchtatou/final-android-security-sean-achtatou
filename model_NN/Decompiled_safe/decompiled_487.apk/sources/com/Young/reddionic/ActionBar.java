package com.Young.reddionic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class ActionBar extends Fragment {
    int thread_count = 0;
    View view;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate((int) R.layout.action_bar, container, false);
        return this.view;
    }

    public void startLoading() {
        this.thread_count++;
        Log.e("View Hidden?", new StringBuilder(String.valueOf(this.view.findViewById(R.id.Button02).getVisibility())).toString());
        getView().findViewById(R.id.Button02).setVisibility(8);
        getView().setVisibility(8);
        Log.e("View Hidden?", String.valueOf(this.view.findViewById(R.id.Button02).getVisibility()) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + 8);
        Log.e("View Hidden?", String.valueOf(getView().findViewById(R.id.Button02).getVisibility()) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + 8);
        getView().refreshDrawableState();
    }

    public void stopLoading() {
    }
}
