package com.Young.reddionic;

public class Listing {
    private ListingData data;
    private String kind;

    public Listing() {
    }

    public Listing(String stuff) {
        this.kind = null;
        this.data = null;
    }

    public void setKind(String kind2) {
        this.kind = kind2;
    }

    public String getKind() {
        return this.kind;
    }

    public void setData(ListingData data2) {
        this.data = data2;
    }

    public ListingData getData() {
        return this.data;
    }
}
