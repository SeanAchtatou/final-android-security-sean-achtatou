package com.Young.reddionic;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ProgressInputStream extends FilterInputStream {
    private final long maxNumBytes;
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private volatile long totalNumBytesRead;

    public ProgressInputStream(InputStream in, long maxNumBytes2) {
        super(in);
        this.maxNumBytes = maxNumBytes2;
    }

    public long getMaxNumBytes() {
        return this.maxNumBytes;
    }

    public long getTotalNumBytesRead() {
        return this.totalNumBytesRead;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.propertyChangeSupport.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.propertyChangeSupport.removePropertyChangeListener(l);
    }

    public int read() throws IOException {
        return (int) updateProgress((long) super.read());
    }

    public int read(byte[] b) throws IOException {
        return (int) updateProgress((long) super.read(b));
    }

    public int read(byte[] b, int off, int len) throws IOException {
        return (int) updateProgress((long) super.read(b, off, len));
    }

    public long skip(long n) throws IOException {
        return updateProgress(super.skip(n));
    }

    public void mark(int readlimit) {
        throw new UnsupportedOperationException();
    }

    public void reset() throws IOException {
        throw new UnsupportedOperationException();
    }

    public boolean markSupported() {
        return false;
    }

    private long updateProgress(long numBytesRead) {
        if (numBytesRead > 0) {
            long oldTotalNumBytesRead = this.totalNumBytesRead;
            this.totalNumBytesRead += numBytesRead;
            this.propertyChangeSupport.firePropertyChange("totalNumBytesRead", Long.valueOf(oldTotalNumBytesRead), Long.valueOf(this.totalNumBytesRead));
        }
        return numBytesRead;
    }
}
