package com.Young.reddionic;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Thread;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Stack;
import org.apache.http.client.methods.HttpGet;

public class ImageLoader {
    /* access modifiers changed from: private */
    public HashMap<String, Bitmap> cache = new HashMap<>();
    private File cacheDir;
    PhotosLoader photoLoaderThread = new PhotosLoader();
    PhotosQueue photosQueue = new PhotosQueue();
    final int stub_id = R.drawable.stub;

    public ImageLoader(Context context) {
        this.photoLoaderThread.setPriority(4);
        if (Environment.getExternalStorageState().equals("mounted")) {
            this.cacheDir = new File(Environment.getExternalStorageDirectory(), "LazyList");
        } else {
            this.cacheDir = context.getCacheDir();
        }
        if (!this.cacheDir.exists()) {
            this.cacheDir.mkdirs();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public void DisplayImage(String url, Context context, Activity activity, ImageView imageView, int type) {
        if (this.cache.containsKey(url)) {
            imageView.setImageBitmap(this.cache.get(url));
            return;
        }
        queuePhoto(url, activity, imageView, type);
        imageView.setImageResource(R.drawable.stub);
    }

    private void queuePhoto(String url, Activity activity, ImageView imageView, int type) {
        this.photosQueue.Clean(imageView);
        PhotoToLoad p = new PhotoToLoad(url, imageView, activity, type);
        synchronized (this.photosQueue.photosToLoad) {
            this.photosQueue.photosToLoad.push(p);
            this.photosQueue.photosToLoad.notifyAll();
        }
        if (this.photoLoaderThread.getState() == Thread.State.NEW) {
            this.photoLoaderThread.start();
        }
    }

    private static InputStream fetch(String urlString) throws MalformedURLException, IOException {
        return RedditAPI.mClient.execute(new HttpGet(urlString)).getEntity().getContent();
    }

    /* access modifiers changed from: private */
    public Bitmap getBitmap(String url, int type) {
        File f = new File(this.cacheDir, String.valueOf(url.hashCode()));
        InputStream is = null;
        BufferedInputStream bis = null;
        Bitmap b = decodeFile(f, type);
        if (b != null) {
            return b;
        }
        try {
            is = fetch(url);
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            Bitmap bitmap = decodeFile(f, type);
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    Log.w("dd", "Error closing stream.");
                }
            }
            if (bis != null) {
                bis.close();
            }
            return bitmap;
        } catch (MalformedURLException e2) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e3) {
                    Log.w("dd", "Error closing stream.");
                    return null;
                }
            }
            if (bis != null) {
                bis.close();
            }
        } catch (IOException e4) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e5) {
                    Log.w("dd", "Error closing stream.");
                    return null;
                }
            }
            if (bis != null) {
                bis.close();
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e6) {
                    Log.w("dd", "Error closing stream.");
                    throw th;
                }
            }
            if (bis != null) {
                bis.close();
            }
            throw th;
        }
    }

    private Bitmap decodeFile(File f, int type) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            int width_tmp = o.outWidth;
            int height_tmp = o.outHeight;
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            if (type != 0) {
                if (type == 1) {
                    int scale = 1;
                    while (width_tmp / 2 >= 300) {
                        width_tmp /= 2;
                        height_tmp /= 2;
                        scale *= 2;
                    }
                    o2.inSampleSize = scale;
                    return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
                }
                return null;
            } else if (width_tmp == 90 || height_tmp == 90) {
                return Bitmap.createScaledBitmap(BitmapFactory.decodeStream(new FileInputStream(f), null, o2), 70, 58, false);
            } else {
                int scale2 = 1;
                while (width_tmp / 2 >= 40 && height_tmp / 2 >= 40) {
                    width_tmp /= 2;
                    height_tmp /= 2;
                    scale2 *= 2;
                }
                o2.inSampleSize = scale2;
                return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            }
        } catch (FileNotFoundException e) {
        }
    }

    private class PhotoToLoad {
        public Activity activity;
        public int i_type;
        public ImageView imageView;
        public String url;

        public PhotoToLoad(String u, ImageView i, Activity a, int type) {
            this.url = u;
            this.imageView = i;
            this.activity = a;
            this.i_type = type;
        }
    }

    public void stopThread() {
        this.photoLoaderThread.interrupt();
    }

    class PhotosQueue {
        /* access modifiers changed from: private */
        public Stack<PhotoToLoad> photosToLoad = new Stack<>();

        PhotosQueue() {
        }

        public void Clean(ImageView image) {
            int j = 0;
            while (j < this.photosToLoad.size()) {
                if (this.photosToLoad.get(j).imageView == image) {
                    this.photosToLoad.remove(j);
                } else {
                    j++;
                }
            }
        }
    }

    class PhotosLoader extends Thread {
        PhotosLoader() {
        }

        public void run() {
            PhotoToLoad photoToLoad;
            do {
                try {
                    if (ImageLoader.this.photosQueue.photosToLoad.size() == 0) {
                        synchronized (ImageLoader.this.photosQueue.photosToLoad) {
                            ImageLoader.this.photosQueue.photosToLoad.wait();
                        }
                    }
                    if (ImageLoader.this.photosQueue.photosToLoad.size() != 0) {
                        synchronized (ImageLoader.this.photosQueue.photosToLoad) {
                            photoToLoad = (PhotoToLoad) ImageLoader.this.photosQueue.photosToLoad.pop();
                        }
                        if (photoToLoad.imageView.getTag() != null) {
                            int type = photoToLoad.i_type;
                            Bitmap bmp = ImageLoader.this.getBitmap(photoToLoad.url, type);
                            ImageLoader.this.cache.put(photoToLoad.url, bmp);
                            Object tag = null;
                            if (type == 0) {
                                tag = ((ThingInfo) photoToLoad.imageView.getTag()).getThumbnail();
                            } else if (type == 1) {
                                tag = ((ThingInfo) photoToLoad.imageView.getTag()).getUrl();
                            }
                            if (!(tag == null || photoToLoad.imageView == null || !((String) tag).equals(photoToLoad.url))) {
                                photoToLoad.activity.runOnUiThread(new BitmapDisplayer(bmp, photoToLoad.imageView, type));
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    return;
                }
            } while (!Thread.interrupted());
        }
    }

    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        int i_type;
        ImageView imageView;

        public BitmapDisplayer(Bitmap b, ImageView i, int t) {
            this.bitmap = b;
            this.imageView = i;
            this.i_type = t;
        }

        public void run() {
            if (this.bitmap != null) {
                this.imageView.setImageBitmap(this.bitmap);
            } else {
                this.imageView.setImageResource(R.drawable.stub);
            }
        }
    }

    public boolean isEmpty() {
        return this.photosQueue.photosToLoad.size() == 0;
    }

    public void stop() {
        this.photosQueue.photosToLoad.clear();
    }

    public void clearCache() {
        this.cache.clear();
        for (File f : this.cacheDir.listFiles()) {
            f.delete();
        }
    }
}
