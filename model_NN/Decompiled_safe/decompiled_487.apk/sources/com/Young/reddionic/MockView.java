package com.Young.reddionic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.ArrayList;

public class MockView extends FragmentActivity {
    static Activity act;
    static int liked = 0;
    static boolean refreshing = false;
    static int score = -1;
    ActionBar actionbar;
    ArrayList<String> checked = new ArrayList<>();
    Context context;
    DisplayMetrics dm;
    boolean feedbacking = false;
    boolean hidden = false;
    ArrayAdapter<String> includedArrayAdapter;
    boolean loggingin = false;
    MyAdapter mAdapter;
    ViewPager mPager;
    private final RedditSettings mSettings = new RedditSettings();
    ArrayList<String> names = new ArrayList<>();
    int pos = 0;
    PopupWindow pw;
    boolean saved = false;
    View theActionBar;
    int thread_count = 0;
    GoogleAnalyticsTracker tracker;
    WebView webView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Webview Activity", "onCreate");
        requestWindowFeature(1);
        setContentView((int) R.layout.mockview);
        this.dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        this.context = getApplicationContext();
        act = this;
        CookieSyncManager.createInstance(getApplicationContext());
        this.mSettings.loadRedditPreferences(getApplicationContext());
        this.mAdapter = new MyAdapter(getSupportFragmentManager());
        this.mPager = (ViewPager) findViewById(R.id.mockview);
        this.mPager.setAdapter(this.mAdapter);
        this.names.add("Reddit ID");
        this.names.add("# of Karma");
        this.names.add("Device ID");
        this.names.add("Device Name");
        this.names.add("Device SDK");
        this.names.add("Device Name");
        this.names.add("Current View");
        this.names.add("Version Number");
        this.names.add("Date of the first run");
    }

    class FeedbackAdapter extends ArrayAdapter<String> {
        FeedbackAdapter() {
            super(MockView.this.context, (int) R.layout.included_list_item, MockView.this.names);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = MockView.this.getLayoutInflater().inflate((int) R.layout.included_list_item, parent, false);
            }
            TextView label = (TextView) row.findViewById(R.id.label);
            label.setText(MockView.this.names.get(position));
            label.setTag(Integer.valueOf(position));
            label.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.e("subredditClick", new StringBuilder().append((Integer) v.getTag()).toString());
                    RedditAPI.currentBucket = ((Integer) v.getTag()).intValue();
                    MockView.this.mPager.setCurrentItem(((Integer) v.getTag()).intValue());
                }
            });
            return row;
        }
    }

    public void startLoading() {
        this.thread_count++;
        findViewById(R.id.refresh).setVisibility(8);
        findViewById(R.id.progressBar1).setVisibility(0);
    }

    public void stopLoading() {
        this.thread_count--;
        if (this.thread_count == 0) {
            findViewById(R.id.progressBar1).setVisibility(8);
            findViewById(R.id.refresh).setVisibility(0);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.feedback /*2131034193*/:
                View popup = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.r_feedback_popup, (ViewGroup) null, false);
                this.pw = new PopupWindow(popup, -1, -1, true);
                this.pw.setBackgroundDrawable(new BitmapDrawable());
                this.pw.showAtLocation(findViewById(R.id.mockview_root), 0, 0, 0);
                ((LinearLayout) popup.findViewById(R.id.requestButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((LinearLayout) temp.findViewById(R.id.request_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView1)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Request");
                        ((Button) ((LinearLayout) temp.findViewById(R.id.request_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                        Log.e("dd", new StringBuilder().append(temp.findViewById(R.id.listView1).getVisibility()).toString());
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.improvementButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((LinearLayout) temp.findViewById(R.id.improvement_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView2)).setAdapter((ListAdapter) new FeedbackAdapter());
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.bugButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((LinearLayout) temp.findViewById(R.id.bug_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView3)).setAdapter((ListAdapter) new FeedbackAdapter());
                    }
                });
                this.feedbacking = true;
                return true;
            case R.id.donate /*2131034194*/:
                startActivity(new Intent(this, DonateActivity.class));
                return true;
            default:
                return false;
        }
    }

    public void submitClick(View button) {
        ArrayList<Object> arguments = (ArrayList) button.getTag();
        arguments.add(((EditText) ((LinearLayout) button.getParent()).findViewById(R.id.text)).getText().toString());
        if (this.checked.get(0).equals("checked")) {
            arguments.add(this.mSettings.firstRunDate);
        } else {
            arguments.add("");
        }
        try {
            if (this.checked.get(1).equals("checked")) {
                arguments.add(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
            } else {
                arguments.add("");
            }
        } catch (PackageManager.NameNotFoundException e) {
            arguments.add("");
            e.printStackTrace();
        }
        if (this.checked.get(2).equals("checked")) {
            arguments.add(this.mSettings.username);
        } else {
            arguments.add("");
        }
        if (this.checked.get(3).equals("checked")) {
            View t = findViewById(R.id.webview_root);
            t.setDrawingCacheEnabled(true);
            t.destroyDrawingCache();
            arguments.add(t.getDrawingCache());
        } else {
            arguments.add("");
        }
        if (this.checked.get(4).equals("checked")) {
            arguments.add(Build.PRODUCT);
        } else {
            arguments.add("");
        }
        if (this.checked.get(5).equals("checked")) {
            arguments.add(Build.VERSION.RELEASE);
        } else {
            arguments.add("");
        }
        new Feedback().execute(arguments);
        this.pw.dismiss();
    }

    public void onBackPressed() {
        Log.e("HERE", "HERE! " + this.feedbacking);
        if (this.feedbacking) {
            Log.e("HERE", "HERE!");
            this.pw.dismiss();
            this.feedbacking = false;
            return;
        }
        super.onBackPressed();
    }

    class Feedback extends AsyncTask<ArrayList<Object>, Void, Void> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<Object>[]) ((ArrayList[]) objArr));
        }

        Feedback() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(ArrayList<Object>... passing) {
            RedditAPI.feedback(passing[0]);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MockView.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute() {
            MockView.this.stopLoading();
            Log.e("AsyncPost", "Finished");
        }
    }

    public static class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        public int getCount() {
            return 5;
        }

        public Fragment getItem(int position) {
            return MockFragment.newInstance(position);
        }
    }

    public static class MockFragment extends Fragment {
        int mNum;

        static MockFragment newInstance(int num) {
            MockFragment f = new MockFragment();
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);
            return f;
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate((int) R.layout.mockview_bar, container, false);
            v.setTag(Integer.valueOf(getArguments().getInt("num")));
            getArguments().getInt("num");
            return v;
        }

        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }
    }
}
