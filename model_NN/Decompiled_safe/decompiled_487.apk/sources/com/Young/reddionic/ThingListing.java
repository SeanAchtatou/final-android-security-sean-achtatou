package com.Young.reddionic;

public class ThingListing {
    private ThingInfo data;
    private String kind;

    public void setKind(String kind2) {
        this.kind = kind2;
    }

    public String getKind() {
        return this.kind;
    }

    public void setData(ThingInfo data2) {
        this.data = data2;
    }

    public ThingInfo getData() {
        return this.data;
    }
}
