package com.Young.reddionic;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.util.Log;
import android.webkit.CookieSyncManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.ObjectMapper;

public class RedditAPI {
    private static final Pattern MY_SUBREDDITS_INNER = Pattern.compile("<a(.*?)/r/(.*?)>(.+?)</a>");
    private static final Pattern MY_SUBREDDITS_OUTER = Pattern.compile("your front page reddits.*?<ul>(.*?)</ul>", 2);
    static final String TAG = "Reddit";
    protected static int bucketSize = 50;
    protected static ArrayList<ThingInfo> commentInfos = new ArrayList<>();
    protected static int currentBucket = 0;
    protected static ArrayList<String> mAfterBucket = new ArrayList<>();
    protected static ArrayList<String> mBeforeBucket = new ArrayList<>();
    static DefaultHttpClient mClient = createGzipHttpClient();
    protected static long mContentLength = 0;
    protected static Context mContext;
    protected static int mCount;
    protected static ArrayList<String> mLastAfterBucket = new ArrayList<>();
    protected static ArrayList<String> mLastBeforeBucket = new ArrayList<>();
    protected static int mLastCount = 0;
    protected static ArrayList<Boolean> mLastPages = new ArrayList<>();
    protected static String mModhash = null;
    static ObjectMapper mOm = new ObjectMapper();
    protected static RedditSettings mSettings = new RedditSettings();
    protected static String mSortByUrl = "";
    protected static String mSortByUrlExtra = "";
    protected static String mSubreddit;
    protected static ArrayList<ThingInfo> mThingInfos2 = new ArrayList<>();
    protected static ArrayList<ArrayList<ThingInfo>> mThingInfoss = new ArrayList<>();
    protected static String mUserError = "Error retrieving subreddit info.";
    protected static String mUsername = null;
    protected static String mUsername2 = null;
    protected static ArrayList<String> newreddits = new ArrayList<>();
    protected static ArrayList<String> oldreddits = new ArrayList<>();
    protected static ArrayList<String> redditBuckets = new ArrayList<>();
    protected static ArrayList<String> reddits = new ArrayList<>();
    static boolean scrollingHorizontally = false;
    static int subRedCounter = 0;
    String cookieDomain = ".reddit.com";
    String server = "http://www.reddit.com";

    static DefaultHttpClient getClient() {
        return mClient;
    }

    static void fetchAnnouncement() {
        HttpGet request = new HttpGet("https://sites.google.com/site/reddionicsite/schedule/schedule");
        HttpParams params = request.getParams();
        try {
            HttpResponse execute = mClient.execute(request);
            Log.d("dd", "new subreddit list size: " + reddits.size());
        } catch (HttpHostConnectException e) {
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    static boolean feedback(ArrayList<Object> passing) {
        HttpEntity entity = null;
        try {
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("entry.0.single", (String) passing.get(0)));
            nvps.add(new BasicNameValuePair("entry.2.single", (String) passing.get(1)));
            nvps.add(new BasicNameValuePair("entry.8.single", (String) passing.get(2)));
            nvps.add(new BasicNameValuePair("entry.6.single", (String) passing.get(3)));
            nvps.add(new BasicNameValuePair("entry.14.single", (String) passing.get(4)));
            nvps.add(new BasicNameValuePair("entry.4.single", upload((Bitmap) passing.get(5))));
            nvps.add(new BasicNameValuePair("entry.12.single", (String) passing.get(6)));
            nvps.add(new BasicNameValuePair("entry.15.single", (String) passing.get(7)));
            HttpPost httppost = new HttpPost("https://spreadsheets.google.com/spreadsheet/formResponse?hl=en_US&formkey=dGVxTTZ4NzdUc24wX3VXclc4V1paYkE6MQ");
            httppost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            HttpParams params = httppost.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 45000);
            HttpConnectionParams.setSoTimeout(params, 45000);
            Log.v("Test", "hmm");
            HttpResponse response = mClient.execute(httppost);
            String status = response.getStatusLine().toString();
            return EntityUtils.toString(response.getEntity()).contains("Your response has been recorded");
        } catch (Exception e) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e2) {
                }
            }
            return false;
        }
    }

    static boolean purchase(ArrayList<String> passing) {
        HttpEntity entity = null;
        try {
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("entry.0.single", passing.get(0)));
            nvps.add(new BasicNameValuePair("entry.1.single", passing.get(1)));
            nvps.add(new BasicNameValuePair("entry.5.single", passing.get(2)));
            nvps.add(new BasicNameValuePair("entry.4.single", passing.get(3)));
            HttpPost httppost = new HttpPost("https://spreadsheets.google.com/spreadsheet/formResponse?hl=en_US&formkey=dFJmN2Y5Yy1idUtxVXFfLWFubUt5Y0E6MQ");
            httppost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            HttpParams params = httppost.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 45000);
            HttpConnectionParams.setSoTimeout(params, 45000);
            Log.v("Test", "hmm");
            HttpResponse response = mClient.execute(httppost);
            String status = response.getStatusLine().toString();
            return EntityUtils.toString(response.getEntity()).contains("Your response has been recorded");
        } catch (Exception e) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e2) {
                }
            }
            return false;
        }
    }

    /* JADX INFO: Multiple debug info for r8v13 org.apache.http.HttpEntity: [D('entity' org.apache.http.HttpEntity), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r9v23 org.codehaus.jackson.JsonParser: [D('jsonFactory' org.codehaus.jackson.JsonFactory), D('jp' org.codehaus.jackson.JsonParser)] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0087 A[SYNTHETIC, Splitter:B:12:0x0087] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean Readability(java.lang.String r8, java.lang.String r9, com.Young.reddionic.RedditSettings r10, android.content.Context r11) {
        /*
            java.lang.String r2 = ""
            java.lang.String r3 = "Error logging in. Please try again."
            r0 = 0
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x0143 }
            r1.<init>()     // Catch:{ Exception -> 0x0143 }
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0143 }
            java.lang.String r5 = "url"
            java.lang.String r6 = "http://news.yahoo.com/s/ap/us_huckabee_obama"
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x0143 }
            r1.add(r4)     // Catch:{ Exception -> 0x0143 }
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0143 }
            java.lang.String r5 = "passwd"
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0143 }
            r4.<init>(r5, r9)     // Catch:{ Exception -> 0x0143 }
            r1.add(r4)     // Catch:{ Exception -> 0x0143 }
            org.apache.http.message.BasicNameValuePair r9 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0143 }
            java.lang.String r4 = "api_type"
            java.lang.String r5 = "json"
            r9.<init>(r4, r5)     // Catch:{ Exception -> 0x0143 }
            r1.add(r9)     // Catch:{ Exception -> 0x0143 }
            org.apache.http.client.methods.HttpPost r9 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x0143 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0143 }
            java.lang.String r5 = "http://www.reddit.com/api/login/"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0143 }
            java.lang.StringBuilder r8 = r4.append(r8)     // Catch:{ Exception -> 0x0143 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0143 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x0143 }
            org.apache.http.client.entity.UrlEncodedFormEntity r8 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x0143 }
            java.lang.String r4 = "UTF-8"
            r8.<init>(r1, r4)     // Catch:{ Exception -> 0x0143 }
            r9.setEntity(r8)     // Catch:{ Exception -> 0x0143 }
            org.apache.http.params.HttpParams r8 = r9.getParams()     // Catch:{ Exception -> 0x0143 }
            r1 = 45000(0xafc8, float:6.3058E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r8, r1)     // Catch:{ Exception -> 0x0143 }
            r1 = 45000(0xafc8, float:6.3058E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r8, r1)     // Catch:{ Exception -> 0x0143 }
            java.lang.String r8 = "Test"
            java.lang.String r1 = "hmm"
            android.util.Log.v(r8, r1)     // Catch:{ Exception -> 0x0143 }
            org.apache.http.impl.client.DefaultHttpClient r8 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x0143 }
            org.apache.http.HttpResponse r8 = r8.execute(r9)     // Catch:{ Exception -> 0x0143 }
            org.apache.http.StatusLine r9 = r8.getStatusLine()     // Catch:{ Exception -> 0x0143 }
            java.lang.String r1 = r9.toString()     // Catch:{ Exception -> 0x0143 }
            java.lang.String r9 = "OK"
            boolean r9 = r1.contains(r9)     // Catch:{ Exception -> 0x0081 }
            if (r9 != 0) goto L_0x0091
            org.apache.http.HttpException r8 = new org.apache.http.HttpException     // Catch:{ Exception -> 0x0081 }
            r8.<init>(r1)     // Catch:{ Exception -> 0x0081 }
            throw r8     // Catch:{ Exception -> 0x0081 }
        L_0x0081:
            r8 = move-exception
            r9 = r0
            r11 = r3
            r10 = r1
        L_0x0085:
            if (r9 == 0) goto L_0x008a
            r9.consumeContent()     // Catch:{ Exception -> 0x0140 }
        L_0x008a:
            r8 = 0
            r7 = r9
            r9 = r10
            r10 = r11
            r11 = r8
            r8 = r7
        L_0x0090:
            return r11
        L_0x0091:
            org.apache.http.HttpEntity r8 = r8.getEntity()     // Catch:{ Exception -> 0x0081 }
            java.io.BufferedReader r9 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00bf }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00bf }
            java.io.InputStream r2 = r8.getContent()     // Catch:{ Exception -> 0x00bf }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00bf }
            r9.<init>(r0)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r0 = r9.readLine()     // Catch:{ Exception -> 0x00bf }
            r9.close()     // Catch:{ Exception -> 0x00bf }
            r8.consumeContent()     // Catch:{ Exception -> 0x00bf }
            if (r0 == 0) goto L_0x00b7
            java.lang.String r9 = ""
            boolean r9 = r9.equals(r0)     // Catch:{ Exception -> 0x00bf }
            if (r9 == 0) goto L_0x00c6
        L_0x00b7:
            org.apache.http.HttpException r9 = new org.apache.http.HttpException     // Catch:{ Exception -> 0x00bf }
            java.lang.String r10 = "No content returned from login POST"
            r9.<init>(r10)     // Catch:{ Exception -> 0x00bf }
            throw r9     // Catch:{ Exception -> 0x00bf }
        L_0x00bf:
            r9 = move-exception
            r11 = r3
            r10 = r1
            r7 = r9
            r9 = r8
            r8 = r7
            goto L_0x0085
        L_0x00c6:
            java.lang.String r9 = "Reddit"
            logDLong(r9, r0)     // Catch:{ Exception -> 0x00bf }
            org.apache.http.impl.client.DefaultHttpClient r9 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x00bf }
            org.apache.http.client.CookieStore r9 = r9.getCookieStore()     // Catch:{ Exception -> 0x00bf }
            java.util.List r9 = r9.getCookies()     // Catch:{ Exception -> 0x00bf }
            boolean r9 = r9.isEmpty()     // Catch:{ Exception -> 0x00bf }
            if (r9 == 0) goto L_0x00e3
            org.apache.http.HttpException r9 = new org.apache.http.HttpException     // Catch:{ Exception -> 0x00bf }
            java.lang.String r10 = "Failed to login: No cookies"
            r9.<init>(r10)     // Catch:{ Exception -> 0x00bf }
            throw r9     // Catch:{ Exception -> 0x00bf }
        L_0x00e3:
            org.codehaus.jackson.JsonFactory r9 = new org.codehaus.jackson.JsonFactory     // Catch:{ Exception -> 0x00bf }
            r9.<init>()     // Catch:{ Exception -> 0x00bf }
            org.codehaus.jackson.JsonParser r9 = r9.createJsonParser(r0)     // Catch:{ Exception -> 0x00bf }
        L_0x00ec:
            org.codehaus.jackson.JsonToken r2 = r9.nextToken()     // Catch:{ Exception -> 0x00bf }
            org.codehaus.jackson.JsonToken r4 = org.codehaus.jackson.JsonToken.FIELD_NAME     // Catch:{ Exception -> 0x00bf }
            if (r2 != r4) goto L_0x00ec
            java.lang.String r2 = "errors"
            java.lang.String r4 = r9.getCurrentName()     // Catch:{ Exception -> 0x00bf }
            boolean r2 = r2.equals(r4)     // Catch:{ Exception -> 0x00bf }
            if (r2 == 0) goto L_0x00ec
            org.codehaus.jackson.JsonToken r2 = r9.nextToken()     // Catch:{ Exception -> 0x00bf }
            org.codehaus.jackson.JsonToken r4 = org.codehaus.jackson.JsonToken.START_ARRAY     // Catch:{ Exception -> 0x00bf }
            if (r2 == r4) goto L_0x0110
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException     // Catch:{ Exception -> 0x00bf }
            java.lang.String r10 = "Login: expecting errors START_ARRAY"
            r9.<init>(r10)     // Catch:{ Exception -> 0x00bf }
            throw r9     // Catch:{ Exception -> 0x00bf }
        L_0x0110:
            org.codehaus.jackson.JsonToken r9 = r9.nextToken()     // Catch:{ Exception -> 0x00bf }
            org.codehaus.jackson.JsonToken r2 = org.codehaus.jackson.JsonToken.END_ARRAY     // Catch:{ Exception -> 0x00bf }
            if (r9 == r2) goto L_0x0137
            java.lang.String r9 = "WRONG_PASSWORD"
            boolean r9 = r0.contains(r9)     // Catch:{ Exception -> 0x00bf }
            if (r9 == 0) goto L_0x0131
            java.lang.String r9 = "Bad password."
            java.lang.Exception r10 = new java.lang.Exception     // Catch:{ Exception -> 0x012a }
            java.lang.String r11 = "Wrong password"
            r10.<init>(r11)     // Catch:{ Exception -> 0x012a }
            throw r10     // Catch:{ Exception -> 0x012a }
        L_0x012a:
            r10 = move-exception
            r11 = r9
            r9 = r8
            r8 = r10
            r10 = r1
            goto L_0x0085
        L_0x0131:
            java.lang.Exception r9 = new java.lang.Exception     // Catch:{ Exception -> 0x00bf }
            r9.<init>(r0)     // Catch:{ Exception -> 0x00bf }
            throw r9     // Catch:{ Exception -> 0x00bf }
        L_0x0137:
            r10.saveRedditPreferences(r11)     // Catch:{ Exception -> 0x00bf }
            r9 = 1
            r10 = r3
            r11 = r9
            r9 = r1
            goto L_0x0090
        L_0x0140:
            r8 = move-exception
            goto L_0x008a
        L_0x0143:
            r8 = move-exception
            r9 = r0
            r11 = r3
            r10 = r2
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.RedditAPI.Readability(java.lang.String, java.lang.String, com.Young.reddionic.RedditSettings, android.content.Context):boolean");
    }

    /* JADX INFO: Multiple debug info for r1v1 org.apache.http.params.HttpParams: [D('nvps' java.util.List<org.apache.http.NameValuePair>), D('params' org.apache.http.params.HttpParams)] */
    /* JADX INFO: Multiple debug info for r8v6 org.apache.http.HttpResponse: [D('httppost' org.apache.http.client.methods.HttpPost), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r8v17 org.codehaus.jackson.JsonParser: [D('jsonFactory' org.codehaus.jackson.JsonFactory), D('jp' org.codehaus.jackson.JsonParser)] */
    static boolean loginUser(String username, String password, RedditSettings settings, Context context) {
        String status = "";
        try {
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("user", username.toString()));
            nvps.add(new BasicNameValuePair("passwd", password.toString()));
            nvps.add(new BasicNameValuePair("api_type", "json"));
            HttpPost httppost = new HttpPost("http://www.reddit.com/api/login/" + username);
            httppost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            HttpParams params = httppost.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 45000);
            HttpConnectionParams.setSoTimeout(params, 45000);
            Log.v("Test", "hmm");
            HttpResponse response = mClient.execute(httppost);
            status = response.getStatusLine().toString();
            if (!status.contains("OK")) {
                throw new HttpException(status);
            }
            HttpEntity entity = response.getEntity();
            BufferedReader in = new BufferedReader(new InputStreamReader(entity.getContent()));
            String line = in.readLine();
            in.close();
            entity.consumeContent();
            if (line == null || "".equals(line)) {
                throw new HttpException("No content returned from login POST");
            }
            logDLong(TAG, line);
            if (mClient.getCookieStore().getCookies().isEmpty()) {
                throw new HttpException("Failed to login: No cookies");
            }
            JsonParser jp = new JsonFactory().createJsonParser(line);
            while (true) {
                if (jp.nextToken() == JsonToken.FIELD_NAME && "errors".equals(jp.getCurrentName())) {
                    break;
                }
            }
            if (jp.nextToken() != JsonToken.START_ARRAY) {
                throw new IllegalStateException("Login: expecting errors START_ARRAY");
            } else if (jp.nextToken() == JsonToken.END_ARRAY) {
                while (true) {
                    if (jp.nextToken() == JsonToken.FIELD_NAME && "modhash".equals(jp.getCurrentName())) {
                        break;
                    }
                }
                jp.nextToken();
                Log.v("ModHash", jp.getText());
                settings.setUsername(username);
                settings.setModhash(jp.getText());
                Iterator it = mClient.getCookieStore().getCookies().iterator();
                while (true) {
                    if (it.hasNext()) {
                        Cookie c = (Cookie) it.next();
                        if (c.getName().equals("reddit_session")) {
                            settings.setRedditSessionCookie(c);
                            break;
                        }
                    } else {
                        break;
                    }
                }
                settings.setUsername(username);
                CookieSyncManager.getInstance().sync();
                CacheInfo.invalidateAllCaches(context);
                settings.saveRedditPreferences(context);
                String str = status;
                HttpEntity httpEntity = entity;
                return true;
            } else if (line.contains("WRONG_PASSWORD")) {
                mUserError = "Wrong Password. Try again.";
                throw new Exception("Wrong password");
            } else if (line.contains("RATELIMIT")) {
                mUserError = "You are trying too much. T" + line.substring(line.lastIndexOf("ry"), line.lastIndexOf("\""));
                throw new Exception(mUserError);
            } else {
                throw new Exception(line);
            }
        } catch (Exception e) {
            HttpEntity entity2 = null;
            if (entity2 != null) {
                try {
                    entity2.consumeContent();
                } catch (Exception e2) {
                }
            }
            return false;
        }
    }

    public static ArrayList<String> about(String username) {
        HttpEntity entity = null;
        ArrayList<String> data = new ArrayList<>();
        try {
            String url = "http://www.reddit.com/user/" + username + "/about.json";
            Log.d("dd", "url=" + url);
            HttpGet request = null;
            try {
                request = new HttpGet(url);
            } catch (IllegalArgumentException e) {
                mUserError = "Invalid User.";
            }
            HttpResponse response = mClient.execute(request);
            Header contentLengthHeader = response.getFirstHeader("Content-Length");
            entity = response.getEntity();
            InputStream in = entity.getContent();
            if (contentLengthHeader != null) {
                mContentLength = Long.valueOf(contentLengthHeader.getValue()).longValue();
                Log.d("dd", "Content length [sent]: " + mContentLength);
            } else {
                mContentLength = -1;
                Log.d("dd", "Content length not available");
            }
            ProgressInputStream pin = new ProgressInputStream(in, mContentLength);
            try {
                AboutData dat = parseAboutJSON(pin);
                data.add(dat.getName());
                data.add(dat.getModhash());
                data.add(dat.getCreated_utc());
                data.add(new StringBuilder(String.valueOf(dat.getLink_karma())).toString());
                data.add(new StringBuilder(String.valueOf(dat.getComment_karma())).toString());
                data.add(new StringBuilder(String.valueOf(dat.getIs_gold())).toString());
                data.add(new StringBuilder(String.valueOf(dat.getIs_mod())).toString());
                pin.close();
                in.close();
            } catch (IllegalStateException e2) {
                mUserError = "Invalid subreddit.";
                pin.close();
                in.close();
            } catch (Exception e3) {
                pin.close();
                in.close();
            } catch (Throwable th) {
                pin.close();
                in.close();
                throw th;
            }
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e4) {
                }
            }
        } catch (Exception e5) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e6) {
                }
            }
        } catch (Throwable th2) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e7) {
                }
            }
            throw th2;
        }
        return data;
    }

    protected static AboutData parseAboutJSON(InputStream in) throws IOException, JsonParseException, IllegalStateException {
        try {
            About about = (About) mOm.readValue(in, About.class);
            if ("t2".equals(about.getKind())) {
                return about.getData();
            }
            throw new IllegalStateException("Not a subreddit listing");
        } catch (Exception e) {
            return null;
        }
    }

    static DefaultHttpClient createGzipHttpClient() {
        BasicHttpParams params = new BasicHttpParams();
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        DefaultHttpClient httpclient = new DefaultHttpClient(new ThreadSafeClientConnManager(params, schemeRegistry), params);
        httpclient.addRequestInterceptor(new HttpRequestInterceptor() {
            public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
                if (!request.containsHeader("Accept-Encoding")) {
                    request.addHeader("Accept-Encoding", "gzip");
                }
            }
        });
        httpclient.addResponseInterceptor(new HttpResponseInterceptor() {
            public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
                Header ceheader = response.getEntity().getContentEncoding();
                if (ceheader != null) {
                    HeaderElement[] codecs = ceheader.getElements();
                    for (HeaderElement name : codecs) {
                        if (name.getName().equalsIgnoreCase("gzip")) {
                            response.setEntity(new GzipDecompressingEntity(response.getEntity()));
                            return;
                        }
                    }
                }
            }
        });
        return httpclient;
    }

    static class GzipDecompressingEntity extends HttpEntityWrapper {
        public GzipDecompressingEntity(HttpEntity entity) {
            super(entity);
        }

        public InputStream getContent() throws IOException, IllegalStateException {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public long getContentLength() {
            return -1;
        }
    }

    static void logDLong(String tag, String msg) {
        boolean done = false;
        StringBuilder sb = new StringBuilder();
        int k = 0;
        while (k < msg.length()) {
            int i = 0;
            while (true) {
                if (i >= 80) {
                    break;
                } else if (k + i >= msg.length()) {
                    done = true;
                    break;
                } else {
                    sb.append((char) msg.charAt(k + i));
                    i++;
                }
            }
            Log.d(tag, "multipart log: " + sb.toString());
            sb = new StringBuilder();
            if (!done) {
                k += 80;
            } else {
                return;
            }
        }
    }

    private static InputStream fetch(String urlString) throws MalformedURLException, IOException {
        return mClient.execute(new HttpGet(urlString)).getEntity().getContent();
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0035 A[SYNTHETIC, Splitter:B:22:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x003a A[Catch:{ IOException -> 0x003e }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x004b A[SYNTHETIC, Splitter:B:30:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0050 A[Catch:{ IOException -> 0x0054 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap readBitmapFromNetwork(java.lang.String r8) {
        /*
            r4 = 0
            r0 = 0
            r2 = 0
            java.io.InputStream r4 = fetch(r8)     // Catch:{ MalformedURLException -> 0x001c, IOException -> 0x0032, all -> 0x0048 }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ MalformedURLException -> 0x001c, IOException -> 0x0032, all -> 0x0048 }
            r1.<init>(r4)     // Catch:{ MalformedURLException -> 0x001c, IOException -> 0x0032, all -> 0x0048 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r4)     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x006c, all -> 0x0069 }
            if (r4 == 0) goto L_0x0015
            r4.close()     // Catch:{ IOException -> 0x005e }
        L_0x0015:
            if (r1 == 0) goto L_0x0067
            r1.close()     // Catch:{ IOException -> 0x005e }
            r0 = r1
        L_0x001b:
            return r2
        L_0x001c:
            r5 = move-exception
        L_0x001d:
            if (r4 == 0) goto L_0x0022
            r4.close()     // Catch:{ IOException -> 0x0028 }
        L_0x0022:
            if (r0 == 0) goto L_0x001b
            r0.close()     // Catch:{ IOException -> 0x0028 }
            goto L_0x001b
        L_0x0028:
            r5 = move-exception
            r3 = r5
            java.lang.String r5 = "Reddit"
            java.lang.String r6 = "Error closing stream."
            android.util.Log.w(r5, r6)
            goto L_0x001b
        L_0x0032:
            r5 = move-exception
        L_0x0033:
            if (r4 == 0) goto L_0x0038
            r4.close()     // Catch:{ IOException -> 0x003e }
        L_0x0038:
            if (r0 == 0) goto L_0x001b
            r0.close()     // Catch:{ IOException -> 0x003e }
            goto L_0x001b
        L_0x003e:
            r5 = move-exception
            r3 = r5
            java.lang.String r5 = "Reddit"
            java.lang.String r6 = "Error closing stream."
            android.util.Log.w(r5, r6)
            goto L_0x001b
        L_0x0048:
            r5 = move-exception
        L_0x0049:
            if (r4 == 0) goto L_0x004e
            r4.close()     // Catch:{ IOException -> 0x0054 }
        L_0x004e:
            if (r0 == 0) goto L_0x0053
            r0.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            throw r5
        L_0x0054:
            r6 = move-exception
            r3 = r6
            java.lang.String r6 = "Reddit"
            java.lang.String r7 = "Error closing stream."
            android.util.Log.w(r6, r7)
            goto L_0x0053
        L_0x005e:
            r5 = move-exception
            r3 = r5
            java.lang.String r5 = "Reddit"
            java.lang.String r6 = "Error closing stream."
            android.util.Log.w(r5, r6)
        L_0x0067:
            r0 = r1
            goto L_0x001b
        L_0x0069:
            r5 = move-exception
            r0 = r1
            goto L_0x0049
        L_0x006c:
            r5 = move-exception
            r0 = r1
            goto L_0x0033
        L_0x006f:
            r5 = move-exception
            r0 = r1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.RedditAPI.readBitmapFromNetwork(java.lang.String):android.graphics.Bitmap");
    }

    /* JADX INFO: Multiple debug info for r1v21 org.apache.http.HttpResponse: [D('params' org.apache.http.params.HttpParams), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r1v22 'entity'  org.apache.http.HttpEntity: [D('entity' org.apache.http.HttpEntity), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r2v13 java.util.regex.Matcher: [D('outer' java.util.regex.Matcher), D('line' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r2v18 java.util.regex.Matcher: [D('inner' java.util.regex.Matcher), D('outer' java.util.regex.Matcher)] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00f1 A[SYNTHETIC, Splitter:B:33:0x00f1] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0122 A[LOOP:2: B:19:0x00be->B:42:0x0122, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void getSubreddit() {
        /*
            int r0 = com.Young.reddionic.RedditAPI.subRedCounter
            if (r0 != 0) goto L_0x00c9
            int r0 = com.Young.reddionic.RedditAPI.subRedCounter
            int r0 = r0 + 1
            com.Young.reddionic.RedditAPI.subRedCounter = r0
            java.util.ArrayList<java.lang.String> r0 = com.Young.reddionic.RedditAPI.oldreddits
            r0.clear()
            com.Young.reddionic.DatabaseHelper r0 = com.Young.reddionic.Browse.dbh
            java.util.ArrayList r0 = r0.getSubReddit()
            com.Young.reddionic.RedditAPI.oldreddits = r0
            java.util.ArrayList<java.lang.String> r0 = com.Young.reddionic.RedditAPI.newreddits
            r0.clear()
            java.util.ArrayList<java.lang.String> r0 = com.Young.reddionic.RedditAPI.reddits
            r0.clear()
            r0 = 0
            java.util.ArrayList<java.lang.String> r1 = com.Young.reddionic.RedditAPI.newreddits
            java.lang.String r2 = "reddit front page"
            r1.add(r2)
            r3 = 0
            org.apache.http.client.methods.HttpGet r2 = new org.apache.http.client.methods.HttpGet
            java.lang.String r1 = "http://www.reddit.com/reddits"
            r2.<init>(r1)
            org.apache.http.params.HttpParams r1 = r2.getParams()
            org.apache.http.impl.client.DefaultHttpClient r1 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ HttpHostConnectException -> 0x0134, ClientProtocolException -> 0x00dd, IOException -> 0x00e7 }
            org.apache.http.HttpResponse r1 = r1.execute(r2)     // Catch:{ HttpHostConnectException -> 0x0134, ClientProtocolException -> 0x00dd, IOException -> 0x00e7 }
            org.apache.http.HttpEntity r1 = r1.getEntity()     // Catch:{ HttpHostConnectException -> 0x0134, ClientProtocolException -> 0x00dd, IOException -> 0x00e7 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.io.InputStream r4 = r1.getContent()     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            r2.<init>(r4)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            r0.<init>(r2)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.lang.String r2 = r0.readLine()     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            r0.close()     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            r1.consumeContent()     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.util.regex.Pattern r0 = com.Young.reddionic.RedditAPI.MY_SUBREDDITS_OUTER     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.util.regex.Matcher r2 = r0.matcher(r2)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            r0 = 0
            boolean r4 = r2.find()     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            if (r4 == 0) goto L_0x0075
            java.util.regex.Pattern r4 = com.Young.reddionic.RedditAPI.MY_SUBREDDITS_INNER     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            r5 = 1
            java.lang.String r2 = r2.group(r5)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.util.regex.Matcher r2 = r4.matcher(r2)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
        L_0x006f:
            boolean r4 = r2.find()     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            if (r4 != 0) goto L_0x00ca
        L_0x0075:
            java.lang.String r0 = "dd"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.lang.String r4 = "new subreddit list size: "
            r2.<init>(r4)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.util.ArrayList<java.lang.String> r4 = com.Young.reddionic.RedditAPI.reddits     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            int r4 = r4.size()     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.lang.String r2 = r2.toString()     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            android.util.Log.d(r0, r2)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            r0 = r1
            r1 = r3
        L_0x0091:
            java.util.ArrayList<java.lang.String> r0 = com.Young.reddionic.RedditAPI.oldreddits     // Catch:{ ConcurrentModificationException -> 0x010c }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ ConcurrentModificationException -> 0x010c }
        L_0x0097:
            boolean r0 = r1.hasNext()     // Catch:{ ConcurrentModificationException -> 0x010c }
            if (r0 != 0) goto L_0x00f1
            java.util.ArrayList<java.lang.String> r0 = com.Young.reddionic.RedditAPI.reddits     // Catch:{ ConcurrentModificationException -> 0x010c }
            java.util.ArrayList<java.lang.String> r1 = com.Young.reddionic.RedditAPI.newreddits     // Catch:{ ConcurrentModificationException -> 0x010c }
            r0.addAll(r1)     // Catch:{ ConcurrentModificationException -> 0x010c }
        L_0x00a4:
            int r0 = com.Young.reddionic.RedditAPI.subRedCounter
            r1 = 1
            int r0 = r0 - r1
            com.Young.reddionic.RedditAPI.subRedCounter = r0
            com.Young.reddionic.DatabaseHelper r0 = com.Young.reddionic.Browse.dbh
            r0.dropSubReddit()
            com.Young.reddionic.DatabaseHelper r0 = new com.Young.reddionic.DatabaseHelper
            android.content.Context r1 = com.Young.reddionic.Browse.context
            r0.<init>(r1)
            com.Young.reddionic.Browse.dbh = r0
            java.util.ArrayList<java.lang.String> r0 = com.Young.reddionic.RedditAPI.reddits
            java.util.Iterator r1 = r0.iterator()
        L_0x00be:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0122
            com.Young.reddionic.DatabaseHelper r0 = com.Young.reddionic.Browse.dbh
            r0.close()
        L_0x00c9:
            return
        L_0x00ca:
            int r0 = r0 + 1
            r4 = 3
            java.lang.String r4 = r2.group(r4)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            java.util.ArrayList<java.lang.String> r5 = com.Young.reddionic.RedditAPI.newreddits     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            r5.add(r4)     // Catch:{ HttpHostConnectException -> 0x00d7, ClientProtocolException -> 0x0132, IOException -> 0x0130 }
            goto L_0x006f
        L_0x00d7:
            r0 = move-exception
        L_0x00d8:
            r0 = 1
            r6 = r0
            r0 = r1
            r1 = r6
            goto L_0x0091
        L_0x00dd:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x00e1:
            r0.printStackTrace()
            r0 = r1
            r1 = r3
            goto L_0x0091
        L_0x00e7:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x00eb:
            r0.printStackTrace()
            r0 = r1
            r1 = r3
            goto L_0x0091
        L_0x00f1:
            java.lang.Object r0 = r1.next()     // Catch:{ ConcurrentModificationException -> 0x010c }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ ConcurrentModificationException -> 0x010c }
            java.lang.String r2 = "*"
            boolean r2 = r0.contains(r2)     // Catch:{ ConcurrentModificationException -> 0x010c }
            java.lang.String r3 = "&"
            boolean r3 = r0.contains(r3)     // Catch:{ ConcurrentModificationException -> 0x010c }
            r2 = r2 | r3
            if (r2 == 0) goto L_0x010e
            java.util.ArrayList<java.lang.String> r2 = com.Young.reddionic.RedditAPI.reddits     // Catch:{ ConcurrentModificationException -> 0x010c }
            r2.add(r0)     // Catch:{ ConcurrentModificationException -> 0x010c }
            goto L_0x0097
        L_0x010c:
            r0 = move-exception
            goto L_0x00a4
        L_0x010e:
            java.util.ArrayList<java.lang.String> r2 = com.Young.reddionic.RedditAPI.newreddits     // Catch:{ ConcurrentModificationException -> 0x010c }
            boolean r2 = r2.contains(r0)     // Catch:{ ConcurrentModificationException -> 0x010c }
            if (r2 == 0) goto L_0x0097
            java.util.ArrayList<java.lang.String> r2 = com.Young.reddionic.RedditAPI.reddits     // Catch:{ ConcurrentModificationException -> 0x010c }
            r2.add(r0)     // Catch:{ ConcurrentModificationException -> 0x010c }
            java.util.ArrayList<java.lang.String> r2 = com.Young.reddionic.RedditAPI.newreddits     // Catch:{ ConcurrentModificationException -> 0x010c }
            r2.remove(r0)     // Catch:{ ConcurrentModificationException -> 0x010c }
            goto L_0x0097
        L_0x0122:
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            com.Young.reddionic.DatabaseHelper r2 = com.Young.reddionic.Browse.dbh
            r3 = 0
            r4 = 1
            r2.insertReddit(r3, r0, r4)
            goto L_0x00be
        L_0x0130:
            r0 = move-exception
            goto L_0x00eb
        L_0x0132:
            r0 = move-exception
            goto L_0x00e1
        L_0x0134:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00d8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.RedditAPI.getSubreddit():void");
    }

    /* JADX INFO: Multiple debug info for r1v3 org.apache.http.HttpResponse: [D('httpget' org.apache.http.client.methods.HttpGet), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r1v4 org.apache.http.HttpEntity: [D('entity' org.apache.http.HttpEntity), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r2v6 java.lang.String: [D('responseString' java.lang.String), D('subreddit_id' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v6 java.util.ArrayList: [D('nvps' java.util.List<org.apache.http.NameValuePair>), D('end' int)] */
    /* JADX INFO: Multiple debug info for r5v15 'subReddit'  java.lang.String: [D('entity' org.apache.http.HttpEntity), D('httppost' org.apache.http.client.methods.HttpPost)] */
    /* JADX INFO: Multiple debug info for r6v16 java.lang.String: [D('error' java.lang.String), D('response' org.apache.http.HttpResponse)] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d2 A[SYNTHETIC, Splitter:B:24:0x00d2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean subscribe(java.lang.String r5, java.lang.String r6) {
        /*
            r0 = 0
            java.lang.String r1 = com.Young.reddionic.RedditAPI.mModhash
            if (r1 != 0) goto L_0x0009
            r5 = 0
            r6 = r5
            r5 = r0
        L_0x0008:
            return r6
        L_0x0009:
            java.lang.String r1 = com.Young.reddionic.RedditAPI.mSubreddit     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            com.Young.reddionic.RedditAPI.mSubreddit = r5     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            com.Young.reddionic.RedditAPI.mSubreddit = r1     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            java.lang.String r3 = "http://www.reddit.com/r/"
            r2.<init>(r3)     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            java.lang.String r3 = ".json?limit=1"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            org.apache.http.impl.client.DefaultHttpClient r2 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            org.apache.http.HttpResponse r1 = r2.execute(r1)     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            org.apache.http.HttpEntity r1 = r1.getEntity()     // Catch:{ Exception -> 0x00e2, all -> 0x00cd }
            java.lang.String r2 = org.apache.http.util.EntityUtils.toString(r1)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r0 = ":"
            java.lang.String r3 = "subreddit_id"
            int r3 = r2.indexOf(r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            int r0 = r2.indexOf(r0, r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            int r3 = r0 + 3
            java.lang.String r0 = ","
            int r0 = r2.indexOf(r0, r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r4 = 1
            int r0 = r0 - r4
            java.lang.String r2 = r2.substring(r3, r0)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r0.<init>()     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.message.BasicNameValuePair r3 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r4 = "sr"
            r3.<init>(r4, r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r0.add(r3)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r3 = "action"
            r2.<init>(r3, r6)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r0.add(r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.message.BasicNameValuePair r6 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r2 = "r"
            r6.<init>(r2, r5)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r0.add(r6)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.message.BasicNameValuePair r5 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r6 = "renderstyle"
            java.lang.String r2 = "html"
            r5.<init>(r6, r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r0.add(r5)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.message.BasicNameValuePair r5 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r6 = "uh"
            java.lang.String r2 = com.Young.reddionic.RedditAPI.mModhash     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r5.<init>(r6, r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r0.add(r5)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r6 = "http://www.reddit.com/api/subscribe"
            r5.<init>(r6)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.client.entity.UrlEncodedFormEntity r6 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r2 = "UTF-8"
            r6.<init>(r0, r2)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            r5.setEntity(r6)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r6 = "Reddit"
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            android.util.Log.d(r6, r0)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.impl.client.DefaultHttpClient r6 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.HttpResponse r6 = r6.execute(r5)     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            org.apache.http.HttpEntity r5 = r6.getEntity()     // Catch:{ Exception -> 0x00e5, all -> 0x00dc }
            java.lang.String r6 = checkResponseErrors(r6, r5)     // Catch:{ Exception -> 0x00bc, all -> 0x00e0 }
            if (r6 == 0) goto L_0x00c5
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x00bc, all -> 0x00e0 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x00bc, all -> 0x00e0 }
            throw r0     // Catch:{ Exception -> 0x00bc, all -> 0x00e0 }
        L_0x00bc:
            r6 = move-exception
        L_0x00bd:
            if (r5 == 0) goto L_0x00c2
            r5.consumeContent()     // Catch:{ Exception -> 0x00d8 }
        L_0x00c2:
            r6 = 0
            goto L_0x0008
        L_0x00c5:
            if (r5 == 0) goto L_0x00ca
            r5.consumeContent()     // Catch:{ Exception -> 0x00d6 }
        L_0x00ca:
            r6 = 1
            goto L_0x0008
        L_0x00cd:
            r5 = move-exception
            r6 = r5
            r5 = r0
        L_0x00d0:
            if (r5 == 0) goto L_0x00d5
            r5.consumeContent()     // Catch:{ Exception -> 0x00da }
        L_0x00d5:
            throw r6
        L_0x00d6:
            r6 = move-exception
            goto L_0x00ca
        L_0x00d8:
            r6 = move-exception
            goto L_0x00c2
        L_0x00da:
            r5 = move-exception
            goto L_0x00d5
        L_0x00dc:
            r5 = move-exception
            r6 = r5
            r5 = r1
            goto L_0x00d0
        L_0x00e0:
            r6 = move-exception
            goto L_0x00d0
        L_0x00e2:
            r5 = move-exception
            r5 = r0
            goto L_0x00bd
        L_0x00e5:
            r5 = move-exception
            r5 = r1
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.RedditAPI.subscribe(java.lang.String, java.lang.String):boolean");
    }

    public static boolean save(String mThingFullname, String save) {
        HttpEntity entity = null;
        if (mModhash == null) {
            return false;
        }
        try {
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("id", mThingFullname));
            nvps.add(new BasicNameValuePair("uh", mModhash));
            HttpPost httppost = new HttpPost("http://www.reddit.com/api/" + save);
            httppost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            Log.d(TAG, nvps.toString());
            HttpResponse response = mClient.execute(httppost);
            HttpEntity entity2 = response.getEntity();
            String error = checkResponseErrors(response, entity2);
            if (error != null) {
                throw new Exception(error);
            }
            if (entity2 != null) {
                try {
                    entity2.consumeContent();
                } catch (Exception e) {
                }
            }
            return true;
        } catch (Exception e2) {
            e2.getMessage();
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e3) {
                }
            }
            return false;
        } catch (Throwable th) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }

    public static boolean hide(String mThingFullname, String hidden) {
        HttpEntity entity = null;
        if (mModhash == null) {
            return false;
        }
        try {
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("id", mThingFullname));
            nvps.add(new BasicNameValuePair("uh", mModhash));
            HttpPost httppost = new HttpPost("http://www.reddit.com/api/" + hidden);
            httppost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            Log.d(TAG, nvps.toString());
            HttpResponse response = mClient.execute(httppost);
            HttpEntity entity2 = response.getEntity();
            String error = checkResponseErrors(response, entity2);
            if (error != null) {
                throw new Exception(error);
            }
            if (entity2 != null) {
                try {
                    entity2.consumeContent();
                } catch (Exception e) {
                }
            }
            return true;
        } catch (Exception e2) {
            e2.getMessage();
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e3) {
                }
            }
            return false;
        } catch (Throwable th) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }

    /* JADX INFO: Multiple debug info for r0v15 int: [D('editor_id_end' int), D('start' int)] */
    /* JADX INFO: Multiple debug info for r1v5 java.lang.String: [D('editor_id_start' int), D('editor_id' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v27 org.apache.http.HttpResponse: [D('httppost' org.apache.http.client.methods.HttpPost), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r10v28 java.lang.String: [D('editor_id_end' int), D('imgLink' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v34 java.io.ByteArrayOutputStream: [D('editor_id_end' int), D('bos' java.io.ByteArrayOutputStream)] */
    /* JADX INFO: Multiple debug info for r0v35 byte[]: [D('bos' java.io.ByteArrayOutputStream), D('data' byte[])] */
    /* JADX INFO: Multiple debug info for r0v37 org.apache.http.HttpResponse: [D('reqEntity' org.apache.http.entity.mime.MultipartEntity), D('response' org.apache.http.HttpResponse)] */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00f5, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00f6, code lost:
        r1 = r6;
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x011d, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x011e, code lost:
        r0 = r10;
        r10 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0121, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0127, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0128, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x012a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x012b, code lost:
        r9 = r0;
        r0 = r10;
        r10 = r9;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00ef A[SYNTHETIC, Splitter:B:16:0x00ef] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0113 A[SYNTHETIC, Splitter:B:34:0x0113] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x011d A[ExcHandler: all (r10v11 'entity' org.apache.http.HttpEntity A[CUSTOM_DECLARE]), Splitter:B:3:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0121 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x00a3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String upload(android.graphics.Bitmap r10) {
        /*
            r0 = 0
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x00ff }
            r3.<init>()     // Catch:{ Exception -> 0x00ff }
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x00ff }
            java.lang.String r2 = "http://minus.com/api/CreateGallery"
            r1.<init>(r2)     // Catch:{ Exception -> 0x00ff }
            org.apache.http.client.entity.UrlEncodedFormEntity r2 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x00ff }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x00ff }
            r1.setEntity(r2)     // Catch:{ Exception -> 0x00ff }
            org.apache.http.impl.client.DefaultHttpClient r2 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x00ff }
            org.apache.http.HttpResponse r6 = r2.execute(r1)     // Catch:{ Exception -> 0x00ff }
            org.apache.http.HttpEntity r2 = r6.getEntity()     // Catch:{ Exception -> 0x00ff }
            java.lang.String r7 = org.apache.http.util.EntityUtils.toString(r2)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            java.lang.String r0 = "reader_id"
            int r0 = r7.indexOf(r0)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            java.lang.String r1 = ":"
            int r0 = r7.indexOf(r1, r0)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            int r1 = r0 + 3
            java.lang.String r0 = ","
            int r0 = r7.indexOf(r0, r1)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            r4 = 1
            int r0 = r0 - r4
            java.lang.String r5 = r7.substring(r1, r0)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            java.lang.String r0 = "editor_id"
            int r0 = r7.indexOf(r0)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            java.lang.String r1 = ":"
            int r0 = r7.indexOf(r1, r0)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            int r1 = r0 + 3
            java.lang.String r0 = "}"
            int r0 = r7.indexOf(r0, r1)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            r4 = 1
            int r0 = r0 - r4
            java.lang.String r1 = r7.substring(r1, r0)     // Catch:{ Exception -> 0x0127, all -> 0x011d }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            r0.<init>()     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            r8 = 75
            r10.compress(r4, r8, r0)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            byte[] r0 = r0.toByteArray()     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            java.lang.String r8 = "http://minus.com/api/UploadItem?editor_id="
            r10.<init>(r8)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            java.lang.StringBuilder r10 = r10.append(r1)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            java.lang.String r1 = "&key=OK&filename=screenshot.jpg"
            java.lang.StringBuilder r10 = r10.append(r1)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            r4.<init>(r10)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            org.apache.http.entity.mime.content.ByteArrayBody r10 = new org.apache.http.entity.mime.content.ByteArrayBody     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            java.lang.String r1 = "screenshot.jpg"
            r10.<init>(r0, r1)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            org.apache.http.entity.mime.MultipartEntity r0 = new org.apache.http.entity.mime.MultipartEntity     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            org.apache.http.entity.mime.HttpMultipartMode r1 = org.apache.http.entity.mime.HttpMultipartMode.BROWSER_COMPATIBLE     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            java.lang.String r1 = "file"
            r0.addPart(r1, r10)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            r4.setEntity(r0)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            org.apache.http.impl.client.DefaultHttpClient r10 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            org.apache.http.HttpResponse r0 = r10.execute(r4)     // Catch:{ Exception -> 0x00f5, all -> 0x011d }
            org.apache.http.HttpEntity r10 = r0.getEntity()     // Catch:{ Exception -> 0x0132, all -> 0x011d }
            java.lang.String r1 = org.apache.http.util.EntityUtils.toString(r10)     // Catch:{ Exception -> 0x0136, all -> 0x0121 }
        L_0x00a7:
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            java.lang.String r2 = "http://minus.com/api/GetItems/m"
            r1.<init>(r2)     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            org.apache.http.client.entity.UrlEncodedFormEntity r1 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            java.lang.String r2 = "UTF-8"
            r1.<init>(r3, r2)     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            r0.setEntity(r1)     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            org.apache.http.impl.client.DefaultHttpClient r1 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            org.apache.http.HttpResponse r0 = r1.execute(r0)     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            org.apache.http.HttpEntity r1 = r0.getEntity()     // Catch:{ Exception -> 0x012a, all -> 0x0121 }
            java.lang.String r2 = org.apache.http.util.EntityUtils.toString(r1)     // Catch:{ Exception -> 0x012f, all -> 0x0123 }
            java.lang.String r10 = "ITEMS_GALLERY"
            int r10 = r2.indexOf(r10)     // Catch:{ Exception -> 0x012f, all -> 0x0123 }
            java.lang.String r0 = "["
            int r10 = r2.indexOf(r0, r10)     // Catch:{ Exception -> 0x012f, all -> 0x0123 }
            int r0 = r10 + 2
            java.lang.String r10 = "]"
            int r10 = r2.indexOf(r10, r0)     // Catch:{ Exception -> 0x012f, all -> 0x0123 }
            r3 = 1
            int r10 = r10 - r3
            java.lang.String r10 = r2.substring(r0, r10)     // Catch:{ Exception -> 0x012f, all -> 0x0123 }
            if (r1 == 0) goto L_0x00f2
            r1.consumeContent()     // Catch:{ Exception -> 0x0117 }
        L_0x00f2:
            r0 = r10
            r10 = r1
        L_0x00f4:
            return r0
        L_0x00f5:
            r10 = move-exception
            r1 = r6
            r0 = r2
        L_0x00f8:
            r10.printStackTrace()     // Catch:{ Exception -> 0x00ff }
            r10 = r0
            r0 = r1
            r1 = r7
            goto L_0x00a7
        L_0x00ff:
            r10 = move-exception
        L_0x0100:
            r10.getMessage()     // Catch:{ all -> 0x010d }
            if (r0 == 0) goto L_0x0108
            r0.consumeContent()     // Catch:{ Exception -> 0x0119 }
        L_0x0108:
            r10 = 0
            r9 = r0
            r0 = r10
            r10 = r9
            goto L_0x00f4
        L_0x010d:
            r10 = move-exception
            r9 = r10
            r10 = r0
            r0 = r9
        L_0x0111:
            if (r10 == 0) goto L_0x0116
            r10.consumeContent()     // Catch:{ Exception -> 0x011b }
        L_0x0116:
            throw r0
        L_0x0117:
            r0 = move-exception
            goto L_0x00f2
        L_0x0119:
            r10 = move-exception
            goto L_0x0108
        L_0x011b:
            r10 = move-exception
            goto L_0x0116
        L_0x011d:
            r10 = move-exception
            r0 = r10
            r10 = r2
            goto L_0x0111
        L_0x0121:
            r0 = move-exception
            goto L_0x0111
        L_0x0123:
            r10 = move-exception
            r0 = r10
            r10 = r1
            goto L_0x0111
        L_0x0127:
            r10 = move-exception
            r0 = r2
            goto L_0x0100
        L_0x012a:
            r0 = move-exception
            r9 = r0
            r0 = r10
            r10 = r9
            goto L_0x0100
        L_0x012f:
            r10 = move-exception
            r0 = r1
            goto L_0x0100
        L_0x0132:
            r10 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x00f8
        L_0x0136:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r10
            r10 = r9
            goto L_0x00f8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.RedditAPI.upload(android.graphics.Bitmap):java.lang.String");
    }

    public static boolean comment(String id, String text) {
        HttpEntity entity = null;
        if (mModhash == null) {
            return false;
        }
        try {
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("thing_id", id));
            nvps.add(new BasicNameValuePair("text", text));
            nvps.add(new BasicNameValuePair("uh", mModhash));
            HttpPost httppost = new HttpPost("http://www.reddit.com/api/comment");
            httppost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            Log.d(TAG, nvps.toString());
            HttpResponse response = mClient.execute(httppost);
            entity = response.getEntity();
            if (EntityUtils.toString(response.getEntity()).contains("content")) {
                if (entity != null) {
                    try {
                        entity.consumeContent();
                    } catch (Exception e) {
                    }
                }
                return true;
            }
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e2) {
                }
            }
            return false;
        } catch (Exception e3) {
            e3.getMessage();
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e4) {
                }
            }
            return false;
        } catch (Throwable th) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e5) {
                }
            }
            throw th;
        }
    }

    public static boolean vote(String mReddit, String mThingFullname, int mDirection) {
        HttpEntity entity = null;
        if (mModhash == null) {
            return false;
        }
        try {
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("id", mThingFullname));
            nvps.add(new BasicNameValuePair("dir", String.valueOf(mDirection)));
            nvps.add(new BasicNameValuePair("uh", mModhash));
            HttpPost httppost = new HttpPost("http://www.reddit.com/api/vote");
            httppost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            Log.d(TAG, nvps.toString());
            HttpResponse response = mClient.execute(httppost);
            Log.e("response", EntityUtils.toString(response.getEntity()));
            String error = checkResponseErrors(response, entity);
            if (error != null) {
                throw new Exception(error);
            }
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e) {
                }
            }
            return true;
        } catch (Exception e2) {
            e2.getMessage();
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e3) {
                }
            }
            return false;
        } catch (Throwable th) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }

    /* JADX INFO: Multiple debug info for r0v19 org.apache.http.Header: [D('request' org.apache.http.client.methods.HttpGet), D('contentLengthHeader' org.apache.http.Header)] */
    /* JADX INFO: Multiple debug info for r2v15 'in'  java.io.InputStream: [D('in' java.io.InputStream), D('response' org.apache.http.HttpResponse)] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ff A[SYNTHETIC, Splitter:B:38:0x00ff] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01f9 A[SYNTHETIC, Splitter:B:65:0x01f9] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0211 A[SYNTHETIC, Splitter:B:74:0x0211] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x023a  */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void getThread(int r9) {
        /*
            r1 = 0
            r0 = 0
            r2 = 0
            java.util.ArrayList<java.lang.String> r3 = com.Young.reddionic.RedditAPI.mAfterBucket
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x0010
            r3 = 0
        L_0x000c:
            int r4 = com.Young.reddionic.RedditAPI.bucketSize
            if (r3 < r4) goto L_0x0106
        L_0x0010:
            if (r9 != 0) goto L_0x01a2
            java.lang.String r3 = "reddit front page"
            java.lang.String r4 = com.Young.reddionic.RedditAPI.mSubreddit     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = com.Young.reddionic.RedditAPI.mSubreddit     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            if (r4 != 0) goto L_0x0122
            r4 = 1
        L_0x001f:
            r3 = r3 | r4
            if (r3 == 0) goto L_0x0125
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "http://reddit.com/"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = com.Young.reddionic.RedditAPI.mSortByUrl     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = ".json"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = com.Young.reddionic.RedditAPI.mSortByUrlExtra     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "&"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
        L_0x004d:
            java.util.ArrayList<java.lang.String> r4 = com.Young.reddionic.RedditAPI.mAfterBucket     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            int r5 = com.Young.reddionic.RedditAPI.currentBucket     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.Object r4 = r4.get(r5)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            if (r4 == 0) goto L_0x0166
            java.lang.String r4 = "count="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            int r4 = com.Young.reddionic.RedditAPI.mCount     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "&after="
            java.lang.StringBuilder r4 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.util.ArrayList<java.lang.String> r3 = com.Young.reddionic.RedditAPI.mAfterBucket     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            int r5 = com.Young.reddionic.RedditAPI.currentBucket     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.Object r3 = r3.get(r5)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "&"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            r0 = 1
            r4 = r2
            r8 = r3
            r3 = r0
            r0 = r8
        L_0x0082:
            java.lang.String r2 = "hooo"
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            android.util.Log.v(r2, r5)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r0 = "dd"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r6 = "url="
            r2.<init>(r6)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            r2 = 0
            r0 = 0
            if (r0 != 0) goto L_0x01e2
            r2 = 0
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ IllegalArgumentException -> 0x01cf }
            r0.<init>(r5)     // Catch:{ IllegalArgumentException -> 0x01cf }
        L_0x00ad:
            org.apache.http.impl.client.DefaultHttpClient r2 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            org.apache.http.HttpResponse r2 = r2.execute(r0)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r0 = "Content-Length"
            org.apache.http.Header r0 = r2.getFirstHeader(r0)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            org.apache.http.HttpEntity r1 = r2.getEntity()     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.io.InputStream r2 = r1.getContent()     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            if (r0 == 0) goto L_0x01d7
            java.lang.String r0 = r0.getValue()     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            long r5 = r0.longValue()     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            com.Young.reddionic.RedditAPI.mContentLength = r5     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r0 = "dd"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r6 = "Content length [sent]: "
            r5.<init>(r6)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            long r6 = com.Young.reddionic.RedditAPI.mContentLength     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            android.util.Log.d(r0, r5)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            r0 = r1
            r1 = r2
        L_0x00e9:
            com.Young.reddionic.ProgressInputStream r2 = new com.Young.reddionic.ProgressInputStream     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            long r5 = com.Young.reddionic.RedditAPI.mContentLength     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            r2.<init>(r1, r5)     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            parseSubredditJSON(r2, r9)     // Catch:{ IllegalStateException -> 0x01e6, Exception -> 0x0201 }
            int r9 = com.Young.reddionic.RedditAPI.mCount     // Catch:{ IllegalStateException -> 0x01e6, Exception -> 0x0201 }
            com.Young.reddionic.RedditAPI.mLastCount = r9     // Catch:{ IllegalStateException -> 0x01e6, Exception -> 0x0201 }
            r2.close()     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            r1.close()     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
        L_0x00fd:
            if (r0 == 0) goto L_0x023a
            r0.consumeContent()     // Catch:{ Exception -> 0x021d }
            r1 = r4
            r9 = r0
            r0 = r3
        L_0x0105:
            return
        L_0x0106:
            java.util.ArrayList<java.lang.String> r4 = com.Young.reddionic.RedditAPI.mAfterBucket
            r5 = 0
            r4.add(r5)
            java.util.ArrayList<java.lang.String> r4 = com.Young.reddionic.RedditAPI.mBeforeBucket
            r5 = 0
            r4.add(r5)
            java.util.ArrayList<java.lang.String> r4 = com.Young.reddionic.RedditAPI.mLastAfterBucket
            r5 = 0
            r4.add(r5)
            java.util.ArrayList<java.lang.String> r4 = com.Young.reddionic.RedditAPI.mLastBeforeBucket
            r5 = 0
            r4.add(r5)
            int r3 = r3 + 1
            goto L_0x000c
        L_0x0122:
            r4 = 0
            goto L_0x001f
        L_0x0125:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "http://reddit.com/r/"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = com.Young.reddionic.RedditAPI.mSubreddit     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = com.Young.reddionic.RedditAPI.mSortByUrl     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = ".json"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = com.Young.reddionic.RedditAPI.mSortByUrlExtra     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "&"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            goto L_0x004d
        L_0x0166:
            java.util.ArrayList<java.lang.String> r4 = com.Young.reddionic.RedditAPI.mBeforeBucket     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            int r5 = com.Young.reddionic.RedditAPI.currentBucket     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.Object r4 = r4.get(r5)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            if (r4 == 0) goto L_0x023f
            java.lang.String r4 = "count="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            int r4 = com.Young.reddionic.RedditAPI.mCount     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            int r4 = r4 + 1
            r5 = 25
            int r4 = r4 - r5
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "&before="
            java.lang.StringBuilder r4 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.util.ArrayList<java.lang.String> r3 = com.Young.reddionic.RedditAPI.mBeforeBucket     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            int r5 = com.Young.reddionic.RedditAPI.currentBucket     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.Object r3 = r3.get(r5)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "&"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            r2 = 1
            r4 = r2
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x0082
        L_0x01a2:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "http://reddit.com/r/"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = com.Young.reddionic.RedditAPI.mSubreddit     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = ".json"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            java.lang.String r4 = "?limit=1"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0231, all -> 0x0225 }
            r4 = r2
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x0082
        L_0x01cf:
            r0 = move-exception
            java.lang.String r0 = "Invalid subreddit."
            com.Young.reddionic.RedditAPI.mUserError = r0     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            r0 = r2
            goto L_0x00ad
        L_0x01d7:
            r5 = -1
            com.Young.reddionic.RedditAPI.mContentLength = r5     // Catch:{ Exception -> 0x0235, all -> 0x022b }
            java.lang.String r0 = "dd"
            java.lang.String r5 = "Content length not available"
            android.util.Log.d(r0, r5)     // Catch:{ Exception -> 0x0235, all -> 0x022b }
        L_0x01e2:
            r0 = r1
            r1 = r2
            goto L_0x00e9
        L_0x01e6:
            r9 = move-exception
            java.lang.String r9 = "Invalid subreddit."
            com.Young.reddionic.RedditAPI.mUserError = r9     // Catch:{ all -> 0x0215 }
            r2.close()     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            r1.close()     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            goto L_0x00fd
        L_0x01f3:
            r9 = move-exception
            r1 = r4
            r9 = r0
            r0 = r3
        L_0x01f7:
            if (r9 == 0) goto L_0x0105
            r9.consumeContent()     // Catch:{ Exception -> 0x01fe }
            goto L_0x0105
        L_0x01fe:
            r2 = move-exception
            goto L_0x0105
        L_0x0201:
            r9 = move-exception
            r2.close()     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            r1.close()     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            goto L_0x00fd
        L_0x020a:
            r9 = move-exception
            r2 = r9
            r1 = r4
            r9 = r0
            r0 = r3
        L_0x020f:
            if (r9 == 0) goto L_0x0214
            r9.consumeContent()     // Catch:{ Exception -> 0x0223 }
        L_0x0214:
            throw r2
        L_0x0215:
            r9 = move-exception
            r2.close()     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            r1.close()     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
            throw r9     // Catch:{ Exception -> 0x01f3, all -> 0x020a }
        L_0x021d:
            r9 = move-exception
            r1 = r4
            r9 = r0
            r0 = r3
            goto L_0x0105
        L_0x0223:
            r9 = move-exception
            goto L_0x0214
        L_0x0225:
            r9 = move-exception
            r8 = r9
            r9 = r1
            r1 = r2
            r2 = r8
            goto L_0x020f
        L_0x022b:
            r9 = move-exception
            r2 = r9
            r0 = r3
            r9 = r1
            r1 = r4
            goto L_0x020f
        L_0x0231:
            r9 = move-exception
            r9 = r1
            r1 = r2
            goto L_0x01f7
        L_0x0235:
            r9 = move-exception
            r0 = r3
            r9 = r1
            r1 = r4
            goto L_0x01f7
        L_0x023a:
            r1 = r4
            r9 = r0
            r0 = r3
            goto L_0x0105
        L_0x023f:
            r4 = r2
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.RedditAPI.getThread(int):void");
    }

    protected static void parseSubredditJSON(InputStream in, int type) throws IOException, JsonParseException, IllegalStateException {
        try {
            Listing listing = (Listing) mOm.readValue(in, Listing.class);
            if (!"Listing".equals(listing.getKind())) {
                throw new IllegalStateException("Not a subreddit listing");
            }
            ListingData data = listing.getData();
            if ("".equals(data.getModhash())) {
                mModhash = null;
            } else {
                mModhash = data.getModhash();
            }
            mLastAfterBucket.set(currentBucket, mAfterBucket.get(currentBucket));
            mLastBeforeBucket.set(currentBucket, mBeforeBucket.get(currentBucket));
            mAfterBucket.set(currentBucket, data.getAfter());
            mBeforeBucket.set(currentBucket, data.getBefore());
            if (data.getChildren().length < 25) {
                mLastPages.set(currentBucket, true);
            }
            try {
                mThingInfoss.get(currentBucket).clear();
            } catch (IndexOutOfBoundsException e) {
            }
            for (ThingListing tiContainer : data.getChildren()) {
                if ("t3".equals(tiContainer.getKind())) {
                    ThingInfo ti = tiContainer.getData();
                    ti.setTitle(Html.fromHtml(ti.getTitle()).toString());
                    if (type == 0) {
                        if (mThingInfoss.size() == 0) {
                            for (int x = 0; x < bucketSize; x++) {
                                mThingInfoss.add(new ArrayList());
                            }
                        }
                        mThingInfoss.get(currentBucket).add(tiContainer.getData());
                    } else {
                        mThingInfos2.add(tiContainer.getData());
                    }
                }
            }
        } catch (Exception e2) {
        }
    }

    /* JADX INFO: Multiple debug info for r6v20 'entity'  org.apache.http.HttpEntity: [D('entity' org.apache.http.HttpEntity), D('contentLengthHeader' org.apache.http.Header)] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a5 A[SYNTHETIC, Splitter:B:44:0x00a5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void getComment(java.lang.String r5, java.lang.String r6, int r7) {
        /*
            r0 = 0
            r1 = 0
            r1 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.String r2 = "http://reddit.com"
            r1.<init>(r2)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.String r2 = "/comments/"
            java.lang.StringBuilder r2 = r1.append(r2)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.StringBuilder r6 = r2.append(r6)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.String r2 = ".json?"
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            com.Young.reddionic.RedditSettings r2 = com.Young.reddionic.RedditAPI.mSettings     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.String r2 = r2.commentsSortByUrl     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.String r2 = "&"
            r6.append(r2)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.String r6 = "hooo"
            java.lang.String r2 = r1.toString()     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            android.util.Log.v(r6, r2)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.String r2 = r1.toString()     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            r1 = 0
            r6 = 0
            if (r6 != 0) goto L_0x00bf
            r1 = 0
            org.apache.http.client.methods.HttpGet r6 = new org.apache.http.client.methods.HttpGet     // Catch:{ IllegalArgumentException -> 0x007a }
            r6.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x007a }
        L_0x0042:
            org.apache.http.impl.client.DefaultHttpClient r1 = com.Young.reddionic.RedditAPI.mClient     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            org.apache.http.HttpResponse r1 = r1.execute(r6)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.lang.String r6 = "Content-Length"
            org.apache.http.Header r6 = r1.getFirstHeader(r6)     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            org.apache.http.HttpEntity r6 = r1.getEntity()     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            java.io.InputStream r0 = r6.getContent()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
        L_0x0056:
            java.lang.String r1 = "here"
            java.lang.String r2 = "got entity"
            android.util.Log.v(r1, r2)     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            com.Young.reddionic.ProgressInputStream r1 = new com.Young.reddionic.ProgressInputStream     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            long r2 = com.Young.reddionic.RedditAPI.mContentLength     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            r1.<init>(r0, r2)     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            parseCommentJSON(r1, r5, r7)     // Catch:{ IllegalStateException -> 0x0081, Exception -> 0x0097 }
            r1.close()     // Catch:{ IllegalStateException -> 0x0081, Exception -> 0x0097 }
            r0.close()     // Catch:{ IllegalStateException -> 0x0081, Exception -> 0x0097 }
            r1.close()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            r0.close()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
        L_0x0073:
            if (r6 == 0) goto L_0x00bd
            r6.consumeContent()     // Catch:{ Exception -> 0x00b1 }
            r5 = r6
        L_0x0079:
            return
        L_0x007a:
            r6 = move-exception
            java.lang.String r6 = "Invalid subreddit."
            com.Young.reddionic.RedditAPI.mUserError = r6     // Catch:{ Exception -> 0x00ba, all -> 0x00b6 }
            r6 = r1
            goto L_0x0042
        L_0x0081:
            r5 = move-exception
            java.lang.String r5 = "Invalid subreddit."
            com.Young.reddionic.RedditAPI.mUserError = r5     // Catch:{ all -> 0x00a9 }
            r1.close()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            r0.close()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            goto L_0x0073
        L_0x008d:
            r5 = move-exception
            r5 = r6
        L_0x008f:
            if (r5 == 0) goto L_0x0079
            r5.consumeContent()     // Catch:{ Exception -> 0x0095 }
            goto L_0x0079
        L_0x0095:
            r6 = move-exception
            goto L_0x0079
        L_0x0097:
            r5 = move-exception
            r1.close()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            r0.close()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            goto L_0x0073
        L_0x009f:
            r5 = move-exception
            r4 = r5
            r5 = r6
            r6 = r4
        L_0x00a3:
            if (r5 == 0) goto L_0x00a8
            r5.consumeContent()     // Catch:{ Exception -> 0x00b4 }
        L_0x00a8:
            throw r6
        L_0x00a9:
            r5 = move-exception
            r1.close()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            r0.close()     // Catch:{ Exception -> 0x008d, all -> 0x009f }
            throw r5     // Catch:{ Exception -> 0x008d, all -> 0x009f }
        L_0x00b1:
            r5 = move-exception
            r5 = r6
            goto L_0x0079
        L_0x00b4:
            r5 = move-exception
            goto L_0x00a8
        L_0x00b6:
            r5 = move-exception
            r6 = r5
            r5 = r0
            goto L_0x00a3
        L_0x00ba:
            r5 = move-exception
            r5 = r0
            goto L_0x008f
        L_0x00bd:
            r5 = r6
            goto L_0x0079
        L_0x00bf:
            r6 = r0
            r0 = r1
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.RedditAPI.getComment(java.lang.String, java.lang.String, int):void");
    }

    protected static void parseCommentJSON(InputStream in, String mThreadId, int indent) throws IOException, JsonParseException, IllegalStateException {
        try {
            Listing[] listing = (Listing[]) mOm.readValue(in, Listing[].class);
            if (!"Listing".equals(listing[0].getKind())) {
                throw new IllegalStateException("Not a comments listing");
            }
            ListingData data = listing[0].getData();
            if ("".equals(data.getModhash())) {
                mModhash = null;
            } else {
                mModhash = data.getModhash();
            }
            ThingListing thingListing = data.getChildren()[0];
            for (ThingListing a : listing[1].getData().getChildren()) {
                if (indent == 0) {
                    insertNestedComment(a, 0, mThreadId);
                } else {
                    insertNestedComment(a, indent, mThreadId);
                }
            }
        } catch (Exception e) {
        }
    }

    static void insertNestedComment(ThingListing commentThingListing, int indentLevel, String link_id) {
        ListingData repliesListingData;
        ThingListing[] replyThingListings;
        ThingInfo ci = commentThingListing.getData();
        ci.setIndent(indentLevel);
        if (!"more".equals(commentThingListing.getKind()) && "t1".equals(commentThingListing.getKind())) {
            commentInfos.add(ci);
            Listing repliesListing = ci.getReplies();
            if (repliesListing != null && (repliesListingData = repliesListing.getData()) != null && (replyThingListings = repliesListingData.getChildren()) != null) {
                for (ThingListing replyThingListing : replyThingListings) {
                    insertNestedComment(replyThingListing, indentLevel + 1, link_id);
                }
                ThingInfo theEnd = new ThingInfo();
                theEnd.setLastComment(true);
                theEnd.setIndent(indentLevel);
                commentInfos.add(theEnd);
            }
        }
    }

    static String checkResponseErrors(HttpResponse response, HttpEntity entity) {
        String status = response.getStatusLine().toString();
        if (!status.contains("OK")) {
            return "HTTP error. Status = " + status;
        }
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(entity.getContent()));
            String line = in.readLine();
            logDLong(TAG, line);
            in.close();
            if (line == null || "".equals(line)) {
                return "API returned empty data.";
            }
            if (line.contains("WRONG_PASSWORD")) {
                return "Wrong password.";
            }
            if (line.contains("USER_REQUIRED")) {
                return "Login expired.";
            }
            if (line.contains("SUBREDDIT_NOEXIST")) {
                return "That subreddit does not exist.";
            }
            if (line.contains("SUBREDDIT_NOTALLOWED")) {
                return "You are not allowed to post to that subreddit.";
            }
            return null;
        } catch (IOException e) {
            return "Error reading retrieved data.";
        }
    }

    public static ArrayList<String> message(String username) {
        HttpEntity entity = null;
        ArrayList<String> data = new ArrayList<>();
        try {
            Log.d("dd", "url=" + "http://www.reddit.com/message/inbox/.json");
            HttpGet request = null;
            try {
                request = new HttpGet("http://www.reddit.com/message/inbox/.json");
            } catch (IllegalArgumentException e) {
                mUserError = "Invalid User.";
            }
            HttpResponse response = mClient.execute(request);
            Header contentLengthHeader = response.getFirstHeader("Content-Length");
            entity = response.getEntity();
            InputStream in = entity.getContent();
            if (contentLengthHeader != null) {
                mContentLength = Long.valueOf(contentLengthHeader.getValue()).longValue();
                Log.d("dd", "Content length [sent]: " + mContentLength);
            } else {
                mContentLength = -1;
                Log.d("dd", "Content length not available");
            }
            ProgressInputStream pin = new ProgressInputStream(in, mContentLength);
            try {
                parseMessageJSON(pin);
                pin.close();
                in.close();
            } catch (IllegalStateException e2) {
                mUserError = "Invalid subreddit.";
                pin.close();
                in.close();
            } catch (Exception e3) {
                pin.close();
                in.close();
            } catch (Throwable th) {
                pin.close();
                in.close();
                throw th;
            }
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e4) {
                }
            }
        } catch (Exception e5) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e6) {
                }
            }
        } catch (Throwable th2) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (Exception e7) {
                }
            }
            throw th2;
        }
        return data;
    }

    protected static void parseMessageJSON(InputStream in) throws IOException, JsonParseException, IllegalStateException {
        try {
            Listing listing = (Listing) mOm.readValue(in, Listing.class);
            if (!"Listing".equals(listing.getKind())) {
                throw new IllegalStateException("Not a subreddit listing");
            }
            ListingData data = listing.getData();
            if ("".equals(data.getModhash())) {
                mModhash = null;
            } else {
                mModhash = data.getModhash();
            }
            mAfterBucket.set(currentBucket, data.getAfter());
            mBeforeBucket.set(currentBucket, data.getBefore());
            if (data.getChildren().length < 25) {
                mLastPages.set(currentBucket, true);
            }
            for (ThingListing tiContainer : data.getChildren()) {
                if ("t3".equals(tiContainer.getKind())) {
                    ThingInfo ti = tiContainer.getData();
                    ti.setTitle(Html.fromHtml(ti.getTitle()).toString());
                    if (mThingInfoss.size() == 0) {
                        for (int x = 0; x < bucketSize; x++) {
                            mThingInfoss.add(new ArrayList());
                        }
                    }
                    mThingInfoss.get(currentBucket).add(tiContainer.getData());
                }
            }
        } catch (Exception e) {
        }
    }

    protected static String getArticle(String url) {
        try {
            BasicHttpContext mHttpContext = new BasicHttpContext();
            CookieStore mCookieStore = new BasicCookieStore();
            mHttpContext.setAttribute("http.cookie-store", mCookieStore);
            HttpResponse execute = mClient.execute(new HttpGet("https://www.readability.com/shorten"), mHttpContext);
            HttpPost httpost = new HttpPost("https://www.readability.com/~/");
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("url", url));
            httpost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            httpost.setHeader("Keep-Alive", "115");
            httpost.setHeader("Connection", "keep-alive");
            httpost.setHeader("Referer", "https://www.readability.com/shorten");
            httpost.setHeader("X-Requested-With", "XMLHttpRequest");
            List<Cookie> cl = mCookieStore.getCookies();
            StringBuffer com2 = new StringBuffer();
            for (Cookie c : cl) {
                com2.append(c.getName());
                com2.append("=");
                com2.append(c.getValue());
                com2.append(";");
            }
            httpost.setHeader("Cookie", com2.toString());
            HttpResponse response = mClient.execute(httpost, mHttpContext);
            return "";
        } catch (IOException e) {
            IOException e2 = e;
            e2.printStackTrace();
            return e2.toString();
        } catch (Exception e3) {
            Exception e4 = e3;
            e4.printStackTrace();
            return e4.toString();
        }
    }
}
