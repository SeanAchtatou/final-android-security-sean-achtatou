package com.Young.reddionic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.support.v4.view.MotionEventCompat;
import android.util.FloatMath;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class TouchImageView extends ImageView {
    static final int DRAG = 1;
    static final int NONE = 0;
    private static final String TAG = "Touch";
    static final int ZOOM = 2;
    static float size_h = 0.0f;
    static float size_w = 0.0f;
    Bitmap bitmap;
    Context context;
    Matrix matrix = new Matrix();
    PointF mid = new PointF();
    int mode = 0;
    float oldDist = 1.0f;
    RectF rect;
    Matrix savedMatrix = new Matrix();
    PointF start = new PointF();

    public TouchImageView(Context context2) {
        super(context2);
        super.setClickable(true);
        this.context = context2;
        this.matrix.setTranslate(1.0f, 1.0f);
        setImageMatrix(this.matrix);
        setScaleType(ImageView.ScaleType.MATRIX);
        setOnTouchListener(new View.OnTouchListener() {
            /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0210  */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0222  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x0256  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x026a  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public boolean onTouch(android.view.View r13, android.view.MotionEvent r14) {
                /*
                    r12 = this;
                    com.Young.reddionic.WrapMotionEvent r0 = com.Young.reddionic.WrapMotionEvent.wrap(r14)
                    int r8 = r0.getAction()
                    r8 = r8 & 255(0xff, float:3.57E-43)
                    switch(r8) {
                        case 0: goto L_0x0018;
                        case 1: goto L_0x008c;
                        case 2: goto L_0x027e;
                        case 3: goto L_0x000d;
                        case 4: goto L_0x000d;
                        case 5: goto L_0x003f;
                        case 6: goto L_0x00b9;
                        default: goto L_0x000d;
                    }
                L_0x000d:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r9 = r9.matrix
                    r8.setImageMatrix(r9)
                    r8 = 1
                    return r8
                L_0x0018:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.savedMatrix
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r9 = r9.matrix
                    r8.set(r9)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.PointF r8 = r8.start
                    float r9 = r0.getX()
                    float r10 = r0.getY()
                    r8.set(r9, r10)
                    java.lang.String r8 = "Touch"
                    java.lang.String r9 = "mode=DRAG"
                    android.util.Log.d(r8, r9)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    r9 = 1
                    r8.mode = r9
                    goto L_0x000d
                L_0x003f:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    float r9 = r9.spacing(r0)
                    r8.oldDist = r9
                    java.lang.String r8 = "Touch"
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    java.lang.String r10 = "oldDist="
                    r9.<init>(r10)
                    com.Young.reddionic.TouchImageView r10 = com.Young.reddionic.TouchImageView.this
                    float r10 = r10.oldDist
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r9 = r9.toString()
                    android.util.Log.d(r8, r9)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    float r8 = r8.oldDist
                    r9 = 1092616192(0x41200000, float:10.0)
                    int r8 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
                    if (r8 <= 0) goto L_0x000d
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.savedMatrix
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r9 = r9.matrix
                    r8.set(r9)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.PointF r9 = r9.mid
                    r8.midPoint(r9, r0)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    r9 = 2
                    r8.mode = r9
                    java.lang.String r8 = "Touch"
                    java.lang.String r9 = "mode=ZOOM"
                    android.util.Log.d(r8, r9)
                    goto L_0x000d
                L_0x008c:
                    float r8 = r0.getX()
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.PointF r9 = r9.start
                    float r9 = r9.x
                    float r8 = r8 - r9
                    float r8 = java.lang.Math.abs(r8)
                    int r5 = (int) r8
                    float r8 = r0.getY()
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.PointF r9 = r9.start
                    float r9 = r9.y
                    float r8 = r8 - r9
                    float r8 = java.lang.Math.abs(r8)
                    int r7 = (int) r8
                    r8 = 8
                    if (r5 >= r8) goto L_0x00b9
                    r8 = 8
                    if (r7 >= r8) goto L_0x00b9
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    r8.performClick()
                L_0x00b9:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    r9 = 0
                    r8.mode = r9
                    java.lang.String r8 = "Touch"
                    java.lang.String r9 = "mode=NONE"
                    android.util.Log.d(r8, r9)
                    r8 = 9
                    float[] r2 = new float[r8]
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.matrix
                    r8.getValues(r2)
                    java.lang.String r8 = "pts1"
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    r10 = 0
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.String r10 = java.lang.String.valueOf(r10)
                    r9.<init>(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    r10 = 1
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    r10 = 2
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    r10 = 3
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    r10 = 4
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    r10 = 5
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    r10 = 6
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    r10 = 7
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    r10 = 8
                    r10 = r2[r10]
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = " "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r9 = r9.toString()
                    android.util.Log.e(r8, r9)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.matrix
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r9 = r9.rect
                    r8.mapRect(r9)
                    java.lang.String r8 = "rect"
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    com.Young.reddionic.TouchImageView r10 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r10 = r10.rect
                    float r10 = r10.top
                    int r10 = java.lang.Math.round(r10)
                    java.lang.String r10 = java.lang.String.valueOf(r10)
                    r9.<init>(r10)
                    java.lang.String r10 = "   "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    com.Young.reddionic.TouchImageView r10 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r10 = r10.rect
                    float r10 = r10.bottom
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = "   "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    com.Young.reddionic.TouchImageView r10 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r10 = r10.rect
                    float r10 = r10.left
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r10 = "   "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    com.Young.reddionic.TouchImageView r10 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r10 = r10.rect
                    float r10 = r10.right
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r9 = r9.toString()
                    android.util.Log.e(r8, r9)
                    r4 = 0
                    r6 = 0
                    java.lang.String r8 = "size"
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    float r10 = com.Young.reddionic.TouchImageView.size_w
                    int r10 = java.lang.Math.round(r10)
                    java.lang.String r10 = java.lang.String.valueOf(r10)
                    r9.<init>(r10)
                    java.lang.String r10 = "       "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    float r10 = com.Young.reddionic.TouchImageView.size_h
                    int r10 = java.lang.Math.round(r10)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r9 = r9.toString()
                    android.util.Log.e(r8, r9)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r8 = r8.rect
                    float r8 = r8.left
                    r9 = 0
                    int r8 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
                    if (r8 >= 0) goto L_0x0256
                    java.lang.String r8 = "move"
                    java.lang.String r9 = "over right"
                    android.util.Log.e(r8, r9)
                L_0x0217:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r8 = r8.rect
                    float r8 = r8.top
                    r9 = 0
                    int r8 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
                    if (r8 >= 0) goto L_0x026a
                    java.lang.String r8 = "move"
                    java.lang.String r9 = "over top"
                    android.util.Log.e(r8, r9)
                L_0x0229:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.matrix
                    r8.postTranslate(r4, r6)
                    java.lang.String r8 = "hmm?"
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    int r10 = java.lang.Math.round(r4)
                    java.lang.String r10 = java.lang.String.valueOf(r10)
                    r9.<init>(r10)
                    java.lang.String r10 = "   "
                    java.lang.StringBuilder r9 = r9.append(r10)
                    int r10 = java.lang.Math.round(r6)
                    java.lang.StringBuilder r9 = r9.append(r10)
                    java.lang.String r9 = r9.toString()
                    android.util.Log.e(r8, r9)
                    goto L_0x000d
                L_0x0256:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r8 = r8.rect
                    float r8 = r8.right
                    float r9 = com.Young.reddionic.TouchImageView.size_w
                    int r8 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
                    if (r8 <= 0) goto L_0x0217
                    java.lang.String r8 = "move"
                    java.lang.String r9 = "over left"
                    android.util.Log.e(r8, r9)
                    goto L_0x0217
                L_0x026a:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.RectF r8 = r8.rect
                    float r8 = r8.bottom
                    float r9 = com.Young.reddionic.TouchImageView.size_h
                    int r8 = (r8 > r9 ? 1 : (r8 == r9 ? 0 : -1))
                    if (r8 <= 0) goto L_0x0229
                    java.lang.String r8 = "move"
                    java.lang.String r9 = "over bottom"
                    android.util.Log.e(r8, r9)
                    goto L_0x0229
                L_0x027e:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    int r8 = r8.mode
                    r9 = 1
                    if (r8 != r9) goto L_0x02af
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.matrix
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r9 = r9.savedMatrix
                    r8.set(r9)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.matrix
                    float r9 = r0.getX()
                    com.Young.reddionic.TouchImageView r10 = com.Young.reddionic.TouchImageView.this
                    android.graphics.PointF r10 = r10.start
                    float r10 = r10.x
                    float r9 = r9 - r10
                    float r10 = r0.getY()
                    com.Young.reddionic.TouchImageView r11 = com.Young.reddionic.TouchImageView.this
                    android.graphics.PointF r11 = r11.start
                    float r11 = r11.y
                    float r10 = r10 - r11
                    r8.postTranslate(r9, r10)
                    goto L_0x000d
                L_0x02af:
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    int r8 = r8.mode
                    r9 = 2
                    if (r8 != r9) goto L_0x000d
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    float r1 = r8.spacing(r0)
                    java.lang.String r8 = "Touch"
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    java.lang.String r10 = "newDist="
                    r9.<init>(r10)
                    java.lang.StringBuilder r9 = r9.append(r1)
                    java.lang.String r9 = r9.toString()
                    android.util.Log.d(r8, r9)
                    r8 = 1092616192(0x41200000, float:10.0)
                    int r8 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
                    if (r8 <= 0) goto L_0x000d
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.matrix
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r9 = r9.savedMatrix
                    r8.set(r9)
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    float r8 = r8.oldDist
                    float r3 = r1 / r8
                    java.lang.String r8 = "scale"
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    int r10 = java.lang.Math.round(r3)
                    java.lang.String r10 = java.lang.String.valueOf(r10)
                    r9.<init>(r10)
                    java.lang.String r9 = r9.toString()
                    android.util.Log.e(r8, r9)
                    float r8 = com.Young.reddionic.TouchImageView.size_h
                    float r8 = r8 * r3
                    com.Young.reddionic.TouchImageView.size_h = r8
                    float r8 = com.Young.reddionic.TouchImageView.size_w
                    float r8 = r8 * r3
                    com.Young.reddionic.TouchImageView.size_w = r8
                    com.Young.reddionic.TouchImageView r8 = com.Young.reddionic.TouchImageView.this
                    android.graphics.Matrix r8 = r8.matrix
                    com.Young.reddionic.TouchImageView r9 = com.Young.reddionic.TouchImageView.this
                    android.graphics.PointF r9 = r9.mid
                    float r9 = r9.x
                    com.Young.reddionic.TouchImageView r10 = com.Young.reddionic.TouchImageView.this
                    android.graphics.PointF r10 = r10.mid
                    float r10 = r10.y
                    r8.postScale(r3, r3, r9, r10)
                    goto L_0x000d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.TouchImageView.AnonymousClass1.onTouch(android.view.View, android.view.MotionEvent):boolean");
            }
        });
    }

    public void setImage(Bitmap bm, int displayWidth, int displayHeight) {
        float scale;
        this.bitmap = bm;
        super.setImageBitmap(this.bitmap);
        if (displayHeight / bm.getHeight() >= displayWidth / bm.getWidth()) {
            scale = ((float) displayWidth) / ((float) bm.getWidth());
        } else {
            scale = ((float) displayHeight) / ((float) bm.getHeight());
        }
        size_h = ((float) bm.getHeight()) * scale;
        size_w = ((float) bm.getWidth()) * scale;
        Log.e("scale", new StringBuilder(String.valueOf(Math.round(scale))).toString());
        this.rect = new RectF(0.0f, 0.0f, (float) this.bitmap.getWidth(), (float) this.bitmap.getHeight());
        this.savedMatrix.set(this.matrix);
        this.matrix.set(this.savedMatrix);
        this.matrix.postScale(scale, scale, this.mid.x, this.mid.y);
        setImageMatrix(this.matrix);
        float redundantYSpace = ((float) displayHeight) - (((float) bm.getHeight()) * scale);
        float redundantXSpace = (((float) displayWidth) - (((float) bm.getWidth()) * scale)) / 2.0f;
        this.savedMatrix.set(this.matrix);
        this.matrix.set(this.savedMatrix);
        this.matrix.postTranslate(redundantXSpace, redundantYSpace / 2.0f);
        setImageMatrix(this.matrix);
    }

    private void dumpEvent(WrapMotionEvent event) {
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEventCompat.ACTION_MASK;
        sb.append("event ACTION_").append(new String[]{"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"}[actionCode]);
        if (actionCode == 5 || actionCode == 6) {
            sb.append("(pid ").append(action >> 8);
            sb.append(")");
        }
        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount()) {
                sb.append(";");
            }
        }
        sb.append("]");
        Log.d(TAG, sb.toString());
    }

    /* access modifiers changed from: private */
    public float spacing(WrapMotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt((x * x) + (y * y));
    }

    /* access modifiers changed from: private */
    public void midPoint(PointF point, WrapMotionEvent event) {
        point.set((event.getX(0) + event.getX(1)) / 2.0f, (event.getY(0) + event.getY(1)) / 2.0f);
    }
}
