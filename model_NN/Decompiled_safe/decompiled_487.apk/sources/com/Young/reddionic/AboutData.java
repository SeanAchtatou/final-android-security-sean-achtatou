package com.Young.reddionic;

public class AboutData {
    private int comment_karma;
    private String created;
    private String created_utc;
    private boolean has_mail;
    private boolean has_mod_mail;
    private String id;
    private boolean is_gold;
    private boolean is_mod;
    private int link_karma;
    private String modhash;
    private String name;

    public void setCreated_utc(String created_utc2) {
        this.created_utc = created_utc2;
    }

    public String getCreated_utc() {
        return this.created_utc;
    }

    public void setLink_karma(int link_karma2) {
        this.link_karma = link_karma2;
    }

    public int getLink_karma() {
        return this.link_karma;
    }

    public void setComment_karma(int comment_karma2) {
        this.comment_karma = comment_karma2;
    }

    public int getComment_karma() {
        return this.comment_karma;
    }

    public void setIs_gold(boolean is_gold2) {
        this.is_gold = is_gold2;
    }

    public boolean getIs_gold() {
        return this.is_gold;
    }

    public void setIs_mod(boolean is_mod2) {
        this.is_mod = is_mod2;
    }

    public boolean getIs_mod() {
        return this.is_mod;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getId() {
        return this.id;
    }

    public void setModhash(String modhash2) {
        this.modhash = modhash2;
    }

    public String getModhash() {
        return this.modhash;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setHas_mail(boolean has_mail2) {
        this.has_mail = has_mail2;
    }

    public boolean getHas_mail() {
        return this.has_mail;
    }

    public void setCreated(String created2) {
        this.created = created2;
    }

    public String getCreated() {
        return this.created;
    }

    public void setHas_mod_mail(boolean has_mod_mail2) {
        this.has_mod_mail = has_mod_mail2;
    }

    public boolean isHas_mod_mail() {
        return this.has_mod_mail;
    }
}
