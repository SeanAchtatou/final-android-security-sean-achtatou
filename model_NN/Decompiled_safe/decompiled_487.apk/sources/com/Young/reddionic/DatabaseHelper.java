package com.Young.reddionic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    static final String PostTable = "Post";
    static final String RedditTable = "Reddits";
    static final String RedditThreadTable = "Reddits_Thread";
    static final String colAfter = "After";
    static final String colBefore = "Before";
    static final String colName = "Name";
    static final String colNoComments = "NoComments";
    static final String colPoints = "Points";
    static final String colPostID = "PostId";
    static final String colReddit = "Reddit";
    static final String colRedditID = "RedditId";
    static final String colSelf = "Self";
    static final String colSite = "Site";
    static final String colTitle = "Title";
    static final String colUrl = "Url";
    static final String colsub = "Sub";
    static final String dbName = "postDB";
    static int movingCounter = 0;
    static final String viewPosts = "ViewPosts";
    static final String viewReddits = "ViewReddits";

    public DatabaseHelper(Context context) {
        super(context, dbName, (SQLiteDatabase.CursorFactory) null, 5);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Post (PostId INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT, NoComments Integer, Points Integer , Site TEXT, Reddit TEXT, Url TEXT, Self TEXT);");
        db.execSQL("CREATE TABLE Reddits (RedditId INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Sub Integer);");
        db.execSQL("CREATE TABLE Reddits_Thread (RedditId INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Before TEXT, After TEXT);");
    }

    public void insertRedditThread(int id, String name, String before, String after) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(colName, name);
        cv.put(colBefore, before);
        cv.put(colAfter, after);
        db.insert(RedditThreadTable, colRedditID, cv);
        db.close();
    }

    public void insertReddit(int id, String name, int sub) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(colName, name);
        cv.put(colsub, Integer.valueOf(sub));
        db.insert(RedditTable, colRedditID, cv);
        db.close();
    }

    public void updateReddit(int id, String name, int sub) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(colRedditID, Integer.valueOf(id));
        cv.put(colName, name);
        cv.put(colsub, Integer.valueOf(sub));
        db.update(RedditTable, cv, "RedditId=?", new String[]{String.valueOf(id)});
        db.close();
    }

    public void deleteRedditByID(int id) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cur = db.query(RedditTable, new String[]{colRedditID}, null, null, null, null, null);
        cur.moveToLast();
        moveReddit(id, cur.getInt(0));
        db.delete(RedditTable, "RedditId = " + cur.getInt(0), null);
        db.close();
    }

    public void deleteRedditByName(String name) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cur = db.query(RedditTable, new String[]{colRedditID}, "Name='" + name + "'", null, null, null, null);
        cur.moveToFirst();
        int first = cur.getInt(0);
        Cursor cur2 = db.query(RedditTable, new String[]{colRedditID}, null, null, null, null, null);
        cur2.moveToLast();
        int second = cur2.getInt(0);
        db.close();
        moveReddit(first - 1, second);
        SQLiteDatabase db2 = getWritableDatabase();
        db2.delete(RedditTable, "RedditId = " + second, null);
        db2.close();
    }

    public void moveReddit(int oldPos, int newPos) {
        int oldPos2 = oldPos + 1;
        int newPos2 = newPos + 1;
        SQLiteDatabase writableDatabase = getWritableDatabase();
        movingCounter = 0;
        if (newPos2 > oldPos2) {
            while (newPos2 - movingCounter != oldPos2) {
                switchReddit(newPos2 - movingCounter, oldPos2);
                movingCounter++;
            }
        } else if (oldPos2 > newPos2) {
            while (oldPos2 - movingCounter != newPos2) {
                switchReddit(movingCounter + newPos2, oldPos2);
                movingCounter++;
            }
        }
    }

    public void switchReddit(int first, int second) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            Cursor cur = db.query(RedditTable, new String[]{colRedditID, colName, colsub}, "RedditId='" + first + "'", null, null, null, null);
            cur.moveToFirst();
            ContentValues cv1 = new ContentValues();
            cv1.put(colName, cur.getString(1));
            cv1.put(colsub, Integer.valueOf(cur.getInt(2)));
            Cursor cur2 = db.query(RedditTable, new String[]{colRedditID, colName, colsub}, "RedditId='" + second + "'", null, null, null, null);
            cur2.moveToFirst();
            ContentValues cv2 = new ContentValues();
            cv2.put(colName, cur2.getString(1));
            cv2.put(colsub, Integer.valueOf(cur2.getInt(2)));
            db.update(RedditTable, cv1, "RedditId='" + second + "'", null);
            db.update(RedditTable, cv2, "RedditId='" + first + "'", null);
            db.close();
        } catch (CursorIndexOutOfBoundsException e) {
            movingCounter++;
            if (second > first) {
                switchReddit(movingCounter + first, second);
            } else {
                switchReddit(first - movingCounter, second);
            }
        }
    }

    public void clearSubReddit() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cur = db.query(RedditTable, new String[]{colRedditID, colName, colsub}, "Sub = 1", null, null, null, null);
        cur.moveToFirst();
        while (!cur.isLast()) {
            try {
                db.delete(RedditTable, "RedditId=?", new String[]{String.valueOf(cur.getInt(0))});
                cur.moveToNext();
            } catch (CursorIndexOutOfBoundsException e) {
            }
        }
        db.delete(RedditTable, "RedditId=?", new String[]{String.valueOf(cur.getInt(0))});
        cur.moveToNext();
        db.close();
    }

    public void dropSubReddit() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE Reddits");
        db.execSQL("CREATE TABLE Reddits (RedditId INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Sub Integer);");
        db.close();
    }

    public ArrayList<String> getSubReddit() {
        ArrayList<String> reddits = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor cur = db.query(RedditTable, new String[]{colRedditID, colName, colsub}, "Sub = 1", null, null, null, null);
        cur.moveToFirst();
        while (!cur.isLast()) {
            try {
                reddits.add(cur.getString(1));
                cur.moveToNext();
            } catch (CursorIndexOutOfBoundsException e) {
            }
        }
        reddits.add(cur.getString(1));
        cur.moveToNext();
        db.close();
        return reddits;
    }

    public boolean isReddit(String name) {
        SQLiteDatabase db = getWritableDatabase();
        if (db.query(RedditTable, new String[]{colName}, "Name = " + name, null, null, null, null).isAfterLast()) {
            db.close();
            return false;
        }
        db.close();
        return true;
    }

    /* access modifiers changed from: package-private */
    public Cursor getAllReddits() {
        return getReadableDatabase().query(RedditTable, new String[]{colRedditID, colName}, null, null, null, null, null);
    }

    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
    }
}
