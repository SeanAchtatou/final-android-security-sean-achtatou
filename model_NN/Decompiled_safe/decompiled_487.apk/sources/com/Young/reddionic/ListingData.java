package com.Young.reddionic;

public class ListingData {
    private String after;
    private String before;
    private ThingListing[] children;
    private String modhash;

    public void setAfter(String after2) {
        this.after = after2;
    }

    public String getAfter() {
        return this.after;
    }

    public void setBefore(String before2) {
        this.before = before2;
    }

    public String getBefore() {
        return this.before;
    }

    public void setModhash(String modhash2) {
        this.modhash = modhash2;
    }

    public String getModhash() {
        return this.modhash;
    }

    public void setChildren(ThingListing[] children2) {
        this.children = children2;
    }

    public ThingListing[] getChildren() {
        return this.children;
    }
}
