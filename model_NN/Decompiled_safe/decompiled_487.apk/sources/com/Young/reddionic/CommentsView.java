package com.Young.reddionic;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.Young.reddionic.Browse;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class CommentsView extends FragmentActivity implements Html.ImageGetter {
    static Activity act;
    static Context context;
    static Html.ImageGetter imageGetter;
    static int liked = 0;
    static boolean refreshing = false;
    static int score = -1;
    ActionBar actionbar;
    String author;
    LinearLayout button;
    boolean buttoning = false;
    ArrayList<String> checked = new ArrayList<>();
    CommentListAdapter cla;
    boolean click = false;
    getComment commentLoader;
    ArrayList<ThingInfo> comments = new ArrayList<>();
    int difference;
    int direction;
    DisplayMetrics dm;
    boolean feedbacking = false;
    View headerView;
    boolean hidden = false;
    public ImageLoader imageLoader;
    ArrayAdapter<String> includedArrayAdapter;
    RelativeLayout info;
    LinearLayout info_t;
    LinearLayout l_info;
    View link;
    boolean loggingin = false;
    boolean longclick = false;
    ListView lv;
    Browse.MyAdapter mAdapter;
    ViewPager mPager;
    String mReddit;
    private final RedditSettings mSettings = new RedditSettings();
    ArrayList<String> names = new ArrayList<>();
    boolean opened = false;
    boolean opened2 = false;
    int pos = 0;
    int position_2;
    String postID;
    LinearLayout post_info;
    boolean pressed = false;
    LinearLayout preview;
    ThingInfo previous;
    ThingInfo previous2;
    PopupWindow pw;
    LinearLayout reply;
    boolean replying = false;
    boolean saved = false;
    View theActionBar;
    int thread_count = 0;
    GoogleAnalyticsTracker tracker;
    Boolean wasPreview = false;
    WebView webView;
    float x1 = 0.0f;
    float x2 = 0.0f;
    float y1 = 0.0f;
    float y2 = 0.0f;

    public static class ViewHolder {
        public LinearLayout beforeRoot;
        public LinearLayout button;
        public ImageView divider;
        public TextView id;
        public ImageView indent;
        public ImageView level;
        public LinearLayout preview;
        public LinearLayout root;
        public TextView score;
        public TextView text;
        public TextView time;
        public LinearLayout top;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.commentslist);
        context = getApplicationContext();
        act = this;
        imageGetter = this;
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-17867595-4", this);
        this.tracker.trackPageView("/CommentView");
        View headerView2 = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.comments, (ViewGroup) null);
        this.commentLoader = new getComment();
        this.commentLoader.execute(getIntent().getStringExtra("id"));
        ImageButton submit = (ImageButton) headerView2.findViewById(R.id.submit);
        submit.setTag(getIntent().getStringExtra("name"));
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<String> args = new ArrayList<>();
                args.add((String) v.getTag());
                args.add(((TextView) ((LinearLayout) v.getParent()).findViewById(R.id.comment_text)).getText().toString());
                new Comment().execute(args);
                ((TextView) ((LinearLayout) v.getParent()).findViewById(R.id.comment_text)).setText("");
            }
        });
        this.imageLoader = new ImageLoader(context);
        this.post_info = (LinearLayout) headerView2.findViewById(R.id.post_info);
        this.post_info.requestFocus();
        ((TextView) this.post_info.findViewById(R.id.title)).setText(getIntent().getStringExtra("title"));
        this.author = getIntent().getStringExtra("author");
        ((TextView) this.post_info.findViewById(R.id.otherinfo)).setText(String.valueOf(this.author) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getIntent().getStringExtra("comments") + " comments");
        ThingInfo tagger = new ThingInfo();
        tagger.setUrl(getIntent().getStringExtra("url"));
        tagger.setThumbnail(getIntent().getStringExtra("thumbnail"));
        ((ImageView) this.post_info.findViewById(R.id.thumbnail)).setTag(tagger);
        this.imageLoader.DisplayImage(getIntent().getStringExtra("thumbnail"), context, act, (ImageView) this.post_info.findViewById(R.id.thumbnail), 0);
        ((ImageView) this.post_info.findViewById(R.id.thumbnail)).setOnClickListener(new View.OnClickListener() {
            /* Debug info: failed to restart local var, previous not found, register: 7 */
            public void onClick(View v) {
                if (CommentsView.this.getIntent().getStringExtra("url").contains("self")) {
                    return;
                }
                if (CommentsView.this.getIntent().getStringExtra("url").contains("market.android.com") || CommentsView.this.getIntent().getStringExtra("url").contains("youtube.com") || CommentsView.this.getIntent().getStringExtra("url").contains("youtu.be")) {
                    CommentsView.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(((ThingInfo) v.getTag()).getUrl())));
                    return;
                }
                CommentsView.this.startActivity(new Intent(CommentsView.act, Webview.class).putExtra("url", ((ThingInfo) v.getTag()).getUrl()).putExtra("title", CommentsView.this.getIntent().getStringExtra("title")).putExtra("points", new StringBuilder(String.valueOf(CommentsView.this.getIntent().getStringExtra("points"))).toString()).putExtra("comments", new StringBuilder(String.valueOf(CommentsView.this.getIntent().getStringExtra("comments"))).toString()).putExtra("likes", new StringBuilder(String.valueOf(CommentsView.this.getIntent().getStringExtra("likes"))).toString()).putExtra("modhash", new StringBuilder(String.valueOf(CommentsView.this.getIntent().getStringExtra("id"))).toString()).putExtra("name", new StringBuilder(String.valueOf(CommentsView.this.getIntent().getStringExtra("title"))).toString()).putExtra("thumbnail", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getThumbnail())).toString()).putExtra("subreddit", new StringBuilder(String.valueOf(((ThingInfo) v.getTag()).getSubreddit())).toString()));
            }
        });
        ImageView voteimg = (ImageView) this.post_info.findViewById(R.id.voteimg);
        score = Integer.parseInt(act.getIntent().getStringExtra("points"));
        ((TextView) this.post_info.findViewById(R.id.vote)).setText(new StringBuilder(String.valueOf(score)).toString());
        if (act.getIntent().getStringExtra("likes").equals("null")) {
            liked = 0;
            voteimg.setImageResource(R.drawable.noarrow);
        } else if (act.getIntent().getStringExtra("likes").equals("true")) {
            liked = 1;
            voteimg.setImageResource(R.drawable.uparrow);
        } else {
            liked = -1;
            voteimg.setImageResource(R.drawable.downarrow);
        }
        ((LinearLayout) this.post_info.findViewById(R.id.container)).setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(final View v) {
                v.performHapticFeedback(0);
                CommentsView.this.longclick = true;
                CommentsView.this.opened = true;
                CommentsView.this.info = (RelativeLayout) v.findViewById(R.id.info);
                CommentsView.this.button = (LinearLayout) v.findViewById(R.id.LinearLayout04);
                if (!CommentsView.this.click) {
                    CommentsView.this.info.setVisibility(0);
                    CommentsView.this.button.findViewById(R.id.LinearLayout04).setVisibility(8);
                }
                CommentsView.this.click = false;
                CommentsView.this.info.setVisibility(8);
                ((ImageView) CommentsView.this.button.findViewById(R.id.up)).setImageResource(R.drawable.upvote);
                ((ImageView) CommentsView.this.button.findViewById(R.id.star)).setImageResource(R.drawable.star_);
                ((ImageView) CommentsView.this.button.findViewById(R.id.reply)).setImageResource(R.drawable.hide_);
                ((ImageView) CommentsView.this.button.findViewById(R.id.share)).setImageResource(R.drawable.share_);
                ((ImageView) CommentsView.this.button.findViewById(R.id.down)).setImageResource(R.drawable.downvote);
                CommentsView.this.postID = CommentsView.this.getIntent().getStringExtra("name");
                CommentsView.this.mReddit = CommentsView.this.getIntent().getStringExtra("subreddit");
                CommentsView.this.button.setVisibility(0);
                v.findViewById(R.id.up).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v2) {
                        CommentsView.this.info.setVisibility(0);
                        CommentsView.this.button.setVisibility(8);
                        LinearLayout theView = (LinearLayout) v.getParent().getParent().getParent();
                        TextView vote = (TextView) theView.findViewById(R.id.vote);
                        ImageView voteimg = (ImageView) theView.findViewById(R.id.voteimg);
                        ArrayList<String> arg = new ArrayList<>();
                        View view = (View) CommentsView.this.button.getParent().getParent().getParent();
                        if (CommentsView.liked == 0) {
                            CommentsView.liked = 1;
                            CommentsView.score++;
                            arg.add(CommentsView.act.getIntent().getStringExtra("subreddit"));
                            arg.add(CommentsView.act.getIntent().getStringExtra("name"));
                            arg.add("1");
                            voteimg.setImageResource(R.drawable.uparrow);
                        } else if (CommentsView.liked == 1) {
                            CommentsView.score--;
                            arg.add(CommentsView.act.getIntent().getStringExtra("subreddit"));
                            arg.add(CommentsView.act.getIntent().getStringExtra("name"));
                            arg.add("0");
                            CommentsView.liked = 0;
                            voteimg.setImageResource(R.drawable.noarrow);
                        } else {
                            CommentsView.score += 2;
                            arg.add(CommentsView.act.getIntent().getStringExtra("subreddit"));
                            arg.add(CommentsView.act.getIntent().getStringExtra("name"));
                            arg.add("1");
                            CommentsView.liked = 1;
                            voteimg.setImageResource(R.drawable.uparrow);
                        }
                        new Voter().execute(arg);
                        vote.setText(new StringBuilder(String.valueOf(CommentsView.score)).toString());
                        CommentsView.this.click = true;
                        CommentsView.this.longclick = false;
                    }
                });
                v.findViewById(R.id.star).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v2) {
                        CommentsView.this.info.setVisibility(0);
                        CommentsView.this.button.setVisibility(8);
                        RedditAPI.save(CommentsView.this.postID.toString(), "save");
                        CommentsView.this.click = true;
                        CommentsView.this.longclick = false;
                    }
                });
                v.findViewById(R.id.reply).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v2) {
                        CommentsView.this.info.setVisibility(0);
                        CommentsView.this.button.setVisibility(8);
                        RedditAPI.hide(CommentsView.this.postID.toString(), "hide");
                        CommentsView.this.replying = true;
                        CommentsView.this.click = true;
                        CommentsView.this.longclick = false;
                    }
                });
                v.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v2) {
                        CommentsView.this.info.setVisibility(0);
                        CommentsView.this.button.setVisibility(8);
                        CommentsView.this.click = true;
                        CommentsView.this.longclick = false;
                    }
                });
                v.findViewById(R.id.down).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v2) {
                        CommentsView.this.info.setVisibility(0);
                        CommentsView.this.button.setVisibility(8);
                        LinearLayout theView = (LinearLayout) v.getParent().getParent().getParent();
                        TextView vote = (TextView) theView.findViewById(R.id.vote);
                        ImageView voteimg = (ImageView) theView.findViewById(R.id.voteimg);
                        ArrayList<String> arg = new ArrayList<>();
                        View view = (View) CommentsView.this.button.getParent().getParent().getParent();
                        if (CommentsView.liked == 0) {
                            CommentsView.liked = -1;
                            CommentsView.score--;
                            arg.add(CommentsView.act.getIntent().getStringExtra("subreddit"));
                            arg.add(CommentsView.act.getIntent().getStringExtra("name"));
                            arg.add("-1");
                            voteimg.setImageResource(R.drawable.downarrow);
                        } else if (CommentsView.liked == 1) {
                            CommentsView.score -= 2;
                            arg.add(CommentsView.act.getIntent().getStringExtra("subreddit"));
                            arg.add(CommentsView.act.getIntent().getStringExtra("name"));
                            arg.add("-1");
                            CommentsView.liked = -1;
                            voteimg.setImageResource(R.drawable.downarrow);
                        } else {
                            CommentsView.score++;
                            arg.add(CommentsView.act.getIntent().getStringExtra("subreddit"));
                            arg.add(CommentsView.act.getIntent().getStringExtra("name"));
                            arg.add("0");
                            CommentsView.liked = 0;
                            voteimg.setImageResource(R.drawable.noarrow);
                        }
                        new Voter().execute(arg);
                        vote.setText(new StringBuilder(String.valueOf(CommentsView.score)).toString());
                        CommentsView.this.click = true;
                        CommentsView.this.longclick = false;
                    }
                });
                return false;
            }
        });
        ArrayList<String> result = Utils.convertHtmlTags(Html.fromHtml(getIntent().getStringExtra("selftext")).toString(), 0);
        Spanned selftext = Html.fromHtml(result.get(0));
        if (!selftext.toString().equals("null")) {
            ((TextView) headerView2.findViewById(R.id.selftext)).setText((Spanned) selftext.subSequence(0, selftext.length() - 2), TextView.BufferType.SPANNABLE);
        } else {
            ((TextView) headerView2.findViewById(R.id.selftext)).setVisibility(8);
            this.link = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.link, (ViewGroup) null);
            String url = getIntent().getStringExtra("url");
            this.link.setTag(url);
            ((TextView) this.link.findViewById(R.id.label)).setText(url);
            ((LinearLayout) headerView2.findViewById(R.id.preview)).addView(this.link);
            if (url.contains(".png") || url.contains(".jpg") || url.contains(".gif")) {
                ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
            } else if (url.contains("imgur.com")) {
                ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
            } else if (url.contains("min.us")) {
                ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
            } else if (url.contains("youtube.com")) {
                ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.video);
            } else {
                ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.text);
            }
            this.link.setOnClickListener(new View.OnClickListener() {
                /* Debug info: failed to restart local var, previous not found, register: 13 */
                public void onClick(View v) {
                    WebView theWebView = (WebView) v.findViewById(R.id.previewWeb);
                    if (theWebView.getVisibility() == 8) {
                        theWebView.setVisibility(0);
                        theWebView.setWebViewClient(new HelloWebViewClient(CommentsView.this, null));
                        theWebView.getSettings().setJavaScriptEnabled(true);
                        theWebView.getSettings().setUserAgent(0);
                        theWebView.getSettings().setJavaScriptEnabled(true);
                        theWebView.getSettings().setAppCacheEnabled(true);
                        theWebView.getSettings().setDomStorageEnabled(true);
                        theWebView.getSettings().setAllowFileAccess(true);
                        theWebView.getSettings().setSupportMultipleWindows(true);
                        theWebView.getSettings().setCacheMode(1);
                        theWebView.getSettings().setAppCachePath("/sdcard/FolderName/.cache");
                        theWebView.setWebChromeClient(new WebChromeClient() {
                            public void onProgressChanged(WebView view, int progress) {
                                ProgressBar pb = (ProgressBar) ((RelativeLayout) view.getParent()).findViewById(R.id.progressBar1);
                                pb.setProgress(progress);
                                if (progress == 100) {
                                    pb.setVisibility(8);
                                } else if (pb.getVisibility() == 8 && progress < 100) {
                                    pb.setVisibility(0);
                                }
                            }
                        });
                        theWebView.setDownloadListener(new DownloadListener() {
                            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long size) {
                                Intent viewIntent = new Intent("android.intent.action.VIEW");
                                viewIntent.setDataAndType(Uri.parse(url), mimeType);
                                try {
                                    CommentsView.this.startActivity(viewIntent);
                                } catch (ActivityNotFoundException e) {
                                    Log.w("YourLogTag", "Couldn't find activity to view mimetype: " + mimeType);
                                }
                            }
                        });
                        String url = (String) v.getTag();
                        if (url.contains(".png") || url.contains(".jpg") || url.contains(".gif")) {
                            theWebView.loadDataWithBaseURL(url, "<img src=\"" + url + "\" width=\"100%\"/>", "text/html", "utf-8", url);
                            ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                        } else if (url.contains("imgur.com")) {
                            String newURL = String.valueOf(url.substring(0, url.indexOf("imgur"))) + "i." + url.substring(url.indexOf("imgur")) + ".png";
                            theWebView.loadDataWithBaseURL(newURL, "<img src=\"" + newURL + "\" width=\"100%\"/>", "text/html", "utf-8", newURL);
                            ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                        } else if (url.contains("min.us")) {
                            String newURL2 = String.valueOf(url.substring(0, 7)) + "k." + url.substring(7, 14) + "i" + url.substring(15, url.length()) + ".png";
                            theWebView.loadDataWithBaseURL(newURL2, "<img src=\"" + newURL2 + "\" width=\"100%\"/>", "text/html", "utf-8", newURL2);
                            ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                        } else if (url.contains("youtube.com") || url.contains("youtu.be")) {
                            int index1 = url.indexOf("?v=");
                            int index2 = url.indexOf("&", index1);
                            if (index2 != -1) {
                                try {
                                    theWebView.loadDataWithBaseURL(url, "<iframe width=\"100%\" height=\"200\" src=\"http://www.youtube.com/embed/" + url.substring(index1 + 3, index2) + "\" frameborder=\"0\" allowfullscreen></iframe>", "text/html", "utf-8", url);
                                } catch (Exception e) {
                                    return;
                                }
                            } else {
                                theWebView.loadDataWithBaseURL(url, "<iframe width=\"200\" src=\"http://www.youtube.com/embed/" + url.substring(index1 + 3) + "\" frameborder=\"0\" allowfullscreen></iframe>", "text/html", "utf-8", url);
                            }
                            ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.video);
                        } else {
                            theWebView.loadUrl(url);
                            ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.text);
                        }
                    } else {
                        theWebView.setVisibility(8);
                    }
                }
            });
        }
        if (result.size() > 1) {
            while (result.size() > 1) {
                this.link = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.link, (ViewGroup) null);
                ((TextView) this.link.findViewById(R.id.label)).setText(result.get(2));
                ((LinearLayout) headerView2.findViewById(R.id.preview)).addView(this.link);
                this.link.setTag(result.get(1));
                String url2 = result.get(1);
                if (url2.contains(".png") || url2.contains(".jpg") || url2.contains(".gif")) {
                    ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                } else if (url2.contains("imgur.com")) {
                    ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                } else if (url2.contains("min.us")) {
                    ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                } else if (url2.contains("youtube.com")) {
                    ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.video);
                } else {
                    ((ImageView) this.link.findViewById(R.id.icon)).setImageResource(R.drawable.text);
                }
                this.link.setOnClickListener(new View.OnClickListener() {
                    /* Debug info: failed to restart local var, previous not found, register: 13 */
                    public void onClick(View v) {
                        WebView theWebView = (WebView) v.findViewById(R.id.previewWeb);
                        if (theWebView.getVisibility() == 8) {
                            theWebView.setVisibility(0);
                            theWebView.setWebViewClient(new HelloWebViewClient(CommentsView.this, null));
                            theWebView.getSettings().setJavaScriptEnabled(true);
                            theWebView.getSettings().setUserAgent(0);
                            theWebView.getSettings().setJavaScriptEnabled(true);
                            theWebView.getSettings().setAppCacheEnabled(true);
                            theWebView.getSettings().setDomStorageEnabled(true);
                            theWebView.getSettings().setAllowFileAccess(true);
                            theWebView.getSettings().setSupportMultipleWindows(true);
                            theWebView.getSettings().setCacheMode(1);
                            theWebView.getSettings().setAppCachePath("/sdcard/FolderName/.cache");
                            theWebView.setWebChromeClient(new WebChromeClient() {
                                public void onProgressChanged(WebView view, int progress) {
                                    ProgressBar pb = (ProgressBar) ((RelativeLayout) view.getParent()).findViewById(R.id.progressBar1);
                                    pb.setProgress(progress);
                                    if (progress == 100) {
                                        pb.setVisibility(8);
                                    } else if (pb.getVisibility() == 8 && progress < 100) {
                                        pb.setVisibility(0);
                                    }
                                }
                            });
                            theWebView.setDownloadListener(new DownloadListener() {
                                public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long size) {
                                    Intent viewIntent = new Intent("android.intent.action.VIEW");
                                    viewIntent.setDataAndType(Uri.parse(url), mimeType);
                                    try {
                                        CommentsView.this.startActivity(viewIntent);
                                    } catch (ActivityNotFoundException e) {
                                        Log.w("YourLogTag", "Couldn't find activity to view mimetype: " + mimeType);
                                    }
                                }
                            });
                            String url = (String) v.getTag();
                            if (url.contains(".png") || url.contains(".jpg") || url.contains(".gif")) {
                                theWebView.loadDataWithBaseURL(url, "<img src=\"" + url + "\" width=\"100%\">", "text/html", "utf-8", url);
                                ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                            } else if (url.contains("imgur.com")) {
                                String newURL = String.valueOf(url.substring(0, url.indexOf("imgur"))) + "i." + url.substring(url.indexOf("imgur")) + ".png";
                                theWebView.loadDataWithBaseURL(newURL, "<img src=\"" + newURL + "\" width=\"100%\"", "text/html", "utf-8", newURL);
                                ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                            } else if (url.contains("min.us")) {
                                String newURL2 = String.valueOf(url.substring(0, 7)) + "k." + url.substring(7, 14) + "i" + url.substring(15, url.length()) + ".png";
                                theWebView.loadDataWithBaseURL(newURL2, "<img src=\"" + newURL2 + "\" width=\"100%\"", "text/html", "utf-8", newURL2);
                                ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.picture);
                            } else if (url.contains("youtube.com") || url.contains("youtu.be")) {
                                int index1 = url.indexOf("?v=");
                                int index2 = url.indexOf("&", index1);
                                if (index2 != -1) {
                                    try {
                                        theWebView.loadDataWithBaseURL(url, "<iframe width=\"100%\" height=\"200\" src=\"http://www.youtube.com/embed/" + url.substring(index1 + 3, index2) + "\" frameborder=\"0\" allowfullscreen></iframe>", "text/html", "utf-8", url);
                                    } catch (Exception e) {
                                        return;
                                    }
                                } else {
                                    theWebView.loadDataWithBaseURL(url, "<iframe width=\"200\" src=\"http://www.youtube.com/embed/" + url.substring(index1 + 3) + "\" frameborder=\"0\" allowfullscreen></iframe>", "text/html", "utf-8", url);
                                }
                                ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.video);
                            } else {
                                theWebView.loadUrl(url);
                                ((ImageView) v.findViewById(R.id.icon)).setImageResource(R.drawable.text);
                            }
                        } else {
                            theWebView.setVisibility(8);
                        }
                    }
                });
                result.remove(1);
                result.remove(1);
            }
        }
        this.lv = (ListView) findViewById(R.id.comments_list);
        this.cla = new CommentListAdapter(context, this.comments);
        ((ListView) findViewById(R.id.comments_list)).addHeaderView(headerView2);
        ((ListView) findViewById(R.id.comments_list)).setAdapter((ListAdapter) this.cla);
        ((ListView) findViewById(R.id.comments_list)).setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == 1 && !CommentsView.this.click) {
                    if (!(CommentsView.this.info == null || CommentsView.this.info.getVisibility() == 0)) {
                        CommentsView.this.info.setVisibility(0);
                    }
                    if (!(CommentsView.this.info_t == null || CommentsView.this.info_t.getVisibility() == 0)) {
                        CommentsView.this.info_t.setVisibility(0);
                    }
                    if (!(CommentsView.this.preview == null || CommentsView.this.preview.getVisibility() == 0)) {
                        CommentsView.this.preview.setVisibility(0);
                    }
                    CommentsView.this.longclick = false;
                    CommentsView.this.click = true;
                }
            }
        });
        CookieSyncManager.createInstance(getApplicationContext());
        this.mSettings.loadRedditPreferences(getApplicationContext());
    }

    final class CommentListAdapter extends ArrayAdapter<ThingInfo> {
        private LayoutInflater mInflater;
        public List<ThingInfo> obj;

        public CommentListAdapter(Context context, List<ThingInfo> objects) {
            super(context, 0, objects);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            CommentsView.this.imageLoader = new ImageLoader(context);
            this.obj = objects;
        }

        /* JADX WARN: Type inference failed for: r11v2, types: [java.lang.CharSequence] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.view.View getView(int r18, android.view.View r19, android.view.ViewGroup r20) {
            /*
                r17 = this;
                r12 = r19
                r0 = r17
                java.util.List<com.Young.reddionic.ThingInfo> r0 = r0.obj
                r13 = r0
                r0 = r13
                r1 = r18
                java.lang.Object r7 = r0.get(r1)
                com.Young.reddionic.ThingInfo r7 = (com.Young.reddionic.ThingInfo) r7
                if (r19 != 0) goto L_0x01f8
                com.Young.reddionic.CommentsView$ViewHolder r6 = new com.Young.reddionic.CommentsView$ViewHolder
                r6.<init>()
                r0 = r17
                android.view.LayoutInflater r0 = r0.mInflater
                r13 = r0
                r14 = 2130903046(0x7f030006, float:1.7412899E38)
                r15 = 0
                android.view.View r12 = r13.inflate(r14, r15)
                r13 = 2131034159(0x7f05002f, float:1.7678828E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.ImageView r19 = (android.widget.ImageView) r19
                r0 = r19
                r1 = r6
                r1.level = r0
                r13 = 2131034160(0x7f050030, float:1.767883E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.TextView r19 = (android.widget.TextView) r19
                r0 = r19
                r1 = r6
                r1.id = r0
                r13 = 2131034161(0x7f050031, float:1.7678832E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.TextView r19 = (android.widget.TextView) r19
                r0 = r19
                r1 = r6
                r1.score = r0
                r13 = 2131034162(0x7f050032, float:1.7678834E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.TextView r19 = (android.widget.TextView) r19
                r0 = r19
                r1 = r6
                r1.time = r0
                r13 = 2131034163(0x7f050033, float:1.7678836E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.TextView r19 = (android.widget.TextView) r19
                r0 = r19
                r1 = r6
                r1.text = r0
                r13 = 2131034164(0x7f050034, float:1.7678838E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.LinearLayout r19 = (android.widget.LinearLayout) r19
                r0 = r19
                r1 = r6
                r1.preview = r0
                r13 = 2131034158(0x7f05002e, float:1.7678826E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.LinearLayout r19 = (android.widget.LinearLayout) r19
                r0 = r19
                r1 = r6
                r1.top = r0
                r13 = 2131034154(0x7f05002a, float:1.7678818E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.LinearLayout r19 = (android.widget.LinearLayout) r19
                r0 = r19
                r1 = r6
                r1.root = r0
                r13 = 2131034155(0x7f05002b, float:1.767882E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.ImageView r19 = (android.widget.ImageView) r19
                r0 = r19
                r1 = r6
                r1.indent = r0
                r13 = 2131034156(0x7f05002c, float:1.7678822E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.LinearLayout r19 = (android.widget.LinearLayout) r19
                r0 = r19
                r1 = r6
                r1.beforeRoot = r0
                r13 = 2131034168(0x7f050038, float:1.7678846E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.LinearLayout r19 = (android.widget.LinearLayout) r19
                r0 = r19
                r1 = r6
                r1.button = r0
                r13 = 2131034157(0x7f05002d, float:1.7678824E38)
                android.view.View r19 = r12.findViewById(r13)
                android.widget.ImageView r19 = (android.widget.ImageView) r19
                r0 = r19
                r1 = r6
                r1.divider = r0
                r12.setTag(r6)
            L_0x00cf:
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                r0 = r18
                r1 = r13
                r1.position_2 = r0
                boolean r13 = r7.isLastComment()
                if (r13 != 0) goto L_0x04fe
                android.widget.LinearLayout r13 = r6.beforeRoot
                r14 = 0
                r13.setVisibility(r14)
                android.widget.ImageView r13 = r6.divider
                r14 = 0
                r13.setVisibility(r14)
                android.widget.LinearLayout r13 = r6.top     // Catch:{ Exception -> 0x0208 }
                r14 = 0
                r13.setVisibility(r14)     // Catch:{ Exception -> 0x0208 }
                android.widget.TextView r13 = r6.id     // Catch:{ Exception -> 0x0208 }
                java.lang.String r14 = r7.getAuthor()     // Catch:{ Exception -> 0x0208 }
                r13.setText(r14)     // Catch:{ Exception -> 0x0208 }
                java.lang.String r13 = r7.getAuthor()     // Catch:{ Exception -> 0x0208 }
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this     // Catch:{ Exception -> 0x0208 }
                r14 = r0
                java.lang.String r14 = r14.author     // Catch:{ Exception -> 0x0208 }
                boolean r13 = r13.equals(r14)     // Catch:{ Exception -> 0x0208 }
                if (r13 == 0) goto L_0x0200
                android.widget.LinearLayout r13 = r6.top     // Catch:{ Exception -> 0x0208 }
                r14 = 255(0xff, float:3.57E-43)
                r15 = 226(0xe2, float:3.17E-43)
                r16 = 153(0x99, float:2.14E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)     // Catch:{ Exception -> 0x0208 }
                r13.setBackgroundColor(r14)     // Catch:{ Exception -> 0x0208 }
            L_0x011a:
                java.lang.String r13 = "text"
                java.lang.String r14 = r7.getBody_html()
                android.text.Spanned r14 = android.text.Html.fromHtml(r14)
                java.lang.String r14 = r14.toString()
                android.util.Log.e(r13, r14)
                java.lang.String r13 = r7.getBody_html()
                android.text.Spanned r13 = android.text.Html.fromHtml(r13)
                java.lang.String r13 = r13.toString()
                r14 = 0
                java.util.ArrayList r9 = com.Young.reddionic.Utils.convertHtmlTags(r13, r14)
                r13 = 0
                java.lang.Object r18 = r9.get(r13)
                java.lang.String r18 = (java.lang.String) r18
                android.text.Html$ImageGetter r13 = com.Young.reddionic.CommentsView.imageGetter
                r14 = 0
                r0 = r18
                r1 = r13
                r2 = r14
                android.text.Spanned r10 = android.text.Html.fromHtml(r0, r1, r2)
                java.lang.String r13 = "text"
                java.lang.String r14 = r10.toString()
                android.util.Log.e(r13, r14)
                r13 = 0
                int r14 = r10.length()     // Catch:{ Exception -> 0x0213 }
                r15 = 2
                int r14 = r14 - r15
                java.lang.CharSequence r11 = r10.subSequence(r13, r14)     // Catch:{ Exception -> 0x0213 }
                r0 = r11
                android.text.Spanned r0 = (android.text.Spanned) r0     // Catch:{ Exception -> 0x0213 }
                r10 = r0
            L_0x0166:
                android.widget.TextView r13 = r6.text
                android.widget.TextView$BufferType r14 = android.widget.TextView.BufferType.SPANNABLE
                r13.setText(r10, r14)
                int r13 = r9.size()
                r14 = 1
                if (r13 != r14) goto L_0x0220
                android.widget.LinearLayout r13 = r6.preview
                int r13 = r13.getVisibility()
                if (r13 != 0) goto L_0x0220
                android.widget.LinearLayout r13 = r6.preview
                r14 = 8
                r13.setVisibility(r14)
            L_0x0183:
                android.widget.TextView r13 = r6.score     // Catch:{ Exception -> 0x03ae }
                java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03ae }
                int r15 = r7.getUps()     // Catch:{ Exception -> 0x03ae }
                int r16 = r7.getDowns()     // Catch:{ Exception -> 0x03ae }
                int r15 = r15 - r16
                java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ Exception -> 0x03ae }
                r14.<init>(r15)     // Catch:{ Exception -> 0x03ae }
                java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x03ae }
                r13.setText(r14)     // Catch:{ Exception -> 0x03ae }
                android.widget.TextView r13 = r6.time     // Catch:{ Exception -> 0x03ae }
                java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03ae }
                double r15 = r7.getCreated_utc()     // Catch:{ Exception -> 0x03ae }
                java.lang.String r15 = com.Young.reddionic.Utils.getTimeAgo2(r15)     // Catch:{ Exception -> 0x03ae }
                java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ Exception -> 0x03ae }
                r14.<init>(r15)     // Catch:{ Exception -> 0x03ae }
                java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x03ae }
                r13.setText(r14)     // Catch:{ Exception -> 0x03ae }
            L_0x01b9:
                int r13 = r7.getIndent()
                switch(r13) {
                    case 0: goto L_0x03d6;
                    case 1: goto L_0x03ed;
                    case 2: goto L_0x0406;
                    case 3: goto L_0x041f;
                    case 4: goto L_0x0438;
                    case 5: goto L_0x0451;
                    case 6: goto L_0x046a;
                    case 7: goto L_0x0483;
                    case 8: goto L_0x049c;
                    case 9: goto L_0x04b5;
                    case 10: goto L_0x04ce;
                    default: goto L_0x01c0;
                }
            L_0x01c0:
                boolean r13 = r7.isReplyOpened()
                if (r13 == 0) goto L_0x04e7
                r13 = 2131034165(0x7f050035, float:1.767884E38)
                android.view.View r13 = r12.findViewById(r13)
                r14 = 0
                r13.setVisibility(r14)
            L_0x01d1:
                boolean r13 = r7.isButtonOpened()
                if (r13 == 0) goto L_0x04f5
                android.widget.LinearLayout r13 = r6.button
                r14 = 0
                r13.setVisibility(r14)
            L_0x01dd:
                android.widget.LinearLayout r13 = r6.beforeRoot
                r13.setTag(r7)
                android.widget.LinearLayout r13 = r6.beforeRoot
                r14 = 1
                r13.setLongClickable(r14)
                android.widget.LinearLayout r13 = r6.beforeRoot
                com.Young.reddionic.CommentsView$CommentListAdapter$2 r14 = new com.Young.reddionic.CommentsView$CommentListAdapter$2
                r0 = r14
                r1 = r17
                r2 = r7
                r3 = r6
                r0.<init>(r2, r3)
                r13.setOnLongClickListener(r14)
            L_0x01f7:
                return r12
            L_0x01f8:
                java.lang.Object r6 = r12.getTag()
                com.Young.reddionic.CommentsView$ViewHolder r6 = (com.Young.reddionic.CommentsView.ViewHolder) r6
                goto L_0x00cf
            L_0x0200:
                android.widget.LinearLayout r13 = r6.top     // Catch:{ Exception -> 0x0208 }
                r14 = 0
                r13.setBackgroundResource(r14)     // Catch:{ Exception -> 0x0208 }
                goto L_0x011a
            L_0x0208:
                r13 = move-exception
                r5 = r13
                android.widget.TextView r13 = r6.id
                java.lang.String r14 = "null"
                r13.setText(r14)
                goto L_0x011a
            L_0x0213:
                r13 = move-exception
                r5 = r13
                java.lang.String r13 = "Parsing Text"
                java.lang.String r14 = r5.toString()
                android.util.Log.e(r13, r14)
                goto L_0x0166
            L_0x0220:
                android.widget.LinearLayout r13 = r6.preview
                int r13 = r13.getVisibility()
                r14 = 8
                if (r13 != r14) goto L_0x0230
                android.widget.LinearLayout r13 = r6.preview
                r14 = 0
                r13.setVisibility(r14)
            L_0x0230:
                android.widget.LinearLayout r13 = r6.preview
                r13.removeAllViews()
            L_0x0235:
                int r13 = r9.size()
                r14 = 1
                if (r13 <= r14) goto L_0x0183
                android.content.Context r13 = com.Young.reddionic.CommentsView.context
                java.lang.String r14 = "layout_inflater"
                java.lang.Object r8 = r13.getSystemService(r14)
                android.view.LayoutInflater r8 = (android.view.LayoutInflater) r8
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                r14 = 2130903056(0x7f030010, float:1.741292E38)
                r15 = 0
                android.view.View r14 = r8.inflate(r14, r15)
                r13.headerView = r14
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 2131034213(0x7f050065, float:1.7678937E38)
                android.view.View r18 = r13.findViewById(r14)
                android.widget.TextView r18 = (android.widget.TextView) r18
                r13 = 2
                java.lang.Object r19 = r9.get(r13)
                java.lang.CharSequence r19 = (java.lang.CharSequence) r19
                r18.setText(r19)
                android.widget.LinearLayout r13 = r6.preview
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r14 = r0
                android.view.View r14 = r14.headerView
                r13.addView(r14)
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 1
                java.lang.Object r14 = r9.get(r14)
                r13.setTag(r14)
                r13 = 1
                java.lang.Object r11 = r9.get(r13)
                java.lang.String r11 = (java.lang.String) r11
                java.lang.String r13 = ".png"
                boolean r13 = r11.contains(r13)
                if (r13 != 0) goto L_0x02a9
                java.lang.String r13 = ".jpg"
                boolean r13 = r11.contains(r13)
                if (r13 != 0) goto L_0x02a9
                java.lang.String r13 = ".gif"
                boolean r13 = r11.contains(r13)
                if (r13 == 0) goto L_0x02de
            L_0x02a9:
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 2131034153(0x7f050029, float:1.7678816E38)
                android.view.View r18 = r13.findViewById(r14)
                android.widget.ImageView r18 = (android.widget.ImageView) r18
                r13 = 2130837598(0x7f02005e, float:1.7280155E38)
                r0 = r18
                r1 = r13
                r0.setImageResource(r1)
            L_0x02c2:
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                com.Young.reddionic.CommentsView$CommentListAdapter$1 r14 = new com.Young.reddionic.CommentsView$CommentListAdapter$1
                r0 = r14
                r1 = r17
                r0.<init>()
                r13.setOnClickListener(r14)
                r13 = 1
                r9.remove(r13)
                r13 = 1
                r9.remove(r13)
                goto L_0x0235
            L_0x02de:
                java.lang.String r13 = "imgur.com"
                boolean r13 = r11.contains(r13)
                if (r13 == 0) goto L_0x0300
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 2131034153(0x7f050029, float:1.7678816E38)
                android.view.View r18 = r13.findViewById(r14)
                android.widget.ImageView r18 = (android.widget.ImageView) r18
                r13 = 2130837598(0x7f02005e, float:1.7280155E38)
                r0 = r18
                r1 = r13
                r0.setImageResource(r1)
                goto L_0x02c2
            L_0x0300:
                java.lang.String r13 = "min.us"
                boolean r13 = r11.contains(r13)
                if (r13 == 0) goto L_0x0322
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 2131034153(0x7f050029, float:1.7678816E38)
                android.view.View r18 = r13.findViewById(r14)
                android.widget.ImageView r18 = (android.widget.ImageView) r18
                r13 = 2130837598(0x7f02005e, float:1.7280155E38)
                r0 = r18
                r1 = r13
                r0.setImageResource(r1)
                goto L_0x02c2
            L_0x0322:
                java.lang.String r13 = "youtube.com"
                boolean r13 = r11.contains(r13)
                if (r13 != 0) goto L_0x0332
                java.lang.String r13 = "youtu.be"
                boolean r13 = r11.contains(r13)
                if (r13 == 0) goto L_0x034d
            L_0x0332:
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 2131034153(0x7f050029, float:1.7678816E38)
                android.view.View r18 = r13.findViewById(r14)
                android.widget.ImageView r18 = (android.widget.ImageView) r18
                r13 = 2130837631(0x7f02007f, float:1.7280222E38)
                r0 = r18
                r1 = r13
                r0.setImageResource(r1)
                goto L_0x02c2
            L_0x034d:
                java.lang.String r13 = "market.android.com"
                boolean r13 = r11.contains(r13)
                if (r13 == 0) goto L_0x0370
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 2131034153(0x7f050029, float:1.7678816E38)
                android.view.View r18 = r13.findViewById(r14)
                android.widget.ImageView r18 = (android.widget.ImageView) r18
                r13 = 2130837588(0x7f020054, float:1.7280134E38)
                r0 = r18
                r1 = r13
                r0.setImageResource(r1)
                goto L_0x02c2
            L_0x0370:
                java.lang.String r13 = "reddit.com"
                boolean r13 = r11.contains(r13)
                if (r13 == 0) goto L_0x0393
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 2131034153(0x7f050029, float:1.7678816E38)
                android.view.View r18 = r13.findViewById(r14)
                android.widget.ImageView r18 = (android.widget.ImageView) r18
                r13 = 2130837605(0x7f020065, float:1.7280169E38)
                r0 = r18
                r1 = r13
                r0.setImageResource(r1)
                goto L_0x02c2
            L_0x0393:
                r0 = r17
                com.Young.reddionic.CommentsView r0 = com.Young.reddionic.CommentsView.this
                r13 = r0
                android.view.View r13 = r13.headerView
                r14 = 2131034153(0x7f050029, float:1.7678816E38)
                android.view.View r18 = r13.findViewById(r14)
                android.widget.ImageView r18 = (android.widget.ImageView) r18
                r13 = 2130837620(0x7f020074, float:1.72802E38)
                r0 = r18
                r1 = r13
                r0.setImageResource(r1)
                goto L_0x02c2
            L_0x03ae:
                r13 = move-exception
                r5 = r13
                java.lang.String r13 = "Error"
                java.lang.String r14 = r5.toString()
                android.util.Log.e(r13, r14)
                java.lang.String r13 = "Error"
                java.lang.StackTraceElement[] r14 = r5.getStackTrace()
                java.lang.String r14 = r14.toString()
                android.util.Log.e(r13, r14)
                android.widget.TextView r13 = r6.score
                java.lang.String r14 = "null"
                r13.setText(r14)
                android.widget.TextView r13 = r6.time
                java.lang.String r14 = "null"
                r13.setText(r14)
                goto L_0x01b9
            L_0x03d6:
                android.widget.ImageView r13 = r6.indent
                r14 = 0
                r13.setImageDrawable(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x03ed:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837565(0x7f02003d, float:1.7280088E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x0406:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837567(0x7f02003f, float:1.7280092E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x041f:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837568(0x7f020040, float:1.7280094E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x0438:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837569(0x7f020041, float:1.7280096E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x0451:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837570(0x7f020042, float:1.7280098E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x046a:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837571(0x7f020043, float:1.72801E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x0483:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837572(0x7f020044, float:1.7280102E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x049c:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837573(0x7f020045, float:1.7280104E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x04b5:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837574(0x7f020046, float:1.7280106E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x04ce:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837566(0x7f02003e, float:1.728009E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01c0
            L_0x04e7:
                r13 = 2131034165(0x7f050035, float:1.767884E38)
                android.view.View r13 = r12.findViewById(r13)
                r14 = 8
                r13.setVisibility(r14)
                goto L_0x01d1
            L_0x04f5:
                android.widget.LinearLayout r13 = r6.button
                r14 = 8
                r13.setVisibility(r14)
                goto L_0x01dd
            L_0x04fe:
                android.widget.TextView r13 = r6.score
                java.lang.String r14 = ""
                r13.setText(r14)
                android.widget.TextView r13 = r6.id
                java.lang.String r14 = ""
                r13.setText(r14)
                android.widget.TextView r13 = r6.time
                java.lang.String r14 = ""
                r13.setText(r14)
                android.widget.TextView r13 = r6.text
                java.lang.String r14 = ""
                r13.setText(r14)
                android.widget.LinearLayout r13 = r6.preview
                r14 = 8
                r13.setVisibility(r14)
                android.widget.LinearLayout r13 = r6.top
                r14 = 8
                r13.setVisibility(r14)
                android.widget.LinearLayout r13 = r6.beforeRoot
                r14 = 8
                r13.setVisibility(r14)
                android.widget.ImageView r13 = r6.divider
                r14 = 8
                r13.setVisibility(r14)
                android.widget.LinearLayout r13 = r6.button
                r14 = 8
                r13.setVisibility(r14)
                int r13 = r7.getIndent()
                switch(r13) {
                    case 0: goto L_0x0546;
                    case 1: goto L_0x0565;
                    case 2: goto L_0x057e;
                    case 3: goto L_0x0597;
                    case 4: goto L_0x05b0;
                    case 5: goto L_0x05c9;
                    case 6: goto L_0x05e2;
                    case 7: goto L_0x05fb;
                    case 8: goto L_0x0614;
                    case 9: goto L_0x062d;
                    case 10: goto L_0x0646;
                    default: goto L_0x0544;
                }
            L_0x0544:
                goto L_0x01f7
            L_0x0546:
                android.widget.ImageView r13 = r6.indent
                r14 = 0
                r13.setImageDrawable(r14)
                android.widget.TextView r13 = r6.text
                android.view.ViewGroup$LayoutParams r4 = r13.getLayoutParams()
                android.widget.LinearLayout$LayoutParams r4 = (android.widget.LinearLayout.LayoutParams) r4
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x0565:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837565(0x7f02003d, float:1.7280088E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x057e:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837567(0x7f02003f, float:1.7280092E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x0597:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837568(0x7f020040, float:1.7280094E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x05b0:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837569(0x7f020041, float:1.7280096E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x05c9:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837570(0x7f020042, float:1.7280098E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x05e2:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837571(0x7f020043, float:1.72801E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x05fb:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837572(0x7f020044, float:1.7280102E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x0614:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837573(0x7f020045, float:1.7280104E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x062d:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837574(0x7f020046, float:1.7280106E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 238(0xee, float:3.34E-43)
                r15 = 238(0xee, float:3.34E-43)
                r16 = 238(0xee, float:3.34E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            L_0x0646:
                android.widget.ImageView r13 = r6.indent
                r14 = 2130837566(0x7f02003e, float:1.728009E38)
                r13.setImageResource(r14)
                android.widget.LinearLayout r13 = r6.root
                r14 = 223(0xdf, float:3.12E-43)
                r15 = 223(0xdf, float:3.12E-43)
                r16 = 223(0xdf, float:3.12E-43)
                int r14 = android.graphics.Color.rgb(r14, r15, r16)
                r13.setBackgroundColor(r14)
                goto L_0x01f7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.Young.reddionic.CommentsView.CommentListAdapter.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
        }
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(CommentsView commentsView, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished() {
        }
    }

    public void onBackPressed() {
        Log.e("HERE", "opened:" + this.opened + " opened2:" + this.opened2 + " replying:" + this.replying);
        if (this.opened) {
            ((RelativeLayout) this.post_info.findViewById(R.id.info)).setVisibility(0);
            ((LinearLayout) this.post_info.findViewById(R.id.LinearLayout04)).setVisibility(8);
            this.opened = false;
        } else if (this.opened2) {
            if (this.info_t != null) {
                this.info_t.setVisibility(0);
            }
            if (this.button != null) {
                this.button.setVisibility(8);
            }
            if (this.preview != null) {
                this.preview.setVisibility(0);
            }
            this.opened2 = false;
        } else if (this.buttoning) {
            this.button.setVisibility(8);
            ((ThingInfo) this.button.getTag()).setReplyOpened(false);
            this.buttoning = false;
        } else if (this.replying) {
            this.reply.setVisibility(8);
            ((ThingInfo) this.reply.getTag()).setReplyOpened(false);
            this.replying = false;
        } else if (this.feedbacking) {
            this.pw.dismiss();
            this.feedbacking = false;
        } else {
            super.onBackPressed();
        }
    }

    public void startLoading() {
        this.thread_count++;
        findViewById(R.id.refresh).setVisibility(8);
        findViewById(R.id.progressBar1).setVisibility(0);
    }

    public void stopLoading() {
        this.thread_count--;
        if (this.thread_count == 0) {
            findViewById(R.id.progressBar1).setVisibility(8);
            findViewById(R.id.refresh).setVisibility(0);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    class FeedbackAdapter extends ArrayAdapter<String> {
        FeedbackAdapter() {
            super(CommentsView.context, (int) R.layout.included_list_item, CommentsView.this.names);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = CommentsView.this.getLayoutInflater().inflate((int) R.layout.included_list_item, parent, false);
            }
            TextView label = (TextView) row.findViewById(R.id.label);
            label.setText(CommentsView.this.names.get(position));
            label.setTag(Integer.valueOf(position));
            CheckBox checkbox = (CheckBox) row.findViewById(R.id.check);
            if (CommentsView.this.checked.get(position).equals("")) {
                checkbox.setChecked(false);
            } else {
                checkbox.setChecked(true);
            }
            checkbox.setTag(Integer.valueOf(position));
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* Debug info: failed to restart local var, previous not found, register: 3 */
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        CommentsView.this.checked.set(((Integer) buttonView.getTag()).intValue(), "checked");
                    } else {
                        CommentsView.this.checked.set(((Integer) buttonView.getTag()).intValue(), "");
                    }
                }
            });
            return row;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.feedback /*2131034193*/:
                View popup = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.r_feedback_popup, (ViewGroup) null, false);
                this.pw = new PopupWindow(popup, -1, -1, true);
                if (this.names.size() == 0) {
                    this.names.add("Date of the first run");
                    this.names.add("Version Number");
                    this.names.add("Reddit ID");
                    this.names.add("Current View");
                    this.names.add("Device Name");
                    this.names.add("Device SDK");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                } else {
                    for (int x = 0; x < this.checked.size(); x++) {
                        this.checked.set(x, "checked");
                    }
                }
                this.pw.setBackgroundDrawable(new BitmapDrawable());
                this.pw.showAtLocation(findViewById(R.id.comments_root), 0, 0, 0);
                ((LinearLayout) popup.findViewById(R.id.requestButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.request_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView1)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Request");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.request_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.improvementButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.improvement_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView2)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Improvement");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.improvement_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.bugButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.bug_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView3)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Bug");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.bug_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                this.feedbacking = true;
                return true;
            case R.id.donate /*2131034194*/:
                Toast.makeText(context, "Almost implemented yet", 1).show();
                return true;
            default:
                return false;
        }
    }

    public void submitClick(View button2) {
        ArrayList<Object> arguments = (ArrayList) button2.getTag();
        arguments.add(((EditText) ((RelativeLayout) button2.getParent()).findViewById(R.id.text)).getText().toString());
        if (this.checked.get(0).equals("checked")) {
            arguments.add(this.mSettings.firstRunDate);
        } else {
            arguments.add("");
        }
        try {
            if (this.checked.get(1).equals("checked")) {
                arguments.add(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
            } else {
                arguments.add("");
            }
        } catch (PackageManager.NameNotFoundException e) {
            arguments.add("");
            e.printStackTrace();
        }
        if (this.checked.get(2).equals("checked")) {
            arguments.add(this.mSettings.username);
        } else {
            arguments.add("");
        }
        if (this.checked.get(3).equals("checked")) {
            View t = findViewById(R.id.comments_root);
            t.setDrawingCacheEnabled(true);
            t.destroyDrawingCache();
            arguments.add(t.getDrawingCache());
        } else {
            arguments.add("");
        }
        if (this.checked.get(4).equals("checked")) {
            arguments.add(Build.PRODUCT);
        } else {
            arguments.add("");
        }
        if (this.checked.get(5).equals("checked")) {
            arguments.add(Build.VERSION.RELEASE);
        } else {
            arguments.add("");
        }
        new Feedback().execute(arguments);
        this.pw.dismiss();
    }

    class Feedback extends AsyncTask<ArrayList<Object>, Void, Boolean> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<Object>[]) ((ArrayList[]) objArr));
        }

        Feedback() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(ArrayList<Object>... passing) {
            return Boolean.valueOf(RedditAPI.feedback(passing[0]));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            CommentsView.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            CommentsView.this.stopLoading();
            if (result.booleanValue()) {
                Toast.makeText(CommentsView.context, "Feedback Submitted!\nThank you for your feedback! ", 1).show();
            } else {
                Toast.makeText(CommentsView.context, "Feedback Failed!", 1).show();
            }
        }
    }

    class Comment extends AsyncTask<ArrayList<String>, Void, Boolean> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<String>[]) ((ArrayList[]) objArr));
        }

        Comment() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(ArrayList<String>... passing) {
            return Boolean.valueOf(RedditAPI.comment(passing[0].get(0), passing[0].get(1)));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            CommentsView.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            CommentsView.this.stopLoading();
            if (result.booleanValue()) {
                Toast.makeText(CommentsView.context, "Successful", 1).show();
            } else {
                Toast.makeText(CommentsView.context, "Failed", 1).show();
            }
        }
    }

    class getComment extends AsyncTask<String, Void, Integer> {
        getComment() {
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(String... passing) {
            RedditAPI.getComment(passing[0], "", 0);
            Iterator<ThingInfo> it = CommentsView.this.comments.iterator();
            while (it.hasNext()) {
                ThingInfo next = it.next();
            }
            return 1;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            CommentsView.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer result) {
            CommentsView.this.stopLoading();
            CommentsView.this.comments.clear();
            CommentsView.this.comments.addAll(RedditAPI.commentInfos);
            RedditAPI.commentInfos.clear();
            CommentsView.this.cla.notifyDataSetChanged();
        }
    }

    class Voter extends AsyncTask<ArrayList<String>, Void, Void> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<String>[]) ((ArrayList[]) objArr));
        }

        Voter() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(ArrayList<String>... arg0) {
            RedditAPI.vote(arg0[0].get(0), arg0[0].get(1), Integer.parseInt(arg0[0].get(2)));
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
        }
    }

    public Drawable getDrawable(String source) {
        Drawable d = getResources().getDrawable(R.drawable.market);
        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
        return d;
    }
}
