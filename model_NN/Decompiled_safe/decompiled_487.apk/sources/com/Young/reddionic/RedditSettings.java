package com.Young.reddionic;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.webkit.CookieSyncManager;
import java.util.Date;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;

public class RedditSettings {
    private static final String TAG = "RedditSettings";
    boolean alwaysShowNextPrevious = true;
    String commentsSortByUrl = "sort=confidence";
    boolean confirmQuit = true;
    boolean firstRun = true;
    String firstRunDate = null;
    String homepage = "reddit front page";
    boolean loadThumbnails = true;
    boolean loadThumbnailsOnlyWifi = false;
    String mailNotificationService = "MAIL_NOTIFICATION_SERVICE_OFF";
    String mailNotificationStyle = "MAIL_NOTIFICATION_STYLE_DEFAULT";
    String modhash = null;
    int numUsername = 0;
    Cookie redditSessionCookie = null;
    int rotation = -1;
    boolean showCommentGuideLines = true;
    int threadDownloadLimit = 25;
    boolean useExternalBrowser = false;
    String username = null;
    String username2 = null;
    String username3 = null;
    String username4 = null;
    String username5 = null;

    public static class Rotation {
        public static int valueOf(String valueString) {
            if ("ROTATION_UNSPECIFIED".equals(valueString)) {
                return -1;
            }
            if ("ROTATION_PORTRAIT".equals(valueString)) {
                return 1;
            }
            return "ROTATION_LANDSCAPE".equals(valueString) ? 0 : -1;
        }

        public static String toString(int value) {
            switch (value) {
                case -1:
                    return "ROTATION_UNSPECIFIED";
                case 0:
                    return "ROTATION_LANDSCAPE";
                case 1:
                    return "ROTATION_PORTRAIT";
                default:
                    return "ROTATION_UNSPECIFIED";
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isLoggedIn() {
        return this.username != null;
    }

    /* access modifiers changed from: package-private */
    public void setAlwaysShowNextPrevious(boolean alwaysShowNextPrevious2) {
        this.alwaysShowNextPrevious = alwaysShowNextPrevious2;
    }

    /* access modifiers changed from: package-private */
    public void setCommentsSortByUrl(String commentsSortByUrl2) {
        this.commentsSortByUrl = commentsSortByUrl2;
    }

    /* access modifiers changed from: package-private */
    public void setConfirmQuit(boolean confirmQuit2) {
        this.confirmQuit = confirmQuit2;
    }

    /* access modifiers changed from: package-private */
    public void setHomepage(String homepage2) {
        this.homepage = homepage2;
    }

    /* access modifiers changed from: package-private */
    public void setLoadThumbnails(boolean loadThumbnails2) {
        this.loadThumbnails = loadThumbnails2;
    }

    /* access modifiers changed from: package-private */
    public void setLoadThumbnailsOnlyWifi(boolean loadThumbnails2) {
        this.loadThumbnailsOnlyWifi = loadThumbnails2;
    }

    /* access modifiers changed from: package-private */
    public void setMailNotificationService(String mailNotificationService2) {
        this.mailNotificationService = mailNotificationService2;
    }

    /* access modifiers changed from: package-private */
    public void setMailNotificationStyle(String mailNotificationStyle2) {
        this.mailNotificationStyle = mailNotificationStyle2;
    }

    /* access modifiers changed from: package-private */
    public void setModhash(String modhash2) {
        this.modhash = modhash2;
    }

    /* access modifiers changed from: package-private */
    public void setRedditSessionCookie(Cookie redditSessionCookie2) {
        this.redditSessionCookie = redditSessionCookie2;
    }

    /* access modifiers changed from: package-private */
    public void setRotation(int rotation2) {
        this.rotation = rotation2;
    }

    /* access modifiers changed from: package-private */
    public void setShowCommentGuideLines(boolean showCommentGuideLines2) {
        this.showCommentGuideLines = showCommentGuideLines2;
    }

    /* access modifiers changed from: package-private */
    public void setThreadDownloadLimit(int threadDownloadLimit2) {
        this.threadDownloadLimit = threadDownloadLimit2;
    }

    /* access modifiers changed from: package-private */
    public void setUseExternalBrowser(boolean useExternalBrowser2) {
        this.useExternalBrowser = useExternalBrowser2;
    }

    /* access modifiers changed from: package-private */
    public void setUsername(String username6) {
        this.username = username6;
    }

    public void saveRedditPreferences(Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        if (this.username != null) {
            editor.putString("username", this.username);
        } else {
            editor.remove("username");
        }
        if (this.redditSessionCookie != null) {
            editor.putString("reddit_sessionValue", this.redditSessionCookie.getValue());
            editor.putString("reddit_sessionDomain", this.redditSessionCookie.getDomain());
            editor.putString("reddit_sessionPath", this.redditSessionCookie.getPath());
            if (this.redditSessionCookie.getExpiryDate() != null) {
                editor.putLong("reddit_sessionExpiryDate", this.redditSessionCookie.getExpiryDate().getTime());
            }
        }
        if (this.modhash != null) {
            editor.putString("modhash", this.modhash.toString());
        }
        editor.putString("firstRunDate", this.firstRunDate);
        editor.putBoolean("firstRun", this.firstRun);
        editor.putString("homepage", this.homepage.toString());
        editor.putBoolean("use_external_browser", this.useExternalBrowser);
        editor.putBoolean("confirm_quit", this.confirmQuit);
        editor.putBoolean("always_show_next_previous", this.alwaysShowNextPrevious);
        editor.putString("sort_by_url", this.commentsSortByUrl);
        editor.putBoolean("show_comment_guide_lines", this.showCommentGuideLines);
        editor.putString("rotation", Rotation.toString(this.rotation));
        editor.putBoolean("load_thumbnails", this.loadThumbnails);
        editor.putBoolean("load_thumbnails_only_wifi", this.loadThumbnailsOnlyWifi);
        editor.putString("mail_notification_style", this.mailNotificationStyle);
        editor.putString("mail_notification_service", this.mailNotificationService);
        editor.commit();
    }

    public void loadRedditPreferences(Context context) {
        DefaultHttpClient client = RedditAPI.getClient();
        SharedPreferences sessionPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.firstRunDate = sessionPrefs.getString("firstRunDate", null);
        this.firstRun = sessionPrefs.getBoolean("firstRun", true);
        setUsername(sessionPrefs.getString("username", null));
        setModhash(sessionPrefs.getString("modhash", null));
        String cookieValue = sessionPrefs.getString("reddit_sessionValue", null);
        String cookieDomain = sessionPrefs.getString("reddit_sessionDomain", null);
        String cookiePath = sessionPrefs.getString("reddit_sessionPath", null);
        long cookieExpiryDate = sessionPrefs.getLong("reddit_sessionExpiryDate", -1);
        if (cookieValue != null) {
            BasicClientCookie redditSessionCookie2 = new BasicClientCookie("reddit_session", cookieValue);
            redditSessionCookie2.setDomain(cookieDomain);
            redditSessionCookie2.setPath(cookiePath);
            if (cookieExpiryDate != -1) {
                redditSessionCookie2.setExpiryDate(new Date(cookieExpiryDate));
            } else {
                redditSessionCookie2.setExpiryDate((Date) null);
            }
            setRedditSessionCookie(redditSessionCookie2);
            if (client != null) {
                client.getCookieStore().addCookie(redditSessionCookie2);
                try {
                    CookieSyncManager.getInstance().sync();
                } catch (IllegalStateException e) {
                }
            }
        }
        String homepage2 = sessionPrefs.getString("homepage", "reddit front page").trim();
        if ("".equals(homepage2)) {
            setHomepage("reddit front page");
        } else {
            setHomepage(homepage2);
        }
        setUseExternalBrowser(sessionPrefs.getBoolean("use_external_browser", false));
        setConfirmQuit(sessionPrefs.getBoolean("confirm_quit", true));
        setAlwaysShowNextPrevious(sessionPrefs.getBoolean("always_show_next_previous", true));
        setCommentsSortByUrl(sessionPrefs.getString("sort_by_url", "sort=confidence"));
        setShowCommentGuideLines(sessionPrefs.getBoolean("show_comment_guide_lines", true));
        setRotation(Rotation.valueOf(sessionPrefs.getString("rotation", "ROTATION_UNSPECIFIED")));
        setLoadThumbnails(sessionPrefs.getBoolean("load_thumbnails", true));
        setLoadThumbnailsOnlyWifi(sessionPrefs.getBoolean("load_thumbnails_only_wifi", false));
        setMailNotificationStyle(sessionPrefs.getString("mail_notification_style", "MAIL_NOTIFICATION_STYLE_DEFAULT"));
        setMailNotificationService(sessionPrefs.getString("mail_notification_service", "MAIL_NOTIFICATION_SERVICE_OFF"));
    }
}
