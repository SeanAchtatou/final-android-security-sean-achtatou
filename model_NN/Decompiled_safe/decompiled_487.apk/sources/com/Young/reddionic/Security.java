package com.Young.reddionic;

import android.text.TextUtils;
import android.util.Log;
import com.Young.reddionic.Consts;
import com.Young.reddionic.util.Base64;
import com.Young.reddionic.util.Base64DecoderException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Security {
    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final SecureRandom RANDOM = new SecureRandom();
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static final String TAG = "Security";
    private static HashSet<Long> sKnownNonces = new HashSet<>();

    public static class VerifiedPurchase {
        public String developerPayload;
        public String notificationId;
        public String orderId;
        public String productId;
        public Consts.PurchaseState purchaseState;
        public long purchaseTime;

        public VerifiedPurchase(Consts.PurchaseState purchaseState2, String notificationId2, String productId2, String orderId2, long purchaseTime2, String developerPayload2) {
            this.purchaseState = purchaseState2;
            this.notificationId = notificationId2;
            this.productId = productId2;
            this.orderId = orderId2;
            this.purchaseTime = purchaseTime2;
            this.developerPayload = developerPayload2;
        }
    }

    public static long generateNonce() {
        long nonce = RANDOM.nextLong();
        sKnownNonces.add(Long.valueOf(nonce));
        return nonce;
    }

    public static void removeNonce(long nonce) {
        sKnownNonces.remove(Long.valueOf(nonce));
    }

    public static boolean isNonceKnown(long nonce) {
        return sKnownNonces.contains(Long.valueOf(nonce));
    }

    /* JADX INFO: Multiple debug info for r3v20 java.security.PublicKey: [D('base64EncodedPublicKey' java.lang.String), D('key' java.security.PublicKey)] */
    /* JADX INFO: Multiple debug info for r20v11 boolean: [D('signature' java.lang.String), D('verified' boolean)] */
    public static ArrayList<VerifiedPurchase> verifyPurchase(String signedData, String signature) {
        boolean verified;
        int numTransactions;
        if (signedData == null) {
            Log.e(TAG, "data is null");
            return null;
        }
        if (!TextUtils.isEmpty(signature)) {
            boolean verified2 = verify(generatePublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApAZsWryC2VXrkQ9WGJ62hZNj9H7adwhaO9rE9C4XRDxc7H6XO5qKmDA7swLeRvkkLEiVLNQy62Kr9YxJ49ubqUBH78YuSUZi+XUJ1kpmm+TVtnvvcpXrLpOBk2Y3B7TBKSZ++8L9Ey+tv53eaE+xzwKLTS67KhYONuN6fXM+VPqSxb8S7Sb78y0a7tgj+Ws+dCxVfrUYuBcRzJwEIiy7cIdemd0UaQu1Uy3+a61xE4zZ9ry6Z8ibFif7ufb8wgC6qaJ7BgQSFUO6M3+jj+EgM40oIWiBO1Qbw97z8lHHr754U9HD/NhQtOqIy7cflekLBTXLM0V+ECY5BU+ON+14cwIDAQAB"), signedData, signature);
            if (!verified2) {
                Log.w(TAG, "signature does not match data.");
                return null;
            }
            verified = verified2;
        } else {
            verified = false;
        }
        try {
            JSONObject jSONObject = new JSONObject(signedData);
            long nonce = jSONObject.optLong("nonce");
            try {
                JSONArray jTransactionsArray = jSONObject.optJSONArray("orders");
                if (jTransactionsArray != null) {
                    try {
                        numTransactions = jTransactionsArray.length();
                    } catch (JSONException e) {
                        return null;
                    }
                } else {
                    numTransactions = 0;
                }
                if (isNonceKnown(nonce) == 0) {
                    Log.w(TAG, "Nonce not found: " + nonce);
                    return null;
                }
                ArrayList<VerifiedPurchase> purchases = new ArrayList<>();
                int i = 0;
                while (i < numTransactions) {
                    try {
                        JSONObject jElement = jTransactionsArray.getJSONObject(i);
                        Consts.PurchaseState purchaseState = Consts.PurchaseState.valueOf(jElement.getInt("purchaseState"));
                        String productId = jElement.getString("productId");
                        String string = jElement.getString("packageName");
                        long purchaseTime = jElement.getLong("purchaseTime");
                        String orderId = jElement.optString("orderId", "");
                        String notifyId = null;
                        if (jElement.has("notificationId")) {
                            notifyId = jElement.getString("notificationId");
                        }
                        String developerPayload = jElement.optString("developerPayload", null);
                        if (purchaseState != Consts.PurchaseState.PURCHASED || verified) {
                            purchases.add(new VerifiedPurchase(purchaseState, notifyId, productId, orderId, purchaseTime, developerPayload));
                        }
                        i++;
                    } catch (JSONException e2) {
                        Log.e(TAG, "JSON exception: ", e2);
                        return null;
                    }
                }
                removeNonce(nonce);
                return purchases;
            } catch (JSONException e3) {
            }
        } catch (JSONException e4) {
            return null;
        }
    }

    public static PublicKey generatePublicKey(String encodedPublicKey) {
        try {
            return KeyFactory.getInstance(KEY_FACTORY_ALGORITHM).generatePublic(new X509EncodedKeySpec(Base64.decode(encodedPublicKey)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e2) {
            Log.e(TAG, "Invalid key specification.");
            throw new IllegalArgumentException(e2);
        } catch (Base64DecoderException e3) {
            Log.e(TAG, "Base64 decoding failed.");
            throw new IllegalArgumentException(e3);
        }
    }

    public static boolean verify(PublicKey publicKey, String signedData, String signature) {
        try {
            Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM);
            sig.initVerify(publicKey);
            sig.update(signedData.getBytes());
            if (sig.verify(Base64.decode(signature))) {
                return true;
            }
            Log.e(TAG, "Signature verification failed.");
            return false;
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "NoSuchAlgorithmException.");
            return false;
        } catch (InvalidKeyException e2) {
            Log.e(TAG, "Invalid key specification.");
            return false;
        } catch (SignatureException e3) {
            Log.e(TAG, "Signature exception.");
            return false;
        } catch (Base64DecoderException e4) {
            Log.e(TAG, "Base64 decoding failed.");
            return false;
        }
    }
}
