package com.Young.reddionic;

public class About {
    private AboutData data;
    private String kind;

    public About() {
    }

    public About(String stuff) {
        this.kind = null;
        this.data = null;
    }

    public void setKind(String kind2) {
        this.kind = kind2;
    }

    public String getKind() {
        return this.kind;
    }

    public void setData(AboutData data2) {
        this.data = data2;
    }

    public AboutData getData() {
        return this.data;
    }
}
