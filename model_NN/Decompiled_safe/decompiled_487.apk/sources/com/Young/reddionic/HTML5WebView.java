package com.Young.reddionic;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

public class HTML5WebView extends WebView {
    static final FrameLayout.LayoutParams COVER_SCREEN_PARAMS = new FrameLayout.LayoutParams(-1, -1);
    static final String LOGTAG = "HTML5WebView";
    private FrameLayout mBrowserFrameLayout;
    private FrameLayout mContentView;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public View mCustomView;
    /* access modifiers changed from: private */
    public WebChromeClient.CustomViewCallback mCustomViewCallback;
    /* access modifiers changed from: private */
    public FrameLayout mCustomViewContainer;
    private FrameLayout mLayout;
    private MyWebChromeClient mWebChromeClient;

    private void init(Context context) {
        this.mContext = context;
        this.mLayout = new FrameLayout(context);
        this.mBrowserFrameLayout = (FrameLayout) LayoutInflater.from((Activity) this.mContext).inflate((int) R.layout.web_screen, (ViewGroup) null);
        this.mContentView = (FrameLayout) this.mBrowserFrameLayout.findViewById(R.id.main_content);
        this.mCustomViewContainer = (FrameLayout) this.mBrowserFrameLayout.findViewById(R.id.fullscreen_custom_content);
        this.mLayout.addView(this.mBrowserFrameLayout, COVER_SCREEN_PARAMS);
        this.mWebChromeClient = new MyWebChromeClient(this, null);
        setWebChromeClient(this.mWebChromeClient);
        setWebViewClient(new MyWebViewClient(this, null));
        WebSettings s = getSettings();
        s.setBuiltInZoomControls(true);
        s.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        s.setUseWideViewPort(true);
        s.setLoadWithOverviewMode(true);
        s.setSavePassword(true);
        s.setSaveFormData(true);
        s.setJavaScriptEnabled(true);
        s.setGeolocationEnabled(true);
        s.setGeolocationDatabasePath("/data/data/org.itri.html5webview/databases/");
        s.setDomStorageEnabled(true);
        this.mContentView.addView(this);
    }

    public HTML5WebView(Context context) {
        super(context);
        init(context);
    }

    public HTML5WebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HTML5WebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public FrameLayout getLayout() {
        return this.mLayout;
    }

    public boolean inCustomView() {
        return this.mCustomView != null;
    }

    public void hideCustomView() {
        this.mWebChromeClient.onHideCustomView();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.mCustomView != null || !canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        goBack();
        return true;
    }

    private class MyWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        private MyWebChromeClient() {
        }

        /* synthetic */ MyWebChromeClient(HTML5WebView hTML5WebView, MyWebChromeClient myWebChromeClient) {
            this();
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
            HTML5WebView.this.setVisibility(8);
            if (HTML5WebView.this.mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            HTML5WebView.this.mCustomViewContainer.addView(view);
            HTML5WebView.this.mCustomView = view;
            HTML5WebView.this.mCustomViewCallback = callback;
            HTML5WebView.this.mCustomViewContainer.setVisibility(0);
        }

        public void onHideCustomView() {
            if (HTML5WebView.this.mCustomView != null) {
                HTML5WebView.this.mCustomView.setVisibility(8);
                HTML5WebView.this.mCustomViewContainer.removeView(HTML5WebView.this.mCustomView);
                HTML5WebView.this.mCustomView = null;
                HTML5WebView.this.mCustomViewContainer.setVisibility(8);
                HTML5WebView.this.mCustomViewCallback.onCustomViewHidden();
                HTML5WebView.this.setVisibility(0);
            }
        }

        public Bitmap getDefaultVideoPoster() {
            if (this.mDefaultVideoPoster == null) {
                this.mDefaultVideoPoster = BitmapFactory.decodeResource(HTML5WebView.this.getResources(), R.drawable.video);
            }
            return this.mDefaultVideoPoster;
        }

        public View getVideoLoadingProgressView() {
            if (this.mVideoProgressView == null) {
                this.mVideoProgressView = LayoutInflater.from(HTML5WebView.this.mContext).inflate((int) R.layout.video_loading_progress, (ViewGroup) null);
            }
            return this.mVideoProgressView;
        }

        public void onReceivedTitle(WebView view, String title) {
            ((Activity) HTML5WebView.this.mContext).setTitle(title);
        }

        public void onProgressChanged(WebView view, int newProgress) {
            ((Activity) HTML5WebView.this.mContext).getWindow().setFeatureInt(2, newProgress * 100);
        }

        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            callback.invoke(origin, true, false);
        }
    }

    private class MyWebViewClient extends WebViewClient {
        private MyWebViewClient() {
        }

        /* synthetic */ MyWebViewClient(HTML5WebView hTML5WebView, MyWebViewClient myWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i(HTML5WebView.LOGTAG, "shouldOverrideUrlLoading: " + url);
            view.loadUrl(url);
            return true;
        }
    }
}
