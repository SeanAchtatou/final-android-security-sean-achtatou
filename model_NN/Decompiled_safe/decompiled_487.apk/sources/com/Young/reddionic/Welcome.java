package com.Young.reddionic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.Young.reddionic.Browse;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.ArrayList;

public class Welcome extends FragmentActivity {
    static Activity act;
    static int liked = 0;
    static boolean refreshing = false;
    static int score = -1;
    ActionBar actionbar;
    boolean clicked = false;
    Context context;
    DisplayMetrics dm;
    boolean feedbacking = false;
    boolean hidden = false;
    ArrayAdapter<String> includedArrayAdapter;
    boolean loggingin = false;
    Browse.MyAdapter mAdapter;
    ViewPager mPager;
    private final RedditSettings mSettings = new RedditSettings();
    ArrayList<String> names = new ArrayList<>();
    int pos = 0;
    PopupWindow pw;
    boolean saved = false;
    View theActionBar;
    int thread_count = 0;
    GoogleAnalyticsTracker tracker;
    WebView webView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.welcome);
        this.context = getApplicationContext();
        act = this;
        CookieSyncManager.createInstance(getApplicationContext());
        this.mSettings.loadRedditPreferences(getApplicationContext());
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 2) {
            setContentView((int) R.layout.welcome);
        } else if (newConfig.orientation == 1) {
            setContentView((int) R.layout.welcome);
        }
        if (this.clicked) {
            ((RelativeLayout) findViewById(R.id.welcome_1)).setVisibility(8);
            ((RelativeLayout) findViewById(R.id.welcome_2)).setVisibility(0);
        }
    }

    public void submitClick(View button) {
        startActivity(new Intent(this, Dashboard.class));
    }

    public void AgreeClick(View button) {
        ((RelativeLayout) findViewById(R.id.welcome_1)).setVisibility(8);
        ((RelativeLayout) findViewById(R.id.welcome_2)).setVisibility(0);
        this.clicked = true;
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        finish();
        super.onPause();
    }
}
