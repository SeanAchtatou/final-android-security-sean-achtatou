package com.Young.reddionic;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableString;
import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonAnySetter;

public class ThingInfo implements Serializable, Parcelable {
    public static final Parcelable.Creator<ThingInfo> CREATOR = new Parcelable.Creator<ThingInfo>() {
        public ThingInfo createFromParcel(Parcel in) {
            return new ThingInfo(in, null);
        }

        public ThingInfo[] newArray(int size) {
            return new ThingInfo[size];
        }
    };
    static final long serialVersionUID = 39;
    private String author;
    private String body;
    private String body_html;
    private boolean buttonOpened;
    private boolean clicked;
    private String context;
    private double created;
    private double created_utc;
    private String dest;
    private String domain;
    private int downs;
    private Long first_message;
    private boolean hidden;
    private String id;
    private boolean is_self;
    private boolean lastComment;
    private Boolean likes;
    private String link_id;
    private int mIndent;
    private String mReplyDraft;
    private transient SpannableString mSSAuthor;
    private transient CharSequence mSpannedBody;
    private transient CharSequence mSpannedSelftext;
    private transient Drawable mThumbnailDrawable;
    private String name;
    private boolean new_;
    private int num_comments;
    private boolean over_18;
    private String parent_id;
    private String permalink;
    private Listing replies;
    private boolean replyOpened;
    private boolean saved;
    private int score;
    private String selftext;
    private String selftext_html;
    private String subject;
    private String subreddit;
    private String subreddit_id;
    private String thumbnail;
    private String title;
    private int ups;
    private String url;
    private boolean was_comment;

    public ThingInfo() {
        this.mSpannedSelftext = null;
        this.mSpannedBody = null;
        this.mSSAuthor = null;
        this.mThumbnailDrawable = null;
        this.mIndent = 0;
        this.mReplyDraft = null;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getBody() {
        return this.body;
    }

    public String getBody_html() {
        return this.body_html;
    }

    public String getContext() {
        return this.context;
    }

    public double getCreated() {
        return this.created;
    }

    public double getCreated_utc() {
        return this.created_utc;
    }

    public String getDest() {
        return this.dest;
    }

    public String getDomain() {
        return this.domain;
    }

    public int getDowns() {
        return this.downs;
    }

    public Long getFirst_message() {
        return this.first_message;
    }

    public String getId() {
        return this.id;
    }

    public int getIndent() {
        return this.mIndent;
    }

    public Boolean getLikes() {
        return this.likes;
    }

    public String getLink_id() {
        return this.link_id;
    }

    public String getName() {
        return this.name;
    }

    public int getNum_comments() {
        return this.num_comments;
    }

    public String getParent_id() {
        return this.parent_id;
    }

    public String getPermalink() {
        return this.permalink;
    }

    public Listing getReplies() {
        return this.replies;
    }

    public String getReplyDraft() {
        return this.mReplyDraft;
    }

    public int getScore() {
        return this.score;
    }

    public String getSelftext() {
        return this.selftext;
    }

    public String getSelftext_html() {
        return this.selftext_html;
    }

    public CharSequence getSpannedBody() {
        return this.mSpannedBody;
    }

    public CharSequence getSpannedSelftext() {
        return this.mSpannedSelftext;
    }

    public SpannableString getSSAuthor() {
        return this.mSSAuthor;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getSubreddit() {
        return this.subreddit;
    }

    public String getSubreddit_id() {
        return this.subreddit_id;
    }

    public String getThumbnail() {
        return absolutePathToURL(this.thumbnail);
    }

    public String absolutePathToURL(String path) {
        if (path.startsWith("/")) {
            return "http://www.reddit.com" + path;
        }
        return path;
    }

    public Drawable getThumbnailDrawable() {
        return this.mThumbnailDrawable;
    }

    public String getTitle() {
        return this.title;
    }

    public int getUps() {
        return this.ups;
    }

    public String getUrl() {
        return this.url;
    }

    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
    }

    public boolean isClicked() {
        return this.clicked;
    }

    public boolean isHidden() {
        return this.hidden;
    }

    public boolean isIs_self() {
        return this.is_self;
    }

    public boolean isNew() {
        return this.new_;
    }

    public boolean isOver_18() {
        return this.over_18;
    }

    public boolean isSaved() {
        return this.saved;
    }

    public boolean isWas_comment() {
        return this.was_comment;
    }

    public void setAuthor(String author2) {
        this.author = author2;
    }

    public void setBody(String body2) {
        this.body = body2;
    }

    public void setBody_html(String body_html2) {
        this.body_html = body_html2;
    }

    public void setClicked(boolean clicked2) {
        this.clicked = clicked2;
    }

    public void setContext(String context2) {
        this.context = context2;
    }

    public void setCreated(double created2) {
        this.created = created2;
    }

    public void setCreated_utc(double created_utc2) {
        this.created_utc = created_utc2;
    }

    public void setDest(String dest2) {
        this.dest = dest2;
    }

    public void setDomain(String domain2) {
        this.domain = domain2;
    }

    public void setDowns(int downs2) {
        this.downs = downs2;
    }

    public void setFirst_message(Long first_message2) {
        this.first_message = first_message2;
    }

    public void setHidden(boolean hidden2) {
        this.hidden = hidden2;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public void setIndent(int indent) {
        this.mIndent = indent;
    }

    public void setIs_self(boolean is_self2) {
        this.is_self = is_self2;
    }

    public void setLikes(Boolean likes2) {
        this.likes = likes2;
    }

    public void setLink_id(String link_id2) {
        this.link_id = link_id2;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public void setNew(boolean new_2) {
        this.new_ = new_2;
    }

    public void setNum_comments(int num_comments2) {
        this.num_comments = num_comments2;
    }

    public void setOver_18(boolean over_182) {
        this.over_18 = over_182;
    }

    public void setParent_id(String parent_id2) {
        this.parent_id = parent_id2;
    }

    public void setPermalink(String permalink2) {
        this.permalink = permalink2;
    }

    public void setReplies(Listing replies2) {
        this.replies = replies2;
    }

    public void setReplyDraft(String replyDraft) {
        this.mReplyDraft = replyDraft;
    }

    public void setSaved(boolean saved2) {
        this.saved = saved2;
    }

    public void setScore(int score2) {
        this.score = score2;
    }

    public void setSelftext(String selftext2) {
        this.selftext = selftext2;
    }

    public void setSelftext_html(String selftext_html2) {
        this.selftext_html = selftext_html2;
    }

    public void setSpannedBody(CharSequence ssbBody) {
        this.mSpannedBody = ssbBody;
    }

    public void setSpannedSelftext(CharSequence selftext2) {
        this.mSpannedSelftext = selftext2;
    }

    public void setSSAuthor(SpannableString ssAuthor) {
        this.mSSAuthor = ssAuthor;
    }

    public void setSubject(String subject2) {
        this.subject = subject2;
    }

    public void setSubreddit(String subreddit2) {
        this.subreddit = subreddit2;
    }

    public void setSubreddit_id(String subreddit_id2) {
        this.subreddit_id = subreddit_id2;
    }

    public void setThumbnail(String thumbnail2) {
        this.thumbnail = thumbnail2;
    }

    public void setThumbnailDrawable(Drawable mThumbnailDrawable2) {
        this.mThumbnailDrawable = mThumbnailDrawable2;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public void setUps(int ups2) {
        this.ups = ups2;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public void setWas_comment(boolean was_comment2) {
        this.was_comment = was_comment2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeValue(this.author);
        out.writeValue(this.body);
        out.writeValue(this.body_html);
        out.writeValue(this.context);
        out.writeDouble(this.created);
        out.writeDouble(this.created_utc);
        out.writeValue(this.dest);
        out.writeValue(this.domain);
        out.writeInt(this.downs);
        out.writeValue(this.first_message);
        out.writeValue(this.id);
        out.writeValue(this.link_id);
        out.writeValue(this.name);
        out.writeInt(this.num_comments);
        out.writeValue(this.parent_id);
        out.writeValue(this.permalink);
        out.writeInt(this.score);
        out.writeValue(this.selftext);
        out.writeValue(this.selftext_html);
        out.writeValue(this.subject);
        out.writeValue(this.subreddit);
        out.writeValue(this.subreddit_id);
        out.writeValue(this.thumbnail);
        out.writeValue(this.title);
        out.writeInt(this.ups);
        out.writeValue(this.url);
        out.writeValue(this.likes);
        out.writeBooleanArray(new boolean[]{this.clicked, this.hidden, this.is_self, this.new_, this.over_18, this.saved, this.was_comment});
    }

    private ThingInfo(Parcel in) {
        this.mSpannedSelftext = null;
        this.mSpannedBody = null;
        this.mSSAuthor = null;
        this.mThumbnailDrawable = null;
        this.mIndent = 0;
        this.mReplyDraft = null;
        this.author = (String) in.readValue(null);
        this.body = (String) in.readValue(null);
        this.body_html = (String) in.readValue(null);
        this.context = (String) in.readValue(null);
        this.created = in.readDouble();
        this.created_utc = in.readDouble();
        this.dest = (String) in.readValue(null);
        this.domain = (String) in.readValue(null);
        this.downs = in.readInt();
        this.first_message = (Long) in.readValue(null);
        this.id = (String) in.readValue(null);
        this.link_id = (String) in.readValue(null);
        this.name = (String) in.readValue(null);
        this.num_comments = in.readInt();
        this.parent_id = (String) in.readValue(null);
        this.permalink = (String) in.readValue(null);
        this.score = in.readInt();
        this.selftext = (String) in.readValue(null);
        this.selftext_html = (String) in.readValue(null);
        this.subject = (String) in.readValue(null);
        this.subreddit = (String) in.readValue(null);
        this.subreddit_id = (String) in.readValue(null);
        this.thumbnail = (String) in.readValue(null);
        this.title = (String) in.readValue(null);
        this.ups = in.readInt();
        this.url = (String) in.readValue(null);
        this.likes = (Boolean) in.readValue(null);
        boolean[] booleans = new boolean[7];
        in.readBooleanArray(booleans);
        this.clicked = booleans[0];
        this.hidden = booleans[1];
        this.is_self = booleans[2];
        this.new_ = booleans[3];
        this.over_18 = booleans[4];
        this.saved = booleans[5];
        this.was_comment = booleans[6];
    }

    /* synthetic */ ThingInfo(Parcel parcel, ThingInfo thingInfo) {
        this(parcel);
    }

    public boolean isReplyOpened() {
        return this.replyOpened;
    }

    public void setReplyOpened(boolean replyOpened2) {
        this.replyOpened = replyOpened2;
    }

    public boolean isLastComment() {
        return this.lastComment;
    }

    public void setLastComment(boolean lastComment2) {
        this.lastComment = lastComment2;
    }

    public boolean isButtonOpened() {
        return this.buttonOpened;
    }

    public void setButtonOpened(boolean buttonOpened2) {
        this.buttonOpened = buttonOpened2;
    }
}
