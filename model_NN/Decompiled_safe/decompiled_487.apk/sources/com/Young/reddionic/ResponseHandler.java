package com.Young.reddionic;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.Young.reddionic.BillingService;
import com.Young.reddionic.Consts;

public class ResponseHandler {
    private static final String TAG = "ResponseHandler";
    /* access modifiers changed from: private */
    public static PurchaseObserver sPurchaseObserver;

    public static synchronized void register(PurchaseObserver observer) {
        synchronized (ResponseHandler.class) {
            sPurchaseObserver = observer;
        }
    }

    public static synchronized void unregister(PurchaseObserver observer) {
        synchronized (ResponseHandler.class) {
            sPurchaseObserver = null;
        }
    }

    public static void checkBillingSupportedResponse(boolean supported) {
        if (sPurchaseObserver != null) {
            sPurchaseObserver.onBillingSupported(supported);
        }
    }

    public static void buyPageIntentResponse(PendingIntent pendingIntent, Intent intent) {
        if (sPurchaseObserver != null) {
            sPurchaseObserver.startBuyPageActivity(pendingIntent, intent);
        }
    }

    public static void purchaseResponse(Context context, Consts.PurchaseState purchaseState, String productId, String orderId, long purchaseTime, String developerPayload) {
        final Context context2 = context;
        final String str = orderId;
        final String str2 = productId;
        final Consts.PurchaseState purchaseState2 = purchaseState;
        final long j = purchaseTime;
        final String str3 = developerPayload;
        new Thread(new Runnable() {
            public void run() {
                PurchaseDatabase db = new PurchaseDatabase(context2);
                int quantity = db.updatePurchase(str, str2, purchaseState2, j, str3);
                db.close();
                synchronized (ResponseHandler.class) {
                    if (ResponseHandler.sPurchaseObserver != null) {
                        ResponseHandler.sPurchaseObserver.postPurchaseStateChange(purchaseState2, str2, quantity, j, str3);
                    }
                }
            }
        }).start();
    }

    public static void responseCodeReceived(Context context, BillingService.RequestPurchase request, Consts.ResponseCode responseCode) {
        if (sPurchaseObserver != null) {
            sPurchaseObserver.onRequestPurchaseResponse(request, responseCode);
        }
    }

    public static void responseCodeReceived(Context context, BillingService.RestoreTransactions request, Consts.ResponseCode responseCode) {
        if (sPurchaseObserver != null) {
            sPurchaseObserver.onRestoreTransactionsResponse(request, responseCode);
        }
    }
}
