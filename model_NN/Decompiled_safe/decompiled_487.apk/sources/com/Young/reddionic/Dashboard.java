package com.Young.reddionic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class Dashboard extends FragmentActivity {
    static boolean refreshing = false;
    Activity act;
    ActionBar actionbar;
    List<String> alpha_testers;
    String[] alpha_testers_array = {"ohseebiscuit", "morehush", "Chillburger", "bigfkncee", "princesscrapfactory", "laos101", "honestbleeps", "digitalshay", "jaymax", "ponchopunch", "JasonWin", "abnc", "Joereist ", "znibbor", "WheninRome", "avree", "zms", "luketheduke03", "bmwracer0", "neonfire", "QuiGonJinn", "GunnerMcGrath", "OpenSOB", "specialk16", "kaolbrec", "arturogoga", "Vigil123", "undertaker97", "spinn", "MilitantNegro", "quantum3k", "umop_apisdn", "CWagner", "mucsun", "deviavir", "stinny7", "givequicheachance", "sealpunter", "letsgoflyers81", "fontaine", "giftedmunchkin", "aaronpaws", "sadisticshane", "larsgj", "UnfortunatelyMacabre", "thevoiceless", "PLUGintotheGRID", "chemosabe", "russphil", "Nidorino", "suburban_war", "east718", "raskytle", "cstark", "jbklego", "t0rn4d0r3x", "beefswizzle", "Linardni ", "TheCatAndSgtBaker", "youhavemyaxe", "gmcdonald93", "jnecessary", "chanop", "zMugen", "ashura001", "esoomenona", "JXEYES", "probablynotclever", "spif", "chaircrow", "CuriousCursor", "nandryshak", "krickettt", "siegfail", "staticfish", "illojal", "jebarooney", "ryansullivan", "aesamattki", "znupi", "chutem", "Bradart", "Deusis", "mandlar", "ProfessorTurnip", "notacyborg", "kudakeru", "dannzeman", "wheredidrealitygo", "vexsten", "LaurensBER", "monolithdigital", "fireblend", "dullknives", "Tarabukka", "ElusiveByte", "Soothsayre", "Jaekmsuka", "goshdarnrootintootin", "goshdarnrootintootin", "Marceriksen", "reddionic", "thegloriousday", "furiousmule", "blogoms", "L8rMr", "Spoonie3372", "mindftw", "woodsyx", "holy_maccaroni", "Ifuckedyourgrandma", "Tliblem", "xeroxpheline", "mountain_mofo", "splooiemello", "ahuisinga", "LotusNotesGuy", "InvaderDJ", "Quailson", "MonkeyCrumpets", "Aaberg321", "thonl", "xurvis", "vassie", "ronjon13", "regtastic", "tictactoejam", "xert", "wellok", "therealginger93", "jcll", "remlap", "denacioust", "Ryguy085", "tymalo", "punch2", "smileythom", "tnecniv", "Flobin", "dogboy53", "jsantos17", "mykeebee", "drumercalzone09", "shocker4256", "jmayniac", "LarcusMywood", "Amanitus", "maverick262", "Gentlelady", "Atratyys", "seanboy42", "rickster747", "jerrrrry", "cabbeer", "EzSiFiMetal", "neveez", "Josefromthe925", "silentmage", "esmith972", "Cauterized", "akajimmy", "carbanion", "geauxtig3rs", "banksj17", "Factual_Pterodactyl", "Exoplasm", "Davsm", "Okitaz", "suparnemo", "djjcast", "notreallystephenfry", "franklink1829", "giants3b", "dinofan01", "eppey1", "RandomChu", "nexus14", "yumcax", "AC_Ra", "piratelax40", "chrisbellttu", "Awesomeade", "GenDan", "MechEng2723", "mahomz", "NasaGeek", "evillozer", "Tr3v0r", "nanorh", "rphillipps16", "Sriyegna", "kjaonline", "pcgamingisted", "tha_funkee_redditor", "mazook98", "hermy65", "whatwhatintehbutt", "SirFadakar", "Antonyourkeyboard", "TheKidd", "camboss", "junkieguru", "fernandotakai", "jadanzzy", "Reita", "incubusmylove", "lumixter", "grandwazoo", "daturkel", "element4life3", "dark_roast", "RobotAnna", "werm82", "cparedes", "muppethead", "CedarMadness", "tehkingo", "steinman17", "TKrios", "Poppeseed", "fryman", "avidal", "illiteratebeef", "desperatechaos", "Unkzdomain", "xmsxms", "Firestarman", "ansam", "wickedsun", "sirjoebob", "Shigster", "milhouse728", "optikalblitz", "numberoneus", "mantra2", "to3000", "dzdaddy", "derppenguin", "classhero", "syrinori", "zorro1979", "wirewolf", "abstractfool", "scriggities", "murphs33", "DevinXtreme", "bfogarty27", "glyserinesoul", "jerkmonkey", "tehmatticus", "nearblack", "apostre", "roonicorn", "alsocan", "myotheraccountsucked", "1337syntax", "SecretObsession", "ircarlton", "skottoboy", "newlyminted", "brandotron", "wannagotopopeyes", "Phargo", "Oblivian92", "Metaphex", "Intel2010", "Scurry", "dnytm", "Remigo", "musicsavesus", "Edu115", "swm5126", "Derimagia", "GODZiGGA", "cmrn", "DeadComedian", "Hannesver", "mlevit", "BeeNull", "Snookrc", "cTrox", "jaylow17", "audy592", "mjewbank", "GrahamX", "StrykerWolf ", "Lhowon", "bullonie", "wearearobot", "Refinery", "rav3nous", "slackmaster", "chtamina", "TossedRightOut", "auraspeed"};
    ArrayList<String> checked = new ArrayList<>();
    Context context;
    DisplayMetrics dm;
    boolean feedbacking = false;
    ArrayAdapter<String> includedArrayAdapter;
    boolean loggingin = false;
    MyAdapter mAdapter;
    ViewPager mPager;
    /* access modifiers changed from: private */
    public final RedditSettings mSettings = new RedditSettings();
    ArrayList<String> names = new ArrayList<>();
    int pos = 0;
    PopupWindow pw;
    View theActionBar;
    int thread_count = 0;
    GoogleAnalyticsTracker tracker;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.dashboard);
        this.dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-17867595-4", this);
        this.tracker.trackPageView("/dashboard");
        this.tracker.dispatch();
        this.context = getApplicationContext();
        this.act = this;
        CookieSyncManager.createInstance(getApplicationContext());
        this.mSettings.loadRedditPreferences(getApplicationContext());
        this.alpha_testers = Arrays.asList(this.alpha_testers_array);
        for (int x = 0; x < this.alpha_testers.size(); x++) {
            this.alpha_testers.set(x, this.alpha_testers.get(x).toUpperCase());
        }
        RedditAPI.mModhash = this.mSettings.modhash;
        RedditAPI.mUsername = this.mSettings.username;
        this.mAdapter = new MyAdapter(getSupportFragmentManager());
        this.mPager = (ViewPager) findViewById(R.id.accountpager);
        this.mPager.setAdapter(this.mAdapter);
        this.theActionBar = findViewById(R.id.actionbar);
        if (RedditAPI.mModhash != null && RedditAPI.mUsername != null) {
            ArrayList<String> passing = new ArrayList<>();
            passing.add(RedditAPI.mUsername);
            new Dashboard();
            new About().execute(passing);
        }
    }

    class FeedbackAdapter extends ArrayAdapter<String> {
        FeedbackAdapter() {
            super(Dashboard.this.context, (int) R.layout.included_list_item, Dashboard.this.names);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = Dashboard.this.getLayoutInflater().inflate((int) R.layout.included_list_item, parent, false);
            }
            TextView label = (TextView) row.findViewById(R.id.label);
            label.setText(Dashboard.this.names.get(position));
            label.setTag(Integer.valueOf(position));
            CheckBox checkbox = (CheckBox) row.findViewById(R.id.check);
            if (Dashboard.this.checked.get(position).equals("")) {
                checkbox.setChecked(false);
            } else {
                checkbox.setChecked(true);
            }
            checkbox.setTag(Integer.valueOf(position));
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* Debug info: failed to restart local var, previous not found, register: 3 */
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Dashboard.this.checked.set(((Integer) buttonView.getTag()).intValue(), "checked");
                    } else {
                        Dashboard.this.checked.set(((Integer) buttonView.getTag()).intValue(), "");
                    }
                }
            });
            return row;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.feedback /*2131034193*/:
                View popup = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.r_feedback_popup, (ViewGroup) null, false);
                this.pw = new PopupWindow(popup, -1, -1, true);
                if (this.names.size() == 0) {
                    this.names.add("Date of the first run");
                    this.names.add("Version Number");
                    this.names.add("Reddit ID");
                    this.names.add("Current View");
                    this.names.add("Device Name");
                    this.names.add("Device SDK");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                } else {
                    for (int x = 0; x < this.checked.size(); x++) {
                        this.checked.set(x, "checked");
                    }
                }
                this.pw.setBackgroundDrawable(new BitmapDrawable());
                this.pw.showAtLocation(findViewById(R.id.home_root), 0, 0, 0);
                ((LinearLayout) popup.findViewById(R.id.requestButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.request_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView1)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Request");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.request_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.improvementButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.improvement_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView2)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Improvement");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.improvement_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.bugButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.bug_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView3)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Bug");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.bug_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                this.feedbacking = true;
                return true;
            case R.id.donate /*2131034194*/:
                Toast.makeText(this.context, "Almost implemented yet", 0).show();
                return true;
            default:
                return false;
        }
    }

    public void submitClick(View button) {
        ArrayList<Object> arguments = (ArrayList) button.getTag();
        arguments.add(((EditText) ((RelativeLayout) button.getParent()).findViewById(R.id.text)).getText().toString());
        if (this.checked.get(0).equals("checked")) {
            arguments.add(this.mSettings.firstRunDate);
        } else {
            arguments.add("");
        }
        try {
            if (this.checked.get(1).equals("checked")) {
                arguments.add(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
            } else {
                arguments.add("");
            }
        } catch (PackageManager.NameNotFoundException e) {
            arguments.add("");
            e.printStackTrace();
        }
        if (this.checked.get(2).equals("checked")) {
            arguments.add(this.mSettings.username);
        } else {
            arguments.add("");
        }
        if (this.checked.get(3).equals("checked")) {
            View t = findViewById(R.id.home_root);
            t.setDrawingCacheEnabled(true);
            t.destroyDrawingCache();
            arguments.add(t.getDrawingCache());
        } else {
            arguments.add("");
        }
        if (this.checked.get(4).equals("checked")) {
            arguments.add(Build.PRODUCT);
        } else {
            arguments.add("");
        }
        if (this.checked.get(5).equals("checked")) {
            arguments.add(Build.VERSION.RELEASE);
        } else {
            arguments.add("");
        }
        new Feedback().execute(arguments);
        this.pw.dismiss();
    }

    class Feedback extends AsyncTask<ArrayList<Object>, Void, Boolean> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<Object>[]) ((ArrayList[]) objArr));
        }

        Feedback() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(ArrayList<Object>... passing) {
            return Boolean.valueOf(RedditAPI.feedback(passing[0]));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            Dashboard.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            Dashboard.this.stopLoading();
            if (result.booleanValue()) {
                Toast.makeText(Dashboard.this.context, "Feedback Submitted!\nThank you for your feedback! ", 0).show();
            } else {
                Toast.makeText(Dashboard.this.context, "Feedback Failed!", 0).show();
            }
        }
    }

    public void startLoading() {
        this.thread_count++;
        findViewById(R.id.refresh).setVisibility(8);
        findViewById(R.id.progressBar1).setVisibility(0);
    }

    public void stopLoading() {
        this.thread_count--;
        if (this.thread_count == 0) {
            findViewById(R.id.progressBar1).setVisibility(8);
            findViewById(R.id.refresh).setVisibility(0);
        }
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void onBrowseClick(View v) {
        try {
            if (this.alpha_testers.contains(this.mSettings.username.toUpperCase())) {
                startActivity(new Intent(this, Browse.class));
            }
        } catch (Exception e) {
            Toast.makeText(this.context, "You have to be logged in to use this function.", 0).show();
            Toast.makeText(this.context, "It will be usable without logging in when alpha is over", 0).show();
        }
    }

    public void onMessageClick(View v) {
        Toast.makeText(this.context, "Not implemented yet", 0).show();
    }

    public void onStarredClick(View v) {
        Toast.makeText(this.context, "Not implemented yet", 0).show();
    }

    public void onProfileClick(View v) {
        Toast.makeText(this.context, "Not implemented yet", 0).show();
    }

    public void onFollowingClick(View v) {
        Toast.makeText(this.context, "Not implemented yet", 0).show();
    }

    public void onLoginClick(View v) {
        if (!this.loggingin) {
            ((ScrollView) findViewById(R.id.login)).setVisibility(0);
            ((LinearLayout) findViewById(R.id.dashboard)).setVisibility(8);
            this.loggingin = true;
            return;
        }
        this.pos = Integer.parseInt(((View) v.getParent().getParent()).getTag().toString());
        ArrayList<String> passing = new ArrayList<>();
        passing.add(((EditText) findViewById(R.id.login_username)).getText().toString().trim());
        passing.add(((EditText) findViewById(R.id.login_password)).getText().toString());
        new Login().execute(passing);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void onBackPressed() {
        if (this.loggingin) {
            ((ScrollView) findViewById(R.id.login)).setVisibility(8);
            ((LinearLayout) findViewById(R.id.dashboard)).setVisibility(0);
            this.loggingin = false;
            ((TextView) findViewById(R.id.login_comment)).setText("Login to gather karma");
        } else if (this.feedbacking) {
            this.pw.dismiss();
            this.feedbacking = false;
        } else {
            finish();
        }
    }

    public void onMenuPressed() {
    }

    class Login extends AsyncTask<ArrayList<String>, Void, ArrayList<String>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<String>[]) ((ArrayList[]) objArr));
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((ArrayList<String>) ((ArrayList) obj));
        }

        Login() {
        }

        /* access modifiers changed from: protected */
        public ArrayList<String> doInBackground(ArrayList<String>... passing) {
            ArrayList<String> result = new ArrayList<>();
            if (!Dashboard.this.alpha_testers.contains(passing[0].get(0).toUpperCase())) {
                result.add("cancelled");
                cancel(true);
            } else if (RedditAPI.loginUser(passing[0].get(0), passing[0].get(1), Dashboard.this.mSettings, Dashboard.this.context)) {
                result.add("true");
            } else {
                result.add("false");
            }
            return result;
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            Dashboard.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(ArrayList<String> result) {
            try {
                if (!result.get(0).equals("cancelled")) {
                    if (Dashboard.this.mSettings.firstRun) {
                        Dashboard.this.mSettings.firstRun = false;
                        Calendar c = Calendar.getInstance();
                        Dashboard.this.mSettings.firstRunDate = String.valueOf(c.get(2) + 1) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + c.get(5) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + c.get(1);
                        Dashboard.this.mSettings.saveRedditPreferences(Dashboard.this.context);
                    }
                    if (result.get(0).equals("true")) {
                        ArrayList<String> passing = new ArrayList<>();
                        passing.add(((EditText) Dashboard.this.findViewById(R.id.login_username)).getText().toString());
                        new About().execute(passing);
                        ((ScrollView) Dashboard.this.findViewById(R.id.login)).setVisibility(8);
                        ((LinearLayout) Dashboard.this.findViewById(R.id.dashboard)).setVisibility(0);
                        ((RelativeLayout) Dashboard.this.mPager.getChildAt(Dashboard.this.pos).findViewById(R.id.account_info)).setVisibility(0);
                        ((LinearLayout) Dashboard.this.mPager.getChildAt(Dashboard.this.pos).findViewById(R.id.account_login)).setVisibility(8);
                        Dashboard.this.mSettings.username = ((EditText) Dashboard.this.findViewById(R.id.login_username)).getText().toString();
                        RedditAPI.mUsername = Dashboard.this.mSettings.username;
                    } else {
                        ((TextView) Dashboard.this.mPager.getChildAt(Dashboard.this.pos).findViewById(R.id.login_comment)).setText(RedditAPI.mUserError);
                    }
                    Dashboard.this.stopLoading();
                }
                Toast.makeText(Dashboard.this.context, "Sorry, you have not been added to the list", 0).show();
                Toast.makeText(Dashboard.this.context, "More people will be selected next week", 0).show();
                Dashboard.this.stopLoading();
            } catch (Exception e) {
                Toast.makeText(Dashboard.this.context, "Error, try again", 0).show();
            }
        }
    }

    class About extends AsyncTask<ArrayList<String>, Void, ArrayList<String>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<String>[]) ((ArrayList[]) objArr));
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((ArrayList<String>) ((ArrayList) obj));
        }

        About() {
        }

        /* access modifiers changed from: protected */
        public ArrayList<String> doInBackground(ArrayList<String>... passing) {
            new ArrayList();
            return RedditAPI.about(passing[0].get(0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            Dashboard.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(ArrayList<String> result) {
            try {
                if (!result.isEmpty() && (Dashboard.this.loggingin || Dashboard.refreshing)) {
                    ((TextView) Dashboard.this.mPager.getChildAt(Dashboard.this.pos).findViewById(R.id.account_info_id)).setText(result.get(0));
                    ((TextView) Dashboard.this.mPager.getChildAt(Dashboard.this.pos).findViewById(R.id.account_info_years)).setText("since " + Utils.getTimeAgo(Double.parseDouble(result.get(2))));
                    ((TextView) Dashboard.this.mPager.getChildAt(Dashboard.this.pos).findViewById(R.id.account_info_karma)).setText(String.valueOf(result.get(3)) + " / " + result.get(4));
                    Dashboard.this.loggingin = false;
                }
            } catch (Exception e) {
            }
            Dashboard.this.stopLoading();
        }
    }

    public static class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        public int getCount() {
            return 2;
        }

        public Fragment getItem(int position) {
            return AccountFragment.newInstance(position);
        }
    }

    public static class AccountFragment extends Fragment {
        int mNum;

        static AccountFragment newInstance(int num) {
            AccountFragment f = new AccountFragment();
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);
            return f;
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate((int) R.layout.account, container, false);
            v.setTag(Integer.valueOf(getArguments().getInt("num")));
            if (!(RedditAPI.mModhash == null || RedditAPI.mUsername == null)) {
                Dashboard.refreshing = true;
                if (getArguments().getInt("num") == 0) {
                    ((TextView) v.findViewById(R.id.account_info_id)).setText(RedditAPI.mUsername);
                    ((RelativeLayout) v.findViewById(R.id.account_info)).setVisibility(0);
                    ((LinearLayout) v.findViewById(R.id.account_login)).setVisibility(8);
                }
            }
            return v;
        }

        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
