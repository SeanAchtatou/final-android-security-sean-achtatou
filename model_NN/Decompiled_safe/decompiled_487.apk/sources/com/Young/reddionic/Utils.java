package com.Young.reddionic;

import android.util.Log;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class Utils {
    public static void CopyStream(InputStream is, OutputStream os) {
        try {
            Log.e("dd", "Copy Stream");
            byte[] bytes = new byte[1024];
            while (true) {
                int count = is.read(bytes, 0, 1024);
                if (count != -1) {
                    os.write(bytes, 0, count);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    /* JADX INFO: Multiple debug info for r4v1 long: [D('diff' long), D('utcTimeSeconds' long)] */
    public static String getTimeAgo(long utcTimeSeconds) {
        long utcTimeSeconds2 = (System.currentTimeMillis() / 1000) - utcTimeSeconds;
        if (utcTimeSeconds2 <= 0) {
            return "very recently";
        }
        if (utcTimeSeconds2 < 60) {
            if (utcTimeSeconds2 == 1) {
                return "1 second ago";
            }
            return String.valueOf(utcTimeSeconds2) + " seconds ago";
        } else if (utcTimeSeconds2 < 3600) {
            if (utcTimeSeconds2 / 60 == 1) {
                return "1 minute ago";
            }
            return String.valueOf(utcTimeSeconds2 / 60) + " minutes ago";
        } else if (utcTimeSeconds2 < 86400) {
            if (utcTimeSeconds2 / 3600 == 1) {
                return "1 hour ago";
            }
            return String.valueOf(utcTimeSeconds2 / 3600) + " hours ago";
        } else if (utcTimeSeconds2 < 604800) {
            if (utcTimeSeconds2 / 86400 == 1) {
                return "1 day ago";
            }
            return String.valueOf(utcTimeSeconds2 / 86400) + " days ago";
        } else if (utcTimeSeconds2 < 2592000) {
            if (utcTimeSeconds2 / 604800 == 1) {
                return "1 week ago";
            }
            return String.valueOf(utcTimeSeconds2 / 604800) + " weeks ago";
        } else if (utcTimeSeconds2 < 31536000) {
            if (utcTimeSeconds2 / 2592000 == 1) {
                return "1 month ago";
            }
            return String.valueOf(utcTimeSeconds2 / 2592000) + " months ago";
        } else if (utcTimeSeconds2 / 31536000 == 1) {
            return "1 year ago";
        } else {
            return String.valueOf(utcTimeSeconds2 / 31536000) + " years ago";
        }
    }

    public static String getTimeAgo(double utcTimeSeconds) {
        return getTimeAgo((long) utcTimeSeconds);
    }

    /* JADX INFO: Multiple debug info for r4v1 long: [D('diff' long), D('utcTimeSeconds' long)] */
    public static String getTimeAgo2(long utcTimeSeconds) {
        long utcTimeSeconds2 = (System.currentTimeMillis() / 1000) - utcTimeSeconds;
        if (utcTimeSeconds2 <= 0) {
            return "0.122s";
        }
        if (utcTimeSeconds2 < 60) {
            if (utcTimeSeconds2 == 1) {
                return "1s";
            }
            return String.valueOf(utcTimeSeconds2) + "s";
        } else if (utcTimeSeconds2 < 3600) {
            if (utcTimeSeconds2 / 60 == 1) {
                return "1m";
            }
            return String.valueOf(utcTimeSeconds2 / 60) + "m";
        } else if (utcTimeSeconds2 < 86400) {
            if (utcTimeSeconds2 / 3600 == 1) {
                return "1h";
            }
            return String.valueOf(utcTimeSeconds2 / 3600) + "h";
        } else if (utcTimeSeconds2 < 604800) {
            if (utcTimeSeconds2 / 86400 == 1) {
                return "1d";
            }
            return String.valueOf(utcTimeSeconds2 / 86400) + "d";
        } else if (utcTimeSeconds2 < 2592000) {
            if (utcTimeSeconds2 / 604800 == 1) {
                return "1w";
            }
            return String.valueOf(utcTimeSeconds2 / 604800) + "w";
        } else if (utcTimeSeconds2 < 31536000) {
            if (utcTimeSeconds2 / 2592000 == 1) {
                return "1mo";
            }
            return String.valueOf(utcTimeSeconds2 / 2592000) + "mo";
        } else if (utcTimeSeconds2 / 31536000 == 1) {
            return "1y";
        } else {
            return String.valueOf(utcTimeSeconds2 / 31536000) + "y";
        }
    }

    public static String getTimeAgo2(double utcTimeSeconds) {
        return getTimeAgo2((long) utcTimeSeconds);
    }

    public static ArrayList<String> convertHtmlTags(String html, int type) {
        ArrayList<String> result = new ArrayList<>();
        result.add(0, "null");
        String html2 = html.replaceAll("<code>", "<tt>").replaceAll("</code>", "</tt>").replaceAll("<div class=\"md\">", "").replaceAll("</div>", "");
        int preIndex = html2.indexOf("<pre>");
        int preEndIndex = -6;
        StringBuilder bodyConverted = new StringBuilder();
        while (preIndex != -1) {
            StringBuilder bodyConverted2 = bodyConverted.append(html2.substring(preEndIndex + 6, preIndex));
            preEndIndex = html2.indexOf("</pre>", preIndex);
            bodyConverted = bodyConverted2.append(html2.substring(preIndex, preEndIndex).replaceAll("\n", "<br>")).append("</pre>");
            preIndex = html2.indexOf("<pre>", preEndIndex);
        }
        String html3 = bodyConverted.append(html2.substring(preEndIndex + 6)).toString();
        int index = html3.indexOf("<ol> <li>");
        int index2 = html3.indexOf("</ol>", index);
        while (true) {
            int counter = 1;
            if (index == -1) {
                break;
            }
            String newString = html3.substring(index, index2);
            while (newString.contains("<li>")) {
                newString = newString.replaceFirst("<li>", String.valueOf(counter) + ". ").replaceFirst("</li>", "<br>");
                counter++;
            }
            html3 = String.valueOf(html3.substring(0, index)) + newString + html3.substring(index2);
            index = html3.indexOf("<ol> <li>");
            index2 = html3.indexOf("</ol>", index);
        }
        String html4 = html3.replaceAll("<li>", "* ").replaceAll("</li>", "<br>").replaceAll("<blockquote><p>", "<font color='#770000'> '").replaceAll("</p></blockquote>", "'</font><br><br>").replaceAll("<strong>", "<b>").replaceAll("</strong>", "</b>").replaceAll("<em>", "<i>").replaceAll("</em>", "</i>");
        if (type == 0) {
            int index3 = html4.indexOf("<a href");
            while (index3 != -1) {
                int index22 = html4.indexOf(">", index3);
                result.add(html4.substring(index3 + 9, index22 - 2));
                Log.e("Linking1", html4.substring(index3 + 9, index22 - 2));
                int index32 = html4.indexOf("</a>", index22);
                Log.e("Linking2", new StringBuilder(String.valueOf(index32)).toString());
                result.add(html4.substring(index22 + 1, index32));
                Log.e("Linking2", html4.substring(index22 + 1, index32));
                Log.e("Linking3", String.valueOf(html4.substring(0, index3)) + "<font color='#005e9f'>" + html4.substring(index22 + 1, index32) + "</font>" + html4.substring(index32 + 4));
                html4 = String.valueOf(html4.substring(0, index3)) + "<font color='#005e9f'>" + html4.substring(index22 + 1, index32) + "</font>" + html4.substring(index32 + 4);
                index3 = html4.indexOf("<a href");
            }
        }
        result.set(0, html4);
        return result;
    }
}
