package com.Young.reddionic;

public final class R {

    public static final class anim {
        public static final int layout_up_animation = 2130968576;
    }

    public static final class attr {
        public static final int allowSingleTap = 2130771973;
        public static final int animateOnClick = 2130771974;
        public static final int bottomOffset = 2130771971;
        public static final int content = 2130771970;
        public static final int direction = 2130771968;
        public static final int dragndrop_background = 2130771978;
        public static final int expanded_height = 2130771976;
        public static final int grabber = 2130771977;
        public static final int handle = 2130771969;
        public static final int normal_height = 2130771975;
        public static final int remove_mode = 2130771979;
        public static final int topOffset = 2130771972;
    }

    public static final class color {
        public static final int error_message = 2131099649;
        public static final int screen_background = 2131099648;
    }

    public static final class drawable {
        public static final int action_item_btn = 2130837504;
        public static final int action_item_selected = 2130837505;
        public static final int androids = 2130837506;
        public static final int background = 2130837507;
        public static final int background_new = 2130837508;
        public static final int background_selected = 2130837509;
        public static final int background_selector = 2130837510;
        public static final int background_white = 2130837511;
        public static final int backgroundse = 2130837512;
        public static final int bacon = 2130837513;
        public static final int bar = 2130837514;
        public static final int blue_button = 2130837515;
        public static final int bottom = 2130837516;
        public static final int button = 2130837517;
        public static final int button9p = 2130837518;
        public static final int button_old = 2130837519;
        public static final int button_white = 2130837520;
        public static final int check = 2130837521;
        public static final int coffee = 2130837522;
        public static final int comment_white = 2130837523;
        public static final int crown = 2130837524;
        public static final int darkgo = 2130837525;
        public static final int darkgray = 2130837526;
        public static final int darkgroup = 2130837527;
        public static final int darkplus = 2130837528;
        public static final int divider = 2130837529;
        public static final int divider2 = 2130837530;
        public static final int divider3 = 2130837531;
        public static final int divider_h = 2130837532;
        public static final int divider_vertical_dark = 2130837533;
        public static final int donation_1 = 2130837534;
        public static final int donation_10 = 2130837535;
        public static final int donation_2 = 2130837536;
        public static final int donation_25 = 2130837537;
        public static final int donation_5 = 2130837538;
        public static final int donation_50 = 2130837539;
        public static final int donation_bg = 2130837540;
        public static final int downarrow = 2130837541;
        public static final int downbutton = 2130837542;
        public static final int downvote = 2130837543;
        public static final int downvote_s = 2130837544;
        public static final int frame = 2130837545;
        public static final int go = 2130837546;
        public static final int go_arrow = 2130837547;
        public static final int grabber = 2130837548;
        public static final int gum = 2130837549;
        public static final int header = 2130837550;
        public static final int header_divider = 2130837551;
        public static final int hide_ = 2130837552;
        public static final int home_browse = 2130837553;
        public static final int home_following = 2130837554;
        public static final int home_message = 2130837555;
        public static final int home_profile = 2130837556;
        public static final int home_star = 2130837557;
        public static final int ic_menu_back = 2130837558;
        public static final int ic_menu_browser = 2130837559;
        public static final int ic_menu_compose = 2130837560;
        public static final int ic_menu_refresh = 2130837561;
        public static final int ic_menu_start_conversation = 2130837562;
        public static final int ic_more = 2130837563;
        public static final int icon = 2130837564;
        public static final int indent = 2130837565;
        public static final int indent10 = 2130837566;
        public static final int indent2 = 2130837567;
        public static final int indent3 = 2130837568;
        public static final int indent4 = 2130837569;
        public static final int indent5 = 2130837570;
        public static final int indent6 = 2130837571;
        public static final int indent7 = 2130837572;
        public static final int indent8 = 2130837573;
        public static final int indent9 = 2130837574;
        public static final int keyboard = 2130837575;
        public static final int light_blue_button = 2130837576;
        public static final int light_gray = 2130837577;
        public static final int lighter_gray = 2130837578;
        public static final int list_background = 2130837579;
        public static final int list_selector_blue_longpress = 2130837580;
        public static final int list_selector_blue_pressed = 2130837581;
        public static final int list_selector_blue_transition = 2130837582;
        public static final int logo = 2130837583;
        public static final int logo_03 = 2130837584;
        public static final int mail = 2130837585;
        public static final int mail_big = 2130837586;
        public static final int mailgray = 2130837587;
        public static final int market = 2130837588;
        public static final int menu = 2130837589;
        public static final int noarrow = 2130837590;
        public static final int patch_blue = 2130837591;
        public static final int patch_bottom = 2130837592;
        public static final int patch_buttons = 2130837593;
        public static final int patch_buttons2 = 2130837594;
        public static final int patch_red = 2130837595;
        public static final int patch_text2 = 2130837596;
        public static final int patch_transparent = 2130837597;
        public static final int picture = 2130837598;
        public static final int plus = 2130837599;
        public static final int popup = 2130837600;
        public static final int popup_bg = 2130837601;
        public static final int processbar = 2130837602;
        public static final int profile = 2130837603;
        public static final int red_button = 2130837604;
        public static final int reddit = 2130837605;
        public static final int refresh = 2130837606;
        public static final int reply = 2130837607;
        public static final int selected_text = 2130837608;
        public static final int selector = 2130837609;
        public static final int self2 = 2130837610;
        public static final int selftext_border = 2130837611;
        public static final int share = 2130837612;
        public static final int share_ = 2130837613;
        public static final int star = 2130837614;
        public static final int star_ = 2130837615;
        public static final int stub = 2130837616;
        public static final int subreddit_not_selected = 2130837617;
        public static final int subreddit_selected = 2130837618;
        public static final int subreddit_selector = 2130837619;
        public static final int text = 2130837620;
        public static final int text9p = 2130837621;
        public static final int thumbnail_background_light = 2130837622;
        public static final int time = 2130837623;
        public static final int transparent_buttons = 2130837624;
        public static final int transparentpatch = 2130837625;
        public static final int uparrow = 2130837626;
        public static final int upbutton = 2130837627;
        public static final int upvote = 2130837628;
        public static final int upvote_s = 2130837629;
        public static final int upvote_white = 2130837630;
        public static final int video = 2130837631;
        public static final int vote_bg = 2130837632;
        public static final int vote_frame = 2130837633;
        public static final int webhosting = 2130837634;
        public static final int white_text = 2130837635;
        public static final int x = 2130837636;
    }

    public static final class id {
        public static final int Button01 = 2131034130;
        public static final int Button02 = 2131034131;
        public static final int Button03 = 2131034133;
        public static final int CommentLinearLayout = 2131034154;
        public static final int GeneralInfo = 2131034199;
        public static final int LinearLayout04 = 2131034210;
        public static final int LinearLayout05 = 2131034202;
        public static final int RelativeLayout02 = 2131034129;
        public static final int RelativeLayout02_ref = 2131034179;
        public static final int ThreadLinearLayout = 2131034237;
        public static final int account_info = 2131034123;
        public static final int account_info_id = 2131034124;
        public static final int account_info_karma = 2131034127;
        public static final int account_info_years = 2131034125;
        public static final int account_login = 2131034121;
        public static final int accountpager = 2131034183;
        public static final int actionbar = 2131034144;
        public static final int add_subreddit = 2131034140;
        public static final int author = 2131034208;
        public static final int before_root = 2131034156;
        public static final int bottomToTop = 2131034113;
        public static final int browse_main = 2131034143;
        public static final int bugButton = 2131034224;
        public static final int bug_feedback = 2131034231;
        public static final int button03 = 2131034132;
        public static final int button1 = 2131034258;
        public static final int button_feedback = 2131034221;
        public static final int button_subreddit = 2131034136;
        public static final int buttons = 2131034168;
        public static final int check = 2131034214;
        public static final int comment_text = 2131034166;
        public static final int comments = 2131034249;
        public static final int comments_list = 2131034180;
        public static final int comments_root = 2131034174;
        public static final int container = 2131034205;
        public static final int content = 2131034147;
        public static final int dashboard = 2131034187;
        public static final int divider = 2131034157;
        public static final int donate = 2131034194;
        public static final int down = 2131034170;
        public static final int downButton = 2131034251;
        public static final int drawer = 2131034146;
        public static final int error_console = 2131034240;
        public static final int feature_request = 2131034195;
        public static final int feedback = 2131034193;
        public static final int first = 2131034245;
        public static final int fling = 2131034117;
        public static final int footer = 2131034234;
        public static final int fullscreen_custom_content = 2131034239;
        public static final int group_subreddit = 2131034141;
        public static final int handle = 2131034148;
        public static final int handle_current = 2131034150;
        public static final int handle_next = 2131034151;
        public static final int handle_previous = 2131034149;
        public static final int home_btn_browse = 2131034188;
        public static final int home_btn_following = 2131034192;
        public static final int home_btn_message = 2131034189;
        public static final int home_btn_profile = 2131034191;
        public static final int home_btn_starred = 2131034190;
        public static final int home_root = 2131034181;
        public static final int icon = 2131034153;
        public static final int id = 2131034160;
        public static final int image = 2131034220;
        public static final int imageView1 = 2131034126;
        public static final int improvementButton = 2131034223;
        public static final int improvement_feedback = 2131034228;
        public static final int included = 2131034227;
        public static final int included2 = 2131034229;
        public static final int included3 = 2131034232;
        public static final int indent = 2131034155;
        public static final int info = 2131034206;
        public static final int iv = 2131034211;
        public static final int label = 2131034213;
        public static final int leftToRight = 2131034114;
        public static final int level = 2131034159;
        public static final int linearLayout1 = 2131034247;
        public static final int linearLayout2 = 2131034178;
        public static final int link_root = 2131034215;
        public static final int listView1 = 2131034137;
        public static final int listView2 = 2131034230;
        public static final int listView3 = 2131034233;
        public static final int loadingText = 2131034236;
        public static final int login = 2131034182;
        public static final int login_comment = 2131034122;
        public static final int login_password = 2131034185;
        public static final int login_username = 2131034184;
        public static final int main_content = 2131034241;
        public static final int message = 2131034142;
        public static final int mockview = 2131034218;
        public static final int mockview_bar_root = 2131034219;
        public static final int mockview_root = 2131034217;
        public static final int more = 2131034235;
        public static final int myselector = 2131034261;
        public static final int name = 2131034139;
        public static final int none = 2131034116;
        public static final int otherinfo = 2131034209;
        public static final int pager = 2131034145;
        public static final int points = 2131034248;
        public static final int post_info = 2131034176;
        public static final int preview = 2131034164;
        public static final int previewWeb = 2131034216;
        public static final int profile = 2131034173;
        public static final int progressBar1 = 2131034134;
        public static final int progress_indicator = 2131034238;
        public static final int refresh = 2131034135;
        public static final int reply = 2131034165;
        public static final int requestButton = 2131034222;
        public static final int request_feedback = 2131034225;
        public static final int rightToLeft = 2131034112;
        public static final int score = 2131034161;
        public static final int second = 2131034252;
        public static final int selftext = 2131034177;
        public static final int share = 2131034172;
        public static final int slide = 2131034118;
        public static final int slideLeft = 2131034120;
        public static final int slideRight = 2131034119;
        public static final int star = 2131034171;
        public static final int submit = 2131034167;
        public static final int submitButton = 2131034226;
        public static final int subreddit = 2131034152;
        public static final int te2 = 2131034246;
        public static final int text = 2131034163;
        public static final int textView1 = 2131034128;
        public static final int textView2 = 2131034186;
        public static final int textView3 = 2131034255;
        public static final int textView4 = 2131034256;
        public static final int textView6 = 2131034260;
        public static final int textView7 = 2131034257;
        public static final int theView = 2131034175;
        public static final int thumbnail = 2131034201;
        public static final int time = 2131034162;
        public static final int title = 2131034207;
        public static final int topToBottom = 2131034115;
        public static final int topinfo = 2131034158;
        public static final int tv = 2131034212;
        public static final int up = 2131034169;
        public static final int upButton = 2131034250;
        public static final int visit_subreddit = 2131034138;
        public static final int vote = 2131034204;
        public static final int voteimg = 2131034203;
        public static final int webView = 2131034244;
        public static final int webv1 = 2131034196;
        public static final int webv2 = 2131034197;
        public static final int webv3 = 2131034198;
        public static final int webview_root = 2131034242;
        public static final int webview_topbar = 2131034243;
        public static final int welcome_1 = 2131034254;
        public static final int welcome_2 = 2131034259;
        public static final int welcome_root = 2131034253;
        public static final int wrap = 2131034200;
    }

    public static final class layout {
        public static final int account = 2130903040;
        public static final int action_bar = 2130903041;
        public static final int actionbar = 2130903042;
        public static final int add_subreddit_popup = 2130903043;
        public static final int billing_not_supported = 2130903044;
        public static final int browse = 2130903045;
        public static final int comment_list_item = 2130903046;
        public static final int comments = 2130903047;
        public static final int commentslist = 2130903048;
        public static final int dashboard = 2130903049;
        public static final int donate = 2130903050;
        public static final int feedback_menu = 2130903051;
        public static final int feedback_popup = 2130903052;
        public static final int general_info = 2130903053;
        public static final int included_list_item = 2130903054;
        public static final int last_comment = 2130903055;
        public static final int link = 2130903056;
        public static final int mockview = 2130903057;
        public static final int mockview_bar = 2130903058;
        public static final int r_feedback_popup = 2130903059;
        public static final int subreddit_list_footer = 2130903060;
        public static final int subreddit_list_item = 2130903061;
        public static final int thread_list = 2130903062;
        public static final int thread_list_item = 2130903063;
        public static final int video_loading_progress = 2130903064;
        public static final int web_screen = 2130903065;
        public static final int webview = 2130903066;
        public static final int webview_bar = 2130903067;
        public static final int welcome = 2130903068;
    }

    public static final class menu {
        public static final int menu = 2131296256;
    }

    public static final class string {
        public static final int android_test_canceled = 2131165204;
        public static final int android_test_item_unavailable = 2131165206;
        public static final int android_test_purchased = 2131165205;
        public static final int android_test_refunded = 2131165207;
        public static final int app_name = 2131165185;
        public static final int billing_not_supported_message = 2131165187;
        public static final int billing_not_supported_title = 2131165186;
        public static final int buy = 2131165193;
        public static final int cannot_connect_message = 2131165189;
        public static final int cannot_connect_title = 2131165188;
        public static final int edit_payload = 2131165195;
        public static final int edit_payload_accept = 2131165197;
        public static final int edit_payload_clear = 2131165198;
        public static final int edit_payload_title = 2131165196;
        public static final int hello = 2131165184;
        public static final int help_url = 2131165192;
        public static final int items_for_sale = 2131165199;
        public static final int items_you_own = 2131165200;
        public static final int learn_more = 2131165191;
        public static final int potions = 2131165203;
        public static final int recent_transactions = 2131165201;
        public static final int restoring_transactions = 2131165190;
        public static final int select_item = 2131165194;
        public static final int two_handed_sword = 2131165202;
    }

    public static final class style {
        public static final int HomeButton = 2131230722;
        public static final int Theme = 2131230720;
        public static final int Theme_IOSched = 2131230721;
    }

    public static final class styleable {
        public static final int[] MultiDirectionSlidingDrawer = {R.attr.direction, R.attr.handle, R.attr.content, R.attr.bottomOffset, R.attr.topOffset, R.attr.allowSingleTap, R.attr.animateOnClick};
        public static final int MultiDirectionSlidingDrawer_allowSingleTap = 5;
        public static final int MultiDirectionSlidingDrawer_animateOnClick = 6;
        public static final int MultiDirectionSlidingDrawer_bottomOffset = 3;
        public static final int MultiDirectionSlidingDrawer_content = 2;
        public static final int MultiDirectionSlidingDrawer_direction = 0;
        public static final int MultiDirectionSlidingDrawer_handle = 1;
        public static final int MultiDirectionSlidingDrawer_topOffset = 4;
        public static final int[] TouchListView = {R.attr.normal_height, R.attr.expanded_height, R.attr.grabber, R.attr.dragndrop_background, R.attr.remove_mode};
        public static final int TouchListView_dragndrop_background = 3;
        public static final int TouchListView_expanded_height = 1;
        public static final int TouchListView_grabber = 2;
        public static final int TouchListView_normal_height = 0;
        public static final int TouchListView_remove_mode = 4;
    }
}
