package com.Young.reddionic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.ArrayList;

public class Webview extends FragmentActivity {
    static Activity act;
    static int liked = 0;
    static boolean refreshing = false;
    static int score = -1;
    ActionBar actionbar;
    ArrayList<String> checked = new ArrayList<>();
    Context context;
    DisplayMetrics dm;
    boolean feedbacking = false;
    boolean hidden = false;
    ArrayAdapter<String> includedArrayAdapter;
    boolean loggingin = false;
    MyAdapter mAdapter;
    ViewPager mPager;
    private final RedditSettings mSettings = new RedditSettings();
    ArrayList<String> names = new ArrayList<>();
    int pos = 0;
    PopupWindow pw;
    boolean saved = false;
    boolean submitSuccessful = false;
    View theActionBar;
    int thread_count = 0;
    GoogleAnalyticsTracker tracker;
    WebView webView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.webview);
        this.dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        this.context = getApplicationContext();
        act = this;
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-17867595-4", this);
        this.tracker.trackPageView("/WebView");
        CookieSyncManager.createInstance(getApplicationContext());
        this.mSettings.loadRedditPreferences(getApplicationContext());
        this.mAdapter = new MyAdapter(getSupportFragmentManager());
        this.mPager = (ViewPager) findViewById(R.id.webview_topbar);
        this.mPager.setAdapter(this.mAdapter);
        this.webView = (WebView) findViewById(R.id.webView);
        this.webView.setWebViewClient(new HelloWebViewClient(this, null));
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setUserAgent(0);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setLoadWithOverviewMode(true);
        this.webView.getSettings().setAppCacheEnabled(true);
        this.webView.getSettings().setDomStorageEnabled(true);
        this.webView.getSettings().setAllowFileAccess(true);
        this.webView.getSettings().setSupportMultipleWindows(true);
        this.webView.getSettings().setCacheMode(1);
        this.webView.getSettings().setAppCachePath("/sdcard/FolderName/.cache");
        this.webView.getSettings().getJavaScriptCanOpenWindowsAutomatically();
        this.webView.setInitialScale(70);
        this.webView.canGoBack();
        this.webView.goBack();
        this.webView.setLongClickable(true);
        this.webView.setTag((ProgressBar) findViewById(R.id.progressBar1));
        String url = getIntent().getStringExtra("url");
        if (url.contains("youtube")) {
            this.webView.setInitialScale(40);
            int index1 = url.indexOf("?v=");
            int index2 = url.indexOf("&", index1);
            try {
                this.webView.getSettings().setPluginsEnabled(true);
                this.webView.getSettings().setUseWideViewPort(false);
                if (index2 != -1) {
                    this.webView.loadDataWithBaseURL(url, "<iframe width=\"100%\" height=\"50%\" src=\"http://www.youtube.com/embed/" + url.substring(index1 + 3, index2) + "\" frameborder=\"0\" allowfullscreen></iframe>", "text/html", "utf-8", url);
                } else {
                    this.webView.loadDataWithBaseURL(url, "<iframe width=\"100%\" height=\"50%\" src=\"http://www.youtube.com/embed/" + url.substring(index1 + 3) + "\" frameborder=\"0\" allowfullscreen></iframe>", "text/html", "utf-8", url);
                }
                this.webView.setInitialScale(210);
                this.webView.getSettings().setBuiltInZoomControls(false);
            } catch (Exception e) {
            }
        } else {
            this.webView.loadUrl(getIntent().getStringExtra("url"));
        }
        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                ((ProgressBar) view.getTag()).setProgress(progress);
                if (progress == 100) {
                    ((ProgressBar) view.getTag()).setVisibility(8);
                } else if (((ProgressBar) view.getTag()).getVisibility() == 8 && progress < 100) {
                    ((ProgressBar) view.getTag()).setVisibility(0);
                }
            }
        });
    }

    class FeedbackAdapter extends ArrayAdapter<String> {
        FeedbackAdapter() {
            super(Webview.this.context, (int) R.layout.included_list_item, Webview.this.names);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = Webview.this.getLayoutInflater().inflate((int) R.layout.included_list_item, parent, false);
            }
            TextView label = (TextView) row.findViewById(R.id.label);
            label.setText(Webview.this.names.get(position));
            label.setTag(Integer.valueOf(position));
            CheckBox checkbox = (CheckBox) row.findViewById(R.id.check);
            if (Webview.this.checked.get(position).equals("")) {
                checkbox.setChecked(false);
            } else {
                checkbox.setChecked(true);
            }
            checkbox.setTag(Integer.valueOf(position));
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* Debug info: failed to restart local var, previous not found, register: 3 */
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Webview.this.checked.set(((Integer) buttonView.getTag()).intValue(), "checked");
                    } else {
                        Webview.this.checked.set(((Integer) buttonView.getTag()).intValue(), "");
                    }
                }
            });
            return row;
        }
    }

    public void startLoading() {
        this.thread_count++;
        findViewById(R.id.refresh).setVisibility(8);
        findViewById(R.id.progressBar1).setVisibility(0);
    }

    public void stopLoading() {
        this.thread_count--;
        if (this.thread_count == 0) {
            findViewById(R.id.progressBar1).setVisibility(8);
            findViewById(R.id.refresh).setVisibility(0);
        }
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(Webview webview, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished() {
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.feedback:
                View popup = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.r_feedback_popup, (ViewGroup) null, false);
                this.pw = new PopupWindow(popup, -1, -1, true);
                if (this.names.size() == 0) {
                    this.names.add("Date of the first run");
                    this.names.add("Version Number");
                    this.names.add("Reddit ID");
                    this.names.add("Current View");
                    this.names.add("Device Name");
                    this.names.add("Device SDK");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                    this.checked.add("checked");
                } else {
                    for (int x = 0; x < this.checked.size(); x++) {
                        this.checked.set(x, "checked");
                    }
                }
                this.pw.setBackgroundDrawable(new BitmapDrawable());
                this.pw.showAtLocation(findViewById(R.id.webview_root), 0, 0, 0);
                ((LinearLayout) popup.findViewById(R.id.requestButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.request_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView1)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Request");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.request_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.improvementButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.improvement_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView2)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Improvement");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.improvement_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                ((LinearLayout) popup.findViewById(R.id.bugButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        View temp = (View) v.getParent().getParent().getParent();
                        ((RelativeLayout) temp.findViewById(R.id.bug_feedback)).setVisibility(0);
                        ((LinearLayout) temp.findViewById(R.id.button_feedback)).setVisibility(8);
                        ((ListView) temp.findViewById(R.id.listView3)).setAdapter((ListAdapter) new FeedbackAdapter());
                        ArrayList<String> arguments = new ArrayList<>();
                        arguments.add("Bug");
                        ((Button) ((RelativeLayout) temp.findViewById(R.id.bug_feedback)).findViewById(R.id.submitButton)).setTag(arguments);
                    }
                });
                this.feedbacking = true;
                return true;
            case R.id.donate:
                Toast.makeText(this.context, "Almost implemented yet", 1).show();
                return true;
            default:
                return false;
        }
    }

    public void submitClick(View button) {
        ArrayList<Object> arguments = (ArrayList) button.getTag();
        arguments.add(((EditText) ((RelativeLayout) button.getParent()).findViewById(R.id.text)).getText().toString());
        if (this.checked.get(0).equals("checked")) {
            arguments.add(this.mSettings.firstRunDate);
        } else {
            arguments.add("");
        }
        try {
            if (this.checked.get(1).equals("checked")) {
                arguments.add(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
            } else {
                arguments.add("");
            }
        } catch (PackageManager.NameNotFoundException e) {
            arguments.add("");
            e.printStackTrace();
        }
        if (this.checked.get(2).equals("checked")) {
            arguments.add(this.mSettings.username);
        } else {
            arguments.add("");
        }
        if (this.checked.get(3).equals("checked")) {
            View t = findViewById(R.id.webview_root);
            t.setDrawingCacheEnabled(true);
            t.destroyDrawingCache();
            arguments.add(t.getDrawingCache());
        } else {
            arguments.add("");
        }
        if (this.checked.get(4).equals("checked")) {
            arguments.add(Build.PRODUCT);
        } else {
            arguments.add("");
        }
        if (this.checked.get(5).equals("checked")) {
            arguments.add(Build.VERSION.RELEASE);
        } else {
            arguments.add("");
        }
        new Feedback().execute(arguments);
        this.pw.dismiss();
    }

    public void onBackPressed() {
        if (this.webView.canGoBack()) {
            this.webView.goBack();
        } else if (this.feedbacking) {
            this.pw.dismiss();
            this.feedbacking = false;
        } else {
            this.webView.loadData("", "text/html", "utf-8");
            finish();
            super.onBackPressed();
        }
    }

    class Feedback extends AsyncTask<ArrayList<Object>, Void, Boolean> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<Object>[]) ((ArrayList[]) objArr));
        }

        Feedback() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(ArrayList<Object>... passing) {
            return Boolean.valueOf(RedditAPI.feedback(passing[0]));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            Webview.this.startLoading();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            Webview.this.stopLoading();
            if (result.booleanValue()) {
                Toast.makeText(Webview.this.context, "Feedback Submitted!\nThank you for your feedback! ", 1).show();
            } else {
                Toast.makeText(Webview.this.context, "Feedback Failed!", 1).show();
            }
        }
    }

    public static class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        public int getCount() {
            return 2;
        }

        public Fragment getItem(int position) {
            return WebviewFragment.newInstance(position);
        }
    }

    public static class WebviewFragment extends Fragment {
        int mNum;

        static WebviewFragment newInstance(int num) {
            WebviewFragment f = new WebviewFragment();
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);
            return f;
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate((int) R.layout.webview_bar, container, false);
            v.setTag(Integer.valueOf(getArguments().getInt("num")));
            if (getArguments().getInt("num") == 0) {
                Webview.score = Integer.parseInt(Webview.act.getIntent().getStringExtra("points"));
                if (Webview.act.getIntent().getStringExtra("likes").equals("null")) {
                    Webview.liked = 0;
                } else if (Webview.act.getIntent().getStringExtra("likes").equals("true")) {
                    Webview.liked = 1;
                } else {
                    Webview.liked = -1;
                }
                ((TextView) v.findViewById(R.id.title)).setText(Webview.act.getIntent().getStringExtra("title"));
                ((TextView) v.findViewById(R.id.points)).setText(Webview.act.getIntent().getStringExtra("points"));
                ((TextView) v.findViewById(R.id.comments)).setText(Webview.act.getIntent().getStringExtra("comments"));
                if (Webview.liked == 1) {
                    ((ImageButton) v.findViewById(R.id.upButton)).setImageResource(R.drawable.uparrow);
                } else if (Webview.liked == -1) {
                    ((ImageButton) v.findViewById(R.id.downButton)).setImageResource(R.drawable.downarrow);
                }
            } else {
                ((LinearLayout) v.findViewById(R.id.second)).setVisibility(0);
                ((RelativeLayout) v.findViewById(R.id.first)).setVisibility(8);
            }
            return v;
        }

        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }
    }

    public void onUpClick(View button) {
        ArrayList<String> arg = new ArrayList<>();
        View temp = (View) button.getParent().getParent().getParent();
        if (liked == 0) {
            liked = 1;
            score++;
            arg.add(act.getIntent().getStringExtra("subreddit"));
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("1");
            ((ImageView) button).setImageResource(R.drawable.uparrow);
        } else if (liked == 1) {
            score--;
            arg.add(act.getIntent().getStringExtra("subreddit"));
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("0");
            liked = 0;
            ((ImageView) button).setImageResource(R.drawable.upbutton);
        } else {
            score += 2;
            arg.add(act.getIntent().getStringExtra("subreddit"));
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("1");
            liked = 1;
            ((ImageView) button).setImageResource(R.drawable.uparrow);
            ((ImageView) temp.findViewById(R.id.downButton)).setImageResource(R.drawable.downbutton);
        }
        ((TextView) temp.findViewById(R.id.points)).setText(new StringBuilder(String.valueOf(score)).toString());
        new Voter().execute(arg);
    }

    public void onRefresh(View button) {
        this.webView.reload();
    }

    public void onUserAgent(View button) {
        if (this.webView.getSettings().getUserAgent() == 0) {
            this.webView.getSettings().setUserAgent(1);
        } else {
            this.webView.getSettings().setUserAgent(0);
        }
        this.webView.reload();
    }

    public void onHide(View button) {
        ArrayList<String> arg = new ArrayList<>();
        if (!this.hidden) {
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("hide");
            this.hidden = true;
        } else {
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("unhide");
            this.hidden = false;
        }
        new Hider().execute(arg);
    }

    public void onSave(View button) {
        ArrayList<String> arg = new ArrayList<>();
        if (!this.hidden) {
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("save");
            this.saved = true;
        } else {
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("unsave");
            this.saved = false;
        }
        new Saver().execute(arg);
    }

    public void onShare(View button) {
        Intent shareIntent = new Intent("android.intent.action.SEND");
        shareIntent.setType("text/plain");
        shareIntent.putExtra("android.intent.extra.SUBJECT", act.getIntent().getStringExtra("title"));
        shareIntent.putExtra("android.intent.extra.TEXT", act.getIntent().getStringExtra("url"));
        startActivity(Intent.createChooser(shareIntent, "Share"));
    }

    public void onComment(View button) {
    }

    public void onDownClick(View button) {
        ArrayList<String> arg = new ArrayList<>();
        View temp = (View) button.getParent().getParent().getParent();
        if (liked == 0) {
            liked = -1;
            score--;
            arg.add(act.getIntent().getStringExtra("subreddit"));
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("-1");
            ((ImageView) button).setImageResource(R.drawable.downarrow);
        } else if (liked == 1) {
            score -= 2;
            arg.add(act.getIntent().getStringExtra("subreddit"));
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("-1");
            liked = -1;
            ((ImageView) button).setImageResource(R.drawable.downarrow);
            ((ImageView) temp.findViewById(R.id.upButton)).setImageResource(R.drawable.upbutton);
        } else {
            score++;
            arg.add(act.getIntent().getStringExtra("subreddit"));
            arg.add(act.getIntent().getStringExtra("name"));
            arg.add("0");
            liked = 0;
            ((ImageView) button).setImageResource(R.drawable.downbutton);
        }
        ((TextView) temp.findViewById(R.id.points)).setText(new StringBuilder(String.valueOf(score)).toString());
        new Voter().execute(arg);
    }

    class Voter extends AsyncTask<ArrayList<String>, Void, Void> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<String>[]) ((ArrayList[]) objArr));
        }

        Voter() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(ArrayList<String>... arg0) {
            RedditAPI.vote(arg0[0].get(0), arg0[0].get(1), Integer.parseInt(arg0[0].get(2)));
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
        }
    }

    class Saver extends AsyncTask<ArrayList<String>, Void, Void> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<String>[]) ((ArrayList[]) objArr));
        }

        Saver() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(ArrayList<String>... arg0) {
            RedditAPI.save(arg0[0].get(0), arg0[0].get(1));
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
        }
    }

    class Hider extends AsyncTask<ArrayList<String>, Void, Void> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
            return doInBackground((ArrayList<String>[]) ((ArrayList[]) objArr));
        }

        Hider() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(ArrayList<String>... arg0) {
            RedditAPI.hide(arg0[0].get(0), arg0[0].get(1));
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
        }
    }
}
