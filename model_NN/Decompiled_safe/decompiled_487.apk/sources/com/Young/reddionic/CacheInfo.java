package com.Young.reddionic;

import android.content.Context;
import android.util.Log;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class CacheInfo implements Serializable {
    static final Object CACHE_LOCK = new Object();
    static final String TAG = "CacheInfo";
    static final long serialVersionUID = 39;
    public ArrayList<String> subredditList = null;
    public long subredditListTime = 0;
    public long subredditTime = 0;
    public String subredditUrl = null;
    public long threadTime = 0;
    public String threadUrl = null;

    static FileInputStream writeThenRead(Context context, InputStream in, String filename) throws IOException {
        synchronized (CACHE_LOCK) {
            FileOutputStream fos = context.openFileOutput(filename, 0);
            byte[] buf = new byte[1024];
            long total = 0;
            while (true) {
                int len = in.read(buf);
                if (len <= 0) {
                    Log.d(TAG, String.valueOf(total) + " bytes written to cache file: " + filename);
                    fos.close();
                    in.close();
                } else {
                    fos.write(buf, 0, len);
                    total += (long) len;
                }
            }
        }
        return context.openFileInput(filename);
    }

    static boolean checkFreshSubredditCache(Context context) {
        return System.currentTimeMillis() - getCachedSubredditTime(context) <= 1800000;
    }

    static boolean checkFreshThreadCache(Context context) {
        return System.currentTimeMillis() - getCachedThreadTime(context) <= 1800000;
    }

    static boolean checkFreshSubredditListCache(Context context) {
        return System.currentTimeMillis() - getCachedSubredditListTime(context) <= 86400000;
    }

    static CacheInfo getCacheInfo(Context context) throws IOException, ClassNotFoundException {
        CacheInfo ci;
        synchronized (CACHE_LOCK) {
            FileInputStream fis = context.openFileInput("cacheinfo.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ci = (CacheInfo) ois.readObject();
            ois.close();
            fis.close();
        }
        return ci;
    }

    static String getCachedSubredditUrl(Context context) {
        try {
            return getCacheInfo(context).subredditUrl;
        } catch (Exception e) {
            Log.e(TAG, "error w/ getCacheInfo", e);
            return null;
        }
    }

    static long getCachedSubredditTime(Context context) {
        try {
            return getCacheInfo(context).subredditTime;
        } catch (Exception e) {
            Log.e(TAG, "error w/ getCacheInfo", e);
            return 0;
        }
    }

    static String getCachedThreadUrl(Context context) {
        try {
            return getCacheInfo(context).threadUrl;
        } catch (Exception e) {
            Log.e(TAG, "error w/ getCacheInfo", e);
            return null;
        }
    }

    static long getCachedThreadTime(Context context) {
        try {
            return getCacheInfo(context).threadTime;
        } catch (Exception e) {
            Log.e(TAG, "error w/ getCacheInfo", e);
            return 0;
        }
    }

    static ArrayList<String> getCachedSubredditList(Context context) {
        try {
            return getCacheInfo(context).subredditList;
        } catch (Exception e) {
            Log.e(TAG, "error w/ getCacheInfo", e);
            return null;
        }
    }

    static long getCachedSubredditListTime(Context context) {
        try {
            return getCacheInfo(context).subredditListTime;
        } catch (Exception e) {
            Log.e(TAG, "error w/ getCacheInfo", e);
            return 0;
        }
    }

    static void invalidateAllCaches(Context context) {
        try {
            synchronized (CACHE_LOCK) {
                FileOutputStream fos = context.openFileOutput("cacheinfo.dat", 0);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(new CacheInfo());
                oos.close();
                fos.close();
                Log.d(TAG, "invalidateAllCaches: wrote blank CacheInfo");
            }
        } catch (IOException e) {
            Log.e(TAG, "invalidateAllCaches: Error writing CacheInfo", e);
        }
    }

    static void invalidateCachedSubreddit(Context context) {
    }

    static void invalidateCachedThread(Context context) {
    }

    static void setCachedSubredditUrl(Context context, String subredditUrl2) throws IOException {
    }

    static void setCachedThreadUrl(Context context, String threadUrl2) throws IOException {
    }

    static void setCachedSubredditList(Context context, ArrayList<String> subredditList2) throws IOException {
        CacheInfo ci = null;
        try {
            ci = getCacheInfo(context);
        } catch (Exception e) {
            Log.e(TAG, "error w/ getCacheInfo", e);
        }
        if (ci == null) {
            ci = new CacheInfo();
        }
        synchronized (CACHE_LOCK) {
            FileOutputStream fos = context.openFileOutput("cacheinfo.dat", 0);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            ci.subredditList = subredditList2;
            ci.subredditListTime = System.currentTimeMillis();
            oos.writeObject(ci);
            oos.close();
            fos.close();
        }
    }
}
