package org.codehaus.jackson.node;

import java.io.IOException;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.Base64Variants;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonLocation;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.NumberInput;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.util.CharTypes;

public final class TextNode extends ValueNode {
    static final TextNode EMPTY_STRING_NODE = new TextNode("");
    static final int INT_SPACE = 32;
    final String _value;

    public TextNode(String v) {
        this._value = v;
    }

    public static TextNode valueOf(String v) {
        if (v == null) {
            return null;
        }
        if (v.length() == 0) {
            return EMPTY_STRING_NODE;
        }
        return new TextNode(v);
    }

    public JsonToken asToken() {
        return JsonToken.VALUE_STRING;
    }

    public boolean isTextual() {
        return true;
    }

    public String getTextValue() {
        return this._value;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0028, code lost:
        if (r0 >= 0) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        _reportInvalidBase64(r13, r2, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002e, code lost:
        r3 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        if (r6 < r4) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        r5 = r6 + 1;
        r2 = r7.charAt(r6);
        r0 = r13.decodeBase64Char(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        if (r0 >= 0) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        _reportInvalidBase64(r13, r2, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0044, code lost:
        r3 = (r3 << 6) | r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0048, code lost:
        if (r5 < r4) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004a, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004d, code lost:
        r6 = r5 + 1;
        r2 = r7.charAt(r5);
        r0 = r13.decodeBase64Char(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0057, code lost:
        if (r0 >= 0) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0059, code lost:
        if (r0 == -2) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005b, code lost:
        _reportInvalidBase64(r13, r2, 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005f, code lost:
        if (r6 < r4) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0061, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0064, code lost:
        r5 = r6 + 1;
        r2 = r7.charAt(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006e, code lost:
        if (r13.usesPaddingChar(r2) != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0070, code lost:
        _reportInvalidBase64(r13, r2, 3, "expected padding character '" + r13.getPaddingChar() + "'");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0090, code lost:
        r1.append(r3 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0097, code lost:
        r3 = (r3 << 6) | r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009b, code lost:
        if (r6 < r4) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009d, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a0, code lost:
        r5 = r6 + 1;
        r2 = r7.charAt(r6);
        r0 = r13.decodeBase64Char(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00aa, code lost:
        if (r0 >= 0) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ac, code lost:
        if (r0 == -2) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ae, code lost:
        _reportInvalidBase64(r13, r2, 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b1, code lost:
        r1.appendTwoBytes(r3 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b8, code lost:
        r1.appendThreeBytes((r3 << 6) | r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        r0 = r13.decodeBase64Char(r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] getBinaryValue(org.codehaus.jackson.Base64Variant r13) throws java.io.IOException {
        /*
            r12 = this;
            r11 = 3
            r10 = -2
            org.codehaus.jackson.util.ByteArrayBuilder r1 = new org.codehaus.jackson.util.ByteArrayBuilder
            r8 = 100
            r1.<init>(r8)
            java.lang.String r7 = r12._value
            r5 = 0
            int r4 = r7.length()
        L_0x0010:
            if (r5 >= r4) goto L_0x001b
        L_0x0012:
            int r6 = r5 + 1
            char r2 = r7.charAt(r5)
            if (r6 < r4) goto L_0x0020
            r5 = r6
        L_0x001b:
            byte[] r8 = r1.toByteArray()
            return r8
        L_0x0020:
            r8 = 32
            if (r2 <= r8) goto L_0x00c1
            int r0 = r13.decodeBase64Char(r2)
            if (r0 >= 0) goto L_0x002e
            r8 = 0
            r12._reportInvalidBase64(r13, r2, r8)
        L_0x002e:
            r3 = r0
            if (r6 < r4) goto L_0x0034
            r12._reportBase64EOF()
        L_0x0034:
            int r5 = r6 + 1
            char r2 = r7.charAt(r6)
            int r0 = r13.decodeBase64Char(r2)
            if (r0 >= 0) goto L_0x0044
            r8 = 1
            r12._reportInvalidBase64(r13, r2, r8)
        L_0x0044:
            int r8 = r3 << 6
            r3 = r8 | r0
            if (r5 < r4) goto L_0x004d
            r12._reportBase64EOF()
        L_0x004d:
            int r6 = r5 + 1
            char r2 = r7.charAt(r5)
            int r0 = r13.decodeBase64Char(r2)
            if (r0 >= 0) goto L_0x0097
            if (r0 == r10) goto L_0x005f
            r8 = 2
            r12._reportInvalidBase64(r13, r2, r8)
        L_0x005f:
            if (r6 < r4) goto L_0x0064
            r12._reportBase64EOF()
        L_0x0064:
            int r5 = r6 + 1
            char r2 = r7.charAt(r6)
            boolean r8 = r13.usesPaddingChar(r2)
            if (r8 != 0) goto L_0x0090
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "expected padding character '"
            java.lang.StringBuilder r8 = r8.append(r9)
            char r9 = r13.getPaddingChar()
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "'"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            r12._reportInvalidBase64(r13, r2, r11, r8)
        L_0x0090:
            int r3 = r3 >> 4
            r1.append(r3)
            goto L_0x0010
        L_0x0097:
            int r8 = r3 << 6
            r3 = r8 | r0
            if (r6 < r4) goto L_0x00a0
            r12._reportBase64EOF()
        L_0x00a0:
            int r5 = r6 + 1
            char r2 = r7.charAt(r6)
            int r0 = r13.decodeBase64Char(r2)
            if (r0 >= 0) goto L_0x00b8
            if (r0 == r10) goto L_0x00b1
            r12._reportInvalidBase64(r13, r2, r11)
        L_0x00b1:
            int r3 = r3 >> 2
            r1.appendTwoBytes(r3)
            goto L_0x0010
        L_0x00b8:
            int r8 = r3 << 6
            r3 = r8 | r0
            r1.appendThreeBytes(r3)
            goto L_0x0010
        L_0x00c1:
            r5 = r6
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.node.TextNode.getBinaryValue(org.codehaus.jackson.Base64Variant):byte[]");
    }

    public byte[] getBinaryValue() throws IOException {
        return getBinaryValue(Base64Variants.getDefaultVariant());
    }

    public String getValueAsText() {
        return this._value;
    }

    public boolean getValueAsBoolean(boolean defaultValue) {
        if (this._value == null || !"true".equals(this._value.trim())) {
            return defaultValue;
        }
        return true;
    }

    public int getValueAsInt(int defaultValue) {
        return NumberInput.parseAsInt(this._value, defaultValue);
    }

    public long getValueAsLong(long defaultValue) {
        return NumberInput.parseAsLong(this._value, defaultValue);
    }

    public double getValueAsDouble(double defaultValue) {
        return NumberInput.parseAsDouble(this._value, defaultValue);
    }

    public final void serialize(JsonGenerator jg, SerializerProvider provider) throws IOException, JsonProcessingException {
        if (this._value == null) {
            jg.writeNull();
        } else {
            jg.writeString(this._value);
        }
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (o.getClass() != getClass()) {
            return false;
        }
        return ((TextNode) o)._value.equals(this._value);
    }

    public int hashCode() {
        return this._value.hashCode();
    }

    public String toString() {
        int len = this._value.length();
        StringBuilder sb = new StringBuilder(len + 2 + (len >> 4));
        appendQuoted(sb, this._value);
        return sb.toString();
    }

    protected static void appendQuoted(StringBuilder sb, String content) {
        sb.append('\"');
        CharTypes.appendQuoted(sb, content);
        sb.append('\"');
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidBase64(Base64Variant b64variant, char ch, int bindex) throws JsonParseException {
        _reportInvalidBase64(b64variant, ch, bindex, null);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidBase64(Base64Variant b64variant, char ch, int bindex, String msg) throws JsonParseException {
        String base;
        if (ch <= INT_SPACE) {
            base = "Illegal white space character (code 0x" + Integer.toHexString(ch) + ") as character #" + (bindex + 1) + " of 4-char base64 unit: can only used between units";
        } else if (b64variant.usesPaddingChar(ch)) {
            base = "Unexpected padding character ('" + b64variant.getPaddingChar() + "') as character #" + (bindex + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (!Character.isDefined(ch) || Character.isISOControl(ch)) {
            base = "Illegal character (code 0x" + Integer.toHexString(ch) + ") in base64 content";
        } else {
            base = "Illegal character '" + ch + "' (code 0x" + Integer.toHexString(ch) + ") in base64 content";
        }
        if (msg != null) {
            base = base + ": " + msg;
        }
        throw new JsonParseException(base, JsonLocation.NA);
    }

    /* access modifiers changed from: protected */
    public void _reportBase64EOF() throws JsonParseException {
        throw new JsonParseException("Unexpected end-of-String when base64 content", JsonLocation.NA);
    }
}
