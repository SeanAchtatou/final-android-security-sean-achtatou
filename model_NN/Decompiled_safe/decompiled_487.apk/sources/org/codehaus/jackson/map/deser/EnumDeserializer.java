package org.codehaus.jackson.map.deser;

import java.io.IOException;
import java.lang.reflect.Method;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.annotate.JsonCachable;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.util.ClassUtil;

@JsonCachable
public class EnumDeserializer extends StdScalarDeserializer<Enum<?>> {
    final EnumResolver<?> _resolver;

    public EnumDeserializer(EnumResolver<?> res) {
        super(Enum.class);
        this._resolver = res;
    }

    public static JsonDeserializer<?> deserializerForCreator(DeserializationConfig config, Class<?> enumClass, AnnotatedMethod factory) {
        if (factory.getParameterType(0) != String.class) {
            throw new IllegalArgumentException("Parameter #0 type for factory method (" + factory + ") not suitable, must be java.lang.String");
        }
        if (config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            ClassUtil.checkAndFixAccess(factory.getMember());
        }
        return new FactoryBasedDeserializer(enumClass, factory);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Enum<?>] */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.lang.Enum<?>] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Enum<?> deserialize(org.codehaus.jackson.JsonParser r9, org.codehaus.jackson.map.DeserializationContext r10) throws java.io.IOException, org.codehaus.jackson.JsonProcessingException {
        /*
            r8 = this;
            org.codehaus.jackson.JsonToken r0 = r9.getCurrentToken()
            org.codehaus.jackson.JsonToken r5 = org.codehaus.jackson.JsonToken.VALUE_STRING
            if (r0 != r5) goto L_0x0023
            java.lang.String r2 = r9.getText()
            org.codehaus.jackson.map.deser.EnumResolver<?> r5 = r8._resolver
            java.lang.Enum r3 = r5.findEnum(r2)
            if (r3 != 0) goto L_0x0021
            org.codehaus.jackson.map.deser.EnumResolver<?> r5 = r8._resolver
            java.lang.Class r5 = r5.getEnumClass()
            java.lang.String r6 = "value not one of declared Enum instance names"
            org.codehaus.jackson.map.JsonMappingException r5 = r10.weirdStringException(r5, r6)
            throw r5
        L_0x0021:
            r4 = r3
        L_0x0022:
            return r4
        L_0x0023:
            org.codehaus.jackson.JsonToken r5 = org.codehaus.jackson.JsonToken.VALUE_NUMBER_INT
            if (r0 != r5) goto L_0x006e
            org.codehaus.jackson.map.DeserializationConfig$Feature r5 = org.codehaus.jackson.map.DeserializationConfig.Feature.FAIL_ON_NUMBERS_FOR_ENUMS
            boolean r5 = r10.isEnabled(r5)
            if (r5 == 0) goto L_0x0036
            java.lang.String r5 = "Not allowed to deserialize Enum value out of JSON number (disable DeserializationConfig.Feature.FAIL_ON_NUMBERS_FOR_ENUMS to allow)"
            org.codehaus.jackson.map.JsonMappingException r5 = r10.mappingException(r5)
            throw r5
        L_0x0036:
            int r1 = r9.getIntValue()
            org.codehaus.jackson.map.deser.EnumResolver<?> r5 = r8._resolver
            java.lang.Enum r3 = r5.getEnum(r1)
            if (r3 != 0) goto L_0x006c
            org.codehaus.jackson.map.deser.EnumResolver<?> r5 = r8._resolver
            java.lang.Class r5 = r5.getEnumClass()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "index value outside legal index range [0.."
            java.lang.StringBuilder r6 = r6.append(r7)
            org.codehaus.jackson.map.deser.EnumResolver<?> r7 = r8._resolver
            int r7 = r7.lastValidIndex()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "]"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            org.codehaus.jackson.map.JsonMappingException r5 = r10.weirdNumberException(r5, r6)
            throw r5
        L_0x006c:
            r4 = r3
            goto L_0x0022
        L_0x006e:
            org.codehaus.jackson.map.deser.EnumResolver<?> r5 = r8._resolver
            java.lang.Class r5 = r5.getEnumClass()
            org.codehaus.jackson.map.JsonMappingException r5 = r10.mappingException(r5)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.deser.EnumDeserializer.deserialize(org.codehaus.jackson.JsonParser, org.codehaus.jackson.map.DeserializationContext):java.lang.Enum");
    }

    protected static class FactoryBasedDeserializer extends StdScalarDeserializer<Object> {
        protected final Class<?> _enumClass;
        protected final Method _factory;

        public FactoryBasedDeserializer(Class<?> cls, AnnotatedMethod f) {
            super(Enum.class);
            this._enumClass = cls;
            this._factory = f.getAnnotated();
        }

        public Object deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            if (jp.getCurrentToken() != JsonToken.VALUE_STRING) {
                throw ctxt.mappingException(this._enumClass);
            }
            String value = jp.getText();
            try {
                return this._factory.invoke(this._enumClass, value);
            } catch (Exception e) {
                ClassUtil.unwrapAndThrowAsIAE(e);
                return null;
            }
        }
    }
}
