package org.codehaus.jackson.map.util;

import org.codehaus.jackson.io.SerializedString;
import org.codehaus.jackson.map.MapperConfig;
import org.codehaus.jackson.map.type.ClassKey;
import org.codehaus.jackson.type.JavaType;

public class RootNameLookup {
    protected LRUMap<ClassKey, SerializedString> _rootNames;

    public SerializedString findRootName(JavaType rootType, MapperConfig<?> config) {
        return findRootName(rootType.getRawClass(), config);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0041, code lost:
        if (r4 != null) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized org.codehaus.jackson.io.SerializedString findRootName(java.lang.Class<?> r10, org.codehaus.jackson.map.MapperConfig<?> r11) {
        /*
            r9 = this;
            monitor-enter(r9)
            org.codehaus.jackson.map.type.ClassKey r3 = new org.codehaus.jackson.map.type.ClassKey     // Catch:{ all -> 0x0044 }
            r3.<init>(r10)     // Catch:{ all -> 0x0044 }
            org.codehaus.jackson.map.util.LRUMap<org.codehaus.jackson.map.type.ClassKey, org.codehaus.jackson.io.SerializedString> r6 = r9._rootNames     // Catch:{ all -> 0x0044 }
            if (r6 != 0) goto L_0x0039
            org.codehaus.jackson.map.util.LRUMap r6 = new org.codehaus.jackson.map.util.LRUMap     // Catch:{ all -> 0x0044 }
            r7 = 20
            r8 = 200(0xc8, float:2.8E-43)
            r6.<init>(r7, r8)     // Catch:{ all -> 0x0044 }
            r9._rootNames = r6     // Catch:{ all -> 0x0044 }
        L_0x0015:
            org.codehaus.jackson.map.BeanDescription r1 = r11.introspectClassAnnotations(r10)     // Catch:{ all -> 0x0044 }
            org.codehaus.jackson.map.introspect.BasicBeanDescription r1 = (org.codehaus.jackson.map.introspect.BasicBeanDescription) r1     // Catch:{ all -> 0x0044 }
            org.codehaus.jackson.map.AnnotationIntrospector r2 = r11.getAnnotationIntrospector()     // Catch:{ all -> 0x0044 }
            org.codehaus.jackson.map.introspect.AnnotatedClass r0 = r1.getClassInfo()     // Catch:{ all -> 0x0044 }
            java.lang.String r5 = r2.findRootName(r0)     // Catch:{ all -> 0x0044 }
            if (r5 != 0) goto L_0x002d
            java.lang.String r5 = r10.getSimpleName()     // Catch:{ all -> 0x0044 }
        L_0x002d:
            org.codehaus.jackson.io.SerializedString r4 = new org.codehaus.jackson.io.SerializedString     // Catch:{ all -> 0x0044 }
            r4.<init>(r5)     // Catch:{ all -> 0x0044 }
            org.codehaus.jackson.map.util.LRUMap<org.codehaus.jackson.map.type.ClassKey, org.codehaus.jackson.io.SerializedString> r6 = r9._rootNames     // Catch:{ all -> 0x0044 }
            r6.put(r3, r4)     // Catch:{ all -> 0x0044 }
        L_0x0037:
            monitor-exit(r9)
            return r4
        L_0x0039:
            org.codehaus.jackson.map.util.LRUMap<org.codehaus.jackson.map.type.ClassKey, org.codehaus.jackson.io.SerializedString> r6 = r9._rootNames     // Catch:{ all -> 0x0044 }
            java.lang.Object r4 = r6.get(r3)     // Catch:{ all -> 0x0044 }
            org.codehaus.jackson.io.SerializedString r4 = (org.codehaus.jackson.io.SerializedString) r4     // Catch:{ all -> 0x0044 }
            if (r4 == 0) goto L_0x0015
            goto L_0x0037
        L_0x0044:
            r6 = move-exception
            monitor-exit(r9)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.util.RootNameLookup.findRootName(java.lang.Class, org.codehaus.jackson.map.MapperConfig):org.codehaus.jackson.io.SerializedString");
    }
}
