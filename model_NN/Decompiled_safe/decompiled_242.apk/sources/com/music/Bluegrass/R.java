package com.music.Bluegrass;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int isGoneWithoutAd = 2130771973;
        public static final int keywords = 2130771971;
        public static final int refreshInterval = 2130771972;
        public static final int testing = 2130771968;
        public static final int textColor = 2130771970;
    }

    public static final class drawable {
        public static final int AntiqueWhite4 = 2130837530;
        public static final int Firebrick = 2130837529;
        public static final int back = 2130837504;
        public static final int background = 2130837524;
        public static final int black = 2130837526;
        public static final int contanct = 2130837505;
        public static final int control_back = 2130837525;
        public static final int del = 2130837506;
        public static final int down = 2130837507;
        public static final int eject = 2130837508;
        public static final int gray91 = 2130837527;
        public static final int grey31 = 2130837528;
        public static final int home = 2130837509;
        public static final int icon = 2130837510;
        public static final int main = 2130837511;
        public static final int menu_help = 2130837512;
        public static final int more = 2130837513;
        public static final int pause = 2130837514;
        public static final int play = 2130837515;
        public static final int s = 2130837516;
        public static final int shengyin = 2130837517;
        public static final int sound_line = 2130837518;
        public static final int sound_line1 = 2130837519;
        public static final int sounddisable = 2130837520;
        public static final int soundenable = 2130837521;
        public static final int stepbackward = 2130837522;
        public static final int stepforward = 2130837523;
        public static final int white = 2130837531;
    }

    public static final class id {
        public static final int about = 2131034128;
        public static final int ad = 2131034129;
        public static final int adview3 = 2131034136;
        public static final int back = 2131034130;
        public static final int button1 = 2131034116;
        public static final int button2 = 2131034117;
        public static final int button3 = 2131034118;
        public static final int button4 = 2131034119;
        public static final int button5 = 2131034120;
        public static final int cancel = 2131034112;
        public static final int download_file = 2131034124;
        public static final int download_image = 2131034122;
        public static final int duration = 2131034115;
        public static final int has_played = 2131034113;
        public static final int list = 2131034121;
        public static final int name = 2131034135;
        public static final int search = 2131034134;
        public static final int searchwebview = 2131034137;
        public static final int seekbar = 2131034114;
        public static final int size = 2131034123;
        public static final int text = 2131034131;
        public static final int text_1 = 2131034125;
        public static final int text_2 = 2131034127;
        public static final int upload_image = 2131034126;
        public static final int vv = 2131034133;
        public static final int webview = 2131034132;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int controler = 2130903041;
        public static final int dialog = 2130903042;
        public static final int download_list = 2130903043;
        public static final int extral = 2130903044;
        public static final int list = 2130903045;
        public static final int main = 2130903046;
        public static final int mn = 2130903047;
        public static final int picwebview = 2130903048;
        public static final int searchwebview = 2130903049;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.testing, R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval, R.attr.isGoneWithoutAd};
        public static final int com_admob_android_ads_AdView_backgroundColor = 1;
        public static final int com_admob_android_ads_AdView_isGoneWithoutAd = 5;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_testing = 0;
        public static final int com_admob_android_ads_AdView_textColor = 2;
    }
}
