package com.music.Bluegrass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DownloadActivity extends Activity {
    private static final String ACTIONTWO = "com.music.Bluegrass.action.DOWNLOAD_LIST";
    private static final int DIALOG1 = 1;
    private static final int DIALOG2 = 2;
    private static final int ITEM1 = 2;
    private static final int ITEM2 = 3;
    private static final String PATH = "/sdcard/sound/";
    public static List<Map<String, Object>> data;
    /* access modifiers changed from: private */
    public static boolean update_flag = true;
    private final BroadcastReceiver DownloadList = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (DownloadActivity.update_flag) {
                DownloadActivity.this.updateList();
            }
        }
    };
    /* access modifiers changed from: private */
    public int deletePosition;
    private boolean flag = false;
    /* access modifiers changed from: private */
    public int installPosition;
    String[] item = {"Share By SMS", "Play It", "Set As DefaultRingtone"};
    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            DownloadActivity.this.installPosition = position;
            DownloadActivity.this.showDialog(2);
        }
    };
    private ListView listView;
    AdapterView.OnItemLongClickListener longItemClickListener = new AdapterView.OnItemLongClickListener() {
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            DownloadActivity.update_flag = false;
            DownloadActivity.this.deletePosition = position;
            DownloadActivity.this.showDialog(1);
            return true;
        }
    };
    int ok_len = 0;
    int wait_len = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver(this.DownloadList, new IntentFilter(ACTIONTWO));
        updateList();
        Toast.makeText(getApplicationContext(), "item click to paly, item longclick to delete.", 1).show();
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.DownloadList);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 3, 0, "Clear All History");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item2) {
        switch (item2.getItemId()) {
            case 3:
                clearHistory();
                updateList();
                break;
        }
        return super.onOptionsItemSelected(item2);
    }

    public void updateList() {
        prepareData();
        this.listView = new ListView(this);
        this.listView.setAdapter((ListAdapter) new SimpleAdapter(this, data, R.layout.download_list, new String[]{"image", "file", "text1", "text2", "size"}, new int[]{R.id.download_image, R.id.download_file, R.id.text_1, R.id.text_2, R.id.size}));
        this.listView.setOnItemClickListener(this.itemClickListener);
        this.listView.setOnItemLongClickListener(this.longItemClickListener);
        setContentView(this.listView);
    }

    private void prepareData() {
        int len;
        int len2;
        Map map;
        Map map2;
        data = new ArrayList();
        synchronized (DownloadApkService.data) {
            len = DownloadApkService.data.size();
            this.wait_len = len;
        }
        for (int i = 0; i < len; i++) {
            synchronized (DownloadApkService.data) {
                map2 = DownloadApkService.data.get(i);
            }
            String fileName = (String) map2.get("NAME");
            String path = (String) map2.get("PATH");
            int pointer = Integer.parseInt(map2.get("POINTER").toString());
            String text1Content = "";
            String text2Content = "";
            if (i == 0) {
                Map<String, Object> item2 = new HashMap<>();
                item2.put("image", Integer.valueOf((int) R.drawable.shengyin));
                item2.put("file", fileName);
                item2.put("path", path);
                if (pointer == -1) {
                    for (int j = 0; j < 100; j++) {
                        text1Content = String.valueOf(text1Content) + "  ";
                    }
                    item2.put("text1", text1Content);
                    item2.put("text2", text2Content);
                    item2.put("size", "100%");
                } else {
                    for (int j2 = 0; j2 < pointer; j2++) {
                        text1Content = String.valueOf(text1Content) + "  ";
                    }
                    for (int j3 = 0; j3 <= 100 - pointer; j3++) {
                        text2Content = String.valueOf(text2Content) + "  ";
                    }
                    item2.put("text1", text1Content);
                    item2.put("text2", text2Content);
                    item2.put("size", String.valueOf(Integer.toString(pointer)) + "%");
                }
                data.add(item2);
            } else {
                Map<String, Object> item3 = new HashMap<>();
                item3.put("image", Integer.valueOf((int) R.drawable.shengyin));
                item3.put("file", fileName);
                for (int j4 = 0; j4 < 100; j4++) {
                    text2Content = String.valueOf(text2Content) + "  ";
                }
                item3.put("text1", text1Content);
                item3.put("text2", text2Content);
                item3.put("size", "waiting");
                item3.put("path", path);
                data.add(item3);
            }
        }
        synchronized (DownloadApkService.history) {
            len2 = DownloadApkService.history.size();
            this.ok_len = len2;
        }
        for (int i2 = 0; i2 < len2; i2++) {
            synchronized (DownloadApkService.history) {
                map = DownloadApkService.history.get(i2);
            }
            String fileName2 = (String) map.get("NAME");
            String path2 = (String) map.get("PATH");
            if (fileName2 != null) {
                Map<String, Object> item4 = new HashMap<>();
                item4.put("image", Integer.valueOf((int) R.drawable.shengyin));
                item4.put("file", fileName2);
                item4.put("path", path2);
                String text1Content2 = "";
                for (int j5 = 0; j5 < 100; j5++) {
                    text1Content2 = String.valueOf(text1Content2) + "  ";
                }
                item4.put("text1", text1Content2);
                item4.put("text2", "");
                item4.put("size", "ok");
                data.add(item4);
            }
        }
    }

    /* access modifiers changed from: private */
    public void deleteTheFile() {
        if (this.deletePosition >= this.wait_len) {
            try {
                File file = new File(PATH + ((String) DownloadApkService.history.get(this.deletePosition - this.wait_len).get("NAME")));
                if (file.exists() && file.isFile()) {
                    file.delete();
                }
                synchronized (DownloadApkService.history) {
                    DownloadApkService.history.remove(this.deletePosition);
                }
            } catch (Exception e) {
                return;
            }
        } else if (this.deletePosition == 0) {
            String fileName = (String) DownloadApkService.data.get(0).get("NAME");
            DownloadApkService.download_flag = true;
            for (int i = 0; i < 99999; i++) {
            }
            try {
                File file2 = new File(PATH + fileName);
                if (file2.exists() && file2.isFile()) {
                    file2.delete();
                }
            } catch (Exception e2) {
                return;
            }
        } else {
            synchronized (DownloadApkService.data) {
                DownloadApkService.data.remove(this.deletePosition);
            }
        }
        updateList();
    }

    private void clearHistory() {
        int i = 0;
        while (i < this.ok_len) {
            try {
                File file = new File(PATH + ((String) DownloadApkService.history.get(0).get("NAME")));
                if (file.exists() && file.isFile()) {
                    file.delete();
                }
                synchronized (DownloadApkService.history) {
                    DownloadApkService.history.remove(0);
                }
                i++;
            } catch (Exception e) {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return buildDialog1(this);
            case R.styleable.com_admob_android_ads_AdView_textColor /*2*/:
                return buildDialog2(this);
            default:
                return null;
        }
    }

    private Dialog buildDialog1(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Delete the file ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                DownloadActivity.this.deleteTheFile();
                DownloadActivity.update_flag = true;
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                DownloadActivity.update_flag = true;
            }
        });
        return builder.create();
    }

    private Dialog buildDialog2(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select");
        builder.setItems(this.item, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    DownloadActivity.this.sendSMS();
                } else if (which == 1) {
                    if (DownloadActivity.this.installPosition >= DownloadActivity.this.wait_len) {
                        DownloadActivity.this.playTheMusic();
                    } else {
                        Toast.makeText(DownloadActivity.this.getApplicationContext(), "Please wait, downloading now.", 0).show();
                    }
                } else if (which != 2) {
                } else {
                    if (DownloadActivity.this.installPosition >= DownloadActivity.this.wait_len) {
                        DownloadActivity.this.setAsRingtone();
                    } else {
                        Toast.makeText(DownloadActivity.this.getApplicationContext(), "Please wait, downloading now.", 0).show();
                    }
                }
            }
        });
        return builder.create();
    }

    /* access modifiers changed from: private */
    public void sendSMS() {
        String tt = Uri.decode((String) data.get(this.installPosition).get("path"));
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.TEXT", tt);
        startActivity(Intent.createChooser(intent, "title"));
    }

    /* access modifiers changed from: private */
    public void playTheMusic() {
        File apk = new File(PATH + ((String) data.get(this.installPosition).get("file")));
        new MediaPlayer();
        try {
            this.flag = true;
            MediaPlayer mp = new MediaPlayer();
            try {
                mp.setDataSource(apk.getPath());
                mp.prepare();
                mp.start();
            } catch (IOException e) {
            }
        } catch (IOException e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    /* access modifiers changed from: private */
    public void setAsRingtone() {
        try {
            String fileName = (String) data.get(this.installPosition).get("file");
            File file = new File(PATH + fileName);
            ContentValues values = new ContentValues();
            values.put("_data", file.getAbsolutePath());
            values.put("title", fileName);
            values.put("_size", (Integer) 215454);
            values.put("mime_type", "audio/mp3");
            values.put("is_ringtone", (Boolean) true);
            values.put("is_notification", (Boolean) false);
            values.put("is_alarm", (Boolean) false);
            values.put("is_music", (Boolean) false);
            RingtoneManager.setActualDefaultRingtoneUri(this, 1, getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath()), values));
            Toast.makeText(getApplicationContext(), "Had set as phone ringtone", 0).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "setting fail", 0).show();
        }
    }
}
