package com.music.Bluegrass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class SearchWebView extends Activity {
    private static final String ACTION = "com.music.Bluegrass.action.NEW_FILE";
    private static final int DIALOG1 = 1;
    private static final int DIALOG2 = 2;
    private static final int DIALOG3 = 3;
    private static final String PATH = "/sdcard/sound/";
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    SearchWebView.this.showDialog(1);
                    return;
                case R.styleable.com_admob_android_ads_AdView_testing:
                default:
                    return;
                case 1:
                    SearchWebView.this.showDialog(2);
                    return;
            }
        }
    };
    EditText key;
    /* access modifiers changed from: private */
    public WebView mWebView;
    ImageButton search;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().requestFeature(2);
        setContentView((int) R.layout.searchwebview);
        this.mWebView = (WebView) findViewById(R.id.searchwebview);
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.setWebViewClient(new MyWebClient());
        this.mWebView.addJavascriptInterface(new DemoJavaScriptInterface(), "apkshare");
        this.key = (EditText) findViewById(R.id.name);
        this.search = (ImageButton) findViewById(R.id.search);
        this.search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SearchWebView.this.mWebView.loadUrl("http://vjoker.apkshare.com/boom/?key=" + SearchWebView.this.key.getText().toString());
            }
        });
        this.mWebView.loadUrl("http://vjoker.apkshare.com/boom/?key=girl");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }

    final class DemoJavaScriptInterface {
        DemoJavaScriptInterface() {
        }

        public void MediaPlay(String file_path, String title, String url) {
            SearchWebView.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(file_path)));
        }

        public void clickOnAndroid(String file_path, String title, String url) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                Intent intentAddFile = new Intent(SearchWebView.ACTION);
                intentAddFile.putExtra("APK", file_path);
                intentAddFile.putExtra("TITLE", title);
                intentAddFile.putExtra("URL", url);
                SearchWebView.this.sendBroadcast(intentAddFile);
                Toast.makeText(SearchWebView.this.getApplicationContext(), "Had added to download list, save at sdcard", 0).show();
                return;
            }
            SearchWebView.this.showDialog(3);
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return true;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onProgressChanged(WebView view, int newProgress) {
            SearchWebView.this.getWindow().setFeatureInt(2, newProgress * 100);
            super.onProgressChanged(view, newProgress);
        }
    }

    final class MyWebClient extends WebViewClient {
        MyWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return buildDialog1(this);
            case 2:
                return buildDialog2(this);
            case 3:
                return buildDialog3(this);
            default:
                return null;
        }
    }

    private Dialog buildDialog1(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Download Failed");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }

    private Dialog buildDialog2(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Install the app right now ?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }

    private Dialog buildDialog3(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("NO sdcard");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }
}
