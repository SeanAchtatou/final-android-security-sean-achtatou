package com.music.Bluegrass;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadApkService extends Service {
    private static final String ACTION = "com.music.Bluegrass.action.NEW_FILE";
    private static final String ACTIONTWO = "com.music.Bluegrass.action.DOWNLOAD_LIST";
    private static final String ACTIONtoast = "com.music.Bluegrass.action.toast";
    private static final String PATH = "/sdcard/sound/";
    public static List<Map<String, Object>> data = new ArrayList();
    public static boolean download_flag = false;
    public static List<Map<String, Object>> history = new ArrayList();
    private final BroadcastReceiver DownloadReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String path = intent.getStringExtra("APK");
            String title = intent.getStringExtra("TITLE");
            String url = intent.getStringExtra("URL");
            Map<String, Object> item = new HashMap<>();
            item.put("PATH", path);
            item.put("NAME", title);
            item.put("URL", url);
            item.put("POINTER", 0);
            synchronized (DownloadApkService.data) {
                DownloadApkService.data.add(item);
                DownloadApkService.data.notify();
            }
        }
    };
    private final String SETTING = "history";
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
        }
    };
    /* access modifiers changed from: private */
    public String name = null;
    /* access modifiers changed from: private */
    public String path = null;
    private HandleThread thread;
    /* access modifiers changed from: private */
    public String url = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        new File(PATH).mkdirs();
        registerReceiver(this.DownloadReceiver, new IntentFilter(ACTION));
        readHistory();
        this.thread = new HandleThread(this, null);
        this.thread.start();
    }

    public void onDestroy() {
        super.onDestroy();
        this.thread.requestExit();
        unregisterReceiver(this.DownloadReceiver);
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    private void readHistory() {
        String tmp = getSharedPreferences("history", 0).getString("HISTORY", "");
        if (tmp.length() > 0) {
            String[] res = tmp.split("&");
            for (int i = 0; i < res.length; i += 4) {
                String name2 = Uri.decode(res[i]);
                String path2 = Uri.decode(res[i + 1]);
                String url2 = Uri.decode(res[i + 2]);
                String pointer = Uri.decode(res[i + 3]);
                Map<String, Object> item = new HashMap<>();
                item.put("NAME", name2);
                item.put("PATH", path2);
                item.put("URL", url2);
                item.put("POINTER", Integer.valueOf(Integer.parseInt(pointer)));
                synchronized (history) {
                    history.add(item);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void initFileInfo(Map<String, Object> cache) {
        this.path = (String) cache.get("PATH");
        this.name = (String) cache.get("NAME");
        this.url = (String) cache.get("URL");
    }

    private class HandleThread extends Thread {
        private boolean bRun;
        private Map<String, Object> cache;

        private HandleThread() {
            this.cache = null;
            this.bRun = true;
        }

        /* synthetic */ HandleThread(DownloadApkService downloadApkService, HandleThread handleThread) {
            this();
        }

        public void requestExit() {
            this.bRun = false;
            synchronized (DownloadApkService.data) {
                DownloadApkService.data.notify();
            }
        }

        public void run() {
            while (this.bRun) {
                synchronized (DownloadApkService.data) {
                    if (DownloadApkService.data.size() > 0) {
                        this.cache = DownloadApkService.data.get(0);
                        if (this.cache != null) {
                            DownloadApkService.this.initFileInfo(this.cache);
                            if (DownloadApkService.this.path == null) {
                                synchronized (DownloadApkService.data) {
                                    DownloadApkService.data.remove(0);
                                }
                            } else {
                                try {
                                    if (DownloadApkService.this.startDownloadFile()) {
                                        Map<String, Object> item = new HashMap<>();
                                        item.put("NAME", DownloadApkService.this.name);
                                        item.put("PATH", DownloadApkService.this.path);
                                        item.put("URL", DownloadApkService.this.url);
                                        item.put("POINTER", -1);
                                        synchronized (DownloadApkService.history) {
                                            DownloadApkService.history.add(0, item);
                                        }
                                        synchronized (DownloadApkService.data) {
                                            DownloadApkService.data.remove(0);
                                        }
                                        DownloadApkService.this.noticeDownloadList();
                                    } else {
                                        synchronized (DownloadApkService.data) {
                                            DownloadApkService.data.remove(0);
                                        }
                                        DownloadApkService.this.noticeDownloadList();
                                    }
                                } catch (Exception e) {
                                    synchronized (DownloadApkService.data) {
                                        DownloadApkService.data.remove(0);
                                        DownloadApkService.this.noticeDownloadList();
                                    }
                                }
                            }
                            this.cache = null;
                        } else {
                            continue;
                        }
                    } else {
                        try {
                            DownloadApkService.data.wait();
                        } catch (InterruptedException e2) {
                        }
                    }
                }
            }
        }
    }

    private void setTheFirstData(int pointer) {
        Map<String, Object> item = new HashMap<>();
        item.put("PATH", this.path);
        item.put("NAME", this.name);
        item.put("URL", this.url);
        item.put("POINTER", Integer.valueOf(pointer));
        synchronized (data) {
            data.set(0, item);
        }
    }

    /* access modifiers changed from: private */
    public void noticeDownloadList() {
        sendBroadcast(new Intent(ACTIONTWO));
    }

    /* access modifiers changed from: private */
    public boolean startDownloadFile() throws IOException {
        URLConnection urlConn = new URL(this.path).openConnection();
        urlConn.connect();
        int len = urlConn.getContentLength();
        String filename = getFilename(urlConn);
        this.name = String.valueOf(this.name) + filename.substring(filename.lastIndexOf(46));
        setTheFirstData(0);
        noticeDownloadList();
        InputStream is = urlConn.getInputStream();
        OutputStream os = new FileOutputStream(new File(PATH + this.name));
        byte[] buf = new byte[2048];
        int num = 0;
        int rate = 0;
        if (len == -1) {
            len = 30000;
        }
        while (true) {
            int str = is.read(buf);
            if (str <= 0) {
                os.flush();
                os.close();
                is.close();
                setTheFirstData(-1);
                noticeDownloadList();
                return true;
            } else if (download_flag) {
                download_flag = false;
                os.flush();
                os.close();
                is.close();
                return false;
            } else {
                num += str;
                os.write(buf, 0, str);
                int temp = setRate(num, len);
                if (rate < temp - 5) {
                    rate = temp;
                    setTheFirstData(setRate(num, len));
                    noticeDownloadList();
                }
            }
        }
    }

    private int setRate(int pointer, int sum) {
        int rate = (int) (100.0d * (((double) pointer) / ((double) sum)));
        if (rate > 100) {
            return 100;
        }
        return rate;
    }

    private String getFilename(URLConnection c) {
        String dispFilename = getFilenameFromDisposition(c.getHeaderField("Content-Disposition"));
        if (dispFilename != null) {
            return dispFilename;
        }
        try {
            String url2 = c.getURL().toString();
            return url2.substring(url2.lastIndexOf(47) + 1);
        } catch (IndexOutOfBoundsException e) {
            return "";
        }
    }

    private String getFilenameFromDisposition(String disp) {
        Pattern p2;
        if (disp == null) {
            return null;
        }
        Matcher m1 = Pattern.compile("filename=(.)").matcher(disp);
        if (!m1.find() || m1.groupCount() < 1) {
            return null;
        }
        String delim = m1.group(1);
        if ("'".equals(delim) || "\"".equals(delim)) {
            p2 = Pattern.compile("filename=" + delim + "([^" + delim + "]*)" + delim);
        } else {
            p2 = Pattern.compile("filename=([^ ]*)( .*)?$");
        }
        Matcher m2 = p2.matcher(disp);
        if (!m2.find() || m2.groupCount() < 1) {
            return null;
        }
        return m2.group(1);
    }
}
