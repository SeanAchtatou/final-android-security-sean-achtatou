package com.music.Bluegrass;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;
import com.admob.android.ads.AdView;
import com.music.Bluegrass.SoundView;
import com.music.Bluegrass.VideoView;
import java.io.File;
import java.io.FileFilter;
import java.util.LinkedList;

public class VideoPlayerActivity extends Activity {
    private static final int HIDE_CONTROLER = 1;
    private static final String PATH = "/sdcard/sound/";
    private static final int PROGRESS_CHANGED = 0;
    private static final int SCREEN_DEFAULT = 1;
    private static final int SCREEN_FULL = 0;
    private static final String TAG = "VideoPlayerActivity";
    private static final int TIME = 6868;
    private static boolean backFromAD = false;
    /* access modifiers changed from: private */
    public static int controlHeight = 0;
    public static LinkedList<MovieInfo> playList = new LinkedList<>();
    /* access modifiers changed from: private */
    public static int position;
    private static int screenHeight = 0;
    /* access modifiers changed from: private */
    public static int screenWidth = 0;
    private AdView adView;
    private ImageButton bn1 = null;
    private ImageButton bn2 = null;
    /* access modifiers changed from: private */
    public ImageButton bn3 = null;
    private ImageButton bn4 = null;
    /* access modifiers changed from: private */
    public ImageButton bn5 = null;
    private View controlView = null;
    /* access modifiers changed from: private */
    public PopupWindow controler = null;
    /* access modifiers changed from: private */
    public int currentVolume = 0;
    /* access modifiers changed from: private */
    public TextView durationTextView = null;
    private View extralView = null;
    /* access modifiers changed from: private */
    public PopupWindow extralWindow = null;
    /* access modifiers changed from: private */
    public boolean isControllerShow = true;
    /* access modifiers changed from: private */
    public boolean isFullScreen = false;
    /* access modifiers changed from: private */
    public boolean isPaused = false;
    /* access modifiers changed from: private */
    public boolean isSilent = false;
    /* access modifiers changed from: private */
    public boolean isSoundShow = false;
    private AudioManager mAudioManager = null;
    private GestureDetector mGestureDetector = null;
    private SoundView mSoundView = null;
    /* access modifiers changed from: private */
    public PopupWindow mSoundWindow = null;
    private int maxVolume = 0;
    Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case R.styleable.com_admob_android_ads_AdView_testing:
                    int i = VideoPlayerActivity.this.vv.getCurrentPosition();
                    VideoPlayerActivity.this.seekBar.setProgress(i);
                    int i2 = i / 1000;
                    int minute = i2 / 60;
                    VideoPlayerActivity.this.playedTextView.setText(String.format("%02d:%02d:%02d", Integer.valueOf(minute / 60), Integer.valueOf(minute % 60), Integer.valueOf(i2 % 60)));
                    sendEmptyMessage(0);
                    break;
                case R.styleable.com_admob_android_ads_AdView_backgroundColor:
                    VideoPlayerActivity.this.hideController();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public TextView playedTextView = null;
    private int playedTime;
    /* access modifiers changed from: private */
    public SeekBar seekBar = null;
    private Uri videoListUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    /* access modifiers changed from: private */
    public VideoView vv = null;

    public class MovieInfo {
        String displayName;
        String path;

        public MovieInfo() {
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mn);
        Looper.myQueue().addIdleHandler(new MessageQueue.IdleHandler() {
            public boolean queueIdle() {
                if (VideoPlayerActivity.this.controler != null && VideoPlayerActivity.this.vv.isShown()) {
                    VideoPlayerActivity.this.controler.showAtLocation(VideoPlayerActivity.this.vv, 80, 0, 0);
                    VideoPlayerActivity.this.controler.update(0, 0, VideoPlayerActivity.screenWidth, VideoPlayerActivity.controlHeight);
                }
                if (VideoPlayerActivity.this.extralWindow != null && VideoPlayerActivity.this.vv.isShown()) {
                    VideoPlayerActivity.this.extralWindow.showAtLocation(VideoPlayerActivity.this.vv, 48, 0, 0);
                    VideoPlayerActivity.this.extralWindow.update(0, 25, VideoPlayerActivity.screenWidth, 60);
                }
                return false;
            }
        });
        this.controlView = getLayoutInflater().inflate((int) R.layout.controler, (ViewGroup) null);
        this.controler = new PopupWindow(this.controlView);
        this.durationTextView = (TextView) this.controlView.findViewById(R.id.duration);
        this.playedTextView = (TextView) this.controlView.findViewById(R.id.has_played);
        this.mSoundView = new SoundView(this);
        this.mSoundView.setOnVolumeChangeListener(new SoundView.OnVolumeChangedListener() {
            public void setYourVolume(int index) {
                VideoPlayerActivity.this.cancelDelayHide();
                VideoPlayerActivity.this.updateVolume(index);
                VideoPlayerActivity.this.hideControllerDelay();
            }
        });
        this.mSoundWindow = new PopupWindow(this.mSoundView);
        this.extralView = getLayoutInflater().inflate((int) R.layout.extral, (ViewGroup) null);
        this.extralWindow = new PopupWindow(this.extralView);
        position = -1;
        ((ImageButton) this.extralView.findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoPlayerActivity.this.finish();
            }
        });
        ((ImageButton) this.extralView.findViewById(R.id.about)).setOnClickListener(new View.OnClickListener() {
            Dialog dialog;
            View.OnClickListener mClickListener = new View.OnClickListener() {
                public void onClick(View v) {
                    Log.d("DIALOG", "DISMISS");
                    AnonymousClass5.this.dialog.dismiss();
                    VideoPlayerActivity.this.vv.start();
                }
            };

            public void onClick(View v) {
                this.dialog = new Dialog(VideoPlayerActivity.this);
                this.dialog.getWindow().requestFeature(1);
                View view = VideoPlayerActivity.this.getLayoutInflater().inflate((int) R.layout.about, (ViewGroup) null);
                this.dialog.setContentView(view);
                view.findViewById(R.id.cancel).setOnClickListener(this.mClickListener);
                VideoPlayerActivity.this.vv.pause();
                this.dialog.show();
                VideoPlayerActivity.this.cancelDelayHide();
            }
        });
        this.bn1 = (ImageButton) this.controlView.findViewById(R.id.button1);
        this.bn2 = (ImageButton) this.controlView.findViewById(R.id.button2);
        this.bn3 = (ImageButton) this.controlView.findViewById(R.id.button3);
        this.bn4 = (ImageButton) this.controlView.findViewById(R.id.button4);
        this.bn5 = (ImageButton) this.controlView.findViewById(R.id.button5);
        this.vv = (VideoView) findViewById(R.id.vv);
        Uri uri = getIntent().getData();
        if (uri != null) {
            if (this.vv.getVideoHeight() == 0) {
                this.vv.setVideoURI(uri);
            }
            this.bn3.setImageResource(R.drawable.pause);
        } else {
            this.bn3.setImageResource(R.drawable.play);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            position = extras.getInt("NUM");
        }
        int len = DownloadActivity.data.size();
        for (int i = 0; i < len; i++) {
            String fileName = (String) DownloadActivity.data.get(i).get("file");
            MovieInfo mi = new MovieInfo();
            mi.displayName = fileName;
            mi.path = PATH + fileName;
            playList.add(mi);
        }
        this.vv.setMySizeChangeLinstener(new VideoView.MySizeChangeLinstener() {
            public void doMyThings() {
                VideoPlayerActivity.this.setVideoScale(1);
            }
        });
        this.bn1.setAlpha(187);
        this.bn2.setAlpha(187);
        this.bn3.setAlpha(187);
        this.bn4.setAlpha(187);
        this.mAudioManager = (AudioManager) getSystemService("audio");
        this.maxVolume = this.mAudioManager.getStreamMaxVolume(3);
        this.currentVolume = this.mAudioManager.getStreamVolume(3);
        this.bn5.setAlpha(findAlphaFromSound());
        this.bn1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(VideoPlayerActivity.this, VideoChooseActivity.class);
                VideoPlayerActivity.this.startActivityForResult(intent, 0);
                VideoPlayerActivity.this.cancelDelayHide();
            }
        });
        this.bn4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int n = VideoPlayerActivity.playList.size();
                int access$12 = VideoPlayerActivity.position + 1;
                VideoPlayerActivity.position = access$12;
                if (access$12 < n) {
                    VideoPlayerActivity.this.vv.setVideoPath(VideoPlayerActivity.playList.get(VideoPlayerActivity.position).path);
                    VideoPlayerActivity.this.cancelDelayHide();
                    VideoPlayerActivity.this.hideControllerDelay();
                    return;
                }
                VideoPlayerActivity.this.finish();
            }
        });
        this.bn3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoPlayerActivity.this.cancelDelayHide();
                if (VideoPlayerActivity.this.isPaused) {
                    VideoPlayerActivity.this.vv.start();
                    VideoPlayerActivity.this.bn3.setImageResource(R.drawable.pause);
                    VideoPlayerActivity.this.hideControllerDelay();
                } else {
                    VideoPlayerActivity.this.vv.pause();
                    VideoPlayerActivity.this.bn3.setImageResource(R.drawable.play);
                }
                VideoPlayerActivity.this.isPaused = !VideoPlayerActivity.this.isPaused;
            }
        });
        this.bn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int access$12 = VideoPlayerActivity.position - 1;
                VideoPlayerActivity.position = access$12;
                if (access$12 >= 0) {
                    VideoPlayerActivity.this.vv.setVideoPath(VideoPlayerActivity.playList.get(VideoPlayerActivity.position).path);
                    VideoPlayerActivity.this.cancelDelayHide();
                    VideoPlayerActivity.this.hideControllerDelay();
                    return;
                }
                VideoPlayerActivity.this.finish();
            }
        });
        this.bn5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoPlayerActivity.this.cancelDelayHide();
                if (VideoPlayerActivity.this.isSoundShow) {
                    VideoPlayerActivity.this.mSoundWindow.dismiss();
                } else if (VideoPlayerActivity.this.mSoundWindow.isShowing()) {
                    VideoPlayerActivity.this.mSoundWindow.update(15, 0, 44, SoundView.MY_HEIGHT);
                } else {
                    VideoPlayerActivity.this.mSoundWindow.showAtLocation(VideoPlayerActivity.this.vv, 21, 15, 0);
                    VideoPlayerActivity.this.mSoundWindow.update(15, 0, 44, SoundView.MY_HEIGHT);
                }
                VideoPlayerActivity.this.isSoundShow = !VideoPlayerActivity.this.isSoundShow;
                VideoPlayerActivity.this.hideControllerDelay();
            }
        });
        this.bn5.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                boolean z;
                if (VideoPlayerActivity.this.isSilent) {
                    VideoPlayerActivity.this.bn5.setImageResource(R.drawable.soundenable);
                } else {
                    VideoPlayerActivity.this.bn5.setImageResource(R.drawable.sounddisable);
                }
                VideoPlayerActivity videoPlayerActivity = VideoPlayerActivity.this;
                if (VideoPlayerActivity.this.isSilent) {
                    z = false;
                } else {
                    z = true;
                }
                videoPlayerActivity.isSilent = z;
                VideoPlayerActivity.this.updateVolume(VideoPlayerActivity.this.currentVolume);
                VideoPlayerActivity.this.cancelDelayHide();
                VideoPlayerActivity.this.hideControllerDelay();
                return true;
            }
        });
        this.seekBar = (SeekBar) this.controlView.findViewById(R.id.seekbar);
        this.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
                if (fromUser) {
                    VideoPlayerActivity.this.vv.seekTo(progress);
                }
            }

            public void onStartTrackingTouch(SeekBar arg0) {
                VideoPlayerActivity.this.myHandler.removeMessages(1);
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                VideoPlayerActivity.this.myHandler.sendEmptyMessageDelayed(1, 6868);
            }
        });
        getScreenSize();
        this.mGestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
            public boolean onDoubleTap(MotionEvent e) {
                boolean z;
                if (VideoPlayerActivity.this.isFullScreen) {
                    VideoPlayerActivity.this.setVideoScale(1);
                } else {
                    VideoPlayerActivity.this.setVideoScale(0);
                }
                VideoPlayerActivity videoPlayerActivity = VideoPlayerActivity.this;
                if (VideoPlayerActivity.this.isFullScreen) {
                    z = false;
                } else {
                    z = true;
                }
                videoPlayerActivity.isFullScreen = z;
                Log.d(VideoPlayerActivity.TAG, "onDoubleTap");
                if (VideoPlayerActivity.this.isControllerShow) {
                    VideoPlayerActivity.this.showController();
                }
                return true;
            }

            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (!VideoPlayerActivity.this.isControllerShow) {
                    VideoPlayerActivity.this.showController();
                    VideoPlayerActivity.this.hideControllerDelay();
                    return true;
                }
                VideoPlayerActivity.this.cancelDelayHide();
                VideoPlayerActivity.this.hideController();
                return true;
            }

            public void onLongPress(MotionEvent e) {
                if (VideoPlayerActivity.this.isPaused) {
                    VideoPlayerActivity.this.vv.start();
                    VideoPlayerActivity.this.bn3.setImageResource(R.drawable.pause);
                    VideoPlayerActivity.this.cancelDelayHide();
                    VideoPlayerActivity.this.hideControllerDelay();
                } else {
                    VideoPlayerActivity.this.vv.pause();
                    VideoPlayerActivity.this.bn3.setImageResource(R.drawable.play);
                    VideoPlayerActivity.this.cancelDelayHide();
                    VideoPlayerActivity.this.showController();
                }
                VideoPlayerActivity.this.isPaused = !VideoPlayerActivity.this.isPaused;
            }
        });
        this.vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer arg0) {
                VideoPlayerActivity.this.setVideoScale(1);
                VideoPlayerActivity.this.isFullScreen = false;
                if (VideoPlayerActivity.this.isControllerShow) {
                    VideoPlayerActivity.this.showController();
                }
                int i = VideoPlayerActivity.this.vv.getDuration();
                Log.d("onCompletion", new StringBuilder().append(i).toString());
                VideoPlayerActivity.this.seekBar.setMax(i);
                int i2 = i / 1000;
                int minute = i2 / 60;
                VideoPlayerActivity.this.durationTextView.setText(String.format("%02d:%02d:%02d", Integer.valueOf(minute / 60), Integer.valueOf(minute % 60), Integer.valueOf(i2 % 60)));
                VideoPlayerActivity.this.vv.start();
                VideoPlayerActivity.this.bn3.setImageResource(R.drawable.pause);
                VideoPlayerActivity.this.hideControllerDelay();
                VideoPlayerActivity.this.myHandler.sendEmptyMessage(0);
            }
        });
        this.vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            /* Debug info: failed to restart local var, previous not found, register: 4 */
            public void onCompletion(MediaPlayer arg0) {
                int n = VideoPlayerActivity.playList.size();
                int access$12 = VideoPlayerActivity.position + 1;
                VideoPlayerActivity.position = access$12;
                if (access$12 < n) {
                    VideoPlayerActivity.this.vv.setVideoPath(VideoPlayerActivity.playList.get(VideoPlayerActivity.position).path);
                } else {
                    VideoPlayerActivity.this.finish();
                }
            }
        });
        this.vv.setVideoPath(playList.get(position).path);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == -1) {
            int result = data.getIntExtra("CHOOSE", -1);
            Log.d("RESULT", new StringBuilder().append(result).toString());
            if (result != -1) {
                this.vv.setVideoPath(playList.get(result).path);
                position = result;
                return;
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean result = this.mGestureDetector.onTouchEvent(event);
        if (result) {
            return result;
        }
        event.getAction();
        return super.onTouchEvent(event);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        getScreenSize();
        if (this.isControllerShow) {
            cancelDelayHide();
            hideController();
            showController();
            hideControllerDelay();
        }
        super.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.playedTime = this.vv.getCurrentPosition();
        this.vv.pause();
        this.bn3.setImageResource(R.drawable.play);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.vv.seekTo(this.playedTime);
        this.vv.start();
        if (this.vv.getVideoHeight() != 0) {
            this.bn3.setImageResource(R.drawable.pause);
            hideControllerDelay();
        }
        Log.d("REQUEST", "NEW AD !");
        this.adView.requestFreshAd();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.controler.isShowing()) {
            this.controler.dismiss();
            this.extralWindow.dismiss();
        }
        if (this.mSoundWindow.isShowing()) {
            this.mSoundWindow.dismiss();
        }
        this.myHandler.removeMessages(0);
        this.myHandler.removeMessages(1);
        playList.clear();
        super.onDestroy();
    }

    private void getScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        screenHeight = display.getHeight();
        screenWidth = display.getWidth();
        controlHeight = screenHeight / 4;
        this.adView = (AdView) this.extralView.findViewById(R.id.ad);
        this.adView.getLayoutParams().width = (screenWidth * 3) / 5;
    }

    /* access modifiers changed from: private */
    public void hideController() {
        if (this.controler.isShowing()) {
            this.controler.update(0, 0, 0, 0);
            this.extralWindow.update(0, 0, screenWidth, 0);
            this.isControllerShow = false;
        }
        if (this.mSoundWindow.isShowing()) {
            this.mSoundWindow.dismiss();
            this.isSoundShow = false;
        }
    }

    /* access modifiers changed from: private */
    public void hideControllerDelay() {
        this.myHandler.sendEmptyMessageDelayed(1, 6868);
    }

    /* access modifiers changed from: private */
    public void showController() {
        this.controler.update(0, 0, screenWidth, controlHeight);
        if (this.isFullScreen) {
            this.extralWindow.update(0, 0, screenWidth, 60);
        } else {
            this.extralWindow.update(0, 25, screenWidth, 60);
        }
        this.isControllerShow = true;
    }

    /* access modifiers changed from: private */
    public void cancelDelayHide() {
        this.myHandler.removeMessages(1);
    }

    /* access modifiers changed from: private */
    public void setVideoScale(int flag) {
        ViewGroup.LayoutParams layoutParams = this.vv.getLayoutParams();
        switch (flag) {
            case R.styleable.com_admob_android_ads_AdView_testing:
                Log.d(TAG, "screenWidth: " + screenWidth + " screenHeight: " + screenHeight);
                this.vv.setVideoScale(screenWidth, screenHeight);
                getWindow().addFlags(1024);
                return;
            case R.styleable.com_admob_android_ads_AdView_backgroundColor:
                int videoWidth = this.vv.getVideoWidth();
                int videoHeight = this.vv.getVideoHeight();
                int mWidth = screenWidth;
                int mHeight = screenHeight - 25;
                if (videoWidth > 0 && videoHeight > 0) {
                    if (videoWidth * mHeight > mWidth * videoHeight) {
                        mHeight = (mWidth * videoHeight) / videoWidth;
                    } else if (videoWidth * mHeight < mWidth * videoHeight) {
                        mWidth = (mHeight * videoWidth) / videoHeight;
                    }
                }
                this.vv.setVideoScale(mWidth, mHeight);
                getWindow().clearFlags(1024);
                return;
            default:
                return;
        }
    }

    private int findAlphaFromSound() {
        if (this.mAudioManager != null) {
            return ((this.currentVolume * 119) / this.maxVolume) + 85;
        }
        return 204;
    }

    /* access modifiers changed from: private */
    public void updateVolume(int index) {
        if (this.mAudioManager != null) {
            if (this.isSilent) {
                this.mAudioManager.setStreamVolume(3, 0, 0);
            } else {
                this.mAudioManager.setStreamVolume(3, index, 0);
            }
            this.currentVolume = index;
            this.bn5.setAlpha(findAlphaFromSound());
        }
    }

    /* access modifiers changed from: private */
    public void getVideoFile(final LinkedList<MovieInfo> list, File file) {
        file.listFiles(new FileFilter() {
            public boolean accept(File file) {
                String name = file.getName();
                int i = name.indexOf(46);
                if (i != -1) {
                    String name2 = name.substring(i);
                    if (name2.equalsIgnoreCase(".mp4") || name2.equalsIgnoreCase(".3gp")) {
                        MovieInfo mi = new MovieInfo();
                        mi.displayName = file.getName();
                        mi.path = file.getAbsolutePath();
                        list.add(mi);
                        return true;
                    }
                } else if (file.isDirectory()) {
                    VideoPlayerActivity.this.getVideoFile(list, file);
                }
                return false;
            }
        });
    }
}
