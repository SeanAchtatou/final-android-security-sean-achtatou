package com.music.Bluegrass;

import android.app.TabActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;
import java.util.Map;

public class StartActivity extends TabActivity implements TabHost.TabContentFactory {
    public static TabHost tabHost = null;
    private final String SETTING = "history";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(2);
        tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator("Bluegrass Music", getResources().getDrawable(R.drawable.main)).setContent(new Intent(this, MainActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator("More Sound", getResources().getDrawable(R.drawable.more)).setContent(new Intent(this, TwoActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator("Downloads", getResources().getDrawable(R.drawable.down)).setContent(new Intent(this, DownloadActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("tab4").setIndicator("Apps", getResources().getDrawable(R.drawable.home)).setContent(new Intent(this, ApkWebView.class)));
        tabHost.setCurrentTab(0);
    }

    public void onDestroy() {
        super.onDestroy();
        writeHistory();
    }

    private void writeHistory() {
        int len = DownloadApkService.history.size();
        String str = "";
        for (int i = 0; i < len; i++) {
            try {
                Map map = DownloadApkService.history.get(i);
                str = String.valueOf(str) + Uri.encode((String) map.get("NAME")) + "&" + Uri.encode((String) map.get("PATH")) + "&" + Uri.encode((String) map.get("URL")) + "&" + Integer.toString(Integer.parseInt(map.get("POINTER").toString())) + "&";
            } catch (Exception e) {
                str = "";
            }
        }
        getSharedPreferences("history", 0).edit().putString("HISTORY", str).commit();
    }

    public View createTabContent(String tag) {
        return null;
    }
}
