package com.music.Bluegrass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.music.Bluegrass.VideoPlayerActivity;
import java.util.LinkedList;

public class VideoChooseActivity extends Activity {
    private static int height;
    private static int width;
    /* access modifiers changed from: private */
    public LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public LinkedList<VideoPlayerActivity.MovieInfo> mLinkedList;
    View root;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(1);
        setContentView((int) R.layout.dialog);
        this.mLinkedList = VideoPlayerActivity.playList;
        this.mInflater = getLayoutInflater();
        ((ImageButton) findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                VideoChooseActivity.this.finish();
            }
        });
        ListView myListView = (ListView) findViewById(R.id.list);
        myListView.setAdapter((ListAdapter) new BaseAdapter() {
            public int getCount() {
                return VideoChooseActivity.this.mLinkedList.size();
            }

            public Object getItem(int arg0) {
                return Integer.valueOf(arg0);
            }

            public long getItemId(int arg0) {
                return (long) arg0;
            }

            public View getView(int arg0, View convertView, ViewGroup arg2) {
                if (convertView == null) {
                    convertView = VideoChooseActivity.this.mInflater.inflate((int) R.layout.list, (ViewGroup) null);
                }
                ((TextView) convertView.findViewById(R.id.text)).setText(((VideoPlayerActivity.MovieInfo) VideoChooseActivity.this.mLinkedList.get(arg0)).displayName);
                return convertView;
            }
        });
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent();
                intent.putExtra("CHOOSE", arg2);
                VideoChooseActivity.this.setResult(-1, intent);
                VideoChooseActivity.this.finish();
            }
        });
    }
}
