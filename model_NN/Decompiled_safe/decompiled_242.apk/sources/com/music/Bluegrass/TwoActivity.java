package com.music.Bluegrass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import java.net.URL;

public class TwoActivity extends Activity {
    private static final String ACTION = "com.music.Bluegrass.action.NEW_FILE";
    private static final int DIALOG1 = 1;
    private static final int DIALOG2 = 2;
    private static final int DIALOG3 = 3;
    private static final int ITEM1 = 2;
    private static final int ITEM2 = 3;
    private static final int ITEM3 = 4;
    private static final int ITEM4 = 5;
    private static final String PATH = "/sdcard/sound/";
    public URL gUrl;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    TwoActivity.this.showDialog(1);
                    return;
                case R.styleable.com_admob_android_ads_AdView_testing:
                default:
                    return;
                case 1:
                    TwoActivity.this.showDialog(2);
                    return;
            }
        }
    };
    private WebView mWebView;
    public ProgressDialog pd;
    Intent serviceIntent;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().requestFeature(2);
        setContentView((int) R.layout.main);
        this.mWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.setWebViewClient(new MyWebClient());
        this.mWebView.addJavascriptInterface(new DemoJavaScriptInterface(), "apkshare");
        this.mWebView.loadUrl("http://audio.apkshare.com");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    final class DemoJavaScriptInterface {
        DemoJavaScriptInterface() {
        }

        public void MediaPlay(String file_path, String title, String url) {
            TwoActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(file_path)));
        }

        public void clickOnAndroid(String file_path, String title, String url) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                Intent intentAddFile = new Intent(TwoActivity.ACTION);
                intentAddFile.putExtra("APK", file_path);
                intentAddFile.putExtra("TITLE", title);
                intentAddFile.putExtra("URL", url);
                TwoActivity.this.sendBroadcast(intentAddFile);
                Toast.makeText(TwoActivity.this.getApplicationContext(), "Had added to download list, save at sdcard", 0).show();
                return;
            }
            TwoActivity.this.showDialog(3);
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return true;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onProgressChanged(WebView view, int newProgress) {
            TwoActivity.this.getWindow().setFeatureInt(2, newProgress * 100);
            super.onProgressChanged(view, newProgress);
        }
    }

    final class MyWebClient extends WebViewClient {
        MyWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return buildDialog1(this);
            case R.styleable.com_admob_android_ads_AdView_textColor:
                return buildDialog2(this);
            case R.styleable.com_admob_android_ads_AdView_keywords:
                return buildDialog3(this);
            default:
                return null;
        }
    }

    private Dialog buildDialog1(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Download Failed");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }

    private Dialog buildDialog2(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Install the app right now ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Toast.makeText(TwoActivity.this.getApplicationContext(), "Had download to /sdcard/.", 1).show();
            }
        });
        return builder.create();
    }

    private Dialog buildDialog3(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("NO sdcard");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }
}
