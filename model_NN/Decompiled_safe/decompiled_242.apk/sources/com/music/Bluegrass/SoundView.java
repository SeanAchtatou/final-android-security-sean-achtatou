package com.music.Bluegrass;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class SoundView extends View {
    private static final int HEIGHT = 11;
    public static final int MY_HEIGHT = 163;
    public static final int MY_WIDTH = 44;
    public static final String TAG = "SoundView";
    private int bitmapHeight;
    private int bitmapWidth;
    private Bitmap bm;
    private Bitmap bm1;
    private int index;
    private Context mContext;
    private OnVolumeChangedListener mOnVolumeChangedListener;

    public interface OnVolumeChangedListener {
        void setYourVolume(int i);
    }

    public void setOnVolumeChangeListener(OnVolumeChangedListener l) {
        this.mOnVolumeChangedListener = l;
    }

    public SoundView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        init();
    }

    public SoundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init();
    }

    public SoundView(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    private void init() {
        this.bm = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.sound_line);
        this.bm1 = BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.sound_line1);
        this.bitmapWidth = this.bm.getWidth();
        this.bitmapHeight = this.bm.getHeight();
        setIndex(((AudioManager) this.mContext.getSystemService("audio")).getStreamVolume(3));
    }

    public boolean onTouchEvent(MotionEvent event) {
        int n = (((int) event.getY()) * 15) / MY_HEIGHT;
        setIndex(15 - n);
        Log.d(TAG, "setIndex: " + (15 - n));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int reverseIndex = 15 - this.index;
        for (int i = 0; i != reverseIndex; i++) {
            canvas.drawBitmap(this.bm1, new Rect(0, 0, this.bitmapWidth, this.bitmapHeight), new Rect(0, i * HEIGHT, this.bitmapWidth, (i * HEIGHT) + this.bitmapHeight), (Paint) null);
        }
        for (int i2 = reverseIndex; i2 != 15; i2++) {
            canvas.drawBitmap(this.bm, new Rect(0, 0, this.bitmapWidth, this.bitmapHeight), new Rect(0, i2 * HEIGHT, this.bitmapWidth, (i2 * HEIGHT) + this.bitmapHeight), (Paint) null);
        }
        super.onDraw(canvas);
    }

    private void setIndex(int n) {
        if (n > 15) {
            n = 15;
        } else if (n < 0) {
            n = 0;
        }
        if (this.index != n) {
            this.index = n;
            if (this.mOnVolumeChangedListener != null) {
                this.mOnVolumeChangedListener.setYourVolume(n);
            }
        }
        invalidate();
    }
}
