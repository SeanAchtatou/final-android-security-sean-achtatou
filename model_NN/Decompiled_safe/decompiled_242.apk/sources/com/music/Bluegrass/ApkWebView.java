package com.music.Bluegrass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApkWebView extends Activity {
    private static final int DIALOG1 = 1;
    private static final int DIALOG2 = 2;
    private static final int DIALOG3 = 3;
    private static final String PATH = "/sdcard/download/";
    /* access modifiers changed from: private */
    public File apk;
    public URL gUrl;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    ApkWebView.this.pd.dismiss();
                    ApkWebView.this.showDialog(1);
                    return;
                case R.styleable.com_admob_android_ads_AdView_testing /*0*/:
                    ApkWebView.this.startProgress();
                    return;
                case 1:
                    ApkWebView.this.pd.dismiss();
                    ApkWebView.this.showDialog(2);
                    return;
                default:
                    return;
            }
        }
    };
    private WebView mWebView;
    public ProgressDialog pd;

    public void startProgress() {
        this.pd = ProgressDialog.show(this, "", "Loading......");
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().requestFeature(2);
        setContentView((int) R.layout.picwebview);
        this.mWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.setWebViewClient(new MyWebClient());
        this.mWebView.addJavascriptInterface(new DemoJavaScriptInterface(), "apkshare");
        this.mWebView.loadUrl("http://tg.apkshare.com/");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }

    final class DemoJavaScriptInterface {
        DemoJavaScriptInterface() {
        }

        public void clickOnAndroid(String file_path) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                try {
                    ApkWebView.this.gUrl = new URL(file_path);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                Message msg = new Message();
                msg.what = 0;
                ApkWebView.this.handler.sendMessage(msg);
                new Downloader(ApkWebView.this, null).start();
                return;
            }
            ApkWebView.this.showDialog(3);
        }
    }

    private class Downloader extends Thread {
        private Downloader() {
        }

        /* synthetic */ Downloader(ApkWebView apkWebView, Downloader downloader) {
            this();
        }

        public void run() {
            try {
                ApkWebView.this.apk = download();
                Message msg = new Message();
                msg.what = 1;
                ApkWebView.this.handler.sendMessage(msg);
            } catch (IOException e) {
                Message msg2 = new Message();
                msg2.what = -1;
                ApkWebView.this.handler.sendMessage(msg2);
            }
        }

        private File download() throws IOException {
            new File(ApkWebView.PATH).mkdir();
            URLConnection urlConn = ApkWebView.this.gUrl.openConnection();
            InputStream is = urlConn.getInputStream();
            String filename = ApkWebView.this.getFilename(urlConn);
            File f = new File("/sdcard/download//" + filename.substring(filename.lastIndexOf(47) + 1));
            OutputStream os = new FileOutputStream(f);
            byte[] buf = new byte[1024];
            while (true) {
                int len = is.read(buf);
                if (len <= 0) {
                    os.flush();
                    os.close();
                    is.close();
                    return f;
                }
                os.write(buf, 0, len);
            }
        }
    }

    /* access modifiers changed from: private */
    public String getFilename(URLConnection c) {
        String dispFilename = getFilenameFromDisposition(c.getHeaderField("Content-Disposition"));
        if (dispFilename != null) {
            return dispFilename;
        }
        try {
            String url = c.getURL().toString();
            return url.substring(url.lastIndexOf(47) + 1);
        } catch (IndexOutOfBoundsException e) {
            return "";
        }
    }

    private String getFilenameFromDisposition(String disp) {
        Pattern p2;
        if (disp == null) {
            return null;
        }
        Matcher m1 = Pattern.compile("filename=(.)").matcher(disp);
        if (!m1.find() || m1.groupCount() < 1) {
            return null;
        }
        String delim = m1.group(1);
        if ("'".equals(delim) || "\"".equals(delim)) {
            p2 = Pattern.compile("filename=" + delim + "([^" + delim + "]*)" + delim);
        } else {
            p2 = Pattern.compile("filename=([^ ]*)( .*)?$");
        }
        Matcher m2 = p2.matcher(disp);
        if (!m2.find() || m2.groupCount() < 1) {
            return null;
        }
        return m2.group(1);
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return true;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onProgressChanged(WebView view, int newProgress) {
            ApkWebView.this.getWindow().setFeatureInt(2, newProgress * 100);
            super.onProgressChanged(view, newProgress);
        }
    }

    final class MyWebClient extends WebViewClient {
        MyWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

    /* access modifiers changed from: private */
    public void installAPK(File apk2) {
        try {
            Uri uri = Uri.parse("file://" + apk2.getAbsolutePath().toString());
            Intent installIntent = new Intent("android.intent.action.VIEW");
            installIntent.setDataAndType(uri, "application/vnd.android.package-archive");
            installIntent.addFlags(268435456);
            startActivity(installIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return buildDialog1(this);
            case 2:
                return buildDialog2(this);
            case 3:
                return buildDialog3(this);
            default:
                return null;
        }
    }

    private Dialog buildDialog1(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Download Failed");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }

    private Dialog buildDialog2(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Install the app right now ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ApkWebView.this.installAPK(ApkWebView.this.apk);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Toast.makeText(ApkWebView.this.getApplicationContext(), "Had download to /sdcard/download/.", 1).show();
            }
        });
        return builder.create();
    }

    private Dialog buildDialog3(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("NO sdcard");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return builder.create();
    }
}
