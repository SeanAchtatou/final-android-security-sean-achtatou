package com.intuitiveworks.common;

import android.app.Activity;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.intuitiveworks.memoryworks.MyHelpers;
import com.intuitiveworks.memoryworks.kids.R;

public class AdMobTracker {
    public void init(Activity act) {
        AdView adView = (AdView) act.findViewById(R.id.adView);
        if (adView != null) {
            if (MyHelpers.isPaidApp(act)) {
                adView.setVisibility(8);
                return;
            }
            adView.loadAd(new AdRequest());
            adView.setVisibility(0);
        }
    }
}
