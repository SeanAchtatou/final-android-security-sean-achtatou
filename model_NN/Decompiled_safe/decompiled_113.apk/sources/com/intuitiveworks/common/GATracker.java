package com.intuitiveworks.common;

import android.content.Context;
import android.content.pm.PackageManager;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class GATracker {
    private static GoogleAnalyticsTracker tracker;

    private GATracker() {
    }

    public static void addEvent(String action, String label) {
        if (tracker != null) {
            tracker.trackEvent("Clicks", action, label, 0);
        }
    }

    public static void stop() {
        if (tracker != null) {
            tracker.stop();
        }
    }

    public static void start(Context cont) {
        tracker = GoogleAnalyticsTracker.getInstance();
        if (tracker != null) {
            tracker.start("UA-23214992-2", 30, cont);
            String packageName = cont.getPackageName();
            try {
                tracker.trackPageView("/memoryWorksMain-" + packageName + "-" + cont.getPackageManager().getPackageInfo(packageName, 0).versionName);
            } catch (PackageManager.NameNotFoundException e) {
                tracker.trackPageView("/memoryWorksMain-" + packageName);
            }
        }
    }
}
