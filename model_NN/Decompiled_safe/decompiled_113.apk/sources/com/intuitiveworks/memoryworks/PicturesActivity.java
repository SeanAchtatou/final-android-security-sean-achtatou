package com.intuitiveworks.memoryworks;

import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.intuitiveworks.common.AdMobTracker;
import com.intuitiveworks.common.GATracker;
import com.intuitiveworks.memoryworks.MWBaseActivity;
import com.intuitiveworks.memoryworks.MyHelpers;
import com.intuitiveworks.memoryworks.kids.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

public class PicturesActivity extends MWBaseActivity {
    private static final int DEFAULT_LEVEL = 1;
    private static final int MAX_FREE_LEVEL = 3;
    private static final int MAX_NUMBER_COLUMNS = 8;
    private static final int[] m_aMenuLevels = {R.id.level1, R.id.level2, R.id.level3, R.id.level4, R.id.level5, R.id.level6, R.id.level7, R.id.level8, R.id.level9};
    private MWBaseActivity.ImageAdapter m_aGrid;
    private String[] m_aPictures;
    private TextView m_vCounter;
    private GridView m_vGrid;

    public void onCreate(Bundle savedInstanceState) {
        this.m_strArea = "Images";
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_file), 0);
        boolean bViewedInstructions = preferences.getBoolean(getString(R.string.pref_pictures_viewed_instructions), false);
        this.m_iLevel = preferences.getInt(getString(R.string.pref_pictures_level), 1);
        this.m_strCover = preferences.getString(getString(R.string.pref_pictures_cover_title), getString(R.string.cover_random));
        resetState(this.m_iLevel, false);
        setContentView((int) R.layout.grid_view);
        if (this.m_NumberOfColumns > MAX_NUMBER_COLUMNS) {
            throw new NullPointerException("Max number of columns is too high");
        }
        getCoverNames();
        resetState(this.m_iLevel, true);
        this.m_vCounter = (TextView) findViewById(R.id.errors);
        this.m_vGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                PicturesActivity.this.checkClick(position);
            }
        });
        new AdMobTracker().init(this);
        if (!bViewedInstructions) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(getString(R.string.pref_pictures_viewed_instructions), true);
            editor.putInt(getString(R.string.pref_pictures_level), this.m_iLevel);
            editor.commit();
            MyHelpers.ShowWebViewDlg(this, getString(R.string.title_instructions), getString(R.string.html_instructions_pictures));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pictures, menu);
        createCoversSubmenu(menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(m_aMenuLevels[this.m_iLevel - 1]);
        item.setChecked(item.isChecked());
        checkDefaultCoversMenuItem(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (MyHelpers.findInArray(this.m_aCIDs, itemId) >= 0) {
            this.m_strCover = item.getTitle().toString();
            resetState(this.m_iLevel, true);
            item.setChecked(item.isChecked());
            return true;
        }
        int iLevel = MyHelpers.findInArray(m_aMenuLevels, itemId);
        if (iLevel >= 0) {
            int iLevel2 = iLevel + 1;
            if (iLevel2 <= 3 || !MyHelpers.showPurchase(this)) {
                GATracker.addEvent("Menu", "pictures: selected level " + iLevel2);
                resetState(iLevel2, true);
                item.setChecked(item.isChecked());
                return true;
            }
            GATracker.addEvent("Menu", "pictures: purchase message - manual");
            return true;
        }
        switch (itemId) {
            case R.id.reset /*2130968660*/:
                GATracker.addEvent("Menu", "pictures: reset");
                resetState(this.m_iLevel, true);
                return true;
            case R.id.instructions /*2130968661*/:
                GATracker.addEvent("Menu", "pictures: instructions");
                MyHelpers.ShowWebViewDlg(this, getString(R.string.title_instructions), getString(R.string.html_instructions_pictures));
                return true;
            default:
                return false;
        }
    }

    private void createItems() {
        int numberItems = (this.m_NumberOfColumns * this.m_NumberOfRows) / 2;
        AssetManager mgr = getAssets();
        try {
            this.m_aPictures = mgr.list("pictures_default");
        } catch (IOException e) {
            Log.e("Memory Works", "can't list assets picture folder");
        }
        this.m_Bitmaps = new Bitmap[numberItems];
        this.m_itemInfo = new ArrayList();
        for (int i = 0; i < numberItems; i++) {
            InputStream stream = null;
            try {
                stream = mgr.open("pictures_default/" + this.m_aPictures[i]);
            } catch (IOException e2) {
                Log.e("Memory Works", "can't open pictures file in assets folder - " + this.m_aPictures[i]);
                e2.printStackTrace();
            }
            if (stream != null) {
                this.m_Bitmaps[i] = BitmapFactory.decodeStream(stream);
                try {
                    stream.close();
                } catch (IOException e3) {
                    Log.e("Memory Works", "can't close picture stream - " + this.m_aPictures[i]);
                    e3.printStackTrace();
                }
            }
            MyHelpers myHelpers = new MyHelpers();
            myHelpers.getClass();
            MyHelpers.MatchemItemInfo itemInfo = new MyHelpers.MatchemItemInfo(i);
            MyHelpers myHelpers2 = new MyHelpers();
            myHelpers2.getClass();
            MyHelpers.MatchemItemInfo itemInfo2 = new MyHelpers.MatchemItemInfo(i);
            this.m_itemInfo.add(itemInfo);
            this.m_itemInfo.add(itemInfo2);
        }
        Collections.shuffle(this.m_itemInfo);
        Collections.shuffle(this.m_itemInfo);
        createStandardBitmaps();
    }

    /* access modifiers changed from: private */
    public void checkClick(int p) {
        if (processGridClicked(p) > -1) {
            if (this.m_vCounter != null) {
                this.m_vCounter.setText(new StringBuilder().append(this.m_iErrors).toString());
            }
            this.m_aGrid.notifyDataSetChanged();
            if (areAllItemsUncovered()) {
                completedLevel();
            }
        }
    }

    private void completedLevel() {
        boolean bShowPurchase;
        StatsDBHelper helper = new StatsDBHelper(this);
        helper.IntertValues("Images", "Level " + this.m_iLevel, "Errors " + this.m_iErrors);
        helper.close();
        if (this.m_iLevel >= m_aMenuLevels.length) {
            resetState(this.m_iLevel, true);
            Toast.makeText(getApplicationContext(), String.format(getString(R.string.highest_level_complete), "Images"), 1).show();
            return;
        }
        if (this.m_iLevel + 1 > 3) {
            bShowPurchase = true;
        } else {
            bShowPurchase = false;
        }
        ShowNextLevelDlg(bShowPurchase);
    }

    public void resetState(int level, boolean bCreateBitmaps) {
        this.m_iLevel = level;
        initVariable();
        switch (level) {
            case 1:
                this.m_NumberOfColumns = 4;
                this.m_NumberOfRows = 4;
                break;
            case 2:
                this.m_NumberOfColumns = 5;
                this.m_NumberOfRows = 4;
                break;
            case 3:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 4;
                break;
            case 4:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 5;
                break;
            case CustomVariable.MAX_CUSTOM_VARIABLES:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 6;
                break;
            case 6:
                this.m_NumberOfColumns = 7;
                this.m_NumberOfRows = 6;
                break;
            case 7:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = 6;
                break;
            case MAX_NUMBER_COLUMNS /*8*/:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = 7;
                break;
            case 9:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = MAX_NUMBER_COLUMNS;
                break;
        }
        initWidth();
        if (bCreateBitmaps) {
            createItems();
            this.m_vGrid = (GridView) findViewById(R.id.gridView);
            this.m_vGrid.setNumColumns(this.m_NumberOfColumns);
            this.m_aGrid = new MWBaseActivity.ImageAdapter(this);
            this.m_vGrid.setAdapter((ListAdapter) this.m_aGrid);
            this.m_vCounter = (TextView) findViewById(R.id.errors);
            this.m_vCounter.setText(new StringBuilder().append(this.m_iErrors).toString());
        }
        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.pref_file), 0).edit();
        editor.putInt(getString(R.string.pref_pictures_level), this.m_iLevel);
        editor.putString(getString(R.string.pref_color_cover_title), this.m_strCover);
        editor.commit();
        setTitle(String.valueOf(this.m_strArea) + " - Level " + this.m_iLevel);
    }
}
