package com.intuitiveworks.memoryworks.kids;

public final class R {

    public static final class array {
        public static final int array_number_digits = 2131034112;
        public static final int array_start_number_digits = 2131034113;
    }

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class color {
        public static final int black = 2131099648;
        public static final int blue = 2131099650;
        public static final int white = 2131099649;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int button_number_disabled = 2130837505;
        public static final int button_number_down = 2130837506;
        public static final int button_number_normal = 2130837507;
        public static final int button_number_over = 2130837508;
        public static final int button_start_disabled = 2130837509;
        public static final int button_start_down = 2130837510;
        public static final int button_start_normal = 2130837511;
        public static final int button_start_over = 2130837512;
        public static final int button_x = 2130837513;
        public static final int facebook_icon = 2130837514;
        public static final int front_about = 2130837515;
        public static final int front_about_button_style = 2130837516;
        public static final int front_about_down = 2130837517;
        public static final int front_colors = 2130837518;
        public static final int front_colors_button_style = 2130837519;
        public static final int front_colors_down = 2130837520;
        public static final int front_feedback = 2130837521;
        public static final int front_feedback_button_style = 2130837522;
        public static final int front_feedback_down = 2130837523;
        public static final int front_numbers = 2130837524;
        public static final int front_numbers_button_style = 2130837525;
        public static final int front_numbers_down = 2130837526;
        public static final int front_pictures = 2130837527;
        public static final int front_pictures_button_style = 2130837528;
        public static final int front_pictures_down = 2130837529;
        public static final int front_results = 2130837530;
        public static final int front_results_button_style = 2130837531;
        public static final int front_results_down = 2130837532;
        public static final int front_sounds = 2130837533;
        public static final int front_sounds_button_style = 2130837534;
        public static final int front_sounds_down = 2130837535;
        public static final int icon = 2130837536;
        public static final int layout_border = 2130837537;
        public static final int number_button_style = 2130837538;
        public static final int number_eight = 2130837539;
        public static final int number_five = 2130837540;
        public static final int number_four = 2130837541;
        public static final int number_nine = 2130837542;
        public static final int number_one = 2130837543;
        public static final int number_seven = 2130837544;
        public static final int number_six = 2130837545;
        public static final int number_three = 2130837546;
        public static final int number_two = 2130837547;
        public static final int number_zero = 2130837548;
        public static final int sound = 2130837549;
        public static final int start_button_style = 2130837550;
        public static final int transparent = 2130837551;
    }

    public static final class id {
        public static final int BANNER = 2130968576;
        public static final int IAB_BANNER = 2130968578;
        public static final int IAB_LEADERBOARD = 2130968579;
        public static final int IAB_MRECT = 2130968577;
        public static final int Layout1 = 2130968628;
        public static final int RelativeLayout1 = 2130968580;
        public static final int adView = 2130968607;
        public static final int button_0 = 2130968606;
        public static final int button_1 = 2130968597;
        public static final int button_2 = 2130968598;
        public static final int button_3 = 2130968599;
        public static final int button_4 = 2130968600;
        public static final int button_5 = 2130968601;
        public static final int button_6 = 2130968602;
        public static final int button_7 = 2130968603;
        public static final int button_8 = 2130968604;
        public static final int button_9 = 2130968605;
        public static final int button_about = 2130968622;
        public static final int button_cancel = 2130968627;
        public static final int button_colors = 2130968615;
        public static final int button_digits = 2130968614;
        public static final int button_feedback = 2130968621;
        public static final int button_ok = 2130968626;
        public static final int button_pictures = 2130968617;
        public static final int button_results = 2130968620;
        public static final int button_sounds = 2130968616;
        public static final int centered_placeholder = 2130968619;
        public static final int clear = 2130968582;
        public static final int date = 2130968630;
        public static final int digit_0 = 2130968583;
        public static final int digit_1 = 2130968584;
        public static final int digit_10 = 2130968593;
        public static final int digit_11 = 2130968594;
        public static final int digit_2 = 2130968585;
        public static final int digit_3 = 2130968586;
        public static final int digit_4 = 2130968587;
        public static final int digit_5 = 2130968588;
        public static final int digit_6 = 2130968589;
        public static final int digit_7 = 2130968590;
        public static final int digit_8 = 2130968591;
        public static final int digit_9 = 2130968592;
        public static final int display = 2130968581;
        public static final int errors = 2130968611;
        public static final int gridView = 2130968608;
        public static final int instructions = 2130968661;
        public static final int intr_text = 2130968631;
        public static final int level = 2130968629;
        public static final int level1 = 2130968637;
        public static final int level10 = 2130968646;
        public static final int level11 = 2130968647;
        public static final int level12 = 2130968648;
        public static final int level13 = 2130968649;
        public static final int level14 = 2130968650;
        public static final int level15 = 2130968651;
        public static final int level16 = 2130968652;
        public static final int level17 = 2130968653;
        public static final int level18 = 2130968654;
        public static final int level19 = 2130968655;
        public static final int level2 = 2130968638;
        public static final int level20 = 2130968656;
        public static final int level21 = 2130968657;
        public static final int level22 = 2130968658;
        public static final int level23 = 2130968659;
        public static final int level24 = 2130968662;
        public static final int level25 = 2130968663;
        public static final int level26 = 2130968664;
        public static final int level27 = 2130968665;
        public static final int level28 = 2130968666;
        public static final int level3 = 2130968639;
        public static final int level4 = 2130968640;
        public static final int level5 = 2130968641;
        public static final int level6 = 2130968642;
        public static final int level7 = 2130968643;
        public static final int level8 = 2130968644;
        public static final int level9 = 2130968645;
        public static final int level_group = 2130968636;
        public static final int linearLayout0 = 2130968612;
        public static final int linearLayout1 = 2130968596;
        public static final int linearLayout2 = 2130968609;
        public static final int linearLayout3 = 2130968634;
        public static final int memory_wokrs = 2130968613;
        public static final int relativeLayout1 = 2130968618;
        public static final int remember_choice = 2130968625;
        public static final int reset = 2130968660;
        public static final int separator = 2130968632;
        public static final int start = 2130968595;
        public static final int stats_list = 2130968633;
        public static final int text1 = 2130968623;
        public static final int text2 = 2130968624;
        public static final int textView1 = 2130968610;
        public static final int webView1 = 2130968635;
    }

    public static final class layout {
        public static final int digits = 2130903040;
        public static final int grid_view = 2130903041;
        public static final int main = 2130903042;
        public static final int next_level_view = 2130903043;
        public static final int stats_row = 2130903044;
        public static final int stats_view = 2130903045;
        public static final int web_view = 2130903046;
    }

    public static final class menu {
        public static final int menu_colors = 2131296256;
        public static final int menu_digits = 2131296257;
        public static final int menu_pictures = 2131296258;
        public static final int menu_sounds = 2131296259;
    }

    public static final class string {
        public static final int about = 2131165231;
        public static final int app_name = 2131165184;
        public static final int app_name_free = 2131165185;
        public static final int app_name_kids = 2131165186;
        public static final int app_name_kids_free = 2131165187;
        public static final int congratulations = 2131165229;
        public static final int correct = 2131165223;
        public static final int cover_random = 2131165245;
        public static final int cover_style = 2131165244;
        public static final int default_number_digits = 2131165269;
        public static final int default_start_number_digits = 2131165270;
        public static final int error_cant_play_sound = 2131165253;
        public static final int error_cant_save_settings = 2131165252;
        public static final int error_dialog = 2131165247;
        public static final int error_facebook = 2131165246;
        public static final int error_failed_load_state = 2131165250;
        public static final int error_failed_save_state = 2131165251;
        public static final int error_no_facebook = 2131165248;
        public static final int error_no_market = 2131165249;
        public static final int facebook_post_submited = 2131165239;
        public static final int highest_level_complete = 2131165242;
        public static final int html_about = 2131165272;
        public static final int html_instructions_colors = 2131165273;
        public static final int html_instructions_digits = 2131165274;
        public static final int html_instructions_pictures = 2131165275;
        public static final int html_instructions_sounds = 2131165276;
        public static final int incorrect = 2131165222;
        public static final int level = 2131165226;
        public static final int level_1 = 2131165188;
        public static final int level_10 = 2131165197;
        public static final int level_11 = 2131165198;
        public static final int level_12 = 2131165199;
        public static final int level_13 = 2131165200;
        public static final int level_14 = 2131165201;
        public static final int level_15 = 2131165202;
        public static final int level_16 = 2131165203;
        public static final int level_17 = 2131165204;
        public static final int level_18 = 2131165205;
        public static final int level_19 = 2131165206;
        public static final int level_2 = 2131165189;
        public static final int level_20 = 2131165207;
        public static final int level_21 = 2131165208;
        public static final int level_22 = 2131165209;
        public static final int level_23 = 2131165210;
        public static final int level_24 = 2131165211;
        public static final int level_25 = 2131165212;
        public static final int level_26 = 2131165213;
        public static final int level_27 = 2131165214;
        public static final int level_28 = 2131165215;
        public static final int level_29 = 2131165216;
        public static final int level_3 = 2131165190;
        public static final int level_4 = 2131165191;
        public static final int level_5 = 2131165192;
        public static final int level_6 = 2131165193;
        public static final int level_7 = 2131165194;
        public static final int level_8 = 2131165195;
        public static final int level_9 = 2131165196;
        public static final int level_complete = 2131165228;
        public static final int level_complete_question = 2131165230;
        public static final int level_group = 2131165233;
        public static final int level_locked = 2131165240;
        public static final int max_number_digits = 2131165268;
        public static final int min_number_digits = 2131165271;
        public static final int no_results_found = 2131165225;
        public static final int number_digits = 2131165234;
        public static final int options = 2131165236;
        public static final int paid_app = 2131165238;
        public static final int please_upgrade = 2131165241;
        public static final int pref_color_cover_title = 2131165258;
        public static final int pref_color_level = 2131165257;
        public static final int pref_color_viewed_instructions = 2131165256;
        public static final int pref_file = 2131165267;
        public static final int pref_number_level = 2131165255;
        public static final int pref_number_viewed_instructions = 2131165254;
        public static final int pref_pictures_cover_title = 2131165264;
        public static final int pref_pictures_level = 2131165263;
        public static final int pref_pictures_viewed_instructions = 2131165262;
        public static final int pref_rememeber_choice = 2131165265;
        public static final int pref_rememebered_choice = 2131165266;
        public static final int pref_sounds_cover_title = 2131165261;
        public static final int pref_sounds_level = 2131165260;
        public static final int pref_sounds_viewed_instructions = 2131165259;
        public static final int remember_choice = 2131165243;
        public static final int reset = 2131165232;
        public static final int share_facebook = 2131165224;
        public static final int start_button = 2131165217;
        public static final int start_in = 2131165227;
        public static final int start_number_digits = 2131165235;
        public static final int statistics = 2131165237;
        public static final int title_about = 2131165220;
        public static final int title_instructions = 2131165219;
        public static final int validate_button = 2131165221;
        public static final int welcome = 2131165218;
    }

    public static final class style {
        public static final int Display = 2131230725;
        public static final int LargeText = 2131230724;
        public static final int MediumText = 2131230723;
        public static final int NumberButton = 2131230721;
        public static final int SmallText = 2131230722;
        public static final int StartButton = 2131230720;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
