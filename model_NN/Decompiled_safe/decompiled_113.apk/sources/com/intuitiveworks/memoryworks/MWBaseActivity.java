package com.intuitiveworks.memoryworks;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.intuitiveworks.memoryworks.MyHelpers;
import com.intuitiveworks.memoryworks.kids.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

public abstract class MWBaseActivity extends Activity {
    public Bitmap[] m_Bitmaps;
    public int m_NumberOfColumns = 0;
    public int m_NumberOfRows = 0;
    public int[] m_aCIDs;
    private String[] m_aCovers;
    public Bitmap m_bmpCovered;
    /* access modifiers changed from: private */
    public Bitmap m_bmpDraw = null;
    public Bitmap m_bmpSolved;
    /* access modifiers changed from: private */
    public int m_cellWidth;
    public int m_iErrors;
    public int m_iLevel;
    private MyHelpers.MatchemItemInfo m_itemFirstClicked;
    public ArrayList<MyHelpers.MatchemItemInfo> m_itemInfo;
    private MyHelpers.MatchemItemInfo m_itemSecondClicked;
    private int m_posFirstClicked;
    private int m_posSecondClicked;
    public String m_strArea;
    public String m_strCover;

    public abstract void resetState(int i, boolean z);

    public void initVariable() {
        this.m_itemFirstClicked = null;
        this.m_posFirstClicked = -1;
        this.m_itemSecondClicked = null;
        this.m_posSecondClicked = -1;
        this.m_cellWidth = 0;
        this.m_iErrors = 0;
    }

    public void initWidth() {
        WindowManager manager = getWindowManager();
        if (manager != null) {
            Display display = manager.getDefaultDisplay();
            if (display == null || this.m_NumberOfColumns == 0) {
                this.m_cellWidth = 20;
            } else {
                this.m_cellWidth = display.getWidth() / this.m_NumberOfColumns;
            }
        }
    }

    public int processGridClicked(int p) {
        MyHelpers.MatchemItemInfo item = this.m_itemInfo.get(p);
        if (item.state == 2) {
            return -1;
        }
        if (this.m_itemFirstClicked == null) {
            this.m_itemFirstClicked = item;
            this.m_posFirstClicked = p;
            item.state = 1;
            this.m_itemInfo.set(p, item);
        } else if (this.m_itemFirstClicked == null || this.m_itemSecondClicked != null) {
            if (!(this.m_itemFirstClicked == null || this.m_itemSecondClicked == null)) {
                int state = 2;
                if (this.m_itemFirstClicked.itemIndex != this.m_itemSecondClicked.itemIndex) {
                    state = 0;
                    this.m_iErrors++;
                }
                this.m_itemFirstClicked.state = state;
                this.m_itemSecondClicked.state = state;
                this.m_itemInfo.set(this.m_posFirstClicked, this.m_itemFirstClicked);
                this.m_itemInfo.set(this.m_posSecondClicked, this.m_itemSecondClicked);
                if (this.m_posSecondClicked != p) {
                    this.m_itemFirstClicked = item;
                    this.m_posFirstClicked = p;
                    item.state = 1;
                    this.m_itemInfo.set(p, item);
                } else {
                    this.m_posFirstClicked = -1;
                    this.m_itemFirstClicked = null;
                }
                this.m_posSecondClicked = -1;
                this.m_itemSecondClicked = null;
            }
        } else if (this.m_posFirstClicked != p) {
            this.m_itemSecondClicked = item;
            this.m_posSecondClicked = p;
            item.state = 1;
            this.m_itemInfo.set(p, item);
        }
        return item.itemIndex;
    }

    public void createStandardBitmaps() {
        this.m_bmpCovered = createCoverBitmap(this.m_strCover);
        this.m_bmpSolved = BitmapFactory.decodeResource(getResources(), R.drawable.transparent);
    }

    public Bitmap createCoverBitmap(String strCover) {
        if (strCover.equals(getString(R.string.cover_random))) {
            strCover = this.m_aCovers[new Random().nextInt(this.m_aCovers.length - 1)];
        }
        InputStream stream = null;
        try {
            stream = getAssets().open("covers_default/" + strCover + ".png");
        } catch (IOException e) {
            Log.e("Memory Works", "can't open cover file in assets folder - " + strCover);
            e.printStackTrace();
        }
        Bitmap bmp = null;
        if (stream != null) {
            bmp = BitmapFactory.decodeStream(stream);
            try {
                stream.close();
            } catch (IOException e2) {
                Log.e("Memory Works", "can't close picture stream - " + strCover);
                e2.printStackTrace();
            }
        }
        return bmp;
    }

    public void getCoverNames() {
        try {
            this.m_aCovers = getAssets().list("covers_default");
            for (int i = 0; i < this.m_aCovers.length; i++) {
                this.m_aCovers[i] = this.m_aCovers[i].substring(0, this.m_aCovers[i].length() - 4);
            }
        } catch (IOException e) {
            Log.e("Memory Works", "can't list assets picture folder");
        }
    }

    public void createCoversSubmenu(Menu m) {
        Random rand = new Random();
        this.m_aCIDs = new int[(this.m_aCovers.length + 1)];
        int randGroupID = rand.nextInt();
        int randRandID = rand.nextInt();
        SubMenu sub = m.addSubMenu(randGroupID, 0, 0, (int) R.string.cover_style);
        sub.add(randGroupID, randRandID, 0, getString(R.string.cover_random));
        for (int i = 0; i < this.m_aCovers.length; i++) {
            this.m_aCIDs[i] = rand.nextInt();
            sub.add(randGroupID, this.m_aCIDs[i], 0, this.m_aCovers[i]);
        }
        this.m_aCIDs[this.m_aCovers.length] = randRandID;
        sub.setGroupCheckable(randGroupID, true, true);
    }

    public void checkDefaultCoversMenuItem(Menu menu) {
        int index = MyHelpers.findInArray(this.m_aCovers, this.m_strCover);
        if (index > -1) {
            MenuItem item = menu.findItem(this.m_aCIDs[index]);
            item.setChecked(item.isChecked());
        }
    }

    public void ShowNextLevelDlg(boolean bShowPurchase) {
        String text1 = String.format(getString(R.string.level_complete), Integer.valueOf(this.m_iLevel));
        String text2 = String.format(getString(R.string.level_complete_question), Integer.valueOf(this.m_iLevel + 1));
        NextLevelAlertDialog dialog = new NextLevelAlertDialog(this, this.m_strArea);
        if (dialog.getRememberedAnswer() != 1) {
            dialog.setContentView((int) R.layout.next_level_view);
            dialog.setTitle(getString(R.string.congratulations));
            dialog.setCancelable(true);
            dialog.setLevel(this.m_iLevel);
            dialog.setShowPurchase(bShowPurchase);
            ((TextView) dialog.findViewById(R.id.text1)).setText(text1);
            ((TextView) dialog.findViewById(R.id.text2)).setText(text2);
            dialog.show();
            return;
        }
        resetState(this.m_iLevel + 1, this.m_strArea != "Numbers");
    }

    public boolean areAllItemsUncovered() {
        for (int i = 0; i < this.m_itemInfo.size(); i++) {
            MyHelpers.MatchemItemInfo item = this.m_itemInfo.get(i);
            if (item.state != 2 && item.state != 1) {
                return false;
            }
        }
        return true;
    }

    public class ImageAdapter extends BaseAdapter {
        Context m_context;

        public ImageAdapter(Context _MyContext) {
            this.m_context = _MyContext;
        }

        public int getCount() {
            return MWBaseActivity.this.m_NumberOfColumns * MWBaseActivity.this.m_NumberOfRows;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = null;
            if (convertView != null) {
                imageView = (ImageView) convertView;
            } else if (convertView == null) {
                imageView = new ImageView(this.m_context);
                imageView.setLayoutParams(new AbsListView.LayoutParams(MWBaseActivity.this.m_cellWidth, MWBaseActivity.this.m_cellWidth));
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setPadding(1, 1, 1, 1);
            }
            MyHelpers.MatchemItemInfo bi = MWBaseActivity.this.m_itemInfo.get(position);
            if (bi.state == 0) {
                imageView.setImageBitmap(MWBaseActivity.this.m_bmpCovered);
            } else if (bi.state == 2) {
                imageView.setImageBitmap(MWBaseActivity.this.m_bmpSolved);
            } else if (MWBaseActivity.this.m_bmpDraw == null) {
                imageView.setImageBitmap(MWBaseActivity.this.m_Bitmaps[bi.itemIndex]);
            } else {
                imageView.setImageBitmap(MWBaseActivity.this.m_bmpDraw);
            }
            return imageView;
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public void setBitmap(Bitmap b) {
            MWBaseActivity.this.m_bmpDraw = b;
        }
    }
}
