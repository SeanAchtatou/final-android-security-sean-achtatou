package com.intuitiveworks.memoryworks;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.intuitiveworks.common.AdMobTracker;
import com.intuitiveworks.common.GATracker;
import com.intuitiveworks.memoryworks.MWBaseActivity;
import com.intuitiveworks.memoryworks.MyHelpers;
import com.intuitiveworks.memoryworks.kids.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ColorsActivity extends MWBaseActivity {
    private static final int BITMAP_SIZE = 96;
    private static final int DEFAULT_LEVEL = 1;
    private static final int MAX_FREE_LEVEL = 3;
    private static final int MAX_NUMBER_COLUMNS = 8;
    private static final int[] m_Colors = {-65536, -26164, -10092544, -65281, -10079284, -16776961, -8407812, -16595772, -16711936, -8389408, -7013632, -256, -5211904, -39424, -3355444, -12303292, -16777216, -1};
    private static final int[] m_aMenuLevels = {R.id.level1, R.id.level2, R.id.level3, R.id.level4, R.id.level5, R.id.level6, R.id.level7, R.id.level8, R.id.level9, R.id.level10, R.id.level11, R.id.level12, R.id.level13, R.id.level14, R.id.level15, R.id.level16, R.id.level17, R.id.level18, R.id.level19, R.id.level20, R.id.level21, R.id.level22, R.id.level23};
    private MWBaseActivity.ImageAdapter m_aGrid;
    private boolean m_bColorOnlyMode;
    private boolean m_bDrawCircle;
    private TextView m_vCounter;
    private GridView m_vGrid;

    public void onCreate(Bundle savedInstanceState) {
        this.m_strArea = "Colors";
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_file), 0);
        boolean bViewedInstructions = preferences.getBoolean(getString(R.string.pref_color_viewed_instructions), false);
        this.m_iLevel = preferences.getInt(getString(R.string.pref_color_level), 1);
        this.m_strCover = preferences.getString(getString(R.string.pref_color_cover_title), getString(R.string.cover_random));
        resetState(this.m_iLevel, false);
        setContentView((int) R.layout.grid_view);
        if (this.m_NumberOfColumns > MAX_NUMBER_COLUMNS) {
            throw new NullPointerException("Max number of columns is too high");
        }
        getCoverNames();
        resetState(this.m_iLevel, true);
        this.m_vCounter = (TextView) findViewById(R.id.errors);
        this.m_vGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ColorsActivity.this.checkClick(position);
            }
        });
        new AdMobTracker().init(this);
        if (!bViewedInstructions) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(getString(R.string.pref_color_viewed_instructions), true);
            editor.putInt(getString(R.string.pref_color_level), this.m_iLevel);
            editor.commit();
            MyHelpers.ShowWebViewDlg(this, getString(R.string.title_instructions), getString(R.string.html_instructions_colors));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_colors, menu);
        createCoversSubmenu(menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(m_aMenuLevels[this.m_iLevel - 1]);
        item.setChecked(item.isChecked());
        checkDefaultCoversMenuItem(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (MyHelpers.findInArray(this.m_aCIDs, itemId) >= 0) {
            this.m_strCover = item.getTitle().toString();
            resetState(this.m_iLevel, true);
            item.setChecked(item.isChecked());
            return true;
        }
        int iLevel = MyHelpers.findInArray(m_aMenuLevels, itemId);
        if (iLevel >= 0) {
            int iLevel2 = iLevel + 1;
            if (iLevel2 <= 3 || !MyHelpers.showPurchase(this)) {
                GATracker.addEvent("Menu", "color: selected level " + iLevel2);
                resetState(iLevel2, true);
                item.setChecked(item.isChecked());
                return true;
            }
            GATracker.addEvent("Menu", "color: purchase message - manual");
            return true;
        }
        switch (itemId) {
            case R.id.reset /*2130968660*/:
                GATracker.addEvent("Menu", "color: reset");
                resetState(this.m_iLevel, true);
                return true;
            case R.id.instructions /*2130968661*/:
                GATracker.addEvent("Menu", "color: instructions");
                MyHelpers.ShowWebViewDlg(this, getString(R.string.title_instructions), getString(R.string.html_instructions_colors));
                return true;
            default:
                return false;
        }
    }

    private void createBitmaps() {
        int color;
        int color2;
        String strColor;
        boolean z;
        ArrayList<Integer> colors = new ArrayList<>();
        ArrayList<Integer> colors2 = new ArrayList<>();
        for (int i = 0; i < m_Colors.length; i++) {
            colors.add(Integer.valueOf(m_Colors[i]));
            colors2.add(Integer.valueOf(m_Colors[i]));
        }
        Collections.shuffle(colors);
        Collections.shuffle(colors2);
        int numberBitmaps = (this.m_NumberOfColumns * this.m_NumberOfRows) / 2;
        this.m_Bitmaps = new Bitmap[numberBitmaps];
        this.m_itemInfo = new ArrayList();
        Map<String, String> uniqueColor = new HashMap<>();
        for (int i2 = 0; i2 < numberBitmaps; i2++) {
            this.m_Bitmaps[i2] = Bitmap.createBitmap(BITMAP_SIZE, BITMAP_SIZE, Bitmap.Config.RGB_565);
            while (true) {
                color = ((Integer) colors.get(i2 / this.m_NumberOfColumns)).intValue();
                color2 = ((Integer) colors2.get(i2 % this.m_NumberOfColumns)).intValue();
                if (this.m_bColorOnlyMode) {
                    color = ((Integer) colors.get(i2)).intValue();
                    color2 = color;
                } else if (color == color2) {
                    Collections.shuffle(colors);
                    Collections.shuffle(colors2);
                }
                strColor = String.format("%d-%d", Integer.valueOf(color), Integer.valueOf(color2));
                if (!uniqueColor.containsKey(strColor)) {
                    break;
                }
                Collections.shuffle(colors);
                Collections.shuffle(colors2);
            }
            uniqueColor.put(strColor, "");
            MyHelpers myHelpers = new MyHelpers();
            myHelpers.getClass();
            MyHelpers.MatchemItemInfo itemInfo = new MyHelpers.MatchemItemInfo(i2);
            MyHelpers myHelpers2 = new MyHelpers();
            myHelpers2.getClass();
            MyHelpers.MatchemItemInfo itemInfo2 = new MyHelpers.MatchemItemInfo(i2);
            this.m_itemInfo.add(itemInfo);
            this.m_itemInfo.add(itemInfo2);
            Bitmap bitmap = this.m_Bitmaps[i2];
            if (i2 % 2 == 0) {
                z = true;
            } else {
                z = false;
            }
            paintRect(bitmap, color, color2, z);
        }
        Collections.shuffle(this.m_itemInfo);
        createStandardBitmaps();
    }

    private void paintRect(Bitmap bitmap, int color, int color2, boolean altColor) {
        int i;
        Paint paint = new Paint();
        Paint paint2 = new Paint();
        Paint paint3 = new Paint();
        paint.setColor(color);
        paint2.setColor(color2);
        paint3.setColor(-16777216);
        paint3.setStyle(Paint.Style.STROKE);
        paint3.setStrokeWidth(3.0f);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawRect(0.0f, 0.0f, 96.0f, 48.0f, paint);
        canvas.drawRect(0.0f, 48.0f, 96.0f, 96.0f, paint2);
        canvas.drawRect(0.0f, 0.0f, 96.0f, 96.0f, paint3);
        if (this.m_bDrawCircle) {
            Paint paint4 = new Paint();
            if (altColor) {
                i = color;
            } else {
                i = color2;
            }
            paint4.setColor(i);
            canvas.drawCircle(48.0f, 48.0f, 32.0f, paint4);
        }
    }

    /* access modifiers changed from: private */
    public void checkClick(int p) {
        if (processGridClicked(p) > -1) {
            if (this.m_vCounter != null) {
                this.m_vCounter.setText(new StringBuilder().append(this.m_iErrors).toString());
            }
            this.m_aGrid.notifyDataSetChanged();
            if (areAllItemsUncovered()) {
                completedLevel();
            }
        }
    }

    private void completedLevel() {
        boolean bShowPurchase;
        StatsDBHelper helper = new StatsDBHelper(this);
        helper.IntertValues("Colors", "Level " + this.m_iLevel, "Errors " + this.m_iErrors);
        helper.close();
        if (this.m_iLevel >= m_aMenuLevels.length) {
            resetState(this.m_iLevel, true);
            Toast.makeText(getApplicationContext(), String.format(getString(R.string.highest_level_complete), "Colors"), 1).show();
            return;
        }
        if (this.m_iLevel + 1 > 3) {
            bShowPurchase = true;
        } else {
            bShowPurchase = false;
        }
        ShowNextLevelDlg(bShowPurchase);
    }

    public void resetState(int level, boolean bCreateBitmaps) {
        this.m_iLevel = level;
        initVariable();
        this.m_bColorOnlyMode = false;
        this.m_bDrawCircle = false;
        switch (level) {
            case 1:
                this.m_NumberOfColumns = 4;
                this.m_NumberOfRows = 4;
                this.m_bColorOnlyMode = true;
                break;
            case 2:
                this.m_NumberOfColumns = 4;
                this.m_NumberOfRows = 4;
                break;
            case 3:
                this.m_NumberOfColumns = 4;
                this.m_NumberOfRows = 4;
                this.m_bDrawCircle = true;
                break;
            case 4:
                this.m_NumberOfColumns = 5;
                this.m_NumberOfRows = 4;
                this.m_bColorOnlyMode = true;
                break;
            case CustomVariable.MAX_CUSTOM_VARIABLES:
                this.m_NumberOfColumns = 5;
                this.m_NumberOfRows = 4;
                break;
            case 6:
                this.m_NumberOfColumns = 5;
                this.m_NumberOfRows = 4;
                this.m_bDrawCircle = true;
                break;
            case 7:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 4;
                this.m_bColorOnlyMode = true;
                break;
            case MAX_NUMBER_COLUMNS /*8*/:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 4;
                break;
            case 9:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 4;
                this.m_bDrawCircle = true;
                break;
            case 10:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 5;
                this.m_bColorOnlyMode = true;
                break;
            case 11:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 5;
                break;
            case 12:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 5;
                this.m_bDrawCircle = true;
                break;
            case 13:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 6;
                this.m_bColorOnlyMode = true;
                break;
            case 14:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 6;
                break;
            case 15:
                this.m_NumberOfColumns = 6;
                this.m_NumberOfRows = 6;
                this.m_bDrawCircle = true;
            case 16:
                this.m_NumberOfColumns = 7;
                this.m_NumberOfRows = 6;
                break;
            case 17:
                this.m_NumberOfColumns = 7;
                this.m_NumberOfRows = 6;
                this.m_bDrawCircle = true;
                break;
            case 18:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = 6;
                break;
            case 19:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = 6;
                this.m_bDrawCircle = true;
                break;
            case 20:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = 7;
                break;
            case 21:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = 7;
                this.m_bDrawCircle = true;
                break;
            case 22:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = MAX_NUMBER_COLUMNS;
                break;
            case 23:
                this.m_NumberOfColumns = MAX_NUMBER_COLUMNS;
                this.m_NumberOfRows = MAX_NUMBER_COLUMNS;
                this.m_bDrawCircle = true;
                break;
        }
        initWidth();
        if (bCreateBitmaps) {
            createBitmaps();
            this.m_vGrid = (GridView) findViewById(R.id.gridView);
            this.m_vGrid.setNumColumns(this.m_NumberOfColumns);
            this.m_aGrid = new MWBaseActivity.ImageAdapter(this);
            this.m_vGrid.setAdapter((ListAdapter) this.m_aGrid);
            this.m_vCounter = (TextView) findViewById(R.id.errors);
            this.m_vCounter.setText(new StringBuilder().append(this.m_iErrors).toString());
        }
        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.pref_file), 0).edit();
        editor.putInt(getString(R.string.pref_color_level), this.m_iLevel);
        editor.putString(getString(R.string.pref_color_cover_title), this.m_strCover);
        editor.commit();
        setTitle(String.valueOf(this.m_strArea) + " - Level " + this.m_iLevel);
    }
}
