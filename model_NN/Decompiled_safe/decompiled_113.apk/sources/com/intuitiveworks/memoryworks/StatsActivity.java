package com.intuitiveworks.memoryworks;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.intuitiveworks.memoryworks.kids.R;

public class StatsActivity extends Activity {
    private static final String FACEBOOK_APPID = "216056691779773";
    private static final String[] fields = {"level", "errors", "date", "_id"};
    private CursorAdapter m_dataSource;
    private SQLiteDatabase m_database;
    private Facebook m_facebook;
    private ListView m_vList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.stats_view);
        this.m_database = new StatsDBHelper(this).getWritableDatabase();
        Cursor data = this.m_database.query("statistics", fields, null, null, null, null, "date DESC");
        this.m_dataSource = new SimpleCursorAdapter(this, R.layout.stats_row, data, fields, new int[]{R.id.level, R.id.errors, R.id.date});
        this.m_vList = (ListView) findViewById(R.id.stats_list);
        this.m_vList.setAdapter((ListAdapter) this.m_dataSource);
        TextView tv = (TextView) findViewById(R.id.intr_text);
        ImageView im = (ImageView) findViewById(R.id.separator);
        if (data.getCount() == 0) {
            tv.setText(getString(R.string.no_results_found));
            im.setVisibility(8);
        } else {
            im.setVisibility(0);
        }
        this.m_vList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                StatsActivity.this.authorizeFacebook();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.m_facebook.authorizeCallback(requestCode, resultCode, data);
    }

    /* access modifiers changed from: private */
    public void authorizeFacebook() {
        this.m_facebook = new Facebook(FACEBOOK_APPID);
        if (this.m_facebook == null) {
            Toast.makeText(getApplicationContext(), getString(R.string.error_no_facebook), 0).show();
        } else {
            this.m_facebook.authorize(this, new Facebook.DialogListener() {
                public void onComplete(Bundle values) {
                    StatsActivity.this.displayFeedDialog();
                }

                public void onFacebookError(FacebookError error) {
                    Toast.makeText(StatsActivity.this.getApplicationContext(), StatsActivity.this.getString(R.string.error_facebook), 0).show();
                }

                public void onError(DialogError e) {
                    Toast.makeText(StatsActivity.this.getApplicationContext(), StatsActivity.this.getString(R.string.error_dialog), 0).show();
                }

                public void onCancel() {
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void displayFeedDialog() {
        Bundle parameters = new Bundle();
        parameters.putString("picture", "http://www.intuitiveworks.biz/icon.png");
        this.m_facebook.dialog(this, "feed", parameters, new Facebook.DialogListener() {
            public void onComplete(Bundle values) {
                Toast.makeText(StatsActivity.this.getApplicationContext(), StatsActivity.this.getString(R.string.facebook_post_submited), 0).show();
            }

            public void onFacebookError(FacebookError error) {
                Toast.makeText(StatsActivity.this.getApplicationContext(), StatsActivity.this.getString(R.string.error_facebook), 0).show();
            }

            public void onError(DialogError e) {
                Toast.makeText(StatsActivity.this.getApplicationContext(), StatsActivity.this.getString(R.string.error_dialog), 0).show();
            }

            public void onCancel() {
            }
        });
    }
}
