package com.intuitiveworks.memoryworks;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.intuitiveworks.common.AdMobTracker;
import com.intuitiveworks.common.GATracker;
import com.intuitiveworks.memoryworks.MyHelpers;
import com.intuitiveworks.memoryworks.kids.R;
import java.util.Random;

public class DigitsActivity extends MWBaseActivity {
    private static final int DEFAULT_LEVEL = 1;
    private static final int MAX_FREE_LEVEL = 3;
    private int[] m_aButtons = {R.id.button_0, R.id.button_1, R.id.button_2, R.id.button_3, R.id.button_4, R.id.button_5, R.id.button_6, R.id.button_7, R.id.button_8, R.id.button_9};
    private int[] m_aDigits = {R.id.digit_0, R.id.digit_1, R.id.digit_2, R.id.digit_3, R.id.digit_4, R.id.digit_5, R.id.digit_6, R.id.digit_7, R.id.digit_8, R.id.digit_9, R.id.digit_10, R.id.digit_11};
    private int[] m_aDigitsImg = {R.drawable.number_zero, R.drawable.number_one, R.drawable.number_two, R.drawable.number_three, R.drawable.number_four, R.drawable.number_five, R.drawable.number_six, R.drawable.number_seven, R.drawable.number_eight, R.drawable.number_nine};
    private int[] m_aMenuLevels = {R.id.level1, R.id.level2, R.id.level3, R.id.level4, R.id.level5, R.id.level6, R.id.level7, R.id.level8, R.id.level9, R.id.level10, R.id.level11, R.id.level12, R.id.level13, R.id.level14, R.id.level15, R.id.level16, R.id.level17, R.id.level18, R.id.level19, R.id.level20, R.id.level21, R.id.level22, R.id.level23, R.id.level24, R.id.level25, R.id.level26, R.id.level27, R.id.level28};
    private boolean m_bStarted;
    private int m_iLastNumber;
    private int m_iMaxNumberDigits;
    private String m_strEnteredNumber;
    private String m_strTotalNumber;
    private ImageButton m_vClear;
    private TextView m_vDisplay;
    private Button m_vStart;

    public void onCreate(Bundle savedInstanceState) {
        this.m_strArea = "Numbers";
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.digits);
        this.m_vStart = (Button) findViewById(R.id.start);
        this.m_vDisplay = (TextView) findViewById(R.id.display);
        this.m_vClear = (ImageButton) findViewById(R.id.clear);
        setButtonDimentions();
        SharedPreferences preferences = getSharedPreferences(getString(R.string.pref_file), 0);
        boolean bViewedInstructions = preferences.getBoolean(getString(R.string.pref_number_viewed_instructions), false);
        this.m_iLevel = preferences.getInt(getString(R.string.pref_number_level), 1);
        if (!bViewedInstructions) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(getString(R.string.pref_number_viewed_instructions), true);
            editor.commit();
            MyHelpers.ShowWebViewDlg(this, getString(R.string.title_instructions), getString(R.string.html_instructions_digits));
        }
        new AdMobTracker().init(this);
        resetState(this.m_iLevel, bViewedInstructions);
    }

    private void setButtonDimentions() {
        MyHelpers.ElementDimentions dm = new MyHelpers().GetDimentions(this);
        this.m_vStart.setHeight(dm.button_ht);
        this.m_vStart.setWidth(dm.button_wt);
        this.m_vStart.setTextSize((float) dm.text_sz);
        for (int item : this.m_aButtons) {
            Button vButton = (Button) findViewById(item);
            vButton.setLayoutParams(new LinearLayout.LayoutParams(dm.screen_wt / 3, dm.button_ht));
            vButton.setTextSize((float) dm.text_sz);
        }
    }

    public void OnStartClicked(View v) {
        if (!this.m_bStarted) {
            startCountDownTimer(3);
        } else {
            Validate();
        }
    }

    public void OnClearClicked(View v) {
        this.m_strEnteredNumber = "";
        showDisplayMessage("");
        this.m_vClear.setVisibility(4);
    }

    public void onButtonClick(View v) {
        if (this.m_strEnteredNumber.length() < Integer.parseInt(getString(R.string.max_number_digits))) {
            this.m_strEnteredNumber = String.valueOf(this.m_strEnteredNumber) + MyHelpers.findInArray(this.m_aButtons, v.getId());
            displayDigitsBasedOnText(this.m_strEnteredNumber);
            this.m_vClear.setVisibility(0);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_digits, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(this.m_aMenuLevels[this.m_iLevel - 1]);
        item.setChecked(item.isChecked());
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        int iLevel = MyHelpers.findInArray(this.m_aMenuLevels, itemId);
        if (iLevel >= 0) {
            int iLevel2 = iLevel + 1;
            if (iLevel2 <= 3 || !MyHelpers.showPurchase(this)) {
                GATracker.addEvent("Menu", "digits: selected level " + iLevel2);
                resetState(iLevel2, true);
                item.setChecked(item.isChecked());
                return true;
            }
            GATracker.addEvent("Menu", "digits: purchase message - manual");
            return true;
        }
        switch (itemId) {
            case R.id.reset /*2130968660*/:
                GATracker.addEvent("Menu", "digits: reset");
                resetState(this.m_iLevel, true);
                return true;
            case R.id.instructions /*2130968661*/:
                GATracker.addEvent("Menu", "digits: instructions");
                MyHelpers.ShowWebViewDlg(this, getString(R.string.title_instructions), getString(R.string.html_instructions_digits));
                return true;
            default:
                return false;
        }
    }

    private void Validate() {
        this.m_vClear.setVisibility(4);
        if (!this.m_strTotalNumber.equals(this.m_strEnteredNumber)) {
            showDisplayMessage(getString(R.string.incorrect));
            this.m_iErrors++;
            this.m_strEnteredNumber = "";
            new CountDownTimer(1000, 500) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    DigitsActivity.this.startDisableTimer();
                }
            }.start();
        } else if (this.m_strTotalNumber.length() == this.m_iMaxNumberDigits) {
            finishTheGame();
        } else {
            this.m_strEnteredNumber = "";
            continueTheGame();
        }
    }

    private void startCountDownTimer(int delay) {
        new CountDownTimer((long) (delay * 1000), 500) {
            public void onTick(long millisUntilFinished) {
                DigitsActivity.this.showDisplayMessage(String.valueOf(DigitsActivity.this.getString(R.string.start_in)) + " " + ((millisUntilFinished / 1000) + 1));
            }

            public void onFinish() {
                DigitsActivity.this.startTheGame(DigitsActivity.this.m_iLevel);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void startTheGame(int level) {
        this.m_strEnteredNumber = "";
        this.m_strTotalNumber = "";
        this.m_bStarted = true;
        this.m_iLevel = getSharedPreferences(getString(R.string.pref_file), 0).getInt(getString(R.string.pref_number_level), 1);
        int iStartNumberDigits = 1;
        switch (this.m_iLevel) {
            case 1:
            case 2:
            case 3:
            case 4:
                this.m_iMaxNumberDigits = 6;
                break;
            case CustomVariable.MAX_CUSTOM_VARIABLES:
            case 6:
            case 7:
            case 8:
                iStartNumberDigits = 2;
                this.m_iMaxNumberDigits = 7;
                break;
            case 9:
            case 10:
            case 11:
            case 12:
                iStartNumberDigits = 3;
                this.m_iMaxNumberDigits = 8;
                break;
            case 13:
            case 14:
            case 15:
            case 16:
                iStartNumberDigits = 4;
                this.m_iMaxNumberDigits = 9;
                break;
            case 17:
            case 18:
            case 19:
            case 20:
                iStartNumberDigits = 5;
                this.m_iMaxNumberDigits = 10;
                break;
            case 21:
            case 22:
            case 23:
            case 24:
                iStartNumberDigits = 5;
                this.m_iMaxNumberDigits = 11;
                break;
            case 25:
            case 26:
            case 27:
            case 28:
                iStartNumberDigits = 5;
                this.m_iMaxNumberDigits = 12;
                break;
        }
        for (int i = 0; i < iStartNumberDigits; i++) {
            this.m_strTotalNumber = String.valueOf(this.m_strTotalNumber) + getRandomNumber();
        }
        startDisableTimer();
    }

    /* access modifiers changed from: private */
    public void startDisableTimer() {
        displayDigitsBasedOnText(this.m_strTotalNumber);
        this.m_vStart.setText(getString(R.string.validate_button));
        enableButtons(false, false);
        new CountDownTimer(3000, 3000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                DigitsActivity.this.showDisplayMessage("");
                DigitsActivity.this.enableButtons(true, false);
            }
        }.start();
    }

    private void continueTheGame() {
        if (this.m_iLevel == 1 || this.m_iLevel == 5 || this.m_iLevel == 9 || this.m_iLevel == 13 || this.m_iLevel == 17 || this.m_iLevel == 21 || this.m_iLevel == 25) {
            this.m_strTotalNumber = String.valueOf(this.m_strTotalNumber) + getRandomNumber();
        } else if (this.m_iLevel == 2 || this.m_iLevel == 6 || this.m_iLevel == 10 || this.m_iLevel == 14 || this.m_iLevel == 18 || this.m_iLevel == 22 || this.m_iLevel == 26) {
            this.m_strTotalNumber = String.valueOf(getRandomNumber()) + this.m_strTotalNumber;
        } else if (this.m_iLevel == 3 || this.m_iLevel == 7 || this.m_iLevel == 11 || this.m_iLevel == 15 || this.m_iLevel == 19 || this.m_iLevel == 23 || this.m_iLevel == 27) {
            if (new Random().nextInt(9) > 4) {
                this.m_strTotalNumber = String.valueOf(getRandomNumber()) + this.m_strTotalNumber;
            } else {
                this.m_strTotalNumber = String.valueOf(this.m_strTotalNumber) + getRandomNumber();
            }
        } else if (this.m_iLevel == 4 || this.m_iLevel == 8 || this.m_iLevel == 12 || this.m_iLevel == 16 || this.m_iLevel == 20 || this.m_iLevel == 24 || this.m_iLevel == 28) {
            String strNew = "";
            for (int i = 0; i < this.m_strTotalNumber.length() + 1; i++) {
                strNew = String.valueOf(strNew) + getRandomNumber();
            }
            this.m_strTotalNumber = strNew;
        }
        startDisableTimer();
    }

    private void finishTheGame() {
        showDisplayMessage(String.format(getString(R.string.level_complete), Integer.valueOf(this.m_iLevel)));
        this.m_vStart.setText(getString(R.string.start_button));
        StatsDBHelper helper = new StatsDBHelper(this);
        helper.IntertValues("Numbers", "Level " + this.m_iLevel, "Errors " + this.m_iErrors);
        helper.close();
        showNextLevelDialog();
    }

    private String getRandomNumber() {
        Random rand = new Random();
        int iRand = rand.nextInt(9) + 1;
        if (this.m_iLastNumber == iRand) {
            iRand = rand.nextInt(9) + 1;
        }
        this.m_iLastNumber = iRand;
        return String.format("%d", Integer.valueOf(iRand));
    }

    /* access modifiers changed from: private */
    public void enableButtons(boolean bEnable, boolean bFirst) {
        if (!bFirst) {
            this.m_vStart.setEnabled(bEnable);
        }
        for (int item : this.m_aButtons) {
            ((Button) findViewById(item)).setEnabled(bEnable);
        }
    }

    public void resetState(int level, boolean bStartGame) {
        this.m_iLevel = level;
        this.m_strEnteredNumber = "";
        this.m_bStarted = false;
        this.m_strTotalNumber = "";
        this.m_iLastNumber = 100;
        this.m_iErrors = 0;
        enableButtons(false, true);
        this.m_vStart.setText(getString(R.string.start_button));
        this.m_vClear.setVisibility(4);
        hideDigits();
        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.pref_file), 0).edit();
        if (editor != null) {
            editor.putInt(getString(R.string.pref_number_level), this.m_iLevel);
            editor.commit();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.error_cant_save_settings), 0).show();
        }
        if (bStartGame) {
            startCountDownTimer(3);
        } else {
            showDisplayMessage(getString(R.string.welcome));
        }
        setTitle(String.valueOf(this.m_strArea) + " - Level " + this.m_iLevel);
    }

    private void showNextLevelDialog() {
        boolean bShowPurchase;
        if (this.m_iLevel >= this.m_aMenuLevels.length) {
            Toast.makeText(getApplicationContext(), String.format(getString(R.string.highest_level_complete), "Numbers"), 1).show();
            return;
        }
        if (this.m_iLevel + 1 > 3) {
            bShowPurchase = true;
        } else {
            bShowPurchase = false;
        }
        ShowNextLevelDlg(bShowPurchase);
    }

    private void hideDigits() {
        for (int item : this.m_aDigits) {
            ((ImageButton) findViewById(item)).setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void showDisplayMessage(String text) {
        hideDigits();
        this.m_vDisplay.setText(text);
    }

    private void displayDigitsBasedOnText(String text) {
        this.m_vDisplay.setText("");
        hideDigits();
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            ImageButton vButton = (ImageButton) findViewById(this.m_aDigits[i]);
            vButton.setVisibility(0);
            if (i != 0) {
                ((RelativeLayout.LayoutParams) vButton.getLayoutParams()).addRule(1, this.m_aDigits[i - 1]);
            }
            vButton.setImageResource(this.m_aDigitsImg[Character.getNumericValue(ch)]);
        }
    }
}
