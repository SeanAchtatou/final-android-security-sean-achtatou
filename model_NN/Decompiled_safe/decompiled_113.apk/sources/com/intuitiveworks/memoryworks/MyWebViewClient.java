package com.intuitiveworks.memoryworks;

import android.app.Activity;
import android.content.Intent;
import android.net.MailTo;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyWebViewClient extends WebViewClient {
    Activity mContext;

    public MyWebViewClient(Activity context) {
        this.mContext = context;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("mailto:")) {
            MailTo mt = MailTo.parse(url);
            Intent i = new Intent("android.intent.action.SEND");
            i.setType("text/plain");
            i.putExtra("android.intent.extra.EMAIL", new String[]{mt.getTo()});
            i.putExtra("android.intent.extra.SUBJECT", mt.getSubject());
            i.putExtra("android.intent.extra.CC", mt.getCc());
            i.putExtra("android.intent.extra.TEXT", mt.getBody());
            this.mContext.startActivity(i);
            view.reload();
            return true;
        }
        view.loadUrl(url);
        return true;
    }
}
