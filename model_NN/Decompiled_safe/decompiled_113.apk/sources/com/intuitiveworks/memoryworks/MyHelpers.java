package com.intuitiveworks.memoryworks;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.Display;
import android.webkit.WebView;
import android.widget.Toast;
import com.intuitiveworks.common.GATracker;
import com.intuitiveworks.memoryworks.kids.R;

public class MyHelpers {
    public ElementDimentions GetDimentions(Activity a) {
        ElementDimentions dm = new ElementDimentions();
        Display display = a.getWindowManager().getDefaultDisplay();
        dm.screen_ht = display.getHeight();
        dm.screen_wt = display.getWidth();
        dm.button_ht = 52;
        dm.button_wt = 140;
        dm.text_sz = 20;
        if (dm.screen_ht >= 800 && dm.screen_ht < 1000) {
            dm.button_ht = 90;
            dm.button_wt = 250;
            dm.text_sz = 32;
        } else if (dm.screen_ht > 1000) {
            dm.button_ht = 180;
            dm.button_wt = 400;
            dm.text_sz = 48;
        }
        return dm;
    }

    public static boolean isPaidApp(Context cont) {
        return cont.getPackageName().endsWith("paid");
    }

    public static boolean isDebug(Context cont) {
        return (cont.getApplicationInfo().flags & 2) != 0;
    }

    public static void ShowWebViewDlg(Context c, String title, String url) {
        Dialog dialog = new Dialog(c);
        dialog.setContentView((int) R.layout.web_view);
        dialog.setTitle(title);
        dialog.setCancelable(true);
        ((WebView) dialog.findViewById(R.id.webView1)).loadUrl(url);
        dialog.show();
    }

    public static void goToMarket(Context c) {
        Intent goToMarket = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + c.getPackageName() + "paid"));
        if (c.getPackageManager().queryIntentActivities(goToMarket, 65536).size() <= 0 || goToMarket == null) {
            Toast.makeText(c.getApplicationContext(), c.getString(R.string.error_no_market), 0).show();
        } else {
            c.startActivity(goToMarket);
        }
    }

    public static boolean showPurchase(final Context c) {
        if (isPaidApp(c)) {
            return false;
        }
        AlertDialog alertDialog = new AlertDialog.Builder(c).create();
        if (alertDialog != null) {
            alertDialog.setCancelable(false);
            alertDialog.setTitle(c.getString(R.string.level_locked));
            alertDialog.setMessage(c.getString(R.string.please_upgrade));
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    GATracker.addEvent("Menu", "main: selected go to market");
                    MyHelpers.goToMarket(c);
                }
            });
            alertDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    GATracker.addEvent("Menu", "main: cancel go to market");
                }
            });
            alertDialog.show();
        }
        return true;
    }

    public static int findInArray(int[] a, int id) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == id) {
                return i;
            }
        }
        return -1;
    }

    public static int findInArray(String[] a, String text) {
        for (int i = 0; i < a.length; i++) {
            if (a[i].equals(text)) {
                return i;
            }
        }
        return -1;
    }

    public class ElementDimentions {
        public int button_ht;
        public int button_wt;
        public int screen_ht;
        public int screen_wt;
        public int text_sz;

        public ElementDimentions() {
        }
    }

    public class MatchemItemInfo {
        public static final int STATE_COVERED = 0;
        public static final int STATE_SOLVED = 2;
        public static final int STATE_UNCOVERED = 1;
        public int itemIndex = -1;
        public int state = 0;

        public MatchemItemInfo(int index) {
            this.itemIndex = index;
        }
    }
}
