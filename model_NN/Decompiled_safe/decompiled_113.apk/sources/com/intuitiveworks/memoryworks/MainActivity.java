package com.intuitiveworks.memoryworks;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.intuitiveworks.common.AdMobTracker;
import com.intuitiveworks.common.GATracker;
import com.intuitiveworks.memoryworks.MyHelpers;
import com.intuitiveworks.memoryworks.kids.R;

public class MainActivity extends MWBaseActivity {
    private int[] m_aButtons = {R.id.button_digits, R.id.button_colors, R.id.button_pictures, R.id.button_sounds};
    private int[] m_aButtonsRelative = {R.id.button_about, R.id.button_results, R.id.button_feedback};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        MyHelpers.ElementDimentions dm = new MyHelpers().GetDimentions(this);
        for (int item : this.m_aButtons) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dm.screen_wt / 4, dm.screen_wt / 4);
            layoutParams.setMargins(5, 0, 5, 0);
            ((Button) findViewById(item)).setLayoutParams(layoutParams);
        }
        for (int item2 : this.m_aButtonsRelative) {
            Button vButton = (Button) findViewById(item2);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(dm.screen_wt / 4, dm.screen_wt / 4);
            layoutParams2.setMargins(5, 5, 5, 5);
            String packageName = getPackageName();
            if (packageName.endsWith("kids") || packageName.endsWith("kidspaid")) {
                findViewById(R.id.button_feedback).setVisibility(8);
                if (item2 == R.id.button_results) {
                    layoutParams2.addRule(0, R.id.centered_placeholder);
                } else {
                    layoutParams2.addRule(1, R.id.centered_placeholder);
                }
            } else {
                findViewById(R.id.centered_placeholder).setVisibility(8);
                if (item2 == R.id.button_feedback) {
                    layoutParams2.addRule(14);
                } else if (item2 == R.id.button_results) {
                    layoutParams2.addRule(0, R.id.button_feedback);
                } else if (item2 == R.id.button_about) {
                    layoutParams2.addRule(1, R.id.button_feedback);
                }
            }
            vButton.setLayoutParams(layoutParams2);
        }
        GATracker.start(this);
        new AdMobTracker().init(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        GATracker.stop();
    }

    public void OnButtonClicked(View v) {
        switch (v.getId()) {
            case R.id.button_digits /*2130968614*/:
                GATracker.addEvent("Button", "Main: game digits");
                startActivity(new Intent(this, DigitsActivity.class));
                return;
            case R.id.button_colors /*2130968615*/:
                GATracker.addEvent("Button", "Main: game colors");
                startActivity(new Intent(this, ColorsActivity.class));
                return;
            case R.id.button_sounds /*2130968616*/:
                GATracker.addEvent("Button", "Main: game sounds");
                startActivity(new Intent(this, SoundsActivity.class));
                return;
            case R.id.button_pictures /*2130968617*/:
                GATracker.addEvent("Button", "Main: game picture");
                startActivity(new Intent(this, PicturesActivity.class));
                return;
            case R.id.relativeLayout1 /*2130968618*/:
            case R.id.centered_placeholder /*2130968619*/:
            default:
                return;
            case R.id.button_results /*2130968620*/:
                GATracker.addEvent("Button", "Main: show results");
                startActivity(new Intent(this, StatsActivity.class));
                return;
            case R.id.button_feedback /*2130968621*/:
                GATracker.addEvent("Button", "Main: send feedback");
                Intent i = new Intent("android.intent.action.SEND");
                i.setType("text/plain");
                i.putExtra("android.intent.extra.EMAIL", new String[]{"info@intuitiveworks.biz"});
                i.putExtra("android.intent.extra.SUBJECT", "Feedback about Memory Works");
                i.putExtra("android.intent.extra.TEXT", "Please send us your feedback and ideas");
                startActivity(i);
                return;
            case R.id.button_about /*2130968622*/:
                GATracker.addEvent("Button", "Main: show about");
                MyHelpers.ShowWebViewDlg(this, getString(R.string.title_about), getString(R.string.html_about));
                return;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                GATracker.addEvent("Button", "Main: show about");
                MyHelpers.ShowWebViewDlg(this, getString(R.string.title_about), getString(R.string.html_about));
                return true;
            default:
                return false;
        }
    }

    public void resetState(int iLevel, boolean bInitialize) {
    }
}
