package com.intuitiveworks.memoryworks;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import com.intuitiveworks.common.Android4PlusFeatures;
import com.intuitiveworks.common.GATracker;
import com.intuitiveworks.memoryworks.kids.R;

public class NextLevelAlertDialog extends Dialog {
    MWBaseActivity m_Context;
    boolean m_bShowPurchase = false;
    int m_iLevel = 0;
    String m_strArea;

    public NextLevelAlertDialog(MWBaseActivity context, String area) {
        super(context);
        this.m_Context = context;
        this.m_strArea = area;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Button) findViewById(R.id.button_ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NextLevelAlertDialog.this.doOKAction();
            }
        });
        ((Button) findViewById(R.id.button_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NextLevelAlertDialog.this.doCancelAction();
            }
        });
        ((CheckBox) findViewById(R.id.remember_choice)).setChecked(this.m_Context.getSharedPreferences(this.m_Context.getString(R.string.pref_file), 0).getBoolean(String.valueOf(this.m_strArea) + this.m_Context.getString(R.string.pref_rememeber_choice), true));
    }

    /* access modifiers changed from: private */
    public void doOKAction() {
        saveChoice(true);
        dismiss();
        if (!this.m_bShowPurchase || !MyHelpers.showPurchase(this.m_Context)) {
            this.m_Context.resetState(this.m_iLevel + 1, true);
            if (Build.VERSION.SDK_INT >= 11) {
                new Android4PlusFeatures().InvalidateOptionsMenu(this.m_Context);
                return;
            }
            return;
        }
        GATracker.addEvent("Menu", String.valueOf(this.m_strArea) + ": purchase message - automatic");
    }

    private void saveChoice(boolean bOK) {
        SharedPreferences.Editor editor = this.m_Context.getSharedPreferences(this.m_Context.getString(R.string.pref_file), 0).edit();
        boolean bChecked = ((CheckBox) findViewById(R.id.remember_choice)).isChecked();
        editor.putBoolean(String.valueOf(this.m_strArea) + this.m_Context.getString(R.string.pref_rememeber_choice), bChecked);
        if (bChecked) {
            editor.putBoolean(String.valueOf(this.m_strArea) + this.m_Context.getString(R.string.pref_rememebered_choice), bOK);
        }
        editor.commit();
    }

    /* access modifiers changed from: private */
    public void doCancelAction() {
        saveChoice(false);
        dismiss();
        this.m_Context.resetState(this.m_iLevel, this.m_strArea != "Numbers");
    }

    public void setShowPurchase(boolean bShowPurchase) {
        this.m_bShowPurchase = bShowPurchase;
    }

    public void setLevel(int iLevel) {
        this.m_iLevel = iLevel;
    }

    public int getRememberedAnswer() {
        SharedPreferences preferences = this.m_Context.getSharedPreferences(this.m_Context.getString(R.string.pref_file), 0);
        if (!preferences.getBoolean(String.valueOf(this.m_strArea) + this.m_Context.getString(R.string.pref_rememeber_choice), false)) {
            return -1;
        }
        return preferences.getBoolean(new StringBuilder(String.valueOf(this.m_strArea)).append(this.m_Context.getString(R.string.pref_rememebered_choice)).toString(), false) ? 1 : 0;
    }
}
