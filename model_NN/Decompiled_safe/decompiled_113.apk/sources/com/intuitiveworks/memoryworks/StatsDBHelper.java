package com.intuitiveworks.memoryworks;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StatsDBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "memory_works";
    private static final int DB_VERSION = 3;
    Context m_Context;

    public StatsDBHelper(Context context) {
        super(context, DB_NAME, (SQLiteDatabase.CursorFactory) null, 3);
        this.m_Context = context;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS statistics (_id INTEGER PRIMARY KEY AUTOINCREMENT, level VARCHAR, date TEXT, errors VARCHAR)");
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Cursor cur = db.rawQuery("SELECT level, date, errors FROM statistics", new String[0]);
        cur.moveToFirst();
        ArrayList<String> temp = new ArrayList<>();
        while (!cur.isAfterLast()) {
            temp.add(cur.getString(0));
            temp.add(cur.getString(1));
            temp.add(cur.getString(2));
            cur.moveToNext();
        }
        cur.close();
        db.execSQL("DROP TABLE statistics");
        onCreate(db);
        for (int i = 0; i < temp.size(); i += 3) {
            db.execSQL("INSERT INTO statistics (level, date, errors) VALUES ('Numbers " + ((String) temp.get(i)) + "', '" + ((String) temp.get(i + 1)) + "', '" + ((String) temp.get(i + 2)) + "')");
        }
    }

    public void IntertValues(String game, String level, String errors) {
        SQLiteDatabase database = getWritableDatabase();
        if (database != null) {
            database.execSQL("INSERT INTO statistics (level, date, errors) VALUES ('" + game + " " + level + "', '" + new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss").format(new Date()) + "', '" + errors + "')");
        }
    }
}
