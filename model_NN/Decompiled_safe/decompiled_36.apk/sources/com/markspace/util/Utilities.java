package com.markspace.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

public class Utilities {
    private static final int CLEAR_ALPHA_MASK = 16777215;
    private static final float[] CORNERS = {0.0f, 0.0f, 5.0f, 5.0f, 5.0f, 5.0f, 0.0f, 0.0f};
    private static final int HIGH_ALPHA = -16777216;
    private static final int LOW_ALPHA = -1778384896;
    private static final int MED_ALPHA = -1275068416;

    public static Drawable getColorChip(int color) {
        int color2 = color & CLEAR_ALPHA_MASK;
        GradientDrawable d = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{color2 | HIGH_ALPHA, color2 | MED_ALPHA, color2 | LOW_ALPHA});
        d.setCornerRadii(CORNERS);
        return d;
    }

    public static boolean isIntentAvailable(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0;
    }
}
