package com.markspace.fliqnotes;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import com.markspace.provider.FliqNotes;
import com.markspace.util.Utilities;

public class CategoryListViewBinder implements SimpleAdapter.ViewBinder {
    private static String mCategoryRowUUID;
    private Context mContext;

    public CategoryListViewBinder(Context context) {
        this.mContext = context;
    }

    public boolean setViewValue(View view, Object data, String textRep) {
        switch (view.getId()) {
            case R.id.category_chip /*2131296260*/:
                if (textRep != null && textRep.length() != 0) {
                    try {
                        view.setBackgroundDrawable(Utilities.getColorChip(Color.parseColor(textRep)));
                        break;
                    } catch (IllegalArgumentException e) {
                        view.setBackgroundDrawable(Utilities.getColorChip(-3355444));
                        break;
                    }
                } else {
                    view.setBackgroundDrawable(Utilities.getColorChip(-3355444));
                    break;
                }
            case R.id.category_locked /*2131296261*/:
                mCategoryRowUUID = textRep;
                if (!textRep.matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
                    ((ImageView) view).setVisibility(8);
                    break;
                } else {
                    ((ImageView) view).setVisibility(0);
                    break;
                }
            case R.id.category_name /*2131296262*/:
                ((CheckedTextView) view).setTextSize(Float.parseFloat(Prefs.getListTextSize(this.mContext)));
                ((CheckedTextView) view).setText(textRep);
                if (CategoryList.mCurrentCategoryUUID.matches(mCategoryRowUUID)) {
                    ((CheckedTextView) view).setChecked(true);
                } else {
                    ((CheckedTextView) view).setChecked(false);
                }
                if (!mCategoryRowUUID.matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
                    ((CheckedTextView) view).setTypeface(Typeface.DEFAULT, 0);
                    break;
                } else {
                    ((CheckedTextView) view).setTypeface(Typeface.DEFAULT, 2);
                    break;
                }
        }
        return true;
    }
}
