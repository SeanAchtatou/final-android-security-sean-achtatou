package com.markspace.fliqnotes;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import com.markspace.test.Config;

public class ReleaseNotes extends Activity {
    private static final String TAG = "ReleaseNotes";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Config.V) {
            Log.v(TAG, ".onCreate");
        }
        setContentView((int) R.layout.release_notes);
    }
}
