package com.markspace.fliqnotes;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.markspace.test.Config;

public class PasswordProtect extends Activity implements View.OnClickListener {
    public static final String ACTION_KEY = "passwordAction";
    private static final String PASSWORD_PREFERENCE_KEY = "password";
    public static final String PASSWORD_SET = "setPassword";
    public static final String PASSWORD_VALIDATE = "validatePassword";
    private static final String TAG = "PasswordProtect";
    private String mAction = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.password_protect);
        this.mAction = getIntent().getStringExtra(ACTION_KEY);
        if (Config.V) {
            Log.v(TAG, ".onCreate action=" + this.mAction);
        }
        TextView title = (TextView) findViewById(R.id.password_title);
        TextView label = (TextView) findViewById(R.id.password_label);
        if (title == null || label == null) {
            Log.w(TAG, "view issue cannot load title and label from layout");
            setResult(0);
            finish();
        }
        if (this.mAction.equals(PASSWORD_SET)) {
            title.setText((int) R.string.password_title_set);
            label.setText((int) R.string.password_set);
        } else if (this.mAction.equals(PASSWORD_VALIDATE)) {
            title.setText((int) R.string.password_title_validate);
            label.setText((int) R.string.password_validate);
            findViewById(R.id.password_confirm).setVisibility(8);
            findViewById(R.id.password_confirm_label).setVisibility(8);
        } else {
            Log.w(TAG, "Invalid action set for this activity, activity canceled");
            setResult(0);
            finish();
        }
        ((TextView) findViewById(R.id.title_left_text)).setText((int) R.string.title_notes_list);
        findViewById(R.id.password_ok).setOnClickListener(this);
        findViewById(R.id.password_cancel).setOnClickListener(this);
        FliqNotesApp.increaseLockSkipCount(this);
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void onClick(View v) {
        if (Config.V) {
            Log.v(TAG, ".onClick");
        }
        if (v.getId() == R.id.password_ok) {
            EditText pw = (EditText) findViewById(R.id.password);
            if (this.mAction.equals(PASSWORD_SET)) {
                EditText pwc = (EditText) findViewById(R.id.password_confirm);
                if (!(pw == null || pwc == null)) {
                    String password = pw.getText().toString();
                    String confirm = pwc.getText().toString();
                    if (Config.V) {
                        Log.v(TAG, ".onClick SET: " + password + "==" + confirm);
                    }
                    if (confirm.equals(password)) {
                        setResult(-1);
                        setPassword(pw.getText().toString());
                        finish();
                    } else {
                        if (Config.V) {
                            Log.v(TAG, ".onClick passwords do not match");
                        }
                        ((TextView) findViewById(R.id.password_message)).setText((int) R.string.password_no_match);
                        return;
                    }
                }
            } else if (pw != null) {
                if (pw.getText().toString().equals(getPassword())) {
                    setResult(-1);
                    finish();
                } else {
                    Log.w(TAG, "invalid password entered");
                    ((TextView) findViewById(R.id.password_message)).setText((int) R.string.password_invalid);
                    return;
                }
            }
        }
        setResult(0);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (Config.V) {
            Log.v(TAG, ".onPause");
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (Config.V) {
            Log.v(TAG, ".onResume");
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (Config.V) {
            Log.v(TAG, ".onDestroy");
        }
        FliqNotesApp.decreaseLockSkipCount();
    }

    public String getPassword() {
        return PreferenceManager.getDefaultSharedPreferences(this).getString(PASSWORD_PREFERENCE_KEY, "");
    }

    public void setPassword(String password) {
        if (Config.D) {
            Log.d(TAG, ".setPassword: " + password);
        }
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(PASSWORD_PREFERENCE_KEY, password);
        editor.commit();
    }
}
