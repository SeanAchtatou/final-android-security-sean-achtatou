package com.markspace.fliqnotes;

public final class R {

    public static final class array {
        public static final int dialog_sort_options = 2131034116;
        public static final int empty_list = 2131034112;
        public static final int settings_textsize_list = 2131034113;
        public static final int settings_typeface_list_names = 2131034114;
        public static final int settings_typeface_list_values = 2131034115;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int appointment_indicator_leftside_1 = 2130837504;
        public static final int appointment_indicator_leftside_2 = 2130837505;
        public static final int appointment_indicator_leftside_3 = 2130837506;
        public static final int bg_arrow_focus = 2130837507;
        public static final int bg_arrow_press = 2130837508;
        public static final int bg_arrow_unselected = 2130837509;
        public static final int btn_arrow_background = 2130837510;
        public static final int btn_arrow_left = 2130837511;
        public static final int btn_arrow_right = 2130837512;
        public static final int clickable = 2130837513;
        public static final int gradient = 2130837514;
        public static final int graph = 2130837515;
        public static final int graph_landscape = 2130837516;
        public static final int header_background = 2130837517;
        public static final int ic_btn_arrow_left_focus = 2130837518;
        public static final int ic_btn_arrow_left_unselected = 2130837519;
        public static final int ic_btn_arrow_right_focus = 2130837520;
        public static final int ic_btn_arrow_right_unselected = 2130837521;
        public static final int ic_menu_allfriends = 2130837522;
        public static final int ic_menu_category = 2130837523;
        public static final int ic_menu_refresh = 2130837524;
        public static final int icon = 2130837525;
        public static final int search_icon = 2130837526;
    }

    public static final class id {
        public static final int about_text = 2131296257;
        public static final int about_title = 2131296256;
        public static final int add_category = 2131296258;
        public static final int add_note = 2131296281;
        public static final int background = 2131296266;
        public static final int category_chip = 2131296260;
        public static final int category_locked = 2131296261;
        public static final int category_name = 2131296262;
        public static final int color_chip = 2131296264;
        public static final int color_list_title = 2131296263;
        public static final int color_name = 2131296265;
        public static final int colorcircle = 2131296268;
        public static final int details = 2131296280;
        public static final int menu_about = 2131296315;
        public static final int menu_add_note = 2131296307;
        public static final int menu_delete_note = 2131296310;
        public static final int menu_edit_note = 2131296308;
        public static final int menu_edit_title = 2131296309;
        public static final int menu_other_apps = 2131296314;
        public static final int menu_preferences = 2131296313;
        public static final int menu_sort = 2131296311;
        public static final int menu_sync = 2131296312;
        public static final int noMissingSync_text1 = 2131296271;
        public static final int noMissingSync_text2 = 2131296272;
        public static final int noMissingSync_title = 2131296270;
        public static final int note = 2131296276;
        public static final int noteEditCategory = 2131296274;
        public static final int noteEditChip = 2131296273;
        public static final int noteEditModDate = 2131296275;
        public static final int notelist_category_chip = 2131296283;
        public static final int notelist_date = 2131296285;
        public static final int notelist_title = 2131296284;
        public static final int notes = 2131296279;
        public static final int other_apps_text_1 = 2131296287;
        public static final int other_apps_text_2 = 2131296288;
        public static final int other_apps_title = 2131296286;
        public static final int password = 2131296291;
        public static final int password_cancel = 2131296296;
        public static final int password_confirm = 2131296293;
        public static final int password_confirm_label = 2131296292;
        public static final int password_label = 2131296290;
        public static final int password_message = 2131296294;
        public static final int password_ok = 2131296295;
        public static final int password_title = 2131296289;
        public static final int rating_reminder_later = 2131296300;
        public static final int rating_reminder_no = 2131296301;
        public static final int rating_reminder_ok = 2131296299;
        public static final int rating_reminder_text = 2131296298;
        public static final int rating_reminder_title = 2131296297;
        public static final int release_notes_text = 2131296302;
        public static final int saturation = 2131296267;
        public static final int showNextNote = 2131296278;
        public static final int showPrevNote = 2131296277;
        public static final int title_add_category = 2131296259;
        public static final int title_add_note = 2131296282;
        public static final int title_left_text = 2131296303;
        public static final int title_right_text = 2131296304;
        public static final int value = 2131296269;
        public static final int view_release_notes = 2131296306;
        public static final int whats_new_ok = 2131296305;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int category_header = 2130903041;
        public static final int category_item = 2130903042;
        public static final int category_list = 2130903043;
        public static final int color_list = 2130903044;
        public static final int color_list_item = 2130903045;
        public static final int color_wheel = 2130903046;
        public static final int no_missing_sync = 2130903047;
        public static final int note_editor = 2130903048;
        public static final int notes = 2130903049;
        public static final int noteslist = 2130903050;
        public static final int noteslist_header = 2130903051;
        public static final int noteslist_item = 2130903052;
        public static final int other_apps = 2130903053;
        public static final int password_protect = 2130903054;
        public static final int rating_reminder = 2130903055;
        public static final int release_notes = 2130903056;
        public static final int title = 2130903057;
        public static final int whats_new = 2130903058;
    }

    public static final class menu {
        public static final int menu = 2131230720;
    }

    public static final class string {
        public static final int about_text_1 = 2131099735;
        public static final int about_text_2 = 2131099736;
        public static final int about_title = 2131099734;
        public static final int add_category_title = 2131099673;
        public static final int app_name = 2131099691;
        public static final int build_version_title = 2131099733;
        public static final int categories_already_exist = 2131099689;
        public static final int categories_unfiled_no_delete = 2131099687;
        public static final int categories_unfiled_no_edit = 2131099688;
        public static final int dialog_delete = 2131099670;
        public static final int dialog_sort = 2131099694;
        public static final int edit_category_title = 2131099674;
        public static final int edit_title_title = 2131099675;
        public static final int error_message = 2131099693;
        public static final int error_title = 2131099692;
        public static final int fast_scroll_alphabet = 2131099731;
        public static final int fast_scroll_numeric_alphabet = 2131099732;
        public static final int fliq_not_available = 2131099690;
        public static final int fliq_note = 2131099744;
        public static final int menu_about = 2131099668;
        public static final int menu_add = 2131099648;
        public static final int menu_add_note = 2131099651;
        public static final int menu_category = 2131099669;
        public static final int menu_delete = 2131099650;
        public static final int menu_delete_note = 2131099658;
        public static final int menu_discard = 2131099653;
        public static final int menu_edit = 2131099649;
        public static final int menu_edit_color = 2131099662;
        public static final int menu_edit_name = 2131099661;
        public static final int menu_edit_note = 2131099659;
        public static final int menu_edit_title = 2131099660;
        public static final int menu_email = 2131099656;
        public static final int menu_fliq = 2131099654;
        public static final int menu_other_apps = 2131099667;
        public static final int menu_preferences = 2131099664;
        public static final int menu_revert = 2131099652;
        public static final int menu_search = 2131099665;
        public static final int menu_send_note = 2131099657;
        public static final int menu_sort = 2131099666;
        public static final int menu_sync = 2131099655;
        public static final int noMissingSync_text1 = 2131099725;
        public static final int noMissingSync_text2 = 2131099726;
        public static final int noMissingSync_title = 2131099724;
        public static final int no_notes = 2131099728;
        public static final int note_title_untitled = 2131099686;
        public static final int notes = 2131099729;
        public static final int other_apps = 2131099738;
        public static final int other_apps_summary = 2131099739;
        public static final int other_apps_text_1 = 2131099751;
        public static final int other_apps_text_2 = 2131099752;
        public static final int other_apps_title = 2131099750;
        public static final int password_confirm = 2131099698;
        public static final int password_invalid = 2131099700;
        public static final int password_no_match = 2131099701;
        public static final int password_set = 2131099697;
        public static final int password_title_set = 2131099695;
        public static final int password_title_validate = 2131099696;
        public static final int password_validate = 2131099699;
        public static final int rating_reminder_later = 2131099756;
        public static final int rating_reminder_no = 2131099757;
        public static final int rating_reminder_ok = 2131099755;
        public static final int rating_reminder_text = 2131099754;
        public static final int rating_reminder_title = 2131099753;
        public static final int release_notes = 2131099740;
        public static final int release_notes_summary = 2131099741;
        public static final int release_notes_text = 2131099758;
        public static final int release_notes_title = 2131099737;
        public static final int resolve_edit = 2131099671;
        public static final int resolve_select_category_title = 2131099672;
        public static final int search_hint = 2131099685;
        public static final int search_settings_description = 2131099742;
        public static final int select_color = 2131099663;
        public static final int send_note = 2131099743;
        public static final int settings_about_title = 2131099723;
        public static final int settings_application_title = 2131099721;
        public static final int settings_default_cat_summary = 2131099718;
        public static final int settings_default_cat_title = 2131099717;
        public static final int settings_display_title = 2131099722;
        public static final int settings_fliq_availability_summary_off = 2131099713;
        public static final int settings_fliq_availability_summary_on = 2131099712;
        public static final int settings_fliq_availability_title = 2131099711;
        public static final int settings_list_textsize_summary = 2131099708;
        public static final int settings_list_textsize_title = 2131099707;
        public static final int settings_list_typeface_summary = 2131099710;
        public static final int settings_list_typeface_title = 2131099709;
        public static final int settings_note_count_summary = 2131099720;
        public static final int settings_note_count_title = 2131099719;
        public static final int settings_password_summary_off = 2131099716;
        public static final int settings_password_summary_on = 2131099715;
        public static final int settings_password_title = 2131099714;
        public static final int settings_textsize_summary = 2131099704;
        public static final int settings_textsize_title = 2131099703;
        public static final int settings_title = 2131099702;
        public static final int settings_typeface_summary = 2131099706;
        public static final int settings_typeface_title = 2131099705;
        public static final int title_add_category = 2131099683;
        public static final int title_add_note = 2131099684;
        public static final int title_categories = 2131099678;
        public static final int title_create = 2131099676;
        public static final int title_edit = 2131099677;
        public static final int title_note = 2131099681;
        public static final int title_notes_list = 2131099679;
        public static final int title_search_notes = 2131099682;
        public static final int title_search_results = 2131099680;
        public static final int unfiled = 2131099730;
        public static final int view_release_notes = 2131099746;
        public static final int whats_new_item_1 = 2131099747;
        public static final int whats_new_item_2 = 2131099748;
        public static final int whats_new_item_3 = 2131099749;
        public static final int whats_new_title = 2131099745;
        public static final int yesterday = 2131099727;
    }

    public static final class style {
        public static final int FliqTheme = 2131165184;
    }

    public static final class xml {
        public static final int searchable = 2130968576;
        public static final int settings = 2130968577;
    }
}
