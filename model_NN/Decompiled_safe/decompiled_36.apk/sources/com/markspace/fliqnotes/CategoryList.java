package com.markspace.fliqnotes;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.markspace.provider.FliqNotes;
import com.markspace.test.Config;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class CategoryList extends ListActivity {
    private static final int ADD_ID = 1;
    private static final int CATEGORY_COLOR_INDEX = 3;
    private static final int CATEGORY_NAME_INDEX = 1;
    private static final String[] CATEGORY_PROJECTION;
    private static final int CATEGORY_UUID_INDEX = 2;
    private static final int DELETE_ID = 2;
    private static final int EDIT_COLOR_ID = 4;
    private static final int EDIT_NAME_ID = 3;
    private static final int PICK_COLOR_REQUEST = 1;
    private static final String TAG = "CategoryList";
    protected static String mCurrentCategoryUUID;
    private SimpleAdapter mAdapter;
    /* access modifiers changed from: private */
    public int mCurrentSelection;
    private String mEditCategoryUuid;
    private TextView mLeftTitle;
    private ArrayList<HashMap<String, String>> mList = new ArrayList<>();
    /* access modifiers changed from: private */
    public ListView mListView;

    static {
        String[] strArr = new String[EDIT_COLOR_ID];
        strArr[0] = "_id";
        strArr[1] = "category";
        strArr[2] = "uuid";
        strArr[3] = FliqNotes.Categories.COLOR;
        CATEGORY_PROJECTION = strArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Config.V) {
            Log.v(TAG, ".onCreate: " + getIntent().toString());
        }
        requestWindowFeature(1);
        setContentView((int) R.layout.category_list);
        mCurrentCategoryUUID = getIntent().getStringExtra("uuid");
        if (Config.D) {
            Log.d(TAG, ".onCreate mCurrentCategoryUUID=" + mCurrentCategoryUUID);
        }
        this.mAdapter = new SimpleAdapter(this, this.mList, R.layout.category_item, new String[]{FliqNotes.Categories.COLOR, "uuid", "category"}, new int[]{R.id.category_chip, R.id.category_locked, R.id.category_name});
        this.mAdapter.setViewBinder(new CategoryListViewBinder(this));
        setListAdapter(this.mAdapter);
        LinearLayout addCategory = (LinearLayout) findViewById(R.id.add_category);
        if (addCategory != null) {
            addCategory.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CategoryList.this.addCategory();
                }
            });
            addCategory.setFocusable(true);
            addCategory.setFocusableInTouchMode(true);
        }
        this.mListView = getListView();
        this.mListView.setItemsCanFocus(false);
        this.mListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                CategoryList.this.mCurrentSelection = CategoryList.this.mListView.getSelectedItemPosition();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        registerForContextMenu(this.mListView);
        this.mLeftTitle = (TextView) findViewById(R.id.title_left_text);
        this.mLeftTitle.setText((int) R.string.title_categories);
    }

    /* access modifiers changed from: private */
    public void refreshList() {
        if (Config.V) {
            Log.v(TAG, ".refreshList");
        }
        this.mList.clear();
        HashMap<String, String> item = new HashMap<>();
        item.put(FliqNotes.Categories.COLOR, Prefs.getUnfiledColor(this));
        item.put("uuid", FliqNotes.Categories.UNFILED_CATEGORY_UUID);
        item.put("category", getResources().getText(R.string.unfiled).toString());
        this.mList.add(item);
        ((TextView) findViewById(R.id.title_add_category)).setTextSize(Float.parseFloat(Prefs.getListTextSize(this)));
        Cursor c = managedQuery(FliqNotes.Categories.CONTENT_URI, CATEGORY_PROJECTION, null, null, "category COLLATE NOCASE");
        if (c == null || !c.moveToFirst()) {
            this.mAdapter.notifyDataSetChanged();
            this.mListView.setSelection(0);
        }
        do {
            HashMap<String, String> item2 = new HashMap<>();
            item2.put(FliqNotes.Categories.COLOR, c.getString(3));
            item2.put("uuid", c.getString(2));
            item2.put("category", c.getString(1));
            this.mList.add(item2);
        } while (c.moveToNext());
        this.mAdapter.notifyDataSetChanged();
        this.mListView.setSelection(0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (Config.V) {
            Log.v(TAG, ".onResume");
        }
        refreshList();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (Config.V) {
            Log.v(TAG, ".onListItemClick position=" + position + " id=" + id);
        }
        String newCategoryUUID = (String) this.mList.get(position).get("uuid");
        if (Config.D) {
            Log.d(TAG, ".onListItemClick uuid=" + newCategoryUUID + " id=" + id);
        }
        setResult(-1, new Intent().putExtra("uuid", newCategoryUUID));
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (Config.V) {
            Log.v(TAG, ".onCreateOptionsMenu");
        }
        menu.add(0, 1, 0, (int) R.string.menu_add).setShortcut('0', 'a').setIcon(17301555);
        menu.add(0, 3, 0, (int) R.string.menu_edit_name).setShortcut('1', 'e').setIcon(17301566);
        menu.add(0, (int) EDIT_COLOR_ID, 0, (int) R.string.menu_edit_color).setShortcut('2', 'e').setIcon(17301566);
        menu.add(0, 2, 0, (int) R.string.menu_delete).setShortcut('2', 'd').setIcon(17301564);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        this.mCurrentSelection = getSelectedItemPosition();
        if (Config.V) {
            Log.v(TAG, ".onPrepareOptionsMenu position=" + this.mCurrentSelection);
        }
        menu.removeItem(EDIT_COLOR_ID);
        menu.removeItem(3);
        menu.removeItem(2);
        if (this.mCurrentSelection > 0) {
            menu.add(0, 3, 0, (int) R.string.menu_edit_name).setShortcut('1', 'e').setIcon(17301566);
            menu.add(0, (int) EDIT_COLOR_ID, 0, (int) R.string.menu_edit_color).setShortcut('2', 'e').setIcon(17301566);
            menu.add(0, 2, 0, (int) R.string.menu_delete).setShortcut('2', 'd').setIcon(17301564);
            return true;
        } else if (this.mCurrentSelection != 0) {
            return true;
        } else {
            menu.add(0, (int) EDIT_COLOR_ID, 0, (int) R.string.menu_edit_color).setShortcut('2', 'e').setIcon(17301566);
            return true;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (Config.V) {
            Log.v(TAG, ".onOptionsItemSelected: " + this.mCurrentSelection);
        }
        switch (item.getItemId()) {
            case 1:
                addCategory();
                break;
            case 2:
                deleteCategory();
                break;
            case 3:
                editCategory();
                break;
            case EDIT_COLOR_ID /*4*/:
                editColor();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        if (Config.V) {
            Log.v(TAG, ".onCreateContextMenu");
        }
        try {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            if (Config.V) {
                Log.v(TAG, ".onCreateContextMenu: position=" + info.position);
            }
            this.mCurrentSelection = info.position;
            menu.setHeaderTitle((CharSequence) this.mList.get(info.position).get("category"));
            if (info.position == 0) {
                menu.add(0, (int) EDIT_COLOR_ID, 0, (int) R.string.menu_edit_color);
                return;
            }
            menu.add(0, 3, 0, (int) R.string.menu_edit_name);
            menu.add(0, (int) EDIT_COLOR_ID, 0, (int) R.string.menu_edit_color);
            menu.add(0, 2, 0, (int) R.string.menu_delete);
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (Config.V) {
            Log.v(TAG, ".onContextItemSelected: position=" + item.getItemId());
        }
        switch (item.getItemId()) {
            case 2:
                deleteCategory();
                return true;
            case 3:
                editCategory();
                return true;
            case EDIT_COLOR_ID /*4*/:
                editColor();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: private */
    public void addCategory() {
        if (Config.V) {
            Log.v(TAG, ".addCategory");
        }
        FrameLayout fl = new FrameLayout(this);
        final EditText input = new EditText(this);
        input.setGravity(3);
        input.setInputType(8192);
        fl.addView(input, new FrameLayout.LayoutParams(-1, -2));
        input.setText("");
        new AlertDialog.Builder(this).setView(fl).setTitle((int) R.string.add_category_title).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
            public void onClick(DialogInterface d, int which) {
                ((InputMethodManager) CategoryList.this.getSystemService("input_method")).hideSoftInputFromWindow(input.getApplicationWindowToken(), 0);
                d.dismiss();
                String categoryName = input.getText().toString();
                if (categoryName.length() > 0) {
                    Cursor c = CategoryList.this.getContentResolver().query(FliqNotes.Categories.CONTENT_URI, null, "category=?", new String[]{categoryName}, null);
                    if (c.moveToFirst() || categoryName.matches(CategoryList.this.getResources().getString(R.string.unfiled))) {
                        Log.e(CategoryList.TAG, "Category already exists: " + categoryName);
                        new AlertDialog.Builder(this).setMessage((int) R.string.categories_already_exist).create().show();
                        return;
                    }
                    ContentValues values = new ContentValues();
                    values.put("uuid", UUID.randomUUID().toString());
                    values.put("category", categoryName);
                    values.put(FliqNotes.Categories.COLOR, "whiteColor");
                    values.put(FliqNotes.Categories.USERORDER, (Integer) 0);
                    Uri category = CategoryList.this.getContentResolver().insert(FliqNotes.Categories.CONTENT_URI, values);
                    if (category != null) {
                        values.clear();
                        values.put(FliqNotes.Categories.COLOR, FliqNotes.Categories.getDefaultCategoryColor(Integer.parseInt(category.getLastPathSegment())));
                        CategoryList.this.getContentResolver().update(category, values, null, null);
                    }
                    c.close();
                }
                CategoryList.this.refreshList();
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int which) {
                ((InputMethodManager) CategoryList.this.getSystemService("input_method")).hideSoftInputFromWindow(input.getApplicationWindowToken(), 0);
                d.dismiss();
            }
        }).create().show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x00e7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void deleteCategory() {
        /*
            r12 = this;
            r11 = 1
            r10 = 0
            java.util.ArrayList<java.util.HashMap<java.lang.String, java.lang.String>> r0 = r12.mList
            int r1 = r12.mCurrentSelection
            java.lang.Object r0 = r0.get(r1)
            java.util.HashMap r0 = (java.util.HashMap) r0
            java.lang.String r1 = "uuid"
            java.lang.Object r7 = r0.get(r1)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r0 = "CategoryList"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = ".deleteCategory - deleting category by active: "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r0, r1)
            java.lang.String r0 = "0"
            boolean r0 = r7.matches(r0)
            if (r0 == 0) goto L_0x0044
            android.app.AlertDialog$Builder r0 = new android.app.AlertDialog$Builder
            r0.<init>(r12)
            r1 = 2131099687(0x7f060027, float:1.7811734E38)
            android.app.AlertDialog$Builder r0 = r0.setMessage(r1)
            android.app.AlertDialog r0 = r0.create()
            r0.show()
        L_0x0043:
            return
        L_0x0044:
            android.content.ContentValues r9 = new android.content.ContentValues
            r9.<init>()
            android.content.ContentResolver r0 = r12.getContentResolver()
            android.net.Uri r1 = com.markspace.provider.FliqNotes.Notes.CONTENT_URI
            java.lang.String[] r2 = new java.lang.String[r11]
            java.lang.String r3 = "uuid"
            r2[r10] = r3
            java.lang.String r3 = "category=?"
            java.lang.String[] r4 = new java.lang.String[r11]
            r4[r10] = r7
            r5 = 0
            android.database.Cursor r6 = r0.query(r1, r2, r3, r4, r5)
            if (r6 == 0) goto L_0x00a7
            boolean r0 = r6.moveToFirst()
            if (r0 == 0) goto L_0x00a7
        L_0x0068:
            r9.clear()
            java.lang.String r0 = "uuid"
            java.lang.String r1 = "uuid"
            int r1 = r6.getColumnIndex(r1)
            java.lang.String r1 = r6.getString(r1)
            r9.put(r0, r1)
            java.lang.String r0 = "CategoryList"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = ".deleteCategory adding note to modifies: "
            r1.<init>(r2)
            java.lang.String r2 = "uuid"
            int r2 = r6.getColumnIndex(r2)
            java.lang.String r2 = r6.getString(r2)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r0, r1)
            android.content.ContentResolver r0 = r12.getContentResolver()
            android.net.Uri r1 = com.markspace.provider.FliqNotes.Modifies.CONTENT_URI
            r0.insert(r1, r9)
            boolean r0 = r6.moveToNext()
            if (r0 != 0) goto L_0x0068
        L_0x00a7:
            r6.close()
            long r0 = java.lang.System.currentTimeMillis()
            java.lang.String r8 = com.markspace.provider.FliqNotes.convertTimeMillisecondsToProtocolString(r0)
            r9.clear()
            java.lang.String r0 = "modificationDate"
            r9.put(r0, r8)
            java.lang.String r0 = "category"
            java.lang.String r1 = "0"
            r9.put(r0, r1)
            android.content.ContentResolver r0 = r12.getContentResolver()
            android.net.Uri r1 = com.markspace.provider.FliqNotes.Notes.CONTENT_URI
            java.lang.String r2 = "category=?"
            java.lang.String[] r3 = new java.lang.String[r11]
            r3[r10] = r7
            r0.update(r1, r9, r2, r3)
            android.content.ContentResolver r0 = r12.getContentResolver()
            android.net.Uri r1 = com.markspace.provider.FliqNotes.Categories.CONTENT_URI
            java.lang.String r2 = "uuid=?"
            java.lang.String[] r3 = new java.lang.String[r11]
            r3[r10] = r7
            r0.delete(r1, r2, r3)
            java.lang.String r0 = com.markspace.fliqnotes.CategoryList.mCurrentCategoryUUID
            boolean r0 = r0.matches(r7)
            if (r0 == 0) goto L_0x00eb
            java.lang.String r0 = "0"
            com.markspace.fliqnotes.CategoryList.mCurrentCategoryUUID = r0
        L_0x00eb:
            r12.refreshList()
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.markspace.fliqnotes.CategoryList.deleteCategory():void");
    }

    private void editCategory() {
        final String editCategoryUuid = (String) this.mList.get(this.mCurrentSelection).get("uuid");
        if (editCategoryUuid.matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
            new AlertDialog.Builder(this).setMessage((int) R.string.categories_unfiled_no_edit).create().show();
            return;
        }
        if (Config.V) {
            Log.v(TAG, ".editCategory - editing: " + editCategoryUuid);
        }
        FrameLayout fl = new FrameLayout(this);
        final EditText input = new EditText(this);
        input.setGravity(3);
        input.setInputType(8192);
        fl.addView(input, new FrameLayout.LayoutParams(-1, -2));
        input.setText((CharSequence) this.mList.get(this.mCurrentSelection).get("category"));
        new AlertDialog.Builder(this).setView(fl).setTitle((int) R.string.edit_category_title).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int which) {
                ((InputMethodManager) CategoryList.this.getSystemService("input_method")).hideSoftInputFromWindow(input.getApplicationWindowToken(), 0);
                d.dismiss();
                String categoryName = input.getText().toString();
                if (categoryName.length() > 0) {
                    ContentValues values = new ContentValues();
                    Cursor c = CategoryList.this.getContentResolver().query(FliqNotes.Notes.CONTENT_URI, new String[]{"uuid"}, "category=?", new String[]{editCategoryUuid}, null);
                    if (c == null || !c.moveToFirst()) {
                        c.close();
                        values.clear();
                        values.put("category", categoryName);
                        CategoryList.this.getContentResolver().update(FliqNotes.Categories.CONTENT_URI, values, "uuid=?", new String[]{editCategoryUuid});
                    } else {
                        do {
                            values.clear();
                            values.put("uuid", c.getString(c.getColumnIndex("uuid")));
                            CategoryList.this.getContentResolver().insert(FliqNotes.Modifies.CONTENT_URI, values);
                        } while (c.moveToNext());
                        c.close();
                        values.clear();
                        values.put("category", categoryName);
                        CategoryList.this.getContentResolver().update(FliqNotes.Categories.CONTENT_URI, values, "uuid=?", new String[]{editCategoryUuid});
                    }
                }
                CategoryList.this.refreshList();
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int which) {
                ((InputMethodManager) CategoryList.this.getSystemService("input_method")).hideSoftInputFromWindow(input.getApplicationWindowToken(), 0);
                d.dismiss();
            }
        }).create().show();
    }

    private void editColor() {
        int currentColor;
        if (Config.V) {
            Log.v(TAG, ".editColor");
        }
        this.mEditCategoryUuid = (String) this.mList.get(this.mCurrentSelection).get("uuid");
        try {
            if (this.mEditCategoryUuid.matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
                currentColor = Color.parseColor(Prefs.getUnfiledColor(this));
            } else {
                currentColor = Color.parseColor((String) this.mList.get(this.mCurrentSelection).get(FliqNotes.Categories.COLOR));
            }
        } catch (IllegalArgumentException e) {
            currentColor = -3355444;
        } catch (StringIndexOutOfBoundsException e2) {
            currentColor = -3355444;
        }
        startActivityForResult(new Intent(this, ColorListActivity.class).putExtra(FliqNotes.Categories.COLOR, currentColor), 1);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Config.V) {
            Log.v(TAG, ".onActivityResult resultCode=" + resultCode);
        }
        if (requestCode != 1) {
            return;
        }
        if (resultCode == -1) {
            String newColor = colorIntToString(data.getIntExtra(FliqNotes.Categories.COLOR, 0));
            if (Config.D) {
                Log.d(TAG, ".onActivityResult new color=" + newColor);
            }
            if (this.mEditCategoryUuid.matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
                Prefs.setUnfiledColor(this, newColor);
                return;
            }
            ContentValues values = new ContentValues();
            Cursor c = getContentResolver().query(FliqNotes.Notes.CONTENT_URI, new String[]{"uuid"}, "category=?", new String[]{this.mEditCategoryUuid}, null);
            if (c == null || !c.moveToFirst()) {
                c.close();
                values.clear();
                values.put(FliqNotes.Categories.COLOR, newColor);
                getContentResolver().update(FliqNotes.Categories.CONTENT_URI, values, "uuid=?", new String[]{this.mEditCategoryUuid});
                refreshList();
            }
            do {
                values.clear();
                values.put("uuid", c.getString(c.getColumnIndex("uuid")));
                getContentResolver().insert(FliqNotes.Modifies.CONTENT_URI, values);
            } while (c.moveToNext());
            c.close();
            values.clear();
            values.put(FliqNotes.Categories.COLOR, newColor);
            getContentResolver().update(FliqNotes.Categories.CONTENT_URI, values, "uuid=?", new String[]{this.mEditCategoryUuid});
            refreshList();
        } else if (resultCode == 1) {
            startActivityForResult(new Intent(this, ColorWheelActivity.class).putExtra(FliqNotes.Categories.COLOR, Color.parseColor((String) this.mList.get(this.mCurrentSelection).get(FliqNotes.Categories.COLOR))), 1);
        }
    }

    private String colorIntToString(int color) {
        Log.w(TAG, ".colorIntToString --> #" + String.format("%06x", Integer.valueOf(color & 16777215)));
        return "#" + String.format("%06x", Integer.valueOf(color & 16777215));
    }
}
