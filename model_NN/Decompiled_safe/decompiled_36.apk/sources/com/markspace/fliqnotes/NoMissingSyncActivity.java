package com.markspace.fliqnotes;

import android.app.Activity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.TextView;

public class NoMissingSyncActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.no_missing_sync);
        Linkify.addLinks((TextView) findViewById(R.id.noMissingSync_text2), 1);
        FliqNotesApp.increaseLockSkipCount(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        FliqNotesApp.decreaseLockSkipCount();
    }
}
