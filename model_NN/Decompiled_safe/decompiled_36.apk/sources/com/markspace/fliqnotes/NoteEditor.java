package com.markspace.fliqnotes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import com.markspace.provider.FliqNotes;
import com.markspace.providers.notes.FliqNotesProvider;
import com.markspace.test.Config;
import com.markspace.util.Utilities;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

public class NoteEditor extends Activity {
    private static final int DELETE_ID = 3;
    private static final int DISCARD_ID = 2;
    private static final int EDIT_TITLE_ID = 7;
    private static final int EMAIL_ID = 5;
    private static final int FLIQ_ID = 6;
    private static final String[] NOTE_PROJECTION;
    private static final int PICK_CATEGORY_REQUEST = 1;
    private static final int REVERT_ID = 1;
    private static final int SELECT_CATEGORY_ID = 4;
    private static final String TAG = "NoteEditor";
    private View mCategoryChip;
    private TextView mCategoryText;
    /* access modifiers changed from: private */
    public String mCategoryUuid = null;
    /* access modifiers changed from: private */
    public Context mContext;
    private Cursor mCursor;
    /* access modifiers changed from: private */
    public boolean mDirtyFlag = false;
    /* access modifiers changed from: private */
    public boolean mEmptyInsertPending = false;
    private String mIntentAction;
    private TextView mModDateText;
    private ImageButton mNextButton;
    private String mNoteUuid;
    private String mOriginalNoteText;
    private ImageButton mPrevButton;
    private EditText mText;
    private TextView mTitleText;
    /* access modifiers changed from: private */
    public Uri mUri;

    static {
        String[] strArr = new String[FLIQ_ID];
        strArr[0] = "_id";
        strArr[1] = "uuid";
        strArr[DISCARD_ID] = FliqNotes.Notes.BODY;
        strArr[DELETE_ID] = "category";
        strArr[SELECT_CATEGORY_ID] = FliqNotes.Notes.CREATION_DATE;
        strArr[EMAIL_ID] = "title";
        NOTE_PROJECTION = strArr;
    }

    public static class LinedEditText extends EditText {
        private Paint mPaint = new Paint();
        private Rect mRect = new Rect();

        public LinedEditText(Context context, AttributeSet attrs) {
            super(context, attrs);
            this.mPaint.setStyle(Paint.Style.STROKE);
            this.mPaint.setColor(-2135431683);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            int count = getLineCount();
            Rect r = this.mRect;
            Paint paint = this.mPaint;
            for (int i = 0; i < count; i++) {
                int baseline = getLineBounds(i, r);
                canvas.drawLine((float) r.left, (float) (baseline + 1), (float) r.right, (float) (baseline + 1), paint);
            }
            super.onDraw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.note_editor);
        this.mContext = this;
        this.mIntentAction = getIntent().getAction();
        if ("android.intent.action.EDIT".equals(this.mIntentAction)) {
            this.mUri = getIntent().getData();
            this.mEmptyInsertPending = false;
        } else if ("android.intent.action.INSERT".equals(this.mIntentAction)) {
            ContentValues values = new ContentValues();
            values.put("uuid", UUID.randomUUID().toString());
            values.put(FliqNotes.Notes.TYPE, "text/plain");
            values.put("title", "");
            values.put(FliqNotes.Notes.BODY, "");
            values.put("category", Prefs.getDefaultCategory(this.mContext));
            this.mUri = getContentResolver().insert(FliqNotes.Notes.CONTENT_URI, values);
            if (Config.D) {
                Log.d(TAG, ".onCreate new note w/ID: " + this.mUri.toString());
            }
            if (this.mUri == null) {
                Log.e(TAG, "failed to insert new note");
                setResult(0);
                finish();
                return;
            }
            setResult(-1, new Intent().setAction(this.mUri.toString()));
            this.mEmptyInsertPending = true;
        } else {
            Log.e(TAG, "Unknown intent, exiting.");
            setResult(0);
            finish();
            return;
        }
        this.mNextButton = (ImageButton) findViewById(R.id.showNextNote);
        if (this.mNextButton != null) {
            this.mNextButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (NoteEditor.this.mDirtyFlag) {
                        NoteEditor.this.updateNote();
                    }
                    NotesList.mCurrentNoteIdIndex++;
                    try {
                        NoteEditor.this.mUri = ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, (long) NotesList.mCurrentNoteIdList[NotesList.mCurrentNoteIdIndex]);
                        NoteEditor.this.loadNoteFromProvider(true);
                    } catch (ArrayIndexOutOfBoundsException e) {
                    }
                }
            });
        }
        this.mPrevButton = (ImageButton) findViewById(R.id.showPrevNote);
        if (this.mPrevButton != null) {
            this.mPrevButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (NoteEditor.this.mDirtyFlag) {
                        NoteEditor.this.updateNote();
                    }
                    NotesList.mCurrentNoteIdIndex--;
                    try {
                        NoteEditor.this.mUri = ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, (long) NotesList.mCurrentNoteIdList[NotesList.mCurrentNoteIdIndex]);
                        NoteEditor.this.loadNoteFromProvider(true);
                    } catch (ArrayIndexOutOfBoundsException e) {
                    }
                }
            });
        }
        this.mText = (EditText) findViewById(R.id.note);
        this.mText.setTextSize(Float.parseFloat(Prefs.getTextSize(this)));
        this.mText.setTypeface(Prefs.getTypeface(this));
        this.mText.requestFocus();
        this.mText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable arg0) {
                NoteEditor.this.mDirtyFlag = true;
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
        this.mTitleText = (TextView) findViewById(R.id.title_left_text);
        this.mTitleText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NoteEditor.editTitleDialog(NoteEditor.this.mContext, NoteEditor.this.mUri, new Runnable() {
                    public void run() {
                        NoteEditor.this.mEmptyInsertPending = false;
                        NoteEditor.this.loadNoteFromProvider(true);
                    }
                });
            }
        });
        this.mCategoryChip = findViewById(R.id.noteEditChip);
        this.mCategoryText = (TextView) findViewById(R.id.noteEditCategory);
        this.mCategoryText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NoteEditor.this.mEmptyInsertPending = false;
                NoteEditor.this.startActivityForResult(new Intent(NoteEditor.this, CategoryList.class).putExtra("uuid", NoteEditor.this.mCategoryUuid), 1);
            }
        });
        this.mModDateText = (TextView) findViewById(R.id.noteEditModDate);
        TextView title = (TextView) findViewById(R.id.title_left_text);
        if ("android.intent.action.EDIT".equals(this.mIntentAction)) {
            title.setText((int) R.string.title_edit);
        } else {
            title.setText((int) R.string.title_create);
        }
        FliqNotesApp.increaseLockSkipCount(this);
    }

    /* access modifiers changed from: private */
    public void loadNoteFromProvider(boolean loadCategoryFromProvider) {
        if (Config.V) {
            Log.v(TAG, ".loadNoteFromProvider mUri=" + this.mUri.toString() + " dirty=" + this.mDirtyFlag);
        }
        this.mCursor = managedQuery(this.mUri, NOTE_PROJECTION, null, null, null);
        if (this.mCursor != null && this.mCursor.moveToFirst()) {
            this.mNoteUuid = this.mCursor.getString(this.mCursor.getColumnIndex("uuid"));
            if (!this.mDirtyFlag) {
                this.mOriginalNoteText = this.mCursor.getString(this.mCursor.getColumnIndex(FliqNotes.Notes.BODY));
                this.mText.setText(this.mOriginalNoteText);
                this.mText.setSelection(0);
                this.mDirtyFlag = false;
            }
            setTitleFromString(this.mCursor.getString(this.mCursor.getColumnIndex("title")));
            if (loadCategoryFromProvider) {
                this.mCategoryUuid = this.mCursor.getString(this.mCursor.getColumnIndex("category"));
            }
            String categoryName = FliqNotesProvider.categoryNameForUUID(this.mCategoryUuid);
            if (categoryName != null) {
                this.mCategoryText.setText(categoryName);
            }
            if (this.mCategoryUuid.matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
                this.mCategoryChip.setBackgroundDrawable(Utilities.getColorChip(Color.parseColor(Prefs.getUnfiledColor(this))));
            } else {
                this.mCategoryChip.setBackgroundDrawable(Utilities.getColorChip(Color.parseColor(FliqNotesProvider.categoryColorForUUID(this.mCategoryUuid))));
            }
            this.mModDateText.setTextKeepState(DateFormat.getDateTimeInstance(DELETE_ID, DELETE_ID).format(new Date(FliqNotes.convertProtocolStringToTimeMilliseconds(this.mCursor.getString(this.mCursor.getColumnIndex(FliqNotes.Notes.CREATION_DATE))))));
            if (!"android.intent.action.EDIT".equals(getIntent().getAction()) || NotesList.mCurrentNoteIdList == null) {
                this.mNextButton.setVisibility(8);
                this.mPrevButton.setVisibility(8);
                return;
            }
            if (NotesList.mCurrentNoteIdIndex == Array.getLength(NotesList.mCurrentNoteIdList) - 1) {
                this.mNextButton.setVisibility(8);
            } else {
                this.mNextButton.setVisibility(0);
            }
            if (NotesList.mCurrentNoteIdIndex == 0) {
                this.mPrevButton.setVisibility(8);
            } else {
                this.mPrevButton.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (Config.V) {
            Log.v(TAG, ".onResume: dirty=" + this.mDirtyFlag + ", insertPending=" + this.mEmptyInsertPending);
        }
        loadNoteFromProvider(this.mCategoryUuid == null);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (Config.V) {
            Log.v(TAG, ".onPause: dirty=" + this.mDirtyFlag + ", insertPending=" + this.mEmptyInsertPending);
        }
        if (this.mDirtyFlag) {
            updateNote();
        } else if (this.mEmptyInsertPending) {
            deleteNote(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (Config.V) {
            Log.v(TAG, ".onDestroy");
        }
        FliqNotesApp.decreaseLockSkipCount();
    }

    /* access modifiers changed from: private */
    public void updateNote() {
        if (Config.V) {
            Log.v(TAG, ".updateNote category=" + this.mCategoryUuid);
        }
        ContentValues values = new ContentValues();
        values.put(FliqNotes.Notes.MODIFICATION_DATE, FliqNotes.convertTimeMillisecondsToProtocolString(System.currentTimeMillis()));
        values.put("title", getTitleFromUI());
        values.put(FliqNotes.Notes.BODY, this.mText.getText().toString());
        values.put("category", this.mCategoryUuid);
        getContentResolver().update(this.mUri, values, null, null);
        values.clear();
        values.put("uuid", this.mNoteUuid);
        getContentResolver().insert(FliqNotes.Modifies.CONTENT_URI, values);
        this.mDirtyFlag = false;
        this.mEmptyInsertPending = false;
    }

    /* access modifiers changed from: private */
    public final void deleteNote(boolean trackDelete) {
        if (Config.V) {
            Log.v(TAG, ".deleteNote track=" + trackDelete);
        }
        if (this.mCursor != null) {
            if (trackDelete) {
                try {
                    if (Config.D) {
                        Log.d(TAG, ".deleteNote - deleting note with UUID: " + this.mCursor.getString(this.mCursor.getColumnIndex("uuid")));
                    }
                    ContentValues values = new ContentValues();
                    values.put("uuid", this.mCursor.getString(this.mCursor.getColumnIndex("uuid")));
                    getContentResolver().insert(FliqNotes.Deletes.CONTENT_URI, values);
                    getContentResolver().delete(FliqNotes.Modifies.CONTENT_URI, "uuid=?", new String[]{this.mCursor.getString(this.mCursor.getColumnIndex("uuid"))});
                } catch (CursorIndexOutOfBoundsException e) {
                }
            }
            getContentResolver().delete(this.mUri, null, null);
            this.mCursor.close();
            this.mCursor = null;
            this.mText.setText("");
        } else {
            if (Config.D) {
                Log.d(TAG, ".deleteNote - deleting note with URI: " + this.mUri.toString());
            }
            getContentResolver().delete(this.mUri, null, null);
        }
        this.mDirtyFlag = false;
        this.mEmptyInsertPending = false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if ("android.intent.action.EDIT".equals(getIntent().getAction())) {
            menu.add(0, 1, 0, (int) R.string.menu_revert).setShortcut('0', 'r').setIcon(17301580);
            menu.add(0, (int) EMAIL_ID, 1, (int) R.string.menu_email).setShortcut('1', 'm').setIcon(17301584);
            menu.add(0, (int) EDIT_TITLE_ID, (int) DISCARD_ID, (int) R.string.menu_edit_title).setShortcut('1', 't').setIcon(17301566);
            menu.add(0, (int) DELETE_ID, (int) DELETE_ID, (int) R.string.menu_delete).setShortcut('3', 'd').setIcon(17301564);
        } else {
            menu.add(0, (int) DISCARD_ID, (int) DELETE_ID, (int) R.string.menu_discard).setShortcut('0', 'd').setIcon(17301564);
        }
        menu.add(0, (int) SELECT_CATEGORY_ID, 0, (int) R.string.menu_category).setShortcut('4', 'c').setIcon((int) R.drawable.ic_menu_category);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                this.mDirtyFlag = false;
                finish();
                break;
            case DISCARD_ID /*2*/:
                this.mDirtyFlag = false;
                deleteNote(false);
                finish();
                break;
            case DELETE_ID /*3*/:
                new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.menu_delete_note).setMessage("'" + ((Object) this.mTitleText.getText()) + "' " + getResources().getString(R.string.dialog_delete)).setCancelable(true).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).setPositiveButton((int) R.string.menu_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        NoteEditor.this.deleteNote(true);
                        NoteEditor.this.finish();
                    }
                }).show();
                break;
            case SELECT_CATEGORY_ID /*4*/:
                this.mEmptyInsertPending = false;
                startActivityForResult(new Intent().setClass(this, CategoryList.class).putExtra("uuid", this.mCategoryUuid), 1);
                break;
            case EMAIL_ID /*5*/:
                Intent emailIntent = new Intent("android.intent.action.SEND");
                emailIntent.setType("text/plain");
                emailIntent.putExtra("android.intent.extra.SUBJECT", String.valueOf(getResources().getString(R.string.fliq_note)) + getTitleFromUI());
                emailIntent.putExtra("android.intent.extra.TEXT", this.mText.getText());
                startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.send_note)));
                break;
            case FLIQ_ID /*6*/:
                new AlertDialog.Builder(this).setMessage((int) R.string.fliq_not_available).create().show();
                break;
            case EDIT_TITLE_ID /*7*/:
                editTitleDialog(this.mContext, this.mUri, new Runnable() {
                    public void run() {
                        NoteEditor.this.mEmptyInsertPending = false;
                        NoteEditor.this.loadNoteFromProvider(true);
                    }
                });
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void editTitleDialog(final Context context, Uri noteUri, Runnable callback) {
        if (Config.V) {
            Log.v(TAG, ".editTitleDialog");
        }
        Cursor c = context.getContentResolver().query(noteUri, NOTE_PROJECTION, null, null, null);
        if (c == null || !c.moveToFirst()) {
            Log.e(TAG, "Failed to load note for title edit - " + noteUri.toString());
            return;
        }
        final String noteUUID = c.getString(c.getColumnIndex("uuid"));
        FrameLayout fl = new FrameLayout(context);
        final EditText input = new EditText(context);
        final String defaultTitle = FliqNotes.titleFromNoteBody(c.getString(c.getColumnIndex(FliqNotes.Notes.BODY)));
        input.setGravity(DELETE_ID);
        fl.addView(input, new FrameLayout.LayoutParams(-1, -2));
        input.setText(c.getString(c.getColumnIndex("title")));
        c.close();
        final Context context2 = context;
        final Uri uri = noteUri;
        final Runnable runnable = callback;
        new AlertDialog.Builder(context).setView(fl).setTitle((int) R.string.edit_title_title).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int which) {
                ((InputMethodManager) context2.getSystemService("input_method")).hideSoftInputFromWindow(input.getApplicationWindowToken(), 0);
                d.dismiss();
                String newTitle = input.getText().toString();
                if (newTitle.length() == 0) {
                    newTitle = defaultTitle;
                }
                if (Config.V) {
                    Log.v(NoteEditor.TAG, ".editTitleDialog  newTitle=" + newTitle + " noteUri=" + uri.toString());
                }
                ContentValues values = new ContentValues();
                values.put("title", newTitle);
                values.put(FliqNotes.Notes.MODIFICATION_DATE, FliqNotes.convertTimeMillisecondsToProtocolString(System.currentTimeMillis()));
                context2.getContentResolver().update(uri, values, null, null);
                values.clear();
                values.put("uuid", noteUUID);
                context2.getContentResolver().insert(FliqNotes.Modifies.CONTENT_URI, values);
                if (runnable != null) {
                    runnable.run();
                }
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int which) {
                ((InputMethodManager) context.getSystemService("input_method")).hideSoftInputFromWindow(input.getApplicationWindowToken(), 0);
                d.dismiss();
            }
        }).create().show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Config.V) {
            Log.v(TAG, ".onActivityResult resultCode=" + resultCode);
        }
        if (requestCode != 1) {
            return;
        }
        if (resultCode == -1) {
            this.mCategoryUuid = data.getStringExtra("uuid");
            if (Config.D) {
                Log.d(TAG, ".onActivityResult categoryUUID=" + this.mCategoryUuid);
            }
            ContentValues values = new ContentValues();
            values.put("category", this.mCategoryUuid);
            values.put(FliqNotes.Notes.MODIFICATION_DATE, FliqNotes.convertTimeMillisecondsToProtocolString(System.currentTimeMillis()));
            this.mContext.getContentResolver().update(this.mUri, values, null, null);
            values.clear();
            values.put("uuid", this.mNoteUuid);
            this.mContext.getContentResolver().insert(FliqNotes.Modifies.CONTENT_URI, values);
        } else if (FliqNotesProvider.categoryNameForUUID(this.mCategoryUuid) == null) {
            this.mCategoryUuid = FliqNotes.Categories.UNFILED_CATEGORY_UUID;
        }
    }

    private void setTitleFromString(String title) {
        if (Config.V) {
            Log.v(TAG, ".setTitleFromString title=" + title);
        }
        if (title == null || title.length() <= 0) {
            this.mTitleText.setText((int) R.string.note_title_untitled);
            this.mTitleText.setTypeface(Typeface.DEFAULT, DISCARD_ID);
            return;
        }
        this.mTitleText.setText(title);
        this.mTitleText.setTypeface(Typeface.DEFAULT, 0);
    }

    private String getTitleFromUI() {
        String title = this.mTitleText.getText().toString();
        if (title.equals(getResources().getString(R.string.note_title_untitled))) {
            return FliqNotes.titleFromNoteBody(this.mText.getText().toString());
        }
        return title;
    }
}
