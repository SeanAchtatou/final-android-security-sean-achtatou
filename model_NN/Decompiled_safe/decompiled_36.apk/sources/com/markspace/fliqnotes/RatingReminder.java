package com.markspace.fliqnotes;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.markspace.test.Config;
import com.markspace.util.Utilities;

public class RatingReminder {
    private static final String PREF_KEY_DAYS_UNTIL_PROMPT = "remind_wait_days";
    private static final String PREF_KEY_DONT_REMIND = "dont_remind";
    private static final String PREF_KEY_FIRST_LAUNCH_DATE = "first_launch_date";
    private static final String PREF_KEY_LAUNCH_COUNT = "launch_count";
    private static final int REMINDER_DAYS_UNTIL_PROMPT = 30;
    private static final int REMINDER_LAUNCHES_UNTIL_PROMPT = 30;
    private static final String TAG = "RatingReminder";

    /* JADX INFO: Multiple debug info for r2v5 long: [D('firstLaunch' long), D('prefs' android.content.SharedPreferences)] */
    /* JADX INFO: Multiple debug info for r2v7 long: [D('firstLaunch' long), D('threshhold' long)] */
    public static void onAppStartup(Context c) {
        if (!Utilities.isIntentAvailable(c, new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + c.getPackageName())))) {
            Log.w(TAG, "device has no market, skippnig rating reminder");
            return;
        }
        Log.e(TAG, "NOT skipping rating reminder for non-Market device.");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
        if (!prefs.getBoolean(PREF_KEY_DONT_REMIND, false)) {
            SharedPreferences.Editor editor = prefs.edit();
            int daysUntilPrompt = prefs.getInt(PREF_KEY_DAYS_UNTIL_PROMPT, 30);
            if (Config.D) {
                Log.d(TAG, ".onAppStartup: days until prompt is " + daysUntilPrompt);
            }
            long launchCount = prefs.getLong(PREF_KEY_LAUNCH_COUNT, 0) + 1;
            editor.putLong(PREF_KEY_LAUNCH_COUNT, launchCount);
            if (Config.D) {
                Log.d(TAG, ".onAppStartup: launch count is " + launchCount);
            }
            long firstLaunch = prefs.getLong(PREF_KEY_FIRST_LAUNCH_DATE, 0);
            if (firstLaunch == 0) {
                if (Config.D) {
                    Log.d(TAG, ".onAppStartup: initializing first launch");
                }
                firstLaunch = System.currentTimeMillis();
                editor.putLong(PREF_KEY_FIRST_LAUNCH_DATE, firstLaunch);
            }
            if (launchCount >= 30) {
                long threshhold = firstLaunch + (((long) daysUntilPrompt) * 24 * 60 * 60 * 1000);
                if (Config.D) {
                    Log.d(TAG, ".onAppStartup: number of launches met");
                }
                if (Config.D) {
                    Log.d(TAG, ".onAppStartup: date thresh is " + mToS(threshhold));
                }
                if (System.currentTimeMillis() >= threshhold) {
                    if (Config.D) {
                        Log.d(TAG, ".onAppStartup: number of days met");
                    }
                    showRateDialog(c, editor, daysUntilPrompt);
                }
            }
            editor.commit();
        } else if (Config.D) {
            Log.d(TAG, ".onAppStartup: don't remind set, skipping");
        }
    }

    public static void showRateDialog(final Context c, final SharedPreferences.Editor editor, final int currentDaysUntilPrompt) {
        if (Config.D) {
            Log.d(TAG, ".showRateDialog");
        }
        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(1);
        dialog.setContentView((int) R.layout.rating_reminder);
        ((TextView) dialog.findViewById(R.id.rating_reminder_title)).setText(c.getText(R.string.rating_reminder_title));
        ((TextView) dialog.findViewById(R.id.rating_reminder_text)).setText(c.getText(R.string.rating_reminder_text));
        ((Button) dialog.findViewById(R.id.rating_reminder_ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                c.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + c.getPackageName())));
                if (editor != null) {
                    editor.putBoolean(RatingReminder.PREF_KEY_DONT_REMIND, true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.rating_reminder_later)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putInt(RatingReminder.PREF_KEY_DAYS_UNTIL_PROMPT, currentDaysUntilPrompt + 5);
                    editor.commit();
                }
                if (Config.D) {
                    Log.d(RatingReminder.TAG, ".showRateDialog set new days until to " + currentDaysUntilPrompt + 5);
                }
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.rating_reminder_no)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean(RatingReminder.PREF_KEY_DONT_REMIND, true);
                    editor.commit();
                }
                if (Config.D) {
                    Log.d(RatingReminder.TAG, ".showRateDialog set don't remind");
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private static String mToS(long millis) {
        Time time = new Time();
        time.set(millis);
        return time.format("%Y-%m-%d %H:%M:%S");
    }
}
