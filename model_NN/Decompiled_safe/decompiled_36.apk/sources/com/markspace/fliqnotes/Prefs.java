package com.markspace.fliqnotes;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import com.markspace.provider.FliqNotes;
import com.markspace.providers.notes.FliqNotesProvider;
import com.markspace.test.Config;

public class Prefs extends PreferenceActivity implements Preference.OnPreferenceClickListener, Preference.OnPreferenceChangeListener {
    static final String KEY_BUILD_NUMBER = "build_version";
    static final String KEY_OTHER_APPS = "other_apps";
    static final String KEY_RELEASE_NOTES = "release_notes";
    static final String KEY_UNFILED_COLOR = "unfiled_color";
    private static final String OPT_DEFAULT_CATEGORY = "defaultcat";
    private static final String OPT_DEFAULT_CATEGORY_DEFAULT = "0";
    private static final String OPT_FLIQAVAIL = "fliqavailability";
    private static final boolean OPT_FLIQAVAIL_DEFAULT = false;
    private static final String OPT_LIST_TEXTSIZE = "list_textsize";
    private static final String OPT_LIST_TEXTSIZE_DEFAULT = "20";
    private static final String OPT_LIST_TYPEFACE = "list_typeface";
    private static final String OPT_LIST_TYPEFACE_DEFAULT = "Normal";
    private static final String OPT_NOTE_COUNT = "notecount";
    private static final String OPT_PASSWORD_REQ = "requirepassword";
    private static final boolean OPT_PASSWORD_REQ_DEFAULT = false;
    private static final String OPT_TEXTSIZE = "textsize";
    private static final String OPT_TEXTSIZE_DEFAULT = "20";
    private static final String OPT_TYPEFACE = "typeface";
    private static final String OPT_TYPEFACE_DEFAULT = "Normal";
    private static final int PASSWORD_PROTECTION_REQUEST = 2;
    private static final int PICK_CATEGORY_REQUEST = 1;
    private static final String TAG = "Prefs";
    Preference mDefaultCategory;
    Preference mOtherApps;
    boolean mPendingNewPasswordSetting;
    Preference mReleaseNotes;
    CheckBoxPreference mRequirePassword;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        if (Config.V) {
            Log.v(TAG, ".onCreate");
        }
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        ContentResolver contentResolver = getContentResolver();
        Uri uri = FliqNotes.Notes.CONTENT_URI;
        String[] strArr = new String[PICK_CATEGORY_REQUEST];
        strArr[0] = "uuid";
        Cursor c = contentResolver.query(uri, strArr, null, null, null);
        if (c != null) {
            findPreference(OPT_NOTE_COUNT).setSummary(Integer.toString(c.getCount()));
            c.close();
        } else {
            findPreference(OPT_NOTE_COUNT).setSummary("0");
        }
        this.mDefaultCategory = findPreference(OPT_DEFAULT_CATEGORY);
        this.mOtherApps = findPreference(KEY_OTHER_APPS);
        this.mReleaseNotes = findPreference(KEY_RELEASE_NOTES);
        this.mRequirePassword = (CheckBoxPreference) findPreference(OPT_PASSWORD_REQ);
        if (this.mOtherApps == null || this.mReleaseNotes == null || this.mDefaultCategory == null || this.mRequirePassword == null) {
            Log.e(TAG, ".onCreate failed to find required preference resources");
        } else {
            this.mOtherApps.setOnPreferenceClickListener(this);
            this.mReleaseNotes.setOnPreferenceClickListener(this);
            this.mDefaultCategory.setOnPreferenceClickListener(this);
            this.mRequirePassword.setOnPreferenceChangeListener(this);
        }
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            if (Config.V) {
                Log.v(TAG, ".onCreate - package info: " + packageInfo.versionName);
            }
            findPreference(KEY_BUILD_NUMBER).setSummary(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            findPreference(KEY_BUILD_NUMBER).setSummary("?");
        }
        FliqNotesApp.increaseLockSkipCount(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (Config.V) {
            Log.v(TAG, ".onResume");
        }
        this.mDefaultCategory.setSummary(String.valueOf(getResources().getString(R.string.settings_default_cat_summary)) + " " + FliqNotesProvider.categoryNameForUUID(getDefaultCategory(this)));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (Config.V) {
            Log.v(TAG, ".onDestroy");
        }
        FliqNotesApp.decreaseLockSkipCount();
    }

    public boolean onPreferenceClick(Preference preference) {
        if (Config.V) {
            Log.v(TAG, ".onPreferenceClick");
        }
        if (preference.getKey().equals(KEY_OTHER_APPS)) {
            FliqNotesApp.fireOtherAppsIntent(this);
        } else if (preference.getKey().equals(KEY_RELEASE_NOTES)) {
            startActivity(new Intent(this, ReleaseNotes.class));
        } else if (preference.getKey().equals(OPT_DEFAULT_CATEGORY)) {
            startActivityForResult(new Intent().setClass(this, CategoryList.class).putExtra("uuid", getDefaultCategory(this)), PICK_CATEGORY_REQUEST);
        }
        return true;
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String passwordActivityCommand;
        if (Config.V) {
            Log.v(TAG, ".onPreferenceChange");
        }
        this.mPendingNewPasswordSetting = ((Boolean) newValue).booleanValue();
        if (!preference.getKey().equals(OPT_PASSWORD_REQ)) {
            return true;
        }
        if (Config.V) {
            Log.v(TAG, ".onPreferenceChange - password request for: " + newValue + " which is a " + newValue.getClass());
        }
        if (this.mPendingNewPasswordSetting) {
            passwordActivityCommand = PasswordProtect.PASSWORD_SET;
        } else {
            passwordActivityCommand = PasswordProtect.PASSWORD_VALIDATE;
        }
        startActivityForResult(new Intent().setClass(this, PasswordProtect.class).putExtra(PasswordProtect.ACTION_KEY, passwordActivityCommand), PASSWORD_PROTECTION_REQUEST);
        return false;
    }

    public static String getDefaultCategory(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(OPT_DEFAULT_CATEGORY, "0");
    }

    public static void setDefaultCategory(Context context, String defaultCategory) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(OPT_DEFAULT_CATEGORY, defaultCategory);
        editor.commit();
    }

    public static String getTextSize(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(OPT_TEXTSIZE, "20");
    }

    public static Typeface getTypeface(Context context) {
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString(OPT_TYPEFACE, "Normal");
        if (value.matches("0")) {
            return Typeface.DEFAULT;
        }
        if (value.matches("1")) {
            return Typeface.SANS_SERIF;
        }
        if (value.matches("2")) {
            return Typeface.SERIF;
        }
        if (value.matches("3")) {
            return Typeface.MONOSPACE;
        }
        return null;
    }

    public static String getListTextSize(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(OPT_LIST_TEXTSIZE, "20");
    }

    public static Typeface getListTypeface(Context context) {
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString(OPT_LIST_TYPEFACE, "Normal");
        if (value.matches("0")) {
            return Typeface.DEFAULT;
        }
        if (value.matches("1")) {
            return Typeface.SANS_SERIF;
        }
        if (value.matches("2")) {
            return Typeface.SERIF;
        }
        if (value.matches("3")) {
            return Typeface.MONOSPACE;
        }
        return null;
    }

    public static boolean getFliqAvailability(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(OPT_FLIQAVAIL, false);
    }

    public static boolean getPasswordRequired(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(OPT_PASSWORD_REQ, false);
    }

    public static int getBuildNumber(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(KEY_BUILD_NUMBER, 0);
    }

    public static void setBuildNumber(Context context, int buildNumber) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(KEY_BUILD_NUMBER, buildNumber);
        editor.commit();
    }

    public static String getUnfiledColor(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_UNFILED_COLOR, "#609c00");
    }

    public static void setUnfiledColor(Context context, String unfiledColor) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(KEY_UNFILED_COLOR, unfiledColor);
        editor.commit();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Config.V) {
            Log.v(TAG, ".onActivityResult requestCode=" + requestCode + " resultCode=" + resultCode);
        }
        if (requestCode == PICK_CATEGORY_REQUEST) {
            if (resultCode == -1) {
                setDefaultCategory(this, data.getStringExtra("uuid"));
            } else if (FliqNotesProvider.categoryNameForUUID(getDefaultCategory(this)) == null) {
                setDefaultCategory(this, "0");
            }
        } else if (requestCode == PASSWORD_PROTECTION_REQUEST && resultCode == -1) {
            if (Config.D) {
                Log.d(TAG, ".onActivityResult password protect passed setting protection to " + this.mPendingNewPasswordSetting);
            }
            this.mRequirePassword.setChecked(this.mPendingNewPasswordSetting);
        }
    }
}
