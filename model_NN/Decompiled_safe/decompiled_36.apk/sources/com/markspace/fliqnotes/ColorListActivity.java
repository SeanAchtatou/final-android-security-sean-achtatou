package com.markspace.fliqnotes;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.markspace.provider.FliqNotes;
import com.markspace.test.Config;
import java.util.ArrayList;
import java.util.HashMap;

public class ColorListActivity extends ListActivity {
    private static final String ITEM_COLOR_HEX = "color_chip";
    private static final String ITEM_COLOR_NAME = "color_name";
    private static final String TAG = "ColorListActivity";
    private static final HashMap<String, String> sBlueRowMap = new HashMap<>();
    private static ArrayList<HashMap<String, String>> sColorList = new ArrayList<>();
    private static final HashMap<String, String> sGreenRowMap = new HashMap<>();
    private static final HashMap<String, String> sOrangeRowMap = new HashMap<>();
    private static final HashMap<String, String> sOtherRowMap = new HashMap<>();
    private static final HashMap<String, String> sPinkRowMap = new HashMap<>();
    private static final HashMap<String, String> sPurpleRowMap = new HashMap<>();
    private static final HashMap<String, String> sRedRowMap = new HashMap<>();
    private SimpleAdapter mAdapter;
    private int mCalendarColor;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.color_list);
        this.mCalendarColor = getIntent().getIntExtra(FliqNotes.Categories.COLOR, -16777216);
        if (Config.V) {
            Log.v(TAG, ".onCreate color=" + this.mCalendarColor);
        }
        this.mAdapter = new SimpleAdapter(this, sColorList, R.layout.color_list_item, new String[]{ITEM_COLOR_HEX, ITEM_COLOR_NAME}, new int[]{R.id.color_chip, R.id.color_name});
        this.mAdapter.setViewBinder(new ColorListViewBinder(this));
        setListAdapter(this.mAdapter);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (Config.V) {
            Log.v(TAG, ".onListItemClick color=" + ((String) sColorList.get(position).get(ITEM_COLOR_NAME)));
        }
        if (position == sColorList.size() - 1) {
            setResult(1);
        } else {
            setResult(-1, new Intent().putExtra(FliqNotes.Categories.COLOR, Color.parseColor((String) sColorList.get(position).get(ITEM_COLOR_HEX))));
        }
        finish();
    }

    private class ColorListViewBinder implements SimpleAdapter.ViewBinder {
        public ColorListViewBinder(Context context) {
        }

        public boolean setViewValue(View view, Object data, String textRep) {
            switch (view.getId()) {
                case R.id.color_chip /*2131296264*/:
                    view.setBackgroundColor(Color.parseColor(textRep));
                    return true;
                case R.id.color_name /*2131296265*/:
                    ((TextView) view).setText(textRep);
                    return true;
                default:
                    return true;
            }
        }
    }

    static {
        sBlueRowMap.put(ITEM_COLOR_HEX, "#0000aa");
        sBlueRowMap.put(ITEM_COLOR_NAME, "Blue");
        sGreenRowMap.put(ITEM_COLOR_HEX, "#00aa00");
        sGreenRowMap.put(ITEM_COLOR_NAME, "Green");
        sRedRowMap.put(ITEM_COLOR_HEX, "#aa0000");
        sRedRowMap.put(ITEM_COLOR_NAME, "Red");
        sOrangeRowMap.put(ITEM_COLOR_HEX, "#ff8c00");
        sOrangeRowMap.put(ITEM_COLOR_NAME, "Orange");
        sPinkRowMap.put(ITEM_COLOR_HEX, "#d02090");
        sPinkRowMap.put(ITEM_COLOR_NAME, "Pink");
        sPurpleRowMap.put(ITEM_COLOR_HEX, "#8000d0");
        sPurpleRowMap.put(ITEM_COLOR_NAME, "Purple");
        sOtherRowMap.put(ITEM_COLOR_HEX, "#999999");
        sOtherRowMap.put(ITEM_COLOR_NAME, "Other...");
        sColorList.add(sBlueRowMap);
        sColorList.add(sGreenRowMap);
        sColorList.add(sRedRowMap);
        sColorList.add(sOrangeRowMap);
        sColorList.add(sPinkRowMap);
        sColorList.add(sPurpleRowMap);
        sColorList.add(sOtherRowMap);
    }
}
