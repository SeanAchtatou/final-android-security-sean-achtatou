package com.markspace.fliqnotes;

import android.app.Activity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.TextView;

public class OtherAppsActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.other_apps);
        Linkify.addLinks((TextView) findViewById(R.id.other_apps_text_2), 1);
    }
}
