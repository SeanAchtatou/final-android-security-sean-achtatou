package com.markspace.fliqnotes;

import android.app.Activity;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.Log;
import com.markspace.test.Config;

public class FliqNotesApp extends Application {
    private static final String TAG = "FliqNotesApp";
    public static FliqNotesApp sContext;
    /* access modifiers changed from: private */
    public static Activity sCurrentActivity;
    /* access modifiers changed from: private */
    public static boolean sLockAppIfNecessary = true;
    private static int sLockSkipCounter = 0;
    private boolean mRestartingListActivity;
    private UserPresentReceiver mUserPresentReceiver = null;

    private class UserPresentReceiver extends BroadcastReceiver {
        private UserPresentReceiver() {
        }

        /* synthetic */ UserPresentReceiver(FliqNotesApp fliqNotesApp, UserPresentReceiver userPresentReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            Log.e(FliqNotesApp.TAG, ".onReceive " + intent.toString());
            if (!"android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                return;
            }
            if (FliqNotesApp.sCurrentActivity == null || !(FliqNotesApp.sCurrentActivity instanceof PasswordProtect)) {
                if (Config.D) {
                    Log.e(FliqNotesApp.TAG, "Resuming from lock - triggering locks");
                }
                FliqNotesApp.sLockAppIfNecessary = true;
            } else if (Config.D) {
                Log.e(FliqNotesApp.TAG, "resuming from lock - password already present");
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        if (Log.isLoggable(TAG, 3)) {
            Config.D = true;
        }
        if (Log.isLoggable(TAG, 2)) {
            Config.V = true;
        }
        if (Config.V) {
            Log.v(TAG, ".onCreate");
        }
        sContext = this;
        this.mUserPresentReceiver = new UserPresentReceiver(this, null);
        registerReceiver(this.mUserPresentReceiver, new IntentFilter("android.intent.action.USER_PRESENT"));
        setRestartStatus(false);
        super.onCreate();
    }

    public void onTerminate() {
        super.onTerminate();
    }

    /* access modifiers changed from: package-private */
    public synchronized void setRestartStatus(boolean restartingListActivity) {
        this.mRestartingListActivity = restartingListActivity;
    }

    /* access modifiers changed from: package-private */
    public boolean getRestartStatus() {
        return this.mRestartingListActivity;
    }

    public static FliqNotesApp getNotesApplicationContext() {
        return sContext;
    }

    public static void increaseLockSkipCount(Activity currentActivity) {
        sCurrentActivity = currentActivity;
        sLockSkipCounter++;
    }

    public static void decreaseLockSkipCount() {
        sCurrentActivity = null;
        sLockSkipCounter--;
        if (sLockSkipCounter == 0) {
            sLockAppIfNecessary = true;
        }
    }

    public static boolean isPasswordLockRequired() {
        return sLockAppIfNecessary && Prefs.getPasswordRequired(sContext) && sLockSkipCounter == 0;
    }

    public static void resetPasswordLockRequired() {
        sLockAppIfNecessary = false;
    }

    public static void fireOtherAppsIntent(Context c) {
        try {
            c.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"Mark/Space, Inc.\"")));
        } catch (ActivityNotFoundException e) {
            c.startActivity(new Intent(c, OtherAppsActivity.class));
        }
    }
}
