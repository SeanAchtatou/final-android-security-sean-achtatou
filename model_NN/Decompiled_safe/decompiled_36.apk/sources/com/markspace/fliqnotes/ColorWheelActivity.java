package com.markspace.fliqnotes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.markspace.provider.FliqNotes;
import com.markspace.test.Config;
import org.openintents.widget.ColorCircle;
import org.openintents.widget.ColorSlider;
import org.openintents.widget.OnColorChangedListener;

public class ColorWheelActivity extends Activity implements OnColorChangedListener {
    private static final String TAG = "ColorWheelActivity";
    ColorCircle mColorCircle;
    Intent mIntent;
    ColorSlider mSaturation;
    ColorSlider mValue;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        int color;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.color_wheel);
        if (Config.V) {
            Log.v(TAG, ".onCreate: " + getIntent().toString());
        }
        this.mIntent = getIntent();
        if (this.mIntent == null) {
            this.mIntent = new Intent();
        }
        ColorPickerState state = (ColorPickerState) getLastNonConfigurationInstance();
        if (state != null) {
            color = state.mColor;
        } else {
            color = this.mIntent.getIntExtra(FliqNotes.Categories.COLOR, -16777216);
        }
        this.mColorCircle = (ColorCircle) findViewById(R.id.colorcircle);
        this.mColorCircle.setOnColorChangedListener(this);
        this.mColorCircle.setColor(color);
        this.mSaturation = (ColorSlider) findViewById(R.id.saturation);
        this.mSaturation.setOnColorChangedListener(this);
        this.mSaturation.setColors(color, -16777216);
        this.mValue = (ColorSlider) findViewById(R.id.value);
        this.mValue.setOnColorChangedListener(this);
        this.mValue.setColors(-1, color);
    }

    class ColorPickerState {
        int mColor;

        ColorPickerState() {
        }
    }

    public Object onRetainNonConfigurationInstance() {
        ColorPickerState state = new ColorPickerState();
        state.mColor = this.mColorCircle.getColor();
        return state;
    }

    public int toGray(int color) {
        int a = Color.alpha(color);
        int gray = ((Color.red(color) + Color.green(color)) + Color.blue(color)) / 3;
        return Color.argb(a, gray, gray, gray);
    }

    public void onColorChanged(View view, int newColor) {
        if (view == this.mColorCircle) {
            this.mValue.setColors(-1, newColor);
            this.mSaturation.setColors(newColor, -16777216);
        } else if (view == this.mSaturation) {
            this.mColorCircle.setColor(newColor);
            this.mValue.setColors(-1, newColor);
        } else if (view == this.mValue) {
            this.mColorCircle.setColor(newColor);
        }
    }

    public void onColorPicked(View view, int newColor) {
        this.mIntent.putExtra(FliqNotes.Categories.COLOR, newColor);
        setResult(-1, this.mIntent);
        finish();
    }
}
