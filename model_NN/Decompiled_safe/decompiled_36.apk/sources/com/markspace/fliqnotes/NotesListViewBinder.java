package com.markspace.fliqnotes;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.markspace.provider.FliqNotes;
import com.markspace.test.Config;
import com.markspace.util.Utilities;
import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

public class NotesListViewBinder implements SimpleCursorAdapter.ViewBinder {
    private static final String TAG = "NotesListViewBinder";
    private Context mContext;
    private DateFormat mDateFormat;
    private DateFormat mTimeFormat;
    private Date mToday = new Date();
    private int mUtcOffset;

    public NotesListViewBinder(Context context) {
        this.mContext = context;
        this.mDateFormat = android.text.format.DateFormat.getDateFormat(context);
        this.mTimeFormat = android.text.format.DateFormat.getTimeFormat(context);
        this.mToday.setHours(0);
        this.mToday.setMinutes(0);
        this.mToday.setSeconds(0);
        this.mUtcOffset = TimeZone.getDefault().getRawOffset();
    }

    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        String text;
        String text2;
        if (Config.V) {
            Log.v(TAG, ".setViewValue(" + columnIndex + ")");
        }
        switch (columnIndex) {
            case 0:
                try {
                    if (cursor.getString(1).matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
                        view.setBackgroundDrawable(Utilities.getColorChip(Color.parseColor(Prefs.getUnfiledColor(this.mContext))));
                        return true;
                    }
                    view.setBackgroundDrawable(Utilities.getColorChip(Color.parseColor(cursor.getString(8))));
                    return true;
                } catch (IllegalArgumentException e) {
                    view.setBackgroundDrawable(Utilities.getColorChip(-3355444));
                    return true;
                }
            case 1:
                TextView title = (TextView) view;
                title.setTextSize(Float.parseFloat(Prefs.getListTextSize(this.mContext)));
                title.setTypeface(Prefs.getListTypeface(this.mContext));
                String titleString = cursor.getString(6);
                if (titleString == null || titleString.length() <= 0) {
                    titleString = FliqNotes.titleFromNoteBody(cursor.getString(2));
                }
                title.setText(titleString);
                return true;
            case 2:
                TextView dateView = (TextView) view;
                dateView.setTextSize((float) (((double) Float.parseFloat(Prefs.getListTextSize(this.mContext))) * 0.75d));
                if (NotesList.pSortSetting == 2) {
                    text = cursor.getString(5);
                } else {
                    text = cursor.getString(3);
                }
                long timestamp = FliqNotes.convertProtocolStringToTimeMilliseconds(text);
                Date compareDate = new Date(((long) this.mUtcOffset) + timestamp);
                Date stringDate = new Date(timestamp);
                if (isDateToday(compareDate)) {
                    text2 = this.mTimeFormat.format(stringDate);
                } else if (isDateYesterday(compareDate)) {
                    dateView.setText((int) R.string.yesterday);
                    return true;
                } else if (isDateThisWeek(compareDate)) {
                    text2 = (String) android.text.format.DateFormat.format("EEEE", stringDate);
                } else {
                    text2 = this.mDateFormat.format(stringDate);
                }
                dateView.setText(text2);
                return true;
            default:
                return true;
        }
    }

    public boolean isDateToday(Date date) {
        if (date.getYear() == this.mToday.getYear() && date.getMonth() == this.mToday.getMonth() && date.getDate() == this.mToday.getDate()) {
            return true;
        }
        return false;
    }

    public boolean isDateYesterday(Date date) {
        if (date.getTime() > this.mToday.getTime() - 86400000) {
            return true;
        }
        return false;
    }

    public boolean isDateThisWeek(Date date) {
        if (date.getTime() > this.mToday.getTime() - 518400000) {
            return true;
        }
        return false;
    }
}
