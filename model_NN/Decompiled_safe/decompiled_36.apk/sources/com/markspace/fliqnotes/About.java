package com.markspace.fliqnotes;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

public class About extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.about);
        TextView t = (TextView) findViewById(R.id.about_text);
        String aboutText = getResources().getString(R.string.about_text_1);
        try {
            aboutText = aboutText.concat(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
        }
        t.setText(aboutText.concat(getResources().getString(R.string.about_text_2)));
        FliqNotesApp.increaseLockSkipCount(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        FliqNotesApp.decreaseLockSkipCount();
    }
}
