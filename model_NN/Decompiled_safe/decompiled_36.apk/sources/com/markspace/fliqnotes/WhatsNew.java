package com.markspace.fliqnotes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.markspace.test.Config;

public class WhatsNew extends Activity implements View.OnClickListener {
    private static final String TAG = "WhatsNew";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Config.V) {
            Log.v(TAG, ".onCreate");
        }
        requestWindowFeature(1);
        setContentView((int) R.layout.whats_new);
        ((Button) findViewById(R.id.whats_new_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.view_release_notes)).setOnClickListener(this);
    }

    public void onClick(View v) {
        if (Config.V) {
            Log.v(TAG, ".onClick");
        }
        if (v.getId() == R.id.view_release_notes) {
            startActivity(new Intent(this, ReleaseNotes.class));
        }
        finish();
    }
}
