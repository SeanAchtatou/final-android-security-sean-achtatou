package com.markspace.fliqnotes;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AlphabetIndexer;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.markspace.provider.FliqNotes;
import com.markspace.test.Config;
import java.text.Collator;
import java.util.Arrays;

public class NotesList extends ListActivity {
    private static final int MENU_ITEM_DELETE = 2;
    private static final int MENU_ITEM_EDIT = 1;
    private static final int MENU_ITEM_EDIT_TITLE = 3;
    private static final int MENU_ITEM_SEND_NOTE = 4;
    private static final int NOTE_BODY_COLUMN_INDEX = 2;
    private static final int NOTE_CATEGORY_COLUMN_INDEX = 7;
    private static final int NOTE_CREATION_DATE_COLUMN_INDEX = 3;
    private static final String[] NOTE_DATA_PROJECTION;
    private static final int NOTE_MODIFICATION_DATE_COLUMN_INDEX = 5;
    private static final int NOTE_TITLE_COLUMN_INDEX = 6;
    private static final int PASSWORD_VALIDATION_REQUEST = 1;
    private static final String PREFS_NAME = "FliqNotes";
    private static final String TAG = "NotesList";
    public static int mCurrentNoteIdIndex = 0;
    public static int[] mCurrentNoteIdList = null;
    static final int mSortSettingCategory = 3;
    static final int mSortSettingCreationDate = 1;
    static final int mSortSettingModificationDate = 2;
    static final int mSortSettingTitle = 0;
    static int pSortSetting = 0;
    /* access modifiers changed from: private */
    public NoteItemListAdapter mAdapter;
    private ChangeObserver mChangeObserver;
    public int mCurrentPosition;
    private int mCurrentTopOfScrollList;
    private Cursor mCursor = null;
    private ListView mListView;
    private NotesListViewBinder mListViewBinder;
    private SharedPreferences mSettings;
    private TextView mTitleBarNoteCount;

    static {
        String[] strArr = new String[9];
        strArr[0] = "_id";
        strArr[1] = "category";
        strArr[2] = FliqNotes.Notes.BODY;
        strArr[3] = FliqNotes.Notes.CREATION_DATE;
        strArr[MENU_ITEM_SEND_NOTE] = "uuid";
        strArr[NOTE_MODIFICATION_DATE_COLUMN_INDEX] = FliqNotes.Notes.MODIFICATION_DATE;
        strArr[NOTE_TITLE_COLUMN_INDEX] = "title";
        strArr[NOTE_CATEGORY_COLUMN_INDEX] = FliqNotes.Notes.CATEGORY_NAME;
        strArr[8] = FliqNotes.Notes.CATEGORY_COLOR;
        NOTE_DATA_PROJECTION = strArr;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String searchClause;
        String order;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.noteslist);
        this.mSettings = getSharedPreferences(PREFS_NAME, 0);
        loadSettings();
        Intent intent = getIntent();
        String action = intent.getAction();
        if (Config.D) {
            Log.d(TAG, ".onCreate action is " + action);
        }
        if ("android.intent.action.SEARCH".equals(action)) {
            String searchString = intent.getStringExtra("query").replaceAll("'", "%");
            if (Config.D) {
                Log.d(TAG, ".onCreate LOCAL SEARCH FOUND: " + searchString);
            }
            searchClause = "(noteBody LIKE '%" + searchString + "%' OR " + "title" + " LIKE '%" + searchString + "%')";
        } else {
            searchClause = null;
        }
        if ("android.intent.action.EDIT".equals(action)) {
            startActivity(new Intent("android.intent.action.EDIT", intent.getData()));
            finish();
        }
        if (pSortSetting == 0 || pSortSetting == 3) {
            order = " ASC";
        } else {
            order = " DESC";
        }
        this.mCursor = managedQuery(FliqNotes.Categories.JOINED_CONTENT_URI, NOTE_DATA_PROJECTION, searchClause, null, String.valueOf(getSortString()) + " COLLATE NOCASE " + order);
        this.mChangeObserver = new ChangeObserver();
        if (this.mCursor != null) {
            this.mCursor.registerContentObserver(this.mChangeObserver);
            buildIdArrayFromCursor(this.mCursor);
        }
        this.mAdapter = new NoteItemListAdapter(this, R.layout.noteslist_item, this.mCursor, NOTE_DATA_PROJECTION, new int[]{R.id.notelist_category_chip, R.id.notelist_title, R.id.notelist_date});
        this.mListViewBinder = new NotesListViewBinder(this);
        this.mAdapter.setViewBinder(this.mListViewBinder);
        ((TextView) findViewById(R.id.title_left_text)).setText(R.string.title_notes_list);
        this.mListView = getListView();
        registerForContextMenu(this.mListView);
        LinearLayout addNoteEmptyRow = (LinearLayout) findViewById(R.id.add_note);
        if (addNoteEmptyRow != null) {
            addNoteEmptyRow.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Config.V) {
                        Log.v(NotesList.TAG, ".addNoteEmptyRow.onClick: ");
                    }
                    NotesList.this.startActivity(new Intent("android.intent.action.INSERT", FliqNotes.Notes.CONTENT_URI));
                }
            });
            addNoteEmptyRow.setFocusable(true);
            addNoteEmptyRow.setFocusableInTouchMode(true);
        }
        setListAdapter(this.mAdapter);
        this.mTitleBarNoteCount = (TextView) findViewById(R.id.title_right_text);
        this.mListView.requestFocus();
        RatingReminder.onAppStartup(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (FliqNotesApp.getNotesApplicationContext().getRestartStatus()) {
            FliqNotesApp.getNotesApplicationContext().setRestartStatus(false);
            FliqNotesApp.resetPasswordLockRequired();
            finish();
        }
        this.mCurrentTopOfScrollList = this.mListView.getFirstVisiblePosition();
    }

    private void showWhatsNew() {
        if (Integer.parseInt(Build.VERSION.SDK) >= NOTE_MODIFICATION_DATE_COLUMN_INDEX) {
            int prevBuild = Prefs.getBuildNumber(this);
            int currBuild = 0;
            try {
                currBuild = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
            }
            if (prevBuild < currBuild) {
                Prefs.setBuildNumber(this, currBuild);
                startActivity(new Intent(this, WhatsNew.class));
            }
            if (Config.V) {
                Log.v(TAG, ".showWhatsNew build " + prevBuild + " to " + currBuild);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (Config.V) {
            Log.v(TAG, ".onResume");
        }
        if (FliqNotesApp.isPasswordLockRequired()) {
            if (Config.D) {
                Log.d(TAG, ".onResume - we need to check password");
            }
            startActivityForResult(new Intent().setClass(this, PasswordProtect.class).putExtra(PasswordProtect.ACTION_KEY, PasswordProtect.PASSWORD_VALIDATE), 1);
        }
        this.mListView.setSelection(this.mCurrentTopOfScrollList);
        if (!(this.mTitleBarNoteCount == null || this.mCursor == null)) {
            this.mTitleBarNoteCount.setText(String.valueOf(this.mCursor.getCount()) + " " + ((Object) getResources().getText(R.string.notes)));
        }
        ((TextView) findViewById(R.id.title_add_note)).setTextSize(Float.parseFloat(Prefs.getListTextSize(this)));
        showWhatsNew();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (Config.V) {
            Log.v(TAG, ".onCreateOptionsMenu");
        }
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (Config.V) {
            Log.v(TAG, ".onPrepareOptionsMenu id=" + getSelectedItemId());
        }
        if (Config.V) {
            Log.v(TAG, ".onPrepareOptionsMenu position=" + this.mCurrentPosition);
        }
        if (getSelectedItemId() >= 0) {
            if (menu.findItem(R.id.menu_edit_note) == null) {
                menu.add(0, (int) R.id.menu_edit_note, 2, (int) R.string.menu_edit_note).setShortcut('0', 'e').setIcon(17301566);
            }
            if (menu.findItem(R.id.menu_edit_title) == null) {
                menu.add(0, (int) R.id.menu_edit_title, 3, (int) R.string.menu_edit_title).setShortcut('0', 't').setIcon(17301566);
            }
            if (menu.findItem(R.id.menu_delete_note) == null) {
                menu.add(0, (int) R.id.menu_delete_note, 3, (int) R.string.menu_delete_note).setShortcut('0', 't').setIcon(17301564);
            }
            menu.removeItem(R.id.menu_add_note);
            return true;
        }
        if (menu.findItem(R.id.menu_add_note) == null) {
            menu.add(0, (int) R.id.menu_add_note, 1, (int) R.string.menu_add_note).setShortcut('0', 'e').setIcon(17301555);
        }
        menu.removeItem(R.id.menu_edit_note);
        menu.removeItem(R.id.menu_edit_title);
        menu.removeItem(R.id.menu_delete_note);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_note /*2131296307*/:
                startActivity(new Intent("android.intent.action.INSERT", FliqNotes.Notes.CONTENT_URI));
                return true;
            case R.id.menu_edit_note /*2131296308*/:
                startActivity(new Intent("android.intent.action.EDIT", ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, getSelectedItemId())));
                return true;
            case R.id.menu_edit_title /*2131296309*/:
                NoteEditor.editTitleDialog(this, ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, getSelectedItemId()), new Runnable() {
                    public void run() {
                        NotesList.this.getContentResolver().notifyChange(FliqNotes.Categories.JOINED_CONTENT_URI, null);
                    }
                });
                return true;
            case R.id.menu_delete_note /*2131296310*/:
                deleteNoteAtPositionInList(getSelectedItemPosition());
                return true;
            case R.id.menu_sort /*2131296311*/:
                chooseSorting();
                return true;
            case R.id.menu_sync /*2131296312*/:
                Intent msIntent = new Intent();
                msIntent.setComponent(new ComponentName("com.markspace.missingsync", "com.markspace.missingsync.MissingSync"));
                try {
                    startActivity(msIntent);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(this, NoMissingSyncActivity.class));
                }
                return true;
            case R.id.menu_preferences /*2131296313*/:
                startActivity(new Intent(this, Prefs.class));
                return true;
            case R.id.menu_other_apps /*2131296314*/:
                FliqNotesApp.fireOtherAppsIntent(this);
                return true;
            case R.id.menu_about /*2131296315*/:
                startActivity(new Intent(this, About.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        if (Config.V) {
            Log.v(TAG, ".onCreateContextMenu");
        }
        try {
            Cursor cursor = (Cursor) getListAdapter().getItem(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
            if (cursor != null) {
                String titleString = cursor.getString(cursor.getColumnIndex("title"));
                if (titleString == null || titleString.length() <= 0) {
                    titleString = cursor.getString(cursor.getColumnIndex(FliqNotes.Notes.BODY));
                }
                if (titleString == null || titleString.length() <= 0) {
                    menu.setHeaderTitle((int) R.string.note_title_untitled);
                } else {
                    menu.setHeaderTitle(titleString);
                }
                menu.add(0, 1, 0, (int) R.string.menu_edit);
                menu.add(0, 3, 0, (int) R.string.menu_edit_title);
                menu.add(0, 2, 0, (int) R.string.menu_delete);
                menu.add(0, (int) MENU_ITEM_SEND_NOTE, 0, (int) R.string.menu_send_note);
            }
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (Config.V) {
            Log.v(TAG, ".onContextItemSelected");
        }
        try {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            switch (item.getItemId()) {
                case 1:
                    mCurrentNoteIdIndex = info.position;
                    startActivity(new Intent("android.intent.action.EDIT", ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, info.id)));
                    break;
                case 2:
                    deleteNoteAtPositionInList(info.position);
                    break;
                case 3:
                    NoteEditor.editTitleDialog(this, ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, info.id), new Runnable() {
                        public void run() {
                            NotesList.this.getContentResolver().notifyChange(FliqNotes.Categories.JOINED_CONTENT_URI, null);
                        }
                    });
                    break;
                case MENU_ITEM_SEND_NOTE /*4*/:
                    Cursor c = managedQuery(FliqNotes.Notes.CONTENT_URI, new String[]{"title", FliqNotes.Notes.BODY}, "_id=" + info.id, null, null);
                    if (c != null && c.moveToFirst()) {
                        Intent emailIntent = new Intent("android.intent.action.SEND");
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra("android.intent.extra.SUBJECT", String.valueOf(getResources().getString(R.string.fliq_note)) + c.getString(0));
                        emailIntent.putExtra("android.intent.extra.TEXT", c.getString(1));
                        startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.send_note)));
                        break;
                    }
            }
            return false;
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (Config.V) {
            Log.v(TAG, ".onListItemClick: position=" + position);
        }
        if (id < 0) {
            startActivity(new Intent("android.intent.action.INSERT", FliqNotes.Notes.CONTENT_URI));
            return;
        }
        Uri uri = ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, id);
        if ("android.intent.action.PICK".equals(getIntent().getAction())) {
            setResult(-1, new Intent().setData(uri));
            return;
        }
        mCurrentNoteIdIndex = position;
        startActivity(new Intent("android.intent.action.EDIT", uri));
    }

    private void chooseSorting() {
        if (Config.V) {
            Log.v(TAG, ".chooseSorting");
        }
        final int selected = pSortSetting;
        new AlertDialog.Builder(this).setTitle((int) R.string.dialog_sort).setSingleChoiceItems((int) R.array.dialog_sort_options, selected, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int which) {
                switch (which) {
                    case 0:
                        NotesList.pSortSetting = 0;
                        break;
                    case 1:
                        NotesList.pSortSetting = 1;
                        break;
                    case 2:
                        NotesList.pSortSetting = 2;
                        break;
                    case 3:
                        NotesList.pSortSetting = 3;
                        break;
                }
                d.dismiss();
                if (selected != NotesList.pSortSetting) {
                    if (Config.V) {
                        Log.v(NotesList.TAG, ".chooseSorting changed from " + selected + " to " + NotesList.pSortSetting);
                    }
                    NotesList.this.saveSettings();
                    FliqNotesApp.getNotesApplicationContext().setRestartStatus(true);
                    Intent i = NotesList.this.getIntent();
                    i.addFlags(268435456);
                    i.addFlags(134217728);
                    NotesList.this.startActivity(NotesList.this.getIntent());
                }
            }
        }).create().show();
    }

    private void loadSettings() {
        pSortSetting = this.mSettings.getInt("sortSetting", 0);
    }

    /* access modifiers changed from: private */
    public void saveSettings() {
        SharedPreferences.Editor editor = this.mSettings.edit();
        editor.putInt("sortSetting", pSortSetting);
        editor.commit();
    }

    private void buildIdArrayFromCursor(Cursor c) {
        if (c.getCount() > 0) {
            c.moveToFirst();
            mCurrentNoteIdList = new int[c.getCount()];
            int i = 0;
            while (true) {
                try {
                    int i2 = i;
                    i = i2 + 1;
                    try {
                        mCurrentNoteIdList[i2] = c.getInt(c.getColumnIndex("_id"));
                        if (!c.moveToNext()) {
                            c.moveToFirst();
                            return;
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                    }
                } catch (ArrayIndexOutOfBoundsException e2) {
                    try {
                        Log.w(TAG, "cursor iterated more rows than count reported");
                        c.moveToFirst();
                        return;
                    } catch (Throwable th) {
                        th = th;
                        c.moveToFirst();
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    c.moveToFirst();
                    throw th;
                }
            }
        } else {
            mCurrentNoteIdIndex = -1;
        }
    }

    private String getSortString() {
        switch (pSortSetting) {
            case 1:
                return "creationDate DESC,COALESCE(NULLIF(notes.title,''),substr(notes.noteBody,0,40))";
            case 2:
                return "modificationDate DESC,COALESCE(NULLIF(notes.title,''),substr(notes.noteBody,0,40))";
            case 3:
                return "category_name,COALESCE(NULLIF(notes.title,''),substr(notes.noteBody,0,40))";
            default:
                return "COALESCE(NULLIF(notes.title,''),substr(notes.noteBody,0,40))";
        }
    }

    private void deleteNoteAtPositionInList(final int position) {
        Cursor cursor = (Cursor) this.mAdapter.getItem(position);
        String titleString = cursor.getString(NOTE_TITLE_COLUMN_INDEX);
        if (titleString == null || titleString.length() <= 0) {
            titleString = FliqNotes.titleFromNoteBody(cursor.getString(2));
        }
        new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.menu_delete_note).setMessage("'" + titleString + "' " + getResources().getString(R.string.dialog_delete)).setCancelable(true).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setPositiveButton((int) R.string.menu_delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Cursor cursor = (Cursor) NotesList.this.mAdapter.getItem(position);
                if (Config.D) {
                    Log.d(NotesList.TAG, ".deleteNoteFromPosition - deleting note from position: " + cursor.getString(cursor.getColumnIndex("uuid")));
                }
                ContentValues values = new ContentValues();
                values.put("uuid", cursor.getString(cursor.getColumnIndex("uuid")));
                NotesList.this.getContentResolver().delete(FliqNotes.Modifies.CONTENT_URI, "uuid=?", new String[]{cursor.getString(cursor.getColumnIndex("uuid"))});
                NotesList.this.getContentResolver().insert(FliqNotes.Deletes.CONTENT_URI, values);
                NotesList.this.getContentResolver().delete(ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, NotesList.this.mAdapter.getItemId(position)), null, null);
                NotesList.this.getContentResolver().notifyChange(FliqNotes.Categories.JOINED_CONTENT_URI, null);
            }
        }).show();
    }

    private final class NoteItemListAdapter extends SimpleCursorAdapter implements SectionIndexer {
        private String mAlphabet;
        private SectionIndexer mIndexer;
        private int[] mSectionPositions;

        public NoteItemListAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
            super(context, layout, c, from, to);
            if (Config.V) {
                Log.v(NotesList.TAG, ".NoteItemListAdapter init");
            }
            this.mAlphabet = context.getString(R.string.fast_scroll_numeric_alphabet);
            updateIndexer(c);
        }

        public void changeCursor(Cursor cursor) {
            super.changeCursor(cursor);
            updateIndexer(cursor);
        }

        public Object[] getSections() {
            return this.mIndexer.getSections();
        }

        public int getPositionForSection(int sectionIndex) {
            if (sectionIndex < 0 || sectionIndex >= this.mSectionPositions.length) {
                return -1;
            }
            if (this.mIndexer == null) {
                Cursor cursor = NotesList.this.mAdapter.getCursor();
                if (cursor == null) {
                    return 0;
                }
                this.mIndexer = getNewIndexer(cursor);
            }
            int position = this.mSectionPositions[sectionIndex];
            if (position == -1) {
                int[] iArr = this.mSectionPositions;
                position = this.mIndexer.getPositionForSection(sectionIndex);
                iArr[sectionIndex] = position;
            }
            return position;
        }

        public int getSectionForPosition(int position) {
            int start = 0;
            int end = this.mSectionPositions.length;
            while (start != end) {
                int pivot = start + ((end - start) / NotesList.MENU_ITEM_SEND_NOTE);
                if (getPositionForSection(pivot) <= position) {
                    start = pivot + 1;
                } else {
                    end = pivot;
                }
            }
            return start - 1;
        }

        private SectionIndexer getNewIndexer(Cursor cursor) {
            switch (NotesList.pSortSetting) {
                case 1:
                    return new NoteDateIndexer(cursor, 3);
                case 2:
                    return new NoteDateIndexer(cursor, NotesList.NOTE_MODIFICATION_DATE_COLUMN_INDEX);
                case 3:
                    return new NoteCategoryIndexer(cursor, NotesList.NOTE_CATEGORY_COLUMN_INDEX);
                default:
                    return new AlphabetIndexer(cursor, NotesList.NOTE_TITLE_COLUMN_INDEX, this.mAlphabet);
            }
        }

        private void updateIndexer(Cursor cursor) {
            this.mIndexer = getNewIndexer(cursor);
            int sectionCount = this.mIndexer.getSections().length;
            if (this.mSectionPositions == null || this.mSectionPositions.length != sectionCount) {
                this.mSectionPositions = new int[sectionCount];
            }
            for (int i = 0; i < sectionCount; i++) {
                this.mSectionPositions[i] = -1;
            }
            if (Config.V) {
                Log.v(NotesList.TAG, ".updateIndexer - sectionCount=" + sectionCount);
            }
        }
    }

    private final class NoteCategoryIndexer extends DataSetObserver implements SectionIndexer {
        protected String[] mCategoryArray;
        private int mCategoryLength;
        private SparseIntArray mCategoryMap;
        private Collator mCollator;
        protected int mColumnIndex;
        protected Cursor mDataCursor;

        public NoteCategoryIndexer(Cursor cursor, int noteCategoryColumnIndex) {
            if (Config.V) {
                Log.v(NotesList.TAG, ".NoteCategoryIndexer init");
            }
            this.mDataCursor = cursor;
            this.mColumnIndex = noteCategoryColumnIndex;
            Cursor c = NotesList.this.managedQuery(FliqNotes.Categories.CONTENT_URI, new String[]{"category"}, null, null, "category COLLATE NOCASE");
            if (c != null && c.moveToFirst()) {
                this.mCategoryLength = c.getCount();
                this.mCategoryArray = new String[(this.mCategoryLength + 1)];
                for (int i = 0; i < this.mCategoryLength; i++) {
                    if (Config.V) {
                        Log.v(NotesList.TAG, ".NoteCategoryIndexer.init adding category=" + c.getString(0));
                    }
                    this.mCategoryArray[i] = c.getString(0);
                    c.moveToNext();
                }
            }
            if (this.mCategoryArray != null) {
                this.mCategoryArray[this.mCategoryLength] = FliqNotesApp.getNotesApplicationContext().getResources().getString(R.string.unfiled);
                Arrays.sort(this.mCategoryArray);
                this.mCategoryMap = new SparseIntArray(this.mCategoryLength);
            }
            if (cursor != null) {
                cursor.registerDataSetObserver(this);
            }
            this.mCollator = Collator.getInstance();
            this.mCollator.setStrength(0);
        }

        public Object[] getSections() {
            if (Config.V) {
                Log.v(NotesList.TAG, ".NoteCategoryIndexer.getSections");
            }
            return this.mCategoryArray;
        }

        public int getPositionForSection(int sectionIndex) {
            int prevCategoryPos;
            if (Config.V) {
                Log.v(NotesList.TAG, ".NoteCategoryIndexer.getPositionForSection sectionIndex=" + sectionIndex);
            }
            SparseIntArray categoryMap = this.mCategoryMap;
            Cursor cursor = this.mDataCursor;
            if (cursor == null || this.mCategoryArray == null || sectionIndex <= 0) {
                return 0;
            }
            if (sectionIndex >= this.mCategoryLength) {
                sectionIndex = this.mCategoryLength - 1;
            }
            int savedCursorPos = cursor.getPosition();
            int count = cursor.getCount();
            int start = 0;
            int end = count;
            String targetCategory = this.mCategoryArray[sectionIndex];
            if (Config.V) {
                Log.v(NotesList.TAG, ".NoteCategoryIndexer.getPositionForSection targetCategory=" + targetCategory);
            }
            int pos = categoryMap.get(sectionIndex, Integer.MIN_VALUE);
            if (Integer.MIN_VALUE != pos) {
                if (Config.V) {
                    Log.v(NotesList.TAG, ".NoteCategoryIndexer.getPositionForSection cached pos=" + pos);
                }
                return pos;
            }
            if (sectionIndex > 0 && (prevCategoryPos = categoryMap.get(sectionIndex - 1, Integer.MIN_VALUE)) != Integer.MIN_VALUE) {
                start = Math.abs(prevCategoryPos);
            }
            if (Config.V) {
                Log.v(NotesList.TAG, ".NoteCategoryIndexer.getPositionForSection start=" + start + " end=" + end);
            }
            int pos2 = (end + start) / 2;
            while (true) {
                if (pos2 >= end) {
                    break;
                }
                cursor.moveToPosition(pos2);
                String curCategory = cursor.getString(this.mColumnIndex);
                if (curCategory == null) {
                    if (pos2 == 0) {
                        break;
                    }
                    pos2--;
                } else {
                    int diff = this.mCollator.compare(curCategory, targetCategory);
                    if (diff == 0) {
                        if (start == pos2) {
                            break;
                        }
                        end = pos2;
                    } else if (diff < 0) {
                        start = pos2 + 1;
                        if (start >= count) {
                            pos2 = count;
                            break;
                        }
                    } else {
                        end = pos2;
                    }
                    pos2 = (start + end) / 2;
                }
            }
            categoryMap.put(sectionIndex, pos2);
            cursor.moveToPosition(savedCursorPos);
            if (Config.V) {
                Log.v(NotesList.TAG, ".NoteCategoryIndexer.getPositionForSection pos=" + pos2);
            }
            return pos2;
        }

        public int getSectionForPosition(int position) {
            if (Config.V) {
                Log.v(NotesList.TAG, "NoteCategoryIndexer.getSectionForPosition");
            }
            int savedCursorPos = this.mDataCursor.getPosition();
            this.mDataCursor.moveToPosition(position);
            String curCategory = this.mDataCursor.getString(this.mColumnIndex);
            this.mDataCursor.moveToPosition(savedCursorPos);
            for (int i = 0; i < this.mCategoryLength; i++) {
                if (this.mCollator.compare(curCategory, this.mCategoryArray[i]) == 0) {
                    return i;
                }
            }
            return 0;
        }

        public void onChanged() {
            super.onChanged();
            this.mCategoryMap.clear();
        }

        public void onInvalidated() {
            super.onInvalidated();
            this.mCategoryMap.clear();
        }
    }

    private final class NoteDateIndexer implements SectionIndexer {
        public NoteDateIndexer(Cursor cursor, int noteCategoryColumnIndex) {
        }

        public int getPositionForSection(int section) {
            return 0;
        }

        public int getSectionForPosition(int position) {
            return 0;
        }

        public Object[] getSections() {
            return new String[]{" "};
        }
    }

    /* access modifiers changed from: protected */
    public void onCursorContentChanged() {
        if (Config.V) {
            Log.e(TAG, ".onContentChanged");
        }
        if (this.mCursor != null) {
            this.mCursor.requery();
            buildIdArrayFromCursor(this.mCursor);
            if (this.mTitleBarNoteCount != null) {
                this.mTitleBarNoteCount.setText(String.valueOf(this.mCursor.getCount()) + " " + ((Object) getResources().getText(R.string.notes)));
            }
        }
    }

    private class ChangeObserver extends ContentObserver {
        public ChangeObserver() {
            super(new Handler());
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean selfChange) {
            NotesList.this.onCursorContentChanged();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mCursor != null) {
            this.mCursor.unregisterContentObserver(this.mChangeObserver);
            this.mCursor.close();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Config.V) {
            Log.v(TAG, ".onActivityResult requestCode=" + requestCode + " resultCode=" + resultCode);
        }
        if (requestCode == 1 && resultCode == -1) {
            if (Config.D) {
                Log.d(TAG, ".onActivityResult password protect passed");
            }
            FliqNotesApp.resetPasswordLockRequired();
            return;
        }
        finish();
    }
}
