package com.markspace.provider;

import android.net.Uri;
import android.provider.BaseColumns;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.TimeZone;

public class FliqNotes {
    public static final String AUTHORITY = "com.markspace.provider.fliqnotes";

    private FliqNotes() {
    }

    public static final class Notes implements BaseColumns {
        public static final String AUTOGENERATE = "autogenerate";
        public static final String BODY = "noteBody";
        public static final String CATEGORY = "category";
        public static final String CATEGORY_COLOR = "category_color";
        public static final String CATEGORY_NAME = "category_name";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.markspace.note";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.markspace.note";
        public static final Uri CONTENT_URI = Uri.parse("content://com.markspace.provider.fliqnotes/notes");
        public static final String CREATION_DATE = "creationDate";
        public static final String DEFAULT_SORT_ORDER = "title";
        public static final String MODIFICATION_DATE = "modificationDate";
        public static final String TITLE = "title";
        public static final String TYPE = "type";
        public static final String UUID = "uuid";

        private Notes() {
        }
    }

    public static final class Categories implements BaseColumns {
        public static final String COLOR = "color";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.markspace.category";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.markspace.category";
        public static final Uri CONTENT_URI = Uri.parse("content://com.markspace.provider.fliqnotes/categories");
        public static final String DEFAULT_SORT_ORDER = "category";
        public static final Uri JOINED_CONTENT_URI = Uri.parse("content://com.markspace.provider.fliqnotes/notes_categories");
        public static final String NAME = "category";
        public static final String UNFILED_CATEGORY_UUID = "0";
        public static final String USERORDER = "userOrder";
        public static final String UUID = "uuid";

        private Categories() {
        }

        public static String getDefaultCategoryColor(int calendarId) {
            switch (calendarId % 9) {
                case 1:
                    return "#cd4573";
                case 2:
                    return "#d67800";
                case 3:
                    return "#3563bf";
                case 4:
                    return "#259c8d";
                case 5:
                    return "#6432bd";
                case 6:
                    return "#2c5b8c";
                case 7:
                    return "#2b855a";
                case 8:
                    return "#0e8a16";
                default:
                    return "#609c00";
            }
        }
    }

    public static final class Adds implements BaseColumns {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.markspace.add";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.markspace.add";
        public static final Uri CONTENT_URI = Uri.parse("content://com.markspace.provider.fliqnotes/adds");
        public static final String DEFAULT_SORT_ORDER = "uuid DESC";
        public static final String UUID = "uuid";

        private Adds() {
        }
    }

    public static final class Modifies implements BaseColumns {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.markspace.modify";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.markspace.modify";
        public static final Uri CONTENT_URI = Uri.parse("content://com.markspace.provider.fliqnotes/modifies");
        public static final String DEFAULT_SORT_ORDER = "uuid DESC";
        public static final String UUID = "uuid";

        private Modifies() {
        }
    }

    public static final class Deletes implements BaseColumns {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.markspace.delete";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.markspace.delete";
        public static final Uri CONTENT_URI = Uri.parse("content://com.markspace.provider.fliqnotes/deletes");
        public static final String DEFAULT_SORT_ORDER = "uuid DESC";
        public static final String UUID = "uuid";

        private Deletes() {
        }
    }

    public static final class Versioning implements BaseColumns {
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.markspace.version";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.markspace.version";
        public static final Uri CONTENT_URI = Uri.parse("content://com.markspace.provider.fliqnotes/versioning");
        public static final String DEFAULT_SORT_ORDER = "version DESC";
        public static final String VERSION = "version";

        private Versioning() {
        }
    }

    public static String convertTimeMillisecondsToProtocolString(long milliseconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(new Date(milliseconds));
    }

    public static String convertTimeMillisecondsToSimpleMonthDay(long milliseconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd");
        formatter.setTimeZone(TimeZone.getDefault());
        return formatter.format(new Date(milliseconds));
    }

    public static long convertProtocolStringToTimeMilliseconds(String timeString) {
        Date theDate;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            theDate = formatter.parse(timeString);
        } catch (ParseException e) {
            theDate = null;
        }
        if (theDate != null) {
            return theDate.getTime();
        }
        return 0;
    }

    public static String titleFromNoteBody(String noteBody) {
        String title;
        try {
            title = new Scanner(noteBody).nextLine();
        } catch (NoSuchElementException e) {
            title = noteBody;
        }
        if (title.length() > 40) {
            return title.substring(0, 40);
        }
        return title;
    }
}
