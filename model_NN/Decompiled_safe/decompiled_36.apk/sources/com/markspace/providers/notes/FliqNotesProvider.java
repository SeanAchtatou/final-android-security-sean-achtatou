package com.markspace.providers.notes;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.markspace.fliqnotes.FliqNotesApp;
import com.markspace.fliqnotes.Prefs;
import com.markspace.fliqnotes.R;
import com.markspace.provider.FliqNotes;
import com.markspace.test.Config;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class FliqNotesProvider extends ContentProvider {
    private static final int ADDS = 5;
    private static final String ADDS_TABLE_NAME = "adds";
    private static final int ADD_ID = 6;
    private static final int CATEGORIES = 3;
    protected static final String CATEGORIES_TABLE_NAME = "categories";
    private static final int CATEGORY_ID = 4;
    private static final String DATABASE_NAME = "notes.sqlite";
    private static final int DATABASE_VERSION = 2;
    private static final int DELETES = 9;
    private static final String DELETES_TABLE_NAME = "deletes";
    private static final int DELETE_ID = 10;
    private static final int MODIFIES = 7;
    private static final String MODIFIES_TABLE_NAME = "modifies";
    private static final int MODIFY_ID = 8;
    private static final int NOTES = 1;
    private static final int NOTES_CATEGORIES = 11;
    protected static final String NOTES_TABLE_NAME = "notes";
    private static final int NOTE_ID = 2;
    private static final int SEARCH_SHORTCUT = 12002;
    private static final int SEARCH_SUGGESTIONS = 12001;
    private static final String TAG = "FliqNotesProvider";
    private static final String VERSIONING_TABLE_NAME = "versioning";
    private static DatabaseHelper mOpenHelper;
    private static HashMap<String, String> sAddsProjectionMap = new HashMap<>();
    private static HashMap<String, String> sCategoriesProjectionMap = new HashMap<>();
    private static HashMap<String, String> sDeletesProjectionMap = new HashMap<>();
    private static HashMap<String, String> sModifiesProjectionMap = new HashMap<>();
    private static HashMap<String, String> sNotesCategoriesProjectionMap = new HashMap<>();
    private static HashMap<String, String> sNotesProjectionMap = new HashMap<>();
    private static final String[] sSearchSuggestionsProjection;
    private static final UriMatcher sUriMatcher = new UriMatcher(-1);

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, FliqNotesProvider.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        public void onCreate(SQLiteDatabase db) {
            if (Config.V) {
                Log.v(FliqNotesProvider.TAG, "DatabaseHelper.onCreate");
            }
            db.execSQL("CREATE TABLE IF NOT EXISTS notes (_id INTEGER PRIMARY KEY AUTOINCREMENT,uuid TEXT,title TEXT,noteBody TEXT,creationDate TEXT,modificationDate TEXT,type TEXT,category TEXT,autogenerate INTEGER);");
            Long now = Long.valueOf(System.currentTimeMillis());
            ContentValues values = new ContentValues();
            values.clear();
            values.put("uuid", UUID.randomUUID().toString());
            values.put(FliqNotes.Notes.CREATION_DATE, FliqNotes.convertTimeMillisecondsToProtocolString(now.longValue()));
            values.put(FliqNotes.Notes.MODIFICATION_DATE, FliqNotes.convertTimeMillisecondsToProtocolString(now.longValue()));
            values.put(FliqNotes.Notes.TYPE, "plain text");
            values.put("title", "More Fliq products");
            values.put(FliqNotes.Notes.BODY, "More Fliq products\nFrom Mark/Space\n\n- Fliq Tasks\n- Fliq for iPhone\n- Fliq for Mac\n- Fliq for PC\n\nLearn more at getfliq.com");
            values.put("category", FliqNotes.Categories.UNFILED_CATEGORY_UUID);
            values.put(FliqNotes.Notes.AUTOGENERATE, (Integer) 0);
            long row_id = db.insert(FliqNotesProvider.NOTES_TABLE_NAME, null, values);
            if (Config.V) {
                Log.v(FliqNotesProvider.TAG, "DatabaseHelper.onCreate added default note w/id=" + row_id);
            }
            db.execSQL("CREATE TABLE  IF NOT EXISTS categories (_id INTEGER PRIMARY KEY AUTOINCREMENT,uuid TEXT,category TEXT,color TEXT,userOrder INTEGER);");
            values.clear();
            values.put("uuid", UUID.randomUUID().toString());
            values.put("category", "Work");
            values.put(FliqNotes.Categories.COLOR, "#0000aa");
            values.put(FliqNotes.Categories.USERORDER, Integer.valueOf((int) FliqNotesProvider.NOTES));
            db.insert(FliqNotesProvider.CATEGORIES_TABLE_NAME, null, values);
            values.clear();
            values.put("uuid", UUID.randomUUID().toString());
            values.put("category", "Home");
            values.put(FliqNotes.Categories.COLOR, "#00aa00");
            values.put(FliqNotes.Categories.USERORDER, (Integer) 2);
            db.insert(FliqNotesProvider.CATEGORIES_TABLE_NAME, null, values);
            values.clear();
            values.put("uuid", UUID.randomUUID().toString());
            values.put("category", "School");
            values.put(FliqNotes.Categories.COLOR, "#aa0000");
            values.put(FliqNotes.Categories.USERORDER, Integer.valueOf((int) FliqNotesProvider.CATEGORIES));
            db.insert(FliqNotesProvider.CATEGORIES_TABLE_NAME, null, values);
            db.execSQL("CREATE TABLE IF NOT EXISTS adds (_id INTEGER PRIMARY KEY,uuid TEXT UNIQUE);");
            db.execSQL("CREATE TABLE IF NOT EXISTS modifies (_id INTEGER PRIMARY KEY,uuid TEXT UNIQUE);");
            db.execSQL("CREATE TABLE IF NOT EXISTS deletes (_id INTEGER PRIMARY KEY,uuid TEXT UNIQUE);");
            db.execSQL("CREATE TABLE IF NOT EXISTS versioning (_id INTEGER PRIMARY KEY,version INTEGER);");
            values.clear();
            values.put(FliqNotes.Versioning.VERSION, (Integer) 2);
            db.insert(FliqNotesProvider.VERSIONING_TABLE_NAME, null, values);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(FliqNotesProvider.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);
            if (oldVersion == FliqNotesProvider.NOTES) {
                String[] strArr = new String[FliqNotesProvider.CATEGORIES];
                strArr[0] = "_id";
                strArr[FliqNotesProvider.NOTES] = FliqNotes.Categories.COLOR;
                strArr[2] = "category";
                Cursor c = db.query(true, FliqNotesProvider.CATEGORIES_TABLE_NAME, strArr, null, null, null, null, null, null);
                ContentValues v = new ContentValues();
                if (c.moveToFirst()) {
                    do {
                        Log.w(FliqNotesProvider.TAG, "coverting color  " + c.getString(2) + " from " + c.getString(FliqNotesProvider.NOTES) + " to " + FliqNotes.Categories.getDefaultCategoryColor(c.getInt(0)));
                        v.clear();
                        v.put(FliqNotes.Categories.COLOR, FliqNotes.Categories.getDefaultCategoryColor(c.getInt(0)));
                        String[] strArr2 = new String[FliqNotesProvider.NOTES];
                        strArr2[0] = c.getString(0);
                        db.update(FliqNotesProvider.CATEGORIES_TABLE_NAME, v, "_id=?", strArr2);
                    } while (c.moveToNext());
                }
                c.close();
                int oldVersion2 = oldVersion + FliqNotesProvider.NOTES;
            }
        }
    }

    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        if (Config.V) {
            Log.v(TAG, ".onCreate");
        }
        if (sNotesCategoriesProjectionMap.get(FliqNotes.Notes.CATEGORY_NAME) == null) {
            sNotesCategoriesProjectionMap.put(FliqNotes.Notes.CATEGORY_NAME, "IFNULL(categories.category,'" + getContext().getResources().getString(R.string.unfiled) + "') AS category_name");
        }
        if (sNotesCategoriesProjectionMap.get(FliqNotes.Notes.CATEGORY_COLOR) != null) {
            return true;
        }
        sNotesCategoriesProjectionMap.put(FliqNotes.Notes.CATEGORY_COLOR, "IFNULL(NULLIF(categories.color,''),'#aaaaaa') AS category_color");
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (Config.V) {
            Log.v(TAG, ".query uri=" + uri.toString() + " match=" + sUriMatcher.match(uri));
        }
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String orderBy = sortOrder;
        switch (sUriMatcher.match(uri)) {
            case NOTES /*1*/:
                qb.setTables(NOTES_TABLE_NAME);
                qb.setProjectionMap(sNotesProjectionMap);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "title";
                    break;
                }
                break;
            case 2:
                qb.setTables(NOTES_TABLE_NAME);
                qb.setProjectionMap(sNotesProjectionMap);
                qb.appendWhere("_id=" + uri.getPathSegments().get(NOTES));
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "title";
                    break;
                }
                break;
            case CATEGORIES /*3*/:
                qb.setTables(CATEGORIES_TABLE_NAME);
                qb.setProjectionMap(sCategoriesProjectionMap);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "category";
                    break;
                }
                break;
            case CATEGORY_ID /*4*/:
                qb.setTables(CATEGORIES_TABLE_NAME);
                qb.setProjectionMap(sCategoriesProjectionMap);
                qb.appendWhere("_id=" + uri.getPathSegments().get(NOTES));
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "category";
                    break;
                }
                break;
            case ADDS /*5*/:
                qb.setTables(ADDS_TABLE_NAME);
                qb.setProjectionMap(sAddsProjectionMap);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "uuid DESC";
                    break;
                }
                break;
            case ADD_ID /*6*/:
                qb.setTables(ADDS_TABLE_NAME);
                qb.setProjectionMap(sAddsProjectionMap);
                qb.appendWhere("_id=" + uri.getPathSegments().get(NOTES));
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "uuid DESC";
                    break;
                }
                break;
            case MODIFIES /*7*/:
                qb.setTables(MODIFIES_TABLE_NAME);
                qb.setProjectionMap(sModifiesProjectionMap);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "uuid DESC";
                    break;
                }
                break;
            case MODIFY_ID /*8*/:
                qb.setTables(MODIFIES_TABLE_NAME);
                qb.setProjectionMap(sModifiesProjectionMap);
                qb.appendWhere("_id=" + uri.getPathSegments().get(NOTES));
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "uuid DESC";
                    break;
                }
                break;
            case DELETES /*9*/:
                qb.setTables(DELETES_TABLE_NAME);
                qb.setProjectionMap(sDeletesProjectionMap);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "uuid DESC";
                    break;
                }
                break;
            case DELETE_ID /*10*/:
                qb.setTables(DELETES_TABLE_NAME);
                qb.setProjectionMap(sDeletesProjectionMap);
                qb.appendWhere("_id=" + uri.getPathSegments().get(NOTES));
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "uuid DESC";
                    break;
                }
                break;
            case NOTES_CATEGORIES /*11*/:
                qb.setTables("notes LEFT JOIN categories ON notes.category = categories.uuid");
                qb.setProjectionMap(sNotesCategoriesProjectionMap);
                if (TextUtils.isEmpty(sortOrder)) {
                    orderBy = "title";
                    break;
                }
                break;
            case SEARCH_SUGGESTIONS /*12001*/:
                if (Prefs.getPasswordRequired(FliqNotesApp.sContext)) {
                    return null;
                }
                qb.setTables(NOTES_TABLE_NAME);
                SQLiteDatabase sdb = mOpenHelper.getReadableDatabase();
                String query = uri.getLastPathSegment().toLowerCase();
                String[] suggestColumns = new String[ADD_ID];
                suggestColumns[0] = "_id";
                suggestColumns[NOTES] = "suggest_shortcut_id";
                suggestColumns[2] = "suggest_intent_data_id";
                suggestColumns[CATEGORIES] = "suggest_text_1";
                suggestColumns[CATEGORY_ID] = "suggest_text_2";
                suggestColumns[ADDS] = "suggest_icon_1";
                StringBuilder sb = new StringBuilder();
                String query2 = query.replaceAll("'", "%");
                sb.append("title");
                sb.append(" LIKE '%");
                sb.append(query2);
                sb.append("%' OR ");
                sb.append(FliqNotes.Notes.BODY);
                sb.append(" LIKE '%");
                sb.append(query2);
                sb.append("%'");
                Cursor c = qb.query(sdb, sSearchSuggestionsProjection, sb.toString(), null, null, null, null);
                if (c == null) {
                    return null;
                }
                MatrixCursor matrixCursor = new MatrixCursor(suggestColumns, c.getCount());
                if (c.moveToFirst()) {
                    String icon = String.valueOf((int) R.drawable.search_icon);
                    do {
                        ArrayList<String> suggestionList = new ArrayList<>();
                        for (int i = 0; i < c.getColumnCount(); i += NOTES) {
                            suggestionList.add(c.getString(i));
                        }
                        suggestionList.add(icon);
                        matrixCursor.addRow(suggestionList.toArray());
                    } while (c.moveToNext());
                }
                c.close();
                return matrixCursor;
            case SEARCH_SHORTCUT /*12002*/:
                String lookupKey = uri.getLastPathSegment();
                Log.e(TAG, "SEARCH SHORTCUT KEY: " + lookupKey);
                qb.setTables(NOTES_TABLE_NAME);
                SQLiteDatabase sdb2 = mOpenHelper.getReadableDatabase();
                String[] suggestColumns2 = new String[ADD_ID];
                suggestColumns2[0] = "_id";
                suggestColumns2[NOTES] = "suggest_shortcut_id";
                suggestColumns2[2] = "suggest_intent_data_id";
                suggestColumns2[CATEGORIES] = "suggest_text_1";
                suggestColumns2[CATEGORY_ID] = "suggest_text_2";
                suggestColumns2[ADDS] = "suggest_icon_1";
                StringBuilder sb2 = new StringBuilder();
                sb2.append("_id");
                sb2.append("=");
                sb2.append(lookupKey);
                Log.e(TAG, "SEARCH SHORTCUT WHERE: " + sb2.toString());
                Cursor c2 = qb.query(sdb2, sSearchSuggestionsProjection, sb2.toString(), null, null, null, null);
                if (c2 == null) {
                    return null;
                }
                MatrixCursor matrixCursor2 = new MatrixCursor(suggestColumns2, c2.getCount());
                if (c2.moveToFirst()) {
                    String icon2 = String.valueOf((int) R.drawable.search_icon);
                    while (c2.moveToNext()) {
                        ArrayList<String> suggestionList2 = new ArrayList<>();
                        for (int i2 = 0; i2 < c2.getColumnCount(); i2 += NOTES) {
                            suggestionList2.add(c2.getString(i2));
                        }
                        suggestionList2.add(icon2);
                        matrixCursor2.addRow(suggestionList2.toArray());
                    }
                    c2.close();
                }
                return matrixCursor2;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        Cursor c3 = qb.query(mOpenHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, orderBy);
        c3.setNotificationUri(getContext().getContentResolver(), uri);
        if (Config.V) {
            Log.v(TAG, ".query rows=" + c3.getCount());
        }
        return c3;
    }

    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case NOTES /*1*/:
                return FliqNotes.Notes.CONTENT_TYPE;
            case 2:
                return FliqNotes.Notes.CONTENT_ITEM_TYPE;
            case CATEGORIES /*3*/:
                return FliqNotes.Categories.CONTENT_TYPE;
            case CATEGORY_ID /*4*/:
                return FliqNotes.Categories.CONTENT_ITEM_TYPE;
            case ADDS /*5*/:
                return FliqNotes.Adds.CONTENT_TYPE;
            case ADD_ID /*6*/:
                return FliqNotes.Adds.CONTENT_ITEM_TYPE;
            case MODIFIES /*7*/:
                return FliqNotes.Modifies.CONTENT_TYPE;
            case MODIFY_ID /*8*/:
                return FliqNotes.Modifies.CONTENT_ITEM_TYPE;
            case DELETES /*9*/:
                return FliqNotes.Deletes.CONTENT_TYPE;
            case DELETE_ID /*10*/:
                return FliqNotes.Deletes.CONTENT_ITEM_TYPE;
            case SEARCH_SUGGESTIONS /*12001*/:
                return "vnd.android.cursor.dir/vnd.android.search.suggest";
            case SEARCH_SHORTCUT /*12002*/:
                return "vnd.android.cursor.item/vnd.android.search.suggest";
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues initialValues) {
        ContentValues values;
        ContentValues values2;
        ContentValues values3;
        ContentValues values4;
        ContentValues values5;
        if (Config.V) {
            Log.v(TAG, ".insert uri=" + uri.toString());
        }
        if (sUriMatcher.match(uri) == NOTES) {
            if (initialValues != null) {
                values5 = new ContentValues(initialValues);
            } else {
                values5 = new ContentValues();
            }
            Long now = Long.valueOf(System.currentTimeMillis());
            if (!values5.containsKey("uuid")) {
                throw new IllegalArgumentException("UUID value not specified uuid");
            }
            if (!values5.containsKey(FliqNotes.Notes.CREATION_DATE)) {
                values5.put(FliqNotes.Notes.CREATION_DATE, FliqNotes.convertTimeMillisecondsToProtocolString(now.longValue()));
            }
            if (!values5.containsKey(FliqNotes.Notes.MODIFICATION_DATE)) {
                values5.put(FliqNotes.Notes.MODIFICATION_DATE, FliqNotes.convertTimeMillisecondsToProtocolString(now.longValue()));
            }
            if (!values5.containsKey(FliqNotes.Notes.TYPE)) {
                throw new IllegalArgumentException("TYPE value not specified type");
            } else if (!values5.containsKey("title")) {
                throw new IllegalArgumentException("TITLE value not specified title");
            } else if (!values5.containsKey(FliqNotes.Notes.BODY)) {
                throw new IllegalArgumentException("BODY value not specified noteBody");
            } else {
                long rowId = mOpenHelper.getWritableDatabase().insert(NOTES_TABLE_NAME, null, values5);
                if (rowId > 0) {
                    Uri noteUri = ContentUris.withAppendedId(FliqNotes.Notes.CONTENT_URI, rowId);
                    getContext().getContentResolver().notifyChange(noteUri, null);
                    return noteUri;
                }
            }
        } else if (sUriMatcher.match(uri) == CATEGORIES) {
            if (initialValues != null) {
                values4 = new ContentValues(initialValues);
            } else {
                values4 = new ContentValues();
            }
            if (!values4.containsKey("uuid")) {
                throw new IllegalArgumentException("value not specified uuid");
            } else if (!values4.containsKey("category")) {
                throw new IllegalArgumentException("value not specified category");
            } else if (!values4.containsKey(FliqNotes.Categories.COLOR)) {
                throw new IllegalArgumentException("value not specified color");
            } else if (!values4.containsKey(FliqNotes.Categories.USERORDER)) {
                throw new IllegalArgumentException("value not specified userOrder");
            } else {
                long rowId2 = mOpenHelper.getWritableDatabase().insert(CATEGORIES_TABLE_NAME, null, values4);
                if (rowId2 > 0) {
                    Uri categoryUri = ContentUris.withAppendedId(FliqNotes.Categories.CONTENT_URI, rowId2);
                    getContext().getContentResolver().notifyChange(categoryUri, null);
                    return categoryUri;
                }
            }
        } else if (sUriMatcher.match(uri) == ADDS) {
            if (initialValues != null) {
                values3 = new ContentValues(initialValues);
            } else {
                values3 = new ContentValues();
            }
            if (!values3.containsKey("uuid")) {
                throw new IllegalArgumentException("value not specified uuid");
            }
            long rowId3 = mOpenHelper.getWritableDatabase().replace(ADDS_TABLE_NAME, null, values3);
            if (rowId3 > 0) {
                Uri addUri = ContentUris.withAppendedId(FliqNotes.Adds.CONTENT_URI, rowId3);
                getContext().getContentResolver().notifyChange(addUri, null);
                return addUri;
            }
        } else if (sUriMatcher.match(uri) == MODIFIES) {
            if (initialValues != null) {
                values2 = new ContentValues(initialValues);
            } else {
                values2 = new ContentValues();
            }
            if (!values2.containsKey("uuid")) {
                throw new IllegalArgumentException("value not specified uuid");
            }
            long rowId4 = mOpenHelper.getWritableDatabase().replace(MODIFIES_TABLE_NAME, null, values2);
            if (rowId4 > 0) {
                Uri modifyUri = ContentUris.withAppendedId(FliqNotes.Modifies.CONTENT_URI, rowId4);
                getContext().getContentResolver().notifyChange(modifyUri, null);
                return modifyUri;
            }
        } else if (sUriMatcher.match(uri) == DELETES) {
            if (initialValues != null) {
                values = new ContentValues(initialValues);
            } else {
                values = new ContentValues();
            }
            if (!values.containsKey("uuid")) {
                throw new IllegalArgumentException("value not specified uuid");
            }
            long rowId5 = mOpenHelper.getWritableDatabase().replace(DELETES_TABLE_NAME, null, values);
            if (rowId5 > 0) {
                Uri deletesUri = ContentUris.withAppendedId(FliqNotes.Deletes.CONTENT_URI, rowId5);
                getContext().getContentResolver().notifyChange(deletesUri, null);
                return deletesUri;
            }
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    public int delete(Uri uri, String where, String[] whereArgs) {
        int count;
        if (Config.V) {
            Log.v(TAG, ".delete uri=" + uri.toString());
        }
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case NOTES /*1*/:
                count = db.delete(NOTES_TABLE_NAME, where, whereArgs);
                break;
            case 2:
                count = db.delete(NOTES_TABLE_NAME, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case CATEGORIES /*3*/:
                count = db.delete(CATEGORIES_TABLE_NAME, where, whereArgs);
                break;
            case CATEGORY_ID /*4*/:
                count = db.delete(CATEGORIES_TABLE_NAME, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case ADDS /*5*/:
                count = db.delete(ADDS_TABLE_NAME, where, whereArgs);
                break;
            case ADD_ID /*6*/:
                count = db.delete(ADDS_TABLE_NAME, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case MODIFIES /*7*/:
                count = db.delete(MODIFIES_TABLE_NAME, where, whereArgs);
                break;
            case MODIFY_ID /*8*/:
                count = db.delete(MODIFIES_TABLE_NAME, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case DELETES /*9*/:
                count = db.delete(DELETES_TABLE_NAME, where, whereArgs);
                break;
            case DELETE_ID /*10*/:
                count = db.delete(DELETES_TABLE_NAME, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        int count;
        if (Config.V) {
            Log.v(TAG, ".update uri=" + uri.toString());
        }
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case NOTES /*1*/:
                count = db.update(NOTES_TABLE_NAME, values, where, whereArgs);
                break;
            case 2:
                count = db.update(NOTES_TABLE_NAME, values, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case CATEGORIES /*3*/:
                count = db.update(CATEGORIES_TABLE_NAME, values, where, whereArgs);
                break;
            case CATEGORY_ID /*4*/:
                count = db.update(CATEGORIES_TABLE_NAME, values, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case ADDS /*5*/:
                count = db.update(ADDS_TABLE_NAME, values, where, whereArgs);
                break;
            case ADD_ID /*6*/:
                count = db.update(ADDS_TABLE_NAME, values, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case MODIFIES /*7*/:
                count = db.update(MODIFIES_TABLE_NAME, values, where, whereArgs);
                break;
            case MODIFY_ID /*8*/:
                count = db.update(MODIFIES_TABLE_NAME, values, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            case DELETES /*9*/:
                count = db.update(DELETES_TABLE_NAME, values, where, whereArgs);
                break;
            case DELETE_ID /*10*/:
                count = db.update(DELETES_TABLE_NAME, values, "_id=" + uri.getPathSegments().get(NOTES) + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public static String categoryNameForUUID(String inUUID) {
        if (inUUID == null || inUUID.matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
            return FliqNotesApp.getNotesApplicationContext().getBaseContext().getResources().getString(R.string.unfiled);
        }
        String category = null;
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        String[] strArr = new String[NOTES];
        strArr[0] = "category";
        Cursor c = db.query(true, CATEGORIES_TABLE_NAME, strArr, "uuid=\"" + inUUID + "\"", null, null, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0) {
            category = c.getString(c.getColumnIndex("category"));
        }
        c.close();
        return category == null ? FliqNotesApp.getNotesApplicationContext().getBaseContext().getResources().getString(R.string.unfiled) : category;
    }

    public static String categoryColorForUUID(String inUUID) {
        if (Config.V) {
            Log.v(TAG, ".categoryColorForUUID for: " + inUUID);
        }
        if (inUUID == null || inUUID.matches(FliqNotes.Categories.UNFILED_CATEGORY_UUID)) {
            return "#aaaaaa";
        }
        String color = null;
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        String[] strArr = new String[NOTES];
        strArr[0] = FliqNotes.Categories.COLOR;
        Cursor c = db.query(true, CATEGORIES_TABLE_NAME, strArr, "uuid=\"" + inUUID + "\"", null, null, null, null, null);
        if (c.moveToFirst() && c.getCount() != 0) {
            color = c.getString(0);
            if (Config.V) {
                Log.v(TAG, ".categoryColorForUUID color: " + color);
            }
        }
        c.close();
        return (color == null || color.length() == 0) ? "#aaaaaa" : color;
    }

    static {
        sUriMatcher.addURI(FliqNotes.AUTHORITY, NOTES_TABLE_NAME, NOTES);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "notes/#", 2);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, CATEGORIES_TABLE_NAME, CATEGORIES);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "categories/#", CATEGORY_ID);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, ADDS_TABLE_NAME, ADDS);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "adds/#", ADD_ID);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, MODIFIES_TABLE_NAME, MODIFIES);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "modifies/#", MODIFY_ID);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, DELETES_TABLE_NAME, DELETES);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "deletes/#", DELETE_ID);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "notes_categories", NOTES_CATEGORIES);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "search_suggest_query", SEARCH_SUGGESTIONS);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "search_suggest_query/*", SEARCH_SUGGESTIONS);
        sUriMatcher.addURI(FliqNotes.AUTHORITY, "search_suggest_shortcut/*", SEARCH_SHORTCUT);
        sNotesProjectionMap.put("_id", "notes._id");
        sNotesProjectionMap.put("uuid", "notes.uuid");
        sNotesProjectionMap.put("title", "notes.title");
        sNotesProjectionMap.put(FliqNotes.Notes.BODY, "notes.noteBody");
        sNotesProjectionMap.put(FliqNotes.Notes.CREATION_DATE, "notes.creationDate");
        sNotesProjectionMap.put(FliqNotes.Notes.MODIFICATION_DATE, "notes.modificationDate");
        sNotesProjectionMap.put(FliqNotes.Notes.TYPE, "notes.type");
        sNotesProjectionMap.put("category", "notes.category");
        sNotesProjectionMap.put(FliqNotes.Notes.AUTOGENERATE, "notes.autogenerate");
        sNotesCategoriesProjectionMap.put("_id", "notes._id");
        sNotesCategoriesProjectionMap.put("uuid", "notes.uuid");
        sNotesCategoriesProjectionMap.put("title", "notes.title");
        sNotesCategoriesProjectionMap.put(FliqNotes.Notes.BODY, "notes.noteBody");
        sNotesCategoriesProjectionMap.put(FliqNotes.Notes.CREATION_DATE, "notes.creationDate");
        sNotesCategoriesProjectionMap.put(FliqNotes.Notes.MODIFICATION_DATE, "notes.modificationDate");
        sNotesCategoriesProjectionMap.put(FliqNotes.Notes.TYPE, "notes.type");
        sNotesCategoriesProjectionMap.put("category", "notes.category");
        sNotesCategoriesProjectionMap.put(FliqNotes.Notes.AUTOGENERATE, "notes.autogenerate");
        sCategoriesProjectionMap.put("_id", "_id");
        sCategoriesProjectionMap.put("uuid", "uuid");
        sCategoriesProjectionMap.put("category", "category");
        sCategoriesProjectionMap.put(FliqNotes.Categories.COLOR, FliqNotes.Categories.COLOR);
        sCategoriesProjectionMap.put(FliqNotes.Categories.USERORDER, FliqNotes.Categories.USERORDER);
        sAddsProjectionMap.put("_id", "_id");
        sAddsProjectionMap.put("uuid", "uuid");
        sModifiesProjectionMap.put("_id", "_id");
        sModifiesProjectionMap.put("uuid", "uuid");
        sDeletesProjectionMap.put("_id", "_id");
        sDeletesProjectionMap.put("uuid", "uuid");
        String[] strArr = new String[ADDS];
        strArr[0] = "_id";
        strArr[NOTES] = "_id AS suggest_shortcut_id";
        strArr[2] = "_id AS suggest_intent_data_id";
        strArr[CATEGORIES] = "COALESCE(NULLIF(notes.title,''),substr(notes.noteBody,0,40)) AS suggest_text_1";
        strArr[CATEGORY_ID] = "substr(notes.noteBody,0,40) AS suggest_text_2";
        sSearchSuggestionsProjection = strArr;
    }
}
