package org.openintents.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ColorSlider extends View {
    public int defaultHeight;
    public int defaultWidth;
    private int mColor1;
    private int mColor2;
    private OnColorChangedListener mListener;
    private Paint mPaint;

    public ColorSlider(Context context) {
        super(context);
        init();
    }

    public ColorSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /* access modifiers changed from: package-private */
    public void init() {
        this.mColor1 = -1;
        this.mColor2 = -16777216;
        this.mPaint = new Paint(1);
        this.mPaint.setStyle(Paint.Style.FILL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.mPaint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) getHeight(), this.mColor1, this.mColor2, Shader.TileMode.CLAMP));
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.mPaint);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = this.defaultWidth;
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = this.defaultHeight;
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    public void setColors(int color1, int color2) {
        this.mColor1 = color1;
        this.mColor2 = color2;
        invalidate();
    }

    public void setOnColorChangedListener(OnColorChangedListener colorListener) {
        this.mListener = colorListener;
    }

    private int ave(int s, int d, float p) {
        return Math.round(((float) (d - s)) * p) + s;
    }

    private int interpColor(int color1, int color2, float unit) {
        if (unit <= 0.0f) {
            return color1;
        }
        if (unit >= 1.0f) {
            return color2;
        }
        float p = unit;
        int c0 = color1;
        int c1 = color2;
        return Color.argb(ave(Color.alpha(c0), Color.alpha(c1), p), ave(Color.red(c0), Color.red(c1), p), ave(Color.green(c0), Color.green(c1), p), ave(Color.blue(c0), Color.blue(c1), p));
    }

    public boolean onTouchEvent(MotionEvent event) {
        float y = event.getY();
        switch (event.getAction()) {
            case 0:
            case 2:
                int newcolor = interpColor(this.mColor1, this.mColor2, y / ((float) getHeight()));
                if (this.mListener != null) {
                    this.mListener.onColorChanged(this, newcolor);
                }
                invalidate();
                return true;
            case 1:
            default:
                return true;
        }
    }
}
