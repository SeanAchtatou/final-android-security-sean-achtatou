package org.openintents.widget;

import android.view.View;

public interface OnColorChangedListener {
    void onColorChanged(View view, int i);

    void onColorPicked(View view, int i);
}
