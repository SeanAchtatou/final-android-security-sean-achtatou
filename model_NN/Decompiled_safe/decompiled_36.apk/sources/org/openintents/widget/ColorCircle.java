package org.openintents.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;

public class ColorCircle extends View {
    private static final float CENTER_RADIUS_SCALE = 0.4f;
    private float center_radius;
    private Paint mCenterPaint;
    private int[] mColors;
    private boolean mHighlightCenter;
    private OnColorChangedListener mListener;
    private Paint mPaint;
    private boolean mTrackingCenter;

    public ColorCircle(Context context) {
        super(context);
        init();
    }

    public ColorCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void}
     arg types: [int, int, int[], ?[OBJECT, ARRAY]]
     candidates:
      ClspMth{android.graphics.SweepGradient.<init>(float, float, long, long):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, int, int):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, long[], float[]):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void} */
    /* access modifiers changed from: package-private */
    public void init() {
        this.mColors = new int[]{-65536, -65281, -16776961, -16711681, -16711936, -256, -65536};
        Shader s = new SweepGradient(0.0f, 0.0f, this.mColors, (float[]) null);
        this.mPaint = new Paint(1);
        this.mPaint.setShader(s);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mCenterPaint = new Paint(1);
        this.mCenterPaint.setStrokeWidth(5.0f);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float outer_radius = (float) (Math.min(getWidth(), getHeight()) / 2);
        float touch_feedback_ring = this.center_radius + (this.mCenterPaint.getStrokeWidth() * 2.0f);
        canvas.translate((float) (getWidth() / 2), (float) (getHeight() / 2));
        this.mPaint.setStrokeWidth(outer_radius - touch_feedback_ring);
        canvas.drawCircle(0.0f, 0.0f, (outer_radius + touch_feedback_ring) / 2.0f, this.mPaint);
        canvas.drawCircle(0.0f, 0.0f, this.center_radius, this.mCenterPaint);
        if (this.mTrackingCenter) {
            int c = this.mCenterPaint.getColor();
            this.mCenterPaint.setStyle(Paint.Style.STROKE);
            if (this.mHighlightCenter) {
                this.mCenterPaint.setAlpha(255);
            } else {
                this.mCenterPaint.setAlpha(128);
            }
            canvas.drawCircle(0.0f, 0.0f, this.center_radius + this.mCenterPaint.getStrokeWidth(), this.mCenterPaint);
            this.mCenterPaint.setStyle(Paint.Style.FILL);
            this.mCenterPaint.setColor(c);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int size = Math.min(View.MeasureSpec.getSize(widthMeasureSpec), View.MeasureSpec.getSize(heightMeasureSpec));
        this.center_radius = (CENTER_RADIUS_SCALE * ((float) size)) / 2.0f;
        setMeasuredDimension(size, size);
    }

    public void setColor(int color) {
        this.mCenterPaint.setColor(color);
        invalidate();
    }

    public int getColor() {
        return this.mCenterPaint.getColor();
    }

    public void setOnColorChangedListener(OnColorChangedListener colorListener) {
        this.mListener = colorListener;
    }

    private int ave(int s, int d, float p) {
        return Math.round(((float) (d - s)) * p) + s;
    }

    private int interpColor(int[] colors, float unit) {
        if (unit <= 0.0f) {
            return colors[0];
        }
        if (unit >= 1.0f) {
            return colors[colors.length - 1];
        }
        float p = unit * ((float) (colors.length - 1));
        int i = (int) p;
        float p2 = p - ((float) i);
        int c0 = colors[i];
        int c1 = colors[i + 1];
        return Color.argb(ave(Color.alpha(c0), Color.alpha(c1), p2), ave(Color.red(c0), Color.red(c1), p2), ave(Color.green(c0), Color.green(c1), p2), ave(Color.blue(c0), Color.blue(c1), p2));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0049  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r12) {
        /*
            r11 = this;
            r8 = 0
            r10 = 1
            float r6 = r12.getX()
            int r7 = r11.getWidth()
            int r7 = r7 / 2
            float r7 = (float) r7
            float r4 = r6 - r7
            float r6 = r12.getY()
            int r7 = r11.getHeight()
            int r7 = r7 / 2
            float r7 = (float) r7
            float r5 = r6 - r7
            float r6 = android.graphics.PointF.length(r4, r5)
            float r7 = r11.center_radius
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 > 0) goto L_0x002f
            r1 = r10
        L_0x0027:
            int r6 = r12.getAction()
            switch(r6) {
                case 0: goto L_0x0031;
                case 1: goto L_0x0075;
                case 2: goto L_0x003b;
                default: goto L_0x002e;
            }
        L_0x002e:
            return r10
        L_0x002f:
            r1 = r8
            goto L_0x0027
        L_0x0031:
            r11.mTrackingCenter = r1
            if (r1 == 0) goto L_0x003b
            r11.mHighlightCenter = r10
            r11.invalidate()
            goto L_0x002e
        L_0x003b:
            boolean r6 = r11.mTrackingCenter
            if (r6 == 0) goto L_0x0049
            boolean r6 = r11.mHighlightCenter
            if (r6 == r1) goto L_0x002e
            r11.mHighlightCenter = r1
            r11.invalidate()
            goto L_0x002e
        L_0x0049:
            double r6 = (double) r5
            double r8 = (double) r4
            double r6 = java.lang.Math.atan2(r6, r8)
            float r0 = (float) r6
            r6 = 1086918619(0x40c90fdb, float:6.2831855)
            float r3 = r0 / r6
            r6 = 0
            int r6 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r6 >= 0) goto L_0x005d
            r6 = 1065353216(0x3f800000, float:1.0)
            float r3 = r3 + r6
        L_0x005d:
            int[] r6 = r11.mColors
            int r2 = r11.interpColor(r6, r3)
            android.graphics.Paint r6 = r11.mCenterPaint
            r6.setColor(r2)
            org.openintents.widget.OnColorChangedListener r6 = r11.mListener
            if (r6 == 0) goto L_0x0071
            org.openintents.widget.OnColorChangedListener r6 = r11.mListener
            r6.onColorChanged(r11, r2)
        L_0x0071:
            r11.invalidate()
            goto L_0x002e
        L_0x0075:
            boolean r6 = r11.mTrackingCenter
            if (r6 == 0) goto L_0x002e
            if (r1 == 0) goto L_0x008a
            org.openintents.widget.OnColorChangedListener r6 = r11.mListener
            if (r6 == 0) goto L_0x008a
            org.openintents.widget.OnColorChangedListener r6 = r11.mListener
            android.graphics.Paint r7 = r11.mCenterPaint
            int r7 = r7.getColor()
            r6.onColorPicked(r11, r7)
        L_0x008a:
            r11.mTrackingCenter = r8
            r11.invalidate()
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.openintents.widget.ColorCircle.onTouchEvent(android.view.MotionEvent):boolean");
    }
}
