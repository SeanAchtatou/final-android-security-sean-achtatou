package tai.haod.rmbd.data;

import android.content.res.Resources;
import android.net.Uri;
import android.provider.BaseColumns;
import tai.haod.rmbd.R;

public class DownloadQueue implements BaseColumns {
    public static final String COL_ALBUM = "album";
    public static final String COL_ARTIST = "artist";
    public static final String COL_CONTROL = "control";
    public static final String COL_CURRENT_BYTES = "current_bytes";
    public static final String COL_FILE_NAME_HINT = "hint";
    public static final String COL_LAST_MODIFICATION = "lastmod";
    public static final String COL_LYRIC = "lyric";
    public static final String COL_MEDIA_SCANNED = "scaned";
    public static final String COL_MIME_TYPE = "mimetype";
    public static final String COL_STATUS = "status";
    public static final String COL_TITLE = "title";
    public static final String COL_TOTAL_BYTES = "total_bytes";
    public static final String COL_URI = "uri";
    public static final String COL_VISIBILITY = "visibility";
    public static final Uri CONTENT_URI = Uri.parse("content://tai.haod.rmbd.mdq/mdq");
    public static final int CONTROL_PAUSED = 1;
    public static final int CONTROL_RUN = 0;
    public static final String ETAG = "etag";
    public static final String FAILED_CONNECTIONS = "numfailed";
    public static final String RETRY_AFTER_X_REDIRECT_COUNT = "method";
    public static final int STATUS_BAD_REQUEST = 400;
    public static final int STATUS_CANCELED = 490;
    public static final int STATUS_FILE_ERROR = 492;
    public static final int STATUS_HTTP_DATA_ERROR = 495;
    public static final int STATUS_HTTP_EXCEPTION = 496;
    public static final int STATUS_LENGTH_REQUIRED = 411;
    public static final int STATUS_NOT_ACCEPTABLE = 406;
    public static final int STATUS_PENDING = 191;
    public static final int STATUS_PENDING_PAUSED = 192;
    public static final int STATUS_PRECONDITION_FAILED = 412;
    public static final int STATUS_RUNNING = 190;
    public static final int STATUS_RUNNING_PAUSED = 193;
    public static final int STATUS_SUCCESS = 200;
    public static final int STATUS_TOO_MANY_REDIRECTS = 497;
    public static final int STATUS_UNHANDLED_HTTP_CODE = 494;
    public static final int STATUS_UNHANDLED_REDIRECT = 493;
    public static final int STATUS_UNKNOWN_ERROR = 491;
    public static final int VISIBILITY_HIDDEN = 2;
    public static final int VISIBILITY_VISIBLE = 0;
    public static final int VISIBILITY_VISIBLE_NOTIFY_COMPLETED = 1;
    public static final String _DATA = "_data";

    private DownloadQueue() {
    }

    public static boolean isStatusInformational(int status) {
        return status >= 100 && status < 200;
    }

    public static boolean isStatusRunning(int status) {
        return status == 191 || status == 190;
    }

    public static boolean isStatusSuspended(int status) {
        return status == 192 || status == 193;
    }

    public static boolean isStatusSuccess(int status) {
        return status >= 200 && status < 300;
    }

    public static boolean isStatusError(int status) {
        return status >= 400 && status < 600;
    }

    public static boolean isStatusClientError(int status) {
        return status >= 400 && status < 500;
    }

    public static boolean isStatusServerError(int status) {
        return status >= 500 && status < 600;
    }

    public static boolean isStatusCompleted(int status) {
        return (status >= 200 && status < 300) || (status >= 400 && status < 600);
    }

    public static String translateErrMsg(Resources res, int errNo) {
        if (isStatusServerError(errNo)) {
            return res.getString(R.string.SERVER_ERROR);
        }
        switch (errNo) {
            case STATUS_BAD_REQUEST /*400*/:
                return res.getString(R.string.STATUS_BAD_REQUEST);
            case STATUS_NOT_ACCEPTABLE /*406*/:
                return res.getString(R.string.STATUS_NOT_ACCEPTABLE);
            case STATUS_LENGTH_REQUIRED /*411*/:
                return res.getString(R.string.STATUS_LENGTH_REQUIRED);
            case STATUS_PRECONDITION_FAILED /*412*/:
                return res.getString(R.string.STATUS_PRECONDITION_FAILED);
            case STATUS_CANCELED /*490*/:
                return res.getString(R.string.STATUS_CANCELED);
            case STATUS_UNKNOWN_ERROR /*491*/:
                return res.getString(R.string.STATUS_UNKNOWN_ERROR);
            case STATUS_FILE_ERROR /*492*/:
                return res.getString(R.string.STATUS_FILE_ERROR);
            case STATUS_UNHANDLED_REDIRECT /*493*/:
                return res.getString(R.string.STATUS_UNHANDLED_REDIRECT);
            case STATUS_UNHANDLED_HTTP_CODE /*494*/:
                return res.getString(R.string.STATUS_UNHANDLED_HTTP_CODE);
            case STATUS_HTTP_DATA_ERROR /*495*/:
                return res.getString(R.string.STATUS_HTTP_DATA_ERROR);
            case STATUS_HTTP_EXCEPTION /*496*/:
                return res.getString(R.string.STATUS_HTTP_EXCEPTION);
            case STATUS_TOO_MANY_REDIRECTS /*497*/:
                return res.getString(R.string.STATUS_TOO_MANY_REDIRECTS);
            default:
                return "Fatal Error";
        }
    }
}
