package tai.haod.rmbd.data;

import android.net.Uri;
import android.provider.BaseColumns;

public class History implements BaseColumns {
    public static final String COL_ALBUM = "album";
    public static final String COL_ALBUMURL = "aurl";
    public static final String COL_ARTIST = "artist";
    public static final String COL_SONG = "song";
    public static final String COL_SONGURL = "surl";
    public static final Uri CONTENT_URI = Uri.parse("content://tai.haod.rmbd.history/history");

    private History() {
    }
}
