package tai.haod.rmbd.data;

import android.net.Uri;
import android.provider.BaseColumns;

public class Downloaded implements BaseColumns {
    public static final String COL_ALBUM = "album";
    public static final String COL_ARTIST = "artist";
    public static final String COL_LAST_MODIFICATION = "lastmod";
    public static final String COL_LYRIC = "lyric";
    public static final String COL_MIME_TYPE = "mimetype";
    public static final String COL_THUMBNAIL = "pic";
    public static final String COL_TITLE = "title";
    public static final String COL_URI = "uri";
    public static final Uri CONTENT_URI = Uri.parse("content://tai.haod.rmbd.mdd/mdd");
    public static final String _DATA = "_data";

    private Downloaded() {
    }
}
