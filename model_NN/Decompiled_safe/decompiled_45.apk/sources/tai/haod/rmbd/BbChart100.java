package tai.haod.rmbd;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.qwapi.adclient.android.utils.Utils;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tai.haod.rmbd.data.History;
import tai.haod.rmbd.utils.NetUtils;

public class BbChart100 extends Activity {
    static final int FEED_TOPTRACK = 1;
    private static final String P_URL = "u";
    ArrayAdapter<String> mAdapter;
    JSONArray mFeedentries;
    int mFeetType = 0;
    /* access modifiers changed from: private */
    public ArrayList<MP3Info> mMp3Title = new ArrayList<>();
    ArrayList<String> mStrings = new ArrayList<>();
    FetchLastFMListTask mTask = null;
    MP3Adapter mTrackAdapter;
    ListView mTypesList;

    public static void startActivite(Context ctx, String url) {
        Intent intent = new Intent(ctx, BbChart100.class);
        intent.putExtra("u", url);
        ctx.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.popular);
        AdsView.createAdWhirl(this);
        this.mTypesList = (ListView) findViewById(R.id.popular);
        this.mTrackAdapter = new MP3Adapter(this);
        this.mTypesList.setAdapter((ListAdapter) this.mTrackAdapter);
        this.mTypesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                try {
                    SearchActivity.startSearch(BbChart100.this, ((MP3Info) BbChart100.this.mMp3Title.get(position)).name);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        this.mTask = new FetchLastFMListTask();
        this.mTask.execute(getIntent().getExtras().getString("u"));
    }

    public void onDestroy() {
        if (!(this.mTask == null || this.mTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.mTask.cancel(true);
        }
        super.onDestroy();
    }

    public class FetchLastFMListTask extends AsyncTask<String, Integer, Integer> {
        public FetchLastFMListTask() {
        }

        public Integer doInBackground(String... urls) {
            String jsonBuff = NetUtils.loadString(urls[0]);
            if (jsonBuff == null) {
                try {
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            } else {
                JSONArray feedEntries = new JSONArray(jsonBuff);
                int entryLength = feedEntries.length();
                int i = 0;
                while (i < entryLength) {
                    try {
                        JSONObject jo = feedEntries.getJSONObject(i);
                        MP3Info song = new MP3Info();
                        song.name = jo.getString(History.COL_SONG);
                        song.artist = jo.getString("artist");
                        BbChart100.this.mMp3Title.add(song);
                        i++;
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                        return 0;
                    }
                }
                return 1;
            }
        }

        public void onPostExecute(Integer result) {
            if (result == null || result.intValue() != 1) {
                Toast.makeText(BbChart100.this, "Network service not available, please try again.", 0).show();
                return;
            }
            try {
                BbChart100.this.findViewById(R.id.center_text).setVisibility(8);
                BbChart100.this.mTrackAdapter.add();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class MP3Info {
        public String artist = Utils.EMPTY_STRING;
        public String name = Utils.EMPTY_STRING;

        public MP3Info() {
        }
    }

    class MP3Adapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MP3Adapter(Context c) {
            this.mInflater = LayoutInflater.from(c);
        }

        public int getCount() {
            return BbChart100.this.mMp3Title.size();
        }

        public Object getItem(int position) {
            return BbChart100.this.mMp3Title.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = BbChart100.this.getLayoutInflater().inflate((int) R.layout.toptracks_item, (ViewGroup) null);
            }
            MP3Info mp3 = (MP3Info) BbChart100.this.mMp3Title.get(position);
            row.findViewById(R.id.Image).setVisibility(8);
            ((TextView) row.findViewById(R.id.Rank)).setText((position + 1) + ". ");
            ((TextView) row.findViewById(R.id.Title)).setText(mp3.name);
            ((TextView) row.findViewById(R.id.Artist)).setText(mp3.artist);
            return row;
        }

        public void add() {
            notifyDataSetChanged();
        }
    }
}
