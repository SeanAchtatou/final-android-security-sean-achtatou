package tai.haod.rmbd;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.qwapi.adclient.android.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import tai.haod.rmbd.utils.Util;

public class LibraryActivity extends Activity {
    AdapterView.OnItemClickListener OnItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View paramView, int paramInt, long paramLong) {
            Class<CategoryActivity> cls = CategoryActivity.class;
            if (paramInt < LibraryActivity.this.mLibraryList.size()) {
                if (paramInt == 0) {
                    Intent localIntent = new Intent(LibraryActivity.this.mContext, SongsActivity.class);
                    localIntent.putExtra("bundle_category_code", 40);
                    localIntent.putExtra("bundle_row_title", Utils.EMPTY_STRING);
                    LibraryActivity.this.mContext.startActivity(localIntent);
                }
                if (paramInt == 1) {
                    Class<CategoryActivity> cls2 = CategoryActivity.class;
                    Intent localIntent2 = new Intent(LibraryActivity.this.mContext, cls);
                    localIntent2.putExtra("bundle_category_code", 41);
                    LibraryActivity.this.mContext.startActivity(localIntent2);
                }
                if (paramInt == 2) {
                    Class<CategoryActivity> cls3 = CategoryActivity.class;
                    Intent localIntent3 = new Intent(LibraryActivity.this.mContext, cls);
                    localIntent3.putExtra("bundle_category_code", 42);
                    LibraryActivity.this.mContext.startActivity(localIntent3);
                }
                if (paramInt == 3) {
                    Class<CategoryActivity> cls4 = CategoryActivity.class;
                    Intent localIntent4 = new Intent(LibraryActivity.this.mContext, cls);
                    localIntent4.putExtra("bundle_category_code", 43);
                    LibraryActivity.this.mContext.startActivity(localIntent4);
                }
                if (paramInt == 4) {
                    Class<CategoryActivity> cls5 = CategoryActivity.class;
                    Intent localIntent5 = new Intent(LibraryActivity.this.mContext, cls);
                    localIntent5.putExtra("bundle_category_code", 44);
                    LibraryActivity.this.mContext.startActivity(localIntent5);
                }
                if (paramInt == 5) {
                    Class<CategoryActivity> cls6 = CategoryActivity.class;
                    Intent localIntent6 = new Intent(LibraryActivity.this.mContext, cls);
                    localIntent6.putExtra("bundle_category_code", 45);
                    LibraryActivity.this.mContext.startActivity(localIntent6);
                }
                if (paramInt == 6) {
                    Intent localIntent7 = new Intent(LibraryActivity.this.mContext, MyDownloadsActivity.class);
                    localIntent7.putExtra("bundle_category_code", 46);
                    LibraryActivity.this.mContext.startActivity(localIntent7);
                }
            }
        }
    };
    Context mContext;
    final ArrayList<HashMap<String, Object>> mLibraryList = new ArrayList<>();
    SimpleAdapter mLibraryListAdapter;
    ListView mLibraryListView;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = this;
        setContentView((int) R.layout.library);
        AdsView.createAdWhirl(this);
        setTitle(getString(R.string.title_library));
        this.mLibraryListView = (ListView) findViewById(R.id.lv_library);
        this.mLibraryListAdapter = new SimpleAdapter(this, this.mLibraryList, R.layout.library_row, new String[]{"key_row_title"}, new int[]{R.id.row_title});
        this.mLibraryListView.setAdapter((ListAdapter) this.mLibraryListAdapter);
        this.mLibraryListView.setOnItemClickListener(this.OnItemClickListener);
        populateLibraryListView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        SharedPreferences localSharedPreferences = getSharedPreferences("prefs_gml", 0);
        if (localSharedPreferences.getBoolean("is_first_import", true) && Util.isSdCardMounted(this)) {
            SharedPreferences.Editor localEditor1 = localSharedPreferences.edit();
            SharedPreferences.Editor putBoolean = localEditor1.putBoolean("is_first_import", false);
            boolean commit = localEditor1.commit();
        }
    }

    /* access modifiers changed from: package-private */
    public void populateLibraryListView() {
        HashMap<String, Object> localHashMap = new HashMap<>();
        localHashMap.put("key_row_title", "All Songs");
        this.mLibraryList.add(localHashMap);
        HashMap<String, Object> localHashMap2 = new HashMap<>();
        localHashMap2.put("key_row_title", "Artists");
        this.mLibraryList.add(localHashMap2);
        HashMap<String, Object> localHashMap3 = new HashMap<>();
        localHashMap3.put("key_row_title", "Albums");
        this.mLibraryList.add(localHashMap3);
        HashMap<String, Object> localHashMap4 = new HashMap<>();
        localHashMap4.put("key_row_title", "Genres");
        this.mLibraryList.add(localHashMap4);
        HashMap<String, Object> localHashMap5 = new HashMap<>();
        localHashMap5.put("key_row_title", "Folders");
        this.mLibraryList.add(localHashMap5);
        HashMap<String, Object> localHashMap6 = new HashMap<>();
        localHashMap6.put("key_row_title", "Playlists");
        this.mLibraryList.add(localHashMap6);
        HashMap<String, Object> localHashMap7 = new HashMap<>();
        localHashMap7.put("key_row_title", "MyDownloads");
        this.mLibraryList.add(localHashMap7);
        this.mLibraryListAdapter.notifyDataSetChanged();
    }
}
