package tai.haod.rmbd;

import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.Browser;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedList;
import java.util.List;
import tai.haod.rmbd.data.DownloadQueue;
import tai.haod.rmbd.task.AlbumTask;
import tai.haod.rmbd.task.LyricTask;
import tai.haod.rmbd.task.SearchResult;
import tai.haod.rmbd.task.SongLinkTask;
import tai.haod.rmbd.utils.HeaderViewHelper;

public class SongActivity extends ListActivity implements SongLinkTask.Listener, LyricTask.Listener, AlbumTask.Listener {
    private static final String P_ALBUM = "album";
    private static final String P_ALBUMURL = "au";
    private static final String P_ARTIST = "artist";
    private static final String P_LYRICURL = "lu";
    private static final String P_SIZE = "size";
    private static final String P_SONG = "song";
    private static final String P_SONGURL = "su";
    private MyAdapter mAdapter = null;
    private AlbumTask mAlbumTask = null;
    private View mBtnView = null;
    private HeaderViewHelper mHeaderView = null;
    private View mProfileView = null;
    private SongLinkTask mSongTask = null;
    /* access modifiers changed from: private */
    public String mSongURL = null;

    public static void startActivity(Context ctx, SearchResult sr) {
        Intent intent = new Intent(ctx, SongActivity.class);
        intent.putExtra("song", sr.mSong);
        intent.putExtra("artist", sr.mArtist);
        intent.putExtra("album", sr.mAlbum);
        intent.putExtra(P_SIZE, sr.mSize);
        intent.putExtra(P_SONGURL, sr.mSongURL);
        intent.putExtra(P_ALBUMURL, sr.mAlbumURL);
        intent.putExtra(P_LYRICURL, sr.mLyricURL);
        ctx.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.song);
        AdsView.createAdWhirl(this);
        Bundle bundle = getIntent().getExtras();
        this.mHeaderView = new HeaderViewHelper(this);
        String title = bundle.getString("album");
        if (title.length() <= 0) {
            title = bundle.getString("song");
        }
        this.mHeaderView.setTitle(title);
        this.mHeaderView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SongActivity.this.finish();
            }
        });
        this.mProfileView = LayoutInflater.from(this).inflate((int) R.layout.song_header, (ViewGroup) null);
        ((TextView) this.mProfileView.findViewById(R.id.Song)).setText(bundle.getString("song"));
        String artist = bundle.getString("artist");
        if (artist.length() <= 0) {
            artist = getResources().getString(R.string.UNKNOWN_ARTIST);
        }
        ((TextView) this.mProfileView.findViewById(R.id.Artist)).setText(artist);
        String album = bundle.getString("album");
        if (album.length() <= 0) {
            album = getResources().getString(R.string.UNKNOWN_ALBUM);
        }
        ((TextView) this.mProfileView.findViewById(R.id.Album)).setText(album);
        getListView().addHeaderView(this.mProfileView, null, false);
        this.mBtnView = LayoutInflater.from(this).inflate((int) R.layout.song_btn, (ViewGroup) null);
        this.mBtnView.setVisibility(8);
        getListView().addHeaderView(this.mBtnView, null, false);
        ((Button) this.mBtnView.findViewById(R.id.BtnPlay)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SongActivity.this.mSongURL != null) {
                    Bundle b = SongActivity.this.getIntent().getExtras();
                    StreamStarterActivity.startActivity(SongActivity.this, SongActivity.this.mSongURL, b.getString("song"), b.getString("artist"), b.getString("album"), b.getString(SongActivity.P_LYRICURL));
                }
            }
        });
        ((Button) this.mBtnView.findViewById(R.id.BtnShare)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle b = SongActivity.this.getIntent().getExtras();
                Browser.sendString(SongActivity.this, String.valueOf(b.getString("song")) + " - " + b.getString("artist") + "\n\n" + SongActivity.this.mSongURL + "\n\nSent from Android with " + SongActivity.this.getString(R.string.app_name));
            }
        });
        ((Button) this.mBtnView.findViewById(R.id.BtnDownload)).setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
            public void onClick(View arg0) {
                Bundle bundle = SongActivity.this.getIntent().getExtras();
                String songTitle = bundle.getString("song");
                String songArtist = bundle.getString("artist");
                String songAlbum = bundle.getString("album");
                String string = bundle.getString(SongActivity.P_SIZE);
                try {
                    songTitle = URLDecoder.decode(songTitle, "gb2312").toString();
                    songArtist = URLDecoder.decode(songArtist, "gb2312").toString();
                    songAlbum = URLDecoder.decode(songAlbum, "gb2312").toString();
                } catch (UnsupportedEncodingException | IllegalArgumentException e) {
                }
                ContentValues values = new ContentValues();
                values.put("title", songTitle);
                values.put("artist", songArtist);
                values.put("album", songAlbum);
                values.put("lyric", bundle.getString(SongActivity.P_LYRICURL));
                values.put(DownloadQueue.COL_CONTROL, (Integer) 0);
                values.put("uri", SongActivity.this.mSongURL);
                values.put("mimetype", "audio/mp3");
                values.put(DownloadQueue.COL_FILE_NAME_HINT, String.valueOf(songTitle) + "-" + songArtist);
                if (SongActivity.this.getContentResolver().insert(DownloadQueue.CONTENT_URI, values) == null) {
                    Toast.makeText(SongActivity.this, (int) R.string.ERR_DATABASE_FAILURE, 1).show();
                    return;
                }
                Toast.makeText(SongActivity.this, SongActivity.this.getString(R.string.INFO_DOWNLOAD_STARTED, new Object[]{songTitle}), 1).show();
            }
        });
        this.mAdapter = (MyAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new MyAdapter(this);
            setListAdapter(this.mAdapter);
            this.mAlbumTask = new AlbumTask(this);
            Log.e("Music", "album : " + bundle.getString(P_ALBUMURL));
            this.mAlbumTask.execute(bundle.getString(P_ALBUMURL));
            String songURL = getIntent().getExtras().getString(P_SONGURL);
            Log.e("Music", "songURL : " + songURL);
            this.mSongTask = new SongLinkTask(this);
            this.mSongTask.execute(songURL);
            return;
        }
        this.mAdapter.mActivity = this;
        setListAdapter(this.mAdapter);
    }

    public Object onRetainNonConfigurationInstance() {
        return this.mAdapter;
    }

    public void onDestroy() {
        if (this.mAlbumTask != null) {
            this.mAlbumTask.cancel(true);
            this.mAlbumTask = null;
        }
        if (this.mSongTask != null) {
            this.mSongTask.cancel(true);
            this.mSongTask = null;
        }
        getListView().setAdapter((ListAdapter) null);
        this.mAdapter = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (this.mAdapter != null && this.mAdapter.getItem(position - 2) == null) {
        }
    }

    private static class MyAdapter extends BaseAdapter {
        /* access modifiers changed from: private */
        public SongActivity mActivity = null;
        Bitmap mAlbumArt = null;
        List<String> mSongs = new LinkedList();

        public MyAdapter(SongActivity activity) {
            this.mActivity = activity;
        }

        private static class ViewHolder {
            TextView mTVSong;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(ViewHolder viewHolder) {
                this();
            }
        }

        public int getCount() {
            return this.mSongs.size();
        }

        public String getItem(int pos) {
            try {
                return this.mSongs.get(pos);
            } catch (Exception e) {
                return null;
            }
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int pos, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = LayoutInflater.from(this.mActivity).inflate((int) R.layout.song_item, parent, false);
                ViewHolder vh = new ViewHolder(null);
                vh.mTVSong = (TextView) view.findViewById(R.id.Name);
                view.setTag(vh);
            } else {
                view = convertView;
            }
            ViewHolder vh2 = (ViewHolder) view.getTag();
            String songName = getItem(pos);
            if (songName != null) {
                vh2.mTVSong.setText(songName);
            }
            return view;
        }

        public void add(String song) {
            this.mSongs.add(song);
            notifyDataSetChanged();
        }
    }

    public void SLT_OnBegin() {
        this.mHeaderView.showProgress();
    }

    public void SLT_OnEnd(boolean bError) {
        this.mHeaderView.hideProgress();
    }

    public void SLT_OnReady(String url) {
        Log.e("Music", "mSongURL : " + url);
        this.mSongURL = url;
        this.mBtnView.setVisibility(0);
    }

    public void LT_OnBegin() {
        this.mHeaderView.showProgress();
    }

    public void LT_OnEnd(boolean bError) {
        this.mHeaderView.hideProgress();
    }

    public void LT_OnReady(String lyric) {
    }

    public void AT_OnAlbumArtReady(Bitmap bmp) {
        if (this.mAdapter != null) {
            this.mAdapter.mAlbumArt = bmp;
            ((ImageView) this.mProfileView.findViewById(R.id.AlbumArt)).setImageBitmap(bmp);
        }
    }

    public void AT_OnBegin() {
        this.mHeaderView.showProgress();
    }

    public void AT_OnEnd(boolean bError) {
        this.mHeaderView.hideProgress();
    }

    public void AT_OnAlbumSongReady(String songName) {
        if (this.mAdapter != null) {
            this.mAdapter.add(songName);
        }
    }
}
