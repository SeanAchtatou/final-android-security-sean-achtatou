package tai.haod.rmbd.editor;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.qwapi.adclient.android.utils.Utils;
import java.util.HashMap;
import java.util.Iterator;

public class SongMetadataReader {
    public Uri GENRES_URI = MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI;
    public Activity mActivity = null;
    public String mAlbum = Utils.EMPTY_STRING;
    public String mArtist = Utils.EMPTY_STRING;
    public String mFilename = Utils.EMPTY_STRING;
    public String mGenre = Utils.EMPTY_STRING;
    public String mTitle = Utils.EMPTY_STRING;
    public int mYear = -1;

    SongMetadataReader(Activity activity, String filename) {
        this.mActivity = activity;
        this.mFilename = filename;
        this.mTitle = getBasename(filename);
        try {
            ReadMetadata();
        } catch (Exception e) {
        }
    }

    private void ReadMetadata() {
        HashMap<String, String> genreIdMap = new HashMap<>();
        Cursor c = this.mActivity.managedQuery(this.GENRES_URI, new String[]{"_id", "name"}, null, null, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            genreIdMap.put(c.getString(0), c.getString(1));
            c.moveToNext();
        }
        this.mGenre = Utils.EMPTY_STRING;
        Iterator it = genreIdMap.keySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String genreId = (String) it.next();
            if (this.mActivity.managedQuery(makeGenreUri(genreId), new String[]{"_data"}, "_data LIKE \"" + this.mFilename + "\"", null, null).getCount() != 0) {
                this.mGenre = (String) genreIdMap.get(genreId);
                break;
            }
        }
        Uri uri = MediaStore.Audio.Media.getContentUriForPath(this.mFilename);
        Cursor c2 = this.mActivity.managedQuery(uri, new String[]{"_id", "title", "artist", "album", "year", "_data"}, "_data LIKE \"" + this.mFilename + "\"", null, null);
        if (c2.getCount() == 0) {
            this.mTitle = getBasename(this.mFilename);
            this.mArtist = Utils.EMPTY_STRING;
            this.mAlbum = Utils.EMPTY_STRING;
            this.mYear = -1;
            return;
        }
        c2.moveToFirst();
        this.mTitle = getStringFromColumn(c2, "title");
        if (this.mTitle == null || this.mTitle.length() == 0) {
            this.mTitle = getBasename(this.mFilename);
        }
        this.mArtist = getStringFromColumn(c2, "artist");
        this.mAlbum = getStringFromColumn(c2, "album");
        this.mYear = getIntegerFromColumn(c2, "year");
    }

    private Uri makeGenreUri(String genreId) {
        return Uri.parse(this.GENRES_URI.toString() + "/" + genreId + "/" + "members");
    }

    private String getStringFromColumn(Cursor c, String columnName) {
        String value = c.getString(c.getColumnIndexOrThrow(columnName));
        if (value == null || value.length() <= 0) {
            return null;
        }
        return value;
    }

    private int getIntegerFromColumn(Cursor c, String columnName) {
        Integer value = Integer.valueOf(c.getInt(c.getColumnIndexOrThrow(columnName)));
        if (value != null) {
            return value.intValue();
        }
        return -1;
    }

    private String getBasename(String filename) {
        return filename.substring(filename.lastIndexOf(47) + 1, filename.lastIndexOf(46));
    }
}
