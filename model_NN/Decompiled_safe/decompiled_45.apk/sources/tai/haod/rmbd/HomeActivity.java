package tai.haod.rmbd;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import com.qwapi.adclient.android.utils.Utils;
import tai.haod.rmbd.editor.RingdroidSelectActivity;
import tai.haod.rmbd.utils.Constants;
import tai.haod.rmbd.utils.Util;

public class HomeActivity extends Activity {
    public static final int DLG_CONFIREM_EXIT = 53000;
    public static final int DLG_RATEME = 51000;
    public static final int DLG_SHARE = 52000;
    private static final String P_ALBUM = "album";
    private static final String P_ARTIST = "artist";
    private static final String P_LYRICURL = "lyricurl";
    private static final String P_SONG = "song";
    private static final String P_SONGURL = "url";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.cover);
        AdsView.createAdWhirl(this);
        ((Button) findViewById(R.id.btn_search)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchActivity.startActivity(HomeActivity.this);
            }
        });
        ((Button) findViewById(R.id.btn_chart)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, MusicCharts.class));
            }
        });
        ((Button) findViewById(R.id.btn_download)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, DownloadActivity.class));
            }
        });
        ((Button) findViewById(R.id.btn_library)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, LibraryActivity.class));
            }
        });
        ((Button) findViewById(R.id.btn_player)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences playback = HomeActivity.this.getSharedPreferences(Constants.PlayBack, 0);
                String artist = playback.getString("artist", Utils.EMPTY_STRING);
                String song = playback.getString("song", Utils.EMPTY_STRING);
                String album = playback.getString("album", Utils.EMPTY_STRING);
                StreamStarterActivity.startActivity(HomeActivity.this, playback.getString(HomeActivity.P_SONGURL, Utils.EMPTY_STRING), song, artist, album, playback.getString(HomeActivity.P_LYRICURL, Utils.EMPTY_STRING), true);
            }
        });
        ((Button) findViewById(R.id.btn_editor)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, RingdroidSelectActivity.class));
            }
        });
        ((Button) findViewById(R.id.btn_rate)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.showDialog(HomeActivity.DLG_RATEME);
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case Util.DOWNLOAD_APP_DIG /*10000*/:
                return Util.createDownloadDialog(this);
            case DLG_RATEME /*51000*/:
                return new AlertDialog.Builder(this).setTitle("Rate me in market").setMessage("If you like G-Tunes Music Lite, please rate it in Android Market. Thanks a lot.").setPositiveButton("Rate me", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            HomeActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:" + HomeActivity.class.getPackage().getName())));
                        } catch (Exception e) {
                        }
                    }
                }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).setCancelable(true).create();
            case DLG_SHARE /*52000*/:
                return new AlertDialog.Builder(this).setTitle("Share to friends").setMessage("If you like G-Tunes Music Lite, you can share app to your friends. Thanks a lot.").setPositiveButton("Share to", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            Browser.sendString(HomeActivity.this, "The app of " + HomeActivity.this.getString(R.string.app_name) + " " + "provides millions of songs for you to enjoy online listening with brilliant sound quality or download for free." + "Therefore I highly recommend it to you, sincerely hope you will love it. " + "http://market.android.com/details?id=" + HomeActivity.this.getApplication().getPackageName());
                        } catch (Exception e) {
                        }
                    }
                }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).setCancelable(true).create();
            case DLG_CONFIREM_EXIT /*53000*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.quit).setIcon(17301659).setMessage((int) R.string.confirm_quit).setPositiveButton((int) R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        HomeActivity.this.finish();
                    }
                }).setNegativeButton((int) R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).setCancelable(true).create();
            default:
                return null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showDialog(DLG_CONFIREM_EXIT);
        return true;
    }
}
