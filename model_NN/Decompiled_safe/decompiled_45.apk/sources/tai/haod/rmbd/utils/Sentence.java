package tai.haod.rmbd.utils;

import java.io.Serializable;

public class Sentence implements Serializable {
    private static final long serialVersionUID = 4095503465005196842L;
    private String mContent;
    private long mFromTime;
    private long mToTime;

    public Sentence(String content, long fromTime, long toTime) {
        this.mContent = content;
        this.mFromTime = fromTime;
        this.mToTime = toTime;
    }

    public Sentence(String content, long fromTime) {
        this(content, fromTime, 0);
    }

    public Sentence(String content) {
        this(content, 0, 0);
    }

    public long getFromTime() {
        return this.mFromTime;
    }

    public void setFromTime(long fromTime) {
        this.mFromTime = fromTime;
    }

    public long getToTime() {
        return this.mToTime;
    }

    public void setToTime(long toTime) {
        this.mToTime = toTime;
    }

    public boolean isInTime(long time) {
        return time >= this.mFromTime && time <= this.mToTime;
    }

    public String getContent() {
        return this.mContent;
    }
}
