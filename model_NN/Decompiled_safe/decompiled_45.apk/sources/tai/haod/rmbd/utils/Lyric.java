package tai.haod.rmbd.utils;

import com.qwapi.adclient.android.utils.Utils;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lyric {
    private static final Pattern pattern = Pattern.compile("(?<=\\[).*?(?=\\])");
    public List<Sentence> list;
    private int mOffset;

    public Lyric(String lyric) {
        this.mOffset = 0;
        this.list = new ArrayList();
        this.mOffset = 0;
        init(lyric);
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    private void init(String content) {
        if (content != null && !content.trim().equals(Utils.EMPTY_STRING)) {
            try {
                BufferedReader br = new BufferedReader(new StringReader(content));
                while (true) {
                    String temp = br.readLine();
                    if (temp == null) {
                        break;
                    }
                    parseLine(temp.trim());
                }
                br.close();
                Collections.sort(this.list, new Comparator<Sentence>() {
                    public int compare(Sentence o1, Sentence o2) {
                        return (int) (o1.getFromTime() - o2.getFromTime());
                    }
                });
                if (this.list.size() != 0) {
                    Sentence sentence = this.list.get(0);
                    int size = this.list.size();
                    for (int i = 0; i < size; i++) {
                        Sentence next = null;
                        if (i + 1 < size) {
                            next = this.list.get(i + 1);
                        }
                        Sentence now = this.list.get(i);
                        if (next != null) {
                            now.setToTime(next.getFromTime() - 1);
                        }
                    }
                    if (this.list.size() == 1) {
                        this.list.get(0).setToTime(2147483647L);
                    } else {
                        Sentence sentence2 = this.list.get(this.list.size() - 1);
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    private int parseOffset(String str) {
        String[] ss = str.split("\\:");
        if (ss.length != 2) {
            return Integer.MAX_VALUE;
        }
        if (ss[0].equalsIgnoreCase("offset")) {
            return Integer.parseInt(ss[1]);
        }
        return Integer.MAX_VALUE;
    }

    private void parseLine(String line) {
        int i;
        if (!line.equals(Utils.EMPTY_STRING)) {
            Matcher matcher = pattern.matcher(line);
            List<String> temp = new ArrayList<>();
            int lastIndex = -1;
            int lastLength = -1;
            while (matcher.find()) {
                String s = matcher.group();
                int index = line.indexOf("[" + s + "]");
                if (lastIndex != -1 && index - lastIndex > lastLength + 2) {
                    String content = line.substring(lastIndex + lastLength + 2, index);
                    for (String str : temp) {
                        long t = parseTime(str);
                        if (t != -1) {
                            this.list.add(new Sentence(content, t));
                        }
                    }
                    temp.clear();
                }
                temp.add(s);
                lastIndex = index;
                lastLength = s.length();
            }
            if (!temp.isEmpty()) {
                int length = lastLength + 2 + lastIndex;
                try {
                    if (length > line.length()) {
                        i = line.length();
                    } else {
                        i = length;
                    }
                    String content2 = line.substring(i).trim();
                    if (!content2.equals(Utils.EMPTY_STRING) || this.mOffset != 0) {
                        for (String s2 : temp) {
                            long t2 = parseTime(s2);
                            if (t2 != -1) {
                                this.list.add(new Sentence(content2, t2));
                            }
                        }
                        return;
                    }
                    for (String s3 : temp) {
                        int of = parseOffset(s3);
                        if (of != Integer.MAX_VALUE) {
                            this.mOffset = of;
                            return;
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    private long parseTime(String time) {
        String[] ss = time.split("\\:|\\.");
        if (ss.length < 2) {
            return -1;
        }
        if (ss.length == 2) {
            try {
                if (this.mOffset != 0 || !ss[0].equalsIgnoreCase("offset")) {
                    int min = Integer.parseInt(ss[0]);
                    int sec = Integer.parseInt(ss[1]);
                    if (min >= 0 && sec >= 0 && sec < 60) {
                        return ((long) ((min * 60) + sec)) * 1000;
                    }
                    throw new RuntimeException("illigal format");
                }
                this.mOffset = Integer.parseInt(ss[1]);
                return -1;
            } catch (Exception e) {
                return -1;
            }
        } else if (ss.length != 3) {
            return -1;
        } else {
            try {
                int min2 = Integer.parseInt(ss[0]);
                int sec2 = Integer.parseInt(ss[1]);
                int mm = Integer.parseInt(ss[2]);
                if (min2 >= 0 && sec2 >= 0 && sec2 < 60 && mm >= 0 && mm <= 99) {
                    return (((long) ((min2 * 60) + sec2)) * 1000) + ((long) (mm * 10));
                }
                throw new RuntimeException("illigal format");
            } catch (Exception e2) {
                return -1;
            }
        }
    }
}
