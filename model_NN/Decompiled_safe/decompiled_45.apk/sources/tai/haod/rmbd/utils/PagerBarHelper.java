package tai.haod.rmbd.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import tai.haod.rmbd.R;

public class PagerBarHelper {
    private Animation mAnimAppear;
    private Animation mAnimDisappear;
    private Button mNextBtn = null;
    private Button mPrevBtn = null;
    private TextView mTVPageNo = null;
    private View mView = null;

    public PagerBarHelper(View rootView, Context ctx) {
        this.mView = rootView;
        this.mAnimDisappear = AnimationUtils.loadAnimation(ctx, R.anim.footer_disappear);
        this.mAnimAppear = AnimationUtils.loadAnimation(ctx, R.anim.footer_appear);
        this.mPrevBtn = (Button) rootView.findViewById(R.id.PrevBtn);
        this.mNextBtn = (Button) rootView.findViewById(R.id.NextBtn);
        this.mTVPageNo = (TextView) rootView.findViewById(R.id.PageNo);
    }

    public void showFooter() {
        if (this.mView.getVisibility() != 0) {
            this.mView.setVisibility(0);
            this.mView.startAnimation(this.mAnimAppear);
        }
    }

    public void hideFooter() {
        if (this.mView.getVisibility() != 8) {
            this.mView.setVisibility(8);
            this.mView.startAnimation(this.mAnimDisappear);
        }
    }

    public void hideFooterNoAnim() {
        this.mView.setVisibility(8);
    }

    public void setPageNo(int no) {
        this.mTVPageNo.setText(new StringBuilder().append(no).toString());
    }

    public void showNext() {
        this.mNextBtn.setVisibility(0);
    }

    public void hideNext() {
        this.mNextBtn.setVisibility(4);
    }

    public void showPrev() {
        this.mPrevBtn.setVisibility(0);
    }

    public void hidePrev() {
        this.mPrevBtn.setVisibility(4);
    }

    public void setNextOnClickListener(View.OnClickListener l) {
        this.mNextBtn.setOnClickListener(l);
    }

    public void setPrevOnClickListener(View.OnClickListener l) {
        this.mPrevBtn.setOnClickListener(l);
    }
}
